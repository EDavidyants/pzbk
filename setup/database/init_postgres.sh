#!/usr/bin/env bash
psql -U postgres -c "CREATE ROLE pzbk LOGIN PASSWORD '1234' NOINHERIT CREATEDB;"
createdb -U postgres pzbk
psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE pzbk TO pzbk"
