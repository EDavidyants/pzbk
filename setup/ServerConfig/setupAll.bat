REM setup JDBC
cmd /C asadmin create-jdbc-connection-pool --datasourceclassname org.postgresql.ds.PGConnectionPoolDataSource --restype javax.sql.ConnectionPoolDataSource --property user=pzbk:password=password:url="jdbc\:postgresql\://localhost\:5432/pzbk" PgrePool
cmd /C asadmin create-jdbc-resource --connectionpoolid PgrePool jdbc/pzbk

REM setup Realm for authentification
cmd /C asadmin delete-auth-realm jdbcRealm
cmd /C asadmin create-auth-realm --classname com.sun.enterprise.security.auth.realm.jdbc.JDBCRealm --property user-name-column=qnumber:password-column=password:group-table-user-name-column=username:group-name-column=rolename:datasource-jndi="jdbc/pzbk":group-table=T_ROLE_USER:user-table=T_USERS:jaas-context=jdbcRealm:digestrealm-password-enc-algorithm=sha-256 jdbcRealm

REM setup EJB Timer 
cmd /C asadmin set configs.config.server-config.ejb-container.ejb-timer-service.timer-datasource=jdbc/pzbk

REM setup mail
cmd /C asadmin create-javamail-resource --mailhost testing.pzbk@gmail.com --mailuser testing.pzbk@gmail.com --fromaddress testing.pzbk@gmail.com --storeprotocolclass com.sun.mail.imap.IMAPSSLStore --transprotocolclass com.sun.mail.smtp.SMTPSSLTransport --property mail-smtp-password="fridawebocr1!":mail-smtp-auth=true:mail-smtp-port=465:mail-smtp-starttls-enable=true:mail-smtp-socketFactory-class=javax.net.ssl.SSLSocketFactory:mail-smtp-user="testing.pzbk@gmail.com":mail-smtp-socketFactory-fallback=false:mail-smtp-host="smtp.gmail.com":mail-smtp-socketFactory-port=465 pzbkMail
pause