CREATE TABLE public."EJB__TIMER__TBL" (
"CREATIONTIMERAW"      BIGINT                 NOT NULL,
"BLOB"                 BYTEA,
"TIMERID"              CHARACTER VARYING(255) NOT NULL,
"CONTAINERID"          BIGINT                 NOT NULL,
"OWNERID"              CHARACTER VARYING(255),
"STATE"                INTEGER                NOT NULL,
"PKHASHCODE"           INTEGER                NOT NULL,
"INTERVALDURATION"     BIGINT                 NOT NULL,
"INITIALEXPIRATIONRAW" BIGINT                 NOT NULL,
"LASTEXPIRATIONRAW"    BIGINT                 NOT NULL,
"SCHEDULE"             CHARACTER VARYING(255),
"APPLICATIONID"        BIGINT                 NOT NULL,
CONSTRAINT "EJB__TIMER__TBL_pkey" PRIMARY KEY ("TIMERID")
);
ALTER TABLE public."EJB__TIMER__TBL" OWNER TO pzbk;

CREATE TABLE dual();

CREATE TABLE t_users (
    qnumber character varying(255) NOT NULL,
    password character varying(255)
);
ALTER TABLE t_users OWNER TO pzbk;
ALTER TABLE ONLY t_users
    ADD CONSTRAINT t_users_pkey PRIMARY KEY (qnumber);
CREATE TABLE localgroup (
    gruppenname character varying(255) NOT NULL
);
ALTER TABLE localgroup OWNER TO pzbk;
ALTER TABLE ONLY localgroup
    ADD CONSTRAINT localgroup_pkey PRIMARY KEY (gruppenname);
CREATE TABLE user_to_group (
    qnumber character varying(255) NOT NULL,
    gruppenname character varying(255) NOT NULL
);
ALTER TABLE user_to_group OWNER TO pzbk;	
ALTER TABLE ONLY user_to_group
    ADD CONSTRAINT user_to_group_pkey PRIMARY KEY (qnumber, gruppenname);	
ALTER TABLE ONLY user_to_group
    ADD CONSTRAINT fk_user_to_group_gruppenname FOREIGN KEY (gruppenname) REFERENCES localgroup(gruppenname);
ALTER TABLE ONLY user_to_group
    ADD CONSTRAINT fk_user_to_group_qnumber FOREIGN KEY (qnumber) REFERENCES t_users(qnumber);