﻿
do $$
DECLARE
    damrow derivatanforderungmodul%rowtype;
    numberOfChanges INTEGER := 0;
    derivatName varchar := 'F39/BMW Basis';
    phase INTEGER = 1;
    damVorher INTEGER;
    ubVorher INTEGER;
    damNachher INTEGER;
    ubNachher INTEGER;
BEGIN

	damVorher := (SELECT count(derivatanforderungmodul.id) FROM derivatanforderungmodul 
INNER JOIN zuordnunganforderungderivat ON derivatanforderungmodul.zuordnunganforderungderivat_id = zuordnunganforderungderivat.id
INNER JOIN derivat ON zuordnunganforderungderivat.derivat_id = derivat.id
WHERE derivat.name LIKE derivatName AND derivatanforderungmodul.status = 3);

ubVorher := (SELECT count(u.id) FROM umsetzungsbestaetigung u, derivatanforderungmodul dam, zuordnunganforderungderivat z, kov_a_phase_im_derivat k, derivat d
WHERE u.modul_id = dam.modul_id 
AND u.anforderung_id = z.anforderung_id 
AND dam.zuordnunganforderungderivat_id = z.id
AND k.derivat_id = z.derivat_id 
AND d.id = k.derivat_id 
AND u.kovaimderivat_id = k.id
AND d.name LIKE derivatName AND k.kov_a_phase = phase
);

    FOR damrow IN SELECT * FROM derivatanforderungmodul WHERE id IN ( SELECT derivatanforderungmodul.id FROM derivatanforderungmodul 
INNER JOIN zuordnunganforderungderivat ON derivatanforderungmodul.zuordnunganforderungderivat_id = zuordnunganforderungderivat.id
INNER JOIN derivat ON zuordnunganforderungderivat.derivat_id = derivat.id
WHERE
derivatanforderungmodul.id NOT IN (
SELECT dam.id FROM umsetzungsbestaetigung u, derivatanforderungmodul dam, zuordnunganforderungderivat z, kov_a_phase_im_derivat k, derivat d
WHERE u.modul_id = dam.modul_id 
AND u.anforderung_id = z.anforderung_id 
AND dam.zuordnunganforderungderivat_id = z.id
AND k.derivat_id = z.derivat_id 
AND d.id = k.derivat_id 
AND u.kovaimderivat_id = k.id
AND d.name LIKE derivatName AND k.kov_a_phase = phase
) AND derivat.name LIKE derivatName AND derivatanforderungmodul.status = 3)    LOOP

	INSERT INTO umsetzungsbestaetigung VALUES (
	nextval('umsetzungsbestaetigung_id_seq'),
	null,
	'',
	0,
	(select anforderung_id from zuordnunganforderungderivat where id = damrow.zuordnunganforderungderivat_id),
	(select id from kov_a_phase_im_derivat where derivat_id = 
	(select derivat_id from zuordnunganforderungderivat where id = damrow.zuordnunganforderungderivat_id)
	and kov_a_phase = phase
	),
	null,
	damrow.modul_id);
	
	numberOfChanges := numberOfChanges + 1;
    END LOOP;

    damNachher := (SELECT count(derivatanforderungmodul.id) FROM derivatanforderungmodul 
INNER JOIN zuordnunganforderungderivat ON derivatanforderungmodul.zuordnunganforderungderivat_id = zuordnunganforderungderivat.id
INNER JOIN derivat ON zuordnunganforderungderivat.derivat_id = derivat.id
WHERE derivat.name LIKE derivatName AND derivatanforderungmodul.status = 3);

ubNachher := (SELECT count(u.id) FROM umsetzungsbestaetigung u, derivatanforderungmodul dam, zuordnunganforderungderivat z, kov_a_phase_im_derivat k, derivat d
WHERE u.modul_id = dam.modul_id 
AND u.anforderung_id = z.anforderung_id 
AND dam.zuordnunganforderungderivat_id = z.id
AND k.derivat_id = z.derivat_id 
AND d.id = k.derivat_id 
AND u.kovaimderivat_id = k.id
AND d.name LIKE derivatName AND k.kov_a_phase = phase
);

        RAISE NOTICE 'Update fuer % abgeschlossen.', derivatName;
        RAISE NOTICE 'Phase %.', phase;
        RAISE NOTICE 'DAM vorher %.', damVorher;
        RAISE NOTICE 'UB vorher %.', ubVorher;
        RAISE NOTICE '% neue Eintraege hinzugefuegt.', numberOfChanges;
        RAISE NOTICE 'DAM nachher %.', damNachher;
        RAISE NOTICE 'UB nachher %.', ubNachher;


END;
$$ LANGUAGE plpgsql;

