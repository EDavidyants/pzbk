-- remove key contraints 
ALTER TABLE ONLY fahrzeugmerkmal DROP CONSTRAINT fahrzeugmerkmal_pkey;
ALTER TABLE ONLY fahrzeugmodul DROP CONSTRAINT fahrzeugmodul_pkey;
ALTER TABLE ONLY fahrzeugprojekt DROP CONSTRAINT fahrzeugprojekt_pkey;
ALTER TABLE ONLY mitarbeiter_derivat DROP CONSTRAINT mitarbeiter_derivat_pkey;
ALTER TABLE ONLY mitarbeiter_modul DROP CONSTRAINT mitarbeiter_modul_pkey;
ALTER TABLE ONLY mitarbeiter DROP CONSTRAINT mitarbeiter_pkey;
ALTER TABLE ONLY mitarbeiter_sensorcoc_lese DROP CONSTRAINT mitarbeiter_sensorcoc_lese_pkey;
ALTER TABLE ONLY mitarbeiter_sensorcoc DROP CONSTRAINT mitarbeiter_sensorcoc_pkey;
ALTER TABLE ONLY mitarbeiter_tteam DROP CONSTRAINT mitarbeiter_tteam_pkey;
ALTER TABLE ONLY sensorcoc_mitarbeiter DROP CONSTRAINT sensorcoc_mitarbeiter_pkey;
ALTER TABLE ONLY user_settings_derivat DROP CONSTRAINT user_settings_derivat_pkey;
ALTER TABLE ONLY user_settings DROP CONSTRAINT user_settings_pkey;
ALTER TABLE ONLY anforderung DROP CONSTRAINT fk_anforderung_fachlicheransprechpartner_qnumber;
ALTER TABLE ONLY anforderung DROP CONSTRAINT fk_anforderung_sensor_qnumber;
ALTER TABLE ONLY anforderung DROP CONSTRAINT fk_anforderung_teamleiter_qnumber;
ALTER TABLE ONLY anhang DROP CONSTRAINT fk_anhang_creator_qnumber;
ALTER TABLE ONLY berechtigung DROP CONSTRAINT fk_berechtigung_mitarbeiter_qnumber;
ALTER TABLE ONLY dbfile DROP CONSTRAINT fk_dbfile_uploader_qnumber;
ALTER TABLE ONLY meldung DROP CONSTRAINT fk_meldung_fachlicheransprechpartner_qnumber;
ALTER TABLE ONLY meldung DROP CONSTRAINT fk_meldung_sensor_qnumber;
ALTER TABLE ONLY meldung DROP CONSTRAINT fk_meldung_teamleiter_qnumber;
ALTER TABLE ONLY mitarbeiter_derivat DROP CONSTRAINT fk_mitarbeiter_derivat_derivatid;
ALTER TABLE ONLY mitarbeiter_derivat DROP CONSTRAINT fk_mitarbeiter_derivat_qnumber;
ALTER TABLE ONLY mitarbeiter_modul DROP CONSTRAINT fk_mitarbeiter_modul_qnumber;
ALTER TABLE ONLY mitarbeiter_sensorcoc_lese DROP CONSTRAINT fk_mitarbeiter_sensorcoc_lese_qnumber;
ALTER TABLE ONLY mitarbeiter_sensorcoc_lese DROP CONSTRAINT fk_mitarbeiter_sensorcoc_lese_sensorcocid;
ALTER TABLE ONLY mitarbeiter_sensorcoc DROP CONSTRAINT fk_mitarbeiter_sensorcoc_qnumber;
ALTER TABLE ONLY mitarbeiter_sensorcoc DROP CONSTRAINT fk_mitarbeiter_sensorcoc_sensorcocid;
ALTER TABLE ONLY mitarbeiter_tteam DROP CONSTRAINT fk_mitarbeiter_tteam_qnumber;
ALTER TABLE ONLY mitarbeiter_tteam DROP CONSTRAINT fk_mitarbeiter_tteam_tteamid;
ALTER TABLE ONLY mitarbeiter DROP CONSTRAINT fk_mitarbeiter_usersettings_id;
ALTER TABLE ONLY mitarbeiter_zakeinordnungen DROP CONSTRAINT fk_mitarbeiter_zakeinordnungen_mitarbeiter_qnumber;
ALTER TABLE ONLY sensorcoc DROP CONSTRAINT fk_sensorcoc_fachlicheransprechpartner_qnumber;
ALTER TABLE ONLY sensorcoc_mitarbeiter DROP CONSTRAINT fk_sensorcoc_mitarbeiter_umsetzungsbestaetiger_qnumber;
ALTER TABLE ONLY t_team DROP CONSTRAINT fk_t_team_teamleiter_qnumber;
ALTER TABLE ONLY user_defined_search DROP CONSTRAINT fk_user_defined_search_mitarbeiter_qnumber;
ALTER TABLE ONLY user_defined_search_search_filter DROP CONSTRAINT fk_user_defined_search_search_filter_userdefinedsearch_id;
ALTER TABLE ONLY user_settings DROP CONSTRAINT fk_user_settings_mitarbeiter_qnumber;
ALTER TABLE ONLY usersettings_selectedderivate DROP CONSTRAINT fk_usersettings_selectedderivate_usersettings_id;
ALTER TABLE ONLY usersettings_selectedzakderivate DROP CONSTRAINT fk_usersettings_selectedzakderivate_usersettings_id;
ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat DROP CONSTRAINT status_vereinbarung_anforderung_im_derivat_anforderer_qnumber;
ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc DROP CONSTRAINT ubzukovaphaseimderivatundsensorcocumsetzungsbestaetiger_qnumber;


-- alter and add columns
ALTER TABLE anforderung ALTER COLUMN beschreibunganforderungde character varying(10000);
ALTER TABLE anforderung ALTER COLUMN beschreibunganforderungen character varying(10000);
ALTER TABLE anforderung ALTER COLUMN beschreibungstaerkeschwaeche character varying(10000);
ALTER TABLE anforderung ALTER COLUMN kommentaranforderung character varying(10000);
ALTER TABLE anforderung ALTER COLUMN loesungsvorschlag character varying(10000);
ALTER TABLE anforderung ALTER COLUMN ursache character varying(10000);
ALTER TABLE anforderung ADD COLUMN bild_id bigint;
ALTER TABLE anforderung ADD COLUMN fachlicheransprechpartner_id bigint;
ALTER TABLE anforderung ADD COLUMN sensor_id bigint;
ALTER TABLE anforderung ADD COLUMN teamleiter_id bigint;
ALTER TABLE anforderung_freigabe_bei_modul ADD COLUMN bearbeiter_id bigint;
ALTER TABLE anforderung_history ALTER COLUMN auf character varying(10000);
ALTER TABLE anforderung_history ALTER COLUMN von character varying(10000);
ALTER TABLE anhang ADD COLUMN creator_id bigint;
ALTER TABLE berechtigung ADD COLUMN mitarbeiter_id bigint;
ALTER TABLE berechtigung_derivat ADD COLUMN mitarbeiter_id bigint;
ALTER TABLE kov_a_phase_im_derivat ADD COLUMN umsetzungsbestaetiger_id bigint;
ALTER TABLE meldung ALTER COLUMN beschreibunganforderungde character varying(10000);
ALTER TABLE meldung ALTER COLUMN beschreibunganforderungen character varying(10000);
ALTER TABLE meldung ALTER COLUMN beschreibungstaerkeschwaeche character varying(10000);
ALTER TABLE meldung ALTER COLUMN kommentaranforderung character varying(10000);
ALTER TABLE meldung ALTER COLUMN loesungsvorschlag character varying(10000);
ALTER TABLE meldung ALTER COLUMN ursache character varying(10000);
ALTER TABLE meldung DROP COLUMN version;
ALTER TABLE meldung ADD COLUMN bild_id bigint;
ALTER TABLE meldung ADD COLUMN fachlicheransprechpartner_id bigint;
ALTER TABLE meldung ADD COLUMN sensor_id bigint;
ALTER TABLE meldung ADD COLUMN teamleiter_id bigint;
ALTER TABLE mitarbeiter ADD COLUMN id bigint NOT NULL;
ALTER TABLE mitarbeiter ALTER COLUMN qnumber character varying(255);
ALTER TABLE mitarbeiter ADD COLUMN mail_receipt_enabled boolean;
ALTER TABLE mitarbeiter_derivat ADD COLUMN mitarbeiter_id bigint NOT NULL;
ALTER TABLE mitarbeiter_derivat RENAME COLUMN derivatid TO derivaten_id;
ALTER TABLE mitarbeiter_modul ADD COLUMN mitarbeiter_id bigint NOT NULL;
ALTER TABLE mitarbeiter_modul RENAME COLUMN modulname TO moduls_id;
ALTER TABLE mitarbeiter_sensorcoc ADD COLUMN mitarbeiter_id bigint NOT NULL;
ALTER TABLE mitarbeiter_sensorcoc RENAME COLUMN sensorcocid TO sensorcoc_sensorcocid;
ALTER TABLE mitarbeiter_sensorcoc_lese ADD COLUMN mitarbeiter_id bigint NOT NULL;
ALTER TABLE mitarbeiter_sensorcoc_lese RENAME COLUMN sensorcocid TO sensorcoc_sensorcocid;
ALTER TABLE mitarbeiter_tteam ADD COLUMN mitarbeiter_id bigint NOT NULL;
ALTER TABLE mitarbeiter_tteam RENAME COLUMN tteamid TO tteams_id;
ALTER TABLE mitarbeiter_zakeinordnungen ADD COLUMN mitarbeiter_id bigint NOT NULL;
ALTER TABLE modul ADD COLUMN modulbild_id bigint;
ALTER TABLE sensorcoc ADD COLUMN fachlicheransprechpartner_id bigint;
ALTER TABLE sensorcoc_mitarbeiter ADD COLUMN umsetzungsbestaetiger_id bigint NOT NULL;
ALTER TABLE status_vereinbarung_anforderung_im_derivat ADD COLUMN anforderer_id bigint;
ALTER TABLE t_team ADD COLUMN teamleiter_id bigint;
ALTER TABLE ub_zu_kova_phase_im_derivat_und_sensorcoc ADD COLUMN umsetzungsbestaetiger_id bigint;
ALTER TABLE umsetzungsbestaetigung ADD COLUMN umsetzungsbestaetiger_id bigint;
ALTER TABLE umsetzungsbestaetigung ALTER COLUMN kommentar character varying(10000);
ALTER TABLE user_defined_search RENAME COLUMN sql_search TO name;
ALTER TABLE wert ALTER COLUMN attribut character varying(10000);

CREATE TABLE modulbild (
    id bigint NOT NULL,
    bild_id bigint,
    modul_id bigint
);
CREATE TABLE searchfilter_stringlistvalue (
    searchfilter_id bigint,
    stringlistvalue character varying(255)
);

-- drop not used tables
DROP TABLE fahrzeugmerkmal;
DROP TABLE fahrzeugmodul;
DROP TABLE fahrzeugprojekt;



-- perform migration of qnumber to id

-- first draft for insert of the id column
DECLARE 
   t_curs cursor for select * from mitarbeiter;
   t_row mitarbeiter%rowtype;
BEGIN
    FOR t_row in t_curs LOOP
		IF EXISTS (SELECT MAX(m.id) FROM mitarbeiter)
		BEGIN
			UPDATE mitarbeiter SET id = SELECT MAX(m.id) FROM mitarbeiter + 1 WHERE current of t_curs;
		END
		ELSE
		BEGIN
			UPDATE mitarbeiter SET id = 1 WHERE current of t_curs;
		END
    END LOOP;
END;





-- perform migration of user settings







-- perform migration of bild ids












-- drop columns related to qnumber
ALTER TABLE anforderung DROP COLUMN fachlicheransprechpartner_qnumber;
ALTER TABLE anforderung DROP COLUMN sensor_qnumber;
ALTER TABLE anforderung DROP COLUMN teamleiter_qnumber;
ALTER TABLE anhang DROP COLUMN creator_qnumber;
ALTER TABLE berechtigung DROP COLUMN mitarbeiter_qnumber;
ALTER TABLE berechtigung_derivat DROP COLUMN mitarbeiter_qnumber;
ALTER TABLE dbfile DROP COLUMN uploader_qnumber;
ALTER TABLE kov_a_phase_im_derivat DROP COLUMN umsetzungsbestaetiger_qnumber;
ALTER TABLE meldung DROP COLUMN fachlicheransprechpartner_qnumber;
ALTER TABLE meldung DROP COLUMN sensor_qnumber;
ALTER TABLE meldung DROP COLUMN teamleiter_qnumber;
ALTER TABLE mitarbeiter_derivat DROP COLUMN qnumber;
ALTER TABLE mitarbeiter_modul DROP COLUMN qnumber;
ALTER TABLE mitarbeiter_sensorcoc DROP COLUMN qnumber;
ALTER TABLE mitarbeiter_sensorcoc_lese DROP COLUMN qnumber;
ALTER TABLE mitarbeiter_tteam DROP COLUMN qnumber;
ALTER TABLE mitarbeiter_zakeinordnungen DROP COLUMN qnumber;
ALTER TABLE sensorcoc DROP COLUMN fachlicheransprechpartner_qnumber;
ALTER TABLE sensorcoc_mitarbeiter DROP COLUMN umsetzungsbestaetiger_qnumber;
ALTER TABLE status_vereinbarung_anforderung_im_derivat DROP COLUMN anforderer_qnumber;
ALTER TABLE t_team DROP COLUMN teamleiter_qnumber;
ALTER TABLE ub_zu_kova_phase_im_derivat_und_sensorcoc DROP COLUMN umsetzungsbestaetiger_qnumber;
ALTER TABLE umsetzungsbestaetigung DROP COLUMN umsetzungsbestaetiger_qnumber;













-- neue zuordnung zur anforderung oder meldung
ALTER TABLE bild DROP COLUMN anhangid;
ALTER TABLE bild DROP COLUMN objectid;
-- -----------------------------------------------------------------------

-- migration der user settings in user
DROP TABLE user_settings;
DROP TABLE user_settings_derivat;
ALTER TABLE mitarbeiter DROP COLUMN usersettings_id;













-- add new contraints
ALTER TABLE ONLY mitarbeiter_derivat ADD CONSTRAINT mitarbeiter_derivat_pkey PRIMARY KEY (mitarbeiter_id, derivaten_id);
ALTER TABLE ONLY mitarbeiter_modul ADD CONSTRAINT mitarbeiter_modul_pkey PRIMARY KEY (mitarbeiter_id, moduls_id);
ALTER TABLE ONLY mitarbeiter ADD CONSTRAINT mitarbeiter_pkey PRIMARY KEY (id);	
ALTER TABLE ONLY mitarbeiter_sensorcoc_lese ADD CONSTRAINT mitarbeiter_sensorcoc_lese_pkey PRIMARY KEY (mitarbeiter_id, sensorcocslese_sensorcocid);
ALTER TABLE ONLY mitarbeiter_sensorcoc ADD CONSTRAINT mitarbeiter_sensorcoc_pkey PRIMARY KEY (mitarbeiter_id, sensorcoc_sensorcocid);	
ALTER TABLE ONLY mitarbeiter_tteam ADD CONSTRAINT mitarbeiter_tteam_pkey PRIMARY KEY (mitarbeiter_id, tteams_id);
ALTER TABLE ONLY modulbild ADD CONSTRAINT modulbild_pkey PRIMARY KEY (id);
ALTER TABLE ONLY sensorcoc_mitarbeiter ADD CONSTRAINT sensorcoc_mitarbeiter_pkey PRIMARY KEY (sensorcoc_sensorcocid, umsetzungsbestaetiger_id);
ALTER TABLE ONLY anforderung ADD CONSTRAINT fk_anforderung_bild_id FOREIGN KEY (bild_id) REFERENCES bild(id);
ALTER TABLE ONLY anforderung ADD CONSTRAINT fk_anforderung_fachlicheransprechpartner_id FOREIGN KEY (fachlicheransprechpartner_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY anforderung_freigabe_bei_modul ADD CONSTRAINT fk_anforderung_freigabe_bei_modul_bearbeiter_id FOREIGN KEY (bearbeiter_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY anforderung_freigabe_bei_modul ADD CONSTRAINT fk_anforderung_freigabe_bei_modul_modul_id FOREIGN KEY (modul_id) REFERENCES modul(id);
ALTER TABLE ONLY anforderung ADD CONSTRAINT fk_anforderung_sensor_id FOREIGN KEY (sensor_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY anforderung ADD CONSTRAINT fk_anforderung_teamleiter_id FOREIGN KEY (teamleiter_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY anhang ADD CONSTRAINT fk_anhang_creator_id FOREIGN KEY (creator_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY berechtigung_derivat ADD CONSTRAINT fk_berechtigung_derivat_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY berechtigung ADD CONSTRAINT fk_berechtigung_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY meldung ADD CONSTRAINT fk_meldung_fachlicheransprechpartner_id FOREIGN KEY (fachlicheransprechpartner_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY meldung ADD CONSTRAINT fk_meldung_bild_id FOREIGN KEY (bild_id) REFERENCES bild(id);
ALTER TABLE ONLY meldung ADD CONSTRAINT fk_meldung_sensor_id FOREIGN KEY (sensor_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY meldung ADD CONSTRAINT fk_meldung_teamleiter_id FOREIGN KEY (teamleiter_id) REFERENCES mitarbeiter(id)
ALTER TABLE ONLY mitarbeiter_derivat ADD CONSTRAINT fk_mitarbeiter_derivat_derivaten_id FOREIGN KEY (derivaten_id) REFERENCES derivat(id);
ALTER TABLE ONLY mitarbeiter_derivat ADD CONSTRAINT fk_mitarbeiter_derivat_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY mitarbeiter_modul ADD CONSTRAINT fk_mitarbeiter_modul_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY mitarbeiter_modul ADD CONSTRAINT fk_mitarbeiter_modul_moduls_id FOREIGN KEY (moduls_id) REFERENCES modul(id);
ALTER TABLE ONLY mitarbeiter_sensorcoc_lese ADD CONSTRAINT fk_mitarbeiter_sensorcoc_lese_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY mitarbeiter_sensorcoc_lese ADD CONSTRAINT fk_mitarbeiter_sensorcoc_lese_sensorcocslese_sensorcocid FOREIGN KEY (sensorcocslese_sensorcocid) REFERENCES sensorcoc(sensorcocid);
ALTER TABLE ONLY mitarbeiter_sensorcoc ADD CONSTRAINT fk_mitarbeiter_sensorcoc_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY mitarbeiter_sensorcoc ADD CONSTRAINT fk_mitarbeiter_sensorcoc_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);
ALTER TABLE ONLY mitarbeiter_tteam ADD CONSTRAINT fk_mitarbeiter_tteam_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY mitarbeiter_tteam ADD CONSTRAINT fk_mitarbeiter_tteam_tteams_id FOREIGN KEY (tteams_id) REFERENCES t_team(id);
ALTER TABLE ONLY mitarbeiter_zakeinordnungen ADD CONSTRAINT fk_mitarbeiter_zakeinordnungen_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY modul ADD CONSTRAINT fk_modul_modulbild_id FOREIGN KEY (modulbild_id) REFERENCES bild(id);
ALTER TABLE ONLY modulbild ADD CONSTRAINT fk_modulbild_bild_id FOREIGN KEY (bild_id) REFERENCES anhang(id);
ALTER TABLE ONLY modulbild ADD CONSTRAINT fk_modulbild_modul_id FOREIGN KEY (modul_id) REFERENCES modul(id);
ALTER TABLE ONLY searchfilter_stringlistvalue ADD CONSTRAINT fk_searchfilter_stringlistvalue_searchfilter_id FOREIGN KEY (searchfilter_id) REFERENCES search_filter(id);
ALTER TABLE ONLY sensorcoc ADD CONSTRAINT fk_sensorcoc_fachlicheransprechpartner_id FOREIGN KEY (fachlicheransprechpartner_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY sensorcoc_mitarbeiter ADD CONSTRAINT fk_sensorcoc_mitarbeiter_umsetzungsbestaetiger_id FOREIGN KEY (umsetzungsbestaetiger_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat ADD CONSTRAINT fk_status_vereinbarung_anforderung_im_derivat_anforderer_id FOREIGN KEY (anforderer_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat ADD CONSTRAINT fk_status_vereinbarung_anforderung_im_derivat_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);
ALTER TABLE ONLY t_team ADD CONSTRAINT fk_t_team_teamleiter_id FOREIGN KEY (teamleiter_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY umsetzungsbestaetigung ADD CONSTRAINT fk_umsetzungsbestaetigung_umsetzungsbestaetiger_id FOREIGN KEY (umsetzungsbestaetiger_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY user_defined_search ADD CONSTRAINT fk_user_defined_search_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY user_defined_search_search_filter ADD CONSTRAINT fk_user_defined_search_search_filter_searchfilterlist_id FOREIGN KEY (searchfilterlist_id) REFERENCES search_filter(id);
ALTER TABLE ONLY user_defined_search_search_filter ADD CONSTRAINT fk_user_defined_search_search_filter_userdefinedsearch_id FOREIGN KEY (userdefinedsearch_id) REFERENCES user_defined_search(id);
ALTER TABLE ONLY usersettings_selectedderivate ADD CONSTRAINT fk_usersettings_selectedderivate_usersettings_id FOREIGN KEY (usersettings_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY usersettings_selectedzakderivate ADD CONSTRAINT fk_usersettings_selectedzakderivate_usersettings_id FOREIGN KEY (usersettings_id) REFERENCES mitarbeiter(id);
ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc ADD CONSTRAINT ubzukova_phase_im_derivat_und_sensorcocumsetzungsbestaetiger_id FOREIGN KEY (umsetzungsbestaetiger_id) REFERENCES mitarbeiter(id);

-- finish