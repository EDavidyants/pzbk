--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: EJB__TIMER__TBL; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE "EJB__TIMER__TBL" (
    "CREATIONTIMERAW" bigint NOT NULL,
    "BLOB" bytea,
    "TIMERID" character varying(255) NOT NULL,
    "CONTAINERID" bigint NOT NULL,
    "OWNERID" character varying(255),
    "STATE" integer NOT NULL,
    "PKHASHCODE" integer NOT NULL,
    "INTERVALDURATION" bigint NOT NULL,
    "INITIALEXPIRATIONRAW" bigint NOT NULL,
    "LASTEXPIRATIONRAW" bigint NOT NULL,
    "SCHEDULE" character varying(255),
    "APPLICATIONID" bigint NOT NULL
);


ALTER TABLE "EJB__TIMER__TBL" OWNER TO qqc01x3;

--
-- Name: T_ROLE_GROUP; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE "T_ROLE_GROUP" (
    id integer,
    rolename character varying(255),
    groupname character varying(255)
);


ALTER TABLE "T_ROLE_GROUP" OWNER TO qqc01x3;

--
-- Name: T_ROLE_USER; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE "T_ROLE_USER" (
    id integer,
    rolename character varying(255),
    username character varying(255)
);


ALTER TABLE "T_ROLE_USER" OWNER TO qqc01x3;

--
-- Name: anforderung; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE anforderung (
    id bigint NOT NULL,
    aenderungsdatum timestamp without time zone,
    beschreibunganforderungde character varying(1024),
    beschreibunganforderungen character varying(1024),
    beschreibungstaerkeschwaeche character varying(1024),
    designrelevant boolean,
    erstellungsdatum timestamp without time zone,
    fachid character varying(255),
    kommentaranforderung character varying(1024),
    loesungsvorschlag character varying(1024),
    phasenbezug boolean,
    potentialstandardisierung boolean,
    prioinbl character varying(255),
    referenzen character varying(255),
    staerkeschwaeche boolean,
    status integer,
    statuswechselkommentar character varying(255),
    umsetzenderbereich character varying(255),
    ursache character varying(1024),
    version integer,
    zeitpktumsetzungsbest boolean,
    einheit character varying(255),
    kommentar character varying(10000),
    wert character varying(255),
    fachlicheransprechpartner_qnumber character varying(255),
    sensor_qnumber character varying(255),
    sensorcoc_sensorcocid bigint,
    teamleiter_qnumber character varying(255),
    tteam_id bigint
);


ALTER TABLE anforderung OWNER TO qqc01x3;

--
-- Name: anforderung_anforderung_freigabe_bei_modul; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE anforderung_anforderung_freigabe_bei_modul (
    anforderung_id bigint NOT NULL,
    anfofreigabebeimodul_id bigint NOT NULL
);


ALTER TABLE anforderung_anforderung_freigabe_bei_modul OWNER TO qqc01x3;

--
-- Name: anforderung_anhang; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE anforderung_anhang (
    anforderung_id bigint NOT NULL,
    anhaenge_id bigint NOT NULL
);


ALTER TABLE anforderung_anhang OWNER TO qqc01x3;

--
-- Name: anforderung_auswirkung; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE anforderung_auswirkung (
    anforderung_id bigint NOT NULL,
    auswirkungen_id bigint NOT NULL
);


ALTER TABLE anforderung_auswirkung OWNER TO qqc01x3;

--
-- Name: anforderung_festgestelltin; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE anforderung_festgestelltin (
    anforderung_id bigint NOT NULL,
    festgestelltin_id bigint NOT NULL
);


ALTER TABLE anforderung_festgestelltin OWNER TO qqc01x3;

--
-- Name: anforderung_freigabe_bei_modul; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE anforderung_freigabe_bei_modul (
    id bigint NOT NULL,
    abgelehnt boolean,
    datum timestamp without time zone,
    freigabe boolean,
    hauptmodul boolean,
    kommentar character varying(255),
    vereinbarung integer,
    zak boolean,
    anforderung_id bigint,
    modul_id bigint
);


ALTER TABLE anforderung_freigabe_bei_modul OWNER TO qqc01x3;

--
-- Name: anforderung_history; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE anforderung_history (
    id bigint NOT NULL,
    anforderung_id bigint,
    auf character varying(1024),
    benutzer character varying(255),
    datum timestamp without time zone,
    kennzeichen character varying(255),
    objekt_name character varying(255),
    objekt_type character varying(255),
    version character varying(255),
    von character varying(1024)
);


ALTER TABLE anforderung_history OWNER TO qqc01x3;

--
-- Name: anforderung_umsetzer; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE anforderung_umsetzer (
    anforderung_id bigint NOT NULL,
    umsetzer_umsetzer_id bigint NOT NULL
);


ALTER TABLE anforderung_umsetzer OWNER TO qqc01x3;

--
-- Name: anhang; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE anhang (
    id bigint NOT NULL,
    dateiname character varying(255),
    dateityp character varying(255),
    filesize integer,
    isstandardbild boolean,
    zeitstempel timestamp without time zone,
    content_id bigint,
    creator_qnumber character varying(255)
);


ALTER TABLE anhang OWNER TO qqc01x3;

--
-- Name: anwender; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE anwender (
    qnumber integer NOT NULL,
    email character varying(255),
    kurzzeichen character varying(255),
    name character varying(255),
    tel character varying(255)
);


ALTER TABLE anwender OWNER TO qqc01x3;

--
-- Name: auswirkung; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE auswirkung (
    id bigint NOT NULL,
    auswirkung character varying(10000),
    wert character varying(10000)
);


ALTER TABLE auswirkung OWNER TO qqc01x3;

--
-- Name: berechtigung; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE berechtigung (
    id bigint NOT NULL,
    fuer integer,
    fuerid character varying(255),
    rechttype integer,
    rolle integer,
    mitarbeiter_qnumber character varying(255)
);


ALTER TABLE berechtigung OWNER TO qqc01x3;

--
-- Name: berechtigung_derivat; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE berechtigung_derivat (
    id bigint NOT NULL,
    derivat_id bigint,
    mitarbeiter_qnumber character varying(255),
    rechttype integer,
    rolle integer
);


ALTER TABLE berechtigung_derivat OWNER TO qqc01x3;

--
-- Name: berechtigunghistory; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE berechtigunghistory (
    id bigint NOT NULL,
    berechtigter character varying(255),
    berechtigter_qnumber character varying(255),
    datum timestamp without time zone,
    erteilt boolean,
    fuer character varying(255),
    ziel_id character varying(255),
    rechttype integer,
    rolle character varying(255),
    berechtigt_von character varying(255),
    berechtigt_von_qnumber character varying(255)
);


ALTER TABLE berechtigunghistory OWNER TO qqc01x3;

--
-- Name: bild; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE bild (
    id bigint NOT NULL,
    anhangid bigint,
    largeimage bytea,
    normalimage bytea,
    objectid bigint,
    thumbnailimage bytea
);


ALTER TABLE bild OWNER TO qqc01x3;

--
-- Name: dbfile; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE dbfile (
    id bigint NOT NULL,
    category character varying(255),
    filecontent bytea,
    creationdate timestamp without time zone,
    filename character varying(255),
    filetype character varying(255),
    uploader_qnumber character varying(255)
);


ALTER TABLE dbfile OWNER TO qqc01x3;

--
-- Name: derivat; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE derivat (
    id bigint NOT NULL,
    bezeichnung character varying(255),
    name character varying(255),
    produktlinie character varying(255),
    startofproduction date
);


ALTER TABLE derivat OWNER TO qqc01x3;

--
-- Name: derivat_kov_a_phase_im_derivat; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE derivat_kov_a_phase_im_derivat (
    derivat_id bigint NOT NULL,
    kovaphasen_id bigint NOT NULL
);


ALTER TABLE derivat_kov_a_phase_im_derivat OWNER TO qqc01x3;

--
-- Name: dual; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE dual (
);


ALTER TABLE dual OWNER TO qqc01x3;

--
-- Name: fahrzeugmerkmal; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE fahrzeugmerkmal (
    id bigint NOT NULL,
    kategorie character varying(255),
    wert character varying(255)
);


ALTER TABLE fahrzeugmerkmal OWNER TO qqc01x3;

--
-- Name: fahrzeugmodul; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE fahrzeugmodul (
    id bigint NOT NULL,
    bereich character varying(255),
    modul character varying(255),
    ppg character varying(255),
    seteam character varying(255)
);


ALTER TABLE fahrzeugmodul OWNER TO qqc01x3;

--
-- Name: fahrzeugprojekt; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE fahrzeugprojekt (
    id bigint NOT NULL,
    status integer
);


ALTER TABLE fahrzeugprojekt OWNER TO qqc01x3;

--
-- Name: festgestelltin; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE festgestelltin (
    id bigint NOT NULL,
    wert character varying(10000)
);


ALTER TABLE festgestelltin OWNER TO qqc01x3;

--
-- Name: infotext; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE infotext (
    id bigint NOT NULL,
    beschreibungstext character varying(1024),
    feldbeschreibunglang character varying(255),
    feldname character varying(255)
);


ALTER TABLE infotext OWNER TO qqc01x3;

--
-- Name: kov_a_phase_im_derivat; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE kov_a_phase_im_derivat (
    id bigint NOT NULL,
    end_date date,
    kov_a_phase integer,
    status integer,
    start_date date,
    umsetzungsbestaetiger_qnumber character varying(255),
    derivat_id bigint
);


ALTER TABLE kov_a_phase_im_derivat OWNER TO qqc01x3;

--
-- Name: localgroup; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE localgroup (
    gruppenname character varying(255) NOT NULL
);


ALTER TABLE localgroup OWNER TO qqc01x3;

--
-- Name: meldung; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE meldung (
    id bigint NOT NULL,
    aenderungsdatum timestamp without time zone,
    assignedtoanforderung boolean,
    beschreibunganforderungde character varying(1024),
    beschreibunganforderungen character varying(1024),
    beschreibungstaerkeschwaeche character varying(1024),
    designrelevant boolean,
    erstellungsdatum timestamp without time zone,
    fachid character varying(255),
    kommentaranforderung character varying(1024),
    loesungsvorschlag character varying(1024),
    prioinbl character varying(255),
    referenzen character varying(255),
    staerkeschwaeche boolean,
    status integer,
    statuswechselkommentar character varying(255),
    umsetzenderbereich character varying(255),
    ursache character varying(1024),
    version integer,
    einheit character varying(255),
    kommentar character varying(10000),
    wert character varying(255),
    fachlicheransprechpartner_qnumber character varying(255),
    sensor_qnumber character varying(255),
    sensorcoc_sensorcocid bigint,
    teamleiter_qnumber character varying(255),
    tteam_id bigint
);


ALTER TABLE meldung OWNER TO qqc01x3;

--
-- Name: meldung_anforderung; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE meldung_anforderung (
    anforderung_id bigint NOT NULL,
    meldung_id bigint NOT NULL
);


ALTER TABLE meldung_anforderung OWNER TO qqc01x3;

--
-- Name: meldung_anhang; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE meldung_anhang (
    meldung_id bigint NOT NULL,
    anhaenge_id bigint NOT NULL
);


ALTER TABLE meldung_anhang OWNER TO qqc01x3;

--
-- Name: meldung_auswirkung; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE meldung_auswirkung (
    meldung_id bigint NOT NULL,
    auswirkungen_id bigint NOT NULL
);


ALTER TABLE meldung_auswirkung OWNER TO qqc01x3;

--
-- Name: meldung_festgestelltin; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE meldung_festgestelltin (
    meldung_id bigint NOT NULL,
    festgestelltin_id bigint NOT NULL
);


ALTER TABLE meldung_festgestelltin OWNER TO qqc01x3;

--
-- Name: mitarbeiter; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE mitarbeiter (
    qnumber character varying(255) NOT NULL,
    abteilung character varying(255),
    email character varying(255),
    kurzzeichen character varying(255),
    nachname character varying(255),
    tel character varying(255),
    vorname character varying(255),
    usersettings_id bigint
);


ALTER TABLE mitarbeiter OWNER TO qqc01x3;

--
-- Name: mitarbeiter_derivat; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE mitarbeiter_derivat (
    qnumber character varying(255) NOT NULL,
    derivatid bigint NOT NULL
);


ALTER TABLE mitarbeiter_derivat OWNER TO qqc01x3;

--
-- Name: mitarbeiter_modul; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE mitarbeiter_modul (
    qnumber character varying(255) NOT NULL,
    modulname character varying(255) NOT NULL
);


ALTER TABLE mitarbeiter_modul OWNER TO qqc01x3;

--
-- Name: mitarbeiter_sensorcoc; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE mitarbeiter_sensorcoc (
    qnumber character varying(255) NOT NULL,
    sensorcocid bigint NOT NULL
);


ALTER TABLE mitarbeiter_sensorcoc OWNER TO qqc01x3;

--
-- Name: mitarbeiter_sensorcoc_lese; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE mitarbeiter_sensorcoc_lese (
    qnumber character varying(255) NOT NULL,
    sensorcocid bigint NOT NULL
);


ALTER TABLE mitarbeiter_sensorcoc_lese OWNER TO qqc01x3;

--
-- Name: mitarbeiter_tteam; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE mitarbeiter_tteam (
    qnumber character varying(255) NOT NULL,
    tteamid bigint NOT NULL
);


ALTER TABLE mitarbeiter_tteam OWNER TO qqc01x3;

--
-- Name: mitarbeiter_zakeinordnungen; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE mitarbeiter_zakeinordnungen (
    mitarbeiter_qnumber character varying(255),
    zakeinordnungen character varying(255)
);


ALTER TABLE mitarbeiter_zakeinordnungen OWNER TO qqc01x3;

--
-- Name: modul; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE modul (
    id bigint NOT NULL,
    beschreibung character varying(255),
    fachbereich character varying(255),
    name character varying(255)
);


ALTER TABLE modul OWNER TO qqc01x3;

--
-- Name: modul_modulseteam; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE modul_modulseteam (
    modul_id bigint NOT NULL,
    seteams_id bigint NOT NULL
);


ALTER TABLE modul_modulseteam OWNER TO qqc01x3;

--
-- Name: modulkomponente; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE modulkomponente (
    id bigint NOT NULL,
    name character varying(255),
    ppg character varying(255),
    seteam_id bigint
);


ALTER TABLE modulkomponente OWNER TO qqc01x3;

--
-- Name: modulseteam; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE modulseteam (
    id bigint NOT NULL,
    name character varying(255),
    elternmodul_id bigint
);


ALTER TABLE modulseteam OWNER TO qqc01x3;

--
-- Name: modulseteam_modulkomponente; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE modulseteam_modulkomponente (
    modulseteam_id bigint NOT NULL,
    komponenten_id bigint NOT NULL
);


ALTER TABLE modulseteam_modulkomponente OWNER TO qqc01x3;

--
-- Name: pzbk; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE pzbk (
    id bigint NOT NULL,
    pzbkid character varying(255)
);


ALTER TABLE pzbk OWNER TO qqc01x3;

--
-- Name: search_filter; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE search_filter (
    id bigint NOT NULL,
    booleanvalue boolean,
    enddatevalue timestamp without time zone,
    filtertype integer,
    startdatevalue timestamp without time zone
);


ALTER TABLE search_filter OWNER TO qqc01x3;

--
-- Name: searchfilter_longlistvalue; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE searchfilter_longlistvalue (
    searchfilter_id bigint,
    longlistvalue bigint
);


ALTER TABLE searchfilter_longlistvalue OWNER TO qqc01x3;

--
-- Name: searchfilter_statuslistvalue; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE searchfilter_statuslistvalue (
    searchfilter_id bigint,
    statuslistvalue character varying(255)
);


ALTER TABLE searchfilter_statuslistvalue OWNER TO qqc01x3;

--
-- Name: sensorcoc; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE sensorcoc (
    sensorcocid bigint NOT NULL,
    ortung character varying(255),
    ressort character varying(255),
    technologie character varying(255),
    zakeinordnung character varying(255),
    fachlicheransprechpartner_qnumber character varying(255)
);


ALTER TABLE sensorcoc OWNER TO qqc01x3;

--
-- Name: sensorcoc_mitarbeiter; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE sensorcoc_mitarbeiter (
    sensorcoc_sensorcocid bigint NOT NULL,
    umsetzungsbestaetiger_qnumber character varying(255) NOT NULL
);


ALTER TABLE sensorcoc_mitarbeiter OWNER TO qqc01x3;

--
-- Name: sequence; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE sequence (
    seq_name character varying(50) NOT NULL,
    seq_count numeric(38,0)
);


ALTER TABLE sequence OWNER TO qqc01x3;

--
-- Name: status_vereinbarung_anforderung_im_derivat; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE status_vereinbarung_anforderung_im_derivat (
    id bigint NOT NULL,
    nach_zak boolean,
    anforderer_qnumber character varying(255),
    anforderung_id bigint,
    derivat_id bigint
);


ALTER TABLE status_vereinbarung_anforderung_im_derivat OWNER TO qqc01x3;

--
-- Name: statusnext; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE statusnext (
    id integer NOT NULL,
    statusid integer,
    nextstatusid integer
);


ALTER TABLE statusnext OWNER TO qqc01x3;

--
-- Name: t_role_group; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE t_role_group (
    id bigint NOT NULL,
    groupname character varying(255),
    rolename character varying(255)
);


ALTER TABLE t_role_group OWNER TO qqc01x3;

--
-- Name: t_role_user; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE t_role_user (
    id bigint NOT NULL,
    rolename character varying(255),
    username character varying(255)
);


ALTER TABLE t_role_user OWNER TO qqc01x3;

--
-- Name: t_team; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE t_team (
    id bigint NOT NULL,
    team_name character varying(255),
    teamleiter_qnumber character varying(255)
);


ALTER TABLE t_team OWNER TO qqc01x3;

--
-- Name: t_users; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE t_users (
    qnumber character varying(255) NOT NULL,
    password character varying(255)
);


ALTER TABLE t_users OWNER TO qqc01x3;

--
-- Name: ub_zu_kova_phase_im_derivat_und_sensorcoc; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE ub_zu_kova_phase_im_derivat_und_sensorcoc (
    id bigint NOT NULL,
    kovaimderivat_id bigint,
    sensorcoc_sensorcocid bigint,
    umsetzungsbestaetiger_qnumber character varying(255)
);


ALTER TABLE ub_zu_kova_phase_im_derivat_und_sensorcoc OWNER TO qqc01x3;

--
-- Name: umsetzer; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE umsetzer (
    umsetzer_id bigint NOT NULL,
    komponente_id bigint,
    seteam_id bigint
);


ALTER TABLE umsetzer OWNER TO qqc01x3;

--
-- Name: umsetzungsbestaetigung; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE umsetzungsbestaetigung (
    id bigint NOT NULL,
    datum timestamp without time zone,
    kommentar character varying(1024),
    status integer,
    umsetzungsbestaetiger_qnumber character varying(255),
    anforderung_id bigint,
    kovaimderivat_id bigint
);


ALTER TABLE umsetzungsbestaetigung OWNER TO qqc01x3;

--
-- Name: user_defined_search; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE user_defined_search (
    id bigint NOT NULL,
    sql_search character varying(255),
    mitarbeiter_qnumber character varying(255)
);


ALTER TABLE user_defined_search OWNER TO qqc01x3;

--
-- Name: user_defined_search_search_filter; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE user_defined_search_search_filter (
    userdefinedsearch_id bigint NOT NULL,
    searchfilterlist_id bigint NOT NULL
);


ALTER TABLE user_defined_search_search_filter OWNER TO qqc01x3;

--
-- Name: user_settings; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE user_settings (
    id bigint NOT NULL,
    mail_receipt_enabled boolean,
    mitarbeiter_qnumber character varying(255)
);


ALTER TABLE user_settings OWNER TO qqc01x3;

--
-- Name: user_settings_derivat; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE user_settings_derivat (
    usersettings_id bigint NOT NULL,
    selectedderivate_id bigint NOT NULL
);


ALTER TABLE user_settings_derivat OWNER TO qqc01x3;

--
-- Name: user_to_group; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE user_to_group (
    qnumber character varying(255) NOT NULL,
    gruppenname character varying(255) NOT NULL
);


ALTER TABLE user_to_group OWNER TO qqc01x3;

--
-- Name: usersettings_selectedderivate; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE usersettings_selectedderivate (
    usersettings_id bigint NOT NULL,
    selectedderivat_id bigint NOT NULL
);


ALTER TABLE usersettings_selectedderivate OWNER TO qqc01x3;

--
-- Name: usersettings_selectedzakderivate; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE usersettings_selectedzakderivate (
    usersettings_id bigint NOT NULL,
    selectedzakderivat_id bigint NOT NULL
);


ALTER TABLE usersettings_selectedzakderivate OWNER TO qqc01x3;

--
-- Name: wert; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE wert (
    id bigint NOT NULL,
    attribut character varying(255),
    wert character varying(10000)
);


ALTER TABLE wert OWNER TO qqc01x3;

--
-- Name: zak_uebertragung; Type: TABLE; Schema: public; Owner: qqc01x3
--

CREATE TABLE zak_uebertragung (
    id bigint NOT NULL,
    aenderungsdatum timestamp without time zone,
    umsetzer character varying(255),
    zak_kommentar character varying(255),
    zakstatus integer,
    zak_uebertrager character varying(255),
    zakuebertragung timestamp without time zone,
    zak_uebertragung_id bigint,
    derivatanforderung_id bigint,
    modul_name character varying(255)
);


ALTER TABLE zak_uebertragung OWNER TO qqc01x3;

--
-- Name: EJB__TIMER__TBL_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY "EJB__TIMER__TBL"
    ADD CONSTRAINT "EJB__TIMER__TBL_pkey" PRIMARY KEY ("TIMERID");


--
-- Name: anforderung_anforderung_freigabe_bei_modul_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_anforderung_freigabe_bei_modul
    ADD CONSTRAINT anforderung_anforderung_freigabe_bei_modul_pkey PRIMARY KEY (anforderung_id, anfofreigabebeimodul_id);


--
-- Name: anforderung_anhang_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_anhang
    ADD CONSTRAINT anforderung_anhang_pkey PRIMARY KEY (anforderung_id, anhaenge_id);


--
-- Name: anforderung_auswirkung_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_auswirkung
    ADD CONSTRAINT anforderung_auswirkung_pkey PRIMARY KEY (anforderung_id, auswirkungen_id);


--
-- Name: anforderung_festgestelltin_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_festgestelltin
    ADD CONSTRAINT anforderung_festgestelltin_pkey PRIMARY KEY (anforderung_id, festgestelltin_id);


--
-- Name: anforderung_freigabe_bei_modul_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_freigabe_bei_modul
    ADD CONSTRAINT anforderung_freigabe_bei_modul_pkey PRIMARY KEY (id);


--
-- Name: anforderung_history_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_history
    ADD CONSTRAINT anforderung_history_pkey PRIMARY KEY (id);


--
-- Name: anforderung_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT anforderung_pkey PRIMARY KEY (id);


--
-- Name: anforderung_umsetzer_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_umsetzer
    ADD CONSTRAINT anforderung_umsetzer_pkey PRIMARY KEY (anforderung_id, umsetzer_umsetzer_id);


--
-- Name: anhang_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anhang
    ADD CONSTRAINT anhang_pkey PRIMARY KEY (id);


--
-- Name: anwender_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anwender
    ADD CONSTRAINT anwender_pkey PRIMARY KEY (qnumber);


--
-- Name: auswirkung_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY auswirkung
    ADD CONSTRAINT auswirkung_pkey PRIMARY KEY (id);


--
-- Name: berechtigung_derivat_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY berechtigung_derivat
    ADD CONSTRAINT berechtigung_derivat_pkey PRIMARY KEY (id);


--
-- Name: berechtigung_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY berechtigung
    ADD CONSTRAINT berechtigung_pkey PRIMARY KEY (id);


--
-- Name: berechtigunghistory_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY berechtigunghistory
    ADD CONSTRAINT berechtigunghistory_pkey PRIMARY KEY (id);


--
-- Name: bild_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY bild
    ADD CONSTRAINT bild_pkey PRIMARY KEY (id);


--
-- Name: dbfile_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY dbfile
    ADD CONSTRAINT dbfile_pkey PRIMARY KEY (id);


--
-- Name: derivat_kov_a_phase_im_derivat_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY derivat_kov_a_phase_im_derivat
    ADD CONSTRAINT derivat_kov_a_phase_im_derivat_pkey PRIMARY KEY (derivat_id, kovaphasen_id);


--
-- Name: derivat_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY derivat
    ADD CONSTRAINT derivat_pkey PRIMARY KEY (id);


--
-- Name: fahrzeugmerkmal_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY fahrzeugmerkmal
    ADD CONSTRAINT fahrzeugmerkmal_pkey PRIMARY KEY (id);


--
-- Name: fahrzeugmodul_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY fahrzeugmodul
    ADD CONSTRAINT fahrzeugmodul_pkey PRIMARY KEY (id);


--
-- Name: fahrzeugprojekt_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY fahrzeugprojekt
    ADD CONSTRAINT fahrzeugprojekt_pkey PRIMARY KEY (id);


--
-- Name: festgestelltin_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY festgestelltin
    ADD CONSTRAINT festgestelltin_pkey PRIMARY KEY (id);


--
-- Name: infotext_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY infotext
    ADD CONSTRAINT infotext_pkey PRIMARY KEY (id);


--
-- Name: kov_a_phase_im_derivat_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY kov_a_phase_im_derivat
    ADD CONSTRAINT kov_a_phase_im_derivat_pkey PRIMARY KEY (id);


--
-- Name: localgroup_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY localgroup
    ADD CONSTRAINT localgroup_pkey PRIMARY KEY (gruppenname);


--
-- Name: meldung_anforderung_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung_anforderung
    ADD CONSTRAINT meldung_anforderung_pkey PRIMARY KEY (anforderung_id, meldung_id);


--
-- Name: meldung_anhang_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung_anhang
    ADD CONSTRAINT meldung_anhang_pkey PRIMARY KEY (meldung_id, anhaenge_id);


--
-- Name: meldung_auswirkung_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung_auswirkung
    ADD CONSTRAINT meldung_auswirkung_pkey PRIMARY KEY (meldung_id, auswirkungen_id);


--
-- Name: meldung_festgestelltin_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung_festgestelltin
    ADD CONSTRAINT meldung_festgestelltin_pkey PRIMARY KEY (meldung_id, festgestelltin_id);


--
-- Name: meldung_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT meldung_pkey PRIMARY KEY (id);


--
-- Name: mitarbeiter_derivat_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_derivat
    ADD CONSTRAINT mitarbeiter_derivat_pkey PRIMARY KEY (qnumber, derivatid);


--
-- Name: mitarbeiter_modul_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_modul
    ADD CONSTRAINT mitarbeiter_modul_pkey PRIMARY KEY (qnumber, modulname);


--
-- Name: mitarbeiter_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter
    ADD CONSTRAINT mitarbeiter_pkey PRIMARY KEY (qnumber);


--
-- Name: mitarbeiter_sensorcoc_lese_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_sensorcoc_lese
    ADD CONSTRAINT mitarbeiter_sensorcoc_lese_pkey PRIMARY KEY (qnumber, sensorcocid);


--
-- Name: mitarbeiter_sensorcoc_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_sensorcoc
    ADD CONSTRAINT mitarbeiter_sensorcoc_pkey PRIMARY KEY (qnumber, sensorcocid);


--
-- Name: mitarbeiter_tteam_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_tteam
    ADD CONSTRAINT mitarbeiter_tteam_pkey PRIMARY KEY (qnumber, tteamid);


--
-- Name: modul_modulseteam_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY modul_modulseteam
    ADD CONSTRAINT modul_modulseteam_pkey PRIMARY KEY (modul_id, seteams_id);


--
-- Name: modul_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY modul
    ADD CONSTRAINT modul_pkey PRIMARY KEY (id);


--
-- Name: modulkomponente_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY modulkomponente
    ADD CONSTRAINT modulkomponente_pkey PRIMARY KEY (id);


--
-- Name: modulseteam_modulkomponente_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY modulseteam_modulkomponente
    ADD CONSTRAINT modulseteam_modulkomponente_pkey PRIMARY KEY (modulseteam_id, komponenten_id);


--
-- Name: modulseteam_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY modulseteam
    ADD CONSTRAINT modulseteam_pkey PRIMARY KEY (id);


--
-- Name: pzbk_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY pzbk
    ADD CONSTRAINT pzbk_pkey PRIMARY KEY (id);


--
-- Name: search_filter_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY search_filter
    ADD CONSTRAINT search_filter_pkey PRIMARY KEY (id);


--
-- Name: sensorcoc_mitarbeiter_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY sensorcoc_mitarbeiter
    ADD CONSTRAINT sensorcoc_mitarbeiter_pkey PRIMARY KEY (sensorcoc_sensorcocid, umsetzungsbestaetiger_qnumber);


--
-- Name: sensorcoc_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY sensorcoc
    ADD CONSTRAINT sensorcoc_pkey PRIMARY KEY (sensorcocid);


--
-- Name: sequence_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY sequence
    ADD CONSTRAINT sequence_pkey PRIMARY KEY (seq_name);


--
-- Name: status_vereinbarung_anforderung_im_derivat_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT status_vereinbarung_anforderung_im_derivat_pkey PRIMARY KEY (id);


--
-- Name: statusnext_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY statusnext
    ADD CONSTRAINT statusnext_pkey PRIMARY KEY (id);


--
-- Name: t_role_group_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY t_role_group
    ADD CONSTRAINT t_role_group_pkey PRIMARY KEY (id);


--
-- Name: t_role_user_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY t_role_user
    ADD CONSTRAINT t_role_user_pkey PRIMARY KEY (id);


--
-- Name: t_team_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY t_team
    ADD CONSTRAINT t_team_pkey PRIMARY KEY (id);


--
-- Name: t_users_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY t_users
    ADD CONSTRAINT t_users_pkey PRIMARY KEY (qnumber);


--
-- Name: ub_zu_kova_phase_im_derivat_und_sensorcoc_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc
    ADD CONSTRAINT ub_zu_kova_phase_im_derivat_und_sensorcoc_pkey PRIMARY KEY (id);


--
-- Name: umsetzer_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY umsetzer
    ADD CONSTRAINT umsetzer_pkey PRIMARY KEY (umsetzer_id);


--
-- Name: umsetzungsbestaetigung_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY umsetzungsbestaetigung
    ADD CONSTRAINT umsetzungsbestaetigung_pkey PRIMARY KEY (id);


--
-- Name: unq_status_vereinbarung_anforderung_im_derivat_0; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT unq_status_vereinbarung_anforderung_im_derivat_0 UNIQUE (anforderung_id, derivat_id);


--
-- Name: user_defined_search_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY user_defined_search
    ADD CONSTRAINT user_defined_search_pkey PRIMARY KEY (id);


--
-- Name: user_defined_search_search_filter_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY user_defined_search_search_filter
    ADD CONSTRAINT user_defined_search_search_filter_pkey PRIMARY KEY (userdefinedsearch_id, searchfilterlist_id);


--
-- Name: user_settings_derivat_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY user_settings_derivat
    ADD CONSTRAINT user_settings_derivat_pkey PRIMARY KEY (usersettings_id, selectedderivate_id);


--
-- Name: user_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY user_settings
    ADD CONSTRAINT user_settings_pkey PRIMARY KEY (id);


--
-- Name: user_to_group_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY user_to_group
    ADD CONSTRAINT user_to_group_pkey PRIMARY KEY (qnumber, gruppenname);


--
-- Name: usersettings_selectedderivate_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY usersettings_selectedderivate
    ADD CONSTRAINT usersettings_selectedderivate_pkey PRIMARY KEY (usersettings_id, selectedderivat_id);


--
-- Name: usersettings_selectedzakderivate_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY usersettings_selectedzakderivate
    ADD CONSTRAINT usersettings_selectedzakderivate_pkey PRIMARY KEY (usersettings_id, selectedzakderivat_id);


--
-- Name: wert_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY wert
    ADD CONSTRAINT wert_pkey PRIMARY KEY (id);


--
-- Name: zak_uebertragung_pkey; Type: CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY zak_uebertragung
    ADD CONSTRAINT zak_uebertragung_pkey PRIMARY KEY (id);


--
-- Name: anforderunganforderungfreigabe_bei_modulanfofreigabebeimodul_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_anforderung_freigabe_bei_modul
    ADD CONSTRAINT anforderunganforderungfreigabe_bei_modulanfofreigabebeimodul_id FOREIGN KEY (anfofreigabebeimodul_id) REFERENCES anforderung_freigabe_bei_modul(id);


--
-- Name: fk_anforderung_anforderung_freigabe_bei_modul_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_anforderung_freigabe_bei_modul
    ADD CONSTRAINT fk_anforderung_anforderung_freigabe_bei_modul_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- Name: fk_anforderung_anhang_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_anhang
    ADD CONSTRAINT fk_anforderung_anhang_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- Name: fk_anforderung_anhang_anhaenge_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_anhang
    ADD CONSTRAINT fk_anforderung_anhang_anhaenge_id FOREIGN KEY (anhaenge_id) REFERENCES anhang(id);


--
-- Name: fk_anforderung_auswirkung_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_auswirkung
    ADD CONSTRAINT fk_anforderung_auswirkung_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- Name: fk_anforderung_auswirkung_auswirkungen_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_auswirkung
    ADD CONSTRAINT fk_anforderung_auswirkung_auswirkungen_id FOREIGN KEY (auswirkungen_id) REFERENCES auswirkung(id);


--
-- Name: fk_anforderung_fachlicheransprechpartner_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_fachlicheransprechpartner_qnumber FOREIGN KEY (fachlicheransprechpartner_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_anforderung_festgestelltin_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_festgestelltin
    ADD CONSTRAINT fk_anforderung_festgestelltin_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- Name: fk_anforderung_festgestelltin_festgestelltin_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_festgestelltin
    ADD CONSTRAINT fk_anforderung_festgestelltin_festgestelltin_id FOREIGN KEY (festgestelltin_id) REFERENCES festgestelltin(id);


--
-- Name: fk_anforderung_freigabe_bei_modul_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_freigabe_bei_modul
    ADD CONSTRAINT fk_anforderung_freigabe_bei_modul_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- Name: fk_anforderung_freigabe_bei_modul_modul_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_freigabe_bei_modul
    ADD CONSTRAINT fk_anforderung_freigabe_bei_modul_modul_id FOREIGN KEY (modul_id) REFERENCES modul(id);


--
-- Name: fk_anforderung_sensor_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_sensor_qnumber FOREIGN KEY (sensor_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_anforderung_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- Name: fk_anforderung_teamleiter_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_teamleiter_qnumber FOREIGN KEY (teamleiter_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_anforderung_tteam_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_tteam_id FOREIGN KEY (tteam_id) REFERENCES t_team(id);


--
-- Name: fk_anforderung_umsetzer_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_umsetzer
    ADD CONSTRAINT fk_anforderung_umsetzer_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- Name: fk_anforderung_umsetzer_umsetzer_umsetzer_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anforderung_umsetzer
    ADD CONSTRAINT fk_anforderung_umsetzer_umsetzer_umsetzer_id FOREIGN KEY (umsetzer_umsetzer_id) REFERENCES umsetzer(umsetzer_id);


--
-- Name: fk_anhang_content_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anhang
    ADD CONSTRAINT fk_anhang_content_id FOREIGN KEY (content_id) REFERENCES dbfile(id);


--
-- Name: fk_anhang_creator_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY anhang
    ADD CONSTRAINT fk_anhang_creator_qnumber FOREIGN KEY (creator_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_berechtigung_mitarbeiter_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY berechtigung
    ADD CONSTRAINT fk_berechtigung_mitarbeiter_qnumber FOREIGN KEY (mitarbeiter_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_dbfile_uploader_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY dbfile
    ADD CONSTRAINT fk_dbfile_uploader_qnumber FOREIGN KEY (uploader_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_derivat_kov_a_phase_im_derivat_derivat_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY derivat_kov_a_phase_im_derivat
    ADD CONSTRAINT fk_derivat_kov_a_phase_im_derivat_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);


--
-- Name: fk_derivat_kov_a_phase_im_derivat_kovaphasen_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY derivat_kov_a_phase_im_derivat
    ADD CONSTRAINT fk_derivat_kov_a_phase_im_derivat_kovaphasen_id FOREIGN KEY (kovaphasen_id) REFERENCES kov_a_phase_im_derivat(id);


--
-- Name: fk_kov_a_phase_im_derivat_derivat_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY kov_a_phase_im_derivat
    ADD CONSTRAINT fk_kov_a_phase_im_derivat_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);


--
-- Name: fk_meldung_anforderung_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung_anforderung
    ADD CONSTRAINT fk_meldung_anforderung_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- Name: fk_meldung_anforderung_meldung_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung_anforderung
    ADD CONSTRAINT fk_meldung_anforderung_meldung_id FOREIGN KEY (meldung_id) REFERENCES meldung(id);


--
-- Name: fk_meldung_anhang_anhaenge_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung_anhang
    ADD CONSTRAINT fk_meldung_anhang_anhaenge_id FOREIGN KEY (anhaenge_id) REFERENCES anhang(id);


--
-- Name: fk_meldung_anhang_meldung_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung_anhang
    ADD CONSTRAINT fk_meldung_anhang_meldung_id FOREIGN KEY (meldung_id) REFERENCES meldung(id);


--
-- Name: fk_meldung_auswirkung_auswirkungen_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung_auswirkung
    ADD CONSTRAINT fk_meldung_auswirkung_auswirkungen_id FOREIGN KEY (auswirkungen_id) REFERENCES auswirkung(id);


--
-- Name: fk_meldung_auswirkung_meldung_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung_auswirkung
    ADD CONSTRAINT fk_meldung_auswirkung_meldung_id FOREIGN KEY (meldung_id) REFERENCES meldung(id);


--
-- Name: fk_meldung_fachlicheransprechpartner_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_fachlicheransprechpartner_qnumber FOREIGN KEY (fachlicheransprechpartner_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_meldung_festgestelltin_festgestelltin_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung_festgestelltin
    ADD CONSTRAINT fk_meldung_festgestelltin_festgestelltin_id FOREIGN KEY (festgestelltin_id) REFERENCES festgestelltin(id);


--
-- Name: fk_meldung_festgestelltin_meldung_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung_festgestelltin
    ADD CONSTRAINT fk_meldung_festgestelltin_meldung_id FOREIGN KEY (meldung_id) REFERENCES meldung(id);


--
-- Name: fk_meldung_sensor_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_sensor_qnumber FOREIGN KEY (sensor_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_meldung_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- Name: fk_meldung_teamleiter_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_teamleiter_qnumber FOREIGN KEY (teamleiter_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_meldung_tteam_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_tteam_id FOREIGN KEY (tteam_id) REFERENCES t_team(id);


--
-- Name: fk_mitarbeiter_derivat_derivatid; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_derivat
    ADD CONSTRAINT fk_mitarbeiter_derivat_derivatid FOREIGN KEY (derivatid) REFERENCES derivat(id);


--
-- Name: fk_mitarbeiter_derivat_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_derivat
    ADD CONSTRAINT fk_mitarbeiter_derivat_qnumber FOREIGN KEY (qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_mitarbeiter_modul_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_modul
    ADD CONSTRAINT fk_mitarbeiter_modul_qnumber FOREIGN KEY (qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_mitarbeiter_sensorcoc_lese_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_sensorcoc_lese
    ADD CONSTRAINT fk_mitarbeiter_sensorcoc_lese_qnumber FOREIGN KEY (qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_mitarbeiter_sensorcoc_lese_sensorcocid; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_sensorcoc_lese
    ADD CONSTRAINT fk_mitarbeiter_sensorcoc_lese_sensorcocid FOREIGN KEY (sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- Name: fk_mitarbeiter_sensorcoc_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_sensorcoc
    ADD CONSTRAINT fk_mitarbeiter_sensorcoc_qnumber FOREIGN KEY (qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_mitarbeiter_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_sensorcoc
    ADD CONSTRAINT fk_mitarbeiter_sensorcoc_sensorcocid FOREIGN KEY (sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- Name: fk_mitarbeiter_tteam_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_tteam
    ADD CONSTRAINT fk_mitarbeiter_tteam_qnumber FOREIGN KEY (qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_mitarbeiter_tteam_tteamid; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_tteam
    ADD CONSTRAINT fk_mitarbeiter_tteam_tteamid FOREIGN KEY (tteamid) REFERENCES t_team(id);


--
-- Name: fk_mitarbeiter_usersettings_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter
    ADD CONSTRAINT fk_mitarbeiter_usersettings_id FOREIGN KEY (usersettings_id) REFERENCES user_settings(id);


--
-- Name: fk_mitarbeiter_zakeinordnungen_mitarbeiter_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY mitarbeiter_zakeinordnungen
    ADD CONSTRAINT fk_mitarbeiter_zakeinordnungen_mitarbeiter_qnumber FOREIGN KEY (mitarbeiter_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_modul_modulseteam_modul_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY modul_modulseteam
    ADD CONSTRAINT fk_modul_modulseteam_modul_id FOREIGN KEY (modul_id) REFERENCES modul(id);


--
-- Name: fk_modul_modulseteam_seteams_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY modul_modulseteam
    ADD CONSTRAINT fk_modul_modulseteam_seteams_id FOREIGN KEY (seteams_id) REFERENCES modulseteam(id);


--
-- Name: fk_modulkomponente_seteam_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY modulkomponente
    ADD CONSTRAINT fk_modulkomponente_seteam_id FOREIGN KEY (seteam_id) REFERENCES modulseteam(id);


--
-- Name: fk_modulseteam_elternmodul_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY modulseteam
    ADD CONSTRAINT fk_modulseteam_elternmodul_id FOREIGN KEY (elternmodul_id) REFERENCES modul(id);


--
-- Name: fk_modulseteam_modulkomponente_komponenten_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY modulseteam_modulkomponente
    ADD CONSTRAINT fk_modulseteam_modulkomponente_komponenten_id FOREIGN KEY (komponenten_id) REFERENCES modulkomponente(id);


--
-- Name: fk_modulseteam_modulkomponente_modulseteam_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY modulseteam_modulkomponente
    ADD CONSTRAINT fk_modulseteam_modulkomponente_modulseteam_id FOREIGN KEY (modulseteam_id) REFERENCES modulseteam(id);


--
-- Name: fk_searchfilter_longlistvalue_searchfilter_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY searchfilter_longlistvalue
    ADD CONSTRAINT fk_searchfilter_longlistvalue_searchfilter_id FOREIGN KEY (searchfilter_id) REFERENCES search_filter(id);


--
-- Name: fk_searchfilter_statuslistvalue_searchfilter_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY searchfilter_statuslistvalue
    ADD CONSTRAINT fk_searchfilter_statuslistvalue_searchfilter_id FOREIGN KEY (searchfilter_id) REFERENCES search_filter(id);


--
-- Name: fk_sensorcoc_fachlicheransprechpartner_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY sensorcoc
    ADD CONSTRAINT fk_sensorcoc_fachlicheransprechpartner_qnumber FOREIGN KEY (fachlicheransprechpartner_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_sensorcoc_mitarbeiter_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY sensorcoc_mitarbeiter
    ADD CONSTRAINT fk_sensorcoc_mitarbeiter_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- Name: fk_sensorcoc_mitarbeiter_umsetzungsbestaetiger_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY sensorcoc_mitarbeiter
    ADD CONSTRAINT fk_sensorcoc_mitarbeiter_umsetzungsbestaetiger_qnumber FOREIGN KEY (umsetzungsbestaetiger_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_status_vereinbarung_anforderung_im_derivat_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT fk_status_vereinbarung_anforderung_im_derivat_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- Name: fk_status_vereinbarung_anforderung_im_derivat_derivat_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT fk_status_vereinbarung_anforderung_im_derivat_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);


--
-- Name: fk_t_team_teamleiter_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY t_team
    ADD CONSTRAINT fk_t_team_teamleiter_qnumber FOREIGN KEY (teamleiter_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_ub_zu_kova_phase_im_derivat_und_sensorcoc_kovaimderivat_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc
    ADD CONSTRAINT fk_ub_zu_kova_phase_im_derivat_und_sensorcoc_kovaimderivat_id FOREIGN KEY (kovaimderivat_id) REFERENCES kov_a_phase_im_derivat(id);


--
-- Name: fk_umsetzer_komponente_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY umsetzer
    ADD CONSTRAINT fk_umsetzer_komponente_id FOREIGN KEY (komponente_id) REFERENCES modulkomponente(id);


--
-- Name: fk_umsetzer_seteam_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY umsetzer
    ADD CONSTRAINT fk_umsetzer_seteam_id FOREIGN KEY (seteam_id) REFERENCES modulseteam(id);


--
-- Name: fk_umsetzungsbestaetigung_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY umsetzungsbestaetigung
    ADD CONSTRAINT fk_umsetzungsbestaetigung_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- Name: fk_umsetzungsbestaetigung_kovaimderivat_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY umsetzungsbestaetigung
    ADD CONSTRAINT fk_umsetzungsbestaetigung_kovaimderivat_id FOREIGN KEY (kovaimderivat_id) REFERENCES kov_a_phase_im_derivat(id);


--
-- Name: fk_user_defined_search_mitarbeiter_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY user_defined_search
    ADD CONSTRAINT fk_user_defined_search_mitarbeiter_qnumber FOREIGN KEY (mitarbeiter_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_user_defined_search_search_filter_searchfilterlist_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY user_defined_search_search_filter
    ADD CONSTRAINT fk_user_defined_search_search_filter_searchfilterlist_id FOREIGN KEY (searchfilterlist_id) REFERENCES search_filter(id);


--
-- Name: fk_user_defined_search_search_filter_userdefinedsearch_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY user_defined_search_search_filter
    ADD CONSTRAINT fk_user_defined_search_search_filter_userdefinedsearch_id FOREIGN KEY (userdefinedsearch_id) REFERENCES user_defined_search(id);


--
-- Name: fk_user_settings_mitarbeiter_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY user_settings
    ADD CONSTRAINT fk_user_settings_mitarbeiter_qnumber FOREIGN KEY (mitarbeiter_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: fk_user_to_group_gruppenname; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY user_to_group
    ADD CONSTRAINT fk_user_to_group_gruppenname FOREIGN KEY (gruppenname) REFERENCES localgroup(gruppenname);


--
-- Name: fk_user_to_group_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY user_to_group
    ADD CONSTRAINT fk_user_to_group_qnumber FOREIGN KEY (qnumber) REFERENCES t_users(qnumber);


--
-- Name: fk_usersettings_selectedderivate_selectedderivat_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY usersettings_selectedderivate
    ADD CONSTRAINT fk_usersettings_selectedderivate_selectedderivat_id FOREIGN KEY (selectedderivat_id) REFERENCES derivat(id);


--
-- Name: fk_usersettings_selectedderivate_usersettings_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY usersettings_selectedderivate
    ADD CONSTRAINT fk_usersettings_selectedderivate_usersettings_id FOREIGN KEY (usersettings_id) REFERENCES user_settings(id);


--
-- Name: fk_usersettings_selectedzakderivate_selectedzakderivat_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY usersettings_selectedzakderivate
    ADD CONSTRAINT fk_usersettings_selectedzakderivate_selectedzakderivat_id FOREIGN KEY (selectedzakderivat_id) REFERENCES derivat(id);


--
-- Name: fk_usersettings_selectedzakderivate_usersettings_id; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY usersettings_selectedzakderivate
    ADD CONSTRAINT fk_usersettings_selectedzakderivate_usersettings_id FOREIGN KEY (usersettings_id) REFERENCES user_settings(id);


--
-- Name: status_vereinbarung_anforderung_im_derivat_anforderer_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT status_vereinbarung_anforderung_im_derivat_anforderer_qnumber FOREIGN KEY (anforderer_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: ub_zu_kova_phase_im_derivat_und_sensorcoc_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc
    ADD CONSTRAINT ub_zu_kova_phase_im_derivat_und_sensorcoc_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- Name: ubzukovaphaseimderivatundsensorcocumsetzungsbestaetiger_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: qqc01x3
--

ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc
    ADD CONSTRAINT ubzukovaphaseimderivatundsensorcocumsetzungsbestaetiger_qnumber FOREIGN KEY (umsetzungsbestaetiger_qnumber) REFERENCES mitarbeiter(qnumber);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

