CREATE OR REPLACE FUNCTION migrate_verantwortlicher()
RETURNS INTEGER AS $$
DECLARE 
   migrated INTEGER DEFAULT 0;
   b_curs cursor for select * from berechtigung WHERE rolle = 3 AND verantwortlicher = false;
   b_rec RECORD;
BEGIN
    open b_curs;
    LOOP
        FETCH b_curs INTO b_rec;
        EXIT WHEN NOT FOUND;        
        UPDATE berechtigung SET rolle = 9 WHERE current of b_curs;
        migrated = migrated + 1;		
    END LOOP;
    
    CLOSE b_curs;

RETURN migrated;
END; $$

LANGUAGE plpgsql;

SELECT migrate_verantwortlicher();

