--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 9.6.2

-- Started on 2017-07-20 11:59:52

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2613 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 1587005)
-- Name: EJB__TIMER__TBL; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE "EJB__TIMER__TBL" (
    "CREATIONTIMERAW" bigint NOT NULL,
    "BLOB" bytea,
    "TIMERID" character varying(255) NOT NULL,
    "CONTAINERID" bigint NOT NULL,
    "OWNERID" character varying(255),
    "STATE" integer NOT NULL,
    "PKHASHCODE" integer NOT NULL,
    "INTERVALDURATION" bigint NOT NULL,
    "INITIALEXPIRATIONRAW" bigint NOT NULL,
    "LASTEXPIRATIONRAW" bigint NOT NULL,
    "SCHEDULE" character varying(255),
    "APPLICATIONID" bigint NOT NULL
);


ALTER TABLE "EJB__TIMER__TBL" OWNER TO pzbk;

--
-- TOC entry 187 (class 1259 OID 2399644)
-- Name: anforderung; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE anforderung (
    id bigint NOT NULL,
    aenderungsdatum timestamp without time zone,
    beschreibunganforderungde character varying(10000),
    beschreibunganforderungen character varying(10000),
    beschreibungstaerkeschwaeche character varying(10000),
    designrelevant boolean,
    erstellungsdatum timestamp without time zone,
    fachid character varying(255),
    kommentaranforderung character varying(10000),
    loesungsvorschlag character varying(10000),
    phasenbezug boolean,
    potentialstandardisierung boolean,
    prioinbl character varying(255),
    referenzen character varying(255),
    staerkeschwaeche boolean,
    status integer,
    statuswechselkommentar character varying(255),
    umsetzenderbereich character varying(255),
    ursache character varying(10000),
    version integer,
    zeitpktumsetzungsbest boolean,
    einheit character varying(255),
    kommentar character varying(10000),
    wert character varying(255),
    bild_id bigint,
    fachlicheransprechpartner_id bigint,
    sensor_id bigint,
    sensorcoc_sensorcocid bigint,
    teamleiter_id bigint,
    tteam_id bigint
);


ALTER TABLE anforderung OWNER TO pzbk;

--
-- TOC entry 224 (class 1259 OID 2399895)
-- Name: anforderung_anforderung_freigabe_bei_modul; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE anforderung_anforderung_freigabe_bei_modul (
    anforderung_id bigint NOT NULL,
    anfofreigabebeimodul_id bigint NOT NULL
);


ALTER TABLE anforderung_anforderung_freigabe_bei_modul OWNER TO pzbk;

--
-- TOC entry 225 (class 1259 OID 2399900)
-- Name: anforderung_anhang; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE anforderung_anhang (
    anforderung_id bigint NOT NULL,
    anhaenge_id bigint NOT NULL
);


ALTER TABLE anforderung_anhang OWNER TO pzbk;

--
-- TOC entry 226 (class 1259 OID 2399905)
-- Name: anforderung_auswirkung; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE anforderung_auswirkung (
    anforderung_id bigint NOT NULL,
    auswirkungen_id bigint NOT NULL
);


ALTER TABLE anforderung_auswirkung OWNER TO pzbk;

--
-- TOC entry 227 (class 1259 OID 2399910)
-- Name: anforderung_festgestelltin; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE anforderung_festgestelltin (
    anforderung_id bigint NOT NULL,
    festgestelltin_id bigint NOT NULL
);


ALTER TABLE anforderung_festgestelltin OWNER TO pzbk;

--
-- TOC entry 192 (class 1259 OID 2399681)
-- Name: anforderung_freigabe_bei_modul; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE anforderung_freigabe_bei_modul (
    id bigint NOT NULL,
    abgelehnt boolean,
    datum timestamp without time zone,
    freigabe boolean,
    hauptmodul boolean,
    kommentar character varying(255),
    vereinbarung integer,
    zak boolean,
    anforderung_id bigint,
    bearbeiter_id bigint,
    modul_id bigint
);


ALTER TABLE anforderung_freigabe_bei_modul OWNER TO pzbk;

--
-- TOC entry 206 (class 1259 OID 2399769)
-- Name: anforderung_history; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE anforderung_history (
    id bigint NOT NULL,
    anforderung_id bigint,
    auf character varying(10000),
    benutzer character varying(255),
    datum timestamp without time zone,
    kennzeichen character varying(255),
    objekt_name character varying(255),
    objekt_type character varying(255),
    version character varying(255),
    von character varying(10000)
);


ALTER TABLE anforderung_history OWNER TO pzbk;

--
-- TOC entry 229 (class 1259 OID 2399920)
-- Name: anforderung_umsetzer; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE anforderung_umsetzer (
    anforderung_id bigint NOT NULL,
    umsetzer_umsetzer_id bigint NOT NULL
);


ALTER TABLE anforderung_umsetzer OWNER TO pzbk;

--
-- TOC entry 215 (class 1259 OID 2399829)
-- Name: anhang; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE anhang (
    id bigint NOT NULL,
    dateiname character varying(255),
    dateityp character varying(255),
    filesize integer,
    isstandardbild boolean,
    zeitstempel timestamp without time zone,
    content_id bigint,
    creator_id bigint
);


ALTER TABLE anhang OWNER TO pzbk;

--
-- TOC entry 189 (class 1259 OID 2399660)
-- Name: anwender; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE anwender (
    qnumber integer NOT NULL,
    email character varying(255),
    kurzzeichen character varying(255),
    name character varying(255),
    tel character varying(255)
);


ALTER TABLE anwender OWNER TO pzbk;

--
-- TOC entry 216 (class 1259 OID 2399837)
-- Name: auswirkung; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE auswirkung (
    id bigint NOT NULL,
    auswirkung character varying(10000),
    wert character varying(10000)
);


ALTER TABLE auswirkung OWNER TO pzbk;

--
-- TOC entry 213 (class 1259 OID 2399819)
-- Name: berechtigung; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE berechtigung (
    id bigint NOT NULL,
    fuer integer,
    fuerid character varying(255),
    rechttype integer,
    rolle integer,
    mitarbeiter_id bigint
);


ALTER TABLE berechtigung OWNER TO pzbk;

--
-- TOC entry 196 (class 1259 OID 2399707)
-- Name: berechtigung_derivat; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE berechtigung_derivat (
    id bigint NOT NULL,
    derivat_id bigint,
    rechttype integer,
    rolle integer,
    mitarbeiter_id bigint
);


ALTER TABLE berechtigung_derivat OWNER TO pzbk;

--
-- TOC entry 218 (class 1259 OID 2399850)
-- Name: berechtigunghistory; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE berechtigunghistory (
    id bigint NOT NULL,
    berechtigter character varying(255),
    berechtigter_qnumber character varying(255),
    datum timestamp without time zone,
    erteilt boolean,
    fuer character varying(255),
    ziel_id character varying(255),
    rechttype integer,
    rolle character varying(255),
    berechtigt_von character varying(255),
    berechtigt_von_qnumber character varying(255)
);


ALTER TABLE berechtigunghistory OWNER TO pzbk;

--
-- TOC entry 219 (class 1259 OID 2399858)
-- Name: bild; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE bild (
    id bigint NOT NULL,
    largeimage bytea,
    normalimage bytea,
    thumbnailimage bytea
);


ALTER TABLE bild OWNER TO pzbk;

--
-- TOC entry 195 (class 1259 OID 2399699)
-- Name: dbfile; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE dbfile (
    id bigint NOT NULL,
    category character varying(255),
    filecontent bytea,
    creationdate timestamp without time zone,
    filename character varying(255),
    filetype character varying(255)
);


ALTER TABLE dbfile OWNER TO pzbk;

--
-- TOC entry 208 (class 1259 OID 2399785)
-- Name: derivat; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE derivat (
    id bigint NOT NULL,
    bezeichnung character varying(255),
    name character varying(255),
    produktlinie character varying(255),
    startofproduction date
);


ALTER TABLE derivat OWNER TO pzbk;

--
-- TOC entry 236 (class 1259 OID 2399958)
-- Name: derivat_kov_a_phase_im_derivat; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE derivat_kov_a_phase_im_derivat (
    derivat_id bigint NOT NULL,
    kovaphasen_id bigint NOT NULL
);


ALTER TABLE derivat_kov_a_phase_im_derivat OWNER TO pzbk;

--
-- TOC entry 209 (class 1259 OID 2399793)
-- Name: festgestelltin; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE festgestelltin (
    id bigint NOT NULL,
    wert character varying(10000)
);


ALTER TABLE festgestelltin OWNER TO pzbk;

--
-- TOC entry 194 (class 1259 OID 2399691)
-- Name: infotext; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE infotext (
    id bigint NOT NULL,
    beschreibungstext character varying(1024),
    feldbeschreibunglang character varying(255),
    feldname character varying(255)
);


ALTER TABLE infotext OWNER TO pzbk;

--
-- TOC entry 200 (class 1259 OID 2399733)
-- Name: kov_a_phase_im_derivat; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE kov_a_phase_im_derivat (
    id bigint NOT NULL,
    end_date date,
    kov_a_phase integer,
    status integer,
    start_date date,
    umsetzungsbestaetiger character varying(255),
    derivat_id bigint
);


ALTER TABLE kov_a_phase_im_derivat OWNER TO pzbk;

--
-- TOC entry 204 (class 1259 OID 2399759)
-- Name: localgroup; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE localgroup (
    gruppenname character varying(255) NOT NULL
);


ALTER TABLE localgroup OWNER TO pzbk;

--
-- TOC entry 203 (class 1259 OID 2399751)
-- Name: meldung; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE meldung (
    id bigint NOT NULL,
    aenderungsdatum timestamp without time zone,
    assignedtoanforderung boolean,
    beschreibunganforderungde character varying(10000),
    beschreibunganforderungen character varying(10000),
    beschreibungstaerkeschwaeche character varying(10000),
    designrelevant boolean,
    erstellungsdatum timestamp without time zone,
    fachid character varying(255),
    kommentaranforderung character varying(10000),
    loesungsvorschlag character varying(10000),
    prioinbl character varying(255),
    referenzen character varying(255),
    staerkeschwaeche boolean,
    status integer,
    statuswechselkommentar character varying(255),
    umsetzenderbereich character varying(255),
    ursache character varying(10000),
    einheit character varying(255),
    kommentar character varying(10000),
    wert character varying(255),
    bild_id bigint,
    fachlicheransprechpartner_id bigint,
    sensor_id bigint,
    sensorcoc_sensorcocid bigint,
    teamleiter_id bigint,
    tteam_id bigint
);


ALTER TABLE meldung OWNER TO pzbk;

--
-- TOC entry 228 (class 1259 OID 2399915)
-- Name: meldung_anforderung; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE meldung_anforderung (
    anforderung_id bigint NOT NULL,
    meldung_id bigint NOT NULL
);


ALTER TABLE meldung_anforderung OWNER TO pzbk;

--
-- TOC entry 232 (class 1259 OID 2399938)
-- Name: meldung_anhang; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE meldung_anhang (
    meldung_id bigint NOT NULL,
    anhaenge_id bigint NOT NULL
);


ALTER TABLE meldung_anhang OWNER TO pzbk;

--
-- TOC entry 233 (class 1259 OID 2399943)
-- Name: meldung_auswirkung; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE meldung_auswirkung (
    meldung_id bigint NOT NULL,
    auswirkungen_id bigint NOT NULL
);


ALTER TABLE meldung_auswirkung OWNER TO pzbk;

--
-- TOC entry 234 (class 1259 OID 2399948)
-- Name: meldung_festgestelltin; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE meldung_festgestelltin (
    meldung_id bigint NOT NULL,
    festgestelltin_id bigint NOT NULL
);


ALTER TABLE meldung_festgestelltin OWNER TO pzbk;

--
-- TOC entry 212 (class 1259 OID 2399811)
-- Name: mitarbeiter; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE mitarbeiter (
    id bigint NOT NULL,
    abteilung character varying(255),
    email character varying(255),
    kurzzeichen character varying(255),
    mail_receipt_enabled boolean,
    nachname character varying(255),
    qnumber character varying(255),
    tel character varying(255),
    vorname character varying(255)
);


ALTER TABLE mitarbeiter OWNER TO pzbk;

--
-- TOC entry 241 (class 1259 OID 2399975)
-- Name: mitarbeiter_derivat; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE mitarbeiter_derivat (
    mitarbeiter_id bigint NOT NULL,
    derivaten_id bigint NOT NULL
);


ALTER TABLE mitarbeiter_derivat OWNER TO pzbk;

--
-- TOC entry 242 (class 1259 OID 2399980)
-- Name: mitarbeiter_modul; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE mitarbeiter_modul (
    mitarbeiter_id bigint NOT NULL,
    moduls_id bigint NOT NULL
);


ALTER TABLE mitarbeiter_modul OWNER TO pzbk;

--
-- TOC entry 245 (class 1259 OID 2399995)
-- Name: mitarbeiter_sensorcoc; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE mitarbeiter_sensorcoc (
    mitarbeiter_id bigint NOT NULL,
    sensorcoc_sensorcocid bigint NOT NULL
);


ALTER TABLE mitarbeiter_sensorcoc OWNER TO pzbk;

--
-- TOC entry 246 (class 1259 OID 2400000)
-- Name: mitarbeiter_sensorcoc_lese; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE mitarbeiter_sensorcoc_lese (
    mitarbeiter_id bigint NOT NULL,
    sensorcocslese_sensorcocid bigint NOT NULL
);


ALTER TABLE mitarbeiter_sensorcoc_lese OWNER TO pzbk;

--
-- TOC entry 247 (class 1259 OID 2400005)
-- Name: mitarbeiter_tteam; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE mitarbeiter_tteam (
    mitarbeiter_id bigint NOT NULL,
    tteams_id bigint NOT NULL
);


ALTER TABLE mitarbeiter_tteam OWNER TO pzbk;

--
-- TOC entry 240 (class 1259 OID 2399972)
-- Name: mitarbeiter_zakeinordnungen; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE mitarbeiter_zakeinordnungen (
    mitarbeiter_id bigint,
    zakeinordnungen character varying(255)
);


ALTER TABLE mitarbeiter_zakeinordnungen OWNER TO pzbk;

--
-- TOC entry 220 (class 1259 OID 2399866)
-- Name: modul; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE modul (
    id bigint NOT NULL,
    beschreibung character varying(255),
    fachbereich character varying(255),
    name character varying(255),
    modulbild_id bigint
);


ALTER TABLE modul OWNER TO pzbk;

--
-- TOC entry 249 (class 1259 OID 2400015)
-- Name: modul_modulseteam; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE modul_modulseteam (
    modul_id bigint NOT NULL,
    seteams_id bigint NOT NULL
);


ALTER TABLE modul_modulseteam OWNER TO pzbk;

--
-- TOC entry 205 (class 1259 OID 2399764)
-- Name: modulbild; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE modulbild (
    id bigint NOT NULL,
    bild_id bigint,
    modul_id bigint
);


ALTER TABLE modulbild OWNER TO pzbk;

--
-- TOC entry 198 (class 1259 OID 2399720)
-- Name: modulkomponente; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE modulkomponente (
    id bigint NOT NULL,
    name character varying(255),
    ppg character varying(255),
    seteam_id bigint
);


ALTER TABLE modulkomponente OWNER TO pzbk;

--
-- TOC entry 199 (class 1259 OID 2399728)
-- Name: modulseteam; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE modulseteam (
    id bigint NOT NULL,
    name character varying(255),
    elternmodul_id bigint
);


ALTER TABLE modulseteam OWNER TO pzbk;

--
-- TOC entry 230 (class 1259 OID 2399925)
-- Name: modulseteam_modulkomponente; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE modulseteam_modulkomponente (
    modulseteam_id bigint NOT NULL,
    komponenten_id bigint NOT NULL
);


ALTER TABLE modulseteam_modulkomponente OWNER TO pzbk;

--
-- TOC entry 223 (class 1259 OID 2399890)
-- Name: pzbk; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE pzbk (
    id bigint NOT NULL,
    pzbkid character varying(255)
);


ALTER TABLE pzbk OWNER TO pzbk;

--
-- TOC entry 211 (class 1259 OID 2399806)
-- Name: search_filter; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE search_filter (
    id bigint NOT NULL,
    booleanvalue boolean,
    enddatevalue timestamp without time zone,
    filtertype integer,
    startdatevalue timestamp without time zone
);


ALTER TABLE search_filter OWNER TO pzbk;

--
-- TOC entry 237 (class 1259 OID 2399963)
-- Name: searchfilter_longlistvalue; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE searchfilter_longlistvalue (
    searchfilter_id bigint,
    longlistvalue bigint
);


ALTER TABLE searchfilter_longlistvalue OWNER TO pzbk;

--
-- TOC entry 238 (class 1259 OID 2399966)
-- Name: searchfilter_statuslistvalue; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE searchfilter_statuslistvalue (
    searchfilter_id bigint,
    statuslistvalue character varying(255)
);


ALTER TABLE searchfilter_statuslistvalue OWNER TO pzbk;

--
-- TOC entry 239 (class 1259 OID 2399969)
-- Name: searchfilter_stringlistvalue; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE searchfilter_stringlistvalue (
    searchfilter_id bigint,
    stringlistvalue character varying(255)
);


ALTER TABLE searchfilter_stringlistvalue OWNER TO pzbk;

--
-- TOC entry 207 (class 1259 OID 2399777)
-- Name: sensorcoc; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE sensorcoc (
    sensorcocid bigint NOT NULL,
    ortung character varying(255),
    ressort character varying(255),
    technologie character varying(255),
    zakeinordnung character varying(255),
    fachlicheransprechpartner_id bigint
);


ALTER TABLE sensorcoc OWNER TO pzbk;

--
-- TOC entry 235 (class 1259 OID 2399953)
-- Name: sensorcoc_mitarbeiter; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE sensorcoc_mitarbeiter (
    sensorcoc_sensorcocid bigint NOT NULL,
    umsetzungsbestaetiger_id bigint NOT NULL
);


ALTER TABLE sensorcoc_mitarbeiter OWNER TO pzbk;

--
-- TOC entry 186 (class 1259 OID 2399559)
-- Name: sequence; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE sequence (
    seq_name character varying(50) NOT NULL,
    seq_count numeric(38,0)
);


ALTER TABLE sequence OWNER TO pzbk;

--
-- TOC entry 193 (class 1259 OID 2399686)
-- Name: status_vereinbarung_anforderung_im_derivat; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE status_vereinbarung_anforderung_im_derivat (
    id bigint NOT NULL,
    nach_zak boolean,
    anforderer_id bigint,
    anforderung_id bigint,
    derivat_id bigint
);


ALTER TABLE status_vereinbarung_anforderung_im_derivat OWNER TO pzbk;

--
-- TOC entry 210 (class 1259 OID 2399801)
-- Name: statusnext; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE statusnext (
    id integer NOT NULL,
    statusid integer,
    nextstatusid integer
);


ALTER TABLE statusnext OWNER TO pzbk;

--
-- TOC entry 221 (class 1259 OID 2399874)
-- Name: t_role_group; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE t_role_group (
    id bigint NOT NULL,
    groupname character varying(255),
    rolename character varying(255)
);


ALTER TABLE t_role_group OWNER TO pzbk;

--
-- TOC entry 190 (class 1259 OID 2399668)
-- Name: t_role_user; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE t_role_user (
    id bigint NOT NULL,
    rolename character varying(255),
    username character varying(255)
);


ALTER TABLE t_role_user OWNER TO pzbk;

--
-- TOC entry 217 (class 1259 OID 2399845)
-- Name: t_team; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE t_team (
    id bigint NOT NULL,
    team_name character varying(255),
    teamleiter_id bigint
);


ALTER TABLE t_team OWNER TO pzbk;

--
-- TOC entry 202 (class 1259 OID 2399743)
-- Name: t_users; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE t_users (
    qnumber character varying(255) NOT NULL,
    password character varying(255)
);


ALTER TABLE t_users OWNER TO pzbk;

--
-- TOC entry 191 (class 1259 OID 2399676)
-- Name: ub_zu_kova_phase_im_derivat_und_sensorcoc; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE ub_zu_kova_phase_im_derivat_und_sensorcoc (
    id bigint NOT NULL,
    kovaimderivat_id bigint,
    sensorcoc_sensorcocid bigint,
    umsetzungsbestaetiger_id bigint
);


ALTER TABLE ub_zu_kova_phase_im_derivat_und_sensorcoc OWNER TO pzbk;

--
-- TOC entry 201 (class 1259 OID 2399738)
-- Name: umsetzer; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE umsetzer (
    umsetzer_id bigint NOT NULL,
    komponente_id bigint,
    seteam_id bigint
);


ALTER TABLE umsetzer OWNER TO pzbk;

--
-- TOC entry 197 (class 1259 OID 2399712)
-- Name: umsetzungsbestaetigung; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE umsetzungsbestaetigung (
    id bigint NOT NULL,
    datum timestamp without time zone,
    kommentar character varying(10000),
    status integer,
    anforderung_id bigint,
    kovaimderivat_id bigint,
    umsetzungsbestaetiger_id bigint
);


ALTER TABLE umsetzungsbestaetigung OWNER TO pzbk;

--
-- TOC entry 214 (class 1259 OID 2399824)
-- Name: user_defined_search; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE user_defined_search (
    id bigint NOT NULL,
    name character varying(255),
    mitarbeiter_id bigint
);


ALTER TABLE user_defined_search OWNER TO pzbk;

--
-- TOC entry 248 (class 1259 OID 2400010)
-- Name: user_defined_search_search_filter; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE user_defined_search_search_filter (
    userdefinedsearch_id bigint NOT NULL,
    searchfilterlist_id bigint NOT NULL
);


ALTER TABLE user_defined_search_search_filter OWNER TO pzbk;

--
-- TOC entry 231 (class 1259 OID 2399930)
-- Name: user_to_group; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE user_to_group (
    qnumber character varying(255) NOT NULL,
    gruppenname character varying(255) NOT NULL
);


ALTER TABLE user_to_group OWNER TO pzbk;

--
-- TOC entry 243 (class 1259 OID 2399985)
-- Name: usersettings_selectedderivate; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE usersettings_selectedderivate (
    usersettings_id bigint NOT NULL,
    selectedderivat_id bigint NOT NULL
);


ALTER TABLE usersettings_selectedderivate OWNER TO pzbk;

--
-- TOC entry 244 (class 1259 OID 2399990)
-- Name: usersettings_selectedzakderivate; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE usersettings_selectedzakderivate (
    usersettings_id bigint NOT NULL,
    selectedzakderivat_id bigint NOT NULL
);


ALTER TABLE usersettings_selectedzakderivate OWNER TO pzbk;

--
-- TOC entry 188 (class 1259 OID 2399652)
-- Name: wert; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE wert (
    id bigint NOT NULL,
    attribut character varying(10000),
    wert character varying(10000)
);


ALTER TABLE wert OWNER TO pzbk;

--
-- TOC entry 222 (class 1259 OID 2399882)
-- Name: zak_uebertragung; Type: TABLE; Schema: public; Owner: pzbk
--

CREATE TABLE zak_uebertragung (
    id bigint NOT NULL,
    aenderungsdatum timestamp without time zone,
    umsetzer character varying(255),
    zak_kommentar character varying(255),
    zakstatus integer,
    zak_uebertrager character varying(255),
    zakuebertragung timestamp without time zone,
    zak_uebertragung_id bigint,
    derivatanforderung_id bigint,
    modul_name character varying(255)
);


ALTER TABLE zak_uebertragung OWNER TO pzbk;

--
-- TOC entry 2280 (class 2606 OID 1587012)
-- Name: EJB__TIMER__TBL PK_EJB__TIMER__TBL; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY "EJB__TIMER__TBL"
    ADD CONSTRAINT "PK_EJB__TIMER__TBL" PRIMARY KEY ("TIMERID");


--
-- TOC entry 2360 (class 2606 OID 2399899)
-- Name: anforderung_anforderung_freigabe_bei_modul anforderung_anforderung_freigabe_bei_modul_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_anforderung_freigabe_bei_modul
    ADD CONSTRAINT anforderung_anforderung_freigabe_bei_modul_pkey PRIMARY KEY (anforderung_id, anfofreigabebeimodul_id);


--
-- TOC entry 2362 (class 2606 OID 2399904)
-- Name: anforderung_anhang anforderung_anhang_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_anhang
    ADD CONSTRAINT anforderung_anhang_pkey PRIMARY KEY (anforderung_id, anhaenge_id);


--
-- TOC entry 2364 (class 2606 OID 2399909)
-- Name: anforderung_auswirkung anforderung_auswirkung_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_auswirkung
    ADD CONSTRAINT anforderung_auswirkung_pkey PRIMARY KEY (anforderung_id, auswirkungen_id);


--
-- TOC entry 2366 (class 2606 OID 2399914)
-- Name: anforderung_festgestelltin anforderung_festgestelltin_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_festgestelltin
    ADD CONSTRAINT anforderung_festgestelltin_pkey PRIMARY KEY (anforderung_id, festgestelltin_id);


--
-- TOC entry 2294 (class 2606 OID 2399685)
-- Name: anforderung_freigabe_bei_modul anforderung_freigabe_bei_modul_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_freigabe_bei_modul
    ADD CONSTRAINT anforderung_freigabe_bei_modul_pkey PRIMARY KEY (id);


--
-- TOC entry 2324 (class 2606 OID 2399776)
-- Name: anforderung_history anforderung_history_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_history
    ADD CONSTRAINT anforderung_history_pkey PRIMARY KEY (id);


--
-- TOC entry 2284 (class 2606 OID 2399651)
-- Name: anforderung anforderung_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT anforderung_pkey PRIMARY KEY (id);


--
-- TOC entry 2370 (class 2606 OID 2399924)
-- Name: anforderung_umsetzer anforderung_umsetzer_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_umsetzer
    ADD CONSTRAINT anforderung_umsetzer_pkey PRIMARY KEY (anforderung_id, umsetzer_umsetzer_id);


--
-- TOC entry 2342 (class 2606 OID 2399836)
-- Name: anhang anhang_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anhang
    ADD CONSTRAINT anhang_pkey PRIMARY KEY (id);


--
-- TOC entry 2288 (class 2606 OID 2399667)
-- Name: anwender anwender_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anwender
    ADD CONSTRAINT anwender_pkey PRIMARY KEY (qnumber);


--
-- TOC entry 2344 (class 2606 OID 2399844)
-- Name: auswirkung auswirkung_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY auswirkung
    ADD CONSTRAINT auswirkung_pkey PRIMARY KEY (id);


--
-- TOC entry 2304 (class 2606 OID 2399711)
-- Name: berechtigung_derivat berechtigung_derivat_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY berechtigung_derivat
    ADD CONSTRAINT berechtigung_derivat_pkey PRIMARY KEY (id);


--
-- TOC entry 2338 (class 2606 OID 2399823)
-- Name: berechtigung berechtigung_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY berechtigung
    ADD CONSTRAINT berechtigung_pkey PRIMARY KEY (id);


--
-- TOC entry 2348 (class 2606 OID 2399857)
-- Name: berechtigunghistory berechtigunghistory_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY berechtigunghistory
    ADD CONSTRAINT berechtigunghistory_pkey PRIMARY KEY (id);


--
-- TOC entry 2350 (class 2606 OID 2399865)
-- Name: bild bild_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY bild
    ADD CONSTRAINT bild_pkey PRIMARY KEY (id);


--
-- TOC entry 2302 (class 2606 OID 2399706)
-- Name: dbfile dbfile_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY dbfile
    ADD CONSTRAINT dbfile_pkey PRIMARY KEY (id);


--
-- TOC entry 2384 (class 2606 OID 2399962)
-- Name: derivat_kov_a_phase_im_derivat derivat_kov_a_phase_im_derivat_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY derivat_kov_a_phase_im_derivat
    ADD CONSTRAINT derivat_kov_a_phase_im_derivat_pkey PRIMARY KEY (derivat_id, kovaphasen_id);


--
-- TOC entry 2328 (class 2606 OID 2399792)
-- Name: derivat derivat_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY derivat
    ADD CONSTRAINT derivat_pkey PRIMARY KEY (id);


--
-- TOC entry 2330 (class 2606 OID 2399800)
-- Name: festgestelltin festgestelltin_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY festgestelltin
    ADD CONSTRAINT festgestelltin_pkey PRIMARY KEY (id);


--
-- TOC entry 2300 (class 2606 OID 2399698)
-- Name: infotext infotext_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY infotext
    ADD CONSTRAINT infotext_pkey PRIMARY KEY (id);


--
-- TOC entry 2312 (class 2606 OID 2399737)
-- Name: kov_a_phase_im_derivat kov_a_phase_im_derivat_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY kov_a_phase_im_derivat
    ADD CONSTRAINT kov_a_phase_im_derivat_pkey PRIMARY KEY (id);


--
-- TOC entry 2320 (class 2606 OID 2399763)
-- Name: localgroup localgroup_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY localgroup
    ADD CONSTRAINT localgroup_pkey PRIMARY KEY (gruppenname);


--
-- TOC entry 2368 (class 2606 OID 2399919)
-- Name: meldung_anforderung meldung_anforderung_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung_anforderung
    ADD CONSTRAINT meldung_anforderung_pkey PRIMARY KEY (anforderung_id, meldung_id);


--
-- TOC entry 2376 (class 2606 OID 2399942)
-- Name: meldung_anhang meldung_anhang_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung_anhang
    ADD CONSTRAINT meldung_anhang_pkey PRIMARY KEY (meldung_id, anhaenge_id);


--
-- TOC entry 2378 (class 2606 OID 2399947)
-- Name: meldung_auswirkung meldung_auswirkung_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung_auswirkung
    ADD CONSTRAINT meldung_auswirkung_pkey PRIMARY KEY (meldung_id, auswirkungen_id);


--
-- TOC entry 2380 (class 2606 OID 2399952)
-- Name: meldung_festgestelltin meldung_festgestelltin_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung_festgestelltin
    ADD CONSTRAINT meldung_festgestelltin_pkey PRIMARY KEY (meldung_id, festgestelltin_id);


--
-- TOC entry 2318 (class 2606 OID 2399758)
-- Name: meldung meldung_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT meldung_pkey PRIMARY KEY (id);


--
-- TOC entry 2386 (class 2606 OID 2399979)
-- Name: mitarbeiter_derivat mitarbeiter_derivat_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_derivat
    ADD CONSTRAINT mitarbeiter_derivat_pkey PRIMARY KEY (mitarbeiter_id, derivaten_id);


--
-- TOC entry 2388 (class 2606 OID 2399984)
-- Name: mitarbeiter_modul mitarbeiter_modul_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_modul
    ADD CONSTRAINT mitarbeiter_modul_pkey PRIMARY KEY (mitarbeiter_id, moduls_id);


--
-- TOC entry 2336 (class 2606 OID 2399818)
-- Name: mitarbeiter mitarbeiter_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter
    ADD CONSTRAINT mitarbeiter_pkey PRIMARY KEY (id);


--
-- TOC entry 2396 (class 2606 OID 2400004)
-- Name: mitarbeiter_sensorcoc_lese mitarbeiter_sensorcoc_lese_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_sensorcoc_lese
    ADD CONSTRAINT mitarbeiter_sensorcoc_lese_pkey PRIMARY KEY (mitarbeiter_id, sensorcocslese_sensorcocid);


--
-- TOC entry 2394 (class 2606 OID 2399999)
-- Name: mitarbeiter_sensorcoc mitarbeiter_sensorcoc_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_sensorcoc
    ADD CONSTRAINT mitarbeiter_sensorcoc_pkey PRIMARY KEY (mitarbeiter_id, sensorcoc_sensorcocid);


--
-- TOC entry 2398 (class 2606 OID 2400009)
-- Name: mitarbeiter_tteam mitarbeiter_tteam_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_tteam
    ADD CONSTRAINT mitarbeiter_tteam_pkey PRIMARY KEY (mitarbeiter_id, tteams_id);


--
-- TOC entry 2402 (class 2606 OID 2400019)
-- Name: modul_modulseteam modul_modulseteam_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modul_modulseteam
    ADD CONSTRAINT modul_modulseteam_pkey PRIMARY KEY (modul_id, seteams_id);


--
-- TOC entry 2352 (class 2606 OID 2399873)
-- Name: modul modul_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modul
    ADD CONSTRAINT modul_pkey PRIMARY KEY (id);


--
-- TOC entry 2322 (class 2606 OID 2399768)
-- Name: modulbild modulbild_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modulbild
    ADD CONSTRAINT modulbild_pkey PRIMARY KEY (id);


--
-- TOC entry 2308 (class 2606 OID 2399727)
-- Name: modulkomponente modulkomponente_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modulkomponente
    ADD CONSTRAINT modulkomponente_pkey PRIMARY KEY (id);


--
-- TOC entry 2372 (class 2606 OID 2399929)
-- Name: modulseteam_modulkomponente modulseteam_modulkomponente_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modulseteam_modulkomponente
    ADD CONSTRAINT modulseteam_modulkomponente_pkey PRIMARY KEY (modulseteam_id, komponenten_id);


--
-- TOC entry 2310 (class 2606 OID 2399732)
-- Name: modulseteam modulseteam_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modulseteam
    ADD CONSTRAINT modulseteam_pkey PRIMARY KEY (id);


--
-- TOC entry 2358 (class 2606 OID 2399894)
-- Name: pzbk pzbk_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY pzbk
    ADD CONSTRAINT pzbk_pkey PRIMARY KEY (id);


--
-- TOC entry 2334 (class 2606 OID 2399810)
-- Name: search_filter search_filter_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY search_filter
    ADD CONSTRAINT search_filter_pkey PRIMARY KEY (id);


--
-- TOC entry 2382 (class 2606 OID 2399957)
-- Name: sensorcoc_mitarbeiter sensorcoc_mitarbeiter_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY sensorcoc_mitarbeiter
    ADD CONSTRAINT sensorcoc_mitarbeiter_pkey PRIMARY KEY (sensorcoc_sensorcocid, umsetzungsbestaetiger_id);


--
-- TOC entry 2326 (class 2606 OID 2399784)
-- Name: sensorcoc sensorcoc_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY sensorcoc
    ADD CONSTRAINT sensorcoc_pkey PRIMARY KEY (sensorcocid);


--
-- TOC entry 2282 (class 2606 OID 2399563)
-- Name: sequence sequence_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY sequence
    ADD CONSTRAINT sequence_pkey PRIMARY KEY (seq_name);


--
-- TOC entry 2296 (class 2606 OID 2399690)
-- Name: status_vereinbarung_anforderung_im_derivat status_vereinbarung_anforderung_im_derivat_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT status_vereinbarung_anforderung_im_derivat_pkey PRIMARY KEY (id);


--
-- TOC entry 2332 (class 2606 OID 2399805)
-- Name: statusnext statusnext_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY statusnext
    ADD CONSTRAINT statusnext_pkey PRIMARY KEY (id);


--
-- TOC entry 2354 (class 2606 OID 2399881)
-- Name: t_role_group t_role_group_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY t_role_group
    ADD CONSTRAINT t_role_group_pkey PRIMARY KEY (id);


--
-- TOC entry 2290 (class 2606 OID 2399675)
-- Name: t_role_user t_role_user_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY t_role_user
    ADD CONSTRAINT t_role_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2346 (class 2606 OID 2399849)
-- Name: t_team t_team_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY t_team
    ADD CONSTRAINT t_team_pkey PRIMARY KEY (id);


--
-- TOC entry 2316 (class 2606 OID 2399750)
-- Name: t_users t_users_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY t_users
    ADD CONSTRAINT t_users_pkey PRIMARY KEY (qnumber);


--
-- TOC entry 2292 (class 2606 OID 2399680)
-- Name: ub_zu_kova_phase_im_derivat_und_sensorcoc ub_zu_kova_phase_im_derivat_und_sensorcoc_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc
    ADD CONSTRAINT ub_zu_kova_phase_im_derivat_und_sensorcoc_pkey PRIMARY KEY (id);


--
-- TOC entry 2314 (class 2606 OID 2399742)
-- Name: umsetzer umsetzer_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY umsetzer
    ADD CONSTRAINT umsetzer_pkey PRIMARY KEY (umsetzer_id);


--
-- TOC entry 2306 (class 2606 OID 2399719)
-- Name: umsetzungsbestaetigung umsetzungsbestaetigung_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY umsetzungsbestaetigung
    ADD CONSTRAINT umsetzungsbestaetigung_pkey PRIMARY KEY (id);


--
-- TOC entry 2298 (class 2606 OID 2400021)
-- Name: status_vereinbarung_anforderung_im_derivat unq_status_vereinbarung_anforderung_im_derivat_0; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT unq_status_vereinbarung_anforderung_im_derivat_0 UNIQUE (anforderung_id, derivat_id);


--
-- TOC entry 2340 (class 2606 OID 2399828)
-- Name: user_defined_search user_defined_search_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY user_defined_search
    ADD CONSTRAINT user_defined_search_pkey PRIMARY KEY (id);


--
-- TOC entry 2400 (class 2606 OID 2400014)
-- Name: user_defined_search_search_filter user_defined_search_search_filter_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY user_defined_search_search_filter
    ADD CONSTRAINT user_defined_search_search_filter_pkey PRIMARY KEY (userdefinedsearch_id, searchfilterlist_id);


--
-- TOC entry 2374 (class 2606 OID 2399937)
-- Name: user_to_group user_to_group_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY user_to_group
    ADD CONSTRAINT user_to_group_pkey PRIMARY KEY (qnumber, gruppenname);


--
-- TOC entry 2390 (class 2606 OID 2399989)
-- Name: usersettings_selectedderivate usersettings_selectedderivate_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY usersettings_selectedderivate
    ADD CONSTRAINT usersettings_selectedderivate_pkey PRIMARY KEY (usersettings_id, selectedderivat_id);


--
-- TOC entry 2392 (class 2606 OID 2399994)
-- Name: usersettings_selectedzakderivate usersettings_selectedzakderivate_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY usersettings_selectedzakderivate
    ADD CONSTRAINT usersettings_selectedzakderivate_pkey PRIMARY KEY (usersettings_id, selectedzakderivat_id);


--
-- TOC entry 2286 (class 2606 OID 2399659)
-- Name: wert wert_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY wert
    ADD CONSTRAINT wert_pkey PRIMARY KEY (id);


--
-- TOC entry 2356 (class 2606 OID 2399889)
-- Name: zak_uebertragung zak_uebertragung_pkey; Type: CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY zak_uebertragung
    ADD CONSTRAINT zak_uebertragung_pkey PRIMARY KEY (id);


--
-- TOC entry 2443 (class 2606 OID 2400222)
-- Name: anforderung_anforderung_freigabe_bei_modul anforderunganforderungfreigabe_bei_modulanfofreigabebeimodul_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_anforderung_freigabe_bei_modul
    ADD CONSTRAINT anforderunganforderungfreigabe_bei_modulanfofreigabebeimodul_id FOREIGN KEY (anfofreigabebeimodul_id) REFERENCES anforderung_freigabe_bei_modul(id);


--
-- TOC entry 2442 (class 2606 OID 2400217)
-- Name: anforderung_anforderung_freigabe_bei_modul fk_anforderung_anforderung_freigabe_bei_modul_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_anforderung_freigabe_bei_modul
    ADD CONSTRAINT fk_anforderung_anforderung_freigabe_bei_modul_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2445 (class 2606 OID 2400232)
-- Name: anforderung_anhang fk_anforderung_anhang_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_anhang
    ADD CONSTRAINT fk_anforderung_anhang_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2444 (class 2606 OID 2400227)
-- Name: anforderung_anhang fk_anforderung_anhang_anhaenge_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_anhang
    ADD CONSTRAINT fk_anforderung_anhang_anhaenge_id FOREIGN KEY (anhaenge_id) REFERENCES anhang(id);


--
-- TOC entry 2446 (class 2606 OID 2400237)
-- Name: anforderung_auswirkung fk_anforderung_auswirkung_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_auswirkung
    ADD CONSTRAINT fk_anforderung_auswirkung_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2447 (class 2606 OID 2400242)
-- Name: anforderung_auswirkung fk_anforderung_auswirkung_auswirkungen_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_auswirkung
    ADD CONSTRAINT fk_anforderung_auswirkung_auswirkungen_id FOREIGN KEY (auswirkungen_id) REFERENCES auswirkung(id);


--
-- TOC entry 2404 (class 2606 OID 2400027)
-- Name: anforderung fk_anforderung_bild_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_bild_id FOREIGN KEY (bild_id) REFERENCES bild(id);


--
-- TOC entry 2405 (class 2606 OID 2400032)
-- Name: anforderung fk_anforderung_fachlicheransprechpartner_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_fachlicheransprechpartner_id FOREIGN KEY (fachlicheransprechpartner_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2449 (class 2606 OID 2400252)
-- Name: anforderung_festgestelltin fk_anforderung_festgestelltin_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_festgestelltin
    ADD CONSTRAINT fk_anforderung_festgestelltin_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2448 (class 2606 OID 2400247)
-- Name: anforderung_festgestelltin fk_anforderung_festgestelltin_festgestelltin_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_festgestelltin
    ADD CONSTRAINT fk_anforderung_festgestelltin_festgestelltin_id FOREIGN KEY (festgestelltin_id) REFERENCES festgestelltin(id);


--
-- TOC entry 2413 (class 2606 OID 2400072)
-- Name: anforderung_freigabe_bei_modul fk_anforderung_freigabe_bei_modul_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_freigabe_bei_modul
    ADD CONSTRAINT fk_anforderung_freigabe_bei_modul_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2412 (class 2606 OID 2400067)
-- Name: anforderung_freigabe_bei_modul fk_anforderung_freigabe_bei_modul_bearbeiter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_freigabe_bei_modul
    ADD CONSTRAINT fk_anforderung_freigabe_bei_modul_bearbeiter_id FOREIGN KEY (bearbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2414 (class 2606 OID 2400077)
-- Name: anforderung_freigabe_bei_modul fk_anforderung_freigabe_bei_modul_modul_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_freigabe_bei_modul
    ADD CONSTRAINT fk_anforderung_freigabe_bei_modul_modul_id FOREIGN KEY (modul_id) REFERENCES modul(id);


--
-- TOC entry 2403 (class 2606 OID 2400022)
-- Name: anforderung fk_anforderung_sensor_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_sensor_id FOREIGN KEY (sensor_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2408 (class 2606 OID 2400047)
-- Name: anforderung fk_anforderung_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- TOC entry 2406 (class 2606 OID 2400037)
-- Name: anforderung fk_anforderung_teamleiter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_teamleiter_id FOREIGN KEY (teamleiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2407 (class 2606 OID 2400042)
-- Name: anforderung fk_anforderung_tteam_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_tteam_id FOREIGN KEY (tteam_id) REFERENCES t_team(id);


--
-- TOC entry 2452 (class 2606 OID 2400267)
-- Name: anforderung_umsetzer fk_anforderung_umsetzer_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_umsetzer
    ADD CONSTRAINT fk_anforderung_umsetzer_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2453 (class 2606 OID 2400272)
-- Name: anforderung_umsetzer fk_anforderung_umsetzer_umsetzer_umsetzer_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anforderung_umsetzer
    ADD CONSTRAINT fk_anforderung_umsetzer_umsetzer_umsetzer_id FOREIGN KEY (umsetzer_umsetzer_id) REFERENCES umsetzer(umsetzer_id);


--
-- TOC entry 2438 (class 2606 OID 2400197)
-- Name: anhang fk_anhang_content_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anhang
    ADD CONSTRAINT fk_anhang_content_id FOREIGN KEY (content_id) REFERENCES dbfile(id);


--
-- TOC entry 2439 (class 2606 OID 2400202)
-- Name: anhang fk_anhang_creator_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY anhang
    ADD CONSTRAINT fk_anhang_creator_id FOREIGN KEY (creator_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2418 (class 2606 OID 2400097)
-- Name: berechtigung_derivat fk_berechtigung_derivat_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY berechtigung_derivat
    ADD CONSTRAINT fk_berechtigung_derivat_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2436 (class 2606 OID 2400187)
-- Name: berechtigung fk_berechtigung_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY berechtigung
    ADD CONSTRAINT fk_berechtigung_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2467 (class 2606 OID 2400342)
-- Name: derivat_kov_a_phase_im_derivat fk_derivat_kov_a_phase_im_derivat_derivat_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY derivat_kov_a_phase_im_derivat
    ADD CONSTRAINT fk_derivat_kov_a_phase_im_derivat_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);


--
-- TOC entry 2466 (class 2606 OID 2400337)
-- Name: derivat_kov_a_phase_im_derivat fk_derivat_kov_a_phase_im_derivat_kovaphasen_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY derivat_kov_a_phase_im_derivat
    ADD CONSTRAINT fk_derivat_kov_a_phase_im_derivat_kovaphasen_id FOREIGN KEY (kovaphasen_id) REFERENCES kov_a_phase_im_derivat(id);


--
-- TOC entry 2424 (class 2606 OID 2400127)
-- Name: kov_a_phase_im_derivat fk_kov_a_phase_im_derivat_derivat_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY kov_a_phase_im_derivat
    ADD CONSTRAINT fk_kov_a_phase_im_derivat_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);


--
-- TOC entry 2450 (class 2606 OID 2400257)
-- Name: meldung_anforderung fk_meldung_anforderung_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung_anforderung
    ADD CONSTRAINT fk_meldung_anforderung_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2451 (class 2606 OID 2400262)
-- Name: meldung_anforderung fk_meldung_anforderung_meldung_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung_anforderung
    ADD CONSTRAINT fk_meldung_anforderung_meldung_id FOREIGN KEY (meldung_id) REFERENCES meldung(id);


--
-- TOC entry 2459 (class 2606 OID 2400297)
-- Name: meldung_anhang fk_meldung_anhang_anhaenge_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung_anhang
    ADD CONSTRAINT fk_meldung_anhang_anhaenge_id FOREIGN KEY (anhaenge_id) REFERENCES anhang(id);


--
-- TOC entry 2458 (class 2606 OID 2400302)
-- Name: meldung_anhang fk_meldung_anhang_meldung_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung_anhang
    ADD CONSTRAINT fk_meldung_anhang_meldung_id FOREIGN KEY (meldung_id) REFERENCES meldung(id);


--
-- TOC entry 2460 (class 2606 OID 2400307)
-- Name: meldung_auswirkung fk_meldung_auswirkung_auswirkungen_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung_auswirkung
    ADD CONSTRAINT fk_meldung_auswirkung_auswirkungen_id FOREIGN KEY (auswirkungen_id) REFERENCES auswirkung(id);


--
-- TOC entry 2461 (class 2606 OID 2400312)
-- Name: meldung_auswirkung fk_meldung_auswirkung_meldung_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung_auswirkung
    ADD CONSTRAINT fk_meldung_auswirkung_meldung_id FOREIGN KEY (meldung_id) REFERENCES meldung(id);


--
-- TOC entry 2429 (class 2606 OID 2400152)
-- Name: meldung fk_meldung_bild_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_bild_id FOREIGN KEY (bild_id) REFERENCES bild(id);


--
-- TOC entry 2431 (class 2606 OID 2400162)
-- Name: meldung fk_meldung_fachlicheransprechpartner_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_fachlicheransprechpartner_id FOREIGN KEY (fachlicheransprechpartner_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2463 (class 2606 OID 2400322)
-- Name: meldung_festgestelltin fk_meldung_festgestelltin_festgestelltin_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung_festgestelltin
    ADD CONSTRAINT fk_meldung_festgestelltin_festgestelltin_id FOREIGN KEY (festgestelltin_id) REFERENCES festgestelltin(id);


--
-- TOC entry 2462 (class 2606 OID 2400317)
-- Name: meldung_festgestelltin fk_meldung_festgestelltin_meldung_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung_festgestelltin
    ADD CONSTRAINT fk_meldung_festgestelltin_meldung_id FOREIGN KEY (meldung_id) REFERENCES meldung(id);


--
-- TOC entry 2428 (class 2606 OID 2400147)
-- Name: meldung fk_meldung_sensor_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_sensor_id FOREIGN KEY (sensor_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2430 (class 2606 OID 2400157)
-- Name: meldung fk_meldung_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- TOC entry 2432 (class 2606 OID 2400167)
-- Name: meldung fk_meldung_teamleiter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_teamleiter_id FOREIGN KEY (teamleiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2427 (class 2606 OID 2400142)
-- Name: meldung fk_meldung_tteam_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_tteam_id FOREIGN KEY (tteam_id) REFERENCES t_team(id);


--
-- TOC entry 2473 (class 2606 OID 2400372)
-- Name: mitarbeiter_derivat fk_mitarbeiter_derivat_derivaten_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_derivat
    ADD CONSTRAINT fk_mitarbeiter_derivat_derivaten_id FOREIGN KEY (derivaten_id) REFERENCES derivat(id);


--
-- TOC entry 2472 (class 2606 OID 2400367)
-- Name: mitarbeiter_derivat fk_mitarbeiter_derivat_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_derivat
    ADD CONSTRAINT fk_mitarbeiter_derivat_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2474 (class 2606 OID 2400377)
-- Name: mitarbeiter_modul fk_mitarbeiter_modul_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_modul
    ADD CONSTRAINT fk_mitarbeiter_modul_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2475 (class 2606 OID 2400382)
-- Name: mitarbeiter_modul fk_mitarbeiter_modul_moduls_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_modul
    ADD CONSTRAINT fk_mitarbeiter_modul_moduls_id FOREIGN KEY (moduls_id) REFERENCES modul(id);


--
-- TOC entry 2482 (class 2606 OID 2400417)
-- Name: mitarbeiter_sensorcoc_lese fk_mitarbeiter_sensorcoc_lese_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_sensorcoc_lese
    ADD CONSTRAINT fk_mitarbeiter_sensorcoc_lese_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2483 (class 2606 OID 2400422)
-- Name: mitarbeiter_sensorcoc_lese fk_mitarbeiter_sensorcoc_lese_sensorcocslese_sensorcocid; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_sensorcoc_lese
    ADD CONSTRAINT fk_mitarbeiter_sensorcoc_lese_sensorcocslese_sensorcocid FOREIGN KEY (sensorcocslese_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- TOC entry 2480 (class 2606 OID 2400407)
-- Name: mitarbeiter_sensorcoc fk_mitarbeiter_sensorcoc_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_sensorcoc
    ADD CONSTRAINT fk_mitarbeiter_sensorcoc_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2481 (class 2606 OID 2400412)
-- Name: mitarbeiter_sensorcoc fk_mitarbeiter_sensorcoc_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_sensorcoc
    ADD CONSTRAINT fk_mitarbeiter_sensorcoc_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- TOC entry 2485 (class 2606 OID 2400432)
-- Name: mitarbeiter_tteam fk_mitarbeiter_tteam_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_tteam
    ADD CONSTRAINT fk_mitarbeiter_tteam_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2484 (class 2606 OID 2400427)
-- Name: mitarbeiter_tteam fk_mitarbeiter_tteam_tteams_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_tteam
    ADD CONSTRAINT fk_mitarbeiter_tteam_tteams_id FOREIGN KEY (tteams_id) REFERENCES t_team(id);


--
-- TOC entry 2471 (class 2606 OID 2400362)
-- Name: mitarbeiter_zakeinordnungen fk_mitarbeiter_zakeinordnungen_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY mitarbeiter_zakeinordnungen
    ADD CONSTRAINT fk_mitarbeiter_zakeinordnungen_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2441 (class 2606 OID 2400212)
-- Name: modul fk_modul_modulbild_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modul
    ADD CONSTRAINT fk_modul_modulbild_id FOREIGN KEY (modulbild_id) REFERENCES bild(id);


--
-- TOC entry 2488 (class 2606 OID 2400447)
-- Name: modul_modulseteam fk_modul_modulseteam_modul_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modul_modulseteam
    ADD CONSTRAINT fk_modul_modulseteam_modul_id FOREIGN KEY (modul_id) REFERENCES modul(id);


--
-- TOC entry 2489 (class 2606 OID 2400452)
-- Name: modul_modulseteam fk_modul_modulseteam_seteams_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modul_modulseteam
    ADD CONSTRAINT fk_modul_modulseteam_seteams_id FOREIGN KEY (seteams_id) REFERENCES modulseteam(id);


--
-- TOC entry 2434 (class 2606 OID 2400177)
-- Name: modulbild fk_modulbild_bild_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modulbild
    ADD CONSTRAINT fk_modulbild_bild_id FOREIGN KEY (bild_id) REFERENCES anhang(id);


--
-- TOC entry 2433 (class 2606 OID 2400172)
-- Name: modulbild fk_modulbild_modul_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modulbild
    ADD CONSTRAINT fk_modulbild_modul_id FOREIGN KEY (modul_id) REFERENCES modul(id);


--
-- TOC entry 2422 (class 2606 OID 2400117)
-- Name: modulkomponente fk_modulkomponente_seteam_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modulkomponente
    ADD CONSTRAINT fk_modulkomponente_seteam_id FOREIGN KEY (seteam_id) REFERENCES modulseteam(id);


--
-- TOC entry 2423 (class 2606 OID 2400122)
-- Name: modulseteam fk_modulseteam_elternmodul_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modulseteam
    ADD CONSTRAINT fk_modulseteam_elternmodul_id FOREIGN KEY (elternmodul_id) REFERENCES modul(id);


--
-- TOC entry 2455 (class 2606 OID 2400282)
-- Name: modulseteam_modulkomponente fk_modulseteam_modulkomponente_komponenten_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modulseteam_modulkomponente
    ADD CONSTRAINT fk_modulseteam_modulkomponente_komponenten_id FOREIGN KEY (komponenten_id) REFERENCES modulkomponente(id);


--
-- TOC entry 2454 (class 2606 OID 2400277)
-- Name: modulseteam_modulkomponente fk_modulseteam_modulkomponente_modulseteam_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY modulseteam_modulkomponente
    ADD CONSTRAINT fk_modulseteam_modulkomponente_modulseteam_id FOREIGN KEY (modulseteam_id) REFERENCES modulseteam(id);


--
-- TOC entry 2468 (class 2606 OID 2400347)
-- Name: searchfilter_longlistvalue fk_searchfilter_longlistvalue_searchfilter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY searchfilter_longlistvalue
    ADD CONSTRAINT fk_searchfilter_longlistvalue_searchfilter_id FOREIGN KEY (searchfilter_id) REFERENCES search_filter(id);


--
-- TOC entry 2469 (class 2606 OID 2400352)
-- Name: searchfilter_statuslistvalue fk_searchfilter_statuslistvalue_searchfilter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY searchfilter_statuslistvalue
    ADD CONSTRAINT fk_searchfilter_statuslistvalue_searchfilter_id FOREIGN KEY (searchfilter_id) REFERENCES search_filter(id);


--
-- TOC entry 2470 (class 2606 OID 2400357)
-- Name: searchfilter_stringlistvalue fk_searchfilter_stringlistvalue_searchfilter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY searchfilter_stringlistvalue
    ADD CONSTRAINT fk_searchfilter_stringlistvalue_searchfilter_id FOREIGN KEY (searchfilter_id) REFERENCES search_filter(id);


--
-- TOC entry 2435 (class 2606 OID 2400182)
-- Name: sensorcoc fk_sensorcoc_fachlicheransprechpartner_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY sensorcoc
    ADD CONSTRAINT fk_sensorcoc_fachlicheransprechpartner_id FOREIGN KEY (fachlicheransprechpartner_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2465 (class 2606 OID 2400332)
-- Name: sensorcoc_mitarbeiter fk_sensorcoc_mitarbeiter_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY sensorcoc_mitarbeiter
    ADD CONSTRAINT fk_sensorcoc_mitarbeiter_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- TOC entry 2464 (class 2606 OID 2400327)
-- Name: sensorcoc_mitarbeiter fk_sensorcoc_mitarbeiter_umsetzungsbestaetiger_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY sensorcoc_mitarbeiter
    ADD CONSTRAINT fk_sensorcoc_mitarbeiter_umsetzungsbestaetiger_id FOREIGN KEY (umsetzungsbestaetiger_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2416 (class 2606 OID 2400087)
-- Name: status_vereinbarung_anforderung_im_derivat fk_status_vereinbarung_anforderung_im_derivat_anforderer_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT fk_status_vereinbarung_anforderung_im_derivat_anforderer_id FOREIGN KEY (anforderer_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2415 (class 2606 OID 2400082)
-- Name: status_vereinbarung_anforderung_im_derivat fk_status_vereinbarung_anforderung_im_derivat_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT fk_status_vereinbarung_anforderung_im_derivat_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2417 (class 2606 OID 2400092)
-- Name: status_vereinbarung_anforderung_im_derivat fk_status_vereinbarung_anforderung_im_derivat_derivat_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT fk_status_vereinbarung_anforderung_im_derivat_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);


--
-- TOC entry 2440 (class 2606 OID 2400207)
-- Name: t_team fk_t_team_teamleiter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY t_team
    ADD CONSTRAINT fk_t_team_teamleiter_id FOREIGN KEY (teamleiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2409 (class 2606 OID 2400052)
-- Name: ub_zu_kova_phase_im_derivat_und_sensorcoc fk_ub_zu_kova_phase_im_derivat_und_sensorcoc_kovaimderivat_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc
    ADD CONSTRAINT fk_ub_zu_kova_phase_im_derivat_und_sensorcoc_kovaimderivat_id FOREIGN KEY (kovaimderivat_id) REFERENCES kov_a_phase_im_derivat(id);


--
-- TOC entry 2426 (class 2606 OID 2400137)
-- Name: umsetzer fk_umsetzer_komponente_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY umsetzer
    ADD CONSTRAINT fk_umsetzer_komponente_id FOREIGN KEY (komponente_id) REFERENCES modulkomponente(id);


--
-- TOC entry 2425 (class 2606 OID 2400132)
-- Name: umsetzer fk_umsetzer_seteam_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY umsetzer
    ADD CONSTRAINT fk_umsetzer_seteam_id FOREIGN KEY (seteam_id) REFERENCES modulseteam(id);


--
-- TOC entry 2420 (class 2606 OID 2400107)
-- Name: umsetzungsbestaetigung fk_umsetzungsbestaetigung_anforderung_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY umsetzungsbestaetigung
    ADD CONSTRAINT fk_umsetzungsbestaetigung_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2421 (class 2606 OID 2400112)
-- Name: umsetzungsbestaetigung fk_umsetzungsbestaetigung_kovaimderivat_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY umsetzungsbestaetigung
    ADD CONSTRAINT fk_umsetzungsbestaetigung_kovaimderivat_id FOREIGN KEY (kovaimderivat_id) REFERENCES kov_a_phase_im_derivat(id);


--
-- TOC entry 2419 (class 2606 OID 2400102)
-- Name: umsetzungsbestaetigung fk_umsetzungsbestaetigung_umsetzungsbestaetiger_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY umsetzungsbestaetigung
    ADD CONSTRAINT fk_umsetzungsbestaetigung_umsetzungsbestaetiger_id FOREIGN KEY (umsetzungsbestaetiger_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2437 (class 2606 OID 2400192)
-- Name: user_defined_search fk_user_defined_search_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY user_defined_search
    ADD CONSTRAINT fk_user_defined_search_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2486 (class 2606 OID 2400437)
-- Name: user_defined_search_search_filter fk_user_defined_search_search_filter_searchfilterlist_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY user_defined_search_search_filter
    ADD CONSTRAINT fk_user_defined_search_search_filter_searchfilterlist_id FOREIGN KEY (searchfilterlist_id) REFERENCES search_filter(id);


--
-- TOC entry 2487 (class 2606 OID 2400442)
-- Name: user_defined_search_search_filter fk_user_defined_search_search_filter_userdefinedsearch_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY user_defined_search_search_filter
    ADD CONSTRAINT fk_user_defined_search_search_filter_userdefinedsearch_id FOREIGN KEY (userdefinedsearch_id) REFERENCES user_defined_search(id);


--
-- TOC entry 2456 (class 2606 OID 2400287)
-- Name: user_to_group fk_user_to_group_gruppenname; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY user_to_group
    ADD CONSTRAINT fk_user_to_group_gruppenname FOREIGN KEY (gruppenname) REFERENCES localgroup(gruppenname);


--
-- TOC entry 2457 (class 2606 OID 2400292)
-- Name: user_to_group fk_user_to_group_qnumber; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY user_to_group
    ADD CONSTRAINT fk_user_to_group_qnumber FOREIGN KEY (qnumber) REFERENCES t_users(qnumber);


--
-- TOC entry 2477 (class 2606 OID 2400392)
-- Name: usersettings_selectedderivate fk_usersettings_selectedderivate_selectedderivat_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY usersettings_selectedderivate
    ADD CONSTRAINT fk_usersettings_selectedderivate_selectedderivat_id FOREIGN KEY (selectedderivat_id) REFERENCES derivat(id);


--
-- TOC entry 2476 (class 2606 OID 2400387)
-- Name: usersettings_selectedderivate fk_usersettings_selectedderivate_usersettings_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY usersettings_selectedderivate
    ADD CONSTRAINT fk_usersettings_selectedderivate_usersettings_id FOREIGN KEY (usersettings_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2478 (class 2606 OID 2400397)
-- Name: usersettings_selectedzakderivate fk_usersettings_selectedzakderivate_selectedzakderivat_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY usersettings_selectedzakderivate
    ADD CONSTRAINT fk_usersettings_selectedzakderivate_selectedzakderivat_id FOREIGN KEY (selectedzakderivat_id) REFERENCES derivat(id);


--
-- TOC entry 2479 (class 2606 OID 2400402)
-- Name: usersettings_selectedzakderivate fk_usersettings_selectedzakderivate_usersettings_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY usersettings_selectedzakderivate
    ADD CONSTRAINT fk_usersettings_selectedzakderivate_usersettings_id FOREIGN KEY (usersettings_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2411 (class 2606 OID 2400062)
-- Name: ub_zu_kova_phase_im_derivat_und_sensorcoc ub_zu_kova_phase_im_derivat_und_sensorcoc_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc
    ADD CONSTRAINT ub_zu_kova_phase_im_derivat_und_sensorcoc_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- TOC entry 2410 (class 2606 OID 2400057)
-- Name: ub_zu_kova_phase_im_derivat_und_sensorcoc ubzukova_phase_im_derivat_und_sensorcocumsetzungsbestaetiger_id; Type: FK CONSTRAINT; Schema: public; Owner: pzbk
--

ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc
    ADD CONSTRAINT ubzukova_phase_im_derivat_und_sensorcocumsetzungsbestaetiger_id FOREIGN KEY (umsetzungsbestaetiger_id) REFERENCES mitarbeiter(id);


-- Completed on 2017-07-20 11:59:53

--
-- PostgreSQL database dump complete
--

