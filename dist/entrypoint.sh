#!/bin/sh

WARFILE="/opt/payara/deployments/*.war"

exec java -Xms16m -XX:+TieredCompilation -XX:TieredStopAtLevel=1 -XX:+UseSerialGC -XX:MinHeapFreeRatio=10 -XX:MaxHeapFreeRatio=25 -XX:GCTimeRatio=4 -XX:AdaptiveSizePolicyWeight=90 -jar /opt/payara/payara-micro.jar --disablephonehome "$@"
