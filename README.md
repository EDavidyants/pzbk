# BMW PZBK
Bestandteile:
- Anforderungsworkflow
- Derivatworkflow
- Zuordnung und Vereinbarung von Anforderungen im Derivat
- Umsetzungsbestätigung von Anforderungen im Derivat
- Reporting zum Anforderungsworkflow
- Reporting zum Derivatworkflow

## Code Qualität
Zustand der Anwendung in soarqube:

[![Quality Gate Status](https://codequality.if-lab.de/api/project_badges/measure?project=de.interfaceag%3APZBK&metric=alert_status)](https://codequality.if-lab.de/dashboard?id=de.interfaceag%3APZBK)
[![Bugs](https://codequality.if-lab.de/api/project_badges/measure?project=de.interfaceag%3APZBK&metric=bugs)](https://codequality.if-lab.de/dashboard?id=de.interfaceag%3APZBK)
[![Code Smells](https://codequality.if-lab.de/api/project_badges/measure?project=de.interfaceag%3APZBK&metric=code_smells)](https://codequality.if-lab.de/dashboard?id=de.interfaceag%3APZBK)
[![Vulnerabilities](https://codequality.if-lab.de/api/project_badges/measure?project=de.interfaceag%3APZBK&metric=vulnerabilities)](https://codequality.if-lab.de/dashboard?id=de.interfaceag%3APZBK)

## Informationen:
* Link zum  [Wiki](https://confluence.if.de/display/PZBK/BMW+PZBK)
* Link zur [Integration](https://integration.if-lab.de/pzbk/) Umgebung
* Link zur [Staging](https://staging.if-lab.de/PZBK-0.0.37/) Umgebung

## Git Wiki
Im [Git Wiki](https://git.if-lab.de/BMW/PZBK/wikis/home) befindet sich technische Dokumentation zu Git bzw. Gitlab.







