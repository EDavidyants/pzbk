FROM docker-registry.if-lab.de/bmw/payara-4:latest
MAINTAINER Christopher Zentgraf <christopher.zentgraf@interface-ag.de>

ENV POSTBOOT_COMMANDS /opt/payara41/payara-postboot.txt

ENV PG_DRIVER_PKG https://jdbc.postgresql.org/download/postgresql-42.2.1.jar
RUN wget --quiet -O /opt/payara41/glassfish/lib/pgjdbc.jar $PG_DRIVER_PKG

COPY --chown=payara:payara target/*.war /opt/payara41/deployments/
COPY --chown=payara:payara dist/payara-postboot.txt /opt/payara41/payara-postboot.txt

USER root
RUN mkdir -p /var/pzbk && chown payara:payara /var/pzbk
USER payara
VOLUME /var/pzbk

