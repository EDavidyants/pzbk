var anfoListInit = null;
var anfoListInitOffset = 250;
function reinitAnforderungList() {
    anfoListInit = null;
    initAnforderungList();
}

function initAnforderungList() {
    var now = new Date().getTime();
    if (anfoListInit === null || (anfoListInit + anfoListInitOffset) < now) {
        $(".anfoListItem .anfoListItemBody").off("click");
        $(".anfoListItem .anfoListItemBody").click(function (e) {
            $body = $(this);
            $kurz = $body.find('.anfoListItemBeschr.kurz');
            $lang = $body.find('.anfoListItemBeschr.lang');
            if (e.target === $kurz[0]) {
                showFullDescription();
            } else if (e.target === $lang[0]) {
                hideFullDescription();
            }
            return false;
        });
        refreshAnfoListVersions();
        anfoListInit = now;
    }
    registerGlobalEventListener_Click(anfoListActionsMenuClickListener);
//    appendButtonToFilterPanel();
}

function showFullDescription() {
    $kurz.css({display: 'none'});
    $lang.css({display: 'block'});
}

function hideFullDescription() {
    $kurz.css({display: 'block'});
    $lang.css({display: 'none'});
}

function refreshAnfoListVersions() {
    var $vWrapper = $(".anfoListItemVersionWrapper");
    $vWrapper.each(function () {
        showVersionButtons($(this));
    });
}

function showVersionButtons(parent, start) {
    var $buttons = parent.find("button.versionButton");
    var selIndex = $buttons.index(parent.find("button.versionButton.selected"));
    var $smLeft = parent.find(".show-more-left");
    var $smRight = parent.find(".show-more-right");
    if ($buttons.length > 3) {
        for (var i = 0; i < $buttons.length; i++) {
            $($buttons[i]).css({display: 'none'});
        }
        var startIndex;
        if (typeof start != 'undefined')
            startIndex = start;
        else if (selIndex === 0)
            startIndex = selIndex;
        else if (selIndex === ($buttons.length - 1))
            startIndex = (selIndex - 2);
        else {
            startIndex = selIndex - 1;
        }

        var endIndex = startIndex + 2; // 3 items
        for (var i = startIndex; i <= endIndex; i++) {
            $($buttons[i]).css({display: ''});
        }
        if (startIndex > 0) {
            $smLeft.css({display: ''});
        } else {
            $smLeft.css({display: 'none'});
        }
        if (endIndex < $buttons.length - 1) {
            $smRight.css({display: ''});
        } else {
            $smRight.css({display: 'none'});
        }
    } else {
        $smLeft.css({display: 'none'});
        $smRight.css({display: 'none'});
    }
}

function nextAnfoListVersion(button) {
    button = $(button);
    var $buttons = button.parent().find("button.versionButton");
    var lastVisIndex = -1;
    for (var i = 0; i < $buttons.length; i++) {
        if ($($buttons[i]).css("display") !== "none") {
            lastVisIndex = i;
        }
    }
    var end = lastVisIndex;
    for (var i = 1; i <= 3; i++) {
        if (lastVisIndex + i > $buttons.length) {
            break;
        }
        end = lastVisIndex + i;
    }
    showVersionButtons(button.parent(), end - 3);
}

function prevAnfoListVersion(button) {
    button = $(button);
    var $buttons = button.parent().find("button.versionButton");
    var firstVisIndex = -1;
    for (var i = 0; i < $buttons.length; i++) {
        if ($($buttons[i]).css("display") !== "none") {
            firstVisIndex = i;
            break;
        }
    }
    var start = firstVisIndex;
    for (var i = 1; i <= 3; i++) {
        if (firstVisIndex - i < 0) {
            break;
        }
        start = firstVisIndex - i;
    }
    showVersionButtons(button.parent(), start);
}

function toggleSaveSelection() {
    var link = $(".saveSelection");
    if (link.css('display') === 'none') {
        link.removeClass("hide");
        link.addClass("show");
    } else {
        link.removeClass("show");
        link.addClass("hide");
    }
}

function openSearchActionsMenu() {
    var ref = $(".save-search_actions_menu");
    ref.show();
}

function closeSearchActionsMenu() {
    var ref = $(".save-search_actions_menu");
    ref.hide();
}

function searchActionsMenuClickListener(event) {
    if ((!$(event.target).is('.save-search_actions_menu') && $('.save-search_actions_menu').has(event.target).length <= 0
            && !$(event.target).is('.topnav_save-search_wrapper') && $('.topnav_save-search_wrapper').has(event.target).length <= 0)
            && $('.save-search_actions_menu').css('display') !== 'none') {
        closeSearchActionsMenu();
    }
}

function toggleAnfoListActionsMenu() {
    var link = $(".anfoList_actions_menu");
    if (link.css('display') === 'none') {
        openAnfoListActionsMenu();
    } else {
        closeAnfoListActionsMenu();
    }
}

function openAnfoListActionsMenu() {
    var ref = $(".anfoList_actions_menu");
    ref.show();
}

function closeAnfoListActionsMenu() {
    var ref = $(".anfoList_actions_menu");
    ref.hide();
}


function anfoListActionsMenuClickListener(event) {
    if ((!$(event.target).is('.anfoList_actions_menu') && $('.anfoList_actions_menu').has(event.target).length <= 0
            && !$(event.target).is('.anfoList_actions_icon') && $('.anfoList_actions_icon').has(event.target).length <= 0)
            && $('.anfoList_actions_menu').css('display') !== 'none') {
        closeAnfoListActionsMenu();
    }
}

function copyToClipboard() {
    var text;
    text = document.getElementById('anfoListForm:urlwert');
    if (window.clipboardData) { // Internet Explorer
        window.clipboardData.setData("Text", text.value);
    } else {
        unsafeWindow.netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
        const clipboardHelper = Components.classes["@mozilla.org/widget/clipboardhelper;1"].getService(Components.interfaces.nsIClipboardHelper);
        clipboardHelper.copyString(text);
    }
}

function copyLinkToClipboard() {
    var text = document.getElementById('dialog:copyLinkDialogForm:displayCommendDialogContent:inputLink');
    console.error(text);

    text.focus();
    text.select();
    var status = document.execCommand('copy');
    if (!status) {
        console.error("Cannot copy text");
    } else {
        console.log("The text is now on the clipboard");
    }
}
