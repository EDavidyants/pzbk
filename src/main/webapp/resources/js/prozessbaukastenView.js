function toggleBeschreibung(rowNumber) {
    var pathForView = '#contentForm\\:content\\:tabView\\:zugeordneteAnforderungenForm\\:anforderungenTab\\:anforderungenTableForm\\:anforderungenTabContentEditMode\\:prozessbaukastenAnforderungenTabelle\\:' + rowNumber.toString();
    var pathForEdit = '#editForm\\:content\\:tabView\\:anforderungenTab\\:anforderungenTabContent\\:prozessbaukastenAnforderungenEditTable\\:' + rowNumber.toString();

    var fullBeschreibungLabelForView = $(pathForView + '\\:beschreibungTextFull');
    var previewBeschreibungLabelForView = $(pathForView + '\\:beschreibungTextPreview');

    var fullBeschreibungLabelForEdit = $(pathForEdit + '\\:beschreibungTextFull');
    var previewBeschreibungLabelForEdit = $(pathForEdit + '\\:beschreibungTextPreview');

    fullBeschreibungLabelForView.toggleClass('displayNone');
    previewBeschreibungLabelForView.toggleClass('displayNone');

    fullBeschreibungLabelForEdit.toggleClass('displayNone');
    previewBeschreibungLabelForEdit.toggleClass('displayNone');

}