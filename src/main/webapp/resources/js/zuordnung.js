function toggleBeschreibung(rowNumber) {
    var path = '#zuordnungContent\\:zuordnungForm\\:zuordnungTable\\:' + rowNumber.toString();
    var previewBeschreibung = $(path + '\\:beschreibungTextPreview');
    var fullBeschreibung = $(path + '\\:beschreibungTextFull');
    fullBeschreibung.toggleClass('displayNone');
    previewBeschreibung.toggleClass('displayNone');
}
