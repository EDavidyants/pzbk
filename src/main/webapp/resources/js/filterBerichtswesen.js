
function appendZAKStatusDerivateButton() {
    $('#zakStatusNavBar\\:berichtswesenNavform\\:filterDerivate_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();berichtswesenDerivateSearch();"></button>');
}
function hidePanel() {
    $('.filterPanel').hide();
}
function appendAnwendenButton() {
    $(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();filter();"></button>');
}