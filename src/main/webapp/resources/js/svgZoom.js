window.addEventListener('wheel', function (e) {

    if (e.deltaY < 0) {
        zoomOut();
    }
    if (e.deltaY > 0) {
        zoomIn();
    }
});
var faktor = 10;
var divFaktor = 10;
function zoomIn() {
    var maxWidth = null;
    var element = document.getElementById("svg-div");

    if (element != null) {
        maxWidth = element.style.maxWidth.replace(/[^-\d\.]/g, '');
    }

    if (maxWidth == null) {
        return;
    }

    if (maxWidth < (window.screen.width - 250) && window.innerWidth >= 1920) {
        document.getElementById("svg-div").style.maxWidth = "".concat(parseInt(maxWidth) + divFaktor).concat("px");
    } else {
        shape = document.getElementsByTagName("svg")[0];
        var param = shape.getAttribute("viewBox");
        var x = parseInt(param.split(" ")[0]);
        var y = parseInt(param.split(" ")[1]);
        var width = parseInt(param.split(" ")[2]);
        var heigth = parseInt(param.split(" ")[3]);
        shape.setAttribute("viewBox", "".concat(x + 2).concat(" ").concat(y).concat(" ").concat(width - faktor).concat(" ").concat(heigth - faktor));
    }
}

function zoomOut() {
    shape = document.getElementsByTagName("svg")[0];
    var param = shape.getAttribute("viewBox");
    var x = parseInt(param.split(" ")[0]);

    var maxWidth = null;
    var element = document.getElementById("svg-div");
    if (element != null) {
        maxWidth = element.style.maxWidth.replace(/[^-\d\.]/g, '');
    }

    if (maxWidth == null) {
        return;
    }

    if (window.innerWidth < 1920 || x != 0) {
        shape = document.getElementsByTagName("svg")[0];
        var param = shape.getAttribute("viewBox");
        var x = parseInt(param.split(" ")[0]);
        var y = parseInt(param.split(" ")[1]);
        var width = parseInt(param.split(" ")[2]);
        var heigth = parseInt(param.split(" ")[3]);
        shape.setAttribute("viewBox", "".concat(x - 2).concat(" ").concat(y).concat(" ").concat(width + faktor).concat(" ").concat(heigth + faktor));
    } else {
        document.getElementById("svg-div").style.maxWidth = "".concat(parseInt(maxWidth) - divFaktor).concat("px");
    }
}

function resetSimple() {
    shape = document.getElementsByTagName("svg")[0];
    shape.setAttribute("viewBox", "0 0 1173 437");
    document.getElementById("svg-div").style.maxWidth = "1920px";
}

function resetDetail() {
    shape = document.getElementsByTagName("svg")[0];
    shape.setAttribute("viewBox", "0 0 1284 530");
    document.getElementById("svg-div").style.maxWidth = "1920px";
}