function resizeColumn(c) {
    var column = $('#zakStatusContent\\:zakStatusForm\\:zakStatusTable\\:modulColumns\\:' + c.toString());
    if (column.hasClass('small')) {
        column.css('width', '60px');
        column.addClass("large");
        column.removeClass("small");
    } else {
        column.css('width', '30px');
        column.addClass("small");
        column.removeClass("large");
    }



    var column = $('#zakStatusContent\\:zakStatusForm\\:zakStatusTable\\:modulColumns\\:' + c.toString()).find('.commentColumn');
    console.log(column.css('display'));
    if (column.css('display') === 'none') {
        column.css('display', 'inherit');
    } else {
        column.css('display', 'none');
    }
}

function copyToClipboard() {
    var text;
    text = document.getElementById('zakStatusNavBar:changePhaseForm:urlText');
    if (window.clipboardData) { // Internet Explorer
        window.clipboardData.setData("Text", text.value);
    } else {
        unsafeWindow.netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
        const clipboardHelper = Components.classes["@mozilla.org/widget/clipboardhelper;1"].getService(Components.interfaces.nsIClipboardHelper);
        clipboardHelper.copyString(text);
    }
}

function copyLinkToClipboard() {
    var text = document.getElementById('dialog:copyLinkDialogForm:displayCommendDialogContent:inputLink');
    console.error(text);

    text.focus();
    text.select();
    var status = document.execCommand('copy');
    if (!status) {
        console.error("Cannot copy text");
    } else {
        console.log("The text is now on the clipboard");
    }
}
