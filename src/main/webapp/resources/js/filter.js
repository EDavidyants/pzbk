//Suche
function appendButton() {
    $("#topnavForm\\:menu_panel").find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();doSearch();"></button>');
    $(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();updateOnHide();"></button>');
}

function appendFilterButton() {
    $("#topnavForm\\:menu_panel").find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();doSearch();"></button>');
}

// Anforderung Zuordnen View
function appendZuordnungDerivateButton() {
    $('#zuordnungNavBar\\:changePhaseForm\\:filterDerivate_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();zuordnungDerivateSearch();"></button>');
}

function appendZuordnungSensorCocButton() {
    $('#zuordnungFilter\\:zuordnungFilterForm\\:filterCoCMany_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();zuordnungSensorCocSearch();"></button>');
}

function appendZuordnungFestgestelltInManyButton() {
    $('#zuordnungFilter\\:zuordnungFilterForm\\:filterFestgestelltInMany_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();zuordnungSensorCocSearch();"></button>');
}


function appendZuordnungFilterButton() {
    $(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();zuordnungSensorCocSearch();"></button>');
}

function appendZuordnungStatusButton() {
    for (n = 0; n < 10; n++) {
        $('#zuordnungFilter\\:zuordnungFilterForm\\:derivatStatusFilter\\:' + n + '\\:filterStatusMany_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();zuordnungStatusSearch();"></button>');
        $('#zuordnungFilter\\:zuordnungFilterForm\\:derivatStatusFilter\\:' + n + '\\:filterStatusUBMany_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();zuordnungStatusSearch();"></button>');
        $('#zuordnungFilter\\:zuordnungFilterForm\\:derivatStatusFilter\\:' + n + '\\:filterStatusZakMany_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();zuordnungStatusSearch();"></button>');
    }
}

// Anforderung vereinbaren View
function appendAddAnfoDerivateButton() {
    $('#addAnfoViewNavBar\\:changePhaseForm\\:filterDerivate_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();vereinbarungDerivateSearch();"></button>');
}

function appendAddAnfoDerivateKommentarButton() {
    $('#addAnfoViewNavBar\\:changePhaseForm\\:kommentarDerivate_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();updateKommentarDerivate();"></button>');
}

function appendAddAnfoSensorCocButton() {
    $('#addAnfoFilter\\:addAnfoFilterForm\\:filterCoCMany_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();addAnfoSensorCocSearch();"></button>');
}

function appendAddAnfoModulButton() {
    $('#addAnfoFilter\\:addAnfoFilterForm\\:filterModulMany_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();addAnfoSensorCocSearch();"></button>');
}

function appendAddAnfoAnwendenButton() {
    $(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();addAnfoSensorCocSearch();"></button>');
}

function appendAddFestgestelltInManyButton() {
    $('#addAnfoFilter\\:addAnfoFilterForm\\:filterFestgestelltInMany_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();addAnfoSensorCocSearch();"></button>');
}

function appendAddAnfoStatusButton() {
    for (n = 0; n < 10; n++) {
        $('#addAnfoFilter\\:addAnfoFilterForm\\:derivatStatusFilter\\:' + n + '\\:filterStatusMany_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();addAnfoStatusSearch();"></button>');
        $('#addAnfoFilter\\:addAnfoFilterForm\\:derivatStatusFilter\\:' + n + '\\:filterStatusUBMany_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();addAnfoStatusSearch();"></button>');
        $('#addAnfoFilter\\:addAnfoFilterForm\\:derivatStatusFilter\\:' + n + '\\:filterStatusZakMany_panel').find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();addAnfoStatusSearch();"></button>');
    }
}



//ZAK Status


//Umsetzungsbestaetigung
function appendUBAnwendenButton() {
    $(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();addUBAnwendenSearch();"></button>');
}

//Allgemein
function hidePanel() {
    $('.filterPanel').hide();
}


// kovaPhasen view

function appendSensorCocFilterButton() {
    $('#kovaPhasenFilter\\:kovaPhasenFilterForm\\:sensorCocFilter\\:filterMany_panel')
            .find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();filter();"></button>');
}

function appendStatusFilterButton() {
    $('#kovaPhasenFilter\\:kovaPhasenFilterForm\\:statusFilter\\:filterMany_panel')
            .find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();filter();"></button>');
}

function appendPhaseFilterButton() {
    $('#kovaPhasenFilter\\:kovaPhasenFilterForm\\:phaseFilter\\:filterMany_panel')
            .find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();filter();"></button>');
}

function appendDerivatFilterButton() {
    $('#kovaPhasenFilter\\:kovaPhasenFilterForm\\:derivatFilter\\:filterMany_panel')
            .find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();filter();"></button>');
}

// generall script
function appendAnwendenButton() {
    $(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();filter();"></button>');
}

function appendGlobalSearchHeaderFilterAnwendenButton() {
    $("#topnavForm\\:menu_panel").find(".ui-selectcheckboxmenu-close").replaceWith('<button class="filterPanelButton" onclick="hidePanel();filterHeader();"></button>');
    $("#topnavForm\\:menu_panel").find(".filterPanelButton").replaceWith('<button class="filterPanelButton" onclick="hidePanel();filterHeader();"></button>');
}
