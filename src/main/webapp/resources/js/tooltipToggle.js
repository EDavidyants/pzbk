function toggleTooltipPanel() {
    var panel = document.getElementById("reportingProcessOverviewForm:processOverviewContent:processOverviewDetail:tooltipPanel");
    var visibility = panel.style.getPropertyValue("visibility");

    if (visibility === "hidden") {
        panel.style.setProperty("visibility", "visible");
    } else {
        panel.style.setProperty("visibility", "hidden");
    }
}

function hideToolTipPanel() {
    var panel = document.getElementById("reportingProcessOverviewForm:processOverviewContent:processOverviewDetail:tooltipPanel");
    panel.style.setProperty("visibility", "hidden");

    var panelHeader = document.getElementById("tooltipHeader");
    panelHeader.style.setProperty("visibility", "hidden");

    var panelWrapper = document.getElementById("tooltipWrapper");
    panelWrapper.style.setProperty("z-index", "-1");
}

function showToolTipPanel() {
    var panel = document.getElementById("reportingProcessOverviewForm:processOverviewContent:processOverviewDetail:tooltipPanel");
    panel.style.setProperty("visibility", "visible");

    var panelHeader = document.getElementById("tooltipHeader");
    panelHeader.style.setProperty("visibility", "visible");

    var panelWrapper = document.getElementById("tooltipWrapper");
    panelWrapper.style.setProperty("z-index", "2");
}


function hideToolTipPanelSimple() {
    var panel = document.getElementById("reportingProcessOverviewForm:processOverviewContent:processOverviewSimple:tooltipPanel");
    panel.style.setProperty("visibility", "hidden");

    var panelHeader = document.getElementById("tooltipHeader");
    panelHeader.style.setProperty("visibility", "hidden");

    var panelWrapper = document.getElementById("tooltipWrapper");
    panelWrapper.style.setProperty("z-index", "-1");
}

function showToolTipPanelSimple() {
    var panel = document.getElementById("reportingProcessOverviewForm:processOverviewContent:processOverviewSimple:tooltipPanel");
    panel.style.setProperty("visibility", "visible");

    var panelHeader = document.getElementById("tooltipHeader");
    panelHeader.style.setProperty("visibility", "visible");

    var panelWrapper = document.getElementById("tooltipWrapper");
    panelWrapper.style.setProperty("z-index", "2");
}