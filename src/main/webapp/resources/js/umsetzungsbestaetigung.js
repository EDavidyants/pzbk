function changeStatus() {
    var ref = $(".kovAcolumn");
    ref.css({background: 'rgba(81, 137, 205, 0.3)'});
}

function removeEmptyColumns() {
    var c = $('ul.ui-columntoggler-items li:first-child').find("label").html().length;
    if (c === 0) {
        $('ul.ui-columntoggler-items li:first-child').remove();
        $('ul.ui-columntoggler-items li:first-child').remove();
    }
}

function toggleBeschreibung(rowNumber) {
    var path = '#umsContent\\:umsForm\\:umsTable\\:' + rowNumber.toString();
    var previewBeschreibung = $(path + '\\:beschreibungTextPreview');
    var fullBeschreibung = $(path + '\\:beschreibungTextFull');
    fullBeschreibung.toggleClass('displayNone');
    previewBeschreibung.toggleClass('displayNone');
}