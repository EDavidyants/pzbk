function toggleAnforderungBeschreibung(rowNumber) {
    var path = '#reportingDialogs\\:anforderunguebersichtForm\\:drilldown\\:anforderungsuebersichtTable\\:' + rowNumber.toString();
    var previewKommentar = $(path + '\\:beschreibungTextPreview');
    var fullKommentar = $(path + '\\:beschreibungTextFull');
    fullKommentar.toggleClass('displayNone');
    previewKommentar.toggleClass('displayNone');
}
