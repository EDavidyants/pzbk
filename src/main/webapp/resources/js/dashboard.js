function initDashboard() {

    registerGlobalEventListener_Click(dbUserMenuClickListener);
    registerGlobalEventListener_Click(dbQlinkMenuClickListener);
    refreshDbItmes();
    refreshDBSearchActionsMenuClickEvents();
    registerGlobalEventListener_Click(dBSearchActionsMenuClickListener);
}

function toggleUserMenu() {
    var link = $(".db-topnav-profile-menu");
    if (link.css('display') === 'none')
        openUserMenu();
    else
        closeUserMenu();
}

function openUserMenu() {
    closeNotifMenu();
    var ref = $(".db-topnav-profile-menu");
    ref.show();
}

function closeUserMenu() {
    var ref = $(".db-topnav-profile-menu");
    ref.hide();
    openNotifMenu();
}

function dbUserMenuClickListener(event) {
    if ((!$(event.target).is('.db-topnav-profile-menu') && $('.db-topnav-profile-menu').has(event.target).length <= 0
            && !$(event.target).is('.db-topnav-profile-icon') && $('.db-topnav-profile-icon').has(event.target).length <= 0)
            && $('.db-topnav-profile-menu').css('display') != 'none') {
        closeUserMenu();
    }
}

function refreshDbItmes() {
    $rows = $('.db-items-grid tr');
    if ($rows) {
        for (var i = 0; i < $rows.size(); i++) {
            $row = $($rows[i]);
            if ($row.find('.amount label.zero').size() !== 0)
                $row.addClass('zero');
            $row.click(function (e) {
                $link = $(this).find('img');
                if (e.target !== $link[0])
                    $link.click();
                return false;
            });
        }
    }
}


function toggleNotifMenu() {
    var link = $(".db-topnav-notif-menu");
    if (link.css('display') === 'none')
        openNotifMenu();
    else
        closeNotifMenu();
}

function openNotifMenu() {
    var ref = $(".db-topnav-notif-menu");
    ref.show();
}

function closeNotifMenu() {
    var ref = $(".db-topnav-notif-menu");
    ref.hide();
}

var qlinkMenu = -1;
function toggleQlinkMenu(index) {
    if (index === qlinkMenu) {
        closeQlinkMenu(index);
    }
    else {
        openQlinkMenu(index);
    }
}

function openQlinkMenu(index) {
    if (qlinkMenu >= 0)
        closeQlinkMenu(qlinkMenu);
    var $menus = $('.db-qlinks .db-qlink-menu-wrapper');
    $($menus[index]).css({
        'transition': 'max-height 0.25s ease-in, border-color 0.25s ease-in',
        'max-height': '250px',
        'border-color': 'rgba(255, 255, 255, 0.70)'
    });
    qlinkMenu = index;
}

function closeQlinkMenu(index) {
    var $menus = $('.db-qlinks .db-qlink-menu-wrapper');
    $($menus[index]).css({
        'transition': 'max-height 0.15s ease-out, border-color 0.15s ease-out',
        'max-height': '0px',
        'border-color': 'transparent'
    });
    qlinkMenu = -1;
}

function dbQlinkMenuClickListener(event) {
    if (qlinkMenu < 0)
        return;
    var $menu = $($('.db-qlinks > div')[qlinkMenu]);
    if ((!$(event.target).is($menu) && $($menu).has(event.target).length <= 0)) {
        closeQlinkMenu(qlinkMenu);
    }
}


function toggleDBSearchActionsMenu() {
    var link = $(".save-search_actions_menu");
    if (link.css('display') === 'none') {
        openSearchActionsMenu();
    } else {
        closeDBSearchActionsMenu();
    }
}

function openSearchActionsMenu() {
    var ref = $(".save-search_actions_menu");
    ref.show();
}

function closeDBSearchActionsMenu() {
    var ref = $(".save-search_actions_menu");
    ref.hide();
}

function refreshDBSearchActionsMenuClickEvents() {
    $('.save-search_actions_menu .menu_grid tr').click(function (e) {
        $link = $(this).find('img');
        if (e.target !== $link[0]){
            $link.click();
        }
    });
}

function dBSearchActionsMenuClickListener(event) {
    if ((!$(event.target).is('.save-search_actions_menu') && $('.save-search_actions_menu').has(event.target).length <= 0
            && !$(event.target).is('.db_save-search_wrapper') && $('.db_save-search_wrapper').has(event.target).length <= 0
            && !$(event.target).is('.db_save-search_button') && $('.db_save-search_button').has(event.target).length <= 0)
            && $('.save-search_actions_menu').css('display') !== 'none') {
        closeDBSearchActionsMenu();
    }
}