//--- Polyfills
if (!Array.prototype.includes) {
    Object.defineProperty(Array.prototype, "includes", {
        enumerable: false,
        value: function (obj) {
            var newArr = this.filter(function (el) {
                return el == obj;
            });
            return newArr.length > 0;
        }
    });
}
//---
function initMain() {
    if (typeof initTopNav === "function")
        initTopNav();
    if (typeof initDashboard === "function")
        initDashboard();
    if (typeof initAnforderungList === "function")
        initAnforderungList();

    if (typeof initAnforderungDerivat === "function")
        initAnforderungDerivat();
}


var eventListener_click = new Array;

$(window).click(function (event) {
    for (var i = 0; i < eventListener_click.length; i++) {
        try {
            eventListener_click[i](event);
        } catch (e) {
            console.log('Exception while executing global <click> listener: ');
            console.log(eventListener_click[i]);
            console.log(e);
        }
    }
});

function registerGlobalEventListener_Click(func) {
    if (eventListener_click.includes(func))
        return;
    eventListener_click.push(func);
}

function clog(val) {
    console.log(val);
}

function doFocus(selector) {
    $(selector).focus();
}

function pipeInput(src, dest) {
    src = $(src);
    dest = $(dest);
    dest.val($.trim(src.val()));
}

function pipeMultiInput(src, dest) {
    src = $(src);
    dest = $(dest);
    dest.val("");
    for (var i = 0; i < src.length; i++) {
        dest.val(dest.val() + " " + $(src[i]).val());
    }
    dest.val($.trim(dest.val()));
}

function toggleComment(c) {
    $('.commentPanel' + c.toString()).toggle();
    resizeColumn(c);
}
