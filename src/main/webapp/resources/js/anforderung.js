function selectAnforderungItem(index) {
    var items = $(".anfoNavigator .items .item");
    for (var i = 0; i < items.size(); i++) {
        var item = items[i];
        if (i == index) {
            $(item).addClass("selected");
        } else {
            $(item).removeClass("selected");
        }
    }
    $('.anfoFormular .ui-carousel-header .ui-carousel-page-links > a')[index].click();
}

function getAnforderungSelectionIndex() {
    var items = $(".anfoNavigator .items .item");
    for (var i = 0; i < items.size(); i++) {
        if ($(items[i]).hasClass("selected"))
            return i;
    }
}

function nextAnforderungItem() {
    selectAnforderungItem(getAnforderungSelectionIndex() + 1);
}

function prevAnforderungItem() {
    selectAnforderungItem(getAnforderungSelectionIndex() - 1);
}

function refreshDataTableClickEvents(table) {
    $(table).find('td').children('label').click(function () {
        $(this).parent().click();
    });
}

function refreshSCoCDialogTable() {
    refreshDataTableClickEvents($('.scocROTable'));
    refreshDataTableClickEvents($('.scocTechnologieTable'));
}

function showAuswirkungInput() {
    $('.auswInputPanel').animate({
        'min-width': "455px"
    }, {
        queue: false,
        duration: 500
    });
    $('.auswInputPanel').show();
    $('.anfoAuswBtn').hide();
}

function hideAuswirkungInput() {
    $('.auswInputPanel').css({'min-width': 'inherit'}).hide();
    $('.anfoAuswBtn').show();
    addAuswirkung();

}

function auswirkungPanelClickListener(event) {
//    if ((!$(event.target).is('.auswirkungenPanel') && $('.auswirkungenPanel').has(event.target).length <= 0 && $('#editAfo\\:anfoForm\\:anfoAccordion\\:auswirkungenForm\\:auswWertInput').has(event.target).length <= 0) && ($('.auswInputPanel').css('display') != 'none') && $('.auswInputPanel').css('width') != '0px') {
//        hideAuswirkungInput();
//    }
}

//function addAuswirkung() {
//    var value = $("[id$='auswWertInput']").val();
//    if (value && ($.trim(value).length > 0))
//        $("[id$='addAuswirkung']").click();
//}

registerGlobalEventListener_Click(auswirkungPanelClickListener);

function changeRequiredReferenz(isStaerke) {
    console.log(isStaerke);
    var ref = $(".anfoListPflichtfeld-3");
    if (isStaerke == 'true') {
        ref.css({color: 'rgba(81, 137, 205)'});
    } else if (isStaerke == 'false') {
        ref.css({color: 'rgb(79, 79, 79)'});
    }
}