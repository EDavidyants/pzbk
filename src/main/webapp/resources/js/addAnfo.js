function initAnforderungDerivat() {
    refreshAnfoDerivatActionsMenuClickEvents();
    registerGlobalEventListener_Click(anfoDerivatActionsMenuClickListener);
}

function resizeColumn(c) {
    var contentColumn = $('.column' + c.toString());
    if (contentColumn.hasClass('small')) {
        contentColumn.css('width', '200px');
        contentColumn.addClass("large");
        contentColumn.removeClass("small");
    } else {
        contentColumn.css('width', '80px');
        contentColumn.addClass("small");
        contentColumn.removeClass("large");
    }

    var column = $('#addAnfoContent\\:addAnfoForm\\:addAnfoTable').find('.commentColumn' + c.toString());
    if (column.css('display') === 'none') {
        column.css('display', 'inherit');
    } else {
        column.css('display', 'none');
    }

    var statusColumn = $('#addAnfoContent\\:addAnfoForm\\:addAnfoTable').find('.statusColumn' + c.toString());
    if (statusColumn.hasClass('ui-g-6')) {
        statusColumn.addClass("ui-g-12");
        statusColumn.removeClass("ui-g-6");
    } else {
        statusColumn.addClass("ui-g-6");
        statusColumn.removeClass("ui-g-12");
    }
}

function disableButton(rowNumber, columnNumber, okButton) {
    var path = '#addAnfoContent\\:addAnfoForm\\:addAnfoTable\\:' + rowNumber.toString() + '\\:derivatColumns\\:' + columnNumber.toString();
    var keineWeiterverfolgungButton = $(path + '\\:keineWeiterverfolgungButton');
    var umsetzungBestaetigungButton = $(path + '\\:umsetzungBestaetigenButton');
    if (okButton === 1) {
        umsetzungBestaetigungButton.addClass("selectedButton");
        keineWeiterverfolgungButton.removeClass("selectedButton");
    } else {
        umsetzungBestaetigungButton.removeClass("selectedButton");
        keineWeiterverfolgungButton.addClass("selectedButton");
    }
}

function toggleBeschreibung(rowNumber) {
    var path = '#addAnfoContent\\:addAnfoForm\\:addAnfoTable\\:' + rowNumber.toString();
    var fullBeschreibungLabel = $(path + '\\:beschreibungTextFull');
    var previewBeschreibungLabel = $(path + '\\:beschreibungTextPreview');
    fullBeschreibungLabel.toggleClass('displayNone');
    previewBeschreibungLabel.toggleClass('displayNone');
}

function toggleKommentar(rowNumber, columnNumber) {
    var path = '#addAnfoContent\\:addAnfoForm\\:addAnfoTable\\:' + rowNumber.toString() + '\\:derivatColumns\\:' + columnNumber.toString();
    var fullBeschreibungLabel = $(path + '\\:umsKommentar');
    var previewBeschreibungLabel = $(path + '\\:umsKommentarShort');
    fullBeschreibungLabel.toggleClass('displayNone');
    previewBeschreibungLabel.toggleClass('displayNone');
}

function resizeTable() {
    var form = $('.content');
    var height = form.height();
    console.log(height);
    var tableBody = $('.ui-datatable-scrollable-body');
    tableBody.height(height - 70);
}

function toggleAnfoDerivatActionsMenu() {
    var link = $(".anfoDerivat_actions_menu");
    if (link.css('display') === 'none') {
        openAnfoDerivatActionsMenu();
    } else {
        closeAnfoDerivatActionsMenu();
    }
}

function openAnfoDerivatActionsMenu() {
    var ref = $(".anfoDerivat_actions_menu");
    ref.show();
}

function closeAnfoDerivatActionsMenu() {
    var ref = $(".anfoDerivat_actions_menu");
    ref.hide();
}

function refreshAnfoDerivatActionsMenuClickEvents() {
    $('.anfoDerivat_actions_menu .menu_grid tr').click(function (e) {
        $link = $(this).find('img');
        if (e.target !== $link[0])
            $link.click();
        return false;
    });
}

function anfoDerivatActionsMenuClickListener(event) {
    if ((!$(event.target).is('.anfoDerivat_actions_menu') && $('.anfoDerivat_actions_menu').has(event.target).length <= 0
            && !$(event.target).is('.clickableTextButton') && $('.clickableTextButton').has(event.target).length <= 0)
            && $('.anfoDerivat_actions_menu').css('display') !== 'none') {
        closeAnfoDerivatActionsMenu();
    }
}