function initTopNav() {
    registerGlobalEventListener_Click(searchActionsMenuClickListener);
    refreshUserMenuClickEvents();
    registerGlobalEventListener_Click(userMenuClickListener);
    refreshNotifMenuClickEvents();
    registerGlobalEventListener_Click(notifMenuClickListener);
    refreshActionsMenuClickEvents();
    registerGlobalEventListener_Click(actionsMenuClickListener);
}

function toggleSearchActionsMenu() {
    var link = $(".save-search_actions_menu");
    if (link.css('display') === 'none') {
        openSearchActionsMenu();
    } else {
        closeSearchActionsMenu();
    }
}

function openSearchActionsMenu() {
    var ref = document.getElementById("save-save-search_actions_menu");
    ref.style.display = "block";
}

function closeSearchActionsMenu() {
    var ref = document.getElementById("save-save-search_actions_menu");
    ref.style.display = "none";
}

function searchActionsMenuClickListener(event) {
    if ((!$(event.target).is('.save-search_actions_menu') && $('.save-search_actions_menu').has(event.target).length <= 0
            && !$(event.target).is('.topnav_save-search_wrapper') && $('.topnav_save-search_wrapper').has(event.target).length <= 0)
            && $('.save-search_actions_menu').css('display') !== 'none') {
        closeSearchActionsMenu();
    }
}

function toggleUserMenu() {
    var link = $(".topnav_profile_menu");
    if (link.css('display') === 'none')
        openUserMenu();
    else
        closeUserMenu();
}

function openUserMenu() {
    var ref = $(".topnav_profile_menu");
    ref.show();
}

function closeUserMenu() {
    var ref = $(".topnav_profile_menu");
    ref.hide();
}

function refreshUserMenuClickEvents() {
    $('.topnav_profile_menu .menu_grid tr').click(function (e) {
        $link = $(this).find('.ui-commandlink');
        if (e.target !== $link[0])
            $link.click();
        return false;
    });
}

function userMenuClickListener(event) {
    if ((!$(event.target).is('.topnav_profile_menu') && $('.topnav_profile_menu').has(event.target).length <= 0
            && !$(event.target).is('.topnav_profile_icon') && $('.topnav_profile_icon').has(event.target).length <= 0)
            && $('.topnav_profile_menu').css('display') !== 'none') {
        closeUserMenu();
    }
}

function toggleNotifMenu() {
    var link = $(".topnav_notif_menu");
    if (link.css('display') === 'none')
        openNotifMenu();
    else
        closeNotifMenu();
}

function openNotifMenu() {
    refreshNotifItmes();
    var ref = $(".topnav_notif_menu");
    ref.show();
}

function closeNotifMenu() {
    var ref = $(".topnav_notif_menu");
    ref.hide();
}

function refreshNotifItmes() {
    $rows = $('.topnav_notif_menu .menu_grid tr');
    if ($rows) {
        for (var i = 0; i < $rows.size(); i++) {
            $row = $($rows[i]);
            if ($row.find('.amount label.zero').size() !== 0)
                $row.addClass('zero');
            $row.click(function (e) {
                $link = $(this).find('img');
                if (e.target !== $link[0])
                    $link.click();
                return false;
            });
        }
    }
}

function refreshNotifMenuClickEvents() {
    $('.topnav_notif_menu .menu_grid tr').click(function (e) {
        $link = $(this).find('img');
        if (e.target !== $link[0])
            $link.click();
        return false;
    });
}

function notifMenuClickListener(event) {
    if ((!$(event.target).is('.topnav_notif_menu') && $('.topnav_notif_menu').has(event.target).length <= 0
            && !$(event.target).is('.topnav_notif_icon') && $('.topnav_notif_icon').has(event.target).length <= 0)
            && $('.topnav_notif_menu').css('display') !== 'none') {
        closeNotifMenu();
    }
}


function toggleActionsMenu() {
    var link = $(".topnav_actions_menu");
    if (link.css('display') === 'none')
        openActionsMenu();
    else
        closeActionsMenu();
}

function openActionsMenu() {
    var ref = $(".topnav_actions_menu");
    ref.show();
}

function closeActionsMenu() {
    var ref = $(".topnav_actions_menu");
    ref.hide();
}

function refreshActionsMenuClickEvents() {
    $('.topnav_actions_menu .menu_grid tr').click(function (e) {
        $link = $(this).find('img');
        if (e.target !== $link[0])
            $link.click();
        return false;
    });
}

function actionsMenuClickListener(event) {
    if ((!$(event.target).is('.topnav_actions_menu') && $('.topnav_actions_menu').has(event.target).length <= 0
            && !$(event.target).is('.topnav_actions_icon') && $('.topnav_actions_icon').has(event.target).length <= 0)
            && $('.topnav_actions_menu').css('display') !== 'none') {
        closeActionsMenu();
    }
}

//-------------------------
function redirectBack() {
    window.history.back();
}
