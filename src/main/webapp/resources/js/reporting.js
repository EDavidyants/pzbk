function toggleAnforderungstext(rowNumber) {
    var path = '#dialog\\:anforderungsuebersichtForm\\:anforderungsuebersichtDlgContent\\:anforderungsuebersichtTable\\:' + rowNumber.toString();
    var previewAnforderungstext = $(path + '\\:beschreibungTextPreview');
    var fullAnforderungstext = $(path + '\\:beschreibungTextFull');
    fullAnforderungstext.toggleClass('displayNone');
    previewAnforderungstext.toggleClass('displayNone');
}

function toggleAnforderungKommentar(rowNumber) {
    var path = '#dialog\\:anforderungsuebersichtForm\\:anforderungsuebersichtDlgContent\\:anforderungsuebersichtTable\\:' + rowNumber.toString();
    var previewKommentar = $(path + '\\:kommentarTextPreview');
    var fullKommentar = $(path + '\\:kommentarTextFull');
    fullKommentar.toggleClass('displayNone');
    previewKommentar.toggleClass('displayNone');
}
