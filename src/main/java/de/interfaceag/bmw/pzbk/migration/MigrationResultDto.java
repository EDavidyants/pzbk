package de.interfaceag.bmw.pzbk.migration;

import java.io.Serializable;

/**
 *
 * @author sl
 */
public class MigrationResultDto implements Serializable {

    private final String id;
    private final boolean successful;
    private final String message;

    public MigrationResultDto(String id, boolean successful, String message) {
        this.id = id;
        this.successful = successful;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getMessage() {
        return message;
    }

}
