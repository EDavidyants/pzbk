package de.interfaceag.bmw.pzbk.migration;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Auswirkung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.Zielwert;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.excel.ExcelUtils;
import de.interfaceag.bmw.pzbk.exceptions.MigrationException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

/**
 * @author qp
 */
public final class MigrationUtils {

    private static final Logger LOG = LoggerFactory.getLogger(MigrationUtils.class);

    private static final String LOG_STR_EIGEN = "%d %s wurden migriert.";
    private static final String LOG_STR_GESAMT = "%d %s wurden insgesamt erfolgreich migriert.";
    private static final String LOG_STR_GESAMT_FEHLER = "%d %s wurden insgesamt nicht erfolgreich migriert.";

    private MigrationUtils() {
    }

    // ---------- public methods -----------------------------------------------

    /**
     * Check if row row contains a zugeordnete Meldung.
     *
     * @param row
     * @return true if row conatins zugeordnete Meldung
     */
    public static boolean isZugeordnetMeldung(Row row) {
        return !isLeerCell(row.getCell(2)) && ExcelUtils.cellToString(row.getCell(1)).contains("Meldung");
    }

    /**
     * Check if row row contains a nicht zugeordnete Meldung.
     *
     * @param row
     * @return true if row conatins nicht zugeordnete Meldung
     */
    public static boolean isNichtZugeordnetMeldung(Row row) {
        return isLeerCell(row.getCell(2)) && ExcelUtils.cellToString(row.getCell(1)).contains("Meldung");
    }

    /**
     * Returns the raw String for the SensorCoc (Technologie) of the
     * Anforderung/ Meldung.
     *
     * @param row
     * @return raw String for SensorCoc
     */
    public static String convertToSensorCocString(Row row) {
        String result = ExcelUtils.cellToString(row.getCell(26));
        if ("T-E/E Fahrwerk_Schweinwerfereinstellung_und_FAS".equals(result)) {
            return "T-E/E Fahrwerk_Scheinwerfereinstellung_und FAS";
        }
        if ("T-E/E Interaktion u Energie".equals(result)) {
            return "T-E/E Interaktion u. Energie";
        }
        return result;
    }

    public static Date convertToDatumStatuswechselString(Row row) {
        try {
            Cell cell = row.getCell(11);
            if (null != convertToDate(cell)) {
                return convertToDate(cell);
            }
        } catch (ParseException ex) {
            LOG.error(null, ex);
        }
        return null;
    }

    /**
     * Returns the raw String for the Mitarbeiter (Sensor) of the Anforderung/
     * Meldung.
     *
     * @param row
     * @return raw string for Sensor
     */
    public static String convertToSensorString(Row row) {
        return ExcelUtils.cellToString(row.getCell(25));
    }

    /**
     * Check if row row contains a Anforderung.
     *
     * @param row
     * @return true if row conatins zugeordnete Meldung
     */
    public static boolean isAnforderung(Row row) {
        return ExcelUtils.cellToString(row.getCell(1)).contains("Anforderung");
    }

    /**
     * Convert row to the original id (fachId).
     *
     * @param row
     * @return
     */
    public static Long convertToOriginalId(Row row) {
        return Long.parseLong(ExcelUtils.cellToString(row.getCell(0)));
    }

    /**
     * Convert row to the parent id (original id of the parent Anforderung).
     *
     * @param row
     * @return
     */
    public static String convertToParentId(Row row) {
        return "A" + ExcelUtils.cellToString(row.getCell(2));
    }

    public static Anforderung convertToAnforderung(Row row) throws MigrationException {
        Anforderung a = new Anforderung();
        try {
            String geloescht = ExcelUtils.cellToString(row.getCell(3));
            if ("JA".equals(geloescht.toUpperCase()) || "DEL".equals(geloescht.toUpperCase())) {
                return null;
            }

            String potentialFachId = ExcelUtils.cellToString(row.getCell(0));
            String firstChar = potentialFachId.substring(0, 1);
            String fachId;
            if ("A".equals(firstChar) || "M".equals(firstChar)) {
                fachId = ExcelUtils.cellToString(row.getCell(0));
            } else {
                fachId = "A" + ExcelUtils.cellToString(row.getCell(0));
            }
            a.setFachId(fachId);

            a.setStatus(convertToAnforderungStatus(row.getCell(28)));
            a.setKommentarAnforderung(convertToKommentarString(row.getCell(16), row.getCell(8), row.getCell(19)));
            a.setBeschreibungAnforderungDe(ExcelUtils.cellToString(row.getCell(9)));
            if (null != convertToDate(row.getCell(4))) {
                a.setAenderungsdatum(convertToDate(row.getCell(4)));
            }
            a.setAuswirkungen(convertToAuswirkung(row.getCell(5), row.getCell(37)));
            a.setBeschreibungStaerkeSchwaeche(ExcelUtils.cellToString(row.getCell(10)));
            if (null != convertToDate(row.getCell(4))) {
                a.setErstellungsdatum(convertToDate(row.getCell(4)));
            }
            a.setZielwert(convertToZielwert(row.getCell(38), row.getCell(12), row.getCell(17)));
            a.setLoesungsvorschlag(ExcelUtils.cellToString(row.getCell(20)));
            a.setPhasenbezug(convertToPhasenbezug(row.getCell(21)));
            a.setReferenzen(ExcelUtils.cellToString(row.getCell(24)));
            a.setStaerkeSchwaeche(convertToStaerkeSchwaeche(row.getCell(27)));
            a.setUmsetzenderBereich(ExcelUtils.cellToString(row.getCell(31)));
            a.setUrsache(ExcelUtils.cellToString(row.getCell(36)));
            a.setStatusWechselKommentar("");
            a.setMeldungen(new HashSet<>());
            a.setVersion((Integer) 1);
            a.setBeschreibungAnforderungEn("");
            a.setZeitpktUmsetzungsbest(convertToZeitpktUmsetzungsbest(row.getCell(15)));
        } catch (MigrationException | ParseException exception) {
            throw new MigrationException(exception);
        }
        return a;
    }

    public static String convertToAnforderungspfad(Row row) {
        return ExcelUtils.cellToString(row.getCell(6));
    }

    public static Meldung convertToMeldung(Row row) throws MigrationException {
        Meldung m = new Meldung();
        try {
            String geloescht = ExcelUtils.cellToString(row.getCell(3));
            if (geloescht.toUpperCase().equals("JA")) {
                return null;
            }

            String potentialFachId = ExcelUtils.cellToString(row.getCell(0));
            String firstChar = potentialFachId.substring(0, 1);
            String fachId;
            if ("A".equals(firstChar) || "M".equals(firstChar)) {
                fachId = ExcelUtils.cellToString(row.getCell(0));
            } else {
                fachId = "M" + ExcelUtils.cellToString(row.getCell(0));
            }
            m.setFachId(fachId);

            m.setStatus(convertToMeldungStatus(row.getCell(28)));
            m.setKommentarAnforderung(convertToKommentarString(row.getCell(16), row.getCell(8), row.getCell(19)));
            m.setBeschreibungAnforderungDe(ExcelUtils.cellToString(row.getCell(9)));
            if (null != convertToDate(row.getCell(4))) {
                m.setAenderungsdatum(convertToDate(row.getCell(4)));
            }
            m.setAuswirkungen(convertToAuswirkung(row.getCell(5), row.getCell(37)));
            m.setBeschreibungStaerkeSchwaeche(ExcelUtils.cellToString(row.getCell(10)));
            if (null != convertToDate(row.getCell(4))) {
                m.setErstellungsdatum(convertToDate(row.getCell(4)));
            }
            m.setZielwert(convertToZielwert(row.getCell(38), row.getCell(12), row.getCell(17)));
            m.setLoesungsvorschlag(ExcelUtils.cellToString(row.getCell(20)));
            m.setReferenzen(ExcelUtils.cellToString(row.getCell(24)));
            m.setStaerkeSchwaeche(convertToStaerkeSchwaeche(row.getCell(27)));
            m.setUmsetzenderBereich(ExcelUtils.cellToString(row.getCell(31)));
            m.setUrsache(ExcelUtils.cellToString(row.getCell(36)));
            m.setStatusWechselKommentar("");
            m.setBeschreibungAnforderungEn("");
        } catch (ParseException | NumberFormatException exception) {
            throw new MigrationException(exception);
        }
        return m;
    }

    /**
     * For the import of the Anforderung sheet! Parse Umsetzer string field
     * which cotins the list of Umsetzer in the form modul / modulSeTeam /
     * modulKomponente.
     *
     * @param row
     * @return
     */
    public static String convertToUmsetzerString(Row row) {
        return ExcelUtils.cellToString(row.getCell(34));
    }

    /**
     * For the import of the Umsetzer sheet! Parse Modul Komponente from row.
     *
     * @param row
     * @return
     */
    public static ModulKomponente convertToModulKomponente(Row row) {
        ModulKomponente m = new ModulKomponente();
        m.setPpg(ExcelUtils.cellToString(row.getCell(5)));
        m.setName(ExcelUtils.cellToString(row.getCell(4)));
        return m;
    }

    public static Modul convertToModul(Row row) {
        String fachbereich = ExcelUtils.cellToString(row.getCell(0));
        String name = ExcelUtils.cellToString(row.getCell(1));
        String beschreibung = ExcelUtils.cellToString(row.getCell(2));
        return new Modul(name, fachbereich, beschreibung);
    }

    public static String convertToModulKomponentePpg(Row row) {
        return ExcelUtils.cellToString(row.getCell(5));
    }

    /**
     * For the import of the Umsetzer sheet! Parse ModulSeTeam Name.
     *
     * @param row
     * @return
     */
    public static String convertToModulSeTeamName(Row row) {
        return ExcelUtils.cellToString(row.getCell(3));
    }

    /**
     * For the import of the Umsetzer sheet! Parse Modul name.
     *
     * @param row
     * @return
     */
    public static String convertToModulNameString(Row row) {
        return ExcelUtils.cellToString(row.getCell(1));
    }

    // ---------- private methods ----------------------------------------------

    private static Status convertToAnforderungStatus(Cell cell) throws MigrationException {
        String status = ExcelUtils.cellToString(cell);
        switch (status) {
            case "in Arbeit":
            case "zu überarbeiten":
                return Status.A_INARBEIT;
            case "keine Weiterverfolgung":
                return Status.A_KEINE_WEITERVERFOLG;
            case "freigegeben (in Überarbeitung)":
            case "freigegeben":
            case "freigegeben (erneut freizugeben)":
                return Status.A_FREIGEGEBEN;
            case "plausibilisiert":
                return Status.A_PLAUSIB;
            case "unstimmig":
                return Status.A_UNSTIMMIG;
            default:
                throw new MigrationException("Status '" + status + "'" + " wurde nicht gefunden.");
        }
    }

    private static Status convertToMeldungStatus(Cell cell) throws MigrationException {
        String status = ExcelUtils.cellToString(cell);
        switch (status) {
            case "angelegt":
                return Status.M_ENTWURF;
            case "Entwurf":
            case "gemeldet":
                return Status.M_GEMELDET;
            case "zugeordnet":
                return Status.M_ZUGEORDNET;
            case "unstimmig":
                return Status.M_UNSTIMMIG;
            default:
                throw new MigrationException("Status '" + status + "'" + " wurde nicht gefunden.");
        }
    }

    public static List<String> convertToFestgestelltIn(Row row) {
        Cell cell = row.getCell(14);
        String s = ExcelUtils.cellToString(cell);
        String[] slist = s.split(",");
        return Arrays.asList(slist);
    }

    public static Date convertToDate(Cell cell) throws ParseException {
        Date result = null;
        if (cell != null && cell.getStringCellValue().matches("\\d\\d.\\d\\d.\\d\\d\\d\\d")) {
            DateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
            result = df.parse(cell.getStringCellValue());
        }
        return result;
    }

    private static boolean isLeerCell(Cell cell) {
        return cell == null || ExcelUtils.cellToString(cell).isEmpty();
    }

    private static String convertToKommentarString(Cell kommentar, Cell bemerkung, Cell zuordnungBaukasten) {

        String kommentarString = ExcelUtils.cellToString(kommentar);
        String bemerkungString = ExcelUtils.cellToString(bemerkung);
        String zuordnungBaukastenString = ExcelUtils.cellToString(zuordnungBaukasten);

        StringBuilder sb = new StringBuilder();
        if (!kommentarString.isEmpty()) {
            sb.append(kommentarString).append("\n");
        }
        if (!bemerkungString.isEmpty()) {
            sb.append("Bemerkung: ").append(bemerkungString).append("\n");
        }
        if (!zuordnungBaukastenString.isEmpty()) {
            sb.append("Zuordnung Baukasten: ").append(zuordnungBaukastenString).append("\n");
        }
        return sb.toString();
    }

    private static boolean convertToStaerkeSchwaeche(Cell cell) throws MigrationException {
        String s = ExcelUtils.cellToString(cell);
        switch (s) {
            case "Schwäche":
                return false;
            case "Stärke":
                return true;
            default:
                throw new MigrationException(s + " konnte nicht konveriert werden.");
        }
    }

    private static boolean convertToPhasenbezug(Cell cell) {
        return !"architekturrelevant".equals(ExcelUtils.cellToString(cell));
    }

    private static boolean convertToZeitpktUmsetzungsbest(Cell cell) {
        return !ExcelUtils.cellToString(cell).contains("VKBG");

    }

    private static List<Auswirkung> convertToAuswirkung(Cell auswirkung, Cell wert) throws MigrationException {
        Auswirkung a = new Auswirkung();
        a.setAuswirkung(ExcelUtils.cellToString(auswirkung));
        a.setWert(ExcelUtils.cellToString(wert));
        List<Auswirkung> list = new ArrayList<>();
        list.add(a);
        return list;
    }

    private static Zielwert convertToZielwert(Cell wert, Cell einheit, Cell kommentar) {
        Zielwert z = new Zielwert();
        z.setWert(ExcelUtils.cellToString(wert) + ExcelUtils.cellToString(einheit));
        z.setKommentar(ExcelUtils.cellToString(kommentar));
        return z;
    }

    // ---------- logger -------------------------------------------------------
    public static void logEigen(int counter, String type) {
        if (counter % 300 == 0) {
            LOG.info(String.format(LOG_STR_EIGEN, counter, type));
        }
    }

    public static void logGesamt(int counter, String type) {
        LOG.info(String.format(LOG_STR_GESAMT, counter, type));
    }

    public static void logFehlerGesamt(int counter, String type) {
        LOG.info(String.format(LOG_STR_GESAMT_FEHLER, counter, type));
    }

    public static void copyRowData(Row targetRow, Row row) {
        row.cellIterator().forEachRemaining(c -> {
            targetRow.createCell(c.getColumnIndex(), c.getCellType());
            Cell targetCell = targetRow.getCell(c.getColumnIndex());
            setCellDataValue(c, targetCell);
        });
    }

    private static void setCellDataValue(Cell oldCell, Cell newCell) {
        switch (oldCell.getCellType()) {
            case Cell.CELL_TYPE_BLANK:
                newCell.setCellValue(oldCell.getStringCellValue());
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                newCell.setCellValue(oldCell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_ERROR:
                newCell.setCellErrorValue(oldCell.getErrorCellValue());
                break;
            case Cell.CELL_TYPE_FORMULA:
                newCell.setCellFormula(oldCell.getCellFormula());
                break;
            case Cell.CELL_TYPE_NUMERIC:
                newCell.setCellValue(oldCell.getNumericCellValue());
                break;
            case Cell.CELL_TYPE_STRING:
                newCell.setCellValue(oldCell.getStringCellValue());
                break;
            default:
                newCell.setCellValue("");
                break;
        }
    }

    public static String normalizeMitarbeiterString(String mitarbeiterString) {
        if (mitarbeiterString.contains("\r\n")) {
            mitarbeiterString = mitarbeiterString.replaceAll("\r\n", " ");
        }
        mitarbeiterString = mitarbeiterString.trim();
        return mitarbeiterString;
    }
}
