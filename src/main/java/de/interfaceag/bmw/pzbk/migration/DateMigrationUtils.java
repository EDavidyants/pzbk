package de.interfaceag.bmw.pzbk.migration;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class DateMigrationUtils {

    private DateMigrationUtils() {
    }

    public static boolean checkForValidInputData(Anforderung anforderung) {
        return anforderung != null;
    }

    public static boolean checkForValidInputData(Date date) {
        return date != null;
    }

    public static boolean checkForValidInputData(Meldung meldung) {
        return meldung != null;
    }

    public static Date getReferenceTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2018, 0, 30);
        return calendar.getTime();
    }

    public static boolean checkForValidDateUpdate(Date currentDate, Date newDate) {
        if (currentDate == null) {
            return true;
        }
        if (newDate == null) {
            return false;
        }
        if (currentDate.getTime() == newDate.getTime()) {
            return false;
        }
        return getReferenceTime().after(currentDate);
    }

}
