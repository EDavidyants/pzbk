package de.interfaceag.bmw.pzbk.migration;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.AbstractAnforderung;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Bild;
import de.interfaceag.bmw.pzbk.entities.DbFile;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.entities.Werk;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.LogLevel;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.SystemType;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.excel.ExcelUtils;
import de.interfaceag.bmw.pzbk.exceptions.MigrationException;
import de.interfaceag.bmw.pzbk.services.AnforderungFreigabeService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.ImageService;
import de.interfaceag.bmw.pzbk.services.LogService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.services.UserService;
import de.interfaceag.bmw.pzbk.services.WerkService;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import de.interfaceag.bmw.pzbk.vereinbarung.VereinbarungHistoryService;
import de.interfaceag.bmw.pzbk.zak.ZakVereinbarungService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @author sl
 */

@Stateless
@Named

public class MigrationService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(MigrationService.class);

    @Inject
    private AnforderungService anforderungService;
    @Inject
    private AnforderungMeldungHistoryService historyService;
    @Inject
    private VereinbarungHistoryService vereinbarungHistoryService;
    @Inject
    private AnforderungFreigabeService anforderungFreigabeService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    private ImageService imageService;
    @Inject
    ModulService modulService;
    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private TteamService tteamService;
    @Inject
    private UserSearchService uss;
    @Inject
    private UserService us;
    @Inject
    private ZakVereinbarungService zakVereinbarungService;
    @Inject
    private ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;
    @Inject
    private WerkService werkService;
    @Inject
    private LogService logService;

    private final HashMap<String, String> linkMeldungToAnforderungHashMap = new HashMap<>();

    private Map<SensorCoc, Tteam> sensorCocTteamMap = new HashMap<>();

    private final Set<String> irregularSensorCocNames = new HashSet<>();
    private final Set<String> missingTteamSensorCocNames = new HashSet<>();
    private final Set<String> irregularSensorNames = new HashSet<>();
    private final Set<String> irregularUmsetzer = new HashSet<>();
    private final List<String> excelLog = new ArrayList<>();

    private Set<FestgestelltIn> allFestgestelltIn = new HashSet<>();

    private Anforderung anforderung;
    private Meldung meldung;
    private String fachid;

    /**
     * Parse Umsetzer Excel Sheet and persistAnforderungHistory new created
     * Module, ModulSeTeams and ModulKomponenten
     *
     * @param sheet
     */
    public void persistUmsetzerlisteFromExcelSheet(Sheet sheet) {
        List<Modul> module = parseModulListFromExcelSheet(sheet);
        modulService.persistModule(module);
    }

    public List<Modul> parseModulListFromExcelSheet(Sheet sheet) {
        List<Modul> result = new ArrayList<>();
        List<ModulSeTeam> seTeams = new ArrayList<>();
        for (int rowNumber = sheet.getFirstRowNum() + 1; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            Row row = sheet.getRow(rowNumber);

            // create new Komponente
            String modulKomponentePpg = MigrationUtils.convertToModulKomponentePpg(row);
            String seTeamName = MigrationUtils.convertToModulSeTeamName(row);
            String modulName = MigrationUtils.convertToModulNameString(row);

            ModulKomponente modulKomponente = modulService.getModulKomponenteByPpg(modulKomponentePpg);
            ModulSeTeam modulSeTeam = modulService.getModulSeTeamByName(seTeamName);
            Modul modul = modulService.getModulByName(modulName);

            if (modul == null) {
                Optional<Modul> optionalModul = result.stream().filter(m -> m.getName().equals(modulName)).findAny();
                if (optionalModul.isPresent()) {
                    modul = optionalModul.get();
                } else {
                    modul = MigrationUtils.convertToModul(row);
                    result.add(modul);
                }
            } else {
                result.add(modul);
            }

            if (modulSeTeam == null && !"".equals(seTeamName)) {
                Optional<ModulSeTeam> optionalModulSeTeam = seTeams.stream().filter(s -> s.getName().equals(seTeamName)).findAny();
                if (optionalModulSeTeam.isPresent() && optionalModulSeTeam.get().getElternModul().getName().equals(modul.getName())) {
                    modulSeTeam = optionalModulSeTeam.get();
                } else {
                    modulSeTeam = new ModulSeTeam(seTeamName, modul, new HashSet<>());
                    modulService.persistModulSeTeam(modulSeTeam);
                    modul.addSeTeam(modulSeTeam);
                    if (modulSeTeam != null) {
                        seTeams.add(modulSeTeam);
                    }
                }
            }

            if (!"".equals(modulKomponentePpg) && modulSeTeam != null) {
                if (modulKomponente == null) {
                    modulKomponente = MigrationUtils.convertToModulKomponente(row);
                }
                modulKomponente.setSeTeam(modulSeTeam);
                modulSeTeam.addKomponente(modulKomponente);
            }
        }

        return result.stream().filter(modul -> modul.getSeTeams() != null).filter(m -> !m.getSeTeams().isEmpty()).collect(Collectors.toList());
    }

    private Map<SensorCoc, Tteam> generateSensorCocTteamMap(Sheet sheet) {
        Map<SensorCoc, Tteam> result = new HashMap<>();
        Row row;
        String currentTteamString;
        Tteam currentTteam = null;
        SensorCoc currentSensorCoc;
        for (int rowNumber = sheet.getFirstRowNum() + 1; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            row = sheet.getRow(rowNumber);
            currentTteamString = ExcelUtils.cellToString(row.getCell(0));
            if (!currentTteamString.isEmpty()) {
                currentTteam = tteamService.getTteamByName(currentTteamString);
            }
            if (currentTteam != null) {
                currentSensorCoc = sensorCocService.getSensorCocByTechnologie(ExcelUtils.cellToString(row.getCell(1)));
                if (currentSensorCoc != null) {
                    result.put(currentSensorCoc, currentTteam);
                }
            }
        }
        return result;
    }

    private List<Derivat> migrateDerivate(Row row) {
        List<Derivat> result = new ArrayList<>();
        for (Integer i = 40; i < row.getLastCellNum(); i++) {
            String derivatRawString = ExcelUtils.cellToString(row.getCell(i));
            if (derivatRawString.contains(" ")) {
                String derivatNameBezeichnung = derivatRawString.substring(derivatRawString.lastIndexOf("PL Vorschlag für") + 17);
                Derivat derivat = derivatService.getDerivatByName(derivatNameBezeichnung);
                if (derivat == null) {
                    derivat = new Derivat(derivatNameBezeichnung, "");
                    derivat.setKovAPhasen(generateKovAPhasenForDerivat(derivat));
                    derivatService.persistDerivat(derivat);
                }
                result.add(derivat);
            } else {
                LOG.error(String.format("Derivat '%s' konnte nicht migriert werden.", ExcelUtils.cellToString(row.getCell(i))));
            }
        }
        return result;
    }

    private List<KovAPhaseImDerivat> generateKovAPhasenForDerivat(Derivat derivat) {
        List<KovAPhaseImDerivat> kovAPhasenImDerivat = new ArrayList<>();
        KovAPhase.getAllKovAPhasen().forEach((phase) -> {
            kovAPhasenImDerivat.add(new KovAPhaseImDerivat(derivat, phase));
        });
        return kovAPhasenImDerivat;
    }

    public void linkMeldungenToAnforderung(Sheet sheet) {
        LOG.debug("Create Link Map");
        for (int rowNumber = sheet.getFirstRowNum() + 1; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            Row row = sheet.getRow(rowNumber);
            if (MigrationUtils.isZugeordnetMeldung(row)) {
                linkMeldungToAnforderungHashMap.put("M" + ExcelUtils.cellToString(row.getCell(0)), MigrationUtils.convertToParentId(row));
            } else {
                LOG.error("Row {} konnte nicht importiert werden!", Integer.toString(rowNumber));
            }
        }
        LOG.info("Link Meldungen to Anforderungen");
        try {
            linkMeldungenToAnforderung();
        } catch (MigrationException ex) {
            LOG.error(null, ex);
        }
    }

    public void generateAnforderungDerivatZuordnung(Sheet sheet) {
        LOG.info("Erzeuge ZuordnungAnforderungDerivat.");
        List<ZuordnungAnforderungDerivat> zuordnungAnforderungDerivat = parseZuordnungAnforderungDerivat(sheet);
        LOG.debug("Persistiere ZuordnungAnforderungDerivat.");
        zuordnungAnforderungDerivatService.persistZuordnungAnforderungDerivat(zuordnungAnforderungDerivat);
        LOG.debug("Persistieren abgeschlossen.");

        LOG.debug("Erzeuge DerivatAnforderungModulen.");
        List<DerivatAnforderungModul> derivatAnforderungModulen = parseAnforderungDerivatZuordnung(sheet);
        LOG.debug("Persistiere DerivatAnforderungModulen.");
        derivatAnforderungModulService.persistDerivatAnforderungModulen(derivatAnforderungModulen);
        LOG.debug("Persistieren abgeschlossen.");
    }

    public List<ZuordnungAnforderungDerivat> parseZuordnungAnforderungDerivat(Sheet sheet) {
        LOG.debug("Starte Migration der ZuordnungAnforderungDerivat");
        List<ZuordnungAnforderungDerivat> result = new ArrayList<>();
        List<Derivat> relevantDerivate = migrateDerivate(sheet.getRow(0));
        Integer offset = 40; // based on excel sheet columns
        Row row;
        String cellValue;
        Derivat derivat;
        Anforderung anforderungFromCell;
        ZuordnungAnforderungDerivat zuordnungAnforderungDerivat;
        for (int rowNumber = sheet.getFirstRowNum() + 1; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            row = sheet.getRow(rowNumber);
            anforderungFromCell = anforderungService.getUniqueAnforderungByFachId("A" + ExcelUtils.cellToString(row.getCell(0)));
            if (anforderungFromCell != null) {
                for (Integer cellNumber = offset; cellNumber < row.getLastCellNum(); cellNumber++) {
                    cellValue = ExcelUtils.cellToString(row.getCell(cellNumber));
                    if (!cellValue.isEmpty()) {
                        zuordnungAnforderungDerivat = getZuordnungAnforderungDerivatBasedOnExcelStatus(relevantDerivate, offset, cellValue, anforderungFromCell, rowNumber, cellNumber);
                        if (zuordnungAnforderungDerivat != null) {
                            result.add(zuordnungAnforderungDerivat);
                        }
                    }
                }
            } else {
                LOG.warn("Anforderung in Zeile {} wurde nicht gefunden.", Integer.toString(rowNumber));
            }
        }
        LOG.debug("Migration der ZuordnungAnforderungDerivat abgeschlossen. "
                + "Es wurden {} ZuordnungAnforderungDerivat erzeugt.", Integer.toString(result.size()));
        return result;
    }

    private ZuordnungAnforderungDerivat getZuordnungAnforderungDerivatBasedOnExcelStatus(List<Derivat> relevantDerivate, Integer offset, String cellValue, Anforderung anforderungFromCell, Integer rowNumber, Integer cellNumber) {
        Derivat derivat;
        ZuordnungAnforderungDerivat zuordnungAnforderungDerivat;
        derivat = relevantDerivate.get(cellNumber - offset);
        zuordnungAnforderungDerivat = zuordnungAnforderungDerivatService.getZuordnungAnforderungDerivatForAnforderungDerivat(anforderungFromCell, derivat);
        if (zuordnungAnforderungDerivat == null) {
            zuordnungAnforderungDerivat = createZuordnungAnforderungDerivatDependingOnExcelStatus(cellValue, anforderungFromCell, derivat);
            if (zuordnungAnforderungDerivat != null) {
                return zuordnungAnforderungDerivat;
            } else {
                LOG.warn("ZuordnungAnforderungDerivat in Zeile {}, Spalte {} konnte nicht erzeugt werden. "
                        + "Unbekannter Status!", new Object[]{rowNumber.toString(), cellNumber.toString()});
            }
        } else {
            LOG.debug("ZuordnungAnforderungDerivat in Zeile {}, Spalte {} existiert bereits",
                    new Object[]{rowNumber.toString(), cellNumber.toString()});
        }
        return null;
    }

    private List<DerivatAnforderungModul> parseAnforderungDerivatZuordnung(Sheet sheet) {
        LOG.debug("Starte Migration der DerivatAnforderungModulen");
        List<DerivatAnforderungModul> result = new ArrayList<>();
        List<Derivat> relevantDerivate = migrateDerivate(sheet.getRow(0));
        Integer offset = 40; // based on excel sheet columns
        Row row;
        Anforderung anforderungFromCell;
        for (Integer rowNumber = sheet.getFirstRowNum() + 1; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            row = sheet.getRow(rowNumber);
            anforderungFromCell = anforderungService.getUniqueAnforderungByFachId("A" + ExcelUtils.cellToString(row.getCell(0)));
            if (anforderungFromCell != null) {
                List<Modul> module = anforderungFromCell.getUmsetzer().stream().map(u -> u.getModul()).distinct().collect(Collectors.toList());
                searchForAllModulsDerivatAnforderungModul(module, offset, row, relevantDerivate, anforderungFromCell, result, rowNumber);
            } else {
                LOG.warn("Anforderung in Zeile {} wurde nicht gefunden.", rowNumber.toString());
            }
        }
        LOG.debug("Migration der DerivatAnforderungModulen abgeschlossen. "
                + "Es wurden {} DerivatAnforderungModulen erzeugt.", Integer.toString(result.size()));
        return result;
    }

    private void searchForAllModulsDerivatAnforderungModul(List<Modul> module, Integer offset, Row row, List<Derivat> relevantDerivate, Anforderung anforderungFromCell, List<DerivatAnforderungModul> result, Integer rowNumber) {
        String cellValue;
        for (Modul modul : module) {
            for (Integer cellNumber = offset; cellNumber < row.getLastCellNum(); cellNumber++) {
                cellValue = ExcelUtils.cellToString(row.getCell(cellNumber));
                if (!cellValue.isEmpty()) {
                    searchForDerivatAnforderungModulAndAddToResult(relevantDerivate, cellNumber, offset, anforderungFromCell, modul, cellValue, result, rowNumber);
                }
            }
        }
    }

    private void searchForDerivatAnforderungModulAndAddToResult(List<Derivat> relevantDerivate, Integer cellNumber, Integer offset, Anforderung anforderung1, Modul modul, String cellValue, List<DerivatAnforderungModul> result, Integer rowNumber) {
        Derivat derivat;
        DerivatAnforderungModul derivatAnforderungModul;
        derivat = relevantDerivate.get(cellNumber - offset);
        derivatAnforderungModul = derivatAnforderungModulService.getDerivatAnforderungModulByAnforderungAndModulAndDerivat(anforderung1, modul, derivat);
        if (derivatAnforderungModul == null) {
            getDerivatAnforderungModulWithZuordnungAnforderungDerivat(anforderung1, derivat, cellValue, modul, result, rowNumber, cellNumber);
        } else {
            LOG.debug("DerivatAnforderungModul in Zeile {}, Spalte {} existiert bereits",
                    new Object[]{rowNumber.toString(), cellNumber.toString()});
        }
    }

    private void getDerivatAnforderungModulWithZuordnungAnforderungDerivat(Anforderung anforderung1, Derivat derivat, String cellValue, Modul modul, List<DerivatAnforderungModul> result, Integer rowNumber, Integer cellNumber) {
        ZuordnungAnforderungDerivat zuordnungAnforderungDerivat;
        zuordnungAnforderungDerivat = zuordnungAnforderungDerivatService.getZuordnungAnforderungDerivatForAnforderungDerivat(anforderung1, derivat);
        if (zuordnungAnforderungDerivat != null) {
            getDerivatAnforderungModulAndAddToResult(cellValue, zuordnungAnforderungDerivat, modul, result, rowNumber, cellNumber);
        } else {
            LOG.warn("ZuordnungAnforderungDerivat fuer Anforderung {} und Derivat {} existiert nicht.", new Object[]{anforderung1.toString(), derivat.toString()});
        }
    }

    private void getDerivatAnforderungModulAndAddToResult(String cellValue, ZuordnungAnforderungDerivat zuordnungAnforderungDerivat, Modul modul, List<DerivatAnforderungModul> result, Integer rowNumber, Integer cellNumber) {
        DerivatAnforderungModul derivatAnforderungModul;
        derivatAnforderungModul = createDerivatAnforderungModulDependingOnExcelStatus(cellValue, zuordnungAnforderungDerivat, modul);
        if (derivatAnforderungModul != null) {
            result.add(derivatAnforderungModul);
        } else {
            LOG.warn("DerivatAnforderungModul in Zeile {}, Spalte {} konnte nicht erzeugt werden. "
                    + "Unbekannter Status!", new Object[]{rowNumber.toString(), cellNumber.toString()});
        }
    }

    private ZuordnungAnforderungDerivat createZuordnungAnforderungDerivatDependingOnExcelStatus(String status, Anforderung anforderung, Derivat derivat) {
        switch (status) {
            case "false":
                return new ZuordnungAnforderungDerivat(anforderung, derivat, ZuordnungStatus.KEINE_ZUORDNUNG);
            case "true":
            case "zak":
                return new ZuordnungAnforderungDerivat(anforderung, derivat, ZuordnungStatus.ZUGEORDNET);
            case "na":
            default:
                return null;
        }
    }

    private DerivatAnforderungModul createDerivatAnforderungModulDependingOnExcelStatus(String status, ZuordnungAnforderungDerivat anforderungDerivat, Modul modul) {
        DerivatAnforderungModul result;
        switch (status) {
            case "false":
                result = new DerivatAnforderungModul(anforderungDerivat, modul);
                result.setStatusZV(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG);
                result.setStatusVKBG(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG);
                break;
            case "true":
            case "zak":
                result = new DerivatAnforderungModul(anforderungDerivat, modul);
                result.setStatusZV(DerivatAnforderungModulStatus.ABZUSTIMMEN);
                result.setStatusVKBG(DerivatAnforderungModulStatus.ABZUSTIMMEN);
                break;
            case "na":
            default:
                return null;
        }
        return result;
    }

    public void persistDatenmigrationFromExcelSheet(Sheet sheet, Sheet tteamSensorCocSheet, Workbook resultWorkbook) {
        LOG.debug("Erzeuge SensorCoc zu T-Team Map.");
        sensorCocTteamMap = generateSensorCocTteamMap(tteamSensorCocSheet);

        this.allFestgestelltIn.addAll(anforderungService.getAllFestgestelltIn());

        Integer resultSheetRowNumber = migriereAnforderungAndMeldung(resultWorkbook, sheet);

        Sheet resultSheet;

        if (resultWorkbook != null) {
            resultSheet = resultWorkbook.createSheet("Datenmigration_Mitarbeiter");
            Row headerRow = resultSheet.createRow(resultSheetRowNumber);
            Cell headerCell = headerRow.createCell(0);
            headerCell.setCellValue("Problematische Mitarbeiter");

            List<String> sensorNameList = new ArrayList<>(irregularSensorNames);
            setCellValue(sensorNameList, resultSheet);

            resultSheet = resultWorkbook.createSheet("Datenmigration_SensorCoc");
            List<String> sensorCocList = new ArrayList<>(irregularSensorCocNames);
            setCellValue(sensorCocList, resultSheet);

            resultSheet = resultWorkbook.createSheet("Datenmigration_Umsetzer");
            List<String> umsetzerList = new ArrayList<>(irregularUmsetzer);
            setCellValue(umsetzerList, resultSheet);

            resultSheet = resultWorkbook.createSheet("Datenmigration_SensorCocTteamF");
            List<String> sensorCocTteamList = new ArrayList<>(missingTteamSensorCocNames);
            setCellValue(sensorCocTteamList, resultSheet);

            resultSheet = resultWorkbook.createSheet("Datenmigration_Log");
            setCellValue(excelLog, resultSheet);
        }

        LOG.debug("Link Meldungen to Anforderungen");
        try {
            linkMeldungenToAnforderung();
        } catch (MigrationException ex) {
            LOG.error(null, ex);
        }

    }

    private Integer migriereAnforderungAndMeldung(Workbook resultWorkbook, Sheet sheet) {
        Integer resultSheetRowNumber = 0;
        Sheet resultSheet = null;
        if (resultWorkbook != null) {
            resultWorkbook.createSheet(sheet.getSheetName());
            resultSheet = resultWorkbook.getSheet(sheet.getSheetName());
            resultSheet.createRow(resultSheetRowNumber);
            Row headerRow = resultSheet.getRow(resultSheetRowNumber);
            MigrationUtils.copyRowData(headerRow, sheet.getRow(0));
            resultSheetRowNumber++;
        }
        LOG.debug("Migriere Derivate.");
        migrateDerivate(sheet.getRow(0));
        Row row;
        LOG.debug("Start der Datenmigration.");
        int anforderung_counter = 0;
        int anforderung_fehler_counter = 0;
        int meldung_counter = 0;
        int meldung_fehler_counter = 0;
        int zugeordnete_meldung_counter = 0;
        int zugeordnete_meldung_fehler_counter = 0;
        int totalRows = sheet.getLastRowNum() - sheet.getFirstRowNum();
        for (Integer rowNumber = sheet.getFirstRowNum() + 1; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            row = sheet.getRow(rowNumber);
            clearEntityManagerAllFiftyRows(rowNumber, totalRows);

            if (MigrationUtils.isNichtZugeordnetMeldung(row)) {
                if (migriereMeldung(row, Boolean.FALSE)) {
                    meldung_counter++;
                } else {
                    resultSheetRowNumber = copyRowData(resultSheet, resultSheetRowNumber, row);
                    meldung_fehler_counter++;
                }
            } else if (MigrationUtils.isAnforderung(row)) {
                if (migriereAnforderung(row)) {
                    anforderung_counter++;
                } else {
                    resultSheetRowNumber = copyRowData(resultSheet, resultSheetRowNumber, row);
                    anforderung_fehler_counter++;
                }
            } else if (MigrationUtils.isZugeordnetMeldung(row)) {
                if (migriereMeldung(row, Boolean.TRUE)) {
                    zugeordnete_meldung_counter++;
                } else {
                    resultSheetRowNumber = copyRowData(resultSheet, resultSheetRowNumber, row);
                    zugeordnete_meldung_fehler_counter++;
                }
            } else {
                LOG.warn("Zeile {} konnte nicht importiert werden!", Integer.toString(rowNumber));
            }
        }
        LOG.debug("Datenmigration: 100%");
        MigrationUtils.logGesamt(anforderung_counter, "Anforderungen");
        MigrationUtils.logFehlerGesamt(anforderung_fehler_counter, "Anforderungen");
        MigrationUtils.logGesamt(meldung_counter, "nicht zugeordnete Meldungen");
        MigrationUtils.logFehlerGesamt(meldung_fehler_counter, "nicht zugeordnete Meldungen");
        MigrationUtils.logGesamt(zugeordnete_meldung_counter, "zugeordnete Meldungen");
        MigrationUtils.logFehlerGesamt(zugeordnete_meldung_fehler_counter, "zugeordnete Meldungen");
        Integer sum = anforderung_counter + meldung_counter + zugeordnete_meldung_counter;
        LOG.debug(String.format("%d Datensätze wurden insgesamt migriert.", sum));
        LOG.debug("Nicht importierte SensorCoc: {}", irregularSensorCocNames.toString());
        LOG.debug("Nicht importierte Sensor: {}", irregularSensorNames.toString());
        LOG.debug("Kein Tteam zu folgenden SensorCoc gefunden: {}", missingTteamSensorCocNames.toString());
        return resultSheetRowNumber;
    }

    private void setCellValue(List<String> sensorNameList, Sheet resultSheet) {
        Row row;
        Cell cell;
        for (int i = 0; i < sensorNameList.size(); i++) {
            row = resultSheet.createRow(i + 1);
            cell = row.createCell(0);
            cell.setCellValue(sensorNameList.get(i));
        }
    }

    private Integer copyRowData(Sheet resultSheet, Integer resultSheetRowNumber, Row row) {
        Row resultRow;
        if (resultSheet != null) {
            resultSheet.createRow(resultSheetRowNumber);
            resultRow = resultSheet.getRow(resultSheetRowNumber);
            MigrationUtils.copyRowData(resultRow, row);
            resultSheetRowNumber++;
        }
        return resultSheetRowNumber;
    }

    private void clearEntityManagerAllFiftyRows(Integer rowNumber, int totalRows) {
        if (rowNumber % 50 == 0) {
            anforderungService.clearEntityManager();
            Double progress = 100.0 * (totalRows - rowNumber) / totalRows;
            LOG.debug("Datenmigration: {}%", Long.toString(Math.round(progress)));
        }
    }

    private void logToExcelSheet(String fachId, String fehler) {
        excelLog.add(fachId + ": " + fehler);
    }

    /**
     * Parse Row of Anforderung / Meldung Excel sheet to a new Anforderung and
     * persistAnforderungHistory it.
     *
     * @param row
     * @return
     */
    private Boolean migriereAnforderung(Row row) {
        try {
            // -------- parse usual fields -------------------------------------
            anforderung = MigrationUtils.convertToAnforderung(row);
            if (anforderung == null) {
                return Boolean.TRUE;
            }
            // geloeschte Meldungen sollen nicht migriert werden
            if (anforderung.getStatus().equals(Status.A_GELOESCHT)) {
                return false;
            }
            fachid = anforderung.getFachId();
            anforderung.setSensor(findMitarbeiter(MigrationUtils.convertToSensorString(row)));
            anforderung.setUmsetzer(parseUmsetzer(anforderung, MigrationUtils.convertToUmsetzerString(row)));
            if (anforderung.getUmsetzer() == null) {
                logToExcelSheet(anforderung.getFachId(), "Kein Umsetzer gefunden");
                return false;
            }

            anforderung.setFestgestelltIn(parseFestgestelltIn(row));
            // Sensor Coc
            String technologie = MigrationUtils.convertToSensorCocString(row);
            List<SensorCoc> sensorCoc = sensorCocService.getSensorCocsByTechnologie(technologie);
            if (sensorCoc.size() == 1) {
                anforderung.setSensorCoc(sensorCoc.get(0));
                Tteam tteam = sensorCocTteamMap.get(sensorCoc.get(0));
                if (tteam != null) {
                    anforderung.setTteam(tteam);
                } else {
                    missingTteamSensorCocNames.add(technologie);
                    logToExcelSheet(fachid, "T-Team zu SensorCoc " + technologie + " nicht vorhanden");
                    LOG.error("T-Team zu SensorCoc {} nicht vorhanden.", technologie);
                    return false;
                }
            } else {
                irregularSensorCocNames.add(technologie);
                logToExcelSheet(fachid, "SensorCoc " + technologie + "nicht vorhanden");
                LOG.error("SensorCoc zu {} ist nicht vorhanden.", technologie);
                return false;
            }

            String anforderungsPfad = MigrationUtils.convertToAnforderungspfad(row);
            anforderung.setAnfoFreigabeBeiModul(generateFreigabenForAnforderung(anforderung, anforderungsPfad));

            // -------- persistAnforderungHistory Anforderung, generate fachId, add umsetzer -----
            if (!anforderungService.fetchVersionDataForFachId(fachid).isEmpty()) {
                LOG.error("FachId {} ist bereits vorhanden.", fachid);
                return true;
            }
            anforderungService.saveAnforderung(anforderung);

            return true;
        } catch (MigrationException exception) {
            LOG.error(exception.getMessage());
            LOG.error(String.format("Anforderung '%s' konnte nicht migriert werden.", row.getCell(0)));
            logToExcelSheet(fachid, exception.getMessage());
            return false;
        }
    }

    private List<AnforderungFreigabeBeiModulSeTeam> generateFreigabenForAnforderung(Anforderung anforderung, String anforderungsPfad) {
        List<AnforderungFreigabeBeiModulSeTeam> result = new ArrayList<>();

        VereinbarungType vereinbarungType;

        if (anforderungsPfad.trim().equals("ZAK/AK Direkt")) {
            vereinbarungType = VereinbarungType.ZAK;
        } else {
            vereinbarungType = VereinbarungType.STANDARD;
        }

        if (anforderung != null) {
            Status status = anforderung.getStatus();
            switch (status) {
                case A_FREIGEGEBEN:
                    anforderung.getUmsetzer().stream()
                            .map(u -> u.getSeTeam()).forEach(se -> result.add(new AnforderungFreigabeBeiModulSeTeam(anforderung, se, true, vereinbarungType)));
                    break;
                default:
                    anforderung.getUmsetzer().stream()
                            .map(u -> u.getSeTeam()).forEach(se -> result.add(new AnforderungFreigabeBeiModulSeTeam(anforderung, se, false, vereinbarungType)));
                    break;
            }
        }
        return result;
    }

    private Set<FestgestelltIn> parseFestgestelltIn(Row row) {
        if (allFestgestelltIn == null || allFestgestelltIn.isEmpty()) {
            this.allFestgestelltIn = new HashSet<>();
            this.allFestgestelltIn.addAll(anforderungService.getAllFestgestelltIn());
        }
        List<String> parseResult = MigrationUtils.convertToFestgestelltIn(row);
        Set<FestgestelltIn> result = new HashSet<>();
        parseResult.stream().map(s -> s.trim()).forEach(s -> {
            Optional<FestgestelltIn> festgestelltIn = allFestgestelltIn.stream().filter(fi -> fi.getWert().equals(s)).findFirst();
            if (festgestelltIn.isPresent()) {
                result.add(festgestelltIn.get());
            } else {
                FestgestelltIn fi = new FestgestelltIn(s);
                anforderungService.persistFestgestelltIn(fi);
                allFestgestelltIn.add(fi);
                result.add(fi);
            }
        });
        return result;
    }

    private String parseKomponenteForBracketCase(String string) {
        String ppg = string.substring(string.length() - 10, string.length() - 1);
        return ppg;
    }

    private String parseSeTeamForSlashCase(String string) {
        if (string.contains("/")) {
            string = string.substring(string.indexOf("/") + 1);
            if (string.contains("/")) {
                String seTeam = string.substring(string.indexOf("/") + 1);
                return seTeam.trim();
            }
        }
        return null;
    }

    /**
     * Parse Umsetzer String of the Form modul / modulSeTeam / modulKomponente
     * (PPG number) \n ... and return a new Set of Umsetzer instances
     *
     * @param umsetzerString
     * @return
     */
    public Set<Umsetzer> parseUmsetzer(Anforderung anforderungForMethod, String umsetzerString) {
        Set<Umsetzer> result = new HashSet<>();
        String originalUmsetzerString = new String(umsetzerString);
        umsetzerString += "\n";
        while (umsetzerString.contains("\n")) {
            umsetzerString = getUmsetzerFromString(umsetzerString, result, anforderungForMethod, originalUmsetzerString);
        }
        if (result.isEmpty()) {
            irregularUmsetzer.add(originalUmsetzerString);
            logToExcelSheet(anforderungForMethod.getFachId(), "Umsetzer " + originalUmsetzerString + " konnte nicht gefunden werden!");
        }
        return result;
    }

    private String getUmsetzerFromString(String umsetzerString, Set<Umsetzer> result, Anforderung anforderungForMethod, String originalUmsetzerString) {
        // parse string
        String modul = null;
        String seTeam = null;
        String alternateSeTeam = null;
        String ppg = null;
        String modulString = umsetzerString.substring(0, umsetzerString.indexOf("\n"));
        if (modulString.contains("/") && modulString.substring(modulString.indexOf("/")).length() > 0 && modulString.substring(modulString.indexOf("/")).contains("/")) {
            seTeam = modulString.substring(modulString.indexOf("/") + 2);
            if (seTeam.length() > 4 && seTeam.contains("/")) {
                seTeam = seTeam.substring(0, seTeam.indexOf("/") - 1);
                alternateSeTeam = parseSeTeamForSlashCase(modulString);
                String komponente = modulString.substring(modulString.substring(modulString.indexOf("/")).indexOf("/") + 1);
                ppg = setPpgFromKomponentAndModul(komponente, ppg, modulString);
            }
        } else if (modulString.matches("\\w+[:]\\s\\w+.*")) {
            modul = modulString;
        }
        addUmsetzerToResult(seTeam, ppg, alternateSeTeam, result, modul, anforderungForMethod, originalUmsetzerString);
        // update string
        umsetzerString = umsetzerString.substring(umsetzerString.indexOf("\n") + 1);
        return umsetzerString;
    }

    private void addUmsetzerToResult(String seTeam, String ppg, String alternateSeTeam, Set<Umsetzer> result, String modul, Anforderung anforderungForMethod, String originalUmsetzerString) {
        if (seTeam != null) {
            addNewUmsetzerToResultFromSeTeam(ppg, seTeam, alternateSeTeam, result);
        } else if (modul != null) {
            addNewUmsetzerFromModul(modul, anforderungForMethod, originalUmsetzerString, result);
        }
    }

    private String setPpgFromKomponentAndModul(String komponente, String ppg, String modulString) {
        if (komponente != null && komponente.contains("(") && komponente.contains(")")) {
            ppg = komponente.substring(komponente.indexOf("(") + 1, komponente.indexOf(")"));
            if (!ppg.contains("_")) { // every ppg number must have a _ in it
                ppg = parseKomponenteForBracketCase(modulString);
                if (!ppg.contains("_")) {
                    ppg = null;
                }
            }
        }
        return ppg;
    }

    private void addNewUmsetzerFromModul(String modul, Anforderung anforderungForMethod, String originalUmsetzerString, Set<Umsetzer> result) {
        Modul m = modulService.getModulByName(modul.trim());
        logToExcelSheet(anforderungForMethod.getFachId(), "Umsetzer " + originalUmsetzerString + " konnte nicht korrekt verarbeitet werden! Verwende alle SeTeams für das gefundene Modul.");
        if (m != null) {
            m.getSeTeams().forEach(se -> result.add(new Umsetzer(se)));
        }
    }

    private void addNewUmsetzerToResultFromSeTeam(String ppg, String seTeam, String alternateSeTeam, Set<Umsetzer> result) {
        // create new Umsetzer
        Umsetzer umsetzer = new Umsetzer();
        if (ppg != null) {
            // find modulKomponente and add to Umsetzer
            ModulKomponente modulKomponente = modulService.getModulKomponenteByPpg(ppg.trim());
            if (modulKomponente != null) {
                umsetzer.setKomponente(modulKomponente);
                umsetzer.setSeTeam(modulKomponente.getSeTeam());
            }
        } else {
            ModulSeTeam modulSeTeam = modulService.getSeTeamByName(seTeam.trim());
            if (modulSeTeam == null && alternateSeTeam != null) {
                modulSeTeam = modulService.getSeTeamByName(alternateSeTeam);
            }
            umsetzer.setSeTeam(modulSeTeam);
        }

        if (umsetzer.getSeTeam() != null) {
            result.add(umsetzer);
        }
    }

    private Boolean migriereMeldung(Row row, Boolean isZugeordnet) {
        Boolean result = persistNewMeldung(row);
        if (result && isZugeordnet) {
            linkMeldungToAnforderungHashMap.put("M" + ExcelUtils.cellToString(row.getCell(0)), MigrationUtils.convertToParentId(row));
        }
        return result;
    }

    private Boolean persistNewMeldung(Row row) {
        try {
            meldung = MigrationUtils.convertToMeldung(row);
            if (meldung == null) {
                return Boolean.TRUE;
            }
            if (meldung.getStatus().equals(Status.M_GELOESCHT)) {
                return false;
            }
            fachid = meldung.getFachId();
            meldung.setSensor(findMitarbeiter(MigrationUtils.convertToSensorString(row)));
            meldung.setFestgestelltIn(parseFestgestelltIn(row));
            String technologie = MigrationUtils.convertToSensorCocString(row);
            List<SensorCoc> sensorCoc = sensorCocService.getSensorCocsByTechnologie(technologie);
            if (sensorCoc.size() == 1) {
                meldung.setSensorCoc(sensorCoc.get(0));
            } else {
                logToExcelSheet(meldung.getFachId(), "SensorCoc " + technologie + "nicht vorhanden");
                LOG.error("SensorCoc zu {} ist nicht vorhanden.", technologie);
                return false;
            }
            if (meldung.getSensor() == null) {
                LOG.error("Meldung {} konne nicht migriert werden!", MigrationUtils.convertToOriginalId(row).toString());
                return false;
            }
            if (!anforderungService.getMeldungByFachIdStartingWith(fachid).isEmpty()) {
                LOG.error("FachId {} existiert bereits!", fachid);
                return true;
            }
            anforderungService.saveMeldung(meldung);

//            Date datumStatusanderung = MigrationUtils.convertToDatumStatuswechselString(row);
//            if (datumStatusanderung != null) {
//                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
//                AnforderungHistory ah = new AnforderungHistory(m.getId(), "M", datumStatusanderung, "Datum Statuswechsel", "", "", sdf.format(datumStatusanderung));
//                historyService.persistAnforderungHistory(ah); // has no jpa link to meldung
//            } else {
//                AnforderungHistory ah = new AnforderungHistory(m.getId(), "M", m.getSensor().toString());
//                ah.setDatum(m.getErstellungsdatum());
//                historyService.persistAnforderungHistory(ah); // has no jpa link to meldung
//            }
            return true;
        } catch (MigrationException exception) {
            LOG.error(exception.getMessage());
            LOG.error("Meldung {} konnte nicht migriert werden.", MigrationUtils.convertToOriginalId(row).toString());
            logToExcelSheet(fachid, exception.getMessage());
            return false;
        }
    }

    private void linkMeldungenToAnforderung() throws MigrationException {
        int i = 0;
        Set<String> meldungFachIdSet = linkMeldungToAnforderungHashMap.keySet();
        LOG.debug(Integer.toString(meldungFachIdSet.size()));
        Meldung m;
        for (String meldungFachId : meldungFachIdSet) {
            if (i++ % 50 == 0) {
                double progress = 100.0 * i / meldungFachIdSet.size();
                LOG.debug("Link Meldung To Anfoderung: " + Long.toString(Math.round(progress)) + "%");
            }
            m = anforderungService.getUniqueMeldungByFachId(meldungFachId);
            linkMeldungToAnfoderung(m, linkMeldungToAnforderungHashMap.get(meldungFachId));
        }
    }

    private Boolean linkMeldungToAnfoderung(Meldung meldung, String anforderungFachId) {
        Anforderung a = anforderungService.getAnforderungByFachIdVersion(anforderungFachId, 1);
        if (meldung != null && a != null) {
            if (!a.getMeldungen().contains(meldung)) {
                a.getMeldungen().add(meldung);
                meldung.setAssignedToAnforderung(true);
                meldung.setStatus(Status.M_ZUGEORDNET);
                anforderungService.saveAnforderung(a);
            }
            return true;
        }
        return false;
    }

    @SuppressWarnings({"checkstyle:CyclomaticComplexity", "checkstyle:MethodLength"})
    private Mitarbeiter findMitarbeiter(String mitarbeiterString) throws MigrationException {
        String vorname;
        String nachname;
        String abteilung;

        mitarbeiterString = MigrationUtils.normalizeMitarbeiterString(mitarbeiterString);

        if (mitarbeiterString.matches("^[\\u00C0-\\u017FA-Za-z-']+\\s[\\u00C0-\\u017FA-Za-z-']+,\\s.*")) {
            abteilung = mitarbeiterString.substring(mitarbeiterString.indexOf(',') + 2, mitarbeiterString.length());
            nachname = mitarbeiterString.substring(mitarbeiterString.substring(0, mitarbeiterString.indexOf(',')).lastIndexOf(" ") + 1, mitarbeiterString.indexOf(','));
            vorname = mitarbeiterString.substring(0, mitarbeiterString.substring(0, mitarbeiterString.indexOf(',')).lastIndexOf(" "));
        } else if (mitarbeiterString.matches("[\\u00C0-\\u017FA-Za-z-']+\\s[\\u00C0-\\u017FA-Za-z-']+\\s\\w+[-]\\d+") && mitarbeiterString.split(" ").length == 3) {
            String[] split = mitarbeiterString.split(" ");
            abteilung = split[2];
            nachname = split[0];
            vorname = split[1];
        } else if (mitarbeiterString.matches("[D][r][.]\\s[\\u00C0-\\u017FA-Za-z-']+\\s[\\u00C0-\\u017FA-Za-z-']+[,]\\s\\w+[-]\\d+") && mitarbeiterString.split(" ").length == 4) {
            String[] split = mitarbeiterString.split(" ");
            abteilung = split[3];
            nachname = split[0] + " " + split[2].replaceAll(",", "");
            vorname = split[1];
        } else if (mitarbeiterString.matches("[D][r][.]\\s[\\u00C0-\\u017FA-Za-z-']+\\s[\\u00C0-\\u017FA-Za-z-']+[,]\\s\\w+[-]\\d+[,].*")) {
            String[] split = mitarbeiterString.split(" ");
            abteilung = split[3].replaceAll(",", "");
            nachname = split[0] + " " + split[2].replaceAll(",", "");
            vorname = split[1];
        } else if (mitarbeiterString.matches("[\\u00C0-\\u017FA-Za-z-']+\\s[\\u00C0-\\u017FA-Za-z-']+[,]\\s\\w+[-]\\d+") && mitarbeiterString.split(" ").length == 3) {
            String[] split = mitarbeiterString.split(" ");
            abteilung = split[2].trim();
            nachname = split[0].trim();
            vorname = split[1].trim();
        } else if (mitarbeiterString.matches("[\\u00C0-\\u017FA-Za-z-']+\\s[\\u00C0-\\u017FA-Za-z-']+")) {
            abteilung = "";
            vorname = mitarbeiterString.substring(0, mitarbeiterString.lastIndexOf(' '));
            nachname = mitarbeiterString.substring(mitarbeiterString.lastIndexOf(' '));
        } else if (mitarbeiterString.matches("[\\u00C0-\\u017FA-Za-z-']+\\s\\w+[-]\\d+")) {
            vorname = "";
            nachname = mitarbeiterString.substring(0, mitarbeiterString.lastIndexOf(' '));
            abteilung = mitarbeiterString.substring(mitarbeiterString.lastIndexOf(' ') + 1);
        } else if (mitarbeiterString.matches("[\\u00C0-\\u017FA-Za-z-']+[,]\\s\\w+[-]\\d+")) {
            vorname = "";
            nachname = mitarbeiterString.substring(0, mitarbeiterString.lastIndexOf(','));
            abteilung = mitarbeiterString.substring(mitarbeiterString.lastIndexOf(' ') + 1);
        } else if (mitarbeiterString.matches("[\\u00C0-\\u017FA-Za-z-']+[.]\\s[\\u00C0-\\u017FA-Za-z-']+[-]\\d+")) {
            vorname = "";
            nachname = mitarbeiterString.substring(0, mitarbeiterString.lastIndexOf(' ') - 1);
            abteilung = mitarbeiterString.substring(mitarbeiterString.lastIndexOf(' ') + 1);
        } else if (mitarbeiterString.matches("^[\\u00C0-\\u017FA-Za-z-']+\\s\\w{}\\s[\\u00C0-\\u017FA-Za-z-']+[,]\\s\\w+[-]\\d+.*")) {
            abteilung = mitarbeiterString.substring(mitarbeiterString.indexOf(',') + 2, mitarbeiterString.length());
            if (abteilung.contains(",")) {
                abteilung = abteilung.substring(0, abteilung.indexOf(','));
            } else {
                abteilung = mitarbeiterString.substring(mitarbeiterString.indexOf(',') + 2);
            }
            nachname = mitarbeiterString.substring(mitarbeiterString.substring(0, mitarbeiterString.indexOf(',')).lastIndexOf(' ') + 1, mitarbeiterString.indexOf(','));
            vorname = mitarbeiterString.substring(0, mitarbeiterString.substring(0, mitarbeiterString.indexOf(',')).indexOf(' '));
        } else if (mitarbeiterString.matches("[\\u00C0-\\u017FA-Za-z-']+\\s[\\u00C0-\\u017FA-Za-z-']+[,]\\s*\\w+[-]\\d+.*")) {
            abteilung = mitarbeiterString.substring(mitarbeiterString.indexOf(',') + 1, mitarbeiterString.length());
            abteilung = abteilung.trim();
            if (abteilung.contains(",")) {
                abteilung = abteilung.substring(0, abteilung.indexOf(','));
            } else {
                abteilung = mitarbeiterString.substring(mitarbeiterString.indexOf(',') + 2);
            }
            nachname = mitarbeiterString.substring(mitarbeiterString.substring(0, mitarbeiterString.indexOf(',')).lastIndexOf(' ') + 1, mitarbeiterString.indexOf(','));
            vorname = mitarbeiterString.substring(0, mitarbeiterString.substring(0, mitarbeiterString.indexOf(',')).indexOf(' '));
        } else if (mitarbeiterString.matches("\\s*[\\u00C0-\\u017FA-Za-z-']+\\s\\w+[-]\\d+")) {
            abteilung = mitarbeiterString.substring(mitarbeiterString.indexOf(' ') + 1);
            nachname = mitarbeiterString.substring(0, mitarbeiterString.indexOf(' '));
            vorname = "";
        } else if (mitarbeiterString.matches("\\s*[\\u00C0-\\u017FA-Za-z-']+,\\s\\w+[-]\\d+.")) {
            abteilung = mitarbeiterString.substring(mitarbeiterString.indexOf(' ') + 1);
            nachname = mitarbeiterString.substring(0, mitarbeiterString.indexOf(','));
            vorname = "";
        } else if (mitarbeiterString.matches("[\\u00C0-\\u017FA-Za-z-']+\\s\\w+[-]\\d+.*") && mitarbeiterString.split(" ").length == 2) {
            String[] split = mitarbeiterString.split(" ");
            abteilung = split[1].replaceAll(",", "");
            nachname = split[0].replaceAll(",", "");
            vorname = "";
        } else if (mitarbeiterString.matches("[\\u00C0-\\u017FA-Za-z-',.]+\\s\\w+[-]\\d+.*") && mitarbeiterString.split(" ").length > 2) {
            String[] split = mitarbeiterString.split(" ");
            abteilung = split[1].replaceAll(",", "");
            nachname = split[0].replaceAll(",", "");
            vorname = "";
        } else if (mitarbeiterString.matches("[\\u00C0-\\u017FA-Za-z|-|'|,|.]+\\s[\\u00C0-\\u017FA-Za-z|-|'|,|.]+\\s\\w+[-]\\d+.*") && mitarbeiterString.split(" ").length > 3) {
            String[] split = mitarbeiterString.split(" ");
            abteilung = split[2].replaceAll(",", "");
            nachname = split[0].replaceAll(",", "");
            vorname = split[1].replaceAll(",", "");
        } else if (mitarbeiterString.matches("[\\u00C0-\\u017FA-Za-z|-|'|,|.|;]+\\s[\\u00C0-\\u017FA-Za-z|-|'|,|.|;]+\\s\\w+[-]\\d+.*") && mitarbeiterString.split(" ").length > 3) {
            String[] split = mitarbeiterString.split(" ");
            abteilung = split[2].replaceAll("[,|;]", "");
            nachname = split[0].replaceAll("[,|;]", "");
            vorname = split[1].replaceAll("[,|;]", "");
        } else {
            abteilung = "";
            nachname = mitarbeiterString;
            vorname = "";
            irregularSensorNames.add(mitarbeiterString);
        }

        if (abteilung.contains(" ")) {
            abteilung = abteilung.substring(0, abteilung.indexOf(" "));
        }
        abteilung = abteilung.replaceAll("[,|;|.]", "");
        nachname = nachname.trim();
        nachname = nachname.replaceAll("[,|;|.]", "");

        List<Mitarbeiter> result = uss.getMitarbeiterByCriteria(vorname, nachname, abteilung);
        if (result.isEmpty()) { // no Mitarbeiter found in LDAP or DB. Create new Dummy-Mitarbeiter
            Mitarbeiter mitarbeiter = new Mitarbeiter(vorname, nachname);
            mitarbeiter.setAbteilung(abteilung);
            mitarbeiter.setLocation("de");
            us.saveMitarbeiter(mitarbeiter);
            return mitarbeiter;
        } else if (result.size() == 1) {
            return result.get(0);
        } else {
            irregularSensorNames.add(mitarbeiterString);
            return result.get(0);
        }
    }

    // ------------  persistAnforderungHistory data  ---------------------------------------------
    public AnforderungHistory convertToAnforderungHistory(Row row) throws MigrationException {
        String fachId = ExcelUtils.cellToString(row.getCell(0));
        AbstractAnforderung a = anforderungService.getUniqueAnforderungOrMeldungByFachIdEndingWith(fachId);
        if (a != null) {
            try {
                String datumStr = ExcelUtils.cellToString(row.getCell(4));
                DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
                Date datum = dateFormat.parse(datumStr);
                if (datum != null) {
                    AnforderungHistory ah = new AnforderungHistory(a.getId(), a.getKennzeichen(),
                            datum, ExcelUtils.cellToString(row.getCell(1)), ExcelUtils.cellToString(row.getCell(5)),
                            ExcelUtils.cellToString(row.getCell(2)), ExcelUtils.cellToString(row.getCell(3)));
                    historyService.persistAnforderungHistory(ah);
                    return ah;
                }
            } catch (ParseException ex) {
                LOG.error(null, ex);
            }
        }
        return null;
    }

    public boolean migrateAnforderungHistory(Row row) {
        AnforderungHistory ah = null;
        Long originalId = MigrationUtils.convertToOriginalId(row);
        try {
            ah = convertToAnforderungHistory(row);
        } catch (MigrationException exception) {
            LOG.error(exception.getMessage());
            LOG.error("Anforderungshistorie von Anforderung ID {} konnte nicht migriert werden.", originalId.toString());
        }
        return ah != null;
    }

    public void persistAnforderungshistorieDatenFromExcelSheet(Sheet sheet) {
        int successCounter = 0;
        int errorCounter = 0;
        int totalRows = sheet.getLastRowNum() - sheet.getFirstRowNum();

        // iterate over rows of the excel sheet and determine the case
        for (int rowNumber = sheet.getFirstRowNum() + 1; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            Row row = sheet.getRow(rowNumber);
            if (rowNumber % 500 == 0) { // log progress
                Double progress = 100.0 * (totalRows - rowNumber) / totalRows;
                LOG.debug("Migration der Anforderungshistorie: " + Long.toString(Math.round(progress)) + "%");
            }
            if (migrateAnforderungHistory(row)) {
                successCounter++;
            } else {
                errorCounter++;
            }
        }

        // print migration report
        LOG.info("Migration der Anforderungshistorie: 100% abgeschlossen");
        LOG.info("Migrierte Datensaetze: {}", successCounter);
        LOG.info("Nicht migrierte Datensaetze: {}", errorCounter);
        int divisor = successCounter + errorCounter;
        int rate;
        if (divisor != 0) {
            rate = successCounter * 100 / (divisor);
        } else {
            rate = 0;
        }
        LOG.info("Erfolgsrate liegt bei {} Prozent", rate);

    }

    public void migrateAnhaenge(InputStream inputStream) throws IOException {
        ZipInputStream zis = new ZipInputStream(inputStream);
        ZipEntry entry;

        while ((entry = zis.getNextEntry()) != null) {
            if (!entry.isDirectory()) {
                String name = entry.getName();
                LOG.info("entry: {}", name);

                DbFile dbFile = new DbFile();
                String[] nameWithExtension = name.split("\\.(?=[^\\.]+$)");
                if (nameWithExtension.length > 1) {
                    dbFile.setName(name.substring(name.lastIndexOf("/") + 1));
                    dbFile.setType(nameWithExtension[1]);
                    Date lmDate = new Date(entry.getLastModifiedTime().toMillis());
                    dbFile.setCreationDate(lmDate);
                    dbFile.setContent(IOUtils.toByteArray(zis));

                    if (dbFile.getName() != null && dbFile.getType() != null
                            && dbFile.getCreationDate() != null && !dbFile.getName().contains("Thumbs.db")) {
                        Anhang anh = new Anhang(dbFile.getName(), dbFile);
                        anh.setDateityp(dbFile.getType());

                        anh.setFilesize(getFileSize(dbFile, anh));

                        anh.setZeitstempel(dbFile.getCreationDate());

                        String fachIdNumber = nameWithExtension[0].substring(nameWithExtension[0].indexOf("/") + 1, nameWithExtension[0].lastIndexOf("/"));
                        AbstractAnforderung aa = anforderungService.getUniqueAnforderungOrMeldungByFachIdEndingWith(Long.parseLong(fachIdNumber));
                        aa.addAnhang(anh);
                        LOG.info("add Anhang: {} to {}", new Object[]{anh.toString(), aa.toString()});

                        persistAbstractAnforderung(aa);
                    }
                }
            }
        }
    }

    private int getFileSize(DbFile dbFile, Anhang anh) {
        if (dbFile.getContent() != null) {
            return dbFile.getContent().length;
        } else {
            return 0;
        }
    }

    private void persistAbstractAnforderung(AbstractAnforderung aa) {
        if (aa instanceof Anforderung) {
            anforderungService.saveAnforderung((Anforderung) aa);
        } else {
            anforderungService.saveMeldung((Meldung) aa);
        }
    }

    public Boolean migrateModulBilder(InputStream inputStream) throws IOException {
        ZipInputStream zis = new ZipInputStream(inputStream);
        ZipEntry entry;
        Boolean success = false;

        while ((entry = zis.getNextEntry()) != null) {
            if (!entry.isDirectory()) {
                String[] nameWithExtension = entry.getName().split("\\.(?=[^\\.]+$)");
                switch (nameWithExtension[1]) {
                    case "jpeg":
                    case "jpg":
                    case "png":
                        String modulName = nameWithExtension[0].substring(nameWithExtension[0].indexOf("/") + 1);
                        ModulSeTeam seTeam = modulService.getModulSeTeamByName(modulName);
                        if (seTeam != null) {
                            Bild bild = imageService.generateBildFromInputStream(zis, nameWithExtension[1]);
                            seTeam.setBild(bild);
                            modulService.persistModulSeTeam(seTeam);
                            success = true;
                        } else {
                            LOG.warn("Modul-Bilder Import: Modul {} wurde nicht gefunden!", modulName);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        return success;
    }

    private DerivatAnforderungModul convertToDerivatAnforderungModul(Row row) {
        Anforderung anforderung = anforderungService.getUniqueAnforderungByFachId("A" + ExcelUtils.cellToString(row.getCell(0)));
        String modulName = ExcelUtils.cellToString(row.getCell(2));
        Modul modul = modulService.getModulByName(modulName);
        if (anforderung != null) {
            if (modul != null) {
                String derivatName = ExcelUtils.cellToString(row.getCell(3));
                Derivat derivat = derivatService.getDerivatByName(derivatName);
                if (derivat != null) {
                    ZuordnungAnforderungDerivat anforderungDerivat = zuordnungAnforderungDerivatService.getZuordnungAnforderungDerivatForAnforderungDerivat(anforderung, derivat);
                    anforderungDerivat = persistZuordnungAnforderungDerivat(anforderung, derivat, anforderungDerivat);

                    DerivatAnforderungModul derivatAnforderungModul = derivatAnforderungModulService.getDerivatAnforderungModulByAnforderungAndModulAndDerivat(anforderung, modul, derivat);
                    derivatAnforderungModul = persistDerivatAnforderungModul(modul, anforderungDerivat, derivatAnforderungModul);
                    derivatAnforderungModul.setZakStatus(ZakStatus.PENDING);
                    return derivatAnforderungModul;
                } else {
                    LOG.warn("Derivat {} nicht vorhanden.", derivatName);
                }
            } else {
                LOG.warn("Modul {} nicht vorhanden.", modulName);
            }

        } else {
            LOG.warn("Anforderung {} nicht vorhanden.", "A" + ExcelUtils.cellToString(row.getCell(0)));
        }
        return null;
    }

    private ZuordnungAnforderungDerivat persistZuordnungAnforderungDerivat(Anforderung anforderung, Derivat derivat, ZuordnungAnforderungDerivat anforderungDerivat) {
        if (anforderungDerivat == null) {
            anforderungDerivat = new ZuordnungAnforderungDerivat(anforderung, derivat, ZuordnungStatus.ZUGEORDNET);
            anforderungDerivat.setNachZakUebertragen(Boolean.TRUE);
            zuordnungAnforderungDerivatService.persistZuordnungAnforderungDerivat(anforderungDerivat);
        }
        return anforderungDerivat;
    }

    private DerivatAnforderungModul persistDerivatAnforderungModul(Modul modul, ZuordnungAnforderungDerivat anforderungDerivat, DerivatAnforderungModul derivatAnforderungModul) {
        if (derivatAnforderungModul == null) {
            derivatAnforderungModul = new DerivatAnforderungModul(anforderungDerivat, modul);
            derivatAnforderungModul.setStatusVKBG(DerivatAnforderungModulStatus.ABZUSTIMMEN);
            derivatAnforderungModul.setStatusZV(DerivatAnforderungModulStatus.ABZUSTIMMEN);
            derivatAnforderungModulService.persistDerivatAnforderungModul(derivatAnforderungModul);
        }
        return derivatAnforderungModul;
    }

    public ZakUebertragung convertToZakUebertragung(Row row) {
        DerivatAnforderungModul da = convertToDerivatAnforderungModul(row);
        if (da != null) {
            String modulName = ExcelUtils.cellToString(row.getCell(2));
            Modul modul = modulService.getModulByName(modulName);
            if (modul != null) {
                Anforderung anforderung = da.getAnforderung();
                modul.getSeTeams().stream()
                        .map(se -> {
                            AnforderungFreigabeBeiModulSeTeam anforderungFreigabe
                                    = anforderungFreigabeService.getAnfoFreigabeByAnforderungAndModulSeTeam(anforderung, se);
                            if (anforderungFreigabe == null) {
                                anforderungFreigabe = new AnforderungFreigabeBeiModulSeTeam(anforderung, se);
                            }
                            anforderungFreigabe.setFreigabe(true);
                            anforderungFreigabe.setVereinbarungType(VereinbarungType.ZAK);
                            return anforderungFreigabe;
                        })
                        .forEachOrdered(anforderungFreigabe -> anforderung.addAnfoFreigabeBeiModul(anforderungFreigabe));
                anforderungService.saveAnforderung(anforderung);

                try {
                    String zakIdStr = ExcelUtils.cellToString(row.getCell(1));
                    Long zakId = Long.parseLong(zakIdStr);

                    String datumStr = ExcelUtils.cellToString(row.getCell(4));
                    DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                    Date datum = dateFormat.parse(datumStr);

                    ZakUebertragung zak = new ZakUebertragung(da, ZakStatus.PENDING, zakId, datum);
                    zakVereinbarungService.persistZakUebertragung(zak);
                    return zak;
                } catch (ParseException ex) {
                    LOG.error(ex.toString());
                }
            } else {
                LOG.warn("Migration Exception. Kein Modul mit dem Namen {} wurde in der Datenbank gefunden", ExcelUtils.cellToString(row.getCell(2)));
            }
        }
        return null;
    }

    public Boolean migrateZakUebertragung(Row row) {
        if (convertToZakUebertragung(row) != null) {
            return true;
        }
        LOG.warn("ZakUebertragung mit ZAK ID {} konnte nicht migriert werden.",
                ExcelUtils.cellToString(row.getCell(1)));
        return false;
    }

    public void persistZakUebertragungDatenFromExcelSheet(Sheet sheet) {
        int successCounter = 0;
        int errorCounter = 0;
        int totalRows = sheet.getLastRowNum() - sheet.getFirstRowNum();

        for (int rowNumber = sheet.getFirstRowNum() + 1; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            Row row = sheet.getRow(rowNumber);
            if (rowNumber % 50 == 0) {
                Double progress = 100.0 * (totalRows - rowNumber) / totalRows;
                LOG.debug("Migration der ZakUebertragung: " + Long.toString(Math.round(progress)) + "%");
            }
            if (migrateZakUebertragung(row)) {
                successCounter++;
            } else {
                errorCounter++;
            }
        }

        LOG.info("Migration der ZakUebertragung: 100% abgeschlossen");
        LOG.info("Migrierte Datensaetze: {}", successCounter);
        LOG.info("Nicht migrierte Datensaetze: {}", errorCounter);
        int divisor = successCounter + errorCounter;
        int rate;
        if (divisor != 0) {
            rate = successCounter * 100 / (divisor);
        } else {
            rate = 0;
        }
        LOG.info("Erfolgsrate liegt bei {} Prozent", rate);
    }

    public void updateLastStatusChangeDateFromHistory() {
        LOG.info("---------- Start update zeitpunktStatusaenderung -------------");

        anforderungService.getAllAnforderung()
                .stream()
                .filter(a -> a.getZeitpunktStatusaenderung() == null)
                .forEach(a -> {
                    Date lastStatusChangeDateForAnforderung = historyService.getLastStatusChangeDateForAnforderung(a);
                    a.setZeitpunktStatusaenderung(lastStatusChangeDateForAnforderung);
                    anforderungService.saveAnforderung(a);
                });

        anforderungService.getAllMeldungen()
                .stream()
                .filter(m -> m.getZeitpunktStatusaenderung() == null)
                .forEach(m -> {
                    Date lastStatusChangeDateForAnforderung = historyService.getLastStatusChangeDateForMeldung(m);
                    m.setZeitpunktStatusaenderung(lastStatusChangeDateForAnforderung);
                    anforderungService.saveMeldung(m);
                });

        LOG.info("---------- update zeitpunktStatusaenderung finished -------------");
    }

    public void updateLatestDerivatAnforderungModulHistory() {
        LOG.info("---------- Start update zeitpunktStatusaenderung -------------");

        derivatAnforderungModulService.getAllDerivatAnforderungModulen().stream()
                .filter(dam -> !dam.getLatestHistoryEntry().isPresent())
                .forEach(dam -> {
                    vereinbarungHistoryService.getLatestHistoryForDerivatAnforderungModul(dam)
                            .ifPresent(h -> {
                                dam.setLatestHistoryEntry(h);
                                derivatAnforderungModulService.persistDerivatAnforderungModul(dam);
                            });
                });

        LOG.info("---------- update zeitpunktStatusaenderung finished -------------");
    }

    public void migrateDateValuesForAnforderungenAndMeldungen(Sheet sheet, Workbook resultWorkbook) {
        int totalRows = sheet.getLastRowNum() - sheet.getFirstRowNum();

        Row row;

        List<MigrationResultDto> migrationResults = new ArrayList<>();

        LOG.info("Start der Datenmigration...");
        for (Integer rowNumber = sheet.getFirstRowNum() + 1; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            row = sheet.getRow(rowNumber);

            String anforderungFachId = ExcelUtils.cellToString(row.getCell(0));
            String meldungFachId = ExcelUtils.cellToString(row.getCell(1));

            String fachId;
            if (meldungFachId != null && !meldungFachId.isEmpty()) {
                fachId = meldungFachId;
            } else if (anforderungFachId != null && !anforderungFachId.isEmpty()) {
                fachId = anforderungFachId;
            } else {
                migrationResults.add(new MigrationResultDto("", false, "Keine FachId vorhanden!"));
                continue;
            }

            try {
                Date erstellungsdatum = MigrationUtils.convertToDate(row.getCell(3));
                Date datumStatusaenderung = MigrationUtils.convertToDate(row.getCell(4));
                Date aenderungsdatum = MigrationUtils.convertToDate(row.getCell(5));

                migrationResults.add(updateDatesForAbstractAnforderung(fachId,
                        erstellungsdatum, aenderungsdatum, datumStatusaenderung));

            } catch (ParseException ex) {
                LOG.error(null, ex);
                migrationResults.add(new MigrationResultDto(fachId, false, "Datum konnte nicht konvertiert werden!"));
                continue;
            }

            if (rowNumber % 50 == 0) {
                anforderungService.clearEntityManager();
                Double progress = 100.0 * rowNumber / totalRows;
                LOG.info("Datenmigration: {}%", Long.toString(Math.round(progress)));
            }

        }

        if (resultWorkbook != null) {
            int resultSheetRowNumber = 0;

            Sheet resultSheet = resultWorkbook.createSheet("Datenmigration");
            Row headerRow = resultSheet.createRow(resultSheetRowNumber++);
            headerRow.createCell(0).setCellValue("FachId");
            headerRow.createCell(1).setCellValue("Status");
            headerRow.createCell(2).setCellValue("Nachricht");

            Row resultRow;
            for (MigrationResultDto migrationResult : migrationResults) {
                resultRow = resultSheet.createRow(resultSheetRowNumber++);
                resultRow.createCell(0).setCellValue(migrationResult.getId());
                resultRow.createCell(1).setCellValue(migrationResult.isSuccessful());
                resultRow.createCell(2).setCellValue(migrationResult.getMessage());
            }
        }

        LOG.info("Datenmigration: 100%");

        long successful = migrationResults.stream().filter(m -> m.isSuccessful()).count();
        long notSuccessful = migrationResults.stream().filter(m -> !m.isSuccessful()).count();
        long total = migrationResults.size();

        LOG.info("Erfolgreich: {} / {}", new Object[]{successful, total});
        LOG.info("Nicht erfolgreich: {} / {}", new Object[]{notSuccessful, total});

    }

    private MigrationResultDto updateDatesForAbstractAnforderung(String fachId,
                                                                 Date erstellungsdatum, Date aenderungsdatum, Date datumStatusaenderung) {

        Optional<Anforderung> anforderung = Optional.ofNullable(anforderungService.getUniqueAnforderungByFachId(fachId));
        Optional<Meldung> meldung = Optional.ofNullable(anforderungService.getUniqueMeldungByFachId(fachId));

        List<MigrationResultDto> migrationResults = new ArrayList<>();

        if (anforderung.isPresent()) {
            migrationResults.add(updateErstellungsdatumForAnforderung(anforderung.get(), erstellungsdatum));
            migrationResults.add(updateAenderungsdatumForAnforderung(anforderung.get(), aenderungsdatum));
            migrationResults.add(updateDatumStatusaenderungForAnforderung(anforderung.get(), datumStatusaenderung));
        } else if (meldung.isPresent()) {
            migrationResults.add(updateErstellungsdatumForMeldung(meldung.get(), erstellungsdatum));
            migrationResults.add(updateAenderungsdatumForMeldung(meldung.get(), aenderungsdatum));
            migrationResults.add(updateDatumStatusaenderungForMeldung(meldung.get(), datumStatusaenderung));
        } else {
            return new MigrationResultDto(fachId, false, "Keine Anforderung oder Meldung mit FachId gefunden.");
        }
        return mergeMigrationResults(migrationResults);
    }

    private static MigrationResultDto mergeMigrationResults(List<MigrationResultDto> migrationResults) {
        String message = migrationResults.stream().map(m -> m.getId() + ": " + m.getMessage()).distinct()
                .collect(Collectors.joining(", "));
        String ids = migrationResults.stream().map(m -> m.getId()).distinct()
                .collect(Collectors.joining(", "));
        boolean successful = migrationResults.stream().allMatch(m -> m.isSuccessful());

        return new MigrationResultDto(ids, successful, message);
    }

    private MigrationResultDto updateErstellungsdatumForAnforderung(Anforderung anforderung, Date erstellungsdatum) {
        if (!DateMigrationUtils.checkForValidInputData(anforderung)) {
            return new MigrationResultDto(anforderung.getFachId(), false, "Anforderung is null!");
        }
        if (!DateMigrationUtils.checkForValidInputData(erstellungsdatum)) {
            return new MigrationResultDto(anforderung.getFachId(), true, "Erstellungsdatum is null!");
        }
        if (DateMigrationUtils.checkForValidDateUpdate(anforderung.getErstellungsdatum(), erstellungsdatum)) {
            anforderung.setErstellungsdatum(erstellungsdatum);
            anforderungService.persistAnforderung(anforderung);
            return new MigrationResultDto(anforderung.getFachId(), true, "Update Erstellungsdatum erfolgreich");
        } else {
            return new MigrationResultDto(anforderung.getFachId(), true, "Update Erstellungsdatum nicht notwendig");
        }
    }

    private MigrationResultDto updateAenderungsdatumForAnforderung(Anforderung anforderung, Date aenderungsdatum) {
        if (!DateMigrationUtils.checkForValidInputData(anforderung)) {
            return new MigrationResultDto(anforderung.getFachId(), false, "Anforderung is null!");
        }
        if (!DateMigrationUtils.checkForValidInputData(aenderungsdatum)) {
            return new MigrationResultDto(anforderung.getFachId(), true, "Aenderungsdatum is null!");
        }
        if (DateMigrationUtils.checkForValidDateUpdate(anforderung.getAenderungsdatum(), aenderungsdatum)) {
            anforderung.setAenderungsdatum(aenderungsdatum);
            anforderungService.persistAnforderung(anforderung);
            return new MigrationResultDto(anforderung.getFachId(), true, "Update Aenderungsdatum erfolgreich");
        } else {
            return new MigrationResultDto(anforderung.getFachId(), true, "Update Aenderungsdatum nicht notwendig");
        }
    }

    private MigrationResultDto updateDatumStatusaenderungForAnforderung(Anforderung anforderung, Date statusaenderung) {
        if (!DateMigrationUtils.checkForValidInputData(anforderung)) {
            return new MigrationResultDto(anforderung.getFachId(), false, "Anforderung is null!");
        }
        if (!DateMigrationUtils.checkForValidInputData(statusaenderung)) {
            return new MigrationResultDto(anforderung.getFachId(), true, "DatumStatusaenderung is null!");
        }
        if (DateMigrationUtils.checkForValidDateUpdate(anforderung.getZeitpunktStatusaenderung(), statusaenderung)) {
            anforderung.setZeitpunktStatusaenderung(statusaenderung);
            anforderungService.persistAnforderung(anforderung);
            return new MigrationResultDto(anforderung.getFachId(), true, "Update DatumStatusaenderung erfolgreich");
        } else {
            return new MigrationResultDto(anforderung.getFachId(), true, "Update DatumStatusaenderung nicht notwendig");
        }
    }

    private MigrationResultDto updateErstellungsdatumForMeldung(Meldung meldung, Date erstellungsdatum) {
        if (!DateMigrationUtils.checkForValidInputData(meldung)) {
            return new MigrationResultDto(meldung.getFachId(), false, "Meldung is null!");
        }
        if (!DateMigrationUtils.checkForValidInputData(erstellungsdatum)) {
            return new MigrationResultDto(meldung.getFachId(), true, "Erstellungsdatum is null!");
        }
        if (DateMigrationUtils.checkForValidDateUpdate(meldung.getErstellungsdatum(), erstellungsdatum)) {
            meldung.setErstellungsdatum(erstellungsdatum);
            anforderungService.persistMeldung(meldung);
            return new MigrationResultDto(meldung.getFachId(), true, "Update Erstellungsdatum erfolgreich");
        } else {
            return new MigrationResultDto(meldung.getFachId(), true, "Update Erstellungsdatum nicht notwendig");
        }
    }

    private MigrationResultDto updateAenderungsdatumForMeldung(Meldung meldung, Date aenderungsdatum) {
        if (!DateMigrationUtils.checkForValidInputData(meldung)) {
            return new MigrationResultDto(meldung.getFachId(), false, "Meldung is null!");
        }
        if (!DateMigrationUtils.checkForValidInputData(aenderungsdatum)) {
            return new MigrationResultDto(meldung.getFachId(), true, "Aenderungsdatum is null!");
        }
        if (DateMigrationUtils.checkForValidDateUpdate(meldung.getAenderungsdatum(), aenderungsdatum)) {
            meldung.setAenderungsdatum(aenderungsdatum);
            anforderungService.persistMeldung(meldung);
            return new MigrationResultDto(meldung.getFachId(), true, "Update Aenderungsdatum erfolgreich");
        } else {
            return new MigrationResultDto(meldung.getFachId(), true, "Update Aenderungsdatum nicht notwendig");
        }
    }

    private MigrationResultDto updateDatumStatusaenderungForMeldung(Meldung meldung, Date statusaenderung) {
        if (!DateMigrationUtils.checkForValidInputData(meldung)) {
            return new MigrationResultDto(meldung.getFachId(), false, "Meldung is null!");
        }
        if (!DateMigrationUtils.checkForValidInputData(statusaenderung)) {
            return new MigrationResultDto(meldung.getFachId(), true, "Aenderungsdatum is null!");
        }
        if (DateMigrationUtils.checkForValidDateUpdate(meldung.getZeitpunktStatusaenderung(), statusaenderung)) {
            meldung.setZeitpunktStatusaenderung(statusaenderung);
            anforderungService.persistMeldung(meldung);
            return new MigrationResultDto(meldung.getFachId(), true, "Update DatumStatusaenderung erfolgreich");
        } else {
            return new MigrationResultDto(meldung.getFachId(), true, "Update DatumStatusaenderung nicht notwendig");
        }
    }

    //--------------------------------Werk Migration------------------------------------------------
    @SuppressWarnings("checkstyle:NestedIfDepth")
    public void migrationOldAnforderungAndMeldungAddWerkFromExcelSheet(Sheet sheet) {

        logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "Start der Werk Datenmigration...", MigrationService.class.getName());

        int i = 0;
        int werkUpdated = 0;
        int werkInExcelNotUpdated = 0;
        int anforderungUpdated = 0;
        int anfoderungInExcelNotUpdated = 0;
        List<Meldung> meldungFromExcel = new ArrayList<>();
        List<Anforderung> anforderungFromExcel = new ArrayList<>();

        for (Integer rowNumber = sheet.getFirstRowNum() + 1; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            Row row = sheet.getRow(rowNumber);
            i++;
            if (row.getPhysicalNumberOfCells() == 4) {

                String meldungId = ExcelUtils.cellToString(row.getCell(0));
                String anforderungId = ExcelUtils.cellToString(row.getCell(1));
                String werkName = ExcelUtils.cellToString(row.getCell(3));

                if (!werkName.isEmpty() && !"".equals(werkName)) {
                    Werk werkToSave = null;
                    List<Werk> werkToSaveList = werkService.getWerkByName(werkName.trim());
                    if (werkToSaveList != null && !werkToSaveList.isEmpty()) {
                        werkToSave = werkToSaveList.get(0);
                    }
                    if (werkToSave == null) {
                        werkToSave = new Werk(werkName.trim());
                        werkService.persistOnlyNotExistingWerk(werkToSave);
                    }

                    if (!meldungId.isEmpty() && !"".equals(meldungId)) {
                        logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "Start werkMigration for Meldung: " + meldungId, MigrationService.class.getName());
                        List<Meldung> meldungen = anforderungService.getMeldungByFachIdStartingWith(meldungId);
                        if (!meldungen.isEmpty()) {
                            werkUpdated = werkUpdateForAllMeldungen(meldungen, werkUpdated, werkToSave, meldungFromExcel, meldungId);
                        } else {
                            logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "The Meldung with the ID: " + meldungId + " was not found in the Database", MigrationService.class.getName());
                            werkInExcelNotUpdated++;
                        }
                    }
                    if (!anforderungId.isEmpty() && !"".equals(anforderungId)) {

                        String[] anfoIdVersion = anforderungId.split("\\|");
                        Anforderung anforderungToSave;

                        Integer version = parseVersionId(anfoIdVersion[1]);
                        if (version != null) {
                            logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "Start werkMigration for Anforderung:" + anforderungId, MigrationService.class.getName());
                            anforderungToSave = anforderungService.getAnforderungByFachIdVersion(anfoIdVersion[0].trim(), version);
                            if (anforderungToSave != null) {
                                anforderungUpdated = setWerkToAnforderungAndSave(anforderungToSave, werkToSave, anforderungFromExcel, anforderungId, anforderungUpdated);
                            } else {
                                logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "A Anforderung with the ID: " + anforderungId + " was not found in the Database", MigrationService.class.getName());
                                anfoderungInExcelNotUpdated++;
                            }
                        }

                    }
                } else {
                    logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "The Name of The Werk was empty in line " + i, MigrationService.class.getName());
                }
            } else {
                logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "Row has not the length 4 " + i, MigrationService.class.getName());
            }
        }

        logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, werkUpdated + " Meldungen wurden mit einem Werk migriert.", MigrationService.class.getName());
        logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, werkInExcelNotUpdated + " Meldungen wurden nicht in der Datenbank gefunden.", MigrationService.class.getName());
        logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, anforderungUpdated + " Anforderungen wurden mit einem Werk migriert.", MigrationService.class.getName());
        logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, anfoderungInExcelNotUpdated + " Anforderungen wurden nicht in der Datenbank gefunden.", MigrationService.class.getName());

        //eventuell als Quirry, ist evtl deutlich Performanter
        checkMeldungUAnforderungWithExcelUDB(meldungFromExcel, anforderungFromExcel);

    }

    private int werkUpdateForAllMeldungen(List<Meldung> meldungen, int werkUpdated, Werk werkToSave, List<Meldung> meldungFromExcel, String meldungId) {
        for (Meldung meldungToSave : meldungen) {
            if (meldungToSave.getWerk() == null) {
                werkUpdated = setWerkToMeldungAndSave(meldungToSave, werkToSave, meldungFromExcel, meldungId, werkUpdated);
            } else {
                logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "The Meldung with the ID: " + meldungId + " has allready a Werk", MigrationService.class.getName());

            }
        }
        return werkUpdated;
    }

    private int setWerkToMeldungAndSave(Meldung meldungToSave, Werk werkToSave, List<Meldung> meldungFromExcel, String meldungId, int werkUpdated) {
        meldungToSave.setWerk(werkToSave);
        meldungFromExcel.add(meldungToSave);
        Long returnId = anforderungService.saveMeldung(meldungToSave);
        if (returnId != null) {
            logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "Meldung " + meldungId + " was saved", MigrationService.class.getName());
            werkUpdated++;
        }
        return werkUpdated;
    }

    private int setWerkToAnforderungAndSave(Anforderung anforderungToSave, Werk werkToSave, List<Anforderung> anforderungFromExcel, String anforderungId, int anforderungUpdated) {
        anforderungToSave.setWerk(werkToSave);
        anforderungFromExcel.add(anforderungToSave);
        Long returnId = anforderungService.saveAnforderung(anforderungToSave);
        if (returnId != null) {
            logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "Anforderung " + anforderungId + " was saved", MigrationService.class.getName());
            anforderungUpdated++;
        }
        return anforderungUpdated;
    }

    private void checkMeldungUAnforderungWithExcelUDB(List<Meldung> meldungFromExcel, List<Anforderung> anforderungFromExcel) {
        List<Anforderung> anforderungFromDB = anforderungService.getAllAnforderung();
        List<Meldung> meldungFromDB = anforderungService.getAllMeldungen();

        for (Anforderung anforderungNotUpdated : anforderungFromDB) {
            if (!anforderungFromExcel.contains(anforderungNotUpdated)) {
                logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "The Anforderung " + anforderungNotUpdated.getFachId() + " | V" + anforderungNotUpdated.getVersion() + " was not in the WerkMigration Excel List", MigrationService.class.getName());
            }
        }
        for (Meldung meldungNotUpdated : meldungFromDB) {
            if (!meldungFromExcel.contains(meldungNotUpdated)) {
                logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "The Meldung " + meldungNotUpdated.getFachId() + " was not in the WerkMigration Excel List", MigrationService.class.getName());
            }
        }
    }

    private Integer parseVersionId(String versionId) {

        if (!"".equals(versionId)) {
            try {
                String number = versionId.trim().substring(1);
                return Integer.parseInt(number);

            } catch (NumberFormatException exception) {
                logService.logWithLogEntry(LogLevel.SEVERE, SystemType.MIGRATION, "Parse Version for Anforderung failed", MigrationService.class.getName(), exception);

                return null;
            }
        } else {
            return null;
        }
    }

}
