package de.interfaceag.bmw.pzbk.migration;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author sl
 */
@Stateless
@Named
public class AnforderungMeldungMigrationService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungMeldungMigrationService.class);

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager em;

    @Inject
    private AnforderungService anforderungService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private AnforderungMeldungHistoryService historyService;

    private List<Anforderung> getAllAnforderungenWithoutMeldung() {
        String query = "SELECT DISTINCT a FROM Anforderung a WHERE a.meldungen IS EMPTY";
        TypedQuery<Anforderung> typedQuery = em.createQuery(query, Anforderung.class);
        return typedQuery.getResultList();
    }

    private List<Meldung> findMeldungForAnforderungBeschreibung(String beschreibungDe, String beschreibungEn) {
        String query = "SELECT DISTINCT m FROM Meldung m "
                + "WHERE m.beschreibungAnforderungDe = :beschreibungDe "
                + "OR m.beschreibungAnforderungEn = :beschreibungDe "
                + "OR m.beschreibungAnforderungEn = :beschreibungEn "
                + "OR m.beschreibungAnforderungDe = :beschreibungEn ";
        TypedQuery<Meldung> typedQuery = em.createQuery(query, Meldung.class)
                .setParameter("beschreibungDe", beschreibungDe)
                .setParameter("beschreibungEn", beschreibungEn);
        return typedQuery.getResultList();
    }

    public String linkMeldungWithAnforderung(String meldungId, String anforderungId) {
        String regex = "\\d+";
        if (meldungId.matches(regex) && anforderungId.matches(regex)) {
            long aid = Long.parseLong(anforderungId);
            long mid = Long.parseLong(meldungId);

            Meldung meldung = anforderungService.getMeldungById(mid);
            Anforderung anforderung = anforderungService.getAnforderungById(aid);

            return linkMeldungWithAnforderung(meldung, anforderung);
        }
        return "Fehler: Input ist keine Nummer";
    }

    public String getAllAnforderungenWithoutMeldungAsString() {
        List<Anforderung> allAnforderungenWithoutMeldung = getAllAnforderungenWithoutMeldung();
        return buildStringForAnforderungList(allAnforderungenWithoutMeldung);
    }

    public String getAllMatchingMeldungenForAnforderungAsString(String anforderungId) {
        String regex = "\\d+";
        if (anforderungId.matches(regex)) {
            long aid = Long.parseLong(anforderungId);
            Anforderung anforderung = anforderungService.getAnforderungById(aid);
            if (anforderung != null && anforderung.getBeschreibungAnforderungDe() != null
                    && anforderung.getBeschreibungAnforderungEn() != null) {
                List<Meldung> relevantMeldungen = findMeldungForAnforderungBeschreibung(
                        anforderung.getBeschreibungAnforderungDe(),
                        anforderung.getBeschreibungAnforderungEn()
                );
                return buildStringForMeldungList(relevantMeldungen);
            }
            return "Fehler: Anforderung oder Beschreibung is null";
        }
        return "Fehler: Input ist keine Nummer";

    }

    private static String buildStringForAnforderungList(List<Anforderung> anforderungen) {
        StringBuilder sb = new StringBuilder();
        anforderungen.stream().filter(Objects::nonNull)
                .forEach(m -> sb.append(m.getFachId())
                        .append(" (id=")
                        .append(m.getId())
                        .append("); "));
        return sb.toString();
    }

    private static String buildStringForMeldungList(List<Meldung> meldungen) {
        StringBuilder sb = new StringBuilder();
        meldungen.stream().filter(Objects::nonNull)
                .forEach(m -> sb.append(m.getFachId())
                        .append(" (id=")
                        .append(m.getId())
                        .append("); "));
        return sb.toString();
    }

    public void linkMeldungenWithAnforderungenWithoutMeldungen() {
        List<Anforderung> relevantAnforderungen = getAllAnforderungenWithoutMeldung();
        LOG.info("Gefundene Anforderungen ohne Meldungen: {}", buildStringForAnforderungList(relevantAnforderungen));

        for (Anforderung a : relevantAnforderungen) {
            LOG.info("Search matching Meldungen for Anforderung {} (id={}).", a.getFachId(), a.getId());

            List<Meldung> relevantMeldungen = findMeldungForAnforderungBeschreibung(
                    a.getBeschreibungAnforderungDe(), a.getBeschreibungAnforderungEn());

            if (relevantMeldungen.size() == 1) {
                Meldung meldung = relevantMeldungen.get(0);
                if (meldung != null) {
                    LOG.info("Passende Meldung {} (id={}) gefunden.", meldung.getFachId(), meldung.getId());
                    linkMeldungWithAnforderung(meldung, a);
                } else {
                    LOG.warn("Passende Meldung ist null");
                }
            } else if (relevantMeldungen.isEmpty()) {
                LOG.info("Keine passende Meldung f\u00fcr Anforderung {} (id={}) gefunden.", a.getFachId(), a.getId());
            } else if (relevantMeldungen.size() > 1) {
                LOG.info("Keine eindeutige Meldung f\u00fcr Anforderung {} (id={}) gefunden. "
                        + "Passenden Meldungen {}", a.getFachId(), a.getId(), buildStringForMeldungList(relevantMeldungen));
            }
        }

    }

    private String linkMeldungWithAnforderung(Meldung meldung, Anforderung anforderung) {
        String message;
        if (meldung != null && anforderung != null) {
            LOG.warn("linkMeldungWithAnforderung: Verlinke Meldung {} mit Anforderung {}", meldung.getFachId(), anforderung.getFachId());

            anforderung.addMeldung(meldung);
            anforderungService.persistAnforderung(anforderung);

            berechtigungService
                    .getSensorCoCLeiterOfSensorCoc(meldung.getSensorCoc())
                    .ifPresent(m -> {
                        LOG.info("SensorCocLeiter {} gefunden.", m.toString());
                        historyService.writeHistoryForNewAnfoMgmtObject(m, anforderung);
                    });

            meldung.setAssignedToAnforderung(Boolean.TRUE);
            meldung.getAnforderung().add(anforderung);
            meldung.setStatus(Status.M_ZUGEORDNET);
            meldung.setAenderungsdatum(anforderung.getErstellungsdatum());
            anforderungService.persistMeldung(meldung);

            message = "linkMeldungWithAnforderung: Erfolgreich";
            LOG.warn("linkMeldungWithAnforderung: Erfolgreich");
        } else {
            message = "linkMeldungWithAnforderung: Meldung oder Anforderung ist null";
            LOG.warn(message);
        }
        return message;
    }

}
