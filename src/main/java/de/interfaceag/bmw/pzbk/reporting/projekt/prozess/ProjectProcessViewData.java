package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureValidationResult;

import java.io.Serializable;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

public final class ProjectProcessViewData implements ProjectProcessKeyFigureAccessController, Serializable {

    private final ProjectProcessFilter filter;

    private final ProjectReportingProcessKeyFigureDataCollection keyFigures;

    private final Derivat selectedDerivat;

    private final DerivatStatusNames derivatStatusNames;

    private final Collection<KeyFigureValidationResult> validationResults;

    private final String validationHeaderMessage;

    private final Date date;

    private ProjectProcessViewData(ProjectProcessFilter filter, ProjectReportingProcessKeyFigureDataCollection keyFigures,
            Derivat selectedDerivat, DerivatStatusNames derivatStatusNames, Collection<KeyFigureValidationResult> validationResults,
            String validationHeaderMessage, Date date) {
        this.filter = filter;
        this.keyFigures = keyFigures;
        this.selectedDerivat = selectedDerivat;
        this.derivatStatusNames = derivatStatusNames;
        this.validationResults = validationResults;
        this.validationHeaderMessage = validationHeaderMessage;
        this.date = date;
    }

    public ProjectProcessFilter getFilter() {
        return filter;
    }

    @Override
    public Long getValueForKeyFigureId(Integer keyFigureId) {
        return keyFigures.getValueForKeyFigureId(keyFigureId);
    }

    @Override
    public boolean isAmpelGreenForKeyFigureId(Integer keyFigureId) {
        return keyFigures.isAmpelGreenForKeyFigureId(keyFigureId);
    }

    public Optional<Derivat> getSelectedDerivat() {
        return Optional.ofNullable(selectedDerivat);
    }

    public Optional<String> getSelectedDerivatStatus() {
        if (getSelectedDerivat().isPresent()) {
            DerivatStatus status = getSelectedDerivat().get().getStatus();
            String nameForStatus = derivatStatusNames.getNameForStatus(status);
            return Optional.of(nameForStatus);
        } else {
            return Optional.empty();
        }
    }

    public String getValidationMessage() {
        if (validationResults != null) {
            return validationResults.stream().map(validationResult -> validationResult.getValidationMessage()).collect(Collectors.joining("; "));
        } else {
            return "";
        }
    }

    public String getValidationHeaderMessage() {
        return validationHeaderMessage;
    }

    public boolean isValidationError() {
        return validationResults != null && !validationResults.isEmpty();
    }

    public boolean isSnapshot() {
        return getFilter().isSnapshot();
    }

    public static Builder withFilter(ProjectProcessFilter filter) {
        return new Builder().withFilter(filter);
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String getKovAPhaseStartDate(int phaseId) {
        if (isDerivatSelected()) {
            return getKovAPhaseStartDateByPhaseId(phaseId);
        }
        return "";
    }

    private String getKovAPhaseStartDateByPhaseId(int phaseId) {
        return selectedDerivat.getKovAPhasen().stream()
                .filter(result -> compareKovAPhaseId(result, phaseId))
                .map(this::getStartDateWithFormat)
                .findFirst()
                .orElse("");
    }

    private static boolean compareKovAPhaseId(KovAPhaseImDerivat element, int phaseId) {
        return element.getKovAPhase().getId() == phaseId;
    }

    private String getStartDateWithFormat(KovAPhaseImDerivat element) {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(Locale.GERMAN);
        if (element.getStartDate() != null) {
            return element.getStartDate().format(formatter);
        } else {
            return "";
        }
    }

    private boolean isDerivatSelected() {
        return selectedDerivat != null && !selectedDerivat.isInaktiv();
    }

    public static final class Builder {

        private ProjectProcessFilter filter;
        private ProjectReportingProcessKeyFigureDataCollection keyFigures;
        private Derivat selectedDerivat;
        private DerivatStatusNames derivatStatusNames;
        private Collection<KeyFigureValidationResult> validationResults;
        private String validationHeaderMesssage = "";
        private Date date;

        private Builder() {
        }

        private Builder withFilter(ProjectProcessFilter filter) {
            this.filter = filter;
            return this;
        }

        public Builder withSelectedDerivat(Derivat selectedDerivat) {
            this.selectedDerivat = selectedDerivat;
            return this;
        }

        public Builder withDerivatStatusNames(DerivatStatusNames derivatStatusNames) {
            this.derivatStatusNames = derivatStatusNames;
            return this;
        }

        public Builder withKeyFigures(ProjectReportingProcessKeyFigureDataCollection keyFigures) {
            this.keyFigures = keyFigures;
            return this;
        }

        public Builder withValidationResults(Collection<KeyFigureValidationResult> validationResults) {
            this.validationResults = validationResults;
            return this;
        }

        public Builder withValidationHeaderMessage(String validationHeaderMessage) {
            this.validationHeaderMesssage = validationHeaderMessage;
            return this;
        }

        public Builder withDate(Date date) {
            this.date = date;
            return this;
        }

        public ProjectProcessViewData get() {
            return new ProjectProcessViewData(filter, keyFigures, selectedDerivat, derivatStatusNames, validationResults, validationHeaderMesssage, date);
        }

    }

}
