package de.interfaceag.bmw.pzbk.reporting.config;


import de.interfaceag.bmw.pzbk.entities.ReportingConfig;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;


@Dependent
public class ReportingConfigDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;


    ReportingConfig getConfigByKeyword(String keyword) {
        if (verifyKeywordIsSet(keyword)) {
            Query q = entityManager.createQuery("SELECT r.keyword, r.value FROM ReportingConfig r WHERE r.keyword= :keyword", ReportingConfig.class);

            q.setParameter("keyword", keyword);
            List<Object[]> result = q.getResultList();

            if ( result.size() == 1) {
                Object[] element = result.get(0);
                return new ReportingConfig((String) element[0], (String) element[1]);
            }

            return null;
        }
        return null;
    }

    void persistReportingConfig(ReportingConfig config) {
        if (verifyKeywordIsSet(config.getKeyword())) {
            Query q = entityManager.createQuery("UPDATE ReportingConfig r SET r.value=:value WHERE r.keyword= :keyword", ReportingConfig.class);
            q.setParameter("value", config.getValue());
            q.setParameter("keyword", config.getKeyword());

            q.executeUpdate();
        }
    }

    private boolean verifyKeywordIsSet(String keyword) {
        return keyword != null && !keyword.isEmpty();
    }

    // Only used for Integrationstests!!
    void initReportingConfigData() {
        entityManager.createNativeQuery("INSERT INTO reportingconfig(id, keyword, value) VALUES (1, 'start_date', '01.02.2018')").executeUpdate();

        entityManager.createNativeQuery("INSERT INTO reportingconfig(id, keyword, value) VALUES (2, 'langlaufer_fachteam', '50')").executeUpdate();

        entityManager.createNativeQuery("INSERT INTO reportingconfig(id, keyword, value) VALUES (3, 'langlaufer_tteam', '50')").executeUpdate();

    }
}
