package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.shared.math.Sum;

import java.util.Collection;
import java.util.stream.Stream;

public final class FachteamLanglauferKeyFigureSumCalculator {

    private FachteamLanglauferKeyFigureSumCalculator() {
    }

    public static int getLanglauferSum(Collection<FachteamKeyFigure> keyFigures) {
        final Stream<Integer> langlauferCounts = keyFigures.stream().map(FachteamKeyFigure::getLanglauferCount);
        return Sum.computeSum(langlauferCounts);
    }

}
