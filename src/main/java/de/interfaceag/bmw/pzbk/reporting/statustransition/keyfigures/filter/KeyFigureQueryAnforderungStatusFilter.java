package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.filter.AnforderungStatusFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;

import java.util.List;
import java.util.Objects;

public final class KeyFigureQueryAnforderungStatusFilter {

    private KeyFigureQueryAnforderungStatusFilter() {

    }

    public static void append(ReportingFilter filter, QueryPart queryPart, Type type) {
        final AnforderungStatusFilter anforderungStatusFilter = filter.getAnforderungStatusFilter();

        if (Objects.nonNull(anforderungStatusFilter) && anforderungStatusFilter.isActive()) {
            if (type.isAnforderung()) {
                final List<Status> anforderungStatus = anforderungStatusFilter.getAsEnumList();

                queryPart.append(" AND a.status IN :anforderungStatus ");
                queryPart.put("anforderungStatus", anforderungStatus);
            } else {
                queryPart.append(" AND 1 = 0 ");
            }
        }
    }

}
