package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;

@Stateless
public class ReportingMeldungStatusTransitionService implements Serializable, MeldungReportingStatusTransitionAdapter, MeldungReportingStatusTransitionAdapterForMigration {

    public static final Logger LOG = LoggerFactory.getLogger(ReportingMeldungStatusTransitionService.class);

    @Inject
    private ReportingMeldungStatusTransitionZuordnungToAnforderungService meldungStatusTransitionZuordnungToAnforderungService;
    @Inject
    private ReportingMeldungStatusTransitionZuordnungToAnforderungAufhebenService meldungStatusTransitionZuordnungToAnforderungAufhebenService;
    @Inject
    private ReportingStatusTransitionChangeMeldungStatusService changeMeldungStatusService;

    @Inject
    private Date currentDate;

    @Override
    public ReportingMeldungStatusTransition changeMeldungStatus(Meldung meldung, Status currentStatus, Status newStatus) {
        try {
            return changeMeldungStatusService.saveStatusChange(meldung, currentStatus, newStatus, currentDate);
        } catch (IllegalArgumentException exception) {
            LOG.warn("ReportingMeldungStatusTransitionService.changeMeldungStatus", exception);
        }
        return null;
    }

    @Override
    public ReportingMeldungStatusTransition addMeldungToNewAnforderung(Meldung meldung) {
        try {
            return meldungStatusTransitionZuordnungToAnforderungService.addMeldungToNewAnforderung(meldung, currentDate);
        } catch (IllegalArgumentException exception) {
            LOG.warn("ReportingMeldungStatusTransitionService.addMeldungToNewAnforderung", exception);
        }
        return null;
    }

    @Override
    public ReportingMeldungStatusTransition addMeldungToExistingAnforderung(Meldung meldung) {
        try {
            return meldungStatusTransitionZuordnungToAnforderungService.addMeldungToExistingAnforderung(meldung, currentDate);
        } catch (IllegalArgumentException exception) {
            LOG.warn("ReportingMeldungStatusTransitionService.addMeldungToExistingAnforderung", exception);
        }
        return null;
    }

    @Override
    public ReportingMeldungStatusTransition removeMeldungFromAnforderung(Meldung meldung, Anforderung anforderung) {
        try {
            return meldungStatusTransitionZuordnungToAnforderungAufhebenService.removeMeldungFromAnforderung(meldung, anforderung, currentDate);
        } catch (IllegalArgumentException exception) {
            LOG.warn("ReportingMeldungStatusTransitionService.removeMeldungFromAnforderung", exception);
        }
        return null;
    }

    @Override
    public ReportingMeldungStatusTransition changeMeldungStatus(Meldung meldung, Status currentStatus, Status newStatus, Date date) {
        try {
            return changeMeldungStatusService.saveStatusChange(meldung, currentStatus, newStatus, date);
        } catch (IllegalArgumentException exception) {
            LOG.warn("ReportingMeldungStatusTransitionService.changeMeldungStatus", exception);
        }
        return null;
    }

    @Override
    public ReportingMeldungStatusTransition addMeldungToNewAnforderung(Meldung meldung, Date date) {
        try {
            return meldungStatusTransitionZuordnungToAnforderungService.addMeldungToNewAnforderung(meldung, date);
        } catch (IllegalArgumentException exception) {
            LOG.warn("ReportingMeldungStatusTransitionService.addMeldungToNewAnforderung", exception);
        }
        return null;
    }

    @Override
    public ReportingMeldungStatusTransition addMeldungToExistingAnforderung(Meldung meldung, Date date) {
        try {
            return meldungStatusTransitionZuordnungToAnforderungService.addMeldungToExistingAnforderung(meldung, date);
        } catch (IllegalArgumentException exception) {
            LOG.warn("ReportingMeldungStatusTransitionService.addMeldungToExistingAnforderung", exception);
        }
        return null;
    }

    @Override
    public ReportingMeldungStatusTransition removeMeldungFromAnforderung(Meldung meldung, Date date) {
        try {
            return meldungStatusTransitionZuordnungToAnforderungAufhebenService.removeMeldungFromAnforderung(meldung, null, date);
        } catch (IllegalArgumentException exception) {
            LOG.warn("ReportingMeldungStatusTransitionService.removeMeldungFromAnforderung", exception);
        }
        return null;
    }
}
