package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ViewScoped
@Named
public class ReportingStatusabgleichController implements Serializable {

    private List<DrilldownTableAnforderungDTO> drilldownDialogData = new ArrayList<>();

    @Inject
    private ReportingStatusabgleichFacade facade;

    private ReportingStatusabgleichViewData viewData;

    @PostConstruct
    public void init() {
        initViewData();
    }

    private void initViewData() {
        this.viewData = facade.getViewData(UrlParameterUtils.getUrlParameter());
    }

    public ReportingStatusabgleichViewData getViewData() {
        return viewData;
    }

    public ReportingStatusabgleichFilter getFilter() {
        return getViewData().getFilter();
    }

    public List<DrilldownTableAnforderungDTO> getDrilldownDialogData() {
        return drilldownDialogData;
    }

    public String createNewSnapshot() {
        Optional<Derivat> derivat = viewData.getDerivat();
        if (derivat.isPresent()) {
            facade.createNewSnapshot(derivat.get());
        }
        return getFilter().filter();
    }

    public String getDerivatStatus() {
        Optional<Derivat> derivat = getViewData().getDerivat();
        if (isDerivatStatusPresent()) {
            DerivatStatusNames derivatStatusNames = getViewData().getDerivatStatusNames();
            DerivatStatus status = derivat.get().getStatus();
            return derivatStatusNames.getNameForStatus(status);
        } else {
            return "";
        }
    }

    public String getDerivatName() {
        if (isDerivatPresent()) {
            Optional<Derivat> derivat = getViewData().getDerivat();
            return derivat.get().getName();
        } else {
            return "";
        }
    }

    public boolean isDerivatStatusPresent() {
        DerivatStatusNames derivatStatusNames = getViewData().getDerivatStatusNames();
        return isDerivatPresent() && derivatStatusNames != null;
    }

    public boolean isDerivatPresent() {
        return getViewData().getDerivat().isPresent();
    }

    public List<DrilldownTableAnforderungDTO> showAnforderungsuebersichtForPosition(int row, int col) {
        Optional<Derivat> derivat = getViewData().getDerivat();
        if (derivat.isPresent()) {
            return facade.showAnforderungsuebersichtForPosition(derivat.get(), getViewData().getFilter(), row, col);
        } else {
            return Collections.emptyList();
        }
    }

    public void openDialog(int row, int col) {
        drilldownDialogData = showAnforderungsuebersichtForPosition(row, col);
        if (drilldownDialogData.size() > 0) {
            DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("reportingDialogs:anforderungsuebersichtForm:statusabgleichDrilldownDialog:anforderungsuebersichtTable");
            dataTable.reset();
            RequestContext.getCurrentInstance().update("reportingDialogs:anforderungsuebersichtForm:statusabgleichDrilldownDialog:anforderungsuebersichtTable");
            RequestContext.getCurrentInstance().execute("PF('anforderungsuebersichtDialog').show()");
        }
    }

    public void downloadExcelExport() {
        String derivatName = getDerivatName();
        facade.downloadExcelExport(derivatName, drilldownDialogData);
    }

    public boolean shouldExportBeDisabled() {
        return drilldownDialogData.isEmpty();
    }

}
