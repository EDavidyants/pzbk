package de.interfaceag.bmw.pzbk.reporting.statustransition.migration;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author fn
 */
public class AnforderungHistoryMigrationDto implements Serializable {

    private final long anforderungId;

    private final Date datum;

    private final String objektName;

    private final String von;

    private final String auf;

    public AnforderungHistoryMigrationDto(long anforderungId, Date datum, String objektName, String von, String auf) {
        this.anforderungId = anforderungId;
        this.datum = datum;
        this.objektName = objektName;
        this.von = von;
        this.auf = auf;
    }

    @Override
    public String toString() {
        return "AnforderungHistoryMigrationDto{" + "anforderungId=" + anforderungId + ", datum=" + datum + ", objektName=" + objektName + ", von=" + von + ", auf=" + auf + ", action=" + getAction() + '}';
    }

    public AnforderungHistoryAction getAction() {
        return AnforderungHistoryActionConverter.convert(objektName, von, auf);
    }

    public long getAnforderungId() {
        return anforderungId;
    }

    public Date getDatum() {
        return datum;
    }

    public String getObjektName() {
        return objektName;
    }

    public String getVon() {
        return von;
    }

    public String getAuf() {
        return auf;
    }

}
