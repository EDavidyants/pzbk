package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

@Dependent
public class ReportingStatusTransitionRestoreAnforderungService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(ReportingStatusTransitionRestoreAnforderungService.class);

    @Inject
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Inject
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;

    public ReportingStatusTransition restoreAnforderung(Anforderung anforderung, Date date) {

        LOG.debug("Write ReportingStatusTransition for restoration of Anforderung {}", anforderung);

        final Optional<ReportingStatusTransition> latestStatusTransitionForAnforderung = statusTransitionDatabaseAdapter.getLatestForAnforderung(anforderung);

        final ReportingStatusTransition statusTransition;
        if (latestStatusTransitionForAnforderung.isPresent()) {
            LOG.debug("Found latest status transition and update");
            statusTransition = updateReportingStatusTransition(latestStatusTransitionForAnforderung.get(), date);
            statusTransitionDatabaseAdapter.save(statusTransition);
            return statusTransition;
        } else {
            LOG.debug("Found no status transition --> do nothing!");
            return null;
        }

    }

    private ReportingStatusTransition updateReportingStatusTransition(ReportingStatusTransition reportingStatusTransition, Date date) {
        clearStatusTransition(reportingStatusTransition);
        updateEntryForInArbeit(reportingStatusTransition, date);
        return reportingStatusTransition;
    }

    private void updateEntryForInArbeit(ReportingStatusTransition reportingStatusTransition, Date date) {
        final EntryExitTimeStamp inArbeit = reportingStatusTransition.getInArbeit();
        reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(inArbeit, date);
    }

    private void clearStatusTransition(ReportingStatusTransition reportingStatusTransition) {
        clearStatusValues(reportingStatusTransition);
        clearUnstimmig(reportingStatusTransition);
        clearGeloescht(reportingStatusTransition);
        clearProzessbaukasten(reportingStatusTransition);
    }

    private void clearStatusValues(ReportingStatusTransition reportingStatusTransition) {
        clearInArbeit(reportingStatusTransition);
        clearAbgestimmt(reportingStatusTransition);
        clearPlausibilsiert(reportingStatusTransition);
        clearFreigegeben(reportingStatusTransition);
        clearKeineWeiterverfolgung(reportingStatusTransition);
    }

    private void clearProzessbaukasten(ReportingStatusTransition reportingStatusTransition) {
        clearFachteamProzessbaukasten(reportingStatusTransition);
        clearTteamProzessbaukasten(reportingStatusTransition);
        clearVereinbarungProzessbaukasten(reportingStatusTransition);
        clearFreigegebenProzessbaukasten(reportingStatusTransition);
    }

    private void clearFreigegebenProzessbaukasten(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp freigegebenProzessbaukasten = reportingStatusTransition.getFreigegebenProzessbaukasten();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(freigegebenProzessbaukasten);
    }

    private void clearVereinbarungProzessbaukasten(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp vereinbarungProzessbaukasten = reportingStatusTransition.getVereinbarungProzessbaukasten();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(vereinbarungProzessbaukasten);
    }

    private void clearTteamProzessbaukasten(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp tteamProzessbaukasten = reportingStatusTransition.getTteamProzessbaukasten();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(tteamProzessbaukasten);
    }

    private void clearFachteamProzessbaukasten(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp fachteamProzessbaukasten = reportingStatusTransition.getFachteamProzessbaukasten();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(fachteamProzessbaukasten);
    }

    private void clearGeloescht(ReportingStatusTransition reportingStatusTransition) {
        clearFachteamGeloescht(reportingStatusTransition);
        clearTteamGeloescht(reportingStatusTransition);
        clearVereinbarungGeloescht(reportingStatusTransition);
        clearFreigegebenGeloescht(reportingStatusTransition);
    }

    private void clearFreigegebenGeloescht(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp freigegebenGeloescht = reportingStatusTransition.getFreigegebenGeloescht();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(freigegebenGeloescht);
    }

    private void clearVereinbarungGeloescht(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp vereinbarungGeloescht = reportingStatusTransition.getVereinbarungGeloescht();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(vereinbarungGeloescht);
    }

    private void clearTteamGeloescht(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp tteamGeloescht = reportingStatusTransition.getTteamGeloescht();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(tteamGeloescht);

    }

    private void clearFachteamGeloescht(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp fachteamGeloescht = reportingStatusTransition.getFachteamGeloescht();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(fachteamGeloescht);
    }

    private void clearUnstimmig(ReportingStatusTransition reportingStatusTransition) {
        clearFachteamUnstimmig(reportingStatusTransition);
        clearTteamUnstimmig(reportingStatusTransition);
        clearVereinbarungUnstimmig(reportingStatusTransition);
    }

    private void clearVereinbarungUnstimmig(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp vereinbarungUnstimmig = reportingStatusTransition.getVereinbarungUnstimmig();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(vereinbarungUnstimmig);
    }

    private void clearTteamUnstimmig(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp tteamUnstimmig = reportingStatusTransition.getTteamUnstimmig();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(tteamUnstimmig);
    }

    private void clearFachteamUnstimmig(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp fachteamUnstimmig = reportingStatusTransition.getFachteamUnstimmig();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(fachteamUnstimmig);
    }

    private void clearKeineWeiterverfolgung(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp keineWeiterverfolgung = reportingStatusTransition.getKeineWeiterverfolgung();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(keineWeiterverfolgung);
    }

    private void clearFreigegeben(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp freigegeben = reportingStatusTransition.getFreigegeben();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(freigegeben);
    }

    private void clearPlausibilsiert(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp plausibilisiert = reportingStatusTransition.getPlausibilisiert();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(plausibilisiert);
    }

    private void clearAbgestimmt(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp abgestimmt = reportingStatusTransition.getAbgestimmt();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(abgestimmt);
    }

    private void clearInArbeit(ReportingStatusTransition reportingStatusTransition) {
        final EntryExitTimeStamp inArbeit = reportingStatusTransition.getInArbeit();
        reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(inArbeit);
    }

}
