package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface DurchlaufzeitKeyFigure extends Serializable {

    KeyType getKeyType();

    int getKeyTypeId();

    int getValue();

}
