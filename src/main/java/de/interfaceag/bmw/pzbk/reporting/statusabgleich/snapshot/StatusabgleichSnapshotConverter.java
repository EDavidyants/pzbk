package de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.StatusabgleichKennzahlen;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sl
 */
public final class StatusabgleichSnapshotConverter {

    private StatusabgleichSnapshotConverter() {
    }

    public static StatusabgleichKennzahlen convertToKennzahlen(StatusabgleichSnapshot snapshot) {
        Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> matrixValues = convertToMatrixValues(snapshot);
        return new StatusabgleichKennzahlen(matrixValues);
    }

    public static StatusabgleichSnapshot convertToSnapshot(StatusabgleichKennzahlen kennzahlen, Derivat derivat) {
        Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> matrixValues = kennzahlen.getMatrixValues();
        Iterator<Map.Entry<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long>> iterator = matrixValues.entrySet().iterator();

        StatusabgleichSnapshot snapshot = new StatusabgleichSnapshot(derivat);

        while (iterator.hasNext()) {
            Map.Entry<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> next = iterator.next();
            Long value = next.getValue();

            if (value > 0) {
                Tuple<DerivatAnforderungModulStatus, ZakStatus> key = next.getKey();
                DerivatAnforderungModulStatus vereinbarungStatus = key.getX();
                ZakStatus zakStatus = key.getY();
                int entry = value.intValue();
                StatusabgleichSnapshotEntry newEntry = new StatusabgleichSnapshotEntry(vereinbarungStatus, zakStatus, entry);
                snapshot.addEntry(newEntry);
            }
        }

        return snapshot;
    }

    private static Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> convertToMatrixValues(StatusabgleichSnapshot snapshot) {
        List<StatusabgleichSnapshotEntry> entries = snapshot.getEntries();

        Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> result = new HashMap<>();
        Iterator<StatusabgleichSnapshotEntry> iterator = entries.iterator();
        while (iterator.hasNext()) {
            StatusabgleichSnapshotEntry next = iterator.next();
            DerivatAnforderungModulStatus vereinbarungStatus = next.getVereinbarungStatus();
            ZakStatus zakStatus = next.getZakStatus();
            int entry = next.getEntry();

            Long value = (long) entry;
            result.put(new GenericTuple<>(vereinbarungStatus, zakStatus), value);
        }
        return result;
    }

}
