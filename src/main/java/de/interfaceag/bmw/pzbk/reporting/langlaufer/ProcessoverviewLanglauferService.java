package de.interfaceag.bmw.pzbk.reporting.langlaufer;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTupleCollection;

public interface ProcessoverviewLanglauferService {

    boolean isFachteamLanglaufer(IdTypeTupleCollection idTypeTuples);

    boolean isTteamLanglaufer(IdTypeTupleCollection idTypeTuples);

    boolean isVereinbarungLanglaufer(IdTypeTupleCollection idTypeTuples);

}
