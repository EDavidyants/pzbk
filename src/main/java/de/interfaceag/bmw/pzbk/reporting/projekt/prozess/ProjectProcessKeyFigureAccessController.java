package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

/**
 *
 * @author sl
 */
public interface ProjectProcessKeyFigureAccessController {

    Long getValueForKeyFigureId(Integer keyFigureId);

    boolean isSnapshot();

    String getKovAPhaseStartDate(int phaseId);

    boolean isAmpelGreenForKeyFigureId(Integer keyFigureId);

}
