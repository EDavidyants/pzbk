package de.interfaceag.bmw.pzbk.reporting;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface ReportingFilterController extends Serializable {

    String filter();

    String reset();

    ReportingFilter getFilter();

    ReportingFilterViewPermission getFilterViewPermission();

    String toggleTooltips();

    Date getMinDate();

    Date getMaxDate();

}
