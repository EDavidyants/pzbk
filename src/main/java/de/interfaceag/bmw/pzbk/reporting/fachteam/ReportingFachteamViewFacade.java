package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class ReportingFachteamViewFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ReportingFachteamViewFacade.class.getName());

    @Inject
    private ReportingFilterService reportingFilterService;
    @Inject
    private FachteamKeyFigureService fachteamKeyFigureService;

    @Produces
    public ReportingFachteamViewData getReportingViewData() {

        LOG.debug("Generate Reporting Fachteam Data");

        final UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();

        ReportingFilter filter = reportingFilterService.getReportingFilter(urlParameter, Page.REPORTING_FACHTEAM);

        FachteamKeyFigures fachteamKeyFigures = fachteamKeyFigureService.getFachteamKeyFigures(filter);

        return new ReportingFachteamViewData(fachteamKeyFigures, filter);

    }

}
