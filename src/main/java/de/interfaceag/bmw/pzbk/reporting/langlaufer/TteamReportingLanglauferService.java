package de.interfaceag.bmw.pzbk.reporting.langlaufer;

import de.interfaceag.bmw.pzbk.entities.Tteam;

public interface TteamReportingLanglauferService {

    int getTteamLanglauferCountByTteam(Tteam tteam);

    int getVereinbarungLanglauferCountByTteam(Tteam tteam);

}
