package de.interfaceag.bmw.pzbk.reporting.svg;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class LinkToExcelDownload implements SVGElement {

    private final KeyFigure keyFigure;

    public LinkToExcelDownload(KeyFigure keyFigure) {
        this.keyFigure = keyFigure;
    }

    @Override
    public String draw() {
        StringBuilder sb = new StringBuilder();

        if (keyFigure != null && keyFigure.getKeyType().isRelevantForDrilldownDialog()) {
            sb.append("<a class='pointer' onclick=\"$('.keyFigure").append(keyFigure.getKeyTypeId()).append("').click();\">");
            sb.append(keyFigure.getValue());
            sb.append("</ a>");
        } else if (keyFigure != null) {
            sb.append(keyFigure.getValue());
        }

        return sb.toString();
    }

}
