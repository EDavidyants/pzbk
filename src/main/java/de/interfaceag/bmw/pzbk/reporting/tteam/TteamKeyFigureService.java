package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;
import de.interfaceag.bmw.pzbk.filter.TteamFilter;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.langlaufer.TteamReportingLanglauferService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.StatusTransitionKeyFigureService;
import de.interfaceag.bmw.pzbk.services.TteamService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static de.interfaceag.bmw.pzbk.reporting.ReportingUtils.getSearchFilterObjectsForId;

/**
 * @author sl
 */
@Stateless
@Named
public class TteamKeyFigureService implements Serializable {

    @Inject
    private TteamService tteamService;
    @Inject
    private LocalizationService localizationService;
    @Inject
    private TteamReportingLanglauferService tteamReportingLanglauferService;
    @Inject
    private StatusTransitionKeyFigureService statusTransitionKeyFigureService;

    public TteamKeyFigures getTteamKeyFigures(ReportingFilter filter) {

        final Collection<Tteam> tteams = getTteamsForTteamFilter(filter);

        List<TteamKeyFigure> tteamKeyFigures = new ArrayList<>();

        for (Tteam tteam : tteams) {
            final TteamKeyFigure keyFiguresForTteam = getKeyFiguresForTteam(tteam, filter);
            tteamKeyFigures.add(keyFiguresForTteam);
        }

        addSumKeyFigure(tteamKeyFigures);

        return new TteamKeyFigures(tteamKeyFigures);
    }

    private void addSumKeyFigure(Collection<TteamKeyFigure> tteamKeyFigures) {
        final String label = localizationService.getValue("fachteam_detail_table_summe");
        tteamKeyFigures.add(TteamSumKeyFigureCalculator.computeSumKeyFigures(tteamKeyFigures, label));
    }

    private Collection<Tteam> getTteamsForTteamFilter(ReportingFilter filter) {
        final IdSearchFilter tteamFilter = filter.getTteamFilter();

        if (tteamFilter.isActive()) {
            List<Long> tteamIds = new ArrayList<>(tteamFilter.getSelectedIdsAsSet());
            return tteamService.getTteamByIdList(tteamIds);
        } else {
            return Collections.emptySet();
        }
    }

    private TteamKeyFigure getKeyFiguresForTteam(Tteam tteam, ReportingFilter filter) {
        IdSearchFilter tteamFilterCopyForReturn = new TteamFilter(filter.getTteamFilter());

        final IdSearchFilter tteamFilter = filter.getTteamFilter();
        final Set<SearchFilterObject> searchFilterObjectsForId = getSearchFilterObjectsForId(tteamFilter, tteam.getId());
        tteamFilter.setSelectedValues(searchFilterObjectsForId);

        final KeyFigureCollection keyFigures = statusTransitionKeyFigureService.getKeyFiguresForTteamReporting(filter);
        final DurchlaufzeitKeyFigureCollection durchlaufzeitKeyFigures = statusTransitionKeyFigureService.computeDurchlaufzeitKeyFigures(filter);
        final DurchlaufzeitKeyFigure tteamDurchlaufzeitKeyFigure = durchlaufzeitKeyFigures.getByKeyType(KeyType.DURCHLAUFZEIT_TTEAM);
        final DurchlaufzeitKeyFigure vereinbarungDurchlaufzeitKeyFigure = durchlaufzeitKeyFigures.getByKeyType(KeyType.DURCHLAUFZEIT_VEREINBARUNG);

        final int tteamLanglauferCount = tteamReportingLanglauferService.getTteamLanglauferCountByTteam(tteam);
        final int vereinbarungLanglauferCount = tteamReportingLanglauferService.getVereinbarungLanglauferCountByTteam(tteam);

        filter.setTteamFilter(tteamFilterCopyForReturn);

        return new TteamKeyFigure(tteam.getId(), tteam.getTeamName(), keyFigures, tteamDurchlaufzeitKeyFigure, vereinbarungDurchlaufzeitKeyFigure, tteamLanglauferCount, vereinbarungLanglauferCount);
    }


}
