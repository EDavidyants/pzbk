package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;

public interface AnforderungReportingStatusTransitionAdapter {

    ReportingStatusTransition createAnforderungFromMeldung(Anforderung anforderung);

    ReportingStatusTransition changeAnforderungStatus(Anforderung anforderung, Status currentStatus, Status newStatus);

    ReportingStatusTransition addAnforderungToProzessbaukasten(Anforderung anforderung, Status currentStatus);

    ReportingStatusTransition removeAnforderungFromProzessbaukasten(Anforderung anforderung, Status currentStatus);

    ReportingStatusTransition createAnforderungAsNewVersion(Anforderung anforderung);

    ReportingStatusTransition restoreAnforderung(Anforderung anforderung);

    void removeAnforderung(Anforderung anforderung);

}
