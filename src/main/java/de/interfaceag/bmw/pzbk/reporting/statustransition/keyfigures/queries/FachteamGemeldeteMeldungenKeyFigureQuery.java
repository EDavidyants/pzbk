package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamGemeldeteMeldungenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Date;

/**
 * @author evda
 */
final class FachteamGemeldeteMeldungenKeyFigureQuery {

    private static final Logger LOG = LoggerFactory.getLogger(FachteamGemeldeteMeldungenKeyFigureQuery.class.getName());

    private FachteamGemeldeteMeldungenKeyFigureQuery() {

    }

    static FachteamGemeldeteMeldungenKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {

        Collection<Long> meldungenOnStartIds
                = getMeldungOnStartIds(filter, entityManager);

        return new FachteamGemeldeteMeldungenKeyFigure(meldungenOnStartIds);
    }

    private static Collection<Long> getMeldungOnStartIds(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT m.id ")
                .append("FROM ReportingMeldungStatusTransition r ")
                .append(" INNER JOIN r.meldung m ")
                .append(" LEFT JOIN m.anforderung a ")
                .append("WHERE r.gemeldet.entry < :endDate ")
                .append("AND (r.gemeldet.exit IS NULL OR r.gemeldet.exit > :endDate) ")
                .append("OR (r.newAnforderungZugeordnet.entry < :endDate AND r.newAnforderungZugeordnet.exit IS NULL) ")
                .append("OR (r.existingAnforderungZugeordnet.entry < :endDate AND r.existingAnforderungZugeordnet.exit IS NULL) ")
                .append(" OR (r.gemeldet.entry < :endDate AND r.gemeldet.exit < :endDate "
                        + "AND r.unstimmig.entry IS NULL AND r.unstimmig.exit IS NULL "
                        + "AND r.geloescht.entry < :endDate AND (r.geloescht.exit IS NULL OR r.geloescht.exit > :endDate)) ");

        KeyFigureQueryDateFilter.append(filter, queryPart);
        queryPart.put("endDate", endDate);

        LOG.debug("getMeldungOnStartIds Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

}
