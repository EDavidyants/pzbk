package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot;

import de.interfaceag.bmw.pzbk.reporting.projekt.snapshot.SnapshotReadService;

public interface ProjectProcessSnapshotReadService extends SnapshotReadService<ProjectProcessSnapshot> {
}
