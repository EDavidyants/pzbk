package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class StatusabgleichKennzahlenService implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager em;

    public StatusabgleichKennzahlen getKennzahlenForDerivat(Derivat derivat) {
        List<Object[]> queryResult = computeKennzahlen(derivat);
        Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> matrixValues = convertToMatrixValues(queryResult, derivat);
        return new StatusabgleichKennzahlen(matrixValues);
    }

    public StatusabgleichKennzahlen getKennzahlenForDerivatWithFilter(Derivat derivat, ReportingStatusabgleichFilter filter) {
        List<Object[]> queryResult = computeKennzahlenWithFilter(derivat, filter);
        Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> matrixValues = convertToMatrixValues(queryResult, derivat);
        return new StatusabgleichKennzahlen(matrixValues);
    }

    private List<Object[]> computeKennzahlen(Derivat derivat) {
        QueryPartDTO queryPart = new QueryPartDTO("SELECT dam.statusZV, dam.statusVKBG, dam.zakStatus, COUNT(dam.id) FROM DerivatAnforderungModul dam "
                + "INNER JOIN dam.zuordnungAnforderungDerivat zad "
                + "INNER JOIN zad.derivat d "
                + "WHERE d.id = :derivatId "
                + "GROUP BY dam.statusVKBG, dam.statusZV, dam.zakStatus");
        queryPart.put("derivatId", derivat.getId());

        Query query = buildGenericQuery(queryPart);
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    private List<Object[]> computeKennzahlenWithFilter(Derivat derivat, ReportingStatusabgleichFilter filter) {
        StringBuilder queryString = new StringBuilder("SELECT dam.statusZV, dam.statusVKBG, dam.zakStatus, COUNT(dam.id) FROM DerivatAnforderungModul dam "
                + "INNER JOIN dam.zuordnungAnforderungDerivat zad "
                + "INNER JOIN zad.derivat d "
                + "INNER JOIN zad.anforderung a "
                + "INNER JOIN a.sensorCoc coc "
                + "INNER JOIN dam.modul modul "
                + "WHERE d.id = :derivatId ");

        if (filter.getFachbereichFilter().isActive()) {
            queryString.append("AND modul.fachBereich IN :fachbereiche ");
        }

        if (filter.getModulFilter().isActive()) {
            queryString.append("AND modul.id IN :modulIds ");
        }

        if (filter.getTechnologieFilter().isActive()) {
            queryString.append("AND coc.ortung IN :technologie ");
        }

        queryString.append("GROUP BY dam.statusVKBG, dam.statusZV, dam.zakStatus");

        QueryPartDTO queryPart = new QueryPartDTO(queryString.toString());

        queryPart.put("derivatId", derivat.getId());

        if (filter.getFachbereichFilter().isActive()) {
            List<String> filters = filter.getFachbereichFilter().getSelectedValuestringsAsList();
            queryPart.put("fachbereiche", filters);
        }

        if (filter.getModulFilter().isActive()) {
            queryPart.put("modulIds", filter.getModulFilter().getSelectedIdsAsSet());
        }

        if (filter.getTechnologieFilter().isActive()) {
            List<String> filters = filter.getTechnologieFilter().getSelectedValuestringsAsList();
            queryPart.put("technologie", filters);
        }

        Query query = buildGenericQuery(queryPart);
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    public List<DrilldownTableAnforderungDTO> computeAnforderungIdWithFilterAndTablePosition(Derivat derivat, ReportingStatusabgleichFilter filter, int row, int col) {
        Collection<DerivatAnforderungModulStatus> toolStatus = ReportingStatusabgleichRowToStatusConverter.convertToDerivatAnforderungModulStatus(row);
        Collection<ZakStatus> zakStatus = ReportingStatusabgleichColumnToStatusConverter.convertColumnToZakStatus(col);

        List<Integer> toolStatusAsInt = toolStatus.stream().map(DerivatAnforderungModulStatus::getStatusId).collect(Collectors.toList());

        return retrieveAnforderungIdWithFilterAndMatrixPosition(derivat, filter, toolStatusAsInt, new ArrayList(zakStatus));
    }

    private List<DrilldownTableAnforderungDTO> retrieveAnforderungIdWithFilterAndMatrixPosition(Derivat derivat, ReportingStatusabgleichFilter filter, List<Integer> toolStatus, List<ZakStatus> zakStatus) {
        if (derivat.getStatus().equals(DerivatStatus.OFFEN)) {
            return Collections.emptyList();
        }
        QueryPart queryPart = new QueryPartDTO("SELECT new de.interfaceag.bmw.pzbk.reporting.statusabgleich.DrilldownTableAnforderungDTO(a.id, a.fachId, a.version, modul.name, z.zakId, a.beschreibungAnforderungDe, ");
        queryPart.append(getToolStatusString(derivat)).append(" dam.zakStatus )");
        queryPart.append(" FROM DerivatAnforderungModul dam "
                + "INNER JOIN dam.zuordnungAnforderungDerivat zad "
                + "INNER JOIN zad.derivat d "
                + "INNER JOIN zad.anforderung a "
                + "INNER JOIN a.sensorCoc coc "
                + "INNER JOIN dam.modul modul "
                + "LEFT JOIN ZakUebertragung z ON dam.id = z.id "
                + "WHERE d.id = :derivatId "
                + "AND dam.zakStatus IN :zakStatus ");

        if (derivat.getStatus().equals(DerivatStatus.VEREINARBUNG_VKBG)) {
            queryPart.append("AND dam.statusVKBG IN :toolStatus ");
        } else {
            queryPart.append("AND dam.statusZV IN :toolStatus ");
        }

        if (filter.getFachbereichFilter().isActive()) {
            queryPart.append("AND modul.fachBereich IN :fachbereiche ");
        }

        if (filter.getModulFilter().isActive()) {
            queryPart.append("AND modul.id IN :modulIds ");
        }

        if (filter.getTechnologieFilter().isActive()) {
            queryPart.append("AND coc.ortung IN :technologie ");
        }

        queryPart.put("zakStatus", zakStatus);
        queryPart.put("toolStatus", toolStatus);
        queryPart.put("derivatId", derivat.getId());

        if (filter.getFachbereichFilter().isActive()) {
            List<String> filters = filter.getFachbereichFilter().getSelectedValuestringsAsList();
            queryPart.put("fachbereiche", filters);
        }

        if (filter.getModulFilter().isActive()) {
            queryPart.put("modulIds", filter.getModulFilter().getSelectedIdsAsSet());
        }

        if (filter.getTechnologieFilter().isActive()) {
            List<String> filters = filter.getTechnologieFilter().getSelectedValuestringsAsList();
            queryPart.put("technologie", filters);
        }

        Query query = queryPart.buildGenericQuery(em);
        List<DrilldownTableAnforderungDTO> resultList = query.getResultList();
        return resultList;
    }

    private String getToolStatusString(Derivat derivat) {
        if (derivat.getStatus().equals(DerivatStatus.VEREINARBUNG_VKBG)) {
            return "dam.statusVKBG, ";
        } else {
            return "dam.statusZV, ";
        }
    }

    public List<SensorCoc> getTechnologieFilterBasedOnSearchResult(Derivat derivat, ReportingStatusabgleichFilter filter) {

        StringBuilder queryString = new StringBuilder("SELECT DISTINCT coc FROM DerivatAnforderungModul dam "
                + "INNER JOIN dam.zuordnungAnforderungDerivat zad "
                + "INNER JOIN zad.derivat d "
                + "INNER JOIN zad.anforderung.sensorCoc coc "
                + "WHERE d.id = :derivatId ");

        if (filter.getFachbereichFilter().isActive()) {
            queryString.append("AND dam.modul.fachBereich IN :fachbereiche ");
        }

        if (filter.getModulFilter().isActive()) {
            queryString.append("AND dam.modul.id IN :modulIds ");
        }

        QueryPartDTO queryPart = new QueryPartDTO(queryString.toString());

        queryPart.put("derivatId", derivat.getId());

        if (filter.getFachbereichFilter().isActive()) {
            List<String> filters = filter.getFachbereichFilter().getSelectedValuestringsAsList();
            queryPart.put("fachbereiche", filters);
        }

        if (filter.getModulFilter().isActive()) {
            queryPart.put("modulIds", filter.getModulFilter().getSelectedIdsAsSet());
        }

        Query query = buildGenericQuery(queryPart);

        List<SensorCoc> resultList = query.getResultList();
        return resultList;
    }

    public List<Modul> getModulFilterBasedOnSearchResult(Derivat derivat, ReportingStatusabgleichFilter filter) {

        StringBuilder queryString = new StringBuilder("SELECT DISTINCT dam.modul FROM DerivatAnforderungModul dam "
                + "INNER JOIN dam.zuordnungAnforderungDerivat zad "
                + "INNER JOIN zad.derivat d "
                + "INNER JOIN zad.anforderung.sensorCoc coc "
                + "WHERE d.id = :derivatId ");

        if (filter.getFachbereichFilter().isActive()) {
            queryString.append("AND dam.modul.fachBereich IN :fachbereiche ");
        }

        if (filter.getTechnologieFilter().isActive()) {
            queryString.append("AND coc.ortung IN :technologie ");
        }
        queryString.append("ORDER BY dam.modul.id");

        QueryPartDTO queryPart = new QueryPartDTO(queryString.toString());

        queryPart.put("derivatId", derivat.getId());

        if (filter.getFachbereichFilter().isActive()) {
            List<String> filters = filter.getFachbereichFilter().getSelectedValuestringsAsList();
            queryPart.put("fachbereiche", filters);
        }

        if (filter.getTechnologieFilter().isActive()) {
            List<String> filters = filter.getTechnologieFilter().getSelectedValuestringsAsList();
            queryPart.put("technologie", filters);
        }
        Query query = buildGenericQuery(queryPart);

        List<Modul> resultList = query.getResultList();
        return resultList;
    }

    private static Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> convertToMatrixValues(List<Object[]> queryRest, Derivat derivat) {
        Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> result = new HashMap<>();
        Iterator<Object[]> iterator = queryRest.iterator();

        int statusIndex = getStatusIndex(derivat.getStatus());

        while (iterator.hasNext()) {
            Object[] next = iterator.next();
            Integer statusId = (Integer) next[statusIndex];
            DerivatAnforderungModulStatus status;

            if (statusId != null) {
                status = DerivatAnforderungModulStatus.getStatusById(statusId);
            } else {
                // error handling for corrupt database entries
                status = DerivatAnforderungModulStatus.ABZUSTIMMEN;
            }

            ZakStatus zakStatus = (ZakStatus) next[2];
            Long count = (Long) next[3];
            result.put(new GenericTuple<>(status, zakStatus), count);
        }

        return result;
    }

    private static int getStatusIndex(DerivatStatus status) {
        switch (status) {
            case OFFEN:
            case VEREINARBUNG_VKBG:
                return 0;
            case VEREINBARUNG_ZV:
            case ZV:
            case INAKTIV:
            default:
                return 1;
        }
    }

    private Query buildGenericQuery(QueryPartDTO qp) {
        Query query = em.createQuery(qp.getQuery());
        if (!qp.getParameterMap().isEmpty()) {
            qp.getParameterMap().entrySet().forEach(parameter -> {
                query.setParameter(parameter.getKey(), parameter.getValue());
            });
        }
        return query;
    }

}
