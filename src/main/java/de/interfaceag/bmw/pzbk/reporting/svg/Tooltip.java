package de.interfaceag.bmw.pzbk.reporting.svg;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class Tooltip implements SVGElement {

    private final double x;
    private final double y;
    private final String value;

    public Tooltip(double xCoordinate, double yCoordinate, String value) {
        this.x = xCoordinate;
        this.y = yCoordinate;
        this.value = value;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String draw() {
        StringBuilder sb = new StringBuilder();
        sb.append("<foreignObject style=\"overflow: visible;\" width=\"1000\" height=\"50\" transform=\"translate(").append(x).append(" ").append(y).append(")\">");
        sb.append("<a onclick=\"showTooltip([{name:'tooltipName', value:'").append(value).append("'}])\" class=\"reportingtooltip\" >");
        sb.append("<span class=\"fa fa-info-circle op-80\" onclick=\"showToolTipPanel()\"></span>");
        sb.append("</a>");
        sb.append("</foreignObject>");
        return sb.toString();
    }

}
