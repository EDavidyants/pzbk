package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class TteamPlausibilisiertKeyFigure extends AbstractKeyFigure {

    public TteamPlausibilisiertKeyFigure(Collection<Long> anforderungIds) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(anforderungIds), KeyType.TTEAM_PLAUSIBILISIERT);
    }

    public TteamPlausibilisiertKeyFigure(Collection<Long> ids, Long runTime) {
        super(ids.size(), IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.TTEAM_PLAUSIBILISIERT, runTime);
    }

}
