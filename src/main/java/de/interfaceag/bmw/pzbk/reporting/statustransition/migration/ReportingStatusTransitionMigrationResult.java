package de.interfaceag.bmw.pzbk.reporting.statustransition.migration;

import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author fn
 */
public class ReportingStatusTransitionMigrationResult implements Serializable {

    private final long anforderungId;

    private final String anforderungBezeichnung;

    private final List<ReportingStatusTransition> migrationResult;

    public ReportingStatusTransitionMigrationResult(long anforderungId, String anforderungBezeichnung, List<ReportingStatusTransition> migrationResult) {
        this.anforderungId = anforderungId;
        this.anforderungBezeichnung = anforderungBezeichnung;
        this.migrationResult = migrationResult;
    }

    public long getAnforderungId() {
        return anforderungId;
    }

    public String getAnforderungBezeichnung() {
        return anforderungBezeichnung;
    }

    public List<ReportingStatusTransition> getMigrationResult() {
        return migrationResult;
    }

}
