package de.interfaceag.bmw.pzbk.reporting.config;

import de.interfaceag.bmw.pzbk.controller.ConfigurationController;
import de.interfaceag.bmw.pzbk.entities.ReportingConfig;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Named
@Stateless
public class ReportingConfigService implements Serializable {

    protected static final String KEYWORD_START_DATE = "start_date";
    protected static final String KEYWORD_LANGLAUFER_TTEAM = "langlaufer_tteam";
    protected static final String KEYWORD_LANGLAUFER_FACHTEAM = "langlaufer_fachteam";

    protected static final int DEFAULT_THRESHOLD = 50;

    @Inject
    private ReportingConfigDao reportingConfigDao;

    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationController.class);

    public Date getStartDateValue() {
        ReportingConfig reportingConfig = reportingConfigDao.getConfigByKeyword(KEYWORD_START_DATE);
        Date startDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        try {
            startDate = formatter.parse(reportingConfig.getValue());
        } catch (ParseException ex) {
            LOG.error(null, ex);
        }
        return startDate;
    }

    public void persistReportingConfigValue(String reportingStartDateWert) {
            ReportingConfig config = reportingConfigDao.getConfigByKeyword(KEYWORD_START_DATE);
            config.setValue(reportingStartDateWert);
            reportingConfigDao.persistReportingConfig(config);
    }

    public int getLanglauferTteamThreshold() {
        ReportingConfig reportingConfig = reportingConfigDao.getConfigByKeyword(KEYWORD_LANGLAUFER_TTEAM);
        return this.getThresholdValue(reportingConfig);
    }

    public void persistLanglauferTteam(String value) {
        if (value.matches("^[0-9]*$")) {
            ReportingConfig config = reportingConfigDao.getConfigByKeyword(KEYWORD_LANGLAUFER_TTEAM);
            config.setValue(value);
            reportingConfigDao.persistReportingConfig(config);
        }
    }

    public int getLanglauferFachteamThreshold() {
        ReportingConfig reportingConfig = reportingConfigDao.getConfigByKeyword(KEYWORD_LANGLAUFER_FACHTEAM);
        return this.getThresholdValue(reportingConfig);
    }

    public void persistLanglauferFachteam(String value) {
        if (value.matches("^[0-9]*$")) {
            ReportingConfig config = reportingConfigDao.getConfigByKeyword(KEYWORD_LANGLAUFER_FACHTEAM);
            config.setValue(value);
            reportingConfigDao.persistReportingConfig(config);
        }
    }

    private int getThresholdValue(ReportingConfig reportingConfig) {
        String value = reportingConfig.getValue();
        if (value == null || !RegexUtils.matchesId(value)) {
            return DEFAULT_THRESHOLD;
        }
        return Integer.valueOf(value);
    }

}
