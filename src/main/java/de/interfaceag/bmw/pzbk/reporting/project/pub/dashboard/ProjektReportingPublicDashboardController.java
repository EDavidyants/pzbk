package de.interfaceag.bmw.pzbk.reporting.project.pub.dashboard;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.reporting.project.pub.FilterPage;
import de.interfaceag.bmw.pzbk.reporting.project.pub.ReportingPublicFilter;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * @author sl
 */
@ViewScoped
@Named
public class ProjektReportingPublicDashboardController implements Serializable {

    @Inject
    @FilterPage(Page.REPORTING_PROJECT_PUBLIC_DASHBOARD)
    private ReportingPublicFilter filter;

    public String getDerivat() {
        return getFilter().getDerivatFilter().getAttributeValue();
    }

    public ReportingPublicFilter getFilter() {
        return filter;
    }

    public String getProjectProcessUrl() {
        return getFilter().getUrlForPage(Page.REPORTING_PROJECT_PUBLIC_PROCESS);
    }

    public String getStatusabgleichUrl() {
        return getFilter().getUrlForPage(Page.REPORTING_PROJECT_PUBLIC_STATUSABGLEICH);
    }

}
