package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import java.util.Date;

final class EntryExitTimeStampBuilder {

    private Date entry;
    private Date exit;

    EntryExitTimeStampBuilder() {
    }

    EntryExitTimeStampBuilder setEntry(Date entry) {
        this.entry = entry;
        return this;
    }

    EntryExitTimeStampBuilder setExit(Date exit) {
        this.exit = exit;
        return this;
    }

    EntryExitTimeStamp build() {
        return new EntryExitTimeStamp(entry, exit);
    }
}
