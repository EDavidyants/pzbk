package de.interfaceag.bmw.pzbk.reporting.projekt.werk;

import de.interfaceag.bmw.pzbk.enums.Page;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ProjectWerkFilter implements Serializable, ProjectWerkFilterController {

    private static final Page PAGE = Page.PROJEKTREPORTING_WERK;

    public ProjectWerkFilter() {
    }

    @Override
    public String filter() {
        return PAGE.getUrl() + getUrlParameter();
    }

    @Override
    public String reset() {
        return PAGE.getUrl() + "?faces-redirect=true";
    }

    private String getUrlParameter() {
        StringBuilder sb = new StringBuilder("?faces-redirect=true");
//        sb.append(sensorCocFilter.getIndependentParameter());
        return sb.toString();
    }

}
