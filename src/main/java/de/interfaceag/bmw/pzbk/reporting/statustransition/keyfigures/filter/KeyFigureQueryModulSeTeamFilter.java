package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;

import java.util.Collection;

public final class KeyFigureQueryModulSeTeamFilter {

    private KeyFigureQueryModulSeTeamFilter() {

    }

    public static void append(ReportingFilter filter, QueryPart queryPart) {
        final IdSearchFilter modulSeTeamFilter = filter.getModulSeTeamFilter();

        if (modulSeTeamFilter.isActive() && modulSeTeamFilter.getSelectedIdsAsSet() != null && !modulSeTeamFilter.getSelectedIdsAsSet().isEmpty()) {
            final Collection<Long> modulSeTeamIds = modulSeTeamFilter.getSelectedIdsAsSet();

            queryPart.append(" AND u.seTeam.id IN :modulSeTeamIds ");
            queryPart.put("modulSeTeamIds", modulSeTeamIds);
        }
    }

}
