package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class IdTypeTuple extends GenericTuple<Type, Long> {

    public IdTypeTuple(Type type, Long id) {
        super(type, id);
    }

    @Override
    public String toString() {
        return getX().getKennzeichen() + getY();
    }

}
