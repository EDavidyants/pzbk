package de.interfaceag.bmw.pzbk.reporting;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Optional;

@Named
@Dependent
public class SelectedDerivatFilterService implements Serializable {

    @Inject
    private DerivatService derivatService;

    public Optional<Derivat> getDerivatForUrlParameter(UrlParameter urlParameter) {
        Optional<String> derivatParameter = urlParameter.getValue("derivat");

        if (derivatParameter.isPresent() && RegexUtils.matchesId(derivatParameter.get())) {
            Long derivatId = Long.parseLong(derivatParameter.get());
            return Optional.ofNullable(derivatService.getDerivatById(derivatId));
        } else if (derivatParameter.isPresent()) {
            String derivatName = derivatParameter.get();
            return Optional.ofNullable(derivatService.getDerivatByName(derivatName));
        } else {
            return Optional.empty();
        }
    }
}
