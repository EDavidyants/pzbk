package de.interfaceag.bmw.pzbk.reporting.svg;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;
import de.interfaceag.bmw.pzbk.shared.dto.Point;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lomu
 */
public class OutputBox implements SVGElement {

    private final double xCoordinate;
    private final double yCoordinate;

    private final KeyFigure keyFigure;

    private final boolean showTooltips;
    private final String tooltip;
    private final String langlauferTooltip;
    private final Diamond.Type type;
    private final OutputBoxType outputBoxType;
    private final Map<String, String> outputBoxTextStrings;

    private final TooltipCollection tooltipCollection = TooltipCollection.createCollection();

    public OutputBox(KeyFigure keyFigure, boolean showTooltips, String tooltip, String langlauferTooltip, Diamond.Type type, OutputBoxType outputBoxType, Map<String, String> outputBoxTextStrings) {
        this.keyFigure = keyFigure;
        this.showTooltips = showTooltips;
        this.tooltip = tooltip;
        this.langlauferTooltip = langlauferTooltip;
        this.xCoordinate = 0;
        this.yCoordinate = 0;
        createTooltips();
        this.type = type;
        this.outputBoxType = outputBoxType;
        this.outputBoxTextStrings = outputBoxTextStrings;
    }

    public OutputBox(double xCoordinate, double yCoordinate, KeyFigure keyFigure, boolean showTooltips, String tooltip, String langlauferTooltip, Diamond.Type type, OutputBoxType outputBoxType, Map<String, String> outputBoxTextStrings) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.keyFigure = keyFigure;
        this.showTooltips = showTooltips;
        this.tooltip = tooltip;
        this.langlauferTooltip = langlauferTooltip;
        createTooltips();
        this.type = type;
        this.outputBoxType = outputBoxType;
        this.outputBoxTextStrings = outputBoxTextStrings;
    }

    public KeyFigure getKeyFigure() {
        return keyFigure;
    }

    private void createTooltips() {
        if (showTooltips) {
            tooltipCollection.add(new Tooltip(xCoordinate + 1023, yCoordinate + 12, tooltip));
        }
    }

    @Override
    public String draw() {
        SvgElementCollection elementCollection = new SvgElementCollection();

        List<Point> points = Arrays.asList(new Point(xCoordinate + 1137.86, yCoordinate + 6.14), new Point(xCoordinate + 1014.02, yCoordinate + 6.14), new Point(xCoordinate + 1031.3, yCoordinate + 71.41), new Point(xCoordinate + 1014.04, yCoordinate + 136.65), new Point(xCoordinate + 1137.88, yCoordinate + 136.65), new Point(xCoordinate + 1137.86, yCoordinate + 6.14));
        SVGElement polygon1 = new Polygon(outputBoxType.polyClass1, points);
        SVGElement polygon2 = new Polygon(outputBoxType.polyClass2, points);
        SVGElement group1 = new Group(outputBoxType.groupClass1, Arrays.asList(polygon1, polygon2));
        SVGElement group2 = new Group(outputBoxType.groupClass2, group1);

        elementCollection.add(group2);

        elementCollection.add(new Diamond(xCoordinate + 1058.29, yCoordinate + 82.62, getKeyFigure(), type));

        if (keyFigure != null && keyFigure.isLanglaufer()) {
            elementCollection.add(new LanglauferHinweis(xCoordinate + 1090, yCoordinate + 55, langlauferTooltip));
        }

        elementCollection.add(new Text(outputBoxType.textClass, "translate(" + (xCoordinate + 1040.14) + " " + (yCoordinate + 42.06) + ")", outputBoxTextStrings.get("freigegeben")));

        return elementCollection.draw();
    }

    public TooltipCollection getTooltipCollection() {
        return tooltipCollection;
    }

    public enum OutputBoxType {
        SIMPLE("1d7519ae-8173-4095-972a-09870179f637", "948d7ce9-e863-45e7-a3e6-179e4cab85de", "d76e5379-b6ef-4363-8b7b-ccff3b21fda0", "f8b67d6e-2a5b-4c45-b1fa-aaeb448e4c8b", "ffa89ab7-8120-4f9d-b7d7-33e849fbefce"),
        DETAIL("1fc087fc-5ad7-4044-a16b-f1da2c7b2c10", "01e056ae-e1a3-4d3c-bc83-4c867c3167ae", "e57f8aad-b61e-4efb-a49d-599c085f3eec", "2d24508d-5474-4519-8463-2fe54f9ec290", "5f459e6e-0411-4145-a215-c5424afc40fe");

        private final String polyClass1;
        private final String polyClass2;
        private final String groupClass1;
        private final String groupClass2;
        private final String textClass;

        OutputBoxType(String polyClass1, String polyClass2, String groupClass1, String groupClass2, String textClass) {
            this.polyClass1 = polyClass1;
            this.polyClass2 = polyClass2;
            this.groupClass1 = groupClass1;
            this.groupClass2 = groupClass2;
            this.textClass = textClass;
        }

        public String getPolyClass1() {
            return polyClass1;
        }

        public String getPolyClass2() {
            return polyClass2;
        }

        public String getGroupClass1() {
            return groupClass1;
        }

        public String getGroupClass2() {
            return groupClass2;
        }

        public String getTextClass() {
            return textClass;
        }

    }

}
