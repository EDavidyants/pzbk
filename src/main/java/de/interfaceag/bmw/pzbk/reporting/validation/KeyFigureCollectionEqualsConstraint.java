package de.interfaceag.bmw.pzbk.reporting.validation;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class KeyFigureCollectionEqualsConstraint implements KeyFigureConstraint {

    private final Collection<KeyFigure> leftSide;
    private final Collection<KeyFigure> rightSide;
    private boolean result;
    private final boolean nullProperty;
    private final String identifier;

    private final String reportingValidationPart1; // = Die Validierung von Bedingung
    private final String reportingValidationKpiMissing; // = wurde wegen fehlender KPIs nicht durchgeführt.
    private final String reportingValidationSuccessful; // = war erfolgreich.
    private final String reportingValidationFailed; // = war nicht erfolgreich mit dem Ergebnis:

    private KeyFigureCollectionEqualsConstraint(@NotNull Collection<KeyFigure> leftSide, @NotNull Collection<KeyFigure> rightSide, String identifier,
                                                String reportingValidationPart1, String reportingValidationKpiMissing,
                                                String reportingValidationSuccessful, String reportingValidationFailed) {
        this.leftSide = new ArrayList<>(leftSide);
        this.rightSide = new ArrayList<>(rightSide);
        this.nullProperty = Boolean.FALSE;
        this.identifier = identifier;
        this.reportingValidationPart1 = reportingValidationPart1;
        this.reportingValidationKpiMissing = reportingValidationKpiMissing;
        this.reportingValidationSuccessful = reportingValidationSuccessful;
        this.reportingValidationFailed = reportingValidationFailed;
    }

    private KeyFigureCollectionEqualsConstraint(boolean nullProperty, String identifier,
                                                String reportingValidationPart1, String reportingValidationKpiMissing,
                                                String reportingValidationSuccessful, String reportingValidationFailed) {
        this.leftSide = new ArrayList<>();
        this.rightSide = new ArrayList<>();
        this.result = Boolean.FALSE;
        this.nullProperty = nullProperty;
        this.identifier = identifier;

        this.reportingValidationPart1 = reportingValidationPart1;
        this.reportingValidationKpiMissing = reportingValidationKpiMissing;
        this.reportingValidationSuccessful = reportingValidationSuccessful;
        this.reportingValidationFailed = reportingValidationFailed;
    }

    @Override
    public KeyFigureValidationResult validate() {

        if (nullProperty) {
            return new KeyFigureValidationResult(Boolean.FALSE, getInsufficientDataMessage());
        }

        int left = getCollectionSize(leftSide);
        int right = getCollectionSize(rightSide);
        result = left == right;
        if (result) {
            return new KeyFigureValidationResult(result, getValidationSuccessfulMessage());
        } else {
            return new KeyFigureValidationResult(result, getValidationFailedMessage());
        }
    }

    private String getInsufficientDataMessage() {
        return reportingValidationPart1 + " " + identifier + " " + reportingValidationKpiMissing;
    }

    private String getValidationSuccessfulMessage() {
        return reportingValidationPart1 + " " + identifier + " : " + collectionToString(leftSide) + " = " + collectionToString(rightSide) + " " + reportingValidationSuccessful;
    }

    private String getValidationFailedMessage() {
        return reportingValidationPart1 + " " + identifier + " : " + collectionToString(leftSide) + " = " + collectionToString(rightSide) + " " + reportingValidationFailed + " "
                + getCollectionSize(leftSide) + " = " + getCollectionSize(rightSide);
    }

    private static String collectionToString(Collection<KeyFigure> collection) {
        if (collection != null) {
            return collection.stream().map(keyFigure -> Integer.toString(keyFigure.getKeyType().getId())).collect(Collectors.joining(" + "));
        }
        return "";
    }

    private static int getCollectionSize(Collection<KeyFigure> collection) {
        if (collection != null) {
            return collection.stream().map(DurchlaufzeitKeyFigure::getValue).mapToInt(Integer::intValue).sum();
        }
        return 0;
    }

    public static KeyFigureConstraintBuilder newBuilder() {
        return new KeyFigureConstraintBuilder();
    }

    public static final class KeyFigureConstraintBuilder {

        private final Collection<KeyFigure> leftSide = new ArrayList<>();
        private final Collection<KeyFigure> rightSide = new ArrayList<>();
        private boolean nullProperty;
        private String identifier = "";

        private String reportingValidationPart1 = "Die Validierung von Bedingung";
        private String reportingValidationKpiMissing = "wurde wegen fehlender KPIs nicht durchgeführt.";
        private String reportingValidationSuccessful = "war erfolgreich.";
        private String reportingValidationFailed = "war nicht erfolgreich mit dem Ergebnis:";

        private KeyFigureConstraintBuilder() {
        }

        KeyFigureConstraintBuilder withValidationMessagePrefix(String part1) {
            if (part1 != null && !part1.isEmpty()) {
                this.reportingValidationPart1 = part1;
            }
            return this;
        }

        KeyFigureConstraintBuilder withValidationMessageKpiMissing(String kpiMissing) {
            if (kpiMissing != null && !kpiMissing.isEmpty()) {
                this.reportingValidationKpiMissing = kpiMissing;
            }
            return this;
        }

        KeyFigureConstraintBuilder withValidationMessageSuccessful(String sucessful) {
            if (sucessful != null && !sucessful.isEmpty()) {
                this.reportingValidationSuccessful = sucessful;
            }
            return this;
        }

        KeyFigureConstraintBuilder withValidationMessageFailed(String failed) {
            if (failed != null && !failed.isEmpty()) {
                this.reportingValidationFailed = failed;
            }
            return this;
        }

        KeyFigureConstraintBuilder withIdentifier(String identifier) {
            if (identifier != null) {
                this.identifier = identifier;
            }
            return this;
        }

        KeyFigureConstraintBuilder addToLeftSide(KeyFigure keyFigure) {
            if (keyFigure != null) {
                leftSide.add(keyFigure);
            } else {
                nullProperty = Boolean.TRUE;
            }
            return this;
        }

        KeyFigureConstraintBuilder addToRightSide(KeyFigure keyFigure) {
            if (keyFigure != null) {
                rightSide.add(keyFigure);
            } else {
                nullProperty = Boolean.TRUE;
            }
            return this;
        }

        public KeyFigureConstraint build() {
            if (nullProperty) {
                return new KeyFigureCollectionEqualsConstraint(nullProperty, identifier, reportingValidationPart1, reportingValidationKpiMissing, reportingValidationSuccessful, reportingValidationFailed);
            } else {
                return new KeyFigureCollectionEqualsConstraint(leftSide, rightSide, identifier, reportingValidationPart1, reportingValidationKpiMissing, reportingValidationSuccessful, reportingValidationFailed);
            }
        }

    }

}
