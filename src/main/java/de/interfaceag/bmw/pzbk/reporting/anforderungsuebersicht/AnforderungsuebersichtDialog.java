package de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht;

import de.interfaceag.bmw.pzbk.reporting.excel.ReportingExcelDataDto;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import org.primefaces.model.SortOrder;

import java.util.Collection;
import java.util.List;

/**
 *
 * @author evda
 */
public interface AnforderungsuebersichtDialog {

    Collection<ReportingExcelDataDto> getExcelData();

    KeyType getSelectedKeyType();

    String getStatusDatumLabel();

    List<Anforderungsuebersicht> getAnforderungsuebersichtData();

    Boolean getDescendingSortOrder();

    void setDescendingSortOrder(Boolean descendingSortOrder);

    SortOrder getSortingFachteam();

    SortOrder getSortingTteam();

    Boolean hasDurchlaufzeitAndLanglaufer();

}
