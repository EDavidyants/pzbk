package de.interfaceag.bmw.pzbk.reporting.statustransition.migration;

import de.interfaceag.bmw.pzbk.enums.Status;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author fn
 */
final class AnforderungHistoryActionConverter {

    private AnforderungHistoryActionConverter() {
    }

    static AnforderungHistoryAction convert(String objektName, String von, String auf) {
        if (anforderungIsNeuAngelegt(objektName, auf)) {
            return AnforderungHistoryAction.CREATION;
        }
        if (anforderungIsNewVersion(objektName)) {
            return AnforderungHistoryAction.NEW_VERSION;
        }
        if (anforderungIsProzessbaukastenZugeordnet(objektName, von)) {
            return AnforderungHistoryAction.ADD_TO_PROZESSBAUKASTEN;
        }
        if (anforderungIsRemovedFromProzessbaukasten(objektName, auf)) {
            return AnforderungHistoryAction.REMOVE_FROM_PROZESSBAUKASTEN;
        }
        
        if (anforderungStatusIsChanged(objektName)) {
            if ("gelöscht".equals(von) && "in Arbeit".equals(auf)) {
                return AnforderungHistoryAction.RESTORE;
            } else {
                return convertStatusChange(von, auf);
            }
        }
        return AnforderungHistoryAction.OTHER;
    }

    private static boolean anforderungStatusIsChanged(String objektName) {
        return "Status".equals(objektName);
    }

    private static boolean anforderungIsRemovedFromProzessbaukasten(String objektName, String auf) {
        return "Zuordnung Prozessbaukasten".equals(objektName) && "Zuordnung entfernt".equals(auf);
    }

    private static boolean anforderungIsProzessbaukastenZugeordnet(String objektName, String von) {
        return "Zuordnung Prozessbaukasten".equals(objektName) && "zugeordnet".equals(von);
    }

    private static boolean anforderungIsNewVersion(String objektName) {
        return "neue Version von Anforderung".equals(objektName);
    }

    private static boolean anforderungIsNeuAngelegt(String objektName, String auf) {
        return "Anforderung".equals(objektName) && "neu angelegt".equals(auf);
    }

    private static AnforderungHistoryAction convertStatusChange(String von, String auf) {
        if (isInvalidStatusValue(von) || isInvalidStatusValue(auf)) {
            return AnforderungHistoryAction.OTHER;
        } else {
            return AnforderungHistoryAction.STATUS_CHANGE;
        }
    }

    private static boolean isInvalidStatusValue(String value) {
        if (value == null || value.isEmpty()) {
            return true;
        } else {
            final Set<String> validStatus = getValidStatusValues();
            return !validStatus.contains(value);
        }
    }

    private static Set<String> getValidStatusValues() {
        return Stream.of(Status.values()).filter(status -> "A".equals(status.getStatusMarker())).map(Status::getStatusBezeichnung).collect(Collectors.toSet());
    }

}
