package de.interfaceag.bmw.pzbk.reporting.projekt.dashboard;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

public class ProjektReportingDashboardViewPermission implements Serializable {

    private final ViewPermission page;

    public ProjektReportingDashboardViewPermission(Set<Rolle> rolesWithPermission) {

        page = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.E_COC)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(rolesWithPermission).get();

    }

    public boolean getPage() {
        return page.isRendered();
    }

}
