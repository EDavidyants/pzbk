package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachteamMeldungExistingAnforderungKeyFigure extends AbstractKeyFigure {

    public FachteamMeldungExistingAnforderungKeyFigure(Collection<Long> meldungIds) {
        super(meldungIds.size(), IdTypeTupleFactory.getIdTypeTuplesForMeldung(meldungIds), KeyType.FACHTEAM_MELDUNG_EXISTING_ANFORDERUNG, 0L);
    }

}
