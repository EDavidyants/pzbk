package de.interfaceag.bmw.pzbk.reporting.processoverview;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.svg.Box;
import de.interfaceag.bmw.pzbk.reporting.svg.BoxPolygon;
import de.interfaceag.bmw.pzbk.reporting.svg.Diamond;
import de.interfaceag.bmw.pzbk.reporting.svg.InputBox;
import de.interfaceag.bmw.pzbk.reporting.svg.Line;
import de.interfaceag.bmw.pzbk.reporting.svg.OutputBox;
import de.interfaceag.bmw.pzbk.reporting.svg.Path;
import de.interfaceag.bmw.pzbk.reporting.svg.Polygon;
import de.interfaceag.bmw.pzbk.reporting.svg.SVGElement;
import de.interfaceag.bmw.pzbk.reporting.svg.SVGElementDrawer;
import de.interfaceag.bmw.pzbk.reporting.svg.Text;
import de.interfaceag.bmw.pzbk.reporting.svg.TooltipCollection;
import de.interfaceag.bmw.pzbk.shared.dto.Point;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author lomu
 */
@FacesComponent(createTag = true, tagName = "SVGProcessOverviewSimple", namespace = "http://java.sun.com/jsf/de/interfaceag/bmw/pzbk/reporting")
public class SVGProcessOverviewSimple extends UIComponentBase {

    private static final double SVGHEIGTH = 437;
    private static final double SVGWIDTH = 1173;

    @Override
    public String getFamily() {
        return "Reporting";
    }

    @Override
    public void encodeBegin(FacesContext context) throws IOException {

        ReportingProcessOverviewSimpleController reportingProcessOverviewController = (ReportingProcessOverviewSimpleController) getAttributes().get("processOverviewController");
        ProcessOverviewViewData<ProcessOverviewSimpleTooltip> viewData = reportingProcessOverviewController.getViewData();

        boolean showTooltips = viewData.isShowTooltips();

        List<SVGElement> elements = new ArrayList<>();

        TooltipCollection tooltipCollection = TooltipCollection.createCollection();

        Map<String, String> textFromProperties = getTextFromProperties(reportingProcessOverviewController);

        final DurchlaufzeitKeyFigure fachteamDurchlaufzeit = viewData.getProcessOverviewKeyFigures().getDurchlaufzeitKeyFigures().getByKeyType(KeyType.DURCHLAUFZEIT_FACHTEAM);
        Box fachteamBox = new Box(248, 0, Box.SimpleBoxType.FACHTEAM,
                viewData.getProcessOverviewKeyFigures().getKeyFigureByTypeId(5), fachteamDurchlaufzeit.getValue(),
                showTooltips,
                ProcessOverviewSimpleTooltip.FACHTEAM_HEADER.toString(),
                ProcessOverviewSimpleTooltip.FACHTEAM_DURCHLAUFZEIT.toString(),
                ProcessOverviewSimpleTooltip.LANGLAUFER_FACHTEAM.toString(),
                BoxPolygon.PolygonType.SIMPLE, Diamond.Type.SIMPLE, textFromProperties);
        elements.add(fachteamBox);
        tooltipCollection.addAll(fachteamBox.getTooltipCollection());

        final DurchlaufzeitKeyFigure tteamDurchlaufzeit = viewData.getProcessOverviewKeyFigures().getDurchlaufzeitKeyFigures().getByKeyType(KeyType.DURCHLAUFZEIT_TTEAM);
        Box tteamBox = new Box(500, 0, Box.SimpleBoxType.TTEAM, viewData.getProcessOverviewKeyFigures().getKeyFigureByTypeId(10),
                tteamDurchlaufzeit.getValue(), showTooltips,
                ProcessOverviewSimpleTooltip.TTEAM_HEADER.toString(),
                ProcessOverviewSimpleTooltip.TTEAM_DURCHLAUFZEIT.toString(),
                ProcessOverviewSimpleTooltip.LANGLAUFER_TTEAM.toString(),
                BoxPolygon.PolygonType.SIMPLE, Diamond.Type.SIMPLE, textFromProperties);
        elements.add(tteamBox);
        tooltipCollection.addAll(tteamBox.getTooltipCollection());

        final DurchlaufzeitKeyFigure vereinbarungDurchlaufzeit = viewData.getProcessOverviewKeyFigures().getDurchlaufzeitKeyFigures().getByKeyType(KeyType.DURCHLAUFZEIT_VEREINBARUNG);
        Box vereinbarungBox = new Box(754, 0, Box.SimpleBoxType.VEREINBARUNG, viewData.getProcessOverviewKeyFigures().getKeyFigureByTypeId(14),
                vereinbarungDurchlaufzeit.getValue(), showTooltips,
                ProcessOverviewSimpleTooltip.VEREINBARUNG_HEADER.toString(),
                ProcessOverviewSimpleTooltip.VEREINBARUNG_DURCHLAUFZEIT.toString(),
                ProcessOverviewSimpleTooltip.LANGLAUFER_VEREINBARUNG.toString(),
                BoxPolygon.PolygonType.SIMPLE, Diamond.Type.SIMPLE, textFromProperties);
        elements.add(vereinbarungBox);
        tooltipCollection.addAll(vereinbarungBox.getTooltipCollection());

        elements.add(new InputBox(0, 0, InputBox.InputBoxType.SIMPLE, textFromProperties));

        OutputBox outputBox = new OutputBox(viewData.getProcessOverviewKeyFigures().getKeyFigureByTypeId(24),
                showTooltips,
                ProcessOverviewSimpleTooltip.FREIGEGEBEN.toString(),
                ProcessOverviewSimpleTooltip.LANGLAUFER_FREIGEGEBEN.toString(),
                Diamond.Type.SIMPLE, OutputBox.OutputBoxType.SIMPLE, textFromProperties);
        elements.add(outputBox);
        tooltipCollection.addAll(outputBox.getTooltipCollection());

        //GesamtdurchlaufzeitBox gesamtdurchlaufzeitBox = new GesamtdurchlaufzeitBox(0, ProcessOverviewSimpleTooltip.DURCHLAUFZEIT_GESAMT), showTooltips, GesamtdurchlaufzeitBox.GesamtdurchlaufzeitType.SIMPLE);
        //elements.add(gesamtdurchlaufzeitBox);
        //tooltipCollection.addAll(gesamtdurchlaufzeitBox.getTooltipCollection());
        ResponseWriter writer = context.getResponseWriter();
        writer.startElement("div", this);
        writer.writeAttribute("class", "svg-wrapper", null);
        writer.writeAttribute("id", "svg-div", null);
        double padding = (SVGHEIGTH / SVGWIDTH) * 100;
        writer.writeAttribute("style", "position: relative; z-index: 1; padding-bottom: " + ((int) padding) + "%; max-width: 1920px;", null);

        writer.startElement("svg", this);
        writer.writeAttribute("id", "161c26c4-3f1f-4092-bed0-980ca117dba4", null);
        writer.writeAttribute("data-name", "Ebene 1", null);
        writer.writeAttribute("xmlns", "http://www.w3.org/2000/svg\\", null);
        writer.writeAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink", null);
        writer.writeAttribute("viewBox", "0 0 " + SVGWIDTH + " " + SVGHEIGTH, null);
        writer.writeAttribute("style", "position: absolute;", null);

        StringBuilder sb = new StringBuilder(getDefs());
        sb.append(SVGElementDrawer.drawElements(elements))
                .append(getPfeile(reportingProcessOverviewController))
                .append(getClock(reportingProcessOverviewController))
                .append(tooltipCollection.drawCollection());

        writer.write(
                getDefs()
                        + SVGElementDrawer.drawElements(elements)
                        + getPfeile(reportingProcessOverviewController)
                        + getClock(reportingProcessOverviewController)
                        + tooltipCollection.drawCollection()
        );

        writer.endElement("svg");
        writer.endElement("div");
    }

    private Map<String, String> getTextFromProperties(ReportingProcessOverviewSimpleController reportingProcessOverviewController) {
        return Stream.of(
                new AbstractMap.SimpleImmutableEntry<>("durchlaufzeit", reportingProcessOverviewController.getStringfromProperty("reporting_durchlaufzeit")),
                new AbstractMap.SimpleImmutableEntry<>("fachteam", reportingProcessOverviewController.getStringfromProperty("reporting_fachteam")),
                new AbstractMap.SimpleImmutableEntry<>("tteam", reportingProcessOverviewController.getStringfromProperty("reporting_tteam")),
                new AbstractMap.SimpleImmutableEntry<>("vereinbarung", reportingProcessOverviewController.getStringfromProperty("reporting_vereinbarung")),
                new AbstractMap.SimpleImmutableEntry<>("vereinbarungPart1", reportingProcessOverviewController.getStringfromProperty("reporting_vereinbarungPart1")),
                new AbstractMap.SimpleImmutableEntry<>("vereinbarungPart2", reportingProcessOverviewController.getStringfromProperty("reporting_vereinbarungPart2")),
                new AbstractMap.SimpleImmutableEntry<>("staerken", reportingProcessOverviewController.getStringfromProperty("reporting_staerken")),
                new AbstractMap.SimpleImmutableEntry<>("schwaechen", reportingProcessOverviewController.getStringfromProperty("reporting_schwaechen")),
                new AbstractMap.SimpleImmutableEntry<>("aus", reportingProcessOverviewController.getStringfromProperty("reporting_aus")),
                new AbstractMap.SimpleImmutableEntry<>("werken", reportingProcessOverviewController.getStringfromProperty("reporting_werkenFiz")),
                new AbstractMap.SimpleImmutableEntry<>("freigegeben", reportingProcessOverviewController.getStringfromProperty("reporting_freigegeben")))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private static String getDefs() {
        return "<defs>\n"
                + "                      <style>\n"
                + "                          .\\36 2cd76c4-80a4-4944-a40d-e6dd289b40b2,.\\39 48d7ce9-e863-45e7-a3e6-179e4cab85de,.ced86064-e9d2-49f2-bdd0-48209c2c4129,.e7979375-7020-4a99-8f60-046cf1838a15{fill:none;}.\\33 49b8e67-6303-488d-a7e9-fa034879202a{clip-path:url(#cc07976c-780c-4058-b79c-6667356f0f50);}.\\39 48d7ce9-e863-45e7-a3e6-179e4cab85de,.e7979375-7020-4a99-8f60-046cf1838a15{stroke:#fff;}.e7979375-7020-4a99-8f60-046cf1838a15{stroke-width:1.5px;}.b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4,.c09415f1-be70-4123-b273-3f949364e3b0,.ffa89ab7-8120-4f9d-b7d7-33e849fbefce{fill:#fff;}.e74f067f-41ce-4693-b2d0-e5ae056d20f0{opacity:0.1;}.\\39 3fdd1a2-f0db-421b-ab67-13a781186ef8{clip-path:url(#5543b119-0a35-4f6a-8995-13035f88b8c8);}.\\39 5d8ff88-c217-4d42-8167-ef4e8468a91f{clip-path:url(#8f3b93e6-7373-41c9-8d45-8ca77089574f);}.\\30 93fd0ba-d0ec-4f8b-a6aa-a26a6b7b6050{clip-path:url(#34655791-7bea-4df6-96e0-21b1d3202541);}.dd6ae827-876a-4553-96e5-bd1bf972d71c{clip-path:url(#2c134469-5434-4b87-bef0-3e51a5ddff38);}.f8b67d6e-2a5b-4c45-b1fa-aaeb448e4c8b{opacity:0.5;}.b1af0568-0922-49a0-8b80-34d724b01ca3{clip-path:url(#e7142ba1-852e-4276-8e75-951dbe3dc91d);}.fdd48387-9079-4f7f-a6dc-c6966447a7a9{fill:#5188cc;}.\\31 d7519ae-8173-4095-972a-09870179f637{fill:#222324;}.\\36 2cd76c4-80a4-4944-a40d-e6dd289b40b2{stroke:#cbcbcb;}.c09415f1-be70-4123-b273-3f949364e3b0{font-size:16px;font-family:Arial-BoldMT, Arial;font-weight:700;}.\\33 96e18d4-985a-416e-8e80-c6e04f9e6963{clip-path:url(#9dbcf618-2fdd-4edb-9bb1-bf1e4f7975f6);}.\\36 750b86e-6d7e-4150-af85-a92b4c419fc5{fill:#7d99aa;}.a0bc97b4-00b0-4886-83d1-452e86389d8d{letter-spacing:-0.02em;}.ca8549ca-5caa-42b6-9100-70881d6a7b8b{clip-path:url(#18513fca-3652-4a9d-a6f0-65aaa65e4df4);}.ada718fd-cebf-4711-b8c3-704fb1f779d3{fill:#3766a0;}.\\33 2b6ab8f-133c-4a3b-a1b9-9a7317c1f0be{letter-spacing:-0.05em;}.b48e2208-b83a-4ec1-aaa4-b4c568f7e4f2{letter-spacing:-0.07em;}.b6578f7b-ad84-44b3-821b-0da8997a72f5{clip-path:url(#42e3cb79-b3b4-4f1e-91f6-29fc70ff71fd);}.\\33 2ea3dbb-e8f2-49b2-bc90-93718ff0c09d{fill:#123054;}.\\31 252fbb6-43e0-41b4-9c05-90c463ae83ca{clip-path:url(#51f58aef-e770-4760-ae30-c450f980ad39);}.d32fb565-8367-4ad9-9c31-83d4ceae1048{clip-path:url(#d05b77a7-9835-4a3b-a28f-720ec3ddf709);}.d76e5379-b6ef-4363-8b7b-ccff3b21fda0{clip-path:url(#dd505d09-b6fe-4bfa-a794-94f42e6dd707);}.ffa89ab7-8120-4f9d-b7d7-33e849fbefce{font-size:14px;font-family:ArialMT, Arial;}.\\35 32ac923-9f8e-48c0-af56-7156d9ba9a2c{letter-spacing:-0.05em;}.fe2a04d7-14bd-452c-a088-a2e19bdfd276{letter-spacing:-0.06em;}.\\32 a902ce1-836a-4c47-93a9-c50279cd09aa{letter-spacing:-0.11em;}.\\36 d55a60d-d6ee-438f-a912-dd248843640d{letter-spacing:-0.05em;}</style>\n"
                + "                          <clipPath id=\"cc07976c-780c-4058-b79c-6667356f0f50\" transform=\"translate(0)\">\n"
                + "                              <rect class=\"ced86064-e9d2-49f2-bdd0-48209c2c4129\" width=\"1173\" height=\"407\"/>\n"
                + "                          </clipPath>\n"
                + "                      <clipPath id=\"5543b119-0a35-4f6a-8995-13035f88b8c8\" transform=\"translate(0)\">\n"
                + "                          <rect class=\"ced86064-e9d2-49f2-bdd0-48209c2c4129\" x=\"239\" y=\"344.5\" width=\"659\" height=\"63.5\"/>\n"
                + "                      </clipPath>\n"
                + "                      <clipPath id=\"8f3b93e6-7373-41c9-8d45-8ca77089574f\" transform=\"translate(0)\">\n"
                + "                          <rect class=\"ced86064-e9d2-49f2-bdd0-48209c2c4129\" x=\"239\" y=\"-7\" width=\"153\" height=\"341\"/>\n"
                + "                      </clipPath>\n"
                + "                      <clipPath id=\"34655791-7bea-4df6-96e0-21b1d3202541\" transform=\"translate(0)\">\n"
                + "                          <rect class=\"ced86064-e9d2-49f2-bdd0-48209c2c4129\" x=\"491\" y=\"-7\" width=\"153\" height=\"341\"/>\n"
                + "                      </clipPath>\n"
                + "                      <clipPath id=\"2c134469-5434-4b87-bef0-3e51a5ddff38\" transform=\"translate(0)\">\n"
                + "                          <rect class=\"ced86064-e9d2-49f2-bdd0-48209c2c4129\" x=\"745\" y=\"-7\" width=\"153\" height=\"341\"/>\n"
                + "                      </clipPath>\n"
                + "                      <clipPath id=\"e7142ba1-852e-4276-8e75-951dbe3dc91d\" transform=\"translate(0)\">\n"
                + "                          <rect class=\"ced86064-e9d2-49f2-bdd0-48209c2c4129\" x=\"243.52\" y=\"-1.36\" width=\"143\" height=\"139.5\"/>\n"
                + "                      </clipPath>\n"
                + "                      <clipPath id=\"9dbcf618-2fdd-4edb-9bb1-bf1e4f7975f6\" transform=\"translate(0)\">\n"
                + "                          <rect class=\"ced86064-e9d2-49f2-bdd0-48209c2c4129\" x=\"-9\" y=\"-1.36\" width=\"143\" height=\"139.5\"/>\n"
                + "                      </clipPath>\n"
                + "                      <clipPath id=\"18513fca-3652-4a9d-a6f0-65aaa65e4df4\" transform=\"translate(0)\">\n"
                + "                          <rect class=\"ced86064-e9d2-49f2-bdd0-48209c2c4129\" x=\"496.8\" y=\"-1.36\" width=\"143\" height=\"139.5\"/>\n"
                + "                      </clipPath>\n"
                + "                      <clipPath id=\"42e3cb79-b3b4-4f1e-91f6-29fc70ff71fd\" transform=\"translate(0)\">\n"
                + "                          <rect class=\"ced86064-e9d2-49f2-bdd0-48209c2c4129\" x=\"750.42\" y=\"-1.36\" width=\"143\" height=\"139.5\"/>\n"
                + "                      </clipPath>\n"
                + "                      <clipPath id=\"51f58aef-e770-4760-ae30-c450f980ad39\" transform=\"translate(0)\">\n"
                + "                          <path class=\"ced86064-e9d2-49f2-bdd0-48209c2c4129\" d=\"M56.74,275.29a12.72,12.72,0,0,0-3.83,3.83v13.44a12.69,12.69,0,0,0,3.83,3.82H70.17A12.57,12.57,0,0,0,74,292.56V279.12a12.61,12.61,0,0,0-3.83-3.83Z\"/>\n"
                + "                      </clipPath>\n"
                + "                      <clipPath id=\"d05b77a7-9835-4a3b-a28f-720ec3ddf709\" transform=\"translate(0)\">\n"
                + "                          <rect class=\"ced86064-e9d2-49f2-bdd0-48209c2c4129\" x=\"52.91\" y=\"275.3\" width=\"21.09\" height=\"21.09\"/>\n"
                + "                      </clipPath>\n"
                + "                      <clipPath id=\"dd505d09-b6fe-4bfa-a794-94f42e6dd707\" transform=\"translate(0)\">\n"
                + "                          <rect class=\"ced86064-e9d2-49f2-bdd0-48209c2c4129\" x=\"1004.38\" y=\"-1.36\" width=\"143\" height=\"139.5\"/>\n"
                + "                      </clipPath>\n"
                + "                  </defs>\n";
    }

    private static String getPfeile(ReportingProcessOverviewSimpleController reportingProcessOverviewController) {
        List<SVGElement> elements = new ArrayList<>();
        //Senkrechte Pfeile
        elements.add(new Path("948d7ce9-e863-45e7-a3e6-179e4cab85de", "M651.5,217.9V198a9,9,0,0,1,0-18V121.5", "translate(0 -0.02)"));
        elements.add(new Path("948d7ce9-e863-45e7-a3e6-179e4cab85de", "M905.5,217.9V198a9,9,0,0,1,0-18V121.5", "translate(0)"));
        elements.add(new Path("1d7519ae-8173-4095-972a-09870179f637", "M63.46,298.34A12.5,12.5,0,1,0,51,285.84a12.5,12.5,0,0,0,12.5,12.5", "translate(0)"));

        // Pfeilspitzen
        elements.add(new Polygon("b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", Arrays.asList(new Point(269.75, 71.69), new Point(263.05, 65.46), new Point(263.05, 77.92), new Point(269.75, 71.69)))); //rechts
        elements.add(new Polygon("b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", Arrays.asList(new Point(522.83, 71.69), new Point(516.13, 65.46), new Point(516.13, 77.92), new Point(522.83, 71.69)))); //rechts
        elements.add(new Polygon("b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", Arrays.asList(new Point(1030.49, 71.69), new Point(1023.79, 65.46), new Point(1023.79, 77.92), new Point(1030.49, 71.69)))); //rechts
        elements.add(new Polygon("b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", Arrays.asList(new Point(776.87, 71.69), new Point(770.17, 65.46), new Point(770.17, 77.92), new Point(776.87, 71.69)))); //rechts
        elements.add(new Polygon("b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", Arrays.asList(new Point(261.0, 103.5), new Point(256.53, 99.34), new Point(256.53, 107.66), new Point(261.0, 103.5)))); //rechts
        elements.add(new Polygon("b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", Arrays.asList(new Point(265.0, 89.16), new Point(260.53, 85.0), new Point(260.53, 93.31), new Point(265.0, 89.16)))); //rechts
        elements.add(new Polygon("b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", Arrays.asList(new Point(905.5, 222.0), new Point(901.35, 217.53), new Point(909.65, 217.53), new Point(905.5, 222.0)))); //unten
        elements.add(new Polygon("b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", Arrays.asList(new Point(688.33, 186.5), new Point(684.18, 182.03), new Point(692.49, 182.03), new Point(688.33, 186.5)))); //unten
        elements.add(new Polygon("b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", Arrays.asList(new Point(651.5, 222.0), new Point(647.35, 217.53), new Point(655.65, 217.53), new Point(651.5, 222.0)))); //unten*/

        //Pfeillinien
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "1172.5", "224", "1172.5", "121.5"));
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "1138", "122", "1173", "122"));
        elements.add(new Line("e7979375-7020-4a99-8f60-046cf1838a15", "123", "71.69", "265.1", "71.69"));
        elements.add(new Line("e7979375-7020-4a99-8f60-046cf1838a15", "376", "71.69", "518.18", "71.69"));
        elements.add(new Line("e7979375-7020-4a99-8f60-046cf1838a15", "629", "71.69", "772.22", "71.69"));
        elements.add(new Line("e7979375-7020-4a99-8f60-046cf1838a15", "883", "71.69", "1025.84", "71.69"));
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "876", "103.5", "942.83", "103.5"));
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "256.9", "103.5", "182", "103.5"));
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "260.9", "89.15", "143", "89.15"));
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "622", "103.5", "688.83", "103.5"));
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "871", "122", "906", "122"));
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "617", "122", "652", "122"));
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "942.33", "189", "942.33", "103"));
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "182.5", "189", "182.5", "103"));
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "143.5", "224.09", "143.5", "89"));
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "688.33", "182.4", "688.33", "103"));
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "182", "189.09", "942.84", "189.09"));
        elements.add(new Line("948d7ce9-e863-45e7-a3e6-179e4cab85de", "143", "223.59", "1173", "223.59"));

        //Beschriftungen
        elements.add(new Text("ffa89ab7-8120-4f9d-b7d7-33e849fbefce", "translate(191.96 180.69)", reportingProcessOverviewController.getStringfromProperty("reporting_ruecklauferInDasFachteam")));
        elements.add(new Text("ffa89ab7-8120-4f9d-b7d7-33e849fbefce", "translate(153.5 215.19)", reportingProcessOverviewController.getStringfromProperty("reporting_neuversioniert")));

        return SVGElementDrawer.drawElements(elements);
    }

    private static String getClock(ReportingProcessOverviewSimpleController reportingProcessOverviewController) {
        List<SVGElement> elements = new ArrayList<>();
        elements.add(new Text("ffa89ab7-8120-4f9d-b7d7-33e849fbefce", "translate(87.05 282.53)", reportingProcessOverviewController.getStringfromProperty("reporting_durchlaufzeitIn")));
        elements.add(new Text("ffa89ab7-8120-4f9d-b7d7-33e849fbefce", "translate(85.05 299.33)", reportingProcessOverviewController.getStringfromProperty("reporting_arbeitstage")));

        String result = SVGElementDrawer.drawElements(elements)
                + "<image width=\"36\" height=\"36\" transform=\"translate(52.91 275.3) scale(0.59)\" xlink:href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAlCAYAAADFniADAAAACXBIWXMAABLqAAAS6gEWyM/fAAACuUlEQVRYR+2YPUhVYRjH/ze0vGFTBJVDQYXODX0IQdBS0VCtEoYtDUVkH9CkBGZNOVWQTUabpDRUUNBU6FotQV1DM9FsNBzy13DOzee+5z3nPV69dIf+8MDDef/P7324XN6vAqB607qQ4V+oLptqCBlS1C6pU9JeSVskbZX0W9JcHGOSnkh6mwbIFJA3moCLQIn8+gxcADYQ5v+NoCGOM8As1WuGiBGaJ3dTPSQ1DwwCx4E2oBnYBLQCJ4CHwE9PXQ/h+YJN3XegX4HzQGMIDKyPvZMO40GoNmvwtgMbIfpfhZpxowkYdli3smrSBrocyEAWJGfcdZhdaV7fxybghyl+BhTSAMBhoBc4m+FRzBg13Dmg6PP6iq+awilgo6/QRG/sfRPwKWZNLePp9vl8K/oVk9+QtODxVKsFRcyyrvlMblMHFK3OklSSNKS115AithTNtd81uE2dMvmoaifLPu0Ouk3tM/kL1U7PTW7nlJRsqsXkJdVOEybf7g5mNTWj2smy7ZySkk0tpeR51CJpW8gUy7ITR1+3qe8mT/ysKXovaVHSbkmfJF2X1JhZUcm2c0pKNjVt8p3Kp2FJbZJeSmqWdEfSR0lHMmp2mNzOKSnZ1LjJjyq/JhT5T0qalLRH0itJT1XZQFnHTD6WGHWW+HazBZR8W0COKAJ9wGLM+QUccjxfWNZBl+ECC8C0KehwC1YQu4DXwAegwXzvMPxpPJu9D9ZtivJsyKHYbPIi8M3wL/tqfJAi0bGirFGyjy55o0B0DCprRUcXAeeo1Foc8gYcZmeaNwvS70BWcxwecVh9WTUhoHtxmGD1F4d7odoQWMBNkpoHHhFdsVqJrlf2ijVIDa9Y5eikzi6j5SgCl6hc+EKq6tpegKrep3wPHEuSZhU9cIxLeizpXRogS9U2VVPV5fvU/6by6g+EMboFf3T09AAAAABJRU5ErkJggg==\"/>\n"
                + "<circle class=\"62cd76c4-80a4-4944-a40d-e6dd289b40b2\" cx=\"63.46\" cy=\"285.84\" r=\"12.5\"/>\n";

        return result;
    }

}
