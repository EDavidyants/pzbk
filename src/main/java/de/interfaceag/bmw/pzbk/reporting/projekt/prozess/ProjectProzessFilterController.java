package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface ProjectProzessFilterController {

    ProjectProcessFilter getFilter();

    String filter();

    String reset();

}
