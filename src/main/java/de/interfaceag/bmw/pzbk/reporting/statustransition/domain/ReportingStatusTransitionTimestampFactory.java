package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import java.io.Serializable;
import java.util.Date;

@Dependent
public class ReportingStatusTransitionTimestampFactory implements Serializable {

    public EntryExitTimeStamp getNewEntryTimeStamp(Date entry) {
        return EntryExitTimeStamp.getBuilder().setEntry(entry).build();
    }

    public EntryExitTimeStamp getNewExitTimeStamp(Date exit) {
        return EntryExitTimeStamp.getBuilder().setExit(exit).build();
    }

    @Produces
    public EntryExitTimeStamp getNewEmptyTimeStamp() {
        return EntryExitTimeStamp.getBuilder().build();
    }

}
