package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot;

import de.interfaceag.bmw.pzbk.reporting.projekt.snapshot.SnapshotWriteService;

public interface ProjectProcessSnapshotWriteService extends SnapshotWriteService<ProjectProcessSnapshot> {
}
