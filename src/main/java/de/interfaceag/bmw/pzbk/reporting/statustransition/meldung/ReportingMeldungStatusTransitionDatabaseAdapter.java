package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;

import java.io.Serializable;
import java.util.Optional;

public interface ReportingMeldungStatusTransitionDatabaseAdapter extends Serializable {

    void save(ReportingMeldungStatusTransition statusTransition);

    Optional<ReportingMeldungStatusTransition> getById(Long id);

    Optional<ReportingMeldungStatusTransition> getLatest(Meldung meldung);

    boolean isHistoryAlreadyExisting(Meldung meldung);

}
