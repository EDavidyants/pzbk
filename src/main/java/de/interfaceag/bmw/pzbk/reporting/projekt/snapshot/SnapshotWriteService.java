package de.interfaceag.bmw.pzbk.reporting.projekt.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;

import java.io.Serializable;

/**
 *
 * @author sl
 */
public interface SnapshotWriteService<T> extends Serializable {

    T createNewSnapshotForDerivat(Derivat derivat);

}
