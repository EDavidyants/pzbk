package de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht;

import de.interfaceag.bmw.pzbk.reporting.excel.ReportingExcelDataDto;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;

import javax.ejb.Stateless;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author evda
 */
@Stateless
@Named
public class AnforderungsuebersichtService implements Serializable {

    public static String getDatumLabel(int keyFigureId) {
        KeyType keyType = KeyType.getKeyTypeById(keyFigureId);

        switch (keyType) {
            case FACHTEAM_CURRENT:
            case TTEAM_CURRENT:
            case VEREINBARUNG_CURRENT:
                return "Datum Eingang";

            case FREIGEGEBEN_CURRENT:
                return "Datum Freigabe";

            case VEREINBARUNG_PZBK:
                return "Datum PZBK Zuordnung";

            case FACHTEAM_NEW_VERSION_ANFORDERUNG:
                return "Datum Neuversionierung";

            case FACHTEAM_GELOESCHT:
            case TTEAM_GELOESCHT:
            case VEREINBARUNG_GELOESCHT:
                return "Datum gelöscht";

            default:
                return "Datum";
        }
    }

    public List<Anforderungsuebersicht> buildFromExcelData(Collection<ReportingExcelDataDto> excelData) {
        List<Anforderungsuebersicht> result = new ArrayList<>();
        if (isDataValid(excelData)) {
            excelData.forEach(record -> {
                String fachId = record.getFachId();
                result.add(new AnforderungsuebersichtDto.Builder()
                        .withId(record.getId())
                        .withFachIdAndVersion(record.getFachId())
                        .withAnforderungstext(record.getAnforderungstext())
                        .withKommentar(record.getKommentar())
                        .withFachteam(record.getFachteam())
                        .withTteam(record.getTteam())
                        .withStatus(record.getStatus())
                        .withDatum(record.getStatusDatum())
                        .withDurchlaufzeit(record.getDurchlaufzeit())
                        .withLanglaufer(record.isLanglaufer())
                        .build());
            });
        }

        result.sort(new AnforderungsuebersichtComparator());

        return result;
    }

    private boolean isDataValid(Collection<ReportingExcelDataDto> excelData) {
        return excelData != null && !excelData.isEmpty();
    }

}
