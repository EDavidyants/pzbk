package de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot;

import de.interfaceag.bmw.pzbk.reporting.projekt.snapshot.SnapshotWriteService;

/**
 *
 * @author sl
 */
public interface StatusabgleichSnapshotWriteService extends SnapshotWriteService<StatusabgleichSnapshot> {

}
