package de.interfaceag.bmw.pzbk.reporting.excel;

import de.interfaceag.bmw.pzbk.enums.ReportingDurchlaufzeitType;
import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.langlaufer.DurchlaufzeitDto;
import de.interfaceag.bmw.pzbk.reporting.langlaufer.DurchlaufzeitService;
import de.interfaceag.bmw.pzbk.reporting.processoverview.ProcessOverviewExcelData;
import de.interfaceag.bmw.pzbk.reporting.processoverview.ProcessOverviewViewData;
import org.apache.poi.ss.usermodel.Workbook;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

/**
 * @author evda
 */
@Named
@Stateless
public class ReportingToExcelService implements Serializable {

    private static final String FILE_NAME = "ReportingData";

    @Inject
    private LocalizationService localizationService;

    @Inject
    private ExcelDownloadService excelDownloadService;

    @Inject
    private DurchlaufzeitService durchlaufzeitService;

    @Inject
    private ReportToExcelDao reportToExcelDao;

    public void downloadExcelExport(ProcessOverviewViewData viewData) {
        Workbook workbook = null;
        ProcessOverviewExcelData processOverviewExcelData = getReportingExcelData(viewData.getProcessOverviewKeyFigures().getKeyFigures());

        for (KeyFigure keyFigure : viewData.getProcessOverviewKeyFigures().getKeyFigures()) {
            workbook = ExcelExportUtil.createExcelExport(workbook, keyFigure.getKeyType(), processOverviewExcelData.getExcelDataForKeyFigure(keyFigure), localizationService.getCurrentLocale());
        }

        if (workbook != null) {
            ExcelExportUtil.sortWorkbookSheetsAlphabetic(workbook);
            excelDownloadService.downloadExcelExport(FILE_NAME, workbook);
        }
    }

    public Collection<ReportingExcelDataDto> getReportingDataForKeyFigure(KeyType keyType, ProcessOverviewViewData viewData) {
        ProcessOverviewExcelData processOverviewExcelData = getReportingExcelData(viewData.getProcessOverviewKeyFigures().getKeyFigures());
        KeyFigure keyFigure = viewData.getProcessOverviewKeyFigures().getKeyFigureByTypeId(keyType.getId());
        return processOverviewExcelData.getExcelDataForKeyFigure(keyFigure);
    }

    public Collection<ReportingExcelDataDto> getReportingDataForKeyFigure(KeyType keyType, ProcessOverviewViewData viewData, ReportingFilter reportingFilter) {
        ProcessOverviewExcelData processOverviewExcelData = getReportingExcelData(viewData.getProcessOverviewKeyFigures().getKeyFigures());
        KeyFigure keyFigure = viewData.getProcessOverviewKeyFigures().getKeyFigureByTypeId(keyType.getId());
        Collection<ReportingExcelDataDto> excelDataForKeyFigure = processOverviewExcelData.getExcelDataForKeyFigure(keyFigure);

        Collection<DurchlaufzeitDto> durchlaufzeiten = getDurchlaufzeitForKeyTypeAndReportingFilter(keyType, reportingFilter);

        for (ReportingExcelDataDto excelData : excelDataForKeyFigure) {

            Optional<DurchlaufzeitDto> durchlaufzeit = durchlaufzeiten.stream().filter(durchlaufzeitDto -> durchlaufzeitDto.getId().equals(excelData.getId())).findFirst();
            if (durchlaufzeit.isPresent()) {
                excelData.setLanglaufer(durchlaufzeit.get().isLanglaufer());
                excelData.setDurchlaufzeit(durchlaufzeit.get().getDurchlaufzeit());
            } else {
                excelData.setLanglaufer(null);
                excelData.setDurchlaufzeit(null);
            }
        }

        return excelDataForKeyFigure;
    }

    private Collection<DurchlaufzeitDto> getDurchlaufzeitForKeyTypeAndReportingFilter(KeyType keyType, ReportingFilter reportingFilter) {
        ReportingDurchlaufzeitType durchlaufzeitType = ReportingDurchlaufzeitTypeUtil.getDurchlaufzeitTypeForKeyType(keyType);
        switch (durchlaufzeitType) {
            case FACHTEAM:
                return durchlaufzeitService.getFachteamDurchlaufzeiten(reportingFilter);
            case TTEAM:
                return durchlaufzeitService.getTteamDurchlaufzeiten(reportingFilter);
            case VEREINBARUNG:
                return durchlaufzeitService.getVereinbarungDurchlaufzeiten(reportingFilter);

            default:
                return Collections.emptyList();
        }

    }

    public void downloadExcelDataForKeyFigure(KeyType keyType, Collection<ReportingExcelDataDto> excelData) {
        Workbook workbook = ExcelExportUtil.createExcelExport(keyType, excelData, localizationService.getCurrentLocale());
        excelDownloadService.downloadExcelExport(FILE_NAME, workbook);
    }

    public void downloadExcelExportForKeyFigure(KeyType keyType, ProcessOverviewViewData viewData) {
        Workbook workbook = ExcelExportUtil.createExcelExport(keyType, getReportingDataForKeyFigure(keyType, viewData), localizationService.getCurrentLocale());
        excelDownloadService.downloadExcelExport(FILE_NAME, workbook);
    }

    public void downloadDevelopmentExport(ProcessOverviewViewData viewData) {
        Workbook workbook = ExcelExportUtil.createDevelopmentExport(viewData.getProcessOverviewKeyFigures().getKeyFigures());
        excelDownloadService.downloadExcelExport("DevelopmentData", workbook);
    }

    private ProcessOverviewExcelData getReportingExcelData(KeyFigureCollection keyFigures) {

        Collection<ReportingExcelDataDto> reportingSupportData = new HashSet<>();

        reportingSupportData.addAll(this.reportToExcelDao.getReportingSupportDataForMeldungen(keyFigures.getAllMeldungIds()));
        reportingSupportData.addAll(this.reportToExcelDao.getReportingSupportDataForAnforderungen(keyFigures.getAllAnforderungIds()));

        return new ProcessOverviewExcelData(reportingSupportData);
    }
}
