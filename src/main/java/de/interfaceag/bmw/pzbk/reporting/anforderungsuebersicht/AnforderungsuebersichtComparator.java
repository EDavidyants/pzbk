package de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht;

import de.interfaceag.bmw.pzbk.comparator.FachIdComparator;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author evda
 */
public class AnforderungsuebersichtComparator implements Comparator<Anforderungsuebersicht>, Serializable {

    @Override
    public int compare(Anforderungsuebersicht anforderungOne, Anforderungsuebersicht anforderungTwo) {
        String fachIdOne = anforderungOne.getFachId();
        String fachIdTwo = anforderungTwo.getFachId();

        String fachIdAndVersionOne = anforderungOne.getFachIdAndVersion();
        String fachIdAndVersionTwo = anforderungTwo.getFachIdAndVersion();

        Integer fullLengthOne = fachIdAndVersionOne.length();
        Integer fullLengthTwo = fachIdAndVersionTwo.length();

        if (fullLengthOne.equals(fullLengthTwo)) {
            if (isMeldung(fachIdOne) && isMeldung(fachIdTwo)) {
                return meldungComparison(anforderungOne, anforderungTwo);

            } else if (isAnforderung(fachIdOne) && isAnforderung(fachIdTwo)) {
                return anforderungComparison(anforderungOne, anforderungTwo);
            }

            return fachIdOne.compareTo(fachIdTwo);
        }

        return fullLengthTwo - fullLengthOne;
    }

    private int anforderungComparison(Anforderungsuebersicht anforderungOne, Anforderungsuebersicht anforderungTwo) {
        String fachIdOne = anforderungOne.getFachId();
        String fachIdTwo = anforderungTwo.getFachId();

        String versionOne = anforderungOne.getVersion();
        String versionTwo = anforderungTwo.getVersion();

        FachIdComparator fachIdComparator = new FachIdComparator();

        int fachIdComparison = fachIdComparator.compare(fachIdTwo, fachIdOne);

        if (fachIdComparison == 0) {
            Integer v1 = Integer.parseInt(versionOne);
            Integer v2 = Integer.parseInt(versionTwo);
            return v2 - v1;

        } else {
            return fachIdComparison;
        }

    }

    private int meldungComparison(Anforderungsuebersicht meldungOne, Anforderungsuebersicht meldungTwo) {
        String fachIdOne = meldungOne.getFachId();
        String fachIdTwo = meldungTwo.getFachId();

        FachIdComparator fachIdComparator = new FachIdComparator();
        return fachIdComparator.compare(fachIdTwo, fachIdOne);
    }

    private boolean isAnforderung(String fachId) {
        return fachId.startsWith("A");
    }

    private boolean isMeldung(String fachId) {
        return fachId.startsWith("M");
    }

}
