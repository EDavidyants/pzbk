package de.interfaceag.bmw.pzbk.reporting;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

/**
 * View Permission Object for the reporting filter.
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ReportingFilterViewPermission implements Serializable {

    private final ViewPermission sensorCocFilter;
    private final ViewPermission technologieFilter;
    private final ViewPermission werkFilter;
    private final ViewPermission derivatFilter;
    private final ViewPermission tteamFilter;
    private final ViewPermission dateFilter;
    private final ViewPermission modulSeTeamFilter;

    public ReportingFilterViewPermission() {
        sensorCocFilter = FalsePermission.get();
        technologieFilter = FalsePermission.get();
        werkFilter = FalsePermission.get();
        derivatFilter = FalsePermission.get();
        tteamFilter = FalsePermission.get();
        dateFilter = FalsePermission.get();
        modulSeTeamFilter = FalsePermission.get();
    }

    public ReportingFilterViewPermission(Set<Rolle> rolesWithPermission) {
        sensorCocFilter = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(rolesWithPermission).get();

        technologieFilter = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(rolesWithPermission).get();

        werkFilter = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(rolesWithPermission).get();

        derivatFilter = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(rolesWithPermission).get();

        modulSeTeamFilter = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.E_COC)
                .compareTo(rolesWithPermission).get();

        tteamFilter = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(rolesWithPermission).get();

        dateFilter = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.E_COC)
                .compareTo(rolesWithPermission).get();
    }

    public boolean isSensorCocFilter() {
        return sensorCocFilter.isRendered();
    }

    public boolean isTechnologieFilter() {
        return technologieFilter.isRendered();
    }

    public boolean isWerkFilter() {
        return werkFilter.isRendered();
    }

    public boolean isDerivatFilter() {
        return derivatFilter.isRendered();
    }

    public boolean isTteamFilter() {
        return tteamFilter.isRendered();
    }

    public boolean isDateFilter() {
        return dateFilter.isRendered();
    }

    public boolean isModulSeTeamFilter() {
        return modulSeTeamFilter.isRendered();
    }
}
