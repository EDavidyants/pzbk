package de.interfaceag.bmw.pzbk.reporting.excel;

import de.interfaceag.bmw.pzbk.enums.ReportingDurchlaufzeitType;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;

import java.io.Serializable;

/**
 *
 * @author evda
 */
final class ReportingDurchlaufzeitTypeUtil implements Serializable {

    private ReportingDurchlaufzeitTypeUtil() {

    }

    static ReportingDurchlaufzeitType getDurchlaufzeitTypeForKeyType(KeyType keyType) {
        switch (keyType) {
            case FACHTEAM_CURRENT:
                return ReportingDurchlaufzeitType.FACHTEAM;

            case TTEAM_CURRENT:
                return ReportingDurchlaufzeitType.TTEAM;

            case VEREINBARUNG_CURRENT:
                return ReportingDurchlaufzeitType.VEREINBARUNG;

            default:
                return ReportingDurchlaufzeitType.UNDEFINED;
        }
    }

}
