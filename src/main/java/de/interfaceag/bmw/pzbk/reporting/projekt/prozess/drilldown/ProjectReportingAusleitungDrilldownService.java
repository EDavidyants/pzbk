package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.drilldown;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures.ProjectReportingAusleitungKeyFigureService;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author lomu
 */
@Named
@Stateless
public class ProjectReportingAusleitungDrilldownService {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingAusleitungKeyFigureService.class);

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public List<ProjectReportingDrilldownDto> getQueryDataForDrilldownG1() {
        LOG.debug("Trying to get project reporting drilldown-data for keyfigure G1");

        QueryPart queryPart = new QueryPartDTO(
                "SELECT new de.interfaceag.bmw.pzbk.reporting.projekt.prozess.drilldown.ProjectReportingDrilldownDto"
                + "(a.id, a.fachId, a.version, a.beschreibungAnforderungDe) "
                + "FROM Anforderung a "
                + "WHERE a.status = :status AND a.prozessbaukasten IS EMPTY ");

        queryPart.put("status", Status.A_FREIGEGEBEN);

        Query query = queryPart.buildGenericQuery(entityManager, Long.class);

        LOG.debug("Excecute query: {}", query.toString());
        List<ProjectReportingDrilldownDto> result = query.getResultList();
        LOG.debug("Retrieved {} entries", result.size());
        return result;
    }

    public List<ProjectReportingDrilldownDto> getQueryDataForDrilldownForAusleitungZakWithoutZakId(Derivat derivat, DerivatStatus derivatStatus) {
        LOG.debug("Trying to get project reporting drilldown-data for keyfigure G2");
        QueryPart queryPart = new QueryPartDTO("");

        queryPart.append("SELECT DISTINCT  "
                + "a.id, a.fachId, a.version, a.beschreibungAnforderungDe ");

        Query query = getBaseQuery(queryPart, derivat, derivatStatus);

        LOG.debug("Excecute query: {}", query.toString());
        List<Object[]> resultList = query.getResultList();
        LOG.debug("Retrieved {} entries", resultList.size());

        List<ProjectReportingDrilldownDto> result = parseObjectArrayToDrillDownDtoListForG2(resultList);
        return result;
    }

    public List<ProjectReportingDrilldownDto> getQueryDataForDrilldownForAusleitungZakWithZakId(Derivat derivat, DerivatStatus derivatStatus) {
        LOG.debug("Trying to get project reporting drilldown-data for keyfigure G2");
        QueryPart queryPart = new QueryPartDTO("");

        queryPart.append("SELECT  "
                + "a.id, a.fachId, a.version, dam.modul.name, zak.zakId, a.beschreibungAnforderungDe, zad.status ");

        Query query = getBaseQuery(queryPart, derivat, derivatStatus);

        LOG.debug("Excecute query: {}", query.toString());
        List<Object[]> resultList = query.getResultList();
        LOG.debug("Retrieved {} entries", resultList.size());

        List<ProjectReportingDrilldownDto> result = parseObjectArrayToDrillDownDtoListForG3(resultList);
        return result;
    }

    private Query getBaseQuery(QueryPart queryPart, Derivat derivat, DerivatStatus derivatStatus) {
        queryPart.append("FROM ZakUebertragung zak "
                + "INNER JOIN zak.derivatAnforderungModul dam "
                + "INNER JOIN zad.anforderung a "
                + "INNER JOIN dam.zuordnungAnforderungDerivat zad "
                + "INNER JOIN zad.derivat d "
                + "WHERE d.id = :derivatId "
                + "AND zad.status IN :zadStatus "
                + "AND zad.zuordnungDerivatStatus = :derivatStatus"
        );
        Collection<Integer> possibleZuordnungStatus = getPossibleZuordnungStatus();
        queryPart.put("derivatId", derivat.getId());
        queryPart.put("zadStatus", possibleZuordnungStatus);
        queryPart.put("derivatStatus", derivatStatus.getId());
        Query query = queryPart.buildGenericQuery(entityManager, Long.class);
        return query;
    }

    private static Collection<Integer> getPossibleZuordnungStatus() {
        return Collections.singletonList(ZuordnungStatus.ZUGEORDNET.getId());
    }

    private List<ProjectReportingDrilldownDto> parseObjectArrayToDrillDownDtoListForG2(List<Object[]> resultList) {
        List<ProjectReportingDrilldownDto> result;

        result = resultList.stream().map(element -> new ProjectReportingDrilldownDto((Long) element[0], (String) element[1], (Integer) element[2], (String) element[3])).collect(Collectors.toList());

        return result;
    }

    private List<ProjectReportingDrilldownDto> parseObjectArrayToDrillDownDtoListForG3(List<Object[]> resultList) {
        List<ProjectReportingDrilldownDto> result;

        result = resultList.stream().map(element -> new ProjectReportingDrilldownDto((Long) element[0], (String) element[1], (Integer) element[2], (String) element[3],
                (String) element[4], (String) element[5], getStatusString(element)))
                .collect(Collectors.toList());

        return result;
    }

    private static String getStatusString(Object[] element) {
        Optional<ZuordnungStatus> status = ZuordnungStatus.getById((Integer) element[6]);
        if (status.isPresent()) {
            return status.get().getStatusBezeichnung();
        } else {
            return "";
        }

    }

}
