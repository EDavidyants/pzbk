package de.interfaceag.bmw.pzbk.reporting.project.pub;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@Stateless
public class ReportingPublicFilterProducer implements Serializable {

    @Inject
    private ReportingPublicFilterFactory factory;

    @Produces
    @FilterPage(Page.REPORTING_PROJECT_PUBLIC_PROCESS)
    public ReportingPublicFilter getPublicProcessViewFilter() {
        final UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();
        return factory.getPublicProcessViewFilter(urlParameter);
    }

    @Produces
    @FilterPage(Page.REPORTING_PROJECT_PUBLIC_DASHBOARD)
    public ReportingPublicFilter getPublicDashboardViewFilter() {
        final UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();
        return factory.getPublicDashboardViewFilter(urlParameter);
    }

    @Produces
    @FilterPage(Page.REPORTING_PROJECT_PUBLIC_STATUSABGLEICH)
    public ReportingPublicFilter getPublicStatusabgleichViewFilter() {
        final UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();
        return factory.getPublicStatusabgleichViewFilter(urlParameter);
    }


}
