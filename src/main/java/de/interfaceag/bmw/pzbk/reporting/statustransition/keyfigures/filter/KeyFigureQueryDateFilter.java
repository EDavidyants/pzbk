package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureQueryUtils;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;

import java.util.Date;
import java.util.Objects;

public final class KeyFigureQueryDateFilter {

    private KeyFigureQueryDateFilter() {

    }

    public static void append(ReportingFilter filter, QueryPart queryPart) {
        Date startDate = filter.getVonDateFilter().getSelected();
        Date endDate = filter.getBisDateFilter().getSelected();

        if (Objects.nonNull(startDate) && Objects.nonNull(endDate)) {
            queryPart.append(" AND r.entryDate BETWEEN ")
                    .append(KeyFigureQueryUtils.getStartDatumForQuery(startDate))
                    .append(" AND ")
                    .append(KeyFigureQueryUtils.getEndDatumForQuery(endDate));
        }
    }

}
