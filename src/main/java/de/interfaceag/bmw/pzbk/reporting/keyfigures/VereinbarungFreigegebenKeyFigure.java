package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class VereinbarungFreigegebenKeyFigure extends AbstractKeyFigure {

    public VereinbarungFreigegebenKeyFigure(Collection<Long> anforderungIds) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(anforderungIds), KeyType.VEREINBARUNG_FREIGEGEBEN);
    }

    public VereinbarungFreigegebenKeyFigure(Collection<Long> ids, Long runTime) {
        super(ids.size(), IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.VEREINBARUNG_FREIGEGEBEN, runTime);
    }

}
