package de.interfaceag.bmw.pzbk.reporting.excel;

import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.Type;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author evda
 */
public final class ReportingExcelDataDto implements Serializable {

    private final Long id;
    private final Type type;
    private final String fachId;
    private final String anforderungstext;
    private final String kommentar;
    private final String fachteam;
    private final String tteam;
    private final Status status;
    private final Date statusDatum;
    private final Date erstellungsdatum;
    private final Date aenderungsdatum;
    private Long durchlaufzeit;
    private Boolean isLanglaufer;

    public ReportingExcelDataDto(Long id, Type type, String fachId, String anforderungstext, String kommentar, String fachteam, Status status, Date statusDatum, Date erstellungsdatum, Date aenderungsdatum, Long durchlaufzeit, Boolean isLanglaufer) {
        this.id = id;
        this.fachId = fachId;
        this.anforderungstext = anforderungstext;
        this.kommentar = kommentar;
        this.fachteam = fachteam;
        this.tteam = "";
        this.status = status;
        this.statusDatum = getTimeStamp(statusDatum);
        this.erstellungsdatum = getTimeStamp(erstellungsdatum);
        this.aenderungsdatum = getTimeStamp(aenderungsdatum);
        this.type = type;
        this.durchlaufzeit = durchlaufzeit;
        this.isLanglaufer = isLanglaufer;
    }

    private ReportingExcelDataDto(Long id, Type type, String fachId, String anforderungstext, String kommentar, String fachteam, String tteam, Status status, Date statusDatum, Date erstellungsdatum, Date aenderungsdatum) {
        this.id = id;
        this.fachId = fachId;
        this.anforderungstext = anforderungstext;
        this.kommentar = kommentar;
        this.fachteam = fachteam;
        this.tteam = tteam;
        this.status = status;
        this.statusDatum = getTimeStamp(statusDatum);
        this.erstellungsdatum = getTimeStamp(erstellungsdatum);
        this.aenderungsdatum = getTimeStamp(aenderungsdatum);
        this.type = type;
    }

    public ReportingExcelDataDto(Long id, Type type, String fachId, String anforderungstext, String kommentar, String fachteam, Status status, Date statusDatum, Date erstellungsdatum, Date aenderungsdatum) {
        this.id = id;
        this.fachId = fachId;
        this.anforderungstext = anforderungstext;
        this.kommentar = kommentar;
        this.fachteam = fachteam;
        this.tteam = "";
        this.status = status;
        this.statusDatum = getTimeStamp(statusDatum);
        this.erstellungsdatum = getTimeStamp(erstellungsdatum);
        this.aenderungsdatum = getTimeStamp(aenderungsdatum);
        this.type = type;
    }

    public static Set<ReportingExcelDataDto> buildFromCollectionWithoutTteam(@NotNull Collection<Object[]> queryResultCollection) {
        Set<ReportingExcelDataDto> result = new HashSet<>();
        Iterator<Object[]> iterator = queryResultCollection.iterator();
        while (iterator.hasNext()) {
            Object[] record = iterator.next();
            Long id = (Long) record[0];
            String fachId = (String) record[1];

            String kennzeichen = fachId.substring(0, 1);
            Type type = Type.getByKennzeichen(kennzeichen);

            String anforderungstext = (String) record[2];
            String kommentar = (String) record[3];
            String fachteam = (String) record[4];
            Status status = (Status) record[5];
            Date statusDatum = (Date) record[6];
            Date erstellungsdatum = (Date) record[7];
            Date aenderungsdatum = (Date) record[8];

            result.add(new ReportingExcelDataDto(id, type, fachId, anforderungstext, kommentar, fachteam, status, statusDatum, erstellungsdatum, aenderungsdatum));
        }
        return result;
    }

    public static Set<ReportingExcelDataDto> buildFromCollectionWithTteam(@NotNull Collection<Object[]> queryResultCollection) {
        Set<ReportingExcelDataDto> result = new HashSet<>();
        Iterator<Object[]> iterator = queryResultCollection.iterator();
        while (iterator.hasNext()) {
            Object[] record = iterator.next();
            Long id = (Long) record[0];
            String fachId = (String) record[1];

            String kennzeichen = fachId.substring(0, 1);
            Type type = Type.getByKennzeichen(kennzeichen);

            String anforderungstext = (String) record[2];
            String kommentar = (String) record[3];
            String fachteam = (String) record[4];
            String tteam = (String) record[5];
            Status status = (Status) record[6];
            Date statusDatum = (Date) record[7];
            Date erstellungsdatum = (Date) record[8];
            Date aenderungsdatum = (Date) record[9];

            result.add(new ReportingExcelDataDto(id, type, fachId, anforderungstext, kommentar, fachteam, tteam, status, statusDatum, erstellungsdatum, aenderungsdatum));
        }
        return result;
    }

    @Override
    public String toString() {
        return fachId;
    }

    public Long getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public String getFachId() {
        return fachId;
    }

    public String getAnforderungstext() {
        return anforderungstext;
    }

    public String getKommentar() {
        return kommentar;
    }

    public String getFachteam() {
        return fachteam;
    }

    public String getTteam() {
        return tteam;
    }

    public Status getStatus() {
        return status;
    }

    public Date getStatusDatum() {
        return getTimeStamp(statusDatum);
    }

    public Date getErstellungsDatum() {
        return getTimeStamp(erstellungsdatum);
    }

    public Date getAenderungsdatum() {
        return getTimeStamp(aenderungsdatum);
    }

    private static Date getTimeStamp(Date datum) {
        return (datum != null) ? new Date(datum.getTime()) : null;
    }

    public void setDurchlaufzeit(Long durchlaufzeit) {
        this.durchlaufzeit = durchlaufzeit;
    }

    public void setLanglaufer(Boolean langlaufer) {
        this.isLanglaufer = langlaufer;
    }

    public Long getDurchlaufzeit() {
        return durchlaufzeit;
    }

    public Boolean isLanglaufer() {
        return isLanglaufer;
    }

}
