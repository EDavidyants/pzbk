package de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht;

import de.interfaceag.bmw.pzbk.enums.Status;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author evda
 */
public class AnforderungsuebersichtGeloeschtUnstimmigDto
        implements AnforderungsuebersichtGeloeschtUnstimmig, Serializable {

    private final AnforderungsuebersichtDto baseData;
    private final String vorigerStatus;
    private final String statusChangeKommentar;

    public AnforderungsuebersichtGeloeschtUnstimmigDto(String fachId, String anforderungstext,
            String kommentar, String fachteam, String tteam, Status status, Date datum,
            String vorigerStatus, String statusChangeKommentar) {

        this.baseData = AnforderungsuebersichtDto.newBuilder()
                .withFachIdAndVersion(fachId)
                .withAnforderungstext(anforderungstext)
                .withKommentar(kommentar)
                .withFachteam(fachteam)
                .withTteam(tteam)
                .withStatus(status)
                .withDatum(datum)
                .build();

        this.vorigerStatus = vorigerStatus;
        this.statusChangeKommentar = statusChangeKommentar;

    }

    @Override
    public String getId() {
        return baseData.getId();
    }

    @Override
    public String getFachId() {
        return baseData.getFachId();
    }

    @Override
    public String getVersion() {
        return baseData.getVersion();
    }

    @Override
    public String getFachIdAndVersion() {
        return baseData.getFachIdAndVersion();
    }

    @Override
    public String getAnforderungstext() {
        return baseData.getAnforderungstext();
    }

    @Override
    public String getAnforderungstextShort() {
        return baseData.getAnforderungstextShort();
    }

    @Override
    public String getKommentar() {
        return baseData.getKommentar();
    }

    @Override
    public String getKommentarShort() {
        return baseData.getKommentarShort();
    }

    @Override
    public Long getDurchlaufzeit() {
        return null;
    }

    @Override
    public Boolean isLanglaufer() {
        return null;
    }

    @Override
    public String getFachteam() {
        return baseData.getFachteam();
    }

    @Override
    public String getTteam() {
        return baseData.getTteam();
    }

    @Override
    public String getStatus() {
        return baseData.getStatus();
    }

    @Override
    public String getDatum() {
        return baseData.getDatum();
    }

    @Override
    public String getVorigerStatus() {
        return vorigerStatus;
    }

    @Override
    public String getStatusChangeKommentar() {
        return statusChangeKommentar;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.baseData);
        hash = 23 * hash + Objects.hashCode(this.vorigerStatus);
        hash = 23 * hash + Objects.hashCode(this.statusChangeKommentar);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnforderungsuebersichtGeloeschtUnstimmigDto other = (AnforderungsuebersichtGeloeschtUnstimmigDto) obj;
        if (!Objects.equals(this.vorigerStatus, other.vorigerStatus)) {
            return false;
        }
        if (!Objects.equals(this.statusChangeKommentar, other.statusChangeKommentar)) {
            return false;
        }
        if (!Objects.equals(this.baseData, other.baseData)) {
            return false;
        }
        return true;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {

        private String fachId;
        private String anforderungstext;
        private String kommentar;
        private String fachteam;
        private String tteam;
        private Status status;
        private Date datum;
        private String vorigerStatus;
        private String statusChangeKommentar;

        public Builder() {

        }

        public Builder withFachId(String fachId) {
            this.fachId = fachId;
            return this;
        }

        public Builder withAnforderungstext(String anforderungstext) {
            this.anforderungstext = anforderungstext;
            return this;
        }

        public Builder withKommentar(String kommentar) {
            this.kommentar = kommentar;
            return this;
        }

        public Builder withFachteam(String fachteam) {
            this.fachteam = fachteam;
            return this;
        }

        public Builder withTteam(String tteam) {
            this.tteam = tteam;
            return this;
        }

        public Builder withStatus(Status status) {
            this.status = status;
            return this;
        }

        public Builder withDatum(Date datum) {
            this.datum = datum;
            return this;
        }

        public Builder withVorigerStatus(String vorigerStatus) {
            this.vorigerStatus = vorigerStatus;
            return this;
        }

        public Builder withStatusChangeKommentar(String statusChangeKommentar) {
            this.statusChangeKommentar = statusChangeKommentar;
            return this;
        }

        public AnforderungsuebersichtGeloeschtUnstimmigDto build() {
            return new AnforderungsuebersichtGeloeschtUnstimmigDto(fachId, anforderungstext, kommentar, fachteam, tteam, status, datum, vorigerStatus, statusChangeKommentar);
        }

    } // end of Builder

}
