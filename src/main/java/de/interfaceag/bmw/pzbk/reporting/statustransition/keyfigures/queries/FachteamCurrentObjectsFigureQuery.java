package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamCurrentObjectsFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Date;

/**
 * @author fn
 */
final class FachteamCurrentObjectsFigureQuery {

    private static final Logger LOG = LoggerFactory.getLogger(FachteamCurrentObjectsFigureQuery.class.getName());

    private FachteamCurrentObjectsFigureQuery() {

    }

    static FachteamCurrentObjectsFigure compute(ReportingFilter filter, EntityManager entityManager) {
        Collection<Long> anforderungIds = getAnforderungIdsForFachteamCurrent(filter, entityManager);
        Collection<Long> meldungIds = getMeldungIdsForFachteamCurrent(filter, entityManager);
        return new FachteamCurrentObjectsFigure(meldungIds, anforderungIds, false);
    }

    private static Collection<Long> getMeldungIdsForFachteamCurrent(ReportingFilter filter, EntityManager entityManager) {
        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO("SELECT m.id ");
        queryPart.append(" FROM ReportingMeldungStatusTransition r ")
                .append(" INNER JOIN r.meldung m ")
                .append(" WHERE (r.gemeldet.entry < :endDate AND r.gemeldet.exit IS NULL ")
                .append(" OR (r.gemeldet.entry < :endDate AND r.gemeldet.exit > :endDate)) ");
        queryPart.put("endDate", endDate);

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getMeldungIdsForFreigegebenEntry Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

    private static Collection<Long> getAnforderungIdsForFachteamCurrent(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO("SELECT a.id ");
        queryPart.append(" FROM ReportingStatusTransition r ")
                .append(" INNER JOIN r.anforderung a ")
                .append(" WHERE (r.inArbeit.entry < :endDate AND r.inArbeit.exit IS NULL ")
                .append(" OR (r.inArbeit.entry < :endDate AND r.inArbeit.exit > :endDate) ");
        appendFachteamCurrentUnstimmig(queryPart);
        queryPart.append(") ");
        queryPart.put("endDate", endDate);

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getAnforderungIdsForFachteamCurrent Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

    private static void appendFachteamCurrentUnstimmig(QueryPart queryPart) {
        appendFachteamUnstimmig(queryPart);
        appendTteamUnstimmig(queryPart);
        appendVereinbarungUnstimmig(queryPart);
    }

    private static void appendFachteamUnstimmig(QueryPart queryPart) {
        queryPart.append(" OR (r.fachteamUnstimmig.entry < :endDate AND r.fachteamUnstimmig.exit IS NULL) ")
                .append(" OR (r.fachteamUnstimmig.entry < :endDate AND r.fachteamUnstimmig.exit > :endDate) ");
    }

    private static void appendTteamUnstimmig(QueryPart queryPart) {
        queryPart.append(" OR (r.tteamUnstimmig.entry < :endDate AND r.tteamUnstimmig.exit IS NULL) ")
                .append(" OR (r.tteamUnstimmig.entry < :endDate AND r.tteamUnstimmig.exit > :endDate) ");
    }

    private static void appendVereinbarungUnstimmig(QueryPart queryPart) {
        queryPart.append(" OR (r.vereinbarungUnstimmig.entry < :endDate AND r.vereinbarungUnstimmig.exit IS NULL) ")
                .append(" OR (r.vereinbarungUnstimmig.entry < :endDate AND r.vereinbarungUnstimmig.exit > :endDate) ");
    }

}
