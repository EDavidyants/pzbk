package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractKeyFigure implements KeyFigure {

    private int value;
    private Collection<IdTypeTuple> idTypeTuples;
    private final KeyType keyType;
    private final Long runTime;

    public AbstractKeyFigure(Collection<IdTypeTuple> idTypeTuples, KeyType keyType) {
        this.value = idTypeTuples.size();
        this.idTypeTuples = idTypeTuples;
        this.keyType = keyType;
        this.runTime = 0L;
    }

    public AbstractKeyFigure(int value, Collection<IdTypeTuple> idTypeTuples, KeyType keyType, Long runTime) {
        this.value = value;
        this.idTypeTuples = idTypeTuples;
        this.keyType = keyType;
        this.runTime = runTime;
    }

    @Override
    public String toString() {
        return keyType + ": {" +
                "value=" + value +
                '}';
    }

    @Override
    public void restrictToIdTypeTuples(IdTypeTupleCollection validIdTypeTuples) {
        if (validIdTypeTuples == null || validIdTypeTuples.isEmpty()) {
            idTypeTuples = Collections.emptyList();
            value = 0;
        } else {
            idTypeTuples.retainAll(validIdTypeTuples);
            value = idTypeTuples.size();
        }
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        AbstractKeyFigure that = (AbstractKeyFigure) object;
        return value == that.value &&
                keyType == that.keyType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, keyType);
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public Collection<IdTypeTuple> getIdTypeTuples() {
        return idTypeTuples;
    }

    @Override
    public KeyType getKeyType() {
        return keyType;
    }

    @Override
    public int getKeyTypeId() {
        return keyType.getId();
    }

    @Override
    public Collection<Long> getMeldungIds() {
        return getIdsForType(Type.MELDUNG);
    }

    @Override
    public Collection<Long> getAnforderungIds() {
        return getIdsForType(Type.ANFORDERUNG);
    }

    private Collection<Long> getIdsForType(Type type) {
        if (idTypeTuples == null || idTypeTuples.isEmpty()) {
            return Collections.emptyList();
        }

        return idTypeTuples.stream()
                .filter(tuple -> tuple.getX().equals(type))
                .map(GenericTuple::getY)
                .collect(Collectors.toList());
    }

    @Override
    public String getMeldungIdsAsString() {
        return getMeldungIds().stream().map(Object::toString).collect(Collectors.joining(","));
    }

    @Override
    public String getAnforderungIdsAsString() {
        return getAnforderungIds().stream().map(Object::toString).collect(Collectors.joining(","));
    }

    @Override
    public String getKeyTypeAsString() {
        return keyType.getBezeichnung();
    }

    @Override
    public boolean isLanglaufer() {
        return Boolean.FALSE;
    }

    @Override
    public Long getRunTime() {
        return runTime;
    }

}
