package de.interfaceag.bmw.pzbk.reporting.excel;

import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.KeyFiguresExcelNamesComparator;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

final class ExcelExportUtil {

    private static final Logger LOG = LoggerFactory.getLogger(ExcelExportUtil.class.getName());

    private ExcelExportUtil() {
    }

    static Workbook createExcelExport(KeyType keyType, Collection<ReportingExcelDataDto> reportingElements, Locale currentLocale) {
        return createExcelExport(null, keyType, reportingElements, currentLocale);
    }

    static Workbook createExcelExport(Workbook workbook, KeyType keyType, Collection<ReportingExcelDataDto> reportingElements, Locale currentLocale) {
        if (workbook == null) {
            workbook = new XSSFWorkbook();
        }

        Sheet sheet = workbook.createSheet(getSheetName(keyType));

        int rowIndex = 0;
        Row headerRow = sheet.createRow(rowIndex);

        Cell fachIdCell = headerRow.createCell(0);
        fachIdCell.setCellValue(LocalizationService.getValue(currentLocale, "fachId"));

        Cell anforderungstextCell = headerRow.createCell(1);
        anforderungstextCell.setCellValue(LocalizationService.getValue(currentLocale, "anforderung_reporting_anforderungstext"));

        Cell kommentarCell = headerRow.createCell(2);
        kommentarCell.setCellValue(LocalizationService.getValue(currentLocale, "anforderung_reporting_kommentar"));

        Cell fachteamCell = headerRow.createCell(3);
        fachteamCell.setCellValue(LocalizationService.getValue(currentLocale, "anforderung_reporting_fachteam"));

        Cell tteamCell = headerRow.createCell(4);
        tteamCell.setCellValue(LocalizationService.getValue(currentLocale, "anforderung_reporting_tteam"));

        Cell statusCell = headerRow.createCell(5);
        statusCell.setCellValue(LocalizationService.getValue(currentLocale, "status"));

        Cell statusDatumCell = headerRow.createCell(6);
        statusDatumCell.setCellValue(LocalizationService.getValue(currentLocale, "anforderung_reporting_statusDatum"));

        Cell erstellungsdatumCell = headerRow.createCell(7);
        erstellungsdatumCell.setCellValue(LocalizationService.getValue(currentLocale, "erstellungsdatum"));

        Cell aenderungsdatumCell = headerRow.createCell(8);
        aenderungsdatumCell.setCellValue(LocalizationService.getValue(currentLocale, "aenderungsdatum"));

        CellStyle headerStyle = createHeaderCellStyle(workbook);
        fachIdCell.setCellStyle(headerStyle);
        anforderungstextCell.setCellStyle(headerStyle);
        kommentarCell.setCellStyle(headerStyle);
        fachteamCell.setCellStyle(headerStyle);
        tteamCell.setCellStyle(headerStyle);
        statusCell.setCellStyle(headerStyle);
        statusDatumCell.setCellStyle(headerStyle);
        erstellungsdatumCell.setCellStyle(headerStyle);
        aenderungsdatumCell.setCellStyle(headerStyle);

        if (isForDurchlaufzeitBerechnungRelevant(keyType)) {
            createHeaderForDurchlaufzeitAndLanglaufer(headerRow, headerStyle, currentLocale);
        }

        if (reportingElements == null || reportingElements.isEmpty()) {
            LOG.warn("ReportingElement Liste fuer KeyType {} ist leer", keyType.name());
            return workbook;
        }

        Row row;
        for (ReportingExcelDataDto reportingExcelDataDto : reportingElements) {
            rowIndex++;
            row = sheet.createRow(rowIndex);

            row.createCell(0).setCellValue(reportingExcelDataDto.getFachId());
            row.createCell(1).setCellValue(reportingExcelDataDto.getAnforderungstext());
            row.createCell(2).setCellValue(reportingExcelDataDto.getKommentar());
            row.createCell(3).setCellValue(reportingExcelDataDto.getFachteam());
            row.createCell(4).setCellValue(reportingExcelDataDto.getTteam());
            row.createCell(5).setCellValue(reportingExcelDataDto.getStatus().getStatusBezeichnung());
            row.createCell(6).setCellValue(new SimpleDateFormat("dd.MM.yyyy").format(reportingExcelDataDto.getStatusDatum()));
            row.createCell(7).setCellValue(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(reportingExcelDataDto.getErstellungsDatum()));
            row.createCell(8).setCellValue(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(reportingExcelDataDto.getAenderungsdatum()));

            if (isForDurchlaufzeitBerechnungRelevant(keyType)) {
                createDataRowsDataForDurchlaufzeitAndLanglaufer(row, reportingExcelDataDto);
            }
        }

        return workbook;
    }

    private static void createDataRowsDataForDurchlaufzeitAndLanglaufer(Row row, ReportingExcelDataDto reportingExcelDataDto) {
        if (reportingExcelDataDto.getDurchlaufzeit() == null) {
            row.createCell(9).setCellValue(0);
        } else {
            row.createCell(9).setCellValue(reportingExcelDataDto.getDurchlaufzeit());
        }

        if (reportingExcelDataDto.isLanglaufer() == null) {
            row.createCell(10).setCellValue(0);
        } else {
            row.createCell(10).setCellValue(reportingExcelDataDto.isLanglaufer());
        }
    }

    private static void createHeaderForDurchlaufzeitAndLanglaufer(Row row, CellStyle cellStyle, Locale currentLocale) {
        Cell durchlaufzeit = row.createCell(9);
        Cell langlaufer = row.createCell(10);

        durchlaufzeit.setCellValue(LocalizationService.getValue(currentLocale, "anforderung_reporting_durchlaufzeit"));
        langlaufer.setCellValue(LocalizationService.getValue(currentLocale, "anforderung_reporting_langlaufer"));

        durchlaufzeit.setCellStyle(cellStyle);
        langlaufer.setCellStyle(cellStyle);
    }

    private static boolean isForDurchlaufzeitBerechnungRelevant(KeyType keyType) {
        return keyType == KeyType.FACHTEAM_CURRENT || keyType == KeyType.TTEAM_CURRENT || keyType == KeyType.VEREINBARUNG_CURRENT;
    }

    static void sortWorkbookSheetsAlphabetic(Workbook workbook) {

        List<String> sheetNames = new ArrayList<>();

        Iterator<Sheet> sheetsIterator = workbook.sheetIterator();
        int sheetCounter = 0;

        Sheet sheet;
        while (sheetsIterator.hasNext()) {
            sheet = sheetsIterator.next();
            sheetCounter++;
            String sheetName = sheet.getSheetName();
            sheetNames.add(sheetName);
        }

        sheetNames.sort(new KeyFiguresExcelNamesComparator());

        String sheetname;
        for (int i = 0; i < sheetCounter; i++) {
            sheetname = sheetNames.get(i);
            workbook.setSheetOrder(sheetname, i);
        }
    }

    static Workbook createDevelopmentExport(KeyFigureCollection keyFigures) {
        Workbook workbook = new XSSFWorkbook();

        String sheetName = "Runtime for Keyfigures";
        Sheet sheet = workbook.createSheet(sheetName);
        if (keyFigures != null && !keyFigures.isEmpty()) {
            int rowIndex = 0;
            Row headerRow = sheet.createRow(rowIndex);
            CellStyle headerStyle = createHeaderCellStyle(workbook);

            Cell keyFigureCell = headerRow.createCell(0);
            Cell runtimeCell = headerRow.createCell(1);
            Cell timeUnitCell = headerRow.createCell(2);

            keyFigureCell.setCellValue("KeyFigure");
            runtimeCell.setCellValue("Runtime");
            timeUnitCell.setCellValue("Unit");

            keyFigureCell.setCellStyle(headerStyle);
            runtimeCell.setCellStyle(headerStyle);
            timeUnitCell.setCellStyle(headerStyle);

            Row row;
            for (KeyFigure keyFigure : keyFigures) {
                rowIndex++;
                row = sheet.createRow(rowIndex);

                row.createCell(0).setCellValue(keyFigure.getKeyTypeAsString());
                row.createCell(1).setCellValue(keyFigure.getRunTime());
                row.createCell(2).setCellValue("ms");

            }
        } else {
            LOG.warn("KeyFigures Liste ist leer");
        }

        return workbook;
    }

    private static String getSheetName(KeyType keyType) {
        return keyType.getId() + "_" + keyType.name();
    }

    private static CellStyle createHeaderCellStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 12);
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        style.setFont(font);
        return style;
    }
}
