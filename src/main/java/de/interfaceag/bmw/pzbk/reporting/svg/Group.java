package de.interfaceag.bmw.pzbk.reporting.svg;

import java.util.Collections;
import java.util.List;

/**
 *
 * @author lomu
 */
public class Group implements SVGElement {

    private final String groupClass;
    private final List<SVGElement> children;

    public Group(String groupClass, List<SVGElement> children) {
        this.groupClass = groupClass;
        this.children = children;
    }

    public Group(String groupClass, SVGElement child) {
        this.groupClass = groupClass;
        this.children = Collections.singletonList(child);
    }

    public String getGroupClass() {
        return groupClass;
    }

    @Override
    public String draw() {
        StringBuilder result = new StringBuilder();
        result.append("<g ");

        if (this.getGroupClass() != null) {
            result.append("class=\"").append(this.getGroupClass());
        }
        result.append("\">");
        result.append(SVGElementDrawer.drawElements(children));

        result.append("</g>");

        return result.toString();
    }

}
