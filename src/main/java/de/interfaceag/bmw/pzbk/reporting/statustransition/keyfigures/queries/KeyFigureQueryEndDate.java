package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;

import java.util.Date;

public final class KeyFigureQueryEndDate {

    private KeyFigureQueryEndDate() {
    }

    public static Date getEndDate(ReportingFilter filter) {
        if (filter.getBisDateFilter().isActive()) {
            final DateSearchFilter bisDateFilter = filter.getBisDateFilter();
            return bisDateFilter.getSelected();
        } else {
            return new Date();
        }
    }
}
