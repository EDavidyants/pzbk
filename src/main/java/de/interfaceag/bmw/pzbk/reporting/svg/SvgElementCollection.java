package de.interfaceag.bmw.pzbk.reporting.svg;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class SvgElementCollection extends AbstractList<SVGElement> {

    private final List<SVGElement> elements;

    public SvgElementCollection() {
        this.elements = new ArrayList<>();
    }

    @Override
    public boolean add(SVGElement tooltip) {
        return elements.add(tooltip);
    }

    @Override
    public Iterator<SVGElement> iterator() {
        return elements.iterator();
    }

    @Override
    public int size() {
        return elements.size();
    }

    public String draw() {
        List<SVGElement> svgElements = new ArrayList<>(elements);
        return SVGElementDrawer.drawElements(svgElements);
    }

    public static SvgElementCollection of(SVGElement... element) {
        SvgElementCollection collection = new SvgElementCollection();
        collection.addAll(Arrays.asList(element));
        return collection;
    }

    @Override
    public SVGElement get(int index) {
        return elements.get(index);
    }

}
