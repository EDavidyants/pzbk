package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author sl
 */
public final class ProjectReportingUmsetzungsverwaltungKeyFigureUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingUmsetzungsverwaltungKeyFigureUtils.class);

    private ProjectReportingUmsetzungsverwaltungKeyFigureUtils() {
    }

    protected static Map<Integer, ProjectReportingProcessKeyFigureData> computeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBG(Map<Integer, ProjectReportingProcessKeyFigureData> result, Double thresholdForGreenAmpel) {
        ProjectReportingProcessKeyFigureData ratioKeyFigureForUmsetzungsbestaetigungBBG = computeRatioKeyFigureForUmsetzungsbestaetigungBBG(result, thresholdForGreenAmpel);
        result.put(ratioKeyFigureForUmsetzungsbestaetigungBBG.getKeyFigure().getId(), ratioKeyFigureForUmsetzungsbestaetigungBBG);

        ProjectReportingProcessKeyFigureData positiveEntryFigureForUmsetzungsbestaetigungBBG = computePositiveEntryFigureForUmsetzungsbestaetigungBBG(result);
        result.put(positiveEntryFigureForUmsetzungsbestaetigungBBG.getKeyFigure().getId(), positiveEntryFigureForUmsetzungsbestaetigungBBG);

        return result;
    }

    protected static Map<Integer, ProjectReportingProcessKeyFigureData> computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBG(Map<Integer, ProjectReportingProcessKeyFigureData> result, Double thresholdForGreenAmpel) {
        ProjectReportingProcessKeyFigureData ratioKeyFigureForUmsetzungsbestaetigungVBBG = computeRatioKeyFigureForUmsetzungsbestaetigungVBBG(result, thresholdForGreenAmpel);
        result.put(ratioKeyFigureForUmsetzungsbestaetigungVBBG.getKeyFigure().getId(), ratioKeyFigureForUmsetzungsbestaetigungVBBG);

        ProjectReportingProcessKeyFigureData positiveEntryFigureForUmsetzungsbestaetigungVKBG = computePositiveEntryFigureForUmsetzungsbestaetigungVBBG(result);
        result.put(positiveEntryFigureForUmsetzungsbestaetigungVKBG.getKeyFigure().getId(), positiveEntryFigureForUmsetzungsbestaetigungVKBG);

        return result;
    }

    protected static Map<Integer, ProjectReportingProcessKeyFigureData> computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBG(Map<Integer, ProjectReportingProcessKeyFigureData> result, Double thresholdForGreenAmpel) {
        ProjectReportingProcessKeyFigureData ratioKeyFigureForUmsetzungsbestaetigungVKBG = computeRatioKeyFigureForUmsetzungsbestaetigungVKBG(result, thresholdForGreenAmpel);
        result.put(ratioKeyFigureForUmsetzungsbestaetigungVKBG.getKeyFigure().getId(), ratioKeyFigureForUmsetzungsbestaetigungVKBG);

        ProjectReportingProcessKeyFigureData positiveEntryFigureForUmsetzungsbestaetigungVKBG = computePositiveEntryFigureForUmsetzungsbestaetigungVKBG(result);
        result.put(positiveEntryFigureForUmsetzungsbestaetigungVKBG.getKeyFigure().getId(), positiveEntryFigureForUmsetzungsbestaetigungVKBG);

        return result;
    }

    private static ProjectReportingProcessKeyFigureData computePositiveEntryFigureForUmsetzungsbestaetigungVKBG(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures) {

        Collection<ProjectReportingProcessKeyFigureData> summands = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures,
                ProjectReportingProcessKeyFigure.L12,
                ProjectReportingProcessKeyFigure.L13,
                ProjectReportingProcessKeyFigure.L14,
                ProjectReportingProcessKeyFigure.L15);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungVKBG summands {}", summands);

        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeSumKeyFigure(ProjectReportingProcessKeyFigure.L10, summands);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungVKBG result {}", result);

        return result;
    }

    private static ProjectReportingProcessKeyFigureData computePositiveEntryFigureForUmsetzungsbestaetigungVBBG(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures) {

        Collection<ProjectReportingProcessKeyFigureData> summands = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures,
                ProjectReportingProcessKeyFigure.R14,
                ProjectReportingProcessKeyFigure.R15,
                ProjectReportingProcessKeyFigure.R16,
                ProjectReportingProcessKeyFigure.R17);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungVBBG summands {}", summands);

        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeSumKeyFigure(ProjectReportingProcessKeyFigure.R12, summands);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungVBBG result {}", result);

        return result;
    }

    private static ProjectReportingProcessKeyFigureData computePositiveEntryFigureForUmsetzungsbestaetigungBBG(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures) {

        Collection<ProjectReportingProcessKeyFigureData> summands = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures,
                ProjectReportingProcessKeyFigure.R21,
                ProjectReportingProcessKeyFigure.R22,
                ProjectReportingProcessKeyFigure.R23,
                ProjectReportingProcessKeyFigure.R24);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungBBG summands {}", summands);

        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeSumKeyFigure(ProjectReportingProcessKeyFigure.R19, summands);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungBBG result {}", result);

        return result;
    }

    private static ProjectReportingProcessKeyFigureData computeRatioKeyFigureForUmsetzungsbestaetigungVKBG(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures, Double thresholdForGreenAmpel) {
        Collection<ProjectReportingProcessKeyFigureData> numerator = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures, ProjectReportingProcessKeyFigure.L12);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungVKBG numerator {}", numerator);

        Collection<ProjectReportingProcessKeyFigureData> denominator = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures,
                ProjectReportingProcessKeyFigure.L12,
                ProjectReportingProcessKeyFigure.L13,
                ProjectReportingProcessKeyFigure.L14,
                ProjectReportingProcessKeyFigure.L15);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungVKBG denominator {}", denominator);

        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeRatioKeyFigure(ProjectReportingProcessKeyFigure.L11, numerator, denominator, thresholdForGreenAmpel);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungVKBG result {}", result);

        return result;
    }

    private static ProjectReportingProcessKeyFigureData computeRatioKeyFigureForUmsetzungsbestaetigungVBBG(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures, Double thresholdForGreenAmpel) {
        Collection<ProjectReportingProcessKeyFigureData> numerator = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures, ProjectReportingProcessKeyFigure.R14);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungVBBG numerator {}", numerator);

        Collection<ProjectReportingProcessKeyFigureData> denominator = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures,
                ProjectReportingProcessKeyFigure.R14,
                ProjectReportingProcessKeyFigure.R15,
                ProjectReportingProcessKeyFigure.R16,
                ProjectReportingProcessKeyFigure.R17);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungVBBG denominator {}", denominator);

        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeRatioKeyFigure(ProjectReportingProcessKeyFigure.R13, numerator, denominator, thresholdForGreenAmpel);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungVBBG result {}", result);

        return result;
    }

    private static ProjectReportingProcessKeyFigureData computeRatioKeyFigureForUmsetzungsbestaetigungBBG(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures, Double thresholdForGreenAmpel) {
        Collection<ProjectReportingProcessKeyFigureData> numerator = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures, ProjectReportingProcessKeyFigure.R21);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungBBG numerator {}", numerator);

        Collection<ProjectReportingProcessKeyFigureData> denominator = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures,
                ProjectReportingProcessKeyFigure.R21,
                ProjectReportingProcessKeyFigure.R22,
                ProjectReportingProcessKeyFigure.R23,
                ProjectReportingProcessKeyFigure.R24);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungBBG denominator {}", denominator);

        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeRatioKeyFigure(ProjectReportingProcessKeyFigure.R20, numerator, denominator, thresholdForGreenAmpel);
        LOG.debug("RatioKeyFigureForUmsetzungsbestaetigungBBG result {}", result);

        return result;
    }

}
