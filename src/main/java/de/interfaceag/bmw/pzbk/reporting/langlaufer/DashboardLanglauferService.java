package de.interfaceag.bmw.pzbk.reporting.langlaufer;

import java.util.List;

public interface DashboardLanglauferService {

    List<Long> getFachteamLanglauferMeldungIds();

    List<Long> getTteamLanglauferIds();

    List<Long> getFachteamLanglauferAnforderungIds();

}
