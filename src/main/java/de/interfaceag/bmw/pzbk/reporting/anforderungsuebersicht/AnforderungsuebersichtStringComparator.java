package de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht;

import de.interfaceag.bmw.pzbk.comparator.FachIdComparator;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author evda
 */
public class AnforderungsuebersichtStringComparator implements Comparator<String>, Serializable {

    @Override
    public int compare(String fachIdAndVersionOne, String fachIdAndVersionTwo) {

        if (isMeldung(fachIdAndVersionOne) && isMeldung(fachIdAndVersionTwo)) {
            return meldungComparison(fachIdAndVersionOne, fachIdAndVersionTwo);

        } else if (isAnforderung(fachIdAndVersionOne) && isAnforderung(fachIdAndVersionTwo)) {
            return anforderungComparison(fachIdAndVersionOne, fachIdAndVersionTwo);
        }

        return (isMeldung(fachIdAndVersionOne)) ? 1 : -1;

    }

    private int anforderungComparison(String fachIdAndVersionOne, String fachIdAndVersionTwo) {
        String splitOn = "\\s";

        String[] fidv1 = fachIdAndVersionOne.split(splitOn);
        String[] fidv2 = fachIdAndVersionTwo.split(splitOn);

        String fachIdOne = fidv1[0];
        String fachIdTwo = fidv2[0];

        Integer f1 = Integer.parseInt(fachIdOne.substring(1));
        Integer f2 = Integer.parseInt(fachIdTwo.substring(1));

        int fachIdComparison = f2 - f1;

        if (fachIdComparison == 0) {
            String versionOne = fidv1[2];
            String versionTwo = fidv2[2];

            Integer v1 = Integer.parseInt(versionOne.substring(1));
            Integer v2 = Integer.parseInt(versionTwo.substring(1));
            return v2 - v1;

        } else {
            return fachIdComparison;
        }

    }

    private int meldungComparison(String fachIdOne, String fachIdTwo) {
        FachIdComparator fachIdComparator = new FachIdComparator();
        return fachIdComparator.compare(fachIdTwo, fachIdOne);
    }

    private boolean isAnforderung(String fachId) {
        return fachId.startsWith("A");
    }

    private boolean isMeldung(String fachId) {
        return fachId.startsWith("M");
    }

}
