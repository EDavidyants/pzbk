package de.interfaceag.bmw.pzbk.reporting.svg;

import de.interfaceag.bmw.pzbk.shared.dto.Point;

import java.util.Arrays;
import java.util.List;

/**
 * @author lomu
 */
public class BoxPolygon implements SVGElement {

    private final int x;
    private final int y;
    private final Box.BoxType boxType;
    private final PolygonType polygonType;

    public BoxPolygon(int xCoordinate, int yCoordinate, Box.BoxType boxType, PolygonType polygonType) {
        this.x = xCoordinate;
        this.y = yCoordinate;
        this.boxType = boxType;
        this.polygonType = polygonType;
    }

    @Override
    public String draw() {
        StringBuilder result = new StringBuilder();
        result.append("<g class=\"").append(polygonType.gClass).append("\">\n");
        result.append("<g class=\"").append(boxType.getBoxClass()).append("\">\n");

        List<Point> points = Arrays.asList(
                new Point(x + 5.0, y + 6.0),
                new Point(x + 22.0, y + 71.0),
                new Point(x + 5.0, y + 136.0),
                new Point(x + 111, y + 136),
                new Point(x + 129, y + 71),
                new Point(x + 111, y + 6)
        );

        result.append(new Polygon(polygonType.getPolygon1Class(), points).draw());
        result.append(new Polygon(polygonType.getPolygon2Class(), points).draw());

        result.append("</g>").append("</g>");

        return result.toString();
    }

    public enum PolygonType {

        SIMPLE("f8b67d6e-2a5b-4c45-b1fa-aaeb448e4c8b", "fdd48387-9079-4f7f-a6dc-c6966447a7a9", "948d7ce9-e863-45e7-a3e6-179e4cab85de"),
        DETAIL("2d24508d-5474-4519-8463-2fe54f9ec290", "b675cbc6-7699-4775-a8c4-02ce9ee8377b", "01e056ae-e1a3-4d3c-bc83-4c867c3167ae");

        private final String gClass;
        private final String polygon1Class;
        private final String polygon2Class;

        PolygonType(String gClass, String polygon1Class, String polygon2Class) {
            this.gClass = gClass;
            this.polygon1Class = polygon1Class;
            this.polygon2Class = polygon2Class;
        }

        public String getgClass() {
            return gClass;
        }

        public String getPolygon1Class() {
            return polygon1Class;
        }

        public String getPolygon2Class() {
            return polygon2Class;
        }

    }

}
