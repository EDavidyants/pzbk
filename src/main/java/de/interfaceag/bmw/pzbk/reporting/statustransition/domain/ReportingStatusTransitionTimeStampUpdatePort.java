package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import java.io.Serializable;
import java.util.Date;

public interface ReportingStatusTransitionTimeStampUpdatePort extends Serializable {

    EntryExitTimeStamp updateEntryForTimeStamp(EntryExitTimeStamp timeStamp, Date date);

    EntryExitTimeStamp removeEntryForTimeStamp(EntryExitTimeStamp timeStamp);

    EntryExitTimeStamp updateExitForTimeStamp(EntryExitTimeStamp timeStamp, Date date);

    EntryExitTimeStamp removeExitForTimeStamp(EntryExitTimeStamp timeStamp);

    EntryExitTimeStamp clearTimeStamp(EntryExitTimeStamp timeStamp);

}
