package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.Map;

/**
 *
 * @author evda
 */
public final class ReportingStatusabgleichExcelExportUtils {

    private static final String FILENAME = "Reporting Statusabgleich";
    private static final int ZAK_STATUS_ANZAHL = 7;

    private ReportingStatusabgleichExcelExportUtils() {

    }

    public static String generateFileName(String derivatName) {
        return FILENAME.concat(" ").concat(derivatName);
    }

    public static Workbook generateExcelFileForStatusabgleich(ReportingStatusabgleichViewData data, String sheetName, String standName, Map<String, String> amToolStatusNames, Map<String, String> zakStatusNames) {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet(sheetName);

        createSheetHeaderRow(sheet, standName, zakStatusNames.get("reporting_statusmatrix_zakstatus"));
        createTableHeaderRowWithZakStatus(sheet, zakStatusNames);

        createRowWithColumnTotals(sheet, amToolStatusNames.get("reporting_statusmatrix_amtoolstatus"), data);
        createRowForAMToolStatusAngenommenUmgesetzt(sheet, amToolStatusNames.get("reporting_statusmatrix_angenommenUmgesetzt"), data);
        createRowForAMToolStatusInKlaerung(sheet, amToolStatusNames.get("reporting_statusmatrix_inklarung"), data);
        createRowForAMToolStatusAbzustimmen(sheet, amToolStatusNames.get("reporting_statusmatrix_abzustimmen"), data);
        createRowForAMToolStatusNichtBewertbar(sheet, amToolStatusNames.get("reporting_statusmatrix_nichtBewertbar"), data);
        createRowForAMToolStatusKeineWeiterverfolgung(sheet, amToolStatusNames.get("reporting_statusmatrix_keineWeiterverfolgung"), data);
        createRowForAMToolStatusUnzureichendeAnforderungsqualitaet(sheet, amToolStatusNames.get("reporting_statusmatrix_unzureichendeAnforderungsqualitaet"), data);

        return workbook;
    }

    private static void createSheetHeaderRow(Sheet sheet, String standName, String zakStatusLabel) {
        Row row = sheet.createRow(0);

        createCellWithValue(row, 0, standName);
        row.createCell(1);
        createCellWithValue(row, 2, zakStatusLabel);
    }

    private static void createTableHeaderRowWithZakStatus(Sheet sheet, Map<String, String> zakStatusNames) {

        int zakStatusNumber = zakStatusNames.size();
        Row row = sheet.createRow(1);

        row.createCell(0);
        row.createCell(1);

        for (int i = 2; i <= zakStatusNumber; i++) {
            String cellValue = zakStatusNames.get(getZakStatusKeyForMatrixColumnIndex(i - 1));
            createCellWithValue(row, i, cellValue);
        }
    }

    private static String getZakStatusKeyForMatrixColumnIndex(int matrixColumnIndex) {
        switch (matrixColumnIndex) {
            case 1:
                return "reporting_statusmatrix_bestaetigt";
            case 2:
                return "reporting_statusmatrix_bestaetigtAusVorprozess";
            case 3:
                return "reporting_statusmatrix_offen";
            case 4:
                return "reporting_statusmatrix_zuUebertragung";
            case 5:
                return "reporting_statusmatrix_nichtBewertbar";
            case 6:
                return "reporting_statusmatrix_nichtUmsetzbar";
            case 7:
                return "reporting_statusmatrix_zuruckgezogen";
            default:
                return "reporting_statusmatrix_zakstatus";
        }
    }

    private static void createRowWithColumnTotals(Sheet sheet, String amToolStatusLabel, ReportingStatusabgleichViewData data) {
        Row row = sheet.createRow(2);
        buildTotalsRow(row, amToolStatusLabel, data);
    }

    private static void createRowForAMToolStatusAngenommenUmgesetzt(Sheet sheet, String amToolStatusName, ReportingStatusabgleichViewData data) {
        Row row = sheet.createRow(3);
        buildRow(row, amToolStatusName, data);
    }

    private static void createRowForAMToolStatusInKlaerung(Sheet sheet, String amToolStatusName, ReportingStatusabgleichViewData data) {
        Row row = sheet.createRow(4);
        buildRow(row, amToolStatusName, data);
    }

    private static void createRowForAMToolStatusAbzustimmen(Sheet sheet, String amToolStatusName, ReportingStatusabgleichViewData data) {
        Row row = sheet.createRow(5);
        buildRow(row, amToolStatusName, data);
    }

    private static void createRowForAMToolStatusNichtBewertbar(Sheet sheet, String amToolStatusName, ReportingStatusabgleichViewData data) {
        Row row = sheet.createRow(6);
        buildRow(row, amToolStatusName, data);
    }

    private static void createRowForAMToolStatusKeineWeiterverfolgung(Sheet sheet, String amToolStatusName, ReportingStatusabgleichViewData data) {
        Row row = sheet.createRow(7);
        buildRow(row, amToolStatusName, data);
    }

    private static void createRowForAMToolStatusUnzureichendeAnforderungsqualitaet(Sheet sheet, String amToolStatusName, ReportingStatusabgleichViewData data) {
        Row row = sheet.createRow(8);
        buildRow(row, amToolStatusName, data);
    }

    private static void buildTotalsRow(Row row, String rowHeader, ReportingStatusabgleichViewData data) {
        int matrixRowIndex = getMatrixRowIndex(row);

        if (matrixRowIndex == -1) {
            return;
        }

        buildAmToolStatusCell(row, rowHeader);
        buildSumSymbolCellForRow(row);

        for (int matrixColumnIndex = 1; matrixColumnIndex <= ZAK_STATUS_ANZAHL; matrixColumnIndex++) {
            String cellValue = data.getColumnSum(matrixColumnIndex);
            createCellWithValue(row, (matrixColumnIndex + 1), cellValue);
        }
    }

    private static void buildSumSymbolCellForRow(Row row) {
        char sumChar = '\u01A9';
        String cellValue = Character.toString(sumChar);
        createCellWithValue(row, 1, cellValue);
    }

    private static void createCellWithValue(Row row, int columnIndex, String cellValue) {
        Cell cell = row.createCell(columnIndex);
        cell.setCellValue(cellValue);
    }

    private static void buildRow(Row row, String rowHeader, ReportingStatusabgleichViewData data) {
        int matrixRowIndex = getMatrixRowIndex(row);

        if (matrixRowIndex == -1) {
            return;
        }

        buildAmToolStatusCell(row, rowHeader);
        createTotalsCellForRow(row, data);

        for (int matrixColumnIndex = 1; matrixColumnIndex <= ZAK_STATUS_ANZAHL; matrixColumnIndex++) {
            String cellValue = data.getMatrixValue(matrixRowIndex, matrixColumnIndex);
            createCellWithValue(row, (matrixColumnIndex + 1), cellValue);
        }
    }

    private static int getMatrixRowIndex(Row row) {
        int offset = 2;
        int matrixRowIndex = row.getRowNum() - offset;

        if (matrixRowIndex < 0) {
            return -1;
        }

        return matrixRowIndex;
    }

    private static void createTotalsCellForRow(Row row, ReportingStatusabgleichViewData data) {
        int matrixRowIndex = getMatrixRowIndex(row);

        if (matrixRowIndex == -1) {
            return;
        }

        String cellValue = data.getRowSum(matrixRowIndex);
        createCellWithValue(row, 1, cellValue);
    }

    private static void buildAmToolStatusCell(Row row, String rowHeader) {
        createCellWithValue(row, 0, rowHeader);
    }

}
