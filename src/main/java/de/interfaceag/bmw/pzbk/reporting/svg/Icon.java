package de.interfaceag.bmw.pzbk.reporting.svg;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class Icon implements SVGElement {

    public final double x;
    public final double y;

    public final String transform;
    public final IconType iconType;
    public final String gClass1;
    public final String gClass2;

    public Icon(double xValue, double yValue, String transform, IconType iconType, String gClass1, String gClass2) {
        this.x = xValue;
        this.y = yValue;
        this.transform = transform;
        this.iconType = iconType;
        this.gClass1 = gClass1;
        this.gClass2 = gClass2;
    }

    @Override
    public String draw() {

        StringBuilder sb = new StringBuilder();
        sb.append("<g class=\"a7a7d0c6-d834-4f16-aa8c-4d2cc48187d8\">\n");
        sb.append("<path class=\"1fc087fc-5ad7-4044-a16b-f1da2c7b2c10\" d=\"M").append(x).append(",").append(y + 25.52).append("A12.5,12.5,0,1,0,").append(x - 12.5).append(",").append(y + 13).append("a12.5,12.5,0,0,0,12.5,12.5\" transform=\"translate(0 -0.02)\"></path>\n");
        sb.append("</g>");
        sb.append("<g class=\"").append(gClass1).append("\">\n");
        sb.append("<g class=\"").append(gClass2).append("\">\n");
        sb.append("<use transform=\"").append(transform).append(" scale(").append(iconType.getScale()).append(")\" xlink:href=\"").append(iconType.getHref()).append("\"></use>\n");
        sb.append("</g>");
        sb.append("</g>");
        sb.append("<g class=\"a7a7d0c6-d834-4f16-aa8c-4d2cc48187d8\">\n");
        sb.append("<circle class=\"e10c3a5a-f925-4bfe-b875-b1dab086c709\" cx=\"").append(x).append("\" cy=\"").append(y + 13.0).append("\" r=\"12.5\"></circle>\n");
        sb.append("</g>");

        return sb.toString();

    }

    public enum IconType {
        GELOESCHT("#835e3d20-6d0a-40fc-9d73-28fd1915bd98", 0.26),
        PZBK("#d714d3a5-8b0f-436a-a5fc-a10b6bc31eee", 0.5),
        MELDUNGEN_MEHRFACH_ZUGEORDNET("#d714d3a5-8b0f-436a-a5fc-a10b6bc31eed", 0.5);

        private final String href;
        private final double scale;

        IconType(String href, double scale) {
            this.href = href;
            this.scale = scale;
        }

        public String getHref() {
            return href;
        }

        public double getScale() {
            return scale;
        }

    }
}
