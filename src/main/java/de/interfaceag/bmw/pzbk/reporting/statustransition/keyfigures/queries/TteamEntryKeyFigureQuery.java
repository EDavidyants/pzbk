package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamAnforderungAbgestimmtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Date;

final class TteamEntryKeyFigureQuery {

    private static final Logger LOG = LoggerFactory.getLogger(TteamEntryKeyFigureQuery.class.getName());

    private TteamEntryKeyFigureQuery() {
    }

    static FachteamAnforderungAbgestimmtKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {
        Collection<Long> value = getAnforderungIdsForTteamEntry(filter, entityManager);
        return new FachteamAnforderungAbgestimmtKeyFigure(value);
    }

    private static Collection<Long> getAnforderungIdsForTteamEntry(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO("SELECT a.id ");
        queryPart.append(" FROM ReportingStatusTransition r ")
                .append(" INNER JOIN r.anforderung a ")
                .append(" WHERE r.abgestimmt.entry < :endDate ");
        queryPart.put("endDate", endDate);

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getAnforderungIdsForTteamEntry Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

}
