package de.interfaceag.bmw.pzbk.reporting.listview;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

public class KeyFigureIdForKeyTypeUtils<T extends ListKeyFigure> {

    public KeyFigureIdForKeyTypeUtils() {
    }

    public Collection<Long> getAnforderungIdsForKeyType(Collection<T> keyFigures, KeyType keyType) {
        return keyFigures.stream()
                .map(keyFigure -> keyFigure.getKeyFigureByType(keyType))
                .filter(Objects::nonNull)
                .map(KeyFigure::getAnforderungIds)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public Collection<Long> getMeldungIdsForKeyType(Collection<T> keyFigures, KeyType keyType) {
        return keyFigures.stream()
                .map(keyFigure -> keyFigure.getKeyFigureByType(keyType))
                .filter(Objects::nonNull)
                .map(KeyFigure::getMeldungIds)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }
}
