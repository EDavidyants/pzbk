package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class FachteamUnstimmigKeyFigure extends AbstractKeyFigure {

    private FachteamUnstimmigKeyFigure(Collection<Long> anforderungIds, Long runTime) {
        super(anforderungIds.size(),
                IdTypeTupleFactory.getIdTypeTuplesForAnforderung(anforderungIds),
                KeyType.FACHTEAM_ANFORDERUNG_UNSTIMMIG, runTime);
    }

    public static FachteamUnstimmigKeyFigure buildFromKeyFigures(@NotNull KeyFigure tteamUnstimmigKeyFigure, @NotNull KeyFigure vereinbarungUnstimmigKeyFigure) throws InvalidDataException {

        if (tteamUnstimmigKeyFigure == null || vereinbarungUnstimmigKeyFigure == null) {
            throw new InvalidDataException("At least one parameter is null!");
        }

        List<Long> anforderungIds = new ArrayList<>(tteamUnstimmigKeyFigure.getAnforderungIds());
        anforderungIds.addAll(vereinbarungUnstimmigKeyFigure.getAnforderungIds());

        return new FachteamUnstimmigKeyFigure(anforderungIds, 0L);
    }

}
