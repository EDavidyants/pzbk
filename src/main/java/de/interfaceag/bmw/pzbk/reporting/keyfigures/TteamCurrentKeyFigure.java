package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class TteamCurrentKeyFigure extends AbstractLanglauferKeyFigure {

    public TteamCurrentKeyFigure(Collection<Long> ids) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.TTEAM_CURRENT);
    }

    public TteamCurrentKeyFigure(Collection<Long> ids, boolean langlaufer) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.TTEAM_CURRENT);
        super.setLanglaufer(langlaufer);
    }

    public TteamCurrentKeyFigure(Collection<Long> ids, Long runTime) {
        super(ids.size(), IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.TTEAM_CURRENT, runTime);
    }

}
