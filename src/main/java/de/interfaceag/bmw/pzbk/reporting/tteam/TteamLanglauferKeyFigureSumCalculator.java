package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.shared.math.Sum;

import java.util.Collection;
import java.util.stream.Stream;

public final class TteamLanglauferKeyFigureSumCalculator {

    private TteamLanglauferKeyFigureSumCalculator() {
    }

    public static int getTteamLanglauferSum(Collection<TteamKeyFigure> keyFigures) {
        final Stream<Integer> langlauferCounts = keyFigures.stream().map(TteamKeyFigure::getTteamLanglauferCount);
        return Sum.computeSum(langlauferCounts);
    }

    public static int getVereinbarungLanglauferSum(Collection<TteamKeyFigure> keyFigures) {
        final Stream<Integer> langlauferCounts = keyFigures.stream().map(TteamKeyFigure::getVereinbarungLanglauferCount);
        return Sum.computeSum(langlauferCounts);
    }

}
