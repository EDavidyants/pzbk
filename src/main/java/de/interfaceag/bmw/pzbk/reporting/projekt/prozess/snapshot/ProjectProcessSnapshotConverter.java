package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataBuilder;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollectionBuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

public final class ProjectProcessSnapshotConverter {

    private ProjectProcessSnapshotConverter() {
    }

    public static ProjectReportingProcessKeyFigureDataCollection toKeyFigureDataCollection(Collection<ProjectProcessSnapshotEntry> entries, Date date) {
        Iterator<ProjectProcessSnapshotEntry> iterator = entries.iterator();

        ProjectReportingProcessKeyFigureDataCollectionBuilder builder = new ProjectReportingProcessKeyFigureDataCollectionBuilder();
        builder.withDate(date);


        while (iterator.hasNext()) {
            ProjectProcessSnapshotEntry entry = iterator.next();

            ProjectReportingProcessKeyFigure keyFigure = entry.getKeyFigure();
            Integer value = entry.getValue();

            ProjectReportingProcessKeyFigureData keyFigureData = ProjectReportingProcessKeyFigureDataBuilder.withKeyFigure(keyFigure).withValue(value.longValue()).build();
            builder.withKeyFigure(keyFigureData);

        }

        return builder.build();

    }

    public static Collection<ProjectProcessSnapshotEntry> toProjectProcessSnapshotEntries(ProjectReportingProcessKeyFigureDataCollection keyFigureDataCollection) {

        Collection<ProjectProcessSnapshotEntry> entries = new ArrayList<>();

        for (ProjectReportingProcessKeyFigure keyFigure : ProjectReportingProcessKeyFigure.values()) {
            Long value = keyFigureDataCollection.getValueForKeyFigure(keyFigure);

            if (value != 0L) {
                ProjectProcessSnapshotEntry snapshotEntry = new ProjectProcessSnapshotEntry(keyFigure, value.intValue());
                entries.add(snapshotEntry);
            }
        }

        return entries;

    }

}
