package de.interfaceag.bmw.pzbk.reporting;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Werk;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.WerkService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sl
 */
@Stateless
@Named
public class ReportingFilterService implements Serializable {

    @Inject
    private Session session;

    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private WerkService werkService;
    @Inject
    private ConfigService configService;
    @Inject
    private TteamService tteamService;
    @Inject
    private ModulService modulService;
    @Inject
    private AnforderungService anforderungService;

    public ReportingFilter getReportingFilter(UrlParameter urlParameter, Page page) {
        Mitarbeiter currentUser = session.getUser();
        Collection<SensorCoc> allSensorCocs = getSensorCocForUser(currentUser);
        Collection<Werk> allWerk = getAllWerk();
        Collection<FestgestelltIn> allFestgestelltIn = getAllFestgestelltIn();
        Collection<Tteam> allTteams = getTteamsForUser(currentUser);
        List<ModulSeTeam> allModulSeTeams = getModulSeTeamsForUser(currentUser);
        Date defaultStartDate = configService.getReportingStartDate();
        return new ReportingFilter(page, urlParameter, allSensorCocs, allWerk, allFestgestelltIn, allTteams, defaultStartDate, allModulSeTeams);
    }

    private List<ModulSeTeam> getModulSeTeamsForUser(Mitarbeiter currentUser) {
        if (session.hasRole(Rolle.ADMIN)) {
            return modulService.getAllSeTeams();
        } else {
            return berechtigungService.getAuthorizedModulSeTeamListForMitarbeiter(currentUser);
        }
    }

    private Collection<SensorCoc> getSensorCocForUser(Mitarbeiter currentUser) {
        return berechtigungService.getAuthorizedSensorCocObjectListForMitarbeiter(currentUser);
    }

    private Collection<Werk> getAllWerk() {
        return werkService.getAllWerk();
    }

    private Collection<FestgestelltIn> getAllFestgestelltIn() {
        return anforderungService.getAllFestgestelltIn();
    }

    private Collection<Tteam> getTteamsForUser(Mitarbeiter currentUser) {
        if (isTteamLeiterOrVertreter()) {
            return getTteamsForTteamLeiterOrVertreter();
        } else {
            return berechtigungService.getTteamsForMitarbeiter(currentUser);
        }
    }

    private Collection<Tteam> getTteamsForTteamLeiterOrVertreter() {
        final UserPermissions<BerechtigungDto> userPermissions = session.getUserPermissions();
        List<Long> tteamIds = new ArrayList<>();
        tteamIds.addAll(userPermissions.getTteamIdsForTteamVertreter());
        tteamIds.addAll(userPermissions.getTteamAsTteamleiterSchreibend().stream().map(BerechtigungDto::getId).collect(Collectors.toSet()));
        return tteamService.getTteamByIdList(tteamIds);
    }

    private boolean isTteamLeiterOrVertreter() {
        return session.hasRole(Rolle.T_TEAMLEITER) || session.hasRole(Rolle.TTEAM_VERTRETER);
    }

}
