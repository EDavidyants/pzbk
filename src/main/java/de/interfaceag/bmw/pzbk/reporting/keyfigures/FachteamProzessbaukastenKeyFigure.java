package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachteamProzessbaukastenKeyFigure extends AbstractKeyFigure {

    public FachteamProzessbaukastenKeyFigure(Collection<Long> anforderungIds) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(anforderungIds), KeyType.FACHTEAM_PZBK);
    }

}
