package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;

import javax.enterprise.context.Dependent;
import java.util.Date;

@Dependent
public class ReportingStatusTransitionFactory implements ReportingStatusTransitionCreatePort, ReportingMeldungStatusTransitionCreatePort {

    @Override
    public ReportingMeldungStatusTransition getNewStatusTransitionForMeldungWithEntryDate(Meldung meldung, Date entryDate) {
        return ReportingMeldungStatusTransition.getBuilder().setEntryDate(entryDate).setMeldung(meldung).build();
    }

    @Override
    public ReportingStatusTransition getNewStatusTransitionForAnforderungWithEntryDate(Anforderung anforderung, Date entryDate) {
        return ReportingStatusTransition.getBuilder().setEntryDate(entryDate).setAnforderung(anforderung).build();
    }

}
