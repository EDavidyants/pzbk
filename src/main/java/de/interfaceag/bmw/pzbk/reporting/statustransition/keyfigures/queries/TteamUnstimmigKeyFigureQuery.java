package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamUnstimmigKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;

final class TteamUnstimmigKeyFigureQuery {

    private static final Logger LOG = LoggerFactory.getLogger(TteamUnstimmigKeyFigureQuery.class.getName());

    private TteamUnstimmigKeyFigureQuery() {
    }

    static TteamUnstimmigKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {
        Collection<Long> value = getCountOfTteamUnstimmigEntry(filter, entityManager);
        return new TteamUnstimmigKeyFigure(value);
    }

    private static Collection<Long> getCountOfTteamUnstimmigEntry(ReportingFilter filter, EntityManager entityManager) {

        QueryPart queryPart = new QueryPartDTO("SELECT a.id ");
        queryPart.append(" FROM ReportingStatusTransition r ")
                .append(" INNER JOIN r.anforderung a ")
                .append(" WHERE r.tteamUnstimmig.entry IS NOT NULL ");

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getCountOfTteamUnstimmigEntry Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

}
