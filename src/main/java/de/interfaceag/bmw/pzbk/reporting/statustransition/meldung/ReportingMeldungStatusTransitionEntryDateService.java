package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungHistoryDto;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.enums.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Dependent
public class ReportingMeldungStatusTransitionEntryDateService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ReportingMeldungStatusTransitionEntryDateService.class);
    private static final String GEMELDET = "gemeldet";

    @Inject
    private AnforderungMeldungHistoryService meldungHistoryService;

    public Optional<Date> getEntryDateForMeldung(Meldung meldung) {
        final Status status = meldung.getStatus();
        if (statusIsValid(status)) {
            return getEntryDateForValidStatus(meldung);
        } else {
            LOG.error("Status of Meldung {} is invalid.", meldung);
            return Optional.empty();
        }
    }

    private Optional<Date> getEntryDateForValidStatus(Meldung meldung) {
        final List<AnforderungHistoryDto> meldungHistory = meldungHistoryService.getHistoryForAnforderung(meldung.getId(), "M");

        Date entryDate = null;
        for (AnforderungHistoryDto historyEntry : meldungHistory) {
            entryDate = updateEntryDateForValidHistoryEntry(entryDate, historyEntry);
        }

        return Optional.ofNullable(entryDate);
    }

    private Date updateEntryDateForValidHistoryEntry(Date entryDate, AnforderungHistoryDto historyEntry) {
        final String auf = historyEntry.getAuf();
        if (isGemeldet(auf)) {
            final Date zeitpunkt = historyEntry.getZeitpunktAsDate();
            entryDate = updateEntryDate(entryDate, zeitpunkt);
        }
        return entryDate;
    }

    private boolean statusIsValid(Status status) {
        return status.equals(Status.M_GEMELDET) || status.equals(Status.M_ZUGEORDNET) || status.equals(Status.M_GELOESCHT);
    }

    private Date updateEntryDate(Date entryDate, Date zeitpunkt) {
        if (isZeitpunktIsAfterEntryDate(entryDate, zeitpunkt)) {
            entryDate = zeitpunkt;
        }
        return entryDate;
    }

    private boolean isZeitpunktIsAfterEntryDate(Date entryDate, Date zeitpunkt) {
        return entryDate == null || zeitpunkt.after(entryDate);
    }

    private boolean isGemeldet(String auf) {
        return auf.equals(GEMELDET);
    }

}
