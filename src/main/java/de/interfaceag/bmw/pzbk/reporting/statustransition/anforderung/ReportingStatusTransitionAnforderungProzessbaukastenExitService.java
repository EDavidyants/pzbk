package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

@Dependent
public class ReportingStatusTransitionAnforderungProzessbaukastenExitService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(ReportingStatusTransitionAnforderungProzessbaukastenExitService.class);

    @Inject
    private ReportingStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Inject
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Inject
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Inject
    private ReportingStatusTransitionAnforderungEntyDateService entyDateService;
    @Inject
    private ReportingStatusTransitionAnforderungStatusChangeService anforderungStatusChangeService;

    public ReportingStatusTransition removeAnforderungFromProzessbaukasten(Anforderung anforderung, Status currentStatus, Date date) {

        LOG.debug("Write ReportingStatusTransition for Anforderung {} removal of Zuordnung to Prozessbaukasten at status {}", anforderung, currentStatus);

        final Optional<ReportingStatusTransition> latestStatusTransitionForAnforderung = statusTransitionDatabaseAdapter.getLatestForAnforderung(anforderung);

        final ReportingStatusTransition statusTransition;
        if (latestStatusTransitionForAnforderung.isPresent()) {
            LOG.debug("Found latest status transition and update or create new instance if necessary");
            statusTransition = updateReportingStatusTransition(latestStatusTransitionForAnforderung.get(), currentStatus, date);
        } else {
            LOG.debug("Found no status transition --> create new instance");
            Date entryDate = entyDateService.getEntryDateForAnforderung(anforderung);
            statusTransition = createNewReportingStatusTransitionForAnforderungAndUpdateEntries(anforderung, currentStatus, entryDate, date);
        }

        statusTransitionDatabaseAdapter.save(statusTransition);
        return statusTransition;
    }

    private ReportingStatusTransition updateReportingStatusTransition(ReportingStatusTransition reportingStatusTransition, Status currentStatus, Date date) {
        updateExitFromProzessbaukasten(reportingStatusTransition);
        return updateEntryIntoStatus(reportingStatusTransition, currentStatus, date);
    }

    private ReportingStatusTransition updateEntryIntoStatus(ReportingStatusTransition reportingStatusTransition, Status currentStatus, Date date) {

        if (Status.A_INARBEIT == currentStatus) {
            EntryExitTimeStamp inArbeit = reportingStatusTransition.getInArbeit();
            inArbeit = reportingStatusTransitionTimeStampUpdatePort.removeExitForTimeStamp(inArbeit);
            inArbeit = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(inArbeit, date);
            reportingStatusTransition.setInArbeit(inArbeit);
            return reportingStatusTransition;
        } else {
            final ReportingStatusTransition updatedStatusTranisition = anforderungStatusChangeService.updateEntryForStatus(reportingStatusTransition, currentStatus, currentStatus, date);
            return removeExitForStatus(updatedStatusTranisition, currentStatus);
        }

    }

    private ReportingStatusTransition removeExitForStatus(ReportingStatusTransition updatedStatusTranisition, Status currentStatus) {
        switch (currentStatus) {
            case A_UNSTIMMIG:
                removeExitForStatusUnstimmig(updatedStatusTranisition);
                break;
            case A_FTABGESTIMMT:
                updateExitForStatusAbgestimmt(updatedStatusTranisition);
                break;
            case A_PLAUSIB:
                updateExitForStatusPlausibilisiert(updatedStatusTranisition);
                break;
            case A_FREIGEGEBEN:
                updateExitForStatusFreigegeben(updatedStatusTranisition);
                break;
            case A_GELOESCHT:
            case A_KEINE_WEITERVERFOLG:
            default:
                break;
        }

        return updatedStatusTranisition;
    }

    private void updateExitForStatusFreigegeben(ReportingStatusTransition updatedStatusTranisition) {
        EntryExitTimeStamp freigegeben = updatedStatusTranisition.getFreigegeben();
        freigegeben = reportingStatusTransitionTimeStampUpdatePort.removeExitForTimeStamp(freigegeben);
        updatedStatusTranisition.setFreigegeben(freigegeben);
    }

    private void updateExitForStatusPlausibilisiert(ReportingStatusTransition updatedStatusTranisition) {
        EntryExitTimeStamp plausibilisiert = updatedStatusTranisition.getPlausibilisiert();
        plausibilisiert = reportingStatusTransitionTimeStampUpdatePort.removeExitForTimeStamp(plausibilisiert);
        updatedStatusTranisition.setPlausibilisiert(plausibilisiert);
    }

    private void updateExitForStatusAbgestimmt(ReportingStatusTransition updatedStatusTranisition) {
        EntryExitTimeStamp abgestimmt = updatedStatusTranisition.getAbgestimmt();
        abgestimmt = reportingStatusTransitionTimeStampUpdatePort.removeExitForTimeStamp(abgestimmt);
        updatedStatusTranisition.setAbgestimmt(abgestimmt);
    }

    private void removeExitForStatusUnstimmig(ReportingStatusTransition updatedStatusTranisition) {
        EntryExitTimeStamp fachteamUnstimmig = updatedStatusTranisition.getFachteamUnstimmig();
        EntryExitTimeStamp tteamUnstimmig = updatedStatusTranisition.getTteamUnstimmig();
        EntryExitTimeStamp vereinbarungUnstimmig = updatedStatusTranisition.getVereinbarungUnstimmig();

        if (onlyFachteamExitIsNotNull(fachteamUnstimmig, tteamUnstimmig, vereinbarungUnstimmig)) {
            removeExitForFachteamUnstimmit(updatedStatusTranisition, fachteamUnstimmig);
        } else if (onlyTteamUnstimmigIsNotNull(fachteamUnstimmig, tteamUnstimmig, vereinbarungUnstimmig)) {
            removeExitForTteamUnstimmig(updatedStatusTranisition, tteamUnstimmig);
        } else if (onlyVereinbarungUnstimmigIsNotNull(fachteamUnstimmig, tteamUnstimmig, vereinbarungUnstimmig)) {
            removeExitForVereinbarungUnstimmig(updatedStatusTranisition, vereinbarungUnstimmig);
        } else if (fachteamUnstimmigAndTteamUnstimmigAreNotNull(fachteamUnstimmig, tteamUnstimmig, vereinbarungUnstimmig)) {
            compareFachteamUnstimmigAndTteamUnstimmigAndUpdateLatest(updatedStatusTranisition, fachteamUnstimmig, tteamUnstimmig);
        } else if (fachteamUnstimmigAndVereinbarungUnstimmitAreNotNull(fachteamUnstimmig, tteamUnstimmig, vereinbarungUnstimmig)) {
            compareFachteamUnstimmigAndVereinbarungUnstimmigAndUpdateLatest(updatedStatusTranisition, fachteamUnstimmig, vereinbarungUnstimmig);
        } else if (tteamUnstimmigAndVereinbarungUnstimmigAreNotNull(fachteamUnstimmig, tteamUnstimmig, vereinbarungUnstimmig)) {
            compareTteamUnstimmigAndVereinbarungUnstimmigAndUpdateLatest(updatedStatusTranisition, tteamUnstimmig, vereinbarungUnstimmig);
        } else if (allUnstimmigValuesAreNotNull(fachteamUnstimmig, tteamUnstimmig, vereinbarungUnstimmig)) {
            compareAllAndUpdateLatest(updatedStatusTranisition, fachteamUnstimmig, tteamUnstimmig, vereinbarungUnstimmig);
        }
    }

    private void compareAllAndUpdateLatest(ReportingStatusTransition updatedStatusTranisition, EntryExitTimeStamp fachteamUnstimmig, EntryExitTimeStamp tteamUnstimmig, EntryExitTimeStamp vereinbarungUnstimmig) {
        final Date fachteamUnstimmigEntry = fachteamUnstimmig.getEntry();
        final Date tteamUnstimmigEntry = tteamUnstimmig.getEntry();
        final Date vereinbarungUnstimmigEntry = vereinbarungUnstimmig.getEntry();

        final Date max = Collections.max(Arrays.asList(fachteamUnstimmigEntry, tteamUnstimmigEntry, vereinbarungUnstimmigEntry));

        if (fachteamUnstimmigEntry.equals(max)) {
            removeExitForFachteamUnstimmit(updatedStatusTranisition, fachteamUnstimmig);
        } else if (tteamUnstimmigEntry.equals(max)) {
            removeExitForTteamUnstimmig(updatedStatusTranisition, tteamUnstimmig);
        } else {
            removeExitForVereinbarungUnstimmig(updatedStatusTranisition, vereinbarungUnstimmig);
        }
    }

    private void compareTteamUnstimmigAndVereinbarungUnstimmigAndUpdateLatest(ReportingStatusTransition updatedStatusTranisition, EntryExitTimeStamp tteamUnstimmig, EntryExitTimeStamp vereinbarungUnstimmig) {
        final Date tteamUnstimmigEntry = tteamUnstimmig.getEntry();
        final Date vereinbarungUnstimmigEntry = vereinbarungUnstimmig.getEntry();

        if (tteamUnstimmigEntry.after(vereinbarungUnstimmigEntry)) {
            removeExitForTteamUnstimmig(updatedStatusTranisition, tteamUnstimmig);
        } else {
            removeExitForVereinbarungUnstimmig(updatedStatusTranisition, vereinbarungUnstimmig);
        }
    }

    private void compareFachteamUnstimmigAndVereinbarungUnstimmigAndUpdateLatest(ReportingStatusTransition updatedStatusTranisition, EntryExitTimeStamp fachteamUnstimmig, EntryExitTimeStamp vereinbarungUnstimmig) {
        final Date fachteamUnstimmigEntry = fachteamUnstimmig.getEntry();
        final Date vereinbarungUnstimmigEntry = vereinbarungUnstimmig.getEntry();

        if (fachteamUnstimmigEntry.after(vereinbarungUnstimmigEntry)) {
            removeExitForFachteamUnstimmit(updatedStatusTranisition, fachteamUnstimmig);
        } else {
            removeExitForVereinbarungUnstimmig(updatedStatusTranisition, vereinbarungUnstimmig);
        }
    }

    private void compareFachteamUnstimmigAndTteamUnstimmigAndUpdateLatest(ReportingStatusTransition updatedStatusTranisition, EntryExitTimeStamp fachteamUnstimmig, EntryExitTimeStamp tteamUnstimmig) {
        final Date fachteamUnstimmigEntry = fachteamUnstimmig.getEntry();
        final Date tteamUnstimmigEntry = tteamUnstimmig.getEntry();

        if (fachteamUnstimmigEntry.after(tteamUnstimmigEntry)) {
            removeExitForFachteamUnstimmit(updatedStatusTranisition, fachteamUnstimmig);
        } else {
            removeExitForTteamUnstimmig(updatedStatusTranisition, tteamUnstimmig);
        }
    }

    private static boolean allUnstimmigValuesAreNotNull(EntryExitTimeStamp fachteamUnstimmig, EntryExitTimeStamp tteamUnstimmig, EntryExitTimeStamp vereinbarungUnstimmig) {
        return Objects.nonNull(fachteamUnstimmig) && Objects.nonNull(tteamUnstimmig) && Objects.nonNull(vereinbarungUnstimmig);
    }

    private static boolean tteamUnstimmigAndVereinbarungUnstimmigAreNotNull(EntryExitTimeStamp fachteamUnstimmig, EntryExitTimeStamp tteamUnstimmig, EntryExitTimeStamp vereinbarungUnstimmig) {
        return Objects.isNull(fachteamUnstimmig) && Objects.nonNull(tteamUnstimmig) && Objects.nonNull(vereinbarungUnstimmig);
    }

    private static boolean fachteamUnstimmigAndVereinbarungUnstimmitAreNotNull(EntryExitTimeStamp fachteamUnstimmig, EntryExitTimeStamp tteamUnstimmig, EntryExitTimeStamp vereinbarungUnstimmig) {
        return Objects.nonNull(fachteamUnstimmig) && Objects.isNull(tteamUnstimmig) && Objects.nonNull(vereinbarungUnstimmig);
    }

    private static boolean fachteamUnstimmigAndTteamUnstimmigAreNotNull(EntryExitTimeStamp fachteamUnstimmig, EntryExitTimeStamp tteamUnstimmig, EntryExitTimeStamp vereinbarungUnstimmig) {
        return Objects.nonNull(fachteamUnstimmig) && Objects.nonNull(tteamUnstimmig) && Objects.isNull(vereinbarungUnstimmig);
    }

    private static boolean onlyVereinbarungUnstimmigIsNotNull(EntryExitTimeStamp fachteamUnstimmig, EntryExitTimeStamp tteamUnstimmig, EntryExitTimeStamp vereinbarungUnstimmig) {
        return Objects.isNull(fachteamUnstimmig) && Objects.isNull(tteamUnstimmig) && Objects.nonNull(vereinbarungUnstimmig);
    }

    private static boolean onlyTteamUnstimmigIsNotNull(EntryExitTimeStamp fachteamUnstimmig, EntryExitTimeStamp tteamUnstimmig, EntryExitTimeStamp vereinbarungUnstimmig) {
        return Objects.isNull(fachteamUnstimmig) && Objects.nonNull(tteamUnstimmig) && Objects.isNull(vereinbarungUnstimmig);
    }

    private static boolean onlyFachteamExitIsNotNull(EntryExitTimeStamp fachteamUnstimmig, EntryExitTimeStamp tteamUnstimmig, EntryExitTimeStamp vereinbarungUnstimmig) {
        return Objects.nonNull(fachteamUnstimmig) && Objects.isNull(tteamUnstimmig) && Objects.isNull(vereinbarungUnstimmig);
    }

    private void removeExitForVereinbarungUnstimmig(ReportingStatusTransition updatedStatusTranisition, EntryExitTimeStamp vereinbarungUnstimmig) {
        vereinbarungUnstimmig = reportingStatusTransitionTimeStampUpdatePort.removeExitForTimeStamp(vereinbarungUnstimmig);
        updatedStatusTranisition.setVereinbarungUnstimmig(vereinbarungUnstimmig);
    }

    private void removeExitForTteamUnstimmig(ReportingStatusTransition updatedStatusTranisition, EntryExitTimeStamp tteamUnstimmig) {
        tteamUnstimmig = reportingStatusTransitionTimeStampUpdatePort.removeExitForTimeStamp(tteamUnstimmig);
        updatedStatusTranisition.setTteamUnstimmig(tteamUnstimmig);
    }

    private void removeExitForFachteamUnstimmit(ReportingStatusTransition updatedStatusTranisition, EntryExitTimeStamp fachteamUnstimmig) {
        fachteamUnstimmig = reportingStatusTransitionTimeStampUpdatePort.removeExitForTimeStamp(fachteamUnstimmig);
        updatedStatusTranisition.setFachteamUnstimmig(fachteamUnstimmig);
    }

    private void updateExitFromProzessbaukasten(ReportingStatusTransition statusTransition) {
        removeFachteamProzessbaukasten(statusTransition);
        removeTteamProzessbaukasten(statusTransition);
        removeVereinbarungProzessbaukasten(statusTransition);
        removeFreigegebenProzessbaukasten(statusTransition);
    }

    private void removeFreigegebenProzessbaukasten(ReportingStatusTransition statusTransition) {
        final EntryExitTimeStamp freigegebenProzessbaukasten = statusTransition.getFreigegebenProzessbaukasten();
        if (freigegebenProzessbaukasten != null && freigegebenProzessbaukasten.isActive()) {
            reportingStatusTransitionTimeStampUpdatePort.removeEntryForTimeStamp(freigegebenProzessbaukasten);
            reportingStatusTransitionTimeStampUpdatePort.removeExitForTimeStamp(freigegebenProzessbaukasten);
            statusTransition.setFreigegebenProzessbaukasten(freigegebenProzessbaukasten);
        }
    }

    private void removeFachteamProzessbaukasten(ReportingStatusTransition statusTransition) {
        final EntryExitTimeStamp fachteamProzessbaukasten = statusTransition.getFachteamProzessbaukasten();
        if (fachteamProzessbaukasten != null && fachteamProzessbaukasten.isActive()) {
            reportingStatusTransitionTimeStampUpdatePort.removeEntryForTimeStamp(fachteamProzessbaukasten);
            reportingStatusTransitionTimeStampUpdatePort.removeExitForTimeStamp(fachteamProzessbaukasten);
            statusTransition.setFachteamProzessbaukasten(fachteamProzessbaukasten);
        }
    }

    private void removeTteamProzessbaukasten(ReportingStatusTransition statusTransition) {
        final EntryExitTimeStamp tteamProzessbaukasten = statusTransition.getTteamProzessbaukasten();
        if (tteamProzessbaukasten != null && tteamProzessbaukasten.isActive()) {
            reportingStatusTransitionTimeStampUpdatePort.removeEntryForTimeStamp(tteamProzessbaukasten);
            reportingStatusTransitionTimeStampUpdatePort.removeExitForTimeStamp(tteamProzessbaukasten);
            statusTransition.setTteamProzessbaukasten(tteamProzessbaukasten);
        }
    }

    private void removeVereinbarungProzessbaukasten(ReportingStatusTransition statusTransition) {
        final EntryExitTimeStamp vereinbarungProzessbaukasten = statusTransition.getVereinbarungProzessbaukasten();
        if (vereinbarungProzessbaukasten != null && vereinbarungProzessbaukasten.isActive()) {
            reportingStatusTransitionTimeStampUpdatePort.removeEntryForTimeStamp(vereinbarungProzessbaukasten);
            reportingStatusTransitionTimeStampUpdatePort.removeExitForTimeStamp(vereinbarungProzessbaukasten);
            statusTransition.setVereinbarungProzessbaukasten(vereinbarungProzessbaukasten);
        }
    }

    private ReportingStatusTransition createNewReportingStatusTransitionForAnforderungAndUpdateEntries(Anforderung anforderung, Status currentStatus, Date entryDate, Date date) {
        ReportingStatusTransition statusTransition = createNewReportingStatusTransition(anforderung, entryDate);
        return updateReportingStatusTransition(statusTransition, currentStatus, date);
    }

    private ReportingStatusTransition createNewReportingStatusTransition(Anforderung anforderung, Date entryDate) {
        LOG.debug("Create new ReportingStatusTransition instance for anforderung {}", anforderung);
        return reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(anforderung, entryDate);
    }
}
