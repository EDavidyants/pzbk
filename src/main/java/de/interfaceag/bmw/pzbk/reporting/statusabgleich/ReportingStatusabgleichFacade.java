package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusService;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import de.interfaceag.bmw.pzbk.reporting.SelectedDerivatFilterService;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot.StatusabgleichSnapshot;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot.StatusabgleichSnapshotConverter;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot.StatusabgleichSnapshotReadService;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot.StatusabgleichSnapshotWriteService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class ReportingStatusabgleichFacade implements Serializable {

    @Inject
    private DerivatService derivatService;
    @Inject
    private StatusabgleichKennzahlenService kennzahlenService;
    @Inject
    private StatusabgleichSnapshotReadService snapshotReadService;
    @Inject
    private StatusabgleichSnapshotWriteService snapshotWriteService;
    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private ModulService modulService;
    @Inject
    private DerivatStatusService derivatStatusService;
    @Inject
    private SelectedDerivatFilterService selectedDerivatFilterService;
    @Inject
    private ReportingStatusabgleichExcelExportService excelExportService;


    public ReportingStatusabgleichViewData getViewData(UrlParameter urlParameter) {
        Optional<Derivat> derivat = selectedDerivatFilterService.getDerivatForUrlParameter(urlParameter);
        if (derivat.isPresent()) {
            return getViewDataForDerivat(derivat.get(), urlParameter);
        } else {
            return getEmptyViewData(urlParameter);
        }
    }

    public ReportingStatusabgleichViewData getPublicViewData(UrlParameter urlParameter) {
        Optional<Derivat> derivat = selectedDerivatFilterService.getDerivatForUrlParameter(urlParameter);
        if (derivat.isPresent()) {
            return getPublicViewDataForDerivat(derivat.get(), urlParameter);
        } else {
            return getEmptyViewData(urlParameter);
        }
    }

    public void createNewSnapshot(Derivat derivat) {
        snapshotWriteService.createNewSnapshotForDerivat(derivat);
    }

    private ReportingStatusabgleichViewData getEmptyViewData(UrlParameter urlParameter) {
        ReportingStatusabgleichFilter filter = getFilterForNoSelectedDerivat(urlParameter);
        return ReportingStatusabgleichViewData.empty(filter);
    }

    private ReportingStatusabgleichViewData getPublicViewDataForDerivat(Derivat derivat, UrlParameter urlParameter) {
        Optional<StatusabgleichSnapshot> snapshot = getSnapshotForUrlParameter(urlParameter);
        ReportingStatusabgleichFilter filter = getFilterForDerivat(urlParameter, derivat);

        StatusabgleichKennzahlen kennzahlen;
        Date date;

        if (snapshot.isPresent()) {
            StatusabgleichSnapshot selectedSnapshot = snapshot.get();
            date = selectedSnapshot.getDatum();
            kennzahlen = getKennzahlenForSnapshot(selectedSnapshot);
        } else {
            Optional<StatusabgleichSnapshot> latestSnapshotForDerivat = snapshotReadService.getLatestSnapshotForDerivat(derivat);
            if (latestSnapshotForDerivat.isPresent()) {
                StatusabgleichSnapshot latestSnapshot = latestSnapshotForDerivat.get();
                kennzahlen = getKennzahlenForSnapshot(latestSnapshot);
                date = latestSnapshot.getDatum();
            } else {
                return ReportingStatusabgleichViewData.empty(filter);
            }
        }

        DerivatStatusNames derivatStatusNames = getDerivatStatusNames();

        filter.setSnapshot(snapshot.isPresent());

        return ReportingStatusabgleichViewData.withKennzahlen(kennzahlen)
                .withFilter(filter)
                .withDerivat(derivat)
                .withDate(date)
                .withDerivatStatusNames(derivatStatusNames)
                .get();
    }

    private ReportingStatusabgleichViewData getViewDataForDerivat(Derivat derivat, UrlParameter urlParameter) {
        Optional<StatusabgleichSnapshot> snapshot = getSnapshotForUrlParameter(urlParameter);
        ReportingStatusabgleichFilter filter = getFilterForDerivat(urlParameter, derivat);

        List<Modul> restrictedModules = kennzahlenService.getModulFilterBasedOnSearchResult(derivat, filter);
        filter.setRestrictedModulFilter(urlParameter, restrictedModules);

        List<SensorCoc> restrictedTechnologie = kennzahlenService.getTechnologieFilterBasedOnSearchResult(derivat, filter);
        filter.setRestrictedTechnoloigieFilter(urlParameter, restrictedTechnologie);

        StatusabgleichKennzahlen kennzahlen;

        if (snapshot.isPresent()) {
            kennzahlen = getKennzahlenForSnapshot(snapshot.get());
        } else {
            kennzahlen = getCurrentKennzahlenForDerivat(derivat, filter);
        }

        DerivatStatusNames derivatStatusNames = getDerivatStatusNames();

        filter.setSnapshot(snapshot.isPresent());

        return ReportingStatusabgleichViewData
                .withKennzahlen(kennzahlen)
                .withFilter(filter)
                .withDerivat(derivat)
                .withDerivatStatusNames(derivatStatusNames)
                .get();
    }

    private StatusabgleichKennzahlen getCurrentKennzahlenForDerivat(Derivat derivat, ReportingStatusabgleichFilter filter) {
        return kennzahlenService.getKennzahlenForDerivatWithFilter(derivat, filter);
    }

    private static StatusabgleichKennzahlen getKennzahlenForSnapshot(StatusabgleichSnapshot snapshot) {
        return StatusabgleichSnapshotConverter.convertToKennzahlen(snapshot);
    }

    private ReportingStatusabgleichFilter getFilterForDerivat(UrlParameter urlParameter, Derivat derivat) {
        Collection<Derivat> allDerivate = derivatService.getAllDerivate();
        List<SnapshotFilter> allSnapshots = snapshotReadService.getSnapshotDatesForDerivat(derivat);
        List<SensorCoc> allSensorCocs = sensorCocService.getAllSortByTechnologie();
        List<Modul> allModules = modulService.getAllModule();
        List<String> allFachbereiche = modulService.getAllFachbereiche();
        return new ReportingStatusabgleichFilter(urlParameter, allDerivate, allSnapshots, allSensorCocs, allFachbereiche, allModules);
    }

    private ReportingStatusabgleichFilter getFilterForNoSelectedDerivat(UrlParameter urlParameter) {
        Collection<Derivat> allDerivate = derivatService.getAllDerivate();
        List<SnapshotFilter> allSnapshots = Collections.emptyList();
        List<SensorCoc> allSensorCocs = sensorCocService.getAllSortByTechnologie();
        List<Modul> allModules = modulService.getAllModule();
        List<String> allFachbereiche = modulService.getAllFachbereiche();
        return new ReportingStatusabgleichFilter(urlParameter, allDerivate, allSnapshots, allSensorCocs, allFachbereiche, allModules);
    }

    private Optional<StatusabgleichSnapshot> getSnapshotForUrlParameter(UrlParameter urlParameter) {
        Optional<String> snapshotParameter = urlParameter.getValue("snapshot");

        if (snapshotParameter.isPresent() && RegexUtils.matchesId(snapshotParameter.get())) {
            Long snapshotId = Long.parseLong(snapshotParameter.get());
            return snapshotReadService.getById(snapshotId);
        }

        return Optional.empty();
    }

    private DerivatStatusNames getDerivatStatusNames() {
        return derivatStatusService.getDerivatStatusNames();
    }

    public void downloadExcelExport(String derivatName, List<DrilldownTableAnforderungDTO> data) {
        excelExportService.downloadDrilldownDataToExcel(derivatName, data);
    }

    public List<DrilldownTableAnforderungDTO> showAnforderungsuebersichtForPosition(Derivat derivat, ReportingStatusabgleichFilter filter, int row, int col) {
        if (filter != null
                && row > 0 && col > 0) {
            return kennzahlenService.computeAnforderungIdWithFilterAndTablePosition(derivat, filter, row, col);
        } else {
            return Collections.emptyList();
        }

    }

}
