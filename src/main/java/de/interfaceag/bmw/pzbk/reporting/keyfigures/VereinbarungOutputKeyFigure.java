package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class VereinbarungOutputKeyFigure extends AbstractKeyFigure {

    public VereinbarungOutputKeyFigure(Collection<Long> anforderungIds) {
        super(anforderungIds.size(),
                IdTypeTupleFactory.getIdTypeTuplesForAnforderung(anforderungIds),
                KeyType.VEREINBARUNG_OUTPUT, 0L);
    }

    public static VereinbarungOutputKeyFigure buildFromKeyFigures(@NotNull KeyFigure vereinbarungFreigegebenKeyFigure,
                                                                  @NotNull KeyFigure vereinbarungGeloeschtKeyFigure, @NotNull KeyFigure vereinbarungUnstimmigKeyFigure, @NotNull KeyFigure vereinbarungProzessbaukastenKeyFigure) throws InvalidDataException {

        if (vereinbarungFreigegebenKeyFigure == null || vereinbarungGeloeschtKeyFigure == null
                || vereinbarungUnstimmigKeyFigure == null || vereinbarungProzessbaukastenKeyFigure == null) {
            throw new InvalidDataException("At leat one parameter is null");
        }

        List<Long> anforderungIds = new ArrayList<>(vereinbarungFreigegebenKeyFigure.getAnforderungIds());
        anforderungIds.addAll(vereinbarungGeloeschtKeyFigure.getAnforderungIds());
        anforderungIds.addAll(vereinbarungUnstimmigKeyFigure.getAnforderungIds());
        anforderungIds.addAll(vereinbarungProzessbaukastenKeyFigure.getAnforderungIds());

        return new VereinbarungOutputKeyFigure(anforderungIds);
    }
}
