package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;

import java.util.Date;

public interface AnforderungReportingStatusTransitionAdapterForMigration {

    ReportingStatusTransition createAnforderungFromMeldung(Anforderung anforderung, Date date);

    ReportingStatusTransition changeAnforderungStatus(Anforderung anforderung, Status currentStatus, Status newStatus, Date date);

    ReportingStatusTransition addAnforderungToProzessbaukasten(Anforderung anforderung, Status currentStatus, Date date);

    ReportingStatusTransition removeAnforderungFromProzessbaukasten(Anforderung anforderung, Status currentStatus, Date date);

    ReportingStatusTransition createAnforderungAsNewVersion(Anforderung anforderung, Date date);

    ReportingStatusTransition restoreAnforderung(Anforderung anforderung, Date date);

}
