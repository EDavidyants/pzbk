package de.interfaceag.bmw.pzbk.reporting.projekt.werk;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class ProjectWerkViewData implements Serializable, ProjectWerkTable {

    private final ProjectWerkFilter filter;

    private final List<ProjectReportDto> projectReports;

    private ProjectWerkViewData(ProjectWerkFilter filter, List<ProjectReportDto> projectReports) {
        this.filter = filter;
        this.projectReports = projectReports;
    }

    public ProjectWerkFilter getFilter() {
        return filter;
    }

    @Override
    public List<ProjectReportDto> getProjectReports() {
        return projectReports;
    }

    public static BuilderWithFilter createNew() {
        return new Builder();
    }

    public static final class Builder implements BuilderWithFilter, BuilderGet,
            BuilderWithProjectReports {

        private ProjectWerkFilter filter;
        private List<ProjectReportDto> projectReports = new ArrayList<>();

        private Builder() {
        }

        @Override
        public BuilderWithProjectReports withFilter(ProjectWerkFilter filter) {
            this.filter = filter;
            return this;
        }

        @Override
        public BuilderAddProjectReports addProjectReport(ProjectReportDto projectReport) {
            if (projectReport != null) {
                this.projectReports.add(projectReport);
            }
            return this;
        }

        @Override
        public BuilderGet withProjectReports(List<ProjectReportDto> projectReports) {
            this.projectReports = projectReports;
            return this;
        }

        @Override
        public ProjectWerkViewData get() {
            return new ProjectWerkViewData(filter, projectReports);
        }

    }

    public interface BuilderWithFilter {

        BuilderWithProjectReports withFilter(ProjectWerkFilter filter);
    }

    public interface BuilderAddProjectReports extends BuilderGet {

        BuilderAddProjectReports addProjectReport(ProjectReportDto projectReport);
    }

    public interface BuilderWithProjectReports extends BuilderAddProjectReports {

        BuilderGet withProjectReports(List<ProjectReportDto> projectReports);
    }

    public interface BuilderGet {

        ProjectWerkViewData get();
    }

}
