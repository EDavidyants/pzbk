package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class TteamOutputKeyFigure extends AbstractKeyFigure {

    public TteamOutputKeyFigure(Collection<Long> anforderungIds) {
        super(anforderungIds.size(),
                IdTypeTupleFactory.getIdTypeTuplesForAnforderung(anforderungIds),
                KeyType.TTEAM_OUTPUT, 0L);
    }

    public static TteamOutputKeyFigure buildFromKeyFigures(@NotNull KeyFigure tteamPlausibilisiertKeyFigure,
                                                           @NotNull KeyFigure tteamGeloeschtKeyFigure,
                                                           @NotNull KeyFigure tteamUnstimmigKeyFigure,
                                                           @NotNull KeyFigure tteamProzessbaukastenKeyFigure) throws InvalidDataException {

        if (tteamGeloeschtKeyFigure == null || tteamPlausibilisiertKeyFigure == null
                || tteamUnstimmigKeyFigure == null || tteamProzessbaukastenKeyFigure == null) {
            throw new InvalidDataException("At least one parameter is null!");
        }

        List<Long> anforderungIds = new ArrayList<>(tteamPlausibilisiertKeyFigure.getAnforderungIds());
        anforderungIds.addAll(tteamGeloeschtKeyFigure.getAnforderungIds());
        anforderungIds.addAll(tteamUnstimmigKeyFigure.getAnforderungIds());
        anforderungIds.addAll(tteamProzessbaukastenKeyFigure.getAnforderungIds());

        return new TteamOutputKeyFigure(anforderungIds);
    }

}
