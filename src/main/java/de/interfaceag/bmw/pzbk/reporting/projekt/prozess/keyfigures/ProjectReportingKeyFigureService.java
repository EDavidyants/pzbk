package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollectionDto;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author sl
 */
@Named
@Stateless
public class ProjectReportingKeyFigureService {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingKeyFigureService.class);

    @Inject
    private ProjectReportingVereinbarungKeyFigureService vereinbarungKeyFigureService;
    @Inject
    private ProjectReportingUmsetzungsverwaltungKeyFigureService umsetzungsverwaltungKeyFigureService;
    @Inject
    private ProjectReportingAusleitungKeyFigureService ausleitungKeyFigureService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private SensorCocService sensorCocService;

    public ProjectReportingProcessKeyFigureDataCollection computeKeyFiguresForDerivat(Derivat derivat, ProjectProcessFilter filter) {
        LOG.debug("Compute keyfigures for derivat {} with filter", derivat);

        Map<Integer, ProjectReportingProcessKeyFigureData> result = new HashMap<>();
        result.putAll(vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter));
        result.putAll(vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter));
        result.putAll(ausleitungKeyFigureService.getKeyFiguresForAusleitung(derivat, filter));

        Optional<ProjectReportingProcessKeyFigureData> vereinbarungVKBGAngenommen = Optional.ofNullable(result.get(ProjectReportingProcessKeyFigure.L4.getId()));

        if (vereinbarungVKBGAngenommen.isPresent()) {
            Long value = vereinbarungVKBGAngenommen.get().getValue();
            result.putAll(umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, value, filter));
        } else {
            result.putAll(umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, 0L, filter));
        }

        Optional<ProjectReportingProcessKeyFigureData> vereinbarungZVAngenommen = Optional.ofNullable(result.get(ProjectReportingProcessKeyFigure.R6.getId()));

        if (vereinbarungZVAngenommen.isPresent()) {
            Long value = vereinbarungZVAngenommen.get().getValue();
            result.putAll(umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVBBG(derivat, value, filter));
            result.putAll(umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, value, filter));
        } else {
            result.putAll(umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVBBG(derivat, 0L, filter));
            result.putAll(umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, 0L, filter));
        }

        LOG.debug("Computed keyfigures: {}", result);

        return ProjectReportingProcessKeyFigureDataCollectionDto.forMap(result, new Date());
    }

    public ProjectReportingProcessKeyFigureDataCollection getEmptyCollection() {
        return ProjectReportingProcessKeyFigureDataCollectionDto.empty();
    }

    public ProjectProcessFilter buildProjectProcessFilterForDerivat() {
        UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();

        Collection<Derivat> allDerivate = derivatService.getAllDerivate();
        List<SnapshotFilter> allSnapshots = Collections.emptyList();
        List<SensorCoc> allSensorCocs = sensorCocService.getAllSortByTechnologie();

        return new ProjectProcessFilter(urlParameter, allDerivate, allSnapshots, allSensorCocs);
    }

}
