package de.interfaceag.bmw.pzbk.reporting;

import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ReportingUtils {

    private ReportingUtils() {
    }

    public static Set<SearchFilterObject> getSearchFilterObjectsForId(IdSearchFilter idSearchFilter, Long id) {
        return idSearchFilter.getAll().stream()
                .filter(searchFilterObject -> searchFilterObject.getId().equals(id))
                .collect(Collectors.toSet());
    }

    public static String getReportingPeriod(ReportingFilter filter) {
        final DateSearchFilter vonDateFilter = filter.getVonDateFilter();
        final DateSearchFilter bisDateFilter = filter.getBisDateFilter();

        String reportingPeriod = "? - ?";
        if (vonDateFilter == null || bisDateFilter == null) {
            return reportingPeriod;
        }

        Date startDate = vonDateFilter.getSelected();
        Date endDate = bisDateFilter.getSelected();

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        if (startDate != null && endDate != null) {
            String startDateStr = sdf.format(startDate);
            String endDateStr = sdf.format(endDate);
            reportingPeriod = startDateStr + " - " + endDateStr;
        }

        return reportingPeriod;
    }

}
