package de.interfaceag.bmw.pzbk.reporting.svg;

/**
 *
 * @author lomu
 */
public class LanglauferHinweis implements SVGElement {

    private final double x;
    private final double y;
    private final String tooltip;

    public LanglauferHinweis(int xCoordinate, int yCoordinate, String tooltip) {
        this.x = xCoordinate;
        this.y = yCoordinate;
        this.tooltip = tooltip;
    }

    public LanglauferHinweis(double xCoordinate, double yCoordinate, String tooltip) {
        this.x = xCoordinate;
        this.y = yCoordinate;
        this.tooltip = tooltip;
    }

    @Override
    public String draw() {
        StringBuilder sb = new StringBuilder();

        sb.append("<image width=\"100\" height=\"100\" transform=\"translate(").append(x).append(" ").append(y).append(") scale(0.20)\" "
                + "xlink:href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFQAAABUCAYAAAAcaxDBAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQwIDc5LjE2MDQ1MSwgMjAxNy8wNS8wNi0wMTowODoyMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NkYxNTYyRjlCOThEMTFFOEE5ODhEMEQ2MUVDMkE2MzgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NkYxNTYyRkFCOThEMTFFOEE5ODhEMEQ2MUVDMkE2MzgiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo2RjE1NjJGN0I5OEQxMUU4QTk4OEQwRDYxRUMyQTYzOCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo2RjE1NjJGOEI5OEQxMUU4QTk4OEQwRDYxRUMyQTYzOCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PsVMbqcAAASLSURBVHja7J0/SBxBFMZnFy/CNZ7R4ppwhSAogmerwtkpVpZBkGCRykqbFGkkELBSm1TpFCRgrWjnEU17FzQGgoqnICImaqOeJ17e82aPu71/e7s7N7Pr++BjGnfv7c83szOzs7Mak6wsYzoUXeAouAfcAY6Aw+B2cNB0yC34EnwOToEPwXvgJPi3xtiTzOvRJEHshGIEPAweALe4dOob8A54E7wBF/eH+VUAMQz+AE6Asw1ygv9m2E8g+8Gr4EwDQZqd4TH0exnkEHhLIsRKxpiGvASyE7ymIEiz13hbrizIZvAn8IMHYBp+4DE3qwazF/zLQyDNxth7VYE5BU57GKZhvIYp2VV8yQcgzV5qeBMAP9gG/u5DmIbx2toa2UHf9TFMw7vCBwQc5sELgGn4oF6oWj3VHIotPoHhrlpgKJ9KOTtHJAIj+RsReYQTL0MA6q/bNyBxbWYolHUsPIfYNtXSjUq3yPQreJC9XA1yBs6B8r7ZBCNNWOmn6rVGQFDME8u85muNqPRq7SYUK+BXxDEvZLFSrT2tlqEfwd3EsETdnI31bhOf1sLuQqAhIYZCjF1dOTtHaytj19eNgprB7mO5RyyVMnShYTC9qQBnVLvK89nsUWJWU6PlZv7LZegssbKs2apA+UOsGHGyrJj5wZ85Q6flTK5m1TiHPU2XvcvzWZVTcJOUsO7voXdnc1737o6xYFAW0EfwGy23kqUoQ99Jg4k6PpZzrHM1cXYlVf6t1NbIu0CL2OkFHfmo1JBOTrwMNGo84zcydET6/dLbGZpnaAAdJqCO9cxQ5+szB6SH4+0qjxpAlsZi1xbKUMdChl269JuRobMz6NE92uuDXlyoMnKKItAeJUJ5eoJhxalXs9NQDwLtUCYcO3DUAtqBQCPKhGPnxqQW0AgCDVOGuqYwAm0noK6pHYEGqcq7pqCuUjQ+yFCmZZ/nRhRRIMBYOg1RWVzDJncetKzUytBMJtfB92h2GkBvPVvt1QN6i0AvlQqpnnWi6gG9RKDnBNQ1nSPQFFV599IBgR4qFVI9fVH1gB4i0D3KUNe0h49Ak0qFdHTE2OSktb9VZx7UUFLjj0D+MRVm7b0tfAXltc736NghHo61gyyNkdIm8XCszcKh5wbxcKyNPFC+tDlJTBzcjPjy8MLJkW/Exbby7NRZzhiLMdbXZ+/YRIKxeFwWzKLljEXiW/HIefN3cdH+e554rLw3llcLGZrnQxeo9tathYpAIW1/QBEnRpYV58wqZihqljhZVgmrEqBabpOBdWJVU+ucFauVoSh8syFDzCoqwyq8MVMWKO+kzhG3ipqrtJVmtaeen8H7xK5E+5wNqwso/AfSUIyDH4hhXshinLNh9WYoQv0JxQxxzGuGM2G2gHKoX6BYJpZsmbNgjoByvQdvv2CY25xBTVmaCNFyOxeOMVEbYaFwnZLdjazwWHHCh5hj1dpN26Kt2gSINhMUA5W2uxQAlTZkFQSWtgwWAJU2tRbUBNC26wLA0ocBBIGlT1cIAksfVxE4IPD953/oA1V+AGqC66tPqP0XYABKSo4dq4rVcQAAAABJRU5ErkJggg==\"/>");

        sb.append("<foreignObject width=\"300\" height=\"50\" transform=\"translate(").append(x).append(" ").append(y).append(")\">");
        sb.append("</a>");
        sb.append("</foreignObject>");

        return sb.toString();
    }

}
