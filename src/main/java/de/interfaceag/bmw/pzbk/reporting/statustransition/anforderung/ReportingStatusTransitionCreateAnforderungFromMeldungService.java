package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;

@Dependent
public class ReportingStatusTransitionCreateAnforderungFromMeldungService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(ReportingStatusTransitionCreateAnforderungFromMeldungService.class);

    @Inject
    private ReportingStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Inject
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Inject
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Inject
    private ReportingStatusTransitionAnforderungEntyDateService entyDateService;

    public ReportingStatusTransition createAnforderungFromMeldung(Anforderung anforderung, Date date) {
        LOG.debug("Write ReportingStatusTransition for creation of Anforderung {} from meldung", anforderung);

        Date entryDate = entyDateService.getEntryDateForAnforderung(anforderung);
        final ReportingStatusTransition statusTransition = createNewReportingStatusTransitionForAnforderung(anforderung, entryDate, date);
        statusTransitionDatabaseAdapter.save(statusTransition);
        return statusTransition;
    }

    private ReportingStatusTransition updateEntryToInArbeit(ReportingStatusTransition statusTransition, Date date) {
        final EntryExitTimeStamp vereinbarungProzessbaukasten = statusTransition.getInArbeit();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(vereinbarungProzessbaukasten, date);
        statusTransition.setInArbeit(timeStamp);
        return statusTransition;
    }

    private ReportingStatusTransition createNewReportingStatusTransitionForAnforderung(Anforderung anforderung, Date entryDate, Date date) {
        LOG.debug("Create new ReportingStatusTransition instance for anforderung {}", anforderung);
        ReportingStatusTransition statusTransition = reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(anforderung, entryDate);
        return updateEntryToInArbeit(statusTransition, date);
    }
}
