package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamAnforderungAbgestimmtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamAnforderungNewVersionKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamCurrentObjectsFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamGeloeschteObjekteKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamGemeldeteMeldungenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamInputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamMeldungExistingAnforderungKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamUnstimmigKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FreigegebenCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FreigegebenGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FreigegebenProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTupleCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamPlausibilisiertKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamUnstimmigKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungFreigegebenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungUnstimmigKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.langlaufer.DurchlaufzeitService;
import de.interfaceag.bmw.pzbk.reporting.processoverview.ProcessOverviewKeyFigures;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureFilterService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.StatusTransitionKeyFigureFactory;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureValidationResult;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureValidationService;
import de.interfaceag.bmw.pzbk.shared.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import static de.interfaceag.bmw.pzbk.reporting.ReportingUtils.getReportingPeriod;

@Dependent
public class StatusTransitionKeyFigureService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(StatusTransitionKeyFigureService.class);

    @Inject
    private StatusTransitionKeyFigureFactory keyFigureFactory;
    @Inject
    private KeyFigureValidationService keyFigureValidationService;
    @Inject
    private KeyFigureFilterService keyFigureFilterService;
    @Inject
    private DurchlaufzeitService durchlaufzeitService;

    public ProcessOverviewKeyFigures getSimplProcessOverviewKeyFigures(ReportingFilter filter) {
        increaseEndDateByOneDay(filter);

        final IdTypeTupleCollection filteredIdTypeTuples = keyFigureFilterService.getFilteredIdTypeTuples(filter);

        KeyFigureCollection keyFigures = new KeyFigureCollection();

        addZeitpunkte(keyFigures, filter, filteredIdTypeTuples);

        DurchlaufzeitKeyFigureCollection durchlaufzeitKeyFigures = computeDurchlaufzeitKeyFigures(filter);

        LOG.debug("keyFigures: {}", keyFigures);

        decreaseEndDateByOneDay(filter);

        return buildProcessOverviewKeyFigures(filter, keyFigures, durchlaufzeitKeyFigures);

    }

    public ProcessOverviewKeyFigures getDetailProcessOverviewKeyFigures(ReportingFilter filter) {
        final KeyFigureCollection keyFigures = getKeyFigures(filter);
        DurchlaufzeitKeyFigureCollection durchlaufzeitKeyFigures = computeDurchlaufzeitKeyFigures(filter);
        return buildProcessOverviewKeyFigures(filter, keyFigures, durchlaufzeitKeyFigures);
    }

    public KeyFigureCollection getKeyFiguresForFachteamReporting(ReportingFilter filter) {

        increaseEndDateByOneDay(filter);

        final IdTypeTupleCollection filteredIdTypeTuples = keyFigureFilterService.getFilteredIdTypeTuples(filter);

        KeyFigureCollection keyFigures = new KeyFigureCollection();

        addFachteamCurrentKeyFigure(filter, filteredIdTypeTuples, keyFigures);
        addFreigegebenCurrentKeyFigure(keyFigures, filter, filteredIdTypeTuples);

        addFachteamGeloeschteObjekteKeyFigure(filter, filteredIdTypeTuples, keyFigures);
        addFachteamProzessbaukastenKeyFigure(filter, keyFigures, filteredIdTypeTuples);
        addMeldungExistingAnforderungZugeordnetKeyFigure(filter, keyFigures, filteredIdTypeTuples);
        addTteamEntryKeyFigure(filter, keyFigures, filteredIdTypeTuples);
        addFachteamOutputKeyFigure(keyFigures);

        addFachteamAnforderungNewVersionKeyFigure(keyFigures, filter, filteredIdTypeTuples);
        addUnstimmigKeyFigures(filter, keyFigures, filteredIdTypeTuples);
        addMeldungEntryKeyFigure(filter, keyFigures, filteredIdTypeTuples);

        addSummeUnstimmig(keyFigures);
        addSummeFachteamEntry(keyFigures);

        decreaseEndDateByOneDay(filter);

        LOG.debug("keyFigures: {}", keyFigures);

        return keyFigures;
    }

    public KeyFigureCollection getKeyFigures(ReportingFilter filter) {

        increaseEndDateByOneDay(filter);

        final IdTypeTupleCollection filteredIdTypeTuples = keyFigureFilterService.getFilteredIdTypeTuples(filter);

        KeyFigureCollection keyFigures = new KeyFigureCollection();

        addZeitpunkte(keyFigures, filter, filteredIdTypeTuples);
        addFachteamAnforderungNewVersionKeyFigure(keyFigures, filter, filteredIdTypeTuples);
        addUnstimmigKeyFigures(filter, keyFigures, filteredIdTypeTuples);
        addGeloeschtKeyFigures(filter, keyFigures, filteredIdTypeTuples);
        addProzessbaukastenKeyFigures(filter, keyFigures, filteredIdTypeTuples);
        addMeldungExistingAnforderungZugeordnetKeyFigure(filter, keyFigures, filteredIdTypeTuples);
        addProcessstepEntryKeyFigures(filter, keyFigures, filteredIdTypeTuples);
        addMeldungEntryKeyFigure(filter, keyFigures, filteredIdTypeTuples);
        addSummeUnstimmig(keyFigures);
        addSummeFachteamEntry(keyFigures);
        addSummeZeitpunktExit(keyFigures);


        decreaseEndDateByOneDay(filter);


        LOG.debug("keyFigures: {}", keyFigures);

        return keyFigures;
    }

    public DurchlaufzeitKeyFigureCollection computeDurchlaufzeitKeyFigures(ReportingFilter filter) {

        increaseEndDateByOneDay(filter);

        DurchlaufzeitKeyFigureCollection durchlaufzeitKeyFigures = new DurchlaufzeitKeyFigureCollection();

        final VereinbarungDurchlaufzeitKeyFigure vereinbarungDurchlaufzeitKeyFigure = durchlaufzeitService.getVereinbarungDurchlaufzeitKeyFigure(filter);
        durchlaufzeitKeyFigures.add(vereinbarungDurchlaufzeitKeyFigure);

        final TteamDurchlaufzeitKeyFigure tteamDurchlaufzeitKeyFigure = durchlaufzeitService.getTteamDurchlaufzeitKeyFigure(filter);
        durchlaufzeitKeyFigures.add(tteamDurchlaufzeitKeyFigure);

        final FachteamDurchlaufzeitKeyFigure fachteamDurchlaufzeitKeyFigure = durchlaufzeitService.getFachteamDurchlaufzeitKeyFigure(filter);
        durchlaufzeitKeyFigures.add(fachteamDurchlaufzeitKeyFigure);

        decreaseEndDateByOneDay(filter);

        return durchlaufzeitKeyFigures;
    }

    /**
     * Move end date of reporting by one day to show changes of the current day
     * in the reporting. Without this change the current day at 0:00:00 is
     * selected as end date which ignores changes which were made during the
     * current day.
     *
     * @param filter
     */
    private static void increaseEndDateByOneDay(ReportingFilter filter) {
        final DateSearchFilter bisDateFilter = filter.getBisDateFilter();
        final Date endDate = bisDateFilter.getSelected();
        final Date newEndDate = DateUtils.addDays(endDate, 1);
        bisDateFilter.setSelected(newEndDate);
    }

    private static void decreaseEndDateByOneDay(ReportingFilter filter) {
        final DateSearchFilter bisDateFilter = filter.getBisDateFilter();
        final Date endDate = bisDateFilter.getSelected();
        final Date newEndDate = DateUtils.addDays(endDate, -1);
        bisDateFilter.setSelected(newEndDate);
    }

    private void addProzessbaukastenKeyFigures(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        addFachteamProzessbaukastenKeyFigure(filter, keyFigures, filteredIdTypeTuples);
        addTteamProzessbaukastenKeyFigure(filter, keyFigures, filteredIdTypeTuples);
        addVereinbarungProzessbaukastenKeyFigure(filter, keyFigures, filteredIdTypeTuples);

        final FreigegebenProzessbaukastenKeyFigure freigegebenProzessbaukastenKeyFigure = keyFigureFactory.computeFreigegebenProzessbaukastenKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(freigegebenProzessbaukastenKeyFigure);
    }

    private void addVereinbarungProzessbaukastenKeyFigure(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        final VereinbarungProzessbaukastenKeyFigure vereinbarungProzessbaukastenKeyFigure = keyFigureFactory.computeVereinbarungProzessbaukastenKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(vereinbarungProzessbaukastenKeyFigure);
    }

    private void addTteamProzessbaukastenKeyFigure(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        final TteamProzessbaukastenKeyFigure tteamProzessbaukastenKeyFigure = keyFigureFactory.computeTteamProzessbaukastenKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(tteamProzessbaukastenKeyFigure);
    }

    private void addFachteamProzessbaukastenKeyFigure(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        final FachteamProzessbaukastenKeyFigure fachteamProzessbaukastenKeyFigure = keyFigureFactory.computeFachteamProzessbaukastenKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(fachteamProzessbaukastenKeyFigure);
    }

    private void addMeldungExistingAnforderungZugeordnetKeyFigure(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        final FachteamMeldungExistingAnforderungKeyFigure fachteamMeldungExistingAnforderungKeyFigure = keyFigureFactory.computeFachteamMeldungExistingAnforderungKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(fachteamMeldungExistingAnforderungKeyFigure);
    }

    private void addProcessstepEntryKeyFigures(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        addTteamEntryKeyFigure(filter, keyFigures, filteredIdTypeTuples);
        getTteamPlausbilisiertKeyFigure(filter, keyFigures, filteredIdTypeTuples);
        getVereinbarungFreigegebenKeyFigure(filter, keyFigures, filteredIdTypeTuples);
    }

    private void getVereinbarungFreigegebenKeyFigure(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        final VereinbarungFreigegebenKeyFigure vereinbarungFreigegebenKeyFigure = keyFigureFactory.computeFreigegebenEntryKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(vereinbarungFreigegebenKeyFigure);
    }

    private void getTteamPlausbilisiertKeyFigure(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        final TteamPlausibilisiertKeyFigure tteamPlausibilisiertKeyFigure = keyFigureFactory.computeVereinbarungEntryKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(tteamPlausibilisiertKeyFigure);
    }

    private void addTteamEntryKeyFigure(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        final FachteamAnforderungAbgestimmtKeyFigure fachteamAnforderungAbgestimmtKeyFigure = keyFigureFactory.computeTteamEntryKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(fachteamAnforderungAbgestimmtKeyFigure);
    }

    private void addGeloeschtKeyFigures(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        addFachteamGeloeschteObjekteKeyFigure(filter, filteredIdTypeTuples, keyFigures);

        addTteamGeloeschtKeyFigure(filter, keyFigures, filteredIdTypeTuples);

        addVereinbarungGeloeschtKeyFigure(filter, keyFigures, filteredIdTypeTuples);

        final FreigegebenGeloeschtKeyFigure freigegebenGeloeschtKeyFigure = keyFigureFactory.computeFreigegebenGeloeschtKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(freigegebenGeloeschtKeyFigure);
    }

    private void addVereinbarungGeloeschtKeyFigure(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        final VereinbarungGeloeschtKeyFigure vereinbarungGeloeschtKeyFigure = keyFigureFactory.computeVereinbarungGeloeschtKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(vereinbarungGeloeschtKeyFigure);
    }

    private void addTteamGeloeschtKeyFigure(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        final TteamGeloeschtKeyFigure tteamGeloeschtKeyFigure = keyFigureFactory.computeTteamGeloeschtKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(tteamGeloeschtKeyFigure);
    }

    private void addFachteamGeloeschteObjekteKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples, KeyFigureCollection keyFigures) {
        final FachteamGeloeschteObjekteKeyFigure fachteamGeloeschteObjekteKeyFigure = keyFigureFactory.computeFachteamGeloeschteObjekteKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(fachteamGeloeschteObjekteKeyFigure);
    }

    private void addUnstimmigKeyFigures(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        TteamUnstimmigKeyFigure tteamUnstimmigKeyFigure = keyFigureFactory.computeTteamUnstimmigKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(tteamUnstimmigKeyFigure);

        VereinbarungUnstimmigKeyFigure vereinbarungUnstimmigKeyFigure = keyFigureFactory.computeVereinbarungUnstimmigKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(vereinbarungUnstimmigKeyFigure);
    }

    private void addFachteamAnforderungNewVersionKeyFigure(KeyFigureCollection keyFigures, ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final FachteamAnforderungNewVersionKeyFigure fachteamAnforderungNewVersionKeyFigure = keyFigureFactory.computeFachteamAnforderungNewVersionKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(fachteamAnforderungNewVersionKeyFigure);
    }

    private ProcessOverviewKeyFigures buildProcessOverviewKeyFigures(ReportingFilter filter, KeyFigureCollection keyFigures, DurchlaufzeitKeyFigureCollection durchlaufzeitKeyFigures) {
        String reportingPeriod = getReportingPeriod(filter);
        Collection<KeyFigureValidationResult> validationResults = keyFigureValidationService.validateKeyFigures(keyFigures);
        return new ProcessOverviewKeyFigures(keyFigures, reportingPeriod, validationResults, durchlaufzeitKeyFigures);
    }

    private void addMeldungEntryKeyFigure(ReportingFilter filter, KeyFigureCollection keyFigures, IdTypeTupleCollection filteredIdTypeTuples) {
        final FachteamGemeldeteMeldungenKeyFigure fachteamGemeldeteMeldungenKeyFigure = keyFigureFactory.computeFachteamGemeldeteMeldungenKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(fachteamGemeldeteMeldungenKeyFigure);
    }

    private void addSummeUnstimmig(KeyFigureCollection keyFigures) {
        final FachteamUnstimmigKeyFigure fachteamUnstimmigKeyFigure;
        try {
            fachteamUnstimmigKeyFigure = keyFigureFactory.computeFachteamUnstimmigKeyFigure(keyFigures);
            keyFigures.add(fachteamUnstimmigKeyFigure);
        } catch (InvalidDataException ex) {
            LOG.warn("Build of Key Figures FachteamUnstimmig failed", ex);
        }

    }

    private void addSummeFachteamEntry(KeyFigureCollection keyFigures) {
        try {
            final FachteamInputKeyFigure fachteamInputKeyFigure = keyFigureFactory.computeFachteamInputKeyFigure(keyFigures);
            keyFigures.add(fachteamInputKeyFigure);
        } catch (InvalidDataException ex) {
            LOG.warn("Build of Key Figures FachtemInput failed", ex);
        }
    }

    private void addSummeZeitpunktExit(KeyFigureCollection keyFigures) {
        addFachteamOutputKeyFigure(keyFigures);
        addTteamOutputKeyFigure(keyFigures);
        addVereinbarungOutputKeyFigure(keyFigures);
    }

    private void addVereinbarungOutputKeyFigure(KeyFigureCollection keyFigures) {
        try {
            final VereinbarungOutputKeyFigure vereinbarungOutputKeyFigure = keyFigureFactory.computeVereinbarungOutputKeyFigure(keyFigures);
            keyFigures.add(vereinbarungOutputKeyFigure);
        } catch (InvalidDataException ex) {
            LOG.warn("Build of Key Figures VereinbarungOutput failed", ex);
        }
    }

    private void addTteamOutputKeyFigure(KeyFigureCollection keyFigures) {
        try {
            final TteamOutputKeyFigure tteamOutputKeyFigure = keyFigureFactory.computeTteamOutputKeyFigure(keyFigures);
            keyFigures.add(tteamOutputKeyFigure);
        } catch (InvalidDataException ex) {
            LOG.warn("Build of Key Figures TteamOutput failed", ex);
        }
    }

    private void addFachteamOutputKeyFigure(KeyFigureCollection keyFigures) {
        try {
            final FachteamOutputKeyFigure fachteamOutputKeyFigure = keyFigureFactory.computeFachteamOutputKeyFigure(keyFigures);
            keyFigures.add(fachteamOutputKeyFigure);
        } catch (InvalidDataException ex) {
            LOG.warn("Build of Key Figures FachteamOutput failed", ex);
        }
    }

    private void addZeitpunkte(KeyFigureCollection keyFigures, ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {

        addFachteamCurrentKeyFigure(filter, filteredIdTypeTuples, keyFigures);

        addTteamCurrentKeyFigure(keyFigures, filter, filteredIdTypeTuples);

        addVereinbarungCurrentKeyFigure(keyFigures, filter, filteredIdTypeTuples);

        addFreigegebenCurrentKeyFigure(keyFigures, filter, filteredIdTypeTuples);

    }

    private void addVereinbarungCurrentKeyFigure(KeyFigureCollection keyFigures, ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final VereinbarungCurrentKeyFigure vereinbarungCurrentKeyFigure = keyFigureFactory.computeVereinbarungCurrentKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(vereinbarungCurrentKeyFigure);
    }

    private void addTteamCurrentKeyFigure(KeyFigureCollection keyFigures, ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final TteamCurrentKeyFigure tteamCurrentKeyFigure = keyFigureFactory.computeTteamCurrentKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(tteamCurrentKeyFigure);
    }

    private void addFreigegebenCurrentKeyFigure(KeyFigureCollection keyFigures, ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final FreigegebenCurrentKeyFigure freigegebenCurrentKeyFigure = keyFigureFactory.computeFreigegebenCurrentKeyFigure(filter, filteredIdTypeTuples);
        keyFigures.add(freigegebenCurrentKeyFigure);
    }

    private void addFachteamCurrentKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples, KeyFigureCollection keyFigures) {
        final FachteamCurrentObjectsFigure fachteamCurrentObjectsFigure = keyFigureFactory.computeFachteamCurrentObjectsFigure(filter, filteredIdTypeTuples);
        keyFigures.add(fachteamCurrentObjectsFigure);
    }

    public KeyFigureCollection getKeyFiguresForTteamReporting(ReportingFilter filter) {

        increaseEndDateByOneDay(filter);

        final IdTypeTupleCollection filteredIdTypeTuples = keyFigureFilterService.getFilteredIdTypeTuples(filter);

        KeyFigureCollection keyFigures = new KeyFigureCollection();

        addTteamCurrentKeyFigure(keyFigures, filter, filteredIdTypeTuples);
        addVereinbarungCurrentKeyFigure(keyFigures, filter, filteredIdTypeTuples);

        addTteamGeloeschtKeyFigure(filter, keyFigures, filteredIdTypeTuples);
        addVereinbarungGeloeschtKeyFigure(filter, keyFigures, filteredIdTypeTuples);

        addTteamProzessbaukastenKeyFigure(filter, keyFigures, filteredIdTypeTuples);
        addVereinbarungProzessbaukastenKeyFigure(filter, keyFigures, filteredIdTypeTuples);

        addTteamEntryKeyFigure(filter, keyFigures, filteredIdTypeTuples);

        getTteamPlausbilisiertKeyFigure(filter, keyFigures, filteredIdTypeTuples);
        getVereinbarungFreigegebenKeyFigure(filter, keyFigures, filteredIdTypeTuples);

        addUnstimmigKeyFigures(filter, keyFigures, filteredIdTypeTuples);

        addTteamOutputKeyFigure(keyFigures);
        addVereinbarungOutputKeyFigure(keyFigures);

        decreaseEndDateByOneDay(filter);

        LOG.debug("keyFigures: {}", keyFigures);

        return keyFigures;
    }
}
