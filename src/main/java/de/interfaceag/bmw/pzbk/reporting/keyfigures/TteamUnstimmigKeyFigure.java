package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class TteamUnstimmigKeyFigure extends AbstractKeyFigure {

    public TteamUnstimmigKeyFigure(Collection<Long> ids) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.TTEAM_UNSTIMMIG);
    }

    public TteamUnstimmigKeyFigure(Collection<Long> ids, Long runTime) {
        super(ids.size(), IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.TTEAM_UNSTIMMIG, runTime);
    }

}
