package de.interfaceag.bmw.pzbk.reporting.statustransition.migration;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author fn
 */
public class MeldungHistoryMigrationDto implements Serializable {

    private final long meldungId;

    private final Date datum;

    private final String objektName;

    private final String von;

    private final String auf;

    private MeldungHistoryAction action;

    public MeldungHistoryMigrationDto(long meldungId, Date datum, String objektName, String von, String auf) {
        this.meldungId = meldungId;
        this.datum = datum;
        this.objektName = objektName;
        this.von = von;
        this.auf = auf;
        this.action = MeldungHistoryActionConverter.convert(objektName, von, auf);
    }

    @Override
    public String toString() {
        return "MeldungHistoryMigrationDto{" + "meldungId=" + meldungId + ", datum=" + datum + ", objektName=" + objektName + ", von=" + von + ", auf=" + auf + '}';
    }

    public MeldungHistoryAction getAction() {
        return action;
    }

    public void setAction(MeldungHistoryAction action) {
        this.action = action;
    }

    public long getMeldungId() {
        return meldungId;
    }

    public Date getDatum() {
        return datum;
    }

    public String getObjektName() {
        return objektName;
    }

    public String getVon() {
        return von;
    }

    public String getAuf() {
        return auf;
    }

}
