package de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht;

import java.io.Serializable;

/**
 *
 * @author evda
 */
public interface Anforderungsuebersicht extends Serializable {

    String getId();

    String getFachId();

    String getVersion();

    String getFachIdAndVersion();

    String getAnforderungstext();

    String getKommentar();

    String getFachteam();

    String getTteam();

    String getStatus();

    String getDatum();

    String getAnforderungstextShort();

    String getKommentarShort();

    Long getDurchlaufzeit();

    Boolean isLanglaufer();

}
