package de.interfaceag.bmw.pzbk.reporting.svg;

import de.interfaceag.bmw.pzbk.shared.dto.Point;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lomu
 */
public class InputBox implements SVGElement {

    private final double x;
    private final double y;
    private final InputBoxType type;
    private final Map<String, String> inputBoxTextStrings;

    public InputBox() {
        this.x = 0;
        this.y = 0;
        this.type = null;
        this.inputBoxTextStrings = null;
    }

    public InputBox(double xCoordinate, double yCoordinate, InputBoxType type, Map<String, String> inputBoxTextStrings) {
        this.x = xCoordinate;
        this.y = yCoordinate;
        this.type = type;
        this.inputBoxTextStrings = inputBoxTextStrings;
    }

    @Override
    public String draw() {
        SvgElementCollection elementCollection = new SvgElementCollection();

        List<Point> points = Arrays.asList(new Point(x + 124.48, y + 71.41), new Point(x + 107.18, y + 6.14), new Point(x + 0.5, y + 6.14), new Point(x + 0.52, y + 136.65), new Point(x + 107.2, y + 136.65), new Point(x + 124.48, y + 71.41));
        SVGElement polygon1 = new Polygon(type.polygonClass1, points);
        SVGElement polygon2 = new Polygon(type.polygonClass2, points);
        SVGElement group1 = new Group(type.groupClass1, Arrays.asList(polygon1, polygon2));
        SVGElement group2 = new Group(type.groupClass2, group1);
        SVGElement group3 = new Group(type.groupClass3, group2);
        elementCollection.add(group3);

        elementCollection.add(new Text(type.getTextClass(), "translate(" + (x + 26.12) + " " + (y + 48.42) + ")", inputBoxTextStrings.get("staerken")));
        elementCollection.add(new Text(type.getTextClass(), "translate(" + (x + 75.26) + " " + (y + 48.42) + ")", "/"));
        elementCollection.add(new Text(type.getTextClass(), "translate(" + (x + 13.69) + " " + (y + 67.62) + ")", inputBoxTextStrings.get("schwaechen")));
        elementCollection.add(new Text(type.getTextClass(), "translate(" + (x + 101.71) + " " + (y + 67.62) + ")", "\n"));
        elementCollection.add(new Text(type.getTextClass(), "translate(" + (x + 43.9) + " " + (y + 86.82) + ")", inputBoxTextStrings.get("aus")));
        elementCollection.add(new Text(type.getTextClass(), "translate(" + (x + 71.47) + " " + (y + 86.82) + ")", "\n"));
        elementCollection.add(new Text(type.getTextClass(), "translate(" + (x + 14.71) + " " + (y + 106.02) + ")", inputBoxTextStrings.get("werken")));

        return elementCollection.draw();
    }

    public enum InputBoxType {
        SIMPLE("6750b86e-6d7e-4150-af85-a92b4c419fc5", "948d7ce9-e863-45e7-a3e6-179e4cab85de", "396e18d4-985a-416e-8e80-c6e04f9e6963", "f8b67d6e-2a5b-4c45-b1fa-aaeb448e4c8b", "349b8e67-6303-488d-a7e9-fa034879202a", "ffa89ab7-8120-4f9d-b7d7-33e849fbefce"),
        DETAIL("04a1c0a0-ee57-45d9-81e5-8b9547204d5a", "01e056ae-e1a3-4d3c-bc83-4c867c3167ae", "de6e5232-07bb-41d1-94c9-bac358104b1c", "2d24508d-5474-4519-8463-2fe54f9ec290", "a7a7d0c6-d834-4f16-aa8c-4d2cc48187d8", "5f459e6e-0411-4145-a215-c5424afc40fe"); //Alte Textclass für Fettgedruckt :"0d95121d-433d-42b6-a81c-6f7ded3a411c");

        private final String polygonClass1;
        private final String polygonClass2;
        private final String groupClass1;
        private final String groupClass2;
        private final String groupClass3;
        private final String textClass;

        InputBoxType(String polygonClass1, String polygonClass2, String groupClass1, String groupClass2, String groupClass3, String textClass) {
            this.polygonClass1 = polygonClass1;
            this.polygonClass2 = polygonClass2;
            this.groupClass1 = groupClass1;
            this.groupClass2 = groupClass2;
            this.groupClass3 = groupClass3;
            this.textClass = textClass;
        }

        public String getPolygonClass1() {
            return polygonClass1;
        }

        public String getPolygonClass2() {
            return polygonClass2;
        }

        public String getGroupClass1() {
            return groupClass1;
        }

        public String getGroupClass2() {
            return groupClass2;
        }

        public String getGroupClass3() {
            return groupClass3;
        }

        public String getTextClass() {
            return textClass;
        }

    }
}
