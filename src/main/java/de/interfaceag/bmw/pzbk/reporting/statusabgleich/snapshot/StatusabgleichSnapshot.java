package de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sl
 */
@Entity
@NamedQueries({
    @NamedQuery(name = StatusabgleichSnapshot.FOR_DERIVAT_AND_DATE, query = "SELECT DISTINCT s FROM StatusabgleichSnapshot s WHERE s.derivat = :derivat AND s.datum =:datum ORDER BY s.datum DESC"),
    @NamedQuery(name = StatusabgleichSnapshot.ALL_FOR_DERIVAT, query = "SELECT DISTINCT s FROM StatusabgleichSnapshot s WHERE s.derivat = :derivat ORDER BY s.datum DESC"),
    @NamedQuery(name = StatusabgleichSnapshot.ALL_DATES_FOR_DERIVAT, query = "SELECT DISTINCT s.id, s.datum FROM StatusabgleichSnapshot s WHERE s.derivat = :derivat ORDER BY s.datum DESC")})
public class StatusabgleichSnapshot implements Serializable {

    private static final String CLASSNAME = "StatusabgleichSnapshot.";
    public static final String FOR_DERIVAT_AND_DATE = CLASSNAME + "forDerivatAndDate";
    public static final String ALL_FOR_DERIVAT = CLASSNAME + "allForDerivat";
    public static final String ALL_DATES_FOR_DERIVAT = CLASSNAME + "allDatesForDerivat";

    @Id
    @SequenceGenerator(name = "statusabgleichsnapshot_id_seq",
            sequenceName = "statusabgleichsnapshot_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "statusabgleichsnapshot_id_seq")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date datum;

    @ManyToOne
    private Derivat derivat;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    private List<StatusabgleichSnapshotEntry> entries = new ArrayList<>();

    public StatusabgleichSnapshot() {
        this.datum = new Date();
    }

    public StatusabgleichSnapshot(Derivat derivat) {
        this.datum = new Date();
        this.derivat = derivat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Derivat getDerivat() {
        return derivat;
    }

    public void setDerivat(Derivat derivat) {
        this.derivat = derivat;
    }

    public List<StatusabgleichSnapshotEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<StatusabgleichSnapshotEntry> entries) {
        this.entries = entries;
    }

    public void addEntry(StatusabgleichSnapshotEntry entry) {
        this.entries.add(entry);
    }

}
