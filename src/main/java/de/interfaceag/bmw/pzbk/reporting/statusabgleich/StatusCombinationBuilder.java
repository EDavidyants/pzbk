package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class StatusCombinationBuilder {

    private StatusCombinationBuilder() {
    }

    protected static Set<Tuple<DerivatAnforderungModulStatus, ZakStatus>> buildCombinations(
            Collection<DerivatAnforderungModulStatus> derivatAnforderungModulStatusCollection,
            Collection<ZakStatus> zakStatusCollection) {

        Set<Tuple<DerivatAnforderungModulStatus, ZakStatus>> combinations = new HashSet<>();

        for (DerivatAnforderungModulStatus derivatAnforderungModulStatus : derivatAnforderungModulStatusCollection) {
            for (ZakStatus zakStatus : zakStatusCollection) {
                Tuple<DerivatAnforderungModulStatus, ZakStatus> newCombination = new GenericTuple<>(derivatAnforderungModulStatus, zakStatus);
                combinations.add(newCombination);
            }
        }

        return combinations;

    }

}
