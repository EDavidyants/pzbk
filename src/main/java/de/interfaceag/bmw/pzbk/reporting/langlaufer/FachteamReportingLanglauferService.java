package de.interfaceag.bmw.pzbk.reporting.langlaufer;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;

public interface FachteamReportingLanglauferService {

    int getFachteamLanglauferCountBySensorCoc(SensorCoc sensorCoc);
}
