package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.filter.AbstractMultiValueFilter;
import de.interfaceag.bmw.pzbk.filter.Filter;
import de.interfaceag.bmw.pzbk.filter.FilterToUrlConverter;
import de.interfaceag.bmw.pzbk.filter.SingleDerivatFilter;
import de.interfaceag.bmw.pzbk.filter.SingleIdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SingleSnapshotFilter;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import de.interfaceag.bmw.pzbk.filter.TechnologieFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ProjectProcessFilter implements Serializable, ProjectProzessFilterController {

    private static final Page PAGE = Page.PROJEKTREPORTING_PROZESS;

    private final boolean snapshot;

    @Filter
    private final SingleIdSearchFilter derivatFilter;
    @Filter
    private final SingleIdSearchFilter snapshotFilter;
    @Filter
    private final AbstractMultiValueFilter technologieFilter;

    public ProjectProcessFilter(UrlParameter urlParameter,
            Collection<Derivat> allDerivate, Collection<SnapshotFilter> allSnapshots,
            Collection<SensorCoc> allSensorCocs) {
        this.derivatFilter = new SingleDerivatFilter(allDerivate, urlParameter);
        this.snapshotFilter = new SingleSnapshotFilter(allSnapshots, urlParameter);
        this.technologieFilter = new TechnologieFilter(allSensorCocs, urlParameter);
        this.snapshot = false;
    }

    public ProjectProcessFilter(UrlParameter urlParameter,
            Collection<Derivat> allDerivate, Collection<SnapshotFilter> allSnapshots,
            Collection<SensorCoc> allSensorCocs, boolean snapshot) {
        this.derivatFilter = new SingleDerivatFilter(allDerivate, urlParameter);
        this.snapshotFilter = new SingleSnapshotFilter(allSnapshots, urlParameter);
        this.technologieFilter = new TechnologieFilter(allSensorCocs, urlParameter);
        this.snapshot = snapshot;
    }

    public boolean isSnapshot() {
        return snapshot;
    }

    public SingleIdSearchFilter getDerivatFilter() {
        return derivatFilter;
    }

    public SingleIdSearchFilter getSnapshotFilter() {
        return snapshotFilter;
    }

    public AbstractMultiValueFilter getTechnologieFilter() {
        return technologieFilter;
    }

    public boolean isDerivatSelected() {
        return derivatFilter.isActive();
    }

    @Override
    public String filter() {
        return PAGE.getUrl() + getUrlParameter();
    }

    @Override
    public String reset() {
        return PAGE.getUrl() + "?faces-redirect=true";
    }

    private String getUrlParameter() {
        FilterToUrlConverter urlConverter = FilterToUrlConverter.forClass(this);
        return urlConverter.getUrl();
    }

    @Override
    public ProjectProcessFilter getFilter() {
        return this;
    }

}
