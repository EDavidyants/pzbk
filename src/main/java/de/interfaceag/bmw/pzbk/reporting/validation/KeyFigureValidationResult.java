package de.interfaceag.bmw.pzbk.reporting.validation;

import java.io.Serializable;
import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class KeyFigureValidationResult implements Serializable {

    private final boolean valid;
    private final String validationMessage;

    @Override
    public String toString() {
        return "ValidationResult{" + "valid=" + valid + ", validationMessage=" + validationMessage + '}';
    }

    public KeyFigureValidationResult(boolean valid) {
        this.valid = valid;
        this.validationMessage = "";
    }

    public KeyFigureValidationResult(boolean valid, String validationMessage) {
        this.valid = valid;
        this.validationMessage = validationMessage;
    }

    public boolean isValid() {
        return valid;
    }

    public String getValidationMessage() {
        return Optional.ofNullable(validationMessage).orElse("");
    }

}
