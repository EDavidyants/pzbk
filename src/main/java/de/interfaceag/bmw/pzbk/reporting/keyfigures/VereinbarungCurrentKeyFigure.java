package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class VereinbarungCurrentKeyFigure extends AbstractLanglauferKeyFigure {

    public VereinbarungCurrentKeyFigure(Collection<Long> ids) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.VEREINBARUNG_CURRENT);
    }

    public VereinbarungCurrentKeyFigure(Collection<Long> ids, boolean langlaufer) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.VEREINBARUNG_CURRENT);
        super.setLanglaufer(langlaufer);
    }

    public VereinbarungCurrentKeyFigure(Collection<Long> ids, Long runTime) {
        super(ids.size(), IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.VEREINBARUNG_CURRENT, runTime);
    }

}
