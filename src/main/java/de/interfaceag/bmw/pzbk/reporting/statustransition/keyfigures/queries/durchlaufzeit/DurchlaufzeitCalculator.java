package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTuple;
import de.interfaceag.bmw.pzbk.shared.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public final class DurchlaufzeitCalculator {

    private static final Logger LOG = LoggerFactory.getLogger(DurchlaufzeitCalculator.class);

    private DurchlaufzeitCalculator() {
    }

    public static Long computeDurchlaufzeit(Collection<DurchlaufzeitPart> part1, Collection<DurchlaufzeitPart> part2) {
        Set<DurchlaufzeitPart> durchlaufzeitParts = new HashSet<>();
        durchlaufzeitParts.addAll(part1);
        durchlaufzeitParts.addAll(part2);
        return computeDurchlaufzeit(durchlaufzeitParts);
    }

    public static Long computeDurchlaufzeit(Collection<DurchlaufzeitPart> part1, Collection<DurchlaufzeitPart> part2, Collection<DurchlaufzeitPart> part3) {
        Set<DurchlaufzeitPart> durchlaufzeitParts = new HashSet<>();
        durchlaufzeitParts.addAll(part1);
        durchlaufzeitParts.addAll(part2);
        durchlaufzeitParts.addAll(part3);
        return computeDurchlaufzeit(durchlaufzeitParts);
    }

    static Long computeDurchlaufzeit(Collection<DurchlaufzeitPart> durchlaufzeitParts) {
        final Long sum = durchlaufzeitParts.stream().map(DurchlaufzeitPart::getEntryExitDifference).reduce(0L, Long::sum);
        final long days = DateUtils.convertLongToDays(sum);

        // use this to count objects
//        final long sizeOfObjects = durchlaufzeitParts.stream().map(DurchlaufzeitPart::getIdTypeTuple).distinct().count();
        // use this to count loops
        final int sizeOfLoops = durchlaufzeitParts.size();

        if (sizeOfLoops > 0) {
            return days / sizeOfLoops;
        } else {
            return 0L;
        }
    }

    public static Map<IdTypeTuple, Long> computeDurchlaufzeitForActiveIdTypeTuple(Collection<DurchlaufzeitPart> durchlaufzeitParts) {
        final Collection<IdTypeTuple> activeIdTypeTuples = durchlaufzeitParts.stream().filter(DurchlaufzeitPart::isActive).map(DurchlaufzeitPart::getIdTypeTuple).collect(Collectors.toSet());
        final List<DurchlaufzeitPart> activeDurchlaufzeitParts = durchlaufzeitParts.stream().filter(durchlaufzeitPart -> activeIdTypeTuples.contains(durchlaufzeitPart.getIdTypeTuple())).collect(Collectors.toList());
        final Map<IdTypeTuple, Long> idTypeTupleDurchlaufzeitDaysMap = computeIdTypeTupleDurchlaufzeitMap(activeDurchlaufzeitParts);
        LOG.debug("Active IdTypeDurchlaufzeit map: {}", idTypeTupleDurchlaufzeitDaysMap);
        return idTypeTupleDurchlaufzeitDaysMap;
    }

    private static Map<IdTypeTuple, Long> computeIdTypeTupleDurchlaufzeitMap(Collection<DurchlaufzeitPart> durchlaufzeitParts) {
        final Map<IdTypeTuple, Long> idTypeTupleDurchlaufzeitMap = durchlaufzeitParts.stream().distinct()
                .collect(Collectors.groupingBy(DurchlaufzeitPart::getIdTypeTuple, Collectors.summingLong(DurchlaufzeitPart::getEntryExitDifference)));

        return idTypeTupleDurchlaufzeitMap.entrySet().stream().distinct()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> DateUtils.convertLongToDays(entry.getValue())));
    }
}
