package de.interfaceag.bmw.pzbk.reporting.statustransition.migration;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.AnforderungReportingStatusTransitionAdapterForMigration;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.ReportingStatusTransitionDatabaseAdapter;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.meldung.MeldungReportingStatusTransitionAdapterForMigration;
import de.interfaceag.bmw.pzbk.reporting.statustransition.meldung.ReportingMeldungStatusTransitionDatabaseAdapter;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author fn
 */
@Stateless
@Named
public class ReportingStatusTransitionMigrationService implements Serializable, ReportingStatusTransitionMigrationAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(ReportingStatusTransitionMigrationService.class);

    @Inject
    private AnforderungService anforderungService;

    @Inject
    private AnforderungReportingStatusTransitionAdapterForMigration anforderungReportingStatusTransitionAdapterForMigration;

    @Inject
    private MeldungReportingStatusTransitionAdapterForMigration meldungReportingStatusTransitionAdapterForMigration;
    @Inject
    private ReportingMeldungStatusTransitionDatabaseAdapter reportingMeldungStatusTransitionDatabaseAdapter;
    @Inject
    private ReportingStatusTransitionDatabaseAdapter reportingStatusTransitionDatabaseAdapter;

    @Inject
    private AnforderungMeldungHistoryService historyService;

    @Override
    public ReportingStatusTransitionMigrationResult migrateAnforderung(Anforderung anforderung) {

        if (historyForAnforderungAlreadyExists(anforderung)) {
            return buildReportingStatusTransitionMigrationResult(Collections.emptyList(), anforderung);
        }
        List<AnforderungHistoryMigrationDto> historyEntries = getHistoryEntries(anforderung);
        List<ReportingStatusTransition> statusTransitions = historyEntries.stream().map(entry -> migrateHistoryEntry(anforderung, entry))
                .filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());

        return buildReportingStatusTransitionMigrationResult(statusTransitions, anforderung);

    }

    @Override
    public List<ReportingStatusTransitionMigrationResult> migrateAnforderungen() {
        List<Anforderung> allAnforderungen = getAllAnforderungenWithMeldung();
        return allAnforderungen.stream().map(this::migrateAnforderung).collect(Collectors.toList());
    }

    private List<Anforderung> getAllAnforderungenWithMeldung() {
        return anforderungService.getAllAnforderungenWithMeldung();
    }

    private List<AnforderungHistoryMigrationDto> getHistoryEntries(Anforderung anforderung) {
        return historyService.getAnforderungHistoryMigrationDtosForAnforderung(anforderung);
    }

    private Optional<ReportingStatusTransition> migrateHistoryEntry(Anforderung anforderung, AnforderungHistoryMigrationDto entry) {

        AnforderungHistoryAction action = entry.getAction();
        Date datum = entry.getDatum();

        switch (action) {
            case CREATION:
                return Optional.ofNullable(anforderungReportingStatusTransitionAdapterForMigration.createAnforderungFromMeldung(anforderung, datum));
            case STATUS_CHANGE:
                final String von = entry.getVon();
                final String auf = entry.getAuf();

                Status vonStatus = Status.getAnforderungStatusByBezeichnung(von);
                Status aufStatus = Status.getAnforderungStatusByBezeichnung(auf);

                return Optional.ofNullable(anforderungReportingStatusTransitionAdapterForMigration.changeAnforderungStatus(anforderung, vonStatus, aufStatus, datum));
            case ADD_TO_PROZESSBAUKASTEN:
                return Optional.ofNullable(anforderungReportingStatusTransitionAdapterForMigration.addAnforderungToProzessbaukasten(anforderung, anforderung.getStatus(), datum));
            case REMOVE_FROM_PROZESSBAUKASTEN:
                return Optional.ofNullable(anforderungReportingStatusTransitionAdapterForMigration.removeAnforderungFromProzessbaukasten(anforderung, anforderung.getStatus(), datum));
            case NEW_VERSION:
                return Optional.ofNullable(anforderungReportingStatusTransitionAdapterForMigration.createAnforderungAsNewVersion(anforderung, datum));
            case RESTORE:
                return Optional.ofNullable(anforderungReportingStatusTransitionAdapterForMigration.restoreAnforderung(anforderung, datum));
            case OTHER:
            default:
                LOG.info("Do nothing for entry {}", entry);
                return Optional.empty();
        }
    }

    private ReportingStatusTransitionMigrationResult buildReportingStatusTransitionMigrationResult(List<ReportingStatusTransition> statusTransitions, Anforderung anforderung) {
        return new ReportingStatusTransitionMigrationResult(anforderung.getId(), anforderung.toString(), statusTransitions);
    }

    @Override
    public ReportingMeldungStatusTransitionMigrationResult migrateMeldung(Meldung meldung) {

        if (historyForMeldungAlreadyExists(meldung)) {
            return buildReportingMeldungStatusTransitionMigrationResult(Collections.emptyList(), meldung);
        }

        List<MeldungHistoryMigrationDto> historyEntries = getMeldungHistoryEntries(meldung);

        List<ReportingMeldungStatusTransition> statusTransitions = new ArrayList<>();

        iterateOverAllEntriesAndMigrate(historyEntries, meldung, statusTransitions);

        return buildReportingMeldungStatusTransitionMigrationResult(statusTransitions, meldung);
    }

    private void iterateOverAllEntriesAndMigrate(List<MeldungHistoryMigrationDto> historyEntries, Meldung meldung, List<ReportingMeldungStatusTransition> statusTransitions) {
        ListIterator<MeldungHistoryMigrationDto> iterator = historyEntries.listIterator();

        if (iterator.hasNext()) {
            MeldungHistoryMigrationDto current = iterator.next();

            current = migrateAllMeldungEntries(iterator, meldung, current, statusTransitions);
            migrateMeldungHistoryEntry(meldung, current).ifPresent(statusTransitions::add);
        }
    }

    private MeldungHistoryMigrationDto migrateAllMeldungEntries(ListIterator<MeldungHistoryMigrationDto> iterator, Meldung meldung, MeldungHistoryMigrationDto current, List<ReportingMeldungStatusTransition> statusTransitions) {
        while (iterator.hasNext()) {

            MeldungHistoryMigrationDto next = iterator.next();

            if (!next.getAction().equals(MeldungHistoryAction.ADD_TO_EXISTING_ANFORDERUNG)) {
                migrateMeldungHistoryEntry(meldung, current).ifPresent(statusTransitions::add);
            }

            current = next;

        }
        return current;
    }

    @Override
    public List<ReportingMeldungStatusTransitionMigrationResult> migrateMeldungen() {
        List<Meldung> allMeldungen = getAllMeldungen();
        return allMeldungen.stream().map(this::migrateMeldung).collect(Collectors.toList());
    }

    private Optional<ReportingMeldungStatusTransition> migrateMeldungHistoryEntry(Meldung meldung, MeldungHistoryMigrationDto entry) {

        MeldungHistoryAction action = entry.getAction();
        Date datum = entry.getDatum();

        switch (action) {
            case STATUS_CHANGE:
                final String von = entry.getVon();
                final String auf = entry.getAuf();

                Status vonStatus = Status.getMeldungStatusByBezeichnung(von);
                Status aufStatus = Status.getMeldungStatusByBezeichnung(auf);

                return Optional.ofNullable(meldungReportingStatusTransitionAdapterForMigration.changeMeldungStatus(meldung, vonStatus, aufStatus, datum));
            case ADD_TO_NEW_ANFORDERUNG:
                return Optional.ofNullable(meldungReportingStatusTransitionAdapterForMigration.addMeldungToNewAnforderung(meldung, datum));
            case ADD_TO_EXISTING_ANFORDERUNG:
                return Optional.ofNullable(meldungReportingStatusTransitionAdapterForMigration.addMeldungToExistingAnforderung(meldung, datum));
            case REMOVE_FROM_ANFORDERUNG:
                return Optional.ofNullable(meldungReportingStatusTransitionAdapterForMigration.removeMeldungFromAnforderung(meldung, datum));
            case OTHER:
            default:
                LOG.info("Do nothing for entry {}", entry);
                return Optional.empty();
        }
    }

    private ReportingMeldungStatusTransitionMigrationResult buildReportingMeldungStatusTransitionMigrationResult(List<ReportingMeldungStatusTransition> statusTransitions, Meldung meldung) {
        return new ReportingMeldungStatusTransitionMigrationResult(meldung.getId(), meldung.toString(), statusTransitions);
    }

    private List<MeldungHistoryMigrationDto> getMeldungHistoryEntries(Meldung meldung) {
        return historyService.getMeldungHistoryMigrationDtosForAnforderung(meldung);
    }

    private List<Meldung> getAllMeldungen() {
        return anforderungService.getAllMeldungen();
    }

    private boolean historyForMeldungAlreadyExists(Meldung meldung) {
        return reportingMeldungStatusTransitionDatabaseAdapter.isHistoryAlreadyExisting(meldung);
    }

    private boolean historyForAnforderungAlreadyExists(Anforderung anforderung) {
        return reportingStatusTransitionDatabaseAdapter.isHistoryAlreadyExisting(anforderung);
    }

}
