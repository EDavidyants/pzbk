package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FreigegebenProzessbaukastenKeyFigure extends AbstractKeyFigure {

    public FreigegebenProzessbaukastenKeyFigure(Collection<Long> anforderungIds) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(anforderungIds), KeyType.FREIGEGEBEN_PZBK);
    }

}
