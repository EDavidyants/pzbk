package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class TteamProzessbaukastenKeyFigure extends AbstractKeyFigure {

    public TteamProzessbaukastenKeyFigure(Collection<Long> anforderungIds) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(anforderungIds), KeyType.TTEAM_PZBK);
    }

}
