package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.filter.AbstractMultiValueFilter;
import de.interfaceag.bmw.pzbk.filter.FachbereichFilter;
import de.interfaceag.bmw.pzbk.filter.Filter;
import de.interfaceag.bmw.pzbk.filter.FilterToUrlConverter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ModulFilter;
import de.interfaceag.bmw.pzbk.filter.SingleDerivatFilter;
import de.interfaceag.bmw.pzbk.filter.SingleIdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SingleSnapshotFilter;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import de.interfaceag.bmw.pzbk.filter.TechnologieFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ReportingStatusabgleichFilter implements Serializable {

    private static final Page PAGE = Page.PROJEKTREPORTING_STATUSABGLEICH;

    private boolean snapshot;

    @Filter
    private final SingleIdSearchFilter derivatFilter;
    @Filter
    private final SingleIdSearchFilter snapshotFilter;
    @Filter
    private AbstractMultiValueFilter fachbereichFilter;
    @Filter
    private IdSearchFilter modulFilter;
    @Filter
    private AbstractMultiValueFilter technologieFilter;

    public ReportingStatusabgleichFilter(UrlParameter urlParameter,
                                         Collection<Derivat> allDerivate,
                                         Collection<SnapshotFilter> allSnapshots,
                                         Collection<SensorCoc> allSensorCocs,
                                         List<String> allFachbereiche,
                                         List<Modul> allModuls) {
        this.derivatFilter = new SingleDerivatFilter(allDerivate, urlParameter);
        this.snapshotFilter = new SingleSnapshotFilter(allSnapshots, urlParameter);
        this.fachbereichFilter = new FachbereichFilter(allFachbereiche, urlParameter);
        this.modulFilter = new ModulFilter(allModuls, urlParameter);
        this.technologieFilter = new TechnologieFilter(allSensorCocs, urlParameter);
    }

    public void setRestrictedModulFilter(UrlParameter urlParameter, List<Modul> modules) {
        this.modulFilter = new ModulFilter(modules, urlParameter);
    }

    public void setRestrictedTechnoloigieFilter(UrlParameter urlParameter, List<SensorCoc> technologies) {
        this.technologieFilter = new TechnologieFilter(technologies, urlParameter);
    }

    public boolean isSnapshot() {
        return snapshot;
    }

    public void setSnapshot(boolean snapshot) {
        this.snapshot = snapshot;
    }

    public boolean isDerivatSelected() {
        return derivatFilter.isActive();
    }

    public AbstractMultiValueFilter getTechnologieFilter() {
        return technologieFilter;
    }

    public IdSearchFilter getModulFilter() {
        return modulFilter;
    }

    public AbstractMultiValueFilter getFachbereichFilter() {
        return fachbereichFilter;
    }

    public SingleIdSearchFilter getDerivatFilter() {
        return derivatFilter;
    }

    public SingleIdSearchFilter getSnapshotFilter() {
        return snapshotFilter;
    }

    public String filter() {
        return PAGE.getUrl() + getUrlParameter();
    }

    public String reset() {
        return PAGE.getUrl() + "?faces-redirect=true";
    }

    private String getUrlParameter() {
        FilterToUrlConverter urlConverter = FilterToUrlConverter.forClass(this);
        return urlConverter.getUrl();
    }
}
