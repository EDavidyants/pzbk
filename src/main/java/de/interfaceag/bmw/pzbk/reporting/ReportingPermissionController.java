package de.interfaceag.bmw.pzbk.reporting;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @param <T>
 */
public interface ReportingPermissionController<T> {

    T getViewPermission();

}
