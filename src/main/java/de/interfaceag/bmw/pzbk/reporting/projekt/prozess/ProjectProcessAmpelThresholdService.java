package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;

import javax.ejb.Stateless;
import javax.inject.Named;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author evda
 */
@Stateless
@Named
public class ProjectProcessAmpelThresholdService implements Serializable {

    public Double getThresholdForVereinbarungVKBG() {
        return getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.L3);
    }

    public Double getThresholdForVereinbarungZV() {
        return getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.R5);
    }

    public Double getThresholdForUmsetzungsverwaltungVKBG() {
        return getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.L11);
    }

    public Double getThresholdForUmsetzungsverwaltungVBBG() {
        return getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.R13);
    }

    public Double getThresholdForUmsetzungsverwaltungBBG() {
        return getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.R20);
    }

    public Double getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure keyFigure) {
        Map<ProjectReportingProcessKeyFigure, Double> thresholdValueForKeyFigureMap = getThresholdValuesMap();
        Double thresholdForGreenAmpel = thresholdValueForKeyFigureMap.get(keyFigure);

        return thresholdForGreenAmpel != null ? thresholdForGreenAmpel : 1.0;

    }

    private static Map<ProjectReportingProcessKeyFigure, Double> getThresholdValuesMap() {
        Map<ProjectReportingProcessKeyFigure, Double> thresholdValuesMap = new HashMap<>();

        thresholdValuesMap.put(ProjectReportingProcessKeyFigure.L3, 0.85);
        thresholdValuesMap.put(ProjectReportingProcessKeyFigure.L11, 0.60);
        thresholdValuesMap.put(ProjectReportingProcessKeyFigure.R5, 0.95);
        thresholdValuesMap.put(ProjectReportingProcessKeyFigure.R13, 0.85);
        thresholdValuesMap.put(ProjectReportingProcessKeyFigure.R20, 0.95);

        return thresholdValuesMap;
    }

}
