package de.interfaceag.bmw.pzbk.reporting.svg;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;

/**
 * @author lomu
 */
public class Diamond implements SVGElement {

    private final double x;
    private final double y;
    private final KeyFigure keyFigure;
    private final Type type;

    public Diamond(double xCoordinate, double yCoordinate, KeyFigure keyFigure, Type type) {
        this.x = xCoordinate;
        this.y = yCoordinate;
        this.keyFigure = keyFigure;
        this.type = type;
    }

    @Override
    public String draw() {
        SVGElement path1 = new Path(type.pathClass1, "M" + this.x + "," + this.y + "a3.37,3.37,0,0,0,0,4.95l19.09,19.1a3.39,3.39,0,0,0,4.95,0l19.09-19.1a3.37,3.37,0,0,0,0-4.95L" + (this.x + 24.04) + "," + (this.y - 19.09) + "a3.37,3.37,0,0,0-4.95,0Z", "translate(0)");
        SVGElement path2 = new Path(type.pathClass2, "M" + this.x + "," + this.y + "a3.37,3.37,0,0,0,0,4.95l19.09,19.1a3.39,3.39,0,0,0,4.95,0l19.09-19.1a3.37,3.37,0,0,0,0-4.95L" + (this.x + 24.04) + "," + (this.y - 19.09) + "a3.37,3.37,0,0,0-4.95,0Z", "translate(0)");

        double offsetX = 0;

        if (keyFigure == null) {
            offsetX = 19.0;
        } else if (keyFigure.getValue() >= 1000 && keyFigure.getValue() < 10000) {
            offsetX = 2.5;
        } else if (keyFigure.getValue() >= 100) {
            offsetX = 8.0;
        } else if (keyFigure.getValue() >= 10) {
            offsetX = 13.5;
        } else if (keyFigure.getValue() < 10) {
            offsetX = 19.0;
        }

        SVGElement label = new Text(type.textClass, "translate(" + Double.toString(this.x + offsetX) + " " + Double.toString(y + 8.3) + ")", new LinkToExcelDownload(keyFigure).draw());
        SvgElementCollection elementCollection = SvgElementCollection.of(path1, path2, label);
        return elementCollection.draw();
    }

    public enum Type {
        SIMPLE("1d7519ae-8173-4095-972a-09870179f637", "62cd76c4-80a4-4944-a40d-e6dd289b40b2", "ffa89ab7-8120-4f9d-b7d7-33e849fbefce"),
        DETAIL("1fc087fc-5ad7-4044-a16b-f1da2c7b2c10", "e10c3a5a-f925-4bfe-b875-b1dab086c709", "5f459e6e-0411-4145-a215-c5424afc40fe");

        private final String pathClass1;
        private final String pathClass2;
        private final String textClass;

        Type(String pathClass1, String pathClass2, String textClass) {
            this.pathClass1 = pathClass1;
            this.pathClass2 = pathClass2;
            this.textClass = textClass;
        }

        public String getPathClass1() {
            return pathClass1;
        }

        public String getPathClass2() {
            return pathClass2;
        }

        public String getTextClass() {
            return textClass;
        }

    }

}
