package de.interfaceag.bmw.pzbk.reporting.excel;

import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Dependent
public class ReportToExcelDao implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ReportToExcelDao.class.getName());

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager em;

    public Collection<ReportingExcelDataDto> getReportingSupportDataForMeldungen(Collection<Long> meldungIds) {
        LOG.debug("getReportingSupportDataForMeldungen called with meldungIds {}", meldungIds);
        if (meldungIds == null || meldungIds.isEmpty()) {
            return new ArrayList<>();
        }

        QueryPartDTO qp = new QueryPartDTO("SELECT DISTINCT m.id, m.fachId AS fullFachId,"
                + " m.beschreibungAnforderungDe, m.kommentarAnforderung, m.sensorCoc.technologie,"
                + " m.status, m.zeitpunktStatusaenderung,"
                + " m.erstellungsdatum, m.aenderungsdatum FROM Meldung m ");

        qp.append(" WHERE m.id IN :meldungIds ");
        qp.put("meldungIds", meldungIds);

        Query q = buildGenericQuery(qp);
        Collection<Object[]> result = q.getResultList();

        LOG.debug("getReportingSupportDataForMeldungen found {} results", result.size());

        return ReportingExcelDataDto.buildFromCollectionWithoutTteam(result);
    }

    public Collection<ReportingExcelDataDto> getReportingSupportDataForAnforderungen(Collection<Long> allAnforderungIds) {
        LOG.debug("getReportingSupportDataForAnforderungen called with anforderungIds {}", allAnforderungIds);

        if (allAnforderungIds == null || allAnforderungIds.isEmpty()) {
            return new ArrayList<>();
        }

        QueryPartDTO qp = new QueryPartDTO("SELECT DISTINCT a.id, CONCAT(a.fachId, ' | V', a.version) AS fullFachId,"
                + " a.beschreibungAnforderungDe, a.kommentarAnforderung, a.sensorCoc.technologie, a.tteam.teamName,"
                + " a.status, a.zeitpunktStatusaenderung, a.erstellungsdatum, a.aenderungsdatum FROM Anforderung a ");

        qp.append(" WHERE a.id IN :anfoIds ");
        qp.put("anfoIds", allAnforderungIds);

        Query q = buildGenericQuery(qp);
        Collection<Object[]> result = q.getResultList();

        LOG.debug("getReportingSupportDataForAnforderungen found {} results", result.size());

        return ReportingExcelDataDto.buildFromCollectionWithTteam(result);
    }

    private Query buildGenericQuery(QueryPartDTO qp) {
        Query query = em.createQuery(qp.getQuery());
        if (!qp.getParameterMap().isEmpty()) {
            qp.getParameterMap().forEach(query::setParameter);
        }

        return query;
    }
}
