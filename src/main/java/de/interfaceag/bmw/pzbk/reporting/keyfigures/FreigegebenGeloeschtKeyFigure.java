package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FreigegebenGeloeschtKeyFigure extends AbstractKeyFigure {

    public FreigegebenGeloeschtKeyFigure(Collection<Long> ids) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.FREIGEGEBEN_GELOESCHT);
    }

}
