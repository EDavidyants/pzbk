package de.interfaceag.bmw.pzbk.reporting.svg;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class KeyFigureBox implements SVGElement {

    private final String dValue;
    private final double xCoordinate;
    private final double yCoordinate;
    private final KeyFigure keyFigure;

    public KeyFigureBox(String dValue, double xCoordinate, double yCoordinate, KeyFigure keyFigure) {
        this.dValue = dValue;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.keyFigure = keyFigure;
    }

    @Override
    public String draw() {
        SVGElement link = new LinkToExcelDownload(keyFigure);

        double offsetX = 0;

        if (keyFigure == null) {
            offsetX = 19.50;
        } else if (keyFigure.getValue() >= 1000 && keyFigure.getValue() < 10000) {
            offsetX = 4.50;
        } else if (keyFigure.getValue() >= 100) {
            offsetX = 9.50;
        } else if (keyFigure.getValue() >= 10) {
            offsetX = 14.50;
        } else if (keyFigure.getValue() < 10) {
            offsetX = 19.50;
        }

        SVGElement label = new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(" + Double.toString(this.xCoordinate + offsetX) + " " + Double.toString(yCoordinate) + ")", link.draw());
        SvgElementCollection elementCollection = SvgElementCollection.of(getKeyFigureBox(), label);
        return elementCollection.draw();
    }

    private SVGElement getKeyFigureBox() {
        return () -> {
            StringBuilder sb = new StringBuilder();
            sb.append("<g class=\"a7a7d0c6-d834-4f16-aa8c-4d2cc48187d8\">"
                    + "<path class=\"e42c4fa2-60b7-493c-bbe3-5b7b96c79774\" d=\"")
                    .append(dValue).append("\" transform=\"translate(0 -0.02)\"/>"
                    + "<path class=\"e10c3a5a-f925-4bfe-b875-b1dab086c709\" d=\"")
                    .append(dValue).append("\" transform=\"translate(0 -0.02)\"/></g>)");
            return sb.toString();
        };
    }

}
