package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ReportingStatusmatrixEntryHighlight {

    private static final Map<Integer, List<Integer>> ENTRIES_TO_HIGHLIGHT = new HashMap<>();

    private ReportingStatusmatrixEntryHighlight() {
    }

    static {
        ENTRIES_TO_HIGHLIGHT.put(1, Arrays.asList(3, 4, 5, 6, 7));
        ENTRIES_TO_HIGHLIGHT.put(2, Arrays.asList(1, 2, 5, 6, 7));
        ENTRIES_TO_HIGHLIGHT.put(3, Arrays.asList(1, 2, 5, 6, 7));
        ENTRIES_TO_HIGHLIGHT.put(4, Arrays.asList(1, 2, 3, 4, 6, 7));
        ENTRIES_TO_HIGHLIGHT.put(5, Arrays.asList(1, 2, 3, 4, 5));
        ENTRIES_TO_HIGHLIGHT.put(6, Arrays.asList(1, 2, 3, 4, 5, 6));
    }

    protected static boolean isEntryHighlighted(int row, int column) {
        List<Integer> columns = ENTRIES_TO_HIGHLIGHT.get(row);
        return columns.contains(column);
    }

}
