package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model;

import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author sl
 */
public enum ProjectReportingProcessKeyFigure {

    G1(0), G2(1), G3(2),
    L1(3), L2(4), L3(5), L4(6), L5(7), L6(8), L7(9), L8(10), L9(11),
    L10(12), L11(13), L12(14), L13(15), L14(16), L15(17), L16(18),
    R1(19), R2(20),
    R3(21), R4(22), R5(23), R6(24), R7(25), R8(26), R9(27), R10(28), R11(29),
    R12(30), R13(31), R14(32), R15(33), R16(34), R17(35), R18(36),
    R19(37), R20(38), R21(39), R22(40), R23(41), R24(42), R25(43);

    private final int id;

    ProjectReportingProcessKeyFigure(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static Optional<ProjectReportingProcessKeyFigure> getById(int id) {
        return Stream.of(ProjectReportingProcessKeyFigure.values()).filter(keyFigure -> keyFigure.getId() == id).findAny();
    }

}
