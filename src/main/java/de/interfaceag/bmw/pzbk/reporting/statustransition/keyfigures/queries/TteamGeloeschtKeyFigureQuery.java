package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Date;

final class TteamGeloeschtKeyFigureQuery {

    private static final Logger LOG = LoggerFactory.getLogger(TteamGeloeschtKeyFigureQuery.class.getName());

    private TteamGeloeschtKeyFigureQuery() {
    }

    static TteamGeloeschtKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {
        Collection<Long> value = getAnforderungIdsForTteamGeloeschtEntry(filter, entityManager);
        return new TteamGeloeschtKeyFigure(value);
    }

    private static Collection<Long> getAnforderungIdsForTteamGeloeschtEntry(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT(a.id) ");
        queryPart.append(" FROM ReportingStatusTransition r ")
                .append(" INNER JOIN r.anforderung a ")
                .append(" WHERE r.tteamGeloescht.entry < :endDate ")
                .append(" AND r.tteamGeloescht.exit IS NULL ");
        queryPart.put("endDate", endDate);

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getAnforderungIdsForTteamGeloeschtEntry Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

}
