package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.AbstractSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DurchlaufzeitKeyFigureCollection extends AbstractSet<DurchlaufzeitKeyFigure> implements Serializable {

    private final Set<DurchlaufzeitKeyFigure> keyFigures;

    public DurchlaufzeitKeyFigureCollection() {
        this.keyFigures = new HashSet<>();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        DurchlaufzeitKeyFigureCollection that = (DurchlaufzeitKeyFigureCollection) object;

        return new EqualsBuilder()
                .appendSuper(super.equals(object))
                .append(keyFigures, that.keyFigures)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(keyFigures)
                .toHashCode();
    }

    @Override
    public boolean add(DurchlaufzeitKeyFigure keyFigure) {
        return keyFigures.add(keyFigure);
    }

    @Override
    public Iterator<DurchlaufzeitKeyFigure> iterator() {
        return keyFigures.iterator();
    }

    @Override
    public int size() {
        return keyFigures.size();
    }

    public DurchlaufzeitKeyFigure getByKeyType(KeyType keyType) {
        return keyFigures.stream().filter(keyFigure -> keyFigure.getKeyTypeId() == keyType.getId()).findAny().orElse(null);
    }

}
