package de.interfaceag.bmw.pzbk.reporting.svg;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class TooltipCollection extends AbstractList<Tooltip> {

    private final List<Tooltip> tooltips;

    private TooltipCollection() {
        this.tooltips = new ArrayList<>();
    }

    @Override
    public boolean add(Tooltip tooltip) {
        return tooltips.add(tooltip);
    }

    @Override
    public Iterator<Tooltip> iterator() {
        return tooltips.iterator();
    }

    @Override
    public int size() {
        return tooltips.size();
    }

    public String drawCollection() {
        List<SVGElement> svgElements = new ArrayList<>(tooltips);
        return SVGElementDrawer.drawElements(svgElements);
    }

    public static TooltipCollection createCollection() {
        return new TooltipCollection();
    }

    @Override
    public Tooltip get(int index) {
        return tooltips.get(index);
    }

}
