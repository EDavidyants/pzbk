package de.interfaceag.bmw.pzbk.reporting.statustransition.migration;

/**
 *
 * @author fn
 */
public enum AnforderungHistoryAction {

    CREATION,
    STATUS_CHANGE,
    ADD_TO_PROZESSBAUKASTEN,
    REMOVE_FROM_PROZESSBAUKASTEN,
    NEW_VERSION,
    RESTORE,
    OTHER;

}
