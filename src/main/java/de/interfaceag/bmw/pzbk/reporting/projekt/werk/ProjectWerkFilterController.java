package de.interfaceag.bmw.pzbk.reporting.projekt.werk;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface ProjectWerkFilterController {

    String filter();

    String reset();

}
