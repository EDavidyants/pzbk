package de.interfaceag.bmw.pzbk.reporting.validation;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Named
@Stateless
public class KeyFigureValidationService implements Serializable {

    @Inject
    private KeyFigureConstraintFactory constraintFactory;

    /**
     * Validate KeyFigureConstraints for given Collection of KeyFigures.
     *
     * @param keyFigureCollection Collection of KeyFigures.
     * @return Validation Results.
     */
    public Collection<KeyFigureValidationResult> validateKeyFigures(KeyFigureCollection keyFigureCollection) {

        KeyFigureConstraint constraint1 = constraintFactory.getConstraint1(keyFigureCollection);
        KeyFigureConstraint constraint2 = constraintFactory.getConstraint2(keyFigureCollection);
        KeyFigureConstraint constraint3 = constraintFactory.getConstraint3(keyFigureCollection);
        KeyFigureConstraint constraint4 = constraintFactory.getConstraint4(keyFigureCollection);

        Collection<KeyFigureValidationResult> results = new ArrayList<>();
        results.add(constraint1.validate());
        results.add(constraint2.validate());
        results.add(constraint3.validate());
        results.add(constraint4.validate());

        return results;

    }

}
