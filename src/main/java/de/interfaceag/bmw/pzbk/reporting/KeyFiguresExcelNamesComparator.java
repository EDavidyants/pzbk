package de.interfaceag.bmw.pzbk.reporting;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author evda
 */
public class KeyFiguresExcelNamesComparator implements Comparator<String>, Serializable {

    @Override
    public int compare(String sheetName1, String sheetName2) {
        int underscoreIndex1 = sheetName1.indexOf("_");
        int underscoreIndex2 = sheetName2.indexOf("_");

        if (underscoreIndex1 != -1 && underscoreIndex2 != -1) {
            String indexStr1 = sheetName1.substring(0, underscoreIndex1);
            String indexStr2 = sheetName2.substring(0, underscoreIndex2);

            if (indexStr1.matches("\\d+") && indexStr2.matches("\\d+")) {
                int keyIndex1 = Integer.parseInt(indexStr1);
                int keyIndex2 = Integer.parseInt(indexStr2);
                return keyIndex1 - keyIndex2;
            }

        }

        return sheetName1.compareTo(sheetName2);
    }

}
