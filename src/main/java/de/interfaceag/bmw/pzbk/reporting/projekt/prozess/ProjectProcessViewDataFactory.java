package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.SelectedDerivatFilterService;
import de.interfaceag.bmw.pzbk.reporting.project.pub.Public;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures.ProjectReportingKeyFigureService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot.ProjectProcessSnapshotReadService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.validation.ProjectReportingConstraintValidationService;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureValidationResult;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;

import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Named
@Stateless
public class ProjectProcessViewDataFactory implements Serializable {

    @Inject
    private DerivatStatusNames derivatStatusNames;

    @Inject
    private DerivatService derivatService;
    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private ProjectReportingConstraintValidationService constraintValidationService;
    @Inject
    private LocalizationService localizationService;

    @Inject
    private ProjectProcessSnapshotViewDataService snapshotViewDataService;
    @Inject
    private ProjectReportingKeyFigureService projectReportingKeyFigureService;
    @Inject
    private ProjectProcessSnapshotReadService snapshotReadService;
    @Inject
    private SelectedDerivatFilterService selectedDerivatFilterService;

    @Public
    @Produces
    public ProjectProcessViewData getPublicViewData() {

        UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();

        Optional<Derivat> selectedDerivat = selectedDerivatFilterService.getDerivatForUrlParameter(urlParameter);

        if (selectedDerivat.isPresent()) {
            Derivat derivat = selectedDerivat.get();
            final ProjectProcessFilter filter = buildFilterForSelectedDerivat(urlParameter, derivat);
            final ProjectReportingProcessKeyFigureDataCollection keyFigures = getPublicKeyFigures(urlParameter, derivat, filter);
            final Date date = keyFigures.getDate();
            final Collection<KeyFigureValidationResult> validationResults = Collections.emptyList();
            final String validationHeaderMessage = "";
            return buildViewDataForDerivat(filter, keyFigures, derivat, validationResults, validationHeaderMessage, date);
        } else {
            final ProjectProcessFilter filter = buildFilterWithoutDerivat(urlParameter);
            return buildEmptyViewData(filter);
        }
    }

    @Default
    @Produces
    public ProjectProcessViewData getViewData() {

        UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();

        Optional<Derivat> selectedDerivat = selectedDerivatFilterService.getDerivatForUrlParameter(urlParameter);

        if (selectedDerivat.isPresent()) {
            final Derivat derivat = selectedDerivat.get();
            final ProjectProcessFilter filter = buildFilterForSelectedDerivat(urlParameter, derivat);
            final ProjectReportingProcessKeyFigureDataCollection keyFigures = getKeyFigures(urlParameter, derivat, filter);
            final Date date = keyFigures.getDate();
            final Collection<KeyFigureValidationResult> validationResults = validateResults(keyFigures);
            final String validationHeaderMessage = getValidationHeaderMessage();
            return buildViewDataForDerivat(filter, keyFigures, derivat, validationResults, validationHeaderMessage, date);
        } else {
            final ProjectProcessFilter filter = buildFilterWithoutDerivat(urlParameter);
            return buildEmptyViewData(filter);
        }
    }

    private ProjectReportingProcessKeyFigureDataCollection getPublicKeyFigures(UrlParameter urlParameter, Derivat derivat, ProjectProcessFilter filter) {
        ProjectReportingProcessKeyFigureDataCollection keyFigures;
        if (isSnapshot(urlParameter)) {
            final Optional<ProjectReportingProcessKeyFigureDataCollection> keyFiguresForSnapshot = snapshotViewDataService.getKeyFiguresForSnapshot(urlParameter);
            keyFigures = keyFiguresForSnapshot.orElseGet(() -> projectReportingKeyFigureService.computeKeyFiguresForDerivat(derivat, filter));
        } else {
            final Optional<ProjectReportingProcessKeyFigureDataCollection> keyFiguresForLatestSnapshot = snapshotViewDataService.getKeyFiguresForLatestSnapshot(derivat);
            keyFigures = keyFiguresForLatestSnapshot.orElseGet(this::getEmptyKeyFigureDataCollection);
        }
        return keyFigures;
    }

    private ProjectReportingProcessKeyFigureDataCollection getEmptyKeyFigureDataCollection() {
        ProjectReportingProcessKeyFigureDataCollection keyFigures;
        keyFigures = new ProjectReportingProcessKeyFigureDataCollection() {
            @Override
            public Long getValueForKeyFigure(ProjectReportingProcessKeyFigure keyFigure) {
                return 0L;
            }

            @Override
            public Long getValueForKeyFigureId(Integer keyFigureId) {
                return 0L;
            }

            @Override
            public Optional<ProjectReportingProcessKeyFigureData> getDataForKeyFigure(ProjectReportingProcessKeyFigure keyFigure) {
                return Optional.empty();
            }

            @Override
            public Date getDate() {
                return new Date();
            }

            @Override
            public boolean isAmpelGreenForKeyFigureId(Integer keyFigureId) {
                return false;
            }
        };
        return keyFigures;
    }

    private ProjectReportingProcessKeyFigureDataCollection getKeyFigures(UrlParameter urlParameter, Derivat derivat, ProjectProcessFilter filter) {
        ProjectReportingProcessKeyFigureDataCollection keyFigures;
        if (isSnapshot(urlParameter)) {
            final Optional<ProjectReportingProcessKeyFigureDataCollection> keyFiguresForSnapshot = snapshotViewDataService.getKeyFiguresForSnapshot(urlParameter);
            keyFigures = keyFiguresForSnapshot.orElseGet(() -> projectReportingKeyFigureService.computeKeyFiguresForDerivat(derivat, filter));
        } else {
            keyFigures = projectReportingKeyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        }
        return keyFigures;
    }

    private static boolean isSnapshot(UrlParameter urlParameter) {
        Optional<String> snapshotParameter = urlParameter.getValue("snapshot");
        return snapshotParameter.isPresent();
    }

    private ProjectProcessViewData buildEmptyViewData(ProjectProcessFilter filter) {

        ProjectReportingProcessKeyFigureDataCollection emptyKeyFigureCollection = getEmptyKeyFigureCollection();

        return ProjectProcessViewData
                .withFilter(filter)
                .withKeyFigures(emptyKeyFigureCollection)
                .get();
    }

    private ProjectProcessViewData buildViewDataForDerivat(ProjectProcessFilter filter, ProjectReportingProcessKeyFigureDataCollection keyFigures, Derivat selectedDerivat, Collection<KeyFigureValidationResult> validationResults, String validationHeaderMessage, Date date) {

        return ProjectProcessViewData
                .withFilter(filter)
                .withKeyFigures(keyFigures)
                .withSelectedDerivat(selectedDerivat)
                .withDerivatStatusNames(derivatStatusNames)
                .withValidationResults(validationResults)
                .withValidationHeaderMessage(validationHeaderMessage)
                .withDate(date)
                .get();
    }

    private ProjectReportingProcessKeyFigureDataCollection getEmptyKeyFigureCollection() {
        return projectReportingKeyFigureService.getEmptyCollection();
    }

    private String getValidationHeaderMessage() {
        return localizationService.getValue("reporting_project_processoverview_kpi_validation_title");
    }

    private Collection<KeyFigureValidationResult> validateResults(ProjectReportingProcessKeyFigureDataCollection dataCollection) {
        return constraintValidationService.validateKeyFigures(dataCollection);
    }

    private ProjectProcessFilter buildFilterForSelectedDerivat(UrlParameter urlParameter, Derivat derivat) {
        Collection<Derivat> allDerivate = derivatService.getAllDerivate();
        final List<SnapshotFilter> allSnapshots = snapshotReadService.getSnapshotDatesForDerivat(derivat);
        List<SensorCoc> allSensorCocs = sensorCocService.getAllSortByTechnologie();
        boolean snapshot = isSnapshot(urlParameter);
        return new ProjectProcessFilter(urlParameter, allDerivate, allSnapshots, allSensorCocs, snapshot);
    }

    private ProjectProcessFilter buildFilterWithoutDerivat(UrlParameter urlParameter) {
        Collection<Derivat> allDerivate = derivatService.getAllDerivate();
        final List<SnapshotFilter> allSnapshots = Collections.emptyList();
        List<SensorCoc> allSensorCocs = sensorCocService.getAllSortByTechnologie();
        return new ProjectProcessFilter(urlParameter, allDerivate, allSnapshots, allSensorCocs);
    }

}
