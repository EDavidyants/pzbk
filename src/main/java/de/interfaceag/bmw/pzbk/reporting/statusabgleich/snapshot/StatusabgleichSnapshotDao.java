package de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author sl
 */
@Dependent
public class StatusabgleichSnapshotDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public List<SnapshotFilter> getAllDatesForDerivat(Derivat derivat) {
        if (derivat == null) {
            throw new IllegalArgumentException("derivat is null");
        }

        Query query = entityManager.createNamedQuery(StatusabgleichSnapshot.ALL_DATES_FOR_DERIVAT);
        query.setParameter("derivat", derivat);

        List<Object[]> result = query.getResultList();
        List<SnapshotFilter> snapshots = SnapshotFilterConverter.fromObjectArray(result);

        return snapshots;
    }

    public List<StatusabgleichSnapshot> getAllForDerivat(Derivat derivat) {
        if (derivat == null) {
            throw new IllegalArgumentException("derivat is null");
        }

        Query query = entityManager.createNamedQuery(StatusabgleichSnapshot.ALL_FOR_DERIVAT, StatusabgleichSnapshot.class);
        query.setParameter("derivat", derivat);

        return query.getResultList();
    }

    public Optional<StatusabgleichSnapshot> getById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("id is null");
        }

        StatusabgleichSnapshot result = entityManager.find(StatusabgleichSnapshot.class, id);

        return Optional.ofNullable(result);
    }

    public Optional<StatusabgleichSnapshot> getForDerivatAndDate(Derivat derivat, Date date) {
        if (derivat == null) {
            throw new IllegalArgumentException("derivat is null");
        }

        if (date == null) {
            throw new IllegalArgumentException("date is null");
        }

        Query query = entityManager.createNamedQuery(StatusabgleichSnapshot.FOR_DERIVAT_AND_DATE, StatusabgleichSnapshot.class);
        query.setParameter("derivat", derivat);
        query.setParameter("datum", date);

        List<StatusabgleichSnapshot> result = query.getResultList();

        if (result.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(result.get(0));
        }
    }

    public void save(StatusabgleichSnapshot snapshot) {
        if (snapshot.getId() != null) {
            entityManager.merge(snapshot);
        } else {
            entityManager.persist(snapshot);
        }
        entityManager.flush();
    }

}
