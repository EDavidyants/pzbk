package de.interfaceag.bmw.pzbk.reporting;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface ReportingNavbarController {

    void downloadExcelExport();

    void downloadDeveloperExport();

}
