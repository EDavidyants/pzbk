package de.interfaceag.bmw.pzbk.reporting.processoverview;

import de.interfaceag.bmw.pzbk.entities.InfoText;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterService;
import de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht.Anforderungsuebersicht;
import de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht.AnforderungsuebersichtService;
import de.interfaceag.bmw.pzbk.reporting.excel.ReportingExcelDataDto;
import de.interfaceag.bmw.pzbk.reporting.excel.ReportingToExcelService;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.StatusTransitionKeyFigureService;
import de.interfaceag.bmw.pzbk.reporting.svg.TooltipMap;
import de.interfaceag.bmw.pzbk.services.InfoTextService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class ProcessOverviewViewFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ProcessOverviewViewFacade.class.getName());

    @Inject
    private Session session;
    @Inject
    private LocalizationService localizationService;
    @Inject
    private ReportingFilterService reportingFilterService;
    @Inject
    private ReportingToExcelService reportingToExcelService;
    @Inject
    private InfoTextService infoTextService;
    @Inject
    private AnforderungsuebersichtService anforderungsuebersichtService;
    @Inject
    private StatusTransitionKeyFigureService statusTransitionKeyFigureService;

    public ProcessOverviewViewData<ProcessOverviewDetailTooltip> getDetailProcessOverviewViewData(UrlParameter urlParameter) {
        LOG.debug("Generate DetailProcessOverviewViewData");

        ReportingFilter filter = reportingFilterService.getReportingFilter(urlParameter, Page.REPORTING_PROCESS_OVERVIEW_DETAIL);
        updateFilterForRestrictedRoles(filter, session);

        final ProcessOverviewKeyFigures statusTransitionKeyFigures = statusTransitionKeyFigureService.getDetailProcessOverviewKeyFigures(filter);

        TooltipMap<ProcessOverviewDetailTooltip> processOverviewDetailTooltipMap = getProcessOverviewDetailTooltipMap();

        String validationFailedHeaderMessage = localizationService.getValue("reporting_validation_message_header");

        return new ProcessOverviewViewData<>(statusTransitionKeyFigures, filter, processOverviewDetailTooltipMap, validationFailedHeaderMessage);
    }

    private static void updateFilterForRestrictedRoles(ReportingFilter filter, Session session) {
        if (isTteamLeiterOrVertreter(session)) {
            updateFilterForTteamLeiterOrVertreter(filter);
        } else if (isSensorCocLeiterOrVertreter(session)) {
            updateFilterForSensorCocLeiterOrVertreter(filter);
        } else if (isEcoc(session)) {
            updateFilterForEcoc(filter);
        }
    }

    private static void updateFilterForEcoc(ReportingFilter filter) {
        final IdSearchFilter modulSeTeamFilter = filter.getModulSeTeamFilter();
        final boolean modulSeTeamFilterActive = modulSeTeamFilter.isActive();
        if (!modulSeTeamFilterActive) {
            final List<SearchFilterObject> all = modulSeTeamFilter.getAll();
            modulSeTeamFilter.setSelectedValues(new HashSet<>(all));
        }
    }


    private static void updateFilterForTteamLeiterOrVertreter(ReportingFilter filter) {
        final IdSearchFilter tteamFilter = filter.getTteamFilter();
        final boolean tteamFilterIsActive = tteamFilter.isActive();
        if (!tteamFilterIsActive) {
            final List<SearchFilterObject> all = tteamFilter.getAll();
            tteamFilter.setSelectedValues(new HashSet<>(all));
        }
    }

    private static void updateFilterForSensorCocLeiterOrVertreter(ReportingFilter filter) {
        final IdSearchFilter sensorCocFilter = filter.getSensorCocFilter();
        final boolean sensorCocFilterActive = sensorCocFilter.isActive();
        if (!sensorCocFilterActive) {
            final List<SearchFilterObject> all = sensorCocFilter.getAll();
            sensorCocFilter.setSelectedValues(new HashSet<>(all));
        }
    }

    private static boolean isEcoc(Session session) {
        return session.hasRole(Rolle.E_COC);
    }

    private static boolean isTteamLeiterOrVertreter(Session session) {
        return session.hasRole(Rolle.T_TEAMLEITER) || session.hasRole(Rolle.TTEAM_VERTRETER);
    }

    private static boolean isSensorCocLeiterOrVertreter(Session session) {
        return session.hasRole(Rolle.SENSORCOCLEITER) || session.hasRole(Rolle.SCL_VERTRETER);
    }

    public ProcessOverviewViewData<ProcessOverviewSimpleTooltip> getSimpleProcessOverviewViewData(UrlParameter urlParameter) {
        LOG.debug("Generate SimpleProcessOverviewViewData");

        ReportingFilter filter = reportingFilterService.getReportingFilter(urlParameter, Page.REPORTING_PROCESS_OVERVIEW_SIMPLE);
        updateFilterForRestrictedRoles(filter, session);


        final ProcessOverviewKeyFigures statusTransitionKeyFigures = statusTransitionKeyFigureService.getSimplProcessOverviewKeyFigures(filter);

        TooltipMap<ProcessOverviewSimpleTooltip> processOverviewSimpleTooltipMap = getProcessOverviewSimpleTooltipMap();

        return new ProcessOverviewViewData<>(statusTransitionKeyFigures, filter, processOverviewSimpleTooltipMap);
    }

    private TooltipMap<ProcessOverviewSimpleTooltip> getProcessOverviewSimpleTooltipMap() {
        Collection<String> keys = ProcessOverviewSimpleTooltip.getNameList();
        Collection<InfoText> infoTextsByKeys = infoTextService.getInfoTextsByKeys(keys);

        TooltipMap<ProcessOverviewSimpleTooltip> result = new TooltipMap<>();

        Iterator<InfoText> iterator = infoTextsByKeys.iterator();
        while (iterator.hasNext()) {
            InfoText next = iterator.next();
            result.addTooltip(ProcessOverviewSimpleTooltip.getByName(next.getFeldName()), next.getFeldBeschreibungLang());
        }

        return result;
    }

    private TooltipMap<ProcessOverviewDetailTooltip> getProcessOverviewDetailTooltipMap() {
        Collection<String> keys = ProcessOverviewDetailTooltip.getNameList();
        Collection<InfoText> infoTextsByKeys = infoTextService.getInfoTextsByKeys(keys);

        TooltipMap<ProcessOverviewDetailTooltip> result = new TooltipMap<>();

        Iterator<InfoText> iterator = infoTextsByKeys.iterator();
        while (iterator.hasNext()) {
            InfoText next = iterator.next();
            result.addTooltip(ProcessOverviewDetailTooltip.getByName(next.getFeldName()), next.getFeldBeschreibungLang());
        }

        return result;
    }

    public void downloadExcelExport(ProcessOverviewViewData viewData) {
        reportingToExcelService.downloadExcelExport(viewData);
    }

    public void downloadDevelopmentExport(ProcessOverviewViewData viewData) {
        reportingToExcelService.downloadDevelopmentExport(viewData);
    }

    public void downloadExcelExportForKeyFigure(KeyType keyType, ProcessOverviewViewData viewData) {
        reportingToExcelService.downloadExcelExportForKeyFigure(keyType, viewData);
    }

    public void downloadExcelDataForKeyFigure(KeyType keyType, Collection<ReportingExcelDataDto> excelData) {
        reportingToExcelService.downloadExcelDataForKeyFigure(keyType, excelData);
    }

    public Collection<ReportingExcelDataDto> getReportingDataForKeyFigure(KeyType keyType, ProcessOverviewViewData viewData) {
        return reportingToExcelService.getReportingDataForKeyFigure(keyType, viewData);
    }

    public Collection<ReportingExcelDataDto> getReportingDataForKeyFigure(KeyType keyType, ProcessOverviewViewData viewData, ReportingFilter reportingFilter) {
        return reportingToExcelService.getReportingDataForKeyFigure(keyType, viewData, reportingFilter);
    }

    public List<Anforderungsuebersicht> getAnforderungsuebersichtData(Collection<ReportingExcelDataDto> excelData) {
        return anforderungsuebersichtService.buildFromExcelData(excelData);
    }

}
