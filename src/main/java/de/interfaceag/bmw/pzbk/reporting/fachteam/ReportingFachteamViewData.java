package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingUtils;
import de.interfaceag.bmw.pzbk.reporting.ReportingPeriodGetter;
import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ReportingFachteamViewData implements ReportingPeriodGetter, Serializable {

    private final FachteamKeyFigures fachteamKeyFigures;

    private final ReportingFilter filter;

    private final String reportingPeriod;

    public ReportingFachteamViewData(FachteamKeyFigures fachteamKeyFigures,
            ReportingFilter filter) {
        this.fachteamKeyFigures = fachteamKeyFigures;
        this.filter = filter;
        this.reportingPeriod = ReportingUtils.getReportingPeriod(filter);
    }

    public FachteamKeyFigures getFachteamKeyFigures() {
        return fachteamKeyFigures;
    }

    public ReportingFilter getFilter() {
        return filter;
    }

    @Override
    public String getReportingPeriod() {
        return reportingPeriod;
    }

}
