package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.shared.math.Mean;

import java.util.Collection;
import java.util.stream.Stream;

final class FachteamKeyFigureMeanDurchlaufzeitCalculator {

    private FachteamKeyFigureMeanDurchlaufzeitCalculator() {
    }

    static int computeMeanDurchlaufzeit(Collection<FachteamKeyFigure> keyFigures) {
        final Stream<Integer> durchlaufzeiten = keyFigures.stream().map(FachteamKeyFigure::getDurchlaufzeit);
        return Mean.computeMean(durchlaufzeiten);
    }

}
