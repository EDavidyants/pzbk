package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author lomu
 */
public class TteamRunningTimeKeyFigure extends AbstractKeyFigure {

    public TteamRunningTimeKeyFigure(int count, Collection<Long> ids, Long runTime) {
        super(count, IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.DURCHLAUFZEIT_TTEAM, runTime);
    }

}
