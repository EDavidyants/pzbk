package de.interfaceag.bmw.pzbk.reporting.langlaufer;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.config.ReportingConfigService;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTuple;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungDurchlaufzeitKeyFigure;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

@Stateless
public class DurchlaufzeitService implements Serializable {

    @Inject
    private DurchlaufzeitDao durchlaufzeitDao;
    @Inject
    private ReportingConfigService reportingConfigService;

    public FachteamDurchlaufzeitKeyFigure getFachteamDurchlaufzeitKeyFigure(ReportingFilter filter) {
        return durchlaufzeitDao.getFachteamDurchlaufzeitKeyFigure(filter);
    }

    public VereinbarungDurchlaufzeitKeyFigure getVereinbarungDurchlaufzeitKeyFigure(ReportingFilter filter) {
        return durchlaufzeitDao.getVereinbarungDurchlaufzeitKeyFigure(filter);
    }

    public TteamDurchlaufzeitKeyFigure getTteamDurchlaufzeitKeyFigure(ReportingFilter filter) {
        return durchlaufzeitDao.getTteamDurchlaufzeitKeyFigure(filter);
    }

    public Collection<DurchlaufzeitDto> getFachteamDurchlaufzeiten(ReportingFilter filter) {
        final Map<IdTypeTuple, Long> fachteamIdTypeTupleDurchlaufzeitMap = durchlaufzeitDao.getFachteamIdTypeTupleDurchlaufzeitMap(filter);

        int threshold = reportingConfigService.getLanglauferFachteamThreshold();

        return convertMapToDtoCollection(fachteamIdTypeTupleDurchlaufzeitMap, threshold);
    }

    public Collection<DurchlaufzeitDto> getTteamDurchlaufzeiten(ReportingFilter filter) {
        final Map<IdTypeTuple, Long> tteamIdTypeTupleDurchlaufzeitMap = durchlaufzeitDao.getTteamIdTypeTupleDurchlaufzeitMap(filter);

        int threshold = reportingConfigService.getLanglauferTteamThreshold();

        return convertMapToDtoCollection(tteamIdTypeTupleDurchlaufzeitMap, threshold);
    }

    public Collection<DurchlaufzeitDto> getVereinbarungDurchlaufzeiten(ReportingFilter filter) {
        final Map<IdTypeTuple, Long> vereinbarungIdTypeTupleDurchlaufzeitMap = durchlaufzeitDao.getVereinbarungIdTypeTupleDurchlaufzeitMap(filter);

        int threshold = reportingConfigService.getLanglauferTteamThreshold();

        return convertMapToDtoCollection(vereinbarungIdTypeTupleDurchlaufzeitMap, threshold);
    }

    private static Collection<DurchlaufzeitDto> convertMapToDtoCollection(Map<IdTypeTuple, Long> vereinbarungIdTypeTupleDurchlaufzeitMap, int threshold) {
        return vereinbarungIdTypeTupleDurchlaufzeitMap.entrySet().stream()
                .map(entry -> new DurchlaufzeitDto(entry.getKey().getX(), entry.getKey().getY(), entry.getValue(), threshold))
                .collect(Collectors.toList());
    }
}
