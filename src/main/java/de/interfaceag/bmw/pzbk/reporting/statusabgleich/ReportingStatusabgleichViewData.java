package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import static de.interfaceag.bmw.pzbk.reporting.statusabgleich.ReportingStatusabgleichColumnToStatusConverter.convertColumnToZakStatus;
import static de.interfaceag.bmw.pzbk.reporting.statusabgleich.ReportingStatusabgleichRowToStatusConverter.convertToDerivatAnforderungModulStatus;
import static de.interfaceag.bmw.pzbk.reporting.statusabgleich.ReportingStatusmatrixEntryHighlight.isEntryHighlighted;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ReportingStatusabgleichViewData implements Serializable,
        ReportingStatusmatrix, ReportingStandLabel {

    private final StatusabgleichKennzahlen kennzahlen;
    private final ReportingStatusabgleichFilter filter;
    private final Derivat derivat;
    private final Date date;
    private final DerivatStatusNames derivatStatusNames;

    private ReportingStatusabgleichViewData(StatusabgleichKennzahlen kennzahlen,
                                            ReportingStatusabgleichFilter filter, Derivat derivat, Date date,
                                            DerivatStatusNames derivatStatusNames) {
        this.kennzahlen = kennzahlen;
        this.filter = filter;
        this.derivat = derivat;
        this.date = date;
        this.derivatStatusNames = derivatStatusNames;
    }

    public static Builder withKennzahlen(StatusabgleichKennzahlen kennzahlen) {
        return new Builder().withKennzahlen(kennzahlen);
    }

    public static Builder withFilter(ReportingStatusabgleichFilter filter) {
        return new Builder().withFilter(filter);
    }

    public static ReportingStatusabgleichViewData empty(ReportingStatusabgleichFilter filter) {
        return new ReportingStatusabgleichViewData(StatusabgleichKennzahlen.empty(), filter, null, new Date(), null);
    }

    public ReportingStatusabgleichFilter getFilter() {
        return filter;
    }

    public Optional<Derivat> getDerivat() {
        return Optional.ofNullable(derivat);
    }


    @Override
    public String getMatrixValue(int row, int column) {
        return getMatrixValueAsLong(row, column).toString();
    }

    @Override
    public boolean isValueZero(int row, int column) {
        return getMatrixValueAsLong(row, column).equals(0L);
    }

    @Override
    public String getRowSum(int row) {
        Collection<DerivatAnforderungModulStatus> statusCollection = convertToDerivatAnforderungModulStatus(row);
        return kennzahlen.getDerivatAnforderungModulStatusValue(statusCollection).toString();
    }

    @Override
    public String getColumnSum(int column) {
        Collection<ZakStatus> statusCollection = convertColumnToZakStatus(column);
        return kennzahlen.getZakStatusValue(statusCollection).toString();
    }

    @Override
    public String getDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return simpleDateFormat.format(date);
    }

    private Long getMatrixValueAsLong(int row, int column) {
        Collection<DerivatAnforderungModulStatus> derivatAnforderungModulStatus = convertToDerivatAnforderungModulStatus(row);
        Collection<ZakStatus> zakStatus = convertColumnToZakStatus(column);
        return kennzahlen.getValue(derivatAnforderungModulStatus, zakStatus);
    }

    @Override
    public boolean isHighlighted(int row, int column) {
        Long value = getMatrixValueAsLong(row, column);
        return value > 0 && isEntryHighlighted(row, column);
    }

    public DerivatStatusNames getDerivatStatusNames() {
        return derivatStatusNames;
    }

    public static final class Builder {

        private StatusabgleichKennzahlen kennzahlen;
        private ReportingStatusabgleichFilter filter;
        private Derivat derivat;
        private Date date = new Date();
        private DerivatStatusNames derivatStatusNames;

        private Builder() {

        }

        public Builder withKennzahlen(StatusabgleichKennzahlen kennzahlen) {
            this.kennzahlen = kennzahlen;
            return this;
        }

        public Builder withFilter(ReportingStatusabgleichFilter filter) {
            this.filter = filter;
            return this;
        }

        public Builder withDerivat(Derivat derivat) {
            this.derivat = derivat;
            return this;
        }

        public Builder withDate(Date date) {
            this.date = date;
            return this;
        }

        public Builder withDerivatStatusNames(DerivatStatusNames derivatStatusNames) {
            this.derivatStatusNames = derivatStatusNames;
            return this;
        }

        public ReportingStatusabgleichViewData get() {
            return new ReportingStatusabgleichViewData(kennzahlen, filter, derivat, date, derivatStatusNames);
        }

    }

}
