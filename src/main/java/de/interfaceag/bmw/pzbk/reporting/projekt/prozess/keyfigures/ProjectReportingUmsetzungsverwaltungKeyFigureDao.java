package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author sl
 */
@Named
@Stateless
public class ProjectReportingUmsetzungsverwaltungKeyFigureDao {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingUmsetzungsverwaltungKeyFigureDao.class);

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public List<Object[]> getQueryDataForUmsetzungsverwaltung(Derivat derivat, KovAPhase kovAPhase, ProjectProcessFilter filter) {
        QueryPart queryPart = getQueryForUmsetzungsverwaltungDerivatAndPhase(derivat, kovAPhase, filter);
        Query query = queryPart.buildNativeQuery(entityManager);
        LOG.debug("Excecute query: {}", query.toString());
        LOG.debug("Query parameters: {}", query.getParameters());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    private static QueryPart getQueryForUmsetzungsverwaltungDerivatAndPhase(Derivat derivat, KovAPhase kovAPhase, ProjectProcessFilter filter) {
        QueryPart queryPart = new QueryPartDTO("SELECT ub.status, COUNT(ub.id) "
                + "FROM umsetzungsbestaetigung ub "
                + "INNER JOIN kov_a_phase_im_derivat kovad ON ub.kovaimderivat_id = kovad.id "
                + "INNER JOIN anforderung a ON ub.anforderung_id = a.id "
                + "INNER JOIN sensorcoc coc ON a.sensorcoc_sensorcocid = coc.sensorcocid "
                + "WHERE kovad.derivat_id = ?1 "
                + "AND kovad.kovAPhase = ?2 "
                + "AND (ub.anforderung_id, ub.modul_id) IN (")
                .append(getQueryForVereinbarungStatus())
                .append(") ");

        if (!filter.isSnapshot() && filter.getTechnologieFilter().isActive()) {
            List<String> filters = filter.getTechnologieFilter().getSelectedValuestringsAsList();
            queryPart.append("AND coc.ortung IN (").append(getQueryParametersAsStringFromList(filters)).append(") ");
        }

        queryPart.append("GROUP BY ub.status");
        queryPart.put("1", derivat.getId());
        queryPart.put("2", kovAPhase.getId());
        queryPart.put("3", derivat.getId());
        return queryPart;
    }

    private static String getQueryForVereinbarungStatus() {
        String anforderungModulQuery = "SELECT zad.anforderung_id, dam.modul_id "
                + "FROM zakuebertragung zak "
                + "INNER JOIN derivatanforderungmodul dam ON dam.id = zak.derivatanforderungmodul_id "
                + "INNER JOIN zuordnunganforderungderivat zad ON zad.id = dam.zuordnunganforderungderivat_id "
                + "WHERE zad.derivat_id = ?3 ";
        return anforderungModulQuery;
    }

    private static String getQueryParametersAsStringFromList(List<String> parameters) {
        if (parameters != null) {
            return parameters.stream()
                    .filter(param -> !param.isEmpty())
                    .map(param -> "'" + param + "'")
                    .collect(Collectors.joining(", "));
        }
        return "'abc'";
    }

}
