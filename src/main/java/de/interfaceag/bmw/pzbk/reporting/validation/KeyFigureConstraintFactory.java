package de.interfaceag.bmw.pzbk.reporting.validation;

import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Factory for keyFigureConstraints. The numeration is based on the PDF provided
 * by BMW. As PZBK KeyFigures are not implemented yet these are not considered
 * in the validation.
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Named
@Stateless
public class KeyFigureConstraintFactory implements Serializable {

    @Inject
    private LocalizationService localizationService;

    private Map<String, String> getValidationMessageMap() {
        Map<String, String> validationMessageMap = new HashMap<>();
        validationMessageMap.put("reporting_validation_part_1", localizationService.getValue("reporting_validation_part_1"));
        validationMessageMap.put("reporting_validation_kpi_missing", localizationService.getValue("reporting_validation_kpi_missing"));
        validationMessageMap.put("reporting_validation_successful", localizationService.getValue("reporting_validation_successful"));
        validationMessageMap.put("reporting_validation_failed", localizationService.getValue("reporting_validation_failed"));
        return validationMessageMap;
    }

    // do not remove pzbk keyFigures!
    public KeyFigureConstraint getConstraint1(KeyFigureCollection keyFigureCollection) {
        Map<String, String> validationMessageMap = getValidationMessageMap();
        return KeyFigureCollectionEqualsConstraint.newBuilder()
                .withIdentifier("1")
                .withValidationMessageSuccessful(validationMessageMap.get("reporting_validation_successful"))
                .withValidationMessageFailed(validationMessageMap.get("reporting_validation_failed"))
                .withValidationMessageKpiMissing(validationMessageMap.get("reporting_validation_kpi_missing"))
                .withValidationMessagePrefix(validationMessageMap.get("reporting_validation_part_1"))
                .addToLeftSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_MELDUNG_GEMELDET))
                .addToLeftSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_NEW_VERSION_ANFORDERUNG))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_CURRENT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_GELOESCHT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_MELDUNG_EXISTING_ANFORDERUNG))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_PZBK))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.TTEAM_CURRENT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.TTEAM_GELOESCHT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.TTEAM_PZBK))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.VEREINBARUNG_CURRENT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.VEREINBARUNG_GELOESCHT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.VEREINBARUNG_PZBK))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.FREIGEGEBEN_CURRENT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.FREIGEGEBEN_GELOESCHT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.FREIGEGEBEN_PZBK))
                .build();
    }

    public KeyFigureConstraint getConstraint2(KeyFigureCollection keyFigureCollection) {
        Map<String, String> validationMessageMap = getValidationMessageMap();
        return KeyFigureCollectionEqualsConstraint.newBuilder()
                .withIdentifier("2")
                .withValidationMessageSuccessful(validationMessageMap.get("reporting_validation_successful"))
                .withValidationMessageFailed(validationMessageMap.get("reporting_validation_failed"))
                .withValidationMessageKpiMissing(validationMessageMap.get("reporting_validation_kpi_missing"))
                .withValidationMessagePrefix(validationMessageMap.get("reporting_validation_part_1"))
                .addToLeftSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_MELDUNG_GEMELDET))
                .addToLeftSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_NEW_VERSION_ANFORDERUNG))
                .addToLeftSide(keyFigureCollection.getByKeyType(KeyType.TTEAM_UNSTIMMIG))
                .addToLeftSide(keyFigureCollection.getByKeyType(KeyType.VEREINBARUNG_UNSTIMMIG))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_CURRENT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_GELOESCHT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_ANFORDERUNG_ABGESTIMMT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_PZBK))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_MELDUNG_EXISTING_ANFORDERUNG))
                .build();
    }

    public KeyFigureConstraint getConstraint3(KeyFigureCollection keyFigureCollection) {
        Map<String, String> validationMessageMap = getValidationMessageMap();
        return KeyFigureCollectionEqualsConstraint.newBuilder()
                .withIdentifier("3")
                .withValidationMessageSuccessful(validationMessageMap.get("reporting_validation_successful"))
                .withValidationMessageFailed(validationMessageMap.get("reporting_validation_failed"))
                .withValidationMessageKpiMissing(validationMessageMap.get("reporting_validation_kpi_missing"))
                .withValidationMessagePrefix(validationMessageMap.get("reporting_validation_part_1"))
                .addToLeftSide(keyFigureCollection.getByKeyType(KeyType.FACHTEAM_ANFORDERUNG_ABGESTIMMT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.TTEAM_CURRENT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.TTEAM_GELOESCHT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.TTEAM_PZBK))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.TTEAM_PLAUSIBILISIERT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.TTEAM_UNSTIMMIG))
                .build();
    }

    public KeyFigureConstraint getConstraint4(KeyFigureCollection keyFigureCollection) {
        Map<String, String> validationMessageMap = getValidationMessageMap();
        return KeyFigureCollectionEqualsConstraint.newBuilder()
                .withIdentifier("4")
                .withValidationMessageSuccessful(validationMessageMap.get("reporting_validation_successful"))
                .withValidationMessageFailed(validationMessageMap.get("reporting_validation_failed"))
                .withValidationMessageKpiMissing(validationMessageMap.get("reporting_validation_kpi_missing"))
                .withValidationMessagePrefix(validationMessageMap.get("reporting_validation_part_1"))
                .addToLeftSide(keyFigureCollection.getByKeyType(KeyType.TTEAM_PLAUSIBILISIERT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.VEREINBARUNG_CURRENT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.VEREINBARUNG_GELOESCHT))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.VEREINBARUNG_PZBK))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.VEREINBARUNG_FREIGEGEBEN))
                .addToRightSide(keyFigureCollection.getByKeyType(KeyType.VEREINBARUNG_UNSTIMMIG))
                .build();
    }

}
