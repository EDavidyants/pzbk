package de.interfaceag.bmw.pzbk.reporting.keyfigures;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public enum KeyType {

    FACHTEAM_MELDUNG_GEMELDET(1, "gemeldete Meldungen"),
    FACHTEAM_NEW_VERSION_ANFORDERUNG(2, "neue Version angelegt"),
    FACHTEAM_ENTRY_SUM(3, "Eingang Fachteam gesamt"),
    FACHTEAM_ANFORDERUNG_UNSTIMMIG(4, "unstimmige Anforderungen"),
    FACHTEAM_CURRENT(5, "Anzahl Objekte im Fachteam"),
    FACHTEAM_OUTPUT(6, "Anzahl OUTPUT im Fachteam"),
    FACHTEAM_ANFORDERUNG_ABGESTIMMT(7, "abgestimmte Anforderungen"),
    FACHTEAM_GELOESCHT(8, "Anzahl gelöschter Objekte im Fachteam"),
    FACHTEAM_PZBK(9, "Fachteam PZBK"),
    TTEAM_CURRENT(10, "Anzahl Anforderungen im T-Team"),
    TTEAM_OUTPUT(11, "Anzahl OUTPUT im T-Team"),
    TTEAM_PLAUSIBILISIERT(12, "Anzahl plausibilisierter Anforderungen im T-Team"),
    TTEAM_UNSTIMMIG(13, "Anzahl unstimmiger Anforderungen im T-Team"),
    TTEAM_GELOESCHT(15, "Anzahl gelöschter Anforderungen im T-Team"),
    TTEAM_PZBK(16, "T-Team PZBK"),
    VEREINBARUNG_CURRENT(14, "Anzahl Anforderungen in der Vereinbarung"),
    VEREINBARUNG_OUTPUT(17, "OUTPUT Vereinbarung"),
    VEREINBARUNG_UNSTIMMIG(18, "Anzahl unstimmiger Anforderungen in der Vereinbarung"),
    VEREINBARUNG_FREIGEGEBEN(19, "Anzahl freigegebener Anforderungen in der Vereinbarung"),
    VEREINBARUNG_GELOESCHT(20, "Anzahl gelöschter Anforderungen in der Vereinbarung"),
    VEREINBARUNG_PZBK(21, "Vereinbarung PZBK"),
    FREIGEGEBEN_GELOESCHT(22, "Anzahl gelöschter Anforderungen aus dem Status freigegeben"),
    FREIGEGEBEN_PZBK(23, "Freigegeben PZBK"),
    FREIGEGEBEN_CURRENT(24, "Anzahl freigegebener Anforderungen"),
    DURCHLAUFZEIT_FACHTEAM(25, "Durchlaufzeit Fachteam"),
    DURCHLAUFZEIT_TTEAM(26, "Durchlaufzeit T-Team"),
    DURCHLAUFZEIT_VEREINBARUNG(27, "Durchlaufzeit Vereinbarung"),
    DURCHLAUFZEIT_GESAMT(28, "Durchlaufzeit gesamt"),
    FACHTEAM_MELDUNG_EXISTING_ANFORDERUNG(29, "Meldung bestehender Anforderung zugeordnet");

    private final int id;

    private final String bezeichnung;

    KeyType(int id, String bezeichnung) {
        this.bezeichnung = bezeichnung;
        this.id = id;
    }

    @Override
    public String toString() {
        return bezeichnung;
    }

    public int getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public static KeyType getKeyTypeById(int id) {
        for (KeyType keyType : KeyType.values()) {
            if (keyType.id == id) {
                return keyType;
            }
        }
        throw new IllegalArgumentException("Invalid KeyType Id: " + id);
    }

    public boolean isRelevantForDrilldownDialog() {
        switch (this) {
            case FACHTEAM_CURRENT:
            case TTEAM_CURRENT:
            case VEREINBARUNG_CURRENT:
            case FREIGEGEBEN_CURRENT:

            case FACHTEAM_MELDUNG_EXISTING_ANFORDERUNG:

            case FACHTEAM_GELOESCHT:
            case TTEAM_GELOESCHT:
            case VEREINBARUNG_GELOESCHT:
            case FREIGEGEBEN_GELOESCHT:

            case FACHTEAM_PZBK:
            case TTEAM_PZBK:
            case VEREINBARUNG_PZBK:
            case FREIGEGEBEN_PZBK:

                return Boolean.TRUE;
            default:
                return Boolean.FALSE;
        }
    }

}
