package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class FachteamOutputKeyFigure extends AbstractKeyFigure {

    public FachteamOutputKeyFigure(Collection<Long> anforderungIds, Collection<Long> meldungIds, Long runTime) {
        super(anforderungIds.size() + meldungIds.size(),
                IdTypeTupleFactory.getIdTypeTuplesForMeldungAndAnforderung(meldungIds, anforderungIds),
                KeyType.FACHTEAM_OUTPUT, runTime);
    }

    public static FachteamOutputKeyFigure buildFromKeyFigures(@NotNull KeyFigure fachteamAnforderungAbgestimmtKeyFigure,
            @NotNull KeyFigure geloeschteObjekteKeyFigure, @NotNull KeyFigure fachteamProzessbaukastenKeyFigure, @NotNull KeyFigure fachteamMeldungExistingAnforderungKeyFigure) throws InvalidDataException {

        if (fachteamAnforderungAbgestimmtKeyFigure == null || geloeschteObjekteKeyFigure == null
                || fachteamProzessbaukastenKeyFigure == null || fachteamMeldungExistingAnforderungKeyFigure == null) {
            throw new InvalidDataException("At least one paramter is null!");
        }

        List<Long> anforderungIds = new ArrayList<>(fachteamAnforderungAbgestimmtKeyFigure.getAnforderungIds());
        List<Long> meldungIds = new ArrayList<>(geloeschteObjekteKeyFigure.getMeldungIds());
        anforderungIds.addAll(geloeschteObjekteKeyFigure.getAnforderungIds());
        anforderungIds.addAll(fachteamProzessbaukastenKeyFigure.getAnforderungIds());
        meldungIds.addAll(fachteamMeldungExistingAnforderungKeyFigure.getMeldungIds());

        return new FachteamOutputKeyFigure(anforderungIds, meldungIds, 0L);
    }
}
