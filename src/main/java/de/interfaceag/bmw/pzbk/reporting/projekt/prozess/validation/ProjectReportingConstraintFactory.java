package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.validation;

import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureConstraint;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 *
 * @author sl
 */
@Named
@Stateless
public class ProjectReportingConstraintFactory implements Serializable {

    @Inject
    private LocalizationService localizationService;

    private String getValidationFailedMessage() {
        return localizationService.getValue("reporting_validation_failed");
    }

    public KeyFigureConstraint getProjectReportingConstraintG3eqL1L2(ProjectReportingProcessKeyFigureDataCollection keyFigureData) {
        String validationFailedMessage = getValidationFailedMessage();

        ProjectReportingCollectionEqualsConstraint.Builder builder = ProjectReportingCollectionEqualsConstraint.build()
                .withValidationMessageFailed(validationFailedMessage);

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.G3).ifPresent(data -> builder.addToLeftSide(data));

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L1).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L2).ifPresent(data -> builder.addToRightSide(data));

        return builder.build();
    }

    public KeyFigureConstraint getProjectReportingConstraintG3eqL4L5L6L7L8L9(ProjectReportingProcessKeyFigureDataCollection keyFigureData) {
        String validationFailedMessage = getValidationFailedMessage();

        ProjectReportingCollectionEqualsConstraint.Builder builder = ProjectReportingCollectionEqualsConstraint.build()
                .withValidationMessageFailed(validationFailedMessage);

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.G3).ifPresent(data -> builder.addToLeftSide(data));

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L4).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L5).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L6).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L7).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L8).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L9).ifPresent(data -> builder.addToRightSide(data));

        return builder.build();
    }

    public KeyFigureConstraint getProjectReportingConstraintL1eqL4L5L6L7(ProjectReportingProcessKeyFigureDataCollection keyFigureData) {
        String validationFailedMessage = getValidationFailedMessage();

        ProjectReportingCollectionEqualsConstraint.Builder builder = ProjectReportingCollectionEqualsConstraint.build()
                .withValidationMessageFailed(validationFailedMessage);

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L1).ifPresent(data -> builder.addToLeftSide(data));

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L4).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L5).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L6).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L7).ifPresent(data -> builder.addToRightSide(data));

        return builder.build();
    }

    public KeyFigureConstraint getProjectReportingConstraintL2eqL8L9(ProjectReportingProcessKeyFigureDataCollection keyFigureData) {
        String validationFailedMessage = getValidationFailedMessage();

        ProjectReportingCollectionEqualsConstraint.Builder builder = ProjectReportingCollectionEqualsConstraint.build()
                .withValidationMessageFailed(validationFailedMessage);

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L2).ifPresent(data -> builder.addToLeftSide(data));

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L8).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L9).ifPresent(data -> builder.addToRightSide(data));

        return builder.build();
    }

    public KeyFigureConstraint getProjectReportingConstraintL10eqL12L13L14L15(ProjectReportingProcessKeyFigureDataCollection keyFigureData) {
        String validationFailedMessage = getValidationFailedMessage();

        ProjectReportingCollectionEqualsConstraint.Builder builder = ProjectReportingCollectionEqualsConstraint.build()
                .withValidationMessageFailed(validationFailedMessage);

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L10).ifPresent(data -> builder.addToLeftSide(data));

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L12).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L13).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L14).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L15).ifPresent(data -> builder.addToRightSide(data));

        return builder.build();
    }

    public KeyFigureConstraint getProjectReportingConstraintR2G3eqR3R4(ProjectReportingProcessKeyFigureDataCollection keyFigureData) {
        String validationFailedMessage = getValidationFailedMessage();

        ProjectReportingCollectionEqualsConstraint.Builder builder = ProjectReportingCollectionEqualsConstraint.build()
                .withValidationMessageFailed(validationFailedMessage);

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R2).ifPresent(data -> builder.addToLeftSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.G3).ifPresent(data -> builder.addToLeftSide(data));

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R3).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R4).ifPresent(data -> builder.addToRightSide(data));

        return builder.build();
    }

    public KeyFigureConstraint getProjectReportingConstraintR3eqR6R7R8R9(ProjectReportingProcessKeyFigureDataCollection keyFigureData) {
        String validationFailedMessage = getValidationFailedMessage();

        ProjectReportingCollectionEqualsConstraint.Builder builder = ProjectReportingCollectionEqualsConstraint.build()
                .withValidationMessageFailed(validationFailedMessage);

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R3).ifPresent(data -> builder.addToLeftSide(data));

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R6).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R7).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R8).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R9).ifPresent(data -> builder.addToRightSide(data));

        return builder.build();
    }

    public KeyFigureConstraint getProjectReportingConstraintR4eqR10R11(ProjectReportingProcessKeyFigureDataCollection keyFigureData) {
        String validationFailedMessage = getValidationFailedMessage();

        ProjectReportingCollectionEqualsConstraint.Builder builder = ProjectReportingCollectionEqualsConstraint.build()
                .withValidationMessageFailed(validationFailedMessage);

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R4).ifPresent(data -> builder.addToLeftSide(data));

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R10).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R11).ifPresent(data -> builder.addToRightSide(data));

        return builder.build();
    }

    public KeyFigureConstraint getProjectReportingConstraintR12eqR14R15R16R17(ProjectReportingProcessKeyFigureDataCollection keyFigureData) {
        String validationFailedMessage = getValidationFailedMessage();

        ProjectReportingCollectionEqualsConstraint.Builder builder = ProjectReportingCollectionEqualsConstraint.build()
                .withValidationMessageFailed(validationFailedMessage);

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R12).ifPresent(data -> builder.addToLeftSide(data));

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R14).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R15).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R16).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R17).ifPresent(data -> builder.addToRightSide(data));

        return builder.build();
    }

    public KeyFigureConstraint getProjectReportingConstraintR19eqR21R22R23R24(ProjectReportingProcessKeyFigureDataCollection keyFigureData) {
        String validationFailedMessage = getValidationFailedMessage();

        ProjectReportingCollectionEqualsConstraint.Builder builder = ProjectReportingCollectionEqualsConstraint.build()
                .withValidationMessageFailed(validationFailedMessage);

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R19).ifPresent(data -> builder.addToLeftSide(data));

        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R21).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R22).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R23).ifPresent(data -> builder.addToRightSide(data));
        keyFigureData.getDataForKeyFigure(ProjectReportingProcessKeyFigure.R24).ifPresent(data -> builder.addToRightSide(data));

        return builder.build();
    }

}
