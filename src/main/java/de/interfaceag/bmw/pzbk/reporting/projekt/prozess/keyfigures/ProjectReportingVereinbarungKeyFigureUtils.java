package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author sl
 */
public final class ProjectReportingVereinbarungKeyFigureUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingVereinbarungKeyFigureUtils.class);

    private ProjectReportingVereinbarungKeyFigureUtils() {
    }

    protected static Map<Integer, ProjectReportingProcessKeyFigureData> computeSumAndRatioKeyFiguresForVereinbarungVKBG(Map<Integer, ProjectReportingProcessKeyFigureData> result, Double thresholdForGreenAmpel) {
        ProjectReportingProcessKeyFigureData ratioKeyFigureForVereinbarungVKBG = computeRatioKeyFigureForVereinbarungVKBG(result, thresholdForGreenAmpel);
        result.put(ratioKeyFigureForVereinbarungVKBG.getKeyFigure().getId(), ratioKeyFigureForVereinbarungVKBG);

        ProjectReportingProcessKeyFigureData positiveEntryFigureForVereinbarungVKBG = computePositiveEntryFigureForVereinbarungVKBG(result);
        result.put(positiveEntryFigureForVereinbarungVKBG.getKeyFigure().getId(), positiveEntryFigureForVereinbarungVKBG);

        ProjectReportingProcessKeyFigureData negativeEntryFigureForVereinbarungVKBG = computeNegativeEntryFigureForVereinbarungVKBG(result);
        result.put(negativeEntryFigureForVereinbarungVKBG.getKeyFigure().getId(), negativeEntryFigureForVereinbarungVKBG);

        return result;
    }

    protected static Map<Integer, ProjectReportingProcessKeyFigureData> computeSumAndRatioKeyFiguresForVereinbarungZV(Map<Integer, ProjectReportingProcessKeyFigureData> result, Double thresholdForGreenAmpel) {
        ProjectReportingProcessKeyFigureData ratioKeyFigureForVereinbarungZV = computeRatioKeyFigureForVereinbarungZV(result, thresholdForGreenAmpel);
        result.put(ratioKeyFigureForVereinbarungZV.getKeyFigure().getId(), ratioKeyFigureForVereinbarungZV);

        ProjectReportingProcessKeyFigureData positiveEntryFigureForVereinbarungZV = computePositiveEntryFigureForVereinbarungZV(result);
        result.put(positiveEntryFigureForVereinbarungZV.getKeyFigure().getId(), positiveEntryFigureForVereinbarungZV);

        ProjectReportingProcessKeyFigureData negativeEntryFigureForVereinbarungZV = computeNegativeEntryFigureForVereinbarungZV(result);
        result.put(negativeEntryFigureForVereinbarungZV.getKeyFigure().getId(), negativeEntryFigureForVereinbarungZV);

        return result;
    }

    private static ProjectReportingProcessKeyFigureData computePositiveEntryFigureForVereinbarungVKBG(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures) {

        Collection<ProjectReportingProcessKeyFigureData> summands = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures,
                ProjectReportingProcessKeyFigure.L4, ProjectReportingProcessKeyFigure.L5, ProjectReportingProcessKeyFigure.L6, ProjectReportingProcessKeyFigure.L7);
        LOG.debug("PositiveEntryFigureForVereinbarungVKBG summands {}", summands);

        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeSumKeyFigure(ProjectReportingProcessKeyFigure.L1, summands);
        LOG.debug("PositiveEntryFigureForVereinbarungVKBG result {}", result);

        return result;
    }

    private static ProjectReportingProcessKeyFigureData computePositiveEntryFigureForVereinbarungZV(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures) {

        Collection<ProjectReportingProcessKeyFigureData> summands = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures,
                ProjectReportingProcessKeyFigure.R6,
                ProjectReportingProcessKeyFigure.R7,
                ProjectReportingProcessKeyFigure.R8,
                ProjectReportingProcessKeyFigure.R9);
        LOG.debug("PositiveEntryFigureForVereinbarungZV summands {}", summands);

        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeSumKeyFigure(ProjectReportingProcessKeyFigure.R3, summands);
        LOG.debug("PositiveEntryFigureForVereinbarungZV result {}", result);

        return result;
    }

    private static ProjectReportingProcessKeyFigureData computeNegativeEntryFigureForVereinbarungVKBG(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures) {

        Collection<ProjectReportingProcessKeyFigureData> summands = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures,
                ProjectReportingProcessKeyFigure.L8,
                ProjectReportingProcessKeyFigure.L9);
        LOG.debug("NegativeEntryFigureForVereinbarungVKBG summands {}", summands);

        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeSumKeyFigure(ProjectReportingProcessKeyFigure.L2, summands);
        LOG.debug("NegativeEntryFigureForVereinbarungVKBG result {}", result);

        return result;
    }

    private static ProjectReportingProcessKeyFigureData computeNegativeEntryFigureForVereinbarungZV(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures) {

        Collection<ProjectReportingProcessKeyFigureData> summands = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures,
                ProjectReportingProcessKeyFigure.R10,
                ProjectReportingProcessKeyFigure.R11);
        LOG.debug("NegativeEntryFigureForVereinbarungZV summands {}", summands);

        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeSumKeyFigure(ProjectReportingProcessKeyFigure.R4, summands);
        LOG.debug("NegativeEntryFigureForVereinbarungZV result {}", result);

        return result;
    }

    private static ProjectReportingProcessKeyFigureData computeRatioKeyFigureForVereinbarungVKBG(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures, Double thresholdForGreenAmpel) {
        Collection<ProjectReportingProcessKeyFigureData> numerator = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures, ProjectReportingProcessKeyFigure.L4);
        LOG.debug("RatioKeyFigureForVereinbarungVKBG numerator {}", numerator);

        Collection<ProjectReportingProcessKeyFigureData> denominator = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures,
                ProjectReportingProcessKeyFigure.L4,
                ProjectReportingProcessKeyFigure.L5,
                ProjectReportingProcessKeyFigure.L6,
                ProjectReportingProcessKeyFigure.L7);
        LOG.debug("RatioKeyFigureForVereinbarungVKBG denominator {}", numerator);

        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeRatioKeyFigure(ProjectReportingProcessKeyFigure.L3, numerator, denominator, thresholdForGreenAmpel);
        LOG.debug("RatioKeyFigureForVereinbarungVKBG result {}", result);

        return result;
    }

    private static ProjectReportingProcessKeyFigureData computeRatioKeyFigureForVereinbarungZV(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures, Double thresholdForGreenAmpel) {
        Collection<ProjectReportingProcessKeyFigureData> numerator = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures, ProjectReportingProcessKeyFigure.R6);
        LOG.debug("RatioKeyFigureForVereinbarungZV numerator {}", numerator);

        Collection<ProjectReportingProcessKeyFigureData> denominator = SumKeyFigureUtils.getKeyFiguresDataFromMap(keyFigures,
                ProjectReportingProcessKeyFigure.R6,
                ProjectReportingProcessKeyFigure.R7,
                ProjectReportingProcessKeyFigure.R8,
                ProjectReportingProcessKeyFigure.R9);
        LOG.debug("RatioKeyFigureForVereinbarungZV denominator {}", numerator);

        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeRatioKeyFigure(ProjectReportingProcessKeyFigure.R5, numerator, denominator, thresholdForGreenAmpel);
        LOG.debug("RatioKeyFigureForVereinbarungZV result {}", result);

        return result;
    }

}
