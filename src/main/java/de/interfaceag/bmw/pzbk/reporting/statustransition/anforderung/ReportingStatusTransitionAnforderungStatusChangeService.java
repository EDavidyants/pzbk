package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

@Dependent
public class ReportingStatusTransitionAnforderungStatusChangeService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(ReportingStatusTransitionAnforderungStatusChangeService.class);

    @Inject
    private ReportingStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Inject
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Inject
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Inject
    private ReportingStatusTransitionAnforderungEntyDateService entyDateService;

    public ReportingStatusTransition changeAnforderungStatus(Anforderung anforderung, Status currentStatus, Status newStatus, Date date) {
        LOG.debug("Write ReportingStatusTransition for Anforderung {} status change from {} to {}", anforderung, currentStatus, newStatus);

        final Optional<ReportingStatusTransition> latestStatusTransitionForAnforderung = statusTransitionDatabaseAdapter.getLatestForAnforderung(anforderung);

        final ReportingStatusTransition statusTransition;
        if (latestStatusTransitionForAnforderung.isPresent()) {
            LOG.debug("Found latest status transition and update or create new instance if necessary");
            statusTransition = updateReportingStatusTransition(latestStatusTransitionForAnforderung.get(), currentStatus, newStatus, date);
        } else {
            LOG.debug("Found no status transition --> create new instance");
            Date entryDate = entyDateService.getEntryDateForAnforderung(anforderung);
            statusTransition = createNewReportingStatusTransitionForAnforderungAndUpdateStatus(anforderung, currentStatus, newStatus, entryDate, date);
        }
        statusTransitionDatabaseAdapter.save(statusTransition);
        return statusTransition;
    }

    private ReportingStatusTransition updateReportingStatusTransition(ReportingStatusTransition reportingStatusTransition, Status currentStatus, Status newStatus, Date date) {
        updateExitForStatus(reportingStatusTransition, currentStatus, date);
        return updateEntryForStatus(reportingStatusTransition, currentStatus, newStatus, date);
    }

    public ReportingStatusTransition updateEntryForStatus(ReportingStatusTransition statusTransition, Status currentStatus, Status newStatus, Date date) {
        LOG.debug("Update {} with entry for new status {}", statusTransition, newStatus);
        switch (newStatus) {
            case A_UNSTIMMIG:
                updateReportingStatusTransitionForNewStatusUnstimmig(statusTransition, currentStatus, date);
                break;
            case A_FTABGESTIMMT:
                return updateEntryForStatusAbgestimmtOrCreateNewRowIfCurrentStatusIsUnstimmig(statusTransition, currentStatus, date);
            case A_GELOESCHT:
                updateReportingStatusTransitionForNewStatusGeloescht(statusTransition, currentStatus, date);
                break;
            case A_PLAUSIB:
                updateEntryForStatusPlausibilisiert(statusTransition, date);
                break;
            case A_FREIGEGEBEN:
                updateEntryForStatusFreigegeben(statusTransition, date);
                break;
            case A_KEINE_WEITERVERFOLG:
                updateEntryForStatusKeineWeiterverfolgung(statusTransition, date);
                break;
            case A_INARBEIT:
            default:
                break;
        }
        return statusTransition;
    }

    private ReportingStatusTransition updateEntryForStatusAbgestimmtOrCreateNewRowIfCurrentStatusIsUnstimmig(ReportingStatusTransition statusTransition, Status currentStatus, Date date) {
        if (currentStatus.equals(Status.A_UNSTIMMIG)) {
            final ReportingStatusTransition newStatusTransition = createNewReportingStatusTransitionRowForStatusTransition(statusTransition);
            return updateEntryForStatusAbgestimmt(newStatusTransition, date);
        } else {
            return updateEntryForStatusAbgestimmt(statusTransition, date);
        }
    }

    private ReportingStatusTransition updateEntryForStatusAbgestimmt(ReportingStatusTransition statusTransition, Date date) {
        final EntryExitTimeStamp abgestimmt = statusTransition.getAbgestimmt();
        EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(abgestimmt, date);
        statusTransition.setAbgestimmt(timeStamp);
        return statusTransition;
    }

    private void updateEntryForStatusPlausibilisiert(ReportingStatusTransition statusTransition, Date date) {
        final EntryExitTimeStamp plausibilisiert = statusTransition.getPlausibilisiert();
        EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(plausibilisiert, date);
        statusTransition.setPlausibilisiert(timeStamp);
    }

    private void updateEntryForStatusFreigegeben(ReportingStatusTransition statusTransition, Date date) {
        final EntryExitTimeStamp freigegeben = statusTransition.getFreigegeben();
        EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(freigegeben, date);
        statusTransition.setFreigegeben(timeStamp);
    }

    private void updateEntryForStatusKeineWeiterverfolgung(ReportingStatusTransition statusTransition, Date date) {
        final EntryExitTimeStamp freigegeben = statusTransition.getKeineWeiterverfolgung();
        EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(freigegeben, date);
        statusTransition.setKeineWeiterverfolgung(timeStamp);
    }

    private void updateReportingStatusTransitionForNewStatusGeloescht(ReportingStatusTransition reportingStatusTransition, Status currentStatus, Date date) {
        EntryExitTimeStamp timeStamp;
        switch (currentStatus) {
            case A_INARBEIT:
            case A_UNSTIMMIG:
                timeStamp = reportingStatusTransition.getFachteamGeloescht();
                timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(timeStamp, date);
                reportingStatusTransition.setFachteamGeloescht(timeStamp);
                break;
            case A_FTABGESTIMMT:
                timeStamp = reportingStatusTransition.getTteamGeloescht();
                timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(timeStamp, date);
                reportingStatusTransition.setTteamGeloescht(timeStamp);
                break;
            case A_PLAUSIB:
                timeStamp = reportingStatusTransition.getVereinbarungGeloescht();
                timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(timeStamp, date);
                reportingStatusTransition.setVereinbarungGeloescht(timeStamp);
                break;
            case A_FREIGEGEBEN:
            case A_KEINE_WEITERVERFOLG:
                timeStamp = reportingStatusTransition.getFreigegebenGeloescht();
                timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(timeStamp, date);
                reportingStatusTransition.setFreigegebenGeloescht(timeStamp);
                break;
            default:
                break;
        }
    }

    private void updateReportingStatusTransitionForNewStatusUnstimmig(ReportingStatusTransition reportingStatusTransition, Status currentStatus, Date date) {
        EntryExitTimeStamp timeStamp;
        switch (currentStatus) {
            case A_INARBEIT:
                timeStamp = reportingStatusTransition.getFachteamUnstimmig();
                timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(timeStamp, date);
                reportingStatusTransition.setFachteamUnstimmig(timeStamp);
                break;
            case A_FTABGESTIMMT:
                timeStamp = reportingStatusTransition.getTteamUnstimmig();
                timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(timeStamp, date);
                reportingStatusTransition.setTteamUnstimmig(timeStamp);
                break;
            case A_PLAUSIB:
                timeStamp = reportingStatusTransition.getVereinbarungUnstimmig();
                timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(timeStamp, date);
                reportingStatusTransition.setVereinbarungUnstimmig(timeStamp);
                break;
            default:
                break;
        }
    }

    public void updateExitForStatus(ReportingStatusTransition reportingStatusTransition, Status currentStatus, Date date) {
        switch (currentStatus) {
            case A_INARBEIT:
                updateExitForStatusInArbeit(reportingStatusTransition, date);
                break;
            case A_UNSTIMMIG:
                updateExitForStatusUnstimmig(reportingStatusTransition, date);
                break;
            case A_FTABGESTIMMT:
                updateExitForStatusAbgestimmt(reportingStatusTransition, date);
                break;
            case A_PLAUSIB:
                updateExitForStatusPlausibilisiert(reportingStatusTransition, date);
                break;
            case A_FREIGEGEBEN:
                updateExitForStatusFreigegeben(reportingStatusTransition, date);
                break;
            case A_KEINE_WEITERVERFOLG:
                updateExitForStatusKeineWeiterverfolgung(reportingStatusTransition, date);
                break;
            case A_GELOESCHT:
            default:
                break;
        }
    }

    private void updateExitForStatusKeineWeiterverfolgung(ReportingStatusTransition reportingStatusTransition, Date date) {
        EntryExitTimeStamp freigegeben = reportingStatusTransition.getKeineWeiterverfolgung();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(freigegeben, date);
        reportingStatusTransition.setKeineWeiterverfolgung(timeStamp);
    }

    private void updateExitForStatusFreigegeben(ReportingStatusTransition reportingStatusTransition, Date date) {
        EntryExitTimeStamp freigegeben = reportingStatusTransition.getFreigegeben();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(freigegeben, date);
        reportingStatusTransition.setFreigegeben(timeStamp);
    }

    private void updateExitForStatusPlausibilisiert(ReportingStatusTransition reportingStatusTransition, Date date) {
        EntryExitTimeStamp plausibilisiert = reportingStatusTransition.getPlausibilisiert();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(plausibilisiert, date);
        reportingStatusTransition.setPlausibilisiert(timeStamp);
    }

    private void updateExitForStatusAbgestimmt(ReportingStatusTransition reportingStatusTransition, Date date) {
        EntryExitTimeStamp abgestimmt = reportingStatusTransition.getAbgestimmt();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(abgestimmt, date);
        reportingStatusTransition.setAbgestimmt(timeStamp);
    }

    private void updateExitForStatusInArbeit(ReportingStatusTransition reportingStatusTransition, Date date) {
        EntryExitTimeStamp inArbeit = reportingStatusTransition.getInArbeit();
        EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(inArbeit, date);
        reportingStatusTransition.setInArbeit(timeStamp);
    }

    private void updateExitForStatusUnstimmig(ReportingStatusTransition reportingStatusTransition, Date date) {
        updateExitForStatusUnstimmigFromFachteam(reportingStatusTransition, date);
        updateExitForStatusUnstimmigFromTteam(reportingStatusTransition, date);
        updateExitForStatusUnstimmigFromVerereinbarung(reportingStatusTransition, date);
    }

    private void updateExitForStatusUnstimmigFromFachteam(ReportingStatusTransition reportingStatusTransition, Date date) {
        final EntryExitTimeStamp fachteamUnstimmig = reportingStatusTransition.getFachteamUnstimmig();
        if (fachteamUnstimmig != null && fachteamUnstimmig.isActive()) {
            final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(fachteamUnstimmig, date);
            reportingStatusTransition.setFachteamUnstimmig(timeStamp);
        }
    }

    private void updateExitForStatusUnstimmigFromTteam(ReportingStatusTransition reportingStatusTransition, Date date) {
        final EntryExitTimeStamp tteamUnstimmig = reportingStatusTransition.getTteamUnstimmig();
        if (tteamUnstimmig != null && tteamUnstimmig.isActive()) {
            final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(tteamUnstimmig, date);
            reportingStatusTransition.setTteamUnstimmig(timeStamp);
        }
    }

    private void updateExitForStatusUnstimmigFromVerereinbarung(ReportingStatusTransition reportingStatusTransition, Date date) {
        final EntryExitTimeStamp vereinbarungUnstimmig = reportingStatusTransition.getVereinbarungUnstimmig();
        if (vereinbarungUnstimmig != null && vereinbarungUnstimmig.isActive()) {
            final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(vereinbarungUnstimmig, date);
            reportingStatusTransition.setVereinbarungUnstimmig(timeStamp);
        }
    }

    private ReportingStatusTransition createNewReportingStatusTransitionRowForStatusTransition(ReportingStatusTransition statusTransition) {
        final Anforderung anforderung = statusTransition.getAnforderung();
        final Date entryDate = statusTransition.getEntryDate();
        return createNewReportingStatusTransitionRow(anforderung, entryDate);
    }

    private ReportingStatusTransition createNewReportingStatusTransitionForAnforderungAndUpdateStatus(Anforderung anforderung, Status currentStatus, Status newStatus, Date entryDate, Date date) {
        LOG.debug("Create new ReportingStatusTransition instance for anforderung {}", anforderung);
        ReportingStatusTransition statusTransition = createNewReportingStatusTransitionRow(anforderung, entryDate);
        return updateReportingStatusTransition(statusTransition, currentStatus, newStatus, date);
    }

    private ReportingStatusTransition createNewReportingStatusTransitionRow(Anforderung anforderung, Date entryDate) {
        return reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(anforderung, entryDate);
    }

}
