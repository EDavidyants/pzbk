package de.interfaceag.bmw.pzbk.reporting.keyfigures;

public class VereinbarungDurchlaufzeitKeyFigure extends AbstractDurchlaufzeitKeyFigure {

    public VereinbarungDurchlaufzeitKeyFigure(int value) {
        super(value, KeyType.DURCHLAUFZEIT_VEREINBARUNG);
    }
}
