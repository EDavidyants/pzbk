package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Objects;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractDurchlaufzeitKeyFigure implements DurchlaufzeitKeyFigure {

    private final int value;
    private final KeyType keyType;

    AbstractDurchlaufzeitKeyFigure(int value, KeyType keyType) {
        this.value = value;
        this.keyType = keyType;
    }

    @Override
    public String toString() {
        return keyType + ": {" +
                "value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        AbstractDurchlaufzeitKeyFigure that = (AbstractDurchlaufzeitKeyFigure) object;
        return value == that.value &&
                keyType == that.keyType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, keyType);
    }

    @Override
    public KeyType getKeyType() {
        return keyType;
    }

    @Override
    public int getKeyTypeId() {
        return keyType.getId();
    }

    @Override
    public int getValue() {
        return value;
    }
}
