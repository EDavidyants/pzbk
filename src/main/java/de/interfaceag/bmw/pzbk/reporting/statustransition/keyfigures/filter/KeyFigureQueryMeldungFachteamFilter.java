package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;

import java.util.Collection;

public final class KeyFigureQueryMeldungFachteamFilter {

    private KeyFigureQueryMeldungFachteamFilter() {

    }

    public static void append(ReportingFilter filter, QueryPart queryPart) {
        final IdSearchFilter sensorCocFilter = filter.getSensorCocFilter();

        if (sensorCocFilter.isActive() && sensorCocFilter.getSelectedIdsAsSet() != null && !sensorCocFilter.getSelectedIdsAsSet().isEmpty()) {
            final Collection<Long> sensorCocIds = sensorCocFilter.getSelectedIdsAsSet();

            queryPart.append(" AND m.sensorCoc.sensorCocId IN :sensorCocIds ");
            queryPart.put("sensorCocIds", sensorCocIds);
        }
    }

}
