package de.interfaceag.bmw.pzbk.reporting;

import java.io.Serializable;

/**
 *
 * @author evda
 */
public interface ReportingPeriodGetter extends Serializable {

    String getReportingPeriod();

}
