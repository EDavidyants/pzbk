package de.interfaceag.bmw.pzbk.reporting.project.pub.process;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.reporting.project.pub.FilterPage;
import de.interfaceag.bmw.pzbk.reporting.project.pub.Public;
import de.interfaceag.bmw.pzbk.reporting.project.pub.ReportingPublicFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessKeyFigureAccessController;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessViewData;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ViewScoped
@Named
public class ProjektReportingPublicProcessController implements Serializable {

    @Inject
    @Public
    private ProjectProcessViewData viewData;

    @Inject
    @FilterPage(Page.REPORTING_PROJECT_PUBLIC_PROCESS)
    private ReportingPublicFilter filter;

    public String getDerivat() {
        Optional<Derivat> derivat = getViewData().getSelectedDerivat();
        if (derivat.isPresent()) {
            return derivat.get().getName();
        } else {
            return "";
        }
    }

    private ProjectProcessViewData getViewData() {
        return viewData;
    }

    public ReportingPublicFilter getFilter() {
        return filter;
    }

    public ProjectProcessKeyFigureAccessController getKeyFigureAccessController() {
        return viewData;
    }

    public String getDateLabel() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        final Date date = getViewData().getDate();
        return simpleDateFormat.format(date);
    }

}
