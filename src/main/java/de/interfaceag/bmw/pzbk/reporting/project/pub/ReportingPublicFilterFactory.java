package de.interfaceag.bmw.pzbk.reporting.project.pub;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import de.interfaceag.bmw.pzbk.reporting.SelectedDerivatFilterService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot.ProjectProcessSnapshotReadService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@Named
@Dependent
public class ReportingPublicFilterFactory implements Serializable {

    @Inject
    private ProjectProcessSnapshotReadService snapshotReadService;
    @Inject
    private SelectedDerivatFilterService selectedDerivatFilterService;

    public ReportingPublicFilter getPublicProcessViewFilter(UrlParameter urlParameter) {
        Page page = Page.REPORTING_PROJECT_PUBLIC_PROCESS;
        return getPublicProcessViewFilterForPage(page, urlParameter);
    }

    public ReportingPublicFilter getPublicStatusabgleichViewFilter(UrlParameter urlParameter) {
        Page page = Page.REPORTING_PROJECT_PUBLIC_STATUSABGLEICH;
        return getPublicProcessViewFilterForPage(page, urlParameter);
    }

    public ReportingPublicFilter getPublicDashboardViewFilter(UrlParameter urlParameter) {
        Page page = Page.REPORTING_PROJECT_PUBLIC_DASHBOARD;
        return getPublicProcessViewFilterForPageWithoutSnapshots(page, urlParameter);
    }

    private ReportingPublicFilter getPublicProcessViewFilterForPageWithoutSnapshots(Page page, UrlParameter urlParameter) {
        return getPublicProcessViewFilterForNoSelectedDerivat(page, urlParameter);
    }

    private ReportingPublicFilter getPublicProcessViewFilterForPage(Page page, UrlParameter urlParameter) {
        Optional<Derivat> selectedDerivat = selectedDerivatFilterService.getDerivatForUrlParameter(urlParameter);
        if (selectedDerivat.isPresent()) {
            return getPublicProcessViewFilterForDerivat(page, urlParameter, selectedDerivat.get());
        } else {
            return getPublicProcessViewFilterForNoSelectedDerivat(page, urlParameter);
        }
    }

    private ReportingPublicFilter getPublicProcessViewFilterForNoSelectedDerivat(Page page, UrlParameter urlParameter) {
        return getNewFilter(page, urlParameter, Collections.emptyList());
    }

    private ReportingPublicFilter getPublicProcessViewFilterForDerivat(Page page, UrlParameter urlParameter, Derivat derivat) {
        Collection<SnapshotFilter> snapshots = snapshotReadService.getSnapshotDatesForDerivat(derivat);
        return getNewFilter(page, urlParameter, snapshots);
    }

    private ReportingPublicFilter getNewFilter(Page page, UrlParameter urlParameter, Collection<SnapshotFilter> snapshots) {
        return new ReportingPublicFilter(urlParameter, snapshots, page);
    }

}
