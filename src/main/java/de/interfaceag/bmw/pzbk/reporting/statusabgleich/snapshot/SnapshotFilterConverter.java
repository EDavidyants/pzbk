package de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot;

import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author sl
 */
public final class SnapshotFilterConverter {

    private SnapshotFilterConverter() {
    }

    public static List<SnapshotFilter> fromObjectArray(List<Object[]> queryResults) {
        List<SnapshotFilter> result = new ArrayList<>();
        Iterator<Object[]> iterator = queryResults.iterator();

        while (iterator.hasNext()) {
            Object[] next = iterator.next();
            Long id = (Long) next[0];
            Date date = (Date) next[1];
            SnapshotFilter filter = new StatusabgleichSnapshotFilter(id, date);
            result.add(filter);
        }

        return result;
    }

}
