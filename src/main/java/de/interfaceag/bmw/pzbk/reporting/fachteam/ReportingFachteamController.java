package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterController;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterViewPermission;
import de.interfaceag.bmw.pzbk.reporting.ReportingNavbarController;
import de.interfaceag.bmw.pzbk.reporting.ReportingPermissionController;
import de.interfaceag.bmw.pzbk.reporting.dashboard.ReportingDashboardViewPermission;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.utils.DateUtils;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ViewScoped
@Named
public class ReportingFachteamController implements Serializable, ReportingFilterController,
        ReportingNavbarController, ReportingPermissionController<ReportingDashboardViewPermission> {

    private static final Logger LOG = LoggerFactory.getLogger(ReportingFachteamController.class.getName());

    @Inject
    private Session session;

    @Inject
    private ReportingFachteamViewData viewData;

    @Inject
    private ReportingDashboardViewPermission viewPermission;

    @Inject
    private LocalizationService localizationService;

    private ReportingFilterViewPermission filterViewPermission;

    @PostConstruct
    public void init() {
        LOG.debug("Begin Reporting Fachteam init");
        session.setLocationForView();
        filterViewPermission = new ReportingFilterViewPermission(session.getUserPermissions().getRoles());
        LOG.debug("End Reporting Fachteam init");
    }

    @Override
    public String filter() {
        return viewData.getFilter().getUrl();
    }

    @Override
    public String reset() {
        return viewData.getFilter().getResetUrl();
    }

    @Override
    public String toggleTooltips() {
        getFilter().getHideTooltipFilter().toggle();
        return filter();
    }

    @Override
    public ReportingFilter getFilter() {
        return getViewData().getFilter();
    }

    public ReportingFachteamViewData getViewData() {
        return viewData;
    }

    @Override
    public ReportingDashboardViewPermission getViewPermission() {
        return viewPermission;
    }

    @Override
    public void downloadExcelExport() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ReportingFilterViewPermission getFilterViewPermission() {
        return filterViewPermission;
    }

    @Override
    public void downloadDeveloperExport() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Date getMinDate() {
        return DateUtils.getMinDate();
    }

    @Override
    public Date getMaxDate() {
        return DateUtils.getMaxDate();
    }

}
