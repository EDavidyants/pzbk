package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Embeddable
public class EntryExitTimeStamp implements Serializable {

    @Temporal(TemporalType.TIMESTAMP)
    private Date entry;

    @Temporal(TemporalType.TIMESTAMP)
    private Date exit;

    /**
     * Default constructor only for jpa. Use static method getBuilder instead!
     */
    public EntryExitTimeStamp() {
    }

    static EntryExitTimeStampBuilder getBuilder() {
        return new EntryExitTimeStampBuilder();
    }

    EntryExitTimeStamp(Date entry, Date exit) {
        this.entry = entry;
        this.exit = exit;
    }

    @Override
    public String toString() {
        return "EntryExitTimeStamp{"
                + "entry=" + entry
                + ", exit=" + exit
                + '}';
    }

    public void entry() {
        setEntry(new Date());
    }

    public void removeEntry() {
        setEntry(null);
    }

    public void exit() {
        setExit(new Date());
    }

    public void removeExit() {
        setExit(null);
    }

    public boolean isActive() {
        return exit == null;
    }

    public Date getEntry() {
        return entry;
    }

    public void setEntry(Date entry) {
        this.entry = entry;
    }

    public Date getExit() {
        return exit;
    }

    public void setExit(Date exit) {
        this.exit = exit;
    }

    public void clear() {
        removeExit();
        removeEntry();
    }

    public Long getEntryExitDifference() {
        if (Objects.nonNull(entry) && Objects.nonNull(exit)) {
            return Math.abs(exit.getTime() - entry.getTime());
        } else if (Objects.nonNull(entry)) {
            final Date now = new Date();
            return Math.abs(now.getTime() - entry.getTime());
        } else {
            return 0L;
        }
    }

}
