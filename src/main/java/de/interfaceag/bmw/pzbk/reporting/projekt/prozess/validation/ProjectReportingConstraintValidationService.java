package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.validation;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureConstraint;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Named
@Stateless
public class ProjectReportingConstraintValidationService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingConstraintValidationService.class);

    @Inject
    private ProjectReportingConstraintFactory constraintFactory;

    public Collection<KeyFigureValidationResult> validateKeyFigures(ProjectReportingProcessKeyFigureDataCollection dataCollection) {
        Collection<KeyFigureConstraint> constraints = buildConstraints(dataCollection);
        Collection<KeyFigureValidationResult> validationFailures = validateConstraints(constraints);
        logFailures(validationFailures);
        return validationFailures;
    }

    private Collection<KeyFigureConstraint> buildConstraints(ProjectReportingProcessKeyFigureDataCollection dataCollection) {
        Collection<KeyFigureConstraint> constraints = new ArrayList<>();

        constraints.add(constraintFactory.getProjectReportingConstraintG3eqL1L2(dataCollection));
        constraints.add(constraintFactory.getProjectReportingConstraintG3eqL4L5L6L7L8L9(dataCollection));
        constraints.add(constraintFactory.getProjectReportingConstraintL2eqL8L9(dataCollection));
        constraints.add(constraintFactory.getProjectReportingConstraintL10eqL12L13L14L15(dataCollection));
        constraints.add(constraintFactory.getProjectReportingConstraintR2G3eqR3R4(dataCollection));
        constraints.add(constraintFactory.getProjectReportingConstraintR3eqR6R7R8R9(dataCollection));
        constraints.add(constraintFactory.getProjectReportingConstraintR4eqR10R11(dataCollection));
        constraints.add(constraintFactory.getProjectReportingConstraintR12eqR14R15R16R17(dataCollection));
        constraints.add(constraintFactory.getProjectReportingConstraintR19eqR21R22R23R24(dataCollection));

        return constraints;
    }

    private static void logFailures(Collection<KeyFigureValidationResult> validationFailures) {
        validationFailures.forEach(result -> LOG.debug("Validation failure: {}", result));
    }

    private Collection<KeyFigureValidationResult> validateConstraints(Collection<KeyFigureConstraint> constraints) {
        return constraints.stream()
                .map(constraint -> constraint.validate())
                .filter(validationResult -> !validationResult.isValid())
                .collect(Collectors.toList());
    }
}
