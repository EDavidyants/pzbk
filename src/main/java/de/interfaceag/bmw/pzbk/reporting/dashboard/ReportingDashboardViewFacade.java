package de.interfaceag.bmw.pzbk.reporting.dashboard;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 *
 * @author fn
 */
@Stateless
@Named
public class ReportingDashboardViewFacade implements Serializable {

    @Inject
    private ReportingFilterService reportingFilterService;

    public ReportingDashboardViewData getDashboardViewData(UrlParameter urlParameter) {
        ReportingFilter filter = reportingFilterService.getReportingFilter(urlParameter, Page.REPORTING_DASHBOARD);
        return new ReportingDashboardViewData(filter);
    }

}
