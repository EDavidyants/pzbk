package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author fn
 */
final class VereinbarungCurrentKeyFigurQuery {

    private static final Logger LOG = LoggerFactory.getLogger(VereinbarungCurrentKeyFigurQuery.class.getName());

    private VereinbarungCurrentKeyFigurQuery() {

    }

    static VereinbarungCurrentKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {
        Collection<Long> value = getAnforderungIdsForVereinbarungCurrent(filter, entityManager);
        return new VereinbarungCurrentKeyFigure(value, false);
    }

    private static Collection<Long> getAnforderungIdsForVereinbarungCurrent(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO("SELECT a.id ");
        queryPart.append(" FROM ReportingStatusTransition r ")
                .append(" INNER JOIN r.anforderung a ")
                .append(" WHERE (r.plausibilisiert.entry < :endDate AND r.plausibilisiert.exit IS NULL ")
                .append(" OR (r.plausibilisiert.entry < :endDate AND r.plausibilisiert.exit > :endDate)) ");
        queryPart.put("endDate", endDate);

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getAnforderungIdsForVereinbarungCurrent Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }
}
