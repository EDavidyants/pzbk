package de.interfaceag.bmw.pzbk.reporting.projekt.dashboard;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterViewPermission;

import java.io.Serializable;

public final class ProjektReportingDashboardViewData implements Serializable {

    private final ReportingFilter filter;
    private final ProjektReportingDashboardViewPermission viewPermission;
    private final ReportingFilterViewPermission filterViewPermission;

    private ProjektReportingDashboardViewData(ReportingFilter filter, ProjektReportingDashboardViewPermission viewPermission, ReportingFilterViewPermission filterViewPermission) {
        this.filter = filter;
        this.viewPermission = viewPermission;
        this.filterViewPermission = filterViewPermission;
    }

    public ReportingFilter getFilter() {
        return filter;
    }

    public ProjektReportingDashboardViewPermission getViewPermission() {
        return viewPermission;
    }

    public ReportingFilterViewPermission getFilterViewPermission() {
        return filterViewPermission;
    }

    public static Builder withFilter(ReportingFilter filter) {
        return new Builder().withFilter(filter);
    }

    public static final class Builder {

        private ReportingFilter filter;
        private ProjektReportingDashboardViewPermission viewPermission;
        private ReportingFilterViewPermission filterViewPermission;

        private Builder() {
        }

        private Builder withFilter(ReportingFilter filter) {
            this.filter = filter;
            return this;
        }

        public Builder withViewPermission(ProjektReportingDashboardViewPermission viewPermission) {
            this.viewPermission = viewPermission;
            return this;
        }

        public Builder withFilterViewPermission(ReportingFilterViewPermission filterViewPermission) {
            this.filterViewPermission = filterViewPermission;
            return this;
        }

        public ProjektReportingDashboardViewData get() {
            return new ProjektReportingDashboardViewData(filter, viewPermission, filterViewPermission);
        }

    }
}
