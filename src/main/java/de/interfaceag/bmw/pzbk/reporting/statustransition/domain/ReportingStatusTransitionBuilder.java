package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import de.interfaceag.bmw.pzbk.entities.Anforderung;

import java.util.Date;

final class ReportingStatusTransitionBuilder {

    private Date entryDate;
    private Anforderung anforderung;
    private EntryExitTimeStamp inArbeit;
    private EntryExitTimeStamp abgestimmt;
    private EntryExitTimeStamp plausibilisiert;
    private EntryExitTimeStamp freigegeben;
    private EntryExitTimeStamp tteamUnstimmig;
    private EntryExitTimeStamp vereinbarungUnstimmig;
    private EntryExitTimeStamp fachteamGeloescht;
    private EntryExitTimeStamp tteamGeloescht;
    private EntryExitTimeStamp vereinbarungGeloescht;
    private EntryExitTimeStamp freigegebenGeloescht;
    private EntryExitTimeStamp fachteamProzessbaukasten;
    private EntryExitTimeStamp tteamProzessbaukasten;
    private EntryExitTimeStamp vereinbarungProzessbaukasten;
    private EntryExitTimeStamp freigegebenProzessbaukasten;
    private boolean newVersion;

    ReportingStatusTransitionBuilder() {
    }

    ReportingStatusTransitionBuilder setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
        return this;
    }

    ReportingStatusTransitionBuilder setAnforderung(Anforderung anforderung) {
        this.anforderung = anforderung;
        return this;
    }

    ReportingStatusTransitionBuilder setInArbeit(EntryExitTimeStamp inArbeit) {
        this.inArbeit = inArbeit;
        return this;
    }

    ReportingStatusTransitionBuilder setAbgestimmt(EntryExitTimeStamp abgestimmt) {
        this.abgestimmt = abgestimmt;
        return this;
    }

    ReportingStatusTransitionBuilder setPlausibilisiert(EntryExitTimeStamp plausibilisiert) {
        this.plausibilisiert = plausibilisiert;
        return this;
    }

    ReportingStatusTransitionBuilder setFreigegeben(EntryExitTimeStamp freigegeben) {
        this.freigegeben = freigegeben;
        return this;
    }

    ReportingStatusTransitionBuilder setTteamUnstimmig(EntryExitTimeStamp tteamUnstimmig) {
        this.tteamUnstimmig = tteamUnstimmig;
        return this;
    }

    ReportingStatusTransitionBuilder setVereinbarungUnstimmig(EntryExitTimeStamp vereinbarungUnstimmig) {
        this.vereinbarungUnstimmig = vereinbarungUnstimmig;
        return this;
    }

    ReportingStatusTransitionBuilder setFachteamGeloescht(EntryExitTimeStamp fachteamGeloescht) {
        this.fachteamGeloescht = fachteamGeloescht;
        return this;
    }

    ReportingStatusTransitionBuilder setTteamGeloescht(EntryExitTimeStamp tteamGeloescht) {
        this.tteamGeloescht = tteamGeloescht;
        return this;
    }

    ReportingStatusTransitionBuilder setVereinbarungGeloescht(EntryExitTimeStamp vereinbarungGeloescht) {
        this.vereinbarungGeloescht = vereinbarungGeloescht;
        return this;
    }

    ReportingStatusTransitionBuilder setFreigegebenGeloescht(EntryExitTimeStamp freigegebenGeloescht) {
        this.freigegebenGeloescht = freigegebenGeloescht;
        return this;
    }

    ReportingStatusTransitionBuilder setFachteamProzessbaukasten(EntryExitTimeStamp fachteamProzessbaukasten) {
        this.fachteamProzessbaukasten = fachteamProzessbaukasten;
        return this;
    }

    ReportingStatusTransitionBuilder setTteamProzessbaukasten(EntryExitTimeStamp tteamProzessbaukasten) {
        this.tteamProzessbaukasten = tteamProzessbaukasten;
        return this;
    }

    ReportingStatusTransitionBuilder setVereinbarungProzessbaukasten(EntryExitTimeStamp vereinbarungProzessbaukasten) {
        this.vereinbarungProzessbaukasten = vereinbarungProzessbaukasten;
        return this;
    }

    ReportingStatusTransitionBuilder setFreigegebenProzessbaukasten(EntryExitTimeStamp freigegebenProzessbaukasten) {
        this.freigegebenProzessbaukasten = freigegebenProzessbaukasten;
        return this;
    }


    ReportingStatusTransitionBuilder setNewVersion(boolean newVersion) {
        this.newVersion = newVersion;
        return this;
    }

    ReportingStatusTransition build() {
        return new ReportingStatusTransition(entryDate, anforderung, newVersion, inArbeit, abgestimmt, plausibilisiert,
                freigegeben, tteamUnstimmig, vereinbarungUnstimmig, fachteamGeloescht, tteamGeloescht, vereinbarungGeloescht,
                freigegebenGeloescht, fachteamProzessbaukasten, tteamProzessbaukasten, vereinbarungProzessbaukasten, freigegebenProzessbaukasten);
    }
}
