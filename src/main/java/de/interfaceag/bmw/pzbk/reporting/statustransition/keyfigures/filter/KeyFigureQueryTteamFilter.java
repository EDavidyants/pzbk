package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;

import java.util.Collection;

public final class KeyFigureQueryTteamFilter {

    private KeyFigureQueryTteamFilter() {

    }

    public static void append(ReportingFilter filter, QueryPart queryPart) {
        final IdSearchFilter tteamFilter = filter.getTteamFilter();

        if (tteamFilter.isActive() && tteamFilter.getSelectedIdsAsSet() != null && !tteamFilter.getSelectedIdsAsSet().isEmpty()) {
            final Collection<Long> tteamIds = tteamFilter.getSelectedIdsAsSet();

            queryPart.append(" AND a.tteam.id IN :tteamIds ");
            queryPart.put("tteamIds", tteamIds);
        }
    }

}
