package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractLanglauferKeyFigure extends AbstractKeyFigure {

    // change this to false when langlaufer implementation is working correctly
    private boolean isLanglaufer = Boolean.FALSE;

    public AbstractLanglauferKeyFigure(int value, Collection<IdTypeTuple> idTypeTuples, KeyType keyType, Long runTime) {
        super(value, idTypeTuples, keyType, runTime);
    }

    public AbstractLanglauferKeyFigure(Collection<IdTypeTuple> idTypeTuples, KeyType keyType) {
        super(idTypeTuples, keyType);
    }

    @Override
    public boolean isLanglaufer() {
        return isLanglaufer;
    }

    public void setLanglaufer(boolean isLanglaufer) {
        this.isLanglaufer = isLanglaufer;
    }

}
