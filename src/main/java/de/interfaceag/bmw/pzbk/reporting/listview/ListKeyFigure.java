package de.interfaceag.bmw.pzbk.reporting.listview;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;

public interface ListKeyFigure {

    KeyFigure getKeyFigureByType(KeyType type);
}
