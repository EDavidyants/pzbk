package de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht;

import de.interfaceag.bmw.pzbk.enums.Status;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author evda
 */
public class AnforderungsuebersichtDto implements Anforderungsuebersicht, Serializable {

    private final String id;
    private final String fachId;
    private final String version;
    private final String fachIdAndVersion;
    private final String anforderungstext;
    private final String kommentar;
    private final String fachteam;
    private final String tteam;
    private final String status;
    private final String datum;
    private Boolean isLanglaufer;
    private Long durchlaufzeit;

    public AnforderungsuebersichtDto(String id, String fachId, String version, String fachIdAndVersion, String anforderungstext, String kommentar, String fachteam, String tteam, String status, String datum, Boolean isLanglaufer, Long durchlaufzeit) {
        this.id = id;
        this.fachId = fachId;
        this.version = version;
        this.fachIdAndVersion = fachIdAndVersion;
        this.anforderungstext = anforderungstext;
        this.kommentar = kommentar;
        this.fachteam = fachteam;
        this.tteam = tteam;
        this.status = status;
        this.datum = datum;
        this.isLanglaufer = isLanglaufer;
        this.durchlaufzeit = durchlaufzeit;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getFachId() {
        return fachId;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public String getFachIdAndVersion() {
        return fachIdAndVersion;
    }

    @Override
    public String getAnforderungstext() {
        return anforderungstext;
    }

    @Override
    public String getAnforderungstextShort() {
        if (getAnforderungstext().length() > 30) {
            return getAnforderungstext().substring(0, 30) + "...";
        }

        return getAnforderungstext();
    }

    @Override
    public String getKommentar() {
        return kommentar;
    }

    @Override
    public String getKommentarShort() {
        if (getKommentar().length() > 30) {
            return getKommentar().substring(0, 30) + "...";
        }

        return getKommentar();
    }

    @Override
    public Boolean isLanglaufer() {
        return isLanglaufer;
    }

    @Override
    public Long getDurchlaufzeit() {
        return durchlaufzeit;
    }

    @Override
    public String getFachteam() {
        return fachteam;
    }

    @Override
    public String getTteam() {
        return tteam;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public String getDatum() {
        return datum;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.fachId);
        hash = 79 * hash + Objects.hashCode(this.version);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnforderungsuebersichtDto other = (AnforderungsuebersichtDto) obj;
        if (!Objects.equals(this.fachId, other.fachId)) {
            return false;
        }
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        return true;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {

        private String id;
        private String fachId;
        private String version;
        private String fachIdAndVersion;
        private String anforderungstext;
        private String kommentar;
        private String fachteam;
        private String tteam;
        private String status;
        private String datum;
        private Boolean isLanglaufer;
        private Long durchlaufzeit;

        public Builder() {

        }

        public Builder withId(Long id) {
            this.id = id.toString();
            return this;
        }

        public Builder withFachIdAndVersion(String fachIdAndVersion) {
            this.fachIdAndVersion = fachIdAndVersion;
            this.fachId = yieldFachId(fachIdAndVersion);
            this.version = yieldVersion(fachIdAndVersion);
            return this;
        }

        public Builder withAnforderungstext(String anforderungstext) {
            this.anforderungstext = anforderungstext;
            return this;
        }

        public Builder withKommentar(String kommentar) {
            this.kommentar = kommentar;
            return this;
        }

        public Builder withFachteam(String fachteam) {
            this.fachteam = fachteam;
            return this;
        }

        public Builder withTteam(String tteam) {
            this.tteam = tteam;
            return this;
        }

        public Builder withStatus(Status status) {
            this.status = status.getStatusBezeichnung();
            return this;
        }

        public Builder withDatum(Date zeitpunkt) {
            if (zeitpunkt == null) {
                return this;
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            this.datum = sdf.format(zeitpunkt);
            return this;
        }

        public Builder withDurchlaufzeit(Long durchlaufzeit) {
            this.durchlaufzeit = durchlaufzeit;
            return this;
        }

        public Builder withLanglaufer(Boolean langlaufer) {
            this.isLanglaufer = langlaufer;
            return this;
        }

        private String yieldFachId(String fachIdAndVersion) {
            if (isAnforderung(fachIdAndVersion)) {
                String[] parts = fachIdAndVersion.split("\\s");
                return parts[0];
            }

            return fachIdAndVersion;
        }

        private String yieldVersion(String fachIdAndVersion) {
            if (isAnforderung(fachIdAndVersion)) {
                String[] parts = fachIdAndVersion.split("\\s");
                if (parts.length == 3 && parts[2].startsWith("V")) {
                    return parts[2].substring(1);
                }
            }

            return null;
        }

        private boolean isAnforderung(String fachIdAndVersion) {
            return fachIdAndVersion.startsWith("A");
        }

        public AnforderungsuebersichtDto build() {
            return new AnforderungsuebersichtDto(id, fachId, version, fachIdAndVersion, anforderungstext, kommentar, fachteam, tteam, status, datum, isLanglaufer, durchlaufzeit);
        }

    } // end of Builder

}
