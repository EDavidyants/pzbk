package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.drilldown;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;

import java.util.Objects;

/**
 *
 * @author lomu
 */
public class ProjectReportingDrilldownDto {

    private final Long id;
    private final String fachId;
    private final int version;
    private final String modulname;
    private final String zakID;
    private final String beschreibung;
    private String status;

    public ProjectReportingDrilldownDto(Long id, String fachId, int version, String modulname, String zakID, String beschreibung, String status) {
        this.id = id;
        this.fachId = fachId;
        this.version = version;
        this.modulname = modulname;
        this.zakID = zakID;
        this.beschreibung = beschreibung;
        this.status = status;
    }

    public ProjectReportingDrilldownDto(Long id, String fachId, int version, String modulname, String zakID, String beschreibung, int statusId) {
        this.id = id;
        this.fachId = fachId;
        this.version = version;
        this.modulname = modulname;
        this.zakID = zakID;
        this.beschreibung = beschreibung;
        this.status = DerivatAnforderungModulStatus.getStatusById(statusId).getStatusBezeichnung();
    }

    public ProjectReportingDrilldownDto(Long id, String fachId, int version, String modulname, String zakID, String beschreibung, DerivatAnforderungModulStatus status) {
        this.id = id;
        this.fachId = fachId;
        this.version = version;
        this.modulname = modulname;
        this.zakID = zakID;
        this.beschreibung = beschreibung;
        this.status = status.getStatusBezeichnung();
    }

    public ProjectReportingDrilldownDto(Long id, String fachId, int version, String beschreibung) {
        this.id = id;
        this.fachId = fachId;
        this.version = version;
        this.modulname = null;
        this.zakID = null;
        this.beschreibung = beschreibung;
        this.status = "zugeordnet";
    }

    public Long getId() {
        return id;
    }

    public String getFachId() {
        return fachId;
    }

    public int getVersion() {
        return version;
    }

    public String getFachIdAndVersion() {
        return new StringBuilder(this.getFachId()).append(" | V").append(this.getVersion()).toString();
    }

    public String getModulname() {
        return modulname;
    }

    public String getZakID() {
        return zakID;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public String getShortBeschreibung() {
        if (getBeschreibung().length() > 150) {
            return getBeschreibung().substring(0, 150) + "...";
        }

        return getBeschreibung();

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.fachId);
        hash = 11 * hash + this.version;
        hash = 11 * hash + Objects.hashCode(this.modulname);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProjectReportingDrilldownDto other = (ProjectReportingDrilldownDto) obj;
        if (this.version != other.version) {
            return false;
        }
        if (!Objects.equals(this.fachId, other.fachId)) {
            return false;
        }
        if (!Objects.equals(this.modulname, other.modulname)) {
            return false;
        }
        return true;
    }

}
