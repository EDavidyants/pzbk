package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.util.Date;

@Dependent
public class ReportingStatusTranstionTimeStampService implements ReportingStatusTransitionTimeStampUpdatePort {

    public static final Logger LOG = LoggerFactory.getLogger(ReportingStatusTranstionTimeStampService.class);

    @Inject
    private ReportingStatusTransitionTimestampFactory timestampFactory;

    @Override
    public EntryExitTimeStamp updateEntryForTimeStamp(EntryExitTimeStamp timeStamp, Date date) {
        if (timeStamp != null) {
            LOG.debug("Update entry timestamp {}", timeStamp);
            timeStamp.setEntry(date);
        } else {
            timeStamp = timestampFactory.getNewEntryTimeStamp(date);
        }
        return timeStamp;
    }

    @Override
    public EntryExitTimeStamp updateExitForTimeStamp(EntryExitTimeStamp timeStamp, Date date) {
        if (timeStamp != null) {
            LOG.debug("Update exit timestamp {}", timeStamp);
            timeStamp.setExit(date);
        } else {
            timeStamp = timestampFactory.getNewExitTimeStamp(date);
        }
        return timeStamp;
    }

    @Override
    public EntryExitTimeStamp removeEntryForTimeStamp(EntryExitTimeStamp timeStamp) {
        if (timeStamp != null) {
            LOG.debug("Remove entry for timestamp {}", timeStamp);
            timeStamp.removeEntry();
        } else {
            timeStamp = timestampFactory.getNewEmptyTimeStamp();
        }
        return timeStamp;
    }

    @Override
    public EntryExitTimeStamp removeExitForTimeStamp(EntryExitTimeStamp timeStamp) {
        if (timeStamp != null) {
            LOG.debug("Remove exit for timestamp {}", timeStamp);
            timeStamp.removeExit();
        } else {
            timeStamp = timestampFactory.getNewEmptyTimeStamp();
        }
        return timeStamp;
    }

    @Override
    public EntryExitTimeStamp clearTimeStamp(EntryExitTimeStamp timeStamp) {
        if (timeStamp != null) {
            LOG.debug("Clear timestamp {}", timeStamp);
            timeStamp.clear();
        } else {
            timeStamp = timestampFactory.getNewEmptyTimeStamp();
        }
        return timeStamp;
    }
}
