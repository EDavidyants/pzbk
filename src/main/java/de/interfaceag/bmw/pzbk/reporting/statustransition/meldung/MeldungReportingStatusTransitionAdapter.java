package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;

public interface MeldungReportingStatusTransitionAdapter {

    ReportingMeldungStatusTransition changeMeldungStatus(Meldung meldung, Status currentStatus, Status newStatus);

    ReportingMeldungStatusTransition addMeldungToNewAnforderung(Meldung meldung);

    ReportingMeldungStatusTransition addMeldungToExistingAnforderung(Meldung meldung);

    ReportingMeldungStatusTransition removeMeldungFromAnforderung(Meldung meldung, Anforderung anforderung);

}
