package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.listview.ListKeyFigure;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachteamKeyFigure implements Serializable, ListKeyFigure {

    private final long sensorCocId;

    private final String sensorCocDisplayName;

    private final KeyFigureCollection keyFigures;

    private final DurchlaufzeitKeyFigure durchlaufzeitKeyFigure;

    private int langlauferCount;

    public FachteamKeyFigure(long sensorCocId, String sensorCocDisplayName, KeyFigureCollection keyFigures, DurchlaufzeitKeyFigure durchlaufzeitKeyFigure) {
        this.sensorCocId = sensorCocId;
        this.sensorCocDisplayName = sensorCocDisplayName;
        this.keyFigures = keyFigures;
        this.durchlaufzeitKeyFigure = durchlaufzeitKeyFigure;
    }

    public FachteamKeyFigure(long sensorCocId, String sensorCocDisplayName, KeyFigureCollection keyFigures, DurchlaufzeitKeyFigure durchlaufzeitKeyFigure, int langlauferCount) {
        this.sensorCocId = sensorCocId;
        this.sensorCocDisplayName = sensorCocDisplayName;
        this.keyFigures = keyFigures;
        this.durchlaufzeitKeyFigure = durchlaufzeitKeyFigure;
        this.langlauferCount = langlauferCount;
    }

    public long getSensorCocId() {
        return sensorCocId;
    }

    public String getSensorCocDisplayName() {
        return sensorCocDisplayName;
    }

    public Collection<KeyFigure> getKeyFigures() {
        return keyFigures;
    }

    public KeyFigure getKeyFigureByTypeId(int typeId) {
        return keyFigures.stream().filter(keyFigure -> keyFigure.getKeyTypeId() == typeId).findAny().orElse(null);
    }

    @Override
    public KeyFigure getKeyFigureByType(KeyType type) {
        return keyFigures.stream().filter(keyFigure -> keyFigure.getKeyType().equals(type)).findAny().orElse(null);
    }

    public int getDurchlaufzeit() {
        return durchlaufzeitKeyFigure != null ? durchlaufzeitKeyFigure.getValue() : 0;
    }

    public int getLanglauferCount() {
        return langlauferCount;
    }
}
