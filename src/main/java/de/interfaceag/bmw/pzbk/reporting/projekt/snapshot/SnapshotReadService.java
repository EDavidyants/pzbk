package de.interfaceag.bmw.pzbk.reporting.projekt.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author sl
 */
public interface SnapshotReadService<T> extends Serializable {

    List<SnapshotFilter> getSnapshotDatesForDerivat(Derivat derivat);

    Optional<T> getLatestSnapshotForDerivat(Derivat derivat);

    Optional<T> getById(Long id);

    Optional<T> getForDerivatAndDate(Derivat derivat, Date date);

}
