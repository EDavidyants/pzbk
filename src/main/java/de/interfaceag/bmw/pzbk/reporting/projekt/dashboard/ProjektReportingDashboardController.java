package de.interfaceag.bmw.pzbk.reporting.projekt.dashboard;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterController;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterViewPermission;
import de.interfaceag.bmw.pzbk.reporting.ReportingPermissionController;
import de.interfaceag.bmw.pzbk.shared.utils.DateUtils;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;

/**
 * @author fn
 */
@ViewScoped
@Named
public class ProjektReportingDashboardController implements Serializable, ReportingFilterController, ReportingPermissionController<ProjektReportingDashboardViewPermission> {

    @Inject
    private ProjektReportingDashboardViewFacade facade;

    private ProjektReportingDashboardViewData viewData;

    @PostConstruct
    public void init() {
        initViewData();
    }

    private void initViewData() {
        viewData = facade.getDashboardViewData(UrlParameterUtils.getUrlParameter());
    }

    @Override
    public String filter() {
        return viewData.getFilter().getUrl();
    }

    @Override
    public String reset() {
        return viewData.getFilter().getResetUrl();
    }

    @Override
    public String toggleTooltips() {
        getFilter().getHideTooltipFilter().toggle();
        return filter();
    }

    @Override
    public ReportingFilter getFilter() {
        return getViewData().getFilter();
    }

    public ProjektReportingDashboardViewData getViewData() {
        return viewData;
    }

    public String getProjectProcessUrl() {
        return getFilter().getUrlForPage(Page.PROJEKTREPORTING_PROZESS);
    }

    public String getWerkUrl() {
        return getFilter().getUrlForPage(Page.PROJEKTREPORTING_WERK);
    }

    public String getStatusabgleichUrl() {
        return getFilter().getUrlForPage(Page.PROJEKTREPORTING_STATUSABGLEICH);
    }

    public ProjektReportingDashboardViewPermission getViewPermission() {
        return getViewData().getViewPermission();
    }

    @Override
    public ReportingFilterViewPermission getFilterViewPermission() {
        return getViewData().getFilterViewPermission();
    }

    @Override
    public Date getMinDate() {
        return DateUtils.getMinDate();
    }

    @Override
    public Date getMaxDate() {
        return DateUtils.getMaxDate();
    }
}
