package de.interfaceag.bmw.pzbk.reporting.statustransition.migration;

import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author fn
 */
public class ReportingMeldungStatusTransitionMigrationResult implements Serializable {

    private final long meldungId;

    private final String meldungBezeichnung;

    private final List<ReportingMeldungStatusTransition> migrationResult;

    public ReportingMeldungStatusTransitionMigrationResult(long meldungId, String meldungBezeichnung, List<ReportingMeldungStatusTransition> migrationResult) {
        this.meldungId = meldungId;
        this.meldungBezeichnung = meldungBezeichnung;
        this.migrationResult = migrationResult;
    }

    public long getMeldungId() {
        return meldungId;
    }

    public String getMeldungBezeichnung() {
        return meldungBezeichnung;
    }

    public List<ReportingMeldungStatusTransition> getMigrationResult() {
        return migrationResult;
    }

}
