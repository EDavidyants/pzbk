package de.interfaceag.bmw.pzbk.reporting.svg;

import de.interfaceag.bmw.pzbk.shared.dto.Point;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author lomu
 */
public class Polygon implements SVGElement {

    private final List<Point> points;
    private final String polyClasses;

    public Polygon(String polyClasses, List<Point> points) {
        if (points != null) {
            this.points = new ArrayList<>(points);
        } else {
            this.points = Collections.EMPTY_LIST;
        }
        this.polyClasses = polyClasses;
    }

    public String getPointsAsString() {
        StringBuilder result = new StringBuilder();
        if (points != null) {
            for (Tuple<Double, Double> point : points) {
                result.append(point.getX()).append(",").append(point.getY()).append(" ");
            }
        }
        return result.toString();
    }

    public String getPolyClasses() {
        return polyClasses;
    }

    @Override
    public String draw() {
        StringBuilder result = new StringBuilder();

        result.append("<polygon ");

        result.append("class=\"").append(polyClasses).append("\"");

        if (this.getPointsAsString() != null) {
            result.append("points=\"").append(this.getPointsAsString()).append("\" ");
        }
        result.append("/>\n");

        return result.toString();
    }

}
