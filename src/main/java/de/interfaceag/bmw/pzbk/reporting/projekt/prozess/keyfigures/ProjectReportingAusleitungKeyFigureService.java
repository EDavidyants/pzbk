package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataBuilder;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author sl
 */
@Named
@Stateless
public class ProjectReportingAusleitungKeyFigureService {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingAusleitungKeyFigureService.class);

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public Map<Integer, ProjectReportingProcessKeyFigureData> getKeyFiguresForAusleitung(Derivat derivat, ProjectProcessFilter filter) {
        LOG.debug("Compute Ausleitung keyfigures for derivat {} with Filter in Project Reporting", derivat);

        if (derivat == null) {
            LOG.error("Derivat is null! Return zero for all values.");
            return Collections.emptyMap();
        }

        Map<Integer, ProjectReportingProcessKeyFigureData> result = new HashMap<>();

        result.putAll(computeKennzahlenFuerG1(filter));
        result.putAll(computeKennzahlenFuerG2(derivat, filter));
        result.putAll(computeKennzahlenFuerG3(derivat, filter));
        result.putAll(computeKennzahlenFuerR1(derivat, filter));
        result.putAll(computeKennzahlenFuerR2(derivat, filter));

        LOG.debug("Result for Ausleitung {}", result);

        return result;
    }

    private Map<Integer, ProjectReportingProcessKeyFigureData> computeKennzahlenFuerG1(ProjectProcessFilter filter) {
        Long queryDataForAusleitungG1 = getQueryDataForAusleitungG1(filter);

        Map<Integer, ProjectReportingProcessKeyFigureData> result = new HashMap<>();
        ProjectReportingProcessKeyFigure keyfigureValue = ProjectReportingProcessKeyFigure.G1;

        result.put(keyfigureValue.getId(), getKeyFigureData(keyfigureValue, queryDataForAusleitungG1));

        return result;
    }

    private Map<Integer, ProjectReportingProcessKeyFigureData> computeKennzahlenFuerG2(Derivat derivat, ProjectProcessFilter filter) {
        Long queryDataForAusleitungG2 = getQueryDataForAusleitungAnforderungen(derivat, DerivatStatus.VEREINARBUNG_VKBG, filter);

        Map<Integer, ProjectReportingProcessKeyFigureData> result = new HashMap<>();
        ProjectReportingProcessKeyFigure keyfigureValue = ProjectReportingProcessKeyFigure.G2;

        result.put(keyfigureValue.getId(), getKeyFigureData(keyfigureValue, queryDataForAusleitungG2));

        return result;
    }

    private Map<Integer, ProjectReportingProcessKeyFigureData> computeKennzahlenFuerG3(Derivat derivat, ProjectProcessFilter filter) {
        Long queryDataForAusleitungG3 = getQueryDataForAusleitungZakIds(derivat, DerivatStatus.VEREINARBUNG_VKBG, filter);

        Map<Integer, ProjectReportingProcessKeyFigureData> result = new HashMap<>();
        ProjectReportingProcessKeyFigure keyfigureValue = ProjectReportingProcessKeyFigure.G3;

        result.put(keyfigureValue.getId(), getKeyFigureData(keyfigureValue, queryDataForAusleitungG3));

        return result;
    }

    private Map<Integer, ProjectReportingProcessKeyFigureData> computeKennzahlenFuerR1(Derivat derivat, ProjectProcessFilter filter) {
        Long queryDataForAusleitungR1 = getQueryDataForAusleitungAnforderungen(derivat, DerivatStatus.VEREINBARUNG_ZV, filter);

        Map<Integer, ProjectReportingProcessKeyFigureData> result = new HashMap<>();
        ProjectReportingProcessKeyFigure keyfigureValue = ProjectReportingProcessKeyFigure.R1;

        result.put(keyfigureValue.getId(), getKeyFigureData(keyfigureValue, queryDataForAusleitungR1));

        return result;
    }

    private Map<Integer, ProjectReportingProcessKeyFigureData> computeKennzahlenFuerR2(Derivat derivat, ProjectProcessFilter filter) {
        Long queryDataForAusleitungR2 = getQueryDataForAusleitungZakIds(derivat, DerivatStatus.VEREINBARUNG_ZV, filter);

        Map<Integer, ProjectReportingProcessKeyFigureData> result = new HashMap<>();
        ProjectReportingProcessKeyFigure keyfigureValue = ProjectReportingProcessKeyFigure.R2;

        result.put(keyfigureValue.getId(), getKeyFigureData(keyfigureValue, queryDataForAusleitungR2));

        return result;
    }

    private static ProjectReportingProcessKeyFigureData getKeyFigureData(ProjectReportingProcessKeyFigure keyFigure, Long value) {
        return ProjectReportingProcessKeyFigureDataBuilder
                .withKeyFigure(keyFigure)
                .withValue(value)
                .build();
    }

    private Long getQueryDataForAusleitungG1(ProjectProcessFilter filter) {
        QueryPart queryPart = new QueryPartDTO("SELECT COUNT(a.id) FROM Anforderung a ");
        queryPart.append("INNER JOIN a.sensorCoc coc ");
        queryPart.append("WHERE a.status = :status AND a.prozessbaukasten IS EMPTY ");

        if (!filter.isSnapshot() && filter.getTechnologieFilter().isActive()) {
            queryPart.append("AND coc.ortung IN :technologie ");
            List<String> filters = filter.getTechnologieFilter().getSelectedValuestringsAsList();
            queryPart.put("technologie", filters);
        }

        queryPart.put("status", Status.A_FREIGEGEBEN);

        Query query = queryPart.buildGenericQuery(entityManager, Long.class);

        LOG.debug("Excecute query: {}", query.toString());
        List<Long> resultList = query.getResultList();
        if (resultList.size() == 1 && resultList.get(0) >= 0) {
            return resultList.get(0);
        } else {
            LOG.error("Something went wrong by getting G1 for Reporting Rechte Seite Projektansicht");
            return 0L;
        }
    }

    private Long getQueryDataForAusleitungAnforderungen(Derivat derivat, DerivatStatus derivatStatus, ProjectProcessFilter filter) {
        QueryPart queryPart = new QueryPartDTO("SELECT COUNT(DISTINCT(zad.anforderung)) FROM ZakUebertragung zak "
                + "INNER JOIN zak.derivatAnforderungModul dam "
                + "INNER JOIN dam.zuordnungAnforderungDerivat zad "
                + "INNER JOIN zad.derivat d "
                + "INNER JOIN zad.anforderung a "
                + "INNER JOIN a.sensorCoc coc "
                + "WHERE d.id = :derivatId "
                + "AND zad.status IN :zadStatus "
                + "AND zad.zuordnungDerivatStatus = :derivatStatus "
        );

        if (!filter.isSnapshot() && filter.getTechnologieFilter().isActive()) {
            queryPart.append("AND coc.ortung IN :technologie ");
            List<String> filters = filter.getTechnologieFilter().getSelectedValuestringsAsList();
            queryPart.put("technologie", filters);
        }

        Collection<ZuordnungStatus> possibleZuordnungStatus = getPossibleZuordnungStatus();

        queryPart.put("derivatId", derivat.getId());
        queryPart.put("zadStatus", getIds(possibleZuordnungStatus));
        queryPart.put("derivatStatus", derivatStatus.getId());

        Query query = queryPart.buildGenericQuery(entityManager, Long.class);

        LOG.debug("Excecute query: {}", query.toString());
        List<Long> resultList = query.getResultList();
        if (resultList.size() == 1 && resultList.get(0) >= 0) {
            return resultList.get(0);
        } else {
            LOG.error("Something went wrong by getting Anforderungen for Reporting Rechte Seite Projektansicht");
            return -1L;
        }
    }

    private Long getQueryDataForAusleitungZakIds(Derivat derivat, DerivatStatus derivatStatus, ProjectProcessFilter filter) {
        QueryPart queryPart = new QueryPartDTO("SELECT COUNT(DISTINCT(dam)) FROM ZakUebertragung zak "
                + "INNER JOIN zak.derivatAnforderungModul dam "
                + "INNER JOIN dam.zuordnungAnforderungDerivat zad "
                + "INNER JOIN zad.derivat d "
                + "INNER JOIN zad.anforderung a "
                + "INNER JOIN a.sensorCoc coc "
                + "WHERE d.id = :derivatId "
                + "AND zad.status IN :zadStatus "
                + "AND zad.zuordnungDerivatStatus = :derivatStatus "
        );

        if (!filter.isSnapshot() && filter.getTechnologieFilter().isActive()) {
            queryPart.append("AND coc.ortung IN :technologie ");
            List<String> filters = filter.getTechnologieFilter().getSelectedValuestringsAsList();
            queryPart.put("technologie", filters);
        }

        Collection<ZuordnungStatus> zuordnungStatus = getPossibleZuordnungStatus();

        queryPart.put("derivatId", derivat.getId());
        queryPart.put("zadStatus", getIds(zuordnungStatus));
        queryPart.put("derivatStatus", derivatStatus.getId());

        Query query = queryPart.buildGenericQuery(entityManager, Long.class);

        LOG.debug("Excecute query: {}", query.toString());
        List<Long> resultList = query.getResultList();
        if (resultList.size() == 1 && resultList.get(0) >= 0) {
            return resultList.get(0);
        } else {
            LOG.error("Something went wrong by getting Zak IDs for Reporting Rechte Seite Projektansicht");
            return -1L;
        }
    }

    private static Collection<Integer> getIds(Collection<ZuordnungStatus> status) {
        return status.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }

    private static Collection<ZuordnungStatus> getPossibleZuordnungStatus() {
        return Collections.singletonList(ZuordnungStatus.ZUGEORDNET);
    }

}
