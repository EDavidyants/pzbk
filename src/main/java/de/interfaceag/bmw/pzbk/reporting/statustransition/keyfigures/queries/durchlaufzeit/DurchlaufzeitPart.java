package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit;

import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTuple;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

final class DurchlaufzeitPart implements Serializable {

    private final long id;
    private Type type;
    private final Date entry;
    private final Date exit;

    /**
     * This method is used for sql dto mapping and should not be removed!
     *
     * @param id
     * @param entry
     */
    @SuppressWarnings("checkstyle:RedundantModifier")
    public DurchlaufzeitPart(long id, Date entry) {
        this.id = id;
        this.type = null;
        this.entry = entry;
        this.exit = null;
    }

    /**
     * This method is used for sql dto mapping and should not be removed!
     *  @param id
     * @param entry
     * @param exit
     */
    @SuppressWarnings("checkstyle:RedundantModifier")
    public DurchlaufzeitPart(long id, Date entry, Date exit) {
        this.id = id;
        this.type = null;
        this.entry = entry;
        this.exit = exit;
    }

    DurchlaufzeitPart(long id, Type type, Date entry, Date exit) {
        this.id = id;
        this.type = type;
        this.entry = entry;
        this.exit = exit;
    }

    Long getEntryExitDifference() {
        if (Objects.nonNull(entry) && Objects.nonNull(exit)) {
            return Math.abs(exit.getTime() - entry.getTime());
        } else if (Objects.nonNull(entry)) {
            final Date now = new Date();
            return Math.abs(now.getTime() - entry.getTime());
        } else {
            return 0L;
        }
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        DurchlaufzeitPart that = (DurchlaufzeitPart) object;
        return id == that.id &&
                type == that.type &&
                Objects.equals(entry, that.entry) &&
                Objects.equals(exit, that.exit);
    }

    public IdTypeTuple getIdTypeTuple() {
        return new IdTypeTuple(type, id);
    }

    public boolean isActive() {
        return Objects.isNull(exit);
    }

    @Override
    public String toString() {
        return "DurchlaufzeitPart{" +
                "id=" + id +
                ", type=" + type +
                ", entry=" + entry +
                ", exit=" + exit +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, entry, exit);
    }

    public void setAnforderung() {
        this.type = Type.ANFORDERUNG;
    }

    public void setMeldung() {
        this.type = Type.MELDUNG;
    }

    public long getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public Date getEntry() {
        return entry;
    }

    public Date getExit() {
        return exit;
    }
}
