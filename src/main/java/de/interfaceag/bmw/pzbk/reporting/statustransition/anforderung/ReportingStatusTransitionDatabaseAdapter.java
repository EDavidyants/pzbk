package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;

import java.util.List;
import java.util.Optional;

public interface ReportingStatusTransitionDatabaseAdapter {

    void save(ReportingStatusTransition statusTransition);

    Optional<ReportingStatusTransition> getById(Long id);

    Optional<ReportingStatusTransition> getLatestForAnforderung(Anforderung anforderung);

    boolean isHistoryAlreadyExisting(Anforderung anforderung);

    void remove(ReportingStatusTransition statusTransition);

    List<ReportingStatusTransition> getAllStatustransitionsForAnforderung(Anforderung anforderung);

}
