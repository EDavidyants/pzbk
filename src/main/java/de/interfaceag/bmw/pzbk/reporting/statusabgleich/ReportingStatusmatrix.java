package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface ReportingStatusmatrix {

    String getMatrixValue(int row, int column);

    String getRowSum(int row);

    String getColumnSum(int column);

    boolean isHighlighted(int row, int column);

    boolean isValueZero(int row, int column);

}
