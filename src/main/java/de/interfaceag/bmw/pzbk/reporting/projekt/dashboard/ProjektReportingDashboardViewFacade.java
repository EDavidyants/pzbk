package de.interfaceag.bmw.pzbk.reporting.projekt.dashboard;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterService;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterViewPermission;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 *
 * @author fn
 */
@Stateless
@Named
public class ProjektReportingDashboardViewFacade implements Serializable {

    @Inject
    private Session session;

    @Inject
    private ReportingFilterService reportingFilterService;

    public ProjektReportingDashboardViewData getDashboardViewData(UrlParameter urlParameter) {
        ReportingFilter filter = getReportingFilter(urlParameter);
        ReportingFilterViewPermission reportingFilterViewPermission = getReportingFilterViewPermission();
        ProjektReportingDashboardViewPermission viewPermission = getViewPermission();

        return ProjektReportingDashboardViewData.withFilter(filter)
                .withFilterViewPermission(reportingFilterViewPermission)
                .withViewPermission(viewPermission)
                .get();
    }

    private ReportingFilter getReportingFilter(UrlParameter urlParameter) {
        return reportingFilterService.getReportingFilter(urlParameter, Page.PROJEKTREPORTING_DASHBOARD);
    }

    private ReportingFilterViewPermission getReportingFilterViewPermission() {
        return new ReportingFilterViewPermission(session.getUserPermissions().getRoles());
    }

    private ProjektReportingDashboardViewPermission getViewPermission() {
        return new ProjektReportingDashboardViewPermission(session.getUserPermissions().getRoles());
    }

}
