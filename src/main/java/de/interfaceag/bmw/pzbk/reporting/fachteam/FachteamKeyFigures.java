package de.interfaceag.bmw.pzbk.reporting.fachteam;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachteamKeyFigures implements Serializable {

    private final List<FachteamKeyFigure> keyFigures;

    public FachteamKeyFigures(List<FachteamKeyFigure> keyFigures) {
        this.keyFigures = keyFigures;
    }

    public Collection<FachteamKeyFigure> getKeyFigures() {
        return keyFigures;
    }

}
