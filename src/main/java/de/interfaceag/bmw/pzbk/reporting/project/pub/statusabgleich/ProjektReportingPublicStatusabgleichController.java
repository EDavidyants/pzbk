package de.interfaceag.bmw.pzbk.reporting.project.pub.statusabgleich;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.reporting.project.pub.FilterPage;
import de.interfaceag.bmw.pzbk.reporting.project.pub.ReportingPublicFilter;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.ReportingStatusabgleichFacade;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.ReportingStatusabgleichViewData;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ViewScoped
@Named
public class ProjektReportingPublicStatusabgleichController implements Serializable {

    @Inject
    private ReportingStatusabgleichFacade facade;

    private ReportingStatusabgleichViewData viewData;

    @Inject
    @FilterPage(Page.REPORTING_PROJECT_PUBLIC_STATUSABGLEICH)
    private ReportingPublicFilter filter;

    @PostConstruct
    public void init() {
        initViewData();
    }

    private void initViewData() {
        this.viewData = facade.getPublicViewData(UrlParameterUtils.getUrlParameter());
    }

    public String getDerivat() {
        Optional<Derivat> derivat = getViewData().getDerivat();
        if (derivat.isPresent()) {
            return derivat.get().getName();
        } else {
            return "";
        }
    }

    public ReportingStatusabgleichViewData getViewData() {
        return viewData;
    }

    public ReportingPublicFilter getFilter() {
        return filter;
    }

}
