package de.interfaceag.bmw.pzbk.reporting.dashboard;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;

import java.io.Serializable;

/**
 * @author fn
 */
public class ReportingDashboardViewData implements Serializable {

    private final ReportingFilter filter;

    public ReportingDashboardViewData(ReportingFilter filter) {
        this.filter = filter;
    }

    public ReportingFilter getFilter() {
        return filter;
    }

}
