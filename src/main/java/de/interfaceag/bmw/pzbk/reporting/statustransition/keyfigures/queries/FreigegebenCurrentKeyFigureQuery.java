package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FreigegebenCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Date;

/**
 * @author fn
 */
final class FreigegebenCurrentKeyFigureQuery {

    private static final Logger LOG = LoggerFactory.getLogger(FreigegebenCurrentKeyFigureQuery.class.getName());

    private FreigegebenCurrentKeyFigureQuery() {

    }

    static FreigegebenCurrentKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {
        Collection<Long> value = getAnforderungIdsForFreigegebenCurrent(filter, entityManager);
        return new FreigegebenCurrentKeyFigure(value);
    }

    private static Collection<Long> getAnforderungIdsForFreigegebenCurrent(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO("SELECT a.id ");
        queryPart.append(" FROM ReportingStatusTransition r ")
                .append(" INNER JOIN r.anforderung a ")
                .append(" WHERE (r.freigegeben.entry < :endDate AND r.freigegeben.exit IS NULL ")
                .append(" OR (r.freigegeben.entry < :endDate AND r.freigegeben.exit > :endDate)) ");
        queryPart.put("endDate", endDate);

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getAnforderungIdsForFreigegebenCurrent Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

}
