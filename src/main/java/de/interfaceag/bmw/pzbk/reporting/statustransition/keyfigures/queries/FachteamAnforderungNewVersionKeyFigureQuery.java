package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamAnforderungNewVersionKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.RunTime;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;

final class FachteamAnforderungNewVersionKeyFigureQuery {

    private static final Logger LOG = LoggerFactory.getLogger(FachteamAnforderungNewVersionKeyFigureQuery.class.getName());

    private FachteamAnforderungNewVersionKeyFigureQuery() {
    }

    static FachteamAnforderungNewVersionKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {
        final RunTime runTime = RunTime.start();

        Collection<Long> neueVersionenIds = getNewVersionAnforderungIds(filter, entityManager);

        return new FachteamAnforderungNewVersionKeyFigure(neueVersionenIds, runTime.stop().getTime());
    }

    private static Collection<Long> getNewVersionAnforderungIds(ReportingFilter filter, EntityManager entityManager) {

        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT a.id ");
        queryPart.append(" FROM ReportingStatusTransition r ")
                .append(" INNER JOIN r.anforderung a ")
                .append(" WHERE r.newVersion = true ");

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getNewVersionAnforderungIds Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

}
