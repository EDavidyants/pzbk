package de.interfaceag.bmw.pzbk.reporting.svg;

/**
 *
 * @author lomu
 */
public class Line implements SVGElement {

    private final String lineClass;
    private final String x1;
    private final String y1;
    private final String x2;
    private final String y2;

    public Line(String lineClass, String x1, String y1, String x2, String y2) {
        this.lineClass = lineClass;
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public Line(String lineClass, int x1, int y1, int x2, int y2) {
        this.lineClass = lineClass;
        this.x1 = Integer.toString(x1);
        this.y1 = Integer.toString(y1);
        this.x2 = Integer.toString(x2);
        this.y2 = Integer.toString(y2);
    }

    public Line(String lineClass, double x1, double y1, double x2, double y2) {
        this.lineClass = lineClass;
        this.x1 = Double.toString(x1);
        this.y1 = Double.toString(y1);
        this.x2 = Double.toString(x2);
        this.y2 = Double.toString(y2);
    }

    public String getLineClass() {
        return lineClass;
    }

    public String getX1() {
        return x1;
    }

    public String getY1() {
        return y1;
    }

    public String getX2() {
        return x2;
    }

    public String getY2() {
        return y2;
    }

    @Override
    public String draw() {
        StringBuilder result = new StringBuilder();
        result.append("<line ");

        if (this.lineClass != null) {
            result.append("class=\"").append(this.getLineClass()).append("\" ");
        }

        if (this.getX1() != null) {
            result.append("x1=\"").append(this.getX1()).append("\" ");
        }

        if (this.getY1() != null) {
            result.append("y1=\"").append(this.getY1()).append("\" ");
        }

        if (this.getX2() != null) {
            result.append("x2=\"").append(this.getX2()).append("\" ");
        }

        if (this.getY2() != null) {
            result.append("y2=\"").append(this.getY2()).append("\" ");
        }

        result.append("/>\n");

        return result.toString();
    }

}
