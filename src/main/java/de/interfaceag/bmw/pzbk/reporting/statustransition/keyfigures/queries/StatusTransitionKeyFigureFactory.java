package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamAnforderungAbgestimmtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamAnforderungNewVersionKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamCurrentObjectsFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamGeloeschteObjekteKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamGemeldeteMeldungenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamInputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamMeldungExistingAnforderungKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamUnstimmigKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FreigegebenCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FreigegebenGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FreigegebenProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTupleCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamPlausibilisiertKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamUnstimmigKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungFreigegebenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungUnstimmigKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.langlaufer.ProcessoverviewLanglauferService;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

@Dependent
public class StatusTransitionKeyFigureFactory implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    @Inject
    private ProcessoverviewLanglauferService processoverviewLanglauferService;

    public FachteamAnforderungNewVersionKeyFigure computeFachteamAnforderungNewVersionKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final FachteamAnforderungNewVersionKeyFigure keyFigure = FachteamAnforderungNewVersionKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public TteamUnstimmigKeyFigure computeTteamUnstimmigKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final TteamUnstimmigKeyFigure keyFigure = TteamUnstimmigKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public VereinbarungUnstimmigKeyFigure computeVereinbarungUnstimmigKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final VereinbarungUnstimmigKeyFigure keyFigure = VereinbarungUnstimmigKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public FachteamGeloeschteObjekteKeyFigure computeFachteamGeloeschteObjekteKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final FachteamGeloeschteObjekteKeyFigure keyFigure = FachteamGeloeschtKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public TteamGeloeschtKeyFigure computeTteamGeloeschtKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final TteamGeloeschtKeyFigure keyFigure = TteamGeloeschtKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public VereinbarungGeloeschtKeyFigure computeVereinbarungGeloeschtKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final VereinbarungGeloeschtKeyFigure keyFigure = VereinbarungGeloeschtKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public FreigegebenGeloeschtKeyFigure computeFreigegebenGeloeschtKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final FreigegebenGeloeschtKeyFigure keyFigure = FreigegebenGeloeschtKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public FachteamProzessbaukastenKeyFigure computeFachteamProzessbaukastenKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final FachteamProzessbaukastenKeyFigure keyFigure = FachteamProzessbaukastenKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public TteamProzessbaukastenKeyFigure computeTteamProzessbaukastenKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final TteamProzessbaukastenKeyFigure keyFigure = TteamProzessbaukastenKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public VereinbarungProzessbaukastenKeyFigure computeVereinbarungProzessbaukastenKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final VereinbarungProzessbaukastenKeyFigure keyFigure = VereinbarungProzessbaukastenKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public FreigegebenProzessbaukastenKeyFigure computeFreigegebenProzessbaukastenKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final FreigegebenProzessbaukastenKeyFigure keyFigure = FreigegebenProzessbaukastenKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public FachteamMeldungExistingAnforderungKeyFigure computeFachteamMeldungExistingAnforderungKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final FachteamMeldungExistingAnforderungKeyFigure keyFigure = FachteamMeldungExistingAnforderungZugeordnetKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public FachteamAnforderungAbgestimmtKeyFigure computeTteamEntryKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final FachteamAnforderungAbgestimmtKeyFigure keyFigure = TteamEntryKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public TteamPlausibilisiertKeyFigure computeVereinbarungEntryKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final TteamPlausibilisiertKeyFigure keyFigure = VereinbarungEntryKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public VereinbarungFreigegebenKeyFigure computeFreigegebenEntryKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final VereinbarungFreigegebenKeyFigure keyFigure = FreigegebenEntryKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public FachteamGemeldeteMeldungenKeyFigure computeFachteamGemeldeteMeldungenKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final FachteamGemeldeteMeldungenKeyFigure keyFigure = FachteamGemeldeteMeldungenKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public FachteamCurrentObjectsFigure computeFachteamCurrentObjectsFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final FachteamCurrentObjectsFigure keyFigure = FachteamCurrentObjectsFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);

        final IdTypeTupleCollection idTypeTuples = new IdTypeTupleCollection(keyFigure.getIdTypeTuples());
        boolean langlaufer = processoverviewLanglauferService.isFachteamLanglaufer(idTypeTuples);
        keyFigure.setLanglaufer(langlaufer);

        return keyFigure;
    }

    public TteamCurrentKeyFigure computeTteamCurrentKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final TteamCurrentKeyFigure keyFigure = TteamCurrentKeyFigurQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);

        final IdTypeTupleCollection idTypeTuples = new IdTypeTupleCollection(keyFigure.getIdTypeTuples());
        boolean langlaufer = processoverviewLanglauferService.isTteamLanglaufer(idTypeTuples);
        keyFigure.setLanglaufer(langlaufer);

        return keyFigure;
    }

    public VereinbarungCurrentKeyFigure computeVereinbarungCurrentKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final VereinbarungCurrentKeyFigure keyFigure = VereinbarungCurrentKeyFigurQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);

        final IdTypeTupleCollection idTypeTuples = new IdTypeTupleCollection(keyFigure.getIdTypeTuples());
        boolean langlaufer = processoverviewLanglauferService.isVereinbarungLanglaufer(idTypeTuples);
        keyFigure.setLanglaufer(langlaufer);

        return keyFigure;
    }

    public FreigegebenCurrentKeyFigure computeFreigegebenCurrentKeyFigure(ReportingFilter filter, IdTypeTupleCollection filteredIdTypeTuples) {
        final FreigegebenCurrentKeyFigure keyFigure = FreigegebenCurrentKeyFigureQuery.compute(filter, entityManager);
        keyFigure.restrictToIdTypeTuples(filteredIdTypeTuples);
        return keyFigure;
    }

    public FachteamUnstimmigKeyFigure computeFachteamUnstimmigKeyFigure(KeyFigureCollection keyFigures) throws InvalidDataException {
        return FachteamUnstimmigKeyFigure.buildFromKeyFigures(keyFigures.getByKeyType(KeyType.TTEAM_UNSTIMMIG), keyFigures.getByKeyType(KeyType.VEREINBARUNG_UNSTIMMIG));
    }

    public FachteamInputKeyFigure computeFachteamInputKeyFigure(KeyFigureCollection keyFigures) throws InvalidDataException {
        return FachteamInputKeyFigure.buildFromKeyFigures(keyFigures.getByKeyType(KeyType.FACHTEAM_MELDUNG_GEMELDET),
                keyFigures.getByKeyType(KeyType.FACHTEAM_NEW_VERSION_ANFORDERUNG),
                keyFigures.getByKeyType(KeyType.FACHTEAM_ANFORDERUNG_UNSTIMMIG));
    }

    public FachteamOutputKeyFigure computeFachteamOutputKeyFigure(KeyFigureCollection keyFigures) throws InvalidDataException {
        return FachteamOutputKeyFigure.buildFromKeyFigures(keyFigures.getByKeyType(KeyType.FACHTEAM_ANFORDERUNG_ABGESTIMMT),
                keyFigures.getByKeyType(KeyType.FACHTEAM_GELOESCHT),
                keyFigures.getByKeyType(KeyType.FACHTEAM_PZBK),
                keyFigures.getByKeyType(KeyType.FACHTEAM_MELDUNG_EXISTING_ANFORDERUNG));
    }

    public TteamOutputKeyFigure computeTteamOutputKeyFigure(KeyFigureCollection keyFigures) throws InvalidDataException {
        return TteamOutputKeyFigure.buildFromKeyFigures(keyFigures.getByKeyType(KeyType.TTEAM_PLAUSIBILISIERT),
                keyFigures.getByKeyType(KeyType.TTEAM_GELOESCHT),
                keyFigures.getByKeyType(KeyType.TTEAM_UNSTIMMIG),
                keyFigures.getByKeyType(KeyType.TTEAM_PZBK));
    }

    public VereinbarungOutputKeyFigure computeVereinbarungOutputKeyFigure(KeyFigureCollection keyFigures) throws InvalidDataException {
        return VereinbarungOutputKeyFigure.buildFromKeyFigures(keyFigures.getByKeyType(KeyType.VEREINBARUNG_FREIGEGEBEN),
                keyFigures.getByKeyType(KeyType.VEREINBARUNG_GELOESCHT),
                keyFigures.getByKeyType(KeyType.VEREINBARUNG_UNSTIMMIG),
                keyFigures.getByKeyType(KeyType.VEREINBARUNG_PZBK));
    }

}
