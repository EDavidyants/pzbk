package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;

public final class KeyFigureFilterUtils {

    private KeyFigureFilterUtils() {
    }

    static void appendMeldungFilterWithoutDateFilter(ReportingFilter filter, QueryPart queryPart) {
        KeyFigureQueryTteamFilter.append(filter, queryPart);
        KeyFigureQueryMeldungFachteamFilter.append(filter, queryPart);
        KeyFigureQueryModulSeTeamFilter.append(filter, queryPart);
        KeyFigureQueryMeldungTechnologieFilter.append(filter, queryPart);
        KeyFigureQueryWerkFilter.append(filter, queryPart);
        KeyFigureQueryFestgestelltInFilter.append(filter, queryPart);
    }

    static void appendAnforderungFilterWithoutDateFilter(ReportingFilter filter, QueryPart queryPart) {
        KeyFigureQueryTteamFilter.append(filter, queryPart);
        KeyFigureQueryFachteamFilter.append(filter, queryPart);
        KeyFigureQueryModulSeTeamFilter.append(filter, queryPart);
        KeyFigureQueryTechnologieFilter.append(filter, queryPart);
        KeyFigureQueryWerkFilter.append(filter, queryPart);
        KeyFigureQueryFestgestelltInFilter.append(filter, queryPart);
    }

    public static void appendAnforderungFilterWithDateFilter(ReportingFilter filter, QueryPart queryPart) {
        KeyFigureQueryDateFilter.append(filter, queryPart);
        appendAnforderungFilterWithoutDateFilter(filter, queryPart);
    }

    public static void appendMeldungFilterWithDateFilter(ReportingFilter filter, QueryPart queryPart) {
        KeyFigureQueryDateFilter.append(filter, queryPart);
        appendMeldungFilterWithoutDateFilter(filter, queryPart);
    }

}
