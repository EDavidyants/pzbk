package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FreigegebenCurrentKeyFigure extends AbstractKeyFigure {

    public FreigegebenCurrentKeyFigure(Collection<Long> ids) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.FREIGEGEBEN_CURRENT);
    }

    public FreigegebenCurrentKeyFigure(Collection<Long> ids, Long runTime) {
        super(ids.size(),
                IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids),
                KeyType.FREIGEGEBEN_CURRENT, runTime);
    }

}
