package de.interfaceag.bmw.pzbk.reporting.processoverview;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureValidationResult;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ProcessOverviewKeyFigures implements Serializable {

    private final KeyFigureCollection keyFigures;
    private final DurchlaufzeitKeyFigureCollection durchlaufzeitKeyFigures;

    private final String reportingPeriod;

    private final Collection<KeyFigureValidationResult> validationResults;

    public ProcessOverviewKeyFigures(Collection<KeyFigure> keyFigures, String reportingPeriod) {
        this.keyFigures = new KeyFigureCollection(keyFigures);
        this.reportingPeriod = reportingPeriod;
        this.validationResults = Collections.emptySet();
        this.durchlaufzeitKeyFigures = new DurchlaufzeitKeyFigureCollection();
    }

    public ProcessOverviewKeyFigures(Collection<KeyFigure> keyFigures, String reportingPeriod, Collection<KeyFigureValidationResult> validationResults, DurchlaufzeitKeyFigureCollection durchlaufzeitKeyFigureCollection) {
        this.keyFigures = new KeyFigureCollection(keyFigures);
        this.reportingPeriod = reportingPeriod;
        this.validationResults = validationResults;
        this.durchlaufzeitKeyFigures = durchlaufzeitKeyFigureCollection;
    }

    public KeyFigureCollection getKeyFigures() {
        return keyFigures;
    }

    public DurchlaufzeitKeyFigureCollection getDurchlaufzeitKeyFigures() {
        return durchlaufzeitKeyFigures;
    }

    public KeyFigure getKeyFigureByTypeId(int typeId) {
        return keyFigures.stream().filter(keyFigure -> keyFigure.getKeyTypeId() == typeId).findAny().orElse(null);
    }

    public String getReportingPeriod() {
        return reportingPeriod;
    }

    public boolean isValidationFailed() {
        return validationResults.stream().anyMatch(validationResult -> !validationResult.isValid());
    }

    public String getFailedValidationsResultsAsString() {
        return validationResults.stream()
                .filter(validationResult -> !validationResult.isValid())
                .map(KeyFigureValidationResult::getValidationMessage)
                .collect(Collectors.joining("; \n"));
    }

}
