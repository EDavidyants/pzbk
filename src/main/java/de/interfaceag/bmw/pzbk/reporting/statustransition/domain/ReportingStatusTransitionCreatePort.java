package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import de.interfaceag.bmw.pzbk.entities.Anforderung;

import java.io.Serializable;
import java.util.Date;

public interface ReportingStatusTransitionCreatePort extends Serializable {

    ReportingStatusTransition getNewStatusTransitionForAnforderungWithEntryDate(Anforderung anforderung, Date entryDate);

}
