package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamAnforderungAbgestimmtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamPlausibilisiertKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungFreigegebenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.listview.KeyFigureIdForKeyTypeUtils;

import java.util.Collection;

final class TteamSumKeyFigureCalculator {

    private static KeyFigureIdForKeyTypeUtils<TteamKeyFigure> keyTypeUtils = new KeyFigureIdForKeyTypeUtils<>();

    private TteamSumKeyFigureCalculator() {
    }

    static TteamKeyFigure computeSumKeyFigures(Collection<TteamKeyFigure> keyFigures, String label) {

        KeyFigureCollection sumKeyFigures = computeSumKeyFigureCollection(keyFigures);

        DurchlaufzeitKeyFigure tteamDurchlaufzeitMeanKeyFigure = computeTteamMeanDurchlaufzeit(keyFigures);
        DurchlaufzeitKeyFigure vereinbarungDurchlaufzeitMeanKeyFigure = computeVereinbarungMeanDurchlaufzeit(keyFigures);

        int countTteamLanglaufer = TteamLanglauferKeyFigureSumCalculator.getTteamLanglauferSum(keyFigures);
        int countVereinbarungLanglaufer = TteamLanglauferKeyFigureSumCalculator.getVereinbarungLanglauferSum(keyFigures);

        return new TteamKeyFigure(label, sumKeyFigures, tteamDurchlaufzeitMeanKeyFigure, vereinbarungDurchlaufzeitMeanKeyFigure, countTteamLanglaufer, countVereinbarungLanglaufer);
    }

    private static DurchlaufzeitKeyFigure computeTteamMeanDurchlaufzeit(Collection<TteamKeyFigure> keyFigures) {
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanTteamDurchlaufzeit(keyFigures);
        return new TteamDurchlaufzeitKeyFigure(durchlaufzeit);
    }

    private static DurchlaufzeitKeyFigure computeVereinbarungMeanDurchlaufzeit(Collection<TteamKeyFigure> keyFigures) {
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanVereinbarungDurchlaufzeit(keyFigures);
        return new VereinbarungDurchlaufzeitKeyFigure(durchlaufzeit);
    }

    private static KeyFigureCollection computeSumKeyFigureCollection(Collection<TteamKeyFigure> keyFigures) {
        final KeyFigureCollection sumKeyFigures = new KeyFigureCollection();

        sumKeyFigures.add(getTteamCurrentKeyFigure(keyFigures));
        sumKeyFigures.add(getVereinbarungCurrentKeyFigure(keyFigures));
        sumKeyFigures.add(getVereinbarungFreigegebenKeyFigure(keyFigures));

        sumKeyFigures.add(getTteamGeloeschteObjekteKeyFigure(keyFigures));
        sumKeyFigures.add(getVereinbarungGeloeschteObjekteKeyFigure(keyFigures));


        sumKeyFigures.add(getTteamEntryKeyFigure(keyFigures));
        sumKeyFigures.add(getVereinbarungEntryKeyFigure(keyFigures));

        sumKeyFigures.add(getTteamOutputKeyFigure(keyFigures));
        sumKeyFigures.add(getVereinbarungOutputKeyFigure(keyFigures));

        return sumKeyFigures;
    }

    private static VereinbarungFreigegebenKeyFigure getVereinbarungFreigegebenKeyFigure(Collection<TteamKeyFigure> keyFigures) {
        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.VEREINBARUNG_FREIGEGEBEN);
        return new VereinbarungFreigegebenKeyFigure(anforderungIdsForKeyType);
    }

    private static TteamGeloeschtKeyFigure getTteamGeloeschteObjekteKeyFigure(Collection<TteamKeyFigure> keyFigures) {
        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.TTEAM_GELOESCHT);
        return new TteamGeloeschtKeyFigure(anforderungIdsForKeyType);
    }

    private static VereinbarungGeloeschtKeyFigure getVereinbarungGeloeschteObjekteKeyFigure(Collection<TteamKeyFigure> keyFigures) {
        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.VEREINBARUNG_GELOESCHT);
        return new VereinbarungGeloeschtKeyFigure(anforderungIdsForKeyType);
    }

    private static TteamOutputKeyFigure getTteamOutputKeyFigure(Collection<TteamKeyFigure> keyFigures) {
        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.TTEAM_OUTPUT);
        return new TteamOutputKeyFigure(anforderungIdsForKeyType);
    }

    private static VereinbarungOutputKeyFigure getVereinbarungOutputKeyFigure(Collection<TteamKeyFigure> keyFigures) {
        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.VEREINBARUNG_OUTPUT);
        return new VereinbarungOutputKeyFigure(anforderungIdsForKeyType);
    }

    private static TteamCurrentKeyFigure getTteamCurrentKeyFigure(Collection<TteamKeyFigure> keyFigures) {
        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.TTEAM_CURRENT);
        return new TteamCurrentKeyFigure(anforderungIdsForKeyType);
    }

    private static VereinbarungCurrentKeyFigure getVereinbarungCurrentKeyFigure(Collection<TteamKeyFigure> keyFigures) {
        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.VEREINBARUNG_CURRENT);
        return new VereinbarungCurrentKeyFigure(anforderungIdsForKeyType);
    }

    private static FachteamAnforderungAbgestimmtKeyFigure getTteamEntryKeyFigure(Collection<TteamKeyFigure> keyFigures) {
        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.FACHTEAM_ANFORDERUNG_ABGESTIMMT);
        return new FachteamAnforderungAbgestimmtKeyFigure(anforderungIdsForKeyType);
    }

    private static TteamPlausibilisiertKeyFigure getVereinbarungEntryKeyFigure(Collection<TteamKeyFigure> keyFigures) {
        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.TTEAM_PLAUSIBILISIERT);
        return new TteamPlausibilisiertKeyFigure(anforderungIdsForKeyType);
    }

}
