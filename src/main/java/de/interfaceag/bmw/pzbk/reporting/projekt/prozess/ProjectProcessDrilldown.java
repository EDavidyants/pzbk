package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.drilldown.ProjectReportingDrilldownDto;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;

import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author lomu
 */
@Dependent
@Named
public class ProjectProcessDrilldown implements Serializable {

    @Inject
    private ProjectProcessViewFacade facade;
    @Inject
    private ProjectProcessViewData viewData;

    List<ProjectReportingDrilldownDto> drilldownData = new ArrayList<>();

    public List<ProjectReportingDrilldownDto> getDrilldownData() {
        return drilldownData;
    }

    public boolean shouldModulColumnBeShown() {
        if (drilldownData != null && drilldownData.size() > 0) {
            return drilldownData.stream().anyMatch(a -> a.getModulname() != null
                    && !"".equals(a.getModulname()));
        }
        return false;
    }

    public boolean shouldZakIdColumnBeShown() {
        if (drilldownData != null && drilldownData.size() > 0) {
            return drilldownData.stream().anyMatch(a -> a.getZakID() != null
                    && !"".equals(a.getZakID()));
        }
        return false;
    }

    public void openDialog(ProjectReportingProcessKeyFigure figure) {
        drilldownData = getDrilldowndataForKeyfigure(figure);
        if (drilldownData.size() > 0) {
            DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("reportingDialogs:anforderunguebersichtForm:drilldown:anforderungsuebersichtTable");
            dataTable.reset();
            RequestContext.getCurrentInstance().update("reportingDialogs:anforderunguebersichtForm:drilldown:anforderungsuebersichtTable");
            RequestContext.getCurrentInstance().execute("PF('anforderungsuebersichtDialog').show()");
        }
    }

    public List<ProjectReportingDrilldownDto> getDrilldowndataForKeyfigure(ProjectReportingProcessKeyFigure figure) {
        final Optional<Derivat> selectedDerivat = viewData.getSelectedDerivat();
        if (selectedDerivat.isPresent()) {
            final Derivat derivat = selectedDerivat.get();
            List<ProjectReportingDrilldownDto> result = facade.getDrilldowndataForKeyfigure(derivat, figure);
            return result;
        } else {
            return Collections.emptyList();
        }
    }

    public List<ProjectReportingProcessKeyFigure> getAllAvailableDrilldownKeyFigures() {
        List<ProjectReportingProcessKeyFigure> result = facade.getAllAvailableDrilldownKeyFigures();
        return result;
    }

}
