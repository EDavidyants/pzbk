package de.interfaceag.bmw.pzbk.reporting.statustransition.migration;

/**
 *
 * @author fn
 */
public enum MeldungHistoryAction {

    STATUS_CHANGE,
    ADD_TO_NEW_ANFORDERUNG,
    ADD_TO_EXISTING_ANFORDERUNG,
    REMOVE_FROM_ANFORDERUNG,
    OTHER;

}
