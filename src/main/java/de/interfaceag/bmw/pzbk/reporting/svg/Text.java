package de.interfaceag.bmw.pzbk.reporting.svg;

/**
 *
 * @author lomu
 */
public class Text implements SVGElement {

    private final String textClass;
    private final String transform;
    private final String content;

    public Text(String textClass, String transform, String content) {
        this.textClass = textClass;
        this.transform = transform;
        this.content = content;
    }

    public String getTextClass() {
        return textClass;
    }

    public String getTransform() {
        return transform;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String draw() {
        StringBuilder result = new StringBuilder();

        result.append("<text ");

        if (this.getTextClass() != null) {
            result.append("class=\"").append(this.getTextClass()).append("\" ");
        }

        if (this.getTransform() != null) {
            result.append("transform=\"").append(this.getTransform()).append("\">");
        }

        if (this.getContent() != null) {
            result.append(this.getContent());
        }
        result.append("</text>\n");

        return result.toString();
    }

}
