package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Dependent
public class ReportingMeldungStatusTransitionDao implements ReportingMeldungStatusTransitionDatabaseAdapter, Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    @Override
    public void save(ReportingMeldungStatusTransition statusTransition) {
        if (statusTransition.getId() != null) {
            entityManager.merge(statusTransition);
        } else {
            entityManager.persist(statusTransition);
        }
        entityManager.flush();
    }

    @Override
    public Optional<ReportingMeldungStatusTransition> getById(Long id) {
        final ReportingMeldungStatusTransition statusTransition = entityManager.find(ReportingMeldungStatusTransition.class, id);
        return Optional.ofNullable(statusTransition);
    }

    @Override
    public Optional<ReportingMeldungStatusTransition> getLatest(Meldung meldung) {
        TypedQuery<ReportingMeldungStatusTransition> query = entityManager.createNamedQuery(ReportingMeldungStatusTransition.LATEST, ReportingMeldungStatusTransition.class);
        query.setParameter("meldungId", meldung.getId());
        query.setMaxResults(1);
        List<ReportingMeldungStatusTransition> result = query.getResultList();
        return result != null && !result.isEmpty() ? Optional.ofNullable(result.get(0)) : Optional.empty();
    }

    @Override
    public boolean isHistoryAlreadyExisting(Meldung meldung) {
        return getLatest(meldung).isPresent();
    }

}
