package de.interfaceag.bmw.pzbk.reporting.projekt.werk;

import javax.ejb.Stateless;
import javax.inject.Named;
import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class ProjectWerkViewFacade implements Serializable {

    public ProjectWerkViewData getViewData() {
        return buildViewData();
    }

    private ProjectWerkViewData buildViewData() {
        ProjectWerkViewData filter = ProjectWerkViewData.createNew()
                .withFilter(getFilter())
                .addProjectReport(getProjectReport())
                .addProjectReport(getProjectReport())
                .addProjectReport(getProjectReport())
                .addProjectReport(getProjectReport())
                .addProjectReport(getProjectReport())
                .addProjectReport(getProjectReport())
                .addProjectReport(getProjectReport())
                .get();
        return filter;
    }

    private static ProjectReportDto getProjectReport() {
        return ProjectReportDto.withProjectName("F12")
                .withNumberOfAusleitungen("12")
                .withNumberOfVereinbarungenBestaetigt(12)
                .withNumberOfVereinbarungenNichtWeiterverfolgt(2)
                .withNumberOfVereinbarungenOffen(3)
                .withFirstUmsetzungsbestaetigungPhaseResult(12, 2, 3245)
                .withSecondUmsetzungsbestaetigungPhaseResult(0, 0, 0)
                .get();
    }

    private ProjectWerkFilter getFilter() {
        return new ProjectWerkFilter();
    }

}
