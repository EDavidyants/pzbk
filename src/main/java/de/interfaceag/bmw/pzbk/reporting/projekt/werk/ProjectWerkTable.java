package de.interfaceag.bmw.pzbk.reporting.projekt.werk;

import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface ProjectWerkTable {

    List<ProjectReportDto> getProjectReports();

}
