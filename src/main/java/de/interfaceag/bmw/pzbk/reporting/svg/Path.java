package de.interfaceag.bmw.pzbk.reporting.svg;

/**
 *
 * @author lomu
 */
public class Path implements SVGElement {

    private final String pathClass;
    private final String d;
    private final String transform;

    public Path(String pathClass, String dValue, String transform) {
        this.pathClass = pathClass;
        this.d = dValue;
        this.transform = transform;
    }

    public String getPathClass() {
        return pathClass;
    }

    public String getD() {
        return d;
    }

    public String getTransform() {
        return transform;
    }

    @Override
    public String draw() {
        StringBuilder result = new StringBuilder();

        result.append("<path ");

        if (this.getPathClass() != null) {
            result.append("class=\"").append(this.getPathClass()).append("\" ");
        }

        if (this.getD() != null) {
            result.append("d=\"").append(this.getD()).append("\" ");
        }
        if (this.getTransform() != null) {
            result.append("transform=\"").append(this.getTransform()).append("\"");
        }
        result.append("/>\n");

        return result.toString();
    }

}
