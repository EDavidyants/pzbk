package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Date;

final class VereinbarungProzessbaukastenKeyFigureQuery {

    private static final Logger LOG = LoggerFactory.getLogger(VereinbarungProzessbaukastenKeyFigureQuery.class.getName());

    private VereinbarungProzessbaukastenKeyFigureQuery() {
    }

    static VereinbarungProzessbaukastenKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {
        Collection<Long> anforderungIds = getAnforderungIdsForVereinbarungProzessbaukastenEntry(filter, entityManager);
        return new VereinbarungProzessbaukastenKeyFigure(anforderungIds);
    }

    private static Collection<Long> getAnforderungIdsForVereinbarungProzessbaukastenEntry(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT a.id ");
        queryPart.append(" FROM ReportingStatusTransition r ")
                .append(" INNER JOIN r.anforderung a ")
                .append(" WHERE r.vereinbarungProzessbaukasten.entry < :endDate ")
                .append(" AND r.vereinbarungProzessbaukasten.exit IS NULL ");
        queryPart.put("endDate", endDate);

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getAnforderungIdsForVereinbarungProzessbaukastenEntry Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

}
