package de.interfaceag.bmw.pzbk.reporting.svg;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class LinkToSearch implements SVGElement {

    private final KeyFigure keyFigure;

    public LinkToSearch(KeyFigure keyFigure) {
        this.keyFigure = keyFigure;
    }

    @Override
    public String draw() {
        StringBuilder sb = new StringBuilder();
        if (keyFigure != null) {
            // TODO /PZBK/ aus URL auslesen und einsetzen!

            sb.append("<a href=/PZBK/anforderungList.xhtml?faces-redirect=true&");
            sb.append(addParam(keyFigure.getAnforderungIdsAsString(), "aId")).append("&");
            sb.append(addParam(keyFigure.getMeldungIdsAsString(), "mId"));
            sb.append(">").append(keyFigure.getValue());
            sb.append("</ a>");
        }
        return sb.toString();

    }

    private static String addParam(String value, String name) {
        return name + "=" + value;
    }

}
