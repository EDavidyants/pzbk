package de.interfaceag.bmw.pzbk.reporting.langlaufer;

import de.interfaceag.bmw.pzbk.enums.Type;

import java.io.Serializable;
import java.util.Objects;

public class DurchlaufzeitDto implements Serializable {

    private final Type type;
    private final Long id;
    private long durchlaufzeit;
    private final int threshold;

    public DurchlaufzeitDto(Type type, Long id, Long durchlaufzeit, int threshold) {
        this.type = type;
        this.id = id;
        this.setDurchlaufzeit(durchlaufzeit);
        this.threshold = threshold;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        DurchlaufzeitDto that = (DurchlaufzeitDto) object;
        return threshold == that.threshold &&
                type == that.type &&
                Objects.equals(id, that.id) &&
                Objects.equals(durchlaufzeit, that.durchlaufzeit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, id, durchlaufzeit, threshold);
    }

    public Type getType() {
        return type;
    }

    public Long getId() {
        return id;
    }

    public Long getDurchlaufzeit() {
        return durchlaufzeit;
    }

    public boolean isLanglaufer() {
        return durchlaufzeit > threshold;
    }

    private void setDurchlaufzeit(Long durchlaufzeit) {
        if (durchlaufzeit == null) {
            this.durchlaufzeit = 0L;
        }
        this.durchlaufzeit = durchlaufzeit;
    }

}
