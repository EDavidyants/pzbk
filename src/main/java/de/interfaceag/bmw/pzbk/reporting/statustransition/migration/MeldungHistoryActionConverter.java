package de.interfaceag.bmw.pzbk.reporting.statustransition.migration;

/**
 *
 * @author fn
 */
final class MeldungHistoryActionConverter {

    private MeldungHistoryActionConverter() {
    }

    static MeldungHistoryAction convert(String objektName, String von, String auf) {
        if ("Status".equals(objektName) && "zugeordnet".equals(von) && "gemeldet".equals(auf)) {
            return MeldungHistoryAction.REMOVE_FROM_ANFORDERUNG;
        }
        if ("Status".equals(objektName) && "zugeordnet".equals(auf) && "gemeldet".equals(von)) {
            return MeldungHistoryAction.ADD_TO_NEW_ANFORDERUNG;
        }
        if ("Kommentar zum Statuswechsel".equals(objektName) && auf.contains("wurde der bestehenden Anforderung")) {
            return MeldungHistoryAction.ADD_TO_EXISTING_ANFORDERUNG;
        }
        if ("Status".equals(objektName) && !"zugeordnet".equals(auf)) {
            return MeldungHistoryAction.STATUS_CHANGE;
        }

        return MeldungHistoryAction.OTHER;
    }

}
