package de.interfaceag.bmw.pzbk.reporting.svg;

/**
 *
 * @author lomu
 */
public interface SVGElement {

    String draw();

}
