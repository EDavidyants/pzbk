package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamCurrentObjectsFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamGeloeschteObjekteKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamInputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FreigegebenCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.listview.KeyFigureIdForKeyTypeUtils;

import java.util.Collection;

final class FachteamSumKeyFigureCalculator {

    private static KeyFigureIdForKeyTypeUtils<FachteamKeyFigure> keyTypeUtils = new KeyFigureIdForKeyTypeUtils<>();

    private FachteamSumKeyFigureCalculator() {
    }

    static FachteamKeyFigure computeSumKeyFigures(Collection<FachteamKeyFigure> keyFigures, String label) {

        KeyFigureCollection sumKeyFigures = computeSumKeyFigureCollection(keyFigures);
        DurchlaufzeitKeyFigure durchlaufzeitMeanKeyFigure = computeFachteamMeanDurchlaufzeit(keyFigures);

        int countLanglaufer = FachteamLanglauferKeyFigureSumCalculator.getLanglauferSum(keyFigures);

        return new FachteamKeyFigure(-1, label, sumKeyFigures, durchlaufzeitMeanKeyFigure, countLanglaufer);
    }

    private static DurchlaufzeitKeyFigure computeFachteamMeanDurchlaufzeit(Collection<FachteamKeyFigure> keyFigures) {
        final int durchlaufzeit = FachteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanDurchlaufzeit(keyFigures);
        return new FachteamDurchlaufzeitKeyFigure(durchlaufzeit);
    }

    private static KeyFigureCollection computeSumKeyFigureCollection(Collection<FachteamKeyFigure> keyFigures) {
        final KeyFigureCollection sumKeyFigures = new KeyFigureCollection();
        sumKeyFigures.add(getFachteamInputKeyFigure(keyFigures));
        sumKeyFigures.add(getFachteamCurrentKeyFigure(keyFigures));
        sumKeyFigures.add(getFachteamOutputKeyFigure(keyFigures));
        sumKeyFigures.add(getFachteamGeloeschteObjekteKeyFigure(keyFigures));
        sumKeyFigures.add(getFreigegebenCurrentKeyFigure(keyFigures));
        return sumKeyFigures;
    }

    private static FreigegebenCurrentKeyFigure getFreigegebenCurrentKeyFigure(Collection<FachteamKeyFigure> keyFigures) {

        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.FREIGEGEBEN_CURRENT);

        return new FreigegebenCurrentKeyFigure(anforderungIdsForKeyType, 0L);
    }

    private static FachteamGeloeschteObjekteKeyFigure getFachteamGeloeschteObjekteKeyFigure(Collection<FachteamKeyFigure> keyFigures) {

        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.FACHTEAM_GELOESCHT);
        final Collection<Long> meldungIdsForKeyType = keyTypeUtils.getMeldungIdsForKeyType(keyFigures, KeyType.FACHTEAM_GELOESCHT);

        return new FachteamGeloeschteObjekteKeyFigure(anforderungIdsForKeyType, meldungIdsForKeyType, 0L);
    }


    private static FachteamOutputKeyFigure getFachteamOutputKeyFigure(Collection<FachteamKeyFigure> keyFigures) {

        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.FACHTEAM_OUTPUT);
        final Collection<Long> meldungIdsForKeyType = keyTypeUtils.getMeldungIdsForKeyType(keyFigures, KeyType.FACHTEAM_OUTPUT);

        return new FachteamOutputKeyFigure(anforderungIdsForKeyType, meldungIdsForKeyType, 0L);
    }

    private static FachteamCurrentObjectsFigure getFachteamCurrentKeyFigure(Collection<FachteamKeyFigure> keyFigures) {

        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.FACHTEAM_CURRENT);
        final Collection<Long> meldungIdsForKeyType = keyTypeUtils.getMeldungIdsForKeyType(keyFigures, KeyType.FACHTEAM_CURRENT);

        return new FachteamCurrentObjectsFigure(meldungIdsForKeyType, anforderungIdsForKeyType, 0L);
    }

    private static FachteamInputKeyFigure getFachteamInputKeyFigure(Collection<FachteamKeyFigure> keyFigures) {

        final Collection<Long> anforderungIdsForKeyType = keyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.FACHTEAM_ENTRY_SUM);
        final Collection<Long> meldungIdsForKeyType = keyTypeUtils.getMeldungIdsForKeyType(keyFigures, KeyType.FACHTEAM_ENTRY_SUM);

        return new FachteamInputKeyFigure(meldungIdsForKeyType, anforderungIdsForKeyType, 0L);
    }

}
