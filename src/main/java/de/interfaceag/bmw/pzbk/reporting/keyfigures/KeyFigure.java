package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface KeyFigure extends DurchlaufzeitKeyFigure {

    Collection<IdTypeTuple> getIdTypeTuples();

    Collection<Long> getMeldungIds();

    Collection<Long> getAnforderungIds();

    String getMeldungIdsAsString();

    String getAnforderungIdsAsString();

    String getKeyTypeAsString();

    boolean isLanglaufer();

    Long getRunTime();

    void restrictToIdTypeTuples(IdTypeTupleCollection validIdTypeTuples);
}
