package de.interfaceag.bmw.pzbk.reporting.processoverview;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public enum ProcessOverviewDetailTooltip {

    FACHTEAM_HEADER, FACHTEAM_DURCHLAUFZEIT,
    TTEAM_HEADER, TTEAM_DURCHLAUFZEIT,
    VEREINBARUNG_HEADER, VEREINBARUNG_DURCHLAUFZEIT,
    FREIGEGEBEN, DURCHLAUFZEIT_GESAMT,
    LANGLAUFER_FACHTEAM, LANGLAUFER_TTEAM,
    LANGLAUFER_VEREINBARUNG, LANGLAUFER_FREIGEGEBEN,
    ZEITRAUM;

    @Override
    public String toString() {
        return "ProcessOverviewDetailTooltip" + this.name();
    }

    public static Collection<String> getNameList() {
        return Stream.of(ProcessOverviewDetailTooltip.values()).map(t -> t.toString()).collect(Collectors.toList());
    }

    public static Optional<ProcessOverviewDetailTooltip> getByName(String name) {
        return Stream.of(ProcessOverviewDetailTooltip.values()).filter(t -> t.toString().equals(name)).findAny();
    }

}
