package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.filter.MeldungStatusFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;

import java.util.List;
import java.util.Objects;

public final class KeyFigureQueryMeldungStatusFilter {

    private KeyFigureQueryMeldungStatusFilter() {

    }

    public static void append(ReportingFilter filter, QueryPart queryPart, Type type) {
        final MeldungStatusFilter meldungStatusFilter = filter.getMeldungStatusFilter();

        if (Objects.nonNull(meldungStatusFilter) && meldungStatusFilter.isActive()) {
            if (type.isMeldung()) {
                final List<Status> meldungStatus = meldungStatusFilter.getAsEnumList();

                queryPart.append(" AND m.status IN :meldungStatus ");
                queryPart.put("meldungStatus", meldungStatus);
            } else {
                queryPart.append(" AND 1 = 0 ");
            }
        }
    }

}
