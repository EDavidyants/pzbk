package de.interfaceag.bmw.pzbk.reporting.projekt.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot.ProjectProcessSnapshotWriteService;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot.StatusabgleichSnapshotWriteService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@Dependent
public class ProjectReportingSnapshotService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(ProjectReportingSnapshotService.class);

    @Inject
    private DerivatService derivatService;
    @Inject
    private ProjectProcessSnapshotWriteService projectProcessSnapshotService;
    @Inject
    private StatusabgleichSnapshotWriteService statusabgleichSnapshotService;

    public void createSnapshotsForActiveDerivate() {
        LOG.info("Start snapshot generation for active derivate");

        List<Derivat> activeDerivate = derivatService.getAllDerivateWithZielvereinbarungFalse();
        LOG.info("Found {} active derivate", activeDerivate.size());

        for (Derivat derivat : activeDerivate) {
            LOG.info("Create snapshots for derivat {}", derivat);
            // create snapshot for statusabgleich matrix
            statusabgleichSnapshotService.createNewSnapshotForDerivat(derivat);
            // create snapshot for project process overview
            projectProcessSnapshotService.createNewSnapshotForDerivat(derivat);
        }

    }

}
