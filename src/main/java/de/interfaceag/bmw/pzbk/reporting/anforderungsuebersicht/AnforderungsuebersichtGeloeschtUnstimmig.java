package de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht;

/**
 *
 * @author evda
 */
public interface AnforderungsuebersichtGeloeschtUnstimmig extends Anforderungsuebersicht {

    String getVorigerStatus();

    String getStatusChangeKommentar();

}
