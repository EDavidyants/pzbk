package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamGeloeschteObjekteKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Date;

final class FachteamGeloeschtKeyFigureQuery {

    private static final Logger LOG = LoggerFactory.getLogger(FachteamGeloeschtKeyFigureQuery.class.getName());

    private FachteamGeloeschtKeyFigureQuery() {
    }

    static FachteamGeloeschteObjekteKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {
        Collection<Long> anforderungIds = getAnforderungIdsForFachteamGeloeschtEntry(filter, entityManager);
        Collection<Long> meldungIds = getMeldungIdsForFachteamGeloeschtEntry(filter, entityManager);
        return new FachteamGeloeschteObjekteKeyFigure(anforderungIds, meldungIds);
    }

    private static Collection<Long> getAnforderungIdsForFachteamGeloeschtEntry(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT(a.id) ");
        queryPart.append(" FROM ReportingStatusTransition r ")
                .append(" INNER JOIN r.anforderung a ")
                .append(" WHERE r.fachteamGeloescht.entry  < :endDate ")
                .append(" AND r.fachteamGeloescht.exit IS NULL ");
        queryPart.put("endDate", endDate);

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getAnforderungIdsForFachteamGeloeschtEntry Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

    private static Collection<Long> getMeldungIdsForFachteamGeloeschtEntry(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT(m.id) ");
        queryPart.append(" FROM ReportingMeldungStatusTransition r ")
                .append(" INNER JOIN r.meldung m ")
                .append(" LEFT JOIN m.anforderung a ")
                .append(" WHERE r.geloescht.entry  < :endDate")
                .append(" AND r.geloescht.exit IS NULL ")
                .append(" AND r.unstimmig.entry IS NULL ")
                .append(" AND r.unstimmig.exit IS NULL ")
                .append(" AND a.id IS NULL ");
        queryPart.put("endDate", endDate);

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getMeldungIdsForFachteamGeloeschtEntry Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

}
