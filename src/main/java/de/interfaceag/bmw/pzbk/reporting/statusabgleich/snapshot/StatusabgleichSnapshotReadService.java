package de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot;

import de.interfaceag.bmw.pzbk.reporting.projekt.snapshot.SnapshotReadService;

/**
 *
 * @author sl
 */
public interface StatusabgleichSnapshotReadService extends SnapshotReadService<StatusabgleichSnapshot> {

}
