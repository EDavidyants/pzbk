package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.excel;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures.ProjectReportingKeyFigureService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 *
 * @author sl
 */
@Named
@Stateless
public class ProjectReportingExcelExportService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingExcelExportService.class);

    @Inject
    private ProjectReportingKeyFigureService vereinbarungKeyFigureService;
    @Inject
    private ExcelDownloadService excelDownloadService;

    public void downloadKeyFiguresAsExcelFile(Derivat derivat, ProjectProcessFilter filter) {
        LOG.debug("Generate Excel export keyfigures for derivat {}", derivat);
        ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat = vereinbarungKeyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        LOG.debug("Generated keyfigures: {}", keyFiguresForDerivat);
        String fileName = ProjectReportingExcelExportUtils.generateFilenName(derivat);
        Workbook excel = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, fileName);
        excelDownloadService.downloadExcelExport(fileName, excel);
    }

}
