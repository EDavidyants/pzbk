package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.validation;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureConstraint;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureValidationResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ProjectReportingCollectionEqualsConstraint implements KeyFigureConstraint {

    private final Collection<ProjectReportingProcessKeyFigureData> leftSide;
    private final Collection<ProjectReportingProcessKeyFigureData> rightSide;

    private final String validationFailedMessage;

    private ProjectReportingCollectionEqualsConstraint(Collection<ProjectReportingProcessKeyFigureData> leftSide,
                                                       Collection<ProjectReportingProcessKeyFigureData> rightSide,
                                                       String failedMessage) {
        this.leftSide = new ArrayList<>(leftSide);
        this.rightSide = new ArrayList<>(rightSide);
        this.validationFailedMessage = failedMessage;
    }

    @Override
    public KeyFigureValidationResult validate() {

        int left = getCollectionSize(leftSide);
        int right = getCollectionSize(rightSide);
        boolean result = left == right;
        if (result) {
            return new KeyFigureValidationResult(result);
        } else {
            String message = getValidationFailedMessage();
            return new KeyFigureValidationResult(result, message);
        }
    }

    private String getValidationFailedMessage() {
        return collectionToString(leftSide) + " = " + collectionToString(rightSide) + " " + validationFailedMessage + " "
                + getCollectionSize(leftSide) + " = " + getCollectionSize(rightSide);
    }

    private static String collectionToString(Collection<ProjectReportingProcessKeyFigureData> collection) {
        if (collection != null) {
            return collection.stream().map(keyFigure -> keyFigure.getKeyFigure().name()).collect(Collectors.joining(" + "));
        }
        return "";
    }

    private static int getCollectionSize(Collection<ProjectReportingProcessKeyFigureData> collection) {
        if (collection != null) {
            return collection.stream().map(ProjectReportingProcessKeyFigureData::getValue).mapToInt(Long::intValue).sum();
        }
        return 0;
    }

    public static Builder build() {
        return new Builder();
    }

    public static final class Builder {

        private final Collection<ProjectReportingProcessKeyFigureData> leftSide = new ArrayList<>();
        private final Collection<ProjectReportingProcessKeyFigureData> rightSide = new ArrayList<>();

        private String reportingValidationFailed;

        private Builder() {
        }

        Builder withValidationMessageFailed(String failed) {
            if (failed != null && !failed.isEmpty()) {
                this.reportingValidationFailed = failed;
            }
            return this;
        }

        Builder addToRightSide(ProjectReportingProcessKeyFigureData keyFigureData) {
            rightSide.add(keyFigureData);
            return this;
        }

        Builder addToLeftSide(ProjectReportingProcessKeyFigureData keyFigureData) {
            leftSide.add(keyFigureData);
            return this;
        }

        public KeyFigureConstraint build() {
            return new ProjectReportingCollectionEqualsConstraint(leftSide, rightSide, reportingValidationFailed);
        }

    }

}
