package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit;

import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTuple;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureFilterUtils;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryAnforderungStatusFilter;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryMeldungStatusFilter;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.KeyFigureQueryEndDate;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class FachteamDurchlaufzeitKeyFigureQuery {

    private static final String SELECT = "SELECT new de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit.DurchlaufzeitPart(";
    private static final String FROM_ANFORDERUNG_REPORTING = ") FROM ReportingStatusTransition r ";
    private static final String FROM_MELDUNG_REPORTING = ") FROM ReportingMeldungStatusTransition r ";
    private static final String ANOFRDERUNG_JOINS = "INNER JOIN r.anforderung a LEFT JOIN a.umsetzer u LEFT JOIN a.festgestelltIn f ";
    private static final String MELDUNG_JOINS = "INNER JOIN r.meldung m LEFT JOIN m.anforderung a LEFT JOIN a.umsetzer u LEFT JOIN a.festgestelltIn f ";
    private static final String AND_NOT_NEW_VERSION = "AND r.newVersion = false ";
    private static final String AND_NEW_VERSION = "AND r.newVersion = true ";

    private FachteamDurchlaufzeitKeyFigureQuery() {
    }

    public static Map<IdTypeTuple, Long> computeIdTypeDurchlaufzeit(ReportingFilter filter, EntityManager entityManager) {
        Collection<DurchlaufzeitPart> case1 = getCase1(filter, entityManager);
        Collection<DurchlaufzeitPart> case2 = getCase2(filter, entityManager);
        Collection<DurchlaufzeitPart> case3 = getCase3(filter, entityManager);

        final List<DurchlaufzeitPart> allCases = Stream.of(case1, case2, case3).flatMap(Collection::stream).collect(Collectors.toList());

        return DurchlaufzeitCalculator.computeDurchlaufzeitForActiveIdTypeTuple(allCases);

    }

    public static FachteamDurchlaufzeitKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {
        Collection<DurchlaufzeitPart> case1 = getCase1(filter, entityManager);
        Collection<DurchlaufzeitPart> case2 = getCase2(filter, entityManager);
        Collection<DurchlaufzeitPart> case3 = getCase3(filter, entityManager);

        final Long durchlaufzeit = DurchlaufzeitCalculator.computeDurchlaufzeit(case1, case2, case3);

        return new FachteamDurchlaufzeitKeyFigure(durchlaufzeit.intValue());
    }

    private static Collection<DurchlaufzeitPart> getCase1(ReportingFilter filter, EntityManager entityManager) {
        List<DurchlaufzeitPart> result = new ArrayList<>();

        result.addAll(getCase1D1(filter, entityManager));
        result.addAll(getCase1D2(filter, entityManager));

        return result;
    }

    private static Collection<DurchlaufzeitPart> getCase2(ReportingFilter filter, EntityManager entityManager) {
        List<DurchlaufzeitPart> result = new ArrayList<>();

        result.addAll(getCase2CurrentlyGemeldet(filter, entityManager));
        result.addAll(getCase2ExistingAnforderungZugeordnet(filter, entityManager));

        return result;
    }

    private static Collection<DurchlaufzeitPart> getCase3(ReportingFilter filter, EntityManager entityManager) {
        List<DurchlaufzeitPart> result = new ArrayList<>();

        result.addAll(getCase3D1(filter, entityManager));
        result.addAll(getCase3D2(filter, entityManager));

        return result;
    }

    private static Collection<DurchlaufzeitPart> getCase1D1(ReportingFilter filter, EntityManager entityManager) {
        List<DurchlaufzeitPart> result = new ArrayList<>();

        result.addAll(getCase1D1CurrentlyInArbeit(filter, entityManager));
        result.addAll(getCase1D1InGemeldetToAbgestimmt(filter, entityManager));
        result.addAll(getCase1D1InGemeldetToUnstimmig(filter, entityManager));
        result.addAll(getCase1D1InArbeitToGeloescht(filter, entityManager));

        return result;
    }

    private static Collection<DurchlaufzeitPart> getCase1D2(ReportingFilter filter, EntityManager entityManager) {
        List<DurchlaufzeitPart> result = new ArrayList<>();

        result.addAll(getCurrentlyFachteamUnstimmigAndNotNewVersion(filter, entityManager));
        result.addAll(getCurrentlyTteamUnstimmigAndNotNewVersion(filter, entityManager));
        result.addAll(getCurrentlyVereinbarungUnstimmigAndNotNewVersion(filter, entityManager));
        result.addAll(getFachteamUnstimmigIntervallsAndNotNewVersion(filter, entityManager));
        result.addAll(getTteamUnstimmigIntervallsAndNotNewVersion(filter, entityManager));
        result.addAll(getVereinbarungUnstimmigIntervallsAndNotNewVersion(filter, entityManager));

        return result;
    }

    private static Collection<DurchlaufzeitPart> getCase3D1(ReportingFilter filter, EntityManager entityManager) {
        List<DurchlaufzeitPart> result = new ArrayList<>();

        result.addAll(getCase3D1CurrentlyInArbeit(filter, entityManager));
        result.addAll(getCase3D1InInArbeitToAbgestimmt(filter, entityManager));
        result.addAll(getCase3D1InInArbeitToUnstimmig(filter, entityManager));
        result.addAll(getCase3D1InArbeitToGeloescht(filter, entityManager));

        return result;
    }

    private static Collection<DurchlaufzeitPart> getCase3D2(ReportingFilter filter, EntityManager entityManager) {
        List<DurchlaufzeitPart> result = new ArrayList<>();

        result.addAll(getCurrentlyFachteamUnstimmigAndNewVersion(filter, entityManager));
        result.addAll(getCurrentlyTteamUnstimmigAndNewVersion(filter, entityManager));
        result.addAll(getCurrentlyVereinbarungUnstimmigAndNewVersion(filter, entityManager));
        result.addAll(getFachteamUnstimmigIntervallsAndNewVersion(filter, entityManager));
        result.addAll(getTteamUnstimmigIntervallsAndNewVersion(filter, entityManager));
        result.addAll(getVereinbarungUnstimmigIntervallsAndNewVersion(filter, entityManager));

        return result;
    }

    private static Collection<DurchlaufzeitPart> getCase2CurrentlyGemeldet(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "m.id, r.entryDate" + FROM_MELDUNG_REPORTING + MELDUNG_JOINS +
                "WHERE r.gemeldet.exit IS NULL " +
                "AND r.gemeldet.entry < :endDate ");

        return getMeldungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCase2ExistingAnforderungZugeordnet(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "m.id, r.entryDate, r.existingAnforderungZugeordnet.entry" + FROM_MELDUNG_REPORTING + MELDUNG_JOINS +
                "WHERE r.gemeldet.exit < :endDate " +
                "AND r.existingAnforderungZugeordnet.entry < :endDate ");

        return getMeldungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCurrentlyFachteamUnstimmigAndNotNewVersion(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.fachteamUnstimmig.entry" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.fachteamUnstimmig.exit IS NULL " +
                "AND r.fachteamUnstimmig.entry < :endDate " +
                AND_NOT_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCurrentlyTteamUnstimmigAndNotNewVersion(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.tteamUnstimmig.entry" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.tteamUnstimmig.exit IS NULL " +
                "AND r.tteamUnstimmig.entry < :endDate " +
                AND_NOT_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCurrentlyVereinbarungUnstimmigAndNotNewVersion(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.vereinbarungUnstimmig.entry" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.vereinbarungUnstimmig.exit IS NULL " +
                "AND r.vereinbarungUnstimmig.entry < :endDate " +
                AND_NOT_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }



    private static Collection<DurchlaufzeitPart> getCurrentlyFachteamUnstimmigAndNewVersion(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.fachteamUnstimmig.entry" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.fachteamUnstimmig.exit IS NULL " +
                "AND r.fachteamUnstimmig.entry < :endDate " +
                AND_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCurrentlyTteamUnstimmigAndNewVersion(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.tteamUnstimmig.entry" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.tteamUnstimmig.exit IS NULL " +
                "AND r.tteamUnstimmig.entry < :endDate " +
                AND_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCurrentlyVereinbarungUnstimmigAndNewVersion(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.vereinbarungUnstimmig.entry" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.vereinbarungUnstimmig.exit IS NULL " +
                "AND r.vereinbarungUnstimmig.entry < :endDate " +
                AND_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getFachteamUnstimmigIntervallsAndNotNewVersion(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.fachteamUnstimmig.entry, r.fachteamUnstimmig.exit" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.fachteamUnstimmig.exit < :endDate " +
                "AND r.fachteamUnstimmig.entry < :endDate " +
                AND_NOT_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getTteamUnstimmigIntervallsAndNotNewVersion(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.tteamUnstimmig.entry, r.tteamUnstimmig.exit" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.tteamUnstimmig.exit < :endDate " +
                "AND r.tteamUnstimmig.entry < :endDate " +
                AND_NOT_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getVereinbarungUnstimmigIntervallsAndNotNewVersion(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.vereinbarungUnstimmig.entry, r.vereinbarungUnstimmig.exit" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.vereinbarungUnstimmig.exit < :endDate " +
                "AND r.vereinbarungUnstimmig.entry < :endDate " +
                AND_NOT_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getFachteamUnstimmigIntervallsAndNewVersion(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.fachteamUnstimmig.entry, r.fachteamUnstimmig.exit" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.fachteamUnstimmig.exit < :endDate " +
                "AND r.fachteamUnstimmig.entry < :endDate " +
                AND_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getTteamUnstimmigIntervallsAndNewVersion(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.tteamUnstimmig.entry, r.tteamUnstimmig.exit" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.tteamUnstimmig.exit < :endDate " +
                "AND r.tteamUnstimmig.entry < :endDate " +
                AND_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getVereinbarungUnstimmigIntervallsAndNewVersion(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.vereinbarungUnstimmig.entry, r.vereinbarungUnstimmig.exit" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.vereinbarungUnstimmig.exit < :endDate " +
                "AND r.vereinbarungUnstimmig.entry < :endDate " +
                AND_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCase1D1CurrentlyInArbeit(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.entryDate" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.inArbeit.exit IS NULL " +
                "AND r.inArbeit.entry < :endDate " +
                AND_NOT_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCase1D1InGemeldetToAbgestimmt(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.entryDate, r.abgestimmt.entry" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.inArbeit.entry < :endDate " +
                "AND r.inArbeit.exit < :endDate " +
                "AND r.abgestimmt.entry < :endDate " +
                AND_NOT_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCase1D1InGemeldetToUnstimmig(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.entryDate, r.fachteamUnstimmig.entry" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.inArbeit.entry < :endDate " +
                "AND r.inArbeit.exit < :endDate " +
                "AND r.fachteamUnstimmig.entry < :endDate " +
                AND_NOT_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCase1D1InArbeitToGeloescht(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.entryDate, r.fachteamGeloescht.entry" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.fachteamGeloescht.entry < :endDate " +
                "AND r.inArbeit.exit < :endDate " +
                AND_NOT_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCase3D1CurrentlyInArbeit(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.inArbeit.entry" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.inArbeit.exit IS NULL " +
                "AND r.inArbeit.entry < :endDate " +
                AND_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCase3D1InInArbeitToAbgestimmt(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.inArbeit.entry, r.abgestimmt.entry" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.inArbeit.entry < :endDate " +
                "AND r.inArbeit.exit < :endDate " +
                AND_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCase3D1InInArbeitToUnstimmig(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.inArbeit.entry, r.fachteamUnstimmig.entry" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.inArbeit.entry < :endDate " +
                "AND r.inArbeit.exit < :endDate " +
                "AND r.fachteamUnstimmig.entry < :endDate " +
                AND_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getCase3D1InArbeitToGeloescht(ReportingFilter filter, EntityManager entityManager) {
        QueryPart queryPart = new QueryPartDTO(SELECT + "a.id, r.inArbeit.entry, r.fachteamGeloescht.entry" + FROM_ANFORDERUNG_REPORTING + ANOFRDERUNG_JOINS +
                "WHERE r.fachteamGeloescht.entry < :endDate " +
                "AND r.inArbeit.exit < :endDate " +
                AND_NEW_VERSION);

        return getAnforderungDurchlaufzeitParts(filter, entityManager, queryPart);
    }

    private static Collection<DurchlaufzeitPart> getMeldungDurchlaufzeitParts(ReportingFilter filter, EntityManager entityManager, QueryPart queryPart) {
        final List<DurchlaufzeitPart> result = getDurchlaufzeitParts(filter, entityManager, queryPart, Type.MELDUNG);
        result.forEach(DurchlaufzeitPart::setMeldung);
        return result;
    }

    private static Collection<DurchlaufzeitPart> getAnforderungDurchlaufzeitParts(ReportingFilter filter, EntityManager entityManager, QueryPart queryPart) {
        final List<DurchlaufzeitPart> result = getDurchlaufzeitParts(filter, entityManager, queryPart, Type.ANFORDERUNG);
        result.forEach(DurchlaufzeitPart::setAnforderung);
        return result;
    }

    private static List<DurchlaufzeitPart> getDurchlaufzeitParts(ReportingFilter filter, EntityManager entityManager, QueryPart queryPart, Type type) {
        appendFilter(filter, queryPart, type);

        QueryFactory<DurchlaufzeitPart> queryFactory = new QueryFactory<>();
        final TypedQuery<DurchlaufzeitPart> query = queryFactory.buildTypedQuery(queryPart, entityManager, DurchlaufzeitPart.class);
        return query.getResultList();
    }

    private static void appendFilter(ReportingFilter filter, QueryPart queryPart, Type type) {
        Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        // is only used as fallback if filter is not correctly implemented e.g. call from dashboard
        if (Objects.isNull(endDate)) {
            endDate = new Date();
        }

        queryPart.put("endDate", endDate);

        KeyFigureQueryAnforderungStatusFilter.append(filter, queryPart, type);
        KeyFigureQueryMeldungStatusFilter.append(filter, queryPart, type);

        if (type == Type.ANFORDERUNG) {
            KeyFigureFilterUtils.appendAnforderungFilterWithDateFilter(filter, queryPart);
        }

        if (type == Type.MELDUNG) {
            KeyFigureFilterUtils.appendMeldungFilterWithDateFilter(filter, queryPart);
        }
    }
}
