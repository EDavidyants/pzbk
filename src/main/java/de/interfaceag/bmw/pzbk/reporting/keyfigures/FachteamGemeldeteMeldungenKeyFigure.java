package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachteamGemeldeteMeldungenKeyFigure extends AbstractKeyFigure {

    public FachteamGemeldeteMeldungenKeyFigure(Collection<Long> meldungIds) {
        super(meldungIds.size(), IdTypeTupleFactory.getIdTypeTuplesForMeldung(meldungIds), KeyType.FACHTEAM_MELDUNG_GEMELDET, 0L);
    }
    
    public FachteamGemeldeteMeldungenKeyFigure(Collection<Long> meldungIds, Long runTime) {
        super(meldungIds.size(), IdTypeTupleFactory.getIdTypeTuplesForMeldung(meldungIds), KeyType.FACHTEAM_MELDUNG_GEMELDET, runTime);
    }

}
