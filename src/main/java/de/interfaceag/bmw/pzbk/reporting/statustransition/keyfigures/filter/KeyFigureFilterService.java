package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTupleCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTupleFactory;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;

@Named
@Stateless
public class KeyFigureFilterService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(KeyFigureFilterService.class);

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public IdTypeTupleCollection getFilteredIdTypeTuples(ReportingFilter filter) {

        final IdTypeTupleCollection filteredAnforderungIds = getFilteredAnforderungIds(filter);
        final IdTypeTupleCollection filteredMeldungIds = getFilteredMeldungIds(filter);

        IdTypeTupleCollection result = new IdTypeTupleCollection();
        result.addAll(filteredAnforderungIds);
        result.addAll(filteredMeldungIds);

        return result;
    }

    private IdTypeTupleCollection getFilteredAnforderungIds(ReportingFilter filter) {

        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT(a.id) ");
        queryPart.append(" FROM Anforderung a ")
                .append(" LEFT JOIN a.umsetzer u ")
                .append(" LEFT JOIN a.festgestelltIn f ")
                .append(" WHERE 1=1 ");

        KeyFigureFilterUtils.appendAnforderungFilterWithoutDateFilter(filter, queryPart);

        LOG.debug("getFilteredAnforderungIds Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        final List<Long> anforderungIds = query.getResultList();

        return IdTypeTupleFactory.getIdTypeTuplesForAnforderung(anforderungIds);
    }

    private IdTypeTupleCollection getFilteredMeldungIds(ReportingFilter filter) {

        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT(m.id) ");
        queryPart.append(" FROM Meldung m ")
                .append(" LEFT JOIN m.anforderung a ")
                .append(" LEFT JOIN a.umsetzer u ")
                .append(" LEFT JOIN a.festgestelltIn f ")
                .append(" WHERE 1=1 ");

        KeyFigureFilterUtils.appendMeldungFilterWithoutDateFilter(filter, queryPart);

        LOG.debug("getFilteredMeldungIds Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        final List<Long> meldungIds = query.getResultList();

        return IdTypeTupleFactory.getIdTypeTuplesForMeldung(meldungIds);

    }


}
