package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static de.interfaceag.bmw.pzbk.reporting.statusabgleich.StatusCombinationBuilder.buildCombinations;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class StatusabgleichKennzahlen implements Serializable {

    private final Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> matrixValues;

    public StatusabgleichKennzahlen(Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> matrixValues) {
        this.matrixValues = matrixValues;
    }

    private StatusabgleichKennzahlen() {
        this.matrixValues = Collections.emptyMap();
    }

    public static StatusabgleichKennzahlen empty() {
        return new StatusabgleichKennzahlen();
    }

    public Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> getMatrixValues() {
        return matrixValues;
    }

    public Long getDerivatAnforderungModulStatusValue(Collection<DerivatAnforderungModulStatus> statusCollection) {
        return statusCollection.stream().map(this::getDerivatAnforderungModulStatusValue)
                .mapToLong(Long::longValue).sum();
    }

    public Long getDerivatAnforderungModulStatusValue(DerivatAnforderungModulStatus status) {
        return matrixValues.entrySet().stream()
                .filter(entry -> entry.getKey().getX().equals(status))
                .map(Map.Entry::getValue)
                .filter(Objects::nonNull)
                .mapToLong(Long::longValue).sum();
    }

    public Long getZakStatusValue(Collection<ZakStatus> statusCollection) {
        return statusCollection.stream().map(this::getZakStatusValue)
                .mapToLong(Long::longValue).sum();
    }

    public Long getZakStatusValue(ZakStatus status) {
        return matrixValues.entrySet().stream()
                .filter(entry -> entry.getKey().getY().equals(status))
                .map(Map.Entry::getValue).mapToLong(Long::longValue).sum();
    }

    public Long getValue(Collection<DerivatAnforderungModulStatus> derivatAnforderungModulStatus, Collection<ZakStatus> zakStatus) {
        Set<Tuple<DerivatAnforderungModulStatus, ZakStatus>> combinations = buildCombinations(derivatAnforderungModulStatus, zakStatus);
        return combinations.stream()
                .map(combination -> getValue(combination.getX(), combination.getY()))
                .mapToLong(Long::longValue).sum();
    }

    public Long getValue(DerivatAnforderungModulStatus derivatAnforderungModulStatus, ZakStatus zakStatus) {
        Tuple<DerivatAnforderungModulStatus, ZakStatus> statusTuple = new GenericTuple<>(derivatAnforderungModulStatus, zakStatus);
        Long value = matrixValues.get(statusTuple);
        return value != null ? value : 0;
    }

}
