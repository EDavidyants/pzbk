package de.interfaceag.bmw.pzbk.reporting.keyfigures;

public class TteamDurchlaufzeitKeyFigure extends AbstractDurchlaufzeitKeyFigure {

    public TteamDurchlaufzeitKeyFigure(int value) {
        super(value, KeyType.DURCHLAUFZEIT_TTEAM);
    }
}
