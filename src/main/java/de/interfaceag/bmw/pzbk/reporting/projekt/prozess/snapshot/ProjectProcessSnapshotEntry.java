package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 *
 * @author sl
 */
@Entity
public class ProjectProcessSnapshotEntry implements Serializable {

    @Id
    @SequenceGenerator(name = "projectprocesssnapshotentry_id_seq",
            sequenceName = "projectprocesssnapshotentry_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "projectprocesssnapshotentry_id_seq")
    private Long id;

    @Column(nullable = false)
    private int keyFigure;

    @Column(nullable = false)
    private int value;

    public ProjectProcessSnapshotEntry() {
    }

    public ProjectProcessSnapshotEntry(ProjectReportingProcessKeyFigure keyFigure, int value) {
        this.keyFigure = keyFigure.getId();
        this.value = value;
    }

    @Override
    public String toString() {
        return "ProjectProcessSnapshotEntry{" +
                "id=" + id +
                ", keyFigure=" + keyFigure +
                ", value=" + value +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProjectReportingProcessKeyFigure getKeyFigure() {
        return ProjectReportingProcessKeyFigure.getById(keyFigure).orElse(null);
    }

    public void setKeyFigure(ProjectReportingProcessKeyFigure keyFigure) {
        this.keyFigure = keyFigure.getId();
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
