package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;

/**
 *
 * @author lomu
 */
public class DrilldownTableAnforderungDTO {

    private final Long anforderungId;
    private final String fachId;
    private final Integer version;
    private final String modulName;
    private final String zakId;
    private final String beschreibung;
    private final String derivatAnforderungModulStatus;
    private final String zakStatus;

    public DrilldownTableAnforderungDTO(Long anforderungId, String fachId, Integer version, String modulName, String zakId, String beschreibung, Integer derivatAnforderungModulStatus, ZakStatus zakStatus) {
        this.anforderungId = anforderungId;
        this.fachId = fachId;
        this.version = version;
        this.modulName = modulName;
        this.zakId = zakId;
        this.beschreibung = beschreibung;
        this.derivatAnforderungModulStatus = DerivatAnforderungModulStatus.getStatusById(derivatAnforderungModulStatus).getStatusBezeichnung();
        this.zakStatus = zakStatus.getStatusBezeichnung();
    }

    public Long getAnforderungId() {
        return anforderungId;
    }

    public String getFachId() {
        return fachId;
    }

    public String getFachIdAndVersion() {
        return new StringBuilder(this.getFachId()).append(" | V").append(this.getVersion().toString()).toString();
    }

    public String getShortBeschreibung() {
        if (getBeschreibung().length() > 150) {
            return getBeschreibung().substring(0, 150) + "...";
        }

        return getBeschreibung();
    }

    public Integer getVersion() {
        return version;
    }

    public String getModulName() {
        return modulName;
    }

    public String getZakId() {
        return zakId;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public String getDerivatAnforderungModulStatus() {
        return derivatAnforderungModulStatus;
    }

    public String getZakStatus() {
        return zakStatus;
    }

}
