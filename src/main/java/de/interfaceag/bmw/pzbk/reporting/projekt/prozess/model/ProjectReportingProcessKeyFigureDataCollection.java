package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

/**
 *
 * @author sl
 */
public interface ProjectReportingProcessKeyFigureDataCollection extends Serializable {

    Long getValueForKeyFigure(ProjectReportingProcessKeyFigure keyFigure);

    Long getValueForKeyFigureId(Integer keyFigureId);

    boolean isAmpelGreenForKeyFigureId(Integer keyFigureId);

    Optional<ProjectReportingProcessKeyFigureData> getDataForKeyFigure(ProjectReportingProcessKeyFigure keyFigure);

    Date getDate();

}
