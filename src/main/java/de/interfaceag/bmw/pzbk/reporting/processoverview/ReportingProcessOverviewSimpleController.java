package de.interfaceag.bmw.pzbk.reporting.processoverview;

import de.interfaceag.bmw.pzbk.entities.InfoText;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterController;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterViewPermission;
import de.interfaceag.bmw.pzbk.reporting.ReportingNavbarController;
import de.interfaceag.bmw.pzbk.reporting.ReportingPermissionController;
import de.interfaceag.bmw.pzbk.reporting.ReportingTooltipController;
import de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht.Anforderungsuebersicht;
import de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht.AnforderungsuebersichtDialogMethods;
import de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht.AnforderungsuebersichtService;
import de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht.AnforderungsuebersichtStringComparator;
import de.interfaceag.bmw.pzbk.reporting.excel.ReportingExcelDataDto;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.services.InfoTextService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.utils.DateUtils;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author fn
 */
@ViewScoped
@Named
public class ReportingProcessOverviewSimpleController implements Serializable, ReportingFilterController,
        ReportingNavbarController, ReportingTooltipController, AnforderungsuebersichtDialogMethods, ReportingPermissionController<ReportingProcessOverviewViewPermission> {

    private static final Logger LOG = LoggerFactory.getLogger(ReportingProcessOverviewController.class.getName());

    @Inject
    private Session session;

    @Inject
    private LocalizationService localizationService;

    @Inject
    private ProcessOverviewViewFacade facade;

    @Inject
    private InfoTextService infoTextService;

    @Inject
    private ReportingProcessOverviewViewPermission viewPermission;
    @Inject
    private ReportingFilterViewPermission filterViewPermission;

    private ProcessOverviewViewData viewData;

    private String infoText;

    @PostConstruct
    public void init() {
        LOG.debug("Begin Reporting Processoverview Simple init");
        session.setLocationForView();
        viewData = facade.getSimpleProcessOverviewViewData(UrlParameterUtils.getUrlParameter());
        filterViewPermission = new ReportingFilterViewPermission(session.getUserPermissions().getRoles());
        LOG.debug("End Reporting Processoverview Simple init");
    }

    @Override
    public String filter() {
        return viewData.getFilter().getUrl();
    }

    @Override
    public String reset() {
        return viewData.getFilter().getResetUrl();
    }

    @Override
    public String toggleTooltips() {
        getFilter().getHideTooltipFilter().toggle();
        return filter();
    }

    @Override
    public ReportingFilter getFilter() {
        return getViewData().getFilter();
    }

    public ProcessOverviewViewData getViewData() {
        return viewData;
    }

    public String getZeitraumTooltip() {
        return getViewData().getProcessOverviewTooltipMap().getTooltip(ProcessOverviewSimpleTooltip.ZEITRAUM);
    }

    @Override
    public ReportingProcessOverviewViewPermission getViewPermission() {
        return viewPermission;
    }

    @Override
    public void downloadExcelExport() {
        facade.downloadExcelExport(viewData);
    }

    @Override
    public ReportingFilterViewPermission getFilterViewPermission() {
        return filterViewPermission;
    }

    @Override
    public void downloadDeveloperExport() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getStringfromProperty(String keyword) {
        return localizationService.getValue(keyword);
    }

    @Override
    public void showTooltip() {
        Map<String, String> requestParameterMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String tooltipIdentifier = requestParameterMap.get("tooltipName"); // identifier in database e.g. ProcessOverviewDetailTooltipFACHTEAM_HEADER
        Optional<InfoText> infoTextByFeldName = infoTextService.getInfoTextByFeldName(tooltipIdentifier);
        if (infoTextByFeldName.isPresent()) {
            infoText = infoTextByFeldName.get().getBeschreibungsText();
        }

        LOG.info(tooltipIdentifier);

    }

    public String getInfoText() {
        return infoText != null ? infoText : "";
    }

    public void showAnforderungsuebersichtForKeyFigure(int keyFigureId) {
        KeyType keyType = KeyType.getKeyTypeById(keyFigureId);
        Collection<ReportingExcelDataDto> excelData = facade.getReportingDataForKeyFigure(keyType, viewData);
        String statusLabel = AnforderungsuebersichtService.getDatumLabel(keyFigureId);
        List<Anforderungsuebersicht> anforderungsuebersichtData = facade.getAnforderungsuebersichtData(excelData);
        viewData.setExcelData(excelData);
        viewData.setSelectedKeyType(keyType);
        viewData.setStatusDatumLabel(statusLabel);
        viewData.setAnforderungsuebersichtData(anforderungsuebersichtData);

        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("dialog:anforderungsuebersichtForm:anforderungsuebersichtDlgContent:anforderungsuebersichtTable");
        dataTable.reset();

        RequestContext.getCurrentInstance().execute("PF('anforderungsuebersichtDialog').show()");

    }

    @Override
    public int sortByFachIdAndVersion(Object fachIdAndVersionOne, Object fachIdAndVersionTwo) {
        if (fachIdAndVersionOne instanceof String && fachIdAndVersionTwo instanceof String) {
            String a1 = (String) fachIdAndVersionOne;
            String a2 = (String) fachIdAndVersionTwo;
            Comparator comparator = new AnforderungsuebersichtStringComparator();
            return comparator.compare(a1, a2);
        }
        return 0;
    }

    @Override
    public void downloadExcelExportForKeyFigure() {
        facade.downloadExcelDataForKeyFigure(viewData.getSelectedKeyType(), viewData.getExcelData());
    }

    @Override
    public String navigateToAnforderungOrMeldungView(String fachId) {
        return (fachId.startsWith("A")) ? "anforderungView" : "meldungView";
    }

    @Override
    public Date getMinDate() {
        return DateUtils.getMinDate();
    }

    @Override
    public Date getMaxDate() {
        return DateUtils.getMaxDate();
    }

}
