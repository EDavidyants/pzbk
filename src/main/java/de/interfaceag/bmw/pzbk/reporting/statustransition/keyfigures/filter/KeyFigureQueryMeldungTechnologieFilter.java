package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.filter.TechnologieFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;

import java.util.Collection;

public final class KeyFigureQueryMeldungTechnologieFilter {

    private KeyFigureQueryMeldungTechnologieFilter() {

    }

    public static void append(ReportingFilter filter, QueryPart queryPart) {
        final TechnologieFilter technologieFilter = filter.getTechnologieFilter();

        if (technologieFilter.isActive()) {
            final Collection<String> technologien = technologieFilter.getSelectedStringValues();

            queryPart.append(" AND m.sensorCoc.ortung IN :technologien ");
            queryPart.put("technologien", technologien);
        }
    }

}
