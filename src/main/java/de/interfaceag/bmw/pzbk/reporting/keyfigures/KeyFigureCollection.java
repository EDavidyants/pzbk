package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.io.Serializable;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class KeyFigureCollection extends AbstractSet<KeyFigure> implements Serializable {

    private final Set<KeyFigure> keyFigures;

    public KeyFigureCollection() {
        this.keyFigures = new HashSet<>();
    }

    public KeyFigureCollection(Collection<KeyFigure> keyFigures) {
        this.keyFigures = new HashSet<>(keyFigures);
    }

    @Override
    public boolean add(KeyFigure keyFigure) {
        return keyFigures.add(keyFigure);
    }

    @Override
    public Iterator<KeyFigure> iterator() {
        return keyFigures.iterator();
    }

    @Override
    public int size() {
        return keyFigures.size();
    }

    public KeyFigure getByKeyType(KeyType keyType) {
        return keyFigures.stream().filter(keyFigure -> keyFigure.getKeyTypeId() == keyType.getId()).findAny().orElse(null);
    }

    public Set<Long> getAllAnforderungIds() {
        return keyFigures.stream().flatMap(keyFigure -> keyFigure.getAnforderungIds().stream()).collect(Collectors.toSet());
    }

    public Set<Long> getAllMeldungIds() {
        return keyFigures.stream().flatMap(keyFigure -> keyFigure.getMeldungIds().stream()).collect(Collectors.toSet());
    }

    public List<KeyFigure> getKeyFiguresAsList() {
        return new ArrayList<>(keyFigures);
    }

    public List<KeyFigure> getDrilldownKeyFigures() {
        return getKeyFiguresAsList()
                .stream()
                .filter(keyFigure -> keyFigure.getKeyType().isRelevantForDrilldownDialog())
                .collect(Collectors.toList());
    }

}
