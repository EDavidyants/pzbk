package de.interfaceag.bmw.pzbk.reporting.permission;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterViewPermission;
import de.interfaceag.bmw.pzbk.reporting.dashboard.ReportingDashboardViewPermission;
import de.interfaceag.bmw.pzbk.reporting.processoverview.ReportingProcessOverviewViewPermission;
import de.interfaceag.bmw.pzbk.session.Session;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Set;

@RequestScoped
public class ReportingViewPermissionFactory implements Serializable {

    @Inject
    private Session session;

    @Produces
    @RequestScoped
    public ReportingDashboardViewPermission getReportingDashboardViewPermission() {
        final Set<Rolle> roles = getUserRoles();
        return new ReportingDashboardViewPermission(roles);
    }

    @Produces
    @RequestScoped
    public ReportingProcessOverviewViewPermission getReportingProcessOverviewViewPermission() {
        final Set<Rolle> roles = getUserRoles();
        return new ReportingProcessOverviewViewPermission(roles);
    }

    @Produces
    @RequestScoped
    public ReportingFilterViewPermission getReportingReportingFilterViewPermission() {
        final Set<Rolle> roles = getUserRoles();
        return new ReportingFilterViewPermission(roles);
    }

    private Set<Rolle> getUserRoles() {
        return session.getUserPermissions().getRoles();
    }

}
