package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class FachteamInputKeyFigure extends AbstractKeyFigure {

    public FachteamInputKeyFigure(Collection<Long> meldungIds, Collection<Long> anforderungIds, Long runTime) {
        super(anforderungIds.size() + meldungIds.size(),
                IdTypeTupleFactory.getIdTypeTuplesForMeldungAndAnforderung(meldungIds, anforderungIds),
                KeyType.FACHTEAM_ENTRY_SUM, runTime);
    }

    public static FachteamInputKeyFigure buildFromKeyFigures(@NotNull KeyFigure fachteamGemeldeteMeldungenKeyFigure,
            @NotNull KeyFigure fachteamAnforderungNewVersionKeyFigure,
            @NotNull KeyFigure fachteamUnstimmigKeyFigure) throws InvalidDataException {

        if (fachteamGemeldeteMeldungenKeyFigure == null
                || fachteamAnforderungNewVersionKeyFigure == null
                || fachteamUnstimmigKeyFigure == null) {
            throw new InvalidDataException("At least one paramter is null!");
        }

        List<Long> anforderungIds = new ArrayList<>(fachteamGemeldeteMeldungenKeyFigure.getAnforderungIds());
        List<Long> meldungIds = new ArrayList<>(fachteamGemeldeteMeldungenKeyFigure.getMeldungIds());

        anforderungIds.addAll(fachteamAnforderungNewVersionKeyFigure.getAnforderungIds());
        anforderungIds.addAll(fachteamUnstimmigKeyFigure.getAnforderungIds());

        meldungIds.addAll(fachteamAnforderungNewVersionKeyFigure.getMeldungIds());
        meldungIds.addAll(fachteamUnstimmigKeyFigure.getMeldungIds());

        return new FachteamInputKeyFigure(meldungIds, anforderungIds, 0L);
    }
}
