package de.interfaceag.bmw.pzbk.reporting.projekt.werk;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ProjectReportDto implements Serializable {

    private final String projectName;
    private final String numberOfAusleitungen;

    private final int numberOfVereinbarungenBestaetigt;
    private final int numberOfVereinbarungenOffen;
    private final int numberOfVereinbarungenNichtWeiterverfolgt;

    private final int firstPhaseBestaetigt;
    private final int firstPhaseOffen;
    private final int firstPhaseNichtWeiterverfolgt;

    private final int secondPhaseBestaetigt;
    private final int secondPhaseOffen;
    private final int secondPhaseNichtWeiterverfolgt;

    private final int thirdPhaseBestaetigt;
    private final int thirdPhaseOffen;
    private final int thirdPhaseNichtWeiterverfolgt;

    private ProjectReportDto(String projectName, String numberOfAusleitungen, int numberOfVereinbarungenBestaetigt, int numberOfVereinbarungenOffen, int numberOfVereinbarungenNichtWeiterverfolgt, int firstPhaseBestaetigt, int firstPhaseOffen, int firstPhaseNichtWeiterverfolgt, int secondPhaseBestaetigt, int secondPhaseOffen, int secondPhaseNichtWeiterverfolgt, int thirdPhaseBestaetigt, int thirdPhaseOffen, int thirdPhaseNichtWeiterverfolgt) {
        this.projectName = projectName;
        this.numberOfAusleitungen = numberOfAusleitungen;
        this.numberOfVereinbarungenBestaetigt = numberOfVereinbarungenBestaetigt;
        this.numberOfVereinbarungenOffen = numberOfVereinbarungenOffen;
        this.numberOfVereinbarungenNichtWeiterverfolgt = numberOfVereinbarungenNichtWeiterverfolgt;
        this.firstPhaseBestaetigt = firstPhaseBestaetigt;
        this.firstPhaseOffen = firstPhaseOffen;
        this.firstPhaseNichtWeiterverfolgt = firstPhaseNichtWeiterverfolgt;
        this.secondPhaseBestaetigt = secondPhaseBestaetigt;
        this.secondPhaseOffen = secondPhaseOffen;
        this.secondPhaseNichtWeiterverfolgt = secondPhaseNichtWeiterverfolgt;
        this.thirdPhaseBestaetigt = thirdPhaseBestaetigt;
        this.thirdPhaseOffen = thirdPhaseOffen;
        this.thirdPhaseNichtWeiterverfolgt = thirdPhaseNichtWeiterverfolgt;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getNumberOfAusleitungen() {
        return numberOfAusleitungen;
    }

    public int getNumberOfVereinbarungenBestaetigt() {
        return numberOfVereinbarungenBestaetigt;
    }

    public int getNumberOfVereinbarungenOffen() {
        return numberOfVereinbarungenOffen;
    }

    public int getNumberOfVereinbarungenNichtWeiterverfolgt() {
        return numberOfVereinbarungenNichtWeiterverfolgt;
    }

    public int getFirstPhaseBestaetigt() {
        return firstPhaseBestaetigt;
    }

    public int getFirstPhaseOffen() {
        return firstPhaseOffen;
    }

    public int getFirstPhaseNichtWeiterverfolgt() {
        return firstPhaseNichtWeiterverfolgt;
    }

    public int getSecondPhaseBestaetigt() {
        return secondPhaseBestaetigt;
    }

    public int getSecondPhaseOffen() {
        return secondPhaseOffen;
    }

    public int getSecondPhaseNichtWeiterverfolgt() {
        return secondPhaseNichtWeiterverfolgt;
    }

    public int getThirdPhaseBestaetigt() {
        return thirdPhaseBestaetigt;
    }

    public int getThirdPhaseOffen() {
        return thirdPhaseOffen;
    }

    public int getThirdPhaseNichtWeiterverfolgt() {
        return thirdPhaseNichtWeiterverfolgt;
    }

    public static Builder withProjectName(String projectName) {
        return new Builder().withProjectName(projectName);
    }

    public static final class Builder {

        private String projectName;
        private String numberOfAusleitungen;

        private int numberOfVereinbarungenBestaetigt;
        private int numberOfVereinbarungenOffen;
        private int numberOfVereinbarungenNichtWeiterverfolgt;

        private int firstPhaseBestaetigt;
        private int firstPhaseOffen;
        private int firstPhaseNichtWeiterverfolgt;

        private int secondPhaseBestaetigt;
        private int secondPhaseOffen;
        private int secondPhaseNichtWeiterverfolgt;

        private int thirdPhaseBestaetigt;
        private int thirdPhaseOffen;
        private int thirdPhaseNichtWeiterverfolgt;

        private Builder() {

        }

        public Builder withProjectName(String projectName) {
            this.projectName = projectName;
            return this;
        }

        public Builder withNumberOfAusleitungen(String numberOfAusleitungen) {
            this.numberOfAusleitungen = numberOfAusleitungen;
            return this;
        }

        public Builder withNumberOfVereinbarungenBestaetigt(int numberOfVereinbarungenBestaetigt) {
            this.numberOfVereinbarungenBestaetigt = numberOfVereinbarungenBestaetigt;
            return this;
        }

        public Builder withNumberOfVereinbarungenOffen(int numberOfVereinbarungenOffen) {
            this.numberOfVereinbarungenOffen = numberOfVereinbarungenOffen;
            return this;
        }

        public Builder withNumberOfVereinbarungenNichtWeiterverfolgt(int numberOfVereinbarungenNichtWeiterverfolgt) {
            this.numberOfVereinbarungenNichtWeiterverfolgt = numberOfVereinbarungenNichtWeiterverfolgt;
            return this;
        }

        public Builder withFirstUmsetzungsbestaetigungPhaseResult(int bestaetigt, int offen, int nichtWeiterverfolgt) {
            this.firstPhaseBestaetigt = bestaetigt;
            this.firstPhaseOffen = offen;
            this.firstPhaseNichtWeiterverfolgt = nichtWeiterverfolgt;
            return this;
        }

        public Builder withSecondUmsetzungsbestaetigungPhaseResult(int bestaetigt, int offen, int nichtWeiterverfolgt) {
            this.secondPhaseBestaetigt = bestaetigt;
            this.secondPhaseOffen = offen;
            this.secondPhaseNichtWeiterverfolgt = nichtWeiterverfolgt;
            return this;
        }

        public Builder withThirdUmsetzungsbestaetigungPhaseResult(int bestaetigt, int offen, int nichtWeiterverfolgt) {
            this.thirdPhaseBestaetigt = bestaetigt;
            this.thirdPhaseOffen = offen;
            this.thirdPhaseNichtWeiterverfolgt = nichtWeiterverfolgt;
            return this;
        }

        public ProjectReportDto get() {
            return new ProjectReportDto(projectName, numberOfAusleitungen, numberOfVereinbarungenBestaetigt, numberOfVereinbarungenOffen, numberOfVereinbarungenNichtWeiterverfolgt, firstPhaseBestaetigt, firstPhaseOffen, firstPhaseNichtWeiterverfolgt, secondPhaseBestaetigt, secondPhaseOffen, secondPhaseNichtWeiterverfolgt, thirdPhaseBestaetigt, thirdPhaseOffen, thirdPhaseNichtWeiterverfolgt);
        }

    }

}
