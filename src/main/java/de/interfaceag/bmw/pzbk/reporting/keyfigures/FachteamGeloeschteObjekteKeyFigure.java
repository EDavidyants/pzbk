package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachteamGeloeschteObjekteKeyFigure extends AbstractKeyFigure {

    public FachteamGeloeschteObjekteKeyFigure(Collection<Long> anforderungIds, Collection<Long> meldungIds) {
        super(IdTypeTupleFactory.getIdTypeTuplesForMeldungAndAnforderung(meldungIds, anforderungIds), KeyType.FACHTEAM_GELOESCHT);
    }

    public FachteamGeloeschteObjekteKeyFigure(Collection<Long> anforderungIds, Collection<Long> meldungIds, Long runTime) {
        super(anforderungIds.size() + meldungIds.size(),
                IdTypeTupleFactory.getIdTypeTuplesForMeldungAndAnforderung(meldungIds, anforderungIds),
                KeyType.FACHTEAM_GELOESCHT, runTime);

    }

}
