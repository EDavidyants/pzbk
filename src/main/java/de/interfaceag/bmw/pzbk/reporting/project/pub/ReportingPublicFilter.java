package de.interfaceag.bmw.pzbk.reporting.project.pub;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.filter.Filter;
import de.interfaceag.bmw.pzbk.filter.FilterToUrlConverter;
import de.interfaceag.bmw.pzbk.filter.SelectedDerivatUrlFilter;
import de.interfaceag.bmw.pzbk.filter.SingleIdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SingleSnapshotFilter;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import de.interfaceag.bmw.pzbk.filter.UrlFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class  ReportingPublicFilter implements Serializable {

    private final Page page;

    @Filter
    private final UrlFilter derivatFilter;
    @Filter
    private final SingleIdSearchFilter snapshotFilter;

    public ReportingPublicFilter(UrlParameter urlParameter,
                                 Collection<SnapshotFilter> allSnapshots,
                                 Page page
    ) {
        this.derivatFilter = SelectedDerivatUrlFilter.withParamterName("derivat").withUrlParameter(urlParameter).build();
        this.snapshotFilter = new SingleSnapshotFilter(allSnapshots, urlParameter);
        this.page = page;
    }

    public UrlFilter getDerivatFilter() {
        return derivatFilter;
    }

    public SingleIdSearchFilter getSnapshotFilter() {
        return snapshotFilter;
    }

    public String filter() {
        return page.getUrl() + getUrlParameter();
    }

    public String reset() {
        String derivatParameter = derivatFilter.getIndependentParameter();
        return page.getUrl() + "?faces-redirect=true&" + derivatParameter;
    }

    private String getUrlParameter() {
        FilterToUrlConverter urlConverter = FilterToUrlConverter.forClass(this);
        return urlConverter.getUrl();
    }

    public String getUrlForPage(Page page) {
        return page.getUrl() + getUrlParameter();
    }

}
