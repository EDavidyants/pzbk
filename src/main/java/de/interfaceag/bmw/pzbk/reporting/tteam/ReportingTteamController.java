package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterController;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterViewPermission;
import de.interfaceag.bmw.pzbk.reporting.ReportingNavbarController;
import de.interfaceag.bmw.pzbk.reporting.ReportingPermissionController;
import de.interfaceag.bmw.pzbk.reporting.dashboard.ReportingDashboardViewPermission;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ViewScoped
@Named
public class ReportingTteamController implements Serializable, ReportingFilterController,
        ReportingNavbarController, ReportingPermissionController<ReportingDashboardViewPermission> {

    private static final Logger LOG = LoggerFactory.getLogger(ReportingTteamController.class.getName());

    @Inject
    private Session session;

    @Inject
    private ReportingTteamViewData viewData;

    @Inject
    private ReportingDashboardViewPermission viewPermission;

    private ReportingFilterViewPermission filterViewPermission;

    @PostConstruct
    public void init() {
        LOG.debug("Begin Reporting T-Team init");
        session.setLocationForView();
        filterViewPermission = new ReportingFilterViewPermission(session.getUserPermissions().getRoles());
        LOG.debug("End Reporting T-Team init");
    }

    @Override
    public String filter() {
        return viewData.getFilter().getUrl();
    }

    @Override
    public String reset() {
        return viewData.getFilter().getResetUrl();
    }

    @Override
    public String toggleTooltips() {
        getFilter().getHideTooltipFilter().toggle();
        return filter();
    }

    @Override
    public ReportingFilter getFilter() {
        return getViewData().getFilter();
    }

    public ReportingTteamViewData getViewData() {
        return viewData;
    }

    @Override
    public ReportingDashboardViewPermission getViewPermission() {
        return viewPermission;
    }

    @Override
    public void downloadExcelExport() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ReportingFilterViewPermission getFilterViewPermission() {
        return filterViewPermission;
    }

    @Override
    public void downloadDeveloperExport() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Date getMinDate() {
        return DateUtils.getMinDate();
    }

    @Override
    public Date getMaxDate() {
        return DateUtils.getMaxDate();
    }

}
