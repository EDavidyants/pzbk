package de.interfaceag.bmw.pzbk.reporting.projekt.werk;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ViewScoped
@Named
public class ProjectWerkViewController implements Serializable, ProjectWerkFilterController {

    @Inject
    private ProjectWerkViewFacade facade;

    private ProjectWerkViewData viewData;

    @PostConstruct
    public void init() {
        initViewData();
    }

    private void initViewData() {
        viewData = facade.getViewData();
    }

    public ProjectWerkViewData getViewData() {
        return viewData;
    }

    public ProjectWerkFilter getFilter() {
        return getViewData().getFilter();
    }

    @Override
    public String filter() {
        return getFilter().filter();
    }

    @Override
    public String reset() {
        return getFilter().reset();
    }

}
