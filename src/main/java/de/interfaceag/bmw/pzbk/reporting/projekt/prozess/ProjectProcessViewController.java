package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.drilldown.ProjectReportingDrilldownDto;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ViewScoped
@Named
public class ProjectProcessViewController implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(ProjectProcessViewController.class);

    @Inject
    private ProjectProcessViewFacade facade;

    @Inject
    private ProjectProcessViewData viewData;

    @Inject
    private ProjectProcessDrilldown projectProcessDrilldown;

    @PostConstruct
    public void init() {
        showGrowlMessageIfValidationFailed();
    }

    private void showGrowlMessageIfValidationFailed() {
        if (getViewData().isValidationError()) {
            String message = getViewData().getValidationMessage();
            String header = getViewData().getValidationHeaderMessage();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                    header,
                    message));
        }
    }

    public ProjectProcessDrilldown getProjectProcessDrilldown() {
        return projectProcessDrilldown;
    }

    public void openDialog(int figure) {
        Optional<ProjectReportingProcessKeyFigure> keyFigure = ProjectReportingProcessKeyFigure.getById(figure);
        if (keyFigure.isPresent()) {
            projectProcessDrilldown.openDialog(keyFigure.get());
        }
    }

    public List<ProjectReportingDrilldownDto> getDrilldowndataForKeyfigure(int figure) {
        Optional<ProjectReportingProcessKeyFigure> keyFigure = ProjectReportingProcessKeyFigure.getById(figure);
        if (keyFigure.isPresent()) {
            return projectProcessDrilldown.getDrilldowndataForKeyfigure(keyFigure.get());
        }

        return Collections.EMPTY_LIST;
    }

    public List<Integer> getAllAvailableDrilldownKeyFigures() {
        List<ProjectReportingProcessKeyFigure> figures = projectProcessDrilldown.getAllAvailableDrilldownKeyFigures();
        List<Integer> result = new ArrayList<>();
        figures.forEach((figure) -> {
            result.add(figure.getId());
        });
        return result;
    }

    public ProjectProcessViewData getViewData() {
        return viewData;
    }

    public ProjectProzessFilterController getFilterController() {
        return getViewData().getFilter();
    }

    public boolean isDerivatPresent() {
        return getViewData().getSelectedDerivat().isPresent();
    }

    public String getDerivatName() {
        return getViewData().getSelectedDerivat().map(Derivat::getName).orElse("");
    }

    public String getDerivatStatus() {
        return getViewData().getSelectedDerivatStatus().orElse("");
    }

    public void downloadExcelExport() {
        Optional<Derivat> selectedDerivat = getViewData().getSelectedDerivat();
        selectedDerivat.ifPresent(derivat -> facade.downloadExcelExport(derivat, getViewData().getFilter()));
    }

    public String createNewSnapshot() {
        final Optional<Derivat> selectedDerivat = viewData.getSelectedDerivat();
        if (selectedDerivat.isPresent()) {
            final Derivat derivat = selectedDerivat.get();
            facade.createNewSnapshot(derivat);
            return getViewData().getFilter().filter();
        } else {
            LOG.warn("No derivat selected for snapshot generation! No snapshot will be created.");
            return "";
        }
    }

}
