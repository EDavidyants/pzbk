package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.meldung.ReportingMeldungStatusTransitionEntryDateService;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

@Dependent
public class ReportingStatusTransitionAnforderungEntyDateService implements Serializable {

    @Inject
    private ReportingMeldungStatusTransitionEntryDateService reportingMeldungStatusTransitionEntryDateService;

    public Date getEntryDateForAnforderung(Anforderung anforderung) {
        final Set<Meldung> meldungen = anforderung.getMeldungen();

        if (hasNoMeldung(meldungen)) {
            throw new IllegalArgumentException("No entry date could be computed for anforderung " + anforderung + " as is has no meldung.");
        } else {
            return computeEntryDateForMeldungenAndValidateResult(anforderung, meldungen);
        }
    }

    private Date computeEntryDateForMeldungenAndValidateResult(Anforderung anforderung, Set<Meldung> meldungen) {
        if (isNewVersion(anforderung)) {
            return anforderung.getErstellungsdatum();
        } else {
            final Date entryDate = computeEntryDateForMeldungen(meldungen);
            return validateResult(anforderung, entryDate);
        }
    }

    private boolean isNewVersion(Anforderung anforderung) {
        return anforderung.getVersion() > 1;
    }

    private Date validateResult(Anforderung anforderung, Date entryDate) {
        if (entryDate == null) {
            throw new IllegalArgumentException("No entry date could be computed for anforderung " + anforderung + ". No meldung of the anforderung had an entry date.");
        } else {
            return entryDate;
        }
    }

    private Date computeEntryDateForMeldungen(Set<Meldung> meldungen) {
        Date entryDateForAnforderung = null;
        for (Meldung meldung : meldungen) {
            entryDateForAnforderung = updateEntryDateForMeldungIfNecessary(entryDateForAnforderung, meldung);
        }
        return entryDateForAnforderung;
    }

    private Date updateEntryDateForMeldungIfNecessary(Date entryDateForAnforderung, Meldung meldung) {
        final Optional<Date> entryDateForMeldung = reportingMeldungStatusTransitionEntryDateService.getEntryDateForMeldung(meldung);
        if (entryDateForMeldung.isPresent()) {
            final Date date = entryDateForMeldung.get();
            if (meldungEntryDateIsBeforeCurrentEntryDate(entryDateForAnforderung, date)) {
                entryDateForAnforderung = date;
            }
        }
        return entryDateForAnforderung;
    }

    private static boolean meldungEntryDateIsBeforeCurrentEntryDate(Date entryDateForAnforderung, Date date) {
        return entryDateForAnforderung == null || date.before(entryDateForAnforderung);
    }

    private static boolean hasNoMeldung(Set<Meldung> meldungen) {
        return meldungen == null || meldungen.isEmpty();
    }

}
