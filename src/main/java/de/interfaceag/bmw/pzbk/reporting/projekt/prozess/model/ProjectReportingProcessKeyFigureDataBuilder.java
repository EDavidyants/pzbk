package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model;

public final class ProjectReportingProcessKeyFigureDataBuilder {

    private ProjectReportingProcessKeyFigure reportingProcessKeyFigure;
    private Long value;
    private boolean isAmpelGreen;

    private ProjectReportingProcessKeyFigureDataBuilder() {
    }

    public static ProjectReportingProcessKeyFigureDataBuilder withKeyFigure(ProjectReportingProcessKeyFigure reportingProcessKeyFigure) {
        return new ProjectReportingProcessKeyFigureDataBuilder().withReportingProcessKeyFigure(reportingProcessKeyFigure);
    }

    private ProjectReportingProcessKeyFigureDataBuilder withReportingProcessKeyFigure(ProjectReportingProcessKeyFigure reportingProcessKeyFigure) {
        this.reportingProcessKeyFigure = reportingProcessKeyFigure;
        return this;
    }

    public ProjectReportingProcessKeyFigureDataBuilder withValue(Long value) {
        this.value = value;
        return this;
    }

    public ProjectReportingProcessKeyFigureDataBuilder withAmpel(boolean isAmpelGreen) {
        this.isAmpelGreen = isAmpelGreen;
        return this;
    }

    public ProjectReportingProcessKeyFigureData build() {
        return new ProjectReportingProcessKeyFigureDataDto(reportingProcessKeyFigure, value, isAmpelGreen);
    }

}
