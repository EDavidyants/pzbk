package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachteamAnforderungNewVersionKeyFigure extends AbstractKeyFigure {

    public FachteamAnforderungNewVersionKeyFigure(Collection<Long> ids, Long runTime) {
        super(ids.size(), IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.FACHTEAM_NEW_VERSION_ANFORDERUNG, runTime);
    }

}
