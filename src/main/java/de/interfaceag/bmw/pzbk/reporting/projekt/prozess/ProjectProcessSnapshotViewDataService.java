package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot.ProjectProcessSnapshot;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot.ProjectProcessSnapshotConverter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot.ProjectProcessSnapshotEntry;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot.ProjectProcessSnapshotReadService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

@Named
@Stateless
public class ProjectProcessSnapshotViewDataService implements Serializable {

    @Inject
    private ProjectProcessSnapshotReadService snapshotReadService;

    public Optional<ProjectReportingProcessKeyFigureDataCollection> getKeyFiguresForLatestSnapshot(Derivat derivat) {
        final Optional<ProjectProcessSnapshot> latestSnapshotForDerivat = snapshotReadService.getLatestSnapshotForDerivat(derivat);
        return latestSnapshotForDerivat.flatMap(ProjectProcessSnapshotViewDataService::getKeyFigureDataCollectionForSnapshot);
    }

    public Optional<ProjectReportingProcessKeyFigureDataCollection> getKeyFiguresForSnapshot(UrlParameter urlParameter) {

        final Optional<Long> snapshotId = getSnapshotId(urlParameter);

        if (snapshotId.isPresent()) {
            return getKeyFiguresForSnapshotId(snapshotId.get());
        } else {
            return Optional.empty();
        }
    }

    private Optional<ProjectReportingProcessKeyFigureDataCollection> getKeyFiguresForSnapshotId(Long snapshotId) {
        final Optional<ProjectProcessSnapshot> snapshot = snapshotReadService.getById(snapshotId);
        return snapshot.flatMap(ProjectProcessSnapshotViewDataService::getKeyFigureDataCollectionForSnapshot);
    }

    private static Optional<ProjectReportingProcessKeyFigureDataCollection> getKeyFigureDataCollectionForSnapshot(ProjectProcessSnapshot snapshot) {
        final Collection<ProjectProcessSnapshotEntry> entries = snapshot.getEntries();
        final Date date = snapshot.getDatum();
        final ProjectReportingProcessKeyFigureDataCollection keyFigureDataCollection = ProjectProcessSnapshotConverter.toKeyFigureDataCollection(entries, date);
        return Optional.of(keyFigureDataCollection);
    }


    private Optional<Long> getSnapshotId(UrlParameter urlParameter) {
        Optional<String> snapshotParameter = urlParameter.getValue("snapshot");

        if (snapshotParameter.isPresent() && RegexUtils.matchesId(snapshotParameter.get())) {
            Long snapshotId = Long.parseLong(snapshotParameter.get());
            return Optional.of(snapshotId);
        } else {
            return Optional.empty();
        }
    }

}
