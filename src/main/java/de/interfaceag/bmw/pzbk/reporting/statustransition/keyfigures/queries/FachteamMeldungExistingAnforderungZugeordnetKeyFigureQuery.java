package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamMeldungExistingAnforderungKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Date;

final class FachteamMeldungExistingAnforderungZugeordnetKeyFigureQuery {

    private static final Logger LOG = LoggerFactory.getLogger(FachteamMeldungExistingAnforderungZugeordnetKeyFigureQuery.class.getName());

    private FachteamMeldungExistingAnforderungZugeordnetKeyFigureQuery() {
    }

    static FachteamMeldungExistingAnforderungKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {
        Collection<Long> value = getMeldungIdsWhichWhereAddedToExistingAnforderung(filter, entityManager);
        return new FachteamMeldungExistingAnforderungKeyFigure(value);
    }

    private static Collection<Long> getMeldungIdsWhichWhereAddedToExistingAnforderung(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT(m.id) ");
        queryPart.append(" FROM ReportingMeldungStatusTransition r ")
                .append(" INNER JOIN r.meldung m ")
                .append(" LEFT JOIN m.anforderung a ")
                .append(" WHERE r.existingAnforderungZugeordnet.entry < :endDate ")
                .append(" AND r.existingAnforderungZugeordnet.exit IS NULL ");
        queryPart.put("endDate", endDate);

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getMeldungIdsWhichWhereAddedToExistingAnforderung Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

}
