package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot.SnapshotFilterConverter;

import javax.enterprise.context.Dependent;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author sl
 */
@Named
@Dependent
public class ProjectProcessSnapshotDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public List<SnapshotFilter> getAllDatesForDerivat(Derivat derivat) {
        if (derivat == null) {
            throw new IllegalArgumentException("derivat is null");
        }

        Query query = entityManager.createNamedQuery(ProjectProcessSnapshot.ALL_DATES_FOR_DERIVAT);
        query.setParameter("derivat", derivat);

        List<Object[]> result = query.getResultList();
        List<SnapshotFilter> snapshots = SnapshotFilterConverter.fromObjectArray(result);

        return snapshots;
    }

    public List<ProjectProcessSnapshot> getAllForDerivat(Derivat derivat) {
        if (derivat == null) {
            throw new IllegalArgumentException("derivat is null");
        }

        Query query = entityManager.createNamedQuery(ProjectProcessSnapshot.ALL_FOR_DERIVAT, ProjectProcessSnapshot.class);
        query.setParameter("derivat", derivat);

        return query.getResultList();
    }

    public Optional<ProjectProcessSnapshot> getById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("id is null");
        }

        ProjectProcessSnapshot result = entityManager.find(ProjectProcessSnapshot.class, id);

        return Optional.ofNullable(result);
    }

    public Optional<ProjectProcessSnapshot> getForDerivatAndDate(Derivat derivat, Date date) {
        if (derivat == null) {
            throw new IllegalArgumentException("derivat is null");
        }

        if (date == null) {
            throw new IllegalArgumentException("date is null");
        }

        Query query = entityManager.createNamedQuery(ProjectProcessSnapshot.FOR_DERIVAT_AND_DATE, ProjectProcessSnapshot.class);
        query.setParameter("derivat", derivat);
        query.setParameter("datum", date);

        List<ProjectProcessSnapshot> result = query.getResultList();

        if (result.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(result.get(0));
        }
    }

    public void save(ProjectProcessSnapshot snapshot) {
        if (snapshot.getId() != null) {
            entityManager.merge(snapshot);
        } else {
            entityManager.persist(snapshot);
        }
        entityManager.flush();
    }

}
