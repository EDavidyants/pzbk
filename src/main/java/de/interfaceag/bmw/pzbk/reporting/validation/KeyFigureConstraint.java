package de.interfaceag.bmw.pzbk.reporting.validation;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface KeyFigureConstraint extends Serializable {

    KeyFigureValidationResult validate();

}
