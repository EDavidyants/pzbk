package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;

@Stateless
public class ReportingStatusTransitionService implements Serializable, AnforderungReportingStatusTransitionAdapter, AnforderungReportingStatusTransitionAdapterForMigration {

    public static final Logger LOG = LoggerFactory.getLogger(ReportingStatusTransitionService.class);

    @Inject
    private ReportingStatusTransitionAnforderungStatusChangeService anforderungStatusChangeService;
    @Inject
    private ReportingStatusTransitionAnforderungProzessbaukastenEntryService anforderungProzessbaukastenEntryService;
    @Inject
    private ReportingStatusTransitionAnforderungProzessbaukastenExitService anforderungProzessbaukastenExitService;
    @Inject
    private ReportingStatusTransitionCreateAnforderungAsNewVersionService anforderungAsNewVersionService;
    @Inject
    private ReportingStatusTransitionCreateAnforderungFromMeldungService createAnforderungFromMeldungService;
    @Inject
    private ReportingStatusTransitionRestoreAnforderungService restoreAnforderungService;
    @Inject
    private ReportingStatusTransitionRemoveAnforderungService removeAnforderungService;

    @Inject
    private Date currentDate;

    @Override
    public ReportingStatusTransition createAnforderungFromMeldung(Anforderung anforderung) {
        return createAnforderungFromMeldung(anforderung, currentDate);
    }

    @Override
    public ReportingStatusTransition createAnforderungFromMeldung(Anforderung anforderung, Date date) {
        try {
            if (anforderung.hasMeldung()) {
                return createAnforderungFromMeldungService.createAnforderungFromMeldung(anforderung, date);
            } else {
                return null;
            }
        } catch (IllegalArgumentException ex) {
            LOG.error("ReportingStatusTransitionService.createAnforderungFromMeldung", ex);
        }
        return null;
    }

    @Override
    public ReportingStatusTransition addAnforderungToProzessbaukasten(Anforderung anforderung, Status currentStatus) {
        try {
            return addAnforderungToProzessbaukasten(anforderung, currentStatus, currentDate);
        } catch (IllegalArgumentException ex) {
            LOG.error("ReportingStatusTransitionService.addAnforderungToProzessbaukasten", ex);
        }
        return null;
    }

    @Override
    public ReportingStatusTransition addAnforderungToProzessbaukasten(Anforderung anforderung, Status currentStatus, Date date) {
        try {
            if (anforderung.hasMeldung()) {
                return anforderungProzessbaukastenEntryService.addAnforderungToProzessbaukasten(anforderung, currentStatus, date);
            } else {
                return null;
            }
        } catch (IllegalArgumentException ex) {
            LOG.error("ReportingStatusTransitionService.addAnforderungToProzessbaukasten", ex);
        }
        return null;
    }

    @Override
    public ReportingStatusTransition removeAnforderungFromProzessbaukasten(Anforderung anforderung, Status currentStatus) {
        try {
            return removeAnforderungFromProzessbaukasten(anforderung, currentStatus, currentDate);
        } catch (IllegalArgumentException ex) {
            LOG.error("ReportingStatusTransitionService.removeAnforderungFromProzessbaukasten", ex);
        }
        return null;
    }

    @Override
    public ReportingStatusTransition removeAnforderungFromProzessbaukasten(Anforderung anforderung, Status currentStatus, Date date) {
        try {
            if (anforderung.hasMeldung()) {
                return anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, currentStatus, date);
            } else {
                return null;
            }
        } catch (IllegalArgumentException ex) {
            LOG.error("ReportingStatusTransitionService.removeAnforderungFromProzessbaukasten", ex);
        }
        return null;
    }

    @Override
    public ReportingStatusTransition changeAnforderungStatus(Anforderung anforderung, Status currentStatus, Status newStatus) {
        return changeAnforderungStatus(anforderung, currentStatus, newStatus, currentDate);
    }

    @Override
    public ReportingStatusTransition changeAnforderungStatus(Anforderung anforderung, Status currentStatus, Status newStatus, Date date) {
        try {
            if (anforderung.hasMeldung()) {
                return anforderungStatusChangeService.changeAnforderungStatus(anforderung, currentStatus, newStatus, date);
            } else {
                return null;
            }
        } catch (IllegalArgumentException ex) {
            LOG.error("ReportingStatusTransitionService.changeAnforderungStatus", ex);
        }
        return null;
    }

    @Override
    public ReportingStatusTransition createAnforderungAsNewVersion(Anforderung anforderung) {
        try {
            return createAnforderungAsNewVersion(anforderung, currentDate);
        } catch (IllegalArgumentException ex) {
            LOG.error("ReportingStatusTransitionService.createAnforderungAsNewVersion", ex);
        }
        return null;
    }

    @Override
    public ReportingStatusTransition createAnforderungAsNewVersion(Anforderung anforderung, Date date) {
        try {
            if (anforderung.hasMeldung()) {
                return anforderungAsNewVersionService.createAnforderungAsNewVersion(anforderung, date);
            } else {
                return null;
            }
        } catch (IllegalArgumentException ex) {
            LOG.error("ReportingStatusTransitionService.createAnforderungAsNewVersion", ex);
        }
        return null;
    }

    @Override
    public ReportingStatusTransition restoreAnforderung(Anforderung anforderung) {
        try {
            return restoreAnforderung(anforderung, currentDate);
        } catch (IllegalArgumentException ex) {
            LOG.error("ReportingStatusTransitionService.restoreAnforderung", ex);
        }
        return null;
    }

    @Override
    public void removeAnforderung(Anforderung anforderung) {
        try {
            removeAnforderungService.removeAnforderungStatusTransitions(anforderung);
        } catch (IllegalArgumentException ex) {
            LOG.error("ReportingStatusTransitionService.removeAnforderung", ex);
        }
    }

    @Override
    public ReportingStatusTransition restoreAnforderung(Anforderung anforderung, Date date) {
        try {
            if (anforderung.hasMeldung()) {
                return restoreAnforderungService.restoreAnforderung(anforderung, date);
            } else {
                return null;
            }
        } catch (IllegalArgumentException ex) {
            LOG.error("ReportingStatusTransitionService.restoreAnforderung", ex);
        }
        return null;
    }
}
