package de.interfaceag.bmw.pzbk.reporting.dashboard;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

public class ReportingDashboardViewPermission implements Serializable {

    private final ViewPermission page;
    private final ViewPermission processOverviewSimple;
    private final ViewPermission processOverviewDetail;
    private final ViewPermission technologieListView;
    private final ViewPermission tteamListView;

    // do not delete. used by @Inject
    public ReportingDashboardViewPermission() {
        page = FalsePermission.get();
        processOverviewSimple = FalsePermission.get();
        processOverviewDetail = FalsePermission.get();
        technologieListView = FalsePermission.get();
        tteamListView = FalsePermission.get();
    }

    public ReportingDashboardViewPermission(Set<Rolle> userRoles) {
        page = getPageViewPermisison(userRoles);
        processOverviewSimple = getProcessOverviewSimpleViewPermission(userRoles);
        processOverviewDetail = getProcessOverviewDetailViewPermission(userRoles);
        technologieListView = getTechnologieListViewPermission(userRoles);
        tteamListView = getTteamListViewPermission(userRoles);
    }

    private ViewPermission getTteamListViewPermission(Set<Rolle> userRoles) {
        return RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .compareTo(userRoles).get();
    }

    private ViewPermission getTechnologieListViewPermission(Set<Rolle> userRoles) {
        return RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .compareTo(userRoles).get();
    }

    private static ViewPermission getPageViewPermisison(Set<Rolle> userRoles) {
        return RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.E_COC)
                .compareTo(userRoles).get();
    }

    private static ViewPermission getProcessOverviewSimpleViewPermission(Set<Rolle> userRoles) {
        return RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.E_COC)
                .compareTo(userRoles).get();
    }

    private static ViewPermission getProcessOverviewDetailViewPermission(Set<Rolle> userRoles) {
        return RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.E_COC)
                .compareTo(userRoles).get();
    }

    public boolean isPage() {
        return page.isRendered();
    }

    public boolean isProcessOverviewSimple() {
        return processOverviewSimple.isRendered();
    }

    public boolean isProcessOverviewDetail() {
        return processOverviewDetail.isRendered();
    }

    public boolean isTechnologieListView() {
        return technologieListView.isRendered();
    }

    public boolean isTteamListView() {
        return tteamListView.isRendered();
    }
}
