package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Dependent
public class ReportingStatusTransitionDao implements ReportingStatusTransitionDatabaseAdapter, Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    @Override
    public void save(ReportingStatusTransition statusTransition) {
        if (statusTransition.getId() != null) {
            entityManager.merge(statusTransition);
        } else {
            entityManager.persist(statusTransition);
        }
        entityManager.flush();
    }

    @Override
    public void remove(ReportingStatusTransition statusTransition) {
        entityManager.remove(statusTransition);
        entityManager.flush();
    }

    @Override
    public List<ReportingStatusTransition> getAllStatustransitionsForAnforderung(Anforderung anforderung) {
        TypedQuery<ReportingStatusTransition> query = entityManager.createNamedQuery(ReportingStatusTransition.LATEST, ReportingStatusTransition.class);
        query.setParameter("anforderungId", anforderung.getId());
        query.setMaxResults(1);
        return query.getResultList();
    }


    @Override
    public Optional<ReportingStatusTransition> getById(Long id) {
        final ReportingStatusTransition statusTransition = entityManager.find(ReportingStatusTransition.class, id);
        return Optional.ofNullable(statusTransition);
    }

    @Override
    public Optional<ReportingStatusTransition> getLatestForAnforderung(Anforderung anforderung) {
        TypedQuery<ReportingStatusTransition> query = entityManager.createNamedQuery(ReportingStatusTransition.LATEST, ReportingStatusTransition.class);
        query.setParameter("anforderungId", anforderung.getId());
        query.setMaxResults(1);
        List<ReportingStatusTransition> result = query.getResultList();
        return result != null && !result.isEmpty() ? Optional.ofNullable(result.get(0)) : Optional.empty();
    }

    @Override
    public boolean isHistoryAlreadyExisting(Anforderung anforderung) {
        return getLatestForAnforderung(anforderung).isPresent();
    }

}
