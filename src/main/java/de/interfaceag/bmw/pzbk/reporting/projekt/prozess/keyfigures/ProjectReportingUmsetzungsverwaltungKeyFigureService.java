package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessAmpelThresholdService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 *
 * @author sl
 */
@Named
@Stateless
public class ProjectReportingUmsetzungsverwaltungKeyFigureService {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingUmsetzungsverwaltungKeyFigureService.class);

    @Inject
    private ProjectReportingUmsetzungsverwaltungKeyFigureDao umsetzungsverwaltungKeyFigureDao;

    @Inject
    private ProjectProcessAmpelThresholdService ampelThresholdService;

    public Map<Integer, ProjectReportingProcessKeyFigureData> getKeyFiguresForUmsetzungsverwaltungVKBG(Derivat derivat, Long vereinbarungVKBGangenommen, ProjectProcessFilter filter) {

        LOG.debug("Compute Umsetzungsverwaltung VKBG keyfigures for derivat {}", derivat);

        if (derivat == null) {
            LOG.error("Derivat is null! Return zero for all values.");
            return Collections.emptyMap();
        }

        Map<Integer, ProjectReportingProcessKeyFigureData> result = getQueryDataForUmsetzungsverwaltungVKBG(derivat, (UmsetzungsBestaetigungStatus status) -> getKeyFigureForUmsetzungsverwaltungVKBGStatus(status), filter);
        Map<Integer, ProjectReportingProcessKeyFigureData> convertToKeyFigureDataForStatusOffen = getQueryDataForUmsetzungsverwaltungStatusOffenVKBG(result, vereinbarungVKBGangenommen);
        result.putAll(convertToKeyFigureDataForStatusOffen);

        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdForUmsetzungsverwaltungVKBG();
        result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBG(result, thresholdForGreenAmpel);

        LOG.debug("Result for Umsetzungsverwaltung VKBG {}", result);

        return result;

    }

    public Map<Integer, ProjectReportingProcessKeyFigureData> getKeyFiguresForUmsetzungsverwaltungVBBG(Derivat derivat, Long vereinbarungVKBGangenommen, ProjectProcessFilter filter) {

        LOG.debug("Compute Umsetzungsverwaltung VBBG keyfigures for derivat {}", derivat);

        if (derivat == null) {
            LOG.error("Derivat is null! Return zero for all values.");
            return Collections.emptyMap();
        }

        Map<Integer, ProjectReportingProcessKeyFigureData> result = getQueryDataForUmsetzungsverwaltungVBBG(derivat, (UmsetzungsBestaetigungStatus status) -> getKeyFigureForUmsetzungsverwaltungVBBGStatus(status), filter);
        Map<Integer, ProjectReportingProcessKeyFigureData> convertToKeyFigureDataForStatusOffen = getQueryDataForUmsetzungsverwaltungStatusOffenVBBG(result, vereinbarungVKBGangenommen);
        result.putAll(convertToKeyFigureDataForStatusOffen);

        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdForUmsetzungsverwaltungVBBG();
        result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBG(result, thresholdForGreenAmpel);

        LOG.debug("Result for Umsetzungsverwaltung VBBG {}", result);

        return result;

    }

    public Map<Integer, ProjectReportingProcessKeyFigureData> getKeyFiguresForUmsetzungsverwaltungBBG(Derivat derivat, Long vereinbarungVKBGangenommen, ProjectProcessFilter filter) {

        LOG.debug("Compute Umsetzungsverwaltung BBG keyfigures for derivat {}", derivat);

        if (derivat == null) {
            LOG.error("Derivat is null! Return zero for all values.");
            return Collections.emptyMap();
        }

        Map<Integer, ProjectReportingProcessKeyFigureData> result = getQueryDataForUmsetzungsverwaltungBBG(derivat, (UmsetzungsBestaetigungStatus status) -> getKeyFigureForUmsetzungsverwaltungBBGStatus(status), filter);
        Map<Integer, ProjectReportingProcessKeyFigureData> convertToKeyFigureDataForStatusOffen = getQueryDataForUmsetzungsverwaltungStatusOffenBBG(result, vereinbarungVKBGangenommen);
        result.putAll(convertToKeyFigureDataForStatusOffen);

        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdForUmsetzungsverwaltungBBG();
        result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBG(result, thresholdForGreenAmpel);

        LOG.debug("Result for Umsetzungsverwaltung BBG {}", result);

        return result;

    }

    private static ProjectReportingProcessKeyFigure getKeyFigureForUmsetzungsverwaltungVKBGStatus(UmsetzungsBestaetigungStatus status) {
        switch (status) {
            case UMGESETZT:
                return ProjectReportingProcessKeyFigure.L12;
            case OFFEN:
                return ProjectReportingProcessKeyFigure.L13;
            case BEWERTUNG_NICHT_MOEGLICH:
                return ProjectReportingProcessKeyFigure.L14;
            case NICHT_UMGESETZT:
                return ProjectReportingProcessKeyFigure.L15;
            case NICHT_RELEVANT:
                return ProjectReportingProcessKeyFigure.L16;
            case KEINE_WEITERVERFOLGUNG:
            case NICHT_UMGESETZT_KEINE_WEITERVERFOLGUNG:
            case NICHT_UMGESETZT_WEITERVERFOLGUNG:
            case NICHT_RELEVANT_KEINE_WEITERVERFOLGUNG:
            default:
                throw new AssertionError(status.name());
        }
    }

    private static ProjectReportingProcessKeyFigure getKeyFigureForUmsetzungsverwaltungVBBGStatus(UmsetzungsBestaetigungStatus status) {
        switch (status) {
            case UMGESETZT:
                return ProjectReportingProcessKeyFigure.R14;
            case OFFEN:
                return ProjectReportingProcessKeyFigure.R15;
            case BEWERTUNG_NICHT_MOEGLICH:
                return ProjectReportingProcessKeyFigure.R16;
            case NICHT_UMGESETZT:
                return ProjectReportingProcessKeyFigure.R17;
            case NICHT_RELEVANT:
                return ProjectReportingProcessKeyFigure.R18;
            case KEINE_WEITERVERFOLGUNG:
            case NICHT_UMGESETZT_KEINE_WEITERVERFOLGUNG:
            case NICHT_UMGESETZT_WEITERVERFOLGUNG:
            case NICHT_RELEVANT_KEINE_WEITERVERFOLGUNG:
            default:
                throw new AssertionError(status.name());
        }
    }

    private static ProjectReportingProcessKeyFigure getKeyFigureForUmsetzungsverwaltungBBGStatus(UmsetzungsBestaetigungStatus status) {
        switch (status) {
            case UMGESETZT:
                return ProjectReportingProcessKeyFigure.R21;
            case OFFEN:
                return ProjectReportingProcessKeyFigure.R22;
            case BEWERTUNG_NICHT_MOEGLICH:
                return ProjectReportingProcessKeyFigure.R23;
            case NICHT_UMGESETZT:
                return ProjectReportingProcessKeyFigure.R24;
            case NICHT_RELEVANT:
                return ProjectReportingProcessKeyFigure.R25;
            case KEINE_WEITERVERFOLGUNG:
            case NICHT_UMGESETZT_KEINE_WEITERVERFOLGUNG:
            case NICHT_UMGESETZT_WEITERVERFOLGUNG:
            case NICHT_RELEVANT_KEINE_WEITERVERFOLGUNG:
            default:
                throw new AssertionError(status.name());
        }
    }

    private Map<Integer, ProjectReportingProcessKeyFigureData> getQueryDataForUmsetzungsverwaltungVBBG(Derivat derivat, Function<UmsetzungsBestaetigungStatus, ProjectReportingProcessKeyFigure> statusToKeyFigure, ProjectProcessFilter filter) {
        List<Object[]> queryDataForUmsetzungsverwaltung = umsetzungsverwaltungKeyFigureDao.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.VBBG, filter);
        return convertToKeyFigureData(queryDataForUmsetzungsverwaltung, statusToKeyFigure);
    }

    private Map<Integer, ProjectReportingProcessKeyFigureData> getQueryDataForUmsetzungsverwaltungVKBG(Derivat derivat, Function<UmsetzungsBestaetigungStatus, ProjectReportingProcessKeyFigure> statusToKeyFigure, ProjectProcessFilter filter) {
        List<Object[]> queryDataForUmsetzungsverwaltung = umsetzungsverwaltungKeyFigureDao.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.VKBG, filter);
        return convertToKeyFigureData(queryDataForUmsetzungsverwaltung, statusToKeyFigure);
    }

    private Map<Integer, ProjectReportingProcessKeyFigureData> getQueryDataForUmsetzungsverwaltungBBG(Derivat derivat, Function<UmsetzungsBestaetigungStatus, ProjectReportingProcessKeyFigure> statusToKeyFigure, ProjectProcessFilter filter) {
        List<Object[]> queryDataForUmsetzungsverwaltung = umsetzungsverwaltungKeyFigureDao.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.BBG, filter);
        return convertToKeyFigureData(queryDataForUmsetzungsverwaltung, statusToKeyFigure);
    }

    private Map<Integer, ProjectReportingProcessKeyFigureData> getQueryDataForUmsetzungsverwaltungStatusOffenVKBG(Map<Integer, ProjectReportingProcessKeyFigureData> result, Long vereinbarungVKBGangenommen) {
        Collection<ProjectReportingProcessKeyFigureData> subtrahends = getSubtrahendsForVKBGStatusOffen(result);
        ProjectReportingProcessKeyFigureData dfferenceKeyFigure = SumKeyFigureUtils.computeDifferenceKeyFigure(ProjectReportingProcessKeyFigure.L13, vereinbarungVKBGangenommen, subtrahends);
        Integer keyFigureId = dfferenceKeyFigure.getKeyFigure().getId();
        result.put(keyFigureId, dfferenceKeyFigure);
        return result;
    }

    private Map<Integer, ProjectReportingProcessKeyFigureData> getQueryDataForUmsetzungsverwaltungStatusOffenVBBG(Map<Integer, ProjectReportingProcessKeyFigureData> result, Long vereinbarungVKBGangenommen) {
        Collection<ProjectReportingProcessKeyFigureData> subtrahends = getSubtrahendsForVBBGStatusOffen(result);
        ProjectReportingProcessKeyFigureData dfferenceKeyFigure = SumKeyFigureUtils.computeDifferenceKeyFigure(ProjectReportingProcessKeyFigure.R15, vereinbarungVKBGangenommen, subtrahends);
        Integer keyFigureId = dfferenceKeyFigure.getKeyFigure().getId();
        result.put(keyFigureId, dfferenceKeyFigure);
        return result;
    }

    private Map<Integer, ProjectReportingProcessKeyFigureData> getQueryDataForUmsetzungsverwaltungStatusOffenBBG(Map<Integer, ProjectReportingProcessKeyFigureData> result, Long vereinbarungVKBGangenommen) {
        Collection<ProjectReportingProcessKeyFigureData> subtrahends = getSubtrahendsForBBGStatusOffen(result);
        ProjectReportingProcessKeyFigureData dfferenceKeyFigure = SumKeyFigureUtils.computeDifferenceKeyFigure(ProjectReportingProcessKeyFigure.R22, vereinbarungVKBGangenommen, subtrahends);
        Integer keyFigureId = dfferenceKeyFigure.getKeyFigure().getId();
        result.put(keyFigureId, dfferenceKeyFigure);
        return result;
    }

    private static Collection<ProjectReportingProcessKeyFigureData> getSubtrahendsForVBBGStatusOffen(Map<Integer, ProjectReportingProcessKeyFigureData> result) {
        return getKeyFigureDataFromMap(result,
                ProjectReportingProcessKeyFigure.R14,
                ProjectReportingProcessKeyFigure.R16,
                ProjectReportingProcessKeyFigure.R17,
                ProjectReportingProcessKeyFigure.R18);
    }

    private static Collection<ProjectReportingProcessKeyFigureData> getSubtrahendsForBBGStatusOffen(Map<Integer, ProjectReportingProcessKeyFigureData> result) {
        return getKeyFigureDataFromMap(result,
                ProjectReportingProcessKeyFigure.R21,
                ProjectReportingProcessKeyFigure.R23,
                ProjectReportingProcessKeyFigure.R24,
                ProjectReportingProcessKeyFigure.R25);
    }

    private static Collection<ProjectReportingProcessKeyFigureData> getKeyFigureDataFromMap(Map<Integer, ProjectReportingProcessKeyFigureData> result, ProjectReportingProcessKeyFigure... keyFigures) {
        Iterator<ProjectReportingProcessKeyFigure> iterator = Stream.of(keyFigures).iterator();

        Collection<ProjectReportingProcessKeyFigureData> subtrahends = new ArrayList<>();

        while (iterator.hasNext()) {
            ProjectReportingProcessKeyFigure keyFigure = iterator.next();
            Integer keyFigureId = keyFigure.getId();
            Optional<ProjectReportingProcessKeyFigureData> keyFigureData = Optional.ofNullable(result.get(keyFigureId));
            keyFigureData.ifPresent(data -> subtrahends.add(data));
        }

        return subtrahends;
    }

    private static Collection<ProjectReportingProcessKeyFigureData> getSubtrahendsForVKBGStatusOffen(Map<Integer, ProjectReportingProcessKeyFigureData> result) {
        return getKeyFigureDataFromMap(result,
                ProjectReportingProcessKeyFigure.L12,
                ProjectReportingProcessKeyFigure.L14,
                ProjectReportingProcessKeyFigure.L15,
                ProjectReportingProcessKeyFigure.L16);
    }

    private static Map<Integer, ProjectReportingProcessKeyFigureData> convertToKeyFigureData(Collection<Object[]> queryResults, Function<UmsetzungsBestaetigungStatus, ProjectReportingProcessKeyFigure> statusToKeyFigure) {
        Iterator<Object[]> iterator = queryResults.iterator();
        Map<Integer, ProjectReportingProcessKeyFigureData> result = new HashMap<>();
        while (iterator.hasNext()) {
            Object[] queryResult = iterator.next();

            UmsetzungsBestaetigungStatus status;
            if (queryResult[0] != null) {
                Integer statusId = (Integer) queryResult[0];
                status = UmsetzungsBestaetigungStatus.getStatusById(statusId);
            } else {
                status = UmsetzungsBestaetigungStatus.OFFEN;
            }

            try {
                ProjectReportingProcessKeyFigure keyFiguresForUmsetzungsverwaltungStatus = statusToKeyFigure.apply(status);

                Long value = (Long) queryResult[1];
                ProjectReportingProcessKeyFigureData keyFigureData = getKeyFigureData(keyFiguresForUmsetzungsverwaltungStatus, value);
                Integer keyFigureId = keyFiguresForUmsetzungsverwaltungStatus.getId();

                result.put(keyFigureId, keyFigureData);
            } catch (AssertionError assertionError) {
                LOG.debug("AssertionError", assertionError);
            }
        }
        return result;
    }

    private static ProjectReportingProcessKeyFigureData getKeyFigureData(ProjectReportingProcessKeyFigure keyFigure, Long value) {
        return ProjectReportingProcessKeyFigureDataBuilder
                .withKeyFigure(keyFigure)
                .withValue(value)
                .build();
    }

}
