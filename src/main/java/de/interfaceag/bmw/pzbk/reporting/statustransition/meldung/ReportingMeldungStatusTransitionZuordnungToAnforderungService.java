package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

@Dependent
public class ReportingMeldungStatusTransitionZuordnungToAnforderungService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(ReportingMeldungStatusTransitionZuordnungToAnforderungService.class);

    @Inject
    private ReportingMeldungStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Inject
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Inject
    private ReportingMeldungStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Inject
    private ReportingMeldungStatusTransitionEntryDateService entyDateService;

    public ReportingMeldungStatusTransition addMeldungToExistingAnforderung(Meldung meldung, Date date) {
        LOG.debug("Write ReportingMeldungStatusTransition for Meldung {}", meldung);

        final Optional<ReportingMeldungStatusTransition> latestStatusTransitionForAnforderung = statusTransitionDatabaseAdapter.getLatest(meldung);

        final ReportingMeldungStatusTransition statusTransition;
        if (latestStatusTransitionForAnforderung.isPresent()) {
            LOG.debug("Found latest status transition and update or create new instance if necessary");
            statusTransition = updateReportingStatusTransitionForExistingAnforderung(latestStatusTransitionForAnforderung.get(), date);
        } else {
            LOG.debug("Found no status transition --> create new instance");
            Optional<Date> entryDate = entyDateService.getEntryDateForMeldung(meldung);
            if (entryDate.isPresent()) {
                statusTransition = createNewReportingStatusTransitionForMeldungAndUpdateStatusForExistingAnforderung(meldung, entryDate.get(), date);
            } else {
                LOG.error("No entry date is present for meldung {}", meldung);
                throw new IllegalArgumentException("No entry date is present for meldung");
            }
        }
        statusTransitionDatabaseAdapter.save(statusTransition);
        return statusTransition;
    }

    public ReportingMeldungStatusTransition addMeldungToNewAnforderung(Meldung meldung, Date date) {
        LOG.debug("Write ReportingMeldungStatusTransition for Meldung {}", meldung);

        final Optional<ReportingMeldungStatusTransition> latestStatusTransitionForAnforderung = statusTransitionDatabaseAdapter.getLatest(meldung);

        final ReportingMeldungStatusTransition statusTransition;
        if (latestStatusTransitionForAnforderung.isPresent()) {
            LOG.debug("Found latest status transition and update or create new instance if necessary");
            statusTransition = updateReportingStatusTransitionForNewAnforderung(latestStatusTransitionForAnforderung.get(), date);
        } else {
            LOG.debug("Found no status transition --> create new instance");
            Optional<Date> entryDate = entyDateService.getEntryDateForMeldung(meldung);
            if (entryDate.isPresent()) {
                statusTransition = createNewReportingStatusTransitionForMeldungAndUpdateStatusForNewAnforderung(meldung, entryDate.get(), date);
            } else {
                LOG.error("No entry date is present for meldung {}", meldung);
                throw new IllegalArgumentException("No entry date is present for meldung");
            }
        }
        statusTransitionDatabaseAdapter.save(statusTransition);
        return statusTransition;

    }

    private ReportingMeldungStatusTransition updateReportingStatusTransitionForExistingAnforderung(ReportingMeldungStatusTransition meldungStatusTransition, Date date) {
        updateGemeldetExit(meldungStatusTransition, date);
        updateExistingnforderungZugeordnet(meldungStatusTransition, date);
        return meldungStatusTransition;
    }

    private ReportingMeldungStatusTransition updateReportingStatusTransitionForNewAnforderung(ReportingMeldungStatusTransition meldungStatusTransition, Date date) {
        updateGemeldetExit(meldungStatusTransition, date);
        updateNewAnforderungZugeordnet(meldungStatusTransition, date);
        return meldungStatusTransition;
    }

    private void updateExistingnforderungZugeordnet(ReportingMeldungStatusTransition meldungStatusTransition, Date date) {
        final EntryExitTimeStamp existingAnforderungZugeordnet = meldungStatusTransition.getExistingAnforderungZugeordnet();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(existingAnforderungZugeordnet, date);
        meldungStatusTransition.setExistingAnforderungZugeordnet(timeStamp);
    }

    private void updateNewAnforderungZugeordnet(ReportingMeldungStatusTransition meldungStatusTransition, Date date) {
        final EntryExitTimeStamp newAnforderungZugeordnet = meldungStatusTransition.getNewAnforderungZugeordnet();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(newAnforderungZugeordnet, date);
        meldungStatusTransition.setNewAnforderungZugeordnet(timeStamp);
    }

    private void updateGemeldetExit(ReportingMeldungStatusTransition meldungStatusTransition, Date date) {
        final EntryExitTimeStamp gemeldet = meldungStatusTransition.getGemeldet();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(gemeldet, date);
        meldungStatusTransition.setGemeldet(timeStamp);
    }

    private ReportingMeldungStatusTransition createNewReportingStatusTransitionForMeldungAndUpdateStatusForNewAnforderung(Meldung meldung, Date entryDate, Date date) {
        LOG.debug("Create new ReportingMeldungStatusTransition instance for meldung {}", meldung);
        ReportingMeldungStatusTransition statusTransition = createNewReportingStatusTransitionRow(meldung, entryDate);
        return updateReportingStatusTransitionForNewAnforderung(statusTransition, date);
    }

    private ReportingMeldungStatusTransition createNewReportingStatusTransitionForMeldungAndUpdateStatusForExistingAnforderung(Meldung meldung, Date entryDate, Date date) {
        LOG.debug("Create new ReportingMeldungStatusTransition instance for meldung {}", meldung);
        ReportingMeldungStatusTransition statusTransition = createNewReportingStatusTransitionRow(meldung, entryDate);
        return updateReportingStatusTransitionForExistingAnforderung(statusTransition, date);
    }

    private ReportingMeldungStatusTransition createNewReportingStatusTransitionRow(Meldung meldung, Date entryDate) {
        return reportingStatusTransitionCreatePort.getNewStatusTransitionForMeldungWithEntryDate(meldung, entryDate);
    }
}
