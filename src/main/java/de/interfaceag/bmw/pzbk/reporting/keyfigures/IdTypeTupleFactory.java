package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.enums.Type;

import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class IdTypeTupleFactory {

    private IdTypeTupleFactory() {
    }

    public static IdTypeTupleCollection getIdTypeTuplesForAnforderung(Collection<Long> anforderungIds) {
        return getIdTypeTuples(anforderungIds, Type.ANFORDERUNG);
    }

    public static IdTypeTupleCollection getIdTypeTuplesForMeldung(Collection<Long> meldungIds) {
        return getIdTypeTuples(meldungIds, Type.MELDUNG);
    }

    public static IdTypeTupleCollection getIdTypeTuplesForMeldungAndAnforderung(Collection<Long> meldungIds, Collection<Long> anforderungIds) {
        IdTypeTupleCollection result = new IdTypeTupleCollection();
        result.addAll(getIdTypeTuples(anforderungIds, Type.ANFORDERUNG));
        result.addAll(getIdTypeTuples(meldungIds, Type.MELDUNG));
        return result;
    }

    private static IdTypeTupleCollection getIdTypeTuples(Collection<Long> ids, Type type) {

        IdTypeTupleCollection result = new IdTypeTupleCollection();

        if (ids != null) {
            Iterator<Long> iterator = ids.iterator();
            while (iterator.hasNext()) {
                Long id = iterator.next();
                result.add(new IdTypeTuple(type, id));
            }
        }

        return result;
    }

}
