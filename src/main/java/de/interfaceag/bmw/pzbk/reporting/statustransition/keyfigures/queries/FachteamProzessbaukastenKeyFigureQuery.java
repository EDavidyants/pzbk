package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Date;

final class FachteamProzessbaukastenKeyFigureQuery {

    private static final Logger LOG = LoggerFactory.getLogger(FachteamProzessbaukastenKeyFigureQuery.class.getName());

    private FachteamProzessbaukastenKeyFigureQuery() {
    }

    static FachteamProzessbaukastenKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {
        Collection<Long> anforderungIds = getAnforderungIdsForFachteamProzessbaukastenEntry(filter, entityManager);
        return new FachteamProzessbaukastenKeyFigure(anforderungIds);
    }

    private static Collection<Long> getAnforderungIdsForFachteamProzessbaukastenEntry(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT a.id ");
        queryPart.append(" FROM ReportingStatusTransition r ")
                .append(" INNER JOIN r.anforderung a ")
                .append(" WHERE r.fachteamProzessbaukasten.entry < :endDate ")
                .append(" AND r.fachteamProzessbaukasten.exit IS NULL ");
        queryPart.put("endDate", endDate);

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getAnforderungIdsForFachteamProzessbaukastenEntry Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

}
