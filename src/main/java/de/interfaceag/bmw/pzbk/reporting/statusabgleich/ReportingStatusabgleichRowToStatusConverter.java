package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ReportingStatusabgleichRowToStatusConverter {

    private ReportingStatusabgleichRowToStatusConverter() {
    }

    protected static Collection<DerivatAnforderungModulStatus> convertToDerivatAnforderungModulStatus(int row) {
        switch (row) {
            case 1:
                return Arrays.asList(DerivatAnforderungModulStatus.ANGENOMMEN);
            case 2:
                return Arrays.asList(DerivatAnforderungModulStatus.IN_KLAERUNG);
            case 3:
                return Arrays.asList(DerivatAnforderungModulStatus.ABZUSTIMMEN);
            case 4:
                return Arrays.asList(DerivatAnforderungModulStatus.NICHT_BEWERTBAR);
            case 5:
                return Arrays.asList(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG);
            case 6:
                return Arrays.asList(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET);
            default:
                return Collections.EMPTY_SET;
        }
    }

}
