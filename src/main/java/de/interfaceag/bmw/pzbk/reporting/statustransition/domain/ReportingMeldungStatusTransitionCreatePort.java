package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import de.interfaceag.bmw.pzbk.entities.Meldung;

import java.io.Serializable;
import java.util.Date;

public interface ReportingMeldungStatusTransitionCreatePort extends Serializable {

    ReportingMeldungStatusTransition getNewStatusTransitionForMeldungWithEntryDate(Meldung meldung, Date entryDate);

}
