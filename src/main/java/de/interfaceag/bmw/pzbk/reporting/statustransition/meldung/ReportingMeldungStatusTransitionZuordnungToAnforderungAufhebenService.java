package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.AnforderungReportingStatusTransitionAdapter;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

@Dependent
public class ReportingMeldungStatusTransitionZuordnungToAnforderungAufhebenService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(ReportingMeldungStatusTransitionZuordnungToAnforderungAufhebenService.class);

    @Inject
    private ReportingMeldungStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Inject
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Inject
    private ReportingMeldungStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Inject
    private ReportingMeldungStatusTransitionEntryDateService entryDateService;
    @Inject
    private AnforderungReportingStatusTransitionAdapter anforderungReportingStatusTransitionAdapter;

    public ReportingMeldungStatusTransition removeMeldungFromAnforderung(Meldung meldung, Anforderung anforderung, Date date) {
        try {
            LOG.debug("Write ReportingMeldungStatusTransition for Meldung Zuordnung Aufhebung {}", meldung);

            final Optional<ReportingMeldungStatusTransition> latestStatusTransitionForMeldung = statusTransitionDatabaseAdapter.getLatest(meldung);

            final ReportingMeldungStatusTransition statusTransition;

            if (latestStatusTransitionForMeldung.isPresent()) {
                ReportingMeldungStatusTransition statusTransitionForUpdate = latestStatusTransitionForMeldung.get();
                LOG.debug("Found latest status transition and update or create new instance if necessary");

                statusTransition = updateReportingStatusTransitionForRemoveAnforderung(statusTransitionForUpdate, date, anforderung);
            } else {
                LOG.debug("Found no status transition --> create new instance");
                Optional<Date> entryDate = entryDateService.getEntryDateForMeldung(meldung);
                if (entryDate.isPresent()) {
                    statusTransition = createNewReportingStatusTransitionForMeldungAndUpdateStatus(meldung, entryDate.get(), date);
                } else {
                    throw new IllegalArgumentException("Pech");
                }
            }

            statusTransitionDatabaseAdapter.save(statusTransition);
            return statusTransition;
        } catch (NullPointerException ex) {
            LOG.error("NPE RemoveMeldungFromAnforderung", ex);
        }
        return null;
    }

    private ReportingMeldungStatusTransition updateReportingStatusTransitionForRemoveAnforderung(ReportingMeldungStatusTransition meldungStatusTransition, Date date, Anforderung anforderung) {
        if (isExistingAnforderungZugeordnet(meldungStatusTransition)) {
            updateExistingAnforderungZugeordnetExit(meldungStatusTransition, date);
        } else if (isNewAnforderungZugeordnet(meldungStatusTransition)) {
            updateNewAnforderungZugeordnetExit(meldungStatusTransition, date);
            removeAnforderungStatusTranstion(anforderung);
        }

        updateGemeldetEntryInNewRow(meldungStatusTransition, date);
        return meldungStatusTransition;
    }

    private void removeAnforderungStatusTranstion(Anforderung anforderung) {
        anforderungReportingStatusTransitionAdapter.removeAnforderung(anforderung);
    }

    private ReportingMeldungStatusTransition updateReportingStatusTransition(ReportingMeldungStatusTransition meldungStatusTransition, Date date) {
        if (isExistingAnforderungZugeordnet(meldungStatusTransition)) {
            updateExistingAnforderungZugeordnetExit(meldungStatusTransition, date);
        } else if (isNewAnforderungZugeordnet(meldungStatusTransition)) {
            updateNewAnforderungZugeordnetExit(meldungStatusTransition, date);
        }

        updateGemeldetEntryInNewRow(meldungStatusTransition, date);
        return meldungStatusTransition;
    }

    private boolean isExistingAnforderungZugeordnet(ReportingMeldungStatusTransition meldungStatusTransition) {
        final EntryExitTimeStamp existingAnforderungZugeordnet = meldungStatusTransition.getExistingAnforderungZugeordnet();
        return existingAnforderungZugeordnet != null && existingAnforderungZugeordnet.isActive();
    }

    private boolean isNewAnforderungZugeordnet(ReportingMeldungStatusTransition meldungStatusTransition) {
        final EntryExitTimeStamp newAnforderungZugeordnet = meldungStatusTransition.getNewAnforderungZugeordnet();
        return newAnforderungZugeordnet != null && newAnforderungZugeordnet.isActive();
    }

    private void updateExistingAnforderungZugeordnetExit(ReportingMeldungStatusTransition meldungStatusTransition, Date date) {
        final EntryExitTimeStamp existingAnforderungZugeordnet = meldungStatusTransition.getExistingAnforderungZugeordnet();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(existingAnforderungZugeordnet, date);
        meldungStatusTransition.setExistingAnforderungZugeordnet(timeStamp);
    }

    private void updateNewAnforderungZugeordnetExit(ReportingMeldungStatusTransition meldungStatusTransition, Date date) {
        final EntryExitTimeStamp newAnforderungZugeordnet = meldungStatusTransition.getNewAnforderungZugeordnet();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(newAnforderungZugeordnet, date);
        meldungStatusTransition.setNewAnforderungZugeordnet(timeStamp);
    }

    private void updateGemeldetEntryInNewRow(ReportingMeldungStatusTransition meldungStatusTransition, Date date) {
        final ReportingMeldungStatusTransition newReportingStatusTransitionRow = createNewReportingStatusTransitionRow(meldungStatusTransition.getMeldung(), meldungStatusTransition.getEntryDate());
        updateMeldungGemeldetStateForNewRow(date, newReportingStatusTransitionRow);
    }

    private void updateMeldungGemeldetStateForNewRow(Date date, ReportingMeldungStatusTransition newReportingStatusTransitionRow) {
        final EntryExitTimeStamp gemeldet = newReportingStatusTransitionRow.getGemeldet();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(gemeldet, date);
        newReportingStatusTransitionRow.setGemeldet(timeStamp);
        statusTransitionDatabaseAdapter.save(newReportingStatusTransitionRow);
    }

    private ReportingMeldungStatusTransition createNewReportingStatusTransitionForMeldungAndUpdateStatus(Meldung meldung, Date entryDate, Date date) {
        LOG.debug("Create new ReportingMeldungStatusTransition instance for meldung {}", meldung);
        ReportingMeldungStatusTransition statusTransition = createNewReportingStatusTransitionRow(meldung, entryDate);
        return updateReportingStatusTransition(statusTransition, date);
    }

    private ReportingMeldungStatusTransition createNewReportingStatusTransitionRow(Meldung meldung, Date entryDate) {
        return reportingStatusTransitionCreatePort.getNewStatusTransitionForMeldungWithEntryDate(meldung, entryDate);
    }
}
