package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.drilldown;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author lomu
 */
@Named
@Stateless
public class ProjectReportingDrilldownService {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingDrilldownService.class);

    @Inject
    private ProjectReportingAusleitungDrilldownService ausleitungDrilldownService;
    @Inject
    private ProjectReportingVereinbarungDrilldownService vereinbarungDrilldownService;
    @Inject
    private ProjectReportingUmsetzungDrilldownService umsetzungDrilldownService;

    public List<ProjectReportingProcessKeyFigure> getAllAvailableKeyFigures() {
        List<ProjectReportingProcessKeyFigure> result = new ArrayList<>();
        result.addAll(getAllAvailableAusleitungKeyFigures());
        result.addAll(getAllAvailableVereinbarungKeyFigures());
        result.addAll(getAllAvailableUmsetzungKeyFigures());

        return result;
    }

    public List<ProjectReportingDrilldownDto> getDrilldowndataForKeyfigure(Derivat derivat, ProjectReportingProcessKeyFigure figure) {
        List<ProjectReportingDrilldownDto> result = new ArrayList<>();

        if (getAllAvailableAusleitungKeyFigures().contains(figure)) {
            result = getDrilldowndataForAusleitung(derivat, figure);
        }
        if (getAllAvailableVereinbarungKeyFigures().contains(figure)) {
            result = getDrilldowndataForVereinbarung(derivat, figure);
        }
        if (getAllAvailableUmsetzungKeyFigures().contains(figure)) {
            result = getDrilldowndataForUmsetzung(derivat, figure);
        }

        return result;
    }

    private List<ProjectReportingDrilldownDto> getDrilldowndataForAusleitung(Derivat derivat, ProjectReportingProcessKeyFigure figure) {
        List<ProjectReportingDrilldownDto> result = new ArrayList<>();
        switch (figure) {
            case G1:
                result = ausleitungDrilldownService.getQueryDataForDrilldownG1();
                break;
            case G2:
                result = ausleitungDrilldownService.getQueryDataForDrilldownForAusleitungZakWithoutZakId(derivat, DerivatStatus.VEREINARBUNG_VKBG);
                break;
            case G3:
                result = ausleitungDrilldownService.getQueryDataForDrilldownForAusleitungZakWithZakId(derivat, DerivatStatus.VEREINARBUNG_VKBG);
                break;
            case R1:
                result = ausleitungDrilldownService.getQueryDataForDrilldownForAusleitungZakWithoutZakId(derivat, DerivatStatus.VEREINBARUNG_ZV);
                break;
            case R2:
                result = ausleitungDrilldownService.getQueryDataForDrilldownForAusleitungZakWithZakId(derivat, DerivatStatus.VEREINBARUNG_ZV);
                break;
            default:
        }
        return result;
    }

    private List<ProjectReportingDrilldownDto> getDrilldowndataForVereinbarung(Derivat derivat, ProjectReportingProcessKeyFigure figure) {
        List<ProjectReportingDrilldownDto> result = new ArrayList<>();
        if (getAllAvailableKeyFiguresForVereinbarungVKBG().contains(figure)) {
            result = getDrilldowndataForVereinbarungVKBG(derivat, figure);
        } else {
            if (getAllAvailableKeyFiguresForVereinbarungZV().contains(figure)) {
                result = getDrilldowndataForVereinbarungZV(derivat, figure);
            }
        }
        return result;
    }

    private List<ProjectReportingDrilldownDto> getDrilldowndataForVereinbarungVKBG(Derivat derivat, ProjectReportingProcessKeyFigure figure) {
        List<ProjectReportingDrilldownDto> result = new ArrayList<>();
        switch (figure) {
            case L1:
                LOG.debug("Calculating Drilldown for positiver Eingang Vereinbarung VKBG");
                result.addAll(getDrilldowndataForVereinbarungVKBG(derivat, ProjectReportingProcessKeyFigure.L4));
                result.addAll(getDrilldowndataForVereinbarungVKBG(derivat, ProjectReportingProcessKeyFigure.L5));
                result.addAll(getDrilldowndataForVereinbarungVKBG(derivat, ProjectReportingProcessKeyFigure.L6));
                result.addAll(getDrilldowndataForVereinbarungVKBG(derivat, ProjectReportingProcessKeyFigure.L7));
                LOG.debug("Result: " + result.size());
                break;
            case L2:
                LOG.debug("Calculating drilldown for negativ eingang vereinbarung VKBG");
                result.addAll(getDrilldowndataForVereinbarungVKBG(derivat, ProjectReportingProcessKeyFigure.L8));
                result.addAll(getDrilldowndataForVereinbarungVKBG(derivat, ProjectReportingProcessKeyFigure.L9));
                LOG.debug("Result: " + result.size());
                break;
            case L4:
                result = vereinbarungDrilldownService.getQueryDataForVereinbarung(derivat, "statusVKBG", DerivatAnforderungModulStatus.ANGENOMMEN);
                break;
            case L5:
                result = vereinbarungDrilldownService.getQueryDataForVereinbarung(derivat, "statusVKBG", DerivatAnforderungModulStatus.IN_KLAERUNG);
                break;
            case L6:
                result = vereinbarungDrilldownService.getQueryDataForVereinbarung(derivat, "statusVKBG", DerivatAnforderungModulStatus.ABZUSTIMMEN);
                break;
            case L7:
                result = vereinbarungDrilldownService.getQueryDataForVereinbarung(derivat, "statusVKBG", DerivatAnforderungModulStatus.NICHT_BEWERTBAR);
                break;
            case L8:
                result = vereinbarungDrilldownService.getQueryDataForVereinbarung(derivat, "statusVKBG", DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG);
                break;
            case L9:
                result = vereinbarungDrilldownService.getQueryDataForVereinbarung(derivat, "statusVKBG", DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET);
                break;
            default:
                break;

        }
        return result;
    }

    private List<ProjectReportingDrilldownDto> getDrilldowndataForVereinbarungZV(Derivat derivat, ProjectReportingProcessKeyFigure figure) {
        List<ProjectReportingDrilldownDto> result = new ArrayList<>();
        switch (figure) {
            case R3:
                LOG.debug("Calculating Drilldown for positiver eingang Vereinbarung VKBG");
                result.addAll(getDrilldowndataForVereinbarungZV(derivat, ProjectReportingProcessKeyFigure.R6));
                result.addAll(getDrilldowndataForVereinbarungZV(derivat, ProjectReportingProcessKeyFigure.R7));
                result.addAll(getDrilldowndataForVereinbarungZV(derivat, ProjectReportingProcessKeyFigure.R8));
                result.addAll(getDrilldowndataForVereinbarungZV(derivat, ProjectReportingProcessKeyFigure.R9));
                LOG.debug("Result: " + result.size());
                break;
            case R4:
                LOG.debug("Calculating Drilldown for negativer Eingang Vereinbarung VKBG");
                result.addAll(getDrilldowndataForVereinbarungZV(derivat, ProjectReportingProcessKeyFigure.R10));
                result.addAll(getDrilldowndataForVereinbarungZV(derivat, ProjectReportingProcessKeyFigure.R11));
                LOG.debug("Result: " + result.size());
                break;
            case R6:
                result = vereinbarungDrilldownService.getQueryDataForVereinbarung(derivat, "statusZV", DerivatAnforderungModulStatus.ANGENOMMEN);
                break;
            case R7:
                result = vereinbarungDrilldownService.getQueryDataForVereinbarung(derivat, "statusZV", DerivatAnforderungModulStatus.IN_KLAERUNG);
                break;
            case R8:
                result = vereinbarungDrilldownService.getQueryDataForVereinbarung(derivat, "statusZV", DerivatAnforderungModulStatus.ABZUSTIMMEN);
                break;
            case R9:
                result = vereinbarungDrilldownService.getQueryDataForVereinbarung(derivat, "statusZV", DerivatAnforderungModulStatus.NICHT_BEWERTBAR);
                break;
            case R10:
                result = vereinbarungDrilldownService.getQueryDataForVereinbarung(derivat, "statusZV", DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG);
                break;
            case R11:
                result = vereinbarungDrilldownService.getQueryDataForVereinbarung(derivat, "statusZV", DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET);
                break;
            default:
                break;
        }
        return result;
    }

    private List<ProjectReportingDrilldownDto> getDrilldowndataForUmsetzung(Derivat derivat, ProjectReportingProcessKeyFigure figure) {
        List<ProjectReportingDrilldownDto> result = new ArrayList<>();
        if (getAllAvailableKeyFiguresForUmsetzungVKBG().contains(figure)) {
            result = getDrilldowndataForUmsetungVKBG(derivat, figure);
        }
        if (getAllAvailableKeyFiguresForUmsetzungVBBG().contains(figure)) {
            result = getDrilldowndataForUmsetungVBBG(derivat, figure);
        }
        if (getAllAvailableKeyFiguresForUmsetzungBBG().contains(figure)) {
            result = getDrilldowndataForUmsetungBBG(derivat, figure);
        }
        return result;
    }

    private List<ProjectReportingDrilldownDto> getDrilldowndataForUmsetungVKBG(Derivat derivat, ProjectReportingProcessKeyFigure figure) {
        List<ProjectReportingDrilldownDto> result = new ArrayList<>();

        switch (figure) {
            case L10:
                result = getDrilldowndataForVereinbarungVKBG(derivat, ProjectReportingProcessKeyFigure.L4);
                result.removeAll(getDrilldowndataForUmsetungVKBG(derivat, ProjectReportingProcessKeyFigure.L16));
                break;
            case L12:
                result = umsetzungDrilldownService.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.VKBG, UmsetzungsBestaetigungStatus.UMGESETZT);
                break;
            case L13:
                LOG.debug("Calculating Umsetzung VKBG Status 'offen'");
                result = getDrilldowndataForVereinbarungVKBG(derivat, ProjectReportingProcessKeyFigure.L4); //L4 = Vereinbarung VKBG Angenommen

                result.removeAll(getDrilldowndataForUmsetungVKBG(derivat, ProjectReportingProcessKeyFigure.L12));
                result.removeAll(getDrilldowndataForUmsetungVKBG(derivat, ProjectReportingProcessKeyFigure.L14));
                result.removeAll(getDrilldowndataForUmsetungVKBG(derivat, ProjectReportingProcessKeyFigure.L15));
                result.removeAll(getDrilldowndataForUmsetungVKBG(derivat, ProjectReportingProcessKeyFigure.L16));

                result.stream().forEach(a -> a.setStatus(ZuordnungStatus.OFFEN.getStatusBezeichnung()));

                LOG.debug("Result: " + result.size());
                break;
            case L14:
                result = umsetzungDrilldownService.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.VKBG, UmsetzungsBestaetigungStatus.BEWERTUNG_NICHT_MOEGLICH);
                break;
            case L15:
                result = umsetzungDrilldownService.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.VKBG, UmsetzungsBestaetigungStatus.NICHT_UMGESETZT);
                break;
            case L16:
                result = umsetzungDrilldownService.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.VKBG, UmsetzungsBestaetigungStatus.NICHT_RELEVANT);
                break;
            default:
                break;

        }
        return result;
    }

    private List<ProjectReportingDrilldownDto> getDrilldowndataForUmsetungVBBG(Derivat derivat, ProjectReportingProcessKeyFigure figure) {
        List<ProjectReportingDrilldownDto> result = new ArrayList<>();

        switch (figure) {
            case R14:
                result = umsetzungDrilldownService.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.VBBG, UmsetzungsBestaetigungStatus.UMGESETZT);
                break;
            case R15:

                LOG.debug("Calculating Umsetzung VBBG Status 'offen'");
                result = getDrilldowndataForVereinbarungZV(derivat, ProjectReportingProcessKeyFigure.R6); //24 = Vereinbarung ZV Angenommen

                result.removeAll(umsetzungDrilldownService.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.VBBG, UmsetzungsBestaetigungStatus.UMGESETZT));
                result.removeAll(getDrilldowndataForUmsetungVBBG(derivat, ProjectReportingProcessKeyFigure.R16));
                result.removeAll(getDrilldowndataForUmsetungVBBG(derivat, ProjectReportingProcessKeyFigure.R17));
                result.removeAll(getDrilldowndataForUmsetungVBBG(derivat, ProjectReportingProcessKeyFigure.R18));
                result.stream().forEach(a -> a.setStatus(ZuordnungStatus.OFFEN.getStatusBezeichnung()));
                LOG.debug("Result: " + result.size());
                break;
            case R16:
                result = umsetzungDrilldownService.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.VBBG, UmsetzungsBestaetigungStatus.BEWERTUNG_NICHT_MOEGLICH);
                break;
            case R17:
                result = umsetzungDrilldownService.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.VBBG, UmsetzungsBestaetigungStatus.NICHT_UMGESETZT);
                break;
            case R18:
                result = umsetzungDrilldownService.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.VBBG, UmsetzungsBestaetigungStatus.NICHT_RELEVANT);
                break;
            default:
                break;

        }
        return result;
    }

    private List<ProjectReportingDrilldownDto> getDrilldowndataForUmsetungBBG(Derivat derivat, ProjectReportingProcessKeyFigure figure) {
        List<ProjectReportingDrilldownDto> result = new ArrayList<>();

        switch (figure) {
            case R21:
                result = umsetzungDrilldownService.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.BBG, UmsetzungsBestaetigungStatus.UMGESETZT);
                break;
            case R22:
                LOG.debug("Calculating Umsetzung BBG Status 'offen'");

                result = getDrilldowndataForVereinbarungZV(derivat, ProjectReportingProcessKeyFigure.R6); //24 = Vereinbarung ZV Angenommen

                result.removeAll(getDrilldowndataForUmsetungBBG(derivat, ProjectReportingProcessKeyFigure.R21));
                result.removeAll(getDrilldowndataForUmsetungBBG(derivat, ProjectReportingProcessKeyFigure.R23));
                result.removeAll(getDrilldowndataForUmsetungBBG(derivat, ProjectReportingProcessKeyFigure.R24));
                result.removeAll(getDrilldowndataForUmsetungBBG(derivat, ProjectReportingProcessKeyFigure.R25));
                result.stream().forEach(a -> a.setStatus(ZuordnungStatus.OFFEN.getStatusBezeichnung()));
                LOG.debug("Result: " + result.size());
                break;
            case R23:
                result = umsetzungDrilldownService.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.BBG, UmsetzungsBestaetigungStatus.BEWERTUNG_NICHT_MOEGLICH);
                break;
            case R24:
                result = umsetzungDrilldownService.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.BBG, UmsetzungsBestaetigungStatus.NICHT_UMGESETZT);
                break;
            case R25:
                result = umsetzungDrilldownService.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.BBG, UmsetzungsBestaetigungStatus.NICHT_RELEVANT);
                break;
            default:
                break;

        }
        return result;
    }

    public List<ProjectReportingProcessKeyFigure> getAllAvailableAusleitungKeyFigures() {
        return Arrays.asList(ProjectReportingProcessKeyFigure.G1,
                ProjectReportingProcessKeyFigure.G2,
                ProjectReportingProcessKeyFigure.G3,
                ProjectReportingProcessKeyFigure.R1,
                ProjectReportingProcessKeyFigure.R2);
    }

    public List<ProjectReportingProcessKeyFigure> getAllAvailableVereinbarungKeyFigures() {
        return Arrays.asList(ProjectReportingProcessKeyFigure.L1,
                ProjectReportingProcessKeyFigure.L2,
                ProjectReportingProcessKeyFigure.L4,
                ProjectReportingProcessKeyFigure.L5,
                ProjectReportingProcessKeyFigure.L6,
                ProjectReportingProcessKeyFigure.L7,
                ProjectReportingProcessKeyFigure.L8,
                ProjectReportingProcessKeyFigure.L9,
                ProjectReportingProcessKeyFigure.R3,
                ProjectReportingProcessKeyFigure.R4,
                ProjectReportingProcessKeyFigure.R5,
                ProjectReportingProcessKeyFigure.R6,
                ProjectReportingProcessKeyFigure.R7,
                ProjectReportingProcessKeyFigure.R8,
                ProjectReportingProcessKeyFigure.R9,
                ProjectReportingProcessKeyFigure.R10,
                ProjectReportingProcessKeyFigure.R11);
    }

    public List<ProjectReportingProcessKeyFigure> getAllAvailableUmsetzungKeyFigures() {
        return Arrays.asList(ProjectReportingProcessKeyFigure.L10,
                ProjectReportingProcessKeyFigure.L12,
                ProjectReportingProcessKeyFigure.L13,
                ProjectReportingProcessKeyFigure.L14,
                ProjectReportingProcessKeyFigure.L15,
                ProjectReportingProcessKeyFigure.L16,
                ProjectReportingProcessKeyFigure.R14,
                ProjectReportingProcessKeyFigure.R15,
                ProjectReportingProcessKeyFigure.R16,
                ProjectReportingProcessKeyFigure.R17,
                ProjectReportingProcessKeyFigure.R18,
                ProjectReportingProcessKeyFigure.R21,
                ProjectReportingProcessKeyFigure.R22,
                ProjectReportingProcessKeyFigure.R23,
                ProjectReportingProcessKeyFigure.R24,
                ProjectReportingProcessKeyFigure.R25
        );
    }

    public List<ProjectReportingProcessKeyFigure> getAllAvailableKeyFiguresForUmsetzungVKBG() {
        return Arrays.asList(ProjectReportingProcessKeyFigure.L10,
                ProjectReportingProcessKeyFigure.L11,
                ProjectReportingProcessKeyFigure.L12,
                ProjectReportingProcessKeyFigure.L13,
                ProjectReportingProcessKeyFigure.L14,
                ProjectReportingProcessKeyFigure.L15,
                ProjectReportingProcessKeyFigure.L16
        );
    }

    public List<ProjectReportingProcessKeyFigure> getAllAvailableKeyFiguresForUmsetzungVBBG() {
        return Arrays.asList(ProjectReportingProcessKeyFigure.R14,
                ProjectReportingProcessKeyFigure.R15,
                ProjectReportingProcessKeyFigure.R16,
                ProjectReportingProcessKeyFigure.R17,
                ProjectReportingProcessKeyFigure.R18
        );
    }

    public List<ProjectReportingProcessKeyFigure> getAllAvailableKeyFiguresForUmsetzungBBG() {
        return Arrays.asList(ProjectReportingProcessKeyFigure.R21,
                ProjectReportingProcessKeyFigure.R22,
                ProjectReportingProcessKeyFigure.R23,
                ProjectReportingProcessKeyFigure.R24,
                ProjectReportingProcessKeyFigure.R25
        );
    }

    public List<ProjectReportingProcessKeyFigure> getAllAvailableKeyFiguresForVereinbarungVKBG() {
        return Arrays.asList(
                ProjectReportingProcessKeyFigure.L1,
                ProjectReportingProcessKeyFigure.L2,
                ProjectReportingProcessKeyFigure.L4,
                ProjectReportingProcessKeyFigure.L5,
                ProjectReportingProcessKeyFigure.L6,
                ProjectReportingProcessKeyFigure.L7,
                ProjectReportingProcessKeyFigure.L8,
                ProjectReportingProcessKeyFigure.L9,
                ProjectReportingProcessKeyFigure.L9
        );
    }

    public List<ProjectReportingProcessKeyFigure> getAllAvailableKeyFiguresForVereinbarungZV() {
        return Arrays.asList(ProjectReportingProcessKeyFigure.R3,
                ProjectReportingProcessKeyFigure.R4,
                ProjectReportingProcessKeyFigure.R5,
                ProjectReportingProcessKeyFigure.R6,
                ProjectReportingProcessKeyFigure.R7,
                ProjectReportingProcessKeyFigure.R8,
                ProjectReportingProcessKeyFigure.R9,
                ProjectReportingProcessKeyFigure.R10,
                ProjectReportingProcessKeyFigure.R11
        );
    }
}
