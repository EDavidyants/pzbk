package de.interfaceag.bmw.pzbk.reporting.langlaufer;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTuple;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit.FachteamDurchlaufzeitKeyFigureQuery;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit.TteamDurchlaufzeitKeyFigureQuery;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit.VereinbarungDurchlaufzeitKeyFigureQuery;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.Map;

@Dependent
public class DurchlaufzeitDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public VereinbarungDurchlaufzeitKeyFigure getVereinbarungDurchlaufzeitKeyFigure(ReportingFilter filter) {
        return VereinbarungDurchlaufzeitKeyFigureQuery.compute(filter, entityManager);
    }

    public TteamDurchlaufzeitKeyFigure getTteamDurchlaufzeitKeyFigure(ReportingFilter filter) {
        return  TteamDurchlaufzeitKeyFigureQuery.compute(filter, entityManager);
    }

    public FachteamDurchlaufzeitKeyFigure getFachteamDurchlaufzeitKeyFigure(ReportingFilter filter) {
        return FachteamDurchlaufzeitKeyFigureQuery.compute(filter, entityManager);
    }

    public Map<IdTypeTuple, Long> getFachteamIdTypeTupleDurchlaufzeitMap(ReportingFilter filter) {
        return FachteamDurchlaufzeitKeyFigureQuery.computeIdTypeDurchlaufzeit(filter, entityManager);
    }

    public Map<IdTypeTuple, Long> getTteamIdTypeTupleDurchlaufzeitMap(ReportingFilter filter) {
        return TteamDurchlaufzeitKeyFigureQuery.computeIdTypeDurchlaufzeit(filter, entityManager);
    }

    public Map<IdTypeTuple, Long> getVereinbarungIdTypeTupleDurchlaufzeitMap(ReportingFilter filter) {
        return VereinbarungDurchlaufzeitKeyFigureQuery.computeIdTypeDurchlaufzeit(filter, entityManager);
    }
}
