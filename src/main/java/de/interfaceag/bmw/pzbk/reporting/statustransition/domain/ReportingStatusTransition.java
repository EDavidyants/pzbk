package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQueries( {
        @NamedQuery(name = ReportingStatusTransition.LATEST, query = "SELECT r FROM ReportingStatusTransition r WHERE r.anforderung.id = :anforderungId ORDER BY r.id DESC")
})
public class ReportingStatusTransition implements Serializable {

    private static final String CLASSNAME = "ReportingStatusTransition";
    public static final String LATEST = CLASSNAME + ".latest";

    @Id
    @SequenceGenerator(name = "reportingstatustransition_id_seq",
            sequenceName = "reportingstatustransition_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reportingstatustransition_id_seq")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date entryDate;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Anforderung anforderung;

    // new version

    @Column(nullable = false)
    private boolean newVersion;

    // positive status transitions

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "inarbeit_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "inarbeit_exit"))
    })
    private EntryExitTimeStamp inArbeit;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "abgestimmt_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "abgestimmt_exit"))
    })
    private EntryExitTimeStamp abgestimmt;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "plausibilisiert_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "plausibilisiert_exit"))
    })
    private EntryExitTimeStamp plausibilisiert;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "freigegeben_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "freigegeben_exit"))
    })
    private EntryExitTimeStamp freigegeben;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "keineweiterverfolgung_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "keineweiterverfolgung_exit"))
    })
    private EntryExitTimeStamp keineWeiterverfolgung;

    // unstimmig

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "fachteam_unstimmig_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "fachteam_unstimmig_exit"))
    })
    private EntryExitTimeStamp fachteamUnstimmig;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "tteam_unstimmig_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "tteam_unstimmig_exit"))
    })
    private EntryExitTimeStamp tteamUnstimmig;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "vereinbarung_unstimmig_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "vereinbarung_unstimmig_exit"))
    })
    private EntryExitTimeStamp vereinbarungUnstimmig;

    // geloescht

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "fachteam_geloescht_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "fachteam_geloescht_exit"))
    })
    private EntryExitTimeStamp fachteamGeloescht;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "tteam_geloescht_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "tteam_geloescht_exit"))
    })
    private EntryExitTimeStamp tteamGeloescht;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "vereinbarung_geloescht_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "vereinbarung_geloescht_exit"))
    })
    private EntryExitTimeStamp vereinbarungGeloescht;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "freigegeben_geloescht_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "freigegeben_geloescht_exit"))
    })
    private EntryExitTimeStamp freigegebenGeloescht;

    // prozessbaukasten 

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "fachteam_prozessbaukasten_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "fachteam_prozessbaukasten_exit"))
    })
    private EntryExitTimeStamp fachteamProzessbaukasten;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "tteam_prozessbaukasten_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "tteam_prozessbaukasten_exit"))
    })
    private EntryExitTimeStamp tteamProzessbaukasten;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "vereinbarung_prozessbaukasten_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "vereinbarung_prozessbaukasten_exit"))
    })
    private EntryExitTimeStamp vereinbarungProzessbaukasten;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "freigegeben_prozessbaukasten_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "freigegeben_prozessbaukasten_exit"))
    })
    private EntryExitTimeStamp freigegebenProzessbaukasten;

    /**
     * Default constructor only for jpa. Use factory instead!
     */
    public ReportingStatusTransition() {
        this.entryDate = new Date();
    }

    ReportingStatusTransition(Date entryDate, Anforderung anforderung, boolean newVersion, EntryExitTimeStamp inArbeit, EntryExitTimeStamp abgestimmt,
                              EntryExitTimeStamp plausibilisiert, EntryExitTimeStamp freigegeben, EntryExitTimeStamp tteamUnstimmig,
                              EntryExitTimeStamp vereinbarungUnstimmig, EntryExitTimeStamp fachteamGeloescht, EntryExitTimeStamp tteamGeloescht,
                              EntryExitTimeStamp vereinbarungGeloescht, EntryExitTimeStamp freigegebenGeloescht,
                              EntryExitTimeStamp fachteamProzessbaukasten, EntryExitTimeStamp tteamProzessbaukasten,
                              EntryExitTimeStamp vereinbarungProzessbaukasten, EntryExitTimeStamp freigegebenProzessbaukasten) {
        this.entryDate = entryDate;
        this.anforderung = anforderung;
        this.inArbeit = inArbeit;
        this.abgestimmt = abgestimmt;
        this.plausibilisiert = plausibilisiert;
        this.freigegeben = freigegeben;
        this.tteamUnstimmig = tteamUnstimmig;
        this.vereinbarungUnstimmig = vereinbarungUnstimmig;
        this.fachteamGeloescht = fachteamGeloescht;
        this.tteamGeloescht = tteamGeloescht;
        this.vereinbarungGeloescht = vereinbarungGeloescht;
        this.freigegebenGeloescht = freigegebenGeloescht;
        this.fachteamProzessbaukasten = fachteamProzessbaukasten;
        this.tteamProzessbaukasten = tteamProzessbaukasten;
        this.vereinbarungProzessbaukasten = vereinbarungProzessbaukasten;
        this.freigegebenProzessbaukasten = freigegebenProzessbaukasten;
    }

    @Override
    public String toString() {
        return "ReportingStatusTransition{" +
                "id=" + id +
                ", entryDate=" + entryDate +
                ", anforderung=" + anforderung +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        ReportingStatusTransition that = (ReportingStatusTransition) object;

        return new EqualsBuilder()
                .append(newVersion, that.newVersion)
                .append(id, that.id)
                .append(entryDate, that.entryDate)
                .append(anforderung, that.anforderung)
                .append(inArbeit, that.inArbeit)
                .append(abgestimmt, that.abgestimmt)
                .append(plausibilisiert, that.plausibilisiert)
                .append(freigegeben, that.freigegeben)
                .append(keineWeiterverfolgung, that.keineWeiterverfolgung)
                .append(fachteamUnstimmig, that.fachteamUnstimmig)
                .append(tteamUnstimmig, that.tteamUnstimmig)
                .append(vereinbarungUnstimmig, that.vereinbarungUnstimmig)
                .append(fachteamGeloescht, that.fachteamGeloescht)
                .append(tteamGeloescht, that.tteamGeloescht)
                .append(vereinbarungGeloescht, that.vereinbarungGeloescht)
                .append(freigegebenGeloescht, that.freigegebenGeloescht)
                .append(fachteamProzessbaukasten, that.fachteamProzessbaukasten)
                .append(tteamProzessbaukasten, that.tteamProzessbaukasten)
                .append(vereinbarungProzessbaukasten, that.vereinbarungProzessbaukasten)
                .append(freigegebenProzessbaukasten, that.freigegebenProzessbaukasten)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(entryDate)
                .append(anforderung)
                .append(newVersion)
                .append(inArbeit)
                .append(abgestimmt)
                .append(plausibilisiert)
                .append(freigegeben)
                .append(keineWeiterverfolgung)
                .append(fachteamUnstimmig)
                .append(tteamUnstimmig)
                .append(vereinbarungUnstimmig)
                .append(fachteamGeloescht)
                .append(tteamGeloescht)
                .append(vereinbarungGeloescht)
                .append(freigegebenGeloescht)
                .append(fachteamProzessbaukasten)
                .append(tteamProzessbaukasten)
                .append(vereinbarungProzessbaukasten)
                .append(freigegebenProzessbaukasten)
                .toHashCode();
    }

    static ReportingStatusTransitionBuilder getBuilder() {
        return new ReportingStatusTransitionBuilder();
    }

    public Long getId() {
        return id;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public Anforderung getAnforderung() {
        return anforderung;
    }

    public void setAnforderung(Anforderung anforderung) {
        this.anforderung = anforderung;
    }

    public boolean isNewVersion() {
        return newVersion;
    }

    public void setNewVersion(boolean newVersion) {
        this.newVersion = newVersion;
    }

    public EntryExitTimeStamp getInArbeit() {
        return inArbeit;
    }

    public void setInArbeit(EntryExitTimeStamp inArbeit) {
        this.inArbeit = inArbeit;
    }

    public EntryExitTimeStamp getAbgestimmt() {
        return abgestimmt;
    }

    public void setAbgestimmt(EntryExitTimeStamp abgestimmt) {
        this.abgestimmt = abgestimmt;
    }

    public EntryExitTimeStamp getPlausibilisiert() {
        return plausibilisiert;
    }

    public void setPlausibilisiert(EntryExitTimeStamp plausibilisiert) {
        this.plausibilisiert = plausibilisiert;
    }

    public EntryExitTimeStamp getFreigegeben() {
        return freigegeben;
    }

    public void setFreigegeben(EntryExitTimeStamp freigegeben) {
        this.freigegeben = freigegeben;
    }

    public EntryExitTimeStamp getKeineWeiterverfolgung() {
        return keineWeiterverfolgung;
    }

    public void setKeineWeiterverfolgung(EntryExitTimeStamp keineWeiterverfolgung) {
        this.keineWeiterverfolgung = keineWeiterverfolgung;
    }

    public EntryExitTimeStamp getFachteamUnstimmig() {
        return fachteamUnstimmig;
    }

    public void setFachteamUnstimmig(EntryExitTimeStamp fachteamUnstimmig) {
        this.fachteamUnstimmig = fachteamUnstimmig;
    }

    public EntryExitTimeStamp getTteamUnstimmig() {
        return tteamUnstimmig;
    }

    public void setTteamUnstimmig(EntryExitTimeStamp tteamUnstimmig) {
        this.tteamUnstimmig = tteamUnstimmig;
    }

    public EntryExitTimeStamp getVereinbarungUnstimmig() {
        return vereinbarungUnstimmig;
    }

    public void setVereinbarungUnstimmig(EntryExitTimeStamp vereinbarungUnstimmig) {
        this.vereinbarungUnstimmig = vereinbarungUnstimmig;
    }

    public EntryExitTimeStamp getFachteamGeloescht() {
        return fachteamGeloescht;
    }

    public void setFachteamGeloescht(EntryExitTimeStamp fachteamGeloescht) {
        this.fachteamGeloescht = fachteamGeloescht;
    }

    public EntryExitTimeStamp getTteamGeloescht() {
        return tteamGeloescht;
    }

    public void setTteamGeloescht(EntryExitTimeStamp tteamGeloescht) {
        this.tteamGeloescht = tteamGeloescht;
    }

    public EntryExitTimeStamp getVereinbarungGeloescht() {
        return vereinbarungGeloescht;
    }

    public void setVereinbarungGeloescht(EntryExitTimeStamp vereinbarungGeloescht) {
        this.vereinbarungGeloescht = vereinbarungGeloescht;
    }

    public EntryExitTimeStamp getFreigegebenGeloescht() {
        return freigegebenGeloescht;
    }

    public void setFreigegebenGeloescht(EntryExitTimeStamp freigegebenGeloescht) {
        this.freigegebenGeloescht = freigegebenGeloescht;
    }

    public EntryExitTimeStamp getFachteamProzessbaukasten() {
        return fachteamProzessbaukasten;
    }

    public void setFachteamProzessbaukasten(EntryExitTimeStamp fachteamProzessbaukasten) {
        this.fachteamProzessbaukasten = fachteamProzessbaukasten;
    }

    public EntryExitTimeStamp getTteamProzessbaukasten() {
        return tteamProzessbaukasten;
    }

    public void setTteamProzessbaukasten(EntryExitTimeStamp tteamProzessbaukasten) {
        this.tteamProzessbaukasten = tteamProzessbaukasten;
    }

    public EntryExitTimeStamp getVereinbarungProzessbaukasten() {
        return vereinbarungProzessbaukasten;
    }

    public void setVereinbarungProzessbaukasten(EntryExitTimeStamp vereinbarungProzessbaukasten) {
        this.vereinbarungProzessbaukasten = vereinbarungProzessbaukasten;
    }

    public EntryExitTimeStamp getFreigegebenProzessbaukasten() {
        return freigegebenProzessbaukasten;
    }

    public void setFreigegebenProzessbaukasten(EntryExitTimeStamp freigegebenProzessbaukasten) {
        this.freigegebenProzessbaukasten = freigegebenProzessbaukasten;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
