package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;

import java.util.Set;

public final class KeyFigureQueryFestgestelltInFilter {

    private KeyFigureQueryFestgestelltInFilter() {

    }

    public static void append(ReportingFilter filter, QueryPart queryPart) {
        final IdSearchFilter derivatFilter = filter.getDerivatFilter();

        if (derivatFilter.isActive()) {
            final Set<Long> festgestelltIn = derivatFilter.getSelectedIdsAsSet();
            queryPart.append(" AND f.id IN :festgestelltIn ");
            queryPart.put("festgestelltIn", festgestelltIn);
        }
    }

}
