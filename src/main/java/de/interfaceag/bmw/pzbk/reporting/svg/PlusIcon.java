package de.interfaceag.bmw.pzbk.reporting.svg;

import de.interfaceag.bmw.pzbk.shared.dto.Point;

import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class PlusIcon implements SVGElement {

    private final double cx;
    private final double cy;
    private final double r;
    private final double rotate;
    private final String d;
    private final String transform;
    private final List<Point> points;

    public PlusIcon(double cx, double cy, double radius, String dValue, String transform, List<Point> points) {
        this.cx = cx;
        this.cy = cy;
        this.r = radius;
        this.rotate = 0;
        this.d = dValue;
        this.transform = transform;
        this.points = points;
    }

    public PlusIcon(double cx, double cy, double radius, double rotate, String dValue, String transform, List<Point> points) {
        this.cx = cx;
        this.cy = cy;
        this.r = radius;
        this.rotate = rotate;
        this.d = dValue;
        this.transform = transform;
        this.points = points;
    }

//              <g class="a7a7d0c6-d834-4f16-aa8c-4d2cc48187d8">
//                        <path class="1fc087fc-5ad7-4044-a16b-f1da2c7b2c10" d="M182.42,165.84a9.45,9.45,0,1,0-9.45-9.45,9.45,9.45,0,0,0,9.45,9.45" transform="translate(0 -0.02)"></path>
//                        <circle class="e10c3a5a-f925-4bfe-b875-b1dab086c709" cx="182.42" cy="156.38" r="9.45"></circle>
//                    </g>
//<polygon class="cb17c188-b9c0-44f6-89fc-25349d833054" points="183.42 160.38 183.42 157.41 186.43 157.41 186.43 155.35 183.42 155.35 183.42 152.37 181.41 152.37 181.41 155.35 178.41 155.35 178.41 157.41 181.41 157.41 181.41 160.38 183.42 160.38"></polygon>
    @Override
    public String draw() {
        StringBuilder sb = new StringBuilder();

        sb.append("<g class=\"a7a7d0c6-d834-4f16-aa8c-4d2cc48187d8\">");
        sb.append("<circle class=\"e10c3a5a-f925-4bfe-b875-b1dab086c709\" cx=\"").append(cx).append("\" cy=\"").append(cy).append("\" r=\"").append(r).append("\" ");

        if (rotate != 0) {
            sb.append("rotate(").append(rotate).append(")");
        }

        sb.append("/>");
        sb.append(new Path("1fc087fc-5ad7-4044-a16b-f1da2c7b2c10", d, transform).draw());
        sb.append("</g>");
        sb.append(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", points).draw());
        return sb.toString();

    }

}
