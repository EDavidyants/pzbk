package de.interfaceag.bmw.pzbk.reporting;

import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Werk;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.filter.AnforderungStatusFilter;
import de.interfaceag.bmw.pzbk.filter.BisDateFilter;
import de.interfaceag.bmw.pzbk.filter.BooleanUrlFilter;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.filter.FestgestelltInFilter;
import de.interfaceag.bmw.pzbk.filter.Filter;
import de.interfaceag.bmw.pzbk.filter.FilterToUrlConverter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.MeldungStatusFilter;
import de.interfaceag.bmw.pzbk.filter.ModulSeTeamFilter;
import de.interfaceag.bmw.pzbk.filter.SensorCocFilter;
import de.interfaceag.bmw.pzbk.filter.ShowTooltipUrlFilter;
import de.interfaceag.bmw.pzbk.filter.TechnologieFilter;
import de.interfaceag.bmw.pzbk.filter.TteamFilter;
import de.interfaceag.bmw.pzbk.filter.VonDateFilter;
import de.interfaceag.bmw.pzbk.filter.WerkFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ReportingFilter implements Serializable {

    private final Page page;

    @Filter
    private final IdSearchFilter derivatFilter;
    @Filter
    private final IdSearchFilter sensorCocFilter;
    @Filter
    private final IdSearchFilter werkFilter;
    @Filter
    private IdSearchFilter tteamFilter;
    @Filter
    private final IdSearchFilter modulSeTeamFilter;
    @Filter
    private final TechnologieFilter technologieFilter;
    @Filter
    private final DateSearchFilter vonDateFilter;
    @Filter
    private final DateSearchFilter bisDateFilter;
    @Filter
    private final BooleanUrlFilter showTooltipUrlFilter;

    private AnforderungStatusFilter anforderungStatusFilter;

    private MeldungStatusFilter meldungStatusFilter;

    public ReportingFilter(UrlParameter urlParameter) {
        this.page = Page.REPORTING_PROCESS_OVERVIEW_DETAIL;
        sensorCocFilter = new SensorCocFilter(Collections.emptyList(), urlParameter);
        derivatFilter = new FestgestelltInFilter(Collections.emptyList(), urlParameter);
        werkFilter = new WerkFilter(Collections.emptyList(), urlParameter);
        tteamFilter = new TteamFilter(Collections.emptyList(), urlParameter);
        modulSeTeamFilter = new ModulSeTeamFilter(Collections.emptyList(), urlParameter);
        technologieFilter = new TechnologieFilter(Collections.emptyList(), urlParameter);
        vonDateFilter = new VonDateFilter("", urlParameter);
        bisDateFilter = new BisDateFilter("", urlParameter);
        this.showTooltipUrlFilter = new ShowTooltipUrlFilter(urlParameter);
    }

    public ReportingFilter(
            Page page,
            UrlParameter urlParameter,
            Collection<SensorCoc> allSensorCocs,
            Collection<Werk> allWerk,
            Collection<FestgestelltIn> allFestgestelltIn,
            Collection<Tteam> allTteams,
            Date defaultStartDate,
            List<ModulSeTeam> allModulSeTeams) {
        this.page = page;
        sensorCocFilter = new SensorCocFilter(allSensorCocs, urlParameter);
        derivatFilter = new FestgestelltInFilter(allFestgestelltIn, urlParameter);
        werkFilter = new WerkFilter(allWerk, urlParameter);
        tteamFilter = new TteamFilter(allTteams, urlParameter);
        modulSeTeamFilter = new ModulSeTeamFilter(allModulSeTeams, urlParameter);
        technologieFilter = new TechnologieFilter(allSensorCocs, urlParameter);

        vonDateFilter = new VonDateFilter("", urlParameter);
        if (vonDateFilter.getSelected() == null) {
            vonDateFilter.setSelected(defaultStartDate);
            vonDateFilter.setActive(true);
        }

        bisDateFilter = new BisDateFilter("", urlParameter);
        if (bisDateFilter.getSelected() == null) {
            bisDateFilter.setSelected(new Date());
            bisDateFilter.setActive(true);
        }

        this.showTooltipUrlFilter = new ShowTooltipUrlFilter(urlParameter);
    }

    public String getUrlForPage(Page page) {
        return page.getUrl() + getUrlParameter();
    }

    public String getUrl() {
        return page.getUrl() + getUrlParameter();
    }

    public String getResetUrl() {
        return page.getUrl() + "?faces-redirect=true";
    }

    public IdSearchFilter getSensorCocFilter() {
        return sensorCocFilter;
    }

    public IdSearchFilter getDerivatFilter() {
        return derivatFilter;
    }

    public IdSearchFilter getWerkFilter() {
        return werkFilter;
    }

    public IdSearchFilter getTteamFilter() {
        return tteamFilter;
    }

    public IdSearchFilter getModulSeTeamFilter() {
        return modulSeTeamFilter;
    }

    public TechnologieFilter getTechnologieFilter() {
        return technologieFilter;
    }

    public DateSearchFilter getVonDateFilter() {
        return vonDateFilter;
    }

    public DateSearchFilter getBisDateFilter() {
        return bisDateFilter;
    }

    public BooleanUrlFilter getHideTooltipFilter() {
        return showTooltipUrlFilter;
    }

    public AnforderungStatusFilter getAnforderungStatusFilter() {
        return anforderungStatusFilter;
    }

    public void setTteamFilter(IdSearchFilter tteamFilter) {
        this.tteamFilter = tteamFilter;
    }

    public void setAnforderungStatusFilter(AnforderungStatusFilter anforderungStatusFilter) {
        this.anforderungStatusFilter = anforderungStatusFilter;
    }

    public MeldungStatusFilter getMeldungStatusFilter() {
        return meldungStatusFilter;
    }

    public void setMeldungStatusFilter(MeldungStatusFilter meldungStatusFilter) {
        this.meldungStatusFilter = meldungStatusFilter;
    }

    public boolean isShowTooltips() {
        return showTooltipUrlFilter.isActive();
    }

    private String getUrlParameter() {
        FilterToUrlConverter urlConverter = FilterToUrlConverter.forClass(this);
        return urlConverter.getUrl();
    }

}
