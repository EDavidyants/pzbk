package de.interfaceag.bmw.pzbk.reporting.svg;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @param <T>
 */
public class TooltipMap<T extends Enum<T>> implements Serializable {

    private final Map<T, String> tooltipMap;

    public TooltipMap() {
        this.tooltipMap = new HashMap<>();
    }

    public TooltipMap(Map<T, String> tooltipMap) {
        this.tooltipMap = tooltipMap;
    }

    public void addTooltip(T key, String value) {
        if (key != null && value != null) {
            this.tooltipMap.put(key, value);
        }
    }

    public void addTooltip(Optional<T> key, String value) {
        if (key.isPresent() && value != null) {
            this.tooltipMap.put(key.get(), value);
        }
    }

    public String getTooltip(T key) {
        if (tooltipMap.containsKey(key)) {
            return tooltipMap.get(key);
        } else {
            return key.toString();
        }
    }

    public List<T> getTooltips() {
        return new ArrayList<>(tooltipMap.keySet());
    }

}
