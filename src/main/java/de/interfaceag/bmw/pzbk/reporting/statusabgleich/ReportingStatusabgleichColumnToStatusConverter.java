package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.enums.ZakStatus;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ReportingStatusabgleichColumnToStatusConverter {

    private ReportingStatusabgleichColumnToStatusConverter() {
    }

    protected static Collection<ZakStatus> convertColumnToZakStatus(int column) {
        switch (column) {
            case 1:
                return Arrays.asList(ZakStatus.DONE);
            case 2:
                return Arrays.asList(ZakStatus.BESTAETIGT_AUS_VORPROZESS, ZakStatus.KEINE_ZAKUEBERTRAGUNG);
            case 3:
                return Arrays.asList(ZakStatus.PENDING);
            case 4:
                return Arrays.asList(ZakStatus.EMPTY, ZakStatus.UEBERTRAGUNG_FEHLERHAFT);
            case 5:
                return Arrays.asList(ZakStatus.NICHT_BEWERTBAR);
            case 6:
                return Arrays.asList(ZakStatus.NICHT_UMSETZBAR);
            case 7:
                return Arrays.asList(ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER);
            default:
                return Collections.EMPTY_SET;
        }
    }

}
