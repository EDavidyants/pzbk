package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ProjectReportingProcessKeyFigureDataCollectionBuilder {

    private final Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures;
    private Date date;

    public ProjectReportingProcessKeyFigureDataCollectionBuilder() {
        keyFigures = new HashMap<>();
    }

    public ProjectReportingProcessKeyFigureDataCollectionBuilder withKeyFigure(ProjectReportingProcessKeyFigureData keyFigure) {
        keyFigures.put(keyFigure.getKeyFigure().getId(), keyFigure);
        return this;
    }

    public ProjectReportingProcessKeyFigureDataCollectionBuilder withDate(Date date) {
        this.date = date;
        return this;
    }

    public ProjectReportingProcessKeyFigureDataCollection build() {
        return ProjectReportingProcessKeyFigureDataCollectionDto.forMap(keyFigures, date);
    }
}
