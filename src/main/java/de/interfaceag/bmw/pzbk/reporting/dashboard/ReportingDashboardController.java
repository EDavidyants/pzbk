package de.interfaceag.bmw.pzbk.reporting.dashboard;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterController;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterViewPermission;
import de.interfaceag.bmw.pzbk.reporting.ReportingPermissionController;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.utils.DateUtils;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author fn
 */
@ViewScoped
@Named
public class ReportingDashboardController implements Serializable, ReportingFilterController, ReportingPermissionController<ReportingDashboardViewPermission> {

    private static final Logger LOG = LoggerFactory.getLogger(ReportingDashboardController.class);

    @Inject
    private Session session;

    @Inject
    private ReportingDashboardViewFacade facade;

    @Inject
    private ReportingDashboardViewPermission viewPermission;

    @Inject
    private ReportingFilterViewPermission filterViewPermission;

    private ReportingDashboardViewData viewData;

    @PostConstruct
    public void init() {
        LOG.debug("Start Reporting Dashboard init");
        session.setLocationForView();
        viewData = facade.getDashboardViewData(UrlParameterUtils.getUrlParameter());
        LOG.debug("End Reporting Dashboard init");
    }

    @Override
    public String filter() {
        return viewData.getFilter().getUrl();
    }

    @Override
    public String reset() {
        return viewData.getFilter().getResetUrl();
    }

    @Override
    public String toggleTooltips() {
        getFilter().getHideTooltipFilter().toggle();
        return filter();
    }

    @Override
    public ReportingFilter getFilter() {
        return getViewData().getFilter();
    }

    public ReportingDashboardViewData getViewData() {
        return viewData;
    }

    public String getReportingProcessOverviewDetailUrl() {
        return getFilter().getUrlForPage(Page.REPORTING_PROCESS_OVERVIEW_DETAIL);
    }

    public String getReportingProcessOverviewSimpleUrl() {
        return getFilter().getUrlForPage(Page.REPORTING_PROCESS_OVERVIEW_SIMPLE);
    }

    public String getReportingFachteamUrl() {
        return getFilter().getUrlForPage(Page.REPORTING_FACHTEAM);
    }

    public String getReportingTteamUrl() {
        return getFilter().getUrlForPage(Page.REPORTING_TTEAM);
    }

    @Override
    public ReportingDashboardViewPermission getViewPermission() {
        return viewPermission;
    }

    @Override
    public ReportingFilterViewPermission getFilterViewPermission() {
        return filterViewPermission;
    }

    @Override
    public Date getMinDate() {
        return DateUtils.getMinDate();
    }

    @Override
    public Date getMaxDate() {
        return DateUtils.getMaxDate();
    }

}
