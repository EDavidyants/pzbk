package de.interfaceag.bmw.pzbk.reporting.svg;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author lomu
 */
public final class SVGElementDrawer {

    private SVGElementDrawer() {
    }

    public static String drawElements(List<SVGElement> elements) {
        StringBuilder sb = new StringBuilder();
        if (elements != null) {
            Iterator<SVGElement> iterator = elements.iterator();
            while (iterator.hasNext()) {
                SVGElement next = iterator.next();
                sb.append(next.draw());
            }
        }
        return sb.toString();
    }

}
