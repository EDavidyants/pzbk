package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.excel;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sl
 */
public final class ProjectReportingExcelExportUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingExcelExportUtils.class);

    private static final String FILENAME = "Reporting";

    private ProjectReportingExcelExportUtils() {
    }

    protected static String generateFilenName(Derivat derivat) {
        return FILENAME.concat(" ").concat(derivat.getName());
    }

    protected static Workbook generateExcelFileForKeyFigures(ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat, String sheetName) {
        Workbook workbook = generateWorkbook();
        Sheet sheet = generateSheet(workbook, sheetName);
        generateHeaderRow(sheet);
        generateKeyFigureRow(sheet, keyFiguresForDerivat);
        return workbook;
    }

    private static Workbook generateWorkbook() {
        LOG.debug("Generate new Workbook");
        Workbook workbook = new XSSFWorkbook();
        return workbook;
    }

    private static Sheet generateSheet(Workbook workbook, String sheetName) {
        LOG.debug("Generate sheet {}", sheetName);
        Sheet sheet = workbook.createSheet(sheetName);
        return sheet;
    }

    private static void generateHeaderRow(Sheet sheet) {
        LOG.debug("Generate header row");

        Row row = sheet.createRow(0);

        generateHeaderForAnforderungenFreigegeben(row);

        generateHeaderForAusleitungVKBG(row);
        generateHeaderForAusleitungZV(row);

        generateHeaderForVereinbarungVKBG(row);
        generateHeaderForVereinbarungZV(row);

        generateHeaderForUmsetzungVKBG(row);
        generateHeaderForUmsetzungVBBG(row);
        generateHeaderForUmsetzungBBG(row);
    }

    private static void generateHeaderForAnforderungenFreigegeben(Row row) {
        row.createCell(0).setCellValue("Anforderungen freigegeben");
    }

    private static void generateHeaderForAusleitungVKBG(Row row) {
        row.createCell(1).setCellValue("Anforderungen ausgeleitet VKBG");
        row.createCell(2).setCellValue("ZAK Ids ausgeleitet VKBG");
    }

    private static void generateHeaderForAusleitungZV(Row row) {
        row.createCell(3).setCellValue("Anforderungen ausgeleitet ZV");
        row.createCell(4).setCellValue("ZAK Ids ausgeleitet ZV");
    }

    private static void generateHeaderForVereinbarungVKBG(Row row) {
        row.createCell(5).setCellValue("ZAK Ids angenommen VKBG");
        row.createCell(6).setCellValue("ZAK Ids in Klaerung VKBG");
        row.createCell(7).setCellValue("ZAK Ids abzustimmen VKBG");
        row.createCell(8).setCellValue("ZAK Ids nicht bewertbar VKBG");
        row.createCell(9).setCellValue("ZAK Ids keine Weiterverfolgung VKBG");
        row.createCell(10).setCellValue("ZAK Ids unzureichende Anforderungsqualitaet VKBG");
    }

    private static void generateHeaderForVereinbarungZV(Row row) {
        row.createCell(11).setCellValue("ZAK Ids angenommen ZV");
        row.createCell(12).setCellValue("ZAK Ids in Klaerung ZV");
        row.createCell(13).setCellValue("ZAK Ids abzustimmen ZV");
        row.createCell(14).setCellValue("ZAK Ids nicht bewertbar ZV");
        row.createCell(15).setCellValue("ZAK Ids keine Weiterverfolgung ZV");
        row.createCell(16).setCellValue("ZAK Ids unzureichende Anforderungsqualitaet ZV");
    }

    private static void generateHeaderForUmsetzungVKBG(Row row) {
        row.createCell(17).setCellValue("ZAK Ids umgesetzt VKBG");
        row.createCell(18).setCellValue("ZAK Ids offen VKBG");
        row.createCell(19).setCellValue("ZAK Ids Bewertung nicht moeglich VKBG");
        row.createCell(20).setCellValue("ZAK Ids nicht umgesetzt VKBG");
        row.createCell(21).setCellValue("ZAK Ids nicht relevant VKBG");
    }

    private static void generateHeaderForUmsetzungVBBG(Row row) {
        row.createCell(22).setCellValue("ZAK Ids umgesetzt VBBG");
        row.createCell(23).setCellValue("ZAK Ids offen VBBG");
        row.createCell(24).setCellValue("ZAK Ids Bewertung nicht moeglich VBBG");
        row.createCell(25).setCellValue("ZAK Ids nicht umgesetzt VBBG");
        row.createCell(26).setCellValue("ZAK Ids nicht relevant VBBG");
    }

    private static void generateHeaderForUmsetzungBBG(Row row) {
        row.createCell(27).setCellValue("ZAK Ids umgesetzt BBG");
        row.createCell(28).setCellValue("ZAK Ids offen BBG");
        row.createCell(29).setCellValue("ZAK Ids Bewertung nicht moeglich BBG");
        row.createCell(30).setCellValue("ZAK Ids nicht umgesetzt BBG");
        row.createCell(31).setCellValue("ZAK Ids nicht relevant BBG");
    }

    private static void generateKeyFigureRow(Sheet sheet, ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat) {
        LOG.debug("Generate content row for map {}", keyFiguresForDerivat);

        Row row = sheet.createRow(1);

        generateRowForAnforderungenFreigegeben(row, keyFiguresForDerivat);

        generateRowForAusleitungVKBG(row, keyFiguresForDerivat);
        generateRowForAusleitungZV(row, keyFiguresForDerivat);

        generateRowForVereinbarungVKBG(row, keyFiguresForDerivat);
        generateRowForVereinbarungZV(row, keyFiguresForDerivat);

        generateRowForUmsetzungVKBG(row, keyFiguresForDerivat);
        generateRowForUmsetzungVBBG(row, keyFiguresForDerivat);
        generateRowForUmsetzungBBG(row, keyFiguresForDerivat);
    }

    private static void insertRowValueAtIndex(Row row, int cellIndex, ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat, ProjectReportingProcessKeyFigure keyFigure) {
        Cell cell = row.createCell(cellIndex);
        Long value = keyFiguresForDerivat.getValueForKeyFigure(keyFigure);
        cell.setCellValue(value);
    }

    private static void generateRowForAnforderungenFreigegeben(Row row, ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat) {
        insertRowValueAtIndex(row, 0, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.G1);
    }

    private static void generateRowForAusleitungVKBG(Row row, ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat) {
        insertRowValueAtIndex(row, 1, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.G2);
        insertRowValueAtIndex(row, 2, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.G3);
    }

    private static void generateRowForAusleitungZV(Row row, ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat) {
        insertRowValueAtIndex(row, 3, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L1);
        insertRowValueAtIndex(row, 4, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L2);
    }

    private static void generateRowForVereinbarungVKBG(Row row, ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat) {
        insertRowValueAtIndex(row, 5, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L3);
        insertRowValueAtIndex(row, 6, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L4);
        insertRowValueAtIndex(row, 7, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L5);
        insertRowValueAtIndex(row, 8, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L6);
        insertRowValueAtIndex(row, 9, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L7);
        insertRowValueAtIndex(row, 10, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L8);
    }

    private static void generateRowForVereinbarungZV(Row row, ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat) {
        insertRowValueAtIndex(row, 11, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L9);
        insertRowValueAtIndex(row, 12, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L10);
        insertRowValueAtIndex(row, 13, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L11);
        insertRowValueAtIndex(row, 14, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L12);
        insertRowValueAtIndex(row, 15, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L13);
        insertRowValueAtIndex(row, 16, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L14);
    }

    private static void generateRowForUmsetzungVKBG(Row row, ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat) {
        insertRowValueAtIndex(row, 17, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L15);
        insertRowValueAtIndex(row, 18, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.L16);
        insertRowValueAtIndex(row, 19, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.R1);
        insertRowValueAtIndex(row, 20, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.R2);
        insertRowValueAtIndex(row, 21, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.R3);
    }

    private static void generateRowForUmsetzungVBBG(Row row, ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat) {
        insertRowValueAtIndex(row, 22, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.R4);
        insertRowValueAtIndex(row, 23, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.R5);
        insertRowValueAtIndex(row, 24, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.R6);
        insertRowValueAtIndex(row, 25, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.R7);
        insertRowValueAtIndex(row, 26, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.R8);
    }

    private static void generateRowForUmsetzungBBG(Row row, ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat) {
        insertRowValueAtIndex(row, 27, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.R9);
        insertRowValueAtIndex(row, 28, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.R10);
        insertRowValueAtIndex(row, 29, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.R11);
        insertRowValueAtIndex(row, 30, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.R12);
        insertRowValueAtIndex(row, 31, keyFiguresForDerivat, ProjectReportingProcessKeyFigure.R13);
    }

}
