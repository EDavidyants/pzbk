package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingUtils;
import de.interfaceag.bmw.pzbk.reporting.ReportingPeriodGetter;
import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ReportingTteamViewData implements ReportingPeriodGetter, Serializable {

    private final TteamKeyFigures tteamKeyFigures;

    private final ReportingFilter filter;

    private final String reportingPeriod;

    public ReportingTteamViewData(TteamKeyFigures tteamKeyFigures,
            ReportingFilter filter) {
        this.tteamKeyFigures = tteamKeyFigures;
        this.filter = filter;
        this.reportingPeriod = ReportingUtils.getReportingPeriod(filter);
    }

    public TteamKeyFigures getTteamKeyFigures() {
        return tteamKeyFigures;
    }

    public ReportingFilter getFilter() {
        return filter;
    }

    @Override
    public String getReportingPeriod() {
        return reportingPeriod;
    }

}
