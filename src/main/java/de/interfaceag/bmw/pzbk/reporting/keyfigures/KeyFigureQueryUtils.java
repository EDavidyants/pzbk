package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class KeyFigureQueryUtils {

    private KeyFigureQueryUtils() {
    }

    public static String getStartDatumForQuery(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return "'" + sdf.format(date) + "'";
    }

    public static String getEndDatumForQuery(Date date) {
        LocalDate dateLocal = Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = dateLocal.plusDays(1);
        String dateString = endDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return "'" + dateString + "'";
    }

}
