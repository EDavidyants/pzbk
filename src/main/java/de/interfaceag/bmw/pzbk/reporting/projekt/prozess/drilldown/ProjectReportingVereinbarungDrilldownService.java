package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.drilldown;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 *
 * @author lomu
 */
@Named
@Stateless
public class ProjectReportingVereinbarungDrilldownService {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingVereinbarungDrilldownService.class);

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public List<ProjectReportingDrilldownDto> getQueryDataForVereinbarung(Derivat derivat, String derivatStatus, DerivatAnforderungModulStatus anforderungModulStatus) {
        QueryPart queryPart = new QueryPartDTO(
                "SELECT new de.interfaceag.bmw.pzbk.reporting.projekt.prozess.drilldown.ProjectReportingDrilldownDto "
                + "(a.id, a.fachId, a.version, dam.modul.name, zak.zakId, a.beschreibungAnforderungDe,"
                + " dam." + derivatStatus + ")  "
                + "FROM ZakUebertragung zak "
                + "INNER JOIN zak.derivatAnforderungModul dam "
                + "INNER JOIN dam.zuordnungAnforderungDerivat zad "
                + "INNER JOIN zad.anforderung a "
                + "INNER JOIN zad.derivat d "
                + "WHERE d.id = :derivatId "
                + "AND dam.");
        queryPart.append(derivatStatus).append(" = :anforderungModulStatus");
        queryPart.put("derivatId", derivat.getId());
        queryPart.put("anforderungModulStatus", anforderungModulStatus.getStatusId());
        Query query = queryPart.buildGenericQuery(entityManager);
        LOG.debug("Excecute query: {}", query.toString());
        LOG.debug("Query parameters: {}", query.getParameters());

        List<ProjectReportingDrilldownDto> resultList = query.getResultList();
        LOG.debug("Retrieved {} entries", resultList.size());
        return resultList;
    }

}
