package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.listview.ListKeyFigure;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class TteamKeyFigure implements Serializable, ListKeyFigure {

    private final long tteamId;

    private final String tteamName;

    private final KeyFigureCollection keyFigures;

    private final DurchlaufzeitKeyFigure tteamDurchlaufzeitKeyFigure;

    private final DurchlaufzeitKeyFigure vereinbarungDurchlaufzeitKeyFigure;

    private int tteamLanglauferCount;

    private int vereinbarungLanglauferCount;


    public TteamKeyFigure(String tteamName, KeyFigureCollection keyFigures,
                          DurchlaufzeitKeyFigure tteamDurchlaufzeitKeyFigure,
                          DurchlaufzeitKeyFigure vereinbarungDurchlaufzeitKeyFigure,
                          int tteamLanglauferCount, int vereinbarungLanglauferCount) {
        this.tteamId = -1;
        this.tteamName = tteamName;
        this.keyFigures = keyFigures;
        this.tteamDurchlaufzeitKeyFigure = tteamDurchlaufzeitKeyFigure;
        this.vereinbarungDurchlaufzeitKeyFigure = vereinbarungDurchlaufzeitKeyFigure;
        this.tteamLanglauferCount = tteamLanglauferCount;
        this.vereinbarungLanglauferCount = vereinbarungLanglauferCount;
    }

    public TteamKeyFigure(long tteamId, String tteamName, KeyFigureCollection keyFigures,
                          DurchlaufzeitKeyFigure tteamDurchlaufzeitKeyFigure,
                          DurchlaufzeitKeyFigure vereinbarungDurchlaufzeitKeyFigure,
                          int tteamLanglauferCount, int vereinbarungLanglauferCount) {
        this.tteamId = tteamId;
        this.tteamName = tteamName;
        this.keyFigures = keyFigures;
        this.tteamDurchlaufzeitKeyFigure = tteamDurchlaufzeitKeyFigure;
        this.vereinbarungDurchlaufzeitKeyFigure = vereinbarungDurchlaufzeitKeyFigure;
        this.tteamLanglauferCount = tteamLanglauferCount;
        this.vereinbarungLanglauferCount = vereinbarungLanglauferCount;
    }

    public long getTteamId() {
        return tteamId;
    }

    public String getTteamName() {
        return tteamName;
    }

    public Collection<KeyFigure> getKeyFigures() {
        return keyFigures;
    }

    public int getTteamDurchlaufzeit() {
        return tteamDurchlaufzeitKeyFigure != null ? tteamDurchlaufzeitKeyFigure.getValue() : 0;
    }

    public int getVereinbarungDurchlaufzeit() {
        return vereinbarungDurchlaufzeitKeyFigure != null ? vereinbarungDurchlaufzeitKeyFigure.getValue() : 0;
    }

    public int getTteamLanglauferCount() {
        return tteamLanglauferCount;
    }

    public int getVereinbarungLanglauferCount() {
        return vereinbarungLanglauferCount;
    }

    public KeyFigure getKeyFigureByTypeId(int typeId) {
        return keyFigures.stream().filter(keyFigure -> keyFigure.getKeyTypeId() == typeId).findAny().orElse(null);
    }

    @Override
    public KeyFigure getKeyFigureByType(KeyType type) {
        return keyFigures.stream().filter(keyFigure -> keyFigure.getKeyType().equals(type)).findAny().orElse(null);
    }

}
