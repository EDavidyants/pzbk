package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model;

/**
 *
 * @author sl
 */
public class ProjectReportingProcessKeyFigureDataDto implements ProjectReportingProcessKeyFigureData {

    private final ProjectReportingProcessKeyFigure reportingProcessKeyFigure;
    private final Long value;
    private final boolean ampelGreen;

    protected ProjectReportingProcessKeyFigureDataDto(ProjectReportingProcessKeyFigure reportingProcessKeyFigure, Long value) {
        this.reportingProcessKeyFigure = reportingProcessKeyFigure;
        this.value = value;
        this.ampelGreen = false;
    }

    protected ProjectReportingProcessKeyFigureDataDto(ProjectReportingProcessKeyFigure reportingProcessKeyFigure, Long value, boolean isAmpelGreen) {
        this.reportingProcessKeyFigure = reportingProcessKeyFigure;
        this.value = value;
        this.ampelGreen = isAmpelGreen;
    }

    @Override
    public String toString() {
        return "ProjectReportingProcessKeyFigureDataDto{" + "keyFigure=" + reportingProcessKeyFigure + ", value=" + value + '}';
    }

    @Override
    public ProjectReportingProcessKeyFigure getKeyFigure() {
        return reportingProcessKeyFigure;
    }

    @Override
    public Long getValue() {
        return value;
    }

    @Override
    public boolean isAmpelGreen() {
        return ampelGreen;
    }

}
