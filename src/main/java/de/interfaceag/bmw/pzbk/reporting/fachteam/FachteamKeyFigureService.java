package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;
import de.interfaceag.bmw.pzbk.filter.TechnologieFilter;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.langlaufer.FachteamReportingLanglauferService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.StatusTransitionKeyFigureService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static de.interfaceag.bmw.pzbk.reporting.ReportingUtils.getSearchFilterObjectsForId;

/**
 * @author sl
 */
@Stateless
@Named
public class FachteamKeyFigureService implements Serializable {

    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private StatusTransitionKeyFigureService statusTransitionKeyFigureService;
    @Inject
    private LocalizationService localizationService;
    @Inject
    private FachteamReportingLanglauferService fachteamReportingLanglauferService;

    public FachteamKeyFigures getFachteamKeyFigures(ReportingFilter filter) {

        final Collection<SensorCoc> sensorCocs = getSensorCocIdsForTechnologieFilter(filter);

        List<FachteamKeyFigure> fachteamKeyFigures = new ArrayList<>();

        for (SensorCoc sensorCoc : sensorCocs) {
            final FachteamKeyFigure keyFiguresForSensorCoc = getKeyFiguresForSensorCoc(sensorCoc, filter);
            fachteamKeyFigures.add(keyFiguresForSensorCoc);
        }

        addSumKeyFigure(fachteamKeyFigures);

        return new FachteamKeyFigures(fachteamKeyFigures);
    }

    private void addSumKeyFigure(Collection<FachteamKeyFigure> fachteamKeyFigures) {
        final String label = localizationService.getValue("fachteam_detail_table_summe");
        fachteamKeyFigures.add(FachteamSumKeyFigureCalculator.computeSumKeyFigures(fachteamKeyFigures, label));
    }

    private Collection<SensorCoc> getSensorCocIdsForTechnologieFilter(ReportingFilter filter) {
        final TechnologieFilter technologieFilter = filter.getTechnologieFilter();
        Set<String> technologien = new HashSet<>(technologieFilter.getSelectedStringValues());

        if (technologieFilter.isActive()) {
            return sensorCocService.getSensorCocsByOrtung(technologien);
        } else {
            return Collections.emptySet();
        }
    }

    private FachteamKeyFigure getKeyFiguresForSensorCoc(SensorCoc sensorCoc, ReportingFilter filter) {
        final IdSearchFilter sensorCocFilter = filter.getSensorCocFilter();
        final Set<SearchFilterObject> searchFilterObjectsForId = getSearchFilterObjectsForId(sensorCocFilter, sensorCoc.getSensorCocId());
        sensorCocFilter.setSelectedValues(searchFilterObjectsForId);

        final KeyFigureCollection keyFigures = statusTransitionKeyFigureService.getKeyFiguresForFachteamReporting(filter);
        final DurchlaufzeitKeyFigureCollection durchlaufzeitKeyFigures = statusTransitionKeyFigureService.computeDurchlaufzeitKeyFigures(filter);
        final DurchlaufzeitKeyFigure fachteamDurchlaufzeitKeyFigure = durchlaufzeitKeyFigures.getByKeyType(KeyType.DURCHLAUFZEIT_FACHTEAM);

        final int langlauferCount = fachteamReportingLanglauferService.getFachteamLanglauferCountBySensorCoc(sensorCoc);

        return new FachteamKeyFigure(sensorCoc.getSensorCocId(), sensorCoc.getTechnologie(), keyFigures, fachteamDurchlaufzeitKeyFigure, langlauferCount);
    }

}
