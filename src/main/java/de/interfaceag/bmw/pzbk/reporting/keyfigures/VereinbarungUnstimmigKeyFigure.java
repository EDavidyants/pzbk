package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class VereinbarungUnstimmigKeyFigure extends AbstractKeyFigure {

    public VereinbarungUnstimmigKeyFigure(Collection<Long> ids) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.VEREINBARUNG_UNSTIMMIG);
    }

    public VereinbarungUnstimmigKeyFigure(Collection<Long> ids, Long runTime) {
        super(ids.size(), IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.VEREINBARUNG_UNSTIMMIG, runTime);
    }

}
