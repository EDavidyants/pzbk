package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class TteamGeloeschtKeyFigure extends AbstractKeyFigure {

    public TteamGeloeschtKeyFigure(Collection<Long> ids) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.TTEAM_GELOESCHT);
    }

    public TteamGeloeschtKeyFigure(Collection<Long> ids, Long runTime) {
        super(ids.size(), IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.TTEAM_GELOESCHT, runTime);
    }

}
