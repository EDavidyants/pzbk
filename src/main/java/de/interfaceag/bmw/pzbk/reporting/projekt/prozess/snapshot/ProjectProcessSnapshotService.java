package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures.ProjectReportingKeyFigureService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author sl
 */
@Named
@Stateless
public class ProjectProcessSnapshotService implements ProjectProcessSnapshotReadService, ProjectProcessSnapshotWriteService {

    public static final Logger LOG = LoggerFactory.getLogger(ProjectProcessSnapshotService.class);

    @Inject
    private ProjectProcessSnapshotDao statusabgleichSnapshotDao;
    @Inject
    private ProjectReportingKeyFigureService kennzahlenService;

    @Override
    public ProjectProcessSnapshot createNewSnapshotForDerivat(Derivat derivat) {
        LOG.debug("Create new snapshot for derivat {}", derivat);

        ProjectProcessFilter filter = kennzahlenService.buildProjectProcessFilterForDerivat();
        ProjectReportingProcessKeyFigureDataCollection keyFigureDataCollection = kennzahlenService.computeKeyFiguresForDerivat(derivat, filter);
        Collection<ProjectProcessSnapshotEntry> snapshotEntries = ProjectProcessSnapshotConverter.toProjectProcessSnapshotEntries(keyFigureDataCollection);

        ProjectProcessSnapshot snapshot = new ProjectProcessSnapshot(derivat, snapshotEntries);

        LOG.debug("Snapshot {} created", snapshot);

        save(snapshot);
        return snapshot;
    }

    @Override
    public List<SnapshotFilter> getSnapshotDatesForDerivat(Derivat derivat) {
        return statusabgleichSnapshotDao.getAllDatesForDerivat(derivat);
    }

    @Override
    public Optional<ProjectProcessSnapshot> getForDerivatAndDate(Derivat derivat, Date date) {
        return statusabgleichSnapshotDao.getForDerivatAndDate(derivat, date);
    }

    @Override
    public Optional<ProjectProcessSnapshot> getById(Long id) {
        return statusabgleichSnapshotDao.getById(id);
    }

    @Override
    public Optional<ProjectProcessSnapshot> getLatestSnapshotForDerivat(Derivat derivat) {
        List<ProjectProcessSnapshot> allForDerivat = statusabgleichSnapshotDao.getAllForDerivat(derivat);
        if (allForDerivat.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(allForDerivat.get(0));
        }
    }

    private void save(ProjectProcessSnapshot snapshot) {
        statusabgleichSnapshotDao.save(snapshot);
    }

}
