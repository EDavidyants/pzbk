package de.interfaceag.bmw.pzbk.reporting.tteam;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class TteamKeyFigures implements Serializable {

    private final List<TteamKeyFigure> keyFigures;

    public TteamKeyFigures(List<TteamKeyFigure> keyFigures) {
        this.keyFigures = keyFigures;
    }

    public Collection<TteamKeyFigure> getKeyFigures() {
        return keyFigures;
    }

}
