package de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht;

/**
 *
 * @author evda
 */
public interface AnforderungsuebersichtDialogMethods {

    void downloadExcelExportForKeyFigure();

    String navigateToAnforderungOrMeldungView(String fachId);

    int sortByFachIdAndVersion(Object anforderung1, Object anforderung2);

}
