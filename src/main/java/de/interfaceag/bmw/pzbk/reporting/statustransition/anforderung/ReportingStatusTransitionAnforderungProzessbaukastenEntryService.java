package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

@Dependent
public class ReportingStatusTransitionAnforderungProzessbaukastenEntryService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(ReportingStatusTransitionAnforderungProzessbaukastenEntryService.class);

    @Inject
    private ReportingStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Inject
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Inject
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Inject
    private ReportingStatusTransitionAnforderungEntyDateService entyDateService;
    @Inject
    private ReportingStatusTransitionAnforderungStatusChangeService statusTransitionAnforderungStatusChangeService;

    public ReportingStatusTransition addAnforderungToProzessbaukasten(Anforderung anforderung, Status currentStatus, Date date) {

        validateInput(anforderung, currentStatus);

        LOG.debug("Write ReportingStatusTransition for Anforderung {} Zuordnung to Prozessbaukasten at status {}", anforderung, currentStatus);

        final Optional<ReportingStatusTransition> latestStatusTransitionForAnforderung = statusTransitionDatabaseAdapter.getLatestForAnforderung(anforderung);

        final ReportingStatusTransition statusTransition;
        if (latestStatusTransitionForAnforderung.isPresent()) {
            LOG.debug("Found latest status transition and update or create new instance if necessary");
            statusTransition = updateReportingStatusTransition(latestStatusTransitionForAnforderung.get(), currentStatus, date);
        } else {
            LOG.debug("Found no status transition --> create new instance");
            Date entryDate = entyDateService.getEntryDateForAnforderung(anforderung);
            statusTransition = createNewReportingStatusTransitionForAnforderung(anforderung, currentStatus, entryDate, date);
        }
        statusTransitionDatabaseAdapter.save(statusTransition);
        return statusTransition;
    }

    private void validateInput(Anforderung anforderung, Status currentStatus) {
        if (currentStatus.equals(Status.A_GELOESCHT) || currentStatus.equals(Status.A_KEINE_WEITERVERFOLG)) {
            throw new IllegalArgumentException("Tried to add Anforderung " + anforderung + " to Prozessbaukasten with invalid Anforderung status value " + currentStatus + "!");
        }
    }

    private ReportingStatusTransition updateReportingStatusTransition(ReportingStatusTransition reportingStatusTransition, Status currentStatus, Date date) {
        updateExitForStatus(reportingStatusTransition, currentStatus, date);
        return updateEntryToProzessbaukastenForStatus(reportingStatusTransition, currentStatus, date);
    }

    private void updateExitForStatus(ReportingStatusTransition reportingStatusTransition, Status currentStatus, Date date) {
        statusTransitionAnforderungStatusChangeService.updateExitForStatus(reportingStatusTransition, currentStatus, date);
    }

    private ReportingStatusTransition updateEntryToProzessbaukastenForStatus(ReportingStatusTransition statusTransition, Status currentStatus, Date date) {
        switch (currentStatus) {
            case A_INARBEIT:
            case A_UNSTIMMIG:
                updateEntryToFachteamProzessbaukasten(statusTransition, date);
                break;
            case A_FTABGESTIMMT:
                updateEntryToTteamProzessbaukasten(statusTransition, date);
                break;
            case A_PLAUSIB:
                updateEntryToVereinbarungProzessbaukasten(statusTransition, date);
                break;
            case A_FREIGEGEBEN:
                updateEntryToFreigegebenProzessbaukasten(statusTransition, date);
                break;
            default:
                break;
        }
        return statusTransition;
    }

    private void updateEntryToFreigegebenProzessbaukasten(ReportingStatusTransition statusTransition, Date date) {
        final EntryExitTimeStamp freigegebenProzessbaukasten = statusTransition.getFreigegebenProzessbaukasten();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(freigegebenProzessbaukasten, date);
        statusTransition.setFreigegebenProzessbaukasten(timeStamp);
    }

    private void updateEntryToFachteamProzessbaukasten(ReportingStatusTransition statusTransition, Date date) {
        final EntryExitTimeStamp fachteamProzessbaukasten = statusTransition.getFachteamProzessbaukasten();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(fachteamProzessbaukasten, date);
        statusTransition.setFachteamProzessbaukasten(timeStamp);
    }

    private void updateEntryToTteamProzessbaukasten(ReportingStatusTransition statusTransition, Date date) {
        final EntryExitTimeStamp tteamProzessbaukasten = statusTransition.getTteamProzessbaukasten();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(tteamProzessbaukasten, date);
        statusTransition.setTteamProzessbaukasten(timeStamp);
    }

    private void updateEntryToVereinbarungProzessbaukasten(ReportingStatusTransition statusTransition, Date date) {
        final EntryExitTimeStamp vereinbarungProzessbaukasten = statusTransition.getVereinbarungProzessbaukasten();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(vereinbarungProzessbaukasten, date);
        statusTransition.setVereinbarungProzessbaukasten(timeStamp);
    }

    private ReportingStatusTransition createNewReportingStatusTransitionForAnforderung(Anforderung anforderung, Status currentStatus, Date entryDate, Date date) {
        LOG.debug("Create new ReportingStatusTransition instance for anforderung {}", anforderung);
        ReportingStatusTransition statusTransition = reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(anforderung, entryDate);
        return updateReportingStatusTransition(statusTransition, currentStatus, date);
    }
}
