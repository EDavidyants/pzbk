package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.enums.Type;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class IdTypeTupleCollection extends AbstractList<IdTypeTuple> {

    private final List<IdTypeTuple> idTypeTuples;

    public IdTypeTupleCollection() {
        this.idTypeTuples = new ArrayList<>();
    }

    public IdTypeTupleCollection(Collection<IdTypeTuple> idTypeTuples) {
        this.idTypeTuples = new ArrayList<>(idTypeTuples);
    }

    @Override
    public boolean add(IdTypeTuple idTypeTuple) {
        return idTypeTuples.add(idTypeTuple);
    }

    @Override
    public Iterator<IdTypeTuple> iterator() {
        return idTypeTuples.iterator();
    }

    @Override
    public int size() {
        return idTypeTuples.size();
    }

    public Collection<Long> getIdsForType(Type type) {
        return idTypeTuples.stream().filter(tuple -> tuple.getX().equals(type)).map(tuple -> tuple.getY()).collect(Collectors.toList());
    }

    /**
     * new empty IdTypeTupleCollection.
     *
     * @return empty IdTypeTupleCollection.
     */
    public static IdTypeTupleCollection empty() {
        return new IdTypeTupleCollection();
    }

    @Override
    public IdTypeTuple get(int index) {
        return idTypeTuples.get(index);
    }
}
