package de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot;

import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author sl
 */
public class StatusabgleichSnapshotFilter implements SnapshotFilter {

    private final Long id;
    private final Date date;

    public StatusabgleichSnapshotFilter(Long id, Date date) {
        this.id = id;
        this.date = date;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        return dateFormat.format(date);
    }

}
