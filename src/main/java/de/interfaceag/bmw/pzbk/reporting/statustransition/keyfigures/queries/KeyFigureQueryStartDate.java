package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;

import java.util.Date;

final class KeyFigureQueryStartDate {

    private KeyFigureQueryStartDate() {
    }

    static Date getStartDate(ReportingFilter filter) {
        final DateSearchFilter vonDateFilter = filter.getVonDateFilter();
        return vonDateFilter.getSelected();
    }
}
