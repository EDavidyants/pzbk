package de.interfaceag.bmw.pzbk.reporting.svg;

import de.interfaceag.bmw.pzbk.shared.dto.Point;

import java.util.Arrays;

/**
 *
 * @author lomu
 */
public class GesamtdurchlaufzeitBox implements SVGElement {

    private final double x;
    private final double y;

    private final int durchlaufzeit;
    private final String tooltipValue;
    private final boolean showTooltips;

    private final TooltipCollection tooltipCollection = TooltipCollection.createCollection();

    private final GesamtdurchlaufzeitType gesamtdurchlaufzeitType;

    public GesamtdurchlaufzeitBox(int durchlaufzeit, String tooltip, boolean showTooltips, GesamtdurchlaufzeitType gesamtdurchlaufzeitType) {
        this.durchlaufzeit = durchlaufzeit;
        this.tooltipValue = tooltip;
        this.showTooltips = showTooltips;
        this.x = 0;
        this.y = 0;
        createTooltips();
        this.gesamtdurchlaufzeitType = gesamtdurchlaufzeitType;
    }

    public GesamtdurchlaufzeitBox(double xCoordinate, double yCoordinate, int durchlaufzeit, String tooltip, boolean showTooltips, GesamtdurchlaufzeitType gesamtdurchlaufzeitType) {
        this.x = xCoordinate;
        this.y = yCoordinate;
        this.durchlaufzeit = durchlaufzeit;
        this.tooltipValue = tooltip;
        this.showTooltips = showTooltips;
        createTooltips();
        this.gesamtdurchlaufzeitType = gesamtdurchlaufzeitType;
    }

    private void createTooltips() {
        if (showTooltips) {
            tooltipCollection.add(new Tooltip(x + 480, y + 385, tooltipValue));
        }
    }

    public int getDurchlaufzeit() {
        return durchlaufzeit;
    }
//<text class=\"5f459e6e-0411-4145-a215-c5424afc40fe\" transform=\"translate(501.06 483.72)\">Durchlaufzeit Gesamt</text>" +
    //0, 85

    @Override
    public String draw() {
        SvgElementCollection elementCollection = new SvgElementCollection();
        elementCollection.add(new Rectangle(gesamtdurchlaufzeitType.rectangeClass1, gesamtdurchlaufzeitType.rectangeClass2, gesamtdurchlaufzeitType.rectangeClass3, x + 248, y + 351.5, 641, 55.5));
        elementCollection.add(new Text(gesamtdurchlaufzeitType.text1Class, "translate(" + (x + 554.95) + " " + (y + 370.56) + ")", Integer.toString(this.getDurchlaufzeit())));
        elementCollection.add(new Text(gesamtdurchlaufzeitType.text1Class, "translate(" + (x + 501.06) + " " + (y + 398.73) + ")", "Durchlaufzeit Gesamt"));
        elementCollection.add(new Line(gesamtdurchlaufzeitType.lineClass, x + 257.17, y + 378.97, x + 879.77, y + 378.97));
        elementCollection.add(new Polygon(gesamtdurchlaufzeitType.polygon1Class, Arrays.asList(new Point(x + 252.52, y + 378.97), new Point(x + 259.22, y + 385.2), new Point(x + 259.22, y + 372.74), new Point(x + 252.52, y + 378.97))));
        elementCollection.add(new Polygon(gesamtdurchlaufzeitType.polygon2Class, Arrays.asList(new Point(x + 884.42, y + 378.97), new Point(x + 877.72, y + 372.74), new Point(x + 877.72, y + 385.2), new Point(x + 884.42, y + 378.97))));
        return elementCollection.draw();
    }

    public TooltipCollection getTooltipCollection() {
        return tooltipCollection;
    }

    public enum GesamtdurchlaufzeitType {
        SIMPLE("e74f067f-41ce-4693-b2d0-e5ae056d20f0", "93fdd1a2-f0db-421b-ab67-13a781186ef8", "c09415f1-be70-4123-b273-3f949364e3b0", "ffa89ab7-8120-4f9d-b7d7-33e849fbefce", "ffa89ab7-8120-4f9d-b7d7-33e849fbefce", "e7979375-7020-4a99-8f60-046cf1838a15", "b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", "b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4"),
        DETAIL("592c5a5e-ada3-49a0-83c0-bc6f96857966", "5457bff4-c760-4146-a628-412b3b58f175", "cb17c188-b9c0-44f6-89fc-25349d833054", "5f459e6e-0411-4145-a215-c5424afc40fe", "5f459e6e-0411-4145-a215-c5424afc40fe", "e7979375-7020-4a99-8f60-046cf1838a15", "b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", "b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4");

        private final String rectangeClass1;
        private final String rectangeClass2;
        private final String rectangeClass3;
        private final String text1Class;
        private final String text2Class;
        private final String lineClass;
        private final String polygon1Class;
        private final String polygon2Class;

        GesamtdurchlaufzeitType(String rectangeClass1, String rectangeClass2, String rectangeClass3, String text1Class, String text2Class, String lineClass, String polygon1Class, String polygon2Class) {
            this.rectangeClass1 = rectangeClass1;
            this.rectangeClass2 = rectangeClass2;
            this.rectangeClass3 = rectangeClass3;
            this.text1Class = text1Class;
            this.text2Class = text2Class;
            this.lineClass = lineClass;
            this.polygon1Class = polygon1Class;
            this.polygon2Class = polygon2Class;
        }

        public String getRectangeClass1() {
            return rectangeClass1;
        }

        public String getRectangeClass2() {
            return rectangeClass2;
        }

        public String getRectangeClass3() {
            return rectangeClass3;
        }

        public String getText1Class() {
            return text1Class;
        }

        public String getText2Class() {
            return text2Class;
        }

        public String getLineClass() {
            return lineClass;
        }

        public String getPolygon1Class() {
            return polygon1Class;
        }

        public String getPolygon2Class() {
            return polygon2Class;
        }

    }

}
