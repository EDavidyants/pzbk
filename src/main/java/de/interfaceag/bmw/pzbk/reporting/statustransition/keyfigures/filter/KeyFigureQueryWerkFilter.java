package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;

import java.util.Set;

public final class KeyFigureQueryWerkFilter {

    private KeyFigureQueryWerkFilter() {

    }

    public static void append(ReportingFilter filter, QueryPart queryPart) {
        final IdSearchFilter werkFilter = filter.getWerkFilter();

        if (werkFilter.isActive()) {
            final Set<Long> werke = werkFilter.getSelectedIdsAsSet();
            queryPart.append(" AND a.werk.id IN :werke ");
            queryPart.put("werke", werke);
        }
    }

}
