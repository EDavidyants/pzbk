package de.interfaceag.bmw.pzbk.reporting.processoverview;

import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.reporting.excel.ReportingExcelDataDto;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 *
 * @author fn
 */
public class ProcessOverviewExcelData {

    private final Collection<ReportingExcelDataDto> reportingExcelDataDtos;

    public ProcessOverviewExcelData(Collection<ReportingExcelDataDto> reportingElements) {
        this.reportingExcelDataDtos = new ArrayList<>(reportingElements);
    }

    public Collection<ReportingExcelDataDto> getExcelDataForKeyFigure(KeyFigure keyFigure) {

        Collection<Long> anforderungIds = keyFigure.getAnforderungIds();
        Collection<Long> meldungIds = keyFigure.getMeldungIds();

        Collection<ReportingExcelDataDto> result = reportingExcelDataDtos.stream()
                .filter(excelData -> excelData.getType().equals(Type.ANFORDERUNG))
                .filter(excelData -> anforderungIds.contains(excelData.getId()))
                .collect(Collectors.toList());

        result.addAll(reportingExcelDataDtos.stream()
                .filter(excelData -> excelData.getType().equals(Type.MELDUNG))
                .filter(excelData -> meldungIds.contains(excelData.getId()))
                .collect(Collectors.toList()));

        return result;
    }

}
