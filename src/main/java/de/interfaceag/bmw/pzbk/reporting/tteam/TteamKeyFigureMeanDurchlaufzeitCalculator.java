package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.shared.math.Mean;

import java.util.Collection;
import java.util.stream.Stream;

final class TteamKeyFigureMeanDurchlaufzeitCalculator {

    private TteamKeyFigureMeanDurchlaufzeitCalculator() {
    }

    static int computeMeanTteamDurchlaufzeit(Collection<TteamKeyFigure> keyFigures) {
        final Stream<Integer> durchlaufzeiten = keyFigures.stream().map(TteamKeyFigure::getTteamDurchlaufzeit);
        return Mean.computeMean(durchlaufzeiten);
    }

    static int computeMeanVereinbarungDurchlaufzeit(Collection<TteamKeyFigure> keyFigures) {
        final Stream<Integer> durchlaufzeiten = keyFigures.stream().map(TteamKeyFigure::getVereinbarungDurchlaufzeit);
        return Mean.computeMean(durchlaufzeiten);
    }

}
