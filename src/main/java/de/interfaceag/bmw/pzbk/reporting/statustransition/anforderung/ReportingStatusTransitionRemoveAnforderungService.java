package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;


import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.util.List;

@Dependent
public class ReportingStatusTransitionRemoveAnforderungService {

    @Inject
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;


    public void removeAnforderungStatusTransitions(Anforderung anforderung) {
        List<ReportingStatusTransition> statusTransitions = statusTransitionDatabaseAdapter.getAllStatustransitionsForAnforderung(anforderung);

        statusTransitions.forEach(statusTransition -> statusTransitionDatabaseAdapter.remove(statusTransition));

    }
}
