package de.interfaceag.bmw.pzbk.reporting.statustransition.migration;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;

import java.util.List;

/**
 *
 * @author fn
 */
public interface ReportingStatusTransitionMigrationAdapter {

    ReportingStatusTransitionMigrationResult migrateAnforderung(Anforderung anforderung);

    List<ReportingStatusTransitionMigrationResult> migrateAnforderungen();

    ReportingMeldungStatusTransitionMigrationResult migrateMeldung(Meldung meldung);

    List<ReportingMeldungStatusTransitionMigrationResult> migrateMeldungen();

}
