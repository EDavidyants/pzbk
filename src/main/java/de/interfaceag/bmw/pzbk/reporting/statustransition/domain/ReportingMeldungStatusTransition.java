package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import de.interfaceag.bmw.pzbk.entities.Meldung;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(name = ReportingMeldungStatusTransition.LATEST, query = "SELECT r FROM ReportingMeldungStatusTransition r WHERE r.meldung.id = :meldungId ORDER BY r.id DESC")
})
public class ReportingMeldungStatusTransition implements Serializable {

    private static final String CLASSNAME = "ReportingMeldungStatusTransition";
    public static final String LATEST = CLASSNAME + ".latest";

    @Id
    @SequenceGenerator(name = "reportingmeldungstatustransition_id_seq",
            sequenceName = "reportingmeldungstatustransition_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reportingmeldungstatustransition_id_seq")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date entryDate;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Meldung meldung;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "gemeldet_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "gemeldet_exit"))
    })
    private EntryExitTimeStamp gemeldet;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "unstimmig_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "unstimmig_exit"))
    })
    private EntryExitTimeStamp unstimmig;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "geloescht_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "geloescht_exit"))
    })
    private EntryExitTimeStamp geloescht;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "newanforderungzugeordnet_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "newanforderungzugeordnet_exit"))
    })
    private EntryExitTimeStamp newAnforderungZugeordnet;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "entry", column = @Column(name = "existinganforderungzugeordnet_entry")),
            @AttributeOverride(name = "exit", column = @Column(name = "existinganforderungzugeordnet_exit"))
    })
    private EntryExitTimeStamp existingAnforderungZugeordnet;

    public ReportingMeldungStatusTransition() {
    }

    /**
     * Default constructor only for jpa. Use factory instead!
     */
    ReportingMeldungStatusTransition(Date entryDate, Meldung meldung, EntryExitTimeStamp gemeldet, EntryExitTimeStamp unstimmig,
                                     EntryExitTimeStamp geloescht, EntryExitTimeStamp newAnforderungZugeordnet,
                                     EntryExitTimeStamp existingAnforderungZugeordnet) {
        this.entryDate = entryDate;
        this.meldung = meldung;
        this.gemeldet = gemeldet;
        this.unstimmig = unstimmig;
        this.geloescht = geloescht;
        this.newAnforderungZugeordnet = newAnforderungZugeordnet;
        this.existingAnforderungZugeordnet = existingAnforderungZugeordnet;
    }

    static ReportingMeldungStatusTransitionBuilder getBuilder() {
        return new ReportingMeldungStatusTransitionBuilder();
    }

    public Long getId() {
        return id;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public Meldung getMeldung() {
        return meldung;
    }

    public EntryExitTimeStamp getGemeldet() {
        return gemeldet;
    }

    public void setGemeldet(EntryExitTimeStamp gemeldet) {
        this.gemeldet = gemeldet;
    }

    public EntryExitTimeStamp getUnstimmig() {
        return unstimmig;
    }

    public void setUnstimmig(EntryExitTimeStamp unstimmig) {
        this.unstimmig = unstimmig;
    }

    public EntryExitTimeStamp getGeloescht() {
        return geloescht;
    }

    public void setGeloescht(EntryExitTimeStamp geloescht) {
        this.geloescht = geloescht;
    }

    public EntryExitTimeStamp getNewAnforderungZugeordnet() {
        return newAnforderungZugeordnet;
    }

    public void setNewAnforderungZugeordnet(EntryExitTimeStamp newAnforderungZugeordnet) {
        this.newAnforderungZugeordnet = newAnforderungZugeordnet;
    }

    public EntryExitTimeStamp getExistingAnforderungZugeordnet() {
        return existingAnforderungZugeordnet;
    }

    public void setExistingAnforderungZugeordnet(EntryExitTimeStamp existingAnforderungZugeordnet) {
        this.existingAnforderungZugeordnet = existingAnforderungZugeordnet;
    }
}
