package de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 *
 * @author sl
 */
@Entity
public class StatusabgleichSnapshotEntry implements Serializable {

    @Id
    @SequenceGenerator(name = "statusabgleichsnapshotentry_id_seq",
            sequenceName = "statusabgleichsnapshotentry_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "statusabgleichsnapshotentry_id_seq")
    private Long id;

    @Column(nullable = false)
    private int vereinbarungStatus;

    @Column(nullable = false)
    private int zakStatus;

    @Column(nullable = false)
    private int entry;

    public StatusabgleichSnapshotEntry() {
    }

    public StatusabgleichSnapshotEntry(DerivatAnforderungModulStatus vereinbarungStatus, ZakStatus zakStatus, int entry) {
        this.vereinbarungStatus = vereinbarungStatus.getStatusId();
        this.zakStatus = zakStatus.getStatusId();
        this.entry = entry;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DerivatAnforderungModulStatus getVereinbarungStatus() {
        return DerivatAnforderungModulStatus.getStatusById(vereinbarungStatus);
    }

    public void setVereinbarungStatus(DerivatAnforderungModulStatus vereinbarungStatus) {
        this.vereinbarungStatus = vereinbarungStatus.getStatusId();
    }

    public ZakStatus getZakStatus() {
        return ZakStatus.getStatusById(zakStatus);
    }

    public void setZakStatus(ZakStatus zakStatus) {
        this.zakStatus = zakStatus.getStatusId();
    }

    public int getEntry() {
        return entry;
    }

    public void setEntry(int entry) {
        this.entry = entry;
    }

}
