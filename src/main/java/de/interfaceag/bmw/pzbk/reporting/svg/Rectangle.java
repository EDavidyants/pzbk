package de.interfaceag.bmw.pzbk.reporting.svg;

/**
 *
 * @author lomu
 */
public class Rectangle implements SVGElement {

    private final String rectangleClass1;
    private final String rectangleClass2;
    private final String rectangleClass3;
    private final String x;
    private final String y;
    private final String width;
    private final String heigth;

    public Rectangle(String class1, String class2, String class3, String xCoordinate, String yCoordinate, String width, String heigth) {
        this.rectangleClass1 = class1;
        this.rectangleClass2 = class2;
        this.rectangleClass3 = class3;
        this.x = xCoordinate;
        this.y = yCoordinate;
        this.width = width;
        this.heigth = heigth;
    }

    public Rectangle(String rectangleClass1, String rectangleClass2, String rectangleClass3, int xCoordinate, int yCoordinate, int width, int heigth) {
        this.rectangleClass1 = rectangleClass1;
        this.rectangleClass2 = rectangleClass2;
        this.rectangleClass3 = rectangleClass3;
        this.x = Integer.toString(xCoordinate);
        this.y = Integer.toString(yCoordinate);
        this.width = Integer.toString(width);
        this.heigth = Integer.toString(heigth);
    }

    public Rectangle(String rectangleClass1, String rectangleClass2, String rectangleClass3, double xCoordinate, double yCoordinate, double width, double heigth) {
        this.rectangleClass1 = rectangleClass1;
        this.rectangleClass2 = rectangleClass2;
        this.rectangleClass3 = rectangleClass3;
        this.x = Double.toString(xCoordinate);
        this.y = Double.toString(yCoordinate);
        this.width = Double.toString(width);
        this.heigth = Double.toString(heigth);
    }

    public String getRectangleClass1() {
        return rectangleClass1;
    }

    public String getRectangleClass2() {
        return rectangleClass2;
    }

    public String getRectangleClass3() {
        return rectangleClass3;
    }

    public String getX() {
        return x;
    }

    public String getY() {
        return y;
    }

    public String getWidth() {
        return width;
    }

    public String getHeigth() {
        return heigth;
    }

    @Override
    public String draw() {
        StringBuilder result = new StringBuilder();

        result.append("<g class=");
        result.append("\"").append(this.getRectangleClass1()).append("\">\n");

        result.append("<g class=");
        result.append("\"").append(this.getRectangleClass2()).append("\">\n");

        result.append("<rect class=");
        result.append("\"").append(this.rectangleClass3).append("\"");

        if (this.getX() != null) {
            result.append(" x=\"").append(this.getX()).append("\"");
        }

        if (this.getY() != null) {
            result.append(" y=\"").append(this.getY()).append("\"");
        }

        if (this.getWidth() != null) {
            result.append(" width=\"").append(this.getWidth()).append("\"");
        }

        if (this.getHeigth() != null) {
            result.append(" height=\"").append(this.getHeigth()).append("\"");
        }

        result.append("/>\n");

        result.append("</g>\n");
        result.append("</g>\n");

        return result.toString();
    }

}
