package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class VereinbarungGeloeschtKeyFigure extends AbstractKeyFigure {

    public VereinbarungGeloeschtKeyFigure(Collection<Long> ids) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.VEREINBARUNG_GELOESCHT);
    }

    public VereinbarungGeloeschtKeyFigure(Collection<Long> ids, Long runTime) {
        super(ids.size(),
                IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids),
                KeyType.VEREINBARUNG_GELOESCHT, runTime);
    }

}
