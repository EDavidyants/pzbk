package de.interfaceag.bmw.pzbk.reporting.processoverview;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

public class ReportingProcessOverviewViewPermission implements Serializable {

    private final ViewPermission page;

    public ReportingProcessOverviewViewPermission() {
        page = FalsePermission.get();
    }

    public ReportingProcessOverviewViewPermission(Set<Rolle> userRoles) {
        page = getPageViewPermisison(userRoles);
    }

    private static ViewPermission getPageViewPermisison(Set<Rolle> userRoles) {
        return RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.E_COC)
                .compareTo(userRoles).get();
    }

    public boolean isPage() {
        return page.isRendered();
    }

}
