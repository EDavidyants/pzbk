package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FreigegebenGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Date;

final class FreigegebenGeloeschtKeyFigureQuery {

    private static final Logger LOG = LoggerFactory.getLogger(FreigegebenGeloeschtKeyFigureQuery.class.getName());

    private FreigegebenGeloeschtKeyFigureQuery() {
    }

    static FreigegebenGeloeschtKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {
        Collection<Long> value = getAnforderungIdsForFreigegebenGeloeschtEntry(filter, entityManager);
        return new FreigegebenGeloeschtKeyFigure(value);
    }

    private static Collection<Long> getAnforderungIdsForFreigegebenGeloeschtEntry(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO("SELECT a.id ");
        queryPart.append(" FROM ReportingStatusTransition r ")
                .append(" INNER JOIN r.anforderung a ")
                .append(" WHERE (r.keineWeiterverfolgung.entry < :endDate ")
                .append(" AND r.keineWeiterverfolgung.exit IS NULL ")
                .append(" OR (r.freigegebenGeloescht.entry < :endDate ")
                .append(" AND r.freigegebenGeloescht.exit IS NULL)) ");
        queryPart.put("endDate", endDate);

        KeyFigureQueryDateFilter.append(filter, queryPart);

        LOG.debug("getAnforderungIdsForFreigegebenGeloeschtEntry Query: {}", queryPart.getQuery());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

}
