package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author sl
 */
public final class SumKeyFigureUtils {

    private SumKeyFigureUtils() {
    }

    public static ProjectReportingProcessKeyFigureData computeDifferenceKeyFigure(ProjectReportingProcessKeyFigure keyFigure, Long minuend, Collection<ProjectReportingProcessKeyFigureData> subtrahends) {
        Long subtrahend = computeSum(subtrahends);
        Long value = minuend - subtrahend;
        value = Math.max(0, value);
        return ProjectReportingProcessKeyFigureDataBuilder
                .withKeyFigure(keyFigure)
                .withValue(value)
                .build();
    }

    public static Collection<ProjectReportingProcessKeyFigureData> getKeyFiguresDataFromMap(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigureData, ProjectReportingProcessKeyFigure... keyFigure) {

        Collection<ProjectReportingProcessKeyFigure> keyFigures = Arrays.asList(keyFigure);
        Iterator<ProjectReportingProcessKeyFigure> iterator = keyFigures.iterator();

        Collection<ProjectReportingProcessKeyFigureData> result = new ArrayList<>();

        while (iterator.hasNext()) {
            ProjectReportingProcessKeyFigure next = iterator.next();
            getKeyFigureDataForKeyFigure(keyFigureData, next).ifPresent(data -> result.add(data));
        }

        return result;
    }

    private static Optional<ProjectReportingProcessKeyFigureData> getKeyFigureDataForKeyFigure(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigureData, ProjectReportingProcessKeyFigure keyFigure) {
        Integer key = keyFigure.getId();

        if (keyFigureData.containsKey(key)) {
            return Optional.ofNullable(keyFigureData.get(key));
        } else {
            return Optional.empty();
        }

    }

    public static ProjectReportingProcessKeyFigureData computeSumKeyFigure(ProjectReportingProcessKeyFigure keyFigure, Collection<ProjectReportingProcessKeyFigureData> summands) {

        Long value = computeSum(summands);

        return ProjectReportingProcessKeyFigureDataBuilder
                .withKeyFigure(keyFigure)
                .withValue(value)
                .build();
    }

    public static ProjectReportingProcessKeyFigureData computeRatioKeyFigure(ProjectReportingProcessKeyFigure keyFigure, Collection<ProjectReportingProcessKeyFigureData> numerator, Collection<ProjectReportingProcessKeyFigureData> denominator, Double thresholdForGreenAmpel) {

        Long numeratorSum = computeSum(numerator);
        Long denominatorSum = computeSum(denominator);

        boolean isAmpelGreen;
        Long value;
        if (denominatorSum.equals(0L)) {
            value = 100L;
            isAmpelGreen = true;
        } else {
            Double ratio = 100.0 * numeratorSum / denominatorSum;
            value = Math.round(ratio);
            isAmpelGreen = value >= (thresholdForGreenAmpel * 100);
        }

        return ProjectReportingProcessKeyFigureDataBuilder
                .withKeyFigure(keyFigure)
                .withValue(value)
                .withAmpel(isAmpelGreen)
                .build();
    }

    private static Long computeSum(Collection<ProjectReportingProcessKeyFigureData> summands) {
        if (summands.isEmpty()) {
            return 0L;
        } else {
            return summands.stream().map(keyFigureData -> keyFigureData.getValue()).reduce(Long::sum).orElse(0L);
        }
    }

}
