package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessAmpelThresholdService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataBuilder;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 *
 * @author sl
 */
@Named
@Stateless
public class ProjectReportingVereinbarungKeyFigureService {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingVereinbarungKeyFigureService.class);

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    @Inject
    private ProjectProcessAmpelThresholdService ampelThresholdService;

    public Map<Integer, ProjectReportingProcessKeyFigureData> getKeyFiguresForVereinbarungVKBG(Derivat derivat, ProjectProcessFilter filter) {

        LOG.debug("Compute Vereinbarung VKBG keyfigures for derivat {}", derivat);

        if (derivat == null) {
            LOG.error("Derivat is null! Return zero for all values.");
            return Collections.emptyMap();
        }

        Collection<Object[]> queryDataForVereinbarungVKBG = getQueryDataForVereinbarungVKBG(derivat, filter);

        Map<Integer, ProjectReportingProcessKeyFigureData> result = convertToKeyFigureData(queryDataForVereinbarungVKBG, (DerivatAnforderungModulStatus status) -> getKeyFigureForVereinbarungVKBGStatus(status));

        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdForVereinbarungVKBG();
        result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungVKBG(result, thresholdForGreenAmpel);

        LOG.debug("Result for Vereinbarung VKBG {}", result);

        return result;

    }

    public Map<Integer, ProjectReportingProcessKeyFigureData> getKeyFiguresForVereinbarungZV(Derivat derivat, ProjectProcessFilter filter) {

        LOG.debug("Compute Vereinbarung ZV keyfigures for derivat {}", derivat);

        if (derivat == null) {
            LOG.error("Derivat is null! Return zero for all values.");
            return Collections.emptyMap();
        }

        Collection<Object[]> queryDataForVereinbarungZV = getQueryDataForVereinbarungZV(derivat, filter);

        Map<Integer, ProjectReportingProcessKeyFigureData> result = convertToKeyFigureData(queryDataForVereinbarungZV, (DerivatAnforderungModulStatus status) -> getKeyFigureForVereinbarungZVStatus(status));

        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdForVereinbarungZV();
        result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungZV(result, thresholdForGreenAmpel);

        LOG.debug("Result for Vereinbarung ZV {}", result);

        return result;

    }

    private static Map<Integer, ProjectReportingProcessKeyFigureData> convertToKeyFigureData(Collection<Object[]> queryResults, Function<DerivatAnforderungModulStatus, ProjectReportingProcessKeyFigure> statusToKeyFigure) {
        Iterator<Object[]> iterator = queryResults.iterator();
        Map<Integer, ProjectReportingProcessKeyFigureData> result = new HashMap<>();
        while (iterator.hasNext()) {
            Object[] queryResult = iterator.next();

            Integer statusId = (Integer) queryResult[0];
            Long value = (Long) queryResult[1];

            DerivatAnforderungModulStatus status = DerivatAnforderungModulStatus.getStatusById(statusId);

            try {
                if (status != null) {
                    ProjectReportingProcessKeyFigure keyFiguresForVereinbarungZVStatus = statusToKeyFigure.apply(status);

                    ProjectReportingProcessKeyFigureData keyFigureData = getKeyFigureData(keyFiguresForVereinbarungZVStatus, value);
                    Integer keyFigureId = keyFiguresForVereinbarungZVStatus.getId();

                    result.put(keyFigureId, keyFigureData);
                }
            } catch (AssertionError assertionError) {
                LOG.debug("AssertionError", assertionError);
            }
        }
        return result;
    }

    private static ProjectReportingProcessKeyFigure getKeyFigureForVereinbarungVKBGStatus(DerivatAnforderungModulStatus status) {
        switch (status) {
            case ANGENOMMEN:
                return ProjectReportingProcessKeyFigure.L4;
            case IN_KLAERUNG:
                return ProjectReportingProcessKeyFigure.L5;
            case ABZUSTIMMEN:
                return ProjectReportingProcessKeyFigure.L6;
            case NICHT_BEWERTBAR:
                return ProjectReportingProcessKeyFigure.L7;
            case KEINE_WEITERVERFOLGUNG:
                return ProjectReportingProcessKeyFigure.L8;
            case UNZUREICHENDE_ANFORDERUNGSQUALITAET:
                return ProjectReportingProcessKeyFigure.L9;
            default:
                throw new AssertionError(status.name());
        }
    }

    private static ProjectReportingProcessKeyFigure getKeyFigureForVereinbarungZVStatus(DerivatAnforderungModulStatus status) {
        switch (status) {
            case ANGENOMMEN:
                return ProjectReportingProcessKeyFigure.R6;
            case IN_KLAERUNG:
                return ProjectReportingProcessKeyFigure.R7;
            case ABZUSTIMMEN:
                return ProjectReportingProcessKeyFigure.R8;
            case NICHT_BEWERTBAR:
                return ProjectReportingProcessKeyFigure.R9;
            case KEINE_WEITERVERFOLGUNG:
                return ProjectReportingProcessKeyFigure.R10;
            case UNZUREICHENDE_ANFORDERUNGSQUALITAET:
                return ProjectReportingProcessKeyFigure.R11;
            default:
                throw new AssertionError(status.name());
        }
    }

    private static ProjectReportingProcessKeyFigureData getKeyFigureData(ProjectReportingProcessKeyFigure keyFigure, Long value) {
        return ProjectReportingProcessKeyFigureDataBuilder
                .withKeyFigure(keyFigure)
                .withValue(value)
                .build();
    }

    private List<Object[]> getQueryDataForVereinbarungVKBG(Derivat derivat, ProjectProcessFilter filter) {
        return getQueryDataForVereinbarung(derivat, "statusVKBG", filter);
    }

    private List<Object[]> getQueryDataForVereinbarungZV(Derivat derivat, ProjectProcessFilter filter) {
        return getQueryDataForVereinbarung(derivat, "statusZV", filter);
    }

    private List<Object[]> getQueryDataForVereinbarung(Derivat derivat, String status, ProjectProcessFilter filter) {
        QueryPart queryPart = new QueryPartDTO(getQueryForVereinbarungStatus(status, filter));
        queryPart.put("derivatId", derivat.getId());

        if (!filter.isSnapshot() && filter.getTechnologieFilter().isActive()) {
            List<String> filters = filter.getTechnologieFilter().getSelectedValuestringsAsList();
            queryPart.put("technologie", filters);
        }

        Query query = queryPart.buildGenericQuery(entityManager);
        LOG.debug("Excecute query: {}", query.toString());
        LOG.debug("Query parameters: {}", query.getParameters());

        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    private static String getQueryForVereinbarungStatus(String status, ProjectProcessFilter filter) {
        StringBuilder stringBuilder = new StringBuilder("SELECT dam.");
        stringBuilder.append(status)
                .append(", COUNT(dam.id) FROM ZakUebertragung zak "
                        + "INNER JOIN zak.derivatAnforderungModul dam "
                        + "INNER JOIN dam.zuordnungAnforderungDerivat zad "
                        + "INNER JOIN zad.derivat d "
                        + "INNER JOIN zad.anforderung a "
                        + "INNER JOIN a.sensorCoc coc "
                        + "WHERE d.id = :derivatId ");

        if (!filter.isSnapshot() && filter.getTechnologieFilter().isActive()) {
            stringBuilder.append("AND coc.ortung IN :technologie ");
        }

        stringBuilder.append("AND dam.")
                .append(status)
                .append(" IS NOT NULL GROUP BY dam.")
                .append(status);
        return stringBuilder.toString();
    }

}
