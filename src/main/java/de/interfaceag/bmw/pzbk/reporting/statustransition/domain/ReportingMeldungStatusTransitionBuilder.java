package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import de.interfaceag.bmw.pzbk.entities.Meldung;

import java.util.Date;

final class ReportingMeldungStatusTransitionBuilder {

    private Date entryDate;
    private Meldung meldung;
    private EntryExitTimeStamp gemeldet;
    private EntryExitTimeStamp unstimmig;
    private EntryExitTimeStamp geloescht;
    private EntryExitTimeStamp newAnforderungZugeordnet;
    private EntryExitTimeStamp existingAnforderungZugeordnet;

    ReportingMeldungStatusTransitionBuilder() {
    }

    ReportingMeldungStatusTransitionBuilder setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
        return this;
    }

    ReportingMeldungStatusTransitionBuilder setMeldung(Meldung meldung) {
        this.meldung = meldung;
        return this;
    }

    ReportingMeldungStatusTransitionBuilder setGemeldet(EntryExitTimeStamp gemeldet) {
        this.gemeldet = gemeldet;
        return this;
    }

    ReportingMeldungStatusTransitionBuilder setUnstimmig(EntryExitTimeStamp unstimmig) {
        this.unstimmig = unstimmig;
        return this;
    }

    ReportingMeldungStatusTransitionBuilder setGeloescht(EntryExitTimeStamp geloescht) {
        this.geloescht = geloescht;
        return this;
    }

    ReportingMeldungStatusTransitionBuilder setNewAnforderungZugeordnet(EntryExitTimeStamp newAnforderungZugeordnet) {
        this.newAnforderungZugeordnet = newAnforderungZugeordnet;
        return this;
    }

    ReportingMeldungStatusTransitionBuilder setExistingAnforderungZugeordnet(EntryExitTimeStamp existingAnforderungZugeordnet) {
        this.existingAnforderungZugeordnet = existingAnforderungZugeordnet;
        return this;
    }

    ReportingMeldungStatusTransition build() {
        return new ReportingMeldungStatusTransition(entryDate, meldung, gemeldet, unstimmig, geloescht, newAnforderungZugeordnet, existingAnforderungZugeordnet);
    }
}
