package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachteamAnforderungAbgestimmtKeyFigure extends AbstractKeyFigure {

    public FachteamAnforderungAbgestimmtKeyFigure(Collection<Long> anforderungIds) {
        super(IdTypeTupleFactory.getIdTypeTuplesForAnforderung(anforderungIds), KeyType.FACHTEAM_ANFORDERUNG_ABGESTIMMT);
    }

    public FachteamAnforderungAbgestimmtKeyFigure(Collection<Long> ids, Long runTime) {
        super(ids.size(), IdTypeTupleFactory.getIdTypeTuplesForAnforderung(ids), KeyType.FACHTEAM_ANFORDERUNG_ABGESTIMMT, runTime);
    }

}
