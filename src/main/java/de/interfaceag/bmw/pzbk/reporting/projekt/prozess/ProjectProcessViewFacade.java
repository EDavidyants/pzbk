package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.drilldown.ProjectReportingDrilldownDto;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.drilldown.ProjectReportingDrilldownService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.excel.ProjectReportingExcelExportService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot.ProjectProcessSnapshotWriteService;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Dependent
@Named
public class ProjectProcessViewFacade implements Serializable {

    @Inject
    private ProjectReportingExcelExportService projectReportingExcelExportService;
    @Inject
    private ProjectProcessSnapshotWriteService snapshotWriteService;
    @Inject
    private ProjectReportingDrilldownService drilldownService;

    public void downloadExcelExport(Derivat derivat, ProjectProcessFilter filter) {
        projectReportingExcelExportService.downloadKeyFiguresAsExcelFile(derivat, filter);
    }

    public void createNewSnapshot(Derivat derivat) {
        snapshotWriteService.createNewSnapshotForDerivat(derivat);
    }

    public List<ProjectReportingProcessKeyFigure> getAllAvailableDrilldownKeyFigures() {
        List<ProjectReportingProcessKeyFigure> result = drilldownService.getAllAvailableKeyFigures();
        return result;
    }

    public List<ProjectReportingDrilldownDto> getDrilldowndataForKeyfigure(Derivat derivat, ProjectReportingProcessKeyFigure figure) {
        List<ProjectReportingDrilldownDto> result = drilldownService.getDrilldowndataForKeyfigure(derivat, figure);
        return result;
    }
}
