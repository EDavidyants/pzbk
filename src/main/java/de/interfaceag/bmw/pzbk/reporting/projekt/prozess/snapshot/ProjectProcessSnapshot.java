package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author sl
 */
@Entity
@NamedQueries({
    @NamedQuery(name = ProjectProcessSnapshot.FOR_DERIVAT_AND_DATE, query = "SELECT DISTINCT s FROM ProjectProcessSnapshot s WHERE s.derivat = :derivat AND s.datum =:datum ORDER BY s.datum DESC"),
    @NamedQuery(name = ProjectProcessSnapshot.ALL_FOR_DERIVAT, query = "SELECT DISTINCT s FROM ProjectProcessSnapshot s WHERE s.derivat = :derivat ORDER BY s.datum DESC"),
    @NamedQuery(name = ProjectProcessSnapshot.ALL_DATES_FOR_DERIVAT, query = "SELECT DISTINCT s.id, s.datum FROM ProjectProcessSnapshot s WHERE s.derivat = :derivat ORDER BY s.datum DESC")})
public class ProjectProcessSnapshot implements Serializable {

    private static final String CLASSNAME = "ProjectProcessSnapshot.";
    static final String FOR_DERIVAT_AND_DATE = CLASSNAME + "forDerivatAndDate";
    static final String ALL_FOR_DERIVAT = CLASSNAME + "allForDerivat";
    static final String ALL_DATES_FOR_DERIVAT = CLASSNAME + "allDatesForDerivat";

    @Id
    @SequenceGenerator(name = "projectprocesssnapshot_id_seq",
            sequenceName = "projectprocesssnapshot_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "projectprocesssnapshot_id_seq")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date datum;

    @ManyToOne
    private Derivat derivat;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    private Collection<ProjectProcessSnapshotEntry> entries = new ArrayList<>();

    public ProjectProcessSnapshot() {
        this.datum = new Date();
    }

    public ProjectProcessSnapshot(Derivat derivat, Collection<ProjectProcessSnapshotEntry> entries) {
        this.datum = new Date();
        this.derivat = derivat;
        this.entries = entries;
    }

    @Override
    public String toString() {
        return "ProjectProcessSnapshot{" +
                "id=" + id +
                ", datum=" + datum +
                ", derivat=" + derivat +
                ", entries=" + entries +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Derivat getDerivat() {
        return derivat;
    }

    public void setDerivat(Derivat derivat) {
        this.derivat = derivat;
    }

    public Collection<ProjectProcessSnapshotEntry> getEntries() {
        return entries;
    }

}
