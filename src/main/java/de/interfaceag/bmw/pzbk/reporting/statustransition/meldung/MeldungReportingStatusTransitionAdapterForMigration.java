package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;

import java.util.Date;

public interface MeldungReportingStatusTransitionAdapterForMigration {

    ReportingMeldungStatusTransition changeMeldungStatus(Meldung meldung, Status currentStatus, Status newStatus, Date date);

    ReportingMeldungStatusTransition addMeldungToNewAnforderung(Meldung meldung, Date date);

    ReportingMeldungStatusTransition addMeldungToExistingAnforderung(Meldung meldung, Date date);

    ReportingMeldungStatusTransition removeMeldungFromAnforderung(Meldung meldung, Date date);

}
