package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface ReportingStandLabel {

    String getDate();

}
