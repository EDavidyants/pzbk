package de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.StatusabgleichKennzahlen;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.StatusabgleichKennzahlenService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author sl
 */
@Named
@Stateless
public class StatusabgleichSnapshotService implements StatusabgleichSnapshotReadService, StatusabgleichSnapshotWriteService {

    @Inject
    private StatusabgleichSnapshotDao statusabgleichSnapshotDao;

    @Inject
    private StatusabgleichKennzahlenService kennzahlenService;

    @Override
    public StatusabgleichSnapshot createNewSnapshotForDerivat(Derivat derivat) {
        StatusabgleichKennzahlen kennzahlen = kennzahlenService.getKennzahlenForDerivat(derivat);
        StatusabgleichSnapshot snapshot = StatusabgleichSnapshotConverter.convertToSnapshot(kennzahlen, derivat);
        save(snapshot);
        return snapshot;
    }

    @Override
    public List<SnapshotFilter> getSnapshotDatesForDerivat(Derivat derivat) {
        return statusabgleichSnapshotDao.getAllDatesForDerivat(derivat);
    }

    @Override
    public Optional<StatusabgleichSnapshot> getForDerivatAndDate(Derivat derivat, Date date) {
        return statusabgleichSnapshotDao.getForDerivatAndDate(derivat, date);
    }

    @Override
    public Optional<StatusabgleichSnapshot> getById(Long id) {
        return statusabgleichSnapshotDao.getById(id);
    }

    @Override
    public Optional<StatusabgleichSnapshot> getLatestSnapshotForDerivat(Derivat derivat) {
        List<StatusabgleichSnapshot> allForDerivat = statusabgleichSnapshotDao.getAllForDerivat(derivat);
        if (allForDerivat.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(allForDerivat.get(0));
        }
    }

    private void save(StatusabgleichSnapshot snapshot) {
        statusabgleichSnapshotDao.save(snapshot);
    }

}
