package de.interfaceag.bmw.pzbk.reporting.processoverview;

import de.interfaceag.bmw.pzbk.reporting.ReportingPeriodGetter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht.Anforderungsuebersicht;
import de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht.AnforderungsuebersichtDialog;
import de.interfaceag.bmw.pzbk.reporting.excel.ReportingExcelDataDto;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.svg.TooltipMap;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.primefaces.model.SortOrder;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 * @param <T> instance of an enum with is used for the tooltip identification in
 * the database.
 */
public class ProcessOverviewViewData<T extends Enum<T>> implements AnforderungsuebersichtDialog, ReportingPeriodGetter, Serializable {

    private final ProcessOverviewKeyFigures processOverviewKeyFigures;

    private final ReportingFilter filter;

    @Deprecated
    private final TooltipMap<T> tooltipMap;

    private final String validationFailedHeaderMessage;

    private Collection<ReportingExcelDataDto> excelData;
    private KeyType selectedKeyType;
    private String statusDatumLabel;
    private List<Anforderungsuebersicht> anforderungsuebersichtData;
    private Boolean descendingSortOrder;

    private SortOrder sortingFachteam;
    private SortOrder sortingTteam;

    public ProcessOverviewViewData(ProcessOverviewKeyFigures processOverviewKeyFigures,
            ReportingFilter filter,
            TooltipMap<T> tooltipMap, String validationFailedHeaderMessage) {
        this.processOverviewKeyFigures = processOverviewKeyFigures;
        this.filter = filter;
        this.tooltipMap = tooltipMap;
        this.validationFailedHeaderMessage = validationFailedHeaderMessage;
        this.descendingSortOrder = Boolean.TRUE;
        // initially no sorting by Fachteam and by T-Team
        this.sortingFachteam = SortOrder.UNSORTED;
        this.sortingTteam = SortOrder.UNSORTED;
    }

    public ProcessOverviewViewData(ProcessOverviewKeyFigures processOverviewKeyFigures,
            ReportingFilter filter,
            TooltipMap<T> tooltipMap) {
        this.processOverviewKeyFigures = processOverviewKeyFigures;
        this.filter = filter;
        this.tooltipMap = tooltipMap;
        this.validationFailedHeaderMessage = "";
        this.descendingSortOrder = Boolean.TRUE;
        // initially no sorting by Fachteam and by T-Team
        this.sortingFachteam = SortOrder.UNSORTED;
        this.sortingTteam = SortOrder.UNSORTED;
    }

    public ProcessOverviewKeyFigures getProcessOverviewKeyFigures() {
        return processOverviewKeyFigures;
    }

    public ReportingFilter getFilter() {
        return filter;
    }

    @Override
    public String getReportingPeriod() {
        return processOverviewKeyFigures.getReportingPeriod();
    }

    public boolean isShowTooltips() {
        return filter.isShowTooltips();
    }

    @Deprecated
    public TooltipMap<T> getProcessOverviewTooltipMap() {
        return tooltipMap;
    }

    public String getValidationFailedHeaderMessage() {
        return validationFailedHeaderMessage;
    }

    @Deprecated
    public List<T> getTooltips() {
        return tooltipMap.getTooltips();
    }

    @Override
    public Collection<ReportingExcelDataDto> getExcelData() {
        return excelData;
    }

    public void setExcelData(Collection<ReportingExcelDataDto> excelData) {
        this.excelData = new ArrayList<>(excelData);
    }

    @Override
    public KeyType getSelectedKeyType() {
        return selectedKeyType;
    }

    public void setSelectedKeyType(KeyType selectedKeyType) {
        this.selectedKeyType = selectedKeyType;
    }

    @Override
    public String getStatusDatumLabel() {
        return statusDatumLabel;
    }

    public void setStatusDatumLabel(String statusDatumLabel) {
        this.statusDatumLabel = statusDatumLabel;
    }

    @Override
    public List<Anforderungsuebersicht> getAnforderungsuebersichtData() {
        return anforderungsuebersichtData;
    }

    public void setAnforderungsuebersichtData(List<Anforderungsuebersicht> anforderungsuebersichtData) {
        this.anforderungsuebersichtData = anforderungsuebersichtData;
    }

    @Override
    public Boolean getDescendingSortOrder() {
        return descendingSortOrder;
    }

    @Override
    public void setDescendingSortOrder(Boolean descendingSortOrder) {
        this.descendingSortOrder = descendingSortOrder;
    }

    @Override
    public SortOrder getSortingFachteam() {
        return sortingFachteam;
    }

    public void setSortingFachteam(SortOrder sortingFachteam) {
        this.sortingFachteam = sortingFachteam;
    }

    @Override
    public SortOrder getSortingTteam() {
        return sortingTteam;
    }

    public void setSortingTteam(SortOrder sortingTteam) {
        this.sortingTteam = sortingTteam;
    }

    @Override
    public Boolean hasDurchlaufzeitAndLanglaufer() {
        return selectedKeyType == KeyType.FACHTEAM_CURRENT
                || selectedKeyType == KeyType.TTEAM_CURRENT
                || selectedKeyType == KeyType.VEREINBARUNG_CURRENT;
    }

}
