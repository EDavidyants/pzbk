package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model;

import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author sl
 */
public final class ProjectReportingProcessKeyFigureDataCollectionDto implements ProjectReportingProcessKeyFigureDataCollection {

    private final Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures;
    private final Date date;

    private ProjectReportingProcessKeyFigureDataCollectionDto(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures, Date date) {
        this.keyFigures = keyFigures;
        this.date = date;
    }

    public static ProjectReportingProcessKeyFigureDataCollection forMap(Map<Integer, ProjectReportingProcessKeyFigureData> keyFigures, Date date) {
        return new ProjectReportingProcessKeyFigureDataCollectionDto(keyFigures, date);
    }

    public static ProjectReportingProcessKeyFigureDataCollection empty() {
        return new ProjectReportingProcessKeyFigureDataCollectionDto(Collections.emptyMap(), new Date());
    }

    @Override
    public Long getValueForKeyFigure(ProjectReportingProcessKeyFigure keyFigure) {
        return getValueForKeyFigureId(keyFigure.getId());
    }

    @Override
    public Long getValueForKeyFigureId(Integer keyFigureId) {
        if (keyFigureId != null && keyFigures.containsKey(keyFigureId)) {
            ProjectReportingProcessKeyFigureData keyFigureData = keyFigures.get(keyFigureId);
            return keyFigureData.getValue();
        } else {
            return 0L;
        }
    }

    @Override
    public boolean isAmpelGreenForKeyFigureId(Integer keyFigureId) {
        if (keyFigureId != null && keyFigures.containsKey(keyFigureId)) {
            ProjectReportingProcessKeyFigureData keyFigureData = keyFigures.get(keyFigureId);
            return keyFigureData.isAmpelGreen();
        } else {
            return false;
        }
    }

    @Override
    public Optional<ProjectReportingProcessKeyFigureData> getDataForKeyFigure(ProjectReportingProcessKeyFigure keyFigure) {
        return Optional.ofNullable(keyFigures.get(keyFigure.getId()));
    }

    @Override
    public Date getDate() {
        return date;
    }
}
