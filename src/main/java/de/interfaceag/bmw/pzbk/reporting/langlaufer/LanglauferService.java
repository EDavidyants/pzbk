package de.interfaceag.bmw.pzbk.reporting.langlaufer;

import de.interfaceag.bmw.pzbk.berechtigung.AnforderungBerechtigungService;
import de.interfaceag.bmw.pzbk.berechtigung.MeldungBerechtigungService;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.filter.AnforderungStatusFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.MeldungStatusFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObjectFactory;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTupleCollection;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Named
@Stateless
public class LanglauferService implements Serializable, FachteamReportingLanglauferService, ProcessoverviewLanglauferService, TteamReportingLanglauferService, DashboardLanglauferService {

    @Inject
    private AnforderungBerechtigungService anforderungBerechtigungService;
    @Inject
    private MeldungBerechtigungService meldungBerechtigungService;
    @Inject
    private DurchlaufzeitService durchlaufzeitService;

    @Override
    public List<Long> getFachteamLanglauferAnforderungIds() {

        ReportingFilter filter = new ReportingFilter(UrlParameterUtils.getUrlParameter());
        final List<Status> status = Arrays.asList(Status.A_INARBEIT, Status.A_UNSTIMMIG);
        filter.setAnforderungStatusFilter(new AnforderungStatusFilter(status));

        final Collection<DurchlaufzeitDto> fachteamDurchlaufzeiten = durchlaufzeitService.getFachteamDurchlaufzeiten(filter);

        final List<Long> anforderungLanglauferIds = getLanglauferIds(fachteamDurchlaufzeiten);

        return new ArrayList<>(anforderungBerechtigungService.restrictToAuthorizedAnforderungen(anforderungLanglauferIds));
    }

    @Override
    public List<Long> getFachteamLanglauferMeldungIds() {

        ReportingFilter filter = new ReportingFilter(UrlParameterUtils.getUrlParameter());
        final List<Status> status = Collections.singletonList(Status.M_GEMELDET);
        filter.setMeldungStatusFilter(new MeldungStatusFilter(status));

        final Collection<DurchlaufzeitDto> fachteamDurchlaufzeiten = durchlaufzeitService.getFachteamDurchlaufzeiten(filter);

        final List<Long> meldungenLanglauferIds = getLanglauferIds(fachteamDurchlaufzeiten);

        return new ArrayList<>(meldungBerechtigungService.restrictToAuthorizedMeldungen(meldungenLanglauferIds));
    }

    @Override
    public List<Long> getTteamLanglauferIds() {

        ReportingFilter filter = new ReportingFilter(UrlParameterUtils.getUrlParameter());
        final List<Status> status = Arrays.asList(Status.A_FTABGESTIMMT, Status.A_PLAUSIB);
        filter.setAnforderungStatusFilter(new AnforderungStatusFilter(status));

        final Collection<DurchlaufzeitDto> tteamDurchlaufzeiten = durchlaufzeitService.getTteamDurchlaufzeiten(filter);

        final List<Long> anforderungLanglauferIds = getLanglauferIds(tteamDurchlaufzeiten);

        return new ArrayList<>(anforderungBerechtigungService.restrictToAuthorizedAnforderungen(anforderungLanglauferIds));
    }

    @Override
    public int getFachteamLanglauferCountBySensorCoc(SensorCoc sensorCoc) {

        ReportingFilter filter = new ReportingFilter(UrlParameterUtils.getUrlParameter());

        final IdSearchFilter sensorCocFilter = filter.getSensorCocFilter();
        final List<SearchFilterObject> searchFilterObjectsForSensorCoc = SearchFilterObjectFactory.getSearchFilterObjectsForSensorCoc(Collections.singletonList(sensorCoc));
        sensorCocFilter.setSelectedValues(new HashSet<>(searchFilterObjectsForSensorCoc));

        final long anforderungLanglaufer = getAnforderungLanglauferCountBySensorCoc(filter);
        final long meldungLanglaufer = getMeldungLanglauferCountBySensorCoc(filter);

        return (int) anforderungLanglaufer + (int) meldungLanglaufer;
    }

    @Override
    public boolean isFachteamLanglaufer(IdTypeTupleCollection idTypeTuples) {
        List<Long> anforderungLanglauferIds = getFachteamLanglauferAnforderungIds();
        Collection<Long> anforderungIds = idTypeTuples.getIdsForType(Type.ANFORDERUNG);
        anforderungLanglauferIds.retainAll(anforderungIds);
        if (!anforderungLanglauferIds.isEmpty()) {
            return true;
        }

        List<Long> meldungLanglauferIds = getFachteamLanglauferMeldungIds();
        Collection<Long> meldungIds = idTypeTuples.getIdsForType(Type.MELDUNG);
        meldungLanglauferIds.retainAll(meldungIds);
        return !meldungLanglauferIds.isEmpty();
    }

    @Override
    public boolean isTteamLanglaufer(IdTypeTupleCollection idTypeTuples) {
        final List<Status> status = Collections.singletonList(Status.A_FTABGESTIMMT);
        ReportingFilter filter = updateAnforderungStatusReportingFilter(status);
        final Collection<DurchlaufzeitDto> tteamDurchlaufzeiten = durchlaufzeitService.getTteamDurchlaufzeiten(filter);
        return isIdTypeTupleLanglaufer(idTypeTuples, tteamDurchlaufzeiten);
    }

    @Override
    public boolean isVereinbarungLanglaufer(IdTypeTupleCollection idTypeTuples) {
        final List<Status> status = Collections.singletonList(Status.A_PLAUSIB);
        ReportingFilter filter = updateAnforderungStatusReportingFilter(status);
        final Collection<DurchlaufzeitDto> vereinbarungDurchlaufzeiten = durchlaufzeitService.getVereinbarungDurchlaufzeiten(filter);
        return isIdTypeTupleLanglaufer(idTypeTuples, vereinbarungDurchlaufzeiten);
    }

    private ReportingFilter updateAnforderungStatusReportingFilter(List<Status> status) {
        ReportingFilter filter = new ReportingFilter(UrlParameterUtils.getUrlParameter());
        filter.setAnforderungStatusFilter(new AnforderungStatusFilter(status));
        return filter;
    }

    private boolean isIdTypeTupleLanglaufer(IdTypeTupleCollection idTypeTuples, Collection<DurchlaufzeitDto> vereinbarungDurchlaufzeiten) {
        final List<Long> anforderungLanglauferIds = getLanglauferIds(vereinbarungDurchlaufzeiten);
        Collection<Long> anforderungIds = idTypeTuples.getIdsForType(Type.ANFORDERUNG);
        anforderungLanglauferIds.retainAll(anforderungIds);
        return !anforderungLanglauferIds.isEmpty();
    }

    private static List<Long> getLanglauferIds(Collection<DurchlaufzeitDto> durchlaufzeiten) {
        return durchlaufzeiten.stream()
                .filter(DurchlaufzeitDto::isLanglaufer)
                .map(DurchlaufzeitDto::getId)
                .collect(Collectors.toList());
    }

    @Override
    public int getTteamLanglauferCountByTteam(Tteam tteam) {
        ReportingFilter filter = new ReportingFilter(UrlParameterUtils.getUrlParameter());

        final IdSearchFilter tteamFilter = filter.getTteamFilter();
        final List<SearchFilterObject> searchFilterObjectsForTteam = SearchFilterObjectFactory.getSearchFilterObjectsForTteams(Collections.singletonList(tteam));
        tteamFilter.setSelectedValues(new HashSet<>(searchFilterObjectsForTteam));

        return getAnforderungLanglauferCountByTteamAndStatus(filter, Status.A_FTABGESTIMMT);
    }

    @Override
    public int getVereinbarungLanglauferCountByTteam(Tteam tteam) {
        ReportingFilter filter = new ReportingFilter(UrlParameterUtils.getUrlParameter());

        final IdSearchFilter tteamFilter = filter.getTteamFilter();
        final List<SearchFilterObject> searchFilterObjectsForTteam = SearchFilterObjectFactory.getSearchFilterObjectsForTteams(Collections.singletonList(tteam));
        tteamFilter.setSelectedValues(new HashSet<>(searchFilterObjectsForTteam));

        return getAnforderungLanglauferCountByTteamAndStatus(filter, Status.A_PLAUSIB);
    }

    private int getAnforderungLanglauferCountByTteamAndStatus(ReportingFilter filter, Status status) {
        final List<Status> statusValues = Collections.singletonList(status);
        filter.setMeldungStatusFilter(null);
        filter.setAnforderungStatusFilter(new AnforderungStatusFilter(statusValues));

        final Collection<DurchlaufzeitDto> fachteamDurchlaufzeiten = durchlaufzeitService.getFachteamDurchlaufzeiten(filter);
        return (int) fachteamDurchlaufzeiten.stream()
                .filter(DurchlaufzeitDto::isLanglaufer)
                .count();
    }

    private long getMeldungLanglauferCountBySensorCoc(ReportingFilter filter) {
        final List<Status> status = Collections.singletonList(Status.M_GEMELDET);
        filter.setAnforderungStatusFilter(null);
        filter.setMeldungStatusFilter(new MeldungStatusFilter(status));

        final Collection<DurchlaufzeitDto> fachteamDurchlaufzeiten = durchlaufzeitService.getFachteamDurchlaufzeiten(filter);
        return fachteamDurchlaufzeiten.stream()
                .filter(DurchlaufzeitDto::isLanglaufer)
                .count();
    }

    private long getAnforderungLanglauferCountBySensorCoc(ReportingFilter filter) {
        final List<Status> status = Arrays.asList(Status.A_INARBEIT, Status.A_UNSTIMMIG);
        filter.setMeldungStatusFilter(null);
        filter.setAnforderungStatusFilter(new AnforderungStatusFilter(status));

        final Collection<DurchlaufzeitDto> fachteamDurchlaufzeiten = durchlaufzeitService.getFachteamDurchlaufzeiten(filter);
        return fachteamDurchlaufzeiten.stream()
                .filter(DurchlaufzeitDto::isLanglaufer)
                .count();
    }

}
