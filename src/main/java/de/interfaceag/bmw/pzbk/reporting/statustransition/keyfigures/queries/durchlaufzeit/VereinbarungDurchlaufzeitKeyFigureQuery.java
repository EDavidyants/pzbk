package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTuple;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureFilterUtils;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.KeyFigureQueryEndDate;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class VereinbarungDurchlaufzeitKeyFigureQuery {

    private static final Logger LOG = LoggerFactory.getLogger(VereinbarungDurchlaufzeitKeyFigureQuery.class.getName());

    private static final String SELECT = "SELECT new de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit.DurchlaufzeitPart(";
    private static final String FROM_ANFORDERUNG_REPORTING = " FROM ReportingStatusTransition r ";
    private static final String ANOFRDERUNG_JOINS = "INNER JOIN r.anforderung a LEFT JOIN a.umsetzer u LEFT JOIN a.festgestelltIn f ";

    private VereinbarungDurchlaufzeitKeyFigureQuery() {
    }

    public static Map<IdTypeTuple, Long> computeIdTypeDurchlaufzeit(ReportingFilter filter, EntityManager entityManager) {

        Collection<DurchlaufzeitPart> plausibilisiertEntryExitNotNull = getTimeStampsForPlausibilisiertEntryExitNotNull(filter, entityManager);
        Collection<DurchlaufzeitPart> plausibilisiertEntryNotNullExitNull = getTimeStampsForPlausibilisiertEntryNotNullExitNull(filter, entityManager);

        plausibilisiertEntryExitNotNull.forEach(DurchlaufzeitPart::setAnforderung);
        plausibilisiertEntryNotNullExitNull.forEach(DurchlaufzeitPart::setAnforderung);

        final List<DurchlaufzeitPart> allCases = Stream.of(plausibilisiertEntryExitNotNull, plausibilisiertEntryNotNullExitNull).flatMap(Collection::stream).collect(Collectors.toList());

        return DurchlaufzeitCalculator.computeDurchlaufzeitForActiveIdTypeTuple(allCases);
    }

    public static VereinbarungDurchlaufzeitKeyFigure compute(ReportingFilter filter, EntityManager entityManager) {

        Collection<DurchlaufzeitPart> plausibilisiertEntryExitNotNull = getTimeStampsForPlausibilisiertEntryExitNotNull(filter, entityManager);
        Collection<DurchlaufzeitPart> plausibilisiertEntryNotNullExitNull = getTimeStampsForPlausibilisiertEntryNotNullExitNull(filter, entityManager);

        final Long durchlaufzeit = DurchlaufzeitCalculator.computeDurchlaufzeit(plausibilisiertEntryExitNotNull, plausibilisiertEntryNotNullExitNull);

        return new VereinbarungDurchlaufzeitKeyFigure(durchlaufzeit.intValue());
    }

    private static Collection<DurchlaufzeitPart> getTimeStampsForPlausibilisiertEntryNotNullExitNull(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO(SELECT);
        queryPart.append("re.anforderung.id, re.plausibilisiert.entry, re.plausibilisiert.exit)")
                .append(" FROM ReportingStatusTransition re ")
                .append(" WHERE re.id IN (SELECT r.id ")
                .append(FROM_ANFORDERUNG_REPORTING)
                .append(ANOFRDERUNG_JOINS)
                .append(" WHERE r.plausibilisiert.entry < :endDate ")
                .append(" AND r.plausibilisiert.exit IS NULL ");

        queryPart.put("endDate", endDate);

        KeyFigureFilterUtils.appendAnforderungFilterWithDateFilter(filter, queryPart);

        queryPart.append(")");

        LOG.debug("getTimeStampsForPlausibilisiertEntryNotNullExitNull Query: {}", queryPart.getQuery());

        QueryFactory<DurchlaufzeitPart> queryFactory = new QueryFactory<>();
        final TypedQuery<DurchlaufzeitPart> query = queryFactory.buildTypedQuery(queryPart, entityManager, DurchlaufzeitPart.class);
        return query.getResultList();
    }


    private static Collection<DurchlaufzeitPart> getTimeStampsForPlausibilisiertEntryExitNotNull(ReportingFilter filter, EntityManager entityManager) {

        final Date endDate = KeyFigureQueryEndDate.getEndDate(filter);

        QueryPart queryPart = new QueryPartDTO(SELECT);
        queryPart.append("re.anforderung.id, re.plausibilisiert.entry, re.plausibilisiert.exit)")
                .append(" FROM ReportingStatusTransition re ")
                .append(" WHERE re.id IN (SELECT r.id ")
                .append(FROM_ANFORDERUNG_REPORTING)
                .append(ANOFRDERUNG_JOINS)
                .append(" WHERE r.plausibilisiert.entry < :endDate ")
                .append(" AND r.plausibilisiert.exit < :endDate ")
                .append(" AND r.plausibilisiert.exit IS NOT NULL ");

        queryPart.put("endDate", endDate);

        KeyFigureFilterUtils.appendAnforderungFilterWithDateFilter(filter, queryPart);

        queryPart.append(")");

        LOG.debug("getTimeStampsForPlausibilisiertEntryExitNotNull Query: {}", queryPart.getQuery());

        QueryFactory<DurchlaufzeitPart> queryFactory = new QueryFactory<>();
        final TypedQuery<DurchlaufzeitPart> query = queryFactory.buildTypedQuery(queryPart, entityManager, DurchlaufzeitPart.class);
        return query.getResultList();
    }

}
