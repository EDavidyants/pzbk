package de.interfaceag.bmw.pzbk.reporting.processoverview;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.svg.Box;
import de.interfaceag.bmw.pzbk.reporting.svg.BoxPolygon;
import de.interfaceag.bmw.pzbk.reporting.svg.Diamond;
import de.interfaceag.bmw.pzbk.reporting.svg.Icon;
import de.interfaceag.bmw.pzbk.reporting.svg.InputBox;
import de.interfaceag.bmw.pzbk.reporting.svg.KeyFigureBox;
import de.interfaceag.bmw.pzbk.reporting.svg.Line;
import de.interfaceag.bmw.pzbk.reporting.svg.OutputBox;
import de.interfaceag.bmw.pzbk.reporting.svg.Path;
import de.interfaceag.bmw.pzbk.reporting.svg.PlusIcon;
import de.interfaceag.bmw.pzbk.reporting.svg.Polygon;
import de.interfaceag.bmw.pzbk.reporting.svg.Rectangle;
import de.interfaceag.bmw.pzbk.reporting.svg.SVGElement;
import de.interfaceag.bmw.pzbk.reporting.svg.SVGElementDrawer;
import de.interfaceag.bmw.pzbk.reporting.svg.Text;
import de.interfaceag.bmw.pzbk.reporting.svg.TooltipCollection;
import de.interfaceag.bmw.pzbk.shared.dto.Point;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lomu, sl
 */
@FacesComponent(createTag = true, tagName = "SVGProcessOverviewDetail", namespace = "http://java.sun.com/jsf/de/interfaceag/bmw/pzbk/reporting")
public class SVGProcessOverviewDetail extends UIComponentBase {

    private static final double SVGHEIGTH = 530;
    private static final double SVGWIDTH = 1284;

    @Override
    public String getFamily() {
        return "Reporting";
    }

    @Override
    public void encodeBegin(FacesContext context) throws IOException {

        ReportingProcessOverviewController reportingProcessOverviewController = (ReportingProcessOverviewController) getAttributes().get("processOverviewController");
        ProcessOverviewViewData viewData = reportingProcessOverviewController.getViewData();

        boolean hideTooltips = viewData.isShowTooltips();

        List<SVGElement> elements = new ArrayList<>();

        TooltipCollection tooltipCollection = TooltipCollection.createCollection();

        Map<String, String> textFromProperties = getTextFromProperties(reportingProcessOverviewController);

        final DurchlaufzeitKeyFigure fachteamDurchlaufzeit = viewData.getProcessOverviewKeyFigures().getDurchlaufzeitKeyFigures().getByKeyType(KeyType.DURCHLAUFZEIT_FACHTEAM);
        Box fachteamBox = new Box(248, 85, Box.DetailBoxType.FACHTEAM,
                viewData.getProcessOverviewKeyFigures().getKeyFigureByTypeId(5), fachteamDurchlaufzeit.getValue(), hideTooltips,
                ProcessOverviewDetailTooltip.FACHTEAM_HEADER.toString(),
                ProcessOverviewDetailTooltip.FACHTEAM_DURCHLAUFZEIT.toString(),
                ProcessOverviewDetailTooltip.LANGLAUFER_FACHTEAM.toString(),
                BoxPolygon.PolygonType.DETAIL, Diamond.Type.DETAIL, textFromProperties);
        elements.add(fachteamBox);
        tooltipCollection.addAll(fachteamBox.getTooltipCollection());
        tooltipCollection.addAll(fachteamBox.getTooltipCollection());

        final DurchlaufzeitKeyFigure tteamDurchlaufzeit = viewData.getProcessOverviewKeyFigures().getDurchlaufzeitKeyFigures().getByKeyType(KeyType.DURCHLAUFZEIT_TTEAM);
        Box tteamBox = new Box(500, 85, Box.DetailBoxType.TTEAM,
                viewData.getProcessOverviewKeyFigures().getKeyFigureByTypeId(10), tteamDurchlaufzeit.getValue(), hideTooltips,
                ProcessOverviewDetailTooltip.TTEAM_HEADER.toString(),
                ProcessOverviewDetailTooltip.TTEAM_DURCHLAUFZEIT.toString(),
                ProcessOverviewDetailTooltip.LANGLAUFER_TTEAM.toString(),
                BoxPolygon.PolygonType.DETAIL, Diamond.Type.DETAIL, textFromProperties);
        elements.add(tteamBox);
        tooltipCollection.addAll(tteamBox.getTooltipCollection());

        final DurchlaufzeitKeyFigure vereinbarungDurchlaufzeit = viewData.getProcessOverviewKeyFigures().getDurchlaufzeitKeyFigures().getByKeyType(KeyType.DURCHLAUFZEIT_VEREINBARUNG);
        Box vereinbarungBox = new Box(754, 85, Box.DetailBoxType.VEREINBARUNG,
                viewData.getProcessOverviewKeyFigures().getKeyFigureByTypeId(14), vereinbarungDurchlaufzeit.getValue(), hideTooltips,
                ProcessOverviewDetailTooltip.VEREINBARUNG_HEADER.toString(),
                ProcessOverviewDetailTooltip.VEREINBARUNG_DURCHLAUFZEIT.toString(),
                ProcessOverviewDetailTooltip.LANGLAUFER_VEREINBARUNG.toString(),
                BoxPolygon.PolygonType.DETAIL, Diamond.Type.DETAIL, textFromProperties);
        elements.add(vereinbarungBox);
        tooltipCollection.addAll(vereinbarungBox.getTooltipCollection());

        elements.add(new InputBox(0, 85, InputBox.InputBoxType.DETAIL, textFromProperties));

        OutputBox outputBox = new OutputBox(0, 85, viewData.getProcessOverviewKeyFigures().getKeyFigureByTypeId(24),
                hideTooltips,
                ProcessOverviewDetailTooltip.FREIGEGEBEN.toString(),
                ProcessOverviewDetailTooltip.LANGLAUFER_FREIGEGEBEN.toString(),
                Diamond.Type.DETAIL, OutputBox.OutputBoxType.DETAIL, textFromProperties);
        elements.add(outputBox);
        tooltipCollection.addAll(outputBox.getTooltipCollection());

        ResponseWriter writer = context.getResponseWriter();
        writer.startElement("div", this);
        writer.writeAttribute("class", "svg-wrapper", null);
        writer.writeAttribute("id", "svg-div", null);
        double padding = (SVGHEIGTH / SVGWIDTH) * 100;
        writer.writeAttribute("style", "position: relative; z-index: 1; padding-bottom: " + ((int) padding) + "%; max-width: 1920px;", null);

        writer.startElement("svg", this);
        writer.writeAttribute("id", "f13ce846-05ff-4d34-9db5-5511feb90951", null);
        writer.writeAttribute("data-name", "Ebene 1", null);
        writer.writeAttribute("xmlns", "http://www.w3.org/2000/svg\\", null);
        writer.writeAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink", null);
        writer.writeAttribute("viewBox", "0 0 1284 530", null);
        writer.writeAttribute("style", "position: absolute;", null);

        String vonDateFilter = sanitize(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("vonDateFilter"));
        String bisDateFilter = sanitize(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("bisDateFilter"));

        StringBuilder sb = new StringBuilder(getDefs());
        sb.append(SVGElementDrawer.drawElements(elements))
                .append(getPfeile(reportingProcessOverviewController))
                .append(getClock(reportingProcessOverviewController))
                .append(getPlusIcons())
                .append(getKeyfigureBoxes(viewData.getProcessOverviewKeyFigures()))
                .append(getPzbkArrowsAndBoxes(viewData.getProcessOverviewKeyFigures()))
                .append(getMehrfachZugeordnetenMeldungenArrowsAndBox(viewData.getProcessOverviewKeyFigures()))
                .append(getIcons())
                .append(tooltipCollection.drawCollection())
                .append(getLegende(vonDateFilter, bisDateFilter, reportingProcessOverviewController));

        writer.write(sb.toString());

        writer.endElement("svg");
        writer.endElement("div");
    }

    private Map<String, String> getTextFromProperties(ReportingProcessOverviewController reportingProcessOverviewController) {

        Map<String, String> returnMap = new HashMap<>();

        returnMap.put("durchlaufzeit", reportingProcessOverviewController.getStringfromProperty("reporting_durchlaufzeit"));
        returnMap.put("fachteam", reportingProcessOverviewController.getStringfromProperty("reporting_fachteam"));
        returnMap.put("tteam", reportingProcessOverviewController.getStringfromProperty("reporting_tteam"));
        returnMap.put("vereinbarung", reportingProcessOverviewController.getStringfromProperty("reporting_vereinbarung"));
        returnMap.put("vereinbarungPart1", reportingProcessOverviewController.getStringfromProperty("reporting_vereinbarungPart1"));
        returnMap.put("vereinbarungPart2", reportingProcessOverviewController.getStringfromProperty("reporting_vereinbarungPart2"));
        returnMap.put("staerken", reportingProcessOverviewController.getStringfromProperty("reporting_staerken"));
        returnMap.put("schwaechen", reportingProcessOverviewController.getStringfromProperty("reporting_schwaechen"));
        returnMap.put("aus", reportingProcessOverviewController.getStringfromProperty("reporting_aus"));
        returnMap.put("werken", reportingProcessOverviewController.getStringfromProperty("reporting_werkenFiz"));
        returnMap.put("freigegeben", reportingProcessOverviewController.getStringfromProperty("reporting_freigegeben"));

        return returnMap;
    }

    private static String getPzbkArrowsAndBoxes(ProcessOverviewKeyFigures keyFigures) {
        List<SVGElement> elements = new ArrayList<>();

        elements.add(new Icon(475.84, 0, "translate(466.84 4)", Icon.IconType.PZBK, "fcb0d8f7-3685-4db9-bbe7-79e8e57ab3c3", "d94fd6dc-5a83-4702-8012-a9f3960fd863"));
        elements.add(new Icon(729.84, 0, "translate(720.84 4.02)", Icon.IconType.PZBK, "f0891a03-befb-4f62-b170-c3ea127c16cf", "9c560fd9-ef00-4c81-8cd8-033b368edd3e"));
        elements.add(new Icon(983.84, 0, "translate(973.84 4.02)", Icon.IconType.PZBK, "f50dcac1-dacf-44b1-a886-ef017e9a9240", "f9b6ca17-63f6-4c35-a81f-299751e4e9fb"));
        elements.add(new Icon(1227.84, 0, "translate(1218.84 4.02)", Icon.IconType.PZBK, "b90bbf8e-7e6e-4754-af1e-2fe549a93cbe", "a78d3cd6-d7d8-4bfd-8918-54086b620e3d"));

        //Fachteam BZPK (KF9)
        elements.add(new KeyFigureBox("M496.19.52a3.37,3.37,0,0,0-3.5,3.5V22a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5V4a3.38,3.38,0,0,0-3.5-3.5Z", 500.0, 18.82, keyFigures.getKeyFigureByTypeId(9)));
        //TTEAM PZBK (KF16)
        elements.add(new KeyFigureBox("M750.19.52a3.37,3.37,0,0,0-3.5,3.5V22a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5V4a3.38,3.38,0,0,0-3.5-3.5Z", 753.5, 18.82, keyFigures.getKeyFigureByTypeId(16)));
        // Vereinbarung PZBK (KF21)
        elements.add(new KeyFigureBox("M1003.19.52a3.37,3.37,0,0,0-3.5,3.5V22a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5V4a3.38,3.38,0,0,0-3.5-3.5Z", 1007.0, 18.82, keyFigures.getKeyFigureByTypeId(21)));
        // Freigegebe PZBK (KF23)
        elements.add(new KeyFigureBox("M1248.19.52a3.37,3.37,0,0,0-3.5,3.5V22a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5V4a3.38,3.38,0,0,0-3.5-3.5Z", 1251.0, 18.82, keyFigures.getKeyFigureByTypeId(23)));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 434.84, 70.92, 512.84, 70.92));
        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 688.84, 70.92, 766.84, 70.92));
        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 941.84, 70.92, 1019.84, 70.92));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 512.19, 71.67, 512.19, 32.33));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(512.19, 27.67), new Point(518.42, 34.38), new Point(505.95, 34.38), new Point(512.19, 27.67)
        )));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 766.19, 71.67, 766.19, 32.33));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(766.19, 27.67), new Point(772.42, 34.38), new Point(759.96, 34.38), new Point(766.19, 27.67)
        )));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 1019.19, 71.67, 1019.19, 32.33));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(1019.19, 27.67), new Point(1025.42, 34.38), new Point(1012.96, 34.38), new Point(1019.19, 27.67)
        )));

        String pzbkIconBlackCircle = "<path class=\"1fc087fc-5ad7-4044-a16b-f1da2c7b2c10\" d=\"M729.84,25.52A12.5,12.5,0,1,0,717.34,13a12.5,12.5,0,0,0,12.5,12.5\" transform=\"translate(0 -0.02)\"/>\n";

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 1138.37, 156.38, 1264.84, 156.38));
        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 1264.19, 157.12, 1264.19, 32.33));

        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(1264.19, 27.67), new Point(1270.42, 34.38), new Point(1257.95, 34.38), new Point(1264.19, 27.67)
        )));
        return SVGElementDrawer.drawElements(elements);
    }

    private static String getMehrfachZugeordnetenMeldungenArrowsAndBox(ProcessOverviewKeyFigures keyFigures) {
        List<SVGElement> elements = new ArrayList<>();

        elements.add(new Icon(555.00, 0, "translate(546.00 4)", Icon.IconType.MELDUNGEN_MEHRFACH_ZUGEORDNET, "", ""));
        elements.add(new KeyFigureBox("M577.19.52a3.37,3.37,0,0,0-3.5,3.5V22a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5V4a3.38,3.38,0,0,0-3.5-3.5Z", 580.0, 18.82, keyFigures.getKeyFigureByTypeId(29)));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 512.84, 70.92, 592.19, 70.92));
        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 592.19, 71.67, 592.19, 32.33));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(592.19, 27.67), new Point(598.42, 34.38), new Point(585.89, 34.38), new Point(592.19, 27.67)
        )));

        return SVGElementDrawer.drawElements(elements);
    }

    private static String getIcons() {
        List<SVGElement> elements = new ArrayList<>();

        elements.add(new Icon(398.84, 0, "translate(390.84 5)", Icon.IconType.GELOESCHT, "d54dadb5-1813-468d-a83a-47d607bf1a7e", "64b08909-b7a5-4076-8b0e-d558e91a1f33"));
        elements.add(new Icon(652.84, 0, "translate(644.84 5.02)", Icon.IconType.GELOESCHT, "a1924305-da8a-4580-8add-d7895e98016d", "b3e5800e-f433-422d-960e-289196af236f"));
        elements.add(new Icon(905.84, 0, "translate(897.84 5)", Icon.IconType.GELOESCHT, "6c5f9f54-6f13-4c42-a1fd-241dd1ac837f", "6175231a-f5ed-41f2-beab-b0b5522c571b"));
        elements.add(new Icon(1150.84, 0, "translate(1142.84 5.02)", Icon.IconType.GELOESCHT, "", ""));

        String neuVersioniert = "<g id=\"neuVersioniert\">"
                + "<g class=\"a7a7d0c6-d834-4f16-aa8c-4d2cc48187d8\">\n"
                + "                        <path class=\"1fc087fc-5ad7-4044-a16b-f1da2c7b2c10\" d=\"M145.84,25.52A12.5,12.5,0,1,0,133.34,13a12.5,12.5,0,0,0,12.5,12.5\" transform=\"translate(0 -0.02)\"></path>\n"
                + "                        <circle class=\"e10c3a5a-f925-4bfe-b875-b1dab086c709\" cx=\"145.84\" cy=\"13\" r=\"12.5\"></circle>\n"
                + "                        <line class=\"8e99f5e6-3091-4098-9825-979e047c5052\" x1=\"145.84\" y1=\"15.5\" x2=\"145.84\" y2=\"8.84\"></line>\n"
                + "                    </g>"
                + "<polygon class=\"cb17c188-b9c0-44f6-89fc-25349d833054\" points=\"145.84 4.5 140.34 10.42 151.34 10.42 145.84 4.5\"></polygon>"
                + "<line class=\"8e99f5e6-3091-4098-9825-979e047c5052\" x1=\"145.84\" y1=\"17.5\" x2=\"145.84\" y2=\"16.5\"></line>"
                + "<line class=\"8e99f5e6-3091-4098-9825-979e047c5052\" x1=\"145.84\" y1=\"19.5\" x2=\"145.84\" y2=\"18.5\"></line>"
                + "<line class=\"8e99f5e6-3091-4098-9825-979e047c5052\" x1=\"145.84\" y1=\"21.5\" x2=\"145.84\" y2=\"20.5\"></line>"
                + "</g>";

        String result = neuVersioniert + SVGElementDrawer.drawElements(elements);

        return result;
    }

    private static String getDefs() {
        return "<defs>\n"
                + "               <style>\n"
                + "                          .\\30 1e056ae-e1a3-4d3c-bc83-4c867c3167ae,.\\38 e99f5e6-3091-4098-9825-979e047c5052,.af0df502-b7f6-45df-a78e-fe8c15c16f77,.b3050d9d-d9a1-416b-be97-57f68a2e524d,.e10c3a5a-f925-4bfe-b875-b1dab086c709{fill:none;}.\\35 4f5d033-071e-455b-8b25-87e0f756615d{clip-path:url(#6f1a5c01-8a67-4f39-9c89-e6ebb5c5104d);}.a7a7d0c6-d834-4f16-aa8c-4d2cc48187d8{clip-path:url(#9ebe03d8-5253-4130-a231-11ee2b3d72e4);}.\\35 92c5a5e-ada3-49a0-83c0-bc6f96857966{opacity:0.1;}.\\35 457bff4-c760-4146-a628-412b3b58f175{clip-path:url(#d7d457c3-9189-455b-81e9-2fdc0b00b6e6);}.\\30 d95121d-433d-42b6-a81c-6f7ded3a411c,.\\35 f459e6e-0411-4145-a215-c5424afc40fe,.cb17c188-b9c0-44f6-89fc-25349d833054{fill:#fff;}.\\35 fd1014f-86f5-41fc-be1a-1fc3808df3e0{clip-path:url(#5ee91c21-ae52-4572-b067-45471febbd86);}.\\37 28c13e9-b56b-49eb-a80e-1297e5d2f1f8{clip-path:url(#8471efae-f4ac-4b5d-b81e-13945af9d1dd);}.\\32 39f280d-b690-40d2-a29e-c463feb22843{clip-path:url(#7d79421d-9bf4-4cd5-bad0-6e5b0c5dc060);}.\\30 1e056ae-e1a3-4d3c-bc83-4c867c3167ae,.\\38 e99f5e6-3091-4098-9825-979e047c5052,.b3050d9d-d9a1-416b-be97-57f68a2e524d{stroke:#fff;}.b3050d9d-d9a1-416b-be97-57f68a2e524d{stroke-width:1.5px;}.\\32 d24508d-5474-4519-8463-2fe54f9ec290{opacity:0.5;}.ccefffb1-ba6d-4dbb-bbbf-b34097f27c01{clip-path:url(#b4823c3a-ecc2-484b-a855-e75a6647830f);}.b675cbc6-7699-4775-a8c4-02ce9ee8377b{fill:#5188cc;}.\\31 fc087fc-5ad7-4044-a16b-f1da2c7b2c10{fill:#222324;}.e10c3a5a-f925-4bfe-b875-b1dab086c709{stroke:#cbcbcb;}.\\30 d95121d-433d-42b6-a81c-6f7ded3a411c{font-size:16px;font-family:Arial-BoldMT, Arial;font-weight:700;}.de6e5232-07bb-41d1-94c9-bac358104b1c{clip-path:url(#b88a74e0-1c37-4d6a-9751-ac977e8b8c1e);}.\\30 4a1c0a0-ee57-45d9-81e5-8b9547204d5a{fill:#7d99aa;}.\\32 a824c58-bbed-43c0-a120-5068e09f3cc0{letter-spacing:-0.02em;}.\\37 cca9b7e-4c0d-4fb6-bb8f-167f5d807814{clip-path:url(#fbff60ff-4779-4f49-b604-66082872749c);}.\\37 6782cdd-3d0b-42f3-987b-a3e6b414c050{fill:#3766a0;}.b5d16331-1c9b-41a4-85a6-82e96fafbbc8{letter-spacing:-0.05em;}.a2a90d70-073f-4ee6-960a-20a277ca5749{letter-spacing:-0.07em;}.\\39 8cd5ad8-03c0-4c32-8374-e9c63082b6e8{clip-path:url(#ea4ed90c-f89a-40f8-ab46-bb24e4a0193d);}.\\34 0084878-491e-4ba9-ae36-376c85c69bc9{fill:#123054;}.e42c4fa2-60b7-493c-bbe3-5b7b96c79774{fill:#4f4f4f;}.\\38 e99f5e6-3091-4098-9825-979e047c5052{stroke-width:6.06px;}.d54dadb5-1813-468d-a83a-47d607bf1a7e{clip-path:url(#1d4f0fff-1e5c-497d-ab22-3283f8ec02de);}.\\36 4b08909-b7a5-4076-8b0e-d558e91a1f33{clip-path:url(#f34f5673-eda8-4f30-9a76-736188696dc8);}.a1924305-da8a-4580-8add-d7895e98016d{clip-path:url(#fdc809f1-7cce-4915-b8cc-f4e1cdbc5232);}.b3e5800e-f433-422d-960e-289196af236f{clip-path:url(#745bae07-913d-4a4e-91b4-a836df6204b4);}.\\36 c5f9f54-6f13-4c42-a1fd-241dd1ac837f{clip-path:url(#6097873a-0458-4f24-bbb2-23a63ada0950);}.\\36 175231a-f5ed-41f2-beab-b0b5522c571b{clip-path:url(#66fa8ae2-4a59-4f36-82fd-f23cc43d491e);}.\\33 f3b4492-06c5-463e-be15-f687ec15584f{clip-path:url(#c0c1634f-3f6e-47dc-9f8c-8006d921eb54);}.fcb0d8f7-3685-4db9-bbe7-79e8e57ab3c3{clip-path:url(#3fccbc3d-da22-423e-935d-df1b3bc74d47);}.d94fd6dc-5a83-4702-8012-a9f3960fd863{clip-path:url(#0115b0e1-6d7c-4ecf-aabc-279c9c9a0c59);}.\\35 696573c-610f-4835-b7b0-483d9f7af8e7{clip-path:url(#a7e3bb04-2f39-44a5-b963-e880172fd5fc);}.bb09ff8a-b3da-4537-8035-b040abf1712e{clip-path:url(#8ddb473d-97ff-42a9-8176-7d79c7d6b72c);}.f0891a03-befb-4f62-b170-c3ea127c16cf{clip-path:url(#be80d5f1-3727-4515-8951-6e4816f0abcd);}.\\39 c560fd9-ef00-4c81-8cd8-033b368edd3e{clip-path:url(#ede669f2-603f-4635-8bb6-655674689d69);}.f50dcac1-dacf-44b1-a886-ef017e9a9240{clip-path:url(#4dd30be1-4919-48b9-ad24-d881f3753df0);}.f9b6ca17-63f6-4c35-a81f-299751e4e9fb{clip-path:url(#389700f5-57e3-4551-a967-485921ac0c41);}.b90bbf8e-7e6e-4754-af1e-2fe549a93cbe{clip-path:url(#f2fdd935-0985-4436-bbb0-c8c98af870ac);}.a78d3cd6-d7d8-4bfd-8918-54086b620e3d{clip-path:url(#3a0ea917-4e1c-4eb8-ae0a-f4a75c0dff88);}.e57f8aad-b61e-4efb-a49d-599c085f3eec{clip-path:url(#42718428-3a2e-4907-a88a-7d2eba36b889);}.\\35 f459e6e-0411-4145-a215-c5424afc40fe{font-size:14px;font-family:ArialMT, Arial;}.\\38 cae1ca6-7965-4810-b62c-d95668469554{letter-spacing:-0.06em;}.\\31 21203a3-8f36-4e2f-9a35-af1017f56860{letter-spacing:-0.11em;}.e9295807-1e0a-42ca-a96d-8aba64f1cfc0{letter-spacing:-0.05em;}</style>\n" + ".\\36 2cd76c4-80a4-4944-a40d-e6dd289b40b2,.\\39 48d7ce9-e863-45e7-a3e6-179e4cab85de,.ced86064-e9d2-49f2-bdd0-48209c2c4129,.e7979375-7020-4a99-8f60-046cf1838a15{fill:none;}.\\33 49b8e67-6303-488d-a7e9-fa034879202a{clip-path:url(#cc07976c-780c-4058-b79c-6667356f0f50);}.\\39 48d7ce9-e863-45e7-a3e6-179e4cab85de,.e7979375-7020-4a99-8f60-046cf1838a15{stroke:#fff;}.e7979375-7020-4a99-8f60-046cf1838a15{stroke-width:1.5px;}.b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4,.c09415f1-be70-4123-b273-3f949364e3b0,.ffa89ab7-8120-4f9d-b7d7-33e849fbefce{fill:#fff;}.e74f067f-41ce-4693-b2d0-e5ae056d20f0{opacity:0.1;}.\\39 3fdd1a2-f0db-421b-ab67-13a781186ef8{clip-path:url(#5543b119-0a35-4f6a-8995-13035f88b8c8);}.\\39 5d8ff88-c217-4d42-8167-ef4e8468a91f{clip-path:url(#8f3b93e6-7373-41c9-8d45-8ca77089574f);}.\\30 93fd0ba-d0ec-4f8b-a6aa-a26a6b7b6050{clip-path:url(#34655791-7bea-4df6-96e0-21b1d3202541);}.dd6ae827-876a-4553-96e5-bd1bf972d71c{clip-path:url(#2c134469-5434-4b87-bef0-3e51a5ddff38);}.f8b67d6e-2a5b-4c45-b1fa-aaeb448e4c8b{opacity:0.5;}.b1af0568-0922-49a0-8b80-34d724b01ca3{clip-path:url(#e7142ba1-852e-4276-8e75-951dbe3dc91d);}.fdd48387-9079-4f7f-a6dc-c6966447a7a9{fill:#5188cc;}.\\31 d7519ae-8173-4095-972a-09870179f637{fill:#222324;}.\\36 2cd76c4-80a4-4944-a40d-e6dd289b40b2{stroke:#cbcbcb;}.c09415f1-be70-4123-b273-3f949364e3b0{font-size:16px;font-family:Arial-BoldMT, Arial;font-weight:700;}.\\33 96e18d4-985a-416e-8e80-c6e04f9e6963{clip-path:url(#9dbcf618-2fdd-4edb-9bb1-bf1e4f7975f6);}.\\36 750b86e-6d7e-4150-af85-a92b4c419fc5{fill:#7d99aa;}.a0bc97b4-00b0-4886-83d1-452e86389d8d{letter-spacing:-0.02em;}.ca8549ca-5caa-42b6-9100-70881d6a7b8b{clip-path:url(#18513fca-3652-4a9d-a6f0-65aaa65e4df4);}.ada718fd-cebf-4711-b8c3-704fb1f779d3{fill:#3766a0;}.\\33 2b6ab8f-133c-4a3b-a1b9-9a7317c1f0be{letter-spacing:-0.05em;}.b48e2208-b83a-4ec1-aaa4-b4c568f7e4f2{letter-spacing:-0.07em;}.b6578f7b-ad84-44b3-821b-0da8997a72f5{clip-path:url(#42e3cb79-b3b4-4f1e-91f6-29fc70ff71fd);}.\\33 2ea3dbb-e8f2-49b2-bc90-93718ff0c09d{fill:#123054;}.\\31 252fbb6-43e0-41b4-9c05-90c463ae83ca{clip-path:url(#51f58aef-e770-4760-ae30-c450f980ad39);}.d32fb565-8367-4ad9-9c31-83d4ceae1048{clip-path:url(#d05b77a7-9835-4a3b-a28f-720ec3ddf709);}.d76e5379-b6ef-4363-8b7b-ccff3b21fda0{clip-path:url(#dd505d09-b6fe-4bfa-a794-94f42e6dd707);}.ffa89ab7-8120-4f9d-b7d7-33e849fbefce{font-size:14px;font-family:ArialMT, Arial;}.\\35 32ac923-9f8e-48c0-af56-7156d9ba9a2c{letter-spacing:-0.05em;}.fe2a04d7-14bd-452c-a088-a2e19bdfd276{letter-spacing:-0.06em;}.\\32 a902ce1-836a-4c47-93a9-c50279cd09aa{letter-spacing:-0.11em;}.\\36 d55a60d-d6ee-438f-a912-dd248843640d{letter-spacing:-0.05em;}</style>\n"
                + "                    <clipPath id=\"6f1a5c01-8a67-4f39-9c89-e6ebb5c5104d\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" y=\"0.02\" width=\"1284.19\" height=\"491.98\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"9ebe03d8-5253-4130-a231-11ee2b3d72e4\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" y=\"0.02\" width=\"1284.19\" height=\"491.98\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"d7d457c3-9189-455b-81e9-2fdc0b00b6e6\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"239\" y=\"429.5\" width=\"659\" height=\"63.5\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"5ee91c21-ae52-4572-b067-45471febbd86\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"239\" y=\"78\" width=\"153\" height=\"341\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"8471efae-f4ac-4b5d-b81e-13945af9d1dd\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"491\" y=\"78\" width=\"153\" height=\"341\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"7d79421d-9bf4-4cd5-bad0-6e5b0c5dc060\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"745\" y=\"78\" width=\"153\" height=\"341\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"b4823c3a-ecc2-484b-a855-e75a6647830f\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"243.52\" y=\"83.64\" width=\"143\" height=\"139.5\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"b88a74e0-1c37-4d6a-9751-ac977e8b8c1e\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"-9\" y=\"83.64\" width=\"143\" height=\"139.5\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"fbff60ff-4779-4f49-b604-66082872749c\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"496.8\" y=\"83.64\" width=\"143\" height=\"139.5\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"ea4ed90c-f89a-40f8-ab46-bb24e4a0193d\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"750.42\" y=\"83.64\" width=\"143\" height=\"139.5\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"1d4f0fff-1e5c-497d-ab22-3283f8ec02de\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"390.84\" y=\"5.02\" width=\"16\" height=\"16\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"f34f5673-eda8-4f30-9a76-736188696dc8\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"390.84\" y=\"5.02\" width=\"16\" height=\"16\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"fdc809f1-7cce-4915-b8cc-f4e1cdbc5232\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"644.84\" y=\"5.02\" width=\"16\" height=\"16\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"745bae07-913d-4a4e-91b4-a836df6204b4\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"644.84\" y=\"5.02\" width=\"16\" height=\"16\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"6097873a-0458-4f24-bbb2-23a63ada0950\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"897.84\" y=\"5.02\" width=\"16\" height=\"16\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"66fa8ae2-4a59-4f36-82fd-f23cc43d491e\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"897.84\" y=\"5.02\" width=\"16\" height=\"16\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"c0c1634f-3f6e-47dc-9f8c-8006d921eb54\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"1142.84\" y=\"5.02\" width=\"16\" height=\"16\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"3fccbc3d-da22-423e-935d-df1b3bc74d47\" transform=\"translate(0 -0.02)\">\n"
                + "                        <path class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" d=\"M467.17,4c-.12.1-.22.21-.33.32V21.69l.33.33h17.34l.33-.33V4.34L484.52,4Z\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"0115b0e1-6d7c-4ecf-aabc-279c9c9a0c59\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"466.84\" y=\"4.02\" width=\"18\" height=\"18\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"a7e3bb04-2f39-44a5-b963-e880172fd5fc\" transform=\"translate(0 -0.02)\">\n"
                + "                        <path class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" d=\"M56.74,360.29a12.72,12.72,0,0,0-3.83,3.83v13.44a12.69,12.69,0,0,0,3.83,3.82H70.17A12.57,12.57,0,0,0,74,377.56V364.12a12.61,12.61,0,0,0-3.83-3.83Z\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"8ddb473d-97ff-42a9-8176-7d79c7d6b72c\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"52.91\" y=\"360.3\" width=\"21.09\" height=\"21.09\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"be80d5f1-3727-4515-8951-6e4816f0abcd\" transform=\"translate(0 -0.02)\">\n"
                + "                        <path class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" d=\"M721.16,4c-.11.1-.21.21-.32.32V21.69l.32.33h17.35l.33-.33V4.34L738.52,4Z\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"ede669f2-603f-4635-8bb6-655674689d69\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"720.84\" y=\"4.02\" width=\"18\" height=\"18\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"4dd30be1-4919-48b9-ad24-d881f3753df0\" transform=\"translate(0 -0.02)\">\n"
                + "                        <path class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" d=\"M974.16,4c-.11.1-.21.21-.32.32V21.69l.32.33h17.35l.33-.33V4.34L991.52,4Z\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"389700f5-57e3-4551-a967-485921ac0c41\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"973.84\" y=\"4.02\" width=\"18\" height=\"18\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"f2fdd935-0985-4436-bbb0-c8c98af870ac\" transform=\"translate(0 -0.02)\">\n"
                + "                        <path class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" d=\"M1219.17,4l-.33.32V21.69c.11.11.21.22.33.33h17.34l.33-.33V4.34l-.33-.32Z\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"3a0ea917-4e1c-4eb8-ae0a-f4a75c0dff88\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"1218.84\" y=\"4.02\" width=\"18\" height=\"18\"/>\n"
                + "                    </clipPath>\n"
                + "                    <clipPath id=\"42718428-3a2e-4907-a88a-7d2eba36b889\" transform=\"translate(0 -0.02)\">\n"
                + "                        <rect class=\"af0df502-b7f6-45df-a78e-fe8c15c16f77\" x=\"1004.38\" y=\"83.64\" width=\"143\" height=\"139.5\"/>\n"
                + "                    </clipPath>\n"
                + "                    <image id=\"835e3d20-6d0a-40fc-9d73-28fd1915bd98\" width=\"62\" height=\"62\" xlink:href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAAA/CAYAAABXXxDfAAAACXBIWXMAACroAAAq6AHYwlNLAAACQklEQVRoQ+2bPWsUQRiAn9NoCrW3MghBRSzUJkUKA4KCIZA0/gArUwki2Pkj/AuWFqYIhhAhRawifuWjCAZJpQTiJ2ogGsdic7Be3t13drJ3u3fvPLCQnXtusw83mUux03DO0Ub6gEXgkiZmsARcBnY1MYRGm+MHgA1NUjjNwa8hckgTDkhDE6qkTxNK5hrwTnHOALOKUwqdjn8BfFUc7fXSaPe0rzW+n/xJ4AEwBBxW3DRHW84X0Ffu1us/BXYkMYM/wApwH9jME31X+3lgRJNqxgxwI0/wjd+l+/5E/qLMUt8gX69OqPesCr1MjPfAa2HoNnzjn2tCDXmpCb6r/SDwEDiviTXhFXAPeJ8n+cb3JL7Tvifx/fc2zRHgmCZVxE/gtyY1CYkfBZ5oUkVMAFOa1MT0tI/xVjEdH7LgZfEMWN37+ThwEzgheDvAOjCXGrtCci9nSb5NWvkBPAa+752fA64LXjGcc0WPcbeft4J3V/Ccc25ScJvHZMZ77gjusuCNC17mUda0/yKMfRbGAD5mjOe99k0Y+ySMFaKs+K4kxlslxlslxlslxlslxlslxlslxlslxlslxlslxlslxlslxlslxlslxlulivi8TUcdvZ+yftkF4FTqvB8YkVUmkB9A6AfGhHGAq/y/a2OAEp4GDXkCcxSY1qSKGKPAvYV88uuaUCEfNCFNSPwa8FqTKmADeKNJaULiAW4D25rUQbaBWyT7arwJjV8Ehkm2ff1S3HayBTwCLpLsACtEyILXM/wDHl8orCdI2ugAAAAASUVORK5CYII=\"/>\n"
                + "                    <image id=\"d714d3a5-8b0f-436a-a5fc-a10b6bc31eee\" width=\"36\" height=\"36\" xlink:href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAlCAYAAADFniADAAAACXBIWXMAABYlAAAWJQFJUiTwAAABKElEQVRYR+2XQU7DMBBFXxB7xIbr4PtwhyrdwSngKOU2za43+GxKlYRxxo4bp6l40iyimdhfnvmJ3Eji1njwCtZg86IaYA8cAV0pjuc1G/pISo13LceHens1Sh/0E/DkFc3kBDz/PuSISi6cyaWFj1NVDt/AwSuKEIDXaFaKztA4xrQJ78SiNda75K2T6rdp6IpKlLQvAK1TEyNMJa1Bj51UtUHP+XhWo6R9Vd1nOkJ/Wcx9d9e+wL/7VqakfSnuC0y5LEKJqAP+TLXMEHV37Qv4JxWcvMnm3dd5BQUM1s4R9ekVFPA1eDL+S+b/6Bw7SZ2uRydpr9E+OTNVjZz2VWMzovpO8K7dc8LFErWky5KI3ZB3wBvwYiULcc2Tc22vxmYGfXVuUtQPE+peevYGvFYAAAAASUVORK5CYII=\"/>\n"
                + "                    <image id=\"d714d3a5-8b0f-436a-a5fc-a10b6bc31eed\" width=\"36\" height=\"36\" xlink:href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAABBCAIAAADKTrrsAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADYSURBVGhD7ZnBDcQgDASpi4Koh2pohmLukghp7TtO8gNsTtr55pGJvd5PEvlJaf3lRu+t5PFiRa6OFoNWxssFpY2HnnyLRIzj2k793A09NKd4xOR04hEykNnh+vbYw2QcK7GueK+GOWhbNf7MYp7QRZxhYT98WoCd8TzDgoUhYG0B1hZgbQHWloCFAWgBWFuAtSVgbQEWBmBtAdYWYG0JWFvgjCOxD2OrhX0Ya+n6q6I0LlTiAz3Unu2Xsh45EHpoj8h8qKAGDkR5xIlojZtc3X+MzTRISukNcmUESXtMOhcAAAAASUVORK5CYII=\"/>\n"
                + "                </defs>";
    }

    @SuppressWarnings("checkstyle:methodlength")
    private String getPfeile(ReportingProcessOverviewController reportingProcessOverviewController) {
        List<SVGElement> elements = new ArrayList<>();
        elements.add(new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(191.96 292.09)", reportingProcessOverviewController.getStringfromProperty("reporting_ruecklauferInDasFachteam")));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 184.75, 156.67, 265.1, 156.67));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(269.75, 156.67), new Point(263.05, 150.44), new Point(263.05, 162.91), new Point(269.75, 156.67)
        )));
        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 437.83, 156.67, 518.18, 156.67));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(522.83, 156.67), new Point(516.13, 150.44), new Point(516.13, 162.91), new Point(522.83, 156.67)
        )));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 691.87, 156.67, 772.22, 156.67));

        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(776.87, 156.67), new Point(770.17, 150.44), new Point(770.17, 162.91), new Point(776.87, 156.67)
        )));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 945.49, 156.67, 1025.84, 156.67));

        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(1030.49, 156.67), new Point(1023.79, 150.44), new Point(1023.79, 162.91), new Point(1030.49, 156.67)
        )));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 149.84, 156.67, 166.10, 156.67));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(170.84, 156.67), new Point(164.14, 150.44), new Point(164.14, 162.91), new Point(170.84, 156.67)
        )));
        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 402.92, 156.67, 419.27, 156.67));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(423.92, 156.67), new Point(417.22, 150.44), new Point(417.22, 162.91), new Point(423.92, 156.67))));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 656.96, 156.67, 673.31, 156.67));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(677.96, 156.67), new Point(671.25, 150.44), new Point(671.25, 162.91), new Point(677.96, 156.67)
        )));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 910.58, 156.67, 926.93, 156.67));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(931.58, 156.67), new Point(924.88, 150.44), new Point(924.88, 162.91), new Point(931.58, 156.67)
        )));

        elements.add(new Line("01e056ae-e1a3-4d3c-bc83-4c867c3167ae", 182.42, 25.67, 182.42, 140.36));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(182.42, 144.47), new Point(186.57, 140.0), new Point(178.26, 140.0), new Point(182.42, 144.47)
        )));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 435.33, 146.46, 435.33, 32.33));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(435.33, 27.67), new Point(441.56, 34.38), new Point(429.1, 34.38), new Point(435.33, 27.67)
        )));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 689.33, 146.46, 689.33, 32.33));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(689.33, 27.67), new Point(695.57, 34.38), new Point(683.1, 34.38), new Point(689.33, 27.67)
        )));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 942.33, 146.46, 942.33, 32.33));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(942.33, 27.67), new Point(948.57, 34.38), new Point(936.1, 34.38), new Point(942.33, 27.67)
        )));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 1187.33, 156.67, 1187.33, 32.33));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(1187.33, 27.67), new Point(1193.57, 34.38), new Point(1181.1, 34.38), new Point(1187.33, 27.67)
        )));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 1138.37, 156.38, 1187.33, 156.38));

        elements.add(new Line("8e99f5e6-3091-4098-9825-979e047c5052", 145.84, 17.5, 145.84, 16.5));
        elements.add(new Line("8e99f5e6-3091-4098-9825-979e047c5052", 145.84, 19.5, 145.84, 18.5));
        elements.add(new Line("8e99f5e6-3091-4098-9825-979e047c5052", 145.84, 21.5, 145.84, 20.5));

        elements.add(new Line("01e056ae-e1a3-4d3c-bc83-4c867c3167ae", 184.42, 238.48, 182.41, 172.58));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(182.42, 168.48), new Point(186.57, 172.95), new Point(178.26, 172.95), new Point(182.42, 168.48)
        )));

        elements.add(new Line("01e056ae-e1a3-4d3c-bc83-4c867c3167ae", 942.33, 300.98, 942.33, 165.98));
        elements.add(new Line("01e056ae-e1a3-4d3c-bc83-4c867c3167ae", 689.33, 293.88, 689.33, 165.98));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(689.33, 297.98), new Point(685.18, 293.51), new Point(693.49, 293.51), new Point(689.33, 297.98)
        )));

        elements.add(new Line("01e056ae-e1a3-4d3c-bc83-4c867c3167ae", 182.41, 300.98, 182.42, 264.08));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(182.41, 259.98), new Point(186.57, 264.45), new Point(178.26, 264.45), new Point(182.41, 259.98)
        )));

        elements.add(new Line("01e056ae-e1a3-4d3c-bc83-4c867c3167ae", 182, 300.48, 942, 300.48));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 257.17, 369.04, 372.87, 369.04));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(252.52, 369.04), new Point(259.22, 375.27), new Point(259.22, 362.81), new Point(252.52, 369.04)
        )));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(377.52, 369.04), new Point(370.81, 362.81), new Point(370.81, 375.27), new Point(377.52, 369.04)
        )));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 510.45, 369.04, 626.15, 369.04));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(505.8, 369.04), new Point(512.5, 375.27), new Point(512.5, 362.81), new Point(505.8, 369.04)
        )));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(630.8, 369.04), new Point(624.1, 362.81), new Point(624.1, 375.27), new Point(630.8, 369.04)
        )));

        elements.add(new Line("b3050d9d-d9a1-416b-be97-57f68a2e524d", 764.07, 369.04, 879.77, 369.04));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(759.42, 369.04), new Point(766.13, 375.27), new Point(766.13, 362.81), new Point(759.42, 369.04)
        )));
        elements.add(new Polygon("cb17c188-b9c0-44f6-89fc-25349d833054", Arrays.asList(
                new Point(884.42, 369.04), new Point(877.72, 362.81), new Point(877.72, 375.27), new Point(884.42, 369.04)
        )));

        return SVGElementDrawer.drawElements(elements);
    }

    private static String getClock(ReportingProcessOverviewController reportingProcessOverviewController) {
        List<SVGElement> elements = new ArrayList<>();
        elements.add(new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(87.05 367.52)", reportingProcessOverviewController.getStringfromProperty("reporting_durchlaufzeitIn")));
        elements.add(new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(85.05 384.32)", reportingProcessOverviewController.getStringfromProperty("reporting_arbeitstage")));

        String result = SVGElementDrawer.drawElements(elements)
                + "g class=\"5696573c-610f-4835-b7b0-483d9f7af8e7\">\n"
                + "                        <g class=\"bb09ff8a-b3da-4537-8035-b040abf1712e\">\n"
                + "                            <image width=\"36\" height=\"36\" transform=\"translate(52.91 360.28) scale(0.59)\" xlink:href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAlCAYAAADFniADAAAACXBIWXMAABLqAAAS6gEWyM/fAAACuUlEQVRYR+2YPUhVYRjH/ze0vGFTBJVDQYXODX0IQdBS0VCtEoYtDUVkH9CkBGZNOVWQTUabpDRUUNBU6FotQV1DM9FsNBzy13DOzee+5z3nPV69dIf+8MDDef/P7324XN6vAqB607qQ4V/of1N51RAypKhdUqekvZK2SNoq6bekuTjGJD2R9DYNkCkgbzQBF4ES+fUZuABsIMz/G0FDHGeAWarXDBEjNE/upnpIah4YBI4DbUAzsAloBU4AD4GfnroewvMFm7rvQL8C54HGEBhYH3snHcaDUG3W4G0HNkL0vwo140YTMOywbmXVpA10OZCBLEjOuOswu9K8vo9NwA9T/AwopAGAw0AvcDbDo5gxarhzQNHn9RVfNYVTwEZfoYne2Psm4FPMmlrG0+3z+Vb0Kya/IWnB46lWC4qYZV3zmdymDihanSWpJGlIa68hRWwpmmu/a3CbOmXyUdVOln3aHXSb2mfyF6qdnpvczikp2VSLyUuqnSZMvt0dzGpqRrWTZds5JSWbWkrJ86hF0raQKZZlJ87jblPfTZ74WVP0XtKipN2SPkm6Lqkxs6KSbeeUlGxq2uQ7lU/DktokvZTULOmOpI+SjmTU7DC5nVNSsqlxkx9Vfk0o8p+UNClpj6RXkp6qsoGyjpl8LDHqLPHtZgso+baAHFEE+oDFmPMLOOR4vrCsgy7DBRaAaVPQ4RasIHYBr4EPQIP53mH403g2ex+s2xTl2ZBDsdnkReCb4V/21fggRaJjRVmjZB9d8kaB6BhU1oqOLgLOUam1OOQNOMzONG8WpN+BrOY4POKw+rJqQkD34jDB6i8O90K1IbCAmyQ1DzwiumK1El2v7BVrkBpescrRSZ1dRstRBC5RufCFVNW1vQBVPZr5HjiWJM0qeuAYl/RY0rs0QJaqbaqmqsv3qbps6g8hZboFRq9YhwAAAABJRU5ErkJggg==\"/>\n"
                + "                        </g>\n"
                + "                    </g>\n"
                + "                    <g class=\"a7a7d0c6-d834-4f16-aa8c-4d2cc48187d8\">\n"
                + "                        <circle class=\"e10c3a5a-f925-4bfe-b875-b1dab086c709\" cx=\"63.46\" cy=\"370.82\" r=\"12.5\"/>\n"
                + "                    </g>\n";

        return result;
    }

    private static String getKeyfigureBoxes(ProcessOverviewKeyFigures keyFigures) {
        List<SVGElement> elements = new ArrayList<>();

        elements.add(new KeyFigureBox("M114.67,143.89a3.38,3.38,0,0,0-3.5,3.5v18a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5v-18a3.38,3.38,0,0,0-3.5-3.5Z", 110.67, 162.2, keyFigures.getKeyFigureByTypeId(1)));
        // neue Version (KF2)
        elements.add(new KeyFigureBox("M166.19.52a3.37,3.37,0,0,0-3.5,3.5V22a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5V4a3.38,3.38,0,0,0-3.5-3.5Z", 158.5, 18.82, keyFigures.getKeyFigureByTypeId(2)));
        //Rückläufer (KF4)
        elements.add(new KeyFigureBox("M166.19,232.59a3.37,3.37,0,0,0-3.5,3.5v18a3.37,3.37,0,0,0,3.5,3.5h32a3.38,3.38,0,0,0,3.5-3.5v-18a3.38,3.38,0,0,0-3.5-3.5Z", 159.5, 250.89, keyFigures.getKeyFigureByTypeId(4)));
        //Eingang Fachteam (KF3)
        elements.add(new KeyFigureBox("M211.38,143.89a3.38,3.38,0,0,0-3.5,3.5v18a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5v-18a3.37,3.37,0,0,0-3.5-3.5Z", 206.5, 162.2, keyFigures.getKeyFigureByTypeId(3)));
        //Ausgang Fachteam (KF6)
        elements.add(new KeyFigureBox("M367.58,143.89a3.38,3.38,0,0,0-3.5,3.5v18a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5v-18a3.37,3.37,0,0,0-3.5-3.5Z", 362.5, 162.2, keyFigures.getKeyFigureByTypeId(6)));
        //Fachteam geloescht (KF8)
        elements.add(new KeyFigureBox("M419.19.52a3.37,3.37,0,0,0-3.5,3.5V22a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5V4a3.38,3.38,0,0,0-3.5-3.5Z", 412.5, 18.82, keyFigures.getKeyFigureByTypeId(8)));

        //TTeam input (KF7)
        elements.add(new KeyFigureBox("M464.3,143.89a3.38,3.38,0,0,0-3.5,3.5v18a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5v-18a3.38,3.38,0,0,0-3.5-3.5Z", 459.5, 162.2, keyFigures.getKeyFigureByTypeId(7)));
        //TTeam output (KF11)
        elements.add(new KeyFigureBox("M621.33,143.89a3.38,3.38,0,0,0-3.5,3.5v18a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5v-18a3.37,3.37,0,0,0-3.5-3.5Z", 615.5, 162.2, keyFigures.getKeyFigureByTypeId(11)));
        //Vereinbarung input (KF12)
        elements.add(new KeyFigureBox("M718,143.89a3.38,3.38,0,0,0-3.5,3.5v18a3.38,3.38,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5v-18a3.37,3.37,0,0,0-3.5-3.5Z", 712.5, 162.2, keyFigures.getKeyFigureByTypeId(12)));
        //TTEAM geloescht (KF15)
        elements.add(new KeyFigureBox("M673.19.52a3.37,3.37,0,0,0-3.5,3.5V22a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5V4a3.38,3.38,0,0,0-3.5-3.5Z", 666.5, 18.82, keyFigures.getKeyFigureByTypeId(15)));

        //TTEAM unstimmig (KF13)
        elements.add(new KeyFigureBox("M673.29,232.59a3.37,3.37,0,0,0-3.5,3.5v18a3.37,3.37,0,0,0,3.5,3.5h32a3.38,3.38,0,0,0,3.5-3.5v-18a3.37,3.37,0,0,0-3.5-3.5Z", 665.5, 250.89, keyFigures.getKeyFigureByTypeId(13)));
        //Vereinbarung ausgang (KF17)
        elements.add(new KeyFigureBox("M874.7,143.89a3.38,3.38,0,0,0-3.5,3.5v18a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5v-18a3.37,3.37,0,0,0-3.5-3.5Z", 868.5, 162.2, keyFigures.getKeyFigureByTypeId(17)));
        //Vereinbarung Freigegeben (KF19)
        elements.add(new KeyFigureBox("M971.41,143.89a3.38,3.38,0,0,0-3.5,3.5v18a3.38,3.38,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5v-18a3.37,3.37,0,0,0-3.5-3.5Z", 964.0, 162.2, keyFigures.getKeyFigureByTypeId(19)));
        // Vereinbarung gelöscht (KF20)
        elements.add(new KeyFigureBox("M926.19.52a3.37,3.37,0,0,0-3.5,3.5V22a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5V4a3.38,3.38,0,0,0-3.5-3.5Z", 918.5, 18.82, keyFigures.getKeyFigureByTypeId(20)));

        // Vereinbarung unstimmig (KF18)
        elements.add(new KeyFigureBox("M927,232.59a3.37,3.37,0,0,0-3.5,3.5v18a3.37,3.37,0,0,0,3.5,3.5h32a3.38,3.38,0,0,0,3.5-3.5v-18a3.37,3.37,0,0,0-3.5-3.5Z", 919.5, 250.89, keyFigures.getKeyFigureByTypeId(18)));
        // Freigegebe geloescht (KF22)
        elements.add(new KeyFigureBox("M1171.19.52a3.37,3.37,0,0,0-3.5,3.5V22a3.37,3.37,0,0,0,3.5,3.5h32a3.37,3.37,0,0,0,3.5-3.5V4a3.38,3.38,0,0,0-3.5-3.5Z", 1165.5, 18.82, keyFigures.getKeyFigureByTypeId(22)));

        return SVGElementDrawer.drawElements(elements);
    }

    private static String getPlusIcons() {
        List<SVGElement> elements = new ArrayList<>();
        elements.add(new PlusIcon(182.42, 156.38, 9.45, "M182.42,165.84a9.45,9.45,0,1,0-9.45-9.45,9.45,9.45,0,0,0,9.45,9.45", "translate(0 -0.02)", Arrays.asList(
                new Point(183.42, 157.41), new Point(183.42, 157.41), new Point(186.43, 157.41), new Point(186.43, 155.35), new Point(183.42, 155.35), new Point(183.42, 152.37),
                new Point(181.41, 152.37), new Point(181.41, 155.35), new Point(178.41, 157.41), new Point(181.41, 157.41), new Point(181.41, 160.38), new Point(183.42, 160.38))));

        elements.add(new PlusIcon(435.33, 156.39, 9.45, "M435.33,165.84a9.45,9.45,0,1,0-9.45-9.45,9.45,9.45,0,0,0,9.45,9.45", "translate(0 -0.02)", Arrays.asList(
                new Point(436.33, 160.38), new Point(436.33, 157.41), new Point(439.34, 157.41), new Point(439.34, 155.35), new Point(436.33, 155.35), new Point(436.33, 152.37),
                new Point(434.33, 152.37), new Point(434.33, 155.35), new Point(431.32, 155.35), new Point(431.32, 157.41), new Point(434.33, 157.41), new Point(434.33, 160.38))));

        elements.add(new PlusIcon(689.08, 156.39, 9.45, -10.9, "M689.08,165.84a9.45,9.45,0,1,0-9.45-9.45,9.45,9.45,0,0,0,9.45,9.45", "translate(0 -0.02)", Arrays.asList(
                new Point(690.08, 160.38), new Point(690.08, 157.41), new Point(693.09, 157.41), new Point(693.09, 155.35), new Point(690.08, 155.35), new Point(690.08, 152.37),
                new Point(688.08, 152.37), new Point(688.08, 155.35), new Point(685.07, 155.35), new Point(685.07, 157.41), new Point(688.08, 157.41), new Point(688.08, 160.38))));

        elements.add(new PlusIcon(942.45, 156.39, 9.45, -10.9, "M942.45,165.84a9.45,9.45,0,1,0-9.45-9.45,9.45,9.45,0,0,0,9.45,9.45", "translate(0 -0.02)", Arrays.asList(
                new Point(943.45, 160.38), new Point(943.45, 157.41), new Point(946.46, 157.41), new Point(946.46, 155.35), new Point(943.45, 155.35), new Point(943.45, 152.37),
                new Point(941.45, 152.37), new Point(941.45, 155.35), new Point(938.44, 155.35), new Point(938.44, 157.41), new Point(941.45, 157.41), new Point(941.45, 160.38))));

        return SVGElementDrawer.drawElements(elements);
    }

    private static String getLegende(String vonDateFilter, String bisDateFilter, ReportingProcessOverviewController reportingProcessOverviewController) {
        List<SVGElement> elements = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        elements.add(new Rectangle("592c5a5e-ada3-49a0-83c0-bc6f96857966", "cb17c188-b9c0-44f6-89fc-25349d833054", "cb17c188-b9c0-44f6-89fc-25349d833054", 0, 450, 1200, 40));
        sb.append(SVGElementDrawer.drawElements(elements));
        elements.clear();

        String zeitraumBox = "<g class=\"a7a7d0c6-d834-4f16-aa8c-4d2cc48187d8\">"
                + "<path class=\"e42c4fa2-60b7-493c-bbe3-5b7b96c79774\" d=\"M15.19,455.59a3.37,3.37,0,0,0-3.5,3.5v18a3.37,3.37,0,0,0,3.5,3.5h32a3.38,3.38,0,0,0,3.5-3.5v-18a3.38,3.38,0,0,0-3.5-3.5Z\" transform=\"translate(0 -0.02)\"/>"
                + "<path class=\"e10c3a5a-f925-4bfe-b875-b1dab086c709\" d=\"M15.19,455.59a3.37,3.37,0,0,0-3.5,3.5v18a3.37,3.37,0,0,0,3.5,3.5h32a3.38,3.38,0,0,0,3.5-3.5v-18a3.38,3.38,0,0,0-3.5-3.5Z\" transform=\"translate(0 -0.02)\"/></g>)";

        sb.append(zeitraumBox);

        elements.add(new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(25 473)", "X"));
        elements.add(new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(55 465), scale(0.7)", vonDateFilter + " - "));
        elements.add(new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(55 478), scale(0.7)", bisDateFilter));
        elements.add(new Path("1fc087fc-5ad7-4044-a16b-f1da2c7b2c10", "M160,625a3.37,3.37,0,0,0,0,4.95l19.09,19.1a3.39,3.39,0,0,0,4.95,0l19.09-19.1a3.37,3.37,0,0,0,0-4.95L184,605a3.37,3.37,0,0,0-4.95,0Z", "translate(0), scale(0.75)"));
        elements.add(new Path("e10c3a5a-f925-4bfe-b875-b1dab086c709", "M160,625a3.37,3.37,0,0,0,0,4.95l19.09,19.1a3.39,3.39,0,0,0,4.95,0l19.09-19.1a3.37,3.37,0,0,0,0-4.95L184,605a3.37,3.37,0,0,0-4.95,0Z", "translate(0), scale(0.75)"));
        elements.add(new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(131 475)", "X"));
        elements.add(new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(158 473), scale(0.9)", bisDateFilter));
        elements.add(new Icon(240, 455, "translate(390.84 400)", Icon.IconType.GELOESCHT, "d54dadb5-1813-468d-a83a-47d607bf1a7e", "64b08909-b7a5-4076-8b0e-d558e91a1f33"));
        elements.add(new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(260 473)", reportingProcessOverviewController.getStringfromProperty("reporting_legende_geloeschteSSAs")));

        elements.add(new Icon(390, 455, "translate(390.84 400)", Icon.IconType.PZBK, "d54dadb5-1813-468d-a83a-47d607bf1a7e", "64b08909-b7a5-4076-8b0e-d558e91a1f33"));
        elements.add(new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(405 473)", reportingProcessOverviewController.getStringfromProperty("reporting_legende_inPZBK")));
        elements.add(new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(540 473)", reportingProcessOverviewController.getStringfromProperty("reporting_legende_neuversionierteSSAs")));
        elements.add(new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(930 473)", reportingProcessOverviewController.getStringfromProperty("reporting_legende_langlaufer")));

        elements.add(new Icon(703, 455, "translate(390.84 400)", Icon.IconType.MELDUNGEN_MEHRFACH_ZUGEORDNET, "d54dadb5-1813-468d-a83a-47d607bf1a7e", "64b08909-b7a5-4076-8b0e-d558e91a1f33"));
        elements.add(new Text("5f459e6e-0411-4145-a215-c5424afc40fe", "translate(720 473)", reportingProcessOverviewController.getStringfromProperty("Reporting_ls_Legende_SSA-mehrfach-zugeordnet")));

        sb.append(SVGElementDrawer.drawElements(elements));
        sb.append("<image id=\"835e3d20-6d0a-40fc-9d73-28fd1915bd98\" transform=\"scale(0.26), translate(891 1772)\" width=\"62\" height=\"62\" xlink:href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAAA/CAYAAABXXxDfAAAACXBIWXMAACroAAAq6AHYwlNLAAACQklEQVRoQ+2bPWsUQRiAn9NoCrW3MghBRSzUJkUKA4KCIZA0/gArUwki2Pkj/AuWFqYIhhAhRawifuWjCAZJpQTiJ2ogGsdic7Be3t13drJ3u3fvPLCQnXtusw83mUux03DO0Ub6gEXgkiZmsARcBnY1MYRGm+MHgA1NUjjNwa8hckgTDkhDE6qkTxNK5hrwTnHOALOKUwqdjn8BfFUc7fXSaPe0rzW+n/xJ4AEwBBxW3DRHW84X0Ffu1us/BXYkMYM/wApwH9jME31X+3lgRJNqxgxwI0/wjd+l+/5E/qLMUt8gX69OqPesCr1MjPfAa2HoNnzjn2tCDXmpCb6r/SDwEDiviTXhFXAPeJ8n+cb3JL7Tvifx/fc2zRHgmCZVxE/gtyY1CYkfBZ5oUkVMAFOa1MT0tI/xVjEdH7LgZfEMWN37+ThwEzgheDvAOjCXGrtCci9nSb5NWvkBPAa+752fA64LXjGcc0WPcbeft4J3V/Ccc25ScJvHZMZ77gjusuCNC17mUda0/yKMfRbGAD5mjOe99k0Y+ySMFaKs+K4kxlslxlslxlslxlslxlslxlslxlslxlslxlslxlslxlslxlslxlslxlulivi8TUcdvZ+yftkF4FTqvB8YkVUmkB9A6AfGhHGAq/y/a2OAEp4GDXkCcxSY1qSKGKPAvYV88uuaUCEfNCFNSPwa8FqTKmADeKNJaULiAW4D25rUQbaBWyT7arwJjV8Ehkm2ff1S3HayBTwCLpLsACtEyILXM/wDHl8orCdI2ugAAAAASUVORK5CYII=\"></image>");
        sb.append("<image id=\"835e3d20-6d0a-40fc-9d73-28fd1915bd98\" transform=\"scale(0.26), translate(2670  1772)\" width=\"62\" height=\"62\" xlink:href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAABBCAIAAADKTrrsAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADYSURBVGhD7ZnBDcQgDASpi4Koh2pohmLukghp7TtO8gNsTtr55pGJvd5PEvlJaf3lRu+t5PFiRa6OFoNWxssFpY2HnnyLRIzj2k793A09NKd4xOR04hEykNnh+vbYw2QcK7GueK+GOWhbNf7MYp7QRZxhYT98WoCd8TzDgoUhYG0B1hZgbQHWloCFAWgBWFuAtSVgbQEWBmBtAdYWYG0JWFvgjCOxD2OrhX0Ya+n6q6I0LlTiAz3Unu2Xsh45EHpoj8h8qKAGDkR5xIlojZtc3X+MzTRISukNcmUESXtMOhcAAAAASUVORK5CYII=\"></image>");
        sb.append("<image id=\"d714d3a5-8b0f-436a-a5fc-a10b6bc31eee\" transform=\"scale(0.5), translate(760 920)\" width=\"36\" height=\"36\" xlink:href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAlCAYAAADFniADAAAACXBIWXMAABYlAAAWJQFJUiTwAAABKElEQVRYR+2XQU7DMBBFXxB7xIbr4PtwhyrdwSngKOU2za43+GxKlYRxxo4bp6l40iyimdhfnvmJ3Eji1njwCtZg86IaYA8cAV0pjuc1G/pISo13LceHens1Sh/0E/DkFc3kBDz/PuSISi6cyaWFj1NVDt/AwSuKEIDXaFaKztA4xrQJ78SiNda75K2T6rdp6IpKlLQvAK1TEyNMJa1Bj51UtUHP+XhWo6R9Vd1nOkJ/Wcx9d9e+wL/7VqakfSnuC0y5LEKJqAP+TLXMEHV37Qv4JxWcvMnm3dd5BQUM1s4R9ekVFPA1eDL+S+b/6Bw7SZ2uRydpr9E+OTNVjZz2VWMzovpO8K7dc8LFErWky5KI3ZB3wBvwYiULcc2Tc22vxmYGfXVuUtQPE+peevYGvFYAAAAASUVORK5CYII=\"></image>");
        sb.append("<use href=\"#neuVersioniert\" transform=\"translate(375 455)\" />");
        sb.append("<image width=\"100\" height=\"100\" transform=\"translate(900.0 458.0) scale(0.20)\" xlink:href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFQAAABUCAYAAAAcaxDBAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQwIDc5LjE2MDQ1MSwgMjAxNy8wNS8wNi0wMTowODoyMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NkYxNTYyRjlCOThEMTFFOEE5ODhEMEQ2MUVDMkE2MzgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NkYxNTYyRkFCOThEMTFFOEE5ODhEMEQ2MUVDMkE2MzgiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo2RjE1NjJGN0I5OEQxMUU4QTk4OEQwRDYxRUMyQTYzOCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo2RjE1NjJGOEI5OEQxMUU4QTk4OEQwRDYxRUMyQTYzOCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PsVMbqcAAASLSURBVHja7J0/SBxBFMZnFy/CNZ7R4ppwhSAogmerwtkpVpZBkGCRykqbFGkkELBSm1TpFCRgrWjnEU17FzQGgoqnICImaqOeJ17e82aPu71/e7s7N7Pr++BjGnfv7c83szOzs7Mak6wsYzoUXeAouAfcAY6Aw+B2cNB0yC34EnwOToEPwXvgJPi3xtiTzOvRJEHshGIEPAweALe4dOob8A54E7wBF/eH+VUAMQz+AE6Asw1ygv9m2E8g+8Gr4EwDQZqd4TH0exnkEHhLIsRKxpiGvASyE7ymIEiz13hbrizIZvAn8IMHYBp+4DE3qwazF/zLQyDNxth7VYE5BU57GKZhvIYp2VV8yQcgzV5qeBMAP9gG/u5DmIbx2toa2UHf9TFMw7vCBwQc5sELgGn4oF6oWj3VHIotPoHhrlpgKJ9KOTtHJAIj+RsReYQTL0MA6q/bNyBxbWYolHUsPIfYNtXSjUq3yPQreJC9XA1yBs6B8r7ZBCNNWOmn6rVGQFDME8u85muNqPRq7SYUK+BXxDEvZLFSrT2tlqEfwd3EsETdnI31bhOf1sLuQqAhIYZCjF1dOTtHaytj19eNgprB7mO5RyyVMnShYTC9qQBnVLvK89nsUWJWU6PlZv7LZegssbKs2apA+UOsGHGyrJj5wZ85Q6flTK5m1TiHPU2XvcvzWZVTcJOUsO7voXdnc1737o6xYFAW0EfwGy23kqUoQ99Jg4k6PpZzrHM1cXYlVf6t1NbIu0CL2OkFHfmo1JBOTrwMNGo84zcydET6/dLbGZpnaAAdJqCO9cxQ5+szB6SH4+0qjxpAlsZi1xbKUMdChl269JuRobMz6NE92uuDXlyoMnKKItAeJUJ5eoJhxalXs9NQDwLtUCYcO3DUAtqBQCPKhGPnxqQW0AgCDVOGuqYwAm0noK6pHYEGqcq7pqCuUjQ+yFCmZZ/nRhRRIMBYOg1RWVzDJncetKzUytBMJtfB92h2GkBvPVvt1QN6i0AvlQqpnnWi6gG9RKDnBNQ1nSPQFFV599IBgR4qFVI9fVH1gB4i0D3KUNe0h49Ak0qFdHTE2OSktb9VZx7UUFLjj0D+MRVm7b0tfAXltc736NghHo61gyyNkdIm8XCszcKh5wbxcKyNPFC+tDlJTBzcjPjy8MLJkW/Exbby7NRZzhiLMdbXZ+/YRIKxeFwWzKLljEXiW/HIefN3cdH+e554rLw3llcLGZrnQxeo9tathYpAIW1/QBEnRpYV58wqZihqljhZVgmrEqBabpOBdWJVU+ucFauVoSh8syFDzCoqwyq8MVMWKO+kzhG3ipqrtJVmtaeen8H7xK5E+5wNqwso/AfSUIyDH4hhXshinLNh9WYoQv0JxQxxzGuGM2G2gHKoX6BYJpZsmbNgjoByvQdvv2CY25xBTVmaCNFyOxeOMVEbYaFwnZLdjazwWHHCh5hj1dpN26Kt2gSINhMUA5W2uxQAlTZkFQSWtgwWAJU2tRbUBNC26wLA0ocBBIGlT1cIAksfVxE4IPD953/oA1V+AGqC66tPqP0XYABKSo4dq4rVcQAAAABJRU5ErkJggg==\"></image>");
        return sb.toString();

    }

    private static String sanitize(String string) {
        return org.apache.commons.lang.StringEscapeUtils.escapeHtml(string);
    }

}
