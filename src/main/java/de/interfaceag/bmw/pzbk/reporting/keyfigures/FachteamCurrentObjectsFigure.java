package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachteamCurrentObjectsFigure extends AbstractLanglauferKeyFigure {

    public FachteamCurrentObjectsFigure(Collection<Long> meldungIds, Collection<Long> anforderungIds, Long runTime) {
        super(anforderungIds.size() + meldungIds.size(),
                IdTypeTupleFactory.getIdTypeTuplesForMeldungAndAnforderung(meldungIds, anforderungIds),
                KeyType.FACHTEAM_CURRENT, runTime);

    }

    public FachteamCurrentObjectsFigure(Collection<Long> meldungIds, Collection<Long> anforderungIds, boolean langlaufer) {
        super(IdTypeTupleFactory.getIdTypeTuplesForMeldungAndAnforderung(meldungIds, anforderungIds), KeyType.FACHTEAM_CURRENT);
        super.setLanglaufer(langlaufer);
    }
}
