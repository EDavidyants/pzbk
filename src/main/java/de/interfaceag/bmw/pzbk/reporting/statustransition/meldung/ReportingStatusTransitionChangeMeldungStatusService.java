package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

/**
 * @author lomu
 */
@Dependent
public class ReportingStatusTransitionChangeMeldungStatusService implements Serializable {

    @Inject
    private ReportingMeldungStatusTransitionCreatePort reportingMeldungStatusTransitionCreatePort;
    @Inject
    private ReportingMeldungStatusTransitionDatabaseAdapter reportingMeldungStatusTransitionDatabaseAdapter;
    @Inject
    private ReportingMeldungStatusTransitionEntryDateService entryDateService;
    @Inject
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Inject
    private AnforderungMeldungHistoryService historyService;

    public ReportingMeldungStatusTransition saveStatusChange(Meldung meldung, Status currentStatus, Status newStatus, Date date) {
        if (newStatus == null && currentStatus.equals(Status.M_ENTWURF)) {
            return null;
        }
        if (currentStatus.equals(Status.M_GELOESCHT)) {
            return handleCurrentStatusGeloescht(meldung, currentStatus, date);
        }

        if (newStatus == Status.M_GEMELDET) {
            return handleNewStatusGemeldet(meldung, currentStatus, date);
        }

        if (newStatus == Status.M_GELOESCHT) {
            return handleStatusChangeWithNewRow(meldung, currentStatus, newStatus, date);
        }

        if (newStatus == Status.M_UNSTIMMIG && currentStatus == Status.M_GEMELDET) {
            return handleStatusChangeWithNewRow(meldung, currentStatus, newStatus, date);
        }

        throw new IllegalArgumentException("Invalid status change for meldung " + meldung + " from " + currentStatus + " to " + newStatus);
    }

    private ReportingMeldungStatusTransition handleStatusChangeWithNewRow(Meldung meldung, Status currentStatus, Status newStatus, Date date) {
        setExitValues(meldung, currentStatus, date);
        return setEntryValues(meldung, newStatus, date);
    }

    private ReportingMeldungStatusTransition handleNewStatusGemeldet(Meldung meldung, Status currentStatus, Date date) {
        ReportingMeldungStatusTransition result = handleChangesWithNewRow(meldung, currentStatus, date);
        EntryExitTimeStamp entry = new EntryExitTimeStamp();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(entry, date);
        result.setGemeldet(timeStamp);
        reportingMeldungStatusTransitionDatabaseAdapter.save(result);
        return result;
    }

    private ReportingMeldungStatusTransition handleCurrentStatusGeloescht(Meldung meldung, Status currentStatus, Date date) {
        ReportingMeldungStatusTransition result = handleChangesWithNewRow(meldung, currentStatus, date);
        Status oldNewStatus = historyService.getLastStatusByObjectnameAnforderungId(meldung.getId(), meldung.getKennzeichen());
        result = handleNewRowEntryStatusAfterStatusGeloescht(oldNewStatus, result, date);
        reportingMeldungStatusTransitionDatabaseAdapter.save(result);
        return result;
    }

    private ReportingMeldungStatusTransition setEntryValues(Meldung meldung, Status newStatus, Date date) {
        Optional<ReportingMeldungStatusTransition> latestEntryOptional = reportingMeldungStatusTransitionDatabaseAdapter.getLatest(meldung);
        if (latestEntryOptional.isPresent()) {
            return handleNewEntryValueBasedOnNewStatus(latestEntryOptional.get(), newStatus, date);
        } else {
            throw new IllegalArgumentException("No entry for meldung is present");
        }
    }

    private ReportingMeldungStatusTransition handleNewEntryValueBasedOnNewStatus(ReportingMeldungStatusTransition latestEntry, Status newStatus, Date date) {
        EntryExitTimeStamp newTimeStamp = new EntryExitTimeStamp();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(newTimeStamp, date);
        switch (newStatus) {
            case M_GELOESCHT:
                latestEntry.setGeloescht(timeStamp);
                break;
            case M_UNSTIMMIG:
                latestEntry.setUnstimmig(timeStamp);
                break;
            case M_GEMELDET:
                latestEntry.setGemeldet(timeStamp);
                break;
            default:
                break;

        }

        reportingMeldungStatusTransitionDatabaseAdapter.save(latestEntry);
        return latestEntry;
    }

    private ReportingMeldungStatusTransition setExitValues(Meldung meldung, Status oldStatus, Date date) {
        Optional<ReportingMeldungStatusTransition> latestEntryOptional = reportingMeldungStatusTransitionDatabaseAdapter.getLatest(meldung);
        if (latestEntryOptional.isPresent()) {
            return handleNewExitValueBasedOnOldStatus(latestEntryOptional.get(), oldStatus, date);
        } else {
            throw new IllegalArgumentException("No entry for meldung is present");
        }
    }

    private ReportingMeldungStatusTransition handleNewExitValueBasedOnOldStatus(ReportingMeldungStatusTransition latestEntry, Status oldStatus, Date date) {

        switch (oldStatus) {
            case M_UNSTIMMIG:
                setExitValueForStatusUnstimmig(latestEntry, date);
                break;
            case M_GELOESCHT:
                setExitValueForStatusGeloescht(latestEntry, date);
                break;
            case M_GEMELDET:
                setExitValueForStatusGemeldet(latestEntry, date);
                break;
            case M_ZUGEORDNET:
                setExitValueForStatusZugeordnet(latestEntry, date);
                break;
            default:
                break;

        }

        reportingMeldungStatusTransitionDatabaseAdapter.save(latestEntry);
        return latestEntry;
    }

    private void setExitValueForStatusZugeordnet(ReportingMeldungStatusTransition latestEntry, Date date) {
        EntryExitTimeStamp zugeordnetTimeStamp = latestEntry.getExistingAnforderungZugeordnet();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(zugeordnetTimeStamp, date);
        latestEntry.setGemeldet(timeStamp);
    }

    private void setExitValueForStatusGemeldet(ReportingMeldungStatusTransition latestEntry, Date date) {
        EntryExitTimeStamp gemeldetTimeStamp = latestEntry.getGemeldet();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(gemeldetTimeStamp, date);
        latestEntry.setGemeldet(timeStamp);
    }

    private void setExitValueForStatusGeloescht(ReportingMeldungStatusTransition latestEntry, Date date) {
        EntryExitTimeStamp geloeschtTimeStamp = latestEntry.getGeloescht();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(geloeschtTimeStamp, date);
        latestEntry.setGeloescht(timeStamp);
    }

    private void setExitValueForStatusUnstimmig(ReportingMeldungStatusTransition latestEntry, Date date) {
        EntryExitTimeStamp unstimmigTimeStamp = latestEntry.getUnstimmig();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(unstimmigTimeStamp, date);
        latestEntry.setUnstimmig(timeStamp);
    }

    private ReportingMeldungStatusTransition handleChangesWithNewRow(Meldung meldung, Status currentStatus, Date date) {
        if (currentStatus == Status.M_ENTWURF) {
            return createNewRow(meldung);
        } else {
            return handleNewRowWithExitValue(currentStatus, meldung, date);
        }
    }

    private ReportingMeldungStatusTransition handleNewRowWithExitValue(Status currentStatus, Meldung meldung, Date date) {
        this.setExitValues(meldung, currentStatus, date);
        return createNewRow(meldung);
    }

    private ReportingMeldungStatusTransition createNewRow(Meldung meldung) {
        final Optional<Date> entryDate = entryDateService.getEntryDateForMeldung(meldung);
        ReportingMeldungStatusTransition transition;
        if (entryDate.isPresent()) {
            transition = reportingMeldungStatusTransitionCreatePort.getNewStatusTransitionForMeldungWithEntryDate(meldung, entryDate.get());
        } else {
            transition = reportingMeldungStatusTransitionCreatePort.getNewStatusTransitionForMeldungWithEntryDate(meldung, new Date());
        }
        reportingMeldungStatusTransitionDatabaseAdapter.save(transition);
        return transition;
    }

    private ReportingMeldungStatusTransition handleNewRowEntryStatusAfterStatusGeloescht(Status oldNewStatus, ReportingMeldungStatusTransition result, Date date) {
        EntryExitTimeStamp newStamp = new EntryExitTimeStamp();
        final EntryExitTimeStamp timeStamp = reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(newStamp, date);
        switch (oldNewStatus) {
            case M_GEMELDET:
                result.setGemeldet(timeStamp);
                break;
            case M_UNSTIMMIG:
                result.setUnstimmig(timeStamp);
                break;
            default:
                break;

        }
        return result;
    }

}
