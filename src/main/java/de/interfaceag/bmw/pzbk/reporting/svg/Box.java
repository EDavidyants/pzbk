package de.interfaceag.bmw.pzbk.reporting.svg;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;
import de.interfaceag.bmw.pzbk.reporting.svg.Diamond.Type;
import de.interfaceag.bmw.pzbk.shared.dto.Point;

import java.util.Arrays;
import java.util.Map;

/**
 * @author lomu
 */
public class Box implements SVGElement {

    private final int x;
    private final int y;
    private final BoxType type;
    private final BoxPolygon.PolygonType polygonType;
    private final int durchlaufzeit;

    private final boolean showTooltips;
    private final String tooltipHeader;
    private final String tooltipDurchlaufzeit;
    private final String tooltipLanglaufer;
    private final KeyFigure keyFigure;
    private final Diamond.Type diamondType;
    private final Map<String, String> boxTextStrings;

    private final TooltipCollection tooltipCollection;

    public Box(int xCoordinate, int yCoordinate, BoxType type, KeyFigure keyFigure, int durchlaufzeit, boolean showTooltips,
               String tooltipHeader, String tooltipDurchlaufzeit, String tooltipLanglaufer,
               BoxPolygon.PolygonType polygonType, Diamond.Type diamondType, Map<String, String> boxTextStrings) {
        this.x = xCoordinate;
        this.y = yCoordinate;
        this.type = type;
        this.diamondType = diamondType;
        this.durchlaufzeit = durchlaufzeit;
        this.keyFigure = keyFigure;
        this.showTooltips = showTooltips;
        this.tooltipDurchlaufzeit = tooltipDurchlaufzeit;
        this.tooltipHeader = tooltipHeader;
        this.tooltipLanglaufer = tooltipLanglaufer;
        this.polygonType = polygonType;
        this.tooltipCollection = TooltipCollection.createCollection();
        createTooltips();
        this.boxTextStrings = boxTextStrings;
    }

    private void createTooltips() {
        if (showTooltips) {
            tooltipCollection.add(new Tooltip(x + (double) 15, y + (double) 12, tooltipHeader));
            tooltipCollection.add(new Tooltip(x + (double) 5, y + (double) 300, tooltipDurchlaufzeit));
        }
    }

    @Override
    public String draw() {

        double offsetX = 23.67;

        if (keyFigure == null) {
            offsetX = 27.67;
        } else if (keyFigure.getValue() >= 1000 && keyFigure.getValue() < 10000) {
            offsetX = 24.67;
        } else if (keyFigure.getValue() >= 100) {
            offsetX = 25.67;
        } else if (keyFigure.getValue() >= 10) {
            offsetX = 26.67;
        } else if (keyFigure.getValue() < 10) {
            offsetX = 27.67;
        }

        String durchLaufzeitLabel = "";
        String headerline1 = "";
        String headerline2 = "";

        if (type == Box.DetailBoxType.FACHTEAM || type == Box.SimpleBoxType.FACHTEAM) {
            durchLaufzeitLabel = boxTextStrings.get("fachteam");
            headerline1 = boxTextStrings.get("fachteam");
            headerline2 = "";
        }
        if (type == Box.DetailBoxType.TTEAM || type == Box.SimpleBoxType.TTEAM) {
            durchLaufzeitLabel = boxTextStrings.get("tteam");
            headerline1 = boxTextStrings.get("tteam");
            headerline2 = "";
        }
        if (type == Box.DetailBoxType.VEREINBARUNG || type == Box.SimpleBoxType.VEREINBARUNG) {
            durchLaufzeitLabel = boxTextStrings.get("vereinbarung");
            headerline1 = boxTextStrings.get("vereinbarungPart1");
            headerline2 = boxTextStrings.get("vereinbarungPart2");
        }

        //elements.add(new Text("c09415f1-be70-4123-b273-3f949364e3b0", "translate(794.18 51.63)", "barung"));
        //elementCollection.add(new Rectangle(type.getRectClass1(), type.getRectClass2(), type.getRectClass3(), x, y, defaultRectWidth, defaultRectHeight));
        SvgElementCollection elementCollection = new SvgElementCollection();
        elementCollection.add(new BoxPolygon(x, y, type, polygonType));
        elementCollection.add(new Diamond(x + 45.45, y + 82.62, keyFigure, diamondType));
        elementCollection.add(new Text(type.getTextClass(), "translate(" + (x + offsetX) + " " + (y + 303.81) + ")", boxTextStrings.get("durchlaufzeit")));
        elementCollection.add(new Text(type.getTextClass(), "translate(" + (x + 9.73 + offsetX + type.getOffsetDurchlaufzeitLabel()) + " " + (y + 320.61) + ")", durchLaufzeitLabel));
        elementCollection.add(new Text(type.getTextClass(), "translate(" + (x + 3.76 + offsetX + type.getOffsetHeaderLine1()) + " " + (y + 42.06) + ")", headerline1));
        elementCollection.add(new Text(type.getTextClass(), "translate(" + (x + 3.76 + offsetX + type.getOffsetHeaderLine2()) + " " + (y + 58.63) + ")", headerline2));
        elementCollection.add(getDurchlaufzeit(offsetX));
        elementCollection.add(new Polygon("b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", Arrays.asList(new Point(x + 4.52, y + 284.06), new Point(x + 11.22, y + 290.29), new Point(x + 11.22, y + 277.83), new Point(x + 4.52, y + 284.06))));
        elementCollection.add(new Polygon("b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", Arrays.asList(new Point(x + 129.52, y + 284.06), new Point(x + 122.82, y + 277.83), new Point(x + 122.82, y + 290.29), new Point(x + 129.52, y + 284.06))));
        elementCollection.add(new Line("e7979375-7020-4a99-8f60-046cf1838a15", x + 9.17, y + 284.06, x + 124.87, y + 284.06));

        if (keyFigure != null && keyFigure.isLanglaufer()) {
            elementCollection.add(new LanglauferHinweis(x + 82, y + 60, tooltipLanglaufer));
        }

        return elementCollection.draw();
    }

    private SVGElement getDurchlaufzeit(double offsetX) {
        int durchlaufzeitOffset = getDurchlaufzeitOffset(Integer.toString(durchlaufzeit));
        return new Text(type.getTextClass(), "translate(" + (x + durchlaufzeitOffset + 39.33 + offsetX) + " " + (y + 275.65) + ")", Integer.toString(this.durchlaufzeit));
    }

    private static int getDurchlaufzeitOffset(String durchlaufZeit) {
        int length = durchlaufZeit.length();

        switch (length) {
            case 0:
            case 1:
                return 0;
            default:
                return (length - 1) * (-5);
        }
    }

    public TooltipCollection getTooltipCollection() {
        return tooltipCollection;
    }

    public Map<String, String> getBoxTextStrings() {
        return boxTextStrings;
    }

    public interface BoxType {

        String getBoxClass();

        String getDurchlaufzeitLabel();

        String getHeaderLine1();

        String getHeaderLine2();

        int getOffsetDurchlaufzeitLabel();

        int getOffsetHeaderLine1();

        int getOffsetHeaderLine2();

        String getRectClass1();

        String getRectClass2();

        String getRectClass3();

        Type getDiamondType();

        String getTextClass();
    }

    public enum SimpleBoxType implements BoxType {
        FACHTEAM("b1af0568-0922-49a0-8b80-34d724b01ca3", "e74f067f-41ce-4693-b2d0-e5ae056d20f0", "95d8ff88-c217-4d42-8167-ef4e8468a91f", "b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", "Fachteam", "", "Fachteam", 0, 0, 0, Diamond.Type.SIMPLE, "ffa89ab7-8120-4f9d-b7d7-33e849fbefce"),
        TTEAM("ca8549ca-5caa-42b6-9100-70881d6a7b8b", "e74f067f-41ce-4693-b2d0-e5ae056d20f0", "093fd0ba-d0ec-4f8b-a6aa-a26a6b7b6050", "b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", "T-Team", "", "T-Team", 8, 10, 0, Diamond.Type.SIMPLE, "ffa89ab7-8120-4f9d-b7d7-33e849fbefce"),
        VEREINBARUNG("b6578f7b-ad84-44b3-821b-0da8997a72f5", "e74f067f-41ce-4693-b2d0-e5ae056d20f0", "dd6ae827-876a-4553-96e5-bd1bf972d71c", "b1eb7fd4-8b82-4ce1-9e8d-d7bcafe1b2c4", "Verein-", "barung", "Vereinbarung", -9, 12, 12, Diamond.Type.SIMPLE, "ffa89ab7-8120-4f9d-b7d7-33e849fbefce");

        private final String boxClass;
        private final String rectClass1;
        private final String rectClass2;
        private final String rectClass3;
        private final String headerLine1;
        private final String headerLine2;
        private final String durchlaufzeitLabel;
        private final int offsetDurchlaufzeitLabel;
        private final int offsetHeaderLine1;
        private final int offsetHeaderLine2;
        private final Type diamondType;
        private final String textClass;

        SimpleBoxType(String boxClass, String rectClass1, String rectClass2, String rectClass3, String headerLine1, String headerLine2, String durchlaufzeitLabel, int offsetDurchlaufzeitLabel, int offsetHeaderLine1, int offsetHeaderLine2, Type diamondType, String textClass) {
            this.boxClass = boxClass;
            this.rectClass1 = rectClass1;
            this.rectClass2 = rectClass2;
            this.rectClass3 = rectClass3;
            this.headerLine1 = headerLine1;
            this.headerLine2 = headerLine2;
            this.durchlaufzeitLabel = durchlaufzeitLabel;
            this.offsetDurchlaufzeitLabel = offsetDurchlaufzeitLabel;
            this.offsetHeaderLine1 = offsetHeaderLine1;
            this.offsetHeaderLine2 = offsetHeaderLine2;
            this.diamondType = diamondType;
            this.textClass = textClass;
        }

        public String getTextClass() {
            return textClass;
        }

        @Override
        public int getOffsetDurchlaufzeitLabel() {
            return offsetDurchlaufzeitLabel;
        }

        @Override
        public Type getDiamondType() {
            return diamondType;
        }

        @Override
        public int getOffsetHeaderLine1() {
            return offsetHeaderLine1;
        }

        @Override
        public int getOffsetHeaderLine2() {
            return offsetHeaderLine2;
        }

        @Override
        public String getHeaderLine2() {
            return headerLine2;
        }

        @Override
        public String getHeaderLine1() {
            return headerLine1;
        }

        @Override
        public String getBoxClass() {
            return boxClass;
        }

        @Override
        public String getDurchlaufzeitLabel() {
            return durchlaufzeitLabel;
        }

        @Override
        public String getRectClass1() {
            return rectClass1;
        }

        @Override
        public String getRectClass2() {
            return rectClass2;
        }

        @Override
        public String getRectClass3() {
            return rectClass3;
        }

    }

    public enum DetailBoxType implements BoxType {
        FACHTEAM("b1af0568-0922-49a0-8b80-34d724b01ca3", "592c5a5e-ada3-49a0-83c0-bc6f96857966", "cb17c188-b9c0-44f6-89fc-25349d833054", "cb17c188-b9c0-44f6-89fc-25349d833054", "Fachteam", "", "Fachteam", 0, 0, 0, Diamond.Type.DETAIL, "5f459e6e-0411-4145-a215-c5424afc40fe"),
        TTEAM("7cca9b7e-4c0d-4fb6-bb8f-167f5d807814", "592c5a5e-ada3-49a0-83c0-bc6f96857966", "728c13e9-b56b-49eb-a80e-1297e5d2f1f8", "cb17c188-b9c0-44f6-89fc-25349d833054", "T-Team", "", "T-Team", 8, 10, 0, Diamond.Type.DETAIL, "5f459e6e-0411-4145-a215-c5424afc40fe"),
        VEREINBARUNG("40084878-491e-4ba9-ae36-376c85c69bc9", "592c5a5e-ada3-49a0-83c0-bc6f96857966", "dd6ae827-876a-4553-96e5-bd1bf972d71c", "cb17c188-b9c0-44f6-89fc-25349d833054", "Verein-", "barung", "Vereinbarung", -9, 12, 12, Diamond.Type.DETAIL, "5f459e6e-0411-4145-a215-c5424afc40fe");

        private final String boxClass;
        private final String rectClass1;
        private final String rectClass2;
        private final String rectClass3;
        private final String headerLine1;
        private final String headerLine2;
        private final String durchlaufzeitLabel;
        private final int offsetDurchlaufzeitLabel;
        private final int offsetHeaderLine1;
        private final int offsetHeaderLine2;
        private final Type diamondType;
        private final String textClass;

        DetailBoxType(String boxClass, String rectClass1, String rectClass2, String rectClass3, String headerLine1, String headerLine2, String durchlaufzeitLabel, int offsetDurchlaufzeitLabel, int offsetHeaderLine1, int offsetHeaderLine2, Type diamondType, String textClass) {
            this.boxClass = boxClass;
            this.rectClass1 = rectClass1;
            this.rectClass2 = rectClass2;
            this.rectClass3 = rectClass3;
            this.headerLine1 = headerLine1;
            this.headerLine2 = headerLine2;
            this.durchlaufzeitLabel = durchlaufzeitLabel;
            this.offsetDurchlaufzeitLabel = offsetDurchlaufzeitLabel;
            this.offsetHeaderLine1 = offsetHeaderLine1;
            this.offsetHeaderLine2 = offsetHeaderLine2;
            this.diamondType = diamondType;
            this.textClass = textClass;
        }

        public String getTextClass() {
            return textClass;
        }

        @Override
        public int getOffsetDurchlaufzeitLabel() {
            return offsetDurchlaufzeitLabel;
        }

        @Override
        public int getOffsetHeaderLine1() {
            return offsetHeaderLine1;
        }

        @Override
        public int getOffsetHeaderLine2() {
            return offsetHeaderLine2;
        }

        @Override
        public String getHeaderLine2() {
            return headerLine2;
        }

        @Override
        public String getHeaderLine1() {
            return headerLine1;
        }

        @Override
        public String getBoxClass() {
            return boxClass;
        }

        @Override
        public String getDurchlaufzeitLabel() {
            return durchlaufzeitLabel;
        }

        @Override
        public String getRectClass1() {
            return rectClass1;
        }

        @Override
        public String getRectClass2() {
            return rectClass2;
        }

        @Override
        public String getRectClass3() {
            return rectClass3;
        }

        @Override
        public Type getDiamondType() {
            return diamondType;
        }

    }

}
