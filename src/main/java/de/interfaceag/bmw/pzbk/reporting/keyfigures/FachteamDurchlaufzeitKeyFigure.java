package de.interfaceag.bmw.pzbk.reporting.keyfigures;

public class FachteamDurchlaufzeitKeyFigure extends AbstractDurchlaufzeitKeyFigure {

    public FachteamDurchlaufzeitKeyFigure(int value) {
        super(value, KeyType.DURCHLAUFZEIT_FACHTEAM);
    }
}
