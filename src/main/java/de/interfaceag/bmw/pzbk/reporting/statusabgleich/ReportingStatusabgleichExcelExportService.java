package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;

/**
 * @author evda
 */
@Named
@Stateless
public class ReportingStatusabgleichExcelExportService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ReportingStatusabgleichExcelExportService.class);

    @Inject
    private ExcelDownloadService excelDownloadService;

    @Inject
    private LocalizationService localizationService;

    public void downloadDrilldownDataToExcel(String derivatName, List<DrilldownTableAnforderungDTO> data) {
        LOG.debug("Download Reporting Statusabgleich Drilldown Data for {} to Excel", derivatName);
        Workbook workbook = new XSSFWorkbook();

        Locale currentLocale = localizationService.getCurrentLocale();

        Sheet sheet = workbook.createSheet("ReportingData");
        if (data != null && !data.isEmpty()) {
            int rowIndex = 0;
            Row headerRow = sheet.createRow(rowIndex);

            Cell idAndVersionCell = headerRow.createCell(0);
            idAndVersionCell.setCellValue(LocalizationService.getValue(currentLocale, "anforderung_id"));

            Cell modulCell = headerRow.createCell(1);
            modulCell.setCellValue(LocalizationService.getValue(currentLocale, "modulname"));

            Cell zakIdCell = headerRow.createCell(2);
            zakIdCell.setCellValue(LocalizationService.getValue(currentLocale, "zak_id"));

            Cell beschreibungCell = headerRow.createCell(3);
            beschreibungCell.setCellValue(LocalizationService.getValue(currentLocale, "beschreibung"));

            Cell derivatAnforderungModulStatusCell = headerRow.createCell(4);
            derivatAnforderungModulStatusCell.setCellValue(LocalizationService.getValue(currentLocale, "derivatAnforderungModulStatus"));

            Cell zakStatusCell = headerRow.createCell(5);
            zakStatusCell.setCellValue(LocalizationService.getValue(currentLocale, "zak_status"));

            CellStyle headerStyle = createHeaderCellStyle(workbook);
            idAndVersionCell.setCellStyle(headerStyle);
            modulCell.setCellStyle(headerStyle);
            zakIdCell.setCellStyle(headerStyle);
            beschreibungCell.setCellStyle(headerStyle);
            derivatAnforderungModulStatusCell.setCellStyle(headerStyle);
            zakStatusCell.setCellStyle(headerStyle);

            Row row;
            for (DrilldownTableAnforderungDTO drilldownElement : data) {
                rowIndex++;
                row = sheet.createRow(rowIndex);

                row.createCell(0).setCellValue(drilldownElement.getFachIdAndVersion());
                row.createCell(1).setCellValue(drilldownElement.getModulName());
                row.createCell(2).setCellValue(drilldownElement.getZakId());
                row.createCell(3).setCellValue(drilldownElement.getBeschreibung());
                row.createCell(4).setCellValue(drilldownElement.getDerivatAnforderungModulStatus());
                row.createCell(5).setCellValue(drilldownElement.getZakStatus());
            }

        } else {
            LOG.warn("ReportingElement Liste fuer Statusabgleich {} ist leer", derivatName);
        }

        excelDownloadService.downloadExcelExport("ReportingData", workbook);
    }

    private CellStyle createHeaderCellStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 12);
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        style.setFont(font);
        return style;
    }


}
