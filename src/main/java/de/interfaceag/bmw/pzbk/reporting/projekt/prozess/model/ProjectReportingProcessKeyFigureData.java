package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model;

import java.io.Serializable;

/**
 *
 * @author sl
 */
public interface ProjectReportingProcessKeyFigureData extends Serializable {

    ProjectReportingProcessKeyFigure getKeyFigure();

    Long getValue();

    boolean isAmpelGreen();

}
