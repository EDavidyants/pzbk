package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.drilldown;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures.ProjectReportingUmsetzungsverwaltungKeyFigureDao;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lomu
 */
@Named
@Stateless
public class ProjectReportingUmsetzungDrilldownService {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectReportingUmsetzungsverwaltungKeyFigureDao.class);

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public List<ProjectReportingDrilldownDto> getQueryDataForUmsetzungsverwaltung(Derivat derivat, KovAPhase kovAPhase, UmsetzungsBestaetigungStatus anforderungModulStatus) {
        QueryPart queryPart = getQueryForUmsetzungsverwaltungDerivatAndPhase(derivat, kovAPhase, anforderungModulStatus);
        Query query = queryPart.buildNativeQuery(entityManager);
        LOG.debug("Excecute query: {}", query.toString());
        LOG.debug("Query parameters: {}", query.getParameters());
        List<Object[]> resultList = query.getResultList();
        List<ProjectReportingDrilldownDto> drilldownDtoResultList = convertToDrilldownDTO(resultList, anforderungModulStatus);
        LOG.debug("Retrieved {} entries", drilldownDtoResultList.size());
        return drilldownDtoResultList;

    }

    private static QueryPart getQueryForUmsetzungsverwaltungDerivatAndPhase(Derivat derivat, KovAPhase kovAPhase, UmsetzungsBestaetigungStatus anforderungModulStatus) {
        QueryPart queryPart = new QueryPartDTO(""
                + "SELECT DISTINCT a.id, a.fachId, a.version, m.name, zak.zakid, a.beschreibungAnforderungDe "
                + "FROM umsetzungsbestaetigung ub "
                + "INNER JOIN anforderung a ON ub.anforderung_id = a.id "
                + "INNER JOIN kov_a_phase_im_derivat kovad ON ub.kovaimderivat_id = kovad.id "
                + "INNER JOIN derivatanforderungmodul dam ON kovad.kovaphase = dam.kovaphase "
                + "INNER JOIN zakuebertragung zak ON dam.id = zak.derivatanforderungmodul_id "
                + "INNER JOIN modul m ON ub.modul_id = m.id "
                + "WHERE kovad.derivat_id = ?1 "
                + "AND ub.status = ?2 "
                + "AND kovad.kovAPhase = ?3 "
                + "AND (ub.anforderung_id, ub.modul_id) IN "
                + "(SELECT zad.anforderung_id, dam.modul_id "
                + "FROM zakuebertragung zak "
                + "INNER JOIN derivatanforderungmodul dam ON dam.id = zak.derivatanforderungmodul_id "
                + "INNER JOIN zuordnunganforderungderivat zad ON zad.id = dam.zuordnunganforderungderivat_id "
                + "WHERE zad.derivat_id = ?4 )");

        queryPart.put("1", derivat.getId());
        queryPart.put("2", anforderungModulStatus.getId());
        queryPart.put("3", kovAPhase.getId());
        queryPart.put("4", derivat.getId());
        return queryPart;
    }

    private List<ProjectReportingDrilldownDto> convertToDrilldownDTO(List<Object[]> resultList, UmsetzungsBestaetigungStatus anforderungModulStatus) {
        List<ProjectReportingDrilldownDto> result = new ArrayList<>();

        resultList.stream().map((element) -> new ProjectReportingDrilldownDto((Long) element[0], (String) element[1], (int) element[2], (String) element[3],
                (String) element[4], (String) element[5], anforderungModulStatus.getStatusBezeichnung()))
                .filter((dtoElement) -> (!result.contains(dtoElement)))
                .forEachOrdered((dtoElement) -> {
                    result.add(dtoElement);
                });

        return result;
    }

}
