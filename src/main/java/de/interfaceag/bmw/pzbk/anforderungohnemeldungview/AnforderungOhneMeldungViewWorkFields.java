package de.interfaceag.bmw.pzbk.anforderungohnemeldungview;

import de.interfaceag.bmw.pzbk.controller.dialogs.ModulFreigabeCommentDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.StatusChangeNextStatusDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.VersionPlusKommentarDialog;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;

import java.io.Serializable;

/**
 * @author fn
 */
public class AnforderungOhneMeldungViewWorkFields implements ModulFreigabeCommentDialog,
        StatusChangeNextStatusDialog, VersionPlusKommentarDialog, Serializable {

    private String beschreibungAnforderungDe;
    private boolean changedInStatusPlausibilisiert;
    private AnforderungFreigabeBeiModulSeTeam anfoModulZumFreigeben;
    private String modulFreigabeKommentar;
    private boolean kommentarRequired;
    private String versionKommentar;
    private Integer selectedNextStatus;
    private String statusChangeKommentar;

    public AnforderungOhneMeldungViewWorkFields(String beschreibungAnforderungDe) {
        this.beschreibungAnforderungDe = beschreibungAnforderungDe;
    }

    public String getBeschreibungAnforderungDe() {
        return beschreibungAnforderungDe;
    }

    public void setBeschreibungAnforderungDe(String beschreibungAnforderungDe) {
        this.beschreibungAnforderungDe = beschreibungAnforderungDe;
    }

    public boolean isChangedInStatusPlausibilisiert() {
        return changedInStatusPlausibilisiert;
    }

    public void setChangedInStatusPlausibilisiert(boolean changedInPlausi) {
        this.changedInStatusPlausibilisiert = changedInPlausi;
    }

    @Override
    public AnforderungFreigabeBeiModulSeTeam getAnfoModulZumFreigeben() {
        return anfoModulZumFreigeben;
    }

    @Override
    public void setAnfoModulZumFreigeben(AnforderungFreigabeBeiModulSeTeam anfoModulZumFreigeben) {
        this.anfoModulZumFreigeben = anfoModulZumFreigeben;
    }

    @Override
    public String getModulFreigabeKommentar() {
        return modulFreigabeKommentar;
    }

    @Override
    public void setModulFreigabeKommentar(String modulFreigabeKommentar) {
        this.modulFreigabeKommentar = modulFreigabeKommentar;
    }

    @Override
    public boolean isKommentarRequired() {
        return kommentarRequired;
    }

    @Override
    public void setKommentarRequired(boolean kommentarRequired) {
        this.kommentarRequired = kommentarRequired;
    }

    @Override
    public String getVersionKommentar() {
        return versionKommentar;
    }

    @Override
    public void setVersionKommentar(String versionKommentar) {
        this.versionKommentar = versionKommentar;
    }

    @Override
    public Integer getSelectedNextStatus() {
        return selectedNextStatus;
    }

    @Override
    public void setSelectedNextStatus(Integer selectedNextStatus) {
        this.selectedNextStatus = selectedNextStatus;
    }

    @Override
    public String getStatusChangeKommentar() {
        return statusChangeKommentar;
    }

    @Override
    public void setStatusChangeKommentar(String statusChangeKommentar) {
        this.statusChangeKommentar = statusChangeKommentar;
    }

}
