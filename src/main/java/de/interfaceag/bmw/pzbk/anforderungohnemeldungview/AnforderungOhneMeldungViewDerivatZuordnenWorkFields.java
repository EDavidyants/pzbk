package de.interfaceag.bmw.pzbk.anforderungohnemeldungview;

import de.interfaceag.bmw.pzbk.controller.dialogs.DerivatZuordnenDialog;
import de.interfaceag.bmw.pzbk.dialog.viewdata.DerivatZuordnenDto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author fn
 */
// DerivatZuordnenDialogWorkFields
public class AnforderungOhneMeldungViewDerivatZuordnenWorkFields implements DerivatZuordnenDialog, Serializable {

    private List<DerivatZuordnenDto> derivateChoosable;
    private List<DerivatZuordnenDto> zugeordneteDerivate;
    private Map<DerivatZuordnenDto, Boolean> canBeRemovedMap = new HashMap<>();

    public AnforderungOhneMeldungViewDerivatZuordnenWorkFields(List<DerivatZuordnenDto> zugeordneteDerivate,
            List<DerivatZuordnenDto> derivateChoosable) {

        this.zugeordneteDerivate = zugeordneteDerivate;
        this.derivateChoosable = derivateChoosable;

        zugeordneteDerivate.forEach(d -> canBeRemovedMap.put(d, Boolean.FALSE));
    }

    @Override
    public void addDerivat(DerivatZuordnenDto derivatZuordnenDto) {
        derivateChoosable.remove(derivatZuordnenDto);
        zugeordneteDerivate.add(derivatZuordnenDto);
    }

    @Override
    public void removeDerivat(DerivatZuordnenDto derivatZuordnenDto) {
        derivateChoosable.add(derivatZuordnenDto);
        zugeordneteDerivate.remove(derivatZuordnenDto);
    }

    @Override
    public List<DerivatZuordnenDto> getDerivateChoosable() {
        return derivateChoosable;
    }

    @Override
    public List<DerivatZuordnenDto> getZugeordneteDerivate() {
        return zugeordneteDerivate;
    }

    @Override
    public void setDerivateChoosable(List<DerivatZuordnenDto> derivateChoosable) {
        this.derivateChoosable = derivateChoosable;
    }

    @Override
    public void setZugeordneteDerivate(List<DerivatZuordnenDto> zugeordneteDerivate) {
        this.zugeordneteDerivate = zugeordneteDerivate;
    }

    @Override
    public Boolean canBeRemoved(DerivatZuordnenDto derivat) {
        return !canBeRemovedMap.containsKey(derivat);
    }

}
