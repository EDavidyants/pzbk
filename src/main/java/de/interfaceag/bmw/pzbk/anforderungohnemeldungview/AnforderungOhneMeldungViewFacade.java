package de.interfaceag.bmw.pzbk.anforderungohnemeldungview;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungStatusChangeService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.StatusUtils;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungVersionMenuViewData;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.folgeprozessttl.ZuordnungAnforderungDerivatDTO;
import de.interfaceag.bmw.pzbk.dialog.viewdata.DerivatZuordnenDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.DerivatenAnzeigenDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungReadData;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDTO;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDialogViewData;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.ImageService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.ProzessbaukastenLinkData;
import de.interfaceag.bmw.pzbk.zak.ZakVereinbarungService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.menu.MenuModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author fn
 */
@Stateless
@Named
public class AnforderungOhneMeldungViewFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungOhneMeldungViewFacade.class.getName());

    @Inject
    protected AnforderungService anforderungService;
    @Inject
    protected ImageService imageService;
    @Inject
    protected ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;
    @Inject
    protected DerivatService derivatService;
    @Inject
    protected DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    protected ZakVereinbarungService zakVereinbarungService;
    @Inject
    protected Session session;
    @Inject
    protected BerechtigungService berechtigungService;
    @Inject
    protected ProzessbaukastenZuordnenService prozessbaukastenZuordnenService;
    @Inject
    protected AnforderungMeldungHistoryService historyService;
    @Inject
    private AnforderungStatusChangeService anforderungStatusChangeSerivce;

    public AnforderungOhneMeldungViewData getViewData(UrlParameter urlParameter) {
        Anforderung anforderung = getAnforderungByUrlParameter(urlParameter);

        if (anforderung == null) {
            return AnforderungOhneMeldungViewData.forNullAnforderung();
        }

        AnforderungOhneMeldungViewPermission viewPermission = getViewPermissionForAnforderung(anforderung);
        AnforderungOhneMeldungViewAnforderungData anforderungData = getAnforderungDataForAnforderung(anforderung);
        ProzessbaukastenZuordnenDialogViewData prozessessbaukastenZuordnenViewData = getProzessbaukastenZuordnenViewData(anforderung);
        List<ProzessbaukastenLinkData> prozessbaukastenLinks = anforderungService.getProzessbaukastenLinksForAnforderung(anforderung);

        setTTeamAndSensorCocLeiter(anforderungData);

        return new AnforderungOhneMeldungViewData(anforderungData, prozessessbaukastenZuordnenViewData, viewPermission, prozessbaukastenLinks);
    }

    private void setTTeamAndSensorCocLeiter(AnforderungOhneMeldungViewAnforderungData anforderungData) {

        berechtigungService.getTteamleiterOfTteam(anforderungData.getTteam())
                .ifPresent(tteamLeiter -> anforderungData.getTteam().setTeamleiter(tteamLeiter));

        berechtigungService.getSensorCoCLeiterOfSensorCoc(anforderungData.getSensorCoc())
                .ifPresent(sensorCocLeiter -> anforderungData.getSensorCoc().setSensorCoCLeiter(sensorCocLeiter));

    }

    protected Set<Rolle> getUserRolesWithWritePermissions(Anforderung anforderung) {
        if (anforderung.getSensorCoc() != null && anforderung.getTteam() != null
                && anforderung.getAnfoFreigabeBeiModul() != null) {
            Set<Rolle> rolesWithWritePermissions = session.getUserPermissions()
                    .getRolesWithWritePermissionsForAnforderung(anforderung.getSensorCoc(),
                            anforderung.getTteam().getId(),
                            anforderung.getAnfoFreigabeBeiModul().stream()
                                    .map(f -> f.getModulSeTeam().getId()).collect(Collectors.toSet())
                    );
            if (session.hasRole(Rolle.TTEAMMITGLIED)) {
                List<Long> tteamIdsForWrite = session.getUserPermissions().getTteamIdsForTteamMitgliedSchreibend();
                if (tteamIdsForWrite.contains(anforderung.getTteam().getId())) {
                    rolesWithWritePermissions.add(Rolle.TTEAMMITGLIED);
                }
            }
            if (session.hasRole(Rolle.TTEAM_VERTRETER)) {
                List<Long> tteamIdsForWrite = session.getUserPermissions().getTteamIdsForTteamVertreter();
                if (tteamIdsForWrite.contains(anforderung.getTteam().getId())) {
                    rolesWithWritePermissions.add(Rolle.TTEAM_VERTRETER);
                }
            }
            return rolesWithWritePermissions;
        } else {
            return session.getUserPermissions()
                    .getRolesWithWritePermissionsForNewAnforderung();
        }

    }

    private AnforderungOhneMeldungViewAnforderungData getAnforderungDataForAnforderung(Anforderung anforderung) {

        List<Status> nextStatusList = getNextStatusListForAnforderung(anforderung);

        Map<ModulSeTeam, Boolean> modulAuthorizationMap = getModulAuthorizationMapForAnforderung(anforderung);

        List<ZuordnungAnforderungDerivatDTO> anforderungDerivatList = anforderungService.getAnforderungeDerivatListForAnforderung(anforderung, getUserRolesWithWritePermissions(anforderung));

        AnforderungOhneMeldungViewAnforderungData anforderungData = AnforderungOhneMeldungViewAnforderungData.forAnforderungNextStatusListAndModulAuthorizationMap(anforderung, nextStatusList, modulAuthorizationMap);
        anforderungData.setAnforderungDerivatList(anforderungDerivatList);

        return anforderungData;
    }

    private AnforderungOhneMeldungViewPermission getViewPermissionForAnforderung(Anforderung anforderung) {

        boolean berechtigt = anforderungService.isUserBerechtigtForAnforderung(anforderung.getId());

        Set<Rolle> rolesWithWritePermissions = getUserRolesWithWritePermissions(anforderung);

        Boolean hasAnforderungZuordnung = anforderung.isProzessbaukastenZugeordnet();

        return new AnforderungOhneMeldungViewPermission(berechtigt, anforderung.getStatus(), rolesWithWritePermissions, hasAnforderungZuordnung);

    }

    private List<Status> getNextStatusListForAnforderung(Anforderung anforderung) {
        List<Status> nextStatusList = new ArrayList<>();
        if (anforderung.getSensorCoc() != null && anforderung.getTteam() != null) {
            nextStatusList = StatusUtils.getAllNextStatusAsList(anforderung.getStatus());
        }
        return nextStatusList;
    }

    private Anforderung getAnforderungByUrlParameter(UrlParameter urlParameter) {

        String fachId = urlParameter.getValue("fachId").orElse("");
        String versionString = urlParameter.getValue("version").orElse("");
        String anforderungId = urlParameter.getValue("id").orElse("");

        Integer version = null;
        try {
            version = Integer.parseInt(versionString);
        } catch (NumberFormatException nfe) {
            LOG.warn("version is not valid. ");
        }
        Anforderung anforderung = null;
        if (!RegexUtils.matchesAnforderungFachId(fachId) || version == null || version <= 0) {
            if (RegexUtils.matchesId(anforderungId)) {
                try {
                    Long longId = Long.parseLong(anforderungId);
                    anforderung = anforderungService.getAnforderungById(longId);
                } catch (NumberFormatException nfe) {
                    LOG.warn("ID is not valid. ", nfe);
                }
            } else {
                return null;
            }
        } else {
            anforderung = anforderungService.getAnforderungByFachIdVersion(fachId, version);
        }

        return anforderung;
    }

    public void saveChangesToAnforderungAndResetFreigabe(AnforderungReadData anforderungObjekt, Anhang anhangForBild, AnforderungOhneMeldungViewWorkFields anforderungOhneMeldungViewWorkFields) {
        Anforderung anforderung = anforderungService.getAnforderungByFachIdVersion(anforderungObjekt.getFachId(), anforderungObjekt.getVersion());
        Anforderung anforderungCopy = anforderungService.getAnforderungWorkCopyByIdOrNewAnforderung(anforderung.getId());
        anforderungCopy.resetFreigabeBeiModulSeTeams();
        setAnforderungsObjektElementsToAnforderungElements(anforderungCopy, anforderungOhneMeldungViewWorkFields);
        anforderungService.saveChangesToAnforderung(anforderungCopy, anhangForBild);
    }

    public void saveChangesToAnforderung(AnforderungReadData anforderungObjekt, Anhang anhangForBild, AnforderungOhneMeldungViewWorkFields anforderungOhneMeldungViewWorkFields) {
        Anforderung anforderung = anforderungService.getAnforderungByFachIdVersion(anforderungObjekt.getFachId(), anforderungObjekt.getVersion());
        Anforderung anforderungCopy = anforderungService.getAnforderungWorkCopyByIdOrNewAnforderung(anforderung.getId());
        setAnforderungsObjektElementsToAnforderungElements(anforderungCopy, anforderungOhneMeldungViewWorkFields);
        anforderungService.saveChangesToAnforderung(anforderungCopy, anhangForBild);

    }

    private void setAnforderungsObjektElementsToAnforderungElements(Anforderung anforderung, AnforderungOhneMeldungViewWorkFields anforderungOhneMeldungViewWorkFields) {
        anforderung.setBeschreibungAnforderungDe(anforderungOhneMeldungViewWorkFields.getBeschreibungAnforderungDe());
    }

    protected Map<ModulSeTeam, Boolean> getModulAuthorizationMapForAnforderung(Anforderung anforderung) {

        final List<AnforderungFreigabeBeiModulSeTeam> freigabeBeiModul = anforderung.getAnfoFreigabeBeiModul();
        Map<ModulSeTeam, Boolean> modulAuthorizationMap = new HashMap<>();

        if (session.hasRole(Rolle.ADMIN) || (session.hasRole(Rolle.T_TEAMLEITER) || session.hasRole(Rolle.TTEAM_VERTRETER)) && Status.A_ANGELEGT_IN_PROZESSBAUKASTEN.equals(anforderung.getStatus())) {
            freigabeBeiModul.forEach(modulFreigabe -> modulAuthorizationMap.put(modulFreigabe.getModulSeTeam(), true));
        } else {
            anforderung.getAnfoFreigabeBeiModul().forEach(modulFreigabe -> {
                boolean isAuthorized;
                if (modulFreigabe != null && modulFreigabe.getModulSeTeam() != null) {
                    isAuthorized = anforderungService.isAuthorizedForModulSeTeam(modulFreigabe.getModulSeTeam(), anforderung.getStatus(), anforderung.getTteam());
                    modulAuthorizationMap.put(modulFreigabe.getModulSeTeam(), isAuthorized);
                }
            });
        }

        return modulAuthorizationMap;
    }

    public boolean checkFreizugeben(AnforderungReadData anforderungObjekt, AnforderungFreigabeBeiModulSeTeam anfoModul) {
        Anforderung anforderung = anforderungService.getAnforderungByFachIdVersion(anforderungObjekt.getFachId(), anforderungObjekt.getVersion());
        return anforderungService.checkFreizugeben(anforderung, anfoModul);
    }

    public StreamedContent downloadAnhang(Anhang anhang) {
        return imageService.downloadAnhang(anhang);
    }

    public List<AnforderungVersionMenuViewData> fetchVersionViewDataForFachId(AnforderungOhneMeldungViewData viewData) {

        Optional<AnforderungOhneMeldungViewAnforderungData> anforderungOhneMeldungViewAnforderungData = viewData.getAnforderungOhneMeldungViewAnforderungData();

        if (anforderungOhneMeldungViewAnforderungData.isPresent()) {
            return anforderungService.fetchVersionViewDataForFachId(
                    anforderungOhneMeldungViewAnforderungData.get().getFachId());
        } else {
            return Collections.emptyList();
        }


    }

    public String modulAblehnen(AnforderungFreigabeBeiModulSeTeam anfoModul, String modulFreigabeKommentar, String fachId, Integer version) {
        if (anforderungService.modulAblehnen(anfoModul, modulFreigabeKommentar)) {
            return PageUtils.getUrlForPageWithFachIdAndVersion(Page.ANFORDERUNGVIEW, fachId, version);
        } else {
            return "";
        }

    }

    public String anforderungForSelectedModulFreigeben(AnforderungFreigabeBeiModulSeTeam anfoModul, String modulFreigabeKommentar, String fachId, Integer version) {
        anforderungService.anforderungForSelectedModulFreigeben(anfoModul, modulFreigabeKommentar);
        return PageUtils.getUrlForPageWithFachIdAndVersion(Page.ANFORDERUNGVIEW, fachId, version);

    }

    public String changeStatus(Integer newStatus, AnforderungOhneMeldungViewWorkFields anforderungOhneMeldungViewWorkFields, AnforderungOhneMeldungViewAnforderungData viewData) {

        if (newStatus != null) {
            String statusWechselKommentar = anforderungOhneMeldungViewWorkFields.getStatusChangeKommentar();
            if ((newStatus == 5 || newStatus == 7) && (statusWechselKommentar == null || "".equals(statusWechselKommentar) || statusWechselKommentar.trim().length() == 0)) {
                FacesContext context = FacesContext.getCurrentInstance();
                // TODO Internationalisierung
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Kommentar zum Statuswechsel fehlt!", "Beim Wechsel zum Status " + Status.getStatusById(newStatus).toString() + " ist ein Kommentar notwendig."));
            } else {
                Anforderung anforderung = anforderungService.getAnforderungByFachIdVersion(viewData.getFachId(), viewData.getVersion());
                anforderungStatusChangeSerivce.changeAnforderungStatus(anforderung, newStatus, anforderungOhneMeldungViewWorkFields.getStatusChangeKommentar());
                return PageUtils.getUrlForPageWithFachIdAndVersion(Page.ANFORDERUNGVIEW, anforderung.getFachId(), anforderung.getVersion());
            }
        }
        return "";
    }

    public String restoreAnforderung(long anforderungId) {

        Anforderung anforderung = anforderungService.getAnforderungById(anforderungId);

        if (anforderung == null) {
            LOG.error("Anforderung for id {} is null!", anforderungId);
            return "";
        }

        String resultMessage = anforderungService.restoreAnforderung(anforderung, session.getUser());

        if (!resultMessage.isEmpty()) {
            FacesContext context = FacesContext.getCurrentInstance();
            // TODO Internationalisierung
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Keine Wiederherstellung möglich", resultMessage));
            return "";
        }

        return PageUtils.getUrlForPageWithFachIdAndVersion(Page.ANFORDERUNGVIEW, anforderung.getFachId(), anforderung.getVersion());
    }

    public void persistDerivatChanges(List<DerivatZuordnenDto> zugeordneteDerivate, String fachId, Integer version) {
        Anforderung anforderung = anforderungService.getAnforderungByFachIdVersion(fachId, version);
        zugeordneteDerivate.forEach(derivat
                -> zuordnungAnforderungDerivatService.assignAnforderungToDerivat(ZuordnungStatus.ZUGEORDNET, anforderung, derivatService.getDerivatById(derivat.getId()),
                "Die Anforderung " + anforderung.toString() + " wurde dem Derivat " + derivat.getDerivatName() + " zugeordnet.", session.getUser())
        );
    }

    public AnforderungOhneMeldungViewDerivatZuordnenWorkFields initDerivatWorkFields(Long anforderungId) {

        List<DerivatZuordnenDto> derivateForAnforderung = zuordnungAnforderungDerivatService.getDerivateViewDataForAnforderung(anforderungId);

        return new AnforderungOhneMeldungViewDerivatZuordnenWorkFields(derivateForAnforderung, initVerfuegbareDerivate(anforderungId));
    }

    private List<DerivatZuordnenDto> initVerfuegbareDerivate(Long anforderungId) {

        List<Long> alleDerivate;

        if (session.hasRole(Rolle.ADMIN)) {
            alleDerivate = derivatService.getAllDerivatIds();
        } else {
            alleDerivate = derivatService.getAuthorizedDerivateIdsForAnforderer(session.getUser());
        }
        return zuordnungAnforderungDerivatService.getDerivateVerfuegbarViewDataForAnforderung(anforderungId, alleDerivate);

    }

    public void persistDerivatAnzeigeAnforderungModul(DerivatenAnzeigenDto derivatenAnzeigenDialogViewData) {
        DerivatAnforderungModul derivatAnforderungModul = derivatAnforderungModulService.getDerivatAnforderungModulById(derivatenAnzeigenDialogViewData.getId());
        derivatAnforderungModul.getZuordnungAnforderungDerivat().setNachZakUebertragen(derivatenAnzeigenDialogViewData.isNachZakUebertragen());
        derivatAnforderungModulService.persistDerivatAnforderungModul(derivatAnforderungModul);
    }

    public void sendNachZak(List<Long> anforderungIdsNachZakList) {

        FacesContext context = FacesContext.getCurrentInstance();
        if (!anforderungIdsNachZakList.isEmpty()) {
            Map<Derivat, List<ZakUebertragung>> report = zakVereinbarungService.sendAnforderungenDerivatIdsNachZak(anforderungIdsNachZakList);
            StringBuilder sb = new StringBuilder();
            for (Derivat derivat : report.keySet()) {
                if (!report.get(derivat).isEmpty()) {
                    sb.append(derivat.getName()).append(": ");
                    for (ZakUebertragung zakUebertragung : report.get(derivat)) {
                        sb.append(zakUebertragung.getDerivatAnforderungModul().getAnforderung().getFachId());
                        sb.append(" v").append(zakUebertragung.getDerivatAnforderungModul().getAnforderung().getVersion());
                        sb.append(" | ");
                        sb.append(zakUebertragung.getDerivatAnforderungModul().getModul().getName()).append("; ");
                    }
                    sb.append("\n");
                }
            }

            context.addMessage(null,
                    new FacesMessage(
                            FacesMessage.SEVERITY_INFO,
                            "Uebertragung nach ZAK abgeschlossen!", "")
            );

            String ergebnis = sb.toString();
            if ("".equals(ergebnis)) {
                ergebnis = "keine Module wurden nach ZAK uebertragen";
            }

            context.addMessage(null,
                    new FacesMessage(
                            FacesMessage.SEVERITY_INFO,
                            "Ergebnis: ", ergebnis)
            );

        } else {
            context.addMessage(null,
                    new FacesMessage(
                            FacesMessage.SEVERITY_INFO,
                            "INFO: ", "Bitte wählen Sie Derivate/Anforderungen für die Übertragung nach ZAK!")
            );
        }
    }

    public DerivatAnzeigenWorkFields initDerivatAnzeigenWorkFields(Long anforderungId) {
        List<DerivatenAnzeigenDto> derivatAnzeigenDialogViewData = zuordnungAnforderungDerivatService.getDerivatAnzeigenDialogViewDataForAnforderungId(anforderungId);
        return new DerivatAnzeigenWorkFields(derivatAnzeigenDialogViewData);
    }

    public String prozessbaukastenZuordnen(ProzessbaukastenZuordnenDTO prozessbaukastenZuordnenDTO, Long anforderungId) {
        return prozessbaukastenZuordnenService.prozessbaukastenZuordnen(prozessbaukastenZuordnenDTO, anforderungId);
    }

    private ProzessbaukastenZuordnenDialogViewData getProzessbaukastenZuordnenViewData(Anforderung anforderung) {
        return prozessbaukastenZuordnenService.getAddProzessbaukastenDialogViewData(anforderung);
    }

    public MenuModel getZugeordneteProzessbaukastenMenuItems(List<ProzessbaukastenLinkData> zugeordneteProzessbaukasten) {
        return anforderungService.getZugeordneteProzessbaukastenMenuItems(zugeordneteProzessbaukasten);
    }

    public Boolean isDemGueltigenProzessbaukastenZugeordnet(Anforderung anforderung) {
        return anforderungService.isDemGueltigenProzessbaukastenZugeordnet(anforderung);
    }

    public String generateGrowlMessageForFreigabeNeuerVersion() {
        return anforderungService.generateGrowlMessageForFreigabeNeuerVersion();
    }

}
