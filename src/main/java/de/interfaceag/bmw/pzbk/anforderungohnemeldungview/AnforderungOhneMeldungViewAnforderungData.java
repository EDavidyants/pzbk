package de.interfaceag.bmw.pzbk.anforderungohnemeldungview;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.folgeprozessttl.ZuordnungAnforderungDerivatDTO;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungReadData;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Bild;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.ReferenzSystemLink;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.Zielwert;
import de.interfaceag.bmw.pzbk.enums.Status;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author fn
 */
public final class AnforderungOhneMeldungViewAnforderungData implements Serializable, AnforderungReadData {

    private final Long anforderungId;
    private final String fachId;
    private final Status status;
    private String beschreibungAnforderungDe;
    private final Integer version;
    private final String beschreibungAnforderungEn;
    private final String kommentarAnforderung;
    private final Zielwert zielwert;
    private final List<AnforderungFreigabeBeiModulSeTeam> anforderungFreigabeBeiModul;
    private final SensorCoc sensorCoc;
    private final Tteam tteam;
    private final boolean zeitpunktUmsetzungsbest;
    private final boolean phasenbezug;
    private final boolean potentialStandardisierung;
    private final Date erstellungsDatum;
    private final List<Anhang> anhaenge;
    private final Bild bild;
    private final Set<FestgestelltIn> festgestelltIn;
    private final String ersteller;
    private final List<ReferenzSystemLink> referenzSystemLinks;
    private Anhang anhangForBild;
    private final Map<ModulSeTeam, Boolean> modulAuthorizationMap;
    private final Set<Umsetzer> umsetzer;
    private Status nextStatus;
    private List<Status> nextStatusList;
    private List<ZuordnungAnforderungDerivatDTO> anforderungDerivatList = new ArrayList<>();

    private final Boolean isProzessbaukastenZugeordnet;

    private AnforderungOhneMeldungViewAnforderungData(Long id, String fachId, Status status,
                                                      Integer version, String beschreibungAnforderungDe,
                                                      String beschreibungAnforderungEn, String kommentarAnforderung,
                                                      Zielwert zielwert, List<AnforderungFreigabeBeiModulSeTeam> anforderungFreigabeBeiModul,
                                                      SensorCoc sensorCoc, Tteam tteam,
                                                      boolean zeitpunktUmsetzungsbest, boolean phasenbezug,
                                                      boolean potentialStandardisierung, Date erstellungsDatum,
                                                      List<Anhang> anhaenge, Bild bild, Set<FestgestelltIn> festgestelltIn,
                                                      String ersteller, List<ReferenzSystemLink> referenzSystemLinks, Map<ModulSeTeam, Boolean> modulAuthorizationMap, Set<Umsetzer> umsetzer, Status nextStatus,
                                                      List<Status> nextStatusList, Boolean isProzessbaukastenZugeordnet) {
        this.anforderungId = id;
        this.fachId = fachId;
        this.status = status;
        this.beschreibungAnforderungDe = beschreibungAnforderungDe;
        this.version = version;
        this.beschreibungAnforderungEn = beschreibungAnforderungEn;
        this.kommentarAnforderung = kommentarAnforderung;
        this.zielwert = zielwert;
        this.anforderungFreigabeBeiModul = anforderungFreigabeBeiModul;
        this.sensorCoc = sensorCoc;
        this.tteam = tteam;
        this.zeitpunktUmsetzungsbest = zeitpunktUmsetzungsbest;
        this.phasenbezug = phasenbezug;
        this.potentialStandardisierung = potentialStandardisierung;
        this.erstellungsDatum = erstellungsDatum;
        this.anhaenge = anhaenge;
        this.bild = bild;
        this.festgestelltIn = festgestelltIn;
        this.ersteller = ersteller;
        this.referenzSystemLinks = referenzSystemLinks;
        this.modulAuthorizationMap = modulAuthorizationMap;
        this.umsetzer = umsetzer;
        this.nextStatus = nextStatus;
        this.nextStatusList = nextStatusList;
        anhaenge.forEach(a -> {
            if (a.isStandardBild()) {
                this.anhangForBild = a;
            }
        });

        this.isProzessbaukastenZugeordnet = isProzessbaukastenZugeordnet;
    }

    public static AnforderungOhneMeldungViewAnforderungData forAnforderungNextStatusListAndModulAuthorizationMap(Anforderung anforderung, List<Status> nextStatusList, Map<ModulSeTeam, Boolean> modulAuthorizationMap) {
        return new AnforderungOhneMeldungViewAnforderungData(anforderung.getId(), anforderung.getFachId(), anforderung.getStatus(),
                anforderung.getVersion(), anforderung.getBeschreibungAnforderungDe(),
                anforderung.getBeschreibungAnforderungEn(), anforderung.getKommentarAnforderung(),
                anforderung.getZielwert(), anforderung.getAnfoFreigabeBeiModul(),
                anforderung.getSensorCoc(), anforderung.getTteam(), anforderung.getZeitpktUmsetzungsbest(),
                anforderung.isPhasenbezug(), anforderung.isPotentialStandardisierung(),
                anforderung.getErstellungsdatum(), anforderung.getAnhaenge(),
                anforderung.getBild(), anforderung.getFestgestelltIn(),
                anforderung.getErsteller(), anforderung.getReferenzSystemLinks(),
                modulAuthorizationMap, anforderung.getUmsetzer(),
                anforderung.getNextStatus(), nextStatusList, anforderung.isProzessbaukastenZugeordnet());
    }

    public Long getId() {
        return anforderungId;
    }

    @Override
    public String getFachId() {
        return fachId;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public String getBeschreibungAnforderungDe() {
        return beschreibungAnforderungDe;
    }

    public void setBeschreibungAnforderungDe(String beschreibungAnforderungDe) {
        this.beschreibungAnforderungDe = beschreibungAnforderungDe;
    }

    @Override
    public String getBeschreibungAnforderungEn() {
        return beschreibungAnforderungEn;
    }

    @Override
    public String getKommentarAnforderung() {
        return kommentarAnforderung;
    }

    @Override
    public Zielwert getZielwert() {
        return zielwert;
    }

    @Override
    public List<AnforderungFreigabeBeiModulSeTeam> getAnfoFreigabeBeiModul() {
        return anforderungFreigabeBeiModul;
    }

    @Override
    public SensorCoc getSensorCoc() {
        return sensorCoc;
    }

    @Override
    public Tteam getTteam() {
        return tteam;
    }

    public boolean getZeitpktUmsetzungsbest() {
        return zeitpunktUmsetzungsbest;
    }

    @Override
    public boolean isPhasenbezug() {
        return phasenbezug;
    }

    @Override
    public boolean isPotentialStandardisierung() {
        return potentialStandardisierung;
    }

    public Date getErstellungsdatum() {
        return erstellungsDatum;
    }

    @Override
    public List<Anhang> getAnhaenge() {
        return anhaenge;
    }

    @Override
    public Bild getBild() {
        return bild;
    }

    @Override
    public Set<FestgestelltIn> getFestgestelltIn() {
        return festgestelltIn;
    }

    @Override
    public String getErsteller() {
        return ersteller;
    }

    @Override
    public List<ReferenzSystemLink> getReferenzSystemLinks() {
        return referenzSystemLinks;
    }

    public Anhang getAnhangForBild() {
        return anhangForBild;
    }

    public Boolean isAuthorizedForModulSeTeam(ModulSeTeam modulSeTeam) {
        return modulAuthorizationMap.containsKey(modulSeTeam) && modulAuthorizationMap.get(modulSeTeam);
    }

    public List<Umsetzer> getUmsetzerListFilteredByModulSeTeam(ModulSeTeam modulSeTeam) {
        List<Umsetzer> result = new ArrayList<>();
        if (umsetzer != null && !umsetzer.isEmpty()) {
            umsetzer.stream().filter(u -> u.getSeTeam().equals(modulSeTeam) && u.getKomponente() != null).forEachOrdered(result::add);
        }
        return result;
    }

    public Status getNextStatus() {
        return nextStatus;
    }

    public List<Status> getNextStatusList() {
        return nextStatusList;
    }

    public void setNextStatusList(List<Status> nextStatusList) {
        this.nextStatusList = nextStatusList;
    }

    public List<ZuordnungAnforderungDerivatDTO> getAnforderungDerivatList() {
        return anforderungDerivatList;
    }

    public void setAnforderungDerivatList(List<ZuordnungAnforderungDerivatDTO> anforderungDerivatList) {
        this.anforderungDerivatList = anforderungDerivatList;
    }

    public String festgestelltInToString() {
        if (festgestelltIn != null && !festgestelltIn.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            festgestelltIn.forEach(f -> {
                sb.append(f.getWert());
                sb.append(" ");
            });
            return sb.toString();
        }
        return "";
    }

    public String getAllReferenzSystemLinkAsString() {
        return getReferenzSystemLinks().stream().map(r -> r.getReferenzsystem().toString() + ": " + r.getReferenzsystemId()).collect(Collectors.joining(",\n "));
    }

    public Boolean isProzessbaukastenZugeordnet() {
        return isProzessbaukastenZugeordnet;
    }
}
