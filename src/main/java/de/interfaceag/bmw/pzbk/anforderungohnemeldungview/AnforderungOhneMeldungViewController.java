package de.interfaceag.bmw.pzbk.anforderungohnemeldungview;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungBeschreibungChangedValidator;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungVersionMenuViewData;
import de.interfaceag.bmw.pzbk.controller.dialogs.AnforderungChangedInStatusPlausiConfirmDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.ChangeVersionProceedDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.DerivatAnzeigenProceedDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.DerivatZuordnenDialogPersistDerivat;
import de.interfaceag.bmw.pzbk.controller.dialogs.ModulFreigabeProccedDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.StatusChangeMethodChangeDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.VersionPlusCreateNewVersionDialog;
import de.interfaceag.bmw.pzbk.dialog.viewdata.DerivatenAnzeigenDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungReadData;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDTO;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDialogMethods;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDialogViewData;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.ProzessbaukastenLinkData;
import org.primefaces.context.RequestContext;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

/**
 * @author fn
 */
@ViewScoped
@Named
public class AnforderungOhneMeldungViewController
        implements Serializable, AnforderungChangedInStatusPlausiConfirmDialog,
        ModulFreigabeProccedDialog, ChangeVersionProceedDialog,
        VersionPlusCreateNewVersionDialog, StatusChangeMethodChangeDialog,
        DerivatZuordnenDialogPersistDerivat, DerivatAnzeigenProceedDialog,
        ProzessbaukastenZuordnenDialogMethods {

    public static final String PF_MODUL_FREIGABE_COMMENT_DIALOG_SHOW = "PF('modulFreigabeCommentDialog').show()";
    @Inject
    private AnforderungOhneMeldungViewFacade anforderungOhneMeldungFacade;

    @Inject
    private Session session;

    @Inject
    private LocalizationService localizationService;

    private AnforderungOhneMeldungViewData viewData;

    private AnforderungOhneMeldungViewWorkFields anforderungOhneMeldungViewWorkFields;

    private AnforderungOhneMeldungViewDerivatZuordnenWorkFields anforderungOhneMeldungViewDerivatZuordnenWorkFields;

    private DerivatAnzeigenWorkFields anforderungOhneMeldungDerivatAnzeigenWorkFields;

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungOhneMeldungViewController.class.getName());

    @PostConstruct
    public void init() {
        initViewData();
        if (!viewData.getAnforderungOhneMeldungViewAnforderungData().isPresent()) {
            FacesContext.getCurrentInstance().getExternalContext().setResponseStatus(HttpServletResponse.SC_NOT_FOUND);
            FacesContext.getCurrentInstance().responseComplete();
        } else {
            anforderungOhneMeldungViewWorkFields = new AnforderungOhneMeldungViewWorkFields(getViewData().getBeschreibungAnforderungDe());
        }
    }

    private void initViewData() {
        viewData = anforderungOhneMeldungFacade.getViewData(UrlParameterUtils.getUrlParameter());
    }

    public String saveFromView() {
        boolean showKommentarChangedDialog = isChangedInStatusPlausibilisiert(anforderungOhneMeldungViewWorkFields, getViewData());

        if (showKommentarChangedDialog) {
            RequestContext.getCurrentInstance().execute("PF('anforderungOhneMeldungPlausiChangedDialog').show()");
            return "";
        }

        return saveValidatedData();
    }

    private static boolean isChangedInStatusPlausibilisiert(AnforderungOhneMeldungViewWorkFields anforderungOhneMeldungViewWorkFields, AnforderungOhneMeldungViewAnforderungData anforderungData) {
        boolean isKommentarChangedInStatusPlausibilisiert = AnforderungBeschreibungChangedValidator
                .isKommentarChangedInStatusPlausibilisiert(anforderungData, anforderungOhneMeldungViewWorkFields.getBeschreibungAnforderungDe());

        if (!anforderungOhneMeldungViewWorkFields.isChangedInStatusPlausibilisiert() && isKommentarChangedInStatusPlausibilisiert) {
            anforderungOhneMeldungViewWorkFields.setChangedInStatusPlausibilisiert(true);
            return true;
        }
        anforderungOhneMeldungViewWorkFields.setChangedInStatusPlausibilisiert(false);
        return false;
    }

    public String saveValidatedData() {

        AnforderungReadData anforderungReadData = getViewData();

        if (anforderungOhneMeldungViewWorkFields.isChangedInStatusPlausibilisiert()) {

            anforderungOhneMeldungFacade.saveChangesToAnforderungAndResetFreigabe(anforderungReadData, getViewData().getAnhangForBild(), anforderungOhneMeldungViewWorkFields);
        } else {

            anforderungOhneMeldungFacade.saveChangesToAnforderung(anforderungReadData, getViewData().getAnhangForBild(), anforderungOhneMeldungViewWorkFields);
        }

        viewData.getAnforderungOhneMeldungViewAnforderungData().ifPresent(anforderungViewData -> anforderungViewData.setBeschreibungAnforderungDe(anforderungOhneMeldungViewWorkFields.getBeschreibungAnforderungDe()));

        return PageUtils.getUrlForPageWithFachIdAndVersion(Page.ANFORDERUNGVIEW, anforderungReadData.getFachId(), anforderungReadData.getVersion());
    }

    public void showModulFreigebenDialog(AnforderungFreigabeBeiModulSeTeam anfoModul, boolean freigegeben) {
        anforderungOhneMeldungViewWorkFields.setAnfoModulZumFreigeben(anfoModul);
        anforderungOhneMeldungViewWorkFields.setModulFreigabeKommentar("");

        if (!freigegeben) {
            processModulAbgelehnt();
        } else {
            processModulFreigabe(anfoModul);
        }
    }

    private void processModulAbgelehnt() {
        anforderungOhneMeldungViewWorkFields.setKommentarRequired(true);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute(PF_MODUL_FREIGABE_COMMENT_DIALOG_SHOW);
    }

    private void processModulFreigabe(AnforderungFreigabeBeiModulSeTeam anfoModul) {
        Anforderung anforderung = anfoModul.getAnforderung();
        if (isDemGueltigenProzessbaukastenZugeordnet(anforderung)) {
            LOG.info("No previous versions with gultig prozessbaukasten found");
            showValidationGrowlForZugeordneteAnforderung();
        } else {
            processAnforderungModulFreigabe(anfoModul);
        }
    }

    private void showValidationGrowlForZugeordneteAnforderung() {
        String message = anforderungOhneMeldungFacade.generateGrowlMessageForFreigabeNeuerVersion();
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
    }

    private Boolean isDemGueltigenProzessbaukastenZugeordnet(Anforderung anforderung) {
        return anforderungOhneMeldungFacade.isDemGueltigenProzessbaukastenZugeordnet(anforderung);
    }

    private void processAnforderungModulFreigabe(AnforderungFreigabeBeiModulSeTeam anfoModul) {
        anforderungOhneMeldungViewWorkFields.setKommentarRequired(false);
        if (anforderungOhneMeldungFacade.checkFreizugeben(getViewData(), anfoModul)) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('changeVersionDialog').show()");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(PF_MODUL_FREIGABE_COMMENT_DIALOG_SHOW);
        }
    }

    public void showAnfoModulCommentDialog(AnforderungFreigabeBeiModulSeTeam anfoModul) {
        anforderungOhneMeldungViewWorkFields.setAnfoModulZumFreigeben(anfoModul);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('anfoModulCommentDialog').show()");
    }

    @Override
    public String saveFromDialog() {
        return saveValidatedData();
    }

    public MenuModel getVersionsMenuItems() {
        DefaultMenuModel versionsMenuItems = new DefaultMenuModel();
        List<AnforderungVersionMenuViewData> versionen = anforderungOhneMeldungFacade.fetchVersionViewDataForFachId(viewData);

        versionen.stream().map(version -> {
            DefaultMenuItem item = new DefaultMenuItem();
            item.setUrl("anforderungView.xhtml?fachId=" + version.getFachId() + "&version=" + version.getVersion() + "&faces-redirect=true");
            item.setIcon("ui-icon-arrow-1-e");
            SimpleDateFormat toformat = new SimpleDateFormat("dd.MM.yyyy");
            String vl;
            if (version.getErstellungsdatum() == null) {
                vl = 'V' + version.getVersion().toString();
            } else {
                vl = 'V' + version.getVersion().toString() + " " + toformat.format(version.getErstellungsdatum());
            }
            item.setValue(vl);
            if (version.getVersion().equals(getViewData().getVersion())) {
                item.setStyleClass("current");
            }
            return item;
        }).forEachOrdered(versionsMenuItems::addElement);

        if (getViewPermission().getNewVersionButton()) {
            DefaultMenuItem item = new DefaultMenuItem();
            item.setOnclick("PF('versionPlusDialog').show()");
            item.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
            item.setUrl("#");
            item.setIcon("ui-icon-plus");
            item.setStyleClass("add");
            item.setValue("Neue Version");
            versionsMenuItems.addElement(item);
        }

        return versionsMenuItems;
    }

    public MenuModel getZugeordneteProzessbaukastenMenuItems() {
        List<ProzessbaukastenLinkData> zugeordneteProzessbaukasten = viewData.getProzessbaukastenLinks();
        return anforderungOhneMeldungFacade.getZugeordneteProzessbaukastenMenuItems(zugeordneteProzessbaukasten);
    }

    @Override
    public String proceedWithModulFreigabe(AnforderungFreigabeBeiModulSeTeam anfoModul) {
        if (anforderungOhneMeldungViewWorkFields.isKommentarRequired()) {
            return anforderungOhneMeldungFacade.modulAblehnen(anfoModul, anforderungOhneMeldungViewWorkFields.getModulFreigabeKommentar(), getViewData().getFachId(), getViewData().getVersion());
        } else { // Modul wird freigegeben
            return anforderungOhneMeldungFacade.anforderungForSelectedModulFreigeben(anfoModul, anforderungOhneMeldungViewWorkFields.getModulFreigabeKommentar(), getViewData().getFachId(), getViewData().getVersion());
        }
    }

    public void showDerivateZuordnenDialog() {
        anforderungOhneMeldungViewDerivatZuordnenWorkFields = anforderungOhneMeldungFacade.initDerivatWorkFields(getViewData().getId());
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('derivateZuordnenDialog').show()");
    }

    public void showDerivatenAnzeigenDialog() {
        anforderungOhneMeldungDerivatAnzeigenWorkFields = anforderungOhneMeldungFacade.initDerivatAnzeigenWorkFields(getViewData().getId());
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('derivatenAnzeigenDialog').show()");
    }

    @Override
    public void proceedWithFullFreigabe() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('changeVersionDialog').hide()");
        context.execute(PF_MODUL_FREIGABE_COMMENT_DIALOG_SHOW);
    }

    @Override
    public String createNewVersion() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('versionPlusDialog').hide();");
        final String comment = anforderungOhneMeldungViewWorkFields.getVersionKommentar();
        String encodedComment;
        try {
            encodedComment = URLEncoder.encode(comment, "UTF-8");
        } catch (UnsupportedEncodingException exception) {
            LOG.error("Could not encode comment {}", comment);
            encodedComment = "";
        }
        return "anforderungEdit.xhtml?id=" + getViewData().getId() + "&created=new" + "&comment=" + encodedComment + "&faces-redirect=true";

    }

    public String processStatusChange(Status newStatus) {
        if (newStatus != null) {
            anforderungOhneMeldungViewWorkFields.setSelectedNextStatus(newStatus.getStatusId());

            if (openAnforderungInEditMode(newStatus)) {
                return changeStatus(newStatus.ordinal());
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                context.update("dialog:statusChangeForm");
                context.execute("PF('statusChangeDialog').show()");
                return "";
            }
        }
        return "";
    }

    @Override
    public String reloadPage() {
        return PageUtils.getUrlForPageWithFachIdAndVersion(Page.ANFORDERUNGVIEW, getViewData().getFachId(), getViewData().getVersion());
    }

    @Override
    public String persistDerivatChanges() {
        anforderungOhneMeldungFacade.persistDerivatChanges(anforderungOhneMeldungViewDerivatZuordnenWorkFields.getZugeordneteDerivate(),
                getViewData().getFachId(), getViewData().getVersion());
        return PageUtils.getUrlForPageWithFachIdAndVersion(Page.ANFORDERUNGVIEW, getViewData().getFachId(), getViewData().getVersion());
    }

    @Override
    public String discardChanges() {
        return PageUtils.getUrlForPageWithFachIdAndVersion(Page.ANFORDERUNGVIEW, getViewData().getFachId(), getViewData().getVersion());
    }

    public boolean openAnforderungInEditMode(Status newStatus) {
        if (newStatus.equals(Status.M_GELOESCHT) || newStatus.equals(Status.M_UNSTIMMIG)) {
            return false;
        }
        // SensorCoc and BeschreibungDe are always required
        if (getViewData().getBeschreibungAnforderungDe() == null
                || getViewData().getBeschreibungAnforderungDe().equals("")
                || getViewData().getSensorCoc() == null) {
            return true;
        }

        return newStatus == Status.M_GEMELDET || newStatus == Status.M_ZUGEORDNET
                || newStatus == Status.A_INARBEIT;
    }

    @Override
    public String changeStatus(Integer newStatus) {
        return anforderungOhneMeldungFacade.changeStatus(newStatus, anforderungOhneMeldungViewWorkFields, getViewData());
    }

    public String restore() {
        return anforderungOhneMeldungFacade.restoreAnforderung(getViewData().getId());
    }

    public String processGeloeschtStatusChange() {
        return processStatusChange(Status.A_GELOESCHT);
    }

    public StreamedContent downloadAnhang(Anhang anhang) {
        return anforderungOhneMeldungFacade.downloadAnhang(anhang);
    }

    public String edit() {
        return PageUtils.getUrlForPageWithFachIdAndVersion(Page.ANFORDERUNGEDIT, getViewData().getFachId(), getViewData().getVersion());
    }

    public AnforderungOhneMeldungViewAnforderungData getViewData() {

        return viewData.getAnforderungOhneMeldungViewAnforderungData().orElse(null);

    }

    public AnforderungOhneMeldungViewPermission getViewPermission() {
        return viewData.getViewPermission();
    }

    public AnforderungOhneMeldungViewWorkFields getAnforderungOhneMeldungViewWorkFields() {
        return anforderungOhneMeldungViewWorkFields;
    }

    public AnforderungOhneMeldungViewDerivatZuordnenWorkFields getAnforderungOhneMeldungViewDerivatZuordnenWorkFields() {
        return anforderungOhneMeldungViewDerivatZuordnenWorkFields;
    }

    public DerivatAnzeigenWorkFields getAnforderungOhneMeldungDerivatAnzeigenWorkFields() {
        return anforderungOhneMeldungDerivatAnzeigenWorkFields;
    }

    @Override
    public void onAnfoDeriEdit(DerivatenAnzeigenDto derivatenAnzeigenDialogViewData) {
        anforderungOhneMeldungFacade.persistDerivatAnzeigeAnforderungModul(derivatenAnzeigenDialogViewData);
    }

    @Override
    public void toggleNachZak(DerivatenAnzeigenDto derivatenAnzeigenDialogViewData) {

        if (!getAnforderungOhneMeldungDerivatAnzeigenWorkFields().getAnfoNachZakIDList().isEmpty() && getAnforderungOhneMeldungDerivatAnzeigenWorkFields().getAnfoNachZakIDList().contains(derivatenAnzeigenDialogViewData.getId())) {
            getAnforderungOhneMeldungDerivatAnzeigenWorkFields().getAnfoNachZakIDList().remove(derivatenAnzeigenDialogViewData.getId());
        } else {

            getAnforderungOhneMeldungDerivatAnzeigenWorkFields().getAnfoNachZakIDList().add(derivatenAnzeigenDialogViewData.getId());
        }
    }

    @Override
    public String sendNachZak() {
        if (!getViewData().isProzessbaukastenZugeordnet()) {
            anforderungOhneMeldungFacade.sendNachZak(getAnforderungOhneMeldungDerivatAnzeigenWorkFields().getAnfoNachZakIDList());
        }
        return reloadPage();

    }

    @Override
    public String prozessbaukastenZuordnen() {
        Optional<ProzessbaukastenZuordnenDTO> prozessbaukastenZuordnenDto = getProzessbaukastenZuordnenDialogViewData().getZugeordneterProzessbaukastenForView();
        if (!prozessbaukastenZuordnenDto.isPresent()) {
            return showGrowlMessage();
        }
        return anforderungOhneMeldungFacade.prozessbaukastenZuordnen(prozessbaukastenZuordnenDto.get(), getViewData().getId());
    }

    private String showGrowlMessage() {
        FacesContext context = FacesContext.getCurrentInstance();
        String title = localizationService.getValue("anforderung_view_prozessbaukasten_zuordnen_errorMessage");
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, title, null));
        return "";
    }

    public ProzessbaukastenZuordnenDialogViewData getProzessbaukastenZuordnenDialogViewData() {
        return viewData.getProzessbaukastenZuordnenDialogViewData().orElse(null);
    }

    public String getProzessbaukastenZuordnenButtonName() {
        if (viewData.getProzessbaukastenZuordnenDialogViewData().isPresent()) {
            if (isProzessbaukastenPresent()) {
                return localizationService.getValue("anforderung_view_zugeordneten_prozessbaukasten");
            } else {
                return localizationService.getValue("prozessbaukastenzuordnendialog_buttonname");
            }
        }
        return "";
    }

    public boolean isProzessbaukastenPresent() {

        Optional<AnforderungOhneMeldungViewAnforderungData> anforderungOhneMeldungViewAnforderungData = viewData.getAnforderungOhneMeldungViewAnforderungData();
        if (anforderungOhneMeldungViewAnforderungData.isPresent()) {
            return anforderungOhneMeldungViewAnforderungData.get().isProzessbaukastenZugeordnet();
        } else {
            return false;
        }
    }

    public String prozessbaukastenZuordnungButton() {
        if (!isProzessbaukastenPresent()) {
            return openZuordnenDialog();
        } else {
            return "";
        }
    }

    private String openZuordnenDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('prozessbaukastenZuordnenDialog').show()");
        return "";
    }

    @Override
    public void resetProzessbaukastenZuordnenDialog() {
        getProzessbaukastenZuordnenDialogViewData().removeProzessbaukasten();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('prozessbaukastenZuordnenDialog').hide()");
    }

    public boolean isAnforderungZuordnungErlaubt() {
        Optional<AnforderungOhneMeldungViewAnforderungData> anforderungDataOptional = viewData.getAnforderungOhneMeldungViewAnforderungData();
        if (!anforderungDataOptional.isPresent()) {
            return false;
        }

        AnforderungOhneMeldungViewAnforderungData anforderungData = anforderungDataOptional.get();

        Status status = anforderungData.getStatus();
        if (status == Status.A_GELOESCHT || status == Status.A_KEINE_WEITERVERFOLG) {
            return false;
        }

        return !anforderungData.isProzessbaukastenZugeordnet();

    }

}
