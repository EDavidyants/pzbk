package de.interfaceag.bmw.pzbk.anforderungohnemeldungview;

import de.interfaceag.bmw.pzbk.controller.dialogs.DerivatAnzeigenDialogDerivatForAnforderungList;
import de.interfaceag.bmw.pzbk.dialog.viewdata.DerivatenAnzeigenDto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fn
 */
public class DerivatAnzeigenWorkFields implements DerivatAnzeigenDialogDerivatForAnforderungList, Serializable {

    private List<DerivatenAnzeigenDto> zuordnungAnforderungDerivatList;

    private List<Long> sendToZakAnforderungIds = new ArrayList<>();

    public DerivatAnzeigenWorkFields(List<DerivatenAnzeigenDto> zuordnungAnforderungDerivatList) {
        this.zuordnungAnforderungDerivatList = zuordnungAnforderungDerivatList;
    }

    @Override
    public List<DerivatenAnzeigenDto> getZuordnungAnforderungenDerivatList() {
        return zuordnungAnforderungDerivatList;
    }

    @Override
    public void setZuordnungAnforderungenDerivatList(List<DerivatenAnzeigenDto> zuordnungAnforderungDerivatList) {
        this.zuordnungAnforderungDerivatList = zuordnungAnforderungDerivatList;
    }

    @Override
    public List<Long> getAnfoNachZakIDList() {
        return sendToZakAnforderungIds;
    }

    @Override
    public void setAnfoNachZakList(List<Long> anfoNachZakIdList) {
        this.sendToZakAnforderungIds = anfoNachZakIdList;
    }

}
