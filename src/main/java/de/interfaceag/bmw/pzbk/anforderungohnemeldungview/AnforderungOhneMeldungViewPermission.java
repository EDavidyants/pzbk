package de.interfaceag.bmw.pzbk.anforderungohnemeldungview;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungInlineEditViewPermission;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.EditViewPermission;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.TruePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

import static de.interfaceag.bmw.pzbk.anforderungsworkflow.permission.AnforderungViewPermissionUtils.buildPermissionForDerivatZuordnenButton;
import static de.interfaceag.bmw.pzbk.anforderungsworkflow.permission.AnforderungViewPermissionUtils.buildPermissionForDerivatZustandButton;
import static de.interfaceag.bmw.pzbk.anforderungsworkflow.permission.AnforderungViewPermissionUtils.buildPermissionForEditButton;
import static de.interfaceag.bmw.pzbk.anforderungsworkflow.permission.AnforderungViewPermissionUtils.buildPermissionForLoeschenButton;
import static de.interfaceag.bmw.pzbk.anforderungsworkflow.permission.AnforderungViewPermissionUtils.buildPermissionForNewVersionButton;
import static de.interfaceag.bmw.pzbk.anforderungsworkflow.permission.AnforderungViewPermissionUtils.buildPermissionForProzessbaukastenButton;
import static de.interfaceag.bmw.pzbk.anforderungsworkflow.permission.AnforderungViewPermissionUtils.buildPermissionForWiederherstellenButton;
import static de.interfaceag.bmw.pzbk.anforderungsworkflow.permission.AnforderungViewPermissionUtils.buildPermissionsForStatusTransitions;

/**
 * @author fn
 */
public class AnforderungOhneMeldungViewPermission implements Serializable {

    // page
    private final ViewPermission page;

    private final ViewPermission inViewEditDisable;
    private final EditPermission newVersionButton;

    private final EditPermission editButton;
    private final EditPermission loeschenButton;
    private final EditPermission wiederherstellenButton;
    private final EditPermission derivatZustandButton;
    private final EditPermission derivatZuordnenButton;
    private final EditPermission statusTransitions;


    private final EditViewPermission prozessbaukastenButton;

    public AnforderungOhneMeldungViewPermission(boolean pageBerechtigt, Status currentStatus, Set<Rolle> rolesWithWritePermissions, boolean anforderungHasProzessbaukatenZuordnung) {
        editButton = buildPermissionForEditButton(rolesWithWritePermissions, currentStatus);

        inViewEditDisable = AnforderungInlineEditViewPermission.isInlineEditViewPermission(currentStatus, editButton.hasRightToEdit());

        newVersionButton = buildPermissionForNewVersionButton(rolesWithWritePermissions, currentStatus);
        derivatZuordnenButton = buildPermissionForDerivatZuordnenButton(rolesWithWritePermissions, currentStatus);

        if (anforderungHasProzessbaukatenZuordnung) {
            derivatZustandButton = FalsePermission.get();
        } else {
            derivatZustandButton = buildPermissionForDerivatZustandButton(rolesWithWritePermissions, currentStatus);
        }

        loeschenButton = buildPermissionForLoeschenButton(currentStatus, rolesWithWritePermissions);
        wiederherstellenButton = buildPermissionForWiederherstellenButton(currentStatus, rolesWithWritePermissions);
        if (pageBerechtigt) {
            page = new TruePermission();
        } else {
            page = FalsePermission.get();
        }

        statusTransitions = buildPermissionsForStatusTransitions(currentStatus, rolesWithWritePermissions);

        prozessbaukastenButton = buildPermissionForProzessbaukastenButton(rolesWithWritePermissions);

    }

    private AnforderungOhneMeldungViewPermission() {
        this.page = FalsePermission.get();
        this.inViewEditDisable = FalsePermission.get();
        this.newVersionButton = FalsePermission.get();
        this.editButton = FalsePermission.get();
        this.loeschenButton = FalsePermission.get();
        this.wiederherstellenButton = FalsePermission.get();
        this.derivatZustandButton = FalsePermission.get();
        this.derivatZuordnenButton = FalsePermission.get();
        this.statusTransitions = FalsePermission.get();
        this.prozessbaukastenButton = FalsePermission.get();
    }

    public static AnforderungOhneMeldungViewPermission forNullAnforderung() {
        return new AnforderungOhneMeldungViewPermission();
    }

    public boolean getPage() {
        return page.isRendered();
    }

    public boolean getInViewEditDisable() {
        return inViewEditDisable.isRendered();
    }

    public boolean getNewVersionButton() {
        return newVersionButton.hasRightToEdit();
    }

    public boolean getEditButton() {
        return editButton.hasRightToEdit();
    }

    public boolean getDerivatZustandButton() {
        return derivatZustandButton.hasRightToEdit();
    }

    public boolean getDerivatZuordnenButton() {
        return derivatZuordnenButton.hasRightToEdit();
    }

    public boolean getLoeschenButton() {
        return loeschenButton.hasRightToEdit();
    }

    public boolean getWiederherstellenButton() {
        return wiederherstellenButton.hasRightToEdit();
    }

    public boolean getStatusTransition() {
        return statusTransitions.hasRightToEdit();
    }

    public EditViewPermission getProzessbaukastenButton() {
        return prozessbaukastenButton;
    }

}
