package de.interfaceag.bmw.pzbk.anforderungohnemeldungview;

import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDialogViewData;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.ProzessbaukastenLinkData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author fn
 */
public class AnforderungOhneMeldungViewData implements Serializable {

    private final AnforderungOhneMeldungViewAnforderungData anforderungOhneMeldungViewAnforderungData;

    private final ProzessbaukastenZuordnenDialogViewData prozessbaukastenZuordnenDialogViewData;

    private final AnforderungOhneMeldungViewPermission viewPermission;

    private final List<ProzessbaukastenLinkData> prozessbaukastenLinks;

    private AnforderungOhneMeldungViewData(
            AnforderungOhneMeldungViewPermission viewPermission) {
        this.viewPermission = viewPermission;
        this.anforderungOhneMeldungViewAnforderungData = null;
        this.prozessbaukastenZuordnenDialogViewData = null;
        this.prozessbaukastenLinks = new ArrayList<>();
    }

    public AnforderungOhneMeldungViewData(
            AnforderungOhneMeldungViewAnforderungData anforderungOhneMeldungViewAnforderungData, ProzessbaukastenZuordnenDialogViewData prozessbaukastenZuordnenDialogViewData,
            AnforderungOhneMeldungViewPermission viewPermission, List<ProzessbaukastenLinkData> prozessbaukastenLinks) {

        this.viewPermission = viewPermission;
        this.prozessbaukastenZuordnenDialogViewData = prozessbaukastenZuordnenDialogViewData;
        this.anforderungOhneMeldungViewAnforderungData = anforderungOhneMeldungViewAnforderungData;
        this.prozessbaukastenLinks = (prozessbaukastenLinks != null) ? prozessbaukastenLinks : new ArrayList<>();
    }

    public static AnforderungOhneMeldungViewData forNullAnforderung() {
        return new AnforderungOhneMeldungViewData(AnforderungOhneMeldungViewPermission.forNullAnforderung());
    }

    public AnforderungOhneMeldungViewPermission getViewPermission() {
        return viewPermission;
    }

    public Optional<AnforderungOhneMeldungViewAnforderungData> getAnforderungOhneMeldungViewAnforderungData() {
        return Optional.ofNullable(anforderungOhneMeldungViewAnforderungData);
    }

    public Optional<ProzessbaukastenZuordnenDialogViewData> getProzessbaukastenZuordnenDialogViewData() {
        return Optional.ofNullable(prozessbaukastenZuordnenDialogViewData);
    }

    public List<ProzessbaukastenLinkData> getProzessbaukastenLinks() {
        return prozessbaukastenLinks;
    }

}
