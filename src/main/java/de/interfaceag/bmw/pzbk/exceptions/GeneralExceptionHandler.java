package de.interfaceag.bmw.pzbk.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.FacesException;
import javax.faces.application.NavigationHandler;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import java.util.Iterator;

/**
 *
 * ExceptionHandlerWrapper - JSF wrapper class which requires only to override
 * getWrapped() method to return the instance of the class you're wrapping,
 * which is often simply passed to the constructor
 *
 * based on
 * http://www.jeejava.com/jsf-2-javax-faces-application-viewexpiredexception/
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class GeneralExceptionHandler extends ExceptionHandlerWrapper {

    private static final Logger LOG = LoggerFactory.getLogger(GeneralExceptionHandler.class);

    private final ExceptionHandler handler;

    public GeneralExceptionHandler(ExceptionHandler handler) {
        this.handler = handler;
    }

    @Override
    public ExceptionHandler getWrapped() {
        return handler;
    }

    @Override
    public void handle() throws FacesException {
        for (Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents()
                .iterator(); i.hasNext();) {
            ExceptionQueuedEvent queuedEvent = i.next();
            ExceptionQueuedEventContext queuedEventContext
                    = (ExceptionQueuedEventContext) queuedEvent.getSource();
            Throwable throwable = queuedEventContext.getException();

            String targetPage = "viewExpired?faces-redirect=true";

            if (!(throwable instanceof ViewExpiredException)) {
                targetPage = "error?faces-redirect=true";
                LOG.warn(throwable.toString());
            }

            FacesContext facesContext = FacesContext.getCurrentInstance();
            NavigationHandler navigationHandler = facesContext.getApplication().getNavigationHandler();
            try {
                navigationHandler.handleNavigation(facesContext, null, targetPage);
                facesContext.renderResponse();
            } finally {
                i.remove();
            }
        }
        getWrapped().handle();
    }

}
