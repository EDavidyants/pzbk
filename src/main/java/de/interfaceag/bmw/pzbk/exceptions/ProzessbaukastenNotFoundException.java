package de.interfaceag.bmw.pzbk.exceptions;

/**
 *
 * @author sl
 */
public class ProzessbaukastenNotFoundException extends IllegalArgumentException {

    public ProzessbaukastenNotFoundException(String message) {
        super(message);
    }

}
