package de.interfaceag.bmw.pzbk.exceptions;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ZakUebertragungException extends Exception {

    public ZakUebertragungException() {
    }

    public ZakUebertragungException(String message) {
        super(message);
    }

    public ZakUebertragungException(String message, Throwable cause) {
        super(message, cause);
    }

    public ZakUebertragungException(Throwable cause) {
        super(cause);
    }

    public ZakUebertragungException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
