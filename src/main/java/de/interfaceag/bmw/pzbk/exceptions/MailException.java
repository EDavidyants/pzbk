package de.interfaceag.bmw.pzbk.exceptions;

/**
 *
 * @author Christian Schauer
 */
public class MailException extends Exception {

    public MailException(String message) {
        super(message);
    }
}
