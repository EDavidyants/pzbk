package de.interfaceag.bmw.pzbk.exceptions;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class GeneralExceptionHandlerFactory extends ExceptionHandlerFactory {

    private final ExceptionHandlerFactory factory;

    public GeneralExceptionHandlerFactory(ExceptionHandlerFactory factory) {
        this.factory = factory;
    }

    @Override
    public ExceptionHandler getExceptionHandler() {
        ExceptionHandler handler = factory.getExceptionHandler();
        return new GeneralExceptionHandler(handler);
    }

}
