package de.interfaceag.bmw.pzbk.exceptions;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class ExcelInputFileException extends Exception {

    public ExcelInputFileException(String message) {
        super(message);
    }
}
