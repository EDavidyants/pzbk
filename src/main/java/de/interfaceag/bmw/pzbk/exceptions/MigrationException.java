package de.interfaceag.bmw.pzbk.exceptions;

/**
 *
 * @author qp
 */
public class MigrationException extends Exception {

    public MigrationException(String message) {
        super(message);
    }

    public MigrationException(Throwable throwable) {
        super(throwable);
    }

}
