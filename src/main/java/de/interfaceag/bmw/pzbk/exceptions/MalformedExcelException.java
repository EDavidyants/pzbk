package de.interfaceag.bmw.pzbk.exceptions;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class MalformedExcelException extends Exception {

    private final List<String> malformedSheetNames;

    public MalformedExcelException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.malformedSheetNames = new ArrayList<>();
    }

    public void addMalformedSheetName(String name) {
        this.malformedSheetNames.add(name);
    }

    public List<String> getMalformedSheetNames() {
        return malformedSheetNames;
    }

    public boolean malformedSheetsExist() {
        return !this.malformedSheetNames.isEmpty();
    }

}
