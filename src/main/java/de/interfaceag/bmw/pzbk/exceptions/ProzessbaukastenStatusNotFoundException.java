package de.interfaceag.bmw.pzbk.exceptions;

/**
 *
 * @author sl
 */
public class ProzessbaukastenStatusNotFoundException extends IllegalArgumentException {

    public ProzessbaukastenStatusNotFoundException(String message) {
        super(message);
    }

}
