package de.interfaceag.bmw.pzbk.exceptions;

/**
 *
 * @author sl
 */
public class InvalidAnhangException extends IllegalArgumentException {

    public InvalidAnhangException(String message) {
        super(message);
    }

}
