package de.interfaceag.bmw.pzbk.zak.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sl
 */
@XmlRootElement(name = "header")
public class Header {

    private String ttool;
    private String savedate;
    private String savetime;
    private String geltung;
    private String user;
    private String neu;

    public Header() {
    }

    public Header(String savedate, String savetime, String geltung, String user, String neu) {
        this.ttool = "ja";
        this.savedate = savedate;
        this.savetime = savetime;
        this.geltung = geltung;
        this.user = user;
        this.neu = neu;
    }

    @Override
    public String toString() {
        return "Header{" + "ttool=" + ttool + ", savedate=" + savedate + ", savetime=" + savetime + ", geltung=" + geltung + ", user=" + user + ", neu=" + neu + '}';
    }

    public String getTtool() {
        return ttool;
    }

    @XmlElement(name = "ttool")
    public void setTtool(String ttool) {
        this.ttool = ttool;
    }

    public String getSavedate() {
        return savedate;
    }

    @XmlElement(name = "savedate")
    public void setSavedate(String savedate) {
        this.savedate = savedate;
    }

    public String getSavetime() {
        return savetime;
    }

    @XmlElement(name = "savetime")
    public void setSavetime(String savetime) {
        this.savetime = savetime;
    }

    public String getGeltung() {
        return geltung;
    }

    @XmlElement(name = "geltung")
    public void setGeltung(String geltung) {
        this.geltung = geltung;
    }

    public String getUser() {
        return user;
    }

    @XmlElement(name = "user")
    public void setUser(String user) {
        this.user = user;
    }

    public String getNeu() {
        return neu;
    }

    @XmlElement(name = "neu")
    public void setNeu(String neu) {
        this.neu = neu;
    }

}
