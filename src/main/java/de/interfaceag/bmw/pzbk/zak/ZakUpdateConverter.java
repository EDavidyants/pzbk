package de.interfaceag.bmw.pzbk.zak;

import de.interfaceag.bmw.pzbk.entities.converter.ToEntityConverter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ZakUpdateConverter extends ToEntityConverter<ZakUpdateDto, ZakUpdate> {

    protected ZakUpdateConverter() {
        super(zakUpdateDto -> new ZakUpdate(
                zakUpdateDto.getDerivat(),
                zakUpdateDto.getZakUrl(),
                zakUpdateDto.getUpdatedFrom(),
                zakUpdateDto.getStatus(),
                zakUpdateDto.getCompressedZakResponse()));
    }

}
