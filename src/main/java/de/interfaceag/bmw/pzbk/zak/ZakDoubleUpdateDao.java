package de.interfaceag.bmw.pzbk.zak;

import de.interfaceag.bmw.pzbk.entities.ZakDoubleUpdate;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

/**
 *
 * @author fn
 */
@Dependent
public class ZakDoubleUpdateDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager em;

    protected void persistZakUpdate(ZakDoubleUpdate zakDoubleUpdate) {
        if (zakDoubleUpdate.getId() != null) {
            em.merge(zakDoubleUpdate);
        } else {
            em.persist(zakDoubleUpdate);
        }
        em.flush();
    }
}
