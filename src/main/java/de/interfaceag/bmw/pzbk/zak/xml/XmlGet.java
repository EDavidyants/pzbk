package de.interfaceag.bmw.pzbk.zak.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 *
 * @author sl
 */
@XmlRootElement(name = "xmlget")
public class XmlGet {

    private List<Anforderung> anforderungList;

    // ---------- constructors -------------------------------------------------
    public XmlGet() {
    }

    public XmlGet(List<Anforderung> anforderungList) {
        this.anforderungList = anforderungList;
    }

    // ---------- getter and setter --------------------------------------------
    public void setAnforderungList(List<Anforderung> anforderungList) {
        this.anforderungList = anforderungList;
    }

    @XmlElementWrapper(name = "anforderungen")
    @XmlElement(name = "anforderung")
    public List<Anforderung> getAnforderungList() {
        return anforderungList;
    }

}
