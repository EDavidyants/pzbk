package de.interfaceag.bmw.pzbk.zak;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.shared.utils.ZipUtils;
import de.interfaceag.bmw.pzbk.zak.xml.XmlGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Date;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ZakUpdateDto {

    private static final Logger LOG = LoggerFactory.getLogger(ZakUpdateDto.class.getName());

    private final Derivat derivat;
    private final String zakUrl;
    private final Date updatedFrom;
    private String status;
    private final String zakResponse;
    private final XmlGet xmlGet;

    public ZakUpdateDto(Derivat derivat, String zakUrl, Status status, String statusMessage) {
        this.derivat = derivat;
        this.zakUrl = zakUrl;
        this.updatedFrom = new Date();
        this.status = status.toString() + ": " + statusMessage;
        this.zakResponse = null;
        this.xmlGet = null;
    }

    public ZakUpdateDto(Derivat derivat, String zakUrl, Status status, String zakResponse, XmlGet xmlGet) {
        this.derivat = derivat;
        this.zakUrl = zakUrl;
        this.updatedFrom = new Date();
        this.status = status.toString();
        this.zakResponse = zakResponse;
        this.xmlGet = xmlGet;
    }

    public Derivat getDerivat() {
        return derivat;
    }

    public String getZakUrl() {
        return zakUrl;
    }

    public Date getUpdatedFrom() {
        if (updatedFrom != null) {
            return new Date(updatedFrom.getTime());
        }
        return null;
    }

    public String getStatus() {
        return status;
    }

    public void setStatusMessage(String statusMessage) {
        this.status = this.status.concat("; ").concat(statusMessage);
    }

    public byte[] getCompressedZakResponse() {
        if (zakResponse != null) {
            try {
                return ZipUtils.compress(zakResponse);
            } catch (IOException ex) {
                LOG.warn(ex.toString(), ex);
            }
        } else {
            try {
                return ZipUtils.compress("keine XML-Daten vorhanden!");
            } catch (IOException ex) {
                LOG.warn(ex.toString(), ex);
            }
        }
        return null;
    }

    public String getZakResponse() {
        return zakResponse;
    }

    public XmlGet getXmlGet() {
        return xmlGet;
    }

    // ---------- status enum --------------------------------------------------
    public enum Status {

        OK, ERROR;

        @Override
        public String toString() {
            switch (this) {
                case OK:
                    return "successful";
                case ERROR:
                    return "error";
                default:
                    return "default";
            }
        }

    }

}
