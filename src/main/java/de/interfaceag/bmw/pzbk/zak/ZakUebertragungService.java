package de.interfaceag.bmw.pzbk.zak;

import de.interfaceag.bmw.pzbk.dao.ZakUebertragungDao;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class ZakUebertragungService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ZakUebertragungService.class.getName());

    @Inject
    private ZakUebertragungDao zakUebertragungDao;

    public List<ZakUebertragung> getZakUebertragungByUebertragungIdsAndDerivat(Derivat derivat, List<Long> uebertragungIds) {
        LOG.info("Search Uebertragungen for Derivat {} and {} uebertragungIds.", new Object[]{derivat.getName(), uebertragungIds.size()});
        List<ZakUebertragung> result = zakUebertragungDao.getZakUebertragungByUebertragungIdsAndDerivatId(uebertragungIds, derivat.getId());
        LOG.info("Found {} Uebertragungen for Derivat {} and {} uebertragungIds.", new Object[]{result.size(), derivat.getName(), uebertragungIds.size()});
        return result;
    }

    /**
     * returns all zak-uebertragung for specific start and end dates.
     * @param startDate == null --> no results
     * @param endDate == null --> set actual date
     * @param derivatName --> secondary filter
     */
    public List<ZakUebertragung> getAllZakUebertragungForTimeRange(Date startDate, Date endDate, String derivatName) {
        LOG.info("Search ZAK-Uebertragungen for Time-Range {} bis {}, Derivat-name: {}", startDate, endDate, derivatName);

        List<ZakUebertragung> result = this.zakUebertragungDao.getAllZakUebertragungForTimeRange(startDate, endDate);

        if (derivatName == null) {
            LOG.info("Found {} ZAK-Uebertragungen for Time-Range", result.size());
            return result;
        }

        result = result.stream()
                .filter(zak -> zak.getDerivatAnforderungModul().getDerivat().getName().equals(derivatName))
                .collect(Collectors.toList());

        LOG.info("Found {} ZAK-Uebertragungen for Time-Range", result.size());
        return result;
    }
}
