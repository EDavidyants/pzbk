package de.interfaceag.bmw.pzbk.zak.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 *
 * @author sl
 */
@XmlRootElement(name = "savexml")
public class SaveXml {

    private Header header;
    private List<Dokument> dokumentList;
    private String protokolle;
    private String postprozesse;

    public SaveXml() {
    }

    public SaveXml(Header header, List<Dokument> dokumentList) {
        this.header = header;
        this.dokumentList = dokumentList;
        this.protokolle = "";
        this.postprozesse = "";
    }

    @Override
    public String toString() {
        return "SaveXml{" + "header=" + header + ", dokumentList=" + dokumentList + ", protokolle=" + protokolle + ", postprozesse=" + postprozesse + '}';
    }

    public Header getHeader() {
        return header;
    }

    @XmlElement(name = "header")
    public void setHeader(Header header) {
        this.header = header;
    }

    public void setDokumentList(List<Dokument> dokumentList) {
        this.dokumentList = dokumentList;
    }

    @XmlElementWrapper(name = "dokumente")
    @XmlElement(name = "dokument")
    public List<Dokument> getDokumentList() {
        return dokumentList;
    }

    public String getProtokolle() {
        return protokolle;
    }

    @XmlElement(name = "protokolle")
    public void setProtokolle(String protokolle) {
        this.protokolle = protokolle;
    }

    public String getPostprozesse() {
        return postprozesse;
    }

    @XmlElement(name = "postprozesse")
    public void setPostprozesse(String postprozesse) {
        this.postprozesse = postprozesse;
    }

}
