package de.interfaceag.bmw.pzbk.zak.xml;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author sl
 */
public class Anforderung {

    private String ttooluid; // wird immer noch als schluessel verwendet um mit ZAK zu kommunizieren
    private String zakid;
    private String abstimmung;
    private String entscheidung;
    private String abstimmungkommentar;

    // ---------- constructors -------------------------------------------------
    public Anforderung() {
    }

    public Anforderung(String ttooluid, String abstimmung, String entscheidung, String abstimmungkommentar) {
        this.ttooluid = ttooluid;
        this.abstimmung = abstimmung;
        this.entscheidung = entscheidung;
        this.abstimmungkommentar = abstimmungkommentar;
    }

    // ---------- getter and setter --------------------------------------------
    public String getTtooluid() {
        return ttooluid;
    }

    @XmlElement(name = "ttooluid")
    public void setTtooluid(String ttooluid) {
        this.ttooluid = ttooluid;
    }

    public String getZakid() {
        return zakid;
    }

    @XmlElement(name = "zakid")
    public void setZakid(String zakid) {
        this.zakid = zakid;
    }

    public String getAbstimmung() {
        return abstimmung;
    }

    @XmlElement(name = "abstimmung")
    public void setAbstimmung(String abstimmung) {
        this.abstimmung = abstimmung;
    }

    public String getEntscheidung() {
        return entscheidung;
    }

    @XmlElement(name = "entscheidung")
    public void setEntscheidung(String entscheidung) {
        this.entscheidung = entscheidung;
    }

    public String getAbstimmungkommentar() {
        return abstimmungkommentar;
    }

    @XmlElement(name = "abstimmungkommentar")
    public void setAbstimmungkommentar(String abstimmungkommentar) {
        this.abstimmungkommentar = abstimmungkommentar;
    }

}
