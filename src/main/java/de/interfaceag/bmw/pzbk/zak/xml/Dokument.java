package de.interfaceag.bmw.pzbk.zak.xml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sl
 */
public class Dokument {

    private String id; // <dokument id="neu" form="Anforderungen">
    private String form;
    private String ttooluid;
    private String anforderer; // AG: Herstellbarkeit SSA
    private String umsetzer; // ML: KF
    private String ordnungsstruktur; // 03. Stärken Schwächenanalyse Technologie Gesamtfahrzeug\03.08.TGF_IMC/Miko
    private String beschreibung;
    private String kommentar;
    private String anfkommentar;
    private String baugruppe; // VKBG
    private String freigabekommentar;
    private String erstellt; // Thomas SC Schmidt, TI-524, SensorCoC-Leiter FTS (oder TTL)

    private List<Konfigwert> konfigwertList;

    public Dokument() {
    }

    public Dokument(String id) {
        this.id = id;
        this.form = "Anforderungen";
        this.konfigwertList = new ArrayList<>();
    }

    public Dokument(String ttooluid, String anforderer, String umsetzer, String ordnungsstruktur,
            String beschreibung, String baugruppe, String freigabekommentar) {
        this.id = "neu";
        this.form = "Anforderungen";
        this.ttooluid = ttooluid;
        this.anforderer = anforderer;
        this.umsetzer = umsetzer;
        this.ordnungsstruktur = ordnungsstruktur;
        this.beschreibung = beschreibung;
        this.baugruppe = baugruppe;
        this.freigabekommentar = freigabekommentar;
        this.konfigwertList = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Dokument{" + "id=" + id + ", form=" + form + ", ttooluid=" + ttooluid + ", anforderer=" + anforderer + ", umsetzer=" + umsetzer + ", ordnungsstruktur=" + ordnungsstruktur + ", beschreibung=" + beschreibung + ", kommentar=" + kommentar + ", anfkommentar=" + anfkommentar + ", baugruppe=" + baugruppe + ", freigabekommentar=" + freigabekommentar + ", erstellt=" + erstellt + ", konfigwertList=" + konfigwertList + '}';
    }

    public String getId() {
        return id;
    }

    @XmlAttribute(name = "id")
    public void setId(String id) {
        this.id = id;
    }

    public String getForm() {
        return form;
    }

    @XmlAttribute(name = "form")
    public void setForm(String form) {
        this.form = form;
    }

    public String getTtooluid() {
        return ttooluid;
    }

    @XmlElement(name = "ttooluid", required = false)
    public void setTtooluid(String ttooluid) {
        this.ttooluid = ttooluid;
    }

    public String getAnforderer() {
        return anforderer;
    }

    @XmlElement(name = "anforderer", required = false)
    public void setAnforderer(String anforderer) {
        this.anforderer = anforderer;
    }

    public String getUmsetzer() {
        return umsetzer;
    }

    @XmlElement(name = "umsetzer", required = false)
    public void setUmsetzer(String umsetzer) {
        this.umsetzer = umsetzer;
    }

    public String getOrdnungsstruktur() {
        return ordnungsstruktur;
    }

    @XmlElement(name = "ordnungsstruktur", required = false)
    public void setOrdnungsstruktur(String ordnungsstruktur) {
        this.ordnungsstruktur = ordnungsstruktur;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    @XmlElement(name = "beschreibung", required = false)
    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getKommentar() {
        return kommentar;
    }

    @XmlElement(name = "kommentar", required = false)
    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    public String getAnfkommentar() {
        return anfkommentar;
    }

    @XmlElement(name = "anfkommentar", required = false)
    public void setAnfkommentar(String anfkommentar) {
        this.anfkommentar = anfkommentar;
    }

    public String getBaugruppe() {
        return baugruppe;
    }

    @XmlElement(name = "baugruppe", required = false)
    public void setBaugruppe(String baugruppe) {
        this.baugruppe = baugruppe;
    }

    public String getFreigabekommentar() {
        return freigabekommentar;
    }

    @XmlElement(name = "freigabekommentar", required = false)
    public void setFreigabekommentar(String freigabekommentar) {
        this.freigabekommentar = freigabekommentar;
    }

    public String getErstellt() {
        return erstellt;
    }

    @XmlElement(name = "erstellt", required = false)
    public void setErstellt(String erstellt) {
        this.erstellt = erstellt;
    }

    public void addKonfigwert(Konfigwert konfigwert) {
        if (konfigwert != null) {
            if (konfigwertList == null) {
                this.konfigwertList = new ArrayList<>();
            }
            this.konfigwertList.add(konfigwert);
        }
    }

    public void setKonfigwertList(List<Konfigwert> konfigwertList) {
        this.konfigwertList = konfigwertList;
    }

    @XmlElementWrapper(name = "konfigwerte")
    @XmlElement(name = "konfigwert")
    public List<Konfigwert> getKonfigwertList() {
        return konfigwertList;
    }

}
