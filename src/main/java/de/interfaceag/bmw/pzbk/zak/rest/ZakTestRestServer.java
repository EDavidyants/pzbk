package de.interfaceag.bmw.pzbk.zak.rest;

import de.interfaceag.bmw.pzbk.zak.xml.Dokument;
import de.interfaceag.bmw.pzbk.zak.xml.Header;
import de.interfaceag.bmw.pzbk.zak.xml.SaveXml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author sl
 */
@Path("/api")
public class ZakTestRestServer {

    private static final Logger LOG = LoggerFactory.getLogger(ZakTestRestServer.class.getName());

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    public Response processSaveXml(SaveXml xml) {
        logXml(xml);
        return Response.status(200).entity("sucess").build();
    }

    private void logXml(SaveXml xml) {
        LOG.info("Received {}", xml.toString());
        Header header = xml.getHeader();
        logHeader(header);
        List<Dokument> dokumentList = xml.getDokumentList();
        logDokumentList(dokumentList);
    }

    private void logHeader(Header header) {
        if (header != null) {
            String neu = header.getNeu();
            LOG.info("NEU: {}", neu);
        } else {
            LOG.info("header is null");
        }
    }

    private void logDokumentList(List<Dokument> dokumentList) {
        if (dokumentList != null) {
            String ttooluids = dokumentList.stream().map(document -> document.getTtooluid()).collect(Collectors.joining(", "));
            LOG.info("ttooluids: {}", ttooluids);
        } else {
            LOG.info("dokumentList is null");
        }
    }

    @GET
    public Response testIfApiIsOnline() {
        return Response.status(200).entity("ZAK TEST REST API is online!").build();
    }

}
