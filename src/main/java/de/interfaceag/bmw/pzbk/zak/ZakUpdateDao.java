package de.interfaceag.bmw.pzbk.zak;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Dependent
public class ZakUpdateDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager em;

    protected void persistZakUpdate(ZakUpdate zakUpdate) {
        if (zakUpdate.getId() != null) {
            em.merge(zakUpdate);
        } else {
            em.persist(zakUpdate);
        }
        em.flush();
    }

    protected Optional<ZakUpdate> find(Long id) {
        if (id != null) {
            return Optional.ofNullable(em.find(ZakUpdate.class, id));
        }
        return Optional.empty();
    }

}
