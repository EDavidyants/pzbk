package de.interfaceag.bmw.pzbk.zak.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl
 */
@ApplicationPath("zak")
public class ZakRestApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> sets = new HashSet<>();
        sets.add(ZakTestRestServer.class);
        return sets;
    }

}
