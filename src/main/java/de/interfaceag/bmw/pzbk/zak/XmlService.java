package de.interfaceag.bmw.pzbk.zak;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusChangeService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.enums.LogLevel;
import de.interfaceag.bmw.pzbk.enums.SystemType;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.exceptions.ZakUebertragungException;
import de.interfaceag.bmw.pzbk.services.AnforderungFreigabeService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.LogService;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;
import de.interfaceag.bmw.pzbk.zak.xml.Dokument;
import de.interfaceag.bmw.pzbk.zak.xml.Header;
import de.interfaceag.bmw.pzbk.zak.xml.Konfigwert;
import de.interfaceag.bmw.pzbk.zak.xml.SaveXml;
import de.interfaceag.bmw.pzbk.zak.xml.XmlGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author sl
 */
@Stateless
public class XmlService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(XmlService.class);

    private static final String TARGETPATH = "/ziel"; //"/ziel?OpenForm";
    private static final String TARGETPATHGET = "/get/ergebniss.xml"; //"/get/ergebniss.xml?open&derivat=";

    private static final String USER = "qqbitr0";

    @Inject
    protected AnforderungFreigabeService anforderungFreigabeService;
    @Inject
    protected BerechtigungService berechtigungService;
    @Inject
    private ConfigService configService;
    @Inject
    protected AnforderungMeldungHistoryService historyService;
    @Inject
    protected LogService logService;
    @Inject
    protected ZakVereinbarungService zakVereinbarungService;
    @Inject
    protected ZakUebertragungService zakUebertragungService;
    @Inject
    private DerivatStatusChangeService derivatStatusChangeSerivce;

    private WebTarget setUpWebTarget(String targetpath) {
        String targetUrl = configService.getZakUrl(); //"https://zaktest.bmwgroup.net/zielkat/";
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(targetUrl + "ttoolss.nsf").path(targetpath);
        LOG.debug("WebTarget: {} established", target);
        return target;
    }

    private String getAuthorizationHeaderValue() {
        String password = configService.getUserPassword();
        if (password.isEmpty()) {
            LOG.error("User password is empty. Authorization will fail!");
        }
        String usernameAndPassword = USER + ":" + password;
        return String.format("Basic %s", java.util.Base64.getEncoder().encodeToString(usernameAndPassword.getBytes()));
    }

    public Boolean processResponseContent(String response) {
        if (response.contains("Validierung fertig")) {
            return Boolean.TRUE;
        } else if (response.contains("Link auf das Ergebnis")) {
            return Boolean.TRUE;
        } else if (response.contains("Form processed")) {
            String message = "XML konnte nicht verarbeitet werden.";
            LOG.warn(message);
            logService.addLogEntry(LogLevel.WARNING, SystemType.ZAKSST, message);
        } else if (response.contains("anforderer#nicht in Werteliste ZAK enthalten")) {
            String message = "Invalid value for <anforderer> tag. (ZAK Einordnung)";
            LOG.warn(message);
            logService.addLogEntry(LogLevel.WARNING, SystemType.ZAKSST, message);
        } else if (response.contains("User#Übergebener User konnte nicht ermittelt werden")) {
            String message = "Invalid value for <user> tag.";
            LOG.warn(message);
            logService.addLogEntry(LogLevel.WARNING, SystemType.ZAKSST, message);
        }
        LOG.warn(response);
        return Boolean.FALSE;
    }

    @SuppressWarnings("checkstyle:IllegalCatch")
    String postZakUbertragungen(SaveXml xml) {
        WebTarget webTarget = setUpWebTarget(TARGETPATH).queryParam("CreateDocument", "");
        String xmlAsString = marshalXml(xml);

        Form form = new Form();
        form.param("XML", xmlAsString);

        try {
            LOG.debug("start post request");
            Response response = webTarget
                    .request(MediaType.TEXT_HTML)
                    .header("Authorization", getAuthorizationHeaderValue())
                    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE));
            LOG.debug("read response");

            String responseContent = response.readEntity(String.class);
            LOG.debug(responseContent);
            return responseContent;
        } catch (Exception exception) {
            logService.addLogEntry(LogLevel.WARNING, SystemType.ZAKSST, exception.toString());
            return exception.toString();
        }
    }

    private String convertToUrlString(String urlString) {
        urlString = urlString.replaceAll("\\s", "%20");
        urlString = urlString.replaceAll("/", "%2F");
        return urlString;
    }

    @SuppressWarnings("checkstyle:IllegalCatch")
    ZakUpdateDto getZakUbertragungen(Derivat derivat) {
        String target = TARGETPATHGET;

        WebTarget webTarget = setUpWebTarget(target)
                .queryParam("open", "")
                .queryParam("derivat", convertToUrlString(derivat.getName()));

        String zakUrl = webTarget.getUri().toString();

        Response response = null;

        try {
            response = webTarget
                    .request(MediaType.APPLICATION_XML)
                    .header("Authorization", getAuthorizationHeaderValue())
                    .get();

            LOG.info("Response Status: {}", response.getStatus());
            if (response.getMediaType() != null) {
                LOG.info("Response MediaType: {}", response.getMediaType());
            }
            LOG.info("Response: {}", response);

            if (response.getStatus() == 200) {
                String result = response.readEntity(String.class);
                LOG.info(result);

                response.close();

                JAXBContext jaxbContext = JAXBContext.newInstance(XmlGet.class);
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                StringReader reader = new StringReader(result);
                XmlGet resultXml = (XmlGet) unmarshaller.unmarshal(reader);
                LOG.info("Parse XML successful: {}", resultXml);

                return new ZakUpdateDto(derivat, zakUrl, ZakUpdateDto.Status.OK, result, resultXml);
            }

        } catch (UnmarshalException ex) {
            logService.addLogEntry(LogLevel.WARNING, SystemType.ZAKSST, ex.toString());
            String message = "XML für Derivat " + derivat.toString() + " ist leer!";
            logService.addLogEntry(LogLevel.WARNING, SystemType.ZAKSST, message);
            return new ZakUpdateDto(derivat, zakUrl, ZakUpdateDto.Status.OK, message);
        } catch (Exception ex) {
            LOG.warn(ex.toString());
            logService.addLogEntry(LogLevel.SEVERE, SystemType.ZAKSST, ex.toString());
            return new ZakUpdateDto(derivat, zakUrl, ZakUpdateDto.Status.ERROR, ex.toString());
        } finally {
            if (response != null) {
                response.close();
            }
        }

        return null;
    }

    private static Optional<ZakUebertragung> getZakUebertragungFromList(List<ZakUebertragung> zakUebertragungen, Long uebertragungId) {
        return zakUebertragungen.stream().filter(z -> z.getUebertragungId().equals(uebertragungId)).findAny();
    }

    List<ZakUebertragung> parseXmlGet(XmlGet xmlGet, Derivat derivat) {
        List<ZakUebertragung> result = new ArrayList<>();
        if (xmlGet != null && xmlGet.getAnforderungList() != null && !xmlGet.getAnforderungList().isEmpty() && derivat != null) {

            List<Long> uebertragungIds = xmlGet.getAnforderungList().stream()
                    .map(xml -> xml.getTtooluid())
                    .filter(id -> RegexUtils.matchesId(id))
                    .map(idString -> Long.parseLong(idString))
                    .collect(Collectors.toList());

            List<ZakUebertragung> zakUebertragungen = zakUebertragungService.getZakUebertragungByUebertragungIdsAndDerivat(derivat, uebertragungIds);

            xmlGet.getAnforderungList().forEach(anforderung -> {

                Long uebertragungId = Long.parseLong(anforderung.getTtooluid());

                Optional<ZakUebertragung> existingZakUebertragungForAnforderungModul = getZakUebertragungFromList(zakUebertragungen, uebertragungId);

                if (existingZakUebertragungForAnforderungModul.isPresent()) {
                    handleExistingZakUebertragungIsPresent(derivat, result, anforderung, existingZakUebertragungForAnforderungModul.get());
                } else {
                    handleNoExistingZakUebertragungFound(derivat, anforderung);
                }
                checkXmlForZielvereinbarung(xmlGet, derivat);
            });
        } else if (derivat != null) {
            handleNoResultForDerivat(derivat);
        } else {
            handleNoDerivatSubmitted();
        }

        if (derivat != null && derivat.getName() != null) {
            if (result.isEmpty()) {
                LOG.info("Es wurden keine ZakUebertragungen f\u00fcr Derivat {} geupdated.", derivat.getName());
                logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, String.format("Es wurden keine ZakUebertragungen für Derivat %s geupdated.", derivat.getName()));
            } else {
                LOG.info("Es wurden {} ZakUebertragungen f\u00fcr Derivat {} geupdated.", result.size(), derivat.getName());
                logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, String.format("Es wurden %d ZakUebertragungen für Derivat %s geupdated.", result.size(), derivat.getName()));
            }
        }

        return result;
    }

    private void handleNoDerivatSubmitted() {
        LOG.error("Es wurde kein Derivat in die Funktion parseXmlGet übergeben");
        logService.addLogEntry(LogLevel.SEVERE, SystemType.ZAKSST, "Es wurde kein Derivat in die Funktion parseXmlGet übergeben");
    }

    private void handleNoResultForDerivat(Derivat derivat) {
        LOG.warn("Das XML f\u00fcr Derivat {} enth\u00e4lt keine Ergebnisse.", derivat.getName());
        logService.addLogEntry(LogLevel.WARNING, SystemType.ZAKSST, "Das XML f\u00fcr Derivat " + derivat.getName() + " enth\u00e4lt keine Ergebnisse.");
    }

    private void handleNoExistingZakUebertragungFound(Derivat derivat, de.interfaceag.bmw.pzbk.zak.xml.Anforderung anforderung) {
        LOG.error("Es wurde keine passende ZAK \u00dcbertragung f\u00fcr Derivat {} und TtoolId {} gefunden.", new Object[]{derivat.getName(), anforderung.getTtooluid()});
        logService.addLogEntry(LogLevel.SEVERE, SystemType.ZAKSST, "Es wurde keine passende ZAK \u00dcbertragung f\u00fcr Derivat " + derivat.getName() + " und TtoolId " + anforderung.getTtooluid() + " gefunden.");
    }

    private void handleExistingZakUebertragungIsPresent(Derivat derivat, List<ZakUebertragung> result, de.interfaceag.bmw.pzbk.zak.xml.Anforderung anforderung, ZakUebertragung zakUebertragung) {
        LOG.info("Found matching ZakUebertragung {}", zakUebertragung.toString());
        logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, "Found matching ZakUebertragung " + zakUebertragung.toString());

        ZakStatus status = parseZakStatusFromXml(anforderung.getAbstimmung(), anforderung.getEntscheidung());
        boolean changed = false;
        if (zakUebertragung.getZakId() == null || !zakUebertragung.getZakId().equals(anforderung.getZakid())) {
            zakUebertragung.setZakId(anforderung.getZakid());
            changed = true;
            logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, "ZAKID f\u00fcr ZAK-\u00dcbertragung " + zakUebertragung.toString() + " wurde auf " + zakUebertragung.getZakId() + "gesetzt");
            LOG.info("ZAKID f\u00fcr ZAK-\u00dcbertragung {} wurde auf {} gesetzt", new Object[]{zakUebertragung.toString(), zakUebertragung.getZakId()});
        }
        if (status != null) {
            changed = handleStatusisNotNull(anforderung, zakUebertragung, status, changed);
        } else {
            LOG.error("ZAK Status f\u00fcr Derivat {} und Zak\u00dcbertragung {} konnte nicht verarbeitet werden!", new Object[]{derivat.getName(), anforderung.getTtooluid()});
            logService.addLogEntry(LogLevel.SEVERE, SystemType.ZAKSST, "ZAK Status f\u00fcr Derivat " + derivat.getName() + " und Zak\u00dcbertragung " + anforderung.getTtooluid() + " konnte nicht verarbeitet werden!");
        }
        if (changed) {
            handleZakUebertragungWasChanged(result, zakUebertragung);
        } else {
            handleZakUebertragungWasNotChanged(zakUebertragung);
        }
    }

    private void handleZakUebertragungWasNotChanged(ZakUebertragung zakUebertragung) {
        LOG.info("ZAK-\u00dcbertragung {} wurde nicht ver\u00e4ndert. Es werden keine \u00c4nderungen gespeichert.", zakUebertragung);
        logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, "ZAK-\u00dcbertragung " + zakUebertragung.toString() + " wurde nicht verändert. Es werden keine Änderungen gespeichert.");
    }

    private void handleZakUebertragungWasChanged(List<ZakUebertragung> result, ZakUebertragung zakUebertragung) {
        result.add(zakUebertragung);
        LOG.info("ZAK-\u00dcbertragung {} wurde ver\u00e4ndert. Es werden \u00c4nderungen gespeichert.", zakUebertragung);
        logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, "ZAK-\u00dcbertragung " + zakUebertragung.toString() + " wurde verändert. Es werden Änderungen gespeichert.");
    }

    private boolean handleStatusisNotNull(de.interfaceag.bmw.pzbk.zak.xml.Anforderung anforderung, ZakUebertragung zakUebertragung, ZakStatus status, boolean changed) {
        if (status != zakUebertragung.getZakStatus()) {
            zakUebertragung.setZakStatus(status);
            zakUebertragung.setZakKommentar(anforderung.getAbstimmungkommentar());
            changed = true;
            LOG.info("Status f\u00fcr ZAK-\u00dcbertragung {} wurde auf {} gesetzt", zakUebertragung, status);
            logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, "Status f\u00fcr ZAK-\u00dcbertragung " + zakUebertragung + " wurde auf " + status + "gesetzt");
        } else {
            LOG.info("Status f\u00fcr ZAK-\u00dcbertragung {} entspricht bereits dem Wert aus ZAK: {}", zakUebertragung, status);
            logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, "Status f\u00fcr ZAK-\u00dcbertragung " + zakUebertragung + " entspricht bereits dem Wert aus ZAK: " + status);
        }
        return changed;
    }

    private void checkXmlForZielvereinbarung(XmlGet xmlGet, Derivat derivat) {
        if (xmlGet != null && xmlGet.getAnforderungList() != null && !xmlGet.getAnforderungList().isEmpty()) {
            boolean isVereinbart = xmlGet.getAnforderungList().stream().map(de.interfaceag.bmw.pzbk.zak.xml.Anforderung::getEntscheidung).anyMatch(e -> e.contains("LOP"));
            if (isVereinbart && !derivat.isInaktiv() && !derivat.getStatus().equals(DerivatStatus.ZV)) {
                LOG.info("Update derivat {} to status Zielvereinbarung from ZAK", derivat);
                derivatStatusChangeSerivce.changeStatus(derivat, DerivatStatus.ZV);
            }
        }
    }

    protected static ZakStatus parseZakStatusFromXml(String xmlStatus, String entscheidung) {
        if (entscheidung.trim().equals("Zurückgezogen durch Anforderer")) {
            return ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER;
        }
        switch (xmlStatus) {
            case "bestätigt":
                return ZakStatus.DONE;
            case "bestätigt aus Vorprozess":
                return ZakStatus.BESTAETIGT_AUS_VORPROZESS;
            case "nicht umsetzbar":
                return ZakStatus.NICHT_UMSETZBAR;
            case "nicht bewertbar":
                return ZakStatus.NICHT_BEWERTBAR;
            case "offen":
                return ZakStatus.PENDING;
            case "abgelehnt":
            default:
                LOG.warn("XmlStatus: {} konnte nicht verarbeitet werden!", xmlStatus);
                return null;
        }
    }

    private void checkForValidZakUebertragung(ZakUebertragung zakUebertragung) throws ZakUebertragungException {
        if (zakUebertragung.getDerivatAnforderungModul() == null) {
            throw new ZakUebertragungException("Fehlerhafte Daten: DerivatAnforderungModul der " + zakUebertragung + " ist null");
        } else if (zakUebertragung.getDerivatAnforderungModul().getAnforderung() == null) {
            throw new ZakUebertragungException("Fehlerhafte Daten: Anforderung der " + zakUebertragung + " ist null");
        } else if (zakUebertragung.getDerivatAnforderungModul().getAnforderer() == null) {
            throw new ZakUebertragungException("Fehlerhafte Daten: Anforderer der " + zakUebertragung + " ist null");
        }

        if (zakUebertragung.getUebertragungId() == null) {
            throw new ZakUebertragungException("Fehlerhafte Daten: UebertragungId der " + zakUebertragung + " ist null");
        }
    }

    public SaveXml generateXmlForZakUebertragung(ZakUebertragung zakUebertragung) throws ZakUebertragungException {
        checkForValidZakUebertragung(zakUebertragung);
        Anforderung anforderung = zakUebertragung.getDerivatAnforderungModul().getAnforderung();
        Modul modul = zakUebertragung.getDerivatAnforderungModul().getModul();
        String uuid = zakUebertragung.getUebertragungId().toString();

        boolean update = zakVereinbarungService.existsAnforderungModulInZak(anforderung, modul);

        boolean changeZakId = false;
        if (update && historyService.anforderungIsChangedAfterLastPlVorschlag(anforderung)) {
            Date posschangeBeschr = historyService.getLastChangeDateForAttribute("Beschreibung", anforderung);
            Date posschangeUmsVorschl = historyService.getLastChangeDateForAttribute("Umsetzer-Änderungsvorschlag", anforderung);

            Date changeDate;

            if (posschangeBeschr != null && posschangeUmsVorschl != null) {
                changeDate = posschangeBeschr.after(posschangeUmsVorschl) ? posschangeBeschr : posschangeUmsVorschl;
            } else if (posschangeBeschr != null && posschangeUmsVorschl == null) {
                changeDate = posschangeBeschr;
            } else if (posschangeBeschr == null && posschangeUmsVorschl != null) {
                changeDate = posschangeUmsVorschl;
            } else {
                changeDate = new Date();
            }

            ZakUebertragung zak = zakVereinbarungService.getZakUebertragungForAnforderungModulAfterDate(anforderung, modul, changeDate);

            if (zak != null) {
                Derivat fistAfterChange = zak.getDerivatAnforderungModul().getDerivat();
                Derivat ourCurrentDerivat = zakUebertragung.getDerivatAnforderungModul().getDerivat();
                changeZakId = Objects.equals(ourCurrentDerivat.getId(), fistAfterChange.getId());
            }

            if (changeZakId) {
                update = false;

                if (!uuid.endsWith("042")) {
                    uuid += "042";
                    try {
                        zakUebertragung.setUebertragungId(Long.parseLong(uuid));
                        zakVereinbarungService.persistZakUebertragung(zakUebertragung);
                        logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, "ZAK ID für " + zakUebertragung.toString() + " wurde angepasst da Änderungen in der Anforderung gefunden wurden.");
                    } catch (NumberFormatException ex) {
                        logService.addLogEntry(LogLevel.WARNING, SystemType.ZAKSST, ex.toString());
                        return null;
                    }
                }
            }
        }

        Dokument dokument;
        if (update) {
            dokument = generateUpdateDokument(uuid);
        } else {
            dokument = generateDokument(uuid, modul, anforderung);
        }

        Konfigwert konfigwert = generateKonfigwert(zakUebertragung.getDerivatAnforderungModul().getDerivat());
        dokument.addKonfigwert(konfigwert);

        Mitarbeiter anforderer = zakUebertragung.getDerivatAnforderungModul().getAnforderer();
        if (anforderer == null) {
            anforderer = berechtigungService.getAnfordererForSensorCocDerivat(zakUebertragung.getDerivatAnforderungModul().getAnforderung().getSensorCoc(),
                    zakUebertragung.getDerivatAnforderungModul().getDerivat());
        }
        if (anforderer != null) {
            Header header = generateHeader(anforderer, update);
            List<Dokument> dokumente = new ArrayList<>();
            dokumente.add(dokument);
            SaveXml result = new SaveXml(header, dokumente);
            updateGeneratedXmlForZakUebertragung(zakUebertragung, result);
            return result;
        } else {
            String message = "Anforderer f\u00fcr Zak\u00dcbertragung " + zakUebertragung.toString() + " ist nicht definiert und es existiert kein berechtigter Nutzer.";
            logService.addLogEntry(LogLevel.WARNING, SystemType.ZAKSST, message);
            LOG.warn(message);
        }
        return null;
    }

    private void updateGeneratedXmlForZakUebertragung(ZakUebertragung zakUebertragung, SaveXml saveXml) {
        String generatedXml = marshalXml(saveXml);
        zakUebertragung.setGeneratedXml(generatedXml);
        zakVereinbarungService.persistZakUebertragung(zakUebertragung);
    }

    private Dokument generateUpdateDokument(String uuid) {
        return new Dokument(uuid);
    }

    private void checkForValidAnforderung(Anforderung anforderung)
            throws ZakUebertragungException {
        if (anforderung.getSensorCoc() == null) {
            throw new ZakUebertragungException("Fehlerhafte Daten: SensorCoc der Anforderung "
                    + anforderung.toString() + " is null");
        } else if (anforderung.getSensorCoc().getOrdnungsstruktur() == null) {
            throw new ZakUebertragungException("Fehlerhafte Daten: Ordnungsstruktur des SensorCoc "
                    + anforderung.getSensorCoc().toString() + " is null");
        } else if (anforderung.getSensorCoc().getZakEinordnung() == null) {
            throw new ZakUebertragungException("Fehlerhafte Daten: ZakEinordnung des SensorCoc "
                    + anforderung.getSensorCoc().toString() + " is null");
        }
    }

    private void checkForValidModul(Modul modul)
            throws ZakUebertragungException {
        if (modul.getName() == null) {
            throw new ZakUebertragungException("Name des Moduls"
                    + modul.toString() + " is null");
        }
    }

    protected Dokument generateDokument(String uuid, Modul modul,
                                        Anforderung anforderung) throws ZakUebertragungException {
        checkForValidAnforderung(anforderung);
        checkForValidModul(modul);
        String anforderer = anforderung.getSensorCoc().getZakEinordnung();
        String umsetzer = modul.getName();
        String ordnungsstruktur = anforderung.getSensorCoc().getOrdnungsstruktur();
        String beschreibung = anforderung.getBeschreibungAnforderungDe();
        // Baugruppe muss aus ZAK Sicht entweder VKBG oder VAGB0/1/2 sein
        String baugruppe = anforderung.getZeitpktUmsetzungsbest() ? "VKBG" : "VABG0";

        List<AnforderungFreigabeBeiModulSeTeam> anforderungFreigaben = anforderungFreigabeService.getAnfoFreigabeByAnforderung(anforderung);
        StringBuilder freigabeKommentarBuilder = new StringBuilder();
        anforderungFreigaben.forEach(af -> {
            freigabeKommentarBuilder
                    .append(af.getDatumFormatted())
                    .append(" | ");
            if (af.getBearbeiter() != null) {
                freigabeKommentarBuilder.append(af.getBearbeiter().toString())
                        .append(": ");
            }
            freigabeKommentarBuilder
                    .append(af.getKommentar())
                    .append(" \n");
        });
        String freigabekommentar = freigabeKommentarBuilder.toString();
        Dokument dokument
                = new Dokument(uuid, anforderer, umsetzer, ordnungsstruktur, beschreibung, baugruppe, freigabekommentar);
        dokument.setKommentar(anforderung.getKommentarAnforderung());

        StringBuilder anfKommentarBuilder = new StringBuilder();
        anfKommentarBuilder.append("Festgestellt In: ");
        anforderung.getFestgestelltIn().forEach(f -> anfKommentarBuilder.append(f.getWert()).append(", "));
        anfKommentarBuilder
                .append("\n").append(anforderung.isStaerkeSchwaeche() ? "Stärke" : "Schwäche")
                .append("\n").append("Referenz: ").append(anforderung.getReferenzen())
                .append("\n").append("Auswirkung: ");
        anforderung.getAuswirkungen().forEach(a -> anfKommentarBuilder.append(a.getAuswirkung()).append(": ").append(a.getWert()).append(", "));
        anfKommentarBuilder
                .append("\n").append("Lösungsvorschlag: ").append(anforderung.getLoesungsvorschlag());

        dokument.setAnfkommentar(normalizeStringToUTF8(anfKommentarBuilder.toString()));

        Optional<Mitarbeiter> sensorCocLeiter = berechtigungService.getSensorCoCLeiterOfSensorCoc(anforderung.getSensorCoc());
        if (sensorCocLeiter.isPresent()) {
            dokument.setErstellt(sensorCocLeiter.get().toString()); // FTS
        } else {
            dokument.setErstellt("");
        }
        return dokument;
    }

    private Header generateHeader(Mitarbeiter user, boolean update) {
        Date now = new Date();
        SimpleDateFormat saveDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String savedate = saveDateFormat.format(now);
        SimpleDateFormat saveTimeFormat = new SimpleDateFormat("HH:mm:ss");
        String savetime = saveTimeFormat.format(now);
        String geltung = "ZA";
        String qNumber = user.getQNumber();
        if (update) {
            return new Header(savedate, savetime, geltung, qNumber, "nein");
        }
        return new Header(savedate, savetime, geltung, qNumber, "ja");
    }

    private Konfigwert generateKonfigwert(Derivat derivat) {
        String zuordnung = derivat.getName();
        String wert = "";
        return new Konfigwert(zuordnung, wert);
    }

    public String marshalResponseXml(XmlGet xml) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(XmlGet.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            jaxbMarshaller.marshal(xml, System.out);
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(xml, sw);
            return sw.toString();
        } catch (JAXBException ex) {
            LOG.error(null, ex);
        }
        return null;
    }

    public String marshalXml(SaveXml xml) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(SaveXml.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            jaxbMarshaller.marshal(xml, System.out);
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(xml, sw);
            return sw.toString();
        } catch (JAXBException ex) {
            LOG.error(null, ex);
        }
        return null;
    }

    public String normalizeStringToUTF8(String input) {
        return input.replace("Ä", "Ae").replace("ä", "ae")
                .replace("Ö", "Oe").replace("ö", "oe")
                .replace("Ü", "Ue").replace("ü", "ue")
                .replace("ß", "ss");
    }
}
