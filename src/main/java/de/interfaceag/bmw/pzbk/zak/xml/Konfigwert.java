package de.interfaceag.bmw.pzbk.zak.xml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sl
 */
@XmlRootElement(name = "konfigwert")
public class Konfigwert {

    private String id; // <konfigwert id="neu">
    private String zuordnung; // U20/MINI
    private String wert; // analog aktueller Baustand F45.
    // optional
    private String kommentarwert;

    public Konfigwert() {
    }

    public Konfigwert(String zuordnung, String wert) {
        this.id = "neu";
        this.zuordnung = zuordnung;
        this.wert = wert;
    }

    @Override
    public String toString() {
        return "Konfigwert{" + "id=" + id + ", zuordnung=" + zuordnung + ", wert=" + wert + ", kommentarwert=" + kommentarwert + '}';
    }

    public String getId() {
        return id;
    }

    @XmlAttribute(name = "id")
    public void setId(String id) {
        this.id = id;
    }

    public String getZuordnung() {
        return zuordnung;
    }

    @XmlElement(name = "zuordnung")
    public void setZuordnung(String zuordnung) {
        this.zuordnung = zuordnung;
    }

    public String getWert() {
        return wert;
    }

    @XmlElement(name = "wert")
    public void setWert(String wert) {
        this.wert = wert;
    }

    public String getKommentarwert() {
        return kommentarwert;
    }

    @XmlElement(name = "kommentarwert", required = false)
    public void setKommentarwert(String kommentarwert) {
        this.kommentarwert = kommentarwert;
    }
}
