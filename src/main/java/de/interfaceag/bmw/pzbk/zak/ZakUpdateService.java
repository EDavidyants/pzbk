package de.interfaceag.bmw.pzbk.zak;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.ZakDoubleUpdate;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.enums.LogLevel;
import de.interfaceag.bmw.pzbk.enums.SystemType;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.LogService;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class ZakUpdateService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ZakUpdateService.class.getName());

    @Inject
    private ZakUpdateDao zakUpdateDao;
    @Inject
    private ZakDoubleUpdateDao zakDoubleUpdateDao;

    @Inject
    private LogService logService;
    @Inject
    private XmlService xmlService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private ZakVereinbarungService zakVereinbarungService;
    @Inject
    private DerivatAnforderungModulService derivatAnforderungModulService;

    public void getZakUpdate() {
        List<Derivat> relevantDerivate = derivatService.getAllDerivateWithZielvereinbarungFalse();
        if (relevantDerivate == null) {
            LOG.error("relevantDerivat is null!");
            return;
        }
        if (relevantDerivate.isEmpty()) {
            LOG.error("relevantDerivat is empty!");
            return;
        }
        LOG.info("{} relevante Derivate gefunden.", Integer.toString(relevantDerivate.size()));

        Iterator<Derivat> iterator = relevantDerivate.iterator();
        while (iterator.hasNext()) {
            Derivat derivat = iterator.next();
            updateZakUebertragungenForDerivat(derivat);
        }
    }

    private void updateZakUebertragungenForDerivat(Derivat derivat) {
        logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, "Starte ZAK Update für Derivat " + derivat.getName());
        Set<Long> ttooluidUpdates = new HashSet<>();
        getZakUpdateAndUpdateDerivat(derivat).forEach(zakUpdate -> updateAndPersistZakUebertragung(ttooluidUpdates, zakUpdate));
    }

    private List<ZakUebertragung> getZakUpdateAndUpdateDerivat(Derivat derivat) {
        ZakUpdateDto zakUpdateDto = xmlService.getZakUbertragungen(derivat);
        addZakUpdate(zakUpdateDto);
        List<ZakUebertragung> result = xmlService.parseXmlGet(zakUpdateDto.getXmlGet(), derivat);
        if (!result.isEmpty()) {
            derivat.setZakUpdate(new Date());
            derivatService.persistDerivat(derivat);
            logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, "ZAK Update für " + derivat.toString() + " erfolgreich. Es wurden " + result.size() + " Änderungen gespeichert.");
        } else {
            logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, "Es gibt keine neuen Updates für " + derivat.toString() + ".");
        }
        return result;
    }

    private void updateAndPersistZakUebertragung(Set<Long> ttooluidUpdatesLongs, ZakUebertragung zakUebertragung) {
        zakUebertragung.getDerivatAnforderungModul().setZakStatus(zakUebertragung.getZakStatus());
        zakUebertragung.setAenderungsdatum(new Date());
        checkForDoubleZakUpdate(ttooluidUpdatesLongs, zakUebertragung);
        zakVereinbarungService.persistZakUebertragung(zakUebertragung);
        logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, "ZAK Update für " + zakUebertragung.toString() + " erfolgreich gespeichert.");
        derivatAnforderungModulService.updateDerivatAnforderungModulZakStatus(zakUebertragung.getZakStatus(), zakUebertragung.getZakKommentar(), zakUebertragung.getDerivatAnforderungModul());
    }

    private void checkForDoubleZakUpdate(Set<Long> ttooluidUpdatesLongs, ZakUebertragung zakUebertragung) {
        if (ttooluidUpdatesLongs.contains(zakUebertragung.getUebertragungId())) {
            ZakDoubleUpdate zakDoubleUpdate = new ZakDoubleUpdate(zakUebertragung.getDerivatAnforderungModul().getDerivat(), new Date(), zakUebertragung.getUebertragungId(), zakUebertragung.getZakId());
            zakDoubleUpdateDao.persistZakUpdate(zakDoubleUpdate);
        } else {
            ttooluidUpdatesLongs.add(zakUebertragung.getUebertragungId());
        }
    }

    private void addZakUpdate(ZakUpdateDto zakUpdateDto) {
        ZakUpdateConverter zakUpdateConverter = new ZakUpdateConverter();
        ZakUpdate zakUpdate = zakUpdateConverter.convertFromDto(zakUpdateDto);
        persistZakUpdate(zakUpdate);
    }

    public byte[] getZakResponseForZakUpdateId(String zakUpdateIdAsString) {
        if (zakUpdateIdAsString != null && RegexUtils.matchesId(zakUpdateIdAsString)) {
            Long zakUpdateId = Long.parseLong(zakUpdateIdAsString);

            Optional<ZakUpdate> zakUpdateById = find(zakUpdateId);
            if (zakUpdateById.isPresent()) {
                byte[] zakResponse = zakUpdateById.get().getZakResponse();
                if (zakResponse != null) {
                    return zakResponse;
                } else {
                    LOG.info("zakResponse is null");
                }
            } else {
                LOG.info("zakUpdate is not present");
            }
        } else {
            LOG.info("zakUpdateIdAsString {} is null or does not match", zakUpdateIdAsString);
        }
        return null;
    }

    private Optional<ZakUpdate> find(Long zakUpdateId) {
        return zakUpdateDao.find(zakUpdateId);
    }

    private void persistZakUpdate(ZakUpdate zakUpdate) {
        zakUpdateDao.persistZakUpdate(zakUpdate);
    }

}
