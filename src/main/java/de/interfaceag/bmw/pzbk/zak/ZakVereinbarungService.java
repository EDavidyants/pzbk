package de.interfaceag.bmw.pzbk.zak;

import de.interfaceag.bmw.pzbk.dao.ZakUebertragungDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.LogLevel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.SystemType;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.LogService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author evda
 */
@Stateless
public class ZakVereinbarungService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ZakVereinbarungService.class.getName());

    private static final Boolean ZAKSIMULATION = Boolean.FALSE; // true: simulation is enabled; false: no simulation

    @Inject
    private ZakUebertragungDao zakUebertragungDao;

    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    private LogService logService;
    @Inject
    private ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;
    @Inject
    private Session session;

    private Boolean hasBerechtigungAsAnfordererForDerivat(Derivat derivat) {
        Mitarbeiter currentUser = session.getUser();
        if (currentUser != null && derivat != null) {
            if (session.hasRole(Rolle.ANFORDERER)) {
                return berechtigungService.getBerechtigungByMitarbeiterAndRolleAndZielById(currentUser, Rolle.ANFORDERER, String.valueOf(derivat.getId()), BerechtigungZiel.DERIVAT)
                        .stream().anyMatch(b -> b.getRechttype() == Rechttype.SCHREIBRECHT);
            } else if (session.hasRole(Rolle.ADMIN)) {
                return true;
            }
        }
        return false;
    }

    public Map<Derivat, List<ZakUebertragung>> sendAnforderungenDerivatIdsNachZak(List<Long> anforderungDerivatIdList) {

        List<ZuordnungAnforderungDerivat> anforderungDerivatList = zuordnungAnforderungDerivatService.getZuordnungAnforderungDerivatByIdList(anforderungDerivatIdList);
        return sendAnforderungenNachZak(anforderungDerivatList);

    }

    public Map<Derivat, List<ZakUebertragung>> sendAnforderungenNachZak(List<ZuordnungAnforderungDerivat> anforderungDerivatList) {
        Map<Derivat, List<ZakUebertragung>> report = new HashMap<>();

        if (anforderungDerivatList != null) {
            for (ZuordnungAnforderungDerivat da : anforderungDerivatList) {
                List<DerivatAnforderungModul> derivatAnforderungModulList = new ArrayList<>();
                List<DerivatAnforderungModul> derivatAnforderungModul = derivatAnforderungModulService.getDerivatAnforderungModulByAnforderungAndDerivat(da.getAnforderung(), da.getDerivat());
                List<DerivatAnforderungModul> filteredDerivatAnforderungModul = derivatAnforderungModul.stream().filter(dam -> !dam.getZakStatus().equals(ZakStatus.KEINE_ZAKUEBERTRAGUNG)).collect(Collectors.toList());
                derivatAnforderungModulList.addAll(filteredDerivatAnforderungModul);

                Map<Derivat, List<ZakUebertragung>> anforderungReport = sendModuleNachZak(derivatAnforderungModulList);
                anforderungReport.entrySet().forEach(e -> {
                    if (report.containsKey(e.getKey())) {
                        List<ZakUebertragung> updatedList = new ArrayList<>(e.getValue());
                        updatedList.addAll(e.getValue());
                        report.replace(e.getKey(), updatedList);
                    } else {
                        report.put(e.getKey(), e.getValue());
                    }
                });

            }
        }
        return report;
    }

    public Map<Derivat, List<ZakUebertragung>> sendModuleNachZak(List<DerivatAnforderungModul> derivatAnforderungModulList) {
        Mitarbeiter currentUser = session.getUser();
        List<DerivatAnforderungModul> cleanList = derivatAnforderungModulService.getDerivatAnforderungModulByIdList(derivatAnforderungModulList.stream().map(da -> da.getId()).collect(Collectors.toList()));

        List<ZakUebertragung> nachZakList = new ArrayList<>();

        for (DerivatAnforderungModul derivatAnforderungModul : cleanList) {
            Derivat derivat = derivatAnforderungModul.getDerivat();

            // check if anforderung has modulSeTeams with VereinbarungType 'ZAK'
            Anforderung anforderung = derivatAnforderungModul.getAnforderung();

            List<AnforderungFreigabeBeiModulSeTeam> anfoModulSeTeam = anforderung.getAnfoFreigabeBeiModul().stream()
                    .filter(f -> f.getVereinbarungType() == VereinbarungType.ZAK).collect(Collectors.toList());

            if (!anfoModulSeTeam.isEmpty()) {
                if (!hasBerechtigungAsAnfordererForDerivat(derivat)) {
                    logService.logWithLogEntry(LogLevel.SEVERE, SystemType.ZAKSST, "user "
                            + currentUser.toString() + " hat insufficient authorization to generate ZakUebertragung for "
                            + derivatAnforderungModul.toString(), ZakVereinbarungService.class.getName());

                } else {
                    ZakUebertragung zakVereinbarung = getOrCreateZakUebertragungForDerivatAnforderungModul(derivatAnforderungModul, currentUser);
                    nachZakList.add(zakVereinbarung);
                }
            }
        }

        Map<Derivat, List<ZakUebertragung>> report = createReportForZakUebertragung(nachZakList);
        return report;
    }

    private ZakUebertragung getOrCreateZakUebertragungForDerivatAnforderungModul(DerivatAnforderungModul derivatAnforderungModul, Mitarbeiter mitarbeiter) {
        ZakUebertragung zakVereinbarung = getZakUebertragungByDerivatAnforderungModul(derivatAnforderungModul);

        if (zakVereinbarung == null) {
            zakVereinbarung = createZakUebertragungenForDerivatAnforderungModul(derivatAnforderungModul);
            updateDerivatAnforderungModulNachZakVereinbarung(derivatAnforderungModul, zakVereinbarung, mitarbeiter);

        } else {
            logService.logWithLogEntry(LogLevel.SEVERE, SystemType.ZAKSST, "ZakUebetragung for "
                    + derivatAnforderungModul.toString() + " already exists. Zuordnung Object will be modified to represent this state.", ZakVereinbarungService.class.getName());
        }

        return zakVereinbarung;
    }

    private void updateDerivatAnforderungModulNachZakVereinbarung(DerivatAnforderungModul derivatAnforderungModul, ZakUebertragung zakVereinbarung, Mitarbeiter mitarbeiter) {
        derivatAnforderungModul.setVereinbarung(zakVereinbarung);
        derivatAnforderungModul.setAnforderer(mitarbeiter);
        derivatAnforderungModul.setZakStatus(ZakStatus.PENDING);
        derivatAnforderungModulService.persistDerivatAnforderungModul(derivatAnforderungModul);
        String kommentar = derivatAnforderungModul.toString() + " nach ZAK übertragen: " + derivatAnforderungModul.getModul().getName();
        derivatAnforderungModulService.updateDerivatAnforderungModulStatus(derivatAnforderungModul.getStatusZV(), kommentar, derivatAnforderungModul, mitarbeiter);
    }

    private Map<Derivat, List<ZakUebertragung>> createReportForZakUebertragung(List<ZakUebertragung> nachZakList) {
        Map<Derivat, List<ZakUebertragung>> report = new HashMap<>();
        Set<Derivat> derivate = new HashSet<>();

        if (!nachZakList.isEmpty()) {
            updateZuordnungAnforderungDerivatAfterZakUebertragung(nachZakList);
            derivate = nachZakList.stream().map(z -> z.getDerivatAnforderungModul().getDerivat()).collect(Collectors.toSet());
        }

        derivate.forEach(d -> report.put(d, nachZakList.stream()
                .filter(z -> z.getDerivatAnforderungModul().getDerivat().getId().equals(d.getId()))
                .collect(Collectors.toList())));

        return report;
    }

    /**
     * this method sets the flag nachzak to true after the zakUebertraung object
     * is created
     *
     * @param newZakUebertragungen
     */
    private void updateZuordnungAnforderungDerivatAfterZakUebertragung(List<ZakUebertragung> newZakUebertragungen) {
        if (newZakUebertragungen != null && !newZakUebertragungen.isEmpty()) {
            newZakUebertragungen.stream()
                    .map(zakUebertragung -> zakUebertragung.getDerivatAnforderungModul().getZuordnungAnforderungDerivat())
                    .distinct().forEach(zuordnung -> {
                updateZuordnungAfterZakUebertragung(zuordnung);
            });
        }
    }

    private void updateZuordnungAfterZakUebertragung(ZuordnungAnforderungDerivat zuordnung) {
        List<ZakUebertragung> zakUebertragungenForDerivatAnforderung = getZakUebertragungByAnforderungDerivatList(zuordnung.getDerivat(), zuordnung.getAnforderung());
        List<AnforderungFreigabeBeiModulSeTeam> anfoModulSeTeam = zuordnung.getAnforderung().getAnfoFreigabeBeiModul().stream().filter(f -> !f.getVereinbarungType().isZak()).collect(Collectors.toList());

        Set<Modul> standardModuls = anfoModulSeTeam.stream().map(m -> m.getModulSeTeam().getElternModul()).collect(Collectors.toSet());
        Set<Modul> allModuls = zuordnung.getAnforderung().getUmsetzer().stream().map(u -> u.getModul()).collect(Collectors.toSet());

        boolean allModulsAreStandard = (standardModuls.size() == allModuls.size());
        boolean allZakModulsAreNachZak = (zakUebertragungenForDerivatAnforderung.size() + standardModuls.size() >= allModuls.size());

        if (!allModulsAreStandard && allZakModulsAreNachZak) {
            boolean isZakUebertragungFehlerhaft = zakUebertragungenForDerivatAnforderung.stream()
                    .anyMatch(zak -> ZakStatus.UEBERTRAGUNG_FEHLERHAFT.equals(zak.getZakStatus()));
            zuordnungAnforderungDerivatService.setDerivatAnforderungAsNachZakUebertragen(zuordnung, isZakUebertragungFehlerhaft);
        }
    }

    private ZakUebertragung simulateNachZakUebertragung(ZakUebertragung zakUebertragung) {
        // choose random status
        List<ZakStatus> possibleStatusList = ZakStatus.getAllStatusList();
        possibleStatusList.remove(ZakStatus.EMPTY);
        Integer i = Math.toIntExact(Math.round(Math.random() * 5));
        zakUebertragung.setZakStatus(possibleStatusList.get(i));
        Date now = new Date();
        zakUebertragung.setErstellungsdatum(now);
        zakUebertragung.setAenderungsdatum(now);
        return zakUebertragung;
    }

    public Long generateZakUebertragungId(Anforderung anforderung, Modul modul) {
        return Long.parseLong(anforderung.getId().toString() + "000" + modul.getId().toString());
    }

    private ZakUebertragung createZakUebertragungenForDerivatAnforderungModul(DerivatAnforderungModul derivatAnforderungModul) {
        ZakUebertragung zakUebertragung = new ZakUebertragung(derivatAnforderungModul);
        Long maxZakUebertragungId = getMaxZakUebertragungIdByAnforderungModul(derivatAnforderungModul.getAnforderung(), derivatAnforderungModul.getModul());
        if (maxZakUebertragungId == null) {
            zakUebertragung.setUebertragungId(generateZakUebertragungId(zakUebertragung.getDerivatAnforderungModul().getAnforderung(), zakUebertragung.getDerivatAnforderungModul().getModul()));
        } else {
            zakUebertragung.setUebertragungId(maxZakUebertragungId);
        }

        if (ZAKSIMULATION) {
            zakUebertragung = simulateNachZakUebertragung(zakUebertragung);
        }

        persistZakUebertragung(zakUebertragung);
        return zakUebertragung;
    }

    public void persistZakUebertragung(ZakUebertragung zakUebertragung) {
        zakUebertragungDao.persistZakUebertragung(zakUebertragung);
    }

    public ZakUebertragung getZakUebertragungByDerivatAnforderungModul(DerivatAnforderungModul derivatAnforderungModul) {
        List<ZakUebertragung> result = zakUebertragungDao.getZakUebertragungByDerivatAnforderungModul(derivatAnforderungModul);
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public Boolean existsAnforderungModulInZak(Anforderung anforderung, Modul modul) {
        List<ZakStatus> statusList = new ArrayList<>();
        statusList.add(ZakStatus.DONE);
        statusList.add(ZakStatus.BESTAETIGT_AUS_VORPROZESS);
        statusList.add(ZakStatus.PENDING);
        statusList.add(ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER);
        statusList.add(ZakStatus.NICHT_BEWERTBAR);
        statusList.add(ZakStatus.NICHT_UMSETZBAR);
        Long found = zakUebertragungDao.getCountByAnforderungAndModulAndStatusList(anforderung, modul, statusList);
        LOG.info("FOUND UEBERTRAGUNGEN NACH ZAK von Anforderung " + anforderung.getFachId() + " und Modul " + modul.getName() + ": " + found);
        return found > 0;
    }

    public List<ZakUebertragung> getZakUebertragungByDerivatAnforderungModulList(List<DerivatAnforderungModul> derivatAnforderungModulList) {
        return zakUebertragungDao.getZakUebertragungByDerivatAnforderungModulList(derivatAnforderungModulList);
    }

    public List<ZakUebertragung> getZakUebertragungenByStatus(ZakStatus status) {
        return zakUebertragungDao.getZakUebertragungByStatus(status);
    }

    public List<ZakUebertragung> getZakUebertragungByAnforderungDerivatList(Derivat derivat, Anforderung anforderung) {
        List<Derivat> derivatList = new ArrayList<>();
        derivatList.add(derivat);
        return zakUebertragungDao.getZakUebertragungByAnforderungDerivatList(derivatList, anforderung);
    }

    public Long getMaxZakUebertragungIdByAnforderungModul(Anforderung anforderung, Modul modul) {
        return zakUebertragungDao.getMaxZakUebertragungIdByAnforderungModul(anforderung, modul);
    }

    public String getZakIdByAnforderungModul(Anforderung anforderung, Modul modul) {
        Set<String> zakIds = zakUebertragungDao.getZakIdsByAnforderungModul(anforderung, modul);
        return zakIds.stream()
                .filter(Objects::nonNull)
                .filter(s -> s.matches("\\d+"))
                .map(Long::parseLong)
                .max(Comparator.comparingLong(l -> l))
                .map(Object::toString)
                .orElse(null);
    }

    public ZakUebertragung getZakUebertragungForAnforderungModulAfterDate(Anforderung anforderung, Modul modul, Date changeDate) {
        return zakUebertragungDao.getZakUebertragungForAnforderungModulAfterDate(anforderung, modul, changeDate);
    }

    @Deprecated
    public List<ZakUebertragung> getZakUebertragungForAnforderungModulIds(List<String> anforderungModulIdsAsString) {
        List<Long> anforderungIds = new ArrayList<>();
        List<Long> modulIds = new ArrayList<>();

        if (anforderungModulIdsAsString == null) {
            LOG.warn("anforderungModulIdsAsString is null");
            return new ArrayList<>();
        }

        anforderungModulIdsAsString.stream()
                .filter(s -> s.matches("[a]\\d+[m]\\d+"))
                .map(s -> s.split("m"))
                .map(splitArray -> {
                    String anforderungIdAsString = splitArray[0].substring(1);
                    if (anforderungIdAsString.matches("\\d+")) {
                        anforderungIds.add(Long.parseLong(anforderungIdAsString));
                    }
                    return splitArray[1];
                })
                .forEach(modulIdAsString -> {
                    if (modulIdAsString.matches("\\d+")) {
                        modulIds.add(Long.parseLong(modulIdAsString));
                    }
                });

        return zakUebertragungDao.getZakUebertragungForAnforderungModulIds(anforderungIds, modulIds);
    }

    public Optional<ZakUebertragung> getZakUebertragungByUebertragungIdDerivatId(Long uebertragungId, Long derivatId) {
        return zakUebertragungDao.getZakUebertragungByUebertragungIdDerivatId(uebertragungId, derivatId);
    }

}
