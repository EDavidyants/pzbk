package de.interfaceag.bmw.pzbk.zak;

import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.enums.LogLevel;
import de.interfaceag.bmw.pzbk.enums.SystemType;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.exceptions.ZakUebertragungException;
import de.interfaceag.bmw.pzbk.services.LogService;
import de.interfaceag.bmw.pzbk.zak.rest.ZakTestRestClient;
import de.interfaceag.bmw.pzbk.zak.xml.SaveXml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author sl
 */
@Stateless
@Named
public class ZakPostService {

    private static final Logger LOG = LoggerFactory.getLogger(ZakPostService.class.getName());

    @Inject
    private ZakVereinbarungService zakVereinbarungService;
    @Inject
    private XmlService xmlService;
    @Inject
    private LogService logService;
    @Inject
    private ZakTestRestClient zakTestRestClient;

    public void sendQueuedZakUebertragungen() {
        Set<ZakUebertragung> relevantZakUebertragungen = getRelevantZakUebertragungen();
        sendZakUebertragungenToZak(relevantZakUebertragungen);
    }

    private Set<ZakUebertragung> getRelevantZakUebertragungen() {
        Set<ZakUebertragung> queuedZakUebertragugen = new HashSet<>();
        queuedZakUebertragugen.addAll(zakVereinbarungService.getZakUebertragungenByStatus(ZakStatus.EMPTY));
        queuedZakUebertragugen.addAll(zakVereinbarungService.getZakUebertragungenByStatus(ZakStatus.UEBERTRAGUNG_FEHLERHAFT));
        return queuedZakUebertragugen;
    }

    private void sendZakUebertragungenToZak(Collection<ZakUebertragung> queuedZakUebertragugen) {
        Integer numberOfPosts = queuedZakUebertragugen.size();
        LOG.debug("{} offene ZAK Uebertragungen vorhanden. Starte POST ...", numberOfPosts);

        Iterator<ZakUebertragung> iterator = queuedZakUebertragugen.iterator();
        while (iterator.hasNext()) {
            ZakUebertragung next = iterator.next();
            sendZakUebertragung(next);
        }

        writeReport(numberOfPosts);
    }

    private void writeReport(Integer numberOfPosts) {
        Integer relevantZakUebertragungenAfterPost = getRelevantZakUebertragungen().size();
        String message = "POST abgeschlossen. " + Integer.toString(relevantZakUebertragungenAfterPost) + " von "
                + Integer.toString(numberOfPosts) + " Übertragungen waren nicht erfolgreich.";
        logService.addLogEntry(LogLevel.INFO, SystemType.ZAKSST, message);
        LOG.debug(message);
    }

    private void sendZakUebertragung(ZakUebertragung zakUebertragung) {
        SaveXml xml = generateXmlForZakUebertragung(zakUebertragung);
        if (xml != null) {
            zakTestRestClient.sendToZakTestRestApi(xml);
            String response = postXmlToZak(xml);
            handleZakResponse(zakUebertragung, response);
        } else {
            logXmlIsNullMessage(zakUebertragung);
        }
    }

    private void handleZakResponse(ZakUebertragung zakUebertragung, String response) {
        if (isUebertragungSuccessful(response)) {
            updateZakUebertragungStatusToPending(zakUebertragung);
        } else {
            updateZakUebertragungStatusToUebertragungFehlerhaft(zakUebertragung);
        }
        saveChanges(zakUebertragung, response);
    }

    private void saveChanges(ZakUebertragung zakUebertragung, String response) {
        updateZakUebertragungResponse(zakUebertragung, response);
        zakUebertragung.setAenderungsdatum(new Date());
        zakVereinbarungService.persistZakUebertragung(zakUebertragung);
    }

    private void updateZakUebertragungStatusToUebertragungFehlerhaft(ZakUebertragung zakUebertragung) {
        logPostNotSucessfulMessage(zakUebertragung);
        zakUebertragung.setZakStatus(ZakStatus.UEBERTRAGUNG_FEHLERHAFT);
        zakUebertragung.getDerivatAnforderungModul().setZakStatus(ZakStatus.UEBERTRAGUNG_FEHLERHAFT);
        zakUebertragung.getDerivatAnforderungModul().getZuordnungAnforderungDerivat().setZakUebertragungErfolgreich(Boolean.FALSE);
    }

    private void updateZakUebertragungStatusToPending(ZakUebertragung zakUebertragung) {
        zakUebertragung.getDerivatAnforderungModul().getZuordnungAnforderungDerivat().setZakUebertragungErfolgreich(Boolean.TRUE);
        zakUebertragung.setZakStatus(ZakStatus.PENDING);
        zakUebertragung.getDerivatAnforderungModul().setZakStatus(ZakStatus.PENDING);
    }

    private boolean isUebertragungSuccessful(String response) {
        return xmlService.processResponseContent(response);
    }

    private void updateZakUebertragungResponse(ZakUebertragung zakUebertragung, String response) {
        zakUebertragung.setZakResponse(buildZakResponseString(response, zakUebertragung.getZakResponse()));
    }

    private String postXmlToZak(SaveXml xml) {
        String response = xmlService.postZakUbertragungen(xml);
        return response;
    }

    private void logPostNotSucessfulMessage(ZakUebertragung zakUebertragung) {
        String message = "Post to ZAK for " + zakUebertragung.toString() + " was not successful!";
        LOG.warn(message);
        logService.addLogEntry(LogLevel.WARNING, SystemType.ZAKSST, message);
    }

    private void logXmlIsNullMessage(ZakUebertragung zakUebertragung) {
        String message = "XML für " + zakUebertragung.toString() + " konnte nicht erstellt werden.";
        logService.addLogEntry(LogLevel.WARNING, SystemType.ZAKSST, message);
        LOG.warn(message);
    }

    private SaveXml generateXmlForZakUebertragung(ZakUebertragung zakUebertragung) {
        SaveXml xml = null;
        try {
            xml = xmlService.generateXmlForZakUebertragung(zakUebertragung);
        } catch (ZakUebertragungException exception) {
            LOG.warn(exception.toString());
        }
        return xml;
    }

    private String buildZakResponseString(String response, String currentZakResponse) {
        if (currentZakResponse == null) {
            currentZakResponse = "";
        }

        StringBuilder sb = new StringBuilder(currentZakResponse);

        DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
        String dateAsString = dateFormat.format(new Date());

        sb.append(dateAsString).append(": ").append(response).append("\n");
        return sb.toString();
    }

}
