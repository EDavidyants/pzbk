package de.interfaceag.bmw.pzbk.zak;

import de.interfaceag.bmw.pzbk.entities.Derivat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Entity
@NamedQueries({
    @NamedQuery(name = ZakUpdate.GET_BY_DERIVAT, query = "SELECT zu FROM ZakUpdate zu WHERE zu.derivat = :derivat")})
public class ZakUpdate implements Serializable {

    public static final String CLASSNAME = "ZakUpdate.";
    public static final String GET_BY_DERIVAT = CLASSNAME + "getByDerivat";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "zakupdate_id_seq",
            sequenceName = "zakupdate_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "zakupdate_id_seq")
    private Long id;

    @ManyToOne
    private Derivat derivat;

    @Column(length = 1000)
    private String zakUrl;

    @Temporal(TIMESTAMP)
    private Date updatedFrom;

    @Column(length = 1000)
    private String status;

    // zip file
    private byte[] zakResponse;

    // ------------  constructors ----------------------------------------------
    public ZakUpdate() {
    }

    public ZakUpdate(Derivat derivat, String zakUrl, Date updatedFrom, String status, byte[] zakResponse) {
        this.derivat = derivat;
        this.zakUrl = zakUrl;
        this.updatedFrom = updatedFrom;
        this.status = status;
        this.zakResponse = zakResponse;
    }

    // ------------  getter / setter  ------------------------------------------
    public Long getId() {
        return id;
    }

    public Derivat getDerivat() {
        return derivat;
    }

    public String getZakUrl() {
        return zakUrl;
    }

    public Date getUpdatedFrom() {
        return updatedFrom;
    }

    public String getStatus() {
        return status;
    }

    public byte[] getZakResponse() {
        return zakResponse;
    }

    public void setZakResponse(byte[] zakResponse) {
        this.zakResponse = zakResponse;
    }

}
