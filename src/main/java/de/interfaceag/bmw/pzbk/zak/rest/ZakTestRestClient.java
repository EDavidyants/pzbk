package de.interfaceag.bmw.pzbk.zak.rest;

import de.interfaceag.bmw.pzbk.config.PropertiesService;
import de.interfaceag.bmw.pzbk.zak.xml.SaveXml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author sl
 */
@Stateless
@Named
public class ZakTestRestClient {

    @Inject
    private PropertiesService propertiesService;

    private static final String PATH = "zak/api";

    private static final Logger LOG = LoggerFactory.getLogger(ZakTestRestClient.class.getName());

    public void sendToZakTestRestApi(SaveXml xml) {
        WebTarget target = buildTarget();
        if (restApiIsOnline(target)) {
            postXmlToRestApi(xml, target);
        } else {
            LOG.info("API is offline");
        }
    }

    private void postXmlToRestApi(SaveXml xml, WebTarget target) {
        Response response = target.request(MediaType.APPLICATION_XML).post(Entity.xml(xml));
        LOG.info(response.toString());
    }

    private boolean restApiIsOnline(WebTarget target) {
        Response response = target.request().get();
        Response.StatusType statusInfo = response.getStatusInfo();
        int statusCode = statusInfo.getStatusCode();
        boolean result = statusCode == 200;
        if (!result) {
            LOG.warn("API status code:{}", statusCode);
        }
        return result;
    }

    private WebTarget buildTarget() {
        Client client = ClientBuilder.newClient();
        String url = getUrl();
        Link link = Link.fromUri(url).build();
        WebTarget target = client.target(link);
        return target;
    }

    private String getUrl() {
        String basePath = propertiesService.getZakRestBasePath();
        return basePath + PATH;
    }

}
