package de.interfaceag.bmw.pzbk.comparator;

import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FestgestelltInComparator implements Comparator<FestgestelltIn>, Serializable {

    @Override
    public int compare(FestgestelltIn festgestelltIn1, FestgestelltIn festgestelltIn2) {
        String wert1 = festgestelltIn1.getWert();
        String wert2 = festgestelltIn2.getWert();
        return wert1.compareTo(wert2);
    }

}
