package de.interfaceag.bmw.pzbk.comparator;

import de.interfaceag.bmw.pzbk.entities.Anforderung;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AnforderungFachIdComparator implements Comparator<Anforderung>, Serializable {

    @Override
    public int compare(Anforderung anforderung1, Anforderung anforderung2) {
        String fachId1 = anforderung1.getFachId();
        String fachId2 = anforderung2.getFachId();

        FachIdComparator fachIdComparator = new FachIdComparator();
        return fachIdComparator.compare(fachId1, fachId2);
    }

}
