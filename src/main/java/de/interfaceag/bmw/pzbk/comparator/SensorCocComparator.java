package de.interfaceag.bmw.pzbk.comparator;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class SensorCocComparator implements Comparator<SensorCoc>, Serializable {

    @Override
    public int compare(SensorCoc sensorCoc1, SensorCoc sensorCoc2) {
        String sensorCocBezeichnung1 = sensorCoc1.toString();
        String sensorCocBezeichnung2 = sensorCoc2.toString();
        return sensorCocBezeichnung1.compareTo(sensorCocBezeichnung2);
    }

}
