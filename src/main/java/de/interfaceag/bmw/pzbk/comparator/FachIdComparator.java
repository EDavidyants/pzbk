package de.interfaceag.bmw.pzbk.comparator;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachIdComparator implements Comparator<String>, Serializable {

    @Override
    public int compare(String fachId1, String fachId2) {

        Integer length1 = fachId1.length();
        Integer length2 = fachId2.length();

        if (Objects.equals(length1, length2)) {
            if (length1 > 1 && length2 > 1) {
                Long fachIdValue1 = Long.parseLong(fachId1.substring(1));
                Long fachIdValue2 = Long.parseLong(fachId2.substring(1));
                return fachIdValue1.compareTo(fachIdValue2);
            }
            return 0;
        } else {
            return length1.compareTo(length2);
        }
    }

}
