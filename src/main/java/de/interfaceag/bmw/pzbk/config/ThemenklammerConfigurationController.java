package de.interfaceag.bmw.pzbk.config;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class ThemenklammerConfigurationController implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ThemenklammerConfigurationController.class);

    @Inject
    private ThemenklammerConfigurationFacade facade;

    @Inject
    private List<ThemenklammerDto> themenklammern;

    public List<ThemenklammerDto> getThemenklammern() {
        return themenklammern;
    }

    public String saveThemenklammern() {
        LOG.debug("Save Themenklammern {}", themenklammern);
        facade.saveThemenklammern(themenklammern);
        return Page.KONFIGURATION.getUrl() + "?faces-redirect=true";
    }

    public void addNewThemenklammer() {
        ThemenklammerDto newThemenklammer = new ThemenklammerDto();
        themenklammern.add(newThemenklammer);
    }

}
