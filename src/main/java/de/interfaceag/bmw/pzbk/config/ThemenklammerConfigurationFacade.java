package de.interfaceag.bmw.pzbk.config;

import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Stateless
public class ThemenklammerConfigurationFacade implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(ThemenklammerConfigurationFacade.class);

    @Inject
    private ThemenklammerService themenklammerService;

    public void saveThemenklammern(Collection<ThemenklammerDto> themenklammerDtos) {
        if (Objects.isNull(themenklammerDtos)) {
            LOG.error("themenklammerDtos is null!");
        } else {
            themenklammerService.saveDtos(themenklammerDtos);
        }
    }

}
