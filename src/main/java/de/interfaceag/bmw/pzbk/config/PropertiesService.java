package de.interfaceag.bmw.pzbk.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Named;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author sl
 */
@Stateless
@Named
public class PropertiesService {

    private static final Logger LOG = LoggerFactory.getLogger(PropertiesService.class);

    private Properties settings;

    @PostConstruct
    public void init() {
        initProperties();
    }

    private void initProperties() {
        try (InputStream stream = PropertiesService.class.getResourceAsStream("/de/interfaceag/settings.properties")) {
            settings = new Properties();
            settings.load(stream);
        } catch (IOException ex) {
            LOG.error(null, ex);
        }
    }

    public static Properties getTimerSettings() {
        Properties props = new Properties();
        try {
            props.load(de.interfaceag.bmw.pzbk.config.PropertiesService.class.getClassLoader().getResourceAsStream("de/interfaceag/timer_settings.properties"));
        } catch (IOException ex) {
            LOG.error(null, ex);
        }
        return props;
    }

    public String getZakRestBasePath() {
        String result = settings.getProperty("zak.rest.basepath");
        if (result == null || result.isEmpty()) {
            return getZakRestFallback();
        }
        return result;
    }

    private String getZakRestFallback() {
        LOG.warn("System Property 'com.bmw.pzbk.zak.rest.basepath' not found. Use fallback option!");
        return "https://t-am.bmwgroup.net/PZBK/";
    }

}
