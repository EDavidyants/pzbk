package de.interfaceag.bmw.pzbk.startup;

import de.interfaceag.bmw.pzbk.excel.ExcelUtils;
import de.interfaceag.bmw.pzbk.exceptions.ExcelInputFileException;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.services.UserService;
import de.interfaceag.bmw.pzbk.services.WerkService;
import de.interfaceag.bmw.pzbk.shared.mail.MailTemplatesService;
import de.interfaceag.bmw.pzbk.testdata.TestdataService;
import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Christian Schauer
 */
@Singleton
@Startup
public class StartupService implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(StartupService.class);

    @Inject
    private UserService userService;
    @Inject
    private UserSearchService userSearchService;
    @Inject
    private ConfigService configService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private DbSchemaMigrationService dbSchemaMigrationService;
    @Inject
    private WerkService werkService;
    @Inject
    private TestdataService testdataService;
    @Inject
    private MailTemplatesService mailTemplatesService;

    @PostConstruct
    public void init() {
        LOGGER.debug("################# StartupService start #######################");
        dbSchemaMigrationService.launchMigrations();

        if (Boolean.TRUE.equals(configService.isDevelopmentEnvironment())) {
            LOGGER.debug("---------- Development Modus");
            berechtigungService.setStartup(true);

            if (userSearchService.getAllMitarbeiter().isEmpty()) {
                userService.createAndPersistDummyUsers();
            }

            if (werkService.getAllWerk().isEmpty()) {
                werkService.createAndPersistDummyWerk();
            }

            mailTemplatesService.initDefaultMailTemplates();

            try {
                XSSFWorkbook wb = ExcelUtils.createXSSFWorkbookForFile(ConfigService.getExcelConfigFile());
                configService.insertFromExcel(wb);
            } catch (ExcelInputFileException ex) {
                LOGGER.error(ex.getMessage());
            }

            LOGGER.debug("---------- Initialisiere Berechtigungen");
            initBerechtigungen();

            LOGGER.debug("------Erzeuge Test Meldungen-Anforderungen-----");
            testdataService.generateTestdata();

            berechtigungService.setStartup(false);
            LOGGER.debug("################# StartupService end #######################");
        }
    }

    private void initBerechtigungen() {
        try (XSSFWorkbook wb = ExcelUtils.createXSSFWorkbookForFile(ConfigService.getExcelConfigFile())) {
            Sheet sheet = wb.getSheet("Berechtigungen");
            berechtigungService.loadBerechtigungenFromExcelSheet(sheet);
        } catch (ExcelInputFileException | IOException ex) {
            LOGGER.error(null, ex);
        }
    }
}
