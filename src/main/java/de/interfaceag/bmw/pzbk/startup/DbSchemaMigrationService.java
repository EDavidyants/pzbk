package de.interfaceag.bmw.pzbk.startup;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.flywaydb.core.api.MigrationInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;
import java.io.Serializable;

/**
 * @author sl
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DbSchemaMigrationService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(DbSchemaMigrationService.class.getName());

    @Resource(lookup = "jdbc/pzbk")
    private transient DataSource dataSource;

    public void launchMigrations() {
        LOG.info("---------- starting migration");
        if (dataSource == null) {
            LOG.error("Could not find JDBC resource.");
            return;
        }

        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);

        try {
            flyway.setBaselineOnMigrate(true);
            flyway.migrate();
        } catch (FlywayException exception) {
            LOG.error(exception.getMessage(), "Migration failed. Repairing the Flyway metadata table.");
            flyway.repair();
            LOG.info("Repair of metadata table successful.");
            flyway.setBaselineOnMigrate(true);
            flyway.migrate();
        }

        for (MigrationInfo info : flyway.info().all()) {
            LOG.info("{}", info);
        }
    }
}
