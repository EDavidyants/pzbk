package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.MitarbeiterDTO;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fn
 */
@Stateless
@Named
public class BerechtigungViewFacade implements Serializable {

    @Inject
    private BerechtigungSearchService berechtigungSearchService;
    @Inject
    private UserSearchService userSearchService;
    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private ModulService modulService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private TteamService tteamService;
    @Inject
    private Session session;

    public BerechtigungViewData getViewData(UrlParameter urlParameter) {

        BerechtigungViewFilter viewFilter = getViewFilter(urlParameter);

        List<MitarbeiterDTO> mitarbeiter;

        mitarbeiter = getMitarbeiterByFilterList(viewFilter);

        return new BerechtigungViewData(mitarbeiter, viewFilter);
    }

    private List<MitarbeiterDTO> getMitarbeiterByFilterList(BerechtigungViewFilter viewFilter) {

        return berechtigungSearchService.getSeachResult(viewFilter.getVornameFilter(),
                viewFilter.getNachnameFilter(), viewFilter.getRolleFilter(),
                viewFilter.getAbteilungFilter(), viewFilter.getSensorCocFilterLesend(),
                viewFilter.getSensorCocFilterSchreibend(), viewFilter.getModulSeTeamFilter(),
                viewFilter.getDerivatFilter(), viewFilter.gettTeamFilter(), viewFilter.getZakEinordnungFilter(),
                viewFilter.getNurMitBerechtigungFilter());
    }

    private BerechtigungViewFilter getViewFilter(UrlParameter urlParameter) {

        List<String> allAbteilung = userSearchService.getAllAbteilung();
        List<SensorCoc> allSensorCoc = sensorCocService.getAllSortByTechnologie();
        List<ModulSeTeam> allModulSeTeam = modulService.getAllSeTeams();
        List<Derivat> allDerivat = derivatService.getAllDerivate();
        List<Tteam> allTteams = tteamService.getAllTteams();
        List<String> allZakEinordung = sensorCocService.getAllZakEinordnung();

        List<Rolle> allRolesForUser = getRolesForUser();

        return new BerechtigungViewFilter(urlParameter, allAbteilung,
                allSensorCoc, allModulSeTeam, allDerivat, allTteams, allZakEinordung, allRolesForUser);
    }

    private List<Rolle> getRolesForUser() {
        List<Rolle> returnList = new ArrayList<>();
        if (session.hasRole(Rolle.ADMIN)) {
            returnList.addAll(Rolle.getAllRolles());
            return returnList;
        }
        if (session.hasRole(Rolle.SENSORCOCLEITER)) {
            returnList.add(Rolle.SENSOR);
            returnList.add(Rolle.SCL_VERTRETER);
            returnList.add(Rolle.SENSOR_EINGESCHRAENKT);
            returnList.add(Rolle.UMSETZUNGSBESTAETIGER);
        }
        if (session.hasRole(Rolle.T_TEAMLEITER)) {
            returnList.add(Rolle.TTEAMMITGLIED);
            returnList.add(Rolle.TTEAM_VERTRETER);
            returnList.add(Rolle.T_TEAMLEITER);
        }
        return returnList;
    }
}
