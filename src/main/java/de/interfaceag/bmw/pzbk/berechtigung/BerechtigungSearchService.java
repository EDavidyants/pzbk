package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.MitarbeiterDTO;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.rollen.TTeamMitglied;
import de.interfaceag.bmw.pzbk.entities.rollen.TteamVertreter;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.filter.AbteilungFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.NurMitBerechtigungFilter;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ZakEinordnungFilter;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author fn
 */
@Stateless
public class BerechtigungSearchService implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private Session session;

    public List<MitarbeiterDTO> getSeachResult(TextSearchFilter vornameFilter, TextSearchFilter nachnameFilter,
            MultiValueEnumSearchFilter<Rolle> rolleFilter, AbteilungFilter abteilungFilter,
            IdSearchFilter sensorCocLeseFilter, IdSearchFilter sensorCocSchreibFilter,
            IdSearchFilter modulSeTeamFilter, IdSearchFilter derivatFilter, IdSearchFilter tteamFilter,
            ZakEinordnungFilter zakEinordungFilter, NurMitBerechtigungFilter nurMitBerechtigungFilter) {

        Set<Mitarbeiter> resultMitarbeiter = new HashSet<>();
        Set<Mitarbeiter> existingMitarbeiter;

        existingMitarbeiter = getExistingMitarbeiter(vornameFilter, nachnameFilter, abteilungFilter);
        if (!existingMitarbeiter.isEmpty()) {

            if (nurMitBerechtigungFilter.getValue()) {
                existingMitarbeiter = getExistingMitarbeiterMitBerechtigung(existingMitarbeiter);

            }

            Set<Mitarbeiter> existingMitarbeiterWithRolle = getBerechtigungForExistingMitarrbeiter(existingMitarbeiter, rolleFilter,
                    sensorCocLeseFilter, sensorCocSchreibFilter, modulSeTeamFilter, derivatFilter, tteamFilter, zakEinordungFilter);

            resultMitarbeiter.addAll(existingMitarbeiterWithRolle);
        }
        return convertToMitarbeiterDto(resultMitarbeiter);

    }

    private Set<Mitarbeiter> getExistingMitarbeiterMitBerechtigung(Set<Mitarbeiter> existingMitarbeiter) {
        Set<Mitarbeiter> returnSet;
        QueryPartDTO qp = BerechtigungSearchUtils.getNurBerechtigteQery(existingMitarbeiter);

        List<Rolle> filterList = getRolesForUser();
        if (!filterList.isEmpty()) {
            BerechtigungSearchUtils.getMitarbeiterWithRole(qp, filterList);
        }

        Query q = buildQuery(qp, "");
        returnSet = new HashSet(q.getResultList());
        returnSet.addAll(getBerechtigteMitarbeiterForTteamMitglied(existingMitarbeiter));
        returnSet.addAll(getBerechtigteMitarbeiterForTteamVertreter(existingMitarbeiter));
        return returnSet;
    }

    private List<MitarbeiterDTO> convertToMitarbeiterDto(Set<Mitarbeiter> mitarbeiterList) {
        List<MitarbeiterDTO> dtoReturnList = new ArrayList<>();

        mitarbeiterList.forEach((mitarbeiter) -> {
            dtoReturnList.add(new MitarbeiterDTO(mitarbeiter.getId(), mitarbeiter.getQNumber(), mitarbeiter.getVorname(), mitarbeiter.getNachname(), mitarbeiter.getAbteilung(), berechtigungService.getRolesForUser(mitarbeiter)));
        });

        return dtoReturnList;
    }

    private Set<Mitarbeiter> getExistingMitarbeiter(TextSearchFilter vornameFilter, TextSearchFilter nachnameFilter,
            AbteilungFilter abteilungFilter) {

        QueryPartDTO qp = BerechtigungSearchUtils.getExistingMitarbeiterBaseQuery();

        if (vornameFilter.isActive()) {
            BerechtigungSearchUtils.getVornameQuery(qp, vornameFilter);
        }
        if (nachnameFilter.isActive()) {
            BerechtigungSearchUtils.getNachnameQuery(qp, nachnameFilter);
        }
        if (abteilungFilter.isActive()) {
            BerechtigungSearchUtils.getAbteilungQuery(qp, abteilungFilter);
        }

        BerechtigungSearchUtils.getSortQueryPart(qp);

        Query q = buildQuery(qp, "");
        return new HashSet<>(q.getResultList());
    }

    private Set<Mitarbeiter> getBerechtigungForExistingMitarrbeiter(Set<Mitarbeiter> existingMitarbeiter,
            MultiValueEnumSearchFilter<Rolle> rolleFilter,
            IdSearchFilter sensorCocLeseFilter, IdSearchFilter sensorCocSchreibFilter,
            IdSearchFilter modulSeTeamFilter, IdSearchFilter derivatFilter, IdSearchFilter tteamFilter,
            ZakEinordnungFilter zakEinordungFilter) {

        Set<Mitarbeiter> returnMitarbeiterList = existingMitarbeiter;

        if (sensorCocLeseFilter.isActive() && !returnMitarbeiterList.isEmpty()) {
            returnMitarbeiterList = getMitarbeiterSensorCocLesend(returnMitarbeiterList, rolleFilter, sensorCocLeseFilter);

        }
        if (sensorCocSchreibFilter.isActive() && !returnMitarbeiterList.isEmpty()) {
            returnMitarbeiterList = getMitarbeiterSensorCocSchreibend(returnMitarbeiterList, rolleFilter, sensorCocSchreibFilter);
        }
        if (modulSeTeamFilter.isActive() && !returnMitarbeiterList.isEmpty()) {
            returnMitarbeiterList = getMitarbeiterModulSeTeam(returnMitarbeiterList, rolleFilter, modulSeTeamFilter);
        }
        if (derivatFilter.isActive() && !returnMitarbeiterList.isEmpty()) {
            returnMitarbeiterList = getMitarbeiterWithDerivat(returnMitarbeiterList, rolleFilter, derivatFilter);
        }
        if (tteamFilter.isActive() && !returnMitarbeiterList.isEmpty()) {
            returnMitarbeiterList = getMitarbeiterWithTteam(returnMitarbeiterList, rolleFilter, tteamFilter);
        }
        if (zakEinordungFilter.isActive() && !returnMitarbeiterList.isEmpty()) {
            returnMitarbeiterList = getMitarbeiterWithZakEinordnung(returnMitarbeiterList, rolleFilter, zakEinordungFilter);
        }
        if (isAtLeastOneRemainingFilterSet(rolleFilter, sensorCocLeseFilter, sensorCocSchreibFilter, modulSeTeamFilter, derivatFilter, tteamFilter, zakEinordungFilter, returnMitarbeiterList)) {
            returnMitarbeiterList = getMitarbeiterForRole(returnMitarbeiterList, rolleFilter);
        }

        return returnMitarbeiterList;
    }

    @SuppressWarnings("checkstyle:BooleanExpressionComplexity")
    private boolean isAtLeastOneRemainingFilterSet(MultiValueEnumSearchFilter<Rolle> rolleFilter, IdSearchFilter sensorCocLeseFilter, IdSearchFilter sensorCocSchreibFilter, IdSearchFilter modulSeTeamFilter, IdSearchFilter derivatFilter, IdSearchFilter tteamFilter, ZakEinordnungFilter zakEinordungFilter, Set<Mitarbeiter> returnMitarbeiterList) {
        return !sensorCocLeseFilter.isActive() && !sensorCocSchreibFilter.isActive()
                && !modulSeTeamFilter.isActive() && !derivatFilter.isActive()
                && !tteamFilter.isActive() && !zakEinordungFilter.isActive()
                && rolleFilter.isActive() && !returnMitarbeiterList.isEmpty();
    }

    private Set<Mitarbeiter> getMitarbeiterForRole(Set<Mitarbeiter> returnMitarbeiterList, MultiValueEnumSearchFilter<Rolle> rolleFilter) {
        QueryPartDTO qp = BerechtigungSearchUtils.getMitarbeiterWhitBerechtigungZielBaseQery(returnMitarbeiterList);
        BerechtigungSearchUtils.getMitarbeiterWithRole(qp, rolleFilter);
        BerechtigungSearchUtils.getSortQueryPartForBerechtigung(qp);
        Query q = buildQuery(qp, "");
        returnMitarbeiterList = new HashSet<>(q.getResultList());
        returnMitarbeiterList.addAll(getMitarbeiterForTteamMitgliedVertreterRoleFilter(rolleFilter));
        return returnMitarbeiterList;
    }

    private Set<Mitarbeiter> getMitarbeiterWithZakEinordnung(Set<Mitarbeiter> returnMitarbeiterList, MultiValueEnumSearchFilter<Rolle> rolleFilter, ZakEinordnungFilter zakEinordungFilter) {
        QueryPartDTO qp = BerechtigungSearchUtils.getMitarbeiterWhitBerechtigungZielBaseQery(returnMitarbeiterList);
        if (rolleFilter.isActive()) {
            BerechtigungSearchUtils.getMitarbeiterWithRole(qp, rolleFilter);

        }
        BerechtigungSearchUtils.getMitarbeiterWhitBerechtigungZielZAKEinordnungSchreibend(qp, zakEinordungFilter);
        BerechtigungSearchUtils.getSortQueryPartForBerechtigung(qp);
        Query q = buildQuery(qp, "");
        returnMitarbeiterList = new HashSet<>(q.getResultList());
        if (rolleFilter.isActive()) {
            returnMitarbeiterList.addAll(getMitarbeiterForTteamMitgliedVertreterRoleFilter(rolleFilter));
        }
        return returnMitarbeiterList;
    }

    private Set<Mitarbeiter> getMitarbeiterWithTteam(Set<Mitarbeiter> returnMitarbeiterList, MultiValueEnumSearchFilter<Rolle> rolleFilter, IdSearchFilter tteamFilter) {
        QueryPartDTO qp = BerechtigungSearchUtils.getMitarbeiterWhitBerechtigungZielBaseQery(returnMitarbeiterList);
        if (rolleFilter.isActive()) {
            BerechtigungSearchUtils.getMitarbeiterWithRole(qp, rolleFilter);

        }
        BerechtigungSearchUtils.getMitarbeiterWhitBerechtigungZielTTeamSchreibend(qp, tteamFilter);
        BerechtigungSearchUtils.getSortQueryPartForBerechtigung(qp);
        Query q = buildQuery(qp, "");
        returnMitarbeiterList = new HashSet<>(q.getResultList());
        if (rolleFilter.isActive()) {
            returnMitarbeiterList.addAll(getMitarbeiterForTteamMitgliedVertreterRoleFilter(rolleFilter));
        }
        return returnMitarbeiterList;
    }

    private Set<Mitarbeiter> getMitarbeiterWithDerivat(Set<Mitarbeiter> returnMitarbeiterList, MultiValueEnumSearchFilter<Rolle> rolleFilter, IdSearchFilter derivatFilter) {
        QueryPartDTO qp = BerechtigungSearchUtils.getMitarbeiterWhitBerechtigungZielBaseQery(returnMitarbeiterList);
        if (rolleFilter.isActive()) {
            BerechtigungSearchUtils.getMitarbeiterWithRole(qp, rolleFilter);

        }
        BerechtigungSearchUtils.getMitarbeiterWhitBerechtigungZielDerivatSchreibend(qp, derivatFilter);
        BerechtigungSearchUtils.getSortQueryPartForBerechtigung(qp);
        Query q = buildQuery(qp, "");
        returnMitarbeiterList = new HashSet<>(q.getResultList());
        if (rolleFilter.isActive()) {
            returnMitarbeiterList.addAll(getMitarbeiterForTteamMitgliedVertreterRoleFilter(rolleFilter));
        }
        return returnMitarbeiterList;
    }

    private Set<Mitarbeiter> getMitarbeiterModulSeTeam(Set<Mitarbeiter> returnMitarbeiterList, MultiValueEnumSearchFilter<Rolle> rolleFilter, IdSearchFilter modulSeTeamFilter) {
        QueryPartDTO qp = BerechtigungSearchUtils.getMitarbeiterWhitBerechtigungZielBaseQery(returnMitarbeiterList);
        if (rolleFilter.isActive()) {
            BerechtigungSearchUtils.getMitarbeiterWithRole(qp, rolleFilter);

        }
        BerechtigungSearchUtils.getMitarbeiterWhitBerechtigungZielModulSETEamSchreibend(qp, modulSeTeamFilter);
        BerechtigungSearchUtils.getSortQueryPartForBerechtigung(qp);
        Query q = buildQuery(qp, "");
        returnMitarbeiterList = new HashSet<>(q.getResultList());
        if (rolleFilter.isActive()) {
            returnMitarbeiterList.addAll(getMitarbeiterForTteamMitgliedVertreterRoleFilter(rolleFilter));
        }
        return returnMitarbeiterList;
    }

    private Set<Mitarbeiter> getMitarbeiterSensorCocSchreibend(Set<Mitarbeiter> returnMitarbeiterList, MultiValueEnumSearchFilter<Rolle> rolleFilter, IdSearchFilter sensorCocSchreibFilter) {
        QueryPartDTO qp = BerechtigungSearchUtils.getMitarbeiterWhitBerechtigungZielBaseQery(returnMitarbeiterList);
        if (rolleFilter.isActive()) {
            BerechtigungSearchUtils.getMitarbeiterWithRole(qp, rolleFilter);

        }
        BerechtigungSearchUtils.getMitarbeiterWhitBerechtigungZielSensorCoCSchreibend(qp, sensorCocSchreibFilter);
        BerechtigungSearchUtils.getSortQueryPartForBerechtigung(qp);
        Query q = buildQuery(qp, "");
        returnMitarbeiterList = new HashSet<>(q.getResultList());
        if (rolleFilter.isActive()) {
            returnMitarbeiterList.addAll(getMitarbeiterForTteamMitgliedVertreterRoleFilter(rolleFilter));
        }
        return returnMitarbeiterList;
    }

    private Set<Mitarbeiter> getMitarbeiterSensorCocLesend(Set<Mitarbeiter> returnMitarbeiterList, MultiValueEnumSearchFilter<Rolle> rolleFilter, IdSearchFilter sensorCocLeseFilter) {
        QueryPartDTO qp = BerechtigungSearchUtils.getMitarbeiterWhitBerechtigungZielBaseQery(returnMitarbeiterList);
        if (rolleFilter.isActive()) {
            BerechtigungSearchUtils.getMitarbeiterWithRole(qp, rolleFilter);
        }
        BerechtigungSearchUtils.getMitarbeiterWhitBerechtigungZielSensorCoCLesend(qp, sensorCocLeseFilter);
        BerechtigungSearchUtils.getSortQueryPartForBerechtigung(qp);
        Query q = buildQuery(qp, "");
        returnMitarbeiterList = new HashSet<>(q.getResultList());
        if (rolleFilter.isActive()) {
            returnMitarbeiterList.addAll(getMitarbeiterForTteamMitgliedVertreterRoleFilter(rolleFilter));
        }
        return returnMitarbeiterList;
    }

    private List<Mitarbeiter> getMitarbeiterForTteamMitgliedVertreterRoleFilter(MultiValueEnumSearchFilter<Rolle> rolleFilter) {

        List<Mitarbeiter> returnMitarbeiterList = new ArrayList();
        if (rolleFilter.getAsEnumList().contains(Rolle.TTEAM_VERTRETER)) {

            List<Mitarbeiter> tteamVertreterMitarbeiter = getAllBerechtigteMitarbeiterForTteamVertreter();
            returnMitarbeiterList.addAll(tteamVertreterMitarbeiter);
        }
        if (rolleFilter.getAsEnumList().contains(Rolle.TTEAMMITGLIED)) {

            List<Mitarbeiter> tteamMitgliedMitarbeiter = getAllBerechtigteMitarbeiterForTteamMitglied();
            returnMitarbeiterList.addAll(tteamMitgliedMitarbeiter);
        }
        return returnMitarbeiterList;
    }

    private List<Mitarbeiter> getAllBerechtigteMitarbeiterForTteamVertreter() {
        QueryPartDTO tteamVertreterQueryPart = BerechtigungSearchUtils.getAllTteamVertreter();
        Query tteamVertreterQuery = buildQuery(tteamVertreterQueryPart, "TTeamVertreter");
        return tteamVertreterQuery.getResultList();
    }

    private List<Mitarbeiter> getBerechtigteMitarbeiterForTteamVertreter(Set<Mitarbeiter> existingMitarbeiter) {
        QueryPartDTO tteamVertreterQueryPart = BerechtigungSearchUtils.getTteamVertreterForMitarbeiter(existingMitarbeiter);
        Query tteamVertreterQuery = buildQuery(tteamVertreterQueryPart, "TTeamVertreter");
        return tteamVertreterQuery.getResultList();
    }


    private List<Mitarbeiter> getAllBerechtigteMitarbeiterForTteamMitglied() {
        QueryPartDTO tteamMitgliedQueryPart = BerechtigungSearchUtils.getAllTteamMitglieder();
        Query tteamMitgliedQuery = buildQuery(tteamMitgliedQueryPart, "TTeamMitglied");
        return tteamMitgliedQuery.getResultList();
    }


    private List<Mitarbeiter> getBerechtigteMitarbeiterForTteamMitglied(Set<Mitarbeiter> existingMitarbeiter) {
        QueryPartDTO tteamMitgliedQueryPart = BerechtigungSearchUtils.getTteamMitgliederforMitarbeiter(existingMitarbeiter);
        Query tteamMitgliedQuery = buildQuery(tteamMitgliedQueryPart, "TTeamMitglied");
        return tteamMitgliedQuery.getResultList();
    }


    private Query buildQuery(QueryPartDTO qp, String classOfQuery) {
        Query q;
        switch (classOfQuery) {
            case "TTeamMitglied":
                q = entityManager.createQuery(qp.getQuery(), TTeamMitglied.class);
                break;
            case "TTeamVertreter":
                q = entityManager.createQuery(qp.getQuery(), TteamVertreter.class);
                break;
            default:
                q = entityManager.createQuery(qp.getQuery(), Mitarbeiter.class);
                break;
        }
        qp.getParameterMap().entrySet().forEach(p -> {
            q.setParameter(p.getKey(), p.getValue());
        });
        return q;
    }

    private List<Rolle> getRolesForUser() {
        List<Rolle> returnList = new ArrayList<>();

        if (session.hasRole(Rolle.SENSORCOCLEITER)) {
            returnList.add(Rolle.SENSOR);
            returnList.add(Rolle.SCL_VERTRETER);
            returnList.add(Rolle.SENSOR_EINGESCHRAENKT);
            returnList.add(Rolle.UMSETZUNGSBESTAETIGER);
        }
        if (session.hasRole(Rolle.T_TEAMLEITER)) {
            returnList.add(Rolle.TTEAMMITGLIED);
            returnList.add(Rolle.TTEAM_VERTRETER);
            returnList.add(Rolle.T_TEAMLEITER);
        }
        return returnList;
    }

    public Set<Rolle> getAllRolesForUserFromBerechtigung(Mitarbeiter mitarbeiter) {
        Set<Rolle> rolesForUser = new HashSet<>();

        rolesForUser.addAll(getAllRolesFromBerechtigung(mitarbeiter));

        rolesForUser.addAll(getAllRolesFromBerechtigungDerivat(mitarbeiter));

        if (hasUserRoleTteamMitglied(mitarbeiter)) {
            rolesForUser.add(Rolle.TTEAMMITGLIED);
        }
        if (hasUserRoleTteamVertreter(mitarbeiter)) {
            rolesForUser.add(Rolle.TTEAM_VERTRETER);
        }

        return rolesForUser;
    }

    private List<Rolle> getAllRolesFromBerechtigung(Mitarbeiter mitarbeiter) {
        QueryPartDTO queryPart = new QueryPartDTO();
        queryPart.append("SELECT DISTINCT b.rolle FROM Berechtigung b ");
        queryPart.append(" WHERE b.mitarbeiter.id = :mitarbeiterId");
        queryPart.put("mitarbeiterId", mitarbeiter.getId());
        Query query = queryPart.buildGenericQuery(entityManager, Rolle.class);

        List<Rolle> result = query.getResultList();

        return result;
    }

    private List<Rolle> getAllRolesFromBerechtigungDerivat(Mitarbeiter mitarbeiter) {
        QueryPartDTO queryPart = new QueryPartDTO();
        queryPart.append("SELECT DISTINCT b.rolle FROM BerechtigungDerivat b ");
        queryPart.append(" WHERE b.mitarbeiter.id = :mitarbeiterId");
        queryPart.put("mitarbeiterId", mitarbeiter.getId());
        Query query = queryPart.buildGenericQuery(entityManager, Rolle.class);

        List<Rolle> result = query.getResultList();

        return result;
    }

    private Boolean hasUserRoleTteamMitglied(Mitarbeiter mitarbeiter) {

        QueryPartDTO queryPart = new QueryPartDTO();
        queryPart.append("SELECT ttm.tteam.id from TTeamMitglied ttm ");

        queryPart.append(" WHERE ttm.mitarbeiter.id = :mitarbeiterId");
        queryPart.put("mitarbeiterId", mitarbeiter.getId());

        Query query = queryPart.buildGenericQuery(entityManager, Long.class);
        List<Long> result = query.getResultList();

        if (result == null || result.isEmpty()) {
            return false;
        } else {
            return true;
        }

    }

    private Boolean hasUserRoleTteamVertreter(Mitarbeiter mitarbeiter) {

        QueryPartDTO queryPart = new QueryPartDTO();
        queryPart.append("SELECT ttm.tteam.id from TteamVertreter ttm ");

        queryPart.append(" WHERE ttm.mitarbeiter.id = :mitarbeiter");
        queryPart.put("mitarbeiter", mitarbeiter.getId());

        Query query = queryPart.buildGenericQuery(entityManager, Long.class);
        List<Long> result = query.getResultList();

        if (result == null || result.isEmpty()) {
            return false;
        } else {
            return true;
        }

    }

}
