package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.dao.BerechtigungDao;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.UserToRole;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.exceptions.UserNotFoundException;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.services.UserService;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author fn
 */
@Stateless
public class QnumberChangeService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(QnumberChangeService.class);

    @Inject
    private UserService userService;
    @Inject
    private BerechtigungDao berechtigungDao;
    @Inject
    private BerechtigungSearchService berechtigungSearchService;
    @Inject
    private UserSearchService userSearchService;

    public Boolean changeQnumber(Mitarbeiter mitarbeiter, String newQnumber) {

        if (!RegexUtils.matchesQnumber(newQnumber)) {
            return false;
        }

        newQnumber = newQnumber.toUpperCase();

        String oldQnumber = mitarbeiter.getQNumber();

        changeQnumberInUserToRole(newQnumber, oldQnumber, mitarbeiter);

        mitarbeiter.setQNumber(newQnumber);

        userService.saveMitarbeiter(mitarbeiter);

        return true;
    }

    private void changeQnumberInUserToRole(String newQnumber, String oldQnumber, Mitarbeiter mitarbeiter) {

        List<UserToRole> userToRoles = berechtigungDao.getUserToRolesByQnumber(oldQnumber);

        List<Rolle> rollesInDatabase = new ArrayList<>();

        changeUserToRolesToNewQnumber(userToRoles, newQnumber, rollesInDatabase);

        fixInconsistencInUserToRoleAndBerechtigung(rollesInDatabase, mitarbeiter);

    }

    private void changeUserToRolesToNewQnumber(List<UserToRole> userToRoles, String newQnumber, List<Rolle> rollesInDatabase) {
        userToRoles.stream().map(userToRole ->
                setNewQnumberToUserToRole(userToRole, newQnumber, rollesInDatabase)
        ).forEachOrdered(userToRole ->
                berechtigungDao.persistRolle(userToRole)
        );
    }

    private UserToRole setNewQnumberToUserToRole(UserToRole userToRole, String newQnumber, List<Rolle> rollesInDatabase) {
        userToRole.setqNumber(newQnumber);
        rollesInDatabase.add(getRoleFromString(userToRole.getRole()));
        return userToRole;
    }

    private Rolle getRoleFromString(String rolleString) {
        return Rolle.getRolleByRawString(rolleString);
    }

    private void fixInconsistencInUserToRoleAndBerechtigung(List<Rolle> rollesInUserToRoles, Mitarbeiter mitarbeiter) {

        Set<Rolle> berechtigungRoles = berechtigungSearchService.getAllRolesForUserFromBerechtigung(mitarbeiter);

        for (Rolle rolle : berechtigungRoles) {
            checkAndSaveUnsavedRolesInUserToRole(rollesInUserToRoles, rolle, mitarbeiter);
        }

    }

    private void checkAndSaveUnsavedRolesInUserToRole(List<Rolle> rollesInUserToRoles, Rolle rolle, Mitarbeiter mitarbeiter) {
        if (!rollesInUserToRoles.contains(rolle)) {
            UserToRole userToRole = new UserToRole(mitarbeiter.getQNumber(), rolle.getRolleRawString());
            berechtigungDao.persistNewRolleForUser(userToRole);
        }
    }

    public Mitarbeiter findMitarbeiterInLdapByQnumber(String qnumberToSearch) {
        try {
            return userSearchService.getMitarbeiterFromLdap(qnumberToSearch);
        } catch (UserNotFoundException ex) {
            LOG.info(ex.toString());
            return null;
        }
    }

}
