package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * @author fn
 */
@Named
@ViewScoped
public class MitarbeiterQnumberChangeController implements Serializable {

    @Inject
    private QnumberChangeService qnumberChangeService;
    @Inject
    private LocalizationService localizationService;

    private Mitarbeiter mitarbeiterToEdit;

    private Mitarbeiter mitarbeiterFound;

    private String qnumberToSearch;

    public void showQnumberChangeDialog(Mitarbeiter mitarbeiterToEdit) {
        updateDialogData(mitarbeiterToEdit);
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialog:changeQnumberDialogForm");
        context.execute("PF('changeQnumberDialog').show()");
    }

    private void updateDialogData(Mitarbeiter mitarbeiterToEdit) {
        this.mitarbeiterToEdit = mitarbeiterToEdit;
    }

    public void handleSuche() {
        mitarbeiterFound = qnumberChangeService.findMitarbeiterInLdapByQnumber(qnumberToSearch);
        if (mitarbeiterFound == null) {
            String message = localizationService.getValue("qnumberchangedialog_qnumber_not_found") + qnumberToSearch;
            writeMessageToGrowl(FacesMessage.SEVERITY_ERROR, message);
        }
    }

    public void processChanges() {
        if (qnumberToSearch == null || qnumberToSearch.isEmpty()) {
            String message = localizationService.getValue("qnumberchangedialog_qnumber_missing");
            writeMessageToGrowl(FacesMessage.SEVERITY_ERROR, message);
        } else {
            doQnumberChange();
        }
    }

    private void writeMessageToGrowl(Severity messageType, String message) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.addMessage(null, new FacesMessage(messageType, message, ""));
    }

    private void doQnumberChange() {
        boolean suceeded = qnumberChangeService.changeQnumber(mitarbeiterToEdit, qnumberToSearch);
        if (suceeded) {
            String message = localizationService.getValue("qnumberchangedialog_qnumber_changed");
            writeMessageToGrowl(FacesMessage.SEVERITY_INFO, message);
            RequestContext.getCurrentInstance().execute("PF('changeQnumberDialog').hide();");
        } else {
            String message = localizationService.getValue("qnumberchangedialog_qnumber_not_valid");
            writeMessageToGrowl(FacesMessage.SEVERITY_ERROR, message);
        }
    }

    public String getQnumberToSearch() {
        return qnumberToSearch;
    }

    public void setQnumberToSearch(String qNumberToSearch) {
        this.qnumberToSearch = qNumberToSearch;
    }

    public Mitarbeiter getMitarbeiterToEdit() {
        return mitarbeiterToEdit;
    }

    public Mitarbeiter getMitarbeiterFound() {
        return mitarbeiterFound;
    }

}
