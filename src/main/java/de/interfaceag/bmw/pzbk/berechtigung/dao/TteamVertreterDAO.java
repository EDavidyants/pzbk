package de.interfaceag.bmw.pzbk.berechtigung.dao;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.rollen.TteamVertreter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * @author fn
 */
@Stateless
public class TteamVertreterDAO implements Serializable {

    private static final String MITARBEITER = "mitarbeiter";
    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public void persistTteamVertreter(TteamVertreter tteamVertreter) {
        entityManager.persist(tteamVertreter);
    }

    public void removeTteamVertreterBerechtigung(TteamVertreter tteamVertreter) {
        entityManager.remove(tteamVertreter);
    }

    public List<TteamVertreter> getTteamVertreterForMitarbeiter(Mitarbeiter mitarbeiter) {
        QueryPartDTO queryPartDTO;
        queryPartDTO = new QueryPartDTO("SELECT ttm from TteamVertreter ttm ");

        queryPartDTO.append(" WHERE ttm.mitarbeiter.id = :mitarbeiter");
        queryPartDTO.put(MITARBEITER, mitarbeiter.getId());


        QueryFactory<TteamVertreter> queryFactory = new QueryFactory<>();
        TypedQuery<TteamVertreter> query = queryFactory.buildTypedQuery(queryPartDTO, entityManager, TteamVertreter.class);

        return query.getResultList();
    }

    public Optional<TteamVertreter> getTTeamVertreterByTteamAndMitarbeiter(Tteam tteam, Mitarbeiter mitarbeiter) {
        QueryPartDTO queryPartDTO;
        queryPartDTO = new QueryPartDTO("SELECT ttm from TteamVertreter ttm ");

        queryPartDTO.append(" WHERE ttm.mitarbeiter.id = :mitarbeiter");
        queryPartDTO.put(MITARBEITER, mitarbeiter.getId());

        queryPartDTO.append(" AND ttm.tteam.id = :tteam");
        queryPartDTO.put("tteam", tteam.getId());

        QueryFactory<TteamVertreter> queryFactory = new QueryFactory<>();
        TypedQuery<TteamVertreter> query = queryFactory.buildTypedQuery(queryPartDTO, entityManager, TteamVertreter.class);

        List<TteamVertreter> result = query.getResultList();
        return result != null && !result.isEmpty() ? Optional.ofNullable(result.get(0)) : Optional.empty();

    }


    public List<Long> getTteamIdsforTteamVertreter(Mitarbeiter mitarbeiter) {

        QueryPartDTO queryPartDTO;
        queryPartDTO = new QueryPartDTO("SELECT ttv.tteam.id from TteamVertreter ttv ");

        queryPartDTO.append("WHERE ttv.mitarbeiter.id = :mitarbeiter");
        queryPartDTO.put(MITARBEITER, mitarbeiter.getId());

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPartDTO, entityManager, Long.class);

        return query.getResultList();

    }

}
