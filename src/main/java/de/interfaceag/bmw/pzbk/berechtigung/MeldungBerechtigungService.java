package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Named
@Stateless
public class MeldungBerechtigungService implements Serializable {

    @Inject
    private Session session;

    @Inject
    private MeldungBerechtigungDao meldungBerechtigungDao;

    /**
     * Get the subset of authorized meldung ids for a collection of meldung ids.
     *
     * @param meldungIds a collection of meldung ids.
     * @return the subset of the input collection for which the current user is
     * authorized.
     */
    public Collection<Long> restrictToAuthorizedMeldungen(Collection<Long> meldungIds) {

        if (meldungIds == null || meldungIds.isEmpty()) {
            return Collections.emptyList();
        }

        final Collection<Long> authorizedMeldungIdsForCurrentUser = this.getAuthorizedMeldungIdsForCurrentUser();

        if (authorizedMeldungIdsForCurrentUser.isEmpty()) {
            return Collections.emptyList();
        } else {
            meldungIds.retainAll(authorizedMeldungIdsForCurrentUser);
            return meldungIds;
        }
    }

    /**
     * Get all ids of authorized meldungen for the current user.
     *
     * @return all authorized meldung ids.
     */
    private Collection<Long> getAuthorizedMeldungIdsForCurrentUser() {

        final UserPermissions<BerechtigungDto> userPermissions = session.getUserPermissions();
        final Mitarbeiter currentUser = session.getUser();
        final Set<Rolle> roles = userPermissions.getRoles();

        Set<Long> allAuthorizedMeldungIds = new HashSet<>();
        for (Rolle role : roles) {
            final Collection<Long> authorizedMeldungIdsForRole = getAuthorizedMeldungIdsForRole(role, userPermissions, currentUser);
            allAuthorizedMeldungIds.addAll(authorizedMeldungIdsForRole);
        }

        return allAuthorizedMeldungIds;
    }

    private Collection<Long> getAuthorizedMeldungIdsForRole(Rolle role, UserPermissions<BerechtigungDto> userPermissions, Mitarbeiter sensor) {
        switch (role) {
            case ADMIN:
            case ANFORDERER:
                return meldungBerechtigungDao.getAllMeldungIds();
            case SENSOR:
                return getAuthorizedMeldungIdsForRoleSensor(userPermissions);
            case SENSOR_EINGESCHRAENKT:
                return getAuthorizedMeldungIdsForRoleSensorEingeschraenkt(userPermissions, sensor);
            case SENSORCOCLEITER:
                return getAuthorizedMeldungIdsForRoleSensorCocLeiter(userPermissions);
            case SCL_VERTRETER:
                return getAuthorizedMeldungIdsForRoleSensorCocVertreter(userPermissions);
            default:
                return Collections.emptyList();
        }
    }

    private Collection<Long> getAuthorizedMeldungIdsForRoleSensorCocVertreter(UserPermissions<BerechtigungDto> userPermissions) {
        final Collection<Long> sensorCocSchreibend = berechtigungToIds(userPermissions.getSensorCocAsVertreterSCLSchreibend());
        return meldungBerechtigungDao.getAllMeldungIdsForSensorCocIds(sensorCocSchreibend);
    }

    private Collection<Long> getAuthorizedMeldungIdsForRoleSensorCocLeiter(UserPermissions<BerechtigungDto> userPermissions) {
        final Collection<Long> sensorCocSchreibend = berechtigungToIds(userPermissions.getSensorCocAsSensorCocLeiterSchreibend());
        return meldungBerechtigungDao.getAllMeldungIdsForSensorCocIds(sensorCocSchreibend);
    }

    private Collection<Long> getAuthorizedMeldungIdsForRoleSensor(UserPermissions<BerechtigungDto> userPermissions) {
        final Collection<Long> sensorCocSchreibend = berechtigungToIds(userPermissions.getSensorCocAsSensorSchreibend());
        final Collection<Long> sensorCocLesend = berechtigungToIds(userPermissions.getSensorCocAsSensorLesend());

        Collection<Long> allSensorCocIds = new HashSet<>(sensorCocSchreibend);
        allSensorCocIds.addAll(sensorCocLesend);

        return meldungBerechtigungDao.getAllMeldungIdsForSensorCocIds(allSensorCocIds);
    }

    private Collection<Long> getAuthorizedMeldungIdsForRoleSensorEingeschraenkt(UserPermissions<BerechtigungDto> userPermissions, Mitarbeiter sensor) {
        final Collection<Long> allSensorCocIds = berechtigungToIds(userPermissions.getSensorCocAsSensorEingSchreibend());

        return meldungBerechtigungDao.getAllMeldungIdsForSensorCocIdsWithUserAsSensor(allSensorCocIds, sensor);
    }

    private static Collection<Long> berechtigungToIds(Collection<BerechtigungDto> berechtigungen) {
        return berechtigungen.stream().map(BerechtigungDto::getId).collect(Collectors.toList());
    }

}
