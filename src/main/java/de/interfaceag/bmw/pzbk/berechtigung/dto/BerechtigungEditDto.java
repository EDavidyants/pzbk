package de.interfaceag.bmw.pzbk.berechtigung.dto;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface BerechtigungEditDto extends BerechtigungDto {

    boolean isDelete();

    void setDelete(boolean delete);

    boolean getEditPermission();

}
