package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.filter.AbteilungFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ZakEinordnungFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author fn
 */
final class BerechtigungSearchUtils {

    public static final String AND_B_RECHTTYPE_RECHTTYPE_AND_B_FUER_FUER_AND_B_FUER_ID_IN_FUER_ID = " AND b.rechttype = :rechttype AND b.fuer = :fuer AND b.fuerId IN :fuerId";
    public static final String RECHTTYPE = "rechttype";
    public static final String FUER_ID = "fuerId";
    public static final String FUER = "fuer";

    private BerechtigungSearchUtils() {
    }

    static QueryPartDTO getExistingMitarbeiterBaseQuery() {
        QueryPartDTO qp;

        qp = new QueryPartDTO("SELECT DISTINCT m FROM Mitarbeiter m WHERE 1 = 1 ");

        return qp;
    }

    static QueryPartDTO getNurBerechtigteQery(Set<Mitarbeiter> existingMitarbeiter) {

        QueryPartDTO qp;

        qp = new QueryPartDTO(" SELECT DISTINCT b.mitarbeiter FROM Berechtigung b WHERE b.mitarbeiter IN :mitarbeiter ");
        qp.put("mitarbeiter", existingMitarbeiter);
        return qp;

    }

    static void getVornameQuery(QueryPartDTO qp, TextSearchFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND LOWER(m.vorname) LIKE LOWER(:vorname)");
            qp.put("vorname", "%" + filter.getAsString() + "%");
        }
    }

    static void getNachnameQuery(QueryPartDTO qp, TextSearchFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND LOWER(m.nachname) LIKE LOWER(:nachname)");
            qp.put("nachname", "%" + filter.getAsString() + "%");
        }
    }

    static void getAbteilungQuery(QueryPartDTO qp, AbteilungFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND m.abteilung IN :abteilung");
            qp.put("abteilung", filter.getAllSelectedAsStringList());
        }
    }

    static void getSortQueryPart(QueryPartDTO qp) {
        qp.append(" ORDER BY m.vorname");
    }

    static QueryPartDTO getMitarbeiterWhitBerechtigungZielBaseQery(Set<Mitarbeiter> mitarbeiter) {
        List<Long> mitarbeiterId = new ArrayList<>();
        mitarbeiter.forEach(m -> mitarbeiterId.add(m.getId()));

        QueryPartDTO qp = new QueryPartDTO("SELECT b.mitarbeiter FROM Berechtigung b WHERE b.mitarbeiter.id IN :mitarbeiter ");
        qp.put("mitarbeiter", mitarbeiterId);
        return qp;
    }

    static QueryPartDTO getMitarbeiterWithRole(QueryPartDTO qp, MultiValueEnumSearchFilter<Rolle> filter) {

        qp.append(" AND b.rolle IN :rolle ");
        qp.put("rolle", filter.getAsEnumList());
        return qp;
    }

    static QueryPartDTO getMitarbeiterWithRole(QueryPartDTO qp, List<Rolle> filter) {

        qp.append(" AND b.rolle IN :rolle ");
        qp.put("rolle", filter);
        return qp;
    }

    static QueryPartDTO getMitarbeiterWhitBerechtigungZielSensorCoCLesend(QueryPartDTO qp, IdSearchFilter filter) {
        List<String> idAsString = new ArrayList<>();
        filter.getSelectedIdsAsSet().forEach(id -> idAsString.add(Long.toString(id)));
        qp.append(AND_B_RECHTTYPE_RECHTTYPE_AND_B_FUER_FUER_AND_B_FUER_ID_IN_FUER_ID);
        qp.put(RECHTTYPE, Rechttype.LESERECHT);
        qp.put(FUER, BerechtigungZiel.SENSOR_COC);
        qp.put(FUER_ID, idAsString);
        return qp;
    }

    static QueryPartDTO getMitarbeiterWhitBerechtigungZielSensorCoCSchreibend(QueryPartDTO qp, IdSearchFilter filter) {
        List<String> idAsString = new ArrayList<>();
        filter.getSelectedIdsAsSet().forEach(id -> idAsString.add(Long.toString(id)));

        qp.append(AND_B_RECHTTYPE_RECHTTYPE_AND_B_FUER_FUER_AND_B_FUER_ID_IN_FUER_ID);
        qp.put(RECHTTYPE, Rechttype.SCHREIBRECHT);
        qp.put(FUER, BerechtigungZiel.SENSOR_COC);
        qp.put(FUER_ID, idAsString);
        return qp;
    }

    static QueryPartDTO getMitarbeiterWhitBerechtigungZielModulSETEamSchreibend(QueryPartDTO qp, IdSearchFilter filter) {
        List<String> idAsString = new ArrayList<>();
        filter.getSelectedIdsAsSet().forEach(id -> idAsString.add(Long.toString(id)));

        qp.append(AND_B_RECHTTYPE_RECHTTYPE_AND_B_FUER_FUER_AND_B_FUER_ID_IN_FUER_ID);
        qp.put(RECHTTYPE, Rechttype.SCHREIBRECHT);
        qp.put(FUER, BerechtigungZiel.MODULSETEAM);
        qp.put(FUER_ID, idAsString);
        return qp;
    }

    static QueryPartDTO getMitarbeiterWhitBerechtigungZielDerivatSchreibend(QueryPartDTO qp, IdSearchFilter filter) {
        List<String> idAsString = new ArrayList<>();
        filter.getSelectedIdsAsSet().forEach(id -> idAsString.add(Long.toString(id)));

        qp.append(AND_B_RECHTTYPE_RECHTTYPE_AND_B_FUER_FUER_AND_B_FUER_ID_IN_FUER_ID);
        qp.put(RECHTTYPE, Rechttype.SCHREIBRECHT);
        qp.put(FUER, BerechtigungZiel.DERIVAT);
        qp.put(FUER_ID, idAsString);
        return qp;
    }

    static QueryPartDTO getMitarbeiterWhitBerechtigungZielTTeamSchreibend(QueryPartDTO qp, IdSearchFilter filter) {
        List<String> idAsString = new ArrayList<>();
        filter.getSelectedIdsAsSet().forEach(id -> idAsString.add(Long.toString(id)));

        qp.append(AND_B_RECHTTYPE_RECHTTYPE_AND_B_FUER_FUER_AND_B_FUER_ID_IN_FUER_ID);
        qp.put(RECHTTYPE, Rechttype.SCHREIBRECHT);
        qp.put(FUER, BerechtigungZiel.TTEAM);
        qp.put(FUER_ID, idAsString);
        return qp;
    }

    static QueryPartDTO getMitarbeiterWhitBerechtigungZielZAKEinordnungSchreibend(QueryPartDTO qp, ZakEinordnungFilter filter) {

        qp.append(AND_B_RECHTTYPE_RECHTTYPE_AND_B_FUER_FUER_AND_B_FUER_ID_IN_FUER_ID);
        qp.put(RECHTTYPE, Rechttype.SCHREIBRECHT);
        qp.put(FUER, BerechtigungZiel.ZAK_EINORDNUNG);
        qp.put(FUER_ID, filter.getAllSelectedAsStringList());
        return qp;
    }

    static void getSortQueryPartForBerechtigung(QueryPartDTO qp) {
        qp.append(" GROUP BY b.mitarbeiter");
    }

    static QueryPartDTO getAllTteamVertreter() {
        QueryPartDTO qp;

        qp = new QueryPartDTO(" SELECT DISTINCT ttv.mitarbeiter FROM TteamVertreter ttv");
        return qp;
    }

    public static QueryPartDTO getTteamVertreterForMitarbeiter(Set<Mitarbeiter> existingMitarbeiter) {
        QueryPartDTO qp;

        qp = new QueryPartDTO(" SELECT DISTINCT ttv.mitarbeiter FROM TteamVertreter ttv WHERE ttv.mitarbeiter IN :existingMitarbeiter");
        qp.put("existingMitarbeiter", existingMitarbeiter);
        return qp;
    }

    static QueryPartDTO getAllTteamMitglieder() {
        QueryPartDTO qp;

        qp = new QueryPartDTO(" SELECT DISTINCT ttm.mitarbeiter FROM TTeamMitglied ttm");
        return qp;
    }

    public static QueryPartDTO getTteamMitgliederforMitarbeiter(Set<Mitarbeiter> existingMitarbeiter) {
        QueryPartDTO qp;

        qp = new QueryPartDTO(" SELECT DISTINCT ttm.mitarbeiter FROM TTeamMitglied ttm WHERE ttm.mitarbeiter IN :existingMitarbeiter");
        qp.put("existingMitarbeiter", existingMitarbeiter);
        return qp;
    }
}
