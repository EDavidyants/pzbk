package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.excel.ExcelSheetRealm;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class BerechtigungsExcelRealm {

    private final String name;
    private final String abteilung;
    private final String mail;
    private final String qnumber;
    private final boolean eingeschraenkt;
    private final List<String> sensors;
    private final List<String> leserechte;
    private final List<String> sensorCocs;
    private final List<String> zakEinordnungen;
    private final List<String> plVorschlaege;
    private final List<String> plueUmsetzer;
    private final List<String> tadmin;
    private final List<String> fadmin;
    public final List<String> tteam;
    public final List<String> tteamVertreter;
    public final List<String> tteamMitgliedLesend;
    public final List<String> tteamMitgliedSchreibend;

    public BerechtigungsExcelRealm(ExcelSheetRealm realm) {
        qnumber = realm.getTag();

        name = realm.getOrthogonalChain(0, true).get(0);
        abteilung = realm.getOrthogonalChain(1, true).get(0);
        mail = realm.getOrthogonalChain(2, true).get(0);

        //
        Integer i;
        if (qnumber.startsWith("Q") || qnumber.startsWith("q")) {
            i = 0;
        } else {
            i = -1;
        }

        eingeschraenkt = realm.getOrthogonalChain(4 + i, true).get(0).equals("ja");
        sensors = realm.getOrthogonalChain(5 + i, false);
        leserechte = realm.getOrthogonalChain(6 + i, false);
        sensorCocs = realm.getOrthogonalChain(7 + i, false);
        zakEinordnungen = realm.getOrthogonalChain(9 + i, false);
        plVorschlaege = realm.getOrthogonalChain(10 + i, false);
        plueUmsetzer = realm.getOrthogonalChain(12 + i, false);
        fadmin = realm.getOrthogonalChain(13 + i, false);
        tadmin = realm.getOrthogonalChain(14 + i, false);
        tteam = realm.getOrthogonalChain(15 + i, false);

        tteamVertreter = realm.getOrthogonalChain(16 + i, false);
        tteamMitgliedLesend = realm.getOrthogonalChain(17 + i, false);
        tteamMitgliedSchreibend = realm.getOrthogonalChain(18 + i, false);
    }

    public String getQnumber() {
        return qnumber;
    }

    public boolean isEingeschraenkt() {
        return eingeschraenkt;
    }

    public List<String> getTteamVertreter() {
        return tteamVertreter;
    }

    public List<String> getTteamMitgliedLesend() {
        return tteamMitgliedLesend;
    }

    public List<String> getTteamMitgliedSchreibend() {
        return tteamMitgliedSchreibend;
    }

    /**
     * @return the sensors
     */
    public List<String> getSensors() {
        return Collections.unmodifiableList(sensors);
    }

    /**
     * @return the leserechte
     */
    public List<String> getLeserechte() {
        return Collections.unmodifiableList(leserechte);
    }

    /**
     * @return the sensorCocs
     */
    public List<String> getSensorCocs() {
        return Collections.unmodifiableList(sensorCocs);
    }

    /**
     * @return the zakEinordnungen
     */
    public List<String> getZakEinordnungen() {
        return Collections.unmodifiableList(zakEinordnungen);
    }

    /**
     * @return the plVorschlaege
     */
    public List<String> getPlVorschlaege() {
        return Collections.unmodifiableList(plVorschlaege);
    }

    /**
     * @return the plueUmsetzer
     */
    public List<String> getPlueUmsetzer() {
        return Collections.unmodifiableList(plueUmsetzer);
    }

    /**
     * @return the tadmin
     */
    public List<String> getTadmin() {
        return tadmin;
    }

    /**
     * @return the fadmin
     */
    public List<String> getFadmin() {
        return fadmin;
    }

    private boolean strListEqual(List<String> l1, List<String> l2) {
        if (l1 == null || l2 == null || l1.size() != l2.size()) {
            return false;
        }
        Iterator<String> iter1 = l1.iterator();
        Iterator<String> iter2 = l2.iterator();

        while (iter1.hasNext()) {
            if (!iter1.next().equals(iter2.next())) {
                return false;
            }
        }
        return true;

    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.qnumber);
        hash = 47 * hash + (this.eingeschraenkt ? 1 : 0);
        hash = 47 * hash + Objects.hashCode(this.sensors);
        hash = 47 * hash + Objects.hashCode(this.leserechte);
        hash = 47 * hash + Objects.hashCode(this.sensorCocs);
        hash = 47 * hash + Objects.hashCode(this.zakEinordnungen);
        hash = 47 * hash + Objects.hashCode(this.plVorschlaege);
        hash = 47 * hash + Objects.hashCode(this.plueUmsetzer);
        hash = 47 * hash + Objects.hashCode(this.tadmin);
        hash = 47 * hash + Objects.hashCode(this.fadmin);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BerechtigungsExcelRealm other = (BerechtigungsExcelRealm) obj;
        if (this.eingeschraenkt != other.eingeschraenkt) {
            return false;
        }
        if (!qnumber.equals(other.qnumber)) {
            return false;
        }

        if (!strListEqual(sensors, other.sensors)) {
            return false;
        }
        if (!strListEqual(leserechte, other.leserechte)) {
            return false;
        }
        if (!strListEqual(sensorCocs, other.sensorCocs)) {
            return false;
        }
        if (!strListEqual(zakEinordnungen, other.zakEinordnungen)) {
            return false;
        }
        if (!strListEqual(plVorschlaege, other.plVorschlaege)) {
            return false;
        }
        if (!strListEqual(plueUmsetzer, other.plueUmsetzer)) {
            return false;
        }
        if (!strListEqual(tadmin, other.tadmin)) {
            return false;
        }
        return strListEqual(fadmin, other.fadmin);
    }

    /**
     * @return the tteam
     */
    public List<String> getTteam() {
        return Collections.unmodifiableList(tteam);
    }

    public String getName() {
        return name;
    }

    public String getAbteilung() {
        return abteilung;
    }

    public String getMail() {
        return mail;
    }

}
