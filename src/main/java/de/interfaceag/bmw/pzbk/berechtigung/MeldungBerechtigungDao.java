package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

@Dependent
public class MeldungBerechtigungDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public Collection<Long> getAllMeldungIds() {
        QueryPart queryPart = new QueryPartDTO("SELECT m.id FROM Meldung m");
        return getQueryResult(queryPart);
    }

    public Collection<Long> getAllMeldungIdsForSensorCocIdsWithUserAsSensor(Collection<Long> sensorCocIds, Mitarbeiter sensor) {
        if (sensorCocIds.isEmpty()) {
            return Collections.emptyList();
        }

        QueryPart queryPart = new QueryPartDTO("SELECT m.id FROM Meldung m WHERE m.sensorCoc.sensorCocId IN :sensorCocIds AND m.sensor = :sensor");
        queryPart.put("sensorCocIds", sensorCocIds);
        queryPart.put("sensor", sensor);
        return getQueryResult(queryPart);
    }

    public Collection<Long> getAllMeldungIdsForSensorCocIds(Collection<Long> sensorCocIds) {
        if (sensorCocIds.isEmpty()) {
            return Collections.emptyList();
        }

        QueryPart queryPart = new QueryPartDTO("SELECT m.id FROM Meldung m WHERE m.sensorCoc.sensorCocId IN :sensorCocIds");
        queryPart.put("sensorCocIds", sensorCocIds);
        return getQueryResult(queryPart);
    }

    private Collection<Long> getQueryResult(QueryPart queryPart) {
        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

}
