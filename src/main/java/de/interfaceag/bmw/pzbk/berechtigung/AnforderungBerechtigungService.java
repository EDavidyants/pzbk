package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Named
@Stateless
public class AnforderungBerechtigungService implements Serializable {

    @Inject
    private Session session;

    @Inject
    private AnforderungBerechtigungDao anforderungBerechtigungDao;

    /**
     * Get the subset of authorized anforderung ids for a collection of
     * anforderung ids.
     *
     * @param anforderungIds a collection of anforderung ids.
     * @return the subset of the input collection for which the current user is
     * authorized.
     */
    public Collection<Long> restrictToAuthorizedAnforderungen(Collection<Long> anforderungIds) {

        if (anforderungIds == null || anforderungIds.isEmpty()) {
            return Collections.emptyList();
        }

        final Collection<Long> authorizedAnforderungIdsForCurrentUser = this.getAuthorizedAnforderungIdsForCurrentUser();
        anforderungIds.retainAll(authorizedAnforderungIdsForCurrentUser);
        return anforderungIds;
    }

    /**
     * Get all ids of authorized anforderungen for the current user.
     *
     * @return all authorized anforderung ids.
     */
    private Collection<Long> getAuthorizedAnforderungIdsForCurrentUser() {

        final UserPermissions<BerechtigungDto> userPermissions = session.getUserPermissions();
        final Mitarbeiter currentUser = session.getUser();
        final Set<Rolle> roles = userPermissions.getRoles();

        Set<Long> allAuthorizedAnforderungIds = new HashSet<>();
        for (Rolle role : roles) {
            final Collection<Long> authorizedAnforderungIdsForRole = getAuthorizedAnforderungIdsForRole(role, userPermissions, currentUser);
            allAuthorizedAnforderungIds.addAll(authorizedAnforderungIdsForRole);
        }

        return allAuthorizedAnforderungIds;
    }

    private Collection<Long> getAuthorizedAnforderungIdsForRole(Rolle role, UserPermissions<BerechtigungDto> userPermissions, Mitarbeiter sensor) {
        switch (role) {
            case ADMIN:
            case ANFORDERER:
                return anforderungBerechtigungDao.getAllAnforderungIds();
            case SENSOR:
                return getAuthorizedAnforderungIdsForRoleSensor(userPermissions);
            case SENSOR_EINGESCHRAENKT:
                return getAuthorizedAnforderungIdsForRoleSensorEingeschraenkt(userPermissions, sensor);
            case SENSORCOCLEITER:
                return getAuthorizedAnforderungIdsForRoleSensorCocLeiter(userPermissions);
            case SCL_VERTRETER:
                return getAuthorizedAnforderungIdsForRoleSensorCocVertreter(userPermissions);
            case T_TEAMLEITER:
                return getAuthorizedAnforderungIdsForRoleTteamLeiter(userPermissions);
            case TTEAM_VERTRETER:
                return getAuthorizedAnforderungIdsForRoleVertreter(userPermissions);
            case TTEAMMITGLIED:
                return getAuthorizedAnforderungIdsForRoleTteamMitglied(userPermissions);
            case E_COC:
                return getAuthorizedAnforderungIdsForRoleEcoc(userPermissions);
            case UMSETZUNGSBESTAETIGER:
                return getAuthorizedAnforderungIdsForRoleUmsetzungsbestaetiger(userPermissions);
            default:
                return Collections.emptyList();
        }
    }

    private Collection<Long> getAuthorizedAnforderungIdsForRoleEcoc(UserPermissions<BerechtigungDto> userPermissions) {
        final Collection<Long> allModulSeTeams = berechtigungToIds(userPermissions.getModulSeTeamAsEcocSchreibend());

        return anforderungBerechtigungDao.getAllAnforderungIdsForModulSeTeamIds(allModulSeTeams);
    }

    private Collection<Long> getAuthorizedAnforderungIdsForRoleTteamMitglied(UserPermissions<BerechtigungDto> userPermissions) {
        final Collection<Long> tteamSchreibend = userPermissions.getTteamIdsForTteamMitgliedSchreibend();
        final Collection<Long> tteamLesend = userPermissions.getTteamIdsForTteamMitgliedLesend();

        Collection<Long> allTteamIds = new HashSet<>(tteamSchreibend);
        allTteamIds.addAll(tteamLesend);

        return anforderungBerechtigungDao.getAllAnforderungIdsForTteamIds(allTteamIds);
    }

    private Collection<Long> getAuthorizedAnforderungIdsForRoleVertreter(UserPermissions<BerechtigungDto> userPermissions) {
        final Collection<Long> tteamIds = userPermissions.getTteamIdsForTteamVertreter();
        return anforderungBerechtigungDao.getAllAnforderungIdsForTteamIds(tteamIds);
    }

    private Collection<Long> getAuthorizedAnforderungIdsForRoleTteamLeiter(UserPermissions<BerechtigungDto> userPermissions) {
        final Collection<Long> tteamIds = berechtigungToIds(userPermissions.getTteamAsTteamleiterSchreibend());
        return anforderungBerechtigungDao.getAllAnforderungIdsForTteamIds(tteamIds);
    }

    private Collection<Long> getAuthorizedAnforderungIdsForRoleUmsetzungsbestaetiger(UserPermissions<BerechtigungDto> userPermissions) {
        final Collection<Long> allSensorCocIds = berechtigungToIds(userPermissions.getSensorCocAsUmsetzungsbestaetigerSchreibend());

        return anforderungBerechtigungDao.getAllAnforderungIdsForSensorCocIds(allSensorCocIds);
    }

    private Collection<Long> getAuthorizedAnforderungIdsForRoleSensorCocVertreter(UserPermissions<BerechtigungDto> userPermissions) {
        final Collection<Long> sensorCocSchreibend = berechtigungToIds(userPermissions.getSensorCocAsVertreterSCLSchreibend());
        return anforderungBerechtigungDao.getAllAnforderungIdsForSensorCocIds(sensorCocSchreibend);
    }

    private Collection<Long> getAuthorizedAnforderungIdsForRoleSensorCocLeiter(UserPermissions<BerechtigungDto> userPermissions) {
        final Collection<Long> sensorCocSchreibend = berechtigungToIds(userPermissions.getSensorCocAsSensorCocLeiterSchreibend());
        return anforderungBerechtigungDao.getAllAnforderungIdsForSensorCocIds(sensorCocSchreibend);
    }

    private Collection<Long> getAuthorizedAnforderungIdsForRoleSensor(UserPermissions<BerechtigungDto> userPermissions) {
        final Collection<Long> sensorCocSchreibend = berechtigungToIds(userPermissions.getSensorCocAsSensorSchreibend());
        final Collection<Long> sensorCocLesend = berechtigungToIds(userPermissions.getSensorCocAsSensorLesend());

        Collection<Long> allSensorCocIds = new HashSet<>(sensorCocSchreibend);
        allSensorCocIds.addAll(sensorCocLesend);

        return anforderungBerechtigungDao.getAllAnforderungIdsForSensorCocIds(allSensorCocIds);
    }

    private Collection<Long> getAuthorizedAnforderungIdsForRoleSensorEingeschraenkt(UserPermissions<BerechtigungDto> userPermissions, Mitarbeiter sensor) {
        final Collection<Long> allSensorCocIds = berechtigungToIds(userPermissions.getSensorCocAsSensorEingSchreibend());

        return anforderungBerechtigungDao.getAllAnforderungIdsForSensorCocIdsWithUserAsSensor(allSensorCocIds, sensor);
    }

    private static Collection<Long> berechtigungToIds(Collection<BerechtigungDto> berechtigungen) {
        return berechtigungen.stream().map(BerechtigungDto::getId).collect(Collectors.toList());
    }

}
