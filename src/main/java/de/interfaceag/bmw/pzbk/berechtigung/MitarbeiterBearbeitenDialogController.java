package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungEditDto;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.permissions.components.MitarbeiterBearbeiterDialogViewPermission;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.session.Session;
import org.primefaces.context.RequestContext;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Named
@ViewScoped
public class MitarbeiterBearbeitenDialogController implements Serializable {

    @Inject
    private Session session;

    @Inject
    private UserSearchService userSearchService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private UserPermissionsService userPermissionsService;
    @Inject
    private TteamService tteamService;

    private Mitarbeiter mitarbeiterToEdit;

    private final List<Rolle> rollenZuEntziehen = new ArrayList<>();

    private final List<BerechtigungEditDto> berechtigungenZuEntziehen = new ArrayList<>();

    private final List<Long> tteamZuEntziehenForTteamMitgliedSchreibend = new ArrayList<>();

    private final List<Long> tteamZuEntziehenForTteamMitgliedLesend = new ArrayList<>();

    private final List<Long> tteamZuEntziehenForTteamVertreter = new ArrayList();

    private UserPermissions<BerechtigungEditDto> userPermissions;

    private MitarbeiterBearbeiterDialogViewPermission viewPermission;

    private String urlParameter;

    // ---------- frontend methods ---------------------------------------------
    public void showMitarbeiterBearbeitenDialog(Long mitarbeiterId, String urlParameter) {
        this.urlParameter = urlParameter;
        updateDialogData(mitarbeiterId);
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialog:mitarbeiterBearbeitenForm");
        context.execute("PF('mitarbeiterBearbeitenDialog').show()");

    }

    public void clearRolleZuEntziehenList() {
        rollenZuEntziehen.clear();
        berechtigungenZuEntziehen.clear();
    }

    private void updateDialogData(Long mitarbeiterId) {
        this.mitarbeiterToEdit = userSearchService.getUniqueMitarbeiterById(mitarbeiterId);
        Set<Rolle> currentUserRoles = session.getUserPermissions().getRoles();
        Set<Long> sensorCocIds = session.getUserPermissions()
                .getSensorCocAsSensorCocLeiterSchreibend()
                .stream().map(BerechtigungDto::getId).collect(Collectors.toSet());
        this.userPermissions = userPermissionsService.getUserPermissionsWithEditPermissions(mitarbeiterToEdit, currentUserRoles, sensorCocIds);
        Set<Rolle> editUserRoles = this.userPermissions.getRoles();
        this.viewPermission = new MitarbeiterBearbeiterDialogViewPermission(editUserRoles, currentUserRoles);
    }

    // ----------- methods -----------------------------------------------------
    public String processChanges() {
        berechtigungService.removeBerechtigungen(berechtigungenZuEntziehen, mitarbeiterToEdit);
        berechtigungService.removeTteamMitgliedBerechtigungen(mitarbeiterToEdit, tteamZuEntziehenForTteamMitgliedLesend, tteamZuEntziehenForTteamMitgliedSchreibend);
        berechtigungService.removeTteamVertreterdBerechtigungen(mitarbeiterToEdit, tteamZuEntziehenForTteamVertreter);

        return urlParameter;
    }

    public boolean isTteamSelectedByMitgliedForDeleteWritable(String tteam) {
        if (tteam != null && !"".equals(tteam)) {
            return tteamZuEntziehenForTteamMitgliedSchreibend.contains(tteamService.getTteamByName(tteam).getId());
        } else {
            return Boolean.FALSE;
        }
    }

    public boolean isTteamSelectedByMitgliedForDeleteReadable(String tteam) {
        if (tteam != null && !"".equals(tteam)) {
            return tteamZuEntziehenForTteamMitgliedLesend.contains(tteamService.getTteamByName(tteam).getId());
        } else {
            return Boolean.FALSE;
        }
    }

    public boolean isTteamSelectedByVertreterForDelete(String tteam) {
        if (tteam != null && !"".equals(tteam)) {
            return tteamZuEntziehenForTteamVertreter.contains(tteamService.getTteamByName(tteam).getId());
        } else {
            return Boolean.FALSE;
        }
    }

    public boolean checkForTteamMitgliedWritingDeleteButton(String tteam) {
        if (tteam != null && !"".equals(tteam)) {
            return !isTteamSelectedByMitgliedForDeleteWritable(tteam) && hasTteamLeiterRightsToEdit(tteam);
        } else {
            return Boolean.FALSE;
        }
    }

    public boolean checkForTteamMitgliedReadingDeleteButton(String tteam) {
        if (tteam != null && !"".equals(tteam)) {
            return !isTteamSelectedByMitgliedForDeleteReadable(tteam) && hasTteamLeiterRightsToEdit(tteam);
        } else {
            return Boolean.FALSE;
        }
    }

    public boolean checkForTteamVertreterDeleteButton(String tteam) {
        if (tteam != null && !"".equals(tteam)) {
            return !isTteamSelectedByVertreterForDelete(tteam) && hasTteamLeiterRightsToEdit(tteam);
        } else {
            return Boolean.FALSE;
        }
    }

    public String resetChanges() {
        return urlParameter;
    }

    public void stripRoleAdmin() {
        addRolleZuEntziehen(Rolle.ADMIN);
        addBerechtigungToRemoveList(userPermissions.getAdminBerechtigung());
    }

    public void stripRoleSensor() {
        addRolleZuEntziehen(Rolle.SENSOR);
        addBerechtigungenToRemoveList(getSensorCocAsSensorLesend());
        addBerechtigungenToRemoveList(getSensorCocAsSensorSchreibend());
    }

    public void stripRoleSensorEingeschraenkt() {
        addRolleZuEntziehen(Rolle.SENSOR_EINGESCHRAENKT);
        addBerechtigungenToRemoveList(getSensorCocAsSensorEingSchreibend());
    }

    public void stripRoleSensorCoCLeiter() {
        addRolleZuEntziehen(Rolle.SENSORCOCLEITER);
        addBerechtigungenToRemoveList(getSensorCocAsSensorCocLeiterLesend());
        addBerechtigungenToRemoveList(getSensorCocAsSensorCocLeiterSchreibend());
        addBerechtigungenToRemoveList(getSensorCocAsUmsetzungsbestaetigerSchreibend(), Rolle.SENSORCOCLEITER);
    }

    public void stripRoleVertreterSCL() {
        addRolleZuEntziehen(Rolle.SCL_VERTRETER);
        addBerechtigungenToRemoveList(getSensorCocAsVertreterSCLLesend());
        addBerechtigungenToRemoveList(getSensorCocAsVertreterSCLSchreibend());
        addBerechtigungenToRemoveList(getSensorCocAsUmsetzungsbestaetigerSchreibend(), Rolle.SCL_VERTRETER);
    }

    public void stripRoleTteamleiter() {
        addRolleZuEntziehen(Rolle.T_TEAMLEITER);
        addBerechtigungenToRemoveList(getTteamAsTteamleiterSchreibend());
    }

    public void stripRoleECoC() {
        addRolleZuEntziehen(Rolle.E_COC);
        addBerechtigungenToRemoveList(getModulSeTeamAsEcocSchreibend());
    }

    public void stripRoleAnforderer() {
        addRolleZuEntziehen(Rolle.ANFORDERER);
        addBerechtigungenToRemoveList(getDerivatAsAnfordererSchreibend());
        addBerechtigungenToRemoveList(getZakEinordnungAsAnfordererSchreibend());
    }

    public void stripRoleUmsetzungsbestaetiger() {
        addRolleZuEntziehen(Rolle.UMSETZUNGSBESTAETIGER);
        addBerechtigungenToRemoveList(getSensorCocAsUmsetzungsbestaetigerSchreibend());
    }

    public void stripRoleLeser() {
        addBerechtigungenToRemoveList(getSensorCocAsLeser());
        addRolleZuEntziehen(Rolle.LESER);
    }

    public void stripRoleTteamMitglied() {
        addRolleZuEntziehen(Rolle.TTEAMMITGLIED);
        tteamZuEntziehenForTteamMitgliedLesend.addAll(userPermissions.getTteamIdsForTteamMitgliedLesend());
        tteamZuEntziehenForTteamMitgliedSchreibend.addAll(userPermissions.getTteamIdsForTteamMitgliedSchreibend());
    }

    public void stripeRoleTteamVertreter() {
        addRolleZuEntziehen(Rolle.TTEAM_VERTRETER);
        tteamZuEntziehenForTteamVertreter.addAll(userPermissions.getTteamIdsForTteamVertreter());
    }

    private void addRolleZuEntziehen(Rolle rolle) {
        rollenZuEntziehen.add(rolle);
    }

    private void addBerechtigungenToRemoveList(List<BerechtigungEditDto> berechtigungen) {
        berechtigungen.forEach(b -> b.setDelete(true));
        this.berechtigungenZuEntziehen.addAll(berechtigungen);
    }

    private void addBerechtigungenToRemoveList(List<BerechtigungEditDto> berechtigungen, Rolle rolle) {
        List<Long> sensorCocIds = new ArrayList<>();
        switch (rolle) {
            case SENSORCOCLEITER:
                sensorCocIdsForSensorCocLeiter(sensorCocIds);
                break;

            case SCL_VERTRETER:
                sensorCocIdsForSensorCocVertreter(sensorCocIds);
                break;
            default:
                break;
        }

        berechtigungen.stream().filter(b -> sensorCocIds.contains(b.getId()))
                .forEachOrdered(b -> {
                    b.setDelete(true);
                    this.berechtigungenZuEntziehen.add(b);
                });
    }

    private void sensorCocIdsForSensorCocVertreter(List<Long> sensorCocIds) {
        List<BerechtigungEditDto> berechtigungenForRolle;
        berechtigungenForRolle = this.getSensorCocAsVertreterSCLSchreibend();
        if (berechtigungenForRolle != null && !berechtigungenForRolle.isEmpty()) {
            sensorCocIds.addAll(berechtigungenForRolle.stream().map(BerechtigungDto::getId).collect(Collectors.toList()));
        }
    }

    private void sensorCocIdsForSensorCocLeiter(List<Long> sensorCocIds) {
        List<BerechtigungEditDto> berechtigungenForRolle;
        berechtigungenForRolle = this.getSensorCocAsSensorCocLeiterSchreibend();
        if (berechtigungenForRolle != null && !berechtigungenForRolle.isEmpty()) {
            sensorCocIds.addAll(berechtigungenForRolle.stream().map(BerechtigungDto::getId).collect(Collectors.toList()));
        }
    }

    public void addBerechtigungToRemoveList(BerechtigungEditDto berechtigungEditDto) {
        berechtigungEditDto.setDelete(Boolean.TRUE);
        this.berechtigungenZuEntziehen.add(berechtigungEditDto);
    }

    public void addTteamLesendToRemoveList(String tteamName) {
        this.tteamZuEntziehenForTteamMitgliedLesend.add(tteamService.getTteamByName(tteamName).getId());
    }

    public void addTteamVertreterToRemoveList(String tteamName) {
        this.tteamZuEntziehenForTteamVertreter.add(tteamService.getTteamByName(tteamName).getId());
    }

    public void addTteamSchreibendToRemoveList(String tteamName) {
        this.tteamZuEntziehenForTteamMitgliedSchreibend.add(tteamService.getTteamByName(tteamName).getId());
    }

    // ---------- getter / setter ----------------------------------------------
    public boolean getRemoveLeser() {
        return rollenZuEntziehen.contains(Rolle.LESER);
    }

    public boolean getRemoveUmsetzungsbestaetiger() {
        return rollenZuEntziehen.contains(Rolle.UMSETZUNGSBESTAETIGER);
    }

    public boolean getRemoveAnforderer() {
        return rollenZuEntziehen.contains(Rolle.ANFORDERER);
    }

    public boolean getRemoveSensorEing() {
        return rollenZuEntziehen.contains(Rolle.SENSOR_EINGESCHRAENKT);
    }

    public boolean getRemoveSensor() {
        return rollenZuEntziehen.contains(Rolle.SENSOR);
    }

    public boolean getRemoveAdmin() {
        return rollenZuEntziehen.contains(Rolle.ADMIN);
    }

    public boolean getRemoveSensorCoCLeiter() {
        return rollenZuEntziehen.contains(Rolle.SENSORCOCLEITER);
    }

    public boolean getRemoveVertreterSCL() {
        return rollenZuEntziehen.contains(Rolle.SCL_VERTRETER);
    }

    public boolean getRemoveTteamleiter() {
        return rollenZuEntziehen.contains(Rolle.T_TEAMLEITER);
    }

    public boolean getRemoveEcoc() {
        return rollenZuEntziehen.contains(Rolle.E_COC);
    }

    public boolean getRemoveTteamMitglied() {
        return rollenZuEntziehen.contains(Rolle.TTEAMMITGLIED);
    }

    public boolean getRemoveTteamVertreter() {
        return rollenZuEntziehen.contains(Rolle.TTEAM_VERTRETER);
    }

    public Mitarbeiter getMitarbeiterToEdit() {
        return mitarbeiterToEdit;
    }

    // ---------- new methods --------------------------------------------------
    public List<Rolle> getRollen() {
        return userPermissions.getRollen();
    }

    public List<BerechtigungEditDto> getSensorCocAsLeser() {
        return userPermissions.getSensorCocAsLeser();
    }

    public List<BerechtigungEditDto> getSensorCocAsSensorSchreibend() {
        return userPermissions.getSensorCocAsSensorSchreibend();
    }

    public List<BerechtigungEditDto> getSensorCocAsSensorLesend() {
        return userPermissions.getSensorCocAsSensorLesend();
    }

    public List<BerechtigungEditDto> getSensorCocAsSensorEingSchreibend() {
        return userPermissions.getSensorCocAsSensorEingSchreibend();
    }

    public List<BerechtigungEditDto> getSensorCocAsSensorCocLeiterSchreibend() {
        return userPermissions.getSensorCocAsSensorCocLeiterSchreibend();
    }

    public List<BerechtigungEditDto> getSensorCocAsSensorCocLeiterLesend() {
        return userPermissions.getSensorCocAsSensorCocLeiterLesend();
    }

    public List<BerechtigungEditDto> getSensorCocAsVertreterSCLSchreibend() {
        return userPermissions.getSensorCocAsVertreterSCLSchreibend();
    }

    public List<BerechtigungEditDto> getSensorCocAsVertreterSCLLesend() {
        return userPermissions.getSensorCocAsVertreterSCLLesend();
    }

    public List<BerechtigungEditDto> getSensorCocAsUmsetzungsbestaetigerSchreibend() {
        return userPermissions.getSensorCocAsUmsetzungsbestaetigerSchreibend();
    }

    public List<BerechtigungEditDto> getTteamAsTteamleiterSchreibend() {
        return userPermissions.getTteamAsTteamleiterSchreibend();
    }

    public List<BerechtigungEditDto> getDerivatAsAnfordererSchreibend() {
        return userPermissions.getDerivatAsAnfordererSchreibend();
    }

    public List<BerechtigungEditDto> getModulSeTeamAsEcocSchreibend() {
        return userPermissions.getModulSeTeamAsEcocSchreibend();
    }

    public List<BerechtigungEditDto> getZakEinordnungAsAnfordererSchreibend() {
        return userPermissions.getZakEinordnungAsAnfordererSchreibend();
    }

    public List<String> getTteamMitgliedLesend() {
        return tteamService.getTteamByIdList(userPermissions.getTteamIdsForTteamMitgliedLesend()).stream().map(Tteam::getTeamName).collect(Collectors.toList());

    }

    public List<String> getTteamVertreterTteamNames() {
        return tteamService.getTteamByIdList(userPermissions.getTteamIdsForTteamVertreter()).stream().map(Tteam::getTeamName).collect(Collectors.toList());
    }

    public boolean hasTteamLeiterRightsToEdit(String string) {
        if (session.hasRole(Rolle.ADMIN)) {
            return true;
        }
        List<Tteam> tteamBerechtigungCurrentUser = berechtigungService.getTteamsForMitarbeiter(session.getUser());
        return tteamBerechtigungCurrentUser.stream().anyMatch(tteam -> (tteam.getTeamName().equals(string)));
    }

    public List<String> getTteamMitgliedSchreibend() {
        return tteamService.getTteamByIdList(userPermissions.getTteamIdsForTteamMitgliedSchreibend()).stream().map(Tteam::getTeamName).collect(Collectors.toList());
    }

    public MitarbeiterBearbeiterDialogViewPermission getViewPermission() {
        return viewPermission;
    }

}
