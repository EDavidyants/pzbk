package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.berechtigung.dao.TteamMitgliedDAO;
import de.interfaceag.bmw.pzbk.berechtigung.dao.TteamVertreterDAO;
import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungEditDto;
import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungEditDtoImpl;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamMitgliedBerechtigung;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamVertreterBerechtigung;
import de.interfaceag.bmw.pzbk.berechtigung.dto.UserPermissionsDTO;
import de.interfaceag.bmw.pzbk.entities.Berechtigung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.SensorCocRolePermission;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class UserPermissionsService implements Serializable {

    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private TteamMitgliedDAO tteamMitgliedDao;
    @Inject
    private TteamVertreterDAO tteamVertreterDao;

    public UserPermissions<BerechtigungEditDtoImpl> getUserPermissions(Mitarbeiter mitarbeiter) {

        List<Rolle> rollen = berechtigungService.getRolesForUser(mitarbeiter);
        List<BerechtigungEditDtoImpl> berechtigungen = getBerechtigungenForMitarbeiter(mitarbeiter, rollen);

        Optional<TteamMitgliedBerechtigung> tteamMitgliedBerechtigung = Optional.empty();
        if (rollen.contains(Rolle.TTEAMMITGLIED)) {
            tteamMitgliedBerechtigung = getTteamMitgliedBerechtigung(mitarbeiter);
        }

        Optional<TteamVertreterBerechtigung> tteamVertreterBerechtigung = Optional.empty();
        if (rollen.contains(Rolle.TTEAM_VERTRETER)) {
            tteamVertreterBerechtigung = getTeamVertreterBerechtigung(mitarbeiter);
        }

        return new UserPermissionsDTO<>(rollen, berechtigungen, tteamMitgliedBerechtigung.orElse(null), tteamVertreterBerechtigung.orElse(null));
    }

    public UserPermissions<BerechtigungEditDto> getUserPermissionsWithEditPermissions(Mitarbeiter mitarbeiter, Set<Rolle> userRoles, Set<Long> sensorCocIds) {
        List<BerechtigungEditDto> berechtigungen = getBerechtigungenWithEditPermissionsForMitarbeiter(mitarbeiter, userRoles, sensorCocIds);
        List<Rolle> rollen = berechtigungService.getRolesForUser(mitarbeiter);

        Optional<TteamMitgliedBerechtigung> tteamMitgliedBerechtigung = Optional.empty();
        if (rollen.contains(Rolle.TTEAMMITGLIED)) {
            tteamMitgliedBerechtigung = getTteamMitgliedBerechtigung(mitarbeiter);
        }

        Optional<TteamVertreterBerechtigung> tteamVertreterBerechtigung = Optional.empty();
        if (rollen.contains(Rolle.TTEAM_VERTRETER)) {
            tteamVertreterBerechtigung = getTeamVertreterBerechtigung(mitarbeiter);
        }

        return new UserPermissionsDTO<>(rollen, berechtigungen, tteamMitgliedBerechtigung.orElse(null), tteamVertreterBerechtigung.orElse(null));
    }

    private List<BerechtigungEditDtoImpl> getBerechtigungenForMitarbeiter(Mitarbeiter mitarbeiter, List<Rolle> userRoles) {
        List<Berechtigung> berechtigungen = berechtigungService.getBerechtigungByMitarbeiter(mitarbeiter);
        List<BerechtigungEditDtoImpl> result = new ArrayList<>();
        result.addAll(getSensorCocBerechtigungen(berechtigungen));
        result.addAll(getZakEinordnungBerechtigungen(berechtigungen));
        result.addAll(getTteamBerechtigungen(berechtigungen, mitarbeiter));
        result.addAll(getModulSeTeamBerechtigungen(berechtigungen, mitarbeiter));
        result.addAll(getDerivatBerechtigungen(mitarbeiter));
        if (userRoles.contains(Rolle.ADMIN)) {
            result.add(new BerechtigungEditDtoImpl(null, "Admin", BerechtigungZiel.SYSTEM,
                    Rechttype.KEIN, Rolle.ADMIN));
        }
        return result;
    }

    private List<BerechtigungEditDto> getBerechtigungenWithEditPermissionsForMitarbeiter(Mitarbeiter mitarbeiter, Set<Rolle> userRoles, Set<Long> sensorCocIds) {
        List<Berechtigung> berechtigungen = berechtigungService.getBerechtigungByMitarbeiter(mitarbeiter);
        List<BerechtigungEditDto> result = new ArrayList<>();
        result.addAll(getSensorCocBerechtigungenWithEditPermissions(berechtigungen, userRoles, sensorCocIds));
        result.addAll(getZakEinordnungBerechtigungenWithEditPermissions(berechtigungen, userRoles));
        result.addAll(getTteamBerechtigungenWithEditPermissions(berechtigungen, mitarbeiter, userRoles));
        result.addAll(getModulSeTeamBerechtigungenWithEditPermissions(berechtigungen, mitarbeiter, userRoles));
        result.addAll(getDerivatBerechtigungenWithEditPermissions(mitarbeiter, userRoles));
        if (userRoles.contains(Rolle.ADMIN)) {
            result.add(new BerechtigungEditDtoImpl(null, "Admin", BerechtigungZiel.SYSTEM,
                    Rechttype.KEIN, Rolle.ADMIN, getAdminEditPermission(userRoles)));
        }
        if (userRoles.contains(Rolle.TTEAMMITGLIED)
                && tteamMitgliedDao.getTteamIdsforTteamMitgliedAndRechttype(mitarbeiter, Rechttype.SCHREIBRECHT.getId()) != null) {
            result.add(new BerechtigungEditDtoImpl(null, "T-Teammitglied", BerechtigungZiel.TTEAM, Rechttype.SCHREIBRECHT, Rolle.TTEAMMITGLIED));
        }
        return result;
    }

    private EditPermission getAdminEditPermission(Set<Rolle> userRoles) {
        return RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN).compareTo(userRoles).get();
    }

    private EditPermission getSensorCocEditPermission(Set<Rolle> userRoles,
                                                      Set<Long> sensorCocIds, Long sensorCocId) {
        return SensorCocRolePermission.builder()
                .compareWithSensorCocId(sensorCocId)
                .forSensorCocidsAsVerantwortlicherSensorCocLeiter(sensorCocIds)
                .forUserRolesWithWritePermissions(userRoles).get();
    }

    private List<BerechtigungEditDtoImpl> getSensorCocBerechtigungen(List<Berechtigung> berechtigungen) {
        List<SensorCoc> sensorCocs = sensorCocService.getSensorCocsByIdList(berechtigungen.stream()
                .filter(berechtigung -> berechtigung.getFuer().equals(BerechtigungZiel.SENSOR_COC))
                .map(berechtigung -> Long.parseLong(berechtigung.getFuerId())).collect(Collectors.toList()));

        List<BerechtigungEditDtoImpl> result = new ArrayList<>();
        berechtigungen.stream().filter(berechtigung -> berechtigung.getFuer().equals(BerechtigungZiel.SENSOR_COC)).forEach(berechtigung -> {
            Optional<SensorCoc> sensorCoc = sensorCocs.stream()
                    .filter(sensor -> sensor.getSensorCocId() == Long.parseLong(berechtigung.getFuerId())).findFirst();
            if (sensorCoc.isPresent()) {
                result.add(new BerechtigungEditDtoImpl(
                        sensorCoc.get().getSensorCocId(), sensorCoc.get().pathToStringShort(),
                        berechtigung.getFuer(), berechtigung.getRechttype(), berechtigung.getRolle())
                );
            }

        });
        return result;
    }

    private List<BerechtigungEditDto> getSensorCocBerechtigungenWithEditPermissions(List<Berechtigung> berechtigungen,
                                                                                    Set<Rolle> userRoles, Set<Long> sensorCocIds) {
        List<SensorCoc> sensorCocs = sensorCocService.getSensorCocsByIdList(berechtigungen.stream()
                .filter(b -> b.getFuer().equals(BerechtigungZiel.SENSOR_COC))
                .map(b -> Long.parseLong(b.getFuerId())).collect(Collectors.toList()));

        List<BerechtigungEditDto> result = new ArrayList<>();
        berechtigungen.stream().filter(b -> b.getFuer().equals(BerechtigungZiel.SENSOR_COC)).forEach(b -> {
            Optional<SensorCoc> sensorCoc = sensorCocs.stream()
                    .filter(s -> s.getSensorCocId() == Long.parseLong(b.getFuerId())).findFirst();
            if (sensorCoc.isPresent()) {

                EditPermission editPermission = getSensorCocEditPermission(userRoles,
                        sensorCocIds, sensorCoc.get().getSensorCocId());

                result.add(new BerechtigungEditDtoImpl(
                        sensorCoc.get().getSensorCocId(), sensorCoc.get().pathToStringShort(),
                        b.getFuer(), b.getRechttype(), b.getRolle(), editPermission)
                );
            }

        });
        return result;
    }

    private List<BerechtigungEditDtoImpl> getTteamBerechtigungen(List<Berechtigung> berechtigungen, Mitarbeiter mitarbeiter) {
        List<Tteam> tteams = berechtigungService.getAuthorizedTteamsForMitarbeiter(mitarbeiter);

        List<BerechtigungEditDtoImpl> result = new ArrayList<>();
        berechtigungen.stream().filter(berechtigung -> berechtigung.getFuer().equals(BerechtigungZiel.TTEAM)).forEach(berechtigung -> {
            Optional<Tteam> tteam = tteams.stream()
                    .filter(team -> team.getId() == Long.parseLong(berechtigung.getFuerId())).findFirst();
            if (tteam.isPresent()) {
                result.add(new BerechtigungEditDtoImpl(tteam.get().getId(), tteam.get().getTeamName(),
                        berechtigung.getFuer(), berechtigung.getRechttype(), berechtigung.getRolle()));
            }

        });
        return result;
    }

    private List<BerechtigungEditDto> getTteamBerechtigungenWithEditPermissions(List<Berechtigung> berechtigungen,
                                                                                Mitarbeiter mitarbeiter, Set<Rolle> userRoles) {
        List<Tteam> tteams = berechtigungService.getAuthorizedTteamsForMitarbeiter(mitarbeiter);

        EditPermission editPermission = getAdminEditPermission(userRoles);

        List<BerechtigungEditDto> result = new ArrayList<>();
        berechtigungen.stream().filter(b -> b.getFuer().equals(BerechtigungZiel.TTEAM)).forEach(b -> {
            Optional<Tteam> tteam = tteams.stream()
                    .filter(s -> s.getId() == Long.parseLong(b.getFuerId())).findFirst();
            if (tteam.isPresent()) {
                result.add(new BerechtigungEditDtoImpl(tteam.get().getId(), tteam.get().getTeamName(),
                        b.getFuer(), b.getRechttype(), b.getRolle(), editPermission));
            }

        });
        return result;
    }

    private List<BerechtigungEditDtoImpl> getModulSeTeamBerechtigungen(List<Berechtigung> berechtigungen,
                                                                       Mitarbeiter mitarbeiter) {
        List<ModulSeTeam> seTeams = berechtigungService.getAuthorizedModulSeTeamListForMitarbeiter(mitarbeiter);

        List<BerechtigungEditDtoImpl> result = new ArrayList<>();
        berechtigungen.stream().filter(berechtigung -> berechtigung.getFuer().equals(BerechtigungZiel.MODULSETEAM)).forEach(berechtigung -> {
            Optional<ModulSeTeam> modulSeTeam = seTeams.stream()
                    .filter(modulseTeam -> modulseTeam.getId() == Long.parseLong(berechtigung.getFuerId())).findFirst();
            if (modulSeTeam.isPresent()) {
                result.add(new BerechtigungEditDtoImpl(modulSeTeam.get().getId(), modulSeTeam.get().getName(),
                        berechtigung.getFuer(), berechtigung.getRechttype(), berechtigung.getRolle()));
            }

        });
        return result;
    }

    private List<BerechtigungEditDto> getModulSeTeamBerechtigungenWithEditPermissions(List<Berechtigung> berechtigungen,
                                                                                      Mitarbeiter mitarbeiter, Set<Rolle> userRoles) {
        List<ModulSeTeam> seTeams = berechtigungService.getAuthorizedModulSeTeamListForMitarbeiter(mitarbeiter);

        EditPermission editPermission = getAdminEditPermission(userRoles);

        List<BerechtigungEditDto> result = new ArrayList<>();
        berechtigungen.stream().filter(b -> b.getFuer().equals(BerechtigungZiel.MODULSETEAM)).forEach(b -> {
            Optional<ModulSeTeam> seTeam = seTeams.stream()
                    .filter(s -> s.getId() == Long.parseLong(b.getFuerId())).findFirst();
            if (seTeam.isPresent()) {
                result.add(new BerechtigungEditDtoImpl(seTeam.get().getId(), seTeam.get().getName(),
                        b.getFuer(), b.getRechttype(), b.getRolle(), editPermission));
            }

        });
        return result;
    }

    private List<BerechtigungEditDtoImpl> getDerivatBerechtigungen(Mitarbeiter mitarbeiter) {
        List<Derivat> derivate = berechtigungService.getAuthorizedDerivatListByMitarbeiterAndRolle(mitarbeiter, Rolle.ANFORDERER);

        List<BerechtigungEditDtoImpl> result = new ArrayList<>();
        derivate.forEach(derivat -> result.add(new BerechtigungEditDtoImpl(derivat.getId(), derivat.getName(),
                BerechtigungZiel.DERIVAT, Rechttype.SCHREIBRECHT, Rolle.ANFORDERER)));

        return result;
    }

    private List<BerechtigungEditDto> getDerivatBerechtigungenWithEditPermissions(Mitarbeiter mitarbeiter,
                                                                                  Set<Rolle> userRoles) {
        List<Derivat> derivate = berechtigungService.getAuthorizedDerivatListByMitarbeiterAndRolle(mitarbeiter, Rolle.ANFORDERER);

        EditPermission editPermission = getAdminEditPermission(userRoles);

        List<BerechtigungEditDto> result = new ArrayList<>();
        derivate.forEach(d -> result.add(new BerechtigungEditDtoImpl(d.getId(), d.getName(),
                BerechtigungZiel.DERIVAT, Rechttype.SCHREIBRECHT, Rolle.ANFORDERER, editPermission)));

        return result;
    }

    private List<BerechtigungEditDtoImpl> getZakEinordnungBerechtigungen(List<Berechtigung> berechtigungen) {
        return berechtigungen.stream()
                .filter(berechtigung -> berechtigung.getFuer().equals(BerechtigungZiel.ZAK_EINORDNUNG))
                .map(berechtigung -> new BerechtigungEditDtoImpl(null, berechtigung.getFuerId(), berechtigung.getFuer(), berechtigung.getRechttype(),
                        berechtigung.getRolle()))
                .collect(Collectors.toList());
    }

    private List<BerechtigungEditDto> getZakEinordnungBerechtigungenWithEditPermissions(List<Berechtigung> berechtigungen,
                                                                                        Set<Rolle> userRoles) {
        EditPermission editPermission = getAdminEditPermission(userRoles);

        return berechtigungen.stream()
                .filter(b -> b.getFuer().equals(BerechtigungZiel.ZAK_EINORDNUNG))
                .map(b -> new BerechtigungEditDtoImpl(null, b.getFuerId(), b.getFuer(), b.getRechttype(),
                        b.getRolle(), editPermission))
                .collect(Collectors.toList());
    }

    public Optional<TteamMitgliedBerechtigung> getTteamMitgliedBerechtigung(Mitarbeiter mitarbeiter) {
        List<Long> schreibrechte = tteamMitgliedDao.getTteamIdsforTteamMitgliedAndRechttype(mitarbeiter, Rechttype.SCHREIBRECHT.getId());
        List<Long> leserechte = tteamMitgliedDao.getTteamIdsforTteamMitgliedAndRechttype(mitarbeiter, Rechttype.LESERECHT.getId());
        return Optional.ofNullable(new TteamMitgliedBerechtigung(schreibrechte, leserechte));
    }

    public Optional<TteamVertreterBerechtigung> getTeamVertreterBerechtigung(Mitarbeiter mitarbeiter) {
        List<Long> tteamIds = tteamVertreterDao.getTteamIdsforTteamVertreter(mitarbeiter);
        return Optional.ofNullable(new TteamVertreterBerechtigung(tteamIds));
    }
}
