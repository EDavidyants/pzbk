package de.interfaceag.bmw.pzbk.berechtigung.dto;

import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author fn
 */
public class MitarbeiterDTO implements Serializable {

    private final Long id;
    private final String vorname;
    private final String nachname;
    private final String abteilung;
    private final List<Rolle> rollen;
    private final String qNumber;

    public MitarbeiterDTO(Long id, String qNumber, String vorname, String nachname, String abteilung, List<Rolle> rollen) {
        this.id = id;
        this.vorname = vorname;
        this.nachname = nachname;
        this.abteilung = abteilung;
        this.rollen = rollen;
        this.qNumber = qNumber;
    }

    public Long getId() {
        return id;
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public String getAbteilung() {
        return abteilung;
    }

    public List<Rolle> getRollen() {
        return rollen;
    }

    public String getQNumber() {
        return qNumber;
    }

}
