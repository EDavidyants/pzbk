package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

@Dependent
public class AnforderungBerechtigungDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public Collection<Long> getAllAnforderungIds() {
        QueryPart queryPart = new QueryPartDTO("SELECT a.id FROM Anforderung a");
        return getQueryResult(queryPart);
    }

    public Collection<Long> getAllAnforderungIdsForSensorCocIdsWithUserAsSensor(Collection<Long> sensorCocIds, Mitarbeiter sensor) {
        if (sensorCocIds.isEmpty()) {
            return Collections.emptyList();
        }

        QueryPart queryPart = new QueryPartDTO("SELECT a.id FROM Anforderung a WHERE a.sensorCoc.sensorCocId IN :sensorCocIds AND a.sensor = :sensor");
        queryPart.put("sensorCocIds", sensorCocIds);
        queryPart.put("sensor", sensor);
        return getQueryResult(queryPart);
    }

    public Collection<Long> getAllAnforderungIdsForSensorCocIds(Collection<Long> sensorCocIds) {
        if (sensorCocIds.isEmpty()) {
            return Collections.emptyList();
        }

        QueryPart queryPart = new QueryPartDTO("SELECT a.id FROM Anforderung a WHERE a.sensorCoc.sensorCocId IN :sensorCocIds");
        queryPart.put("sensorCocIds", sensorCocIds);
        return getQueryResult(queryPart);
    }

    public Collection<Long> getAllAnforderungIdsForModulSeTeamIds(Collection<Long> modulSeTeamIds) {
        if (modulSeTeamIds.isEmpty()) {
            return Collections.emptyList();
        }

        QueryPart queryPart = new QueryPartDTO("SELECT a.id FROM Anforderung a INNER JOIN a.umsetzer u WHERE u.seTeam.id IN :modulSeTeamIds");
        queryPart.put("modulSeTeamIds", modulSeTeamIds);
        return getQueryResult(queryPart);
    }

    public Collection<Long> getAllAnforderungIdsForTteamIds(Collection<Long> tteamIds) {
        if (tteamIds.isEmpty()) {
            return Collections.emptyList();
        }

        QueryPart queryPart = new QueryPartDTO("SELECT a.id FROM Anforderung a WHERE a.tteam.id IN :tteamIds ");
        queryPart.put("tteamIds", tteamIds);
        return getQueryResult(queryPart);
    }

    private Collection<Long> getQueryResult(QueryPart queryPart) {
        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPart, entityManager, Long.class);
        return query.getResultList();
    }

}
