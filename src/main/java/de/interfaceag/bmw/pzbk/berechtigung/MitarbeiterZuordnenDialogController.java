package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamMitgliedBerechtigung;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamVertreterBerechtigung;
import de.interfaceag.bmw.pzbk.converters.DerivatConverter;
import de.interfaceag.bmw.pzbk.converters.ModulSeTeamConverter;
import de.interfaceag.bmw.pzbk.converters.SensorCocConverter;
import de.interfaceag.bmw.pzbk.converters.TteamConverter;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.comparator.DerivatComparator;
import de.interfaceag.bmw.pzbk.entities.comparator.RollenComparator;
import de.interfaceag.bmw.pzbk.entities.comparator.SensorCocComparator;
import de.interfaceag.bmw.pzbk.entities.comparator.TteamComparator;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.exceptions.UserNotFoundException;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.services.UserService;
import de.interfaceag.bmw.pzbk.session.Session;
import org.primefaces.component.wizard.Wizard;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl
 */
@Dependent
@Named
public class MitarbeiterZuordnenDialogController implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(MitarbeiterZuordnenDialogController.class);

    public static final String ROLLE_AUSWAHL_TAB = "rolleAuswahlTab";
    public static final String DIALOG_MITARBEITER_ZUORDNEN_FORM_MITARBEITER_ZUORDNEN_DLG_CONTENT_BERECHTIGUNG_WIZARD = "dialog:mitarbeiterZuordnenForm:mitarbeiterZuordnenDlgContent:berechtigungWizard";
    public static final String DIALOG_MITARBEITER_ZUORDNEN_FORM = "dialog:mitarbeiterZuordnenForm";

    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private TteamService tteamService;
    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private ModulService modulService;
    @Inject
    private UserSearchService userSearchService;
    @Inject
    private UserService userService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private Session session;

    // wizard fields
    // user search fields
    private String vornameZuSuchen;
    private String nachnameZuSuchen;
    private String abteilungZuSuchen;
    private List<Mitarbeiter> gefundeneMitarbeiters;
    private Mitarbeiter selectedMitarbeiter;

    private Boolean addAnotherRoleBoolean;
    private Boolean mitarbeiterAuswahlRenderCondition;

    // role
    private Rolle aktuelleRolle;
    private List<Rolle> rollen;
    // sensor coc
    private List<SensorCoc> allSensorCoc;
    private List<SensorCoc> sensorCocs;
    private List<SensorCoc> sensorCocsRo;
    private List<SensorCoc> selectedSensorCocs;
    private List<SensorCoc> selectedSensorCocsRO;
    // tteam
    private List<Tteam> tteams;
    private List<Tteam> selectedTteams;

    private List<Tteam> tteamMitgliedList;
    private List<Tteam> tteamMitgliedRoList;
    private List<Tteam> selectedTteamsForMitglied;
    private List<Tteam> selectedROTteamsForMitglied;

    private List<Tteam> selectedTteamsForVertreter;
    // modul
    private List<ModulSeTeam> modules;
    private List<ModulSeTeam> selectedModules;
    // derivat
    private List<Derivat> derivate;
    private List<Derivat> selectedDerivate;
    // zak einordnung
    private List<String> zakEinordnungen;
    private List<String> selectedZakEinordnungen;
    // user lists
    private List<Tteam> userTteams = new ArrayList<>();
    private List<Tteam> userROMitgliedTteams = new ArrayList<>();
    private List<Tteam> userMitgliedTteams = new ArrayList<>();

    private List<Tteam> userTteamVertreterTeams = new ArrayList<>();

    private List<Derivat> userDerivate;
    private List<String> userZakEinordnungen;
    private List<ModulSeTeam> userModule = new ArrayList<>();

    private List<SensorCoc> userSensorCocSchreibend = new ArrayList<>();
    private List<SensorCoc> userSensorCocLesend = new ArrayList<>();

    @PostConstruct
    public void init() {

        this.gefundeneMitarbeiters = new ArrayList<>();
        this.selectedTteams = new ArrayList<>();
        this.selectedROTteamsForMitglied = new ArrayList<>();
        this.selectedTteamsForMitglied = new ArrayList<>();
        this.selectedSensorCocs = new ArrayList<>();
        this.selectedSensorCocsRO = new ArrayList<>();
        this.selectedModules = new ArrayList<>();
        this.selectedDerivate = new ArrayList<>();
        this.userDerivate = new ArrayList<>();
        this.userZakEinordnungen = new ArrayList<>();
        this.addAnotherRoleBoolean = false;
        this.mitarbeiterAuswahlRenderCondition = true;

        initRollen(berechtigungService.getAllRoles());
    }


    private void initRollen(List<Rolle> rollen) {
        this.rollen = new ArrayList<>();
        if (session.hasRole(Rolle.ADMIN)) {
            rollen.stream().filter(rolle -> !(rolle.getBezeichnung().equals("Leser") || rolle.getBezeichnung().equals("Umsetzer"))).forEachOrdered(rolle ->
                    this.rollen.add(rolle)
            );
        } else if (session.hasRole(Rolle.SENSORCOCLEITER)) {
            this.rollen.add(Rolle.UMSETZUNGSBESTAETIGER);
            this.rollen.add(Rolle.SENSOR);
            this.rollen.add(Rolle.SENSOR_EINGESCHRAENKT);
            this.rollen.add(Rolle.SCL_VERTRETER);
        } else if (session.hasRole(Rolle.T_TEAMLEITER)) {
            this.rollen.add(Rolle.TTEAMMITGLIED);
            this.rollen.add(Rolle.TTEAM_VERTRETER);
        }
        // init aktuelle rolle for render condition

        this.rollen.sort(new RollenComparator());
    }

    private void initTteams() {
        this.tteams = new ArrayList<>();
        this.tteams = tteamService.getAllTteams();
        tteams.sort(new TteamComparator());
    }

    private void initTteamsForMitglied() {
        if (session.hasRole(Rolle.ADMIN)) {
            initTteams();
        } else if (session.hasRole(Rolle.T_TEAMLEITER)) {
            this.tteams = tteamService.getTteamByTeamleiter(session.getUser());

        }
        this.tteamMitgliedList = new ArrayList<>(tteams);
        this.tteamMitgliedRoList = new ArrayList<>(tteams);
    }

    private void initTteamsForTteamVertreter() {
        if (session.hasRole(Rolle.ADMIN)) {
            initTteams();
        } else if (session.hasRole(Rolle.T_TEAMLEITER)) {
            this.tteams = tteamService.getTteamByTeamleiter(session.getUser());
        }
    }

    private void initSensorCocs() {
        this.sensorCocs = new ArrayList<>();
        this.allSensorCoc = new ArrayList<>();
        this.sensorCocsRo = new ArrayList<>();
        if (session.hasRole(Rolle.ADMIN)) {
            this.sensorCocs = sensorCocService.getAllSortByTechnologie();
        } else if (session.hasRole(Rolle.SENSORCOCLEITER)) {
            this.sensorCocs = sensorCocService.getSensorCocsByRessortTechnologieOrtungByRole(null, null, null);
        }
        this.sensorCocs.sort(new SensorCocComparator(true));
        this.sensorCocsRo.addAll(sensorCocs);
        this.sensorCocsRo.sort(new SensorCocComparator(true));
        this.allSensorCoc.addAll(sensorCocs);
    }

    private void initModules() {
        this.modules = new ArrayList<>();
        this.setModules(modulService.getAllSeTeams());
    }

    private void initDerivate() {
        this.derivate = new ArrayList<>();
        this.setDerivate(derivatService.getAllDerivate());
        this.derivate.sort(new DerivatComparator());
    }

    private void initZakEinordnungen() {
        this.zakEinordnungen = new ArrayList<>();
        this.setZakEinordnungen(sensorCocService.getAllZakEinordnung());
        Collections.sort(zakEinordnungen);
    }

    private void initTteamMitglied(Mitarbeiter mitarbeiter) {

        Optional<TteamMitgliedBerechtigung> berechtigung = berechtigungService.getTteamMitgliedBerechtigungForUser(mitarbeiter);
        if (berechtigung.isPresent()) {
            this.userMitgliedTteams = tteamService.getTteamByIdList(berechtigung.get().getTteamIdsMitSchreibrechten());
            this.userROMitgliedTteams = tteamService.getTteamByIdList(berechtigung.get().getTteamIdsMitLeserechten());
            this.selectedTteamsForMitglied = new ArrayList(userMitgliedTteams);
            this.selectedROTteamsForMitglied = new ArrayList(userROMitgliedTteams);
        }
    }

    private void initTteamVertreter(Mitarbeiter mitarbeiter) {
        Optional<TteamVertreterBerechtigung> berechtigung = berechtigungService.getTteamVertreterBerechtigungForUser(mitarbeiter);
        if (berechtigung.isPresent()) {
            this.userTteamVertreterTeams = tteamService.getTteamByIdList(berechtigung.get().getTteamIdsMitSchreibrechten());
            this.selectedTteamsForVertreter = new ArrayList(userTteamVertreterTeams);
        }
    }

    public void showMitarbeiterZuordnenDialog() {
        resetInputFields();
        mitarbeiterAuswahlRenderCondition = true;
        RequestContext context = RequestContext.getCurrentInstance();
        Wizard wizard = (Wizard) FacesContext.getCurrentInstance().getViewRoot().findComponent(DIALOG_MITARBEITER_ZUORDNEN_FORM_MITARBEITER_ZUORDNEN_DLG_CONTENT_BERECHTIGUNG_WIZARD);
        wizard.setStep("mitarbeiterAuswahlTab");
        context.update(DIALOG_MITARBEITER_ZUORDNEN_FORM);
        context.execute("PF('mitarbeiterZuordnenDialog').show()");
    }

    void showAddZuordnungDialog(Mitarbeiter mitarbeiter) {

        mitarbeiterAuswahlRenderCondition = false;


        setSelectedMitarbeiter(mitarbeiter);

        initAddSensorCocs(mitarbeiter);

        initAddTteams(mitarbeiter);

        initTteamMitglied(mitarbeiter);

        initTteamVertreter(mitarbeiter);

        initAddModules(mitarbeiter);

        initAddDerivate(mitarbeiter);

        initAddZakEinordung(mitarbeiter);

        RequestContext context = RequestContext.getCurrentInstance();
        Wizard wizard = (Wizard) FacesContext.getCurrentInstance().getViewRoot().findComponent(DIALOG_MITARBEITER_ZUORDNEN_FORM_MITARBEITER_ZUORDNEN_DLG_CONTENT_BERECHTIGUNG_WIZARD);
        wizard.setStep(ROLLE_AUSWAHL_TAB);

        context.update(DIALOG_MITARBEITER_ZUORDNEN_FORM);
        context.execute("PF('mitarbeiterZuordnenDialog').show()");
    }

    private void initAddSensorCocs(Mitarbeiter mitarbeiter) {
        this.sensorCocs = new ArrayList<>();
        this.allSensorCoc = new ArrayList<>();
        this.sensorCocsRo = new ArrayList<>();
        this.selectedSensorCocs = new ArrayList<>();
        this.selectedSensorCocsRO = new ArrayList<>();
        userSensorCocSchreibend = berechtigungService.getSensorCocForMitarbeiterForRoleEdit(mitarbeiter, true);
        userSensorCocLesend = berechtigungService.getSensorCocForMitarbeiterForRoleEdit(mitarbeiter, false);
        this.selectedSensorCocs.addAll(userSensorCocSchreibend);
        this.sensorCocs.removeAll(userSensorCocLesend);
        this.selectedSensorCocsRO.addAll(userSensorCocLesend);
        this.sensorCocsRo.removeAll(userSensorCocSchreibend);
    }

    private void initAddTteams(Mitarbeiter mitarbeiter) {
        this.selectedTteams = new ArrayList<>();
        userTteams = berechtigungService.getTteamsForMitarbeiterForZuordnenDialog(mitarbeiter);
        this.selectedTteams.addAll(userTteams);
    }

    private void initAddModules(Mitarbeiter mitarbeiter) {
        this.selectedModules = new ArrayList<>();
        userModule = berechtigungService.getAuthorizedModulSeTeamListForMitarbeiter(mitarbeiter);
        this.selectedModules.addAll(userModule);
    }

    private void initAddDerivate(Mitarbeiter mitarbeiter) {
        this.selectedDerivate = new ArrayList<>();
        userDerivate = berechtigungService.getAuthorizedDerivatListByMitarbeiterAndRolle(mitarbeiter, Rolle.ANFORDERER);
        this.selectedDerivate.addAll(userDerivate);
    }

    private void initAddZakEinordung(Mitarbeiter mitarbeiter) {
        this.selectedZakEinordnungen = new ArrayList<>();
        userZakEinordnungen = berechtigungService.getAuthorizedZakEinordnungenForMitarbeiter(mitarbeiter);
        this.selectedZakEinordnungen.addAll(userZakEinordnungen);
    }

    public void updateSensorCocRoList() {
        this.sensorCocsRo.clear();
        this.sensorCocsRo.addAll(allSensorCoc);
        this.sensorCocsRo.removeAll(selectedSensorCocs);
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialog:mitarbeiterZuordnenForm:mitarbeiterZuordnenDlgContent:sucheSensorROBerechtigungSelectMany");
    }

    public void updateSensorCocList() {
        this.sensorCocs.clear();
        this.sensorCocs.addAll(allSensorCoc);
        this.sensorCocs.removeAll(selectedSensorCocsRO);
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialog:mitarbeiterZuordnenForm:mitarbeiterZuordnenDlgContent:sucheSensorBerechtigungSelectMany");
    }

    public void updateTteamMitgliedList() {
        this.tteamMitgliedList.clear();
        this.tteamMitgliedList.addAll(tteams);
        this.tteamMitgliedList.removeAll(selectedROTteamsForMitglied);
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialog:mitarbeiterZuordnenForm:mitarbeiterZuordnenDlgContent:sucheTTeamMitgliedBerechtigungSelectMany");
    }

    public void updateTteamMitgliedRoList() {
        this.tteamMitgliedRoList.clear();
        this.tteamMitgliedRoList.addAll(tteams);
        this.tteamMitgliedRoList.removeAll(selectedTteamsForMitglied);
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialog:mitarbeiterZuordnenForm:mitarbeiterZuordnenDlgContent:sucheTTeamMitgliedROBerechtigungSelectMany");
    }

    public String onFlowProcess(FlowEvent event) {
        if (event.getNewStep().equals("roleBerechtigenTab")) {
            updateBerechtigungenForRole(aktuelleRolle);
        }
        if (event.getNewStep().equals(ROLLE_AUSWAHL_TAB) && selectedMitarbeiter == null) {
            return event.getOldStep();
        }
        return event.getNewStep();
    }

    private void updateBerechtigungenForRole(Rolle rolle) {
        switch (rolle) {
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
            case UMSETZUNGSBESTAETIGER:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                initSensorCocs();
                this.userSensorCocSchreibend = berechtigungService.getSensorCocForMitarbeiterAndRolle(selectedMitarbeiter, rolle, Boolean.TRUE);
                this.userSensorCocLesend = berechtigungService.getSensorCocForMitarbeiterAndRolle(selectedMitarbeiter, rolle, Boolean.FALSE);
                this.selectedSensorCocs = new ArrayList<>(userSensorCocSchreibend);
                this.selectedSensorCocsRO = new ArrayList<>(userSensorCocLesend);
                this.sensorCocs.removeAll(userSensorCocLesend);
                this.sensorCocsRo.removeAll(userSensorCocSchreibend);
                break;
            case T_TEAMLEITER:
                initTteams();
                break;
            case TTEAMMITGLIED:
                initTteamsForMitglied();
                break;
            case TTEAM_VERTRETER:
                initTteamsForTteamVertreter();
                break;
            case E_COC:
                initModules();
                break;
            case ANFORDERER:
                initDerivate();
                initZakEinordnungen();
                break;
            default:
                break;
        }
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialog:mitarbeiterZuordnenForm:mitarbeiterZuordnenDlgContent:sucheSensorBerechtigungSelectMany");
        context.update("dialog:mitarbeiterZuordnenForm:mitarbeiterZuordnenDlgContent:sucheSensorROBerechtigungSelectMany");
    }

    public void processWizard() {
        if (getAddAnotherRoleBoolean()) {
            Wizard wizard = (Wizard) FacesContext.getCurrentInstance().getViewRoot().findComponent(DIALOG_MITARBEITER_ZUORDNEN_FORM_MITARBEITER_ZUORDNEN_DLG_CONTENT_BERECHTIGUNG_WIZARD);
            wizard.setStep(ROLLE_AUSWAHL_TAB);
            RequestContext context = RequestContext.getCurrentInstance();
            context.update(DIALOG_MITARBEITER_ZUORDNEN_FORM_MITARBEITER_ZUORDNEN_DLG_CONTENT_BERECHTIGUNG_WIZARD);
            context.execute("PF('mitarbeiterZuordnenDialog').show();");
        } else {
            Wizard wizard = (Wizard) FacesContext.getCurrentInstance().getViewRoot().findComponent(DIALOG_MITARBEITER_ZUORDNEN_FORM_MITARBEITER_ZUORDNEN_DLG_CONTENT_BERECHTIGUNG_WIZARD);
            wizard.setStep("mitarbeiterAuswahlTab");
            RequestContext context = RequestContext.getCurrentInstance();
            context.update(DIALOG_MITARBEITER_ZUORDNEN_FORM);
            context.execute("PF('mitarbeiterZuordnenDialog').hide();");
        }
    }

    // --------- process input -------------------------------------------------
    public void addUsersToRole() {
        Mitarbeiter mitarbeiter = getSelectedMitarbeiter();
        if (mitarbeiter == null) {
            return;
        }
        if (mitarbeiter.getId() == null) {
            userService.saveMitarbeiter(mitarbeiter);
        }
        if (mitarbeiter.getQNumber() == null || mitarbeiter.getQNumber().isEmpty()) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Rollenzuweiseung nicht erfolgreich!",
                    "Der Datenbankeintrag für den Mitarbeiter " + mitarbeiter.toString() + " ist fehlerhaft!\nBitte wenden Sie sich an den Support für diese Anwendung."));
            LOG.warn("addUserToRole() - Mitarbeiterdaten fehlerhaft: {}", mitarbeiter);
        }

        if (null != aktuelleRolle) {
            switch (aktuelleRolle) {
                case ADMIN:
                    berechtigungService.grantRoleAdmin(mitarbeiter);
                    break;

                case SENSOR:
                    processBerechtigungForSensor(mitarbeiter);
                    break;

                case SENSOR_EINGESCHRAENKT:
                    processBerechtigungForSensorEingeschraenkt(mitarbeiter);
                    break;

                case SENSORCOCLEITER:
                    processBerechtigungForSCL(mitarbeiter);
                    break;

                case SCL_VERTRETER:
                    processBerechtigungForVertreterSCL(mitarbeiter);
                    break;

                case T_TEAMLEITER:
                    processBerechtigungForTteamLeiter(mitarbeiter);
                    break;

                case E_COC:
                    processBerechtigungForEcoc(mitarbeiter);
                    break;

                case ANFORDERER:
                    processBerechtigungForAnforderer(mitarbeiter);
                    break;

                case UMSETZUNGSBESTAETIGER:
                    processBerechtigungForUmsetzungsbestaetiger(mitarbeiter);
                    break;

                case TTEAMMITGLIED:
                    processBerechtigungForTteamMitglied();
                    break;


                case TTEAM_VERTRETER:
                    processBerechtigungForTteamVertreter();
                    break;

                default:
                    break;
            }
        }
    }

    private void processBerechtigungForTteamVertreter() {
        List<Tteam> addList = selectedTteamsForVertreter.stream().filter(t -> !userTteamVertreterTeams.contains(t)).collect(Collectors.toList());
        List<Tteam> removeList = userTteamVertreterTeams.stream().filter(t -> !selectedTteamsForVertreter.contains(t)).collect(Collectors.toList());

        berechtigungService.grantRoleTteamVertreter(selectedMitarbeiter, addList);
        berechtigungService.stripRoleTteamVertreter(selectedMitarbeiter, removeList);
    }

    private void processBerechtigungForTteamMitglied() {
        List<Tteam> addWriteList = selectedTteamsForMitglied.stream().filter(t -> !userMitgliedTteams.contains(t)).collect(Collectors.toList());
        List<Tteam> removeWriteList = userMitgliedTteams.stream().filter(t -> !selectedTteamsForMitglied.contains(t)).collect(Collectors.toList());
        List<Tteam> addReadList = selectedROTteamsForMitglied.stream().filter(t -> !userROMitgliedTteams.contains(t)).collect(Collectors.toList());
        List<Tteam> removeReadList = userROMitgliedTteams.stream().filter(t -> !selectedROTteamsForMitglied.contains(t)).collect(Collectors.toList());

        berechtigungService.grantRoleTteamMitglied(selectedMitarbeiter, addWriteList, Rechttype.SCHREIBRECHT);
        berechtigungService.grantRoleTteamMitglied(selectedMitarbeiter, addReadList, Rechttype.LESERECHT);
        berechtigungService.stripRoleTteamMitglied(selectedMitarbeiter, removeWriteList, Rechttype.SCHREIBRECHT);
        berechtigungService.stripRoleTteamMitglied(selectedMitarbeiter, removeReadList, Rechttype.LESERECHT);
    }

    private void processBerechtigungForUmsetzungsbestaetiger(Mitarbeiter mitarbeiter) {
        List<SensorCoc> addList = selectedSensorCocs.stream().filter(s -> !userSensorCocSchreibend.contains(s)).collect(Collectors.toList());
        List<SensorCoc> removeList = userSensorCocSchreibend.stream().filter(s -> !selectedSensorCocs.contains(s)).collect(Collectors.toList());
        addList.forEach(s -> berechtigungService.grantRoleUmsetzungsbestaetigerForSCoC(mitarbeiter, s.getSensorCocId()));
        removeList.forEach(s -> berechtigungService.stripRoleUmsetzungsbestaetigerForSCoC(mitarbeiter, s.getSensorCocId()));
    }

    private void processBerechtigungForAnforderer(Mitarbeiter mitarbeiter) {
        List<Derivat> addList = selectedDerivate.stream().filter(d -> !userDerivate.contains(d)).collect(Collectors.toList());
        List<Derivat> removeList = userDerivate.stream().filter(d -> !selectedDerivate.contains(d)).collect(Collectors.toList());
        List<String> addZakList = selectedZakEinordnungen.stream().filter(z -> !userZakEinordnungen.contains(z)).collect(Collectors.toList());
        List<String> removeZakList = userZakEinordnungen.stream().filter(z -> !selectedZakEinordnungen.contains(z)).collect(Collectors.toList());

        addList.forEach(d -> berechtigungService.grantRoleAnforderer(mitarbeiter, d, Rechttype.SCHREIBRECHT, BerechtigungZiel.DERIVAT));
        removeList.forEach(d -> berechtigungService.stripRoleAnforderer(mitarbeiter, d, Rechttype.SCHREIBRECHT));
        addZakList.forEach(z -> berechtigungService.grantRoleAnforderer(mitarbeiter, z, Rechttype.SCHREIBRECHT));
        removeZakList.forEach(z -> berechtigungService.stripRoleAnforderer(mitarbeiter, z, Rechttype.SCHREIBRECHT));
    }

    private void processBerechtigungForEcoc(Mitarbeiter mitarbeiter) {
        List<ModulSeTeam> addList = selectedModules.stream().filter(t -> !userModule.contains(t)).collect(Collectors.toList());
        List<ModulSeTeam> removeList = userModule.stream().filter(t -> !selectedModules.contains(t)).collect(Collectors.toList());
        addList.forEach(t -> berechtigungService.grantRoleEcoc(mitarbeiter, t, Rechttype.SCHREIBRECHT));
        removeList.forEach(t -> berechtigungService.stripRoleEcoc(mitarbeiter, t, Rechttype.SCHREIBRECHT));
    }

    private void processBerechtigungForTteamLeiter(Mitarbeiter mitarbeiter) {
        List<Tteam> addList = selectedTteams.stream().filter(t -> !userTteams.contains(t)).collect(Collectors.toList());
        List<Tteam> removeList = userTteams.stream().filter(t -> !selectedTteams.contains(t)).collect(Collectors.toList());
        addList.forEach(t -> berechtigungService.grantRoleTeamleiter(mitarbeiter, t.getId(), Rechttype.SCHREIBRECHT));
        removeList.forEach(t -> berechtigungService.stripRoleTeamleiter(mitarbeiter, t.getId(), Rechttype.SCHREIBRECHT));
    }

    private void processBerechtigungForSensorEingeschraenkt(Mitarbeiter mitarbeiter) {
        List<SensorCoc> addList = selectedSensorCocs.stream().filter(s -> !userSensorCocSchreibend.contains(s)).collect(Collectors.toList());
        List<SensorCoc> removeList = userSensorCocSchreibend.stream().filter(s -> !selectedSensorCocs.contains(s)).collect(Collectors.toList());
        addList.forEach(s -> berechtigungService.grantRoleSensorEingeschraenkt(mitarbeiter, s.getSensorCocId(), Rechttype.SCHREIBRECHT));
        removeList.forEach(s -> berechtigungService.stripRoleSensorEingeschraenkt(mitarbeiter, s.getSensorCocId(), Rechttype.SCHREIBRECHT));
    }

    private void processBerechtigungForSensor(Mitarbeiter mitarbeiter) {
        List<SensorCoc> addList = selectedSensorCocs.stream().filter(s -> !userSensorCocSchreibend.contains(s)).collect(Collectors.toList());
        List<SensorCoc> removeList = userSensorCocSchreibend.stream().filter(s -> !selectedSensorCocs.contains(s)).collect(Collectors.toList());
        addList.forEach(s -> berechtigungService.grantRoleSensor(mitarbeiter, s.getSensorCocId(), Rechttype.SCHREIBRECHT));
        removeList.forEach(s -> berechtigungService.stripRoleSensor(mitarbeiter, s.getSensorCocId(), Rechttype.SCHREIBRECHT));
        addList.clear();
        removeList.clear();
        addList = selectedSensorCocsRO.stream().filter(s -> !userSensorCocLesend.contains(s)).collect(Collectors.toList());
        removeList = userSensorCocLesend.stream().filter(s -> !selectedSensorCocsRO.contains(s)).collect(Collectors.toList());
        addList.forEach(s -> berechtigungService.grantRoleSensor(mitarbeiter, s.getSensorCocId(), Rechttype.LESERECHT));
        removeList.forEach(s -> berechtigungService.stripRoleSensor(mitarbeiter, s.getSensorCocId(), Rechttype.LESERECHT));
    }

    public void resetInputFields() {
        if (getAddAnotherRoleBoolean()) {
            selectedTteams.clear();
            selectedSensorCocs.clear();
            selectedSensorCocsRO.clear();
            setAddAnotherRoleBoolean(false);
        } else {
            selectedMitarbeiter = null;
            gefundeneMitarbeiters.clear();
            setVornameZuSuchen("");
            setNachnameZuSuchen("");
            setAbteilungZuSuchen("");
            selectedTteams.clear();
            selectedSensorCocs.clear();
            selectedSensorCocsRO.clear();
            setAddAnotherRoleBoolean(false);
        }
    }

    public void handleSuche() {
        try {
            gefundeneMitarbeiters = new ArrayList<>();

            List<Mitarbeiter> ldapMitarbeiter = userSearchService.getMitarbeiterFromLdap(vornameZuSuchen, nachnameZuSuchen, abteilungZuSuchen);
            gefundeneMitarbeiters.addAll(ldapMitarbeiter);

            List<Mitarbeiter> lokalMitarbeiter = userSearchService.getMitarbeiterByCriteria(vornameZuSuchen, nachnameZuSuchen, abteilungZuSuchen);
            Set<String> mailSetLokal = lokalMitarbeiter.stream().map(Mitarbeiter::getEmail).collect(Collectors.toSet());

            gefundeneMitarbeiters = gefundeneMitarbeiters.stream().filter(m -> !mailSetLokal.contains(m.getEmail())).distinct().collect(Collectors.toList());
            gefundeneMitarbeiters.addAll(lokalMitarbeiter);
        } catch (UserNotFoundException ex) {
            LOG.error(null, ex);
        }
    }

    private void processBerechtigungForSCL(Mitarbeiter mitarbeiter) {
        List<SensorCoc> addList = selectedSensorCocs.stream().filter(s -> !userSensorCocSchreibend.contains(s)).collect(Collectors.toList());
        List<SensorCoc> removeList = userSensorCocSchreibend.stream().filter(s -> !selectedSensorCocs.contains(s)).collect(Collectors.toList());
        addList.forEach(s -> berechtigungService.grantRoleSensorCoCLeiter(mitarbeiter, s.getSensorCocId(), Rechttype.SCHREIBRECHT));
        removeList.forEach(s -> berechtigungService.stripRoleSensorCoCLeiter(mitarbeiter, s.getSensorCocId(), Rechttype.SCHREIBRECHT));
        addList.clear();
        removeList.clear();
        addList = selectedSensorCocsRO.stream().filter(s -> !userSensorCocLesend.contains(s)).collect(Collectors.toList());
        removeList = userSensorCocLesend.stream().filter(s -> !selectedSensorCocsRO.contains(s)).collect(Collectors.toList());
        addList.forEach(s -> berechtigungService.grantRoleSensorCoCLeiter(mitarbeiter, s.getSensorCocId(), Rechttype.LESERECHT));
        removeList.forEach(s -> berechtigungService.stripRoleSensorCoCLeiter(mitarbeiter, s.getSensorCocId(), Rechttype.LESERECHT));
    }

    private void processBerechtigungForVertreterSCL(Mitarbeiter mitarbeiter) {
        List<SensorCoc> addList = selectedSensorCocs.stream().filter(s -> !userSensorCocSchreibend.contains(s)).collect(Collectors.toList());
        List<SensorCoc> removeList = userSensorCocSchreibend.stream().filter(s -> !selectedSensorCocs.contains(s)).collect(Collectors.toList());
        addList.forEach(s -> berechtigungService.grantRoleVertreterSCL(mitarbeiter, s.getSensorCocId(), Rechttype.SCHREIBRECHT));
        removeList.forEach(s -> berechtigungService.stripRoleVertreterSCL(mitarbeiter, s.getSensorCocId(), Rechttype.SCHREIBRECHT));
        addList.clear();
        removeList.clear();
        addList = selectedSensorCocsRO.stream().filter(s -> !userSensorCocLesend.contains(s)).collect(Collectors.toList());
        removeList = userSensorCocLesend.stream().filter(s -> !selectedSensorCocsRO.contains(s)).collect(Collectors.toList());
        addList.forEach(s -> berechtigungService.grantRoleVertreterSCL(mitarbeiter, s.getSensorCocId(), Rechttype.LESERECHT));
        removeList.forEach(s -> berechtigungService.stripRoleVertreterSCL(mitarbeiter, s.getSensorCocId(), Rechttype.LESERECHT));
    }

    // ---------- get render conditions ----------------------------------------
    public Boolean getAdminRenderCondition() {
        return Rolle.ADMIN.equals(getAktuelleRolle());
    }

    public Boolean getSensorRenderCondition() {
        return Rolle.SENSOR.equals(getAktuelleRolle()) || Rolle.SENSOR_EINGESCHRAENKT.equals(getAktuelleRolle())
                || Rolle.SENSORCOCLEITER.equals(getAktuelleRolle()) || Rolle.SCL_VERTRETER.equals(getAktuelleRolle());
    }

    public Boolean getUmsetzungsbestaetigerRenderCondition() {
        return Rolle.UMSETZUNGSBESTAETIGER.equals(getAktuelleRolle());
    }

    public Boolean getTTeamleiterRenderCondition() {
        return Rolle.T_TEAMLEITER.equals(getAktuelleRolle());
    }

    public Boolean getECocRenderCondition() {
        return Rolle.E_COC.equals(getAktuelleRolle());
    }

    public boolean getOneMitarbeiterRenderCondition() {
        return getSelectedMitarbeiter() != null;
    }

    public Boolean getAnfordererRenderCondition() {
        return Rolle.ANFORDERER.equals(getAktuelleRolle());
    }

    public Boolean getTTeamMitgliedRenderCondition() {
        return Rolle.TTEAMMITGLIED.equals(getAktuelleRolle());
    }

    public Boolean getTteamVertreterRenderCondition() {
        return Rolle.TTEAM_VERTRETER.equals(getAktuelleRolle());
    }

    public SensorCocConverter getSensorCocConverter() {
        return new SensorCocConverter(sensorCocService);
    }

    public ModulSeTeamConverter getModulSeTeamConverter() {
        return new ModulSeTeamConverter(modules);
    }

    public TteamConverter getTteamConverter() {
        return new TteamConverter(tteams);
    }

    public DerivatConverter getDerivatConverter() {
        return new DerivatConverter(derivate);
    }

    public Boolean getAddAnotherRoleBoolean() {
        return addAnotherRoleBoolean;
    }

    public void setAddAnotherRoleBoolean(Boolean addAnotherRoleBoolean) {
        this.addAnotherRoleBoolean = addAnotherRoleBoolean;
    }

    public String getVornameZuSuchen() {
        return vornameZuSuchen;
    }

    public void setVornameZuSuchen(String vornameZuSuchen) {
        this.vornameZuSuchen = vornameZuSuchen;
    }

    public String getNachnameZuSuchen() {
        return nachnameZuSuchen;
    }

    public void setNachnameZuSuchen(String nachnameZuSuchen) {
        this.nachnameZuSuchen = nachnameZuSuchen;
    }

    public String getAbteilungZuSuchen() {
        return abteilungZuSuchen;
    }

    public void setAbteilungZuSuchen(String abteilungZuSuchen) {
        this.abteilungZuSuchen = abteilungZuSuchen;
    }

    public List<Mitarbeiter> getGefundeneMitarbeiters() {
        return Collections.unmodifiableList(gefundeneMitarbeiters);
    }

    public Mitarbeiter getSelectedMitarbeiter() {
        return selectedMitarbeiter;
    }

    public void setSelectedMitarbeiter(Mitarbeiter selectedMitarbeiter) {
        this.selectedMitarbeiter = selectedMitarbeiter;
    }

    public Rolle getAktuelleRolle() {
        return aktuelleRolle;
    }

    public void setAktuelleRolle(Rolle aktuelleRolle) {
        this.aktuelleRolle = aktuelleRolle;
    }

    public List<Rolle> getRollen() {
        return Collections.unmodifiableList(this.rollen);
    }

    public void setRollen(List<Rolle> rollen) {
        this.rollen.clear();
        rollen.forEach(rolle ->
                this.rollen.add(rolle)
        );
    }

    public List<Tteam> getTteams() {
        return tteams;
    }

    public void setTteams(List<Tteam> tteams) {
        this.tteams = tteams;
    }

    public List<Tteam> getSelectedTteams() {
        return selectedTteams;
    }

    public void setSelectedTteams(List<Tteam> selectedTteams) {
        this.selectedTteams = selectedTteams;
    }

    public List<SensorCoc> getSensorCocs() {
        return sensorCocs;
    }

    public void setSensorCocs(List<SensorCoc> sensorCocs) {
        this.sensorCocs = sensorCocs;
    }

    public List<SensorCoc> getSelectedSensorCocs() {
        return selectedSensorCocs;
    }

    public void setSelectedSensorCocs(List<SensorCoc> selectedSensorCocs) {
        this.selectedSensorCocs = selectedSensorCocs;
    }

    public List<SensorCoc> getSelectedSensorCocsRO() {
        return selectedSensorCocsRO;
    }

    public void setSelectedSensorCocsRO(List<SensorCoc> selectedSensorCocsRO) {
        this.selectedSensorCocsRO = selectedSensorCocsRO;
    }

    public List<ModulSeTeam> getModules() {
        return modules;
    }

    public void setModules(List<ModulSeTeam> modules) {
        this.modules = modules;
    }

    public List<ModulSeTeam> getSelectedModules() {
        return selectedModules;
    }

    public void setSelectedModules(List<ModulSeTeam> selectedModules) {
        this.selectedModules = selectedModules;
    }

    public List<Derivat> getDerivate() {
        return derivate;
    }

    public void setDerivate(List<Derivat> derivate) {
        this.derivate = derivate;
    }

    public List<Derivat> getSelectedDerivate() {
        return selectedDerivate;
    }

    public void setSelectedDerivate(List<Derivat> selectedDerivate) {
        this.selectedDerivate = selectedDerivate;
    }

    public Boolean getMitarbeiterAuswahlRenderCondition() {
        return mitarbeiterAuswahlRenderCondition;
    }

    public void setMitarbeiterAuswahlRenderCondition(Boolean mitarbeiterAuswahlRenderCondition) {
        this.mitarbeiterAuswahlRenderCondition = mitarbeiterAuswahlRenderCondition;
    }

    public List<String> getZakEinordnungen() {
        return zakEinordnungen;
    }

    public void setZakEinordnungen(List<String> zakEinordnungen) {
        this.zakEinordnungen = zakEinordnungen;
    }

    public List<String> getSelectedZakEinordnungen() {
        return selectedZakEinordnungen;
    }

    public void setSelectedZakEinordnungen(List<String> selectedZakEinordnungen) {
        this.selectedZakEinordnungen = selectedZakEinordnungen;
    }

    public List<SensorCoc> getSensorCocsRo() {
        return sensorCocsRo;
    }

    public void setSensorCocsRo(List<SensorCoc> sensorCocsRo) {
        this.sensorCocsRo = sensorCocsRo;
    }

    public List<Tteam> getSelectedTteamsForMitglied() {
        return selectedTteamsForMitglied;
    }

    public void setSelectedTteamsForMitglied(List<Tteam> selectedTteamsForMitglied) {
        this.selectedTteamsForMitglied = selectedTteamsForMitglied;
    }

    public List<Tteam> getSelectedROTteamsForMitglied() {
        return selectedROTteamsForMitglied;
    }

    public void setSelectedROTteamsForMitglied(List<Tteam> selectedROTteamsForMitglied) {
        this.selectedROTteamsForMitglied = selectedROTteamsForMitglied;
    }

    public List<Tteam> getSelectedTteamsForVertreter() {
        return selectedTteamsForVertreter;
    }

    public void setSelectedTteamsForVertreter(List<Tteam> selectedTteamsForVertreter) {
        this.selectedTteamsForVertreter = selectedTteamsForVertreter;
    }

    public List<Tteam> getTteamMitgliedList() {
        return tteamMitgliedList;
    }

    public void setTteamMitgliedList(List<Tteam> tteamMitgliedList) {
        this.tteamMitgliedList = tteamMitgliedList;
    }

    public List<Tteam> getTteamMitgliedRoList() {
        return tteamMitgliedRoList;
    }

    public void setTteamMitgliedRoList(List<Tteam> tteamMitgliedRoList) {
        this.tteamMitgliedRoList = tteamMitgliedRoList;
    }

}
