package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.excel.ExcelSheetRealm;
import de.interfaceag.bmw.pzbk.excel.ExcelUtils;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public final class BerechtigungsUtils {

    private BerechtigungsUtils() {
    }

    public static Collection<BerechtigungsExcelRealm> loadBerechtigungenFromExcelSheet(Sheet sheet, String columnName) {
        Collection<ExcelSheetRealm> realms = ExcelUtils.getRealmsForColumn(sheet, columnName, 1);
        return parseBerechtigungen(realms);
    }

    public static Collection<BerechtigungsExcelRealm> parseBerechtigungen(Collection<ExcelSheetRealm> realms) {
        Collection<BerechtigungsExcelRealm> result = new ArrayList<>();
        for (ExcelSheetRealm realm : realms) {
            BerechtigungsExcelRealm br = new BerechtigungsExcelRealm(realm);
            result.add(br);
        }
        return result;
    }
}
