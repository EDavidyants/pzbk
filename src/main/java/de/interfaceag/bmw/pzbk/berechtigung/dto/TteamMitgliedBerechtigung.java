package de.interfaceag.bmw.pzbk.berechtigung.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author fn
 */
public class TteamMitgliedBerechtigung implements Serializable {

    private final List<Long> tteamIdsMitSchreibrechten;

    private final List<Long> tteamIdsMitLeserechten;

    public TteamMitgliedBerechtigung(List<Long> tteamIdsMitSchreibrechten, List<Long> tteamIdsMitLeserechten) {
        this.tteamIdsMitSchreibrechten = tteamIdsMitSchreibrechten;
        this.tteamIdsMitLeserechten = tteamIdsMitLeserechten;
    }

    public List<Long> getTteamIdsMitSchreibrechten() {
        return tteamIdsMitSchreibrechten;
    }

    public List<Long> getTteamIdsMitLeserechten() {
        return tteamIdsMitLeserechten;
    }

}
