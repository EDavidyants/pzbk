package de.interfaceag.bmw.pzbk.berechtigung.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author fn
 */
public class TteamVertreterBerechtigung implements Serializable {

    private final List<Long> tteamIdsMitSchreibrechten;

    public TteamVertreterBerechtigung(List<Long> tteamIdsMitSchreibrechten) {
        this.tteamIdsMitSchreibrechten = tteamIdsMitSchreibrechten;
    }

    public List<Long> getTteamIdsMitSchreibrechten() {
        return tteamIdsMitSchreibrechten;
    }

}
