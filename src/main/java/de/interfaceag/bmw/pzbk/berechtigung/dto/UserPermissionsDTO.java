package de.interfaceag.bmw.pzbk.berechtigung.dto;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @param <T>
 * @author sl
 */
public class UserPermissionsDTO<T extends BerechtigungDto> implements Serializable, UserPermissions<T> {

    private final List<Rolle> rollen;

    private final List<T> berechtigungen;

    private final TteamMitgliedBerechtigung tteamMitgliedBerechtigung;

    private final TteamVertreterBerechtigung tteamVertreterBerechtigung;

    // ---------- constructor --------------------------------------------------
    public UserPermissionsDTO(List<Rolle> rollen, List<T> berechtigungen,
                              TteamMitgliedBerechtigung tteamMitgliedBerechtigung,
                              TteamVertreterBerechtigung tteamVertreterBerechtigung) {
        this.rollen = rollen;
        this.berechtigungen = berechtigungen;
        this.tteamMitgliedBerechtigung = tteamMitgliedBerechtigung;
        this.tteamVertreterBerechtigung = tteamVertreterBerechtigung;
    }

    // ---------- methods ------------------------------------------------------
    private List<T> getBerechtigungenForRolleRechttypeAndType(Rolle rolle,
                                                              Rechttype rechttype, BerechtigungZiel type) {
        return berechtigungen
                .stream()
                .filter(b -> b.getRechttype().equals(rechttype)
                        && b.getType().equals(type) && b.getRolle().equals(rolle))
                .collect(Collectors.toList());

    }

    private Set<Rolle> getRolesWithPermissions(Long id, BerechtigungZiel type, Rechttype rechttype) {
        if (rollen.contains(Rolle.ADMIN)) {
            Set<Rolle> admin = new HashSet<>();
            admin.add(Rolle.ADMIN);
            return admin;
        }
        return berechtigungen.stream()
                .filter(b -> b.getId() != null
                        && b.getId().equals(id)
                        && b.getRechttype().equals(rechttype)
                        && b.getType().equals(type))
                .map(BerechtigungDto::getRolle).collect(Collectors.toSet());
    }

    private Set<Rolle> getRolesWithPermissions(String name, BerechtigungZiel type, Rechttype rechttype) {
        if (rollen.contains(Rolle.ADMIN)) {
            Set<Rolle> admin = new HashSet<>();
            admin.add(Rolle.ADMIN);
            return admin;
        }
        return berechtigungen.stream()
                .filter(b -> b.getName() != null
                        && b.getName().equals(name)
                        && b.getRechttype().equals(rechttype)
                        && b.getType().equals(type))
                .map(BerechtigungDto::getRolle).collect(Collectors.toSet());
    }

    private Set<Rolle> getRolesWithPermissions(Set<Long> ids, BerechtigungZiel type, Rechttype rechttype) {
        if (rollen.contains(Rolle.ADMIN)) {
            Set<Rolle> admin = new HashSet<>();
            admin.add(Rolle.ADMIN);
            return admin;
        }
        return berechtigungen.stream()
                .filter(b -> ids.contains(b.getId())
                        && b.getRechttype().equals(rechttype)
                        && b.getType().equals(type))
                .map(BerechtigungDto::getRolle).collect(Collectors.toSet());
    }

    private Set<Rolle> getRolesWithWritePermissionsForSensorCoC(Long sensorCoC) {
        return getRolesWithPermissions(sensorCoC, BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT);
    }

    private Set<Rolle> getRolesWithWritePermissionsForZakEinordnung(String zakEinordnung) {
        return getRolesWithPermissions(zakEinordnung, BerechtigungZiel.ZAK_EINORDNUNG, Rechttype.SCHREIBRECHT);
    }

    private Set<Rolle> getRolesWithWritePermissionsForTteam(Long tTeam) {
        return getRolesWithPermissions(tTeam, BerechtigungZiel.TTEAM, Rechttype.SCHREIBRECHT);
    }

    private Set<Rolle> getRolesWithWritePermissionsForModulSeTeams(Set<Long> modulSeTeams) {
        return getRolesWithPermissions(modulSeTeams, BerechtigungZiel.MODULSETEAM, Rechttype.SCHREIBRECHT);
    }

    private Set<Rolle> getRolesWithReadPermissionsForSensorCoC(Long sensorCoC) {
        Set<Rolle> result = new HashSet<>();
        result.addAll(getRolesWithWritePermissionsForSensorCoC(sensorCoC));
        result.addAll(getRolesWithPermissions(sensorCoC, BerechtigungZiel.SENSOR_COC, Rechttype.LESERECHT));
        return result;
    }

    private Set<Rolle> getRolesWithReadPermissionsForZakEinordnung(String zakEinordnung) {
        Set<Rolle> result = new HashSet<>();
        result.addAll(getRolesWithWritePermissionsForZakEinordnung(zakEinordnung));
        result.addAll(getRolesWithPermissions(zakEinordnung, BerechtigungZiel.ZAK_EINORDNUNG, Rechttype.LESERECHT));
        return result;
    }

    private Set<Rolle> getRolesWithReadPermissionsForTteam(Long tTeam) {
        Set<Rolle> result = new HashSet<>();
        result.addAll(getRolesWithWritePermissionsForTteam(tTeam));
        result.addAll(getRolesWithPermissions(tTeam, BerechtigungZiel.TTEAM, Rechttype.LESERECHT));
        return result;
    }

    private Set<Rolle> getRolesWithReadPermissionsForModulSeTeams(Set<Long> modulSeTeams) {
        Set<Rolle> result = new HashSet<>();
        result.addAll(getRolesWithWritePermissionsForModulSeTeams(modulSeTeams));
        result.addAll(getRolesWithPermissions(modulSeTeams, BerechtigungZiel.MODULSETEAM, Rechttype.LESERECHT));
        return result;
    }

    @Override
    public boolean hasRole(Rolle rolle) {
        return this.rollen.contains(rolle);
    }

    // ---------- getter -------------------------------------------------------
    @Override
    public List<Rolle> getRollen() {
        return Collections.unmodifiableList(rollen);
    }

    @Override
    public Set<Rolle> getRoles() {
        return Collections.unmodifiableSet(new HashSet<>(rollen));
    }

    @Override
    public List<T> getSensorCocAsSensorSchreibend() {
        return getBerechtigungenForRolleRechttypeAndType(Rolle.SENSOR,
                Rechttype.SCHREIBRECHT, BerechtigungZiel.SENSOR_COC);
    }

    @Override
    public List<T> getSensorCocAsSensorLesend() {
        return getBerechtigungenForRolleRechttypeAndType(Rolle.SENSOR,
                Rechttype.LESERECHT, BerechtigungZiel.SENSOR_COC);
    }

    @Override
    public List<T> getSensorCocAsSensorEingSchreibend() {
        return getBerechtigungenForRolleRechttypeAndType(Rolle.SENSOR_EINGESCHRAENKT,
                Rechttype.SCHREIBRECHT, BerechtigungZiel.SENSOR_COC);
    }

    @Override
    public List<T> getSensorCocAsSensorCocLeiterSchreibend() {
        return getBerechtigungenForRolleRechttypeAndType(Rolle.SENSORCOCLEITER,
                Rechttype.SCHREIBRECHT, BerechtigungZiel.SENSOR_COC);
    }

    @Override
    public List<T> getSensorCocAsSensorCocLeiterLesend() {
        return getBerechtigungenForRolleRechttypeAndType(Rolle.SENSORCOCLEITER,
                Rechttype.LESERECHT, BerechtigungZiel.SENSOR_COC);
    }

    @Override
    public List<T> getSensorCocAsVertreterSCLSchreibend() {
        return getBerechtigungenForRolleRechttypeAndType(Rolle.SCL_VERTRETER,
                Rechttype.SCHREIBRECHT, BerechtigungZiel.SENSOR_COC);
    }

    @Override
    public List<T> getSensorCocAsVertreterSCLLesend() {
        return getBerechtigungenForRolleRechttypeAndType(Rolle.SCL_VERTRETER,
                Rechttype.LESERECHT, BerechtigungZiel.SENSOR_COC);
    }

    @Override
    public List<T> getSensorCocAsUmsetzungsbestaetigerSchreibend() {
        return getBerechtigungenForRolleRechttypeAndType(Rolle.UMSETZUNGSBESTAETIGER,
                Rechttype.SCHREIBRECHT, BerechtigungZiel.SENSOR_COC);
    }

    @Override
    public List<T> getTteamAsTteamleiterSchreibend() {
        return getBerechtigungenForRolleRechttypeAndType(Rolle.T_TEAMLEITER,
                Rechttype.SCHREIBRECHT, BerechtigungZiel.TTEAM);
    }

    @Override
    public List<T> getDerivatAsAnfordererSchreibend() {
        return getBerechtigungenForRolleRechttypeAndType(Rolle.ANFORDERER,
                Rechttype.SCHREIBRECHT, BerechtigungZiel.DERIVAT);
    }

    @Override
    public List<T> getModulSeTeamAsEcocSchreibend() {
        return getBerechtigungenForRolleRechttypeAndType(Rolle.E_COC,
                Rechttype.SCHREIBRECHT, BerechtigungZiel.MODULSETEAM);
    }

    @Override
    public List<T> getZakEinordnungAsAnfordererSchreibend() {
        return getBerechtigungenForRolleRechttypeAndType(Rolle.ANFORDERER,
                Rechttype.SCHREIBRECHT, BerechtigungZiel.ZAK_EINORDNUNG);
    }

    @Override
    public List<T> getSensorCocAsLeser() {
        return getBerechtigungenForRolleRechttypeAndType(Rolle.LESER,
                Rechttype.LESERECHT, BerechtigungZiel.SENSOR_COC);
    }

    @Override
    public T getAdminBerechtigung() {
        List<T> result = getBerechtigungenForRolleRechttypeAndType(Rolle.ADMIN,
                Rechttype.KEIN, BerechtigungZiel.SYSTEM);
        return !result.isEmpty() ? result.get(0) : null;
    }

    @Override
    public Set<Rolle> getRolesWithWritePermissionsForMeldung(Long sensorCocId) {
        Set<Rolle> result = new HashSet<>();
        result.addAll(getRolesWithWritePermissionsForSensorCoC(sensorCocId));
        return result;
    }

    @Override
    public Set<Rolle> getRolesWithWritePermissionsForAnforderung(SensorCoc sensorCocId, Long tteamId, Set<Long> modulSeTeamIds) {
        Set<Rolle> result = new HashSet<>();
        result.addAll(getRolesWithWritePermissionsForSensorCoC(sensorCocId.getSensorCocId()));
        result.addAll(getRolesWithWritePermissionsForZakEinordnung(sensorCocId.getZakEinordnung()));
        result.addAll(getRolesWithWritePermissionsForTteam(tteamId));
        result.addAll(getRolesWithWritePermissionsForModulSeTeams(modulSeTeamIds));
        return result;
    }

    @Override
    public Set<Rolle> getRolesWithReadPermissionsForMeldung(Long sensorCocId) {
        Set<Rolle> result = new HashSet<>();
        result.addAll(getRolesWithReadPermissionsForSensorCoC(sensorCocId));
        return result;
    }

    @Override
    public Set<Rolle> getRolesWithReadPermissionsForAnforderung(SensorCoc sensorCocId, Long tteamId, Set<Long> modulSeTeamIds) {
        Set<Rolle> result = new HashSet<>();
        result.addAll(getRolesWithReadPermissionsForSensorCoC(sensorCocId.getSensorCocId()));
        result.addAll(getRolesWithReadPermissionsForZakEinordnung(sensorCocId.getZakEinordnung()));
        result.addAll(getRolesWithReadPermissionsForTteam(tteamId));
        result.addAll(getRolesWithReadPermissionsForModulSeTeams(modulSeTeamIds));
        return result;
    }

    @Override
    public Set<Rolle> getRolesWithWritePermissionsForNewMeldung() {
        return getRollen().stream()
                .filter(r -> r.equals(Rolle.ADMIN)
                        || r.equals(Rolle.SENSOR)
                        || r.equals(Rolle.SENSOR_EINGESCHRAENKT)
                        || r.equals(Rolle.SENSORCOCLEITER)
                )
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Rolle> getRolesWithWritePermissionsForNewAnforderung() {
        return getRollen().stream()
                .filter(rolle -> rolle.equals(Rolle.ADMIN)
                        || rolle.equals(Rolle.SENSORCOCLEITER)
                        || rolle.equals(Rolle.SCL_VERTRETER)
                        || rolle.equals(Rolle.T_TEAMLEITER)
                        || rolle.equals(Rolle.TTEAMMITGLIED)
                        || rolle.equals(Rolle.TTEAM_VERTRETER)
                )
                .collect(Collectors.toSet());
    }

    @Override
    public List<Long> getTteamIdsForTteamMitgliedSchreibend() {
        if (tteamMitgliedBerechtigung != null) {
            return tteamMitgliedBerechtigung.getTteamIdsMitSchreibrechten();
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public List<Long> getTteamIdsForTteamMitgliedLesend() {
        if (tteamMitgliedBerechtigung != null) {
            TteamMitgliedBerechtigung berechtigung = tteamMitgliedBerechtigung;
            return berechtigung.getTteamIdsMitLeserechten();
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public List<Long> getTteamIdsForTteamVertreter() {
        if (tteamVertreterBerechtigung != null) {
            return tteamVertreterBerechtigung.getTteamIdsMitSchreibrechten();
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public TteamMitgliedBerechtigung getTteamMitgliedBerechtigung() {
        if (tteamMitgliedBerechtigung != null) {
            return tteamMitgliedBerechtigung;
        } else {
            return new TteamMitgliedBerechtigung(Collections.emptyList(), Collections.emptyList());
        }
    }

}
