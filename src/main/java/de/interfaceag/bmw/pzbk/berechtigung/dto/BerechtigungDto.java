package de.interfaceag.bmw.pzbk.berechtigung.dto;

import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface BerechtigungDto extends Serializable {

    Long getId();

    String getName();

    Rechttype getRechttype();

    Rolle getRolle();

    BerechtigungZiel getType();

    @Override
    String toString();

}
