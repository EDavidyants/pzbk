package de.interfaceag.bmw.pzbk.berechtigung.dto;

import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class BerechtigungEditDtoImpl implements BerechtigungEditDto {

    private final Long id;
    private final String name;

    private final Rolle rolle;
    private final BerechtigungZiel type;
    private final Rechttype rechttype;

    private final EditPermission editPermission;

    private boolean delete;

    public BerechtigungEditDtoImpl(Long id, String name, BerechtigungZiel type,
            Rechttype rechttype, Rolle rolle) {
        this.id = id;
        this.type = type;
        this.rechttype = rechttype;
        this.name = name;
        this.rolle = rolle;
        this.editPermission = new FalsePermission();
    }

    public BerechtigungEditDtoImpl(Long id, String name, BerechtigungZiel type,
            Rechttype rechttype, Rolle rolle, EditPermission editPermission) {
        this.id = id;
        this.type = type;
        this.rechttype = rechttype;
        this.name = name;
        this.rolle = rolle;
        this.editPermission = editPermission;
    }

    @Override
    public String toString() {
        return this.name;
    }

    // ---------- getter ------------------------------------------------------- 
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BerechtigungZiel getType() {
        return type;
    }

    @Override
    public Rechttype getRechttype() {
        return rechttype;
    }

    @Override
    public Rolle getRolle() {
        return rolle;
    }

    @Override
    public boolean isDelete() {
        return delete;
    }

    @Override
    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    @Override
    public boolean getEditPermission() {
        return editPermission.hasRightToEdit() && !delete;
    }

}
