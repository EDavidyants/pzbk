package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.MitarbeiterDTO;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author fn
 */
public class BerechtigungViewData implements Serializable {

    private final BerechtigungViewFilter filter;
    private final List<MitarbeiterDTO> tableData;

    BerechtigungViewData(List<MitarbeiterDTO> tableData, BerechtigungViewFilter filter) {
        this.tableData = tableData;
        this.filter = filter;
    }

    public BerechtigungViewFilter getFilter() {
        return filter;
    }

    public List<MitarbeiterDTO> getTableData() {
        return tableData;
    }
}
