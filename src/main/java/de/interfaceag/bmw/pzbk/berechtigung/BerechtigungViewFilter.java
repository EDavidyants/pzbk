package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.filter.AbteilungFilter;
import de.interfaceag.bmw.pzbk.filter.DerivatFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ModulSeTeamFilter;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.NurMitBerechtigungFilter;
import de.interfaceag.bmw.pzbk.filter.RolleFilter;
import de.interfaceag.bmw.pzbk.filter.SensorCocLeseUndSchreibFilter;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.filter.TteamFilter;
import de.interfaceag.bmw.pzbk.filter.ZakEinordnungFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author fn
 */
public class BerechtigungViewFilter implements Serializable {

    private static final Page PAGE = Page.BERECHTIGUNG;

    private final TextSearchFilter vornameFilter;
    private final TextSearchFilter nachnameFilter;
    private final MultiValueEnumSearchFilter<Rolle> rolleFilter;
    private final AbteilungFilter abteilungFilter;
    private final IdSearchFilter sensorCocFilterLesend;
    private final IdSearchFilter sensorCocFilterSchreibend;
    private final IdSearchFilter modulSeTeamFilter;
    private final IdSearchFilter derivatFilter;
    private final IdSearchFilter tTeamFilter;
    private final ZakEinordnungFilter zakEinordnungFilter;
    private final NurMitBerechtigungFilter nurMitBerechtigungFilter;

    protected BerechtigungViewFilter(UrlParameter urlParameter, List<String> allAbteilung,
            List<SensorCoc> allSensorCocs, List<ModulSeTeam> allModulSeTeams,
            List<Derivat> allDerivate, List<Tteam> allTteams, List<String> allZakEinordung, List<Rolle> rollenForFilter) {

        this.vornameFilter = new TextSearchFilter("vorname", urlParameter);
        this.nachnameFilter = new TextSearchFilter("nachname", urlParameter);
        this.rolleFilter = new RolleFilter(urlParameter, rollenForFilter);
        this.abteilungFilter = new AbteilungFilter(allAbteilung, urlParameter);
        this.sensorCocFilterLesend = new SensorCocLeseUndSchreibFilter(allSensorCocs, urlParameter, "sensorCocLesend", false);
        this.sensorCocFilterSchreibend = new SensorCocLeseUndSchreibFilter(allSensorCocs, urlParameter, "sensorCocSchreibend", true);
        this.modulSeTeamFilter = new ModulSeTeamFilter(allModulSeTeams, urlParameter);
        this.derivatFilter = new DerivatFilter(allDerivate, urlParameter);
        this.tTeamFilter = new TteamFilter(allTteams, urlParameter);
        this.zakEinordnungFilter = new ZakEinordnungFilter(allZakEinordung, urlParameter);
        this.nurMitBerechtigungFilter = new NurMitBerechtigungFilter(urlParameter);
    }

    public String getUrl() {
        return PAGE.getUrl() + getUrlParameter();
    }

    private String getUrlParameter() {
        StringBuilder sb = new StringBuilder("?faces-redirect=true");

        sb.append(vornameFilter.getIndependentParameter());
        sb.append(nachnameFilter.getIndependentParameter());
        sb.append(rolleFilter.getIndependentParameter());
        sb.append(abteilungFilter.getIndependentParameter());
        sb.append(sensorCocFilterLesend.getIndependentParameter());
        sb.append(sensorCocFilterSchreibend.getIndependentParameter());
        sb.append(modulSeTeamFilter.getIndependentParameter());
        sb.append(derivatFilter.getIndependentParameter());
        sb.append(tTeamFilter.getIndependentParameter());
        sb.append(zakEinordnungFilter.getIndependentParameter());
        sb.append(nurMitBerechtigungFilter.getIndependentParameter());

        return sb.toString();
    }

    public TextSearchFilter getVornameFilter() {
        return vornameFilter;
    }

    public TextSearchFilter getNachnameFilter() {
        return nachnameFilter;
    }

    public MultiValueEnumSearchFilter<Rolle> getRolleFilter() {
        return rolleFilter;
    }

    public IdSearchFilter getSensorCocFilterLesend() {
        return sensorCocFilterLesend;
    }

    public IdSearchFilter getSensorCocFilterSchreibend() {
        return sensorCocFilterSchreibend;
    }

    public IdSearchFilter getModulSeTeamFilter() {
        return modulSeTeamFilter;
    }

    public IdSearchFilter getDerivatFilter() {
        return derivatFilter;
    }

    public IdSearchFilter gettTeamFilter() {
        return tTeamFilter;
    }

    public ZakEinordnungFilter getZakEinordnungFilter() {
        return zakEinordnungFilter;
    }

    public AbteilungFilter getAbteilungFilter() {
        return abteilungFilter;
    }

    public NurMitBerechtigungFilter getNurMitBerechtigungFilter() {
        return nurMitBerechtigungFilter;
    }

    public String getResetUrl() {
        return PAGE.getUrl() + "?faces-redirect=true";
    }

}
