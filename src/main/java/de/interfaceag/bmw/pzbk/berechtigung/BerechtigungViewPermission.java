package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author fn
 */
public final class BerechtigungViewPermission implements Serializable {

    private final ViewPermission page;

    private final ViewPermission adminPermission;
    private final ViewPermission sensorCocPermission;
    private final ViewPermission tteamleiterPermission;

    private BerechtigungViewPermission(Set<Rolle> userRoles) {
        page = getPagePermission(userRoles);
        adminPermission = getAdminPermission(userRoles);
        sensorCocPermission = getSensorCocPermission(userRoles);
        tteamleiterPermission = getTteamleiterPermission(userRoles);
    }

    private BerechtigungViewPermission() {
        page = new FalsePermission();
        adminPermission = new FalsePermission();
        sensorCocPermission = new FalsePermission();
        tteamleiterPermission = new FalsePermission();
    }

    public static BerechtigungViewPermission denied() {
        return new BerechtigungViewPermission();
    }

    public boolean getPage() {
        return page.isRendered();
    }

    public boolean isAdmin() {
        return adminPermission.isRendered();
    }

    public boolean isTteamleiter() {
        return tteamleiterPermission.isRendered();
    }

    public boolean getSensorCocFilter() {
        return isAdmin() || isSensorCocLeiter();
    }

    public boolean getTteamFilter() {
        return isAdmin() || isTteamleiter();
    }

    public boolean isSensorCocLeiter() {
        return sensorCocPermission.isRendered();
    }

    public static ViewPermission getAdminPermission(Set<Rolle> userRoles) {
        return RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .compareTo(userRoles)
                .get();
    }

    public static ViewPermission getSensorCocPermission(Set<Rolle> userRoles) {
        return RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .compareTo(userRoles)
                .get();
    }

    public static ViewPermission getTteamleiterPermission(Set<Rolle> userRoles) {
        return RolePermission.builder()
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .compareTo(userRoles)
                .get();
    }

    private static ViewPermission getPagePermission(Set<Rolle> userRoles) {
        return RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .compareTo(userRoles)
                .get();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private Set<Rolle> userRoles;

        public Builder() {
        }

        public Builder forUserRoles(Set<Rolle> userRoles) {
            this.userRoles = userRoles;
            return this;
        }

        public BerechtigungViewPermission build() {
            return new BerechtigungViewPermission(userRoles);
        }

    }
}
