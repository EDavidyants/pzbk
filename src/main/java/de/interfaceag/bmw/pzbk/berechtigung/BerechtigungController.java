package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.MitarbeiterDTO;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@ViewScoped
@Named
public class BerechtigungController implements Serializable {

    //---------new------------------------
    @Inject
    private BerechtigungViewFacade berechtigungViewFacade;

    private BerechtigungViewData berechtigungViewData;

    //-------------------------------
    @Inject
    private Session session;
    @Inject
    private UserSearchService userSearchService;
    @Inject
    private MitarbeiterZuordnenDialogController mitarbeiterZuordnenDialogController;

    private BerechtigungViewPermission viewPermission;

    @PostConstruct
    public void init() {

        session.setLocationForView();

        initViewPermission();

        initViewData();

    }
    //----------------------------------new Methods----------------------------------------

    private void initViewData() {
        berechtigungViewData = berechtigungViewFacade.getViewData(UrlParameterUtils.getUrlParameter());
    }

    private void initViewPermission() {
        viewPermission = BerechtigungViewPermission.builder()
                .forUserRoles(session.getUserPermissions().getRoles())
                .build();
    }

    public BerechtigungViewData getViewData() {
        return berechtigungViewData;
    }

    public BerechtigungViewFilter getFilter() {
        return berechtigungViewData.getFilter();
    }

    public String reset() {
        return getFilter().getResetUrl();
    }

    public String filter() {
        return getFilter().getUrl();
    }

    public List<MitarbeiterDTO> getTableData() {
        return getViewData().getTableData();
    }
    //---------------------old used Methods

    public String processInput() {
        // process data
        getMitarbeiterZuordnenDialogController().addUsersToRole();
        // process wizard dialog
        processWizard();
        // reset dialog
        getMitarbeiterZuordnenDialogController().resetInputFields();
        return filter();
    }

    private void processWizard() {
        getMitarbeiterZuordnenDialogController().processWizard();
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("viewAdmContent:adminAccordionPanel:benutzerverwaltungForm");
    }

    public void showAddZuordnungDialog(Long mitarbeiterId) {
        if (mitarbeiterId != null) {
            Mitarbeiter mitarbeiter = userSearchService.getUniqueMitarbeiterById(mitarbeiterId);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('mitarbeiterBearbeitenDialog').hide()");
            mitarbeiterZuordnenDialogController.showAddZuordnungDialog(mitarbeiter);
            context.update("dialog:mitarbeiterBearbeitenForm");
        }
    }

    public MitarbeiterZuordnenDialogController getMitarbeiterZuordnenDialogController() {
        return mitarbeiterZuordnenDialogController;
    }

    public BerechtigungViewPermission getViewPermission() {
        return viewPermission;
    }

}
