package de.interfaceag.bmw.pzbk.berechtigung.dao;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.rollen.TTeamMitglied;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * @author fn
 */
@Stateless
public class TteamMitgliedDAO implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(TteamMitgliedDAO.class);

    private static final String WHERE_TTM_MITARBEITER_ID_MITARBEITER = " WHERE ttm.mitarbeiter.id = :mitarbeiter";
    private static final String MITARBEITER = "mitarbeiter";

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public void persistTteamMitglied(TTeamMitglied tteamMitglied) {
        entityManager.persist(tteamMitglied);
    }

    public void removeTteamMitgliedBerechtigung(TTeamMitglied tteamMitglied) {
        entityManager.remove(tteamMitglied);
    }

    public List<TTeamMitglied> getTteamMitgliedsForMitarbeiter(Mitarbeiter mitarbeiter) {
        QueryPartDTO queryPartDTO;
        queryPartDTO = new QueryPartDTO("SELECT ttm from TTeamMitglied ttm ");

        queryPartDTO.append(WHERE_TTM_MITARBEITER_ID_MITARBEITER);
        queryPartDTO.put(MITARBEITER, mitarbeiter.getId());

        QueryFactory<TTeamMitglied> queryFactory = new QueryFactory<>();
        TypedQuery<TTeamMitglied> query = queryFactory.buildTypedQuery(queryPartDTO, entityManager, TTeamMitglied.class);

        LOG.debug("Find existing Berechtigungen for user {}", mitarbeiter);

        return query.getResultList();
    }

    public Optional<TTeamMitglied> getTTeamMitgliedByTteamAndMitarbeiter(Tteam tteam, Mitarbeiter mitarbeiter) {
        QueryPartDTO queryPartDTO;
        queryPartDTO = new QueryPartDTO("SELECT ttm from TTeamMitglied ttm ");

        queryPartDTO.append(WHERE_TTM_MITARBEITER_ID_MITARBEITER);
        queryPartDTO.put(MITARBEITER, mitarbeiter.getId());

        queryPartDTO.append(" AND ttm.tteam.id = :tteam");
        queryPartDTO.put("tteam", tteam.getId());

        QueryFactory<TTeamMitglied> queryFactory = new QueryFactory<>();
        TypedQuery<TTeamMitglied> query = queryFactory.buildTypedQuery(queryPartDTO, entityManager, TTeamMitglied.class);

        List<TTeamMitglied> result = query.getResultList();
        return result != null && !result.isEmpty() ? Optional.ofNullable(result.get(0)) : Optional.empty();
    }

    public List<Long> getTteamIdsforTteamMitgliedAndRechttype(Mitarbeiter mitarbeiter, Integer rechttype) {

        QueryPartDTO queryPartDTO;
        queryPartDTO = new QueryPartDTO("SELECT ttm.tteam.id from TTeamMitglied ttm ");

        queryPartDTO.append(WHERE_TTM_MITARBEITER_ID_MITARBEITER);
        queryPartDTO.put(MITARBEITER, mitarbeiter.getId());

        queryPartDTO.append(" AND ttm.rechttype = :rechttype");
        queryPartDTO.put("rechttype", rechttype);

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPartDTO, entityManager, Long.class);

        return query.getResultList();
    }

}
