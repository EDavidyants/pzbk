package de.interfaceag.bmw.pzbk.dashboard;

/**
 *
 * @author evda
 */
public interface DashboardResult {
    
    int getCount();
    
}
