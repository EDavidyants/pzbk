package de.interfaceag.bmw.pzbk.dashboard;

import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;

/**
 *
 * @author fn
 */
public class SencorCocIdKovaPhaseImDerivatIdTuple extends GenericTuple<Long, Long> {

    public SencorCocIdKovaPhaseImDerivatIdTuple(Long sensorCocId, Long kovaPhaseImDerivatId) {
        super(sensorCocId, kovaPhaseImDerivatId);
    }

    public Long getSensorCocId() {
        return getX();
    }

    public Long getKovaPhaseImDerivatId() {
        return getY();
    }
}
