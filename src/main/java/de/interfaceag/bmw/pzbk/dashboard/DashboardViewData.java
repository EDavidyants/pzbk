package de.interfaceag.bmw.pzbk.dashboard;

import de.interfaceag.bmw.pzbk.enums.DashboardStep;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author evda
 */
public class DashboardViewData implements Serializable {

    private final String version;
    private final DashboardViewPermission viewPermission;
    private final Map<DashboardStep, DashboardResultWithIDs> steps;

    DashboardViewData(String version, DashboardViewPermission viewPermission,
            Map<DashboardStep, DashboardResultWithIDs> steps) {

        this.version = version;
        this.viewPermission = viewPermission;
        this.steps = steps;
    }

    public String getVersion() {
        return version;
    }

    public DashboardViewPermission getViewPermission() {
        return viewPermission;
    }

    public int getStepCount(DashboardStep dashboardStep) {
        DashboardResultWithIDs stepData = getStepData(dashboardStep);
        return stepData.getCount();
    }

    public DashboardResultWithIDs getStepData(DashboardStep dashboardStep) {
        DashboardResultWithIDs stepData = getSteps().get(dashboardStep);
        return stepData != null ? stepData : new DashboardResultDTO();
    }

    @Override
    public String toString() {
        if (getSteps().isEmpty()) {
            return "No relevant Dashboard Steps";
        }

        StringBuilder stringBuilder = new StringBuilder("Dashboard Steps:\n");
        getSteps().entrySet().forEach(step -> {
            stringBuilder.append(step.getKey()).append(": ").append(step.getValue().getCount()).append("\n");
        });

        return stringBuilder.toString();
    }

    public Map<DashboardStep, DashboardResultWithIDs> getSteps() {
        return steps;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public static class Builder {

        private String version;
        private DashboardViewPermission viewPermission;
        private Map<DashboardStep, DashboardResultWithIDs> steps;

        public Builder() {
            steps = new HashMap<>();
        }

        public Builder withVersion(String version) {
            this.version = version;
            return this;
        }

        public Builder withViewPermission(DashboardViewPermission viewPermission) {
            this.viewPermission = viewPermission;
            return this;
        }

        public Builder forStep(DashboardStep step, DashboardResultWithIDs stepData) {
            if (stepData != null) {
                steps.put(step, stepData);
            }
            return this;
        }

        public DashboardViewData build() {
            return new DashboardViewData(version, viewPermission, steps);
        }

    }

}
