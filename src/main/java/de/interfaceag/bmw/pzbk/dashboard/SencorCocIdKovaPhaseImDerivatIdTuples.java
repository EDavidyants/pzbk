package de.interfaceag.bmw.pzbk.dashboard;

import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author fn
 */
public class SencorCocIdKovaPhaseImDerivatIdTuples {

    private final List<SencorCocIdKovaPhaseImDerivatIdTuple> tupleList;

    public SencorCocIdKovaPhaseImDerivatIdTuples(List<SencorCocIdKovaPhaseImDerivatIdTuple> tupleList) {
        this.tupleList = tupleList;
    }

    public String getTuplesForQuery() {
        return tupleList.stream().map(SencorCocIdKovaPhaseImDerivatIdTuples::tupleString).collect(Collectors.joining(",", "(", ")"));
    }

    private static String tupleString(SencorCocIdKovaPhaseImDerivatIdTuple tuple) {
        final String sensorCocId = tuple.getSensorCocId().toString();
        final String kovaPhaseImDerivatId = tuple.getKovaPhaseImDerivatId().toString();
        return "(".concat(sensorCocId.concat(",").concat(kovaPhaseImDerivatId)).concat(")");
    }
}
