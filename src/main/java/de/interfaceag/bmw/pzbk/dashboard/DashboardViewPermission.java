package de.interfaceag.bmw.pzbk.dashboard;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.components.ActionMenuViewPermission;
import de.interfaceag.bmw.pzbk.permissions.components.DashboardStepViewPermission;
import de.interfaceag.bmw.pzbk.permissions.components.PersonalMenuViewPermission;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @author fp
 */
public class DashboardViewPermission implements Serializable {

    private final ActionMenuViewPermission actionMenuViewPermission;
    private final DashboardStepViewPermission dashboardStepViewPermission;
    private final PersonalMenuViewPermission personalMenuViewPermission;

    public DashboardViewPermission(List<Rolle> userRoles) {
        actionMenuViewPermission = new ActionMenuViewPermission(userRoles);
        dashboardStepViewPermission = new DashboardStepViewPermission(userRoles);
        personalMenuViewPermission = new PersonalMenuViewPermission(userRoles);

    }

    public boolean getMenuButton() {
        return dashboardStepViewPermission.getMenuButton().isRendered();
    }

    public boolean getStepOneToFourPanel() {
        return dashboardStepViewPermission.getStepOneToFourPanel().isRendered();
    }

    public boolean getStepOne() {
        return dashboardStepViewPermission.getStepOne().isRendered();
    }

    public boolean getStepTwo() {
        return dashboardStepViewPermission.getStepTwo().isRendered();
    }

    public boolean getStepThree() {
        return dashboardStepViewPermission.getStepThree().isRendered();
    }

    public boolean getStepFour() {
        return dashboardStepViewPermission.getStepFour().isRendered();
    }

    public boolean getStepFiveToSevenPanel() {
        return dashboardStepViewPermission.getStepFiveToSevenPanel().isRendered();
    }

    public boolean getStepFive() {
        return dashboardStepViewPermission.getStepFive().isRendered();
    }

    public boolean getStepSix() {
        return dashboardStepViewPermission.getStepSix().isRendered();
    }

    public boolean getStepEightToThirteenPanel() {
        return dashboardStepViewPermission.getStepEightToThirteenPanel().isRendered();
    }

    public boolean getStepEight() {
        return dashboardStepViewPermission.getStepEight().isRendered();
    }

    public boolean getStepNine() {
        return dashboardStepViewPermission.getStepNine().isRendered();
    }

    public boolean getStepTen() {
        return dashboardStepViewPermission.getStepTen().isRendered();
    }

    public boolean getStepEleven() {
        return dashboardStepViewPermission.getStepEleven().isRendered();
    }

    public boolean getStepTwelve() {
        return dashboardStepViewPermission.getStepTwelve().isRendered();
    }

    public boolean getStepThirteen() {
        return dashboardStepViewPermission.getStepThirteen().isRendered();
    }

    public boolean getStepFourteen() {
        return dashboardStepViewPermission.getStepFourteen().isRendered();
    }

    public boolean getArbeitsvorratBerechtigungAntrag() {
        return dashboardStepViewPermission.getArbeitsvorratBerechtigungAntrag().isRendered();
    }

    public boolean getBenutzerverwaltung() {
        return personalMenuViewPermission.getBenutzerverwaltung().isRendered();
    }

    public boolean getAntragsverwaltung() {
        return personalMenuViewPermission.getAntragsverwaltung().isRendered();
    }

    public boolean getSystemLog() {
        return personalMenuViewPermission.getSystemLog().isRendered();
    }

    public boolean getConfig() {
        return personalMenuViewPermission.getConfig().isRendered();
    }

    public boolean getNeueMeldungMenuItem() {
        return actionMenuViewPermission.getNeueMeldungMenuItem().isRendered();
    }

    public boolean getNeueAnforderungMenuItem() {
        return actionMenuViewPermission.getNeueAnforderungMenuItem().isRendered();
    }

    public boolean getReportingAnforderungMenuItem() {
        return actionMenuViewPermission.getReportingAnforderungMenuItem().isRendered();
    }

    public boolean getProjektverwaltungMenuItem() {
        return actionMenuViewPermission.getProjektverwaltungMenuItem().isRendered();
    }

    public boolean getAnforderungZuordnenMenuItem() {
        return actionMenuViewPermission.getAnforderungZuordnenMenuItem().isRendered();
    }

    public boolean getAnforderungVereinbarenMenuItem() {
        return actionMenuViewPermission.getAnforderungVereinbarenMenuItem().isRendered();
    }

    public boolean getBerichtswesenMenuItem() {
        return actionMenuViewPermission.getBerichtswesenMenuItem().isRendered();
    }

    public boolean getUmsetzungsbestaetigungMenuItem() {
        return actionMenuViewPermission.getUmsetzungsbestaetigungMenuItem().isRendered();
    }

    public boolean getReportingProjektMenuItem() {
        return actionMenuViewPermission.getReportingProjektMenuItem().isRendered();
    }

    public boolean getNeuerProzessbaukastenMenuItem() {
        return actionMenuViewPermission.getNeuerProzessbaukastenMenuItem().isRendered();
    }
}
