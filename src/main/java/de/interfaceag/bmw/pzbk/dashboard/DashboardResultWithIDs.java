package de.interfaceag.bmw.pzbk.dashboard;

import java.util.Set;

/**
 *
 * @author evda
 */
public interface DashboardResultWithIDs extends DashboardResult {

    Set<Long> getMeldungIds();

    Set<Long> getAnforderungIds();

    Set<Long> getDerivatAnforderungModulIds();

}
