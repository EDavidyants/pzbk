package de.interfaceag.bmw.pzbk.dashboard;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DashboardResultDTO implements DashboardResultWithIDs, Serializable {

    private Set<Long> anforderungIds;
    private Set<Long> meldungIds;
    private Set<Long> derivatAnforderungModulIds;
    private Integer count;
    private int bestaetigerZuBestimmenNumber;
    private int offeneBestaetigungenNumber;

    DashboardResultDTO() {
        anforderungIds = new HashSet<>();
        meldungIds = new HashSet<>();
        derivatAnforderungModulIds = new HashSet<>();
    }

    void setCount(int count) {
        this.count = count;
    }

    void addMeldungIds(Set<Long> newMeldungIds) {
        if (newMeldungIds != null) {
            meldungIds.addAll(newMeldungIds);
        }
    }

    void addMeldungIds(List<Long> newMeldungIds) {
        if (newMeldungIds != null) {
            meldungIds.addAll(newMeldungIds);
        }
    }

    void addAnforderungIds(Set<Long> newAnforderungIds) {
        if (newAnforderungIds != null) {
            anforderungIds.addAll(newAnforderungIds);
        }
    }

    void addAnforderungIds(List<Long> newAnforderungIds) {
        if (newAnforderungIds != null) {
            anforderungIds.addAll(newAnforderungIds);
        }
    }

    void addDerivatAnforderungModulIds(List<Long> newDerivatAnforderungModulIds) {
        if (newDerivatAnforderungModulIds != null) {
            derivatAnforderungModulIds.addAll(newDerivatAnforderungModulIds);
        }
    }

    void addDerivatAnforderungModulIds(Set<Long> newDerivatAnforderungModulIds) {
        if (newDerivatAnforderungModulIds != null) {
            derivatAnforderungModulIds.addAll(newDerivatAnforderungModulIds);
        }
    }

    void addBestaetigerZuBestimmen(int bestaetigerNumber) {
        this.bestaetigerZuBestimmenNumber += bestaetigerNumber;
    }

    void addOffeneBestaetigungen(int offeneBestaetigungen) {
        this.offeneBestaetigungenNumber += offeneBestaetigungen;
    }

    @Override
    public Set<Long> getAnforderungIds() {
        return anforderungIds;
    }

    public void setAnforderungIds(Set<Long> anforderungIds) {
        this.anforderungIds = anforderungIds;
    }

    @Override
    public Set<Long> getMeldungIds() {
        return meldungIds;
    }

    public void setMeldungIds(Set<Long> meldungIds) {
        this.meldungIds = meldungIds;
    }

    @Override
    public Set<Long> getDerivatAnforderungModulIds() {
        return derivatAnforderungModulIds;
    }


    @Override
    public int getCount() {

        // return count if it is explicitly set by the application
        if (Objects.nonNull(count)) {
            return count;
        }

        int anforderungenCount = anforderungIds != null ? anforderungIds.size() : 0;
        int meldungenCount = meldungIds != null ? meldungIds.size() : 0;
        int derivatAnforderungModulenCount = derivatAnforderungModulIds != null ? derivatAnforderungModulIds.size() : 0;

        return anforderungenCount + meldungenCount + derivatAnforderungModulenCount
                + bestaetigerZuBestimmenNumber + offeneBestaetigungenNumber;
    }

}
