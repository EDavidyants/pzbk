package de.interfaceag.bmw.pzbk.dashboard;

import de.interfaceag.bmw.pzbk.enums.DashboardStep;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author evda
 */
@Stateless
@Named
public class DashboardFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(DashboardFacade.class.getName());

    @Inject
    private DashboardStatusService dashboardStatusService;

    @Inject
    private ConfigService configService;

    @Inject
    private Session session;

    public DashboardViewData getViewData() {

        String version = getVersionFromProperty();

        LOG.debug("Version: {}", version);

        List<Rolle> allUserRollen = getAllUserRollen();

        DashboardViewPermission viewPermission = new DashboardViewPermission(allUserRollen);

        List<Rolle> relevantRollen = getRelevantRollen(allUserRollen);

        Set<DashboardStep> dashboardStepsForUserRoles = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(relevantRollen);

        DashboardViewData viewData = buildWithData(version, viewPermission, dashboardStepsForUserRoles, relevantRollen);


        LOG.debug("DashbaordViewData: {}", viewData);
        return viewData;
    }

    private String getVersionFromProperty() {
        return configService.getVersionFromProperty();
    }

    private DashboardViewData buildWithData(String version, DashboardViewPermission viewPermission, Set<DashboardStep> dashboardStepsForUserRoles, List<Rolle> relevantRollen) {

        DashboardViewData.Builder viewDataBuilder = DashboardViewData.getBuilder()
                .withVersion(version)
                .withViewPermission(viewPermission);


        for (DashboardStep dashboardStep : dashboardStepsForUserRoles) {

            DashboardResultWithIDs stepData = dashboardStatusService.calculateDashboardStepForSession(dashboardStep, relevantRollen);
            viewDataBuilder.forStep(dashboardStep, stepData);
        }

        return viewDataBuilder.build();
    }

    private List<Rolle> getAllUserRollen() {
        return session.getUserPermissions().getRollen();
    }

    private List<Rolle> getRelevantRollen(List<Rolle> rollen) {
        if (rollen == null) {
            return Collections.emptyList();
        }

        return rollen.contains(Rolle.T_TEAMLEITER) || rollen.contains(Rolle.TTEAM_VERTRETER)
                ? rollen.stream().filter(rolle -> !rolle.equals(Rolle.E_COC)).collect(Collectors.toList())
                : rollen;
    }

    public void setLocationForDashboardView() {
        session.setLocationForView();
    }

}
