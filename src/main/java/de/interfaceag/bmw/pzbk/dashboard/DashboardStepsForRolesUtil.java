package de.interfaceag.bmw.pzbk.dashboard;

import de.interfaceag.bmw.pzbk.enums.DashboardStep;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author evda
 */
final class DashboardStepsForRolesUtil {

    private static final Map<Rolle, List<DashboardStep>> ROLE_STEPS_MAP = new HashMap<>();

    static {
        ROLE_STEPS_MAP.put(Rolle.ADMIN, Collections.singletonList(DashboardStep.BERECHTIGUNG_ANTRAG));
        ROLE_STEPS_MAP.put(Rolle.TTEAMMITGLIED, Collections.emptyList());
        ROLE_STEPS_MAP.put(Rolle.SENSOR,
                Arrays.asList(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG,
                        DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG,
                        DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT,
                        DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        ROLE_STEPS_MAP.put(Rolle.SENSOR_EINGESCHRAENKT,
                Arrays.asList(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG,
                        DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG,
                        DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT,
                        DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        ROLE_STEPS_MAP.put(Rolle.SENSORCOCLEITER,
                Arrays.asList(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG,
                        DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG,
                        DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT,
                        DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN,
                        DashboardStep.ANFORDERUNGEN_VEREINBART,
                        DashboardStep.ANFORDERUNGEN_UMGESETZT,
                        DashboardStep.BESTAETIGER_ZUORDNEN,
                        DashboardStep.UMSETZUNG_BESTAETIGEN,
                        DashboardStep.LANGLAEUFER,
                        DashboardStep.BERECHTIGUNG_ANTRAG));
        ROLE_STEPS_MAP.put(Rolle.SCL_VERTRETER,
                Arrays.asList(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG,
                        DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG,
                        DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT,
                        DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN,
                        DashboardStep.ANFORDERUNGEN_VEREINBART,
                        DashboardStep.ANFORDERUNGEN_UMGESETZT,
                        DashboardStep.BESTAETIGER_ZUORDNEN,
                        DashboardStep.UMSETZUNG_BESTAETIGEN,
                        DashboardStep.LANGLAEUFER,
                        DashboardStep.BERECHTIGUNG_ANTRAG));
        ROLE_STEPS_MAP.put(Rolle.UMSETZUNGSBESTAETIGER,
                Collections.singletonList(DashboardStep.UMSETZUNG_BESTAETIGEN));
        ROLE_STEPS_MAP.put(Rolle.E_COC,
                Arrays.asList(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT,
                        DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        ROLE_STEPS_MAP.put(Rolle.T_TEAMLEITER,
                Arrays.asList(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT,
                        DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN,
                        DashboardStep.ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET,
                        DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK,
                        DashboardStep.LANGLAEUFER,
                        DashboardStep.BERECHTIGUNG_ANTRAG));
        ROLE_STEPS_MAP.put(Rolle.TTEAM_VERTRETER,
                Arrays.asList(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT,
                        DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN,
                        DashboardStep.ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET,
                        DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK,
                        DashboardStep.LANGLAEUFER,
                        DashboardStep.BERECHTIGUNG_ANTRAG));
        ROLE_STEPS_MAP.put(Rolle.ANFORDERER,
                Arrays.asList(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN,
                        DashboardStep.NICHT_UMGESETZTEN_ANFORDERUNGEN,
                        DashboardStep.ANFORDERUNGEN_OFFEN_IN_ZAK));
    }

    private DashboardStepsForRolesUtil() {

    }

    static Set<DashboardStep> getDashbaordStepsForRoles(List<Rolle> userRoles) {
        Set<DashboardStep> dashboardSteps = new HashSet<>();
        for (Rolle rolle : userRoles) {
            dashboardSteps.addAll(ROLE_STEPS_MAP.get(rolle));
        }
        return dashboardSteps;
    }

}
