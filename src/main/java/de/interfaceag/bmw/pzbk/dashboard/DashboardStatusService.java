package de.interfaceag.bmw.pzbk.dashboard;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigungantrag.arbeitsvorrat.BerechtigungAntragArbeitsvorratService;
import de.interfaceag.bmw.pzbk.dao.StatusUebergangDao;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.DashboardStep;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.reporting.langlaufer.DashboardLanglauferService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.utils.BerechtigungDtoUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class DashboardStatusService implements Serializable {

    @Inject
    private StatusUebergangDao statusUebergangDao;
    @Inject
    private Session session;
    @Inject
    private DashboardLanglauferService dashboardLanglauferService;
    @Inject
    private BerechtigungAntragArbeitsvorratService berechtigungAntragArbeitsvorratService;

    private List<Rolle> removeRoleEcocForTteamleiter(List<Rolle> rollen) {
        return rollen.contains(Rolle.T_TEAMLEITER) || rollen.contains(Rolle.TTEAM_VERTRETER)
                ? rollen.stream().filter(r -> !r.equals(Rolle.E_COC)).collect(Collectors.toList())
                : rollen;
    }

    public DashboardResultWithIDs getDashboardResultForStep(int stepNumber) {

        Optional<DashboardStep> step = DashboardStep.getByStepNumber(stepNumber);

        if (!step.isPresent()) {
            return new DashboardResultDTO();
        }

        Mitarbeiter currentUser = session.getUser();
        if (currentUser == null) {
            return new DashboardResultDTO();
        }

        List<Rolle> mitarbeiterRollen = session.getUserPermissions().getRollen();
        if (mitarbeiterRollen == null || mitarbeiterRollen.isEmpty()) {
            return new DashboardResultDTO();
        }

        if (mitarbeiterRollen.contains(Rolle.ADMIN)) {
            return new DashboardResultDTO();
        }

        mitarbeiterRollen = removeRoleEcocForTteamleiter(mitarbeiterRollen);

        return calculateDashboardStep(step.get(), currentUser, mitarbeiterRollen);
    }

    public DashboardResultDTO calculateDashboardStepForSession(DashboardStep step, List<Rolle> relevantRollen) {
        if (DashboardStep.BERECHTIGUNG_ANTRAG.equals(step)) {
            return calculateBerechtigungAntragArbeitsvorrat();
        } else {
            return calculateDashboardStep(step, session.getUser(), relevantRollen);
        }
    }

    private DashboardResultDTO calculateBerechtigungAntragArbeitsvorrat() {
        final int arbeitsvorratForCurrentUser = berechtigungAntragArbeitsvorratService.getArbeitsvorratForCurrentUser();
        DashboardResultDTO result = new DashboardResultDTO();
        result.setCount(arbeitsvorratForCurrentUser);
        return result;
    }

    private DashboardResultDTO calculateDashboardStep(DashboardStep step, Mitarbeiter currentUser, List<Rolle> mitarbeiterRollen) {
        DashboardResultDTO result = new DashboardResultDTO();

        mitarbeiterRollen.forEach(role -> {
            DashboardResultDTO stepResult;

            switch (step) {
                case MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG:
                    stepResult = processStepOne(currentUser, role);
                    result.addMeldungIds(stepResult.getMeldungIds());
                    break;
                case GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG:
                    stepResult = processStepTwo(currentUser, role);
                    result.addAnforderungIds(stepResult.getAnforderungIds());
                    result.addMeldungIds(stepResult.getMeldungIds());
                    break;
                case ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT:
                    stepResult = processStepThree(currentUser, role);
                    result.addAnforderungIds(stepResult.getAnforderungIds());
                    result.addMeldungIds(stepResult.getMeldungIds());
                    break;
                case FREIGEGEBENEN_ANFORDERUNGEN:
                    stepResult = processStepFour(currentUser, role);
                    result.addAnforderungIds(stepResult.getAnforderungIds());
                    break;
                case ANFORDERUNGEN_VEREINBART:
                    stepResult = processStepFive(role);
                    result.addAnforderungIds(stepResult.getAnforderungIds());
                    break;
                case ANFORDERUNGEN_UMGESETZT:
                    stepResult = processStepSix(role);
                    result.addAnforderungIds(stepResult.getAnforderungIds());
                    break;
                case BESTAETIGER_ZUORDNEN:
                    int bestaetigerZuBestimmen = processStepEight(role);
                    result.addBestaetigerZuBestimmen(bestaetigerZuBestimmen);
                    break;
                case UMSETZUNG_BESTAETIGEN:
                    int offeneBestaetigungen = processStepNine(currentUser, role);
                    result.addOffeneBestaetigungen(offeneBestaetigungen);
                    break;
                case ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET:
                    stepResult = processStepTen(role);
                    result.addAnforderungIds(stepResult.getAnforderungIds());
                    break;
                case NICHT_UMGESETZTEN_ANFORDERUNGEN:
                    stepResult = processStepEleven(role);
                    result.addDerivatAnforderungModulIds(stepResult.getDerivatAnforderungModulIds());
                    break;
                case ANFORDERUNGEN_OFFEN_IN_ZAK:
                    stepResult = processStepTwelve(role);
                    result.addDerivatAnforderungModulIds(stepResult.getDerivatAnforderungModulIds());
                    break;
                case ANFORDERUNGEN_ABGELEHNT_IN_ZAK:
                    stepResult = processStepThirteen(role);
                    result.addDerivatAnforderungModulIds(stepResult.getDerivatAnforderungModulIds());
                    break;
                case LANGLAEUFER:
                    stepResult = processStepFourteen(role);
                    result.addAnforderungIds(stepResult.getAnforderungIds());
                    result.addMeldungIds(stepResult.getMeldungIds());
                    break;
                default:
                    break;
            }
        });

        return result;
    }

    private DashboardResultDTO processStepOne(Mitarbeiter currentUser, Rolle role) {
        DashboardResultDTO stepResult = new DashboardResultDTO();
        List<Status> statusList = Arrays.asList(Status.M_ENTWURF, Status.M_UNSTIMMIG);

        List<BerechtigungDto> berechtigungen;
        List<Long> sensorCocIds;
        switch (role) {
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
                berechtigungen = getBerechtigungenForRole(role);
                sensorCocIds = BerechtigungDtoUtils.selectSensorCocBerechtigingen(berechtigungen);
                stepResult.addMeldungIds(statusUebergangDao.getMeldungenForSensorInStatus(statusList, currentUser, sensorCocIds));
                break;

            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                berechtigungen = getBerechtigungenForRole(role);
                sensorCocIds = BerechtigungDtoUtils.selectSensorCocBerechtigingen(berechtigungen);
                stepResult.addMeldungIds(statusUebergangDao.getMeldungenForSensorCocLeiterInStatus(statusList, sensorCocIds));
                break;
            default:
                break;
        }

        return stepResult;
    }

    private DashboardResultDTO processStepTwo(Mitarbeiter currentUser, Rolle role) {
        DashboardResultDTO stepResult = new DashboardResultDTO();
        List<Status> statusList = Arrays.asList(Status.M_GEMELDET, Status.A_INARBEIT, Status.A_UNSTIMMIG);

        List<BerechtigungDto> berechtigungen;
        List<Long> sensorCocIds;
        switch (role) {
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
                berechtigungen = getBerechtigungenForRole(role);
                sensorCocIds = BerechtigungDtoUtils.selectSensorCocBerechtigingen(berechtigungen);
                stepResult.addMeldungIds(statusUebergangDao.getMeldungenForSensorInStatus(statusList, currentUser, sensorCocIds));
                stepResult.addAnforderungIds(statusUebergangDao.getAnforderungenForSensorInStatus(statusList, currentUser, sensorCocIds));
                break;
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                berechtigungen = getBerechtigungenForRole(role);
                sensorCocIds = BerechtigungDtoUtils.selectSensorCocBerechtigingen(berechtigungen);
                stepResult.addMeldungIds(statusUebergangDao.getMeldungenForSensorCocLeiterInStatus(statusList, sensorCocIds));
                stepResult.addAnforderungIds(statusUebergangDao.getAnforderungenForSensorCocLeiterInStatus(statusList, sensorCocIds));
                break;
            default:
                break;
        }

        return stepResult;
    }

    private DashboardResultDTO processStepThree(Mitarbeiter currentUser, Rolle role) {
        DashboardResultDTO stepResult = new DashboardResultDTO();

        List<Status> statusList = new ArrayList<>();
        statusList.add(Status.A_PLAUSIB);

        List<BerechtigungDto> berechtigungen;
        List<Long> sensorCocIds;
        List<Long> tteamIds;

        switch (role) {
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
                statusList.add(Status.A_FTABGESTIMMT);
                berechtigungen = getBerechtigungenForRole(role);
                sensorCocIds = BerechtigungDtoUtils.selectSensorCocBerechtigingen(berechtigungen);
                stepResult.addAnforderungIds(statusUebergangDao.getAnforderungenForSensorInStatus(statusList, currentUser, sensorCocIds));
                break;
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                statusList.add(Status.A_FTABGESTIMMT);
                berechtigungen = getBerechtigungenForRole(role);
                sensorCocIds = BerechtigungDtoUtils.selectSensorCocBerechtigingen(berechtigungen);
                stepResult.addAnforderungIds(statusUebergangDao.getAnforderungenForSensorCocLeiterInStatus(statusList, sensorCocIds));
                break;
            case T_TEAMLEITER:
                statusList.add(Status.A_FTABGESTIMMT);
                berechtigungen = getBerechtigungenForRole(role);
                tteamIds = BerechtigungDtoUtils.selectTteamBerechtigungen(berechtigungen);
                stepResult.addAnforderungIds(statusUebergangDao.getAnforderungenForTteamLeiterInStatus(statusList, tteamIds));
                break;
            case TTEAM_VERTRETER:
                statusList.add(Status.A_FTABGESTIMMT);
                tteamIds = session.getUserPermissions().getTteamIdsForTteamVertreter();
                stepResult.addAnforderungIds(statusUebergangDao.getAnforderungenForTteamLeiterInStatus(statusList, tteamIds));
                break;
            case E_COC:
                berechtigungen = getBerechtigungenForRole(role);
                List<Long> modulSeTeamIds = BerechtigungDtoUtils.selectModulSeTeamBerechtigingen(berechtigungen);
                stepResult.addAnforderungIds(statusUebergangDao.getAnforderungenForEcocInStatus(statusList, modulSeTeamIds));
                break;
            default:
                break;
        }

        return stepResult;
    }

    private DashboardResultDTO processStepFour(Mitarbeiter currentUser, Rolle role) {
        DashboardResultDTO stepResult = new DashboardResultDTO();

        List<Status> statusList = new ArrayList<>();
        statusList.add(Status.A_FREIGEGEBEN);

        List<BerechtigungDto> berechtigungen;
        List<Long> sensorCocIds;
        List<Long> tteamIds;

        switch (role) {
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
                berechtigungen = getBerechtigungenForRole(role);
                sensorCocIds = BerechtigungDtoUtils.selectSensorCocBerechtigingen(berechtigungen);
                stepResult.addAnforderungIds(statusUebergangDao.getAnforderungenForSensorInStatus(statusList, currentUser, sensorCocIds));
                break;
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                berechtigungen = getBerechtigungenForRole(role);
                sensorCocIds = BerechtigungDtoUtils.selectSensorCocBerechtigingen(berechtigungen);
                stepResult.addAnforderungIds(statusUebergangDao.getAnforderungenForSensorCocLeiterInStatus(statusList, sensorCocIds));
                break;
            case T_TEAMLEITER:
                berechtigungen = getBerechtigungenForRole(role);
                tteamIds = BerechtigungDtoUtils.selectTteamBerechtigungen(berechtigungen);
                stepResult.addAnforderungIds(statusUebergangDao.getAnforderungenForTteamLeiterInStatus(statusList, tteamIds));
                break;
            case TTEAM_VERTRETER:
                tteamIds = session.getUserPermissions().getTteamIdsForTteamVertreter();
                stepResult.addAnforderungIds(statusUebergangDao.getAnforderungenForTteamLeiterInStatus(statusList, tteamIds));
                break;
            case E_COC:
                berechtigungen = getBerechtigungenForRole(role);
                List<Long> modulSeTeamIds = BerechtigungDtoUtils.selectModulSeTeamBerechtigingen(berechtigungen);
                stepResult.addAnforderungIds(statusUebergangDao.getAnforderungenForEcocInStatus(statusList, modulSeTeamIds));
                break;
            case ANFORDERER:
                berechtigungen = getBerechtigungenForRole(role);
                List<String> zakEinordnungen = BerechtigungDtoUtils.selectZakEinordnungBerechtigingen(berechtigungen);
                stepResult.addAnforderungIds(statusUebergangDao.getAnforderungenForAnfordererInStatus(statusList, zakEinordnungen));
                break;
            default:
                break;
        }

        return stepResult;
    }

    private DashboardResultDTO processStepFive(Rolle role) {
        DashboardResultDTO stepResult = new DashboardResultDTO();

        if (role == Rolle.SENSORCOCLEITER || role == Rolle.SCL_VERTRETER) {
            List<BerechtigungDto> berechtigungen = getBerechtigungenForRole(role);
            List<Long> sensorCocIds = BerechtigungDtoUtils.selectSensorCocBerechtigungenForSCLAndVertreter(berechtigungen);
            stepResult.addAnforderungIds(statusUebergangDao.processStepFive(sensorCocIds));
        }

        return stepResult;
    }

    private DashboardResultDTO processStepSix(Rolle role) {
        DashboardResultDTO stepResult = new DashboardResultDTO();

        if (role == Rolle.SENSORCOCLEITER || role == Rolle.SCL_VERTRETER) {
            List<BerechtigungDto> berechtigungen = getBerechtigungenForRole(role);
            List<Long> sensorCocIds = BerechtigungDtoUtils.selectSensorCocBerechtigungenForSCLAndVertreter(berechtigungen);
            stepResult.addAnforderungIds(statusUebergangDao.processStepSix(sensorCocIds));
        }

        return stepResult;
    }

    private int processStepEight(Rolle role) {
        int stepResult = 0;

        if (role == Rolle.SENSORCOCLEITER || role == Rolle.SCL_VERTRETER) {
            List<BerechtigungDto> berechtigungen = getBerechtigungenForRole(role);
            List<Long> sensorCocIds = BerechtigungDtoUtils.selectSensorCocBerechtigungenForSCLAndVertreter(berechtigungen);
            stepResult = statusUebergangDao.processStepEight(sensorCocIds);
        }

        return stepResult;
    }

    private int processStepNine(Mitarbeiter currentUser, Rolle role) {
        int stepResult = 0;

        if (role == Rolle.UMSETZUNGSBESTAETIGER) {
            List<BerechtigungDto> berechtigungen = getBerechtigungenForRole(role);
            List<Long> sensorCocIds = BerechtigungDtoUtils.selectSensorCocBerechtigungenForUmsetzungbestaetigung(berechtigungen);
            stepResult = statusUebergangDao.processStepNine(currentUser, sensorCocIds);
        }

        return stepResult;
    }

    private DashboardResultDTO processStepTen(Rolle role) {
        DashboardResultDTO stepResult = new DashboardResultDTO();

        if (role == Rolle.T_TEAMLEITER) {
            List<BerechtigungDto> berechtigungen = getBerechtigungenForRole(role);
            List<Long> tteamIds = BerechtigungDtoUtils.selectTteamBerechtigungen(berechtigungen);
            stepResult.addAnforderungIds(statusUebergangDao.processStepTen(tteamIds));

        } else if (role == Rolle.TTEAM_VERTRETER) {
            List<Long> tteamIds = session.getUserPermissions().getTteamIdsForTteamVertreter();
            stepResult.addAnforderungIds(statusUebergangDao.processStepTen(tteamIds));
        }

        return stepResult;
    }

    private DashboardResultDTO processStepEleven(Rolle role) {
        DashboardResultDTO stepResult = new DashboardResultDTO();

        if (role == Rolle.ANFORDERER) {
            List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus = Collections.singletonList(UmsetzungsBestaetigungStatus.NICHT_UMGESETZT);
            List<BerechtigungDto> berechtigungen = getBerechtigungenForRole(role);
            List<String> zakEinordnungen = BerechtigungDtoUtils.selectZakEinordnungBerechtigingen(berechtigungen);
            List<Long> derivatIds = BerechtigungDtoUtils.selectDerivatBerechtigungen(berechtigungen);
            stepResult.addDerivatAnforderungModulIds(statusUebergangDao.getUmsetzungInZAKForAnfordererByUBStatus(zakEinordnungen, derivatIds, umsetzungsBestaetigungStatus));
        }

        return stepResult;
    }

    private DashboardResultDTO processStepTwelve(Rolle role) {
        DashboardResultDTO stepResult = new DashboardResultDTO();

        if (role == Rolle.ANFORDERER) {
            List<ZakStatus> zakStatus = Collections.singletonList(ZakStatus.PENDING);
            List<BerechtigungDto> berechtigungen = getBerechtigungenForRole(role);
            List<String> zakEinordnungen = BerechtigungDtoUtils.selectZakEinordnungBerechtigingen(berechtigungen);
            List<Long> derivatIds = BerechtigungDtoUtils.selectDerivatBerechtigungen(berechtigungen);
            stepResult.addDerivatAnforderungModulIds(statusUebergangDao.getUmsetzungInZAKForAnfordererByZakStatus(zakEinordnungen, derivatIds, zakStatus));
        }

        return stepResult;
    }

    private DashboardResultDTO processStepThirteen(Rolle role) {
        DashboardResultDTO stepResult = new DashboardResultDTO();

        if (role == Rolle.T_TEAMLEITER) {
            List<ZakStatus> zakStatus = Collections.singletonList(ZakStatus.NICHT_UMSETZBAR);
            List<BerechtigungDto> berechtigungen = getBerechtigungenForRole(role);
            List<Long> tteamIds = BerechtigungDtoUtils.selectTteamBerechtigungen(berechtigungen);
            stepResult.addDerivatAnforderungModulIds(statusUebergangDao.getUmsetzungInZAKForTteamleiter(tteamIds, zakStatus));

        } else if (role == Rolle.TTEAM_VERTRETER) {
            List<ZakStatus> zakStatus = Collections.singletonList(ZakStatus.NICHT_UMSETZBAR);
            List<Long> tteamIds = session.getUserPermissions().getTteamIdsForTteamVertreter();
            stepResult.addDerivatAnforderungModulIds(statusUebergangDao.getUmsetzungInZAKForTteamleiter(tteamIds, zakStatus));
        }

        return stepResult;
    }

    /**
     * step for langlaufer
     */
    private DashboardResultDTO processStepFourteen(Rolle role) {
        DashboardResultDTO stepResult = new DashboardResultDTO();

        if (role == Rolle.T_TEAMLEITER || role == Rolle.TTEAM_VERTRETER) {
            stepResult.addAnforderungIds(this.dashboardLanglauferService.getTteamLanglauferIds());

        } else if (role == Rolle.SENSORCOCLEITER || role == Rolle.SCL_VERTRETER) {
            stepResult.addAnforderungIds(this.dashboardLanglauferService.getFachteamLanglauferAnforderungIds());
            stepResult.addMeldungIds(this.dashboardLanglauferService.getFachteamLanglauferMeldungIds());
        }

        return stepResult;
    }

    private List<BerechtigungDto> getBerechtigungenForAnforderer() {
        List<BerechtigungDto> berechtigungen = new ArrayList<>();
        berechtigungen.addAll(session.getUserPermissions().getZakEinordnungAsAnfordererSchreibend());
        berechtigungen.addAll(session.getUserPermissions().getDerivatAsAnfordererSchreibend());
        return berechtigungen;
    }

    private List<BerechtigungDto> getBerechtigungenForRole(Rolle role) {
        List<BerechtigungDto> berechtigungen = new ArrayList<>();

        if (role != null) {
            switch (role) {
                case SENSOR:
                    berechtigungen = session.getUserPermissions().getSensorCocAsSensorSchreibend();
                    break;
                case SENSOR_EINGESCHRAENKT:
                    berechtigungen = session.getUserPermissions().getSensorCocAsSensorEingSchreibend();
                    break;
                case SENSORCOCLEITER:
                    berechtigungen = session.getUserPermissions().getSensorCocAsSensorCocLeiterSchreibend();
                    break;
                case SCL_VERTRETER:
                    berechtigungen = session.getUserPermissions().getSensorCocAsVertreterSCLSchreibend();
                    break;
                case T_TEAMLEITER:
                    berechtigungen = session.getUserPermissions().getTteamAsTteamleiterSchreibend();
                    break;
                case E_COC:
                    berechtigungen = session.getUserPermissions().getModulSeTeamAsEcocSchreibend();
                    break;
                case ANFORDERER:
                    berechtigungen = getBerechtigungenForAnforderer();
                    break;
                case UMSETZUNGSBESTAETIGER:
                    berechtigungen = session.getUserPermissions().getSensorCocAsUmsetzungsbestaetigerSchreibend();
                    break;
                default:
                    break;
            }
        }

        return berechtigungen;
    }

    public int getDashboardCountForStep(int stepNumber) {
        DashboardResultWithIDs result = getDashboardResultForStep(stepNumber);
        return result.getCount();
    }

}
