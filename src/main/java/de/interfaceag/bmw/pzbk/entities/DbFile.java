package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Christian Schauer
 */
@Entity
@NamedQueries( {
        @NamedQuery(name = DbFile.DBDATEI_BY_NAME, query = "select f from DbFile f where f.name LIKE :name"),
        @NamedQuery(name = DbFile.DBDATEI_BY_ID, query = "select f from DbFile f where f.id = :id")})
public class DbFile implements Serializable {

    // ------------  queries ---------------------------------------------------    
    public static final String DBDATEI_BY_ID = "dbDateiById";
    public static final String DBDATEI_BY_NAME = "dbDateiByName";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "dbfile_id_seq",
            sequenceName = "dbfile_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dbfile_id_seq")
    private long id;

    @Column(name = "FILECONTENT")
    private byte[] content;
    @Column(name = "FILENAME")
    private String name;
    @Column(name = "FILETYPE")
    private String type;
    @Column(name = "CATEGORY")
    private String category;
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    // save to filesystem
    @Column(length = 1000)
    private String filePath;

    // ------------  constructors ----------------------------------------------
    public DbFile() {
    }

    public DbFile(String name, String type, String filePath) {
        this.name = name;
        this.type = type;
        this.content = null;
        this.creationDate = new Date(System.currentTimeMillis());
        this.filePath = filePath;
    }

    public DbFile(String name, String type, byte[] content) {
        this.name = name;
        this.type = type;
        this.content = content != null ? content.clone() : null;
        this.creationDate = new Date(System.currentTimeMillis());
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return " " + this.name;
    }

    public int getSize() {
        if (content == null) {
            return 0;
        } else {
            return content.length;
        }
    }

    public DbFile getCopy() {
        DbFile file = new DbFile();
        file.setName(name);
        file.setType(type);
        file.setCategory(category);
        file.setContent(content);
        file.setCreationDate(creationDate);
        file.setFilePath(filePath);
        return file;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(151, 631);
        hb.append(id);
        hb.append(name);
        hb.append(type);
        hb.append(category);
        hb.append(content);

        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof DbFile)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        DbFile other = (DbFile) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, other.id);
        eb.append(name, other.name);
        eb.append(type, other.type);
        eb.append(category, other.category);
        eb.append(content, other.content);

        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte[] getContent() {
        return content != null ? content.clone() : null;
    }

    public void setContent(byte[] content) {
        this.content = content != null ? content.clone() : null;
    }

    public Date getCreationDate() {
        return new Date(creationDate.getTime());
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = new Date(creationDate.getTime());
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

} // end of class
