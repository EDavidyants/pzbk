package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.AntragStatus;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.shared.objectIds.DomainObject;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * @author evda
 */
@Entity
@NamedQuery(name = Berechtigungsantrag.ALL_FOR_VIEW, query = "SELECT DISTINCT antrag FROM Berechtigungsantrag antrag WHERE antrag.status = 0 ORDER BY antrag.antragsteller")
@NamedQuery(name = Berechtigungsantrag.BY_ID, query = "SELECT antrag FROM Berechtigungsantrag antrag WHERE antrag.id = :id")
@NamedQuery(name = Berechtigungsantrag.GET_ALL_NOT_OFFEN, query = "SELECT ba FROM Berechtigungsantrag ba WHERE ba.status != 0 ORDER BY ba.aenderungsdatum DESC")
@NamedQuery(name = Berechtigungsantrag.GET_ANTRAEGE_BY_ANTRAGSTELLER_ROLLE_RECHTTYP_SENSOR_COC, query = "SELECT ba FROM Berechtigungsantrag ba"
        + " WHERE ba.antragsteller = :antragsteller "
        + "AND ba.rolle = :rolle "
        + "AND ba.rechttype = :rechttype "
        + "AND ba.sensorCoc = :sensorCoc")
@NamedQuery(name = Berechtigungsantrag.GET_ANTRAEGE_BY_ANTRAGSTELLER_ROLLE_RECHTTYP_TTEAM, query = "SELECT ba FROM Berechtigungsantrag ba"
        + " WHERE ba.antragsteller = :antragsteller "
        + "AND ba.rolle = :rolle "
        + "AND ba.rechttype = :rechttype "
        + "AND ba.tteam = :tteam")
@NamedQuery(name = Berechtigungsantrag.GET_ANTRAEGE_BY_ANTRAGSTELLER_ROLLE_RECHTTYP_MODUL_SE_TEAM, query = "SELECT ba FROM Berechtigungsantrag ba"
        + " WHERE ba.antragsteller = :antragsteller "
        + "AND ba.rolle = :rolle "
        + "AND ba.rechttype = :rechttype "
        + "AND ba.modulSeTeam = :modulSeTeam")
public class Berechtigungsantrag implements Serializable, DomainObject {

    private static final String CLASSNAME = "Berechtigungsantrag.";
    public static final String ALL_FOR_VIEW = CLASSNAME + "all";
    public static final String BY_ID = CLASSNAME + "byId";
    public static final String GET_ALL_NOT_OFFEN = CLASSNAME + "getAllNotOffen";
    public static final String GET_ANTRAEGE_BY_ANTRAGSTELLER_ROLLE_RECHTTYP_SENSOR_COC = "getAntraegeByAntragstellerRolleRechttypSensorCoc";
    public static final String GET_ANTRAEGE_BY_ANTRAGSTELLER_ROLLE_RECHTTYP_TTEAM = "getAntraegeByAntragstellerRolleRechttypTteam";
    public static final String GET_ANTRAEGE_BY_ANTRAGSTELLER_ROLLE_RECHTTYP_MODUL_SE_TEAM = "getAntraegeByAntragstellerRolleRechttypModulSeTeam";

    @Id
    @SequenceGenerator(name = "berechtigungsantrag_id_seq",
            sequenceName = "berechtigungsantrag_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "berechtigungsantrag_id_seq")
    @Column(nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "antragsteller_id", referencedColumnName = "id", nullable = false)
    private Mitarbeiter antragsteller;

    @Column(nullable = false)
    private Integer status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bearbeiter_id", referencedColumnName = "id")
    private Mitarbeiter bearbeiter;

    @Column(length = 10000)
    private String kommentar;

    @Column(nullable = false)
    private Integer rolle;

    @Column(nullable = false)
    private Integer rechttype;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sensorcoc_id", referencedColumnName = "sensorcocid")
    private SensorCoc sensorCoc;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tteam_id", referencedColumnName = "id")
    private Tteam tteam;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modulseteam_id", referencedColumnName = "id")
    private ModulSeTeam modulSeTeam;

    @Temporal(TemporalType.TIMESTAMP)
    private Date erstellungsdatum;

    @Temporal(TemporalType.TIMESTAMP)
    private Date aenderungsdatum;

    public Berechtigungsantrag() {
        this.status = 0;
        this.kommentar = "";
        this.erstellungsdatum = new Date();
        this.aenderungsdatum = new Date();
    }

    public Berechtigungsantrag(Mitarbeiter antragsteller, Rolle rolle, Rechttype rechttype, AntragStatus status) {
        this.antragsteller = antragsteller;
        this.rolle = rolle.getId();
        this.rechttype = rechttype.getId();
        this.status = status.getId();
        this.kommentar = "";
        this.erstellungsdatum = new Date();
        this.aenderungsdatum = new Date();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hashCodeBuilder = new HashCodeBuilder(11, 389);
        return hashCodeBuilder.append(id)
                .append(antragsteller)
                .append(status)
                .append(bearbeiter)
                .append(kommentar)
                .append(rolle)
                .append(rechttype)
                .append(sensorCoc)
                .append(tteam)
                .append(modulSeTeam)
                .append(erstellungsdatum)
                .append(aenderungsdatum)
                .toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Berechtigungsantrag)) {
            return false;
        }
        if (object == this) {
            return true;
        }

        Berechtigungsantrag other = (Berechtigungsantrag) object;
        EqualsBuilder equalsBuilder = new EqualsBuilder();
        return equalsBuilder.append(id, other.id)
                .append(antragsteller, other.antragsteller)
                .append(status, other.status)
                .append(bearbeiter, other.bearbeiter)
                .append(kommentar, other.kommentar)
                .append(rolle, other.rolle)
                .append(rechttype, other.rechttype)
                .append(sensorCoc, other.sensorCoc)
                .append(tteam, other.tteam)
                .append(modulSeTeam, other.modulSeTeam)
                .append(erstellungsdatum, other.erstellungsdatum)
                .append(aenderungsdatum, other.aenderungsdatum)
                .isEquals();
    }

    @Override
    public String toString() {
        String erstelltAm = "";
        if (erstellungsdatum != null) {
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            erstelltAm = " am " + format.format(erstellungsdatum);
        }

        String antragVon = antragsteller.toString();
        String forRolle = getRolle().toString();
        String mitRechttype = getRechttype().toString();
        String fuer = getBerechtigungZiel();

        String mitBeschluss = getAntragBeschluss();

        return "Berechtigungsantrag ID " + id
                + " von " + antragVon
                + " fuer Rolle " + forRolle
                + " mit Rechttype " + mitRechttype
                + " fuer " + fuer
                + " wurde" + erstelltAm + " erstellt.\n"
                + mitBeschluss;
    }

    public String getBerechtigungZiel() {
        if (sensorCoc != null) {
            return "SensorCoC '" + sensorCoc.getTechnologie() + "'";
        }

        if (tteam != null) {
            return "T-Team '" + tteam.toString() + "'";
        }

        if (modulSeTeam != null) {
            return "ModulSeTeam '" + modulSeTeam.toString() + "'";
        }

        return "Kein gueltige Berechtigungsziel";
    }

    String getAntragBeschluss() {
        if (getStatus() == AntragStatus.OFFEN) {
            return "Beschluss steht aus.";
        }

        String bearbeitetAm = "";
        if (aenderungsdatum != null) {
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            bearbeitetAm = " am " + format.format(aenderungsdatum);
        }

        String bearbeitetVon = " von " + bearbeiter.toString();
        String mitKommentar = kommentar != null && !kommentar.isEmpty() ? " mit Kommentar '" + kommentar + "'" : "";
        return getStatus().getBezeichnung() + bearbeitetAm + bearbeitetVon + mitKommentar + ".";
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Mitarbeiter getAntragsteller() {
        return antragsteller;
    }

    public void setAntragsteller(Mitarbeiter antragsteller) {
        this.antragsteller = antragsteller;
    }

    public AntragStatus getStatus() {
        return AntragStatus.getById(status);
    }

    public void setStatus(AntragStatus status) {
        this.status = status.getId();
    }

    public Mitarbeiter getBearbeiter() {
        return bearbeiter;
    }

    public void setBearbeiter(Mitarbeiter bearbeiter) {
        this.bearbeiter = bearbeiter;
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    public Rolle getRolle() {
        return Rolle.getRolleById(rolle);
    }

    public void setRolle(Rolle rolle) {
        this.rolle = rolle.getId();
    }

    public Rechttype getRechttype() {
        return Rechttype.getById(rechttype);
    }

    public void setRechttype(Rechttype rechttype) {
        this.rechttype = rechttype.getId();
    }

    public SensorCoc getSensorCoc() {
        return sensorCoc;
    }

    public void setSensorCoc(SensorCoc sensorCoc) {
        this.sensorCoc = sensorCoc;
    }

    public Tteam getTteam() {
        return tteam;
    }

    public void setTteam(Tteam tteam) {
        this.tteam = tteam;
    }

    public ModulSeTeam getModulSeTeam() {
        return modulSeTeam;
    }

    public void setModulSeTeam(ModulSeTeam modulSeTeam) {
        this.modulSeTeam = modulSeTeam;
    }

    public Date getErstellungsdatum() {
        return erstellungsdatum;
    }

    public void setErstellungsdatum(Date erstellungsdatum) {
        this.erstellungsdatum = erstellungsdatum;
    }

    public Date getAenderungsdatum() {
        return aenderungsdatum;
    }

    public void setAenderungsdatum(Date aenderungsdatum) {
        this.aenderungsdatum = aenderungsdatum;
    }

}
