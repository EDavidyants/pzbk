package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 * @author Stefan Luchs sl
 */
@Entity
@NamedQuery(name = Bild.BY_ID, query = "SELECT b FROM Bild b WHERE b.id = :id")
public class Bild implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String BY_ID = "BILD.by_id";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "bild_id_seq",
            sequenceName = "bild_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bild_id_seq")
    private Long id;

    private byte[] thumbnailImage;

    private byte[] normalImage;

    private byte[] largeImage;

    // ------------  constructors ----------------------------------------------
    public Bild() {

    }

    public Bild(byte[] thumbnailImage, byte[] normalImage, byte[] largeImage) {
        this.thumbnailImage = thumbnailImage.clone();
        this.normalImage = normalImage.clone();
        this.largeImage = largeImage.clone();
    }

    // ------------  methods ---------------------------------------------------    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Bild: ").append(id);
        return sb.toString();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(23, 463);
        hb.append(id);
        hb.append(thumbnailImage);
        hb.append(normalImage);
        hb.append(largeImage);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Bild)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        Bild other = (Bild) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, other.id);
        eb.append(thumbnailImage, other.thumbnailImage);
        eb.append(normalImage, other.normalImage);
        eb.append(largeImage, other.largeImage);
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getThumbnailImage() {
        return thumbnailImage != null ? thumbnailImage.clone() : null;
    }

    public void setThumbnailImage(byte[] thumbnailImage) {
        this.thumbnailImage = thumbnailImage != null ? thumbnailImage.clone() : null;
    }

    public byte[] getNormalImage() {
        return normalImage != null ? normalImage.clone() : null;
    }

    public void setNormalImage(byte[] normalImage) {
        this.normalImage = normalImage != null ? normalImage.clone() : null;
    }

    public byte[] getLargeImage() {
        return largeImage != null ? largeImage.clone() : null;
    }

    public void setLargeImage(byte[] largeImage) {
        this.largeImage = largeImage != null ? largeImage.clone() : null;
    }

    public Bild getCopy() {
        Bild bild = new Bild();
        bild.setLargeImage(this.getLargeImage());
        bild.setNormalImage(this.getNormalImage());
        bild.setThumbnailImage(this.getThumbnailImage());
        return bild;
    }

}
