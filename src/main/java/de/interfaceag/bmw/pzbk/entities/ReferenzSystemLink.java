package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.ReferenzSystem;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author evda
 */
@Entity
@Table(name = "referenz_system_link")
public class ReferenzSystemLink implements Serializable {

    // TODO move to entities package
    public static final String CLASSNAME = "ReferenzSystemLink.";
    public static final String GET_BY_ANFORDERUNG = CLASSNAME + "getByAnforderung";
    public static final String GET_BY_ANFORDERUNG_ID = CLASSNAME + "getByAnforderungId";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "ref_system_link_id_seq",
            sequenceName = "ref_system_link_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ref_system_link_id_seq")
    private Long id;

    private Integer referenzSystem;

    @Column(name = "referenzsystem_id")
    private String referenzSystemId;

    public ReferenzSystemLink() {

    }

    public ReferenzSystemLink(ReferenzSystem referenzSystem, String referenzSystemId) {
        this.referenzSystem = referenzSystem.getId();
        this.referenzSystemId = referenzSystemId;
    }

    @Override
    public String toString() {
        return referenzSystem.toString() + " : " + referenzSystemId;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(47, 479);
        hb.append(id);
        hb.append(referenzSystem);
        hb.append(referenzSystemId);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ReferenzSystemLink)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        ReferenzSystemLink rsl = (ReferenzSystemLink) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, rsl.id);
        eb.append(referenzSystem, rsl.referenzSystem);
        eb.append(referenzSystemId, rsl.referenzSystemId);
        return eb.isEquals();
    }

    public ReferenzSystemLink copy() {
        ReferenzSystemLink clone = new ReferenzSystemLink();
        clone.setReferenzsystem(this.getReferenzsystem());
        clone.setReferenzsystemId(this.getReferenzsystemId());
        return clone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ReferenzSystem getReferenzsystem() {
        return ReferenzSystem.getById(referenzSystem);
    }

    public void setReferenzsystem(ReferenzSystem referenzSystem) {
        if (referenzSystem != null) {
            this.referenzSystem = referenzSystem.getId();
        } else {
            this.referenzSystem = null;
        }
    }

    public String getReferenzsystemId() {
        return referenzSystemId;
    }

    public void setReferenzsystemId(String referenzsystemId) {
        this.referenzSystemId = referenzsystemId;
    }

}
