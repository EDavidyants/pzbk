package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 * @author ig
 */
@Entity
@NamedQueries( {
        @NamedQuery(name = InfoText.ALL, query = "SELECT i FROM InfoText i ORDER BY i.feldName"),
        @NamedQuery(name = InfoText.BY_ID, query = "SELECT i FROM InfoText i WHERE i.id = :id"),
        @NamedQuery(name = InfoText.BY_FELDNAME, query = "SELECT i FROM InfoText i WHERE i.feldName LIKE :feldName"),
        @NamedQuery(name = InfoText.BY_FELDNAMES, query = "SELECT i FROM InfoText i WHERE i.feldName IN :feldNames")})

public class InfoText implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String ALL = "infotext.all";
    public static final String BY_ID = "infotext.by_id";
    public static final String BY_FELDNAME = "infotext.by_feldname";
    public static final String BY_FELDNAMES = "infotext.by_feldnames";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "infotext_id_seq",
            sequenceName = "infotext_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "infotext_id_seq")
    private Long id;

    private String feldName;

    @Column(length = 10000)
    private String feldBeschreibungLang;

    @Column(length = 10000)
    private String beschreibungsText;

    // ------------  constructors ----------------------------------------------
    public InfoText() {
    }

    public InfoText(String feldName, String feldBeschreibungLang, String beschreibungsText) {
        this.feldName = feldName;
        this.feldBeschreibungLang = feldBeschreibungLang;
        this.beschreibungsText = beschreibungsText;
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(31, 89);
        hb.append(id);
        hb.append(feldName);
        hb.append(beschreibungsText);
        hb.append(feldBeschreibungLang);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof InfoText)) {
            return false;
        }
        if (object == this) {
            return true;
        }
        InfoText executer = (InfoText) object;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, executer.id);
        eb.append(feldName, executer.feldName);
        eb.append(beschreibungsText, executer.beschreibungsText);
        eb.append(feldBeschreibungLang, executer.feldBeschreibungLang);
        return eb.isEquals();
    }

    @Override
    public String toString() {
        return "Feld: " + feldName + ", Beschreibung = " + beschreibungsText;
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFeldName() {
        return feldName;
    }

    public void setFeldName(String feldName) {
        this.feldName = feldName;
    }

    public String getBeschreibungsText() {
        return beschreibungsText;
    }

    public void setBeschreibungsText(String beschreibungsText) {
        this.beschreibungsText = beschreibungsText;
    }

    public String getFeldBeschreibungLang() {
        return feldBeschreibungLang;
    }

    public void setFeldBeschreibungLang(String feldBeschreibungLang) {
        this.feldBeschreibungLang = feldBeschreibungLang;
    }

} // end of class
