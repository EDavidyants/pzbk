package de.interfaceag.bmw.pzbk.entities;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author sl
 */
@Entity
@Cacheable(false)
@Table(name = "T_ROLE_GROUP")
public class GroupToRole implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String rolename;
    private String groupname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 67 * hash + (this.rolename != null ? this.rolename.hashCode() : 0);
        hash = 67 * hash + (this.groupname != null ? this.groupname.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final GroupToRole other = (GroupToRole) obj;
        if (!Objects.equals(this.id, other.id) && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.rolename == null ? other.rolename != null : !this.rolename.equals(other.rolename)) {
            return false;
        }
        return !(this.groupname == null ? other.groupname != null : !this.groupname.equals(other.groupname));
    }

    @Override
    public String toString() {
        return "GroupToRole{" + "id=" + id + ", rolename=" + rolename + ", groupname=" + groupname + '}';
    }

}
