package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.Optional;

/**
 * @author evda
 */
@Entity
@Table(name = "derivatanforderungmodul", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"zuordnunganforderungderivat_id", "modul_id"})})
@NamedQueries( {
        @NamedQuery(name = DerivatAnforderungModul.ALL, query = "SELECT df FROM DerivatAnforderungModul df"),
        @NamedQuery(name = DerivatAnforderungModul.BY_ID, query = "SELECT df FROM DerivatAnforderungModul df WHERE df.id = :id"),
        @NamedQuery(name = DerivatAnforderungModul.BY_ID_LIST, query = "SELECT df FROM DerivatAnforderungModul df WHERE df.id IN :idList"),
        @NamedQuery(name = DerivatAnforderungModul.BY_ANFORDERUNG, query = "SELECT df FROM DerivatAnforderungModul df WHERE df.zuordnungAnforderungDerivat.anforderung = :anforderung"),
        @NamedQuery(name = DerivatAnforderungModul.BY_ANFORDERUNG_STATUS, query = "SELECT df FROM DerivatAnforderungModul df WHERE df.zuordnungAnforderungDerivat.anforderung.id = :anforderungId and df.statusZV = :status"),
        @NamedQuery(name = DerivatAnforderungModul.BY_ANFORDERUNG_LIST, query = "SELECT df FROM DerivatAnforderungModul df WHERE df.zuordnungAnforderungDerivat.anforderung IN :anfoList"),
        @NamedQuery(name = DerivatAnforderungModul.FETCH_BY_ANFORDERUNG_LIST, query = "SELECT d FROM DerivatAnforderungModul d "
                + "INNER JOIN FETCH d.zuordnungAnforderungDerivat.derivat "
                + "INNER JOIN FETCH d.zuordnungAnforderungDerivat.anforderung "
                + "LEFT JOIN FETCH d.anforderer "
                + "WHERE d.zuordnungAnforderungDerivat.anforderung IN :anforderungList"),
        @NamedQuery(name = DerivatAnforderungModul.FETCH_BY_ANFORDERUNG_LIST_DERIVAT_LIST, query = "SELECT d FROM DerivatAnforderungModul d "
                + "INNER JOIN FETCH d.zuordnungAnforderungDerivat.derivat "
                + "INNER JOIN FETCH d.zuordnungAnforderungDerivat.anforderung "
                + "LEFT JOIN FETCH d.anforderer "
                + "WHERE d.zuordnungAnforderungDerivat.anforderung.id IN :anforderungList AND d.zuordnungAnforderungDerivat.derivat.id IN :derivatList"),
        @NamedQuery(name = DerivatAnforderungModul.BY_DERIVAT, query = "SELECT df FROM DerivatAnforderungModul df WHERE df.zuordnungAnforderungDerivat.derivat = :derivat"),
        @NamedQuery(name = DerivatAnforderungModul.BY_ANFORDERUNG_DERIVAT, query = "SELECT df FROM DerivatAnforderungModul df WHERE df.zuordnungAnforderungDerivat.anforderung = :anforderung AND df.zuordnungAnforderungDerivat.derivat = :derivat"),
        @NamedQuery(name = DerivatAnforderungModul.BY_ANFORDERUNG_MODUL, query = "SELECT df FROM DerivatAnforderungModul df "
                + "WHERE df.zuordnungAnforderungDerivat.anforderung.id = :anforderung AND df.modul.id = :modul"),
        @NamedQuery(name = DerivatAnforderungModul.BY_ANFORDERUNG_MODUL_DERIVAT, query = "SELECT df FROM DerivatAnforderungModul df WHERE df.zuordnungAnforderungDerivat.anforderung = :anforderung AND df.modul = :modul AND df.zuordnungAnforderungDerivat.derivat = :derivat"),
        @NamedQuery(name = DerivatAnforderungModul.GET_DERIVATIDS_FOR_DERIVAT_LIST_NOT_ZAKSTATUS, query = "SELECT DISTINCT df.zuordnungAnforderungDerivat.derivat.id FROM DerivatAnforderungModul df INNER JOIN df.vereinbarung AS zak  WHERE df.zuordnungAnforderungDerivat.derivat IN :derivatList AND zak.zakStatus != :zakStatus"),
        @NamedQuery(name = DerivatAnforderungModul.GET_DERIVATE_FOR_ANFORDERUNG, query = "SELECT DISTINCT df.zuordnungAnforderungDerivat.derivat FROM DerivatAnforderungModul df WHERE df.zuordnungAnforderungDerivat.anforderung = :anforderung ORDER BY df.zuordnungAnforderungDerivat.derivat.name"),
        @NamedQuery(name = DerivatAnforderungModul.GET_ANFORDERUNGEN_FOR_DERIVAT, query = "SELECT df.zuordnungAnforderungDerivat.anforderung FROM DerivatAnforderungModul df WHERE df.zuordnungAnforderungDerivat.derivat = :derivat"),
        @NamedQuery(name = DerivatAnforderungModul.GET_ANFOIDS_FOR_DERIVAT_LIST, query = "SELECT df.zuordnungAnforderungDerivat.anforderung.id FROM DerivatAnforderungModul df WHERE df.zuordnungAnforderungDerivat.derivat.id IN :deriList"),
        @NamedQuery(name = DerivatAnforderungModul.GET_SENSORCOC_FOR_STATUS_NOT_IN_SENSORCOCIDLIST_AND_DERIVAT,
                query = "SELECT DISTINCT dam.zuordnungAnforderungDerivat.anforderung.sensorCoc FROM DerivatAnforderungModul dam WHERE dam.zuordnungAnforderungDerivat.derivat.id "
                        + "= :derivatId AND dam.zuordnungAnforderungDerivat.anforderung.sensorCoc.sensorCocId NOT IN :sensorCocIdList "
                        + "AND dam.statusZV = :status"),
        @NamedQuery(name = DerivatAnforderungModul.GET_SENSORCOC_FOR_STATUS_AND_DERIVAT,
                query = "SELECT DISTINCT dam.zuordnungAnforderungDerivat.anforderung.sensorCoc FROM DerivatAnforderungModul dam WHERE dam.zuordnungAnforderungDerivat.derivat.id "
                        + "= :derivatId AND dam.statusZV = :status")})
public class DerivatAnforderungModul implements Serializable {

    public static final String CLASSNAME = "DerivatAnforderungModul.";

    // ------------  queries ---------------------------------------------------
    public static final String ALL = CLASSNAME + "all";
    public static final String BY_ID = CLASSNAME + "by_id";
    public static final String BY_ID_LIST = CLASSNAME + "by_id_list";
    public static final String BY_ANFORDERUNG = CLASSNAME + "by_anforderung";
    public static final String BY_ANFORDERUNG_STATUS = CLASSNAME + "by_anforderung_status";
    public static final String BY_ANFORDERUNG_LIST = CLASSNAME + "by_anforderung_list";
    public static final String FETCH_BY_ANFORDERUNG_LIST = CLASSNAME + "fetch_by_anforderung_list";
    public static final String FETCH_BY_ANFORDERUNG_LIST_DERIVAT_LIST = CLASSNAME + "fetch_by_anforderung_list_derivat_list";
    public static final String BY_DERIVAT = CLASSNAME + "by_derivat";
    public static final String BY_ANFORDERUNG_DERIVAT = CLASSNAME + "by_anforderung_derivat";
    public static final String BY_ANFORDERUNG_MODUL = CLASSNAME + "by_anforderung_modul";
    public static final String BY_ANFORDERUNG_MODUL_DERIVAT = CLASSNAME + "by_anforderung_modul_derivat";
    public static final String GET_DERIVATIDS_FOR_DERIVAT_LIST_NOT_ZAKSTATUS = CLASSNAME + "GET_DERIVATIDS_FOR_DERIVAT_LIST_NOT_ZAKSTATUS";
    public static final String GET_DERIVATE_FOR_ANFORDERUNG = CLASSNAME + "getDerivateForAnforderung";
    public static final String GET_ANFORDERUNGEN_FOR_DERIVAT = CLASSNAME + "getAnforderungenForDerivat";
    public static final String GET_ANFOIDS_FOR_DERIVAT_LIST = CLASSNAME + "getAnfoIdsForDerivatList";
    public static final String GET_SENSORCOC_FOR_STATUS_NOT_IN_SENSORCOCIDLIST_AND_DERIVAT = CLASSNAME + "getSensorCocForStatusAndNotInSensorCocIdListAndDerivat";
    public static final String GET_SENSORCOC_FOR_STATUS_AND_DERIVAT = CLASSNAME + "getSensorCocForStatusAndDerivat";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "derivatanforderungmodul_id_seq",
            sequenceName = "derivatanforderungmodul_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "derivatanforderungmodul_id_seq")
    private Long id;

    @ManyToOne
    @JoinColumn(nullable = false)
    private ZuordnungAnforderungDerivat zuordnungAnforderungDerivat;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Modul modul;

    @ManyToOne
    private Mitarbeiter anforderer;

    private Integer statusZV;

    private Integer statusVKBG;

    @Enumerated(EnumType.ORDINAL)
    private ZakStatus zakStatus = ZakStatus.EMPTY;

    @Enumerated(EnumType.ORDINAL)
    private UmsetzungsBestaetigungStatus umsetzungsBestaetigungStatus = UmsetzungsBestaetigungStatus.OFFEN;

    private Integer kovAPhase;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY, mappedBy = "derivatAnforderungModul")
    private ZakUebertragung vereinbarung;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private DerivatAnforderungModulHistory latestHistoryEntry;

    // ------------  constructors ----------------------------------------------
    public DerivatAnforderungModul() {
    }

    public DerivatAnforderungModul(ZuordnungAnforderungDerivat zuordnungAnforderungDerivat, Modul modul) {
        this.zuordnungAnforderungDerivat = zuordnungAnforderungDerivat;
        this.modul = modul;
    }

    public DerivatAnforderungModul(ZuordnungAnforderungDerivat zuordnungAnforderungDerivat, Modul modul, Mitarbeiter anforderer) {
        this.zuordnungAnforderungDerivat = zuordnungAnforderungDerivat;
        this.modul = modul;
        this.anforderer = anforderer;
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return zuordnungAnforderungDerivat.toString() + " Modul: " + this.modul.getName();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(43, 151);
        hb.append(id);
        hb.append(zuordnungAnforderungDerivat);
        hb.append(modul);
        hb.append(anforderer);
        hb.append(getKovAPhase());
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof DerivatAnforderungModul)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        DerivatAnforderungModul df = (DerivatAnforderungModul) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, df.id);
        eb.append(zuordnungAnforderungDerivat, df.zuordnungAnforderungDerivat);
        eb.append(modul, df.modul);
        eb.append(anforderer, df.anforderer);
        eb.append(getKovAPhase(), df.getKovAPhase());
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Anforderung getAnforderung() {
        return zuordnungAnforderungDerivat.getAnforderung();
    }

    public Derivat getDerivat() {
        return zuordnungAnforderungDerivat.getDerivat();
    }

    public ZuordnungAnforderungDerivat getZuordnungAnforderungDerivat() {
        return zuordnungAnforderungDerivat;
    }

    public void setZuordnungAnforderungDerivat(ZuordnungAnforderungDerivat zuordnungAnforderungDerivat) {
        this.zuordnungAnforderungDerivat = zuordnungAnforderungDerivat;
    }

    public Mitarbeiter getAnforderer() {
        return anforderer;
    }

    public void setAnforderer(Mitarbeiter anforderer) {
        this.anforderer = anforderer;
    }

    public ZakUebertragung getVereinbarung() {
        return vereinbarung;
    }

    public void setVereinbarung(ZakUebertragung vereinbarung) {
        this.vereinbarung = vereinbarung;
    }

    public DerivatAnforderungModulStatus getStatusZV() {
        return DerivatAnforderungModulStatus.getStatusById(statusZV);
    }

    public void setStatusZV(DerivatAnforderungModulStatus statusZV) {
        if (statusZV != null) {
            this.statusZV = statusZV.getStatusId();
        } else {
            this.statusZV = null;
        }
    }

    public DerivatAnforderungModulStatus getStatusVKBG() {
        return DerivatAnforderungModulStatus.getStatusById(statusVKBG);
    }

    public void setStatusVKBG(DerivatAnforderungModulStatus statusVKBG) {
        if (statusVKBG != null) {
            this.statusVKBG = statusVKBG.getStatusId();
        } else {
            this.statusVKBG = null;
        }
    }

    public Optional<KovAPhase> getKovAPhase() {
        if (kovAPhase == null) {
            return Optional.empty();
        } else {
            return KovAPhase.getById(kovAPhase);
        }
    }

    public void setKovAPhase(KovAPhase kovAPhase) {
        this.kovAPhase = kovAPhase.getId();
    }

    public UmsetzungsBestaetigungStatus getUmsetzungsBestaetigungStatus() {
        return umsetzungsBestaetigungStatus;
    }

    public void setUmsetzungsBestaetigungStatus(UmsetzungsBestaetigungStatus umsetzungsBestaetigungStatus) {
        this.umsetzungsBestaetigungStatus = umsetzungsBestaetigungStatus;
    }

    public ZakStatus getZakStatus() {
        return zakStatus;
    }

    public void setZakStatus(ZakStatus zakStatus) {
        this.zakStatus = zakStatus;
    }

    public Modul getModul() {
        return modul;
    }

    public void setModul(Modul modul) {
        this.modul = modul;
    }

    public Optional<DerivatAnforderungModulHistory> getLatestHistoryEntry() {
        return Optional.ofNullable(latestHistoryEntry);
    }

    public void setLatestHistoryEntry(DerivatAnforderungModulHistory latestHistoryEntry) {
        this.latestHistoryEntry = latestHistoryEntry;
    }

}
