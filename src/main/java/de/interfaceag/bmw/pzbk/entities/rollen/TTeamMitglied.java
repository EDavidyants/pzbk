package de.interfaceag.bmw.pzbk.entities.rollen;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Rechttype;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 *
 * @author fn
 */
@Entity
public class TTeamMitglied implements Serializable {

    @Id
    @SequenceGenerator(name = "tteam_mitglied_id_seq",
            sequenceName = "tteam_mitglied_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tteam_mitglied_id_seq")
    private Long id;

    @ManyToOne
    private Tteam tteam;

    @ManyToOne
    private Mitarbeiter mitarbeiter;

    private Integer rechttype;

    public TTeamMitglied() {
    }

    public TTeamMitglied(Tteam tteam, Mitarbeiter mitarbeiter, Rechttype rechttype) {
        this.tteam = tteam;
        this.mitarbeiter = mitarbeiter;
        this.rechttype = rechttype.getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Tteam getTteam() {
        return tteam;
    }

    public void setTteam(Tteam tteam) {
        this.tteam = tteam;
    }

    public Mitarbeiter getMitarbeiter() {
        return mitarbeiter;
    }

    public void setMitarbeiter(Mitarbeiter mitarbeiter) {
        this.mitarbeiter = mitarbeiter;
    }

    public Rechttype getRechttype() {
        return Rechttype.getById(rechttype);
    }

    public void setRechttype(Rechttype rechttype) {
        this.rechttype = rechttype.getId();
    }

}
