package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.enums.Status;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.TemporalType.TIMESTAMP;

/**
 * @author evda
 */
@Entity
@Table(name = "search_filter")
@NamedQueries( {
        @NamedQuery(name = SearchFilter.BY_ID, query = "SELECT sf FROM SearchFilter sf WHERE sf.id = :id")})
public class SearchFilter implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String BY_ID = "searchFilter.byId";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "searchfilter_id_seq",
            sequenceName = "searchfilter_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "searchfilter_id_seq")
    private Long id;

    private SearchFilterType filterType;

    private Boolean booleanValue;

    @ElementCollection
    private List<Long> longListValue;

    @ElementCollection
    @Enumerated(EnumType.ORDINAL)
    private List<Status> statusListValue;

    @ElementCollection
    private List<String> stringListValue;

    @Temporal(TIMESTAMP)
    private Date startDateValue;

    @Temporal(TIMESTAMP)
    private Date endDateValue;

    // ------------  constructors ----------------------------------------------
    public SearchFilter() {
    }

    public SearchFilter(SearchFilter sf) {
        this.filterType = sf.filterType;
        this.booleanValue = sf.booleanValue;
        this.longListValue = sf.longListValue;
        this.stringListValue = sf.stringListValue;
        if (sf.statusListValue != null) {
            this.statusListValue = new ArrayList<>(sf.statusListValue);
        }
        this.startDateValue = sf.startDateValue;
        this.endDateValue = sf.endDateValue;
    }

    public SearchFilter(SearchFilterType filterType) {
        this.filterType = filterType;
    }

    public SearchFilter(SearchFilterType filterType, Boolean booleanValue) {
        this.filterType = filterType;
        this.booleanValue = booleanValue;
    }

    public SearchFilter(SearchFilterType filterType, List listValue) {
        this.filterType = filterType;
        if (!listValue.isEmpty() && listValue.get(0) instanceof Long) {
            this.longListValue = listValue;
        } else if (!listValue.isEmpty() && listValue.get(0) instanceof String) {
            stringListValue = listValue;
        }
    }

    /**
     * Set value intValue 0 for startDate, 1 for endDate.
     *
     * @param filterType of the filter;
     * @param dateValue  instance of Date;
     * @param intValue          selector for start and end date;
     */
    public SearchFilter(SearchFilterType filterType, Date dateValue, Integer intValue) {
        this.filterType = filterType;
        if (intValue == 0) {
            this.startDateValue = new Date(dateValue.getTime());
        } else if (intValue == 1) {
            this.endDateValue = new Date(dateValue.getTime());
        }
    }

    public SearchFilter(SearchFilterType filterType, Date startDateValue, Date endDateValue) {
        this.filterType = filterType;
        this.startDateValue = new Date(startDateValue.getTime());
        this.endDateValue = new Date(endDateValue.getTime());
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return " Filter: " + filterType;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(167, 337);
        hb.append(id);
        hb.append(filterType);
        hb.append(booleanValue);
        hb.append(longListValue);
        hb.append(statusListValue);
        hb.append(startDateValue);
        hb.append(endDateValue);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SearchFilter)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        SearchFilter sf = (SearchFilter) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, sf.id);
        eb.append(filterType, sf.filterType);
        eb.append(booleanValue, sf.booleanValue);
        eb.append(longListValue, sf.longListValue);
        eb.append(statusListValue, sf.statusListValue);
        eb.append(startDateValue, sf.startDateValue);
        eb.append(endDateValue, sf.endDateValue);
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SearchFilterType getFilterType() {
        return filterType;
    }

    public void setFilterType(SearchFilterType filterType) {
        this.filterType = filterType;
    }

    public Boolean getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(Boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public List<Long> getLongListValue() {
        return longListValue;
    }

    public void setLongListValue(List<Long> longListValue) {
        this.longListValue = longListValue;
    }

    public List<Status> getStatusListValue() {
        return statusListValue;
    }

    public void setStatusListValue(List<Status> statusListValue) {
        this.statusListValue = statusListValue;
    }

    public Date getStartDateValue() {
        return new Date(startDateValue.getTime());
    }

    public void setStartDateValue(Date startDateValue) {
        if (startDateValue != null) {
            this.startDateValue = new Date(startDateValue.getTime());
        } else {
            this.startDateValue = null;
        }
    }

    public Date getEndDateValue() {
        return new Date(endDateValue.getTime());
    }

    public void setEndDateValue(Date endDateValue) {
        if (endDateValue != null) {
            this.endDateValue = new Date(endDateValue.getTime());
        } else {
            this.endDateValue = null;
        }
    }

    /**
     * @return the stringListValue
     */
    public List<String> getStringListValue() {
        return stringListValue;
    }

    /**
     * @param stringListValue the stringListValue to set
     */
    public void setStringListValue(List<String> stringListValue) {
        this.stringListValue = stringListValue;
    }

} // end of class
