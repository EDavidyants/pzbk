package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author Christian Schauer
 */
@Entity
@Table(name = "meldung")
@Cacheable(false)
@NamedQueries({
    @NamedQuery(name = Meldung.ALL, query = "SELECT DISTINCT m FROM Meldung m"),
    @NamedQuery(name = Meldung.BY_ID, query = "SELECT m FROM Meldung m WHERE m.id = :id ORDER BY m.id"),
    @NamedQuery(name = Meldung.BY_ID_LIST, query = "SELECT m FROM Meldung m WHERE m.id IN :idList"),
    @NamedQuery(name = Meldung.ID_BY_STATUS, query = "SELECT DISTINCT m.id FROM Meldung m WHERE m.status = :status"),
    @NamedQuery(name = Meldung.GET_ALL_ANFORDERUNGEN, query = "SELECT DISTINCT anfo FROM Meldung m INNER JOIN m.anforderung AS anfo WHERE m.id = :id")})
public class Meldung extends AbstractAnforderung {

    public static final String CLASSNAME = "Meldung.";

    // ------------  queries ---------------------------------------------------
    public static final String ALL = CLASSNAME + "all";
    public static final String BY_ID = CLASSNAME + "by_id";
    public static final String BY_ID_LIST = CLASSNAME + "by_id_list";
    public static final String ID_BY_STATUS = CLASSNAME + "id_by_status";
    public static final String GET_ALL_ANFORDERUNGEN = CLASSNAME + "get_all_anforderungen";

    // ------------  fields ----------------------------------------------------
    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "meldung_id_seq",
            sequenceName = "meldung_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "meldung_id_seq")
    private Long id;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "meldungen", targetEntity = Anforderung.class)
    private Set<Anforderung> anforderung;

    private boolean assignedToAnforderung;

    // ------------  constructors ----------------------------------------------
    public Meldung() {
        Date now = new Date();
        super.setErstellungsdatum(now);
        super.setAenderungsdatum(now);
        super.setZeitpunktStatusaenderung(now);
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getKennzeichen() {
        return "M";
    }

    public void copy(Meldung meldungCopy) throws IllegalAccessException, InvocationTargetException {
        BeanUtils.copyProperties(meldungCopy, this);
    }

    @Override
    public String toString() {
        return getFachId();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(53, 97);
        hb.append(this.getId());

        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AbstractAnfoMgmtObject)) {
            return false;
        }
        Meldung m = (Meldung) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(this.getId(), m.getId());
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    public Set<Anforderung> getAnforderung() {
        return anforderung;
    }

    public void setAnforderung(Set<Anforderung> anforderung) {
        this.anforderung = anforderung;
    }

    public boolean isAssignedToAnforderung() {
        return assignedToAnforderung;
    }

    public void setAssignedToAnforderung(boolean assignedToAnforderung) {
        this.assignedToAnforderung = assignedToAnforderung;
    }
} //end of class
