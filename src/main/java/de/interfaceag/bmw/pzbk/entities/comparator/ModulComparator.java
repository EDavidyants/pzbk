package de.interfaceag.bmw.pzbk.entities.comparator;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author sl
 */
public class ModulComparator implements Comparator<ModulCompareFields>, Serializable {

    @Override
    public int compare(ModulCompareFields t1, ModulCompareFields t2) {
        if (t1 != null && t2 != null) {
            String s1 = t1.getName();
            String s2 = t2.getName();
            int res = String.CASE_INSENSITIVE_ORDER.compare(s1, s2);
            if (res == 0) {
                res = s1.compareTo(s2);
            }
            return res;
        }
        return -2;
    }
}
