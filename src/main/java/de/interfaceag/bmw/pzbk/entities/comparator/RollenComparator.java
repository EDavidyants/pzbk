package de.interfaceag.bmw.pzbk.entities.comparator;

import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author sl
 */
public class RollenComparator implements Comparator<Rolle>, Serializable {

    @Override
    public int compare(Rolle t1, Rolle t2) {
        if (t1 != null && t2 != null) {
            String s1 = t1.getBezeichnung();
            String s2 = t2.getBezeichnung();
            int res = String.CASE_INSENSITIVE_ORDER.compare(s1, s2);
            if (res == 0) {
                res = s1.compareTo(s2);
            }
            return res;
        }
        return -2;
    }
}
