package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Schauer
 */
@Entity
public class Auswirkung implements Serializable {

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "auswirkung_id_seq",
            sequenceName = "auswirkung_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auswirkung_id_seq")
    private long id;

    @Column(length = 10000)
    private String auswirkung;

    @Column(length = 10000)
    private String wert;

    // ------------  methods ---------------------------------------------------
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Auswirkung)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        Auswirkung mywert = (Auswirkung) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, mywert.id);
        eb.append(auswirkung, mywert.auswirkung);
        eb.append(wert, mywert.wert);

        return eb.isEquals();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(11, 41);
        hb.append(id);
        hb.append(auswirkung);
        hb.append(wert);
        return hb.toHashCode();
    }

    public Auswirkung getCopy() {
        Auswirkung a = new Auswirkung();
        a.setAuswirkung(auswirkung);
        a.setWert(wert);
        return a;
    }

    /**
     * For testing
     *
     * @param supplement ...
     * @return ...
     */
    public static Auswirkung createTestAuswirkung(String supplement) {
        Auswirkung a = new Auswirkung();
        a.setAuswirkung("Auswirkung_" + supplement);
        a.setWert("Wert_" + supplement);
        return a;
    }

    @Override
    public String toString() {
        return this.auswirkung + ": " + this.wert;
    }

    public static List<Auswirkung> createExampleAuswirkugnen(int count, String additive) {
        List<Auswirkung> auswirkungen = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Auswirkung aw = new Auswirkung();
            aw.setAuswirkung("2k Prüfung (Punkte) " + additive);
            aw.setWert("Wert der Auswirkung " + additive);
            auswirkungen.add(aw);
        }
        return auswirkungen;
    }

    // ------------  getter / setter -------------------------------------------

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the auswirkung
     */
    public String getAuswirkung() {
        return auswirkung;
    }

    /**
     * @param auswirkung the auswirkung to set
     */
    public void setAuswirkung(String auswirkung) {
        this.auswirkung = auswirkung;
    }

    /**
     * @return the wert
     */
    public String getWert() {
        return wert;
    }

    /**
     * @param wert the wert to set
     */
    public void setWert(String wert) {
        this.wert = wert;
    }
}
