package de.interfaceag.bmw.pzbk.entities.comparator;

import de.interfaceag.bmw.pzbk.entities.Derivat;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author sl
 */
public class DerivatComparator implements Comparator<Derivat>, Serializable {

    @Override
    public int compare(Derivat t1, Derivat t2) {
        if (t1 != null && t2 != null) {
            String s1 = t1.getName();
            String s2 = t2.getName();
            int res = String.CASE_INSENSITIVE_ORDER.compare(s1, s2);
            if (res == 0) {
                res = s1.compareTo(s2);
            }
            return res;
        }
        return -2;
    }
}
