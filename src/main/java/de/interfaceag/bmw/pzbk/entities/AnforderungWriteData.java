package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.Status;

import java.util.List;

/**
 *
 * @author fn
 */
public interface AnforderungWriteData extends AnforderungReadData {

    void setStatus(Status status);

    void setAnhaenge(List<Anhang> anhaenge);

}
