package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author evda
 */
@Entity
@Table(name = "berechtigung")
@NamedQueries( {
        @NamedQuery(name = Berechtigung.BY_ID, query = "SELECT b FROM Berechtigung b WHERE b.id = :id"),
        @NamedQuery(name = Berechtigung.BY_MITARBEITER, query = "SELECT b FROM Berechtigung b WHERE b.mitarbeiter = :mitarbeiter ORDER BY b.fuerId"),
        @NamedQuery(name = Berechtigung.BY_MITARBEITER_AND_ROLLE, query = "SELECT b FROM Berechtigung b WHERE b.mitarbeiter = :mitarbeiter AND b.rolle = :rolle ORDER BY b.fuerId"),
        @NamedQuery(name = Berechtigung.BY_ROLLE, query = "SELECT b FROM Berechtigung b WHERE b.rolle = :rolle ORDER BY b.fuerId"),
        @NamedQuery(name = Berechtigung.BY_MITARBEITER_ROLLE_ZIEL, query = "SELECT b FROM Berechtigung b WHERE b.mitarbeiter = :mitarbeiter AND b.rolle = :rolle AND b.fuer = :fuer"),
        @NamedQuery(name = Berechtigung.BY_MITARBEITER_ZIEL, query = "SELECT b.fuerId FROM Berechtigung b WHERE b.mitarbeiter = :mitarbeiter AND b.fuer = :fuer AND b.rechttype != :rechttype ORDER BY b.fuerId"),
        @NamedQuery(name = Berechtigung.BY_MITARBEITER_ROLLE_ZIEL_WITH_RECHTTYPE, query = "SELECT b.fuerId FROM Berechtigung b WHERE b.mitarbeiter = :mitarbeiter AND b.rolle = :rolle AND b.fuer = :fuer AND b.rechttype = :rechttype ORDER BY b.fuerId"),
        @NamedQuery(name = Berechtigung.GET_BERECHTIGTEN_MITARBEITER, query = "SELECT b.mitarbeiter FROM Berechtigung b WHERE b.rolle = :rolle AND b.fuerId LIKE :fuerId AND b.fuer = :fuer AND b.rechttype = :rechttype ORDER BY b.id"),
        @NamedQuery(name = Berechtigung.GET_MITARBEITER_BY_FUERLIST_ROLLE_ZIEL_RECHTTYPE, query = "SELECT DISTINCT b.mitarbeiter FROM Berechtigung b WHERE b.rolle = :rolle AND b.fuerId IN :fuerIdList AND b.fuer = :fuer AND b.rechttype = :rechttype"),
        @NamedQuery(name = Berechtigung.GET_MITARBEITER_BY_FUERID_FUER_ROLLE_RECHTTYPE, query = "SELECT DISTINCT b.mitarbeiter FROM Berechtigung b "
                + "WHERE b.rolle = :rolle AND b.fuerId = :fuerId AND b.fuer = :fuer AND b.rechttype = :rechttype"),
        @NamedQuery(name = Berechtigung.GET_BY_FUERID_FUER_ROLLE_RECHTTYPE, query = "SELECT DISTINCT b FROM Berechtigung b "
                + "WHERE b.rolle = :rolle AND b.fuerId = :fuerId AND b.fuer = :fuer AND b.rechttype = :rechttype"),
        @NamedQuery(name = Berechtigung.GET_MITARBEITER_BY_FUERLIST_ZIEL, query = "SELECT DISTINCT b.mitarbeiter FROM Berechtigung b WHERE b.fuerId IN :fuerIdList AND b.fuer = :fuer")})
public class Berechtigung implements Serializable {

    public static final String CLASSNAME = "Berechtigung.";

    // ------------  queries ---------------------------------------------------
    public static final String BY_ID = "getBerechtigungById";
    public static final String BY_MITARBEITER = "getBerechtigungByMitarbeiter";
    public static final String BY_ROLLE = "getBerechtigungByRolle";
    public static final String BY_MITARBEITER_AND_ROLLE = "getBerechtigungByMitarbeiterAndRolle";
    public static final String BY_MITARBEITER_ROLLE_ZIEL = "getBerechtigungByMitarbeiterAndRolleAndZiel";
    public static final String BY_MITARBEITER_ROLLE_ZIEL_WITH_RECHTTYPE = "getBerechtigungByMitarbeiterAndRolleAndZielWithRechttype";
    public static final String BY_MITARBEITER_ZIEL = "getBerechtigungByMitarbeiterAndZiel";
    public static final String GET_BERECHTIGTEN_MITARBEITER = "getBerechtigtenMitarbeiterMitRechttype";
    public static final String GET_MITARBEITER_BY_FUERLIST_ROLLE_ZIEL_RECHTTYPE = "getMitarbeiterByFuerListRolleZielRechttype";
    public static final String GET_MITARBEITER_BY_FUERLIST_ZIEL = "getMitarbeiterByFuerListZiel";
    public static final String GET_MITARBEITER_BY_FUERID_FUER_ROLLE_RECHTTYPE = CLASSNAME + "getMitarbeiterByFuerIdFuerRolleRechttypeMitarbeiter";
    public static final String GET_BY_FUERID_FUER_ROLLE_RECHTTYPE = CLASSNAME + "getByFuerIdFuerRolleRechttype";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "berechtigung_id_seq",
            sequenceName = "berechtigung_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "berechtigung_id_seq")
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Mitarbeiter mitarbeiter;

    @Enumerated(EnumType.ORDINAL)
    private Rolle rolle;

    @Enumerated(EnumType.ORDINAL)
    private Rechttype rechttype;

    private String fuerId;

    @Enumerated(EnumType.ORDINAL)
    private BerechtigungZiel fuer;

    // ------------  constructors ----------------------------------------------
    public Berechtigung() {

    }

    public Berechtigung(Mitarbeiter mitarbeiter, Rolle rolle, Rechttype rechttype, String fuerId, BerechtigungZiel fuer) {
        this.mitarbeiter = mitarbeiter;
        this.rolle = rolle;
        this.rechttype = rechttype;
        this.fuerId = fuerId;
        this.fuer = fuer;
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return "Mitarbeiter " + this.mitarbeiter.getFullName()
                + " hat " + this.rolle.toString()
                + " mit " + this.rechttype.toString()
                + " für " + this.fuer.toString()
                + " ID: " + this.fuerId;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(17, 211);
        hb.append(id);
        hb.append(mitarbeiter);
        hb.append(rolle);
        hb.append(fuerId);
        hb.append(fuer);
        hb.append(rechttype);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Berechtigung)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        Berechtigung b = (Berechtigung) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, b.id);
        eb.append(mitarbeiter, b.mitarbeiter);
        eb.append(rolle, b.rolle);
        eb.append(fuerId, b.fuerId);
        eb.append(fuer, b.fuer);
        eb.append(rechttype, b.rechttype);
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Mitarbeiter getMitarbeiter() {
        return mitarbeiter;
    }

    public void setMitarbeiter(Mitarbeiter mitarbeiter) {
        this.mitarbeiter = mitarbeiter;
    }

    public Rolle getRolle() {
        return rolle;
    }

    public void setRolle(Rolle rolle) {
        this.rolle = rolle;
    }

    public Rechttype getRechttype() {
        return rechttype;
    }

    public void setRechttype(Rechttype rechttype) {
        this.rechttype = rechttype;
    }

    public String getFuerId() {
        return fuerId;
    }

    public void setFuerId(String fuerId) {
        this.fuerId = fuerId;
    }

    public BerechtigungZiel getFuer() {
        return fuer;
    }

    public void setFuer(BerechtigungZiel fuer) {
        this.fuer = fuer;
    }

} // end of class
