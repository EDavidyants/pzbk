package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @author evda
 */
@Entity
@Table(name = "t_team")
@NamedQueries( {
        @NamedQuery(name = Tteam.BY_ID, query = "SELECT t FROM Tteam t WHERE t.id = :id"),
        @NamedQuery(name = Tteam.BY_ID_LIST, query = "SELECT t FROM Tteam t WHERE t.id IN :idList"),
        @NamedQuery(name = Tteam.BY_TEAMNAME, query = "SELECT t FROM Tteam t WHERE t.teamName LIKE :teamname"),
        @NamedQuery(name = Tteam.BY_NAME_LIST, query = "SELECT t FROM Tteam t WHERE t.teamName IN :names"),
        @NamedQuery(name = Tteam.ALL_TTEAMS, query = "SELECT t FROM Tteam t"),
        @NamedQuery(name = Tteam.ALL_TTEAMS_SORT_BY_NAME, query = "SELECT t FROM Tteam t ORDER BY t.teamName")})
public class Tteam implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String BY_ID = "tteam.by_id";
    public static final String BY_ID_LIST = "tteam.by_id_list";
    public static final String BY_TEAMNAME = "tteam.by_teamname";
    public static final String BY_NAME_LIST = "tteam.by_name_list";
    public static final String ALL_TTEAMS = "tteam.all_tteams";
    public static final String ALL_TTEAMS_SORT_BY_NAME = "tteam.all_tteams_sort_by_name";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "tteam_id_seq",
            sequenceName = "tteam_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tteam_id_seq")
    private Long id;

    @Column(name = "team_name")
    private String teamName;

    @Transient
    private Mitarbeiter teamleiter;

    // ------------  constructors ----------------------------------------------
    public Tteam() {

    }

    public Tteam(String teamName) {
        this.teamName = teamName;
    }

    public Tteam(String teamName, Mitarbeiter teamleiter) {
        this.teamName = teamName;
        this.teamleiter = teamleiter;
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return this.teamName;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(113, 347);
        hb.append(id);
        hb.append(teamName);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Tteam)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        Tteam tm = (Tteam) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, tm.id);
        eb.append(teamName, tm.teamName);
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Mitarbeiter getTeamleiter() {
        return teamleiter;
    }

    public void setTeamleiter(Mitarbeiter teamleiter) {
        this.teamleiter = teamleiter;
    }
} // end of class
