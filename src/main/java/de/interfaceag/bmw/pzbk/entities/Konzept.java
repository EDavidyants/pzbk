package de.interfaceag.bmw.pzbk.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author lomu
 */
@Entity
@Table(name = "konzept")
public class Konzept implements Serializable, Comparable {

    @Id
    @SequenceGenerator(name = "konzept_id_seq",
            sequenceName = "konzept_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "konzept_id_seq")
    private long id;

    @Column(name = "bezeichnung")
    private String bezeichnung;

    @Column(name = "beschreibung")
    private String beschreibung;

    @JoinColumn(name = "anhang_id")
    @OneToOne(cascade = CascadeType.PERSIST)
    private Anhang anhang;

    @JoinColumn(name = "bild_id")
    @OneToOne(cascade = CascadeType.PERSIST)
    private Bild bild;

    public Konzept() {
    }

    public Konzept(String bezeichnung, String beschreibung, Anhang anhang) {
        this.bezeichnung = bezeichnung;
        this.beschreibung = beschreibung;
        this.anhang = anhang;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 61 * hash + Objects.hashCode(this.bezeichnung);
        hash = 61 * hash + Objects.hashCode(this.beschreibung);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Konzept other = (Konzept) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.bezeichnung, other.bezeichnung)) {
            return false;
        }
        if (!Objects.equals(this.beschreibung, other.beschreibung)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Konzept{" + "id=" + id + ", bezeichnung=" + bezeichnung + '}';
    }

    @Override
    public int compareTo(Object other) {
        Konzept otherKonzept = (Konzept) other;
        return this.bezeichnung.compareToIgnoreCase(otherKonzept.bezeichnung);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public Anhang getAnhang() {
        return anhang;
    }

    public void setAnhang(Anhang anhang) {
        this.anhang = anhang;
    }

    public void removeAnhang() {
        this.anhang = null;
    }

    public Bild getBild() {
        return bild;
    }

    public void setBild(Bild bild) {
        this.bild = bild;
    }

    public Konzept getCopy() {
        Konzept konzept = new Konzept();
        konzept.setBezeichnung(this.getBezeichnung());
        konzept.setBeschreibung(this.getBeschreibung());

        if (this.getAnhang() != null) {
            konzept.setAnhang(this.getAnhang().getCopy());
        }

        if (this.getBild() != null) {
            konzept.setBild(this.getBild().getCopy());
        }

        return konzept;
    }

    public Konzept createCopyForHistory() {
        Konzept konzept = getCopy();
        konzept.setId(this.id);
        return konzept;
    }

}
