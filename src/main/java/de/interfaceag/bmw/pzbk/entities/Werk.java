package de.interfaceag.bmw.pzbk.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Florian Neuner
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Werk.QUERY_ALL, query = "SELECT f FROM Werk f ORDER BY f.name"),
    @NamedQuery(name = Werk.QUERY_BY_ID, query = "SELECT f FROM Werk f WHERE f.id = :id"),
    @NamedQuery(name = Werk.QUERY_BY_NAME, query = "SELECT f FROM Werk f WHERE f.name = :name ORDER BY f.name")})
public class Werk implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String QUERY_ALL = "Werk";
    public static final String QUERY_BY_ID = "Werk.byId";
    public static final String QUERY_BY_NAME = "Werk.byName";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "werk_id_seq",
            sequenceName = "werk_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "werk_id_seq")
    private Long id;

    private String name;

    // ------------  constructors ----------------------------------------------
    public Werk() {
    }

    public Werk(String name) {
        this.name = name.trim();
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Werk other = (Werk) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim();
    }

}
