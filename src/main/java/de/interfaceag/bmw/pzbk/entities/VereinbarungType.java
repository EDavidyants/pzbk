package de.interfaceag.bmw.pzbk.entities;

import java.util.stream.Stream;

/**
 * @author evda
 */
public enum VereinbarungType {

    STANDARD(0, "STD"),
    ZAK(1, "in ZAK"),
    PZBK(2, "PZBK");

    private final int id;
    private final String label;

    VereinbarungType(int id, String label) {
        this.id = id;
        this.label = label;
    }

    public static VereinbarungType getVereinbarungTypeForZAK(boolean isZak) {
        if (isZak) {
            return VereinbarungType.ZAK;
        } else {
            return VereinbarungType.STANDARD;
        }
    }

    public static VereinbarungType getById(int id) {
        return Stream.of(VereinbarungType.values())
                .filter(vereinbarungType -> vereinbarungType.getId() == id)
                .findAny().orElse(null);
    }

    public boolean isZak() {
        return this.equals(VereinbarungType.ZAK);
    }

    @Override
    public String toString() {
        return getLabel();
    }

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

}
