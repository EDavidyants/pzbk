package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.DomainObject;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

@Entity
@NamedQuery(name = AnforderungModulSeTeamFahrzeugmerkmalAuspraegung.AUSPRAEGUNGEN_BY_ANFORDERUNG_AND_MODULSETEAM,
        query = "SELECT DISTINCT f.fahrzeugmerkmalAuspraegung FROM AnforderungModulSeTeamFahrzeugmerkmalAuspraegung f " +
                "WHERE f.anforderung.id = :anforderungId AND f.modulSeTeam.id = :modulSeTeamId " +
                "ORDER BY f.fahrzeugmerkmalAuspraegung.auspraegung ASC")
@NamedQuery(name = AnforderungModulSeTeamFahrzeugmerkmalAuspraegung.BY_ANFORDERUNG,
        query = "SELECT f FROM AnforderungModulSeTeamFahrzeugmerkmalAuspraegung f " +
                "WHERE f.anforderung.id = :anforderungId ")
@NamedQuery(name = AnforderungModulSeTeamFahrzeugmerkmalAuspraegung.AUSPRAEGUNGEN_BY_ANFORDERUNG_MODULSETEAM_AND_FAHRZEUGMERKMAL_AUSPRAEGUNG,
        query = "SELECT DISTINCT f FROM AnforderungModulSeTeamFahrzeugmerkmalAuspraegung f " +
                "WHERE f.anforderung.id = :anforderungId AND f.modulSeTeam.id = :modulSeTeamId AND f.fahrzeugmerkmalAuspraegung.id = :fahrzeugmerkmalAuspraegungId " +
                "ORDER BY f.fahrzeugmerkmalAuspraegung.auspraegung ASC")
public class AnforderungModulSeTeamFahrzeugmerkmalAuspraegung implements DomainObject {

    private static final String CLASSNAME = "AnforderungModulSeTeamFahrzeugmerkmalAuspraegung.";
    public static final String AUSPRAEGUNGEN_BY_ANFORDERUNG_AND_MODULSETEAM = CLASSNAME + "AUSPRAEGUNGEN_BY_ANFORDERUNG_AND_MODULSETEAM";
    public static final String BY_ANFORDERUNG = CLASSNAME + "BY_ANFORDERUNG";
    public static final String AUSPRAEGUNGEN_BY_ANFORDERUNG_MODULSETEAM_AND_FAHRZEUGMERKMAL_AUSPRAEGUNG = CLASSNAME + "AUSPRAEGUNGEN_BY_ANFORDERUNG_MODULSETEAM_AND_FAHRZEUGMERKMAL_AUSPRAEGUNG";

    @Id
    @SequenceGenerator(name = "anforderungmodulseteamfahrzeugmerkmalauspraegung_id_seq",
            sequenceName = "anforderungmodulseteamfahrzeugmerkmalauspraegung_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "anforderungmodulseteamfahrzeugmerkmalauspraegung_id_seq")
    private Long id;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Anforderung anforderung;

    @ManyToOne
    @JoinColumn(nullable = false)
    private ModulSeTeam modulSeTeam;

    @ManyToOne
    @JoinColumn(nullable = false)
    private FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung;


    public AnforderungModulSeTeamFahrzeugmerkmalAuspraegung() {
    }

    public AnforderungModulSeTeamFahrzeugmerkmalAuspraegung(Anforderung anforderung, ModulSeTeam modulSeTeam, FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung) {
        this.anforderung = anforderung;
        this.modulSeTeam = modulSeTeam;
        this.fahrzeugmerkmalAuspraegung = fahrzeugmerkmalAuspraegung;
    }

    @Override
    public String toString() {
        return "AnforderungModulSeTeamFahrzeugmerkmalAuspraegung{" +
                "id=" + id +
                ", anforderung=" + anforderung +
                ", modulSeTeam=" + modulSeTeam +
                ", fahrzeugmerkmalAuspraegung=" + fahrzeugmerkmalAuspraegung +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        AnforderungModulSeTeamFahrzeugmerkmalAuspraegung that = (AnforderungModulSeTeamFahrzeugmerkmalAuspraegung) object;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(anforderung, that.anforderung)
                .append(modulSeTeam, that.modulSeTeam)
                .append(fahrzeugmerkmalAuspraegung, that.fahrzeugmerkmalAuspraegung)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(anforderung)
                .append(modulSeTeam)
                .append(fahrzeugmerkmalAuspraegung)
                .toHashCode();
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AnforderungModulSeTeamFahrzeugmerkmalAuspraegungId getAnforderungModulSeTeamFahrzeugmerkmalAuspraegungId() {
        return new AnforderungModulSeTeamFahrzeugmerkmalAuspraegungId(getId());
    }

    public Anforderung getAnforderung() {
        return anforderung;
    }

    public void setAnforderung(Anforderung anforderung) {
        this.anforderung = anforderung;
    }

    public ModulSeTeam getModulSeTeam() {
        return modulSeTeam;
    }

    public void setModulSeTeam(ModulSeTeam modulSeTeam) {
        this.modulSeTeam = modulSeTeam;
    }

    public FahrzeugmerkmalAuspraegung getFahrzeugmerkmalAuspraegung() {
        return fahrzeugmerkmalAuspraegung;
    }

    public void setFahrzeugmerkmalAuspraegung(FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung) {
        this.fahrzeugmerkmalAuspraegung = fahrzeugmerkmalAuspraegung;
    }

    public String getAttributeStringForHistory() {
        return this.getModulSeTeam().getName() + " - Fahrzeugmerkmal:" + this.getFahrzeugmerkmalAuspraegung().getFahrzeugmerkmal().getMerkmal();
    }
}
