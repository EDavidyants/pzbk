package de.interfaceag.bmw.pzbk.entities;

import java.util.List;

/**
 *
 * @author fn
 */
public interface MeldungObjekt {

    Werk getWerk();

    boolean isStaerkeSchwaeche();

    String getBeschreibungStaerkeSchwaeche();

    String getUrsache();

    String getReferenzen();

    List<Auswirkung> getAuswirkungen();

    List<String> getDetektoren();

    String getLoesungsvorschlag();

}
