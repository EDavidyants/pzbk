package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 *
 * @author Christian Schauer
 */
@Embeddable
public class Zielwert implements Serializable {

    @Column(length = 10000)
    private String wert;

    @Column(length = 10000)
    private String kommentar;


    public String getWert() {
        return wert;
    }


    public void setWert(String wert) {
        this.wert = wert;
    }


    public String getKommentar() {
        return kommentar;
    }


    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    public static Zielwert createTestZielwert(String supplement) {
        Zielwert z = new Zielwert();
        z.setKommentar("Kommentar_" + supplement);
        z.setWert("Wert_" + supplement);
        return z;
    }

    @Override
    public String toString() {
        return "" + this.wert;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        Zielwert zielwert = (Zielwert) object;

        return new EqualsBuilder()
                .append(wert, zielwert.wert)
                .append(kommentar, zielwert.kommentar)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(wert)
                .append(kommentar)
                .toHashCode();
    }
}
