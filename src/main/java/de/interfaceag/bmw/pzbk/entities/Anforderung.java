package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.Kategorie;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.shared.PersistedObject;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Christian Schauer
 */
@Entity
@Table(name = "anforderung")
@Cacheable(false)
@NamedQuery(name = Anforderung.ALL, query = "SELECT DISTINCT a FROM Anforderung a")
@NamedQuery(name = Anforderung.ALL_WITH_MELDUNG, query = "SELECT DISTINCT a FROM Anforderung a WHERE a.meldungen IS NOT EMPTY")
@NamedQuery(name = Anforderung.BY_ID, query = "SELECT a FROM Anforderung a WHERE a.id = :id")
@NamedQuery(name = Anforderung.ANY_BY_STATUS, query = "SELECT a FROM Anforderung a WHERE a.status = :status")
@NamedQuery(name = Anforderung.ID_BY_STATUS, query = "SELECT DISTINCT a.id FROM Anforderung a WHERE a.status = :status")
@NamedQuery(name = Anforderung.BY_ID_LIST, query = "SELECT a FROM Anforderung a WHERE a.id IN :idList")
@NamedQuery(name = Anforderung.BY_KOMPONENTE, query = "SELECT a FROM Anforderung a INNER JOIN a.umsetzer AS u WHERE u.komponente.id = :id")
@NamedQuery(name = Anforderung.BY_SETEAMNAME, query = "SELECT DISTINCT a FROM Anforderung a INNER JOIN a.umsetzer AS u WHERE u.seTeam.name LIKE :seTeamName")
@NamedQuery(name = Anforderung.GET_OTHER_VERSIONS_BY_FACHID_AND_PROZESSBAUKASTEN_IN_STATUS, query = "SELECT DISTINCT a FROM Anforderung a INNER JOIN FETCH a.prozessbaukasten p WHERE a.fachId LIKE :fachId AND p.status = :prozessbaukastenStatus")
@NamedQuery(name = Anforderung.GET_VORIGE_VERSIONEN, query = "SELECT DISTINCT a FROM Anforderung a WHERE a.fachId LIKE :fachId AND a.version < :version")
public class Anforderung extends AbstractAnforderung implements AnforderungReadData, PersistedObject {

    // ------------  queries ---------------------------------------------------
    public static final String ALL = "anforderung.all";
    public static final String ALL_WITH_MELDUNG = "anforderung.all_ids_with_meldung";
    public static final String BY_ID = "anforderung.by_id";
    public static final String ANY_BY_STATUS = "anforderung.any_by_status";
    public static final String ID_BY_STATUS = "anforderung.id_by_status";
    public static final String BY_ID_LIST = "anforderung.by_id_list";
    public static final String BY_KOMPONENTE = "anforderung.by_komponente";
    public static final String BY_SETEAMNAME = "anforderung.by_seTeamName";
    public static final String GET_OTHER_VERSIONS_BY_FACHID_AND_PROZESSBAUKASTEN_IN_STATUS = "anforderung.getOtherVersionsByFachIdForProzessbaukastenInStatus";
    public static final String GET_VORIGE_VERSIONEN = "anforderung.getVorigeVersionen";

    // ------------  fields ----------------------------------------------------
    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "anforderung_id_seq",
            sequenceName = "anforderung_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "anforderung_id_seq")
    private Long id;

    //Konzeptrelevant = true, Architekturrelevant = false
    private boolean phasenbezug;
    //VKBG = true, VAGB = false
    private boolean zeitpktUmsetzungsbest;
    //ja = true, nein = false
    private boolean potentialStandardisierung;

    @NotNull(message = "Version may not be null!")
    private Integer version;

    @NotNull(message = "T-Team may not be null!")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tteam tteam;

    // ***************** "komplexe" Attribute bzw. Verweise auf andere Entities *******************
    // Meldungen
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "meldung_anforderung",
            joinColumns = {
                    @JoinColumn(name = "anforderung_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "meldung_id", referencedColumnName = "id")
            }
    )
    private Set<Meldung> meldungen;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<AnforderungFreigabeBeiModulSeTeam> anfoFreigabeBeiModul = new ArrayList<>();

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY, targetEntity = Umsetzer.class, orphanRemoval = true)
    private Set<Umsetzer> umsetzer = new HashSet<>();

    private String ersteller;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY, targetEntity = ReferenzSystemLink.class, orphanRemoval = true)
    private List<ReferenzSystemLink> referenzSystemLinks = new ArrayList<>();

    private Integer kategorie = 2;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "anforderungen", targetEntity = Prozessbaukasten.class)
    private Set<Prozessbaukasten> prozessbaukasten;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    @JoinTable(name = "anforderung_prozessbaukastenthemenklammer",
            joinColumns = {
                    @JoinColumn(name = "anforderung_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "prozessbaukastenthemenklammer_id", referencedColumnName = "id")
            }
    )
    private List<ProzessbaukastenThemenklammer> prozessbaukastenThemenklammern;

    // ------------  constructors ----------------------------------------------
    public Anforderung() {
        this.version = 1;
        this.meldungen = new HashSet<>();
        Date now = new Date();
        super.setErstellungsdatum(now);
        super.setAenderungsdatum(now);
        super.setZeitpunktStatusaenderung(now);
        this.prozessbaukasten = new HashSet<>();
        this.prozessbaukastenThemenklammern = new ArrayList<>();
    }

    public Anforderung(Integer version) {
        this.version = version;
        Date now = new Date();
        super.setErstellungsdatum(now);
        super.setAenderungsdatum(now);
        super.setZeitpunktStatusaenderung(now);
        this.prozessbaukasten = new HashSet<>();
        this.prozessbaukastenThemenklammern = new ArrayList<>();
    }

    public Anforderung(Integer version, String kommentar) {
        this.version = version;
        super.setKommentarAnforderung(kommentar);
        Date now = new Date();
        super.setErstellungsdatum(now);
        super.setAenderungsdatum(now);
        super.setZeitpunktStatusaenderung(now);
        this.prozessbaukasten = new HashSet<>();
        this.prozessbaukastenThemenklammern = new ArrayList<>();
    }

    public Anforderung(String fachId, Integer version) {
        super.setFachId(fachId);
        this.version = version;
        Date now = new Date();
        super.setErstellungsdatum(now);
        super.setAenderungsdatum(now);
        super.setZeitpunktStatusaenderung(now);
        this.prozessbaukasten = new HashSet<>();
        this.prozessbaukastenThemenklammern = new ArrayList<>();
    }

    public Anforderung(String fachId, Integer version, String kommentar) {
        super.setFachId(fachId);
        this.version = version;
        super.setKommentarAnforderung(kommentar);
        Date now = new Date();
        super.setErstellungsdatum(now);
        super.setAenderungsdatum(now);
        super.setZeitpunktStatusaenderung(now);
        this.prozessbaukasten = new HashSet<>();
        this.prozessbaukastenThemenklammern = new ArrayList<>();
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String getKennzeichen() {
        return "A";
    }

    public void cloneForNewAnforderung(Anforderung anforderungClone) throws IllegalAccessException, InvocationTargetException {
        BeanUtils.copyProperties(anforderungClone, this);

        anforderungClone.setUmsetzer(new HashSet<>());
        for (Umsetzer u : getUmsetzer()) {
            anforderungClone.addUmsetzer(u.getCopy());
        }

        anforderungClone.setAnfoFreigabeBeiModul(new ArrayList<>());
        for (AnforderungFreigabeBeiModulSeTeam af : getAnfoFreigabeBeiModul()) {
            anforderungClone.addAnfoFreigabeBeiModul(af.getCopyForNewAnforderung(af, anforderungClone));
        }

        anforderungClone.setFestgestelltIn(new HashSet<>());
        for (FestgestelltIn w : getFestgestelltIn()) {
            anforderungClone.addFestgestelltIn(w);
        }

        anforderungClone.setAuswirkungen(new ArrayList<>());
        for (Auswirkung auswirkung : getAuswirkungen()) {
            anforderungClone.addAuswirkung(auswirkung.getCopy());
        }

        anforderungClone.clearAnhanege();
        for (Anhang anhang : getAnhaenge()) {
            anforderungClone.addAnhang(anhang.getCopy());
        }

        anforderungClone.setMeldungen(new HashSet<>());
        for (Meldung meldung : getMeldungen()) {
            anforderungClone.addMeldung(meldung);
        }

        List<ReferenzSystemLink> referenzSystemCopies = new ArrayList<>();
        for (ReferenzSystemLink link : getReferenzSystemLinks()) {
            ReferenzSystemLink copy = link.copy();
            referenzSystemCopies.add(copy);
        }
        anforderungClone.setReferenzSystemLinks(referenzSystemCopies);

        List<ProzessbaukastenThemenklammer> prozessbaukastenThemenklammerCopies = new ArrayList<>();
        for (ProzessbaukastenThemenklammer prozessbaukastenThemenklammer : prozessbaukastenThemenklammern) {
            final ProzessbaukastenThemenklammer copy = prozessbaukastenThemenklammer.copy();
            prozessbaukastenThemenklammerCopies.add(copy);
        }
        anforderungClone.setProzessbaukastenThemenklammern(prozessbaukastenThemenklammerCopies);

        Date now = new Date();
        anforderungClone.setAenderungsdatum(now);
        anforderungClone.setErstellungsdatum(now);
        anforderungClone.setStatus(Status.A_INARBEIT);
    }

    public void copy(Anforderung anforderungCopy) throws IllegalAccessException, InvocationTargetException {
        BeanUtils.copyProperties(anforderungCopy, this);
    }

    public void addMeldung(Meldung meldung) {
        // Wenn dieses Objekt eine Anforderung und die Meldung vom Typ Meldung

        if (this.getMeldungen() == null) {
            this.setMeldungen(new HashSet<>());
        }
        this.getMeldungen().add(meldung);
    }

    public void removeMeldung(Meldung meldung) {
        if (this.getMeldungen() != null) {
            this.getMeldungen().remove(meldung);
        }
    }

    public void addUmsetzer(Umsetzer umsetzer) {
        if (this.getUmsetzer() == null) {
            this.umsetzer = new HashSet<>();
        }

        if (!this.getUmsetzer().contains(umsetzer)) {
            this.getUmsetzer().add(umsetzer);
        }
    }

    public void removeUmsetzer(Umsetzer umsetzer) {
        if (this.getUmsetzer() != null && this.getUmsetzer().contains(umsetzer)) {
            this.getUmsetzer().remove(umsetzer);
        }
    }

    public void addAnfoFreigabeBeiModul(AnforderungFreigabeBeiModulSeTeam af) {
        if (getAnfoFreigabeBeiModul() == null) {
            setAnfoFreigabeBeiModul(new ArrayList<>());
        }
        if (!getAnfoFreigabeBeiModul().contains(af)) {
            getAnfoFreigabeBeiModul().add(af);
        }
    }

    public void removeAnfoFreigabeBeiModul(AnforderungFreigabeBeiModulSeTeam af) {
        if (!getAnfoFreigabeBeiModul().isEmpty() && getAnfoFreigabeBeiModul().contains(af)) {
            getAnfoFreigabeBeiModul().remove(af);
        }
    }

    @Override
    public String toString() {
        return getFachId() + " | V" + version;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(53, 97);
        hb.append(this.getId());

        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Anforderung)) {
            return false;
        }
        Anforderung a = (Anforderung) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(this.getId(), a.getId());
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean isPotentialStandardisierung() {
        return potentialStandardisierung;
    }

    public void setPotentialStandardisierung(boolean potentialStandardisierung) {
        this.potentialStandardisierung = potentialStandardisierung;
    }

    @Override
    public boolean isPhasenbezug() {
        return phasenbezug;
    }

    public void setPhasenbezug(boolean phasenbezug) {
        this.phasenbezug = phasenbezug;
        setZeitpktUmsetzungsbest(phasenbezug);
    }

    public boolean getZeitpktUmsetzungsbest() {
        return zeitpktUmsetzungsbest;
    }

    public void setZeitpktUmsetzungsbest(boolean zeitpktUmsetzungsbest) {
        this.zeitpktUmsetzungsbest = zeitpktUmsetzungsbest;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Set<Meldung> getMeldungen() {
        return meldungen;
    }

    public void setMeldungen(Set<Meldung> meldungen) {
        this.meldungen = meldungen;
    }

    public Set<Umsetzer> getUmsetzer() {
        if (umsetzer == null) {
            this.umsetzer = new HashSet<>();
        }
        return umsetzer;
    }

    public void setUmsetzer(Set<Umsetzer> umsetzer) {
        this.umsetzer = umsetzer;
    }

    @Override
    public List<AnforderungFreigabeBeiModulSeTeam> getAnfoFreigabeBeiModul() {
        return anfoFreigabeBeiModul;
    }

    public void setAnfoFreigabeBeiModul(List<AnforderungFreigabeBeiModulSeTeam> anfoFreigabeBeiModul) {
        this.anfoFreigabeBeiModul = anfoFreigabeBeiModul;
    }

    public void resetFreigabeBeiModulSeTeams() {
        List<AnforderungFreigabeBeiModulSeTeam> anfoModuls = this.getAnfoFreigabeBeiModul();

        for (AnforderungFreigabeBeiModulSeTeam anfoModulElement : anfoModuls) {
            anfoModulElement.setAbgelehnt(false);
            anfoModulElement.setFreigabe(false);
            anfoModulElement.setKommentar("");
        }
        this.setAnfoFreigabeBeiModul(anfoModuls);
    }

    @Override
    public String getErsteller() {
        return ersteller;
    }

    public void setErsteller(String ersteller) {
        this.ersteller = ersteller;
    }

    @Override
    public List<ReferenzSystemLink> getReferenzSystemLinks() {
        return referenzSystemLinks;
    }

    public void setReferenzSystemLinks(List<ReferenzSystemLink> referenzSystemLinks) {
        this.referenzSystemLinks = referenzSystemLinks;
    }

    public Kategorie getKategorie() {
        if (kategorie != null) {
            return Kategorie.getById(kategorie);
        } else {
            return null;
        }
    }

    public void setKategorie(Kategorie kategorie) {
        if (kategorie != null) {
            this.kategorie = kategorie.getId();
        } else {
            this.kategorie = null;
        }
    }

    public Set<Prozessbaukasten> getProzessbaukasten() {
        return prozessbaukasten;
    }

    public void setProzessbaukasten(Set<Prozessbaukasten> prozessbaukasten) {
        this.prozessbaukasten = prozessbaukasten;
    }

    public Boolean isProzessbaukastenZugeordnet() {
        return this.prozessbaukasten != null && !this.prozessbaukasten.isEmpty();
    }

    public Optional<Prozessbaukasten> getGueltigerProzessbaukasten() {
        if (this.prozessbaukasten != null) {
            return getProzessbaukasten().stream()
                    .filter(prozessbaukastenElement -> prozessbaukastenElement.getStatus().equals(ProzessbaukastenStatus.GUELTIG))
                    .findAny();
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Tteam getTteam() {
        return tteam;
    }

    public void setTteam(Tteam tteam) {
        this.tteam = tteam;
    }

    public Boolean hasMeldung() {
        return getMeldungen() != null && !getMeldungen().isEmpty();
    }

    public List<ProzessbaukastenThemenklammer> getProzessbaukastenThemenklammernForProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        if (prozessbaukasten == null) {
            return Collections.emptyList();
        }

        return prozessbaukastenThemenklammern.stream()
                .filter(prozessbaukastenThemenklammer -> prozessbaukastenThemenklammer.getProzessbaukasten().equals(prozessbaukasten))
                .collect(Collectors.toList());
    }

    public List<Themenklammer> getThemenklammernForProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        return getProzessbaukastenThemenklammernForProzessbaukasten(prozessbaukasten).stream()
                .map(ProzessbaukastenThemenklammer::getThemenklammer)
                .collect(Collectors.toList());
    }

    public void removeProzessbaukastenThemenklammer(ProzessbaukastenThemenklammer prozessbaukastenThemenklammer) {
        prozessbaukastenThemenklammern.remove(prozessbaukastenThemenklammer);
    }

    public void addProzessbaukastenThemenklammer(ProzessbaukastenThemenklammer prozessbaukastenThemenklammer) {
        prozessbaukastenThemenklammern.add(prozessbaukastenThemenklammer);
    }

    public List<ProzessbaukastenThemenklammer> getProzessbaukastenThemenklammern() {
        return prozessbaukastenThemenklammern;
    }

    public void setProzessbaukastenThemenklammern(List<ProzessbaukastenThemenklammer> prozessbaukastenThemenklammern) {
        this.prozessbaukastenThemenklammern = prozessbaukastenThemenklammern;
    }

    public AnforderungId getAnforderungId() {
        return new AnforderungId(getId());
    }

}
