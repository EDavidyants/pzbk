package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.Status;
import org.apache.commons.beanutils.BeanUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static javax.persistence.TemporalType.TIMESTAMP;

/**
 *
 * @author Christian Schauer
 */
@MappedSuperclass
@Cacheable(false)
public abstract class AbstractAnforderung extends AbstractAnfoMgmtObject implements Serializable {

    // ------------  fields ----------------------------------------------------
    protected String referenzen; // Referenzen (z.B. PQM-Nr.)
    //protected String prioInBl;
    protected String umsetzenderBereich;
    protected boolean staerkeSchwaeche; // Stärke = true, Schäche = false

    @Column(length = 10000)
    protected String beschreibungStaerkeSchwaeche;

    @Column(length = 10000)
    protected String ursache; // (vermutete) Ursache

    @Column(length = 10000)
    protected String beschreibungAnforderungDe;

    @Column(length = 10000)
    protected String beschreibungAnforderungEn;

    @Column(length = 10000)
    protected String kommentarAnforderung;

    @Column(length = 10000)
    protected String loesungsvorschlag;

    @Temporal(TIMESTAMP)
    protected Date zeitpunktStatusaenderung;

    // ***************** "komplexe" Attribute bzw. Verweise auf andere Entities *******************
    // LAZY = Select erfolgt erst bei Zugrif auf Entität sensor
    @ManyToOne(fetch = FetchType.LAZY)//(fetch = FetchType.LAZY)
    protected Mitarbeiter sensor;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private List<Auswirkung> auswirkungen;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<FestgestelltIn> festgestelltIn;

    @ManyToOne(fetch = FetchType.LAZY)
    protected Werk werk;

    @ElementCollection
    private List<String> detektor = new ArrayList<>();

    // *************************** embedded Attribute *********************************************
    // Befüllung aus komplexer Werteliste
    @Embedded
    protected Zielwert zielwert;

    // ------------  constructors ----------------------------------------------
    public AbstractAnforderung() {
        this.zielwert = new Zielwert();
    }

    // ------------  methods ---------------------------------------------------
    public String getClassname() {
        return this.getClass().getSimpleName();
    }

    public void cloneAbstractAnforderung(AbstractAnforderung abstractAnforderungClone) throws IllegalAccessException, InvocationTargetException {
        BeanUtils.copyProperties(abstractAnforderungClone, this);

        abstractAnforderungClone.clearAnhanege();
        for (Anhang anhang : getAnhaenge()) {
            abstractAnforderungClone.addAnhang(anhang.getCopy());
        }

        abstractAnforderungClone.setAuswirkungen(new ArrayList<>());
        for (Auswirkung auswirkung : getAuswirkungen()) {
            abstractAnforderungClone.addAuswirkung(auswirkung.getCopy());
        }

        abstractAnforderungClone.setDetektoren(new ArrayList<>(this.getDetektoren()));

        Date now = new Date();
        abstractAnforderungClone.setAenderungsdatum(now);
        abstractAnforderungClone.setErstellungsdatum(now);
    }

    /**
     * @param festgestelltIn the festgestelltIn to set
     */
    public void setFestgestelltIn(Set<FestgestelltIn> festgestelltIn) {
        this.festgestelltIn = festgestelltIn;
    }

    public void setFestgestelltInList(List<FestgestelltIn> festgestelltInList) {
        if (festgestelltInList == null) {
            setFestgestelltIn(null);
        } else {
            Set<FestgestelltIn> festgestelltInSet = new HashSet<>(festgestelltInList);
            setFestgestelltIn(festgestelltInSet);
        }
    }

    public List<FestgestelltIn> getFestgestelltInList() {
        if (getFestgestelltIn() == null) {
            return null;
        } else {
            return new ArrayList<>(getFestgestelltIn());
        }
    }

    /**
     *
     * @param abstractAnforderung ...
     * @param mitarbeiter ...
     * @param festgestelltIn ...
     * @param auswirkungen ...
     * @param sensorCoc ...
     * @param staerke ...
     * @param status ...
     * @param zusatz ...
     * @param date ...
     */
    public static void populateAbstractTestanforderung(
            AbstractAnforderung abstractAnforderung,
            Mitarbeiter mitarbeiter,
            Set<FestgestelltIn> festgestelltIn,
            List<Auswirkung> auswirkungen,
            SensorCoc sensorCoc,
            boolean staerke,
            Status status,
            Date date,
            String zusatz) {
        String classname = abstractAnforderung.getClassname();

        abstractAnforderung.setAenderungsdatum(date);
        if (auswirkungen != null) {
            abstractAnforderung.setAuswirkungen(auswirkungen);
        }
        abstractAnforderung.setBeschreibungAnforderungDe("Beschreibung der " + classname + " auf deutsch " + zusatz);
        abstractAnforderung.setBeschreibungAnforderungEn("Description of the requirement (" + classname + ") in english" + zusatz);
        abstractAnforderung.setBeschreibungStaerkeSchwaeche("Beschreibung der Stärke bzw. Schwäche");
        abstractAnforderung.setErstellungsdatum(date);
        abstractAnforderung.setFestgestelltIn(festgestelltIn);
        abstractAnforderung.setKommentarAnforderung("Kommentar zur " + classname + " ");
        abstractAnforderung.setLoesungsvorschlag("Lösungsvorschlag ");
        abstractAnforderung.setReferenzen("Referenzen");
        abstractAnforderung.setStatus(status);
        abstractAnforderung.setSensor(mitarbeiter);
        abstractAnforderung.setSensorCoc(sensorCoc);
        abstractAnforderung.setStaerkeSchwaeche(staerke);
        abstractAnforderung.setUrsache("Ursache");
        Zielwert zw = new Zielwert();
        zw.setWert("5000 ");
        zw.setKommentar("Kommentar zum Zielwert");
        abstractAnforderung.setZielwert(zw);

    }

    @Override
    public String toString() {
        return getFachId();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.sensor);
        hash = 97 * hash + Objects.hashCode(this.auswirkungen);
        hash = 97 * hash + Objects.hashCode(this.festgestelltIn);
        hash = 97 * hash + Objects.hashCode(getAnhaenge());
        hash = 97 * hash + Objects.hashCode(getSensorCoc());
        hash = 97 * hash + Objects.hashCode(this.zielwert);
        hash = 97 * hash + Objects.hashCode(this.getFachId());
        hash = 97 * hash + Objects.hashCode(getStatus());
        hash = 97 * hash + (this.staerkeSchwaeche ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.beschreibungStaerkeSchwaeche);
        hash = 97 * hash + Objects.hashCode(this.ursache);
        hash = 97 * hash + Objects.hashCode(this.referenzen);
        //hash = 97 * hash + Objects.hashCode(this.prioInBl);
        hash = 97 * hash + Objects.hashCode(this.beschreibungAnforderungDe);
        hash = 97 * hash + Objects.hashCode(this.beschreibungAnforderungEn);
        hash = 97 * hash + Objects.hashCode(this.kommentarAnforderung);
        hash = 97 * hash + Objects.hashCode(this.loesungsvorschlag);
        hash = 97 * hash + Objects.hashCode(this.umsetzenderBereich);

        return hash;
    }

    @SuppressWarnings("checkstyle:CyclomaticComplexity")
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractAnforderung other = (AbstractAnforderung) obj;
        if (this.staerkeSchwaeche != other.staerkeSchwaeche) {
            return false;
        }
        if (!Objects.equals(this.getFachId(), other.getFachId())) {
            return false;
        }
        if (!Objects.equals(this.beschreibungStaerkeSchwaeche, other.beschreibungStaerkeSchwaeche)) {
            return false;
        }
        if (!Objects.equals(this.ursache, other.ursache)) {
            return false;
        }
        if (!Objects.equals(this.referenzen, other.referenzen)) {
            return false;
        }
        if (!Objects.equals(this.beschreibungAnforderungDe, other.beschreibungAnforderungDe)) {
            return false;
        }
        if (!Objects.equals(this.beschreibungAnforderungEn, other.beschreibungAnforderungEn)) {
            return false;
        }
        if (!Objects.equals(this.kommentarAnforderung, other.kommentarAnforderung)) {
            return false;
        }
        if (!Objects.equals(this.loesungsvorschlag, other.loesungsvorschlag)) {
            return false;
        }
        if (!Objects.equals(this.umsetzenderBereich, other.umsetzenderBereich)) {
            return false;
        }
        if (!Objects.equals(this.sensor, other.sensor)) {
            return false;
        }
        if (!Objects.equals(this.auswirkungen, other.auswirkungen)) {
            return false;
        }
        if (!Objects.equals(this.festgestelltIn, other.festgestelltIn)) {
            return false;
        }
        if (!Objects.equals(this.getAnhaenge(), other.getAnhaenge())) {
            return false;
        }
        if (!Objects.equals(this.getSensorCoc(), other.getSensorCoc())) {
            return false;
        }
        if (!Objects.equals(this.zielwert, other.zielwert)) {
            return false;
        }
        if (this.getStatus() != other.getStatus()) {
            return false;
        }
        if (!Objects.equals(this.werk, other.werk)) {
            return false;
        }
        return true;
    }

    public void addFestgestelltIn(FestgestelltIn festgestelltIn) {
        if (this.getFestgestelltIn() == null) {
            this.setFestgestelltIn(new HashSet<>());
        }
        if (!this.festgestelltIn.contains(festgestelltIn)) {
            this.getFestgestelltIn().add(festgestelltIn);
        }
    }

    public void removeFestgestelltIn(FestgestelltIn festgestelltIn) {
        if (this.getFestgestelltIn() != null) {
            this.getFestgestelltIn().remove(festgestelltIn);
        }
    }

    public String festgestelltInToString() {
        if (festgestelltIn != null && !festgestelltIn.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            festgestelltIn.forEach(f -> {
                sb.append(f.getWert());
                sb.append(" ");
            });
            return sb.toString();
        }
        return "";
    }

    // ------------  getter / setter -------------------------------------------
    @Override
    public abstract Long getId();

    @Override
    public abstract void setId(Long id);

    public Mitarbeiter getSensor() {
        return sensor;
    }

    public void setSensor(Mitarbeiter sensor) {
        this.sensor = sensor;
    }

    public boolean isStaerkeSchwaeche() {
        return staerkeSchwaeche;
    }

    public void setStaerkeSchwaeche(boolean staerkeSchwaeche) {
        this.staerkeSchwaeche = staerkeSchwaeche;
    }

    public String getBeschreibungStaerkeSchwaeche() {
        return beschreibungStaerkeSchwaeche;
    }

    public void setBeschreibungStaerkeSchwaeche(String beschreibungStaerkeSchwaeche) {
        this.beschreibungStaerkeSchwaeche = beschreibungStaerkeSchwaeche;
    }

    public String getUrsache() {
        return ursache;
    }

    public void setUrsache(String ursache) {
        this.ursache = ursache;
    }

    public String getReferenzen() {
        return referenzen;
    }

    public void setReferenzen(String referenzen) {
        this.referenzen = referenzen;
    }

    public String getKommentarAnforderung() {
        return kommentarAnforderung;
    }

    public void setKommentarAnforderung(String kommentarAnforderung) {
        this.kommentarAnforderung = kommentarAnforderung;
    }

    public String getLoesungsvorschlag() {
        return loesungsvorschlag;
    }

    public void setLoesungsvorschlag(String loesungsvorschlag) {
        this.loesungsvorschlag = loesungsvorschlag;
    }

    public String getUmsetzenderBereich() {
        return umsetzenderBereich;
    }

    public void setUmsetzenderBereich(String umsetzenderBereich) {
        this.umsetzenderBereich = umsetzenderBereich;
    }

    public List<Auswirkung> getAuswirkungen() {
        if (auswirkungen == null) {
            this.auswirkungen = new ArrayList<>();
        }
        return auswirkungen;
    }

    public void setAuswirkungen(List<Auswirkung> auswirkungen) {
        this.auswirkungen = auswirkungen;
    }

    public void addAuswirkung(Auswirkung auswirkung) {
        if (this.auswirkungen == null) {
            this.auswirkungen = new ArrayList<>();
        }
        if (auswirkungen.stream().filter(aw -> aw.getAuswirkung().equals(auswirkung.getAuswirkung())
                && aw.getWert().equals(auswirkung.getWert())).count() == 0L) {
            this.auswirkungen.add(auswirkung);
        }
    }

    public void removeAuswirkung(Auswirkung auswirkung) {
        if (this.auswirkungen != null) {
            this.auswirkungen.remove(auswirkung);
        }
    }

    public Zielwert getZielwert() {
        return zielwert;
    }

    public void setZielwert(Zielwert zielwert) {
        this.zielwert = zielwert;
    }

    public String getBeschreibungAnforderungDe() {
        return beschreibungAnforderungDe;
    }

    public void setBeschreibungAnforderungDe(String beschreibungAnforderungDe) {
        this.beschreibungAnforderungDe = beschreibungAnforderungDe;
    }

    public String getBeschreibungAnforderungEn() {
        return beschreibungAnforderungEn;
    }

    public void setBeschreibungAnforderungEn(String beschreibungAnforderungEn) {
        this.beschreibungAnforderungEn = beschreibungAnforderungEn;
    }

    public Set<FestgestelltIn> getFestgestelltIn() {
        return festgestelltIn;
    }

    public StreamedContent getBildAsStreamedContent() {
        if (getBild() != null) {
            return new DefaultStreamedContent(new ByteArrayInputStream(getBild().getNormalImage()), "image/jpeg");
        }
        return new DefaultStreamedContent();
    }

    public StreamedContent getLargeBildAsStreamedContent() {
        if (getBild() != null) {
            return new DefaultStreamedContent(new ByteArrayInputStream(getBild().getLargeImage()), "image/jpeg");
        }
        return new DefaultStreamedContent();
    }

    public Date getZeitpunktStatusaenderung() {
        return zeitpunktStatusaenderung;
    }

    public void setZeitpunktStatusaenderung(Date zeitpunktStatusaenderung) {
        this.zeitpunktStatusaenderung = zeitpunktStatusaenderung;
    }

    public Werk getWerk() {
        return werk;
    }

    public void setWerk(Werk werk) {
        this.werk = werk;
    }

    public List<String> getDetektoren() {
        return Collections.unmodifiableList(detektor);
    }

    public void setDetektoren(List<String> detektor) {
        this.detektor = detektor;
    }

    public String getDetektorenAsString() {
        if (this.getDetektoren() != null) {
            return getDetektoren().stream().collect(Collectors.joining(", "));
        } else {
            return "";
        }
    }

    public void addDetektor(String detektor) {
        if (this.detektor != null && detektor != null && !detektor.isEmpty()) {
            List<String> detektoren = new ArrayList<>(getDetektoren());
            detektoren.add(detektor);
            setDetektoren(detektoren);
        } else {
            setDetektoren(Arrays.asList(detektor));
        }
    }

    public void removeDetektor(String detektor) {
        if (this.detektor != null && detektor != null) {
            List<String> detektoren = new ArrayList<>(getDetektoren());
            detektoren.remove(detektor);
            setDetektoren(detektoren);
        }
    }

} // end of class
