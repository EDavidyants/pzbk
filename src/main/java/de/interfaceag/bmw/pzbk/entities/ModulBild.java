package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author evda
 */
@Entity
@Table(name = "modulbild")
@NamedQueries( {
        @NamedQuery(name = ModulBild.BY_ID, query = "SELECT mbild FROM ModulBild mbild WHERE mbild.id = :id"),
        @NamedQuery(name = ModulBild.GET_DEFAULT_BILD_FOR_MODUL, query = "SELECT mbild FROM ModulBild mbild WHERE mbild.modul = :modul")})
public class ModulBild implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String BY_ID = "modulbild.by_id";
    public static final String GET_DEFAULT_BILD_FOR_MODUL = "modulbild.get_default_for_modul";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "modulbild_id_seq",
            sequenceName = "modulbild_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "modulbild_id_seq")
    private Long id;

    @OneToOne(cascade = CascadeType.MERGE)
    private Modul modul;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Anhang bild;

    public ModulBild() {

    }

    @Override
    public String toString() {
        return "default bild ID " + this.id + " for Modul " + modul.getName();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(67, 827);
        hb.append(id);
        hb.append(modul);
        hb.append(bild);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ModulBild)) {
            return false;
        }
        if (object == this) {
            return true;
        }
        ModulBild other = (ModulBild) object;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, other.id);
        eb.append(modul, other.modul);
        eb.append(bild, other.bild);
        return eb.isEquals();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Modul getModul() {
        return modul;
    }

    public void setModul(Modul modul) {
        this.modul = modul;
    }

    public Anhang getBild() {
        return bild;
    }

    public void setBild(Anhang bild) {
        this.bild = bild;
    }

} // end of class
