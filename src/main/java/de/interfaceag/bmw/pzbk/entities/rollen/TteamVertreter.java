package de.interfaceag.bmw.pzbk.entities.rollen;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Tteam;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 *
 * @author fn
 */
@Entity
public class TteamVertreter implements Serializable {

    @Id
    @SequenceGenerator(name = "tteam_vertreter_id_seq",
            sequenceName = "tteam_vertreter_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tteam_vertreter_id_seq")
    private Long id;

    @ManyToOne
    private Tteam tteam;

    @ManyToOne
    private Mitarbeiter mitarbeiter;

    public TteamVertreter() {
    }

    public TteamVertreter(Tteam tteam, Mitarbeiter mitarbeiter) {
        this.tteam = tteam;
        this.mitarbeiter = mitarbeiter;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Tteam getTteam() {
        return tteam;
    }

    public void setTteam(Tteam tteam) {
        this.tteam = tteam;
    }

    public Mitarbeiter getMitarbeiter() {
        return mitarbeiter;
    }

    public void setMitarbeiter(Mitarbeiter mitarbeiter) {
        this.mitarbeiter = mitarbeiter;
    }

}
