package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.entities.comparator.ModulSeTeamCompareFields;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Cacheable(false)
@NamedQueries({
    @NamedQuery(name = ModulSeTeam.ALL, query = "SELECT se FROM ModulSeTeam se ORDER BY se.name"),
    @NamedQuery(name = ModulSeTeam.ALL_FETCH, query = "SELECT se FROM ModulSeTeam se INNER JOIN FETCH se.elternModul ORDER BY se.name"),
    @NamedQuery(name = ModulSeTeam.BY_ID, query = "SELECT se FROM ModulSeTeam se WHERE se.id = :id"),
    @NamedQuery(name = ModulSeTeam.BY_ID_LIST, query = "SELECT se FROM ModulSeTeam se WHERE se.id IN :idList"),
    @NamedQuery(name = ModulSeTeam.BY_NAME, query = "SELECT se FROM ModulSeTeam se where se.name like :name ORDER BY se.name"),
    @NamedQuery(name = ModulSeTeam.BY_NAME_LIST, query = "SELECT se FROM ModulSeTeam se where se.name IN :nameList"),
    @NamedQuery(name = ModulSeTeam.STARTS_WITH, query = "SELECT se FROM ModulSeTeam se WHERE se.name LIKE :start ORDER BY se.name"),
    @NamedQuery(name = ModulSeTeam.BY_MODUL_AND_STARTS_WITH, query = "SELECT se FROM ModulSeTeam se WHERE se.elternModul = :modul AND se.name LIKE :start ORDER BY se.name"),
    @NamedQuery(name = ModulSeTeam.BY_MODUL, query = "SELECT se FROM ModulSeTeam se WHERE se.elternModul = :modul ORDER BY se.name")})
public class ModulSeTeam implements Serializable, ModulSeTeamCompareFields {

    public static final String CLASSNAME = "ModulSeTeam.";

    public static final String ALL = "modulseteam.all";
    public static final String ALL_FETCH = CLASSNAME + "all_fetch";
    public static final String BY_ID = "modulseteam.by_id";
    public static final String BY_ID_LIST = "modulseteam.by_id_list";
    public static final String BY_NAME = "modulseteam.by_name";
    public static final String BY_NAME_LIST = "modulseteam.by_name_list";
    public static final String STARTS_WITH = "modulseteam.starts_with";
    public static final String BY_MODUL_AND_STARTS_WITH = "modulseteam.by_modul_and_starts_with";
    public static final String BY_MODUL = "modulseteam.by_modul";

    @Id
    @SequenceGenerator(name = "modulseteam_id_seq",
            sequenceName = "modulseteam_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "modulseteam_id_seq")
    private Long id;

    private String name;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Set<ModulKomponente> komponenten;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Modul elternModul;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private Bild bild;

    public ModulSeTeam() {

    }

    public ModulSeTeam(String name, Modul modul, Set<ModulKomponente> komponenten) {
        this.name = name;
        this.elternModul = modul;
        this.komponenten = komponenten;
    }

    public static boolean areSeTeamsEqual(ModulSeTeam se1, ModulSeTeam se2) {
        if (se1 != null && se2 != null) {
            return se1.getName().equals(se2.getName());
        } else {
            return se1 == null && se2 == null;
        }
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(31, 71);
        hb.append(id);
        hb.append(name);
        hb.append(elternModul);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ModulSeTeam)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        ModulSeTeam mst = (ModulSeTeam) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, mst.id);
        eb.append(name, mst.name);
        eb.append(elternModul, mst.elternModul);
        return eb.isEquals();
    }

    public ModulSeTeamId getModulSeTeamId() {
        return new ModulSeTeamId(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addKomponente(ModulKomponente komponente) {
        if (this.komponenten == null) {
            this.komponenten = new HashSet<>();
        }
        this.komponenten.add(komponente);
    }

    public Set<ModulKomponente> getKomponenten() {
        return komponenten;
    }

    public void setKomponenten(Set<ModulKomponente> komponenten) {
        this.komponenten = komponenten;
    }

    public Modul getElternModul() {
        return elternModul;
    }

    public void setElternModul(Modul elternModul) {
        this.elternModul = elternModul;
    }

    public Bild getBild() {
        return bild;
    }

    public void setBild(Bild bild) {
        this.bild = bild;
    }

    @Override
    public String toString() {
        return elternModul.getName() + " > " + name;
    }

}
