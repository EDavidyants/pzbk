package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author Christian Schauer
 */
@Entity
@Table(name = "umsetzer")
@Cacheable(false)
@NamedQueries( {
        @NamedQuery(name = Umsetzer.BY_ID, query = "SELECT u FROM Umsetzer u WHERE u.id = :id")})
public class Umsetzer implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String BY_ID = "umsetzer.by_id";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "umsetzer_id_seq",
            sequenceName = "umsetzer_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "umsetzer_id_seq")
    @Column(name = "umsetzer_id")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    private ModulSeTeam seTeam;

    @OneToOne(fetch = FetchType.LAZY)
    private ModulKomponente komponente;

    // ------------  constructors ----------------------------------------------
    public Umsetzer() {
    }

    public Umsetzer(ModulSeTeam seTeam, ModulKomponente komponente) {
        this.seTeam = seTeam;
        this.komponente = komponente;
    }

    public Umsetzer(ModulSeTeam seTeam) {
        this.seTeam = seTeam;
        this.komponente = null;
    }

    // ------------  methods ---------------------------------------------------
    public Modul getModul() {
        if (this.seTeam != null) {
            return this.seTeam.getElternModul();
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        if (seTeam != null) {
            if (seTeam.getElternModul() != null) {
                sb.append(seTeam.getElternModul().getName());
                sb.append(" > ");
            }

            sb.append(seTeam.getName());

            if (komponente != null) {
                sb.append(" > ");
                sb.append(komponente.getName());
            }
        }

        return sb.toString();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(31, 89);
        hb.append(id);
        hb.append(seTeam);
        hb.append(komponente);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Umsetzer)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        Umsetzer executer = (Umsetzer) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, executer.id);
        eb.append(seTeam, executer.seTeam);
        eb.append(komponente, executer.komponente);
        return eb.isEquals();
    }

    public Umsetzer getCopy() {
        Umsetzer u = new Umsetzer();
        u.setKomponente(komponente);
        u.setSeTeam(seTeam);
        return u;
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ModulSeTeam getSeTeam() {
        return seTeam;
    }

    public void setSeTeam(ModulSeTeam seTeam) {
        this.seTeam = seTeam;
    }

    public ModulKomponente getKomponente() {
        return komponente;
    }

    public void setKomponente(ModulKomponente komponente) {
        this.komponente = komponente;
    }

} // end of class
