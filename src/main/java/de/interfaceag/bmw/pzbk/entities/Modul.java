package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.entities.comparator.ModulCompareFields;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Christian Schauer
 */
@Entity
@Cacheable(false)
@NamedQueries( {
        @NamedQuery(name = Modul.ALL, query = "SELECT DISTINCT m FROM Modul m ORDER BY m.name"),
        @NamedQuery(name = Modul.BY_ID, query = "SELECT m FROM Modul m WHERE m.id = :id"),
        @NamedQuery(name = Modul.QUERY_BY_ID_LIST, query = "SELECT m FROM Modul m WHERE m.id in :ids"),
        @NamedQuery(name = Modul.ALL_IDS, query = "SELECT DISTINCT m.name FROM Modul m ORDER BY m.name"),
        @NamedQuery(name = Modul.BY_NAME, query = "SELECT m FROM Modul m WHERE m.name LIKE :name"),
        @NamedQuery(name = Modul.BY_NAME_LIST, query = "SELECT m FROM Modul m WHERE m.name IN :names"),
        @NamedQuery(name = Modul.STARTS_WITH, query = "SELECT DISTINCT m FROM Modul m WHERE m.name LIKE :start ORDER BY m.name"),
        @NamedQuery(name = Modul.ALL_FACHBEREICHE, query = "SELECT DISTINCT m.fachBereich FROM Modul m")})
public class Modul implements Serializable, ModulCompareFields {

    // ------------  queries ---------------------------------------------------
    public static final String ALL = "modul.all";
    public static final String BY_ID = "modul.by_id";
    public static final String QUERY_BY_ID_LIST = "modul.by_id_list";
    public static final String ALL_IDS = "modul.allIds";
    public static final String BY_NAME = "modul.by_name";
    public static final String BY_NAME_LIST = "modul.by_name_list";
    public static final String STARTS_WITH = "modul.starts_with";
    public static final String ALL_FACHBEREICHE = "fachbereich.all";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "modul_id_seq",
            sequenceName = "modul_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "modul_id_seq")
    private Long id;

    private String name;

    private String fachBereich;

    @Column(length = 10000)
    private String beschreibung;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private Bild modulBild;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Set<ModulSeTeam> seTeams;

    // ------------  constructors ----------------------------------------------
    public Modul() {
        this.fachBereich = "";
        this.beschreibung = "";
    }

    public Modul(String name) {
        this.name = name;
        this.fachBereich = "";
        this.beschreibung = "";
    }

    public Modul(String name, String fachBereich, String beschreibung) {
        this.name = name;
        this.fachBereich = fachBereich;
        this.beschreibung = beschreibung;
    }

    // ------------  methods ---------------------------------------------------
    public static boolean areModulesEqual(Modul modul1, Modul modul2) {
        if (modul1 != null && modul2 != null) {
            return modul1.getName().equals(modul2.getName());
        } else {
            return modul1 == null && modul2 == null;
        }
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(23, 83);
        hb.append(id);
        hb.append(name);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Modul)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        Modul modul = (Modul) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, modul.id);
        eb.append(name, modul.name);
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addSeTeam(ModulSeTeam seTeam) {
        if (this.seTeams == null) {
            this.seTeams = new HashSet<>();
        }
        this.seTeams.add(seTeam);
    }

    public Set<ModulSeTeam> getSeTeams() {
        return seTeams;
    }

    public void setSeTeams(Set<ModulSeTeam> seTeams) {
        this.seTeams = seTeams;
    }

    public String getFachBereich() {
        return fachBereich;
    }

    public void setFachBereich(String fachBereich) {
        this.fachBereich = fachBereich;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public Bild getModulBild() {
        return modulBild;
    }

    public void setModulBild(Bild modulBild) {
        this.modulBild = modulBild;
    }

} // end of class
