package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

/**
 * @author Christian Schauer
 */
@Entity
@NamedQueries( {
        @NamedQuery(name = Anhang.GET_ANHANG_BY_ID, query = "SELECT anh FROM Anhang anh WHERE anh.id = :id"),
        @NamedQuery(name = Anhang.ANHANG_BY_NAME, query = "SELECT a FROM Anhang a WHERE a.dateiname LIKE :name"),
        @NamedQuery(name = Anhang.ANHANG_BY_CONTENT_ID, query = "SELECT a FROM Anhang a WHERE a.content.id = :id"),
        @NamedQuery(name = Anhang.GET_ANHANG_BY_ID_FETCH, query = "SELECT a FROM Anhang a "
                + "INNER JOIN FETCH a.content "
                + "WHERE a.id = :id")})
public class Anhang implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String GET_ANHANG_BY_ID = "anhang.get_anhang_by_id";
    public static final String ANHANG_BY_NAME = "anhang_by_name";
    public static final String ANHANG_BY_CONTENT_ID = "anhang_by_content_id";
    public static final String GET_ANHANG_BY_ID_FETCH = "anhang.get_anhang_by_id_fetch";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "anhang_id_seq",
            sequenceName = "anhang_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "anhang_id_seq")
    private Long id;

    private String dateiname;
    private String dateityp;
    private int filesize;
    private boolean standardBild;

    @Temporal(TIMESTAMP)
    private Date zeitstempel;
    @ManyToOne(fetch = FetchType.LAZY)
    private Mitarbeiter creator;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private DbFile content;

    // ------------  constructors ----------------------------------------------
    public Anhang() {
        this.standardBild = false;
        this.content = new DbFile();
    }

    public Anhang(Mitarbeiter creator, String name, String type, DbFile content) {
        this.creator = creator;
        this.dateiname = name;
        this.dateityp = type;
        this.content = content;
        this.zeitstempel = new Date(System.currentTimeMillis());
        this.standardBild = false;
    }

    public Anhang(Mitarbeiter creator, String name, String type, byte[] content) {
        this.creator = creator;
        this.filesize = content.length;
        this.dateiname = name;
        this.dateityp = type;
        this.content = new DbFile(name, type, content);
        this.zeitstempel = new Date(System.currentTimeMillis());
        this.standardBild = false;
    }

    public Anhang(Mitarbeiter owner, DbFile content) {
        this.creator = owner;
        this.dateityp = content.getType();
        this.content = content;
        this.zeitstempel = new Date(System.currentTimeMillis());
        this.standardBild = false;
    }

    public Anhang(String name, DbFile content) {
        this.dateiname = name;
        this.dateityp = content.getType();
        this.content = content;
        this.filesize = content.getSize();
        this.zeitstempel = new Date(System.currentTimeMillis());
        this.standardBild = false;
    }

    // ------------  methods ---------------------------------------------------
    public static Anhang createTestAnhang(String supplement, Date date) {
        Anhang a = new Anhang();
        a.setDateiname("Dateiname_" + supplement);
        a.setZeitstempel(date);
        return a;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" ").append("/").append(dateiname);
        sb.append("...").append(zeitstempel);
        return sb.toString();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(23, 463);
        hb.append(id);
        hb.append(dateiname);
        hb.append(dateityp);
        hb.append(filesize);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Anhang)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        Anhang other = (Anhang) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, other.id);
        eb.append(dateiname, other.dateiname);
        eb.append(dateityp, other.dateityp);
        eb.append(filesize, other.filesize);
        return eb.isEquals();
    }

    public String getFilesizeAsString() {
        float divisor = (float) 1024 * 1024;
        float sizeMb = this.filesize / divisor;
        String result;

        if (sizeMb < 0.01) {
            result = this.filesize + " Byte";
        } else if (sizeMb < 1.0) {
            result = this.filesize / 1024 + " kB";
        } else {
            result = String.format("%.2f", sizeMb) + " MB";
        }
        return result;
    }

    public Anhang getCopy() {
        Anhang anh = new Anhang();
        anh.setDateiname(dateiname);
        anh.setDateityp(dateityp);
        anh.setContent(content.getCopy());
        anh.setFilesize(filesize);
        anh.setZeitstempel(zeitstempel);
        anh.setStandardBild(isStandardBild());
        return anh;
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDateiname() {
        return dateiname;
    }

    public void setDateiname(String dateiname) {
        this.dateiname = dateiname;
    }

    public Date getZeitstempel() {
        return new Date(zeitstempel.getTime());
    }

    public void setZeitstempel(Date zeitstempel) {
        this.zeitstempel = new Date(zeitstempel.getTime());
    }

    public DbFile getContent() {
        return content;
    }

    public void setContent(DbFile content) {
        this.content = content;
    }

    public Mitarbeiter getCreator() {
        return creator;
    }

    public void setCreator(Mitarbeiter creator) {
        this.creator = creator;
    }

    public String getDateityp() {
        return dateityp;
    }

    public void setDateityp(String dateityp) {
        this.dateityp = dateityp;
    }

    public int getFilesize() {
        return filesize;
    }

    public void setFilesize(int filesize) {
        this.filesize = filesize;
    }

    /**
     * @return the standardBild
     */
    public boolean isStandardBild() {
        return standardBild;
    }

    /**
     * @param standardBild the standardBild to set
     */
    public void setStandardBild(boolean standardBild) {
        this.standardBild = standardBild;
    }

} // end of class
