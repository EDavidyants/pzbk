package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.shared.objectIds.DomainObject;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = DerivatFahrzeugmerkmal.BY_DERIVAT, query = "SELECT derivatFahrzeugmerkmal FROM DerivatFahrzeugmerkmal derivatFahrzeugmerkmal WHERE derivatFahrzeugmerkmal.derivat.id = :derivatId"),
        @NamedQuery(name = DerivatFahrzeugmerkmal.AUSPRAEGUNG_BY_DERIVAT_AND_MERKMAL, query = "SELECT auspraegung FROM DerivatFahrzeugmerkmal derivatFahrzeugmerkmal INNER JOIN derivatFahrzeugmerkmal.auspraegungen auspraegung WHERE derivatFahrzeugmerkmal.derivat.id = :derivatId AND derivatFahrzeugmerkmal.fahrzeugmerkmal.id = :fahrzeugmerkmalId ORDER BY auspraegung")
})
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"derivat_id", "fahrzeugmerkmal_id"})})
public class DerivatFahrzeugmerkmal implements DomainObject {

    private static final String CLASSNAME = "DerivatFahrzeugmerkmal.";
    public static final String BY_DERIVAT = CLASSNAME + "by_derivat";
    public static final String AUSPRAEGUNG_BY_DERIVAT_AND_MERKMAL = CLASSNAME + "AUSPRAEGUNG_BY_DERIVAT_AND_MERKMAL";

    @Id
    @SequenceGenerator(name = "derivatfahrzeugmerkmal_id_seq",
            sequenceName = "derivatfahrzeugmerkmal_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "derivatfahrzeugmerkmal_id_seq")
    private Long id;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Derivat derivat;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Fahrzeugmerkmal fahrzeugmerkmal;

    @ManyToMany
    @JoinTable(name = "derivatfahrzeugmerkmal_fahrzeugmerkmalauspraegung",
            joinColumns = {@JoinColumn(name = "derivatfahrzeugmerkmal_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "fahrzeugmerkmalauspraegung_id", referencedColumnName = "id")}
    )
    private List<FahrzeugmerkmalAuspraegung> auspraegungen;

    private boolean isNotRelevant;


    public DerivatFahrzeugmerkmal() {

    }

    public DerivatFahrzeugmerkmal(Derivat derivat, Fahrzeugmerkmal fahrzeugmerkmal, List<FahrzeugmerkmalAuspraegung> auspraegungen) {
        this.derivat = derivat;
        this.fahrzeugmerkmal = fahrzeugmerkmal;
        this.auspraegungen = auspraegungen;
    }

    public DerivatFahrzeugmerkmal(Derivat derivat, Fahrzeugmerkmal fahrzeugmerkmal, List<FahrzeugmerkmalAuspraegung> auspraegungen, boolean isNotRelevant) {
        this.derivat = derivat;
        this.fahrzeugmerkmal = fahrzeugmerkmal;
        this.auspraegungen = auspraegungen;
        this.isNotRelevant = isNotRelevant;
    }

    @Override
    public String toString() {
        return "DerivatFahrzeugmerkmal{" +
                "id=" + id +
                ", derivat=" + derivat +
                ", fahrzeugmerkmal=" + fahrzeugmerkmal +
                ", auspraegungen=" + auspraegungen +
                ", isNotRelevant=" + isNotRelevant +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        DerivatFahrzeugmerkmal that = (DerivatFahrzeugmerkmal) object;

        return new EqualsBuilder()
                .append(isNotRelevant, that.isNotRelevant)
                .append(id, that.id)
                .append(derivat, that.derivat)
                .append(fahrzeugmerkmal, that.fahrzeugmerkmal)
                .append(auspraegungen, that.auspraegungen)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(derivat)
                .append(fahrzeugmerkmal)
                .append(auspraegungen)
                .append(isNotRelevant)
                .toHashCode();
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Derivat getDerivat() {
        return derivat;
    }

    public void setDerivat(Derivat derivat) {
        this.derivat = derivat;
    }

    public Fahrzeugmerkmal getFahrzeugmerkmal() {
        return fahrzeugmerkmal;
    }

    public void setFahrzeugmerkmal(Fahrzeugmerkmal fahrzeugmerkmal) {
        this.fahrzeugmerkmal = fahrzeugmerkmal;
    }

    public List<FahrzeugmerkmalAuspraegung> getAuspraegungen() {
        return auspraegungen;
    }

    public void setAuspraegungen(List<FahrzeugmerkmalAuspraegung> auspraegungen) {
        this.auspraegungen = auspraegungen;
    }

    public boolean isNotRelevant() {
        return isNotRelevant;
    }

    public void setNotRelevant(boolean notRelevant) {
        isNotRelevant = notRelevant;
    }
}
