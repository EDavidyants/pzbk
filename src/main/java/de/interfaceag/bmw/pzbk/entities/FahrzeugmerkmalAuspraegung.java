package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalAuspraegungDto;
import de.interfaceag.bmw.pzbk.shared.objectIds.DomainObject;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

@Entity
@NamedQueries({
        @NamedQuery(name = FahrzeugmerkmalAuspraegung.ALL, query = "SELECT f FROM FahrzeugmerkmalAuspraegung f ORDER BY f.auspraegung ASC"),
        @NamedQuery(name = FahrzeugmerkmalAuspraegung.ALL_FOR_FAHRZEUGMERKMAL, query = "SELECT f FROM FahrzeugmerkmalAuspraegung f WHERE f.fahrzeugmerkmal.id = :fahrzeugmerkmalId ORDER BY f.auspraegung ASC")
})
public class FahrzeugmerkmalAuspraegung implements DomainObject {

    private static final String CLASSNAME = "FahrzeugmerkmalAuspraegung.";
    public static final String ALL = CLASSNAME + "all";
    public static final String ALL_FOR_FAHRZEUGMERKMAL = CLASSNAME + "ALL_FOR_FAHRZEUGMERKMAL";

    @Id
    @SequenceGenerator(name = "fahrzeugmerkmalwert_id_seq",
            sequenceName = "fahrzeugmerkmalwert_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fahrzeugmerkmalwert_id_seq")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Fahrzeugmerkmal fahrzeugmerkmal;

    @Column(length = 1000, nullable = false)
    private String auspraegung;

    public FahrzeugmerkmalAuspraegung() {

    }

    public FahrzeugmerkmalAuspraegung(Fahrzeugmerkmal fahrzeugmerkmal, String auspraegung) {
        this.fahrzeugmerkmal = fahrzeugmerkmal;
        this.auspraegung = auspraegung;
    }

    @Override
    public String toString() {
        return "FahrzeugmerkmalAuspraegung{" +
                "id=" + id +
                ", fahrzeugmerkmal=" + fahrzeugmerkmal +
                ", auspraegung='" + auspraegung + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        FahrzeugmerkmalAuspraegung that = (FahrzeugmerkmalAuspraegung) object;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(fahrzeugmerkmal, that.fahrzeugmerkmal)
                .append(auspraegung, that.auspraegung)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(fahrzeugmerkmal)
                .append(auspraegung)
                .toHashCode();
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FahrzeugmerkmalAuspraegungId getFahrzeugmerkmalAuspraegungId() {
        return new FahrzeugmerkmalAuspraegungId(id);
    }

    public Fahrzeugmerkmal getFahrzeugmerkmal() {
        return fahrzeugmerkmal;
    }

    public void setFahrzeugmerkmal(Fahrzeugmerkmal fahrzeugmerkmal) {
        this.fahrzeugmerkmal = fahrzeugmerkmal;
    }

    public String getAuspraegung() {
        return auspraegung;
    }

    public void setAuspraegung(String auspraegung) {
        this.auspraegung = auspraegung;
    }

    public FahrzeugmerkmalAuspraegungDto getDto() {
        return new FahrzeugmerkmalAuspraegungDto(id, auspraegung);
    }
}
