package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.entities.comparator.ModulKomponenteComparFields;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 * @author Christian Schauer
 */
@Entity
@Cacheable(false)
@NamedQueries( {
        @NamedQuery(name = ModulKomponente.ALL, query = "SELECT mk FROM ModulKomponente mk ORDER BY mk.name"),
        @NamedQuery(name = ModulKomponente.BY_ID, query = "SELECT mk FROM ModulKomponente mk WHERE mk.id = :id"),
        @NamedQuery(name = ModulKomponente.BY_PPG, query = "SELECT mk FROM ModulKomponente mk WHERE mk.ppg LIKE :ppg"),
        @NamedQuery(name = ModulKomponente.BY_NAME, query = "SELECT mk FROM ModulKomponente mk WHERE mk.name LIKE :name ORDER BY mk.ppg"),
        @NamedQuery(name = ModulKomponente.BY_SETEAM, query = "SELECT mk FROM ModulKomponente mk WHERE mk.seTeam = :seTeam ORDER BY mk.ppg"),
        @NamedQuery(name = ModulKomponente.LIKE, query = "SELECT mk FROM ModulKomponente mk WHERE mk.name LIKE :start ORDER BY mk.name"),
        @NamedQuery(name = ModulKomponente.BY_SETEAM_AND_LIKE, query = "SELECT mk FROM ModulKomponente mk WHERE mk.seTeam = :seTeam AND mk.name LIKE :start ORDER BY mk.name")})
public class ModulKomponente implements Serializable, ModulKomponenteComparFields {

    // ------------  queries ---------------------------------------------------
    public static final String ALL = "modulkomponente.all";
    public static final String BY_ID = "modulkomponente.by_id";
    public static final String BY_PPG = "modulkomponente.by_ppg";
    public static final String BY_NAME = "modulkomponente.by_name";
    public static final String BY_SETEAM = "modulkomponente.by_seteam";
    public static final String LIKE = "modulkomponente.like";
    public static final String BY_SETEAM_AND_LIKE = "modulkomponente.by_seteam_and_like";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "modulkomponente_id_seq",
            sequenceName = "modulkomponente_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "modulkomponente_id_seq")
    private Long id;

    private String ppg;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private ModulSeTeam seTeam;

    // ------------  constructors ----------------------------------------------
    public ModulKomponente() {

    }

    public ModulKomponente(String ppg, String name) {
        this.ppg = ppg;
        this.name = name;
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(31, 83);
        hb.append(id);
        hb.append(ppg);
        hb.append(name);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ModulKomponente)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        ModulKomponente mk = (ModulKomponente) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, mk.id);
        eb.append(ppg, mk.ppg);
        eb.append(name, mk.name);
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPpg() {
        return ppg;
    }

    public void setPpg(String ppg) {
        this.ppg = ppg;
    }

    public ModulSeTeam getSeTeam() {
        return seTeam;
    }

    public void setSeTeam(ModulSeTeam seTeam) {
        this.seTeam = seTeam;
    }

} // end of class
