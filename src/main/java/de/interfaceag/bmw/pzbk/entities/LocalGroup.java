package de.interfaceag.bmw.pzbk.entities;

import javax.annotation.PostConstruct;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Christian Schauer
 */
@Entity
@NamedQueries({
    @NamedQuery(name = LocalGroup.GRUPPE_ALL, query = "select g from LocalGroup g"),
    @NamedQuery(name = LocalGroup.GRUPPE_BY_NAME, query = "select g from LocalGroup g where g.gruppenname = :name")})
public class LocalGroup implements Serializable {

    public static final String GRUPPE_ALL = "gruppeAll";
    public static final String GRUPPE_BY_NAME = "gruppeByName";

    @Id
    private String gruppenname;

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER, mappedBy = "gruppen")
    private List<LocalUser> mitglieder;

    public LocalGroup() {
        init();
    }

    public LocalGroup(String gruppenname) {
        init();
        this.gruppenname = gruppenname;
    }

    @PostConstruct
    public final void init() {
        this.mitglieder = new ArrayList<>();
    }

    public void addMitglied(LocalUser mitglied) {
        mitglieder.add(mitglied);
        mitglied.addGruppe(this);
    }

    public String getGruppenname() {
        return gruppenname;
    }


}
