package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungFreigabeDto;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalButtonData;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

/**
 * @author evda
 */
@Entity
@Table(name = "anforderung_freigabe_bei_modul")
@NamedQueries({
        @NamedQuery(name = AnforderungFreigabeBeiModulSeTeam.BY_ID, query = "SELECT af FROM AnforderungFreigabeBeiModulSeTeam af WHERE af.id = :id"),
        @NamedQuery(name = AnforderungFreigabeBeiModulSeTeam.BY_ANFORDERUNG, query = "SELECT af FROM AnforderungFreigabeBeiModulSeTeam af WHERE af.anforderung = :anforderung ORDER BY af.datum"),
        @NamedQuery(name = AnforderungFreigabeBeiModulSeTeam.BY_MODULSETEAM, query = "SELECT af FROM AnforderungFreigabeBeiModulSeTeam af WHERE af.modulSeTeam = :modulSeTeam"),
        @NamedQuery(name = AnforderungFreigabeBeiModulSeTeam.GET_ANFORDERUNG_ID_BY_MODULSETEAMNAMELIST, query = "SELECT af.anforderung.id FROM AnforderungFreigabeBeiModulSeTeam af WHERE af.modulSeTeam.id IN :modulSeTeamIdList"),
        @NamedQuery(name = AnforderungFreigabeBeiModulSeTeam.BY_ANFORDERUNG_AND_MODULSETEAM, query = "SELECT af FROM AnforderungFreigabeBeiModulSeTeam af WHERE af.anforderung = :anforderung AND af.modulSeTeam = :modul"),
        @NamedQuery(name = AnforderungFreigabeBeiModulSeTeam.GET_FREIGEGEBENE_MODULSETEAMS_FOR_ANFORDERUNG, query = "SELECT DISTINCT af.modulSeTeam FROM AnforderungFreigabeBeiModulSeTeam af WHERE af.anforderung = :anforderung AND af.freigabe = true"),
        @NamedQuery(name = AnforderungFreigabeBeiModulSeTeam.GET_FREIGEGEBENE_ZAK_MODULE_FOR_ANFORDERUNG, query = "SELECT DISTINCT af.modulSeTeam FROM AnforderungFreigabeBeiModulSeTeam af WHERE af.anforderung = :anforderung AND af.freigabe = true AND af.vereinbarung = :vereinbarung"),
        @NamedQuery(name = AnforderungFreigabeBeiModulSeTeam.GET_BY_ANFORDERUNG_WITH_ID_NOT_MATCHING, query = "SELECT af FROM AnforderungFreigabeBeiModulSeTeam af WHERE af.anforderung = :anforderung AND af.id != :id"),
        @NamedQuery(name = AnforderungFreigabeBeiModulSeTeam.COUNT_ALL_MODULE_FOR_ANFORDERUNG, query = "SELECT COUNT(af.id) FROM AnforderungFreigabeBeiModulSeTeam af WHERE af.anforderung = :anforderung"),
        @NamedQuery(name = AnforderungFreigabeBeiModulSeTeam.COUNT_FREIGEGEBENE_MODULE_FOR_ANFORDERUNG, query = "SELECT COUNT(af.id) FROM AnforderungFreigabeBeiModulSeTeam af WHERE af.anforderung = :anforderung AND af.freigabe = true"),
        @NamedQuery(name = AnforderungFreigabeBeiModulSeTeam.GET_FREIGEGEBENE_MODULE_FOR_ANFORDERUNG, query = "SELECT af FROM AnforderungFreigabeBeiModulSeTeam af WHERE af.anforderung = :anforderung AND af.freigabe = true")})
public class AnforderungFreigabeBeiModulSeTeam implements Serializable, AnforderungEditFahrzeugmerkmalButtonData {

    // ------------  queries ---------------------------------------------------
    public static final String BY_ID = "anforderungFreigabeInModul.by_id";
    public static final String BY_ANFORDERUNG = "anforderungFreigabeInModul.by_anforderung";
    public static final String BY_MODULSETEAM = "anforderungFreigabeInModul.by_modul";
    public static final String BY_ANFORDERUNG_AND_MODULSETEAM = "anforderungFreigabeInModul.by_anforderung_and_modul";
    public static final String GET_ANFORDERUNG_ID_BY_MODULSETEAMNAMELIST = "anforderungFreigabeInModul.get_anforderung_id_by_modul_list";
    public static final String GET_FREIGEGEBENE_MODULSETEAMS_FOR_ANFORDERUNG = "anforderungFreigabeInModul.getFreigegebene_modulseteams_for_anforderung";
    public static final String GET_FREIGEGEBENE_ZAK_MODULE_FOR_ANFORDERUNG = "anforderungFreigabeInModul.getFreigegebene_zak_module_for_anforderung";
    public static final String GET_BY_ANFORDERUNG_WITH_ID_NOT_MATCHING = "anforderungFreigabeInModul.get_by_anforderung_with_id_not_matching";
    public static final String COUNT_ALL_MODULE_FOR_ANFORDERUNG = "anforderungFreigabeInModul.count_all_module_for_anforderung";
    public static final String COUNT_FREIGEGEBENE_MODULE_FOR_ANFORDERUNG = "anforderungFreigabeInModul.count_freigegebene_module_for_anforderung";
    public static final String GET_FREIGEGEBENE_MODULE_FOR_ANFORDERUNG = "anforderungFreigabeInModul.get_freigegebene_module_for_anforderung";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "anforderungfreigabe_id_seq",
            sequenceName = "anforderungfreigabe_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "anforderungfreigabe_id_seq")
    private Long id;

    @ManyToOne
    private Anforderung anforderung;

    @ManyToOne
    private ModulSeTeam modulSeTeam;

    @ManyToOne(fetch = FetchType.LAZY)
    private Mitarbeiter bearbeiter;

    private boolean freigabe;

    private boolean abgelehnt;

    @Column(length = 10000)
    private String kommentar;

    @Enumerated(EnumType.ORDINAL)
    private VereinbarungType vereinbarung;

    @Temporal(TIMESTAMP)
    private Date datum;

    @Transient
    private boolean fahrzeugmerkmalConfigured;

    // ------------  constructors ----------------------------------------------
    public AnforderungFreigabeBeiModulSeTeam() {
        this.vereinbarung = VereinbarungType.ZAK;
        this.abgelehnt = false;
    }

    public AnforderungFreigabeBeiModulSeTeam(Anforderung anforderung, ModulSeTeam modulSeTeam) {
        this.anforderung = anforderung;
        this.modulSeTeam = modulSeTeam;
        this.vereinbarung = VereinbarungType.ZAK;
        this.datum = new Date();
        this.abgelehnt = false;
    }

    public AnforderungFreigabeBeiModulSeTeam(Anforderung anforderung, ModulSeTeam modulSeTeam, VereinbarungType vereinbarungTyp) {
        this.anforderung = anforderung;
        this.modulSeTeam = modulSeTeam;
        this.vereinbarung = vereinbarungTyp;
        this.datum = new Date();
        this.abgelehnt = false;
    }

    public AnforderungFreigabeBeiModulSeTeam(Anforderung anforderung, ModulSeTeam modulSeTeam, boolean freigabe, String kommentar, VereinbarungType vereinbarung) {
        this.anforderung = anforderung;
        this.modulSeTeam = modulSeTeam;
        this.freigabe = freigabe;
        this.datum = new Date();
        this.kommentar = kommentar;
        this.vereinbarung = vereinbarung;
        this.abgelehnt = false;
    }

    public AnforderungFreigabeBeiModulSeTeam(Anforderung anforderung, ModulSeTeam modulSeTeam, boolean freigabe, VereinbarungType vereinbarung) {
        this.anforderung = anforderung;
        this.modulSeTeam = modulSeTeam;
        this.freigabe = freigabe;
        this.datum = new Date();
        this.vereinbarung = vereinbarung;
        this.abgelehnt = false;
    }

    public AnforderungFreigabeBeiModulSeTeam(Anforderung anforderung, ModulSeTeam modulSeTeam, Boolean freigabe) {
        this.anforderung = anforderung;
        this.modulSeTeam = modulSeTeam;
        this.freigabe = freigabe;
        this.datum = new Date();
        this.kommentar = "";
        this.vereinbarung = VereinbarungType.ZAK;
        this.abgelehnt = false;
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        String freigegeben;
        if (freigabe) {
            freigegeben = " freigegeben";
        } else {
            freigegeben = " abgelehnt";
        }
        return "Anforderung ID: " + anforderung + " in SE-Team: " + modulSeTeam + " von " + bearbeiter + freigegeben;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(61, 199);
        hb.append(id);
        hb.append(anforderung);
        hb.append(modulSeTeam);
        hb.append(bearbeiter);
        hb.append(freigabe);
        hb.append(kommentar);
        hb.append(vereinbarung);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AnforderungFreigabeBeiModulSeTeam)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        AnforderungFreigabeBeiModulSeTeam af = (AnforderungFreigabeBeiModulSeTeam) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, af.id);
        eb.append(anforderung, af.anforderung);
        eb.append(modulSeTeam, af.modulSeTeam);
        eb.append(bearbeiter, af.bearbeiter);
        eb.append(freigabe, af.freigabe);
        eb.append(kommentar, af.kommentar);
        eb.append(vereinbarung, af.vereinbarung);
        return eb.isEquals();
    }

    public String getDatumFormatted() {
        SimpleDateFormat toformat = new SimpleDateFormat("dd.MM.yyyy");
        String formattedDate;
        if (datum != null) {
            formattedDate = toformat.format(datum);
        } else {
            formattedDate = toformat.format(new Date());
        }
        return formattedDate;
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Anforderung getAnforderung() {
        return anforderung;
    }

    public void setAnforderung(Anforderung anforderung) {
        this.anforderung = anforderung;
    }

    public ModulSeTeam getModulSeTeam() {
        return modulSeTeam;
    }

    public void setModulSeTeam(ModulSeTeam modulSeTeam) {
        this.modulSeTeam = modulSeTeam;
    }

    public Mitarbeiter getBearbeiter() {
        return bearbeiter;
    }

    public void setBearbeiter(Mitarbeiter bearbeiter) {
        this.bearbeiter = bearbeiter;
    }

    public boolean isFreigabe() {
        return freigabe;
    }

    public void setFreigabe(boolean freigabe) {
        this.freigabe = freigabe;
    }

    public boolean isAbgelehnt() {
        return abgelehnt;
    }

    public void setAbgelehnt(boolean abgelehnt) {
        this.abgelehnt = abgelehnt;
    }

    public Date getDatum() {
        return new Date(datum.getTime());
    }

    public void setDatum(Date datum) {
        this.datum = new Date(datum.getTime());
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    public VereinbarungType getVereinbarungType() {
        return vereinbarung;
    }

    public void setVereinbarungType(VereinbarungType vereinbarung) {
        this.vereinbarung = vereinbarung;
    }

    // these methods are only used in the old edit mask of the Anforderung
    public boolean isZak() {
        return this.vereinbarung.isZak();
    }

    public void setZak(boolean isZak) {
        this.vereinbarung = VereinbarungType.getVereinbarungTypeForZAK(isZak);
    }

    public AnforderungFreigabeDto toDto() {

        return new AnforderungFreigabeDto(this.getId(),
                this.getAnforderung().getId(),
                this.getModulSeTeam().getId(), this.getModulSeTeam().getName(),
                this.isFreigabe(), this.isAbgelehnt(),
                this.getKommentar(), this.getVereinbarungType(),
                this.getModulSeTeam().getElternModul().getName());

    }

    public AnforderungFreigabeBeiModulSeTeam getCopyForNewAnforderung(AnforderungFreigabeBeiModulSeTeam anforderungFreigabeBeiModulSeTeam, Anforderung anforderung) {
        AnforderungFreigabeBeiModulSeTeam copy = new AnforderungFreigabeBeiModulSeTeam();
        copy.setAbgelehnt(Boolean.FALSE);
        copy.setAnforderung(anforderung);
        copy.setBearbeiter(anforderungFreigabeBeiModulSeTeam.getBearbeiter());
        copy.setFreigabe(Boolean.FALSE);
        copy.setDatum(new Date());
        copy.setKommentar("");
        copy.setModulSeTeam(anforderungFreigabeBeiModulSeTeam.getModulSeTeam());
        copy.setVereinbarungType(anforderungFreigabeBeiModulSeTeam.getVereinbarungType());
        copy.setZak(anforderungFreigabeBeiModulSeTeam.isZak());
        return copy;
    }

    public void reset() {
        setFreigabe(Boolean.FALSE);
    }

    public void setFahrzeugmerkmalConfigured(Boolean fahrzeugmerkmalConfigured) {
        this.fahrzeugmerkmalConfigured = fahrzeugmerkmalConfigured;
    }

    @Override
    public boolean isConfigured() {
        return fahrzeugmerkmalConfigured;
    }
}
