package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Entity
@Table
@NamedQueries( {
        @NamedQuery(name = ProzessbaukastenAnforderungKonzept.BY_PROZESSBAUKASTEN, query = "SELECT p FROM ProzessbaukastenAnforderungKonzept p WHERE p.prozessbaukasten.id = :prozessbaukastenId"),
        @NamedQuery(name = ProzessbaukastenAnforderungKonzept.BY_ANFORDERUNG, query = "SELECT p FROM ProzessbaukastenAnforderungKonzept p WHERE p.anforderung.id = :anforderungId")})
public class ProzessbaukastenAnforderungKonzept implements Serializable {

    private static final String CLASSNAME = "ProzessbaukastenAnforderungKonzept";
    public static final String BY_PROZESSBAUKASTEN = CLASSNAME + ".byProzessbaukasten";
    public static final String BY_ANFORDERUNG = CLASSNAME + ".byAnforderung";

    @Id
    @SequenceGenerator(name = "prozessbaukasten_anforderung_konzept_id_seq",
            sequenceName = "prozessbaukasten_anforderung_konzept_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prozessbaukasten_anforderung_konzept_id_seq")
    private Long id;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Anforderung anforderung;

    @ManyToOne
    private Konzept konzept;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Prozessbaukasten prozessbaukasten;

    public ProzessbaukastenAnforderungKonzept() {
    }

    public ProzessbaukastenAnforderungKonzept(Anforderung anforderung, Konzept konzept, Prozessbaukasten prozessbaukasten) {
        this.anforderung = anforderung;
        this.konzept = konzept;
        this.prozessbaukasten = prozessbaukasten;
    }

    @Override
    public String toString() {
        return "ProzessbaukastenAnforderungKonzept{" + "id=" + id + ", " + anforderung + ", " + konzept + ", " + prozessbaukasten + '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        ProzessbaukastenAnforderungKonzept that = (ProzessbaukastenAnforderungKonzept) object;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(anforderung, that.anforderung)
                .append(konzept, that.konzept)
                .append(prozessbaukasten, that.prozessbaukasten)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(anforderung)
                .append(konzept)
                .append(prozessbaukasten)
                .toHashCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Anforderung getAnforderung() {
        return anforderung;
    }

    public void setAnforderung(Anforderung anforderung) {
        this.anforderung = anforderung;
    }

    public Konzept getKonzept() {
        return konzept;
    }

    public void setKonzept(Konzept konzept) {
        this.konzept = konzept;
    }

    public Prozessbaukasten getProzessbaukasten() {
        return prozessbaukasten;
    }

    public void setProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        this.prozessbaukasten = prozessbaukasten;
    }

}
