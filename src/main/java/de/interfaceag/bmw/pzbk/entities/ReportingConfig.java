package de.interfaceag.bmw.pzbk.entities;


import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 * @author lomu
 */
@Entity
@NamedQueries( {
        @NamedQuery(name = ReportingConfig.GET_BY_KEYWORD, query = "SELECT r.keyword, r.value FROM ReportingConfig r WHERE r.keyword = :keyword")
})
public class ReportingConfig implements Serializable {

    public static final String GET_BY_KEYWORD = "getByKeyword";


    @Id
    @SequenceGenerator(name = "reporting_config_id_seq",
            sequenceName = "reporting_config_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reporting_config_id_seq")
    private Long id;

    @Column(length = 10000)
    private String keyword;

    @Column(length = 10000)
    private String value;

    public ReportingConfig() {

    }

    public ReportingConfig(String keyword, String value) {
        this.keyword = keyword;
        this.value = value;
    }

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public String getKeyword() {
        return keyword;
    }


    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }


    public String getValue() {
        return value;
    }


    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return " " + this.value;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(13, 41);
        hb.append(id);
        hb.append(keyword);
        hb.append(value);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ReportingConfig)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        ReportingConfig mywert = (ReportingConfig) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, mywert.id);
        eb.append(keyword, mywert.keyword);
        eb.append(value, mywert.value);
        return eb.isEquals();
    }
}
