package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.Page;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Entity
@NamedQueries({
    @NamedQuery(name = UrlEncoding.BY_URLID, query = "SELECT u FROM UrlEncoding u WHERE u.urlId = :urlId")})
public class UrlEncoding implements Serializable {

    private static final String CLASSNAME = "UrlEncoding.";

    public static final String BY_URLID = CLASSNAME + "by_urlid";

    @Id
    @SequenceGenerator(name = "urlencoding_id_seq",
            sequenceName = "urlencoding_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "urlencoding_id_seq")
    private Long id;

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private Page page;

    @Column(unique = true)
    private String urlId;

    @Column(length = 10000)
    private String urlParameter;

    // ---------- constructors -------------------------------------------------
    public UrlEncoding() {
    }

    public UrlEncoding(Page page, String urlId, String urlParameter) {
        this.page = page;
        this.urlId = urlId;
        this.urlParameter = urlParameter;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        UrlEncoding that = (UrlEncoding) object;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(page, that.page)
                .append(urlId, that.urlId)
                .append(urlParameter, that.urlParameter)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(page)
                .append(urlId)
                .append(urlParameter)
                .toHashCode();
    }

    // ---------- methods ------------------------------------------------------
    public String getUrl() {
        return page.getUrl() + "?p=" + urlId;
    }

    // ---------- getter / setter ----------------------------------------------
    public Long getId() {
        return id;
    }

    public Page getPage() {
        return page;
    }

    public String getUrlId() {
        return urlId;
    }

    public String getUrlParameter() {
        return urlParameter;
    }

}
