package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author evda
 */
@Entity
@NamedQueries({
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.BY_ID, query = "SELECT ub FROM UBzuKovaPhaseImDerivatUndSensorCoC ub WHERE ub.id = :id"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_ALL, query = "SELECT ub FROM UBzuKovaPhaseImDerivatUndSensorCoC ub"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.BY_DERIVAT, query = "SELECT ub FROM UBzuKovaPhaseImDerivatUndSensorCoC ub WHERE ub.kovaImDerivat.derivat = :derivat"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.BY_SENSORCOC_DERIVAT_PHASE, query = "SELECT ub FROM UBzuKovaPhaseImDerivatUndSensorCoC ub WHERE ub.sensorCoc = :sensorCoc AND ub.kovaImDerivat.derivat = :derivat AND ub.kovaImDerivat.kovAPhase = :kovAPhase"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_UB_BY_DERIVAT_PHASE, query = "SELECT DISTINCT u FROM UBzuKovaPhaseImDerivatUndSensorCoC ub INNER JOIN ub.umsetzungsbestaetiger AS u WHERE ub.kovaImDerivat.derivat = :derivat AND ub.kovaImDerivat.kovAPhase = :kovAPhase"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_SENSORCOC_BY_DERIVAT_PHASE, query = "SELECT ub.sensorCoc FROM UBzuKovaPhaseImDerivatUndSensorCoC ub WHERE ub.kovaImDerivat.derivat = :derivat AND ub.kovaImDerivat.kovAPhase = :kovAPhase"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_BY_KOVAIMDERIVAT, query = "SELECT ub FROM UBzuKovaPhaseImDerivatUndSensorCoC ub WHERE ub.kovaImDerivat = :kovAPhaseImDerivat"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_COUNT_BY_KOVAIMDERIVATID_SENSORCOCIDLIST, query = "SELECT COUNT(DISTINCT ub.id) FROM UBzuKovaPhaseImDerivatUndSensorCoC ub INNER JOIN ub.umsetzungsbestaetiger u WHERE ub.kovaImDerivat = :kovAPhaseImDerivat AND ub.sensorCoc IN :sensorCocList"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_COUNT_BY_KOVAIMDERIVAT_SENSORCOCIDLIST, query = "SELECT COUNT(DISTINCT ub.id) FROM UBzuKovaPhaseImDerivatUndSensorCoC ub INNER JOIN ub.umsetzungsbestaetiger u WHERE ub.kovaImDerivat.id = :kovAPhaseImDerivatId AND ub.sensorCoc IN :sensorCocList"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_BY_KOVAIMDERIVAT_SENSORCOC, query = "SELECT ub FROM UBzuKovaPhaseImDerivatUndSensorCoC ub WHERE ub.kovaImDerivat = :kovAPhaseImDerivat AND ub.sensorCoc = :sensorCoc"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_SENSORCOC_BY_MITARBEITER, query = "SELECT DISTINCT ub.sensorCoc FROM UBzuKovaPhaseImDerivatUndSensorCoC ub INNER JOIN ub.umsetzungsbestaetiger AS uub "
                + " INNER JOIN Berechtigung b ON b.fuerId LIKE CAST (ub.sensorCoc.sensorCocId AS varchar) AND b.mitarbeiter = uub "
                + " WHERE uub = :user AND b.rolle = :rolle and b.fuer = :bereZiel"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_KOVAPHASEN_BY_MITARBEITER, query = "SELECT DISTINCT ub.kovaImDerivat FROM UBzuKovaPhaseImDerivatUndSensorCoC ub INNER JOIN ub.umsetzungsbestaetiger AS uub "
                + " WHERE uub = :user"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_FOR_KOVAPHASEIMDERIVAT_AND_STATUS, query = "SELECT DISTINCT ub.sensorCoc FROM UBzuKovaPhaseImDerivatUndSensorCoC ub "
                + " WHERE ub.kovaImDerivat = :kovaImDerivat AND ub.kovaImDerivat.kovaStatus = :status"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_FOR_KOVAPHASEIMDERIVATID_AND_STATUS, query = "SELECT DISTINCT ub.sensorCoc FROM UBzuKovaPhaseImDerivatUndSensorCoC ub "
                + " WHERE ub.kovaImDerivat.id = :kovaImDerivatId AND ub.kovaImDerivat.kovaStatus = :status"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_FOR_KOVAPHASEIMDERIVAT_AND_STATUS_AND_USER, query = "SELECT DISTINCT ub.sensorCoc FROM UBzuKovaPhaseImDerivatUndSensorCoC ub INNER JOIN ub.umsetzungsbestaetiger AS uub "
                + " WHERE uub = :user AND ub.kovaImDerivat = :kovaImDerivat AND ub.kovaImDerivat.kovaStatus = :status"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_SENSORCOCIDS_FOR_KOVAPHASEIMDERIVAT_AND_USER, query = "SELECT DISTINCT ub.sensorCoc.sensorCocId FROM UBzuKovaPhaseImDerivatUndSensorCoC ub INNER JOIN ub.umsetzungsbestaetiger AS uub "
                + " WHERE uub = :user AND ub.kovaImDerivat = :kovAPhaseImDerivat"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_FOR_KOVAPHASEIMDERIVATID_AND_STATUS_AND_USER, query = "SELECT DISTINCT ub.sensorCoc FROM UBzuKovaPhaseImDerivatUndSensorCoC ub INNER JOIN ub.umsetzungsbestaetiger AS uub "
                + " WHERE uub = :user AND ub.kovaImDerivat.id = :kovaImDerivatId AND ub.kovaImDerivat.kovaStatus = :status"),
        @NamedQuery(name = UBzuKovaPhaseImDerivatUndSensorCoC.GET_BY_KOVAIMDERIVAT_AND_UB, query = "SELECT ub FROM UBzuKovaPhaseImDerivatUndSensorCoC ub INNER JOIN ub.umsetzungsbestaetiger AS uub "
                + " WHERE ub.kovaImDerivat = :kovAPhaseImDerivat AND uub = :user")})
public class UBzuKovaPhaseImDerivatUndSensorCoC implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String BY_ID = "getUBById";
    public static final String GET_ALL = "getAllUB";
    public static final String BY_DERIVAT = "getUBByDerivatId";
    public static final String BY_SENSORCOC_DERIVAT_PHASE = "getUBBySensorCocAndDerivatIdAndPhase";
    public static final String GET_UB_BY_DERIVAT_PHASE = "getUmsetzungsbestaetigerByDerivatIdAndPhase";
    public static final String GET_SENSORCOC_BY_DERIVAT_PHASE = "get_sensorcocId_by_derivat_phase";
    public static final String GET_BY_KOVAIMDERIVAT = "getUBByKovaImDerivat";
    public static final String GET_BY_KOVAIMDERIVAT_SENSORCOC = "getUBByKovaImDerivatSensorCoc";
    public static final String GET_COUNT_BY_KOVAIMDERIVATID_SENSORCOCIDLIST = "getUBCountBYKovaImDerivatAndSensorCocIdList";
    public static final String GET_COUNT_BY_KOVAIMDERIVAT_SENSORCOCIDLIST = "getUBCountBYKovaImDerivatIdAndSensorCocIdList";
    public static final String GET_SENSORCOC_BY_MITARBEITER = "getSensorCocByMitarbeiter";
    public static final String GET_KOVAPHASEN_BY_MITARBEITER = "getKovaPhasenByMitarbeiter";
    public static final String GET_FOR_KOVAPHASEIMDERIVAT_AND_STATUS = "getByKovaPhaseImDerivatAndStatus";
    public static final String GET_FOR_KOVAPHASEIMDERIVATID_AND_STATUS = "getByKovaPhaseImDerivatIdAndStatus";
    public static final String GET_FOR_KOVAPHASEIMDERIVAT_AND_STATUS_AND_USER = "getByKovaPhaseImDerivatAndStatusAndUser";
    public static final String GET_SENSORCOCIDS_FOR_KOVAPHASEIMDERIVAT_AND_USER = "getSensorCocIdsByKovaPhaseImDerivatAndUser";
    static final String GET_FOR_KOVAPHASEIMDERIVATID_AND_STATUS_AND_USER = "getByKovaPhaseImDerivatIdAndStatusAndUser";
    static final String GET_BY_KOVAIMDERIVAT_AND_UB = "getUBByKovaImDerivatWithNoUBSet";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "ubzukovaphaseimderivatundsensorcoc_id_seq",
            sequenceName = "ubzukovaphaseimderivatundsensorcoc_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ubzukovaphaseimderivatundsensorcoc_id_seq")
    @Column(name = "id")
    private Long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    private SensorCoc sensorCoc;

    @ManyToOne(cascade = CascadeType.MERGE)
    private KovAPhaseImDerivat kovaImDerivat;

    @ManyToMany(cascade = CascadeType.MERGE)
    private List<Mitarbeiter> umsetzungsbestaetiger;

    // ------------  constructors ----------------------------------------------
    public UBzuKovaPhaseImDerivatUndSensorCoC() {
        this.umsetzungsbestaetiger = new ArrayList<>();
    }

    public UBzuKovaPhaseImDerivatUndSensorCoC(SensorCoc sensorCoc, KovAPhaseImDerivat kovaImDerivat) {
        this.sensorCoc = sensorCoc;
        this.kovaImDerivat = kovaImDerivat;
        this.umsetzungsbestaetiger = new ArrayList<>();
    }

    public UBzuKovaPhaseImDerivatUndSensorCoC(SensorCoc sensorCoc, KovAPhaseImDerivat kovaImDerivat, Mitarbeiter sensorCocLeiter) {
        this.sensorCoc = sensorCoc;
        this.kovaImDerivat = kovaImDerivat;
        this.umsetzungsbestaetiger = Arrays.asList(sensorCocLeiter);
    }

    public UBzuKovaPhaseImDerivatUndSensorCoC(SensorCoc sensorCoc, KovAPhaseImDerivat kovaImDerivat, List<Mitarbeiter> umsetzungsbestaetiger) {
        this.sensorCoc = sensorCoc;
        this.kovaImDerivat = kovaImDerivat;
        this.umsetzungsbestaetiger = umsetzungsbestaetiger;
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return "SensorCoc ID: " + this.sensorCoc.getSensorCocId()
                + ", Derivat ID: " + this.kovaImDerivat.getDerivat().getId()
                + ", KovAPhase:  " + this.kovaImDerivat.getKovAPhase().toString()
                + ", UB: " + this.umsetzungsbestaetiger;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(67, 787);
        hb.append(id);
        hb.append(sensorCoc);
        hb.append(kovaImDerivat);
        hb.append(umsetzungsbestaetiger);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof UBzuKovaPhaseImDerivatUndSensorCoC)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        UBzuKovaPhaseImDerivatUndSensorCoC ub = (UBzuKovaPhaseImDerivatUndSensorCoC) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, ub.id);
        eb.append(sensorCoc, ub.sensorCoc);
        eb.append(kovaImDerivat, kovaImDerivat);
        eb.append(umsetzungsbestaetiger, ub.umsetzungsbestaetiger);
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SensorCoc getSensorCoc() {
        return sensorCoc;
    }

    public void setSensorCoc(SensorCoc sensorCoc) {
        this.sensorCoc = sensorCoc;
    }

    public KovAPhaseImDerivat getKovaImDerivat() {
        return kovaImDerivat;
    }

    public void setKovaImDerivat(KovAPhaseImDerivat kovaImDerivat) {
        this.kovaImDerivat = kovaImDerivat;
    }

    public List<Mitarbeiter> getUmsetzungsbestaetiger() {
        return umsetzungsbestaetiger;
    }

    public void setUmsetzungsbestaetiger(List<Mitarbeiter> umsetzungsbestaetiger) {
        this.umsetzungsbestaetiger = umsetzungsbestaetiger;
    }

    public void addUmsetzungsbestaetiger(Mitarbeiter umsetzungsbestaetiger) {
        this.umsetzungsbestaetiger.add(umsetzungsbestaetiger);
    }

} // end of class
