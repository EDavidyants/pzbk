package de.interfaceag.bmw.pzbk.entities.converter;

import java.util.function.Function;

/**
 * @param <T> dto
 * @param <U> entity
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ToEntityConverter<T, U> {

    private final Function<T, U> fromDto;

    protected ToEntityConverter(Function<T, U> fromDto) {
        this.fromDto = fromDto;
    }

    public final U convertFromDto(final T dto) {
        return fromDto.apply(dto);
    }

}
