package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.shared.objectIds.DomainObject;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQuery(name = Fahrzeugmerkmal.ALL, query = "SELECT f FROM Fahrzeugmerkmal f ORDER BY f.merkmal ASC")
@NamedQuery(name = Fahrzeugmerkmal.BY_SE_TEAM, query = "SELECT fahrzeugmerkmal FROM Fahrzeugmerkmal fahrzeugmerkmal INNER JOIN fahrzeugmerkmal.modulSeTeams modulSeTeam WHERE modulSeTeam.id = :modulSeTeamId ORDER BY fahrzeugmerkmal.merkmal ASC")
public class Fahrzeugmerkmal implements DomainObject {

    private static final String CLASSNAME = "Fahrzeugmerkmal.";
    public static final String ALL = CLASSNAME + "all";
    public static final String BY_SE_TEAM = CLASSNAME + "BY_SE_TEAM";

    @Id
    @SequenceGenerator(name = "fahrzeugmerkmal_id_seq",
            sequenceName = "fahrzeugmerkmal_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fahrzeugmerkmal_id_seq")
    private Long id;

    @Column(length = 1000, nullable = false)
    private String merkmal;

    @ManyToMany
    @JoinTable(name = "fahrzeugmerkmal_modulseteam",
            joinColumns = {@JoinColumn(name = "fahrzeugmerkmal_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "modulseteam_id", referencedColumnName = "id")}
    )
    private List<ModulSeTeam> modulSeTeams;

    public Fahrzeugmerkmal() {
        modulSeTeams = new ArrayList<>();
    }

    public Fahrzeugmerkmal(String merkmal) {
        this.merkmal = merkmal;
        modulSeTeams = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Fahrzeugmerkmal{" +
                "id=" + id +
                ", merkmal='" + merkmal + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        Fahrzeugmerkmal that = (Fahrzeugmerkmal) object;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(merkmal, that.merkmal)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(merkmal)
                .toHashCode();
    }

    @Override
    public Long getId() {
        return id;
    }

    public FahrzeugmerkmalId getFahrzeugmerkmalId() {
        return new FahrzeugmerkmalId(id);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMerkmal() {
        return merkmal;
    }

    public void setMerkmal(String merkmal) {
        this.merkmal = merkmal;
    }

    public List<ModulSeTeam> getModulSeTeams() {
        return modulSeTeams;
    }

    public void setModulSeTeams(List<ModulSeTeam> modulSeTeams) {
        this.modulSeTeams = modulSeTeams;
    }

    public boolean addModulSeTeam(ModulSeTeam modulSeTeam) {
        return modulSeTeams.add(modulSeTeam);
    }

    public void removeModulSeTeam(ModulSeTeam modulSeTeam) {
        modulSeTeams.remove(modulSeTeam);
    }
}
