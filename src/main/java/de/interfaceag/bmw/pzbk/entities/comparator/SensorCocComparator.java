package de.interfaceag.bmw.pzbk.entities.comparator;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author sl
 */
public class SensorCocComparator implements Comparator<SensorCoc>, Serializable {

    private final Boolean fullString;

    /**
     * true compare toString / false compare technologie.
     *
     * @param fullString true compare toString false compare technologie
     */
    public SensorCocComparator(Boolean fullString) {
        this.fullString = fullString;
    }

    @Override
    public int compare(SensorCoc t1, SensorCoc t2) {
        if (t1 != null && t2 != null) {
            String s1;
            String s2;
            if (fullString) {
                s1 = t1.toString();
                s2 = t2.toString();
            } else {
                s1 = t1.getTechnologie();
                s2 = t2.getTechnologie();
            }
            int res = String.CASE_INSENSITIVE_ORDER.compare(s1, s2);
            if (res == 0) {
                res = s1.compareTo(s2);
            }
            return res;
        }
        return -2;
    }
}
