package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.Status;

import java.util.List;
import java.util.Set;

/**
 *
 * @author fn
 */
public interface AnforderungReadData {

    String getFachId();

    Integer getVersion();

    Status getStatus();

    String getBeschreibungAnforderungDe();

    String getBeschreibungAnforderungEn();

    String getKommentarAnforderung();

    Zielwert getZielwert();

    List<AnforderungFreigabeBeiModulSeTeam> getAnfoFreigabeBeiModul(); //

    SensorCoc getSensorCoc();

    Tteam getTteam();

    boolean isPhasenbezug();

    boolean isPotentialStandardisierung();

    List<Anhang> getAnhaenge();

    Bild getBild();

    Set<FestgestelltIn> getFestgestelltIn();

    String getErsteller();

    List<ReferenzSystemLink> getReferenzSystemLinks();

}
