package de.interfaceag.bmw.pzbk.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author evda
 */
@Entity
@Table(name = "anforderung_history")
@NamedQueries({
    @NamedQuery(name = AnforderungHistory.BY_ID, query = "SELECT ah FROM AnforderungHistory ah WHERE ah.id = :id"),
    @NamedQuery(name = AnforderungHistory.GET_BY_ID_AND_KENNZEICHEN, query = "SELECT h FROM AnforderungHistory h WHERE h.anforderungId = :id AND h.kennzeichen = :kennzeichen ORDER BY h.datum DESC"),
    @NamedQuery(name = AnforderungHistory.GET_BY_OBJEKTNAME_ANFORDERUNG_ID, query = "SELECT h FROM AnforderungHistory h WHERE h.anforderungId = :id AND h.kennzeichen = :kennzeichen AND h.objektName LIKE :objektName ORDER BY h.datum ASC")})
public class AnforderungHistory implements Serializable {

    // ------------  queries ---------------------------------------------------
    private static final String CLASSNAME = "AnforderungHistory.";
    public static final String BY_ID = CLASSNAME + "getById";
    public static final String GET_BY_ID_AND_KENNZEICHEN = CLASSNAME + "getByIdAndKennzeichen";
    public static final String GET_BY_OBJEKTNAME_ANFORDERUNG_ID = CLASSNAME + "getByObjektnameAnforderungId";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "anforderunghistory_id_seq",
            sequenceName = "anforderunghistory_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "anforderunghistory_id_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "anforderung_id")
    private long anforderungId;

    @Column(name = "kennzeichen")
    private String kennzeichen;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "datum")
    private Date datum;

    @Column(name = "objekt_name")
    private String objektName;

    @Column(name = "benutzer")
    private String benutzer;

    @Column(name = "von", length = 10000)
    private String von;

    @Column(name = "auf", length = 10000)
    private String auf;

    // ------------  constructors ----------------------------------------------
    public AnforderungHistory() {
        this.kennzeichen = "";
        this.datum = new Date();
        this.objektName = "";
        this.von = "";
        this.auf = "";
        this.benutzer = "";
    }

    public AnforderungHistory(long anforderungId, String kennzeichen, String benutzer) {
        this.anforderungId = anforderungId;
        this.kennzeichen = kennzeichen;
        this.datum = new Date();
        this.benutzer = benutzer;
        if ("M".equals(kennzeichen)) {
            this.objektName = "Meldung";
        } else if ("A".equals(kennzeichen)) {
            this.objektName = "Anforderung";
        } else if ("P".equals(kennzeichen)) {
            this.objektName = "Pzbk";
        } else {
            this.objektName = "";
        }
        this.von = "";
        this.auf = "neu angelegt";
    }

    public AnforderungHistory(long anforderungId, String kennzeichen,
            Date datum, String objektName, String benutzer,
            String von, String auf) {
        this.anforderungId = anforderungId;
        this.kennzeichen = kennzeichen;
        this.datum = datum != null ? new Date(datum.getTime()) : null;
        this.objektName = objektName;
        this.benutzer = benutzer;
        this.von = von;
        this.auf = auf;
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Anforderung ID ").append(this.anforderungId).append(", ");
        sb.append("Kennzeichen ").append(this.kennzeichen).append(" ");
        sb.append("wurde bei ").append(this.objektName).append("' ");
        sb.append("von ").append(this.benutzer).append(" ");
        sb.append("von ").append(this.von).append(" auf ").append(this.auf).append(" ");
        sb.append("am ").append(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(this.datum)).append(" geändert ");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (int) (this.anforderungId ^ (this.anforderungId >>> 32));
        hash = 53 * hash + Objects.hashCode(this.kennzeichen);
        hash = 53 * hash + Objects.hashCode(this.datum);
        hash = 53 * hash + Objects.hashCode(this.objektName);
        hash = 53 * hash + Objects.hashCode(this.benutzer);
        hash = 53 * hash + Objects.hashCode(this.von);
        hash = 53 * hash + Objects.hashCode(this.auf);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnforderungHistory other = (AnforderungHistory) obj;
        if (this.anforderungId != other.anforderungId) {
            return false;
        }
        if (!Objects.equals(this.kennzeichen, other.kennzeichen)) {
            return false;
        }
        if (!Objects.equals(this.objektName, other.objektName)) {
            return false;
        }
        if (!Objects.equals(this.benutzer, other.benutzer)) {
            return false;
        }
        if (!Objects.equals(this.von, other.von)) {
            return false;
        }
        if (!Objects.equals(this.auf, other.auf)) {
            return false;
        }
        return Objects.equals(this.datum, other.datum);
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getAnforderungId() {
        return anforderungId;
    }

    public void setAnforderungId(long anforderungId) {
        this.anforderungId = anforderungId;
    }

    public String getKennzeichen() {
        return kennzeichen;
    }

    public void setKennzeichen(String kennzeichen) {
        this.kennzeichen = kennzeichen;
    }

    public Date getDatum() {
        return new Date(datum.getTime());
    }

    public void setDatum(Date datum) {
        this.datum = new Date(datum.getTime());
    }

    public void setObjektName(String objektName) {
        this.objektName = objektName;
    }

    public String getObjektName() {
        return objektName;
    }

    public String getBenutzer() {
        return benutzer;
    }

    public void setBenutzer(String benutzer) {
        this.benutzer = benutzer;
    }

    public String getVon() {
        return von;
    }

    public void setVon(String von) {
        this.von = von;
    }

    public String getAuf() {
        return auf;
    }

    public void setAuf(String auf) {
        this.auf = auf;
    }

} // end of class
