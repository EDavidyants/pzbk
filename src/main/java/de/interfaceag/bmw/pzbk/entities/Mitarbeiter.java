package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Schauer
 */
@Entity
@NamedQueries( {
        @NamedQuery(name = Mitarbeiter.QUERY_ALL, query = "select m from Mitarbeiter m ORDER BY m.vorname"),
        @NamedQuery(name = Mitarbeiter.QUERY_BY_ID, query = "SELECT m FROM Mitarbeiter m WHERE m.id = :id"),
        @NamedQuery(name = Mitarbeiter.QUERY_BY_QNUMBER, query = "select m from Mitarbeiter m where LOWER(m.qNumber) like LOWER(:qNumber)"),
        @NamedQuery(name = Mitarbeiter.QUERY_BY_MAIL, query = "select m from Mitarbeiter m where LOWER(m.email) like LOWER(:email)"),
        @NamedQuery(name = Mitarbeiter.QUERY_BY_QNUMBER_LIST, query = "SELECT m from Mitarbeiter m WHERE LOWER(m.qNumber) IN :qNumberList"),
        @NamedQuery(name = Mitarbeiter.GET_BY_VORNAME, query = "select m from Mitarbeiter m where m.vorname like :vorname order by m.qNumber"),
        @NamedQuery(name = Mitarbeiter.GET_BY_NACHNAME, query = "select m from Mitarbeiter m where m.nachname like :nachname order by m.qNumber"),
        @NamedQuery(name = Mitarbeiter.SEARCH_MITARBEITER_FOR_NAME_OR_ABTEILUNG, query = "select m from Mitarbeiter m WHERE lower(m.vorname) in :args OR lower(m.nachname) in :args OR lower(m.abteilung) in :args"),
        @NamedQuery(name = Mitarbeiter.SEARCH_MITARBEITER_FOR_NAME_AND_ABTEILUNG_SEPARATED, query = "select m from Mitarbeiter m WHERE lower(m.vorname) LIKE :vorname AND lower(m.nachname) LIKE :nachname AND lower(m.abteilung) LIKE :abteilung"),
        @NamedQuery(name = Mitarbeiter.SEARCH_MITARBEITER_FOR_NAME_AND_ABTEILUNG, query = "select m from Mitarbeiter m WHERE lower(m.vorname) in :args AND lower(m.nachname) in :args AND lower(m.abteilung) in :args"),
        @NamedQuery(name = Mitarbeiter.SEARCH_MITARBEITER_FOR_NAME_AND_OR_ABTEILUNG, query = "select m from Mitarbeiter m WHERE lower(m.vorname) in :args AND (lower(m.nachname) in :args OR lower(m.abteilung) in :args) OR (lower(m.abteilung) in :args AND lower(m.nachname) in :args)"),
        @NamedQuery(name = Mitarbeiter.ALL_ABTEILUNG, query = " select m.abteilung from Mitarbeiter m GROUP BY m.abteilung ")})
public class Mitarbeiter implements Serializable {

    public static final String QUERY_ALL = "all";
    public static final String QUERY_BY_ID = "byId";
    public static final String QUERY_BY_QNUMBER = "byQnumber";
    public static final String QUERY_BY_MAIL = "byMail";
    public static final String QUERY_BY_QNUMBER_LIST = "byQnumberList";
    public static final String GET_BY_VORNAME = "getByVorname";
    public static final String GET_BY_NACHNAME = "getByNachname";
    public static final String SEARCH_MITARBEITER_FOR_NAME_OR_ABTEILUNG = "searchMitarbeiterForNameOrAbteilung";
    public static final String SEARCH_MITARBEITER_FOR_NAME_AND_ABTEILUNG_SEPARATED = "searchMitarbeiterForNameAndAbteilungSeparated";
    public static final String SEARCH_MITARBEITER_FOR_NAME_AND_ABTEILUNG = "searchMitarbeiterForNameAndAbteilung";
    public static final String SEARCH_MITARBEITER_FOR_NAME_AND_OR_ABTEILUNG = "searchMitarbeiterForNameAndOrAbteilung";
    public static final String ALL_ABTEILUNG = "allAbteilung";

    @Id
    @SequenceGenerator(name = "mitarbeiter_id_seq",
            sequenceName = "mitarbeiter_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mitarbeiter_id_seq")
    private Long id;

    private String qNumber;
    private String vorname;
    private String nachname;
    private String kurzzeichen;
    private String tel;
    private String email;
    private String abteilung;
    private String location;

    @Column(name = "mail_receipt_enabled")
    private boolean mailReceiptEnabled = true;

    @ManyToMany
    @JoinTable(name = "mitarbeiter_selectedderivate")
    private List<Derivat> selectedDerivate = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "mitarbeiter_selectedzakderivate")
    private List<Derivat> selectedZakDerivate = new ArrayList<>();

    private boolean isDatenschutzAngenommen;

    // ------------  constructors ----------------------------------------------
    public Mitarbeiter() {
        location = "de";
    }

    public Mitarbeiter(String vorname, String nachname) {
        this.vorname = vorname;
        this.nachname = nachname;
        location = "de";
        isDatenschutzAngenommen = false;
    }

    private Mitarbeiter(Long id) {
        this.id = id;
        isDatenschutzAngenommen = false;
    }

    // Diese Methode dient zu erstellung eines Testanwenders in der Testdatafactory, da gelegentlich eine ID benötigt wird
    public static Mitarbeiter createTestAnwender(String supplement, String qNumber) {
        Mitarbeiter a = new Mitarbeiter(1L);
        a.setEmail("Email_" + supplement);
        a.setKurzzeichen("Kurzzeichen_" + supplement);
        a.setTel("Tel_" + supplement);
        a.setVorname("NachName_" + supplement);
        a.setNachname("VorName_" + supplement);
        a.setQNumber(qNumber);
        a.setLocation("DE");
        return a;
    }

    public String getFullName() {
        return vorname + " " + nachname;
    }

    public String getName() {
        return vorname + " " + nachname;
    }

    @Override
    public String toString() {
        return this.vorname + " " + this.nachname + " | " + this.abteilung;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(17, 67);
        hb.append(id);
        hb.append(vorname);
        hb.append(nachname);
        hb.append(kurzzeichen);
        hb.append(tel);
        hb.append(email);
        hb.append(qNumber);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Mitarbeiter)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        Mitarbeiter employee = (Mitarbeiter) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, employee.id);
        eb.append(qNumber, employee.qNumber);
        eb.append(vorname, employee.vorname);
        eb.append(nachname, employee.nachname);
        eb.append(kurzzeichen, employee.kurzzeichen);
        eb.append(tel, employee.tel);
        eb.append(email, employee.email);
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getKurzzeichen() {
        return kurzzeichen;
    }

    public void setKurzzeichen(String kurzzeichen) {
        this.kurzzeichen = kurzzeichen;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAbteilung() {
        return abteilung;
    }

    public void setAbteilung(String abteilung) {
        this.abteilung = abteilung;
    }

    public String getQNumber() {
        return qNumber;
    }

    public void setQNumber(String qNumber) {
        this.qNumber = qNumber;
    }

    public boolean isMailReceiptEnabled() {
        return mailReceiptEnabled;
    }

    public void setMailReceiptEnabled(boolean mailReceiptEnabled) {
        this.mailReceiptEnabled = mailReceiptEnabled;
    }

    public List<Derivat> getSelectedDerivate() {
        return selectedDerivate;
    }

    public void setSelectedDerivate(List<Derivat> selectedDerivate) {
        this.selectedDerivate = selectedDerivate;
    }

    public List<Derivat> getSelectedZakDerivate() {
        return selectedZakDerivate;
    }

    public void setSelectedZakDerivate(List<Derivat> selectedZakDerivate) {
        this.selectedZakDerivate = selectedZakDerivate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isDatenschutzAngenommen() {
        return isDatenschutzAngenommen;
    }

    public void setDatenschutzAngenommen(boolean datenschutzAngenommen) {
        isDatenschutzAngenommen = datenschutzAngenommen;
    }
}
