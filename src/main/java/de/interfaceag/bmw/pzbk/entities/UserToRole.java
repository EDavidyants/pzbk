package de.interfaceag.bmw.pzbk.entities;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author sl
 */
@Entity
@Table(name = "T_ROLE_USER")
@Cacheable(false)
@NamedQueries({
    @NamedQuery(name = UserToRole.ALL, query = "SELECT to FROM UserToRole to ORDER BY to.id"),
    @NamedQuery(name = UserToRole.ALL_BY_QNUMBER, query = "SELECT to FROM UserToRole to WHERE LOWER(to.username) LIKE LOWER(:qnumber)"),
    @NamedQuery(name = UserToRole.ALL_BY_QNUMBER_LIST, query = "SELECT to FROM UserToRole to WHERE LOWER(to.username) IN :qNumberList"),
    @NamedQuery(name = UserToRole.ALL_QNUMBER_BY_ROLENAME, query = "SELECT ur.username FROM UserToRole ur WHERE ur.rolename = :rolename"),
    @NamedQuery(name = UserToRole.BY_ID, query = "SELECT to FROM UserToRole to WHERE to.id = :id"),
    @NamedQuery(name = UserToRole.BY_QNUMBER_ROLLE, query = "SELECT to FROM UserToRole to WHERE LOWER(to.username) = LOWER(:qnumber) AND to.rolename = :rolename")})
public class UserToRole implements Serializable {

    public static final String ALL = "userToRole.all";
    public static final String ALL_BY_QNUMBER = "userToRole.allByQnumber";
    public static final String ALL_BY_QNUMBER_LIST = "userToRole.allByQnumberList";
    public static final String ALL_QNUMBER_BY_ROLENAME = "userToRole.allByRolename";
    public static final String BY_ID = "userToRole.byId";
    public static final String BY_QNUMBER_ROLLE = "userToRole.byQnumberRole";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String username;
    private String rolename;

    public UserToRole() {
    }

    public UserToRole(String username, String rolename) {
        this.username = username;
        this.rolename = rolename;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getqNumber() {
        return username;
    }

    public void setqNumber(String qNumber) {
        this.username = qNumber;
    }

    public String getRole() {
        return rolename;
    }

    public void setRole(String role) {
        this.rolename = role;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 59 * hash + (this.username != null ? this.username.hashCode() : 0);
        hash = 59 * hash + (this.rolename != null ? this.rolename.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserToRole other = (UserToRole) obj;
        if (!Objects.equals(this.id, other.id) && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.username == null ? other.username != null : !this.username.equals(other.username)) {
            return false;
        }
        return !(this.rolename == null ? other.rolename != null : !this.rolename.equals(other.rolename));
    }

    @Override
    public String toString() {
        return "ValidUser{" + "id=" + id + ", qNumber=" + username + ", roll=" + rolename + '}';
    }

}
