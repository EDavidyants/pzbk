package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.LogLevel;
import de.interfaceag.bmw.pzbk.enums.SystemType;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Entity
@NamedQueries({
    @NamedQuery(name = LogEntry.ALL, query = "SELECT l FROM LogEntry l ORDER BY l.datum DESC"),
    @NamedQuery(name = LogEntry.BY_ID, query = "SELECT l FROM LogEntry l WHERE l.id = :id"),
    @NamedQuery(name = LogEntry.BY_SYSTEM_TYPE, query = "SELECT l FROM LogEntry l WHERE l.systemType = :systemType"),
    @NamedQuery(name = LogEntry.BY_ZAKSST_TIMERANGE, query = "SELECT l FROM LogEntry l WHERE l.systemType = :systemType AND l.datum > :startDate AND l.datum < :endDate ORDER BY l.datum DESC")
})
public class LogEntry implements Serializable {

    private static final String CLASSNAME = "LogEntry.";

    public static final String ALL = CLASSNAME + "all";
    public static final String BY_ID = CLASSNAME + "by_id";
    public static final String BY_SYSTEM_TYPE = CLASSNAME + "by_systemType";
    public static final String BY_ZAKSST_TIMERANGE = CLASSNAME + "by_zaksst_timerange";

    @Id
    @SequenceGenerator(name = "logentry_id_seq",
            sequenceName = "logentry_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "logentry_id_seq")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date datum;

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private LogLevel logLevel;

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private SystemType systemType;

    @Column(length = 10000, nullable = false)
    private String logValue;

    public LogEntry() {
    }

    public LogEntry(LogLevel logLevel, SystemType systemType, String logValue) {
        this.datum = new Date();
        this.logLevel = logLevel;
        this.systemType = systemType;
        this.logValue = logValue;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        LogEntry logEntry = (LogEntry) object;

        return new EqualsBuilder()
                .append(id, logEntry.id)
                .append(datum, logEntry.datum)
                .append(logLevel, logEntry.logLevel)
                .append(systemType, logEntry.systemType)
                .append(logValue, logEntry.logValue)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(datum)
                .append(logLevel)
                .append(systemType)
                .append(logValue)
                .toHashCode();
    }

    // ---------- getter / setter ----------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public LogLevel getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(LogLevel logLevel) {
        this.logLevel = logLevel;
    }

    public String getLogValue() {
        return logValue;
    }

    public void setLogValue(String logValue) {
        this.logValue = logValue;
    }
}
