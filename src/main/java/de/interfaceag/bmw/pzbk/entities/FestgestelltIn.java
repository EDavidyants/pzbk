package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
@Entity
@NamedQueries({
    @NamedQuery(name = FestgestelltIn.QUERY_ALL, query = "SELECT f FROM FestgestelltIn f ORDER BY f.wert"),
    @NamedQuery(name = FestgestelltIn.QUERY_BY_ID, query = "SELECT f FROM FestgestelltIn f WHERE f.id = :id"),
    @NamedQuery(name = FestgestelltIn.QUERY_BY_IDLIST, query = "SELECT f FROM FestgestelltIn f WHERE f.id in :idList"),
    @NamedQuery(name = FestgestelltIn.QUERY_BY_WERT, query = "SELECT f FROM FestgestelltIn f WHERE f.wert = :wert")})
public class FestgestelltIn implements Serializable {

    public static final String QUERY_ALL = "FestgestelltIn.all";
    public static final String QUERY_BY_ID = "FestgestelltIn.byId";
    public static final String QUERY_BY_IDLIST = "FestgestelltIn.byIdList";
    public static final String QUERY_BY_WERT = "FestgestelltIn.byWert";

    @Id
    @SequenceGenerator(name = "festgestelltin_id_seq",
            sequenceName = "festgestelltin_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "festgestelltin_id_seq")
    private Long id;

    @Column(length = 10000)
    private String wert;

    public FestgestelltIn() {
    }

    public FestgestelltIn(String wert) {
        this.wert = wert;
    }

    public FestgestelltIn(Wert wert) {
        this.wert = wert.getWert();
    }

    @Override
    public String toString() {
        return " " + this.wert;
    }

    public FestgestelltIn getCopy() {
        return new FestgestelltIn(this.wert);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        FestgestelltIn that = (FestgestelltIn) object;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(wert, that.wert)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(wert)
                .toHashCode();
    }


    public void setId(Long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }


    public String getWert() {
        return wert;
    }


    public void setWert(String wert) {
        this.wert = wert;
    }
}
