package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author fn
 */
@Entity
public class ZakDoubleUpdate implements Serializable {

    @Id
    @SequenceGenerator(name = "zakdoubleupdate_id_seq",
            sequenceName = "zakdoubleupdate_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "zakdoubleupdate_id_seq")
    private Long id;

    @ManyToOne
    private Derivat derivat;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date updateDate;

    private Long ttooluId;

    private String zakId;

    public ZakDoubleUpdate() {

    }

    public ZakDoubleUpdate(Derivat derivat, Date updateDate, Long ttooluId, String zakId) {
        this.derivat = derivat;
        this.updateDate = updateDate;
        this.ttooluId = ttooluId;
        this.zakId = zakId;
    }

    @Override
    public String toString() {
        return "ZAKDoubleUpdate{" + "id=" + id + ", derivat=" + derivat + ", updateDate=" + updateDate + ", ttooluId=" + ttooluId + ", zakId=" + zakId + '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        ZakDoubleUpdate that = (ZakDoubleUpdate) object;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(derivat, that.derivat)
                .append(updateDate, that.updateDate)
                .append(ttooluId, that.ttooluId)
                .append(zakId, that.zakId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(derivat)
                .append(updateDate)
                .append(ttooluId)
                .append(zakId)
                .toHashCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Derivat getDerivat() {
        return derivat;
    }

    public void setDerivat(Derivat derivat) {
        this.derivat = derivat;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getTtooluId() {
        return ttooluId;
    }

    public void setTtooluId(Long ttooluId) {
        this.ttooluId = ttooluId;
    }

    public String getZakId() {
        return zakId;
    }

    public void setZakId(String zakId) {
        this.zakId = zakId;
    }

}
