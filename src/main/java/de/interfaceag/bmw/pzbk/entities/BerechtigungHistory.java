package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.Rechttype;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * @author evda
 */
@Entity
@Table(name = "berechtigunghistory")
@NamedQuery(name = BerechtigungHistory.BY_ID, query = "SELECT b FROM BerechtigungHistory b WHERE b.id = :id")
@NamedQuery(name = BerechtigungHistory.BY_MITARBEITER, query = "SELECT b FROM BerechtigungHistory b WHERE b.berechtigterId like :berechtigterId ORDER BY b.datum DESC, b.fuerId")
@NamedQuery(name = BerechtigungHistory.BY_MITARBEITER_AND_ROLLE, query = "SELECT b FROM BerechtigungHistory b WHERE b.berechtigterId like :berechtigterId AND b.rolle like :rolle ORDER BY b.datum DESC, b.fuerId")
@NamedQuery(name = BerechtigungHistory.BY_MITARBEITER_ROLLE_ZIEL, query = "SELECT b FROM BerechtigungHistory b WHERE b.berechtigterId like :berechtigterId AND b.rolle like :rolle AND b.fuer like :fuer AND b.rechttype != :rechttype ORDER BY b.datum DESC, b.fuerId")
@NamedQuery(name = BerechtigungHistory.GET_MITARBEITERQN_BY_ROLLE_ZIEL_FUERID_RECHTTYPE, query = "SELECT b.berechtigterId FROM BerechtigungHistory b WHERE b.rolle LIKE :rolle AND b.fuer LIKE :fuer AND b.fuerId LIKE :fuerId AND b.rechttype = :rechttype AND b.erteilt = true ORDER BY b.datum DESC")
@NamedQuery(name = BerechtigungHistory.GET_MITARBEITERQN_BY_ROLLE_ZIEL_FUERID_RECHTTYPE_EXCLUSIVE_ONE_MITARBEITER, query = "SELECT b.berechtigterId FROM BerechtigungHistory b WHERE b.rolle LIKE :rolle AND b.fuer LIKE :fuer AND b.fuerId LIKE :fuerId AND b.rechttype = :rechttype AND b.erteilt = true AND b.berechtigterId NOT LIKE :exclMitarbeiter ORDER BY b.datum DESC")
public class BerechtigungHistory implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String BY_ID = "getBerechtigungHistoryById";
    public static final String BY_MITARBEITER = "getBerechtigungHistoryByMitarbeiter";
    public static final String BY_MITARBEITER_AND_ROLLE = "getBerechtigungHistoryByMitarbeiterAndRolle";
    public static final String BY_MITARBEITER_ROLLE_ZIEL = "getBerechtigungHistoryByMitarbeiterAndRolleAndZiel";
    public static final String GET_MITARBEITERQN_BY_ROLLE_ZIEL_FUERID_RECHTTYPE = "getMitarbeiterQN_by_rolle_ziel_fuerId_rechttype";
    public static final String GET_MITARBEITERQN_BY_ROLLE_ZIEL_FUERID_RECHTTYPE_EXCLUSIVE_ONE_MITARBEITER = "getMitarbeiterQN_by_rolle_ziel_fuerId_rechttype_exclusive_one_mitarbeiter";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "berechtigunghistory_id_seq",
            sequenceName = "berechtigunghistory_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "berechtigunghistory_id_seq")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Datum")
    Date datum;

    @Column(name = "berechtigt_von_qnumber")
    private String zustaendigerId;

    @Column(name = "berechtigt_von")
    private String zustaendiger;

    @Column(name = "berechtigter_qnumber")
    private String berechtigterId;

    @Column(name = "berechtigter")
    private String berechtigter;

    @Column(name = "rolle")
    private String rolle;

    @Column(name = "ziel_id")
    private String fuerId;

    @Column(name = "fuer")
    private String fuer;

    @Column(name = "erteilt")
    private boolean erteilt;

    @Column(name = "rechttype")
    private Rechttype rechttype;

    // ------------  constructors ----------------------------------------------
    public BerechtigungHistory() {

    }

    public BerechtigungHistory(String zustaendigerId, String zustaendiger, String berechtigterId, String berechtigter, String rolle, String fuerId, String fuer, boolean erteilt, Rechttype rechttype) {
        this.zustaendigerId = zustaendigerId;
        this.zustaendiger = zustaendiger;
        this.berechtigterId = berechtigterId;
        this.berechtigter = berechtigter;
        this.rolle = rolle;
        this.fuerId = fuerId;
        this.fuer = fuer;
        this.erteilt = erteilt;
        this.rechttype = rechttype;
        this.datum = new Date();
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        String action;
        if (this.erteilt) {
            action = "zugewiesen";
        } else {
            action = "entzogen";
        }

        return " " + this.zustaendiger + " hat dem "
                + this.berechtigter + " die Rolle "
                + this.rolle + " für " + this.fuer
                + " " + action;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(29, 313);
        hb.append(id);
        hb.append(zustaendigerId);
        hb.append(zustaendiger);
        hb.append(berechtigterId);
        hb.append(berechtigter);
        hb.append(rolle);
        hb.append(fuerId);
        hb.append(fuer);
        hb.append(erteilt);
        hb.append(rechttype);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof BerechtigungHistory)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        BerechtigungHistory bh = (BerechtigungHistory) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, bh.id);
        eb.append(zustaendigerId, bh.zustaendigerId);
        eb.append(zustaendiger, bh.zustaendiger);
        eb.append(berechtigterId, bh.berechtigterId);
        eb.append(berechtigter, bh.berechtigter);
        eb.append(rolle, bh.rolle);
        eb.append(fuerId, bh.fuerId);
        eb.append(fuer, bh.fuer);
        eb.append(erteilt, bh.erteilt);
        eb.append(rechttype, bh.rechttype);

        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------

    /**
     * @return id Long
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id Long
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return datum Date
     */
    public Date getDatum() {
        return new Date(datum.getTime());
    }

    /**
     * @param datum Date
     */
    public void setDatum(Date datum) {
        this.datum = new Date(datum.getTime());
    }

    /**
     * @return zustaendigerId String (qnumber)
     */
    public String getZustaendigerId() {
        return zustaendigerId;
    }

    /**
     * @param zustaendigerId String (qnumber)
     */
    public void setZustaendigerId(String zustaendigerId) {
        this.zustaendigerId = zustaendigerId;
    }

    /**
     * @return zustaendiger String - Person, die eine Rolle zuweist
     */
    public String getZustaendiger() {
        return zustaendiger;
    }

    /**
     * @param zustaendiger String
     */
    public void setZustaendiger(String zustaendiger) {
        this.zustaendiger = zustaendiger;
    }

    /**
     * @return berechtigterId String (qnumber)
     */
    public String getBerechtigterId() {
        return berechtigterId;
    }

    /**
     * @param berechtigterId String (qnumber)
     */
    public void setBerechtigterId(String berechtigterId) {
        this.berechtigterId = berechtigterId;
    }

    /**
     * @return berechtigter String - Person, der eine Rolle zugewiesen wird
     */
    public String getBerechtigter() {
        return berechtigter;
    }

    /**
     * @param berechtigter String
     */
    public void setBerechtigter(String berechtigter) {
        this.berechtigter = berechtigter;
    }

    /**
     * @return rolle String
     */
    public String getRolle() {
        return rolle;
    }

    /**
     * @param rolle String
     */
    public void setRolle(String rolle) {
        this.rolle = rolle;
    }

    /**
     * @return fuerId String
     */
    public String getFuerId() {
        return fuerId;
    }

    /**
     * @param fuerId String
     */
    public void setFuerId(String fuerId) {
        this.fuerId = fuerId;
    }

    /**
     * @return fuer String - die Bezeichnung von der betroffenen SensorCoC bzw.
     * dem betroffenen Modul
     */
    public String getFuer() {
        return fuer;
    }

    /**
     * @param fuer String
     */
    public void setFuer(String fuer) {
        this.fuer = fuer;
    }

    /**
     * @return erteilt boolean - true falls die Rolle erteilt wurde, false falls
     * die Rolle entzogen wurde
     */
    public boolean isErteilt() {
        return erteilt;
    }

    /**
     * @param erteilt boolean
     */
    public void setErteilt(boolean erteilt) {
        this.erteilt = erteilt;
    }

    /**
     * @return rechttype Rechttype - leserecht, schreibrecht oder kein recht
     */
    public Rechttype getRechttype() {
        return rechttype;
    }

    /**
     * @param rechttype Rechttype
     */
    public void setRechttype(Rechttype rechttype) {
        this.rechttype = rechttype;
    }
}
