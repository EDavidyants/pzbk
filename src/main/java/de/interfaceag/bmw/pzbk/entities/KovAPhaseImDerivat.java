package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PostLoad;
import javax.persistence.PostUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author evda
 */
@Entity
@Table(name = "kov_a_phase_im_derivat")
@NamedQueries( {
        @NamedQuery(name = KovAPhaseImDerivat.ALL, query = "SELECT kova FROM KovAPhaseImDerivat kova"),
        @NamedQuery(name = KovAPhaseImDerivat.ALL_KONF, query = "SELECT kova FROM KovAPhaseImDerivat kova WHERE kova.kovaStatus != :kovAStatus"),
        @NamedQuery(name = KovAPhaseImDerivat.BY_ID, query = "SELECT kova FROM KovAPhaseImDerivat kova WHERE kova.id = :id"),
        @NamedQuery(name = KovAPhaseImDerivat.BY_ID_FETCH, query = "SELECT kova FROM KovAPhaseImDerivat kova INNER JOIN FETCH kova.derivat WHERE kova.id = :id"),
        @NamedQuery(name = KovAPhaseImDerivat.BY_KOVASTATUS, query = "SELECT kova.id FROM KovAPhaseImDerivat kova WHERE kova.kovaStatus = :kovaStatus"),
        @NamedQuery(name = KovAPhaseImDerivat.BY_DERIVAT, query = "SELECT kova FROM KovAPhaseImDerivat kova WHERE kova.derivat = :derivat"),
        @NamedQuery(name = KovAPhaseImDerivat.BY_DERIVAT_FETCH, query = "SELECT kova FROM KovAPhaseImDerivat kova INNER JOIN FETCH kova.derivat WHERE kova.derivat = :derivat"),
        @NamedQuery(name = KovAPhaseImDerivat.BY_DERID_KONF, query = "SELECT kova FROM KovAPhaseImDerivat kova WHERE kova.derivat.id = :deriId AND kova.kovaStatus != :kovAStatus ORDER BY kova.id"),
        @NamedQuery(name = KovAPhaseImDerivat.BY_DERIVATLIST_KONF, query = "SELECT kova FROM KovAPhaseImDerivat kova WHERE kova.derivat IN :derivatList AND kova.kovaStatus != :kovAStatus"),
        @NamedQuery(name = KovAPhaseImDerivat.BY_DERIVAT_PHASE, query = "SELECT kova FROM KovAPhaseImDerivat kova WHERE kova.derivat = :derivat AND kova.kovAPhase = :kovAPhase"),
        @NamedQuery(name = KovAPhaseImDerivat.START_AND_END_SET, query = "SELECT kova FROM KovAPhaseImDerivat kova WHERE kova.startDate IS NOT NULL AND kova.endDate IS NOT NULL ORDER BY kova.id")})
public class KovAPhaseImDerivat implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String ALL = "kovaImDerivat.all";
    public static final String ALL_KONF = "kovaImDerivat.all_konf";
    public static final String BY_ID = "kovaImDerivat.by_id";
    public static final String BY_ID_FETCH = "kovaImDerivat.by_id_fetch";
    public static final String BY_KOVASTATUS = "kovaImDerivat.by_kovastatus";
    public static final String BY_DERIVAT = "kovaImDerivat.by_derivat";
    public static final String BY_DERIVAT_FETCH = "kovaImDerivat.by_did_fetch";
    public static final String BY_DERIVATLIST_KONF = "kovaImDerivat.by_derivatListKonf";
    public static final String BY_DERID_KONF = "kovaImDerivat.by_did_konf";
    public static final String BY_DERIVAT_PHASE = "kovaImDerivat.by_derivat_phase";
    public static final String START_AND_END_SET = "kovaImDerivat.startAndEndSet";
    public static final String GET_OFFENE = "kovaImDerivat.getOffene";

    // ------------  fields ----------------------------------------------------
    @Transient
    private transient KovAPhaseImDerivat savedKovaImDerivat;

    @PostLoad
    @PostUpdate
    public void saveState() {
        this.savedKovaImDerivat = (KovAPhaseImDerivat) SerializationUtils.clone(this);
    }

    public KovAPhaseImDerivat getSavedKovaImDerivat() {
        return savedKovaImDerivat;
    }

    @Id
    @SequenceGenerator(name = "kovaphaseimderivat_id_seq",
            sequenceName = "kovaphaseimderivat_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kovaphaseimderivat_id_seq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "derivat_id", insertable = false, updatable = false)
    private Derivat derivat;

    private Integer kovAPhase;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Mitarbeiter umsetzungsbestaetiger;

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    private KovAStatus kovaStatus;

    // ------------  constructors ----------------------------------------------
    public KovAPhaseImDerivat() {

    }

    public KovAPhaseImDerivat(Derivat derivat, KovAPhase kovAPhase) {
        this.derivat = derivat;
        this.kovAPhase = kovAPhase.getId();
        this.kovaStatus = KovAStatus.OFFEN;
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return "Derivat ID: " + derivat.getId() + ", KovAPhase: " + kovAPhase.toString();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(181, 701);
        hb.append(id);
        hb.append(derivat);
        hb.append(kovAPhase);
        hb.append(umsetzungsbestaetiger);
        hb.append(kovaStatus);

        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof KovAPhaseImDerivat)) {
            return false;
        }

        if (object == this) {
            return true;
        }

        KovAPhaseImDerivat kova = (KovAPhaseImDerivat) object;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, kova.id);
        eb.append(derivat, kova.derivat);
        eb.append(kovAPhase, kova.kovAPhase);
        eb.append(umsetzungsbestaetiger, kova.umsetzungsbestaetiger);
        eb.append(kovaStatus, kova.kovaStatus);

        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Derivat getDerivat() {
        return derivat;
    }

    public void setDerivat(Derivat derivat) {
        this.derivat = derivat;
    }

    public KovAPhase getKovAPhase() {
        if (kovAPhase != null) {
            return KovAPhase.getById(kovAPhase).orElse(null);
        } else {
            return null;
        }

    }

    public void setKovAPhase(KovAPhase kovAPhase) {
        if (kovAPhase == null) {
            this.kovAPhase = null;
        } else {
            this.kovAPhase = kovAPhase.getId();
        }
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Mitarbeiter getUmsetzungsbestaetiger() {
        return umsetzungsbestaetiger;
    }

    public void setUmsetzungsbestaetiger(Mitarbeiter umsetzungsbestaetiger) {
        this.umsetzungsbestaetiger = umsetzungsbestaetiger;
    }

    public KovAStatus getKovaStatus() {
        return kovaStatus;
    }

    public void setKovaStatus(KovAStatus kovaStatus) {
        this.kovaStatus = kovaStatus;
    }

}
