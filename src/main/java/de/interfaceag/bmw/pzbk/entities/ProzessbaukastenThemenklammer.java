package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 * @author evda
 */
@Entity
public class ProzessbaukastenThemenklammer implements Serializable {

    @Id
    @SequenceGenerator(name = "prozessbaukasten_themenklammer_id_seq",
            sequenceName = "prozessbaukasten_themenklammer_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prozessbaukasten_themenklammer_id_seq")
    private Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "prozessbaukasten_id", nullable = false)
    private Prozessbaukasten prozessbaukasten;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "themenklammer_id", nullable = false)
    private Themenklammer themenklammer;

    public ProzessbaukastenThemenklammer() {

    }

    public ProzessbaukastenThemenklammer(Prozessbaukasten prozessbaukasten, Themenklammer themenklammer) {
        this.prozessbaukasten = prozessbaukasten;
        this.themenklammer = themenklammer;
    }

    public ProzessbaukastenThemenklammer copy() {
        ProzessbaukastenThemenklammer clone = new ProzessbaukastenThemenklammer();
        clone.setProzessbaukasten(this.getProzessbaukasten());
        clone.setThemenklammer(this.getThemenklammer());
        return clone;
    }

    @Override
    public String toString() {
        return "ProzessbaukastenThemenklammer ID: " + id + ", Prozessbaukasten: " + prozessbaukasten.getBezeichnung() + ", Themenklammer: " + themenklammer.getBezeichnung();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hashCodeBuilder = new HashCodeBuilder(83, 773);
        hashCodeBuilder.append(id);
        hashCodeBuilder.append(prozessbaukasten);
        hashCodeBuilder.append(themenklammer);
        return hashCodeBuilder.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProzessbaukastenThemenklammer other = (ProzessbaukastenThemenklammer) obj;
        EqualsBuilder equalsBuilder = new EqualsBuilder();
        equalsBuilder.append(id, other.id);
        equalsBuilder.append(prozessbaukasten, other.prozessbaukasten);
        equalsBuilder.append(themenklammer, other.themenklammer);
        return equalsBuilder.isEquals();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Prozessbaukasten getProzessbaukasten() {
        return prozessbaukasten;
    }

    public void setProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        this.prozessbaukasten = prozessbaukasten;
    }

    public Themenklammer getThemenklammer() {
        return themenklammer;
    }

    public void setThemenklammer(Themenklammer themenklammer) {
        this.themenklammer = themenklammer;
    }

}
