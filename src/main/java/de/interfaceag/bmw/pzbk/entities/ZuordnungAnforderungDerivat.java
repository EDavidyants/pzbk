package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Entity
@Table(name = "zuordnunganforderungderivat", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"anforderung_id", "derivat_id"})})
@NamedQueries( {
        @NamedQuery(name = ZuordnungAnforderungDerivat.ALL, query = "SELECT zad FROM ZuordnungAnforderungDerivat zad"),
        @NamedQuery(name = ZuordnungAnforderungDerivat.GET_BY_ANFORDERUNG_DERIVAT, query = "SELECT z FROM ZuordnungAnforderungDerivat z "
                + "WHERE z.anforderung = :anforderung AND z.derivat = :derivat"),
        @NamedQuery(name = ZuordnungAnforderungDerivat.GET_BY_ANFORDERUNG, query = "SELECT z FROM ZuordnungAnforderungDerivat z "
                + "WHERE z.anforderung = :anforderung ORDER BY z.derivat.name"),
        @NamedQuery(name = ZuordnungAnforderungDerivat.GET_DERIVAT_BY_ANFORDERUNG, query = "SELECT z.derivat FROM ZuordnungAnforderungDerivat z "
                + "WHERE z.anforderung = :anforderung"),
        @NamedQuery(name = ZuordnungAnforderungDerivat.GET_DERIVAT_BY_ANFORDERUNG_LIST, query = "SELECT z.derivat FROM ZuordnungAnforderungDerivat z "
                + "WHERE z.anforderung IN :anforderungList"),
        @NamedQuery(name = ZuordnungAnforderungDerivat.GET_DERIVAT_BY_ID_LIST, query = "SELECT z FROM ZuordnungAnforderungDerivat z "
                + "WHERE z.id IN :idList"),
        @NamedQuery(name = ZuordnungAnforderungDerivat.GET_BY_ANFORDERUNGLIST_DERIVATLIST, query = "SELECT z FROM ZuordnungAnforderungDerivat z "
                + "INNER JOIN FETCH z.anforderung "
                + "INNER JOIN FETCH z.derivat "
                + "LEFT JOIN FETCH z.anforderung.anfoFreigabeBeiModul "
                + "WHERE z.anforderung.id IN :anforderungList AND z.derivat.id IN :derivatList"),
        @NamedQuery(name = ZuordnungAnforderungDerivat.GET_BY_DERIVAT, query = "SELECT z FROM ZuordnungAnforderungDerivat z "
                + "WHERE z.derivat = :derivat"),
        @NamedQuery(name = ZuordnungAnforderungDerivat.GET_BY_ANFORDERUNG_DERIVATIDS, query = "SELECT z FROM ZuordnungAnforderungDerivat z "
                + "WHERE z.anforderung = :anforderung AND z.derivat.id IN :derivatIds ORDER BY z.derivat.name")
})
public class ZuordnungAnforderungDerivat implements Serializable {

    public static final String CLASSNAME = "zuordnungAnforderungDerivat.";

    public static final String ALL = CLASSNAME + "all";
    public static final String GET_BY_ANFORDERUNG = CLASSNAME + "getByAnforderung";
    public static final String GET_DERIVAT_BY_ANFORDERUNG = CLASSNAME + "getDerivatByAnforderung";
    public static final String GET_DERIVAT_BY_ANFORDERUNG_LIST = CLASSNAME + "getDerivatByAnforderungList";
    public static final String GET_BY_ANFORDERUNG_DERIVAT = CLASSNAME + "getByAnforderungDerivat";
    public static final String GET_BY_ANFORDERUNGLIST_DERIVATLIST = CLASSNAME + "getByAnforderungListDerivatList";
    public static final String GET_BY_DERIVAT = CLASSNAME + "getByDerivat";
    public static final String GET_DERIVAT_BY_ID_LIST = CLASSNAME + "getByIdList";
    public static final String GET_BY_ANFORDERUNG_DERIVATIDS = CLASSNAME + "getByAnforderungAndDerivatList";

    @Id
    @SequenceGenerator(name = "zuordnunganforderungderivat_id_seq",
            sequenceName = "zuordnunganforderungderivat_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "zuordnunganforderungderivat_id_seq")
    private Long id;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Anforderung anforderung;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Derivat derivat;

    @Column(nullable = false)
    private Integer status;

    @Column(nullable = false)
    private Boolean nachZakUebertragen = Boolean.FALSE;

    private Boolean zakUebertragungErfolgreich;

    private Boolean zurKenntnisGenommen = Boolean.FALSE;

    private Integer zuordnungDerivatStatus;

    public ZuordnungAnforderungDerivat() {
    }

    public ZuordnungAnforderungDerivat(Anforderung anforderung, Derivat derivat, ZuordnungStatus status) {
        this.anforderung = anforderung;
        this.derivat = derivat;
        this.status = status.getId();
    }

    @Override
    public String toString() {
        return "Zuordnung Anforderung " + this.anforderung.getFachId() + " zu Derivat" + this.derivat.getName();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(43, 151);
        hb.append(this.id);
        hb.append(this.anforderung);
        hb.append(this.derivat);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ZuordnungAnforderungDerivat)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        ZuordnungAnforderungDerivat df = (ZuordnungAnforderungDerivat) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, df.id);
        eb.append(anforderung, df.anforderung);
        eb.append(derivat, df.derivat);
        return eb.isEquals();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Anforderung getAnforderung() {
        return anforderung;
    }

    public void setAnforderung(Anforderung anforderung) {
        this.anforderung = anforderung;
    }

    public Derivat getDerivat() {
        return derivat;
    }

    public void setDerivat(Derivat derivat) {
        this.derivat = derivat;
    }

    public ZuordnungStatus getStatus() {
        return ZuordnungStatus.getById(status).orElse(null);
    }

    public void setStatus(ZuordnungStatus status) {
        if (status == null) {
            this.status = null;
        } else {
            this.status = status.getId();
        }
    }

    public Boolean isNachZakUebertragen() {
        return nachZakUebertragen;
    }

    public void setNachZakUebertragen(Boolean nachZakUebertragen) {
        this.nachZakUebertragen = nachZakUebertragen;
    }

    public Boolean isZakUebertragungErfolgreich() {
        return zakUebertragungErfolgreich;
    }

    public void setZakUebertragungErfolgreich(Boolean zakUebertragungErfolgreich) {
        this.zakUebertragungErfolgreich = zakUebertragungErfolgreich;
    }

    public Boolean isZurKenntnisGenommen() {
        return zurKenntnisGenommen;
    }

    public void setZurKenntnisGenommen(Boolean zurKenntnisGenommen) {
        this.zurKenntnisGenommen = zurKenntnisGenommen;
    }

    public DerivatStatus getZuordnungDerivatStatus() {
        return DerivatStatus.getById(zuordnungDerivatStatus);
    }

    public void setZuordnungDerivatStatus(DerivatStatus zuordnungStatus) {
        if (zuordnungStatus != null) {
            this.zuordnungDerivatStatus = zuordnungStatus.getId();
        } else {
            this.zuordnungDerivatStatus = null;
        }
    }

}
