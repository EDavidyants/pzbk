package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author fp
 */
@Entity
@NamedQueries( {
        @NamedQuery(name = DerivatAnforderungModulHistory.BY_ID, query = "SELECT dah FROM DerivatAnforderungModulHistory dah WHERE dah.id = :id"),
        @NamedQuery(name = DerivatAnforderungModulHistory.BY_DERIVATANFORDERUNGMODUL, query = "SELECT dah FROM DerivatAnforderungModulHistory dah WHERE dah.derivatAnforderungModul = :da ORDER BY dah.datum DESC"),
        @NamedQuery(name = DerivatAnforderungModulHistory.BY_DERIVATANFORDERUNGMODUL_ID, query = "SELECT dah FROM DerivatAnforderungModulHistory dah WHERE dah.derivatAnforderungModul.id = :id ORDER BY dah.datum DESC")})
public class DerivatAnforderungModulHistory implements Serializable {

    private static final String CLASSNAME = "DerivatAnforderungModulHistory.";

    public static final String BY_ID = CLASSNAME + "by_id";
    public static final String BY_DERIVATANFORDERUNGMODUL = CLASSNAME + "by_derivatAnforderungModul";
    public static final String BY_DERIVATANFORDERUNGMODUL_ID = CLASSNAME + "by_derivatAnforderungModulId";

    @Id
    @SequenceGenerator(name = "derivatanforderungmodulhistory_id_seq",
            sequenceName = "derivatanforderungmodulhistory_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "derivatanforderungmodulhistory_id_seq")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private DerivatAnforderungModul derivatAnforderungModul;

    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    private Date datum;

    private String bearbeiter;

    @Column(length = 10000)
    private String kommentar;

    public DerivatAnforderungModulHistory() {
    }

    public DerivatAnforderungModulHistory(DerivatAnforderungModul derivatAnforderungModul, String status, Date datum, String bearbeiter, String kommentar) {
        this.derivatAnforderungModul = derivatAnforderungModul;
        this.status = status;
        this.datum = datum;
        this.bearbeiter = bearbeiter;
        this.kommentar = kommentar;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        DerivatAnforderungModulHistory that = (DerivatAnforderungModulHistory) object;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(derivatAnforderungModul, that.derivatAnforderungModul)
                .append(status, that.status)
                .append(datum, that.datum)
                .append(bearbeiter, that.bearbeiter)
                .append(kommentar, that.kommentar)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(derivatAnforderungModul)
                .append(status)
                .append(datum)
                .append(bearbeiter)
                .append(kommentar)
                .toHashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Derivat derivat = derivatAnforderungModul.getDerivat();
        Anforderung anfo = derivatAnforderungModul.getAnforderung();
        Modul modul = derivatAnforderungModul.getModul();
        sb.append(derivat.getName()).append(" ");
        sb.append(anfo.getFachId()).append(" V").append(anfo.getVersion()).append(" ");
        sb.append(modul.getName()).append(" | ");
        sb.append(getKommentarDetails());
        return sb.toString();
    }

    public String getKommentarDetails() {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat toformat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        String formatedDate = toformat.format(datum);
        sb.append(formatedDate).append(" ").append(bearbeiter).append(": ").append(kommentar);
        return sb.toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public DerivatAnforderungModul getDerivatAnforderungModul() {
        return derivatAnforderungModul;
    }


    public void setDerivatAnforderungModul(DerivatAnforderungModul derivatAnforderungModul) {
        this.derivatAnforderungModul = derivatAnforderungModul;
    }


    public String getStatus() {
        return status;
    }


    public void setStatus(String status) {
        this.status = status;
    }


    public Date getDatum() {
        return datum;
    }


    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getBearbeiter() {
        return bearbeiter;
    }

    public void setBearbeiter(String bearbeiter) {
        this.bearbeiter = bearbeiter;
    }


    public String getKommentar() {
        return kommentar;
    }


    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

}
