package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author evda
 */
@Entity
@Table(name = "kovaphase")
@NamedQueries( {
        @NamedQuery(name = KovAPhaseDef.GET_ALL, query = "SELECT kova FROM KovAPhaseDef kova ORDER BY kova.code"),
        @NamedQuery(name = KovAPhaseDef.GET_BY_ID, query = "SELECT kova FROM KovAPhaseDef kova WHERE kova.id = :id"),
        @NamedQuery(name = KovAPhaseDef.GET_BEZEICHNUNG_BY_CODE, query = "SELECT kova.bezeichnung FROM KovAPhaseDef kova WHERE kova.code = :code"),
        @NamedQuery(name = KovAPhaseDef.GET_NEXT_KOVAPHASE_CODE, query = "SELECT max(kova.code) FROM KovAPhaseDef kova"),
        @NamedQuery(name = KovAPhaseDef.GET_BY_BEZEICHNUNG, query = "SELECT kova FROM KovAPhaseDef kova WHERE kova.bezeichnung LIKE :bezeichnung"),
        @NamedQuery(name = KovAPhaseDef.GET_BY_CODE, query = "SELECT kova FROM KovAPhaseDef kova WHERE kova.code = :code")})
public class KovAPhaseDef implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String GET_ALL = "KovAPhase.get_all";
    public static final String GET_BY_ID = "KovAPhase.get_byId";
    public static final String GET_BEZEICHNUNG_BY_CODE = "KovAPhase.get_bezeichnung_byCode";
    public static final String GET_NEXT_KOVAPHASE_CODE = "KovAPhase.get_next_KovAPhase_Code";
    public static final String GET_BY_BEZEICHNUNG = "KovAPhase.get_byBezeichnung";
    public static final String GET_BY_CODE = "KovAPhase.get_byCode";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "kovaphase_id_seq",
            sequenceName = "kovaphase_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kovaphase_id_seq")
    @Column(name = "id")
    private Long id;

    private Integer code;
    private String bezeichnung;

    // ------------  constructors ----------------------------------------------
    public KovAPhaseDef() {
    }


    @Override
    public String toString() {
        return bezeichnung;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(83, 617);
        hb.append(id);
        hb.append(code);
        hb.append(bezeichnung);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof KovAPhaseDef)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        KovAPhaseDef kova = (KovAPhaseDef) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, kova.id);
        eb.append(code, kova.code);
        eb.append(bezeichnung, kova.bezeichnung);
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

} // end of class
