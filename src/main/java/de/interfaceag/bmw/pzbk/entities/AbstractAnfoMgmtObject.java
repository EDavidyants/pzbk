package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.Status;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.TemporalType.TIMESTAMP;

/**
 *
 * @author fp
 */
@MappedSuperclass
@Cacheable(false)
public abstract class AbstractAnfoMgmtObject implements Serializable {

    protected String fachId;

    @Enumerated(EnumType.ORDINAL)
    protected Status status;

    @Column(length = 10000)
    protected String statusWechselKommentar;

    @Enumerated(EnumType.ORDINAL)
    protected Status nextStatus;

    protected boolean statusChangeRequested;

    @Temporal(TIMESTAMP)
    protected Date erstellungsdatum;

    @Temporal(TIMESTAMP)
    protected Date aenderungsdatum;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Anhang> anhaenge;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    protected Bild bild;

    @ManyToOne(fetch = FetchType.LAZY)
    protected SensorCoc sensorCoc;

    public AbstractAnfoMgmtObject() {
        anhaenge = new ArrayList<>();
    }

    @Override
    public String toString() {
        return getFachId();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(53, 97);
        hb.append(this.getId());
        hb.append(this.getKennzeichen());
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AbstractAnfoMgmtObject)) {
            return false;
        }
        AbstractAnfoMgmtObject a = (AbstractAnfoMgmtObject) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(this.getId(), a.getId());
        eb.append(this.getKennzeichen(), a.getKennzeichen());
        return eb.isEquals();
    }

    public abstract String getKennzeichen();

    public abstract Long getId();

    public abstract void setId(Long id);

    public boolean hasStatus(String status) {
        return status.equals(getStatus().name());
    }

    public boolean hasStatus(Status status) {
        return status.equals(getStatus());
    }

    public boolean isStatusEqualOrHigher(Status status) {
        return isStatusHigherThan(status) || isStatusEqual(status);
    }

    public boolean isStatusHigherThan(Status status) {
        return getStatus().compareTo(status) > 0;
    }

    public boolean isStatusEqual(Status status) {
        return getStatus().compareTo(status) == 0;
    }

    public boolean isStatusLowerThan(Status status) {
        return getStatus().compareTo(status) < 0;
    }

    public boolean isStatusLowerThanById(int id) {
        return isStatusLowerThan(Status.getStatusById(id));
    }

    public boolean isStatusLowerThanByName(String status) {
        Status s = Status.getStatusByBezeichnung(status);
        return isStatusLowerThan(s);
    }

    public boolean isStatusHigherThanById(int id) {
        return isStatusHigherThan(Status.getStatusById(id));
    }

    public boolean isStatusHigherThan(String status) {
        return isStatusHigherThan(Status.getStatusByBezeichnung(status));
    }

    public boolean isStatusEqualOrHigherById(int id) {
        return isStatusEqualOrHigher(Status.getStatusById(id));
    }

    public boolean isStatusEqualOrHigherByName(String status) {
        return isStatusEqualOrHigher(Status.getStatusByBezeichnung(status));
    }

    public boolean isStatusEqualById(int id) {
        return isStatusEqual(Status.getStatusById(id));
    }

    public boolean isStatusEqualByName(String status) {
        return isStatusEqual(Status.getStatusByBezeichnung(status));
    }

    public void addAnhang(Anhang anhang) {
        anhaenge.add(anhang);
    }

    public void removeAnhang(Anhang anhang) {
        if (anhaenge != null) {
            anhaenge.remove(anhang);
        }
    }

    public void clearAnhanege() {
        this.anhaenge = new ArrayList<>();
    }

    public String getFachId() {
        return fachId;
    }

    public void setFachId(String fachId) {
        this.fachId = fachId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getNextStatus() {
        return nextStatus;
    }

    public void setNextStatus(Status nextStatus) {
        this.nextStatus = nextStatus;
    }

    public boolean isStatusChangeRequested() {
        return statusChangeRequested;
    }

    public void setStatusChangeRequested(boolean statusChangeRequested) {
        this.statusChangeRequested = statusChangeRequested;
    }

    public String getStatusWechselKommentar() {
        return statusWechselKommentar;
    }

    public void setStatusWechselKommentar(String statusWechselKommentar) {
        this.statusWechselKommentar = statusWechselKommentar;
    }

    /**
     * @return the erstellungsdatum
     */
    public Date getErstellungsdatum() {
        return erstellungsdatum;
    }

    /**
     * @param erstellungsdatum the erstellungsdatum to set
     */
    public void setErstellungsdatum(Date erstellungsdatum) {
        this.erstellungsdatum = erstellungsdatum;
    }

    /**
     * @return the aenderungsdatum
     */
    public Date getAenderungsdatum() {
        return aenderungsdatum;
    }

    /**
     * @param aenderungsdatum the aenderungsdatum to set
     */
    public void setAenderungsdatum(Date aenderungsdatum) {
        this.aenderungsdatum = aenderungsdatum;
    }

    /**
     * @return the anhaenge
     */
    public List<Anhang> getAnhaenge() {
        return anhaenge;
    }

    /**
     * @param anhaenge the anhaenge to set
     */
    public void setAnhaenge(List<Anhang> anhaenge) {
        this.anhaenge = anhaenge;
    }

    /**
     * @return the bild
     */
    public Bild getBild() {
        return bild;
    }

    /**
     * @param bild the bild to set
     */
    public void setBild(Bild bild) {
        this.bild = bild;
    }

    public SensorCoc getSensorCoc() {
        return sensorCoc;
    }

    public void setSensorCoc(SensorCoc sensorCoc) {
        this.sensorCoc = sensorCoc;
    }
}
