package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import de.interfaceag.bmw.pzbk.shared.objectIds.DomainObject;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "derivat")
@NamedQueries({
        @NamedQuery(name = Derivat.ALL, query = "SELECT d FROM Derivat d WHERE d.status != 4 "),
        @NamedQuery(name = Derivat.ALL_IDS, query = "SELECT d.id FROM Derivat d WHERE d.status != 4 "),
        @NamedQuery(name = Derivat.ALL_FETCH, query = "SELECT DISTINCT d FROM Derivat d INNER JOIN FETCH d.kovAPhasen WHERE d.status != 4  ORDER BY d.name"),
        @NamedQuery(name = Derivat.ALL_WITH_ZIELVEREINBARUNG_FALSE, query = "SELECT d FROM Derivat d WHERE d.status NOT IN (3,4)"),
        @NamedQuery(name = Derivat.BY_ID, query = "SELECT d FROM Derivat d WHERE d.id = :id"),
        @NamedQuery(name = Derivat.BY_ID_LIST, query = "SELECT d FROM Derivat d WHERE d.status != 4 AND d.id IN :idList"),
        @NamedQuery(name = Derivat.BY_NAME, query = "SELECT d FROM Derivat d WHERE d.name LIKE :name"),
        @NamedQuery(name = Derivat.BY_NAME_LIST, query = "SELECT d FROM Derivat d WHERE d.status != 4 AND d.name IN :names"),
        @NamedQuery(name = Derivat.ALL_ERSTANLAEUFER, query = "SELECT d FROM Derivat d WHERE d.status != 3 AND d.status != 4 ORDER BY d.name")})
public class Derivat implements Serializable, DerivatDto, DomainObject {

    public static final String ALL = "derivat.all";
    public static final String ALL_IDS = "derivat.all_ids";
    public static final String ALL_FETCH = "derivat.allFetch";
    public static final String ALL_WITH_ZIELVEREINBARUNG_FALSE = "derivat.allWithZielvereinbarungFalse";
    public static final String BY_ID = "derivat.by_id";
    public static final String BY_ID_LIST = "derivat.by_id_list";
    public static final String BY_NAME = "derivat.by_name";
    public static final String BY_NAME_LIST = "derivat.by_name_list";
    public static final String ALL_ERSTANLAEUFER = "derivat.allErstanlaeufer";

    @Id
    @SequenceGenerator(name = "derivat_id_seq",
            sequenceName = "derivat_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "derivat_id_seq")
    private Long id;

    private String produktlinie;
    private String name;
    private String bezeichnung;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "derivat_id")
    private List<KovAPhaseImDerivat> kovAPhasen = new ArrayList<>();

    @Temporal(TemporalType.TIMESTAMP)
    private Date zakUpdate;

    private Integer status;

    private Integer lastStatus;

    public Derivat() {
        this.bezeichnung = "";
        initStatus();
    }

    public Derivat(String name, String bezeichnung) {
        this.name = name;
        this.bezeichnung = bezeichnung;
        initStatus();
    }

    public Derivat(String name, String bezeichnung, String produktlinie) {
        this.name = name;
        this.bezeichnung = bezeichnung;
        this.produktlinie = produktlinie;
    }

    private void initStatus() {
        setStatus(DerivatStatus.OFFEN);
        setLastStatus(DerivatStatus.OFFEN);
    }

    public Derivat getCopy() {
        Derivat derivat = new Derivat();
        derivat.setName(name);
        derivat.setBezeichnung(bezeichnung);
        derivat.setProduktlinie(produktlinie);
        derivat.setStatus(this.getStatus());
        derivat.setLastStatus(this.getLastStatus());
        return derivat;
    }

    public String getLabel() {
        return this.produktlinie + " | " + this.name;
    }

    @Override
    public String toString() {
        String result = " Derivat: " + this.name;
        if (this.bezeichnung != null && !this.bezeichnung.isEmpty()) {
            result += ", Bezeichnung: " + this.bezeichnung;
        }
        if (this.produktlinie != null && !this.produktlinie.isEmpty()) {
            result += ", Produktlinie: " + this.produktlinie;
        }
        return result;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(31, 89);
        hb.append(id);
        hb.append(name);
        hb.append(produktlinie);
        hb.append(bezeichnung);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Derivat)) {
            return false;
        }
        if (object == this) {
            return true;
        }
        Derivat executer = (Derivat) object;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, executer.id);
        eb.append(name, executer.name);
        eb.append(produktlinie, executer.produktlinie);
        eb.append(bezeichnung, executer.bezeichnung);
        return eb.isEquals();
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DerivatId getDerivatId() {
        return new DerivatId(id);
    }

    public String getProduktlinie() {
        return produktlinie;
    }

    public void setProduktlinie(String produktlinie) {
        this.produktlinie = produktlinie;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public List<KovAPhaseImDerivat> getKovAPhasen() {
        return kovAPhasen;
    }

    public void setKovAPhasen(List<KovAPhaseImDerivat> kovAPhasen) {
        this.kovAPhasen = kovAPhasen;
    }

    public Date getZakUpdate() {
        return zakUpdate;
    }

    public void setZakUpdate(Date zakUpdate) {
        this.zakUpdate = zakUpdate;
    }

    @Override
    public DerivatStatus getStatus() {
        return DerivatStatus.getById(status);
    }

    public void setStatus(DerivatStatus status) {
        if (status != null) {
            this.status = status.getId();
        } else {
            this.status = null;
        }
    }

    public DerivatStatus getLastStatus() {
        return DerivatStatus.getById(lastStatus);
    }

    public void setLastStatus(DerivatStatus lastStatus) {
        if (lastStatus != null) {
            this.lastStatus = lastStatus.getId();
        } else {
            this.lastStatus = null;
        }
    }

    public boolean isInaktiv() {
        return getStatus().isInaktiv();
    }

    @Override
    public boolean isVereinbarungActive() {
        return getStatus().equals(DerivatStatus.VEREINARBUNG_VKBG) || getStatus().equals(DerivatStatus.VEREINBARUNG_ZV);
    }

}
