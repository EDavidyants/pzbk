package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

/**
 * @author evda
 */
@Entity
@NamedQueries( {
        @NamedQuery(name = ZakUebertragung.ALL, query = "SELECT zak FROM ZakUebertragung zak"),
        @NamedQuery(name = ZakUebertragung.BY_ID, query = "SELECT zak FROM ZakUebertragung zak WHERE zak.id = :id"),
        @NamedQuery(name = ZakUebertragung.BY_DERIVATANFORDERUNGMODUL, query = "SELECT zak FROM ZakUebertragung zak WHERE zak.derivatAnforderungModul = :derivatAnforderungModul"),
        @NamedQuery(name = ZakUebertragung.BY_DERIVAT, query = "SELECT zak FROM ZakUebertragung zak WHERE zak.derivatAnforderungModul.zuordnungAnforderungDerivat.derivat = :derivat"),
        @NamedQuery(name = ZakUebertragung.BY_DERIVAT_LIST, query = "SELECT z FROM ZakUebertragung z WHERE z.derivatAnforderungModul.zuordnungAnforderungDerivat.derivat IN :derivatList"),
        @NamedQuery(name = ZakUebertragung.BY_DERIVATANFORDERUNGMODUL_LIST, query = "SELECT z FROM ZakUebertragung z WHERE z.derivatAnforderungModul IN :derivatAnforderungModulList"),
        @NamedQuery(name = ZakUebertragung.BY_DERIVATANFORDERUNGMODUL_ID_LIST, query = "SELECT z FROM ZakUebertragung z WHERE z.derivatAnforderungModul.id IN :derivatAnforderungModulIdList"),
        @NamedQuery(name = ZakUebertragung.GET_COUNT_BY_ANFORDERUNG_AND_MODUL_STATUSLIST, query = "SELECT COUNT(zak) FROM ZakUebertragung zak WHERE zak.derivatAnforderungModul.zuordnungAnforderungDerivat.anforderung = :anforderung AND zak.derivatAnforderungModul.modul = :modul AND zak.zakStatus IN :statusList"),
        @NamedQuery(name = ZakUebertragung.GET_BY_ZAK_UEBERTRAGUNG_IDS, query = "SELECT zak FROM ZakUebertragung zak WHERE zak.uebertragungId IN :uids"),
        @NamedQuery(name = ZakUebertragung.GET_BY_ZAK_IDS, query = "SELECT zak FROM ZakUebertragung zak WHERE zak.zakId IN :zakIds"),
        @NamedQuery(name = ZakUebertragung.GET_BY_ANFORDERUNG_MODUL_IDS, query = "SELECT zak FROM ZakUebertragung zak WHERE zak.derivatAnforderungModul.modul.id IN :modulIds AND zak.derivatAnforderungModul.zuordnungAnforderungDerivat.anforderung.id IN :anforderungIds"),
        @NamedQuery(name = ZakUebertragung.GET_BY_ANFORDERUNGID_MODULID, query = "SELECT zak FROM ZakUebertragung zak WHERE zak.derivatAnforderungModul.modul.id = :modulId AND zak.derivatAnforderungModul.zuordnungAnforderungDerivat.anforderung.id = :anforderungId"),
        @NamedQuery(name = ZakUebertragung.GET_BY_UEBERTRAGUNG_ID_DERIVAT_ID, query = "SELECT z FROM ZakUebertragung z WHERE z.uebertragungId = :uebertragungId and z.derivatAnforderungModul.zuordnungAnforderungDerivat.derivat.id = :derivatId"),
        @NamedQuery(name = ZakUebertragung.GET_BY_UEBERTRAGUNG_ID_LIST_DERIVAT_ID, query = "SELECT z FROM ZakUebertragung z WHERE z.uebertragungId IN :uebertragungIds and z.derivatAnforderungModul.zuordnungAnforderungDerivat.derivat.id = :derivatId"),
        @NamedQuery(name = ZakUebertragung.GET_ZAK_DERIVATE, query = "SELECT DISTINCT da.derivat FROM ZakUebertragung zak INNER JOIN zak.derivatAnforderungModul.zuordnungAnforderungDerivat AS da ORDER BY da.derivat.bezeichnung"),
        @NamedQuery(name = ZakUebertragung.GET_BY_STATUS, query = "SELECT DISTINCT z FROM ZakUebertragung z WHERE z.zakStatus = :status"),
        @NamedQuery(name = ZakUebertragung.GET_BY_ANFORDERUNG_DERIVATLIST, query = "SELECT DISTINCT z FROM ZakUebertragung z WHERE z.derivatAnforderungModul.zuordnungAnforderungDerivat.anforderung = :anforderung AND z.derivatAnforderungModul.zuordnungAnforderungDerivat.derivat IN :derivatList"),
        @NamedQuery(name = ZakUebertragung.GET_ZAK_DERIVATE_FOR_USER, query = "SELECT DISTINCT da.derivat FROM ZakUebertragung zak INNER JOIN zak.derivatAnforderungModul.zuordnungAnforderungDerivat AS da WHERE da.derivat IN :derivatList ORDER BY da.derivat.bezeichnung"),
        @NamedQuery(name = ZakUebertragung.GET_MAXUEBERTRAGUNGID_BY_ANFORDERUNG_MODUL, query = "SELECT max(zak.uebertragungId) FROM ZakUebertragung zak WHERE zak.derivatAnforderungModul.zuordnungAnforderungDerivat.anforderung = :anforderung AND zak.derivatAnforderungModul.modul = :modul"),
        @NamedQuery(name = ZakUebertragung.GET_ZAKIDS_BY_ANFORDERUNG_MODUL, query = "SELECT DISTINCT zak.zakId FROM ZakUebertragung zak WHERE zak.derivatAnforderungModul.zuordnungAnforderungDerivat.anforderung = :anforderung AND zak.derivatAnforderungModul.modul = :modul"),
        @NamedQuery(name = ZakUebertragung.GET_FIRST_ZAKUEBERTRAGUNG_FOR_ANFORDERUNG_MODUL, query = "SELECT DISTINCT z FROM ZakUebertragung z WHERE z.derivatAnforderungModul.zuordnungAnforderungDerivat.anforderung = :anforderung AND z.derivatAnforderungModul.modul = :modul AND z.erstellungsdatum >= :changeDate ORDER BY z.erstellungsdatum"),
        @NamedQuery(name = ZakUebertragung.GET_ALL_FOR_TIME_RANGE, query = "SELECT DISTINCT z FROM ZakUebertragung z INNER JOIN z.derivatAnforderungModul.zuordnungAnforderungDerivat AS da WHERE z.aenderungsdatum > :startDate AND z.aenderungsdatum < :endDate ORDER BY da.derivat.name")
})
public class ZakUebertragung implements Serializable {

    public static final String CLASSNAME = "ZakUebertragung.";

    // ------------  queries ---------------------------------------------------
    public static final String ALL = CLASSNAME + "all";
    public static final String BY_ID = CLASSNAME + "by_id";
    public static final String BY_DERIVATANFORDERUNGMODUL = CLASSNAME + "by_derivatanforderungmodul";
    public static final String BY_DERIVATANFORDERUNGMODUL_LIST = CLASSNAME + "by_derivatanforderungmodul_list";
    public static final String BY_DERIVATANFORDERUNGMODUL_ID_LIST = CLASSNAME + "by_derivatanforderungmodul_id_list";
    public static final String BY_DERIVAT = CLASSNAME + "by_derivat";
    public static final String BY_DERIVAT_LIST = CLASSNAME + "by_derivat_list";
    public static final String FETCH_BY_DERIVAT_LIST = CLASSNAME + "fetch_by_derivat_list";
    public static final String GET_COUNT_BY_ANFORDERUNG_AND_MODUL_STATUSLIST = CLASSNAME + "getCountByAnforderungAndModul_StatusList";
    public static final String GET_BY_ZAK_UEBERTRAGUNG_IDS = CLASSNAME + "getByZakUebertragungIds";
    public static final String GET_BY_ZAK_IDS = CLASSNAME + "getByZakIds";
    public static final String GET_BY_ANFORDERUNG_MODUL_IDS = CLASSNAME + "getByAnforderungModulIds";
    public static final String GET_BY_ANFORDERUNGID_MODULID = CLASSNAME + "getByAnforderungIdModulId";
    public static final String GET_ZAK_DERIVATE = CLASSNAME + "getZakDerivate";
    public static final String GET_ZAK_DERIVATE_FOR_USER = CLASSNAME + "getZakDerivateForUser";
    public static final String GET_BY_STATUS = CLASSNAME + "getByStatus";
    public static final String GET_BY_ANFORDERUNG_DERIVATLIST = CLASSNAME + "byAnforderungDerivatList";
    public static final String GET_MAXUEBERTRAGUNGID_BY_ANFORDERUNG_MODUL = CLASSNAME + "getMaxUebertragungIdByAnforderungModul";
    public static final String GET_ZAKIDS_BY_ANFORDERUNG_MODUL = CLASSNAME + "getZakIdsByAnforderungModul";
    public static final String GET_BY_UEBERTRAGUNG_ID_DERIVAT_ID = CLASSNAME + "getByUebertragungIdDerivatId";
    public static final String GET_BY_UEBERTRAGUNG_ID_LIST_DERIVAT_ID = CLASSNAME + "getByUebertragungIdListDerivatId";

    public static final String GET_FIRST_ZAKUEBERTRAGUNG_FOR_ANFORDERUNG_MODUL = CLASSNAME + "getFirstZakUebertragungForAnforderungModul";

    public static final String GET_ALL_FOR_TIME_RANGE = CLASSNAME + "getAllForTimeRange";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "zakuebertragung_id_seq",
            sequenceName = "zakuebertragung_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "zakuebertragung_id_seq")
    private Long id;

    @OneToOne(cascade = CascadeType.MERGE)
    private DerivatAnforderungModul derivatAnforderungModul;

    // wird vom System generiert basierend auf anforderung id und modul id und nach ZAK uebertragen
    // wird fuer den Absprung nach ZAK und die eindeutige Identifizierung einer Uebertragung verwendet
    private Long uebertragungId;

    // wird von ZAK geliefert und fuer die Anzeige verwendet
    private String zakId;

    // der Benutzer der Anwendung der die ZAK Uebertragung getaetigt hat
    private String ersteller;

    @Enumerated(EnumType.ORDINAL)
    private ZakStatus zakStatus;

    @Column(length = 1000000)
    private String zakKommentar;

    @Temporal(TIMESTAMP)
    private Date erstellungsdatum;

    @Temporal(TIMESTAMP)
    private Date aenderungsdatum;

    @Column(length = 1000000)
    private String zakResponse;

    @Column(length = 1000000)
    private String generatedXml;

    // ------------  constructors ----------------------------------------------
    public ZakUebertragung() {
        this.zakStatus = ZakStatus.EMPTY;
        this.erstellungsdatum = new Date();
        this.aenderungsdatum = new Date();
        this.ersteller = "";
        this.zakKommentar = "";
    }

    public ZakUebertragung(DerivatAnforderungModul derivatAnforderungModul) {
        this.derivatAnforderungModul = derivatAnforderungModul;
        this.zakStatus = ZakStatus.EMPTY;
        this.erstellungsdatum = new Date();
        this.aenderungsdatum = new Date();
        this.ersteller = "";
        this.zakKommentar = "";
    }

    public ZakUebertragung(DerivatAnforderungModul derivatAnforderungModul, String ersteller) {
        this.derivatAnforderungModul = derivatAnforderungModul;
        this.zakStatus = ZakStatus.EMPTY;
        this.erstellungsdatum = new Date();
        this.ersteller = ersteller;
        this.zakKommentar = "";
    }

    public ZakUebertragung(DerivatAnforderungModul derivatAnforderungModul, ZakStatus zakStatus, Long uebertragungId, Date erstellungsdatum) {
        this.derivatAnforderungModul = derivatAnforderungModul;
        this.zakStatus = zakStatus;
        this.uebertragungId = uebertragungId;
        this.erstellungsdatum = erstellungsdatum;
        this.aenderungsdatum = erstellungsdatum;
        this.zakKommentar = "";
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return "ZakÜbertragung von Derivat " + derivatAnforderungModul.getZuordnungAnforderungDerivat().getDerivat().getName()
                + ", Anforderung " + derivatAnforderungModul.getZuordnungAnforderungDerivat().getAnforderung().getFachId()
                + ", Modul: " + derivatAnforderungModul.getModul().getName()
                + ", ttoolid: " + this.getUebertragungId().toString()
                + ", zakid: " + this.getZakId();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(131, 827);
        hb.append(id);
        hb.append(derivatAnforderungModul);
        hb.append(zakStatus);
        hb.append(uebertragungId);
        hb.append(ersteller);
        hb.append(zakKommentar);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ZakUebertragung)) {
            return false;
        }
        if (object == this) {
            return true;
        }
        ZakUebertragung zak = (ZakUebertragung) object;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, zak.id);
        eb.append(derivatAnforderungModul, zak.derivatAnforderungModul);
        eb.append(zakStatus, zak.zakStatus);
        eb.append(uebertragungId, zak.uebertragungId);
        eb.append(ersteller, zak.ersteller);
        eb.append(zakKommentar, zak.zakKommentar);
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DerivatAnforderungModul getDerivatAnforderungModul() {
        return derivatAnforderungModul;
    }

    public void setDerivatAnforderungModul(DerivatAnforderungModul derivatAnforderungModul) {
        this.derivatAnforderungModul = derivatAnforderungModul;
    }

    public ZakStatus getZakStatus() {
        return zakStatus;
    }

    public void setZakStatus(ZakStatus zakStatus) {
        this.zakStatus = zakStatus;
    }

    public String getZakKommentar() {
        return zakKommentar;
    }

    public void setZakKommentar(String zakKommentar) {
        this.zakKommentar = zakKommentar;
    }

    public Date getAenderungsdatum() {
        return new Date(aenderungsdatum.getTime());
    }

    public void setAenderungsdatum(Date aenderungsdatum) {
        this.aenderungsdatum = new Date(aenderungsdatum.getTime());
    }

    public Long getUebertragungId() {
        return uebertragungId;
    }

    public void setUebertragungId(Long uebertragungId) {
        this.uebertragungId = uebertragungId;
    }

    public String getZakId() {
        return zakId;
    }

    public void setZakId(String zakId) {
        this.zakId = zakId;
    }

    public String getErsteller() {
        return ersteller;
    }

    public void setErsteller(String ersteller) {
        this.ersteller = ersteller;
    }

    public Date getErstellungsdatum() {
        return erstellungsdatum;
    }

    public void setErstellungsdatum(Date erstellungsdatum) {
        this.erstellungsdatum = erstellungsdatum;
    }

    public String getZakResponse() {
        return zakResponse;
    }

    public void setZakResponse(String zakResponse) {
        this.zakResponse = zakResponse;
    }

    public String getGeneratedXml() {
        return generatedXml;
    }

    public void setGeneratedXml(String generatedXml) {
        this.generatedXml = generatedXml;
    }

}
