package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author evda
 */
@Entity
@Table(name = "prozessbaukasten_history")
@NamedQueries( {
        @NamedQuery(name = ProzessbaukastenHistory.BY_FACHID_VERSION, query = "SELECT history FROM ProzessbaukastenHistory history WHERE history.prozessbaukasten.fachId = :fachId AND history.prozessbaukasten.version = :version ORDER BY history.datum DESC")})
public class ProzessbaukastenHistory implements Serializable {

    private static final String CLASSNAME = "ProzessbaukastenHistory.";
    public static final String BY_FACHID_VERSION = CLASSNAME + "getByFachIdAndVersion";

    @Id
    @SequenceGenerator(name = "prozessbaukastenhistory_id_seq",
            sequenceName = "prozessbaukastenhistory_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prozessbaukastenhistory_id_seq")
    @Column(name = "id")
    private Long id;

    @ManyToOne
    private Prozessbaukasten prozessbaukasten;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "datum")
    private Date datum;

    @Column(name = "changedfield")
    private String changedField;

    @Column(name = "benutzer")
    private String benutzer;

    @Column(name = "von", length = 10000)
    private String von;

    @Column(name = "auf", length = 10000)
    private String auf;

    public ProzessbaukastenHistory() {
        this.datum = new Date();
    }

    public ProzessbaukastenHistory(Prozessbaukasten prozessbaukasten) {
        this.prozessbaukasten = prozessbaukasten;
        this.datum = new Date();
    }

    public ProzessbaukastenHistory(Prozessbaukasten prozessbaukasten, String benutzer) {
        this.prozessbaukasten = prozessbaukasten;
        this.benutzer = benutzer;
        this.datum = new Date();
    }

    public ProzessbaukastenHistory(Prozessbaukasten prozessbaukasten, Date datum, String benutzer, String changedField, String von, String auf) {
        this.prozessbaukasten = prozessbaukasten;
        this.datum = datum;
        this.benutzer = benutzer;
        this.changedField = changedField;
        this.von = von;
        this.auf = auf;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Prozessbaukasten ").append(this.prozessbaukasten.getFachId()).append("| ");
        sb.append("V").append(this.prozessbaukasten.getVersion()).append(" ");
        sb.append("wurde bei '").append(this.changedField).append("' ");
        sb.append("von ").append(this.benutzer).append(" ");
        sb.append("von ").append(this.von).append(" auf ").append(this.auf).append(" ");
        sb.append("am ").append(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(this.datum)).append(" geändert ");
        String info = sb.toString();
        return info;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(71, 503);
        hb.append(id);
        hb.append(prozessbaukasten);
        hb.append(changedField);
        hb.append(von);
        hb.append(auf);
        hb.append(benutzer);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ProzessbaukastenHistory)) {
            return false;
        }
        if (object == this) {
            return true;
        }

        ProzessbaukastenHistory other = (ProzessbaukastenHistory) object;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, other.id);
        eb.append(prozessbaukasten, other.prozessbaukasten);
        eb.append(changedField, other.changedField);
        eb.append(von, von);
        eb.append(auf, auf);
        eb.append(benutzer, other.benutzer);
        return eb.isEquals();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Prozessbaukasten getProzessbaukasten() {
        return prozessbaukasten;
    }

    public void setProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        this.prozessbaukasten = prozessbaukasten;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getChangedField() {
        return changedField;
    }

    public void setChangedField(String changedField) {
        this.changedField = changedField;
    }

    public String getBenutzer() {
        return benutzer;
    }

    public void setBenutzer(String benutzer) {
        this.benutzer = benutzer;
    }

    public String getVon() {
        return von;
    }

    public void setVon(String von) {
        this.von = von;
    }

    public String getAuf() {
        return auf;
    }

    public void setAuf(String auf) {
        this.auf = auf;
    }

}
