package de.interfaceag.bmw.pzbk.entities.comparator;

import de.interfaceag.bmw.pzbk.entities.Tteam;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author sl
 */
public class TteamComparator implements Comparator<Tteam>, Serializable {

    @Override
    public int compare(Tteam t1, Tteam t2) {
        if (t1 != null && t2 != null) {
            String s1 = t1.getTeamName();
            String s2 = t2.getTeamName();
            int res = String.CASE_INSENSITIVE_ORDER.compare(s1, s2);
            if (res == 0) {
                res = s1.compareTo(s2);
            }
            return res;
        }
        return -2;
    }
}
