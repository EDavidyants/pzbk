package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author evda
 */
@Entity
@Table(name = "prozessbaukasten", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"fachId", "version"})})
@NamedQueries( {
        @NamedQuery(name = Prozessbaukasten.ALL, query = "SELECT DISTINCT p FROM Prozessbaukasten p INNER JOIN FETCH Anforderung LEFT JOIN FETCH Tteam LEFT JOIN FETCH Derivat LEFT JOIN FETCH Mitarbeiter ORDER BY LENGTH(p.fachId) DESC, p.fachId DESC"),
        @NamedQuery(name = Prozessbaukasten.GET_BY_FACHID_VERSION, query = "SELECT p FROM Prozessbaukasten p WHERE p.fachId = :fachId AND p.version = :version"),
        @NamedQuery(name = Prozessbaukasten.GET_BY_ANFORDERUNG_IDS, query = "SELECT DISTINCT p FROM Prozessbaukasten p INNER JOIN p.anforderungen AS a WHERE a.id IN :anforderungIds ORDER BY LENGTH(p.fachId) DESC, p.fachId DESC"),
        @NamedQuery(name = Prozessbaukasten.ALL_ZUORTENBARE, query = "SELECT p FROM Prozessbaukasten p WHERE p.status IN :statusList ORDER BY p.id"),
        @NamedQuery(name = Prozessbaukasten.GET_BY_ANFORDERUNG, query = "SELECT p FROM Prozessbaukasten p "
                + "INNER JOIN p.anforderungen a WHERE a.id = :anforderungId"),
        @NamedQuery(name = Prozessbaukasten.GET_LATEST_VERSION_NUMBER, query = "SELECT MAX(p.version) FROM Prozessbaukasten p WHERE p.fachId = :fachId")})
public class Prozessbaukasten implements Serializable {

    public static final String CLASSNAME = "Prozessbaukasten";
    public static final String ALL = CLASSNAME + ".all";
    public static final String GET_BY_FACHID_VERSION = CLASSNAME + ".getByFachIdAndVersion";
    public static final String GET_BY_ANFORDERUNG_IDS = CLASSNAME + ".getByAnforderungIds";
    public static final String ALL_ZUORTENBARE = CLASSNAME + ".getAllZuortenbareProzessbaukaesten";
    public static final String GET_BY_ANFORDERUNG = CLASSNAME + ".getByAnforderung";
    public static final String GET_LATEST_VERSION_NUMBER = CLASSNAME + ".getLatestVersionNumber";

    @Id
    @SequenceGenerator(name = "prozessbaukasten_id_seq",
            sequenceName = "prozessbaukasten_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prozessbaukasten_id_seq")
    private Long id;

    @Column(length = 10, nullable = false)
    private String fachId;

    @Column(nullable = false)
    private int version;

    @Column(nullable = false)
    private String bezeichnung;

    @Column(length = 10000, nullable = false)
    private String beschreibung;

    @ManyToOne
    private Tteam tteam;

    @ManyToOne
    private Mitarbeiter leadTechnologie;

    @Column(length = 10000)
    private String standardisierterFertigungsprozess;

    @Temporal(TemporalType.TIMESTAMP)
    private Date erstellungsdatum;

    @Temporal(TemporalType.TIMESTAMP)
    private Date aenderungsdatum;

    @Temporal(TemporalType.TIMESTAMP)
    private Date datumStatuswechsel;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(name = "prozessbaukasten_anforderung",
            joinColumns = {
                    @JoinColumn(name = "prozessbaukasten_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "anforderung_id", referencedColumnName = "id")
            })
    @OrderColumn
    private List<Anforderung> anforderungen;

    private Integer status;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Anhang startbrief;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Anhang grafikUmfang;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Anhang konzeptbaum;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Anhang beauftragungTPK;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Anhang genehmigungTPK;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Anhang> weitereAnhaenge;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    protected Bild grafikUmfangBild;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    protected Bild konzeptbaumBild;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY, targetEntity = Konzept.class, orphanRemoval = true)
    private List<Konzept> konzepte = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "erstanlaeufer_id", referencedColumnName = "id")
    private Derivat erstanlaeufer;

    public Prozessbaukasten() {
        this.fachId = "";
        this.version = 1;
        Date currentDate = new Date();
        this.erstellungsdatum = currentDate;
        this.aenderungsdatum = currentDate;
        this.datumStatuswechsel = currentDate;
        this.status = ProzessbaukastenStatus.ANGELEGT.getId();
        this.anforderungen = new ArrayList<>();
        this.weitereAnhaenge = new ArrayList<>();
    }

    public Prozessbaukasten(String fachId, int version) {
        this.fachId = fachId;
        this.version = version;
        Date currentDate = new Date();
        this.erstellungsdatum = currentDate;
        this.aenderungsdatum = currentDate;
        this.datumStatuswechsel = currentDate;
        this.status = ProzessbaukastenStatus.ANGELEGT.getId();
        this.anforderungen = new ArrayList<>();
        this.weitereAnhaenge = new ArrayList<>();
    }

    @Override
    public String toString() {
        return fachId + " | V" + version;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(113, 509);
        hb.append(id);
        hb.append(fachId);
        hb.append(version);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Prozessbaukasten)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        Prozessbaukasten other = (Prozessbaukasten) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, other.id);
        eb.append(fachId, other.fachId);
        eb.append(version, other.version);
        return eb.isEquals();
    }

    // ----- Getters and Setters -----
    public List<Konzept> getKonzepte() {
        return konzepte;
    }

    public void setKonzepte(List<Konzept> konzepte) {
        this.konzepte = konzepte;
    }

    public void addKonzept(Konzept konzept) {
        this.konzepte.add(konzept);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFachId() {
        return fachId;
    }

    public void setFachId(String fachId) {
        this.fachId = fachId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public Tteam getTteam() {
        return tteam;
    }

    public void setTteam(Tteam tteam) {
        this.tteam = tteam;
    }

    public Mitarbeiter getLeadTechnologie() {
        return leadTechnologie;
    }

    public void setLeadTechnologie(Mitarbeiter leadTechnologie) {
        this.leadTechnologie = leadTechnologie;
    }

    public String getStandardisierterFertigungsprozess() {
        return standardisierterFertigungsprozess;
    }

    public void setStandardisierterFertigungsprozess(String standardisierterFertigungsprozess) {
        this.standardisierterFertigungsprozess = standardisierterFertigungsprozess;
    }

    public Date getErstellungsdatum() {
        return erstellungsdatum;
    }

    public void setErstellungsdatum(Date erstellungsdatum) {
        this.erstellungsdatum = erstellungsdatum;
    }

    public Date getAenderungsdatum() {
        return aenderungsdatum;
    }

    public void setAenderungsdatum(Date aenderungsdatum) {
        this.aenderungsdatum = aenderungsdatum;
    }

    public ProzessbaukastenStatus getStatus() {
        return ProzessbaukastenStatus.getById(status);
    }

    public void setStatus(ProzessbaukastenStatus prozessbaukastenStatus) {
        if (prozessbaukastenStatus != null) {
            this.status = prozessbaukastenStatus.getId();
        } else {
            this.status = ProzessbaukastenStatus.ANGELEGT.getId();
        }
    }

    public List<Anforderung> getAnforderungen() {
        return anforderungen;
    }

    public void setAnforderungen(List<Anforderung> anforderungen) {
        this.anforderungen = anforderungen;
    }

    public void addAnforderung(Anforderung anforderung) {
        if (getAnforderungen() == null) {
            setAnforderungen(new ArrayList<>());
        }

        if (!getAnforderungen().contains(anforderung)) {
            getAnforderungen().add(anforderung);
        }
    }

    public void removeAnforderung(Anforderung anforderung) {
        if (getAnforderungen() != null && getAnforderungen().contains(anforderung)) {
            getAnforderungen().remove(anforderung);
        }
    }

    public Anhang getStartbrief() {
        return startbrief;
    }

    public void setStartbrief(Anhang startbrief) {
        this.startbrief = startbrief;
    }

    public Anhang getGrafikUmfang() {
        return grafikUmfang;
    }

    public void setGrafikUmfang(Anhang grafikUmfang) {
        this.grafikUmfang = grafikUmfang;
    }

    public Anhang getKonzeptbaum() {
        return konzeptbaum;
    }

    public void setKonzeptbaum(Anhang konzeptbaum) {
        this.konzeptbaum = konzeptbaum;
    }

    public Anhang getBeauftragungTPK() {
        return beauftragungTPK;
    }

    public void setBeauftragungTPK(Anhang beauftragungTPK) {
        this.beauftragungTPK = beauftragungTPK;
    }

    public Anhang getGenehmigungTPK() {
        return genehmigungTPK;
    }

    public void setGenehmigungTPK(Anhang genehmigungTPK) {
        this.genehmigungTPK = genehmigungTPK;
    }

    public List<Anhang> getWeitereAnhaenge() {
        return weitereAnhaenge;
    }

    public void setWeitereAnhaenge(List<Anhang> weitereAnhaenge) {
        this.weitereAnhaenge = weitereAnhaenge;
    }

    public void addWeiterenAnhang(Anhang anhang) {
        if (getWeitereAnhaenge() == null) {
            setWeitereAnhaenge(new ArrayList<>());
        }

        if (!getWeitereAnhaenge().contains(anhang)) {
            getWeitereAnhaenge().add(anhang);
        }
    }

    public void removeWeiterenAnhang(Anhang anhang) {
        if (getWeitereAnhaenge() != null && getWeitereAnhaenge().contains(anhang)) {
            getWeitereAnhaenge().remove(anhang);
        }
    }

    public String getNameCompleteName() {
        return fachId + " | V" + version + " : " + bezeichnung;
    }

    public Bild getGrafikUmfangBild() {
        return grafikUmfangBild;
    }

    public void setGrafikUmfangBild(Bild grafikUmfangBild) {
        this.grafikUmfangBild = grafikUmfangBild;
    }

    public Bild getKonzeptbaumBild() {
        return konzeptbaumBild;
    }

    public void setKonzeptbaumBild(Bild konzeptbaumBild) {
        this.konzeptbaumBild = konzeptbaumBild;
    }

    public Date getDatumStatuswechsel() {
        return datumStatuswechsel;
    }

    public void setDatumStatuswechsel(Date datumStatuswechsel) {
        this.datumStatuswechsel = datumStatuswechsel;
    }

    public Derivat getErstanlaeufer() {
        return erstanlaeufer;
    }

    public void setErstanlaeufer(Derivat erstanlaeufer) {
        this.erstanlaeufer = erstanlaeufer;
    }

}
