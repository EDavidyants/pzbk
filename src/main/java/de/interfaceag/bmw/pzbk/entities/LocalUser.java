package de.interfaceag.bmw.pzbk.entities;

import javax.annotation.PostConstruct;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Christian Schauer
 */
@Entity
@Table(name = "T_USERS")
@NamedQueries({
    @NamedQuery(name = LocalUser.LOCALUSER_ALL, query = "select m from LocalUser m"),
    @NamedQuery(name = LocalUser.LOCALUSER_BY_QNUMBER, query = "select m from LocalUser m where LOWER(:m) = LOWER(m.qnumber)")})
public class LocalUser implements Serializable {

    public static final String LOCALUSER_ALL = "localuser_all";
    public static final String LOCALUSER_BY_QNUMBER = "localuser_by_qnumber";

    @Id
    private String qnumber;
    private String password;

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinTable(name = "user_to_group", joinColumns = @JoinColumn(name = "qnumber"), inverseJoinColumns = @JoinColumn(name = "gruppenname"))
    private List<LocalGroup> gruppen;

    public LocalUser() {
        this.gruppen = new ArrayList<>();
    }

    @PostConstruct
    public void init() {
        this.gruppen = new ArrayList<>();
    }


    public void addGruppe(LocalGroup gruppe) {
        gruppen.add(gruppe);
    }

    public String getQnumber() {
        return qnumber;
    }

    public void setQnumber(String qnumber) {
        this.qnumber = qnumber;
    }

    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }

}
