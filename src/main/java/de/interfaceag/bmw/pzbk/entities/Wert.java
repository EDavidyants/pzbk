package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.Attribut;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 * @author fp
 */
@Entity
@NamedQueries( {
        @NamedQuery(name = Wert.QUERY_BY_ATTRIBUT, query = "select w from Wert w where (:a is null or w.attribut = :a) ORDER BY w.wert"),
        @NamedQuery(name = Wert.QUERY_BY_ID, query = "select w from Wert w where w.id = :id"),
        @NamedQuery(name = Wert.QUERY_BY_ATTRIBUT_AND_WERT, query = "select w from Wert w where w.wert = :wert AND w.attribut = :attribut"),
        @NamedQuery(name = Wert.QUERY_ALL_WERTE_BY_ATTRIBUT, query = "select w.wert from Wert w where w.attribut = :attribut ORDER BY w.wert"),
        @NamedQuery(name = Wert.QUERY_ALL_ATTRIBUTE, query = "select distinct w.attribut from Wert w")})
public class Wert implements Serializable {

    public static final String QUERY_BY_ATTRIBUT = "wertByAttribut";
    public static final String QUERY_BY_ID = "wertById";
    public static final String QUERY_BY_ATTRIBUT_AND_WERT = "wertByAttributAndWert";
    public static final String QUERY_ALL_WERTE_BY_ATTRIBUT = "allWerteByAttribut";
    public static final String QUERY_ALL_ATTRIBUTE = "allAttribute";

    @Id
    @SequenceGenerator(name = "wert_id_seq",
            sequenceName = "wert_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wert_id_seq")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(length = 10000)
    private Attribut attribut;

    @Column(length = 10000)
    private String wert;

    public Wert() {

    }

    public Wert(Attribut attribut, String wert) {
        this.attribut = attribut;
        this.wert = wert;
    }


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public Attribut getAttribut() {
        return attribut;
    }


    public void setAttribut(Attribut attribut) {
        this.attribut = attribut;
    }


    public String getWert() {
        return wert;
    }


    public void setWert(String wert) {
        this.wert = wert;
    }

    @Override
    public String toString() {
        return " " + this.wert;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(13, 41);
        hb.append(id);
        hb.append(attribut);
        hb.append(wert);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Wert)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        Wert mywert = (Wert) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, mywert.id);
        eb.append(attribut, mywert.attribut);
        eb.append(wert, mywert.wert);
        return eb.isEquals();
    }
}
