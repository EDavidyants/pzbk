package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.AnforderungModulTupel;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

/**
 * @author evda
 */
@Entity
@Table(name = "umsetzungsbestaetigung")
@NamedQueries( {
        @NamedQuery(name = Umsetzungsbestaetigung.BY_ID, query = "SELECT u FROM Umsetzungsbestaetigung u WHERE u.id = :id"),
        @NamedQuery(name = Umsetzungsbestaetigung.BY_DERIVAT, query = "SELECT u FROM Umsetzungsbestaetigung u WHERE u.kovaImDerivat.derivat = :derivat"),
        @NamedQuery(name = Umsetzungsbestaetigung.BY_DERIVAT_STATUS, query = "SELECT u FROM Umsetzungsbestaetigung u WHERE u.kovaImDerivat.derivat = :derivat AND u.status = :status"),
        @NamedQuery(name = Umsetzungsbestaetigung.BY_DERIVAT_ANFORDERUNG, query = "SELECT u FROM Umsetzungsbestaetigung u WHERE u.anforderung = :anforderung AND u.kovaImDerivat.derivat = :derivat"),
        @NamedQuery(name = Umsetzungsbestaetigung.BY_ANFORDERUNGID_MODULID, query = "SELECT u FROM Umsetzungsbestaetigung u WHERE u.anforderung.id = :anforderungId AND u.modul.id = :modulId"),
        @NamedQuery(name = Umsetzungsbestaetigung.BY_ANFORDERUNG_DERIVAT_PHASE, query = "SELECT u FROM Umsetzungsbestaetigung u WHERE u.anforderung = :anforderung AND u.kovaImDerivat.derivat = :derivat AND u.kovaImDerivat.kovAPhase = :kovAPhase"),
        @NamedQuery(name = Umsetzungsbestaetigung.BY_ANFORDERUNG_MODUL_DERIVAT_PHASE, query = "SELECT u FROM Umsetzungsbestaetigung u WHERE u.anforderung.id = :anforderungId AND u.kovaImDerivat.derivat.id = :derivatId AND u.modul.id = :modulId AND u.kovaImDerivat.kovAPhase = :kovAPhase"),
        @NamedQuery(name = Umsetzungsbestaetigung.BY_KOVAIMDERIVAT_AND_ANFO_MODUL, query = "SELECT u FROM Umsetzungsbestaetigung u WHERE u.kovaImDerivat = :kovaImDerivat AND u.anforderung = :anforderung AND u.modul = :modul"),
        @NamedQuery(name = Umsetzungsbestaetigung.BY_DERIVAT_ANFORDERUNG_MODUL, query = "SELECT u FROM Umsetzungsbestaetigung u WHERE u.kovaImDerivat.derivat = :derivat AND u.anforderung = :anforderung AND u.modul = :modul"),
        @NamedQuery(name = Umsetzungsbestaetigung.COMMENT_BY_KOVAIMDERIVAT_AND_ANFO, query = "SELECT u.kommentar FROM Umsetzungsbestaetigung u WHERE u.kovaImDerivat = :kovaImDerivat AND u.anforderung = :anforderung"),
        @NamedQuery(name = Umsetzungsbestaetigung.GET_UMSETZUNGSBESTAETIGER_BY_DERIVAT_AND_PHASE, query = "SELECT DISTINCT u.umsetzungsbestaetiger FROM Umsetzungsbestaetigung u WHERE u.kovaImDerivat.derivat = :derivat AND u.kovaImDerivat.kovAPhase = :kovAPhase"),
        @NamedQuery(name = Umsetzungsbestaetigung.GET_BY_DERIVATLIST_ANFORDERUNGLIST_AND_PHASE, query = "SELECT DISTINCT u FROM Umsetzungsbestaetigung u WHERE u.kovaImDerivat.derivat IN :derivatList AND u.anforderung IN :anforderungList AND u.kovaImDerivat.kovAPhase = :kovAPhase"),
        @NamedQuery(name = Umsetzungsbestaetigung.ID_BY_KOVASTATUS_STATUS, query = "SELECT u.id FROM Umsetzungsbestaetigung u WHERE u.kovaImDerivat.kovaStatus = :kovaStatus AND u.status = :status"),
        @NamedQuery(name = Umsetzungsbestaetigung.ID_BY_UMSAETZBESTAETIGER_KOVASTATUS_STATUS, query = "SELECT u.id FROM Umsetzungsbestaetigung u WHERE u.umsetzungsbestaetiger = :umsetzungsbestaetiger AND u.kovaImDerivat.kovaStatus = :kovaStatus AND u.status = :status"),
        @NamedQuery(name = Umsetzungsbestaetigung.GET_KOVAPHASEIMDERIVAT_WITH_STATUS, query = "SELECT u.kovaImDerivat FROM Umsetzungsbestaetigung u WHERE u.kovaImDerivat.kovaStatus = :kovaStatus AND u.status = :status"),
        @NamedQuery(name = Umsetzungsbestaetigung.GET_BY_DERIVAT_KOVAPHASE_SENSORCOC, query = "SELECT u FROM Umsetzungsbestaetigung u WHERE u.kovaImDerivat.derivat = :derivat AND u.kovaImDerivat.kovAPhase = :kovAPhase AND u.anforderung.sensorCoc.sensorCocId IN :sensorCocList")})
public class Umsetzungsbestaetigung implements Serializable {

    public static final String CLASSNAME = "Umsetzungsbestaetigung.";

    // ------------  queries ---------------------------------------------------
    public static final String BY_ID = "umsetzungsbestaetigung.by_id";
    public static final String BY_DERIVAT = "umsetzungsbestaetigung.by_derivat";
    public static final String BY_DERIVAT_STATUS = "umsetzungsbestaetigung.by_derivat_status";
    public static final String BY_DERIVAT_ANFORDERUNG = "umsetzungsbestaetigung.by_derivat_anforderung";
    public static final String BY_ANFORDERUNGID_MODULID = "umsetzungsbestaetigung.by_anforderung_modul";
    public static final String BY_ANFORDERUNG_DERIVAT_PHASE = "umsetzungsbestaetigung.by_anforderung_derivat_phase";
    public static final String BY_ANFORDERUNG_MODUL_DERIVAT_PHASE = "umsetzungsbestaetigung.by_anforderung_modul_derivat_phase";
    public static final String BY_KOVAIMDERIVAT_AND_ANFO_MODUL = CLASSNAME + "by_kovaImDerivat_and_anforderung_modul";
    public static final String BY_DERIVAT_ANFORDERUNG_MODUL = CLASSNAME + "by_derivat_anforderung_modul";
    public static final String COMMENT_BY_KOVAIMDERIVAT_AND_ANFO = "umsetzungsbestaetigung.comment_by_kovaImDerivat_and_anfo";
    public static final String GET_UMSETZUNGSBESTAETIGER_BY_DERIVAT_AND_PHASE = "umsetzungsbestaetigung.getUmsetzungsbestaetigerByDerivatAndPhase";
    public static final String ID_BY_KOVASTATUS_STATUS = "umsetzungsbestaetigung.by_kovastatus_bestaetigungStatus";
    public static final String ID_BY_UMSAETZBESTAETIGER_KOVASTATUS_STATUS = "umsetzungsbestaetigung.by_umsaetzbestaetiger_kovastatus_bestaetigungStatus";
    public static final String GET_KOVAPHASEIMDERIVAT_WITH_STATUS = "umsetzungsbestaetigung.getByKovaphaseStatusAndStatus";
    public static final String GET_BY_DERIVATLIST_ANFORDERUNGLIST_AND_PHASE = "umsetzungsbestaetigung.getByDerivatListAnforderungListAndPhase";
    public static final String GET_BY_DERIVAT_KOVAPHASE_SENSORCOC = "umsetzungsbestaetigung.getByDerivatAndKovAPhase";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "umsetzungsbestaetigung_id_seq",
            sequenceName = "umsetzungsbestaetigung_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "umsetzungsbestaetigung_id_seq")
    private Long id;

    @ManyToOne
    private Anforderung anforderung;

    @ManyToOne
    private Modul modul;

    @ManyToOne(cascade = CascadeType.MERGE)
    private KovAPhaseImDerivat kovaImDerivat;

    @ManyToOne(fetch = FetchType.LAZY)
    private Mitarbeiter umsetzungsbestaetiger;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "status")
    private UmsetzungsBestaetigungStatus status;

    @Temporal(TIMESTAMP)
    @Column(name = "datum")
    private Date datum;

    @Column(name = "kommentar", length = 10000)
    private String kommentar;

    // ------------  constructors ----------------------------------------------
    public Umsetzungsbestaetigung() {
        this.datum = new Date();
        this.kommentar = "";
        this.status = UmsetzungsBestaetigungStatus.OFFEN;
    }

    public Umsetzungsbestaetigung(Anforderung anforderung, KovAPhaseImDerivat kovaImDerivat, Modul modul) {
        this.anforderung = anforderung;
        this.kovaImDerivat = kovaImDerivat;
        this.modul = modul;
        this.datum = new Date();
        this.kommentar = "";
        this.status = UmsetzungsBestaetigungStatus.OFFEN;
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Umsetzungsbestaetigung von Anforderung ");
        if (anforderung != null) {
            sb.append(anforderung.toString());
        } else {
            sb.append(" is null");
        }
        sb.append(" im Derivat ");
        if (kovaImDerivat != null && kovaImDerivat.getDerivat() != null) {
            sb.append(kovaImDerivat.getDerivat().getName());
        } else {
            sb.append(" is null");
        }
        sb.append(" - Status: ");
        if (status != null) {
            sb.append(status.toString());
        } else {
            sb.append(" is null");
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(73, 773);
        hb.append(id);
        hb.append(anforderung);
        hb.append(kovaImDerivat);
        hb.append(umsetzungsbestaetiger);
        hb.append(kommentar);

        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Umsetzungsbestaetigung)) {
            return false;
        }
        if (object == this) {
            return true;
        }
        Umsetzungsbestaetigung ums = (Umsetzungsbestaetigung) object;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, ums.id);
        eb.append(anforderung, ums.anforderung);
        eb.append(kovaImDerivat, ums.kovaImDerivat);
        eb.append(umsetzungsbestaetiger, ums.umsetzungsbestaetiger);
        eb.append(kommentar, ums.kommentar);
        return eb.isEquals();
    }

    public AnforderungModulTupel getAnforderungModulTupel() {
        return new AnforderungModulTupel(anforderung, modul);
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Anforderung getAnforderung() {
        return anforderung;
    }

    public void setAnforderung(Anforderung anforderung) {
        this.anforderung = anforderung;
    }

    public KovAPhaseImDerivat getKovaImDerivat() {
        return kovaImDerivat;
    }

    public void setKovaImDerivat(KovAPhaseImDerivat kovaImDerivat) {
        this.kovaImDerivat = kovaImDerivat;
    }

    public Mitarbeiter getUmsetzungsbestaetiger() {
        return umsetzungsbestaetiger;
    }

    public void setUmsetzungsbestaetiger(Mitarbeiter umsetzungsbestaetiger) {
        this.umsetzungsbestaetiger = umsetzungsbestaetiger;
    }

    public Date getDatum() {
        return new Date(datum.getTime());
    }

    public String getDatumFormatted() {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm").format(datum);
    }

    public void setDatum(Date datum) {
        this.datum = new Date(datum.getTime());
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    public UmsetzungsBestaetigungStatus getStatus() {
        return status;
    }

    public void setStatus(UmsetzungsBestaetigungStatus status) {
        this.status = status;
    }

    public Modul getModul() {
        return modul;
    }

    public void setModul(Modul modul) {
        this.modul = modul;
    }

} // end of class
