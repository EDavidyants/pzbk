package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @author Christian Schauer
 */
@Entity
@NamedQueries( {
        @NamedQuery(name = de.interfaceag.bmw.pzbk.entities.SensorCoc.QUERY_BY_RESSORT_ORTUNG_MODUL,
                query
                        = "select s from SensorCoc s"
                        + " where (:ressort is null or s.ressort = :ressort)"
                        + "   and (:technologie is null or s.technologie = :technologie)"
                        + "   and (:ortung is null or s.ortung = :ortung)"),
        @NamedQuery(name = de.interfaceag.bmw.pzbk.entities.SensorCoc.QUERY_BY_RESSORT_ORTUNG_MODUL_IN_LIST,
                query
                        = "select s from SensorCoc s"
                        + " where (:ressort is null or s.ressort = :ressort)"
                        + "   and (:technologie is null or s.technologie = :technologie)"
                        + "   and (:ortung is null or s.ortung = :ortung)"
                        + " and s.sensorCocId IN :lst"),
        @NamedQuery(name = de.interfaceag.bmw.pzbk.entities.SensorCoc.QUERY_BY_TECHNOLOGIE,
                query = "select s from SensorCoc s where s.technologie = :technologie"),
        @NamedQuery(name = de.interfaceag.bmw.pzbk.entities.SensorCoc.QUERY_BY_ORTUNG,
                query = "select s from SensorCoc s where s.ortung IN :ortung"),
        @NamedQuery(name = SensorCoc.QUERY_BY_TECHNOLOGIELIST,
                query = "select s from SensorCoc s where s.technologie IN :technologieList"),
        @NamedQuery(name = SensorCoc.QUERY_ALL, query = "select s from SensorCoc s"),
        @NamedQuery(name = SensorCoc.QUERY_ALL_SORT_BY_TECHNOLOGIE, query = "select s from SensorCoc s ORDER BY s.technologie"),
        @NamedQuery(name = SensorCoc.QUERY_ALL_IDS, query = "select s.sensorCocId from SensorCoc s order by s.sensorCocId"),
        @NamedQuery(name = SensorCoc.QUERY_BY_ID, query = "select s from SensorCoc s where s.sensorCocId = :id"),
        @NamedQuery(name = SensorCoc.QUERY_BY_ID_LIST, query = "SELECT s FROM SensorCoc s WHERE s.sensorCocId IN :ids"),
        @NamedQuery(name = SensorCoc.QUERY_BY_ID_LIST_DTO, query = "SELECT s.sensorCocId, s.technologie FROM SensorCoc s WHERE s.sensorCocId IN :ids"),
        @NamedQuery(name = SensorCoc.QUERY_BY_ZAK_EINORDNUNG, query = "select s from SensorCoc s where s.zakEinordnung like :zakEinordnung"),
        @NamedQuery(name = SensorCoc.QUERY_ID_BY_ZAK_EINORDNUNG_LIST, query = "SELECT s.sensorCocId FROM SensorCoc s WHERE s.zakEinordnung IN :zakList"),
        @NamedQuery(name = SensorCoc.QUERY_ALL_ZAK_EINORDNUNG, query = "SELECT s.zakEinordnung from SensorCoc s")})
public class SensorCoc implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String QUERY_BY_RESSORT_ORTUNG_MODUL = "sensorCocByRessortOrtungModul";
    public static final String QUERY_BY_TECHNOLOGIE = "sensorCocByTechnologie";
    public static final String QUERY_BY_ORTUNG = "sensorCocByOrtung";
    public static final String QUERY_BY_TECHNOLOGIELIST = "sensorCocByTechnologieList";
    public static final String QUERY_ALL = "sensorCocAll";
    public static final String QUERY_ALL_SORT_BY_TECHNOLOGIE = "sensorCocAllSortByTechnologie";
    public static final String QUERY_ALL_IDS = "sensorCocAllIds";
    public static final String QUERY_BY_ID = "sensorCocById";
    public static final String QUERY_BY_ID_LIST = "sensorCoc_id_list";
    public static final String QUERY_BY_ID_LIST_DTO = "sensorCoc_id_list_dto";
    public static final String QUERY_BY_ZAK_EINORDNUNG = "sensorCocByZakEinordnung";
    public static final String QUERY_BY_ZAK_EINORDNUNG_LIST = "sensorCocByZakEinordnungList";
    public static final String QUERY_BY_RESSORT_ORTUNG_MODUL_IN_LIST = "sensorCocByRessortOrtungModulInList";
    public static final String QUERY_ALL_ZAK_EINORDNUNG = "allZakEinordnung";
    public static final String QUERY_ID_BY_ZAK_EINORDNUNG_LIST = "sensorCocIdByZakEinordnungList";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "sensorcoc_id_seq",
            sequenceName = "sensorcoc_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sensorcoc_id_seq")
    private Long sensorCocId;

    private String ressort;
    private String ortung;
    private String technologie;
    private String zakEinordnung;

    @Transient
    private Mitarbeiter sensorCoCLeiter;

    // ------------  constructors ----------------------------------------------
    public SensorCoc() {

    }

    public SensorCoc(Mitarbeiter fts) {
        this.sensorCoCLeiter = fts;
    }

    // ------------  methods ---------------------------------------------------
    public String pathToString() {
        String s = "";
        if (ressort != null) {
            s += ressort;
        }
        if (ortung != null) {
            s += ">" + ortung;
        }
        if (technologie != null) {
            s += ">" + technologie;
        }
        return s;
    }

    public String pathToStringShort() {
        String s = "";
        if (ortung != null) {
            s += ortung;
        }
        if (technologie != null) {
            s += ">" + technologie;
        }
        return s;
    }

    public String getOrdnungsstruktur() {
        String s = "";
        if (ressort != null) {
            s += ressort;
        }
        if (ortung != null) {
            s += "\\" + ortung;
        }
        if (technologie != null) {
            s += "\\" + technologie;
        }
        return s;
    }

    public String getTechnologie() {
        return technologie;
    }

    public void setTechnologie(String technologie) {
        this.technologie = technologie;
    }

    public static SensorCoc createTestSensorCoc(String supplement, String qNumberPrefix) {
        SensorCoc s = new SensorCoc();
        s.setTechnologie("Modul_" + supplement);
        s.setOrtung("Ortung_" + supplement);
        s.setRessort("Ressort_" + supplement);
        s.setSensorCoCLeiter(Mitarbeiter.createTestAnwender("Sensor_" + supplement, qNumberPrefix));
        return s;
    }

    @Override
    public String toString() {
        return pathToString();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(29, 101);
        hb.append(sensorCocId);
        hb.append(ressort);
        hb.append(ortung);
        hb.append(technologie);

        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SensorCoc)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        SensorCoc soc = (SensorCoc) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(sensorCocId, soc.sensorCocId);
        eb.append(ressort, soc.ressort);
        eb.append(ortung, soc.ortung);
        eb.append(technologie, soc.technologie);
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------

    /**
     * @return the sensorCoCLeiter
     */
    public Mitarbeiter getSensorCoCLeiter() {
        return sensorCoCLeiter;
    }

    /**
     * @param sensorCoCLeiter the sensorCoCLeiter to set
     */
    public void setSensorCoCLeiter(Mitarbeiter sensorCoCLeiter) {
        this.sensorCoCLeiter = sensorCoCLeiter;
    }

    /**
     * @return the zakEinordnung
     */
    public String getZakEinordnung() {
        return zakEinordnung;
    }

    /**
     * @param zakEinordnung the zakEinordnung to set
     */
    public void setZakEinordnung(String zakEinordnung) {
        this.zakEinordnung = zakEinordnung;
    }

    /**
     * @return the sensorCocId
     */
    public Long getSensorCocId() {
        return sensorCocId;
    }

    /**
     * @param sensorCocId the sensorCocId to set
     */
    public void setSensorCocId(Long sensorCocId) {
        this.sensorCocId = sensorCocId;
    }

    public String getRessort() {
        return ressort;
    }

    public void setRessort(String ressort) {
        this.ressort = ressort;
    }

    /**
     * @return the ortung
     */
    public String getOrtung() {
        return ortung;
    }

    /**
     * @param ortung the ortung to set
     */
    public void setOrtung(String ortung) {
        this.ortung = ortung;
    }

} // end of class
