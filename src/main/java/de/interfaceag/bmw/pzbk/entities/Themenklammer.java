package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.shared.objectIds.ThemenklammerId;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 * @author evda
 */
@Entity
@NamedQuery(name = Themenklammer.GET_ALL, query = "SELECT tk FROM Themenklammer tk ORDER BY tk.id")
public class Themenklammer implements Serializable {

    private static final String CLASSNAME = "themenklammer.";
    public static final String GET_ALL = CLASSNAME + "get_all";

    @Id
    @SequenceGenerator(name = "themenklammer_id_seq",
            sequenceName = "themenklammer_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "themenklammer_id_seq")
    private Long id;

    private String bezeichnung;

    public Themenklammer() {

    }

    public Themenklammer(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    @Override
    public String toString() {
        return "Themenklammer " + bezeichnung;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hashCodeBuilder = new HashCodeBuilder(71, 577);
        hashCodeBuilder.append(id);
        hashCodeBuilder.append(bezeichnung);
        return hashCodeBuilder.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Themenklammer other = (Themenklammer) obj;
        EqualsBuilder equalsBuilder = new EqualsBuilder();
        equalsBuilder.append(id, other.id);
        equalsBuilder.append(bezeichnung, other.bezeichnung);
        return equalsBuilder.isEquals();
    }

    public ThemenklammerId getThemenklammerId() {
        return new ThemenklammerId(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public ThemenklammerDto getDto() {
        return new ThemenklammerDto(id, bezeichnung);
    }

}
