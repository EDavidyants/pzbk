package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author evda
 */
@Entity
@Table(name = "berechtigung_derivat")

@NamedQuery(name = BerechtigungDerivat.BY_ID, query = "SELECT bere FROM BerechtigungDerivat bere WHERE bere.id = :id")

@NamedQuery(name = BerechtigungDerivat.BY_MITARBEITER_AND_ROLLE_FOR_DERIVAT, query = "SELECT bere FROM BerechtigungDerivat bere WHERE bere.derivatId = :derivatId AND bere.mitarbeiter = :mitarbeiter AND bere.rolle = :rolle")

@NamedQuery(name = BerechtigungDerivat.GET_MITARBEITER_BY_DERIVATEN_IN_LIST_ROLLE_AND_RECHTTYPE, query = "SELECT bere.mitarbeiter FROM BerechtigungDerivat bere WHERE bere.derivatId IN :derivaten AND bere.rolle = :rolle AND bere.rechttype = :rechttype")

@NamedQuery(name = BerechtigungDerivat.GET_DERIVATE_BY_MITARBEITER_AND_ROLLE, query = "SELECT b.derivatId FROM BerechtigungDerivat b WHERE b.mitarbeiter = :mitarbeiter AND b.rolle = :rolle")
public class BerechtigungDerivat implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String BY_ID = "getBerechtigungDerivatById";
    public static final String BY_MITARBEITER_AND_ROLLE_FOR_DERIVAT = "getBerechtigungDerivatByMitarbeiterAndRolleForDerivat";
    public static final String GET_MITARBEITER_BY_DERIVATEN_IN_LIST_ROLLE_AND_RECHTTYPE = "getBerechtigungDerivatByDerivatenInListRolleAndRechttype";
    public static final String GET_DERIVATE_BY_MITARBEITER_AND_ROLLE = "getDerivateByMitarbeiterAndRolle";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "berechtigungderivat_id_seq",
            sequenceName = "berechtigungderivat_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "berechtigungderivat_id_seq")
    private Long id;

    @Column(name = "derivat_id")
    private Long derivatId;

    @ManyToOne(fetch = FetchType.LAZY)
    private Mitarbeiter mitarbeiter;

    @Column(name = "rolle")
    private Rolle rolle;

    @Column(name = "rechttype")
    private Rechttype rechttype;

    // ------------  constructors ----------------------------------------------
    public BerechtigungDerivat() {

    }

    public BerechtigungDerivat(Long derivatId, Mitarbeiter mitarbeiter, Rolle rolle, Rechttype rechttype) {
        this.derivatId = derivatId;
        this.mitarbeiter = mitarbeiter;
        this.rolle = rolle;
        this.rechttype = rechttype;
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return "Derivat ID: " + derivatId + ", Mitarbeiter: " + mitarbeiter.toString();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(409, 727);
        hb.append(id);
        hb.append(derivatId);
        hb.append(mitarbeiter);
        hb.append(rolle);
        hb.append(rechttype);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof BerechtigungDerivat)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        BerechtigungDerivat bere = (BerechtigungDerivat) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, bere.id);
        eb.append(derivatId, bere.derivatId);
        eb.append(mitarbeiter, bere.mitarbeiter);
        eb.append(rolle, bere.rolle);
        eb.append(rechttype, bere.rechttype);
        return eb.isEquals();
    }

    // ------------  getter / setter -------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDerivatId() {
        return derivatId;
    }

    public void setDerivatId(Long derivatId) {
        this.derivatId = derivatId;
    }

    public Mitarbeiter getMitarbeiter() {
        return mitarbeiter;
    }

    public void setMitarbeiter(Mitarbeiter mitarbeiter) {
        this.mitarbeiter = mitarbeiter;
    }

    public Rolle getRolle() {
        return rolle;
    }

    public void setRolle(Rolle rolle) {
        this.rolle = rolle;
    }

    public Rechttype getRechttype() {
        return rechttype;
    }

    public void setRechttype(Rechttype rechttype) {
        this.rechttype = rechttype;
    }
} // end of class
