package de.interfaceag.bmw.pzbk.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author evda
 */
@Entity
@Table(name = "user_defined_search")
@NamedQueries({
    @NamedQuery(name = UserDefinedSearch.BY_ID, query = "SELECT uds FROM UserDefinedSearch uds WHERE uds.id = :id"),
    @NamedQuery(name = UserDefinedSearch.BY_MITARBEITER, query = "SELECT uds FROM UserDefinedSearch uds WHERE uds.mitarbeiter = :mitarbeiter ORDER BY uds.id"),
    @NamedQuery(name = UserDefinedSearch.BY_NAME_MITARBEITER, query = "SELECT uds FROM UserDefinedSearch uds WHERE uds.name = :name AND uds.mitarbeiter = :mitarbeiter")})
public class UserDefinedSearch implements Serializable {

    // ------------  queries ---------------------------------------------------
    public static final String BY_ID = "userDefinedSearch.byId";
    public static final String BY_MITARBEITER = "userDefinedSearch.byMitarbeiter";
    public static final String BY_NAME_MITARBEITER = "userDefinedSearch.byNameAndMitarbeiter";

    // ------------  fields ----------------------------------------------------
    @Id
    @SequenceGenerator(name = "userdefinedsearch_id_seq",
            sequenceName = "userdefinedsearch_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userdefinedsearch_id_seq")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Mitarbeiter mitarbeiter;

    private String name = "";

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<SearchFilter> searchFilterList;

    // ------------  constructors ----------------------------------------------
    public UserDefinedSearch() {
        this.searchFilterList = new ArrayList<>();
    }

    public UserDefinedSearch(Mitarbeiter mitarbeiter) {
        this.mitarbeiter = mitarbeiter;
        this.searchFilterList = new ArrayList<>();
    }

    public UserDefinedSearch(Mitarbeiter mitarbeiter, String name) {
        this.mitarbeiter = mitarbeiter;
        this.name = name;
        this.searchFilterList = new ArrayList<>();
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return " User Defined Search " + name + " von " + mitarbeiter.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        UserDefinedSearch that = (UserDefinedSearch) object;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(mitarbeiter, that.mitarbeiter)
                .append(name, that.name)
                .append(searchFilterList, that.searchFilterList)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(mitarbeiter)
                .append(name)
                .append(searchFilterList)
                .toHashCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Mitarbeiter getMitarbeiter() {
        return mitarbeiter;
    }

    public void setMitarbeiter(Mitarbeiter mitarbeiter) {
        this.mitarbeiter = mitarbeiter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SearchFilter> getSearchFilterList() {
        return searchFilterList;
    }

    public void setSearchFilterList(List<SearchFilter> searchFilterList) {
        this.searchFilterList = searchFilterList;
    }

} // end of class
