package de.interfaceag.bmw.pzbk.mail;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.exceptions.MailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author Christian Schauer
 */
public class BasicMail implements IMail, Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(BasicMail.class);

    private MimeMessage message;
    private MailContent content;
    private MailParticipants mp;

    public BasicMail(MimeMessage message, MailContent content, MailParticipants mp) {
        init(message, content, mp);
    }

    private void init(MimeMessage message, MailContent content, MailParticipants mp) {
        this.message = message;
        this.mp = mp;
        this.content = content;
    }

    public MimeMessage getMimeMessage() throws MailException {
        Multipart multipart = new MimeMultipart("alternative");
        MimeBodyPart messageBodyPart = new MimeBodyPart();

        try {
            message.setFrom(new InternetAddress(mp.getSender()));

            message.addRecipients(Message.RecipientType.TO, mp.getRecipientsArr());

            message.addRecipients(Message.RecipientType.CC, mp.getCcRecipientsArr());

            message.addRecipients(Message.RecipientType.BCC, mp.getBcRecipientsArr());

            message.setSubject(this.content.getSubject(), "UTF-8");
            messageBodyPart.setContent(content.getContent(), "text/html; charset=utf-8");

            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
        } catch (MessagingException ex) {
            LOG.error(null, ex);
            throw new MailException(ex.getMessage());
        }

        return message;
    }

    @Override
    public void send() throws MailException {
        LOG.info("Sending mail with subject >>{}<< to: {}", new Object[]{content.getSubject(), mp.getAllRecipients(",")});
        try {
            Transport.send(this.getMimeMessage());
        } catch (MailException | MessagingException ex) {
            LOG.error(null, ex);
            throw new MailException(ex.getMessage());
        }
    }

    public MimeMessage getMessage() {
        return message;
    }

    public void setMessage(MimeMessage message) {
        this.message = message;
    }

    public MailContent getContent() {
        return content;
    }

    public void setContent(MailContent content) {
        this.content = content;
    }

    @Override
    public void run() {
        try {
            this.send();
        } catch (MailException ex) {
            LOG.error("Error sending mail: {}", ex.getMessage(), ex);
        }
    }

    public void removeRecipientCompletely(Mitarbeiter mitarbeiter) {
        String email = mitarbeiter.getEmail();
        this.mp.removeRecipient(email);
        this.mp.removeCC(email);
        this.mp.removeBCC(email);
    }

    public int numberOfRecipients() {
        return this.mp.getNumberOfRecipients();
    }

}
