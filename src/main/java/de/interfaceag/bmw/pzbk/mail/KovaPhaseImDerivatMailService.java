package de.interfaceag.bmw.pzbk.mail;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Attribut;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.shared.mail.MailTemplatesService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungBerechtigungService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungService;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.mail.internet.MimeMessage;

@Dependent
public class KovaPhaseImDerivatMailService implements Serializable {

    @Inject
    private ConfigService configService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;
    @Inject
    private UmsetzungsbestaetigungBerechtigungService umsetzungsbestaetigungBerechtigungService;
    @Inject
    private MailService mailService;
    @Inject
    private MailTemplatesService mailTemplatesService;

    public void sendMailToUmsetzungsverwalter(KovAPhaseImDerivat kovAPhaseImDerivat, UmsetzungsverwalterMailTemplate mailTemplate) {

        if (kovAPhaseImDerivat == null || kovAPhaseImDerivat.getKovAPhase() == null) {
            return;
        }

        MailParticipants mailParticipants = getMailParticipantsForMailToUmsetzungsverwalter(kovAPhaseImDerivat);

        MimeMessage message = getNewMail();
        MailFactory mailFactory = getMailFactory(mailParticipants, message);
        Attribut attribut = getAttributForUmsetzungsvertwalterMail(mailTemplate);
        MailContent mailContent = getMailContent(attribut);

        MailArgs mailArgs = getMailArgsForUmsetzungsverwalterMail(kovAPhaseImDerivat);
        BasicMail mail = mailFactory.generateMailToUB(mailContent, mailArgs);

        sendMail(mail);
    }

    private static Attribut getAttributForUmsetzungsvertwalterMail(UmsetzungsverwalterMailTemplate mailTemplate) {
        Attribut attribut = null;
        switch (mailTemplate) {
            case BEWERTUNGSAUFTRAG:
                attribut = Attribut.MAILTEMPLATE_BEWERTUNGSAUFTRAGS_VORLAGE;
                break;
            case REMINDER:
                attribut = Attribut.MAILTEMPLATE_REMINDER_VORLAGE;
                break;
            case LASTCALL:
                attribut = Attribut.MAILTEMPLATE_LASTCALL_VORLAGE;
                break;
            default:
                break;
        }
        return attribut;
    }

    private MailArgs getMailArgsForUmsetzungsverwalterMail(KovAPhaseImDerivat kovAPhaseImDerivat) {
        String derivate = getDerivateForKovaPhaseImDerivat(kovAPhaseImDerivat);

        LocalDate phaseEndDate = kovAPhaseImDerivat.getEndDate();
        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        int weekNumber = phaseEndDate.get(weekFields.weekOfWeekBasedYear());
        int weekNumberOneWeekLater = weekNumber + 1;

        MailArgs mailArgs = new MailArgs();
        mailArgs.putValue("deriId", String.valueOf(kovAPhaseImDerivat.getDerivat().getId()));
        mailArgs.putValue("derivat", kovAPhaseImDerivat.getDerivat().getName());
        mailArgs.putValue("kovaPhase", kovAPhaseImDerivat.getKovAPhase().toString());
        mailArgs.putValue("kovaPhaseEnde", phaseEndDate.toString());
        mailArgs.putValue("derivate", derivate);
        mailArgs.putValue("kovaPhaseEndeKW", String.valueOf(weekNumber));
        mailArgs.putValue("kovaPhaseOneWeekLaterKW", String.valueOf(weekNumberOneWeekLater));
        return mailArgs;
    }

    private String getDerivateForKovaPhaseImDerivat(KovAPhaseImDerivat kovAPhaseImDerivat) {
        List<String> derivateList = derivatService.getDerivatNamenWithEndDatumOfPhase(kovAPhaseImDerivat.getKovAPhase(), kovAPhaseImDerivat.getEndDate());
        int size = derivateList.size();
        int counter = 0;
        StringBuilder sb = new StringBuilder();
        for (String dr : derivateList) {
            sb.append(dr);
            counter++;
            if (counter < size) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

    public void sendMailToSensorCocLeiterAndVertreter(KovAPhaseImDerivat kovAPhaseImDerivat) {
        MimeMessage message = getNewMail();

        MailParticipants mailParticipants = getMailParticipantsForMailToSensorCocLeiterAndVertreter(kovAPhaseImDerivat);
        MailFactory mailFactory = getMailFactory(mailParticipants, message);

        MailContent mailContent = getMailContent(Attribut.MAILTEMPLATE_MAIL_TO_FTS);
        MailArgs mailArgs = getMailArgsForSensorCocLeiterAndVertreterMail(kovAPhaseImDerivat);
        BasicMail basicMail = mailFactory.generateMailToUB(mailContent, mailArgs);

        sendMail(basicMail);
    }

    private void sendMail(BasicMail basicMail) {
        mailService.sendMailInThread(basicMail);
    }

    private MailArgs getMailArgsForSensorCocLeiterAndVertreterMail(KovAPhaseImDerivat kovAPhaseImDerivat) {
        LocalDate phaseStartDate = kovAPhaseImDerivat.getStartDate();
        LocalDate vorOneWeek = phaseStartDate.minusWeeks(1L);
        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        int weekNumber = vorOneWeek.get(weekFields.weekOfWeekBasedYear());

        MailArgs args = new MailArgs();
        args.putValue("deriId", String.valueOf(kovAPhaseImDerivat.getDerivat().getId()));
        args.putValue("derivat", kovAPhaseImDerivat.getDerivat().getName());
        args.putValue("kovaPhase", kovAPhaseImDerivat.getKovAPhase().toString());
        args.putValue("bisDatum", vorOneWeek.toString());
        args.putValue("bisEndeKW", String.valueOf(weekNumber));
        return args;
    }

    private MimeMessage getNewMail() {
        return mailService.createNewMail();
    }

    private MailFactory getMailFactory(MailParticipants mailParticipants, MimeMessage message) {
        return new MailFactory(message, mailParticipants);
    }

    private MailContent getMailContent(Attribut attribut) {
        return new MailContent(mailTemplatesService.getMailTemplate(attribut));
    }

    private MailParticipants getMailParticipantsForMailToUmsetzungsverwalter(KovAPhaseImDerivat kovAPhaseImDerivat) {
        if (isSendMailActive()) {
            return getUmsetzungverwalterAsMailParticipantsForKovaPhaseImDerivat(kovAPhaseImDerivat);
        } else {
            return getEmptyMailParticipants();
        }
    }

    private MailParticipants getUmsetzungverwalterAsMailParticipantsForKovaPhaseImDerivat(KovAPhaseImDerivat kovAPhaseImDerivat) {
        final Derivat derivat = kovAPhaseImDerivat.getDerivat();
        final KovAPhase phase = kovAPhaseImDerivat.getKovAPhase();
        List<Mitarbeiter> umsetzungsbestaetiger = umsetzungsbestaetigungBerechtigungService.getUBForDerivatAndPhase(derivat, phase);

        MailParticipants mailParticipants = getEmptyMailParticipants();
        mailParticipants.addRecipient(umsetzungsbestaetiger);
        return mailParticipants;
    }

    private MailParticipants getMailParticipantsForMailToSensorCocLeiterAndVertreter(KovAPhaseImDerivat kovAPhaseImDerivat) {
        if (isSendMailActive()) {
            return getSensorCocLeiterAndVertreterAsMailParticipantsForKovaPhaseImDerivat(kovAPhaseImDerivat);
        } else {
            return getEmptyMailParticipants();
        }
    }

    private boolean isSendMailActive() {
        return configService.isSendEmailsActivated();
    }

    private MailParticipants getSensorCocLeiterAndVertreterAsMailParticipantsForKovaPhaseImDerivat(KovAPhaseImDerivat kovAPhaseImDerivat) {
        MailParticipants mailParticipants = getEmptyMailParticipants();
        Collection<Mitarbeiter> sensorCoCLeiterAndVertreter = umsetzungsbestaetigungService.getSensorCocLeiterAndVertreterForKovaPhaseImDerivat(kovAPhaseImDerivat);
        if (sensorCoCLeiterAndVertreter != null) {
            mailParticipants.addRecipient(sensorCoCLeiterAndVertreter);
        }
        return mailParticipants;
    }

    private MailParticipants getEmptyMailParticipants() {
        return mailService.createMailParticipants();
    }

}
