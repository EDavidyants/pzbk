package de.interfaceag.bmw.pzbk.mail;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class PotentialMailParticipants implements IPotentialMailParticipants {

    private Map<Rolle, Set<String>> rolleEmailMap;
    private Set<String> detektoren = new HashSet<>();

    public PotentialMailParticipants() {
        init();
    }

    private void init() {
        this.rolleEmailMap = new HashMap<>();
    }

    @Override
    public String getEmailByRolle(Rolle rolle) {
        if (rolleEmailMap.containsKey(rolle)) {
            Set<String> mSet = rolleEmailMap.get(rolle);
            if (mSet != null && !mSet.isEmpty()) {
                Iterator<String> iter = mSet.iterator();
                if (iter.hasNext()) {
                    return iter.next();
                }
            }
        }
        return null;
    }

    @Override
    public void addMitarbeiter(Mitarbeiter mitarbeiter, Rolle rolle) {
        if (mitarbeiter != null && mitarbeiter.isMailReceiptEnabled()) {
            initSetForRole(rolle);
            Set<String> setM = this.rolleEmailMap.get(rolle);
            setM.add(mitarbeiter.getEmail());
        }
    }

    @Override
    public Set<String> getAllEmailsByRolle(Rolle rolle) {
        return rolleEmailMap.containsKey(rolle) ? rolleEmailMap.get(rolle) : null;
    }

    @Override
    public void addMitarbeiter(Collection<Mitarbeiter> mitarbeiters, Rolle rolle) {
        initSetForRole(rolle);
        Set<String> newEmails = mitarbeiters.stream()
                .filter(mitarbeiter -> mitarbeiter.isMailReceiptEnabled())
                .map(mitarbeiter -> mitarbeiter.getEmail())
                .collect(Collectors.toSet());
        this.rolleEmailMap.get(rolle).addAll(newEmails);
    }

    private void initSetForRole(Rolle rolle) {
        if (this.rolleEmailMap.get(rolle) == null) {
            this.rolleEmailMap.put(rolle, new HashSet<>());
        }
    }

    @Override
    public Set<String> getAllEmails() {
        Set<String> result = new HashSet<>();
        Iterator<Rolle> iter = this.rolleEmailMap.keySet().iterator();
        while (iter.hasNext()) {
            result.addAll(rolleEmailMap.get(iter.next()));
        }
        return result;
    }

    @Override
    public void addDetektor(String mail) {
        this.detektoren.add(mail);
    }

    @Override
    public void addDetektoren(Collection<String> emails) {
        Iterator<String> mailIterator = emails.iterator();
        while (mailIterator.hasNext()) {
            addDetektor(mailIterator.next());
        }
    }

    @Override
    public Set<String> getDetektoren() {
        return detektoren;
    }

}
