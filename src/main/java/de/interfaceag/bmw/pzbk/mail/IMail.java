package de.interfaceag.bmw.pzbk.mail;

import de.interfaceag.bmw.pzbk.exceptions.MailException;

/**
 *
 * @author Christian Schauer
 */
public interface IMail {

    void send() throws MailException;
}
