package de.interfaceag.bmw.pzbk.mail;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Christian Schauer
 */
public class MailArgs {

    private final Map<String, String> argMap;

    public MailArgs() {
        argMap = new HashMap<>();
    }

    public String getValueFor(String key) {
        return argMap.get(key);
    }

    public void putValue(String key, String value) {
        this.argMap.put(key, value);
    }

    public int size() {
        return argMap.size();
    }

    public String[] toStringArr() {
        String[] result = new String[this.argMap.size()];
        Iterator<Map.Entry<String, String>> iter = argMap.entrySet().iterator();
        int i = 0;
        while (iter.hasNext()) {
            result[i] = iter.next().getValue();
            i++;
        }
        return result;
    }

    public Map<String, String> getArgMap() {
        return argMap;
    }

}
