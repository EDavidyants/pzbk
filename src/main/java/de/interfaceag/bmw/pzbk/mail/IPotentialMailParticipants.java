package de.interfaceag.bmw.pzbk.mail;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.Collection;
import java.util.Set;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public interface IPotentialMailParticipants {

    String getEmailByRolle(Rolle rolle);

    Set<String> getDetektoren();

    Set<String> getAllEmailsByRolle(Rolle rolle);

    Set<String> getAllEmails();

    void addMitarbeiter(Mitarbeiter mitarbeiter, Rolle rolle);

    void addDetektor(String mail);

    void addDetektoren(Collection<String> emails);

    void addMitarbeiter(Collection<Mitarbeiter> mitarbeiters, Rolle rolle);
}
