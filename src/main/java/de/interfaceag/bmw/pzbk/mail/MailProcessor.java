package de.interfaceag.bmw.pzbk.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *
 * @author Christian Schauer
 */
public class MailProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(MailProcessor.class.getName());

    private final char varMark;

    public MailProcessor(char varMark) {
        this.varMark = varMark;
    }

    public String generateContent(InputStream is, MailArgs args) {
        StringBuilder output = new StringBuilder();
        String line;

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            while ((line = br.readLine()) != null) {
                output.append(replaceByOrder(line, args));
            }
        } catch (IOException ex) {
            LOG.error(null, ex);
        }

        return output.toString();
    }

    public String generateContent(String text, MailArgs args) {
        return replaceByOrder(text, args);
    }

    private boolean isVar(String text, int pos) {
        if (pos + 1 < text.length()) {
            char c1 = text.charAt(pos);
            char c2 = text.charAt(pos + 1);
            return c1 == this.varMark && c2 == '{';
        } else {
            return false;
        }
    }

    public String replaceByOrder(String text, MailArgs args) {
        if (text.length() < 1) {
            return "";
        }

        StringBuilder output = new StringBuilder();

        int len = text.length();

        for (int i = 0; i < len; i++) {
            char c1 = text.charAt(i);

            if (isVar(text, i)) {
                i++;
                String tmpTxt = processBracket(text, i);
                if (tmpTxt.length() > 0) {
                    output.append(args.getValueFor(tmpTxt));
                    // +1 for final bracket
                    i += tmpTxt.length() + 1;
                }
            } else {
                output.append(c1);
            }
        }
        return output.toString();
    }

    private String processBracket(String text, int start) {
        int end = -1;
        if (text.charAt(start) != '{') {
            return "";
        } else {
            for (int i = start + 1; i < text.length(); i++) {
                if (text.charAt(i) == '}') {
                    end = i;
                    break;
                }
            }
        }
        if (end > start + 1) {
            return text.substring(start + 1, end);
        } else {
            return "";
        }
    }

    public static int parseInt(String text, int start) {
        int end = 0;
        for (int i = start; i < text.length(); i++) {
            if (!Character.isDigit(text.charAt(i))) {
                break;
            }
            end = i + 1;
        }
        String numStr = text.substring(start, end);
        int result = Integer.parseInt(numStr);
        return result;
    }
}
