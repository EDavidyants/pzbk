package de.interfaceag.bmw.pzbk.mail;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class AttrChangeComparatorReverse implements Comparator<AttrChange>, Serializable {

    @Override
    public int compare(AttrChange a1, AttrChange a2) {
        //int result = -a1.compareTo(a2)
        int result = a2.compareTo(a1);
        // TreeSet verliert wohl sonst einen "gleichen" Wert. Bug?
        if (result == 0) {
            result = 1;
        }
        return result;
    }
}
