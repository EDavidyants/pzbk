package de.interfaceag.bmw.pzbk.mail;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Christian Schauer
 */
public class MailParticipants {

    private static final Logger LOG = LoggerFactory.getLogger(MailParticipants.class);

    private final String sender;
    private Set<String> recipients;
    private Set<String> ccRecipients;
    private Set<String> bccRecipients;

    public MailParticipants() {
        this.sender = "";
        init();
    }

    public MailParticipants(String sender) {
        this.sender = sender;
        init();
    }

    private void init() {
        this.recipients = new HashSet<>();
        this.ccRecipients = new HashSet<>();
        this.bccRecipients = new HashSet<>();
    }

    public void addRecipient(Collection<Mitarbeiter> mitarbeiterList) {
        for (Mitarbeiter mitarbeiter : mitarbeiterList) {
            addRecipient(mitarbeiter);
        }
    }

    public void addRecipient(Mitarbeiter mitarbeiter) {
        if (mitarbeiter != null && mitarbeiter.getEmail() != null) {
            this.recipients.add(mitarbeiter.getEmail());
        }
    }

    public void addRecipient(String mail) {
        if (mail != null) {
            this.recipients.add(mail);
        }
    }

    public void addRecipientMail(Collection<String> mCollection) {
        if (recipients != null && mCollection != null) {
            mCollection.stream().filter(Objects::nonNull)
                    .forEach(m -> recipients.add(m));
        }
    }

    public void addCC(String mail) {
        if (mail != null) {
            this.ccRecipients.add(mail);
        }
    }

    public void addCC(Collection<String> mails) {
        this.ccRecipients.addAll(mails);
    }

    public void addBCC(String bbcRecipient) {
        if (bbcRecipient != null) {
            this.bccRecipients.add(bbcRecipient);
        }
    }

    public void removeRecipient(String mail) {
        this.recipients.remove(mail);
    }

    public void removeCC(String mail) {
        this.ccRecipients.remove(mail);
    }

    public void removeBCC(String mail) {
        this.bccRecipients.remove(mail);
    }

    public Address[] getRecipientsArr() {
        return mitarbeiterListToAddressArray(recipients);
    }

    public Address[] getCcRecipientsArr() {
        return mitarbeiterListToAddressArray(ccRecipients);
    }

    public Address[] getBcRecipientsArr() {
        return mitarbeiterListToAddressArray(bccRecipients);
    }

    public String getAllRecipients(String separator) {
        return mitarbeiterCollToSeparatedMailString(getAllRecipients(), separator);
    }

    public Collection<String> getAllRecipients() {
        Set<String> all = new HashSet<>();
        all.addAll(recipients);
        all.addAll(ccRecipients);
        all.addAll(bccRecipients);
        return all;
    }

    private static InternetAddress convertToEmailAddress(String email) {
        InternetAddress emailAddr = null;
        if (email != null) {
            try {
                emailAddr = new InternetAddress(email);
            } catch (AddressException ex) {
                LOG.error(null, ex);
            }
        }
        return emailAddr;
    }

    public String mitarbeiterCollToSeparatedMailString(Collection<String> mColl, String separator) {
        return mColl.stream().collect(Collectors.joining(separator));
    }

    private Address[] mitarbeiterListToAddressArray(Set<String> mList) {
        List<Address> result = new ArrayList<>();
        Iterator<String> iter = mList.iterator();
        while (iter.hasNext()) {
            InternetAddress emailAddr = convertToEmailAddress(iter.next());
            result.add(emailAddr);
        }
        Address[] resultArr = new Address[result.size()];
        return result.toArray(resultArr);
    }

    public String getSender() {
        return sender;
    }

    public Set<String> getRecipients() {
        return recipients;
    }

    public Set<String> getCcRecipients() {
        return ccRecipients;
    }

    public Set<String> getBccRecipients() {
        return bccRecipients;
    }

    public int getNumberOfRecipients() {
        return this.recipients.size();
    }

}
