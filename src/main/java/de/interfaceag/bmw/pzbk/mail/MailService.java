package de.interfaceag.bmw.pzbk.mail;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.AbstractAnforderung;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Attribut;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.shared.mail.MailTemplatesService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungBerechtigungService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungService;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Christian Schauer
 */
@Stateless
@Named
public class MailService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(MailService.class);

    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private ConfigService cs;
    @Inject
    private DerivatService ds;
    @Inject
    private DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    private AnforderungMeldungHistoryService hs;
    @Inject
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;
    @Inject
    private UmsetzungsbestaetigungBerechtigungService umsetzungsbestaetigungBerechtigungService;
    @Inject
    private MailTemplatesService mailTemplatesService;

    @Resource(lookup = "pzbkMail")
    private Session mailSession;

    public static final String MAILTEMPLATES_BASE_PATH = "/MailTemplates/";
    private static final String SENDER = "t-am@bmw.de";

    private String getApplicationUrl() {
        String result = cs.getUrlPrefix();
        if (result == null || result.isEmpty()) {
            HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            String anfUrl = req.getRequestURL().toString();
            int lastSlash = anfUrl.lastIndexOf("/");
            result = anfUrl.substring(0, lastSlash);
        }
        return result;
    }

    public void sendMailAnforderungChanged(AbstractAnforderung anf, Mitarbeiter user) {
        if (mailSession != null && anf.getSensorCoc() != null) {
            MimeMessage message = new MimeMessage(mailSession);
            // Schwellwert in Millisekunden um nur die aktuellen Änderungen zu betrachten, und nicht die letzten Änderungen generell,
            // da man ja auch speichern kann ohne etwas zu ändern.
            Long marginDate = 10000L;
            Date currentDate = new Date(System.currentTimeMillis());

            Collection<AttrChange> lastChanges = AttrChange.parseLastChanges(hs.getHistoryForAnforderung(anf.getId(), anf.getKennzeichen()), currentDate, marginDate);

            IPotentialMailParticipants pmp = getPotentialMailParticipants(anf);
            MailFactory mf = new MailFactory(message, anf, pmp, getSenderMailAdress());

            String appUrl = getApplicationUrl();
            if (AttrChange.hasAttributeChanged("status", lastChanges, false)) {
                MailContent mcStatusChanged = new MailContent(mailTemplatesService.getMailTemplate(Attribut.MAILTEMPLATE_STATUS_CHANGED));
                BasicMail mailStatusChaned = mf.generateMailStatusChanged(mcStatusChanged, lastChanges, appUrl, anf.getStatus().getStatusMarker());
                sendMailInThread(mailStatusChaned, user);
            } else {
                // Else deswegen da beim Statuswechsel selber auch der Inhalt der Anforderung editierbar ist

                // If bestimmte Felder changed (Attributnamen aus AnforderungChangeListener)
                String[] relevantAttributes = new String[]{"Umsetzer", "Hauptumsetzer", "Beschreibung DE", "Kommentar"};
                Collection<AttrChange> relevantChanges = AttrChange.getAttributes(lastChanges, false, relevantAttributes);
                if (!relevantChanges.isEmpty() && anf.getKennzeichen().equals("A")) {
                    MailContent mcAttrChanged = new MailContent(mailTemplatesService.getMailTemplate(Attribut.MAILTEMPLATE_ATTR_CHANGED));
                    BasicMail mailAttrChanged = mf.generateMailAttrChanged(mcAttrChanged, relevantChanges, appUrl);
                    sendMailInThread(mailAttrChanged, user);
                }
            }
        } else if (anf.getSensorCoc() != null) { // error messages
            LOG.warn("The JavaMail Session is not configured");
        } else {
            LOG.warn("The SensorCoc value is invalid");
        }
    }

    private IPotentialMailParticipants getPotentialMailParticipants(AbstractAnforderung abstractAnforderung) {
        IPotentialMailParticipants result = new PotentialMailParticipants();
        result.addMitarbeiter(abstractAnforderung.getSensor(), Rolle.SENSOR);
        Optional<Mitarbeiter> sensorCocLeiter = berechtigungService.getSensorCoCLeiterOfSensorCoc(abstractAnforderung.getSensorCoc());
        if (sensorCocLeiter.isPresent()) {
            result.addMitarbeiter(sensorCocLeiter.get(), Rolle.SENSORCOCLEITER);
        }

        if (abstractAnforderung instanceof Anforderung) {
            Anforderung anforderung = (Anforderung) abstractAnforderung;

            result.addMitarbeiter(berechtigungService.getEcocForAnforderung(anforderung), Rolle.E_COC);
            if (anforderung.getTteam() != null) {
                berechtigungService.getTteamleiterOfTteam(anforderung.getTteam())
                        .ifPresent(t -> result.addMitarbeiter(t, Rolle.T_TEAMLEITER));
            }
            if (anforderung.getStatus() == Status.A_FREIGEGEBEN) {
                result.addMitarbeiter(derivatAnforderungModulService.getAnfordererForDerivatenOfAnforderung(anforderung), Rolle.ANFORDERER);
            }
        }

        List<String> detektoren = abstractAnforderung.getDetektoren();
        result.addDetektoren(detektoren);

        return result;
    }

    /**
     * modus value 1 == case zak, value 0 == case ub
     *
     * @param derivatAnforderungModul derivatanforderungmodul instance
     * @param modus value 1 == zak, value 0 == ub
     */
    public void sendMailToAnforderer(DerivatAnforderungModul derivatAnforderungModul, Boolean modus) {
        MailParticipants mailParticipants = selectAnfordererAsMailParticipants(derivatAnforderungModul);
        if (mailSession != null && mailParticipants != null) {
            Attribut templateName;
            if (modus) {
                templateName = Attribut.MAILTEMPLATE_MAIL_TO_ANFORDERER_ZAK;
            } else {
                templateName = Attribut.MAILTEMPLATE_MAIL_TO_ANFORDERER_UB;
            }

            MailArgs args = new MailArgs();
            args.putValue("anforderung", derivatAnforderungModul.getAnforderung().toString());
            args.putValue("modul", ""); // derivatAnforderungModul.getModul().getName()); // TODO: activate modul
            args.putValue("derivat", derivatAnforderungModul.getDerivat().getName());
            args.putValue("status", derivatAnforderungModul.getStatusZV().toString());
            if (modus) {
                args.putValue("konfliktStatus", derivatAnforderungModul.getZakStatus().toString());
            } else {
                args.putValue("konfliktStatus", derivatAnforderungModul.getUmsetzungsBestaetigungStatus().toString());
            }
            args.putValue("anwendungsUrl", getApplicationUrl());
            args.putValue("id", derivatAnforderungModul.getId().toString());
            args.putValue("umsetzer", derivatAnforderungModul.getModul().getName());

            MimeMessage message = new MimeMessage(mailSession);
            MailFactory mf = new MailFactory(message, mailParticipants);
            MailContent mc = new MailContent(mailTemplatesService.getMailTemplate(templateName));
            BasicMail basicMail = mf.generateMailToUB(mc, args);
            sendMailInThread(basicMail);
        }
    }

    public void sendMailInThread(BasicMail mail) {
        if (cs.isSendEmailsActivated()) {
            Thread t = new Thread(mail);
            t.start();
        } else {
            LOG.info("Sending mails is disabled. For activation switch the attribute 'sendMail' in the settings.properties to 'on'");
        }
    }

    public void sendMailInThread(BasicMail mail, Mitarbeiter userToIgnore) {
        if (cs.isSendEmailsActivated()) {
            mail.removeRecipientCompletely(userToIgnore);
            if (mail.numberOfRecipients() > 0) {
                Thread t = new Thread(mail);
                t.start();
            }
        } else {
            LOG.info("Sending mails is disabled. For activation switch the attribute 'sendMail' in the settings.properties to 'on'");
        }
    }

    private String getSenderMailAdress() {
        return SENDER;
    }

    private MailParticipants selectAnfordererAsMailParticipants(DerivatAnforderungModul derivatAnforderungModul) {
        MailParticipants mailParticipants = new MailParticipants(getSenderMailAdress());
        if (cs.isSendEmailsActivated() && derivatAnforderungModul.getAnforderer() != null) {
            mailParticipants.addRecipient(derivatAnforderungModul.getAnforderer());
        }
        return mailParticipants;
    }

    public MimeMessage createNewMail() {
        return new MimeMessage(mailSession);
    }

    public MailParticipants createMailParticipants() {
        return new MailParticipants(getSenderMailAdress());
    }

}
