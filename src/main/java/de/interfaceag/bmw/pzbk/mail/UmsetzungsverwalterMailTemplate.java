package de.interfaceag.bmw.pzbk.mail;

/**
 *
 * @author evda
 */
public enum UmsetzungsverwalterMailTemplate {

    BEWERTUNGSAUFTRAG, REMINDER, LASTCALL;

}
