package de.interfaceag.bmw.pzbk.mail;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungHistoryDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class AttrChange implements Comparable<AttrChange> {

    private static final Logger LOG = LoggerFactory.getLogger(AttrChange.class.getName());

    private Date date;
    private String person;
    private String attrName;
    private String newVal;
    private String oldVal;
    private String id;

    public AttrChange(AnforderungHistoryDto anforderungHistory) {
        this.id = String.valueOf(anforderungHistory.getAnforderungId());
        this.date = parseDate(anforderungHistory.getZeitpunkt());
        this.person = anforderungHistory.getBenutzer();
        this.attrName = anforderungHistory.getAttribut();
        this.oldVal = anforderungHistory.getVon();
        this.newVal = anforderungHistory.getAuf();
    }

    public static Collection<AttrChange> getAttributes(Collection<AttrChange> changes, boolean caseSensitive, String... attrsToAdd) {
        Collection<AttrChange> result = new ArrayList<>();
        for (AttrChange change : changes) {
            String attrName = change.getAttrName();
            if (!caseSensitive) {
                attrName = attrName.toLowerCase();
                for (int i = 0; i < attrsToAdd.length; i++) {
                    attrsToAdd[i] = attrsToAdd[i].toLowerCase();
                }
            }
            for (String attrToAdd : attrsToAdd) {
                if (attrToAdd.equals(attrName)) {
                    result.add(change);
                }
            }
        }
        return result;
    }

    private Date parseDate(String input) {
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date result;
        try {
            result = f.parse(input);
        } catch (ParseException ex) {
            LOG.error(null, ex);
            return new Date(0);
        }
        return result;
    }

    public static Collection<AttrChange> parseLastChanges(List<AnforderungHistoryDto> input, Date changesFromDate, Long dateMarginInMillis) {
        SortedSet<AttrChange> attrChanges = new TreeSet<>(new AttrChangeComparator());
        for (AnforderungHistoryDto history : input) {
            AttrChange a = new AttrChange(history);
            //wieso das mit der Zeit, Absolute Werte sind doch dann meisten größer
            long tmp = Math.abs(a.getDate().getTime() - changesFromDate.getTime());
            if (tmp < dateMarginInMillis) {
                attrChanges.add(new AttrChange(history));
            }
        }
        return getLastChanges(attrChanges);
    }

    public static Collection<AttrChange> getLastChanges(Iterable<AttrChange> input) {
        SortedSet<AttrChange> sortedChanges = new TreeSet<>(new AttrChangeComparatorReverse());
        for (AttrChange aChange : input) {
            sortedChanges.add(aChange);
        }
        Iterator<AttrChange> iter = sortedChanges.iterator();
        AttrChange ref = null;
        AttrChange tmpChange;

        List<AttrChange> result = new ArrayList<>();
        boolean finished = false;

        if (iter.hasNext()) {
            ref = iter.next();
            result.add(ref);
        }

        while (iter.hasNext() && !finished) {
            tmpChange = iter.next();
            if (tmpChange.compareTo(ref) == 0) {
                result.add(tmpChange);
            } else {
                finished = true;
            }
        }
        return result;
    }

    public static boolean hasAttributeChanged(String name, Collection<AttrChange> changes, boolean caseSensitive) {
        if (caseSensitive) {
            for (AttrChange change : changes) {
                if (name.equals(change.getAttrName())) {
                    return true;
                }
            }
        } else {
            String attrName = name.toLowerCase();
            for (AttrChange change : changes) {
                if (attrName.equals(change.getAttrName().toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static AttrChange getChangeByAttr(String name, Collection<AttrChange> changes, boolean caseSensitive) {
        String attrName;
        if (caseSensitive) {
            attrName = name;
        } else {
            attrName = name.toLowerCase();
        }

        String changeAttrName;
        for (AttrChange change : changes) {
            if (caseSensitive) {
                changeAttrName = change.attrName;
            } else {
                changeAttrName = change.attrName.toLowerCase();
            }

            if (attrName.equals(changeAttrName)) {
                return change;
            }
        }
        return null;
    }

    @Override
    public int compareTo(AttrChange attrChange) {
        return this.date.compareTo(attrChange.getDate());
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return new Date(date.getTime());
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = new Date(date.getTime());
    }

    /**
     * @return the attrName
     */
    public String getAttrName() {
        return attrName;
    }

    /**
     * @return the newVal
     */
    public String getNewVal() {
        return newVal;
    }

    /**
     * @return the oldVal
     */
    public String getOldVal() {
        return oldVal;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the person
     */
    public String getPerson() {
        return person;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.date);
        hash = 79 * hash + Objects.hashCode(this.person);
        hash = 79 * hash + Objects.hashCode(this.attrName);
        hash = 79 * hash + Objects.hashCode(this.newVal);
        hash = 79 * hash + Objects.hashCode(this.oldVal);
        hash = 79 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AttrChange other = (AttrChange) obj;
        if (!Objects.equals(this.person, other.person)) {
            return false;
        }
        if (!Objects.equals(this.attrName, other.attrName)) {
            return false;
        }
        if (!Objects.equals(this.newVal, other.newVal)) {
            return false;
        }
        if (!Objects.equals(this.oldVal, other.oldVal)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return Objects.equals(this.date, other.date);
    }

}
