package de.interfaceag.bmw.pzbk.mail;

/**
 *
 * @author Christian Schauer
 */
public class MailContent {

    private String subject;
    private String content;

    public MailContent(String subject, String content) {
        this.subject = subject;
        this.content = content;
    }

    public MailContent(String content) {
        this.content = content;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

}
