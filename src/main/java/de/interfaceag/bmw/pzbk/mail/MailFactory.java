package de.interfaceag.bmw.pzbk.mail;

import de.interfaceag.bmw.pzbk.entities.AbstractAnforderung;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.shared.utils.HtmlUtils;

import javax.mail.internet.MimeMessage;
import java.util.Collection;

/**
 * @author Christian Schauer
 */
public class MailFactory {

    private MimeMessage message;
    private AbstractAnforderung a;
    private IPotentialMailParticipants pmp;
    private MailParticipants mp;

    public MailFactory(MimeMessage message, AbstractAnforderung abstractAnforderung, IPotentialMailParticipants pmp, String senderAddress) {
        this.message = message;
        this.a = abstractAnforderung;
        this.pmp = pmp;
        this.mp = new MailParticipants(senderAddress);
    }

    public MailFactory(MimeMessage message, MailParticipants mp) {
        this.message = message;
        this.mp = mp;
    }

    public BasicMail generateMailStatusChanged(MailContent mc, Collection<AttrChange> changes, String anfUrl, String marker) {
        MailArgs args = new MailArgs();

        AttrChange statusChange = AttrChange.getChangeByAttr("status", changes, false);

        Status oldStatus = Status.getStatusByBezeichnungAndMarker(statusChange.getOldVal(), marker);
        Status newStatus = Status.getStatusByBezeichnungAndMarker(statusChange.getNewVal(), marker);
        Status.setMailParticipantsByStatusChange(oldStatus, newStatus, a, mp, pmp);

        args.putValue("fachId", a.getFachId());
        args.putValue("bearbeitetDurch", statusChange.getPerson());
        args.putValue("alterStatus", statusChange.getOldVal());
        args.putValue("neuerStatus", a.getStatus().toString());
        args.putValue("statusWechselKommentar", a.getStatusWechselKommentar());
        args.putValue("beschreibung", HtmlUtils.insertHtmlLineBreaks(a.getBeschreibungAnforderungDe()));
        args.putValue("beschreibungEn", HtmlUtils.insertHtmlLineBreaks(a.getBeschreibungAnforderungEn()));
        Mitarbeiter sensor = a.getSensor();
        if (sensor != null) {
            args.putValue("sensor", sensor.getFullName() + ", " + sensor.getAbteilung()); //"Rudolf xxxx, TI-xxxx, +49-89-xxxxx";
        } else {
            args.putValue("sensor", "");
        }
        args.putValue("technologie", a.getSensorCoc().getTechnologie()); //"TGF_Vorder- und Hinterachse";
        args.putValue("ressort", a.getSensorCoc().getRessort()); //"T-Ressort";
        args.putValue("ortung", a.getSensorCoc().getOrtung()); //"T";
        args.putValue("anwendungsUrl", anfUrl);

        final Long id = getObjId(a);
        if (id == null) {
            args.putValue("id", "");
        } else {
            args.putValue("id", id.toString());
        }

        if (a.getKennzeichen().equals("A")) {
            Anforderung anforderung = (Anforderung) a;
            args.putValue("anforderungstyp", "Anforderung");
            args.putValue("version", anforderung.getVersion().toString());
            args.putValue("anforderungstypKlein", "anforderung");
            args.putValue("versionText", " | V" + anforderung.getVersion().toString());
        } else {
            args.putValue("versionsParameter", "");
            args.putValue("anforderungstyp", "Meldung");
            args.putValue("anforderungstypKlein", "meldung");
            args.putValue("versionText", "");
            args.putValue("version", "null");
        }
        String contentStr = new MailProcessor('$').generateContent(mc.getContent(), args);

        MailContent content = new MailContent(HtmlUtils.getTitle(contentStr), contentStr);
        return new BasicMail(message, content, mp);
    }

    public BasicMail generateMailAttrChanged(MailContent mc, Collection<AttrChange> changes, String anfUrl) {
        Status.setMailParticipantsForEditedByStatus(a.getStatus(), mp, pmp);
        String changesTableContent = statusChangesToTableContent(changes);
        String changedBy = "";
        if (!changes.isEmpty()) {
            changedBy = changes.iterator().next().getPerson();
        }

        MailArgs args = new MailArgs();
        args.putValue("fachId", a.getFachId());
        args.putValue("bearbeitetDurch", changedBy);
        args.putValue("bearbeiteteAttribute", changesTableContent);
        if (a.getSensor() != null) {
            args.putValue("sensor", a.getSensor().getFullName() + ", " + a.getSensor().getAbteilung()); //"Rudolf xxxx, TI-xxxx, +49-89-xxxxx";
        } else {
            args.putValue("sensor", "");
        }
        args.putValue("beschreibung", HtmlUtils.insertHtmlLineBreaks(a.getBeschreibungAnforderungDe()));
        args.putValue("beschreibungEn", HtmlUtils.insertHtmlLineBreaks(a.getBeschreibungAnforderungEn()));
        args.putValue("technologie", a.getSensorCoc().getTechnologie()); //"TGF_Vorder- und Hinterachse";
        args.putValue("anwendungsUrl", anfUrl);
        final Long id = getObjId(a);
        if (id == null) {
            args.putValue("id", "");
        } else {
            args.putValue("id", id.toString());
        }

        if (a.getKennzeichen().equals("A")) {
            Anforderung anforderung = (Anforderung) a;
            args.putValue("anforderungstyp", "Anforderung");
            args.putValue("anforderungstypKlein", "anforderung");
            args.putValue("version", anforderung.getVersion().toString());
        } else {
            args.putValue("versionsParameter", "");
            args.putValue("anforderungstyp", "Meldung");
            args.putValue("anforderungstypKlein", "meldung");
            args.putValue("versionText", "");
            args.putValue("version", "null");
        }
        String contentStr = new MailProcessor('$').generateContent(mc.getContent(), args);
        MailContent content = new MailContent(HtmlUtils.getTitle(contentStr), contentStr);
        return new BasicMail(message, content, mp);
    }

    public BasicMail generateMailToUB(MailContent mc, MailArgs args) {
        MailProcessor mproc = new MailProcessor('$');
        String contentStr = mproc.generateContent(mc.getContent(), args);
        MailContent content = new MailContent(HtmlUtils.getTitle(contentStr), contentStr);
        return new BasicMail(message, content, mp);
    }

    private static String statusChangesToTableContent(Collection<AttrChange> changes) {
        StringBuilder sb = new StringBuilder();
        changes.forEach(change ->
                sb.append(HtmlUtils.createTableRow(change.getAttrName(), change.getOldVal(), change.getNewVal()))
        );
        return sb.toString();
    }

    public MimeMessage getMessage() {
        return message;
    }

    public void setMessage(MimeMessage message) {
        this.message = message;
    }

    public AbstractAnforderung getA() {
        return a;
    }

    public void setA(AbstractAnforderung abstractAnforderung) {
        this.a = abstractAnforderung;
    }

    public IPotentialMailParticipants getPmp() {
        return pmp;
    }

    public void setPmp(IPotentialMailParticipants pmp) {
        this.pmp = pmp;
    }

    private Long getObjId(AbstractAnforderung anfo) {
        Long aId = null;
        if (anfo instanceof Meldung) {
            aId = ((Meldung) anfo).getId();
        } else if (anfo instanceof Anforderung) {
            aId = ((Anforderung) anfo).getId();
        }

        return aId;
    }

} // end of class
