package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.entities.AnforderungReadData;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.enums.Status;

import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class AnforderungTitelbildValidator {

    private AnforderungTitelbildValidator() {
    }

    public static boolean isValid(AnforderungReadData anforderung, Anhang anhangForBild) {
        // null is always false
        if (anforderung == null) {
            return Boolean.FALSE;
        }

        // if it is not required return true
        if (!isTitelbildRequiredForStatus(anforderung.getStatus())) {
            return Boolean.TRUE;
        }

        // anhaenge cant be empty as one must be selected
        if (anforderung.getAnhaenge().isEmpty()) {
            return Boolean.FALSE;
        }

        // check if anhang object is not null
        if (anhangForBild == null) {
            return Boolean.FALSE;
        }

        // it it has not failed up to this point it should be valid
        return Boolean.TRUE;
    }

    protected static boolean isTitelbildRequiredForStatus(Status status) {
        return getAnhangRequiredStatus().anyMatch(s -> s.equals(status));
    }

    protected static Stream<Status> getAnhangRequiredStatus() {
        return Stream.of(
                Status.A_INARBEIT,
                Status.A_UNSTIMMIG,
                Status.A_FTABGESTIMMT,
                Status.A_PLAUSIB,
                Status.A_FREIGEGEBEN);
    }

}
