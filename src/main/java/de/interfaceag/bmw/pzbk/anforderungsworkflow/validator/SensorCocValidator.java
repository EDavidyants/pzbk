package de.interfaceag.bmw.pzbk.anforderungsworkflow.validator;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class SensorCocValidator {

    private SensorCocValidator() {
    }

    public static void validateSensorCoc(FacesContext context, UIComponent component, Object value, SensorCoc sensorCoc) {
        String summary = "SensorCoc: no message defined";
        if (value instanceof String) {
            summary = (String) value;
        }
        String detail = "----------kein SensorCoC-----------";
        if (sensorCoc == null || sensorCoc.equals(new SensorCoc())) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
        }
    }

}
