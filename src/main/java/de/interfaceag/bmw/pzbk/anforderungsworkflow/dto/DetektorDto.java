package de.interfaceag.bmw.pzbk.anforderungsworkflow.dto;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DetektorDto implements Serializable {

    private String value;

    public DetektorDto(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
