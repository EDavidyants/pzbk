package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.converters.DomainObjectConverter;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.faces.convert.Converter;
import java.util.List;
import java.util.StringJoiner;

public class AnforderungEditFahrzeugmerkmalDto implements AnforderungEditFahrzeugmerkmal {

    private final FahrzeugmerkmalId fahrzeugmerkmalId;
    private final String merkmal;

    private final List<AnforderungEditFahrzeugmerkmalAuspraegung> allFahrzeugmerkmalAuspraegungen;

    private List<AnforderungEditFahrzeugmerkmalAuspraegung> selectedFahrzeugmerkmalAuspraegungen;

    public AnforderungEditFahrzeugmerkmalDto(FahrzeugmerkmalId fahrzeugmerkmalId, String merkmal,
                                             List<AnforderungEditFahrzeugmerkmalAuspraegung> allFahrzeugmerkmalAuspraegungen,
                                             List<AnforderungEditFahrzeugmerkmalAuspraegung> selectedFahrzeugmerkmalAuspraegungen) {
        this.fahrzeugmerkmalId = fahrzeugmerkmalId;
        this.merkmal = merkmal;
        this.allFahrzeugmerkmalAuspraegungen = allFahrzeugmerkmalAuspraegungen;
        this.selectedFahrzeugmerkmalAuspraegungen = selectedFahrzeugmerkmalAuspraegungen;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", AnforderungEditFahrzeugmerkmalDto.class.getSimpleName() + "[", "]")
                .add("fahrzeugmerkmalId=" + fahrzeugmerkmalId)
                .add("merkmal='" + merkmal + "'")
                .add("allFahrzeugmerkmalAuspraegungen=" + allFahrzeugmerkmalAuspraegungen)
                .add("selectedFahrzeugmerkmalAuspraegungen=" + selectedFahrzeugmerkmalAuspraegungen)
                .toString();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        AnforderungEditFahrzeugmerkmalDto that = (AnforderungEditFahrzeugmerkmalDto) object;

        return new EqualsBuilder()
                .append(fahrzeugmerkmalId, that.fahrzeugmerkmalId)
                .append(merkmal, that.merkmal)
                .append(allFahrzeugmerkmalAuspraegungen, that.allFahrzeugmerkmalAuspraegungen)
                .append(selectedFahrzeugmerkmalAuspraegungen, that.selectedFahrzeugmerkmalAuspraegungen)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(fahrzeugmerkmalId)
                .append(merkmal)
                .append(allFahrzeugmerkmalAuspraegungen)
                .append(selectedFahrzeugmerkmalAuspraegungen)
                .toHashCode();
    }

    @Override
    public FahrzeugmerkmalId getFahrzeugmerkmalId() {
        return fahrzeugmerkmalId;
    }

    @Override
    public String getMerkmal() {
        return merkmal;
    }

    @Override
    public List<AnforderungEditFahrzeugmerkmalAuspraegung> getAllAuspraegungen() {
        return allFahrzeugmerkmalAuspraegungen;
    }

    @Override
    public List<AnforderungEditFahrzeugmerkmalAuspraegung> getSelectedAuspraegungen() {
        return selectedFahrzeugmerkmalAuspraegungen;
    }

    @Override
    public void setSelectedAuspraegungen(List<AnforderungEditFahrzeugmerkmalAuspraegung> selectedAuspraegungen) {
        this.selectedFahrzeugmerkmalAuspraegungen = selectedAuspraegungen;
    }

    @Override
    public Converter getAuspraegungConverter() {
        return new DomainObjectConverter<>(allFahrzeugmerkmalAuspraegungen);
    }
}
