package de.interfaceag.bmw.pzbk.anforderungsworkflow.folgeprozessttl;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.EditViewPermission;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;

import java.io.Serializable;
import java.util.Set;

/**
 * @author fp
 */
public class ZuordnungAnforderungDerivatDTO implements Serializable {

    private Long id;
    private final Long derivatId;
    private String derivatName;
    private ZuordnungStatus status;
    private boolean isUnzureichendeAnforderungsqualitaet = Boolean.FALSE;
    private boolean nachZakUebertragen;
    private boolean zurKenntnisGenommen;

    private EditViewPermission zurKenntnisGenommenColumn;
    private EditViewPermission zurKenntnisGenommenLabel;
    private EditViewPermission zurKenntnisGenommenButton;
    private final Set<Rolle> rolesWithWritePermissions;

    public ZuordnungAnforderungDerivatDTO(Long id, Long derivatId, String derivatName, ZuordnungStatus status, boolean nachZakUebertragen, boolean zurKenntnisGenommen, Set<Rolle> rolesWithWritePermissions) {
        this.id = id;
        this.derivatId = derivatId;
        this.derivatName = derivatName;
        this.status = status;
        this.nachZakUebertragen = nachZakUebertragen;
        this.zurKenntnisGenommen = zurKenntnisGenommen;
        this.rolesWithWritePermissions = rolesWithWritePermissions;

        updateViewPermissions();

    }

    public final void updateViewPermissions() {
        zurKenntnisGenommenColumn = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN)
                .authorizeWriteForRole(Rolle.T_TEAMLEITER)
                .authorizeWriteForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(rolesWithWritePermissions).get();

        if (zurKenntnisGenommen) {
            zurKenntnisGenommenLabel = RolePermission.builder()
                    .authorizeWriteForRole(Rolle.ADMIN)
                    .authorizeWriteForRole(Rolle.T_TEAMLEITER)
                    .authorizeWriteForRole(Rolle.TTEAM_VERTRETER)
                    .compareTo(rolesWithWritePermissions).get();
            zurKenntnisGenommenButton = new FalsePermission();
        } else if (isUnzureichendeAnforderungsqualitaet) {
            zurKenntnisGenommenLabel = new FalsePermission();
            zurKenntnisGenommenButton = RolePermission.builder()
                    .authorizeWriteForRole(Rolle.ADMIN)
                    .authorizeWriteForRole(Rolle.T_TEAMLEITER)
                    .authorizeWriteForRole(Rolle.TTEAM_VERTRETER)
                    .compareTo(rolesWithWritePermissions).get();
        } else {
            zurKenntnisGenommenLabel = new FalsePermission();
            zurKenntnisGenommenButton = new FalsePermission();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUnzureichendeAnforderungsqualitaet(boolean unzureichendeAnforderungsqualitaet) {
        isUnzureichendeAnforderungsqualitaet = unzureichendeAnforderungsqualitaet;
    }

    public String getDerivatName() {
        return derivatName;
    }

    public void setDerivatName(String derivatName) {
        this.derivatName = derivatName;
    }

    public ZuordnungStatus getStatus() {
        return status;
    }

    public void setStatus(ZuordnungStatus status) {
        this.status = status;
    }

    public boolean isNachZakUebertragen() {
        return nachZakUebertragen;
    }

    public void setNachZakUebertragen(boolean nachZakUebertragen) {
        this.nachZakUebertragen = nachZakUebertragen;
    }

    public boolean isZurKenntnisGenommen() {
        return zurKenntnisGenommen;
    }

    public void setZurKenntnisGenommen(boolean zurKenntnisGenommen) {
        this.zurKenntnisGenommen = zurKenntnisGenommen;
    }

    public boolean getZurKenntnisGenommenColumn() {
        return zurKenntnisGenommenColumn.isRendered();
    }

    public boolean getZurKenntnisGenommenLabel() {
        return zurKenntnisGenommenLabel.isRendered();
    }

    public boolean getZurKenntnisGenommenButton() {
        return zurKenntnisGenommenButton.hasRightToEdit();
    }

    public Long getDerivatId() {
        return derivatId;
    }

    @Override
    public String toString() {
        return derivatName + " " + status.getStatusBezeichnung();
    }

}
