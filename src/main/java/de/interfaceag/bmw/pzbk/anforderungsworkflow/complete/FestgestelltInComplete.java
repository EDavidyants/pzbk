package de.interfaceag.bmw.pzbk.anforderungsworkflow.complete;

import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * @author sl
 */
public final class FestgestelltInComplete {

    private FestgestelltInComplete() {
    }

    public static List<FestgestelltIn> completeFestgestelltIn(String query, Set<FestgestelltIn> festgestelltInFromAnforderung, List<FestgestelltIn> allFestgestelltIn) {

        List<FestgestelltIn> result = new ArrayList<>();
        Pattern pattern = Pattern.compile(".*" + query + ".*", Pattern.CASE_INSENSITIVE);


        allFestgestelltIn.forEach(festgestelltIn -> {
            if (festgestelltInFromAnforderung == null) {
                if (pattern.matcher(festgestelltIn.getWert()).matches()) {
                    result.add(festgestelltIn);
                }
            } else {
                if (pattern.matcher(festgestelltIn.getWert()).matches() && !festgestelltInFromAnforderung.contains(festgestelltIn)) {
                    result.add(festgestelltIn);
                }
            }
        });
        return result;
    }

}
