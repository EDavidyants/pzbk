package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.AnforderungEditFahrzeugmerkmalDialogService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalDialogData;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDTO;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDialogViewData;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.shared.dto.ProcessResultDto;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.ProzessbaukastenLinkData;
import de.interfaceag.bmw.pzbk.zak.ZakVereinbarungService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import org.primefaces.model.menu.MenuModel;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author fn
 */
@Stateless
@Named
public class AnforderungMitMeldungFacade implements Serializable {

    @Inject
    protected AnforderungService anforderungService;
    @Inject
    protected ProzessbaukastenZuordnenService prozessbaukastenZuordnenService;

    @Inject
    private ZakVereinbarungService zakService;
    @Inject
    private ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;
    @Inject
    private AnforderungEditFahrzeugmerkmalDialogService anforderungEditFahrzeugmerkmalDialogService;

    public AnforderungMitMeldungViewData initViewData(Anforderung anforderung) {
        Boolean zuordnungZuProzessbaukasten = anforderung.isProzessbaukastenZugeordnet();
        ProzessbaukastenZuordnenDialogViewData prozessbaukastenDialogData = getProzessbaukastenZuordnenViewData(anforderung);
        List<ProzessbaukastenLinkData> prozessbaukastenLinks = anforderungService.getProzessbaukastenLinksForAnforderung(anforderung);

        List<ZuordnungAnforderungDerivat> derivatZuordnungenWithBerechtigung = zuordnungAnforderungDerivatService.getDerivatZuordnungenWithBerechtigungForAnforderung(anforderung);

        return new AnforderungMitMeldungViewData(prozessbaukastenDialogData, zuordnungZuProzessbaukasten, prozessbaukastenLinks, derivatZuordnungenWithBerechtigung);

    }

    public String prozessbaukastenZuordnen(ProzessbaukastenZuordnenDTO prozessbaukastenZuordnenDTO, Long anforderungId) {
        return prozessbaukastenZuordnenService.prozessbaukastenZuordnen(prozessbaukastenZuordnenDTO, anforderungId);
    }

    private ProzessbaukastenZuordnenDialogViewData getProzessbaukastenZuordnenViewData(Anforderung anforderung) {
        return prozessbaukastenZuordnenService.getAddProzessbaukastenDialogViewData(anforderung);
    }

    public MenuModel getZugeordneteProzessbaukastenMenuItems(List<ProzessbaukastenLinkData> zugeordneteProzessbaukasten) {
        return anforderungService.getZugeordneteProzessbaukastenMenuItems(zugeordneteProzessbaukasten);
    }

    public MenuModel getVersionsMenuItemsForAnforderung(Anforderung anforderung, boolean hasRightToCreateNewVersion) {
        return anforderungService.getVersionsMenuItemsForAnforderung(anforderung, hasRightToCreateNewVersion);
    }

    public MenuModel getZugeordneteMeldungenMenuItemsForAnforderung(Anforderung anforderung, boolean hasRightToZuordnen) {
        return anforderungService.getZugeordneteMeldungenMenuItemsForAnforderung(anforderung, hasRightToZuordnen);
    }

    public Boolean isDemGueltigenProzessbaukastenZugeordnet(Anforderung anforderung) {
        return anforderungService.isDemGueltigenProzessbaukastenZugeordnet(anforderung);
    }

    public String generateGrowlMessageForFreigabeNeuerVersion() {
        return anforderungService.generateGrowlMessageForFreigabeNeuerVersion();
    }

    public String anforderungForSelectedModulFreigeben(AnforderungFreigabeBeiModulSeTeam anfoModul, String modulFreigabeKommentar, Long anforderungId) {
        anforderungService.anforderungForSelectedModulFreigeben(anfoModul, modulFreigabeKommentar);
        return PageUtils.getUrlForPageWithId(Page.ANFORDERUNGVIEW, anforderungId);
    }

    public String modulAblehnen(AnforderungFreigabeBeiModulSeTeam anfoModul, String modulFreigabeKommentar, Long anforderungId) {
        if (anforderungService.modulAblehnen(anfoModul, modulFreigabeKommentar)) {
            return PageUtils.getUrlForPageWithId(Page.ANFORDERUNGVIEW, anforderungId);
        } else {
            return "";
        }
    }

    public ProcessResultDto generateReportNachZakUebertragung(List<ZuordnungAnforderungDerivat> zuordnungen) {
        Map<Derivat, List<ZakUebertragung>> report = zakService.sendAnforderungenNachZak(zuordnungen);
        String zakMessage = getErgebnisForZakUebertragung(report);
        return new ProcessResultDto(true, zakMessage);
    }

    private String getErgebnisForZakUebertragung(Map<Derivat, List<ZakUebertragung>> report) {
        StringBuilder sb = new StringBuilder();

        Iterator<Derivat> iterator = report.keySet().iterator();

        while (iterator.hasNext()) {
            Derivat derivat = iterator.next();
            sb.append(derivat.getName()).append(": ");

            report.get(derivat).forEach(zak -> sb.append(generateMessageForZakUebertragung(zak)));
            sb.append("\n");
        }

        String ergebnis = sb.toString();
        if ("".equals(ergebnis)) {
            ergebnis = "keine Module wurden nach ZAK uebertragen";
        }

        return ergebnis;
    }

    private String generateMessageForZakUebertragung(ZakUebertragung zak) {
        StringBuilder sb = new StringBuilder();
        sb.append(zak.getDerivatAnforderungModul().getAnforderung().getFachId());
        sb.append(" v").append(zak.getDerivatAnforderungModul().getAnforderung().getVersion());
        sb.append(" | ");
        sb.append(zak.getDerivatAnforderungModul().getModul().getName()).append("; ");
        return sb.toString();
    }

    public AnforderungEditFahrzeugmerkmalDialogData getAnforderungEditFahrzeugmerkmalDialogData(AnforderungId anforderungId, ModulSeTeamId modulSeTeamId) {
        return anforderungEditFahrzeugmerkmalDialogService.getDialogData(anforderungId, modulSeTeamId);
    }
}
