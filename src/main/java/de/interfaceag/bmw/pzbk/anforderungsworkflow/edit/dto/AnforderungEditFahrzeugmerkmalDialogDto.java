package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalDialogData;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;

import java.util.List;
import java.util.StringJoiner;

public class AnforderungEditFahrzeugmerkmalDialogDto implements AnforderungEditFahrzeugmerkmalDialogData {

    private final ModulSeTeamId modulSeTeamId;
    private List<AnforderungEditFahrzeugmerkmal> fahrzeugmerkmale;

    public AnforderungEditFahrzeugmerkmalDialogDto(ModulSeTeamId modulSeTeamId, List<AnforderungEditFahrzeugmerkmal> fahrzeugmerkmale) {
        this.modulSeTeamId = modulSeTeamId;
        this.fahrzeugmerkmale = fahrzeugmerkmale;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", AnforderungEditFahrzeugmerkmalDialogDto.class.getSimpleName() + "[", "]")
                .add("modulSeTeamId=" + modulSeTeamId)
                .add("fahrzeugmerkmale=" + fahrzeugmerkmale)
                .toString();
    }

    @Override
    public ModulSeTeamId getModulSeTeamId() {
        return modulSeTeamId;
    }

    @Override
    public List<AnforderungEditFahrzeugmerkmal> getFahrzeugmerkmale() {
        return fahrzeugmerkmale;
    }

    @Override
    public void setFahrzeugmerkmale(List<AnforderungEditFahrzeugmerkmal> fahrzeugmerkmale) {
        this.fahrzeugmerkmale = fahrzeugmerkmale;
    }
}
