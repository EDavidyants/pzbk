package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.entities.AnforderungReadData;
import de.interfaceag.bmw.pzbk.enums.Status;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class AnforderungBeschreibungChangedValidator {

    private AnforderungBeschreibungChangedValidator() {
    }

    public static boolean isKommentarChangedInStatusPlausibilisiert(AnforderungReadData anforderung, String originalBeschreibungDe) {

        if (anforderung == null) {
            return Boolean.FALSE;
        }

        // only status plausibilisiert is relevant for us
        if (!anforderung.getStatus().equals(Status.A_PLAUSIB)) {
            return Boolean.FALSE;
        }

        if (anforderung.getBeschreibungAnforderungDe().trim().equals(originalBeschreibungDe.trim())) {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

}
