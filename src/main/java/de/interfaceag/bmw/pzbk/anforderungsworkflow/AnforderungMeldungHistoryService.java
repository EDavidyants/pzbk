package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungBasisDataDto;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungHistoryDto;
import de.interfaceag.bmw.pzbk.dao.AnforderungHistoryDao;
import de.interfaceag.bmw.pzbk.entities.AbstractAnfoMgmtObject;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Zielwert;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.migration.AnforderungHistoryMigrationDto;
import de.interfaceag.bmw.pzbk.reporting.statustransition.migration.MeldungHistoryMigrationDto;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author evda
 */
@Stateless
public class AnforderungMeldungHistoryService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungMeldungHistoryService.class);

    @Inject
    private AnforderungHistoryDao anforderungHistoryDao;

    @Inject
    private Date currentDate;

    private static final List<String> PROPS;
    private static final Map<String, String> HISTORY_OBJEKTNAME_MAP;

    private static final String ANFO_FREIGABE_BEI_MODUL = "anfoFreigabeBeiModul";
    private static final String STATUS_WECHSEL_KOMMENTAR = "statusWechselKommentar";
    private static final String STAERKE_SCHWAECHE = "staerkeSchwaeche";
    private static final String ZIELWERT = "zielwert";
    private static final String ZEITPKT_UMSETZUNGSBEST = "zeitpktUmsetzungsbest";
    private static final String STATUS = "Status";
    private static final String MELDUNG_ZUORDNUNG = "meldungZuordnung";
    private static final String POTENTIAL_STANDARDISIERUNG = "potentialStandardisierung";
    private static final String PHASENBEZUG = "phasenbezug";

    static {
        PROPS = new ArrayList<>();
        //Meldung, Anforderung, Pzbk
        PROPS.add("status");
        PROPS.add(STATUS_WECHSEL_KOMMENTAR);
        PROPS.add("anhaenge");
        PROPS.add("tteam");
        PROPS.add("sensorCoc");
        //Meldung + Anforderung
        PROPS.add("referenzen");
        PROPS.add("umsetzenderBereich");
        PROPS.add(STAERKE_SCHWAECHE);
        PROPS.add("beschreibungStaerkeSchwaeche");
        PROPS.add("ursache");
        PROPS.add("beschreibungAnforderungDe");
        PROPS.add("beschreibungAnforderungEn");
        PROPS.add("kommentarAnforderung");
        PROPS.add("loesungsvorschlag");
        PROPS.add("sensor");
        PROPS.add("auswirkungen");
        PROPS.add("festgestelltIn");
        PROPS.add(ZIELWERT);
        PROPS.add("werk");
        //Meldung + Pzbk
        PROPS.add("anforderungen");
        //Anforderung
        PROPS.add(PHASENBEZUG);
        PROPS.add(ZEITPKT_UMSETZUNGSBEST);
        PROPS.add(POTENTIAL_STANDARDISIERUNG);
        PROPS.add("meldungen");
        PROPS.add(MELDUNG_ZUORDNUNG);
        //Anforderung + Pzbk
        PROPS.add("version");
        PROPS.add("umsetzer");
        PROPS.add("ersteller");
        PROPS.add("referenzSystemLinks");
        PROPS.add("kategorie");
        PROPS.add(ANFO_FREIGABE_BEI_MODUL);
        //Pzbk
        PROPS.add("titel");
        PROPS.add("beschreibung");
        PROPS.add("zieleinsatzDerivat");
        PROPS.add("addAnforderungToProzessbaukasten");

        HISTORY_OBJEKTNAME_MAP = new HashMap<>();
        HISTORY_OBJEKTNAME_MAP.put("status", STATUS);
        HISTORY_OBJEKTNAME_MAP.put(STATUS_WECHSEL_KOMMENTAR, "Kommentar zum Statuswechsel");
        HISTORY_OBJEKTNAME_MAP.put("anhaenge", "Anhang");
        HISTORY_OBJEKTNAME_MAP.put("tteam", "T-Team");
        HISTORY_OBJEKTNAME_MAP.put("sensorCoc", "Sensor CoC");
        HISTORY_OBJEKTNAME_MAP.put("referenzen", "Referenz");
        HISTORY_OBJEKTNAME_MAP.put("umsetzenderBereich", "Umsetzender Bereich");
        HISTORY_OBJEKTNAME_MAP.put(STAERKE_SCHWAECHE, "Stärke/Schwäche");
        HISTORY_OBJEKTNAME_MAP.put("beschreibungStaerkeSchwaeche", "Beschreibung der Stärke/Schwäche");
        HISTORY_OBJEKTNAME_MAP.put("ursache", "Vermutete Ursache");
        HISTORY_OBJEKTNAME_MAP.put("beschreibungAnforderungDe", "Beschreibung DE");
        HISTORY_OBJEKTNAME_MAP.put("beschreibungAnforderungEn", "Beschreibung EN");
        HISTORY_OBJEKTNAME_MAP.put("kommentarAnforderung", "Kommentar zur Anforderung");
        HISTORY_OBJEKTNAME_MAP.put("loesungsvorschlag", "Lösungsvorschlag");
        HISTORY_OBJEKTNAME_MAP.put("sensor", "Sensor");
        HISTORY_OBJEKTNAME_MAP.put("auswirkungen", "Auswirkungen");
        HISTORY_OBJEKTNAME_MAP.put("festgestelltIn", "Festgestellt in");
        HISTORY_OBJEKTNAME_MAP.put(ZIELWERT, "Zielwert");
        HISTORY_OBJEKTNAME_MAP.put("anforderungen", "Anforderungen");
        HISTORY_OBJEKTNAME_MAP.put(PHASENBEZUG, "Zeitpunkt Anforderungs-Einsteuerung");
        HISTORY_OBJEKTNAME_MAP.put(ZEITPKT_UMSETZUNGSBEST, "Zeitpunkt Umsetzungsbestätigung");
        HISTORY_OBJEKTNAME_MAP.put(POTENTIAL_STANDARDISIERUNG, "Potential zur Standardisierung");
        HISTORY_OBJEKTNAME_MAP.put("meldungen", "Meldungen");
        HISTORY_OBJEKTNAME_MAP.put("version", "Version");
        HISTORY_OBJEKTNAME_MAP.put("umsetzer", "Umsetzer");
        HISTORY_OBJEKTNAME_MAP.put("titel", "Titel");
        HISTORY_OBJEKTNAME_MAP.put("beschreibung", "Beschreibung");
        HISTORY_OBJEKTNAME_MAP.put("zieleinsatzDerivat", "Zieleinsatzderivat");
        HISTORY_OBJEKTNAME_MAP.put("werk", "Werk");
        HISTORY_OBJEKTNAME_MAP.put("ersteller", "Ersteller");
        HISTORY_OBJEKTNAME_MAP.put("referenzSystemLinks", "Referenz System Links");
        HISTORY_OBJEKTNAME_MAP.put("addAnforderungToProzessbaukasten", "Zuordnung Prozessbaukasten");
        HISTORY_OBJEKTNAME_MAP.put("kategorie", "Kategorie Wechsel");
        HISTORY_OBJEKTNAME_MAP.put(MELDUNG_ZUORDNUNG, "Meldung Zuordnung");
        HISTORY_OBJEKTNAME_MAP.put(ANFO_FREIGABE_BEI_MODUL, "Modul Vereinbarung");

    }

    public List<AnforderungHistoryDto> getHistoryForAnforderung(Long anforderungId, String kennzeichen) {
        List<AnforderungHistory> anforderungHistory = anforderungHistoryDao.getAnforderungHistoryByIdAndKennzeichen(anforderungId, kennzeichen);
        List<AnforderungHistoryDto> anforderungHistoryDto = new ArrayList<>();

        anforderungHistory.forEach(history -> {
            AnforderungHistoryDto dto = AnforderungHistoryDto.newBuilder()
                    .withAnforderungMeldungId(anforderungId)
                    .withKennzeichen(kennzeichen)
                    .withZeitpunkt(history.getDatum())
                    .withBenutzer(history.getBenutzer())
                    .withAttribut(history.getObjektName())
                    .withVon(history.getVon())
                    .withAuf(history.getAuf())
                    .build();

            // ignore dublicates
            if (!anforderungHistoryDto.contains(dto)) {
                anforderungHistoryDto.add(dto);
            }

        });
        return anforderungHistoryDto;
    }

    public void persistAnforderungHistory(AnforderungHistory anforderungHistory) {
        anforderungHistoryDao.persist(anforderungHistory);
    }

    public void writeHistoryForNewAnfoMgmtObjectVersion(Mitarbeiter user, AbstractAnfoMgmtObject anfoNew, int versionBase, int versionNew, String kommentar) {

        String von = anfoNew.getFachId() + " | V" + versionBase;

        StringBuilder sb = new StringBuilder();
        sb.append(anfoNew.getFachId()).append(" | V").append(versionNew);
        if (kommentar != null && !kommentar.isEmpty()) {
            sb.append(": ").append(kommentar);
        }
        String auf = sb.toString();

        AnforderungHistory ah = new AnforderungHistory(anfoNew.getId(), anfoNew.getKennzeichen(), anfoNew.getAenderungsdatum(),
                "neue Version von " + anfoNew.getClass().getSimpleName(), user.getName(), von, auf);
        persistAnforderungHistory(ah);
    }

    public void writeHistoryForChangedValuesOfAnfoMgmtObject(Mitarbeiter user, AbstractAnfoMgmtObject newObject, AbstractAnfoMgmtObject oldObject) {
        LOG.debug("Start to write history for user {}, newObject {}, oldObject {}", user, newObject, oldObject);
        try {
            Map<String, String> description = BeanUtils.describe(newObject);
            LOG.debug("newObject description {}", description);
            Set<String> keys = description.keySet();
            LOG.debug("keys {}", keys);
            for (String key : keys) {
                writeHistoryForChangedField(key, oldObject, newObject, user);
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            LOG.error(null, ex);
        }
        LOG.debug("Finished writing history");
    }

    private void writeHistoryForChangedField(String key, AbstractAnfoMgmtObject oldObject, AbstractAnfoMgmtObject newObject, Mitarbeiter user) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        LOG.debug("Compare key {}", key);

        Object oldValue = PropertyUtils.getProperty(oldObject, key);
        Object newValue = PropertyUtils.getProperty(newObject, key);
        if (oldValue != newValue && (oldValue != null && !oldValue.equals(newValue) || !newValue.equals(oldValue)) && PROPS.contains(key)) {

            LOG.debug("Add entry for newValue {}, oldValue {}", newValue, oldValue);

            String oldValueToString = "";
            if (!STATUS_WECHSEL_KOMMENTAR.equals(key) && !MELDUNG_ZUORDNUNG.equals(key)) {
                oldValueToString = convertValuesOfAnfoMgmtObjectToString(oldValue, key);
                LOG.debug("oldValueToString {}", oldValueToString);
            }
            String newValueToString = convertValuesOfAnfoMgmtObjectToString(newValue, key);
            LOG.debug("newValueToString {}", newValueToString);

            if (ANFO_FREIGABE_BEI_MODUL.equals(key)) {
                writeChangesOfModulenVereinbarungType(oldObject, newObject, user);

            } else if (!ZEITPKT_UMSETZUNGSBEST.equals(key)) {
                writeHistoryForFieldWithValue(newObject.getId(), newObject.getKennzeichen(),
                        HISTORY_OBJEKTNAME_MAP.get(key), oldValueToString, newValueToString, user.getName());
            }

        }
    }

    protected void writeChangesOfModulenVereinbarungType(AbstractAnfoMgmtObject oldObject, AbstractAnfoMgmtObject newObject, Mitarbeiter user) {
        Long anforderungId = newObject.getId();
        Anforderung anforderungOld = (Anforderung) oldObject;
        List<AnforderungFreigabeBeiModulSeTeam> oldValues = anforderungOld.getAnfoFreigabeBeiModul();
        Anforderung anforderungNew = (Anforderung) newObject;
        List<AnforderungFreigabeBeiModulSeTeam> newValues = anforderungNew.getAnfoFreigabeBeiModul();

        writeHistoryForAnforderungModulVereinbarung(anforderungId, oldValues, newValues, user);
    }

    private void writeHistoryForAnforderungModulVereinbarung(Long anforderungId, List<AnforderungFreigabeBeiModulSeTeam> oldValues, List<AnforderungFreigabeBeiModulSeTeam> newValues, Mitarbeiter user) {
        oldValues.forEach(oldModul ->
                generateHistoryRecordForAnforderungModul(anforderungId, oldModul, newValues, user)
        );
    }

    private void generateHistoryRecordForAnforderungModul(Long anforderungId, AnforderungFreigabeBeiModulSeTeam oldModul, List<AnforderungFreigabeBeiModulSeTeam> newValues, Mitarbeiter user) {
        Optional<AnforderungFreigabeBeiModulSeTeam> newModulOptional = newValues.stream()
                .filter(newModul -> Objects.isNull(newModul.getId()) || newModul.getId().equals(oldModul.getId())).findAny();

        if (newModulOptional.isPresent()) {
            AnforderungFreigabeBeiModulSeTeam newModul = newModulOptional.get();
            if (isModulVereinbarungChanged(oldModul, newModul)) {
                String fieldName = oldModul.getModulSeTeam().toString();
                String oldValue = "Vereinbarung " + oldModul.getVereinbarungType().getLabel();
                String newValue = "Vereinbarung " + newModul.getVereinbarungType().getLabel();
                String userName = user.getName();
                writeHistoryForFieldWithValue(anforderungId, "A", fieldName, oldValue, newValue, userName);
            }
        }
    }

    private static boolean isModulVereinbarungChanged(AnforderungFreigabeBeiModulSeTeam oldModul, AnforderungFreigabeBeiModulSeTeam newModul) {
        return oldModul.getVereinbarungType() != newModul.getVereinbarungType();
    }

    private void writeHistoryForFieldWithValue(Long objectId, String kennzeichen, String fieldName, String oldValue, String newValue, String userName) {
        LOG.debug("Create new AnforderungHistroy entry");
        AnforderungHistory history = new AnforderungHistory(objectId, kennzeichen,
                currentDate, fieldName, userName, oldValue, newValue);
        LOG.debug("Persist new AnforderungHistroy entry {}", history);
        persistAnforderungHistory(history);
    }

    public String convertValuesOfAnfoMgmtObjectToString(Object toConvert, String key) {
        String valueAsString = "";
        if (ZIELWERT.equals(key)) {
            Zielwert zielwert = (Zielwert) toConvert;
            return "Zielwert: " + zielwert.getWert() + " \nKommentar zum Zielwert: " + zielwert.getKommentar();
        }
        if (!String.valueOf(toConvert).equals("null")) {
            valueAsString = String.valueOf(toConvert).replace("[", "").replace("]", "").replace("{", "").replace("}", "");
            if ("true".equals(valueAsString)) {
                switch (key) {
                    case STAERKE_SCHWAECHE:
                        valueAsString = "Stärke";
                        break;
                    case PHASENBEZUG:
                        valueAsString = "Konzeptrelevant";
                        break;
                    case POTENTIAL_STANDARDISIERUNG:
                        valueAsString = "Ja";
                        break;
                    default:
                        break;
                }
            } else if ("false".equals(valueAsString)) {
                switch (key) {
                    case STAERKE_SCHWAECHE:
                        valueAsString = "Schwäche";
                        break;
                    case PHASENBEZUG:
                        valueAsString = "Architekturrelevant";
                        break;
                    case POTENTIAL_STANDARDISIERUNG:
                        valueAsString = "Nein";
                        break;
                    default:
                        break;
                }
            }
        }
        return valueAsString;
    }

    public void writeHistoryForNewAnfoMgmtObject(Mitarbeiter user, AbstractAnfoMgmtObject newObject) {

        AnforderungHistory ah = new AnforderungHistory(newObject.getId(), newObject.getKennzeichen(),
                newObject.getAenderungsdatum(), newObject.getClass().getSimpleName(), user.getName(), "", "neu angelegt");
        persistAnforderungHistory(ah);
    }

    public void writeHistoryForAnforderungFreigabeBeiModulSeTeam(Mitarbeiter user, AnforderungFreigabeBeiModulSeTeam anfoModul) {

        String status;
        if (anfoModul.isFreigabe()) {
            status = "freigegeben: ";
        } else {
            status = "abgelehnt: ";
        }

        AnforderungHistory ah = new AnforderungHistory(anfoModul.getAnforderung().getId(), "A", anfoModul.getDatum(),
                anfoModul.getModulSeTeam().getName(), user.getName(),
                "Steht zur Freigabe an", status + anfoModul.getKommentar());
        persistAnforderungHistory(ah);
    }

    public boolean anforderungIsChangedAfterLastPlVorschlag(Anforderung anforderung) {
        List<AnforderungHistory> plVorschlag = getAnforderungHistoryByObjectnameLikeAnforderung(anforderung, "PL Vorschlag");
        Optional<Date> optionalMinDate = plVorschlag.stream().map(AnforderungHistory::getDatum).max(Date::compareTo);
        if (optionalMinDate.isPresent()) {
            Date minDate = optionalMinDate.get();
            if (attributeChangedSinceDate(minDate, "Beschreibung", anforderung)) {
                return Boolean.TRUE;
            }
            if (attributeChangedSinceDate(minDate, "Umsetzer-Änderungsvorschlag", anforderung)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    private boolean attributeChangedSinceDate(Date date, String attribute, Anforderung anforderung) {
        List<AnforderungHistory> attributeList = getAnforderungHistoryByObjectnameLikeAnforderung(anforderung, attribute);
        Optional<Date> maxDate = attributeList.stream().map(AnforderungHistory::getDatum).max(Date::compareTo);
        return maxDate.isPresent() && maxDate.get().after(date);
    }

    public Date getLastChangeDateForAttribute(String attribute, Anforderung anforderung) {
        List<AnforderungHistory> attributeList = getAnforderungHistoryByObjectnameLikeAnforderung(anforderung, attribute);
        Optional<Date> maxDate = attributeList.stream().map(AnforderungHistory::getDatum).max(Date::compareTo);
        return maxDate.orElse(null);
    }

    public Date getLastChangeDateForAttribute(String attribute, Meldung meldung) {
        List<AnforderungHistory> attributeList = getAnforderungHistoryByObjectnameLikeMeldung(meldung, attribute);
        Optional<Date> maxDate = attributeList.stream().map(AnforderungHistory::getDatum).max(Date::compareTo);
        return maxDate.orElse(null);
    }

    public Date getLastStatusChangeDateForMeldung(Meldung meldung) {
        return getLastChangeDateForAttribute(STATUS, meldung);
    }

    public Date getLastStatusChangeDateForAnforderung(Anforderung anforderung) {
        return getLastChangeDateForAttribute(STATUS, anforderung);
    }

    private List<AnforderungHistory> getAnforderungHistoryByObjectnameLikeMeldung(Meldung meldung, String objectName) {
        String objectNameExpression = "%" + objectName + "%";
        return getAnforderungHistoryByObjectnameAnforderungId(meldung.getId(), meldung.getKennzeichen(), objectNameExpression);
    }

    private List<AnforderungHistory> getAnforderungHistoryByObjectnameLikeAnforderung(Anforderung anforderung, String objectName) {
        String objectNameExpression = "%" + objectName + "%";
        return getAnforderungHistoryByObjectnameAnforderungId(anforderung.getId(), anforderung.getKennzeichen(), objectNameExpression);
    }

    public List<AnforderungHistory> getAnforderungHistoryByObjectnameAnforderungId(Long anforderungId, String kennzeichen, String objectName) {
        return anforderungHistoryDao.getAnforderungHistoryByObjectnameAnforderungId(anforderungId, kennzeichen, objectName);
    }

    public Status getLastStatusByObjectnameAnforderungId(Long anforderungId, String kennzeichen) {
        Status status = null;
        List<AnforderungHistory> attributeList = anforderungHistoryDao.getAnforderungHistoryByObjectnameAnforderungId(anforderungId, kennzeichen, STATUS);
        if (!attributeList.isEmpty()) {
            Optional<Date> maxDate = attributeList.stream().map(AnforderungHistory::getDatum).max(Date::compareTo);
            String bezeichnung = "";
            Optional<AnforderungHistory> oldAnforderungHistory = attributeList.stream().filter(anforderungHistory -> anforderungHistory.getDatum().equals(maxDate.get())).findFirst();
            if (oldAnforderungHistory.isPresent()) {
                bezeichnung = oldAnforderungHistory.get().getVon();
            }
            status = Status.getStatusByBezeichnungAndMarker(bezeichnung, kennzeichen);
        }
        return status;
    }

    public void writeHistoryForChangedKeyValue(Mitarbeiter user, AbstractAnfoMgmtObject newObject, String newValueToString, String oldValueToString, String key) {
        if (key != null && PROPS.contains(key)) {
            AnforderungHistory ah = new AnforderungHistory(newObject.getId(), newObject.getKennzeichen(),
                    new Date(), HISTORY_OBJEKTNAME_MAP.get(key), user.getName(),
                    oldValueToString, newValueToString);
            persistAnforderungHistory(ah);
        }
    }

    public AnforderungBasisDataDto getAnforderungBasisInfo(Long id, String kennzeichen) {
        switch (kennzeichen) {
            case "M":
                return anforderungHistoryDao.getMeldungBasisInfo(id);

            case "A":
                return anforderungHistoryDao.getAnforderungBasisInfo(id);

            case "P":
                return anforderungHistoryDao.getPzbkBasisInfo(id);

            default:
                return null;
        }

    }

    public List<AnforderungHistoryMigrationDto> getAnforderungHistoryMigrationDtosForAnforderung(Anforderung anforderung) {
        List<AnforderungHistoryDto> anforderungHistory = getHistoryForAnforderung(anforderung.getId(), "A");
        Collections.reverse(anforderungHistory);
        return anforderungHistory.stream().map(history
                -> new AnforderungHistoryMigrationDto(history.getAnforderungId(), history.getZeitpunktAsDate(), history.getAttribut(), history.getVon(), history.getAuf())).collect(Collectors.toList());
    }

    public List<MeldungHistoryMigrationDto> getMeldungHistoryMigrationDtosForAnforderung(Meldung meldung) {
        List<AnforderungHistoryDto> anforderungHistory = getHistoryForAnforderung(meldung.getId(), "M");
        Collections.reverse(anforderungHistory);
        return anforderungHistory.stream().map(history
                -> new MeldungHistoryMigrationDto(history.getAnforderungId(), history.getZeitpunktAsDate(), history.getAttribut(), history.getVon(), history.getAuf())).collect(Collectors.toList());
    }
}
