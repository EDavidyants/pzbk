package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalDialogData;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalAuspraegungDto;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDialogDto;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDto;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class AnforderungEditFahrzeugmerkmalDialogService implements Serializable {

    @Inject
    private FahrzeugmerkmalService fahrzeugmerkmalService;
    @Inject
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService anforderungModulSeTeamFahrzeugmerkmalAuspraegungService;

    public AnforderungEditFahrzeugmerkmalDialogData getDialogData(AnforderungId anforderungId, ModulSeTeamId modulSeTeamId) {

        final List<Fahrzeugmerkmal> allFahrzeugmerkmaleForModulSeTeam = fahrzeugmerkmalService.getFahrzeugmerkmaleForModulSeTeam(modulSeTeamId);
        final List<FahrzeugmerkmalAuspraegung> selectedAuspraegungenForModulSeTeamAndAnforderung = getSelectedAuspraegungenForAnforderungAndModulSeTeam(anforderungId, modulSeTeamId);

        List<AnforderungEditFahrzeugmerkmal> fahrzeugmerkmale = new ArrayList<>();

        for (Fahrzeugmerkmal fahrzeugmerkmal : allFahrzeugmerkmaleForModulSeTeam) {
            AnforderungEditFahrzeugmerkmal anforderungEditFahrzeugmerkmal = createAnforderungEditFahrzeugmerkmalForFahrzeugmerkmal(selectedAuspraegungenForModulSeTeamAndAnforderung, fahrzeugmerkmal);
            fahrzeugmerkmale.add(anforderungEditFahrzeugmerkmal);
        }

        return new AnforderungEditFahrzeugmerkmalDialogDto(modulSeTeamId, fahrzeugmerkmale);
    }

    private AnforderungEditFahrzeugmerkmal createAnforderungEditFahrzeugmerkmalForFahrzeugmerkmal(List<FahrzeugmerkmalAuspraegung> selectedAuspraegungenForModulSeTeamAndAnforderung, Fahrzeugmerkmal fahrzeugmerkmal) {
        final List<AnforderungEditFahrzeugmerkmalAuspraegung> auspraegungenForFahrzeugmerkmal = getAllAuspraegungenForFahrzeugmerkmalAsDto(fahrzeugmerkmal);
        final List<AnforderungEditFahrzeugmerkmalAuspraegung> selectedAuspraegungenForFahrzeugmerkmal = getSelectedAuspraegungenForFahrzeugmerkmalAsDto(selectedAuspraegungenForModulSeTeamAndAnforderung, fahrzeugmerkmal);
        return buildAnforderungEditFahrzeugmerkmal(fahrzeugmerkmal, auspraegungenForFahrzeugmerkmal, selectedAuspraegungenForFahrzeugmerkmal);
    }

    private AnforderungEditFahrzeugmerkmal buildAnforderungEditFahrzeugmerkmal(Fahrzeugmerkmal fahrzeugmerkmal, List<AnforderungEditFahrzeugmerkmalAuspraegung> auspraegungenForFahrzeugmerkmal, List<AnforderungEditFahrzeugmerkmalAuspraegung> selectedAuspraegungenForFahrzeugmerkmal) {
        return new AnforderungEditFahrzeugmerkmalDto(fahrzeugmerkmal.getFahrzeugmerkmalId(), fahrzeugmerkmal.getMerkmal(), auspraegungenForFahrzeugmerkmal, selectedAuspraegungenForFahrzeugmerkmal);
    }

    private static List<AnforderungEditFahrzeugmerkmalAuspraegung> getSelectedAuspraegungenForFahrzeugmerkmalAsDto(List<FahrzeugmerkmalAuspraegung> selectedAuspraegungen, Fahrzeugmerkmal fahrzeugmerkmal) {
        return selectedAuspraegungen.stream()
                .filter(fahrzeugmerkmalAuspraegung -> fahrzeugmerkmalAuspraegung.getFahrzeugmerkmal().getFahrzeugmerkmalId().equals(fahrzeugmerkmal.getFahrzeugmerkmalId()))
                .map(auspraegung -> new AnforderungEditFahrzeugmerkmalAuspraegungDto(auspraegung.getFahrzeugmerkmalAuspraegungId(), auspraegung.getAuspraegung()))
                .collect(Collectors.toList());
    }

    private List<AnforderungEditFahrzeugmerkmalAuspraegung> getAllAuspraegungenForFahrzeugmerkmalAsDto(Fahrzeugmerkmal fahrzeugmerkmal) {
        final List<FahrzeugmerkmalAuspraegung> allAuspraegungenForFahrzeugmerkmal = fahrzeugmerkmalService.getAllAuspraegungenForFahrzeugmerkmal(fahrzeugmerkmal.getFahrzeugmerkmalId());

        return allAuspraegungenForFahrzeugmerkmal.stream()
                .map(auspraegung -> new AnforderungEditFahrzeugmerkmalAuspraegungDto(auspraegung.getFahrzeugmerkmalAuspraegungId(), auspraegung.getAuspraegung()))
                .collect(Collectors.toList());
    }

    private List<FahrzeugmerkmalAuspraegung> getSelectedAuspraegungenForAnforderungAndModulSeTeam(AnforderungId anforderungId, ModulSeTeamId modulSeTeamId) {
        return anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(anforderungId, modulSeTeamId);
    }

}
