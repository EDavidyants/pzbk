package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api;

import java.io.Serializable;

public interface AnforderungEditFahrzeugmerkmalDialogActions extends Serializable {

    void processFahrzeugmerkmalChanges();

}
