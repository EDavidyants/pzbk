package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.TruePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

/**
 *
 * @author sl
 */
public final class AnforderungInlineEditViewPermission {

    private AnforderungInlineEditViewPermission() {
    }

    public static ViewPermission isInlineEditViewPermission(Status status, boolean hasWritePermissionForAnforderung) {
        if (isInlineEdit(status, hasWritePermissionForAnforderung)) {
            return TruePermission.get();
        } else {
            return FalsePermission.get();
        }
    }

    public static boolean isInlineEdit(Status status, boolean hasWritePermissionForAnforderung) {
        if (hasWritePermissionForAnforderung) {
            return isStatusEnabledForEdit(status);
        } else {
            return Boolean.FALSE;
        }
    }

    private static boolean isStatusEnabledForEdit(Status status) {
        switch (status) {
            case A_INARBEIT:
            case A_UNSTIMMIG:
            case A_FTABGESTIMMT:
            case A_PLAUSIB:
            case A_ANGELEGT_IN_PROZESSBAUKASTEN:
                return Boolean.TRUE;
            default:
                return Boolean.FALSE;
        }
    }

}
