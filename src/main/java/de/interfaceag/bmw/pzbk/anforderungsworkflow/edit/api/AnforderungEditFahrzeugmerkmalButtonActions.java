package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api;

import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;

import java.io.Serializable;

public interface AnforderungEditFahrzeugmerkmalButtonActions extends Serializable {

    void showFahrzeugmerkmalEditDialogForModulSeTeam(ModulSeTeamId modulSeTeamId);

}
