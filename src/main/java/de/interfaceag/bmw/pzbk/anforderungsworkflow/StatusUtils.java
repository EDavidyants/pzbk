package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.enums.Status;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @author fp
 */
public final class StatusUtils {

    private StatusUtils() {
    }

    /**
     * Method to get a List of all possible status transitions for the current
     * status.
     *
     * @param currentStatus the current status.
     * @return all possible transitions for the current status.
     */
    public static List<Status> getAllNextStatusAsList(Status currentStatus) {
        return getAllNextStatusAsStream(currentStatus).collect(Collectors.toList());
    }

    /**
     * Method to get a Set of all possible status transitions for the current
     * status.
     *
     * @param currentStatus the current status.
     * @return all possible transitions for the current status.
     */
    public static Set<Status> getAllNextStatusAsSet(Status currentStatus) {
        return getAllNextStatusAsStream(currentStatus).collect(Collectors.toSet());
    }

    /**
     * Method to get a Stream of all possible status transitions for the current
     * status.
     *
     * @param currentStatus the current status.
     * @return all possible transitions for the current status.
     */
    public static Stream<Status> getAllNextStatusAsStream(Status currentStatus) {
        switch (currentStatus) {
            // meldung
            case M_ENTWURF:
                return Stream.of(
                        Status.M_GELOESCHT,
                        Status.M_GEMELDET);
            case M_GEMELDET:
                return Stream.of(
                        Status.M_UNSTIMMIG,
                        Status.M_GELOESCHT,
                        Status.M_ZUGEORDNET);
            case M_ZUGEORDNET:
                return Stream.of(
                        Status.A_INARBEIT);
            case M_UNSTIMMIG:
                return Stream.of(
                        Status.M_GEMELDET,
                        Status.M_GELOESCHT);

            // anforderung
            case A_INARBEIT:
                return Stream.of(
                        Status.A_UNSTIMMIG,
                        Status.A_FTABGESTIMMT,
                        Status.A_GELOESCHT);
            case A_UNSTIMMIG:
                return Stream.of(
                        Status.A_FTABGESTIMMT,
                        Status.A_GELOESCHT);
            case A_FTABGESTIMMT:
                return Stream.of(
                        Status.A_PLAUSIB,
                        Status.A_UNSTIMMIG,
                        Status.A_GELOESCHT);
            case A_PLAUSIB:
                return Stream.of(
                        Status.A_UNSTIMMIG,
                        Status.A_GELOESCHT);
            case A_FREIGEGEBEN:
                return Stream.of(
                        Status.A_KEINE_WEITERVERFOLG);
            case A_KEINE_WEITERVERFOLG:
                return Stream.of(
                        Status.A_GELOESCHT);
            case A_GELOESCHT:
            case M_GELOESCHT:
            case M_ANGELEGT:
            default:
                return Stream.empty();
        }
    }

}
