package de.interfaceag.bmw.pzbk.anforderungsworkflow.dialogs;

import de.interfaceag.bmw.pzbk.enums.Page;
import org.primefaces.context.RequestContext;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ViewScoped
@Named
public class MeldungZuordnungsDialogController implements Serializable {

    @Inject
    private MeldungZuordnenDialogFacade facade;

    private Long anforderungId;
    private final Collection<MeldungZuordnenDialogMeldungDto> meldungenToRemove = new ArrayList<>();
    private Collection<MeldungZuordnenDialogMeldungDto> meldungenChosen = new ArrayList<>();
    private Collection<MeldungZuordnenDialogMeldungDto> choosableMeldungen = new ArrayList<>();

    public void showMeldungenZuordnenDialog(Long anforderungId) {
        this.anforderungId = anforderungId;
        initData();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('meldungenZuordnenDialog').show()");
    }

    private void initData() {
        initMeldungenChosen();
        initChoosableMeldungen();
    }

    private void initChoosableMeldungen() {
        this.choosableMeldungen = facade.getSelectableMeldungenForDialog(anforderungId);
        this.choosableMeldungen.removeAll(meldungenChosen);
    }

    private void initMeldungenChosen() {
        this.meldungenChosen = facade.getSelectedMeldungenForDialog(anforderungId);
    }

    public Collection<MeldungZuordnenDialogMeldungDto> getChosenMeldungen() {
        return Collections.unmodifiableCollection(meldungenChosen);
    }

    public void addMeldung(MeldungZuordnenDialogMeldungDto meldung) {
        if (!this.meldungenChosen.contains(meldung)) {
            this.meldungenChosen.add(meldung);
            this.meldungenToRemove.remove(meldung);
            this.choosableMeldungen.remove(meldung);
        }
    }

    public void removeMeldung(MeldungZuordnenDialogMeldungDto meldung) {
        this.meldungenToRemove.add(meldung);
        this.meldungenChosen.remove(meldung);
        initChoosableMeldungen();
    }

    public String discardChanges() {
        return getUrl(anforderungId);
    }

    public String save() {
        facade.addMeldungenToAnforderung(anforderungId, meldungenChosen);
        facade.removeMeldungenFromAnforderung(anforderungId, meldungenToRemove);
        return getUrl(anforderungId);
    }

    private static String getUrl(Long anforderungId) {
        if (anforderungId == null) {
            return Page.DASHBOARD.getUrl() + "?faces-redirect=true";
        }
        return Page.ANFORDERUNGVIEW.getUrl() + "?id=" + Long.toString(anforderungId) + "&faces-redirect=true";
    }

    public int getAnzahlChosenMeldungen() {
        return this.meldungenChosen.size();
    }

    public Collection<MeldungZuordnenDialogMeldungDto> getChoosableMeldungen() {
        return choosableMeldungen;
    }
}
