package de.interfaceag.bmw.pzbk.anforderungsworkflow.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author evda
 */
public final class AnforderungHistoryDto implements Serializable {

    private final Long anforderungId;
    private final String kennzeichen;
    private final Date zeitpunkt;
    private final String benutzer;
    private final String attribut;
    private final String von;
    private final String auf;

    private AnforderungHistoryDto(Long anforderungId, String kennzeichen, Date zeitpunkt,
            String benutzer, String attribut, String von, String auf) {
        this.anforderungId = anforderungId;
        this.kennzeichen = kennzeichen;
        this.zeitpunkt = zeitpunkt;
        this.benutzer = benutzer;
        this.attribut = attribut;
        this.von = von;
        this.auf = auf;
    }

    @Override
    public String toString() {
        return kennzeichen + anforderungId + ": " + zeitpunkt + " | " + benutzer + " | " + attribut + " | " + von + " | " + auf;
    }

    public static AnforderungHistoryDtoBuilder newBuilder() {
        return new AnforderungHistoryDtoBuilder();
    }

    public static class AnforderungHistoryDtoBuilder {

        Long anforderungId;
        String kennzeichen;
        Date zeitpunkt;
        String benutzer;
        String attribut;
        String von;
        String auf;

        public AnforderungHistoryDtoBuilder() {

        }

        public AnforderungHistoryDtoBuilder withAnforderungMeldungId(Long id) {
            this.anforderungId = id;
            return this;
        }

        public AnforderungHistoryDtoBuilder withKennzeichen(String kennzeichen) {
            this.kennzeichen = kennzeichen;
            return this;
        }

        public AnforderungHistoryDtoBuilder withZeitpunkt(Date datum) {
            this.zeitpunkt = datum;
            return this;
        }

        public AnforderungHistoryDtoBuilder withBenutzer(String benutzer) {
            this.benutzer = benutzer;
            return this;
        }

        public AnforderungHistoryDtoBuilder withAttribut(String attribut) {
            this.attribut = attribut;
            return this;
        }

        public AnforderungHistoryDtoBuilder withVon(String von) {
            this.von = von;
            return this;
        }

        public AnforderungHistoryDtoBuilder withAuf(String auf) {
            this.auf = auf;
            return this;
        }

        public AnforderungHistoryDto build() {
            return new AnforderungHistoryDto(anforderungId, kennzeichen, zeitpunkt, benutzer, attribut, von, auf);
        }

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.anforderungId);
        hash = 43 * hash + Objects.hashCode(this.kennzeichen);
        hash = 43 * hash + Objects.hashCode(this.zeitpunkt);
        hash = 43 * hash + Objects.hashCode(this.benutzer);
        hash = 43 * hash + Objects.hashCode(this.attribut);
        hash = 43 * hash + Objects.hashCode(this.von);
        hash = 43 * hash + Objects.hashCode(this.auf);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnforderungHistoryDto other = (AnforderungHistoryDto) obj;
        if (!Objects.equals(this.kennzeichen, other.kennzeichen)) {
            return false;
        }
        if (!Objects.equals(this.benutzer, other.benutzer)) {
            return false;
        }
        if (!Objects.equals(this.attribut, other.attribut)) {
            return false;
        }
        if (!Objects.equals(this.von, other.von)) {
            return false;
        }
        if (!Objects.equals(this.auf, other.auf)) {
            return false;
        }
        if (!Objects.equals(this.anforderungId, other.anforderungId)) {
            return false;
        }
        return Objects.equals(this.zeitpunkt, other.zeitpunkt);
    }

    //------------ Getter -------------
    public Long getAnforderungId() {
        return anforderungId;
    }

    public String getKennzeichen() {
        return kennzeichen;
    }

    public String getZeitpunkt() {
        if (zeitpunkt == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return sdf.format(zeitpunkt);
    }

    public Date getZeitpunktAsDate() {
        return zeitpunkt;
    }

    public String getBenutzer() {
        return benutzer;
    }

    public String getAttribut() {
        return attribut;
    }

    public String getVon() {
        return von;
    }

    public String getAuf() {
        return auf;
    }

}
