package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDialogViewData;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.ProzessbaukastenLinkData;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author fn
 */
public class AnforderungMitMeldungViewData implements Serializable {

    private final ProzessbaukastenZuordnenDialogViewData prozessbaukastenZuordnenDialogViewData;
    private final Boolean hasZuordnungZuProzessbaukasten;
    private final List<ProzessbaukastenLinkData> prozessbaukastenLinks;

    private final List<ZuordnungAnforderungDerivat> derivatZuordnungenWithBerechtigung;

    public AnforderungMitMeldungViewData(ProzessbaukastenZuordnenDialogViewData prozessbaukastenZuordnenDialogViewData, Boolean hasZuordnungZuProzessbaukasten, List<ProzessbaukastenLinkData> prozessbaukastenLinks, List<ZuordnungAnforderungDerivat> derivatZuordnungenWithBerechtigung) {
        this.prozessbaukastenZuordnenDialogViewData = prozessbaukastenZuordnenDialogViewData;
        this.hasZuordnungZuProzessbaukasten = hasZuordnungZuProzessbaukasten;
        this.prozessbaukastenLinks = (prozessbaukastenLinks != null) ? prozessbaukastenLinks : Collections.emptyList();
        this.derivatZuordnungenWithBerechtigung = derivatZuordnungenWithBerechtigung;
    }

    public Optional<ProzessbaukastenZuordnenDialogViewData> getProzessbaukastenZuordnenDialogViewData() {
        return Optional.ofNullable(prozessbaukastenZuordnenDialogViewData);
    }

    public Boolean hasZuordnungZuProzessbaukasten() {
        return hasZuordnungZuProzessbaukasten;
    }

    public List<ProzessbaukastenLinkData> getProzessbaukastenLinks() {
        return prozessbaukastenLinks;
    }

    public List<ZuordnungAnforderungDerivat> getDerivatZuordnungenWithBerechtigung() {
        return derivatZuordnungenWithBerechtigung;
    }

}
