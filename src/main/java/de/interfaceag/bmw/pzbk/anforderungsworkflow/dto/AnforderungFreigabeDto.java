package de.interfaceag.bmw.pzbk.anforderungsworkflow.dto;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalButtonData;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;

import java.io.Serializable;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AnforderungFreigabeDto implements Serializable, AnforderungEditFahrzeugmerkmalButtonData {

    private Long id;
    private Long anforderungId;

    private Long seTeamId;
    private String seTeamName;

    private String modulName;

    private boolean freigabe;
    private boolean abgelehnt;
    private String kommentar;
    private VereinbarungType vereinbarungType;

    private boolean fahrzeugmerkmalConfigured;

    public AnforderungFreigabeDto(Long seTeamId, String seTeamName, String modulName, VereinbarungType vereinbarungType) {
        this.seTeamId = seTeamId;
        this.seTeamName = seTeamName;
        this.modulName = modulName;
        this.freigabe = false;
        this.abgelehnt = false;
        this.kommentar = "";
        this.vereinbarungType = vereinbarungType;
    }

    public AnforderungFreigabeDto(Long id, Long anforderungId, Long seTeamId, String seTeamName,
            boolean freigabe, boolean abgelehnt, String kommentar, VereinbarungType vereinbarungType,
            String modulName) {
        this.id = id;
        this.anforderungId = anforderungId;
        this.seTeamId = seTeamId;
        this.seTeamName = seTeamName;
        this.freigabe = freigabe;
        this.abgelehnt = abgelehnt;
        this.kommentar = kommentar;
        this.vereinbarungType = vereinbarungType;
        this.modulName = modulName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAnforderungId() {
        return anforderungId;
    }

    public void setAnforderungId(Long anforderungId) {
        this.anforderungId = anforderungId;
    }

    public Long getSeTeamId() {
        return seTeamId;
    }

    public ModulSeTeamId getModulSeTeamId() {
        return new ModulSeTeamId(seTeamId);
    }

    public void setSeTeamId(Long seTeamId) {
        this.seTeamId = seTeamId;
    }

    public String getSeTeamName() {
        return seTeamName;
    }

    public void setSeTeamName(String seTeamName) {
        this.seTeamName = seTeamName;
    }

    public boolean isFreigabe() {
        return freigabe;
    }

    public void setFreigabe(boolean freigabe) {
        this.freigabe = freigabe;
    }

    public boolean isAbgelehnt() {
        return abgelehnt;
    }

    public void setAbgelehnt(boolean abgelehnt) {
        this.abgelehnt = abgelehnt;
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    public VereinbarungType getVereinbarungType() {
        return vereinbarungType;
    }

    public void setVereinbarungType(VereinbarungType vereinbarungType) {
        this.vereinbarungType = vereinbarungType;
    }

    public boolean isZak() {
        return this.vereinbarungType.isZak();
    }

    public void setZak(boolean isZak) {
        this.vereinbarungType = VereinbarungType.getVereinbarungTypeForZAK(isZak);
    }

    public String getModulName() {
        return modulName;
    }

    public void setModulName(String modulName) {
        this.modulName = modulName;
    }

    @Override
    public boolean isConfigured() {
        return fahrzeugmerkmalConfigured;
    }

    public void setConfiguredTrue() {
        this.fahrzeugmerkmalConfigured = Boolean.TRUE;
    }

    public void setFahrzeugmerkmalConfigured(boolean fahrzeugmerkmalConfigured) {
        this.fahrzeugmerkmalConfigured = fahrzeugmerkmalConfigured;
    }
}
