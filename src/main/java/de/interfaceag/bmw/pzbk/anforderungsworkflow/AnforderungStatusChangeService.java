package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.mail.MailService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.AnforderungReportingStatusTransitionAdapter;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 * @author sl
 */
@Stateless
public class AnforderungStatusChangeService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungStatusChangeService.class);

    @Inject
    private Session session;
    @Inject
    private AnforderungService anforderungService;
    @Inject
    private AnforderungMeldungHistoryService anforderungMeldungHistoryService;
    @Inject
    private MailService mailService;
    @Inject
    private AnforderungReportingStatusTransitionAdapter reportingStatusTransitionAdapter;

    public Anforderung changeAnforderungStatus(Anforderung anforderung, Status newStatus, String statusChangeKommentar) {
        int newStatusId = newStatus.getStatusId();
        return changeAnforderungStatus(anforderung, newStatusId, statusChangeKommentar);
    }

    public Anforderung changeAnforderungStatus(Anforderung anforderung, Status newStatus, String statusChangeKommentar, Mitarbeiter currentUser) {
        int newStatusId = newStatus.getStatusId();
        return changeAnforderungStatus(anforderung, newStatusId, statusChangeKommentar, currentUser);
    }

    public Anforderung changeAnforderungStatus(Anforderung anforderung, int newStatusId, String statusChangeKommentar, Mitarbeiter currentUser) {
        LOG.debug("Change status for anforderung {} to new status {} with statusChangeKommentar {}", anforderung, newStatusId, statusChangeKommentar);

        Anforderung anforderungCopy = getAnforderungCopy(anforderung);
        final Status currentStatus = anforderungCopy.getStatus();
        final Status newStatus = Status.getStatusById(newStatusId);

        updateAnforderung(anforderung, newStatus, statusChangeKommentar);

        if (newStatus.equals(Status.A_GELOESCHT)) {
            updateMeldungForAnforderungGeloescht(anforderung);
        } else if (newStatus.equals(Status.A_FREIGEGEBEN)) {
            updateAllAnforderungFreigabenForAnforderung(anforderung, statusChangeKommentar, currentUser);
        }

        reportingStatusTransitionAdapter.changeAnforderungStatus(anforderung, currentStatus, newStatus);

        writeHistoryEntryForAnforderung(anforderung, anforderungCopy, currentUser);
        sendMailForAnforderung(anforderung, currentUser);
        return anforderung;
    }

    public Anforderung changeAnforderungStatus(Anforderung anforderung, int newStatusId, String statusChangeKommentar) {
        return changeAnforderungStatus(anforderung, newStatusId, statusChangeKommentar, session.getUser());
    }

    private void sendMailForAnforderung(Anforderung anforderung, Mitarbeiter currentUser) {
        mailService.sendMailAnforderungChanged(anforderung, currentUser);
    }

    private void writeHistoryEntryForAnforderung(Anforderung anforderung, Anforderung anforderungCopy, Mitarbeiter currentUser) {
        anforderungMeldungHistoryService.writeHistoryForChangedValuesOfAnfoMgmtObject(currentUser, anforderung, anforderungCopy);
    }

    private Anforderung updateAllAnforderungFreigabenForAnforderung(Anforderung anforderung, String statusChangeKommentar, Mitarbeiter currentUser) {
        if ("".equals(statusChangeKommentar) || statusChangeKommentar == null) {
            statusChangeKommentar = "Wurde freigegeben";
        }
        return anforderungService.anforderungForAllMyModulSeTeamsFreigeben(anforderung, currentUser, statusChangeKommentar);
    }

    private void updateMeldungForAnforderungGeloescht(Anforderung anforderung) {
        final Set<Meldung> meldungen = anforderung.getMeldungen();
        Iterator<Meldung> iterator = meldungen.iterator();

        while (iterator.hasNext()) {
            Meldung meldung = iterator.next();

            if (meldungHasExactlyOneAnforderung(meldung)) {
                Meldung copy = getMeldungCopy(meldung);
                updateMeldungStatusToGeloescht(meldung);
                writeHistoryEntryForMeldung(meldung, copy);
            }
        }
    }

    private void writeHistoryEntryForMeldung(Meldung copy, Meldung meldung) {
        Mitarbeiter currentUser = session.getUser();
        anforderungMeldungHistoryService.writeHistoryForChangedValuesOfAnfoMgmtObject(currentUser, copy, meldung);
    }

    private void updateMeldungStatusToGeloescht(Meldung meldung) {
        meldung.setStatus(Status.M_GELOESCHT);
        meldung.setAenderungsdatum(new Date());
        meldung.setZeitpunktStatusaenderung(new Date());
        anforderungService.saveMeldung(meldung);
    }

    private static boolean meldungHasExactlyOneAnforderung(Meldung meldung) {
        return meldung.getAnforderung() != null && meldung.getAnforderung().size() == 1;
    }

    private Meldung getMeldungCopy(Meldung meldung) {
        return anforderungService.getMeldungWorkCopyByIdOrNewMeldung(meldung);
    }

    private void updateAnforderung(Anforderung anforderung, final Status newStatus, String statusChangeKommentar) {
        anforderung.setNextStatus(newStatus);
        anforderung.setStatusChangeRequested(true);
        anforderung.setAenderungsdatum(new Date());
        anforderung.setZeitpunktStatusaenderung(new Date());
        anforderung.setStatus(newStatus);
        anforderung.setStatusWechselKommentar(statusChangeKommentar);
        anforderungService.saveAnforderung(anforderung);
    }

    private Anforderung getAnforderungCopy(Anforderung anforderung) {
        return anforderungService.getAnforderungWorkCopyByIdOrNewAnforderung(anforderung.getId());
    }

}
