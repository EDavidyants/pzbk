package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalDialogData;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalData;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungModulSeTeamFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Stateless
public class AnforderungEditFahrzeugmerkmalSaveService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(AnforderungEditFahrzeugmerkmalSaveService.class);

    @Inject
    private ModulService modulService;
    @Inject
    private FahrzeugmerkmalService fahrzeugmerkmalService;
    @Inject
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService anforderungModulSeTeamFahrzeugmerkmalAuspraegungService;

    public void saveAnforderungFahrzeugmerkmalChanges(Anforderung anforderung, AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData) {
        final Collection<AnforderungEditFahrzeugmerkmalDialogData> changedDialogDataSets = getChangedDialogDataSets(anforderungEditFahrzeugmerkmalData);

        for (AnforderungEditFahrzeugmerkmalDialogData anforderungEditFahrzeugmerkmalDialogData : changedDialogDataSets) {
            saveChangesForModulSeTeam(anforderung, anforderungEditFahrzeugmerkmalDialogData);
        }
    }

    private void saveChangesForModulSeTeam(Anforderung anforderung, AnforderungEditFahrzeugmerkmalDialogData anforderungEditFahrzeugmerkmalDialogDataSet) {
        final List<AnforderungEditFahrzeugmerkmal> fahrzeugmerkmale = anforderungEditFahrzeugmerkmalDialogDataSet.getFahrzeugmerkmale();
        final ModulSeTeamId modulSeTeamId = anforderungEditFahrzeugmerkmalDialogDataSet.getModulSeTeamId();

        final List<FahrzeugmerkmalAuspraegung> existingAuspraegungen = getExistingSelectedAuspraegungenForAnforderungModulSeTeam(anforderung, modulSeTeamId);

        final Collection<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> newEntries = addNewEntriesForAnforderungModulSeTeamFahrzeugmerkmale(anforderung, fahrzeugmerkmale, modulSeTeamId, existingAuspraegungen);
        LOG.debug("new entries {}", newEntries);

        final Collection<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> removedEntries = removeEntriesForAnforderungModulSeTeamFahrzeugmerkmale(anforderung, fahrzeugmerkmale, modulSeTeamId, existingAuspraegungen);
        LOG.debug("removed entries {}", removedEntries);
    }

    private Collection<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> removeEntriesForAnforderungModulSeTeamFahrzeugmerkmale(Anforderung anforderung,
                                                                                                                                List<AnforderungEditFahrzeugmerkmal> fahrzeugmerkmale,
                                                                                                                                ModulSeTeamId modulSeTeamId,
                                                                                                                                List<FahrzeugmerkmalAuspraegung> existingAuspraegungen) {

        final Set<FahrzeugmerkmalAuspraegungId> selectedFahrzeugmerkmalAuspraegungIds = getSelectedFahrzeugmerkmalAuspraegungids(fahrzeugmerkmale);

        final Iterator<FahrzeugmerkmalAuspraegung> auspraegungIterator = existingAuspraegungen.stream()
                .filter(auspraegung -> !selectedFahrzeugmerkmalAuspraegungIds.contains(auspraegung.getFahrzeugmerkmalAuspraegungId()))
                .iterator();

        Collection<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> removedEntries = new ArrayList<>();
        while (auspraegungIterator.hasNext()) {
            final FahrzeugmerkmalAuspraegung auspraegung = auspraegungIterator.next();
            removeEntryForAnforderungModulSeTeamFahrzeugmerkmalAuspraegung(anforderung, modulSeTeamId, auspraegung, removedEntries);
        }
        return removedEntries;
    }

    private void removeEntryForAnforderungModulSeTeamFahrzeugmerkmalAuspraegung(Anforderung anforderung, ModulSeTeamId modulSeTeamId,
                                                                                FahrzeugmerkmalAuspraegung auspraegung,
                                                                                Collection<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> removedEntries) {

        final FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId = auspraegung.getFahrzeugmerkmalAuspraegungId();

        final Optional<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> entryForAnforderungModulSeTeamFahrzeugmerkmalAuspraegung = anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungModulSeTeamAndFahrzeugmerkmalAuspraegung(anforderung.getAnforderungId(), modulSeTeamId, fahrzeugmerkmalAuspraegungId);

        if (entryForAnforderungModulSeTeamFahrzeugmerkmalAuspraegung.isPresent()) {
            final AnforderungModulSeTeamFahrzeugmerkmalAuspraegung entry = entryForAnforderungModulSeTeamFahrzeugmerkmalAuspraegung.get();
            anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.remove(entry);
            LOG.info("remove {}", entry);
            removedEntries.add(entry);
        } else {
            LOG.error("Could not find AnforderungModulSeTeamFahrzeugmerkmalAuspraegung for {}, {}, {}", anforderung.getAnforderungId(), modulSeTeamId, fahrzeugmerkmalAuspraegungId);
        }
    }

    private static Set<FahrzeugmerkmalAuspraegungId> getSelectedFahrzeugmerkmalAuspraegungids(List<AnforderungEditFahrzeugmerkmal> fahrzeugmerkmale) {
        return fahrzeugmerkmale.stream()
                    .map(AnforderungEditFahrzeugmerkmal::getSelectedAuspraegungen)
                    .flatMap(Collection::stream)
                    .map(AnforderungEditFahrzeugmerkmalAuspraegung::getFahrzeugmerkmalAuspraegungId)
                    .collect(Collectors.toSet());
    }

    private Collection<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> addNewEntriesForAnforderungModulSeTeamFahrzeugmerkmale(Anforderung anforderung,
                                                                                                                                List<AnforderungEditFahrzeugmerkmal> fahrzeugmerkmale,
                                                                                                                                ModulSeTeamId modulSeTeamId,
                                                                                                                                List<FahrzeugmerkmalAuspraegung> existingAuspraegungen) {
        final Set<FahrzeugmerkmalAuspraegungId> existingAuspraegungIds = getExistingAuspraegungIds(existingAuspraegungen);
        final Collection<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> newEntries = new ArrayList<>();

        for (AnforderungEditFahrzeugmerkmal fahrzeugmerkmal : fahrzeugmerkmale) {
            newEntries.addAll(addNewEntriesForAnforderungModulSeTeamFahrzeugmerkmal(anforderung, modulSeTeamId, existingAuspraegungIds, fahrzeugmerkmal));
        }
        return newEntries;
    }

    private Collection<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> addNewEntriesForAnforderungModulSeTeamFahrzeugmerkmal(Anforderung anforderung,
                                                                                                                               ModulSeTeamId modulSeTeamId,
                                                                                                                               Set<FahrzeugmerkmalAuspraegungId> existingAuspraegungIds,
                                                                                                                               AnforderungEditFahrzeugmerkmal fahrzeugmerkmal) {
        final List<AnforderungEditFahrzeugmerkmalAuspraegung> selectedAuspraegungen = fahrzeugmerkmal.getSelectedAuspraegungen();
        final Collection<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> newEntries = new ArrayList<>();
        for (AnforderungEditFahrzeugmerkmalAuspraegung auspraegung : selectedAuspraegungen) {
            addNewEntryForSelectedAuspraegungIfItIsNotPresentInDatabase(anforderung, modulSeTeamId, existingAuspraegungIds, auspraegung).ifPresent(newEntries::add);
        }
        return newEntries;
    }

    private Optional<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> addNewEntryForSelectedAuspraegungIfItIsNotPresentInDatabase(Anforderung anforderung, ModulSeTeamId seTeamId,
                                                                                                                                   Set<FahrzeugmerkmalAuspraegungId> existingAuspraegungIds,
                                                                                                                                   AnforderungEditFahrzeugmerkmalAuspraegung auspraegungDto) {
        final FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId = auspraegungDto.getFahrzeugmerkmalAuspraegungId();

        if (!existingAuspraegungIds.contains(fahrzeugmerkmalAuspraegungId)) {
            return createNewEntryForSelectedAuspraegung(anforderung, seTeamId, fahrzeugmerkmalAuspraegungId);
        } else {
            LOG.debug("Found existing entry in database for {}", fahrzeugmerkmalAuspraegungId);
            return Optional.empty();
        }
    }

    private Optional<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> createNewEntryForSelectedAuspraegung(Anforderung anforderung, ModulSeTeamId seTeamId, FahrzeugmerkmalAuspraegungId auspraegungId) {
        final Optional<ModulSeTeam> seTeamById = Optional.ofNullable(modulService.getSeTeamById(seTeamId.getId()));
        final Optional<FahrzeugmerkmalAuspraegung> auspraegungById = fahrzeugmerkmalService.findFahrzeugmerkmalAuspraegung(auspraegungId);

        if (seTeamById.isPresent() && auspraegungById.isPresent()) {
            final ModulSeTeam modulSeTeam = seTeamById.get();
            final FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung = auspraegungById.get();
            return Optional.of(createAndSaveNewEntry(anforderung, modulSeTeam, fahrzeugmerkmalAuspraegung));
        } else {
            LOG.error("ModulSeTeam {} or FahrzeugmerkmalAuspraegung {} is null. Could not write new entry", seTeamById, auspraegungById);
            return Optional.empty();
        }
    }

    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegung createAndSaveNewEntry(Anforderung anforderung, ModulSeTeam modulSeTeam, FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung) {
        AnforderungModulSeTeamFahrzeugmerkmalAuspraegung newEntry = new AnforderungModulSeTeamFahrzeugmerkmalAuspraegung(anforderung, modulSeTeam, fahrzeugmerkmalAuspraegung);
        LOG.info("Create {}", newEntry);
        anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.save(newEntry, false);
        return newEntry;
    }

    private List<FahrzeugmerkmalAuspraegung> getExistingSelectedAuspraegungenForAnforderungModulSeTeam(Anforderung anforderung, ModulSeTeamId modulSeTeamId) {
        return anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(anforderung.getAnforderungId(), modulSeTeamId);
    }

    private static Collection<AnforderungEditFahrzeugmerkmalDialogData> getChangedDialogDataSets(AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData) {
        final Map<ModulSeTeamId, AnforderungEditFahrzeugmerkmalDialogData> dialogDataMap = anforderungEditFahrzeugmerkmalData.getModulSeTeamFahrzeugmerkmalDialogDataMap();
        return dialogDataMap.values();
    }

    private static Set<FahrzeugmerkmalAuspraegungId> getExistingAuspraegungIds(List<FahrzeugmerkmalAuspraegung> existingAuspraegungen) {
        return existingAuspraegungen.stream().map(FahrzeugmerkmalAuspraegung::getFahrzeugmerkmalAuspraegungId).collect(Collectors.toSet());
    }

}
