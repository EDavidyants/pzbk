package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;

import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
final class AnforderungViewUtils {

    private AnforderungViewUtils() {
    }

    static boolean hasRightToSetTteam(Status status, List<Rolle> userRoles) {
        if (status != null && userRoles != null && !userRoles.isEmpty()) {
            boolean correctStatus = getEditTteamStatus().anyMatch(s -> s.equals(status));
            boolean correctRole = checkForCorrectRole(getEditTteamRoles(), userRoles);
            return correctStatus && correctRole;
        }
        return false;
    }

    private static boolean checkForCorrectRole(Stream<Rolle> validRoles, List<Rolle> userRoles) {
        if (validRoles != null && userRoles != null) {
            return validRoles.anyMatch(userRoles::contains);
        }
        return false;
    }

    static Stream<Status> getEditTteamStatus() {
        return Stream.of(Status.M_GEMELDET,
                Status.A_FTABGESTIMMT,
                Status.A_INARBEIT,
                Status.A_UNSTIMMIG);
    }

    private static Stream<Rolle> getEditTteamRoles() {
        return Stream.of(Rolle.SENSORCOCLEITER,
                Rolle.SCL_VERTRETER,
                Rolle.ADMIN,
                Rolle.T_TEAMLEITER, Rolle.TTEAM_VERTRETER);
    }
}
