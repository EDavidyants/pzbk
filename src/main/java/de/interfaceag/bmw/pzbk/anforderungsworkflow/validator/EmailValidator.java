package de.interfaceag.bmw.pzbk.anforderungsworkflow.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Pattern;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@FacesValidator("MailValidator")
public class EmailValidator implements Validator {

    private static final Pattern EMAIL_PATTERN = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");

    @Override
    public void validate(FacesContext facesContext,
                         UIComponent uiComponent,
                         Object object) {

        if (object == null) {
            return;
        }

        String email = (String) object;
        email = email.toLowerCase();
        if (email.isEmpty()) {
            return;
        }

        boolean matches = EMAIL_PATTERN.matcher(email)
                .matches();
        if (!matches) {
            FacesMessage msg = new FacesMessage(
                    FacesMessage.SEVERITY_FATAL,
                    "Email is invalid",
                    null);
            throw new ValidatorException(msg);
        }

    }

    public static boolean validate(String email) {
        if (email.isEmpty()) {
            return false;
        }
        email = email.toLowerCase();

        return EMAIL_PATTERN.matcher(email)
                .matches();
    }
}
