package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ViewScoped
@Named
public class AnforderungViewController implements Serializable {

    @Inject
    private AnforderungViewFacade facade;

    private boolean hasMeldung;

    private String id;
    private String fachId;
    private String version;

    @PostConstruct
    public void init() {
        UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();
        hasMeldung = facade.anforderungHasMeldung(urlParameter);
    }

    public boolean isHasMeldung() {
        return hasMeldung;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFachId() {
        return fachId;
    }

    public void setFachId(String fachId) {
        this.fachId = fachId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
