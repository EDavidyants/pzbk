package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api;

import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;

import java.io.Serializable;
import java.util.List;

public interface AnforderungEditFahrzeugmerkmalDialogData extends Serializable {

    ModulSeTeamId getModulSeTeamId();

    List<AnforderungEditFahrzeugmerkmal> getFahrzeugmerkmale();

    void setFahrzeugmerkmale(List<AnforderungEditFahrzeugmerkmal> fahrzeugmerkmale);

}
