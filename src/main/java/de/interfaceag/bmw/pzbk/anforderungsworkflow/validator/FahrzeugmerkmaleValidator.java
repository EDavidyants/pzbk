package de.interfaceag.bmw.pzbk.anforderungsworkflow.validator;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungFreigabeDto;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.util.List;

public final class FahrzeugmerkmaleValidator {

    private FahrzeugmerkmaleValidator() {
    }

    public static void validateFahrzeugmerkmale(FacesContext context, UIComponent component, Object value, List<AnforderungFreigabeDto> anforderungFreigabeDto) {
        String summary = "Fahrzeugmerkmale no message defined";
        if (value instanceof String) {
            summary = (String) value;
        }
        String detail = "----------Fahrzeugmerkmale sind nicht configuriert-----------";
        boolean isNotConfigured = isNotSet(anforderungFreigabeDto);
        if (anforderungFreigabeDto == null || anforderungFreigabeDto.isEmpty() || isNotConfigured) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
        }
    }

    private static boolean isNotSet(List<AnforderungFreigabeDto> anforderungFreigabeDto) {
        long fahrzeugmerkmalIsNotConfig = anforderungFreigabeDto.stream().filter(dto -> !dto.isConfigured()).count();
        return fahrzeugmerkmalIsNotConfig > 0L;
    }
}
