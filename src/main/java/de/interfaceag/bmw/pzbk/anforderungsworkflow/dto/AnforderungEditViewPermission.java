package de.interfaceag.bmw.pzbk.anforderungsworkflow.dto;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.permission.AnforderungViewPermissionUtils;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author evda
 */
public class AnforderungEditViewPermission implements Serializable {

    private final EditPermission fahrzeugmerkmaleButton;
    private final EditPermission umsetzerPanel;

    public AnforderungEditViewPermission(Set<Rolle> rolesWithWritePermissions, Status currentStatus) {
        fahrzeugmerkmaleButton = AnforderungViewPermissionUtils.buildPermissionForFahrzeugmerkmaleButton(rolesWithWritePermissions, currentStatus);
        umsetzerPanel = AnforderungViewPermissionUtils.buildPermissionForUmsetzerPanel(rolesWithWritePermissions, currentStatus);
    }

    public boolean getFahrzeugmerkmaleButton() {
        return fahrzeugmerkmaleButton.hasRightToEdit();
    }

    public boolean getUmsetzerPanel() {
        return umsetzerPanel.hasRightToEdit();
    }

}
