package de.interfaceag.bmw.pzbk.anforderungsworkflow.validator;

import de.interfaceag.bmw.pzbk.dialog.viewdata.DialogUmsetzerDto;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.util.Set;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class UmsetzerValidator {

    private UmsetzerValidator() {
    }

    public static void validateUmsetzer(FacesContext context, UIComponent component, Object value, Set<DialogUmsetzerDto> umsetzers) {
        String summary = "Umsetzer no message defined";
        if (value instanceof String) {
            summary = (String) value;
        }
        String detail = "----------Umsetzer leer-----------";
        if (umsetzers == null || umsetzers.isEmpty()) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
        }
    }

}
