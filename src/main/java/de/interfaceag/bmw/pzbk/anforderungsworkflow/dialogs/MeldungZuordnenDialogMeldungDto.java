package de.interfaceag.bmw.pzbk.anforderungsworkflow.dialogs;

import de.interfaceag.bmw.pzbk.entities.Meldung;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class MeldungZuordnenDialogMeldungDto implements Serializable {

    private final long id;
    private final String fachId;
    private final String technologie;
    private final String beschreibung;

    public MeldungZuordnenDialogMeldungDto(long id, String fachId, String technologie, String beschreibung) {
        this.id = id;
        this.fachId = fachId;
        this.technologie = technologie;
        this.beschreibung = beschreibung;
    }

    public long getId() {
        return id;
    }

    public String getFachId() {
        return fachId;
    }

    public String getTechnologie() {
        return technologie;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public static Collection<MeldungZuordnenDialogMeldungDto> getDtosForMeldungen(Collection<Meldung> meldungen) {
        if (meldungen == null) {
            return Collections.emptyList();
        }
        List<MeldungZuordnenDialogMeldungDto> result = new ArrayList<>();
        Iterator<Meldung> iterator = meldungen.iterator();
        while (iterator.hasNext()) {
            Meldung meldung = iterator.next();
            result.add(new MeldungZuordnenDialogMeldungDto(meldung.getId(), meldung.getFachId(), meldung.getSensorCoc().getTechnologie(), meldung.getBeschreibungAnforderungDe()));
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MeldungZuordnenDialogMeldungDto other = (MeldungZuordnenDialogMeldungDto) obj;
        return this.id == other.id;
    }

}
