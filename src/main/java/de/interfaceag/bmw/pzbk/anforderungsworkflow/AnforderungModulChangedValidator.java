package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungReadData;
import de.interfaceag.bmw.pzbk.enums.Status;

import java.util.List;

/**
 *
 * @author fn
 */
public final class AnforderungModulChangedValidator {

    private AnforderungModulChangedValidator() {
    }

    public static boolean isModulChangedInStatusPlausibilisiert(AnforderungReadData anforderung, List<AnforderungFreigabeBeiModulSeTeam> originalFreigabeBeiModulSeTeam) {

        if (anforderung == null) {
            return Boolean.FALSE;
        }

        // only status plausibilisiert is relevant for us
        if (!anforderung.getStatus().equals(Status.A_PLAUSIB)) {
            return Boolean.FALSE;
        }

        List<AnforderungFreigabeBeiModulSeTeam> newFreigabeBeiModulSeTeam = anforderung.getAnfoFreigabeBeiModul();
        //List<ModulSeTeam> newModulSeTeam = newFreigabeBeiModulSeTeam.
        if (newFreigabeBeiModulSeTeam.size() != originalFreigabeBeiModulSeTeam.size()) {
            return Boolean.TRUE;
        }

        boolean isNotInOriginal;

        for (AnforderungFreigabeBeiModulSeTeam newAnforderungFreigabeBeiModulSeTeam : newFreigabeBeiModulSeTeam) {
            isNotInOriginal = true;
            for (AnforderungFreigabeBeiModulSeTeam originalAnforderungFreigabeBeiModulSeTeam : originalFreigabeBeiModulSeTeam) {
                if (newAnforderungFreigabeBeiModulSeTeam.getModulSeTeam().equals(originalAnforderungFreigabeBeiModulSeTeam.getModulSeTeam())) {
                    isNotInOriginal = false;
                }
            }
            if (isNotInOriginal) {
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

}
