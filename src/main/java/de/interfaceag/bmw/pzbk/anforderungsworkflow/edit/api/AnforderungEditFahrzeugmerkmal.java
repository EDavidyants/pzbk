package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api;

import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;

import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.List;

public interface AnforderungEditFahrzeugmerkmal extends Serializable {

    FahrzeugmerkmalId getFahrzeugmerkmalId();

    String getMerkmal();

    List<AnforderungEditFahrzeugmerkmalAuspraegung> getAllAuspraegungen();

    List<AnforderungEditFahrzeugmerkmalAuspraegung> getSelectedAuspraegungen();

    void setSelectedAuspraegungen(List<AnforderungEditFahrzeugmerkmalAuspraegung> selectedAuspraegungen);

    Converter getAuspraegungConverter();

}
