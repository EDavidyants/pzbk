package de.interfaceag.bmw.pzbk.anforderungsworkflow.dto;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.permission.AnforderungViewPermissionUtils;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.EditViewPermission;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.TruePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

import static de.interfaceag.bmw.pzbk.anforderungsworkflow.permission.AnforderungViewPermissionUtils.buildPermissionForProzessbaukastenButton;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @author fp
 */
public class AnforderungViewPermission implements Serializable {

    // page
    private final ViewPermission page;

    // header
    private final EditPermission editButton;
    private final EditPermission loeschenButton;
    private final EditPermission wiederherstellenButton;
    private final EditPermission newVersionButton;
    private final EditPermission meldungZuordnenButton;
    private final EditPermission derivatZustandButton;
    private final EditPermission derivatZuordnenButton;
    private final EditPermission statusTransitions;

    // body
    private final ViewPermission derivatPanel;

    private final EditViewPermission prozessbaukastenButton;

    public AnforderungViewPermission(Set<Rolle> rolesWithWritePermissions,
            Status currentStatus, boolean pageBerechtigt, boolean anforderungHasProzessbaukatenZuordnung) {

        derivatPanel = AnforderungViewPermissionUtils.buildPermissionForDerivatPanel(currentStatus);
        derivatZuordnenButton = AnforderungViewPermissionUtils.buildPermissionForDerivatZuordnenButton(
                rolesWithWritePermissions, currentStatus
        );

        if (anforderungHasProzessbaukatenZuordnung) {
            derivatZustandButton = FalsePermission.get();
        } else {
            derivatZustandButton = AnforderungViewPermissionUtils.buildPermissionForDerivatZustandButton(
                    rolesWithWritePermissions, currentStatus
            );
        }
        editButton = AnforderungViewPermissionUtils.buildPermissionForEditButton(
                rolesWithWritePermissions, currentStatus
        );
        meldungZuordnenButton = AnforderungViewPermissionUtils.buildPermissionForMeldungZuordnenButton(
                rolesWithWritePermissions, currentStatus
        );
        newVersionButton = AnforderungViewPermissionUtils.buildPermissionForNewVersionButton(
                rolesWithWritePermissions, currentStatus
        );
        loeschenButton = AnforderungViewPermissionUtils.buildPermissionForLoeschenButton(
                currentStatus, rolesWithWritePermissions
        );
        wiederherstellenButton = AnforderungViewPermissionUtils.buildPermissionForWiederherstellenButton(
                currentStatus, rolesWithWritePermissions
        );

        if (pageBerechtigt) {
            page = new TruePermission();
        } else {
            page = new FalsePermission();
        }

        statusTransitions = AnforderungViewPermissionUtils.buildPermissionsForStatusTransitions(currentStatus,
                rolesWithWritePermissions);

        prozessbaukastenButton = buildPermissionForProzessbaukastenButton(rolesWithWritePermissions);

    }

    public boolean getPage() {
        return page.isRendered();
    }

    public boolean getDerivatPanel() {
        return derivatPanel.isRendered();
    }

    public boolean getEditButton() {
        return editButton.hasRightToEdit();
    }

    public boolean getNewVersionButton() {
        return newVersionButton.hasRightToEdit();
    }

    public boolean getMeldungZuordnenButton() {
        return meldungZuordnenButton.hasRightToEdit();
    }

    public boolean getDerivatZustandButton() {
        return derivatZustandButton.hasRightToEdit();
    }

    public boolean getDerivatZuordnenButton() {
        return derivatZuordnenButton.hasRightToEdit();
    }

    public boolean getLoeschenButton() {
        return loeschenButton.hasRightToEdit();
    }

    public boolean getWiederherstellenButton() {
        return wiederherstellenButton.hasRightToEdit();
    }

    public boolean getStatusTransition() {
        return statusTransitions.hasRightToEdit();
    }

    public EditViewPermission getProzessbaukastenButton() {
        return prozessbaukastenButton;
    }

}
