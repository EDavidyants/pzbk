package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api;

import java.io.Serializable;

public interface AnforderungEditFahrzeugmerkmalButtonData extends Serializable {

    boolean isConfigured();

}
