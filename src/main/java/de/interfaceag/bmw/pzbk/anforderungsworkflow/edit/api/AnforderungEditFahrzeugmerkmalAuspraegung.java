package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api;

import de.interfaceag.bmw.pzbk.shared.objectIds.DomainObject;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;

public interface AnforderungEditFahrzeugmerkmalAuspraegung extends DomainObject {

    Long getId();

    FahrzeugmerkmalAuspraegungId getFahrzeugmerkmalAuspraegungId();

    String getAuspraegung();

}
