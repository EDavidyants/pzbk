package de.interfaceag.bmw.pzbk.anforderungsworkflow.dto;

import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author evda
 */
public final class AnforderungBasisDataDto implements Serializable {

    private final Long id;
    private final String fachId;
    private final Integer version;
    private final String status;

    private AnforderungBasisDataDto(Long id, String fachId, Integer version, String status) {
        this.id = id;
        this.fachId = fachId;
        this.version = version;
        this.status = status;
    }


    public static List<AnforderungBasisDataDto> buildFromProzessbaukasten(@NotNull List<Object[]> queryResultList) {
        List<AnforderungBasisDataDto> resultList = new ArrayList<>();
        if (queryResultList.isEmpty()) {
            return new ArrayList<>();
        }

        Iterator<Object[]> iterator = queryResultList.iterator();
        while (iterator.hasNext()) {
            Object[] record = iterator.next();

            Long id = (Long) record[0];
            String fachId = (String) record[1];
            Integer version = (Integer) record[2];
            Integer statusId = (Integer) record[3];
            ProzessbaukastenStatus status = ProzessbaukastenStatus.getById(statusId);
            // Kommentar: Statuslabel sollte aus den Internationalisiereungsdaten geladen werden.
            // allerdings ist die Bezeichnung fuer den Status immer deutsch.
            String statusLabel = LocalizationService.getGermanValue(status.getLocalizationKey());

            resultList.add(AnforderungBasisDataDto.newBuilder()
                    .withId(id)
                    .withfachId(fachId)
                    .withVersion(version)
                    .withStatus(statusLabel)
                    .build()
            );
        }

        return resultList;
    }

    public static List<AnforderungBasisDataDto> buildFromQueryResultListWithVersion(@NotNull List<Object[]> queryResultList) {
        List<AnforderungBasisDataDto> resultList = new ArrayList<>();
        if (queryResultList.isEmpty()) {
            return new ArrayList<>();
        }

        Iterator<Object[]> iterator = queryResultList.iterator();
        while (iterator.hasNext()) {
            Object[] record = iterator.next();

            Long id = (Long) record[0];
            String fachId = (String) record[1];
            Integer version = (Integer) record[2];
            Status status = (Status) record[3];

            resultList.add(AnforderungBasisDataDto.newBuilder()
                    .withId(id)
                    .withfachId(fachId)
                    .withVersion(version)
                    .withStatus(status.getStatusBezeichnung())
                    .build()
            );
        }

        return resultList;
    }

    public static List<AnforderungBasisDataDto> buildFromQueryResultListWithoutVersion(@NotNull List<Object[]> queryResultList) {
        List<AnforderungBasisDataDto> resultList = new ArrayList<>();
        if (queryResultList.isEmpty()) {
            return new ArrayList<>();
        }

        Iterator<Object[]> iterator = queryResultList.iterator();
        while (iterator.hasNext()) {
            Object[] record = iterator.next();

            Long id = (Long) record[0];
            String fachId = (String) record[1];
            Status status = (Status) record[2];

            resultList.add(AnforderungBasisDataDto.newBuilder()
                    .withId(id)
                    .withfachId(fachId)
                    .withStatus(status.getStatusBezeichnung())
                    .build()
            );
        }

        return resultList;
    }

    @Override
    public String toString() {
        if (version != null) {
            return fachId + "| V" + version + " " + status;
        } else {
            return fachId + " " + status;
        }
    }

    public Long getId() {
        return id;
    }

    public String getFachId() {
        return fachId;
    }

    public Integer getVersion() {
        return version;
    }

    public String getStatus() {
        return status;
    }

    public static AnforderungBasisDataDtoBuilder newBuilder() {
        return new AnforderungBasisDataDtoBuilder();
    }

    public static class AnforderungBasisDataDtoBuilder {

        Long id;
        String fachId;
        Integer version;
        String status;

        public AnforderungBasisDataDtoBuilder() {
        }

        public AnforderungBasisDataDtoBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public AnforderungBasisDataDtoBuilder withfachId(String fachId) {
            this.fachId = fachId;
            return this;
        }

        public AnforderungBasisDataDtoBuilder withVersion(Integer version) {
            this.version = version;
            return this;
        }

        public AnforderungBasisDataDtoBuilder withStatus(String status) {
            this.status = status;
            return this;
        }

        public AnforderungBasisDataDto build() {
            return new AnforderungBasisDataDto(id, fachId, version, status);
        }
    }

}
