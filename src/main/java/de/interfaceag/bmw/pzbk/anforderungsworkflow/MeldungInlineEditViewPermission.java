package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.enums.Status;

/**
 *
 * @author evda
 */
public final class MeldungInlineEditViewPermission {

    private MeldungInlineEditViewPermission() {

    }

    public static boolean isInlineEdit(Status status, boolean hasWritePermissionForMeldung) {
        if (hasWritePermissionForMeldung) {
            return isStatusEnabledForEdit(status);
        } else {
            return Boolean.FALSE;
        }
    }

    private static boolean isStatusEnabledForEdit(Status status) {
        switch (status) {
            case M_ENTWURF:
            case M_GEMELDET:
            case M_UNSTIMMIG:
                return Boolean.TRUE;
            default:
                return Boolean.FALSE;
        }
    }

}
