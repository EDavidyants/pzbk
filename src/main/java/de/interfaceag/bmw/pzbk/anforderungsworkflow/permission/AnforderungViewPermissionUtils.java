package de.interfaceag.bmw.pzbk.anforderungsworkflow.permission;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.StatusUtils;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.EditViewPermission;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.StatusPermission;
import de.interfaceag.bmw.pzbk.permissions.StatusToRole;
import de.interfaceag.bmw.pzbk.permissions.StatusToRolePermission;
import de.interfaceag.bmw.pzbk.permissions.TruePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author fn
 */
public final class AnforderungViewPermissionUtils {

    private AnforderungViewPermissionUtils() {
    }

    public static EditPermission buildPermissionForNewVersionButton(
            Set<Rolle> userRolesWithWritePermissions, Status currentStatus) {
        Set<StatusToRole> anforderungNewVersionPermissions = new HashSet<>();
        anforderungNewVersionPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_INARBEIT)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAM_VERTRETER).get());
        anforderungNewVersionPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_FTABGESTIMMT)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAM_VERTRETER).get());
        anforderungNewVersionPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_PLAUSIB)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAM_VERTRETER).get());
        anforderungNewVersionPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_FREIGEGEBEN)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAM_VERTRETER).get());
        anforderungNewVersionPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_ANGELEGT_IN_PROZESSBAUKASTEN)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAM_VERTRETER).get());
        anforderungNewVersionPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAM_VERTRETER).get());
        anforderungNewVersionPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_GENEHMIGT_IN_GUELTIGEM_PROZESSBAUKASTEN)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAM_VERTRETER).get());
        anforderungNewVersionPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_KEINE_WEITERVERFOLG)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAM_VERTRETER).get());
        anforderungNewVersionPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_GELOESCHT)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAM_VERTRETER).get());

        return StatusToRolePermission.builder()
                .authorizeWriteForStatusToRoles(anforderungNewVersionPermissions)
                .compareToRolesWithWritePermission(userRolesWithWritePermissions)
                .compareToStatus(currentStatus).get();
    }

    public static EditPermission buildPermissionForDerivatZustandButton(
            Set<Rolle> userRolesWithWritePermissions, Status currentStatus) {
        Set<StatusToRole> anforderungDerivatZustandPermissions = new HashSet<>();
        anforderungDerivatZustandPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_FREIGEGEBEN)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.ANFORDERER).get());

        return StatusToRolePermission.builder()
                .authorizeWriteForStatusToRoles(anforderungDerivatZustandPermissions)
                .compareToRolesWithWritePermission(userRolesWithWritePermissions)
                .compareToStatus(currentStatus).get();
    }

    public static EditPermission buildPermissionForEditButton(Set<Rolle> userRolesWithWritePermissions,
            Status currentStatus) {
        Set<StatusToRole> anforderungMeldungEditPermissions = getAnforderungMeldungEditPermissions();
        return StatusToRolePermission.builder()
                .authorizeWriteForStatusToRoles(anforderungMeldungEditPermissions)
                .compareToRolesWithWritePermission(userRolesWithWritePermissions)
                .compareToStatus(currentStatus).get();
    }

    public static EditPermission buildPermissionForFahrzeugmerkmaleButton(Set<Rolle> userRolesWithWritePermissions,
            Status currentStatus) {

        Set<StatusToRole> anforderungMeldungEditPermissions = getFahrzeugmerkmaleEditPermissions();
        return StatusToRolePermission.builder()
                .authorizeWriteForStatusToRoles(anforderungMeldungEditPermissions)
                .compareToRolesWithWritePermission(userRolesWithWritePermissions)
                .compareToStatus(currentStatus).get();
    }

    private static Set<StatusToRole> getFahrzeugmerkmaleEditPermissions() {
        Set<StatusToRole> result = new HashSet<>();
        result.addAll(getAnforderungMeldungEditPermissions());
        result.add(statusToRoleForNewAnforderung());
        return result;
    }

    private static Set<StatusToRole> getAnforderungMeldungEditPermissions() {
        Set<StatusToRole> result = new HashSet<>();
        result.add(statusToRoleForMeldungEntwurf());

        result.add(statusToRoleForMeldungGemeldet());

        result.add(statusToRoleForMeldungZugeordnet());

        result.add(statusToRoleForMeldungUnstimmig());

        result.add(statusToRoleForMeldungGeloescht());

        result.add(statusToRoleForAnforderungInArbeit());

        result.add(statusToRoleForAnforderungAbgestimmt());

        result.add(statusToRoleForAnforderungPlausibilisiert());

        result.add(statusToRoleForAnforderungFreigegeben());

        result.add(statusToRoleForAnforderungAngelegt());

        result.add(statusToRoleForAnforderungGenehmigtInProzessbaukasten());

        result.add(statusToRoleForAnforderungUnstimmig());

        result.add(statusToRoleForAnforderungKeineWeiterverfolgung());

        result.add(statusToRoleForAnforderungGeloescht());
        return result;
    }

    private static StatusToRole statusToRoleForAnforderungGeloescht() {
        return StatusToRole.builder()
                .createMapFor(Status.A_GELOESCHT).get();
    }

    private static StatusToRole statusToRoleForAnforderungKeineWeiterverfolgung() {
        return StatusToRole.builder()
                .createMapFor(Status.A_KEINE_WEITERVERFOLG).get();
    }

    private static StatusToRole statusToRoleForAnforderungUnstimmig() {
        return StatusToRole.builder()
                .createMapFor(Status.A_UNSTIMMIG)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER).get();
    }

    private static StatusToRole statusToRoleForAnforderungGenehmigtInProzessbaukasten() {
        return StatusToRole.builder()
                .createMapFor(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN)
                .addRole(Rolle.ADMIN).get();
    }

    private static StatusToRole statusToRoleForAnforderungAngelegt() {
        return StatusToRole.builder()
                .createMapFor(Status.A_ANGELEGT_IN_PROZESSBAUKASTEN)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAMMITGLIED)
                .addRole(Rolle.TTEAM_VERTRETER).get();
    }

    private static StatusToRole statusToRoleForAnforderungFreigegeben() {
        return StatusToRole.builder()
                .createMapFor(Status.A_FREIGEGEBEN)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAMMITGLIED)
                .addRole(Rolle.TTEAM_VERTRETER).get();
    }

    private static StatusToRole statusToRoleForAnforderungPlausibilisiert() {
        return StatusToRole.builder()
                .createMapFor(Status.A_PLAUSIB)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAMMITGLIED)
                .addRole(Rolle.TTEAM_VERTRETER).get();
    }

    private static StatusToRole statusToRoleForAnforderungAbgestimmt() {
        return StatusToRole.builder()
                .createMapFor(Status.A_FTABGESTIMMT)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAMMITGLIED)
                .addRole(Rolle.TTEAM_VERTRETER).get();
    }

    private static StatusToRole statusToRoleForAnforderungInArbeit() {
        return StatusToRole.builder()
                .createMapFor(Status.A_INARBEIT)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER).get();
    }

    private static StatusToRole statusToRoleForMeldungGeloescht() {
        return StatusToRole.builder()
                .createMapFor(Status.M_GELOESCHT).get();
    }

    private static StatusToRole statusToRoleForMeldungUnstimmig() {
        return StatusToRole.builder()
                .createMapFor(Status.M_UNSTIMMIG)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSOR)
                .addRole(Rolle.SENSOR_EINGESCHRAENKT)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER).get();
    }

    private static StatusToRole statusToRoleForMeldungZugeordnet() {
        return StatusToRole.builder()
                .createMapFor(Status.M_ZUGEORDNET).get();
    }

    private static StatusToRole statusToRoleForMeldungGemeldet() {
        return StatusToRole.builder()
                .createMapFor(Status.M_GEMELDET)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER).get();
    }

    private static StatusToRole statusToRoleForNewAnforderung() {
        return StatusToRole.builder()
                .createMapFor(Status.A_INARBEIT)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAM_VERTRETER)
                .addRole(Rolle.TTEAMMITGLIED).get();
    }

    private static StatusToRole statusToRoleForMeldungEntwurf() {
        return StatusToRole.builder()
                .createMapFor(Status.M_ENTWURF)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSOR)
                .addRole(Rolle.SENSOR_EINGESCHRAENKT)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER).get();
    }

    public static ViewPermission buildPermissionForDerivatPanel(Status currentStatus) {
        return StatusPermission.builder()
                .authorizeRead(Status.A_FREIGEGEBEN)
                .authorizeRead(Status.A_KEINE_WEITERVERFOLG)
                .authorizeRead(Status.A_GELOESCHT)
                .authorizeRead(Status.A_GENEHMIGT_IN_GUELTIGEM_PROZESSBAUKASTEN)
                .compareTo(currentStatus).get();
    }

    public static EditPermission buildPermissionForMeldungZuordnenButton(
            Set<Rolle> userRolesWithWritePermissions, Status currentStatus) {
        Set<StatusToRole> anforderungMeldungZuordnenPermissions = new HashSet<>();
        anforderungMeldungZuordnenPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_INARBEIT)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER).get());
        anforderungMeldungZuordnenPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_FTABGESTIMMT)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER).get());
        anforderungMeldungZuordnenPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_PLAUSIB)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER).get());
        anforderungMeldungZuordnenPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_FREIGEGEBEN)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER).get());
        anforderungMeldungZuordnenPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_UNSTIMMIG)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER).get());

        return StatusToRolePermission.builder()
                .authorizeWriteForStatusToRoles(anforderungMeldungZuordnenPermissions)
                .compareToRolesWithWritePermission(userRolesWithWritePermissions)
                .compareToStatus(currentStatus).get();
    }

    public static EditPermission buildPermissionForLoeschenButton(Status currentStatus, Set<Rolle> rolesWithWritePermissions) {
        return StatusUtils.getAllNextStatusAsStream(currentStatus).anyMatch(s -> s.equals(Status.M_GELOESCHT)
                || s.equals(Status.A_GELOESCHT)) && isAnyRoleAuthorizedForCurrentStatus(currentStatus, rolesWithWritePermissions)
                ? new TruePermission() : new FalsePermission();
    }

    private static boolean isAnyRoleAuthorizedForCurrentStatus(Status currentStatus, Set<Rolle> roles) {
        return roles.stream().anyMatch(r -> isRoleAuthorizedForCurrentStatus(currentStatus, r));
    }

    public static EditPermission buildPermissionForWiederherstellenButton(
            Status currentStatus, Set<Rolle> rolesWithWritePermissions) {

        Set<StatusToRole> anforderungDerivatZuordnenPermissions = new HashSet<>();

        anforderungDerivatZuordnenPermissions.add(StatusToRole.builder()
                .createMapFor(Status.M_GELOESCHT)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAM_VERTRETER).get());
        anforderungDerivatZuordnenPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_GELOESCHT)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.SENSORCOCLEITER)
                .addRole(Rolle.SCL_VERTRETER)
                .addRole(Rolle.T_TEAMLEITER)
                .addRole(Rolle.TTEAM_VERTRETER).get());

        return StatusToRolePermission.builder()
                .authorizeWriteForStatusToRoles(anforderungDerivatZuordnenPermissions)
                .compareToRolesWithWritePermission(rolesWithWritePermissions)
                .compareToStatus(currentStatus).get();
    }

    public static EditPermission buildPermissionForDerivatZuordnenButton(
            Set<Rolle> userRolesWithWritePermissions, Status currentStatus) {
        Set<StatusToRole> anforderungDerivatZuordnenPermissions = new HashSet<>();
        anforderungDerivatZuordnenPermissions.add(StatusToRole.builder()
                .createMapFor(Status.A_FREIGEGEBEN)
                .addRole(Rolle.ADMIN)
                .addRole(Rolle.ANFORDERER).get());

        return StatusToRolePermission.builder()
                .authorizeWriteForStatusToRoles(anforderungDerivatZuordnenPermissions)
                .compareToRolesWithWritePermission(userRolesWithWritePermissions)
                .compareToStatus(currentStatus).get();
    }

    public static EditPermission buildPermissionsForStatusTransitions(Status currentStatus, Set<Rolle> rolesWithWritePermissions) {
        return isAnyRoleAuthorizedForCurrentStatus(currentStatus, rolesWithWritePermissions) ? new TruePermission() : new FalsePermission();
    }

    public static EditViewPermission buildPermissionForProzessbaukastenButton(Set<Rolle> userRoles) {
        return RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.SENSOR)
                .authorizeReadForRole(Rolle.SENSOR_EINGESCHRAENKT)
                .authorizeReadForRole(Rolle.UMSETZUNGSBESTAETIGER)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.E_COC)
                .authorizeWriteForRole(Rolle.TTEAMMITGLIED)
                .authorizeWriteForRole(Rolle.ADMIN)
                .authorizeWriteForRole(Rolle.TTEAMMITGLIED)
                .authorizeWriteForRole(Rolle.T_TEAMLEITER)
                .authorizeWriteForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(userRoles).get();
    }

    private static boolean isRoleAuthorizedForCurrentStatus(Status currentStatus, Rolle role) {
        switch (role) {
            case ADMIN:
                return Boolean.TRUE;
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
                return Arrays.asList(
                        Status.M_ENTWURF,
                        Status.M_UNSTIMMIG
                ).contains(currentStatus);
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                return Arrays.asList(
                        Status.M_ENTWURF,
                        Status.M_GEMELDET,
                        Status.M_UNSTIMMIG,
                        Status.M_ZUGEORDNET,
                        Status.A_INARBEIT,
                        Status.A_UNSTIMMIG
                ).contains(currentStatus);
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
                return Arrays.asList(
                        Status.A_FTABGESTIMMT,
                        Status.A_FREIGEGEBEN,
                        Status.A_PLAUSIB,
                        Status.A_ANGELEGT_IN_PROZESSBAUKASTEN,
                        Status.A_KEINE_WEITERVERFOLG
                ).contains(currentStatus);
            case E_COC:
            case ANFORDERER:
            case UMSETZUNGSBESTAETIGER:
            case LESER:
            default:
                return Boolean.FALSE;

        }
    }

    public static EditPermission buildPermissionForUmsetzerPanel(Set<Rolle> userRolesWithWritePermissions,
            Status currentStatus) {

        Set<StatusToRole> anforderungMeldungEditPermissions = getUmsetzerPanelEditPermissions();
        return StatusToRolePermission.builder()
                .authorizeWriteForStatusToRoles(anforderungMeldungEditPermissions)
                .compareToRolesWithWritePermission(userRolesWithWritePermissions)
                .compareToStatus(currentStatus).get();
    }

    private static Set<StatusToRole> getUmsetzerPanelEditPermissions() {
        Set<StatusToRole> result = new HashSet<>();
        result.addAll(getAnforderungUmsetzerPanelEditPermissions());
        return result;
    }

    private static Set<StatusToRole> getAnforderungUmsetzerPanelEditPermissions() {
        Set<StatusToRole> result = new HashSet<>();
        result.add(statusToRoleForNewAnforderung());
        result.add(statusToRoleForAnforderungAbgestimmt());
        result.add(statusToRoleForAnforderungPlausibilisiert());
        result.add(statusToRoleForAnforderungFreigegeben());
        result.add(statusToRoleForAnforderungAngelegt());
        result.add(statusToRoleForAnforderungGenehmigtInProzessbaukasten());
        result.add(statusToRoleForAnforderungUnstimmig());
        result.add(statusToRoleForAnforderungKeineWeiterverfolgung());
        result.add(statusToRoleForAnforderungGeloescht());
        return result;
    }

}
