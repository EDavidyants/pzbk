package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;

import java.util.Collection;

/**
 *
 * @author evda
 */
public interface AnforderungMeldungZuordnungAdapter {

    Anforderung addMeldungenToAnforderung(Anforderung anforderung, Collection<Meldung> meldungen, Mitarbeiter user);

    Anforderung removeMeldungenFromAnforderung(Anforderung anforderung, Collection<Meldung> meldungen, Mitarbeiter user);

}
