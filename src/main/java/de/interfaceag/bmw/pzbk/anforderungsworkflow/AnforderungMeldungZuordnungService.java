package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.meldung.MeldungReportingStatusTransitionAdapter;
import de.interfaceag.bmw.pzbk.services.AnforderungService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author evda
 */
@Stateless
@Named
public class AnforderungMeldungZuordnungService implements AnforderungMeldungZuordnungAdapter, Serializable {

    @Inject
    private AnforderungService anforderungService;
    @Inject
    private AnforderungMeldungHistoryService historyService;
    @Inject
    private MeldungReportingStatusTransitionAdapter meldungReportingStatusTransitionAdapter;

    @Override
    public Anforderung addMeldungenToAnforderung(Anforderung anforderung, Collection<Meldung> meldungen, Mitarbeiter user) {
        List<Anhang> anhaengeToSave = new ArrayList<>();

        Iterator<Meldung> iterator = meldungen.iterator();
        while (iterator.hasNext()) {
            Meldung meldung = iterator.next();
            if (isErsteZuordnungForMeldung(meldung)) {
                writeAnforderungMeldungZuordnung(meldung, anforderung, user, anhaengeToSave);
                meldungReportingStatusTransitionAdapter.addMeldungToExistingAnforderung(meldung);

            } else if (!isMeldungZuAnforderungZugeordnet(anforderung, meldung)) {
                writeAnforderungMeldungZuordnung(meldung, anforderung, user, anhaengeToSave);
            }
        }

        anhaengeToSave.forEach(anforderung::addAnhang);
        return updateAnforderung(anforderung);
    }

    private Anforderung updateAnforderung(Anforderung anforderung) {
        anforderungService.saveAnforderung(anforderung);
        return anforderungService.getAnforderungById(anforderung.getId());
    }

    private void writeAnforderungMeldungZuordnung(Meldung meldung, Anforderung anforderung, Mitarbeiter user, List<Anhang> anhaengeToSave) {
        String oldStatusValue = meldung.getStatus().getStatusBezeichnung();
        String oldKommentar = meldung.getStatusWechselKommentar();

        String newStatusValue = Status.M_ZUGEORDNET.getStatusBezeichnung();
        String newKommentar = buildStatusChangeKommentarBeimZuordnen(anforderung);
        updateMeldungFieldsBeimZuordnen(anforderung, meldung, newKommentar);

        historyService.writeHistoryForChangedKeyValue(user, meldung, newStatusValue, oldStatusValue, "status");
        historyService.writeHistoryForChangedKeyValue(user, meldung, newKommentar, oldKommentar, "statusWechselKommentar");
        String historyKommentar = "Meldung " + meldung.getFachId() + " wurde zugeordnet";
        historyService.writeHistoryForChangedKeyValue(user, anforderung, historyKommentar, "", "meldungZuordnung");
        anforderungService.saveMeldung(meldung);

        buildNewAnhaengeListForAnforderung(meldung, anhaengeToSave);
    }

    private void buildNewAnhaengeListForAnforderung(Meldung meldung, List<Anhang> anhaengeToSave) {
        meldung.getAnhaenge().stream()
                .map(Anhang::getCopy)
                .forEachOrdered(anhaengeToSave::add);
    }

    private void updateMeldungFieldsBeimZuordnen(Anforderung anforderung, Meldung meldung, String newKommentar) {
        anforderung.addMeldung(meldung);
        meldung.setStatus(Status.M_ZUGEORDNET);
        meldung.setAssignedToAnforderung(true);
        meldung.setStatusWechselKommentar(newKommentar);
        meldung.setAenderungsdatum(new Date());
        meldung.setZeitpunktStatusaenderung(new Date());
    }

    private String buildStatusChangeKommentarBeimZuordnen(Anforderung anforderung) {
        StringBuilder sb = new StringBuilder();
        sb.append("wurde der bestehenden Anforderung ");
        sb.append(anforderung.getFachId());
        sb.append(" |V").append(anforderung.getVersion());
        sb.append(" zugeordnet");
        return sb.toString();
    }

    private static boolean isErsteZuordnungForMeldung(Meldung meldung) {
        return meldung.getAnforderung() == null || meldung.getAnforderung().isEmpty();
    }

    private static boolean isMeldungZuAnforderungZugeordnet(Anforderung anforderung, Meldung meldung) {
        return anforderung.getMeldungen().contains(meldung);
    }

    @Override
    public Anforderung removeMeldungenFromAnforderung(Anforderung anforderung, Collection<Meldung> meldungen, Mitarbeiter user) {
        Set<Meldung> updatedMeldungen = anforderung.getMeldungen();

        Iterator<Meldung> iterator = meldungen.iterator();
        while (iterator.hasNext()) {
            Meldung meldung = iterator.next();
            if (isMeldungZuAnforderungZugeordnet(anforderung, meldung)) {
                removeMeldungZuordnung(meldung, anforderung, user, updatedMeldungen);
            }
        }

        anforderung.setMeldungen(updatedMeldungen);
        return updateAnforderung(anforderung);
    }

    private void removeMeldungZuordnung(Meldung meldung, Anforderung anforderung, Mitarbeiter user, Set<Meldung> updatedMeldungen) {
        updatedMeldungen.remove(meldung);

        if (isLastZuordnungForMeldung(meldung)) {
            removeLastMeldungZuordnung(meldung, anforderung, user);
        }

        String historyKommentar = "Zuordnung der Meldung " + meldung.getFachId() + " wurde aufgehoben";
        historyService.writeHistoryForChangedKeyValue(user, anforderung, historyKommentar, "", "meldungZuordnung");
    }

    private static boolean isLastZuordnungForMeldung(Meldung meldung) {
        return meldung.getAnforderung() != null && meldung.getAnforderung().size() == 1;
    }

    private void removeLastMeldungZuordnung(Meldung meldung, Anforderung anforderung, Mitarbeiter user) {
        String oldStatusValue = meldung.getStatus().getStatusBezeichnung();
        String oldKommentar = meldung.getStatusWechselKommentar();

        String newStatusValue = Status.M_GEMELDET.getStatusBezeichnung();
        String newKommentar = buildStatusChangeKommentarBeimZuordnenAufheben(anforderung);
        updateMeldungFieldsBeimZuordnungAufheben(meldung, newKommentar);

        meldungReportingStatusTransitionAdapter.removeMeldungFromAnforderung(meldung, anforderung);
        historyService.writeHistoryForChangedKeyValue(user, meldung, newStatusValue, oldStatusValue, "status");
        historyService.writeHistoryForChangedKeyValue(user, meldung, newKommentar, oldKommentar, "statusWechselKommentar");
        meldung.setAnforderung(new HashSet<>());
        anforderungService.saveMeldung(meldung);
    }

    private void updateMeldungFieldsBeimZuordnungAufheben(Meldung meldung, String newKommentar) {
        meldung.setStatus(Status.M_GEMELDET);
        meldung.setAssignedToAnforderung(false);
        meldung.setStatusWechselKommentar(newKommentar);
        meldung.setAenderungsdatum(new Date());
        meldung.setZeitpunktStatusaenderung(new Date());
    }

    private String buildStatusChangeKommentarBeimZuordnenAufheben(Anforderung anforderung) {
        StringBuilder sb = new StringBuilder();
        sb.append("Die Zuordnung der Anforderung ");
        sb.append(anforderung.getFachId());
        sb.append(" |V").append(anforderung.getVersion());
        sb.append(" wurde aufgehoben");
        return sb.toString();
    }

}
