package de.interfaceag.bmw.pzbk.anforderungsworkflow.dto;

import java.util.Date;

/**
 *
 * @author fn
 */
public class AnforderungVersionMenuViewData {

    private final String fachId;
    private final Integer version;
    private final Date erstellungsdatum;

    public AnforderungVersionMenuViewData(String fachId, Integer version, Date erstellungsdatum) {
        this.fachId = fachId;
        this.version = version;
        this.erstellungsdatum = erstellungsdatum;
    }

    public String getFachId() {
        return fachId;
    }

    public Integer getVersion() {
        return version;
    }

    public Date getErstellungsdatum() {
        return erstellungsdatum;
    }

}
