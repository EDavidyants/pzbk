package de.interfaceag.bmw.pzbk.anforderungsworkflow.dialogs;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.session.Session;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static de.interfaceag.bmw.pzbk.anforderungsworkflow.dialogs.MeldungZuordnenDialogMeldungDto.getDtosForMeldungen;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class MeldungZuordnenDialogFacade implements Serializable {

    @Inject
    private Session session;

    @Inject
    private AnforderungService anforderungService;

    @Inject
    private BerechtigungService berechtigungService;

    public void addMeldungenToAnforderung(Long anforderungId, Collection<MeldungZuordnenDialogMeldungDto> meldungenChosen) {
        Anforderung anforderungById = anforderungService.getAnforderungById(anforderungId);
        List<Meldung> meldungenByIdList = anforderungService.getMeldungenByIdList(meldungenChosen.stream().map(MeldungZuordnenDialogMeldungDto::getId).collect(Collectors.toList()));
        anforderungService.addMeldungenToAnforderung(anforderungById, meldungenByIdList, session.getUser());
    }

    public void removeMeldungenFromAnforderung(Long anforderungId, Collection<MeldungZuordnenDialogMeldungDto> meldungenToRemove) {
        Anforderung anforderungById = anforderungService.getAnforderungById(anforderungId);
        List<Meldung> meldungenByIdList = anforderungService.getMeldungenByIdList(meldungenToRemove.stream().map(MeldungZuordnenDialogMeldungDto::getId).collect(Collectors.toList()));
        anforderungService.removeMeldungenFromAnforderung(anforderungById, meldungenByIdList, session.getUser());
    }

    public Collection<MeldungZuordnenDialogMeldungDto> getSelectableMeldungenForDialog(Long anforderungId) {

        List<SensorCoc> sensorCocs = berechtigungService.getAuthorizedSensorCocObjectListForMitarbeiter(session.getUser());
        Anforderung anforderung = anforderungService.getAnforderungById(anforderungId);

        if (anforderung == null || anforderung.getMeldungen() == null
                || anforderung.getMeldungen().isEmpty() || sensorCocs == null || sensorCocs.isEmpty()) {
            return Collections.emptyList();
        }

        List<Long> currentMeldungIds = anforderung.getMeldungen().stream().map(Meldung::getId).collect(Collectors.toList());

        List<Status> statusList = Arrays.asList(Status.M_GEMELDET, Status.M_ZUGEORDNET);

        if (currentMeldungIds.isEmpty()) {
            return getDtosForMeldungen(anforderungService.getAllMeldungenBySensorCocAndStatus(sensorCocs, statusList));
        } else {
            return getDtosForMeldungen(anforderungService.getMeldungenBySensorCocAndStatusNotInIdList(sensorCocs, statusList, currentMeldungIds));
        }
    }

    public Collection<MeldungZuordnenDialogMeldungDto> getSelectedMeldungenForDialog(Long anforderungId) {

        Anforderung anforderung = anforderungService.getAnforderungById(anforderungId);

        if (anforderung == null || anforderung.getMeldungen() == null || anforderung.getMeldungen().isEmpty()) {
            return Collections.emptyList();
        }

        return (getDtosForMeldungen(anforderung.getMeldungen()));

    }

}
