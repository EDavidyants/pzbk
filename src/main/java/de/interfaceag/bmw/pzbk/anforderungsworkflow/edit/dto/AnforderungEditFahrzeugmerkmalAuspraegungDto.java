package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.StringJoiner;

public class AnforderungEditFahrzeugmerkmalAuspraegungDto implements AnforderungEditFahrzeugmerkmalAuspraegung {

    private final FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId;
    private final String auspraegung;

    public AnforderungEditFahrzeugmerkmalAuspraegungDto(FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId, String auspraegung) {
        this.fahrzeugmerkmalAuspraegungId = fahrzeugmerkmalAuspraegungId;
        this.auspraegung = auspraegung;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", AnforderungEditFahrzeugmerkmalAuspraegungDto.class.getSimpleName() + "[", "]")
                .add("fahrzeugmerkmalAuspraegungId=" + fahrzeugmerkmalAuspraegungId)
                .add("auspraegung='" + auspraegung + "'")
                .toString();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        AnforderungEditFahrzeugmerkmalAuspraegungDto that = (AnforderungEditFahrzeugmerkmalAuspraegungDto) object;

        return new EqualsBuilder()
                .append(fahrzeugmerkmalAuspraegungId, that.fahrzeugmerkmalAuspraegungId)
                .append(auspraegung, that.auspraegung)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(fahrzeugmerkmalAuspraegungId)
                .append(auspraegung)
                .toHashCode();
    }

    @Override
    public Long getId() {
        return fahrzeugmerkmalAuspraegungId.getId();
    }

    @Override
    public FahrzeugmerkmalAuspraegungId getFahrzeugmerkmalAuspraegungId() {
        return fahrzeugmerkmalAuspraegungId;
    }

    @Override
    public String getAuspraegung() {
        return auspraegung;
    }
}
