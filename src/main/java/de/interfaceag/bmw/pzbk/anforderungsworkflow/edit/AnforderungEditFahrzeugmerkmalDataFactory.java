package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalData;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import java.io.Serializable;

@Stateless
public class AnforderungEditFahrzeugmerkmalDataFactory implements Serializable {

    @Produces
    public AnforderungEditFahrzeugmerkmalData getNewAnforderungEditFahrzeugmerkmalData() {
        return new AnforderungEditFahrzeugmerkmalData();
    }

}
