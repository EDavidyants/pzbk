package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalDialogData;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class AnforderungEditFahrzeugmerkmalData implements Serializable {

    private ModulSeTeamId activeModulSeTeamId;

    private AnforderungEditFahrzeugmerkmalDialogData activeFahrzeugmerkmalDialogData;

    private Map<ModulSeTeamId, AnforderungEditFahrzeugmerkmalDialogData> modulSeTeamFahrzeugmerkmalDialogDataMap = new HashMap<>();

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        AnforderungEditFahrzeugmerkmalData that = (AnforderungEditFahrzeugmerkmalData) object;

        return new EqualsBuilder()
                .append(activeModulSeTeamId, that.activeModulSeTeamId)
                .append(activeFahrzeugmerkmalDialogData, that.activeFahrzeugmerkmalDialogData)
                .append(modulSeTeamFahrzeugmerkmalDialogDataMap, that.modulSeTeamFahrzeugmerkmalDialogDataMap)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(activeModulSeTeamId)
                .append(activeFahrzeugmerkmalDialogData)
                .append(modulSeTeamFahrzeugmerkmalDialogDataMap)
                .toHashCode();
    }

    public AnforderungEditFahrzeugmerkmalDialogData getActiveFahrzeugmerkmalDialogData() {
        return activeFahrzeugmerkmalDialogData;
    }

    public void addDataForModulSeTeam(ModulSeTeamId modulSeTeamId, AnforderungEditFahrzeugmerkmalDialogData fahrzeugmerkmalDialogData) {
        this.activeFahrzeugmerkmalDialogData = fahrzeugmerkmalDialogData;
        this.activeModulSeTeamId = modulSeTeamId;
        modulSeTeamFahrzeugmerkmalDialogDataMap.put(modulSeTeamId, fahrzeugmerkmalDialogData);
    }

    public ModulSeTeamId getActiveModulSeTeamId() {
        return activeModulSeTeamId;
    }

    public Map<ModulSeTeamId, AnforderungEditFahrzeugmerkmalDialogData> getModulSeTeamFahrzeugmerkmalDialogDataMap() {
        return modulSeTeamFahrzeugmerkmalDialogDataMap;
    }

    /**
     * Update active AnforderungEditFahrzeugmerkmalDialogData for modulSeTeamId
     * @param modulSeTeamId selected ModulSeTeam Id
     * @return if a dataset exists in the in memory map
     */
    public boolean updateActiveData(ModulSeTeamId modulSeTeamId) {
        final boolean containsKey = modulSeTeamFahrzeugmerkmalDialogDataMap.containsKey(modulSeTeamId);
        if (containsKey) {
            this.activeFahrzeugmerkmalDialogData = this.getAnforderungEditFahrzeugmerkmalDialogDataForModulSeTeam(modulSeTeamId);
            this.activeModulSeTeamId = modulSeTeamId;
        } else {
            this.activeFahrzeugmerkmalDialogData = null;
            this.activeModulSeTeamId = null;
        }
        return containsKey;
    }

    public AnforderungEditFahrzeugmerkmalDialogData getAnforderungEditFahrzeugmerkmalDialogDataForModulSeTeam(ModulSeTeamId modulSeTeamId) {
        return modulSeTeamFahrzeugmerkmalDialogDataMap.get(modulSeTeamId);
    }
}
