package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class AnforderungViewFacade implements Serializable {

    @Inject
    private AnforderungService anforderungService;

    public boolean anforderungHasMeldung(UrlParameter urlParameter) {

        Optional<String> idAsString = urlParameter.getValue("id");
        Optional<String> fachIdAsString = urlParameter.getValue("fachId");
        Optional<String> versionAsString = urlParameter.getValue("version");
        Optional<String> newAnforderungOhneMeldung = urlParameter.getValue("new");

        if (fachIdAndVersionParameterArePresent(fachIdAsString, versionAsString)) {
            Optional<Anforderung> anforderung = Optional.ofNullable(anforderungService.getAnforderungByFachIdVersion(fachIdAsString.get(), Integer.parseInt(versionAsString.get())));
            return anforderungHasMeldung(anforderung);
        }

        if (idParameterIsPresent(idAsString)) {
            Optional<Anforderung> anforderung = Optional.ofNullable(anforderungService.getAnforderungById(Long.parseLong(idAsString.get())));
            return anforderungHasMeldung(anforderung);
        }

        return !isNewAnforderungOhneMeldungPresent(newAnforderungOhneMeldung);
    }

    private static boolean idParameterIsPresent(Optional<String> idAsString) {
        return idAsString.isPresent() && RegexUtils.matchesId(idAsString.get());
    }

    private static boolean fachIdAndVersionParameterArePresent(Optional<String> fachIdAsString, Optional<String> versionAsString) {
        return fachIdAsString.isPresent() && RegexUtils.matchesAnforderungFachId(fachIdAsString.get())
                && versionAsString.isPresent() && RegexUtils.matchesId(versionAsString.get());
    }

    private static boolean anforderungHasMeldung(Optional<Anforderung> anforderung) {
        if (anforderung.isPresent()) {
            return !anforderung.get().getMeldungen().isEmpty();
        } else {
            return false;
        }
    }

    private static boolean isNewAnforderungOhneMeldungPresent(Optional<String> newAnforderungOhneMeldung) {
        return newAnforderungOhneMeldung.isPresent();
    }
}
