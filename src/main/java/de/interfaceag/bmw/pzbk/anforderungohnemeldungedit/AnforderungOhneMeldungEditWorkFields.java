package de.interfaceag.bmw.pzbk.anforderungohnemeldungedit;

import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.Tteam;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author fn
 */
public class AnforderungOhneMeldungEditWorkFields implements Serializable {

    private boolean newVersion;
    private boolean newAnforderung;

    private boolean changedInStatusPlausibilisiert;
    private final String beschreibungAnforderungDe;
    private String statusChangeKommentar;
    private String versionKommentar;

    private final List<FestgestelltIn> festgestelltIn;
    private final List<Tteam> allTteams;
    private final List<AnforderungFreigabeBeiModulSeTeam> originalFreigabeBeiModulSeTeam;

    private Prozessbaukasten prozessbaukasten;

    private final Map<ModulSeTeam, Boolean> modulBerechtigungMap;

    public AnforderungOhneMeldungEditWorkFields(List<FestgestelltIn> festgestelltIn,
            List<Tteam> allTteams, String beschreibungAnforderungDe,
            List<AnforderungFreigabeBeiModulSeTeam> originalFreigabeBeiModulSeTeam,
            Map<ModulSeTeam, Boolean> modulBerechtigungMap) {
        this.festgestelltIn = festgestelltIn;
        this.allTteams = allTteams;
        this.beschreibungAnforderungDe = beschreibungAnforderungDe;
        this.originalFreigabeBeiModulSeTeam = originalFreigabeBeiModulSeTeam;
        this.modulBerechtigungMap = modulBerechtigungMap;
    }

    public boolean isChangedInStatusPlausibilisiert() {
        return changedInStatusPlausibilisiert;
    }

    public void setChangedInStatusPlausibilisiert(boolean changedInPlausi) {
        this.changedInStatusPlausibilisiert = changedInPlausi;
    }

    public String getBeschreibungAnforderungDe() {
        return beschreibungAnforderungDe;
    }

    public List<AnforderungFreigabeBeiModulSeTeam> getOriginalFreigabeBeiModulSeTeam() {
        return originalFreigabeBeiModulSeTeam;
    }

    public boolean isNewVersion() {
        return newVersion;
    }

    public void setNewVersion(boolean newVersion) {
        this.newVersion = newVersion;
    }

    public boolean isNewAnforderung() {
        return newAnforderung;
    }

    public void setNewAnforderung(boolean newAnforderung) {
        this.newAnforderung = newAnforderung;
    }

    public String getStatusChangeKommentar() {
        return statusChangeKommentar;
    }

    public void setStatusChangeKommentar(String statusChangeKommentar) {
        this.statusChangeKommentar = statusChangeKommentar;
    }

    public List<FestgestelltIn> getFestgestelltIn() {
        return festgestelltIn;
    }

    public List<Tteam> getAllTteams() {
        return allTteams;
    }

    public String getVersionKommentar() {
        return versionKommentar;
    }

    public void setVersionKommentar(String versionKommentar) {
        this.versionKommentar = versionKommentar;
    }

    public boolean isMyModulSeTeam(ModulSeTeam modulSeTeam) {
        Boolean isBerechtigtForSeTeam = modulBerechtigungMap.get(modulSeTeam);
        return isBerechtigtForSeTeam != null && isBerechtigtForSeTeam;
    }

    public Optional<Prozessbaukasten> getProzessbaukasten() {
        return Optional.ofNullable(prozessbaukasten);
    }

    public void setProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        if (prozessbaukasten != null) {
            this.prozessbaukasten = prozessbaukasten;
        }
    }

}
