package de.interfaceag.bmw.pzbk.anforderungohnemeldungedit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungFreigabeDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.DialogUmsetzerDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungWriteData;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Bild;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ReferenzSystemLink;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.entities.Zielwert;
import de.interfaceag.bmw.pzbk.enums.ReferenzSystem;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author fn
 */
public class AnforderungOhneMeldungEditViewData implements AnforderungWriteData, Serializable {

    private final Anforderung anforderungCopy;

    private final AnforderungId anforderungId;
    private final String fachId;
    private final Integer version;
    private Status status;
    private String ersteller;
    private Set<FestgestelltIn> festgestelltIn;
    private String beschreibungAnforderungDe;
    private String beschreibungAnforderungEn;
    private String kommentarAnforderung;
    private Zielwert zielwert;
    private Set<DialogUmsetzerDto> umsetzer;
    private List<AnforderungFreigabeDto> freigaben;
    private List<AnforderungFreigabeBeiModulSeTeam> anfoFreigabeBeiModul;
    private SensorCoc sensorCoc;
    private Tteam tteam;
    private boolean phasenbezug;
    private boolean potentialStandardisierung;
    private List<Anhang> anhaenge;
    private Anhang anhangForBild;
    private Anhang selectedAnhang;

    private final Bild bild;

    private final List<ReferenzSystemLink> referenzSystemLinks;

    private String newReferenzSystemId;
    private ReferenzSystem newReferenzSystem;

    private VereinbarungType defaultVereinbarungType;

    public AnforderungOhneMeldungEditViewData(AnforderungId anforderungId, String fachId,
            Integer version, Status status, String ersteller,
            Set<FestgestelltIn> festgestelltIn,
            String beschreibungAnforderungDe, String beschreibungAnforderungEn,
            String kommentarAnforderung, Zielwert zielwert,
            Set<DialogUmsetzerDto> umsetzer, List<AnforderungFreigabeBeiModulSeTeam> anfoFreigabeBeiModul,
            SensorCoc sensorCoc, Tteam tteam,
            boolean phasenbezug, boolean potentialStandardisierung,
            List<Anhang> anhaenge, Anhang anhangForBild,
            Bild bild, List<ReferenzSystemLink> referenzSystemLinks,
            List<AnforderungFreigabeDto> freigaben, Anforderung anforderungCopy) {

        this.anforderungId = anforderungId;
        this.fachId = fachId;
        this.version = version;
        this.status = status;
        this.ersteller = ersteller;
        this.festgestelltIn = festgestelltIn;
        this.beschreibungAnforderungDe = beschreibungAnforderungDe;
        this.beschreibungAnforderungEn = beschreibungAnforderungEn;
        this.kommentarAnforderung = kommentarAnforderung;
        this.zielwert = zielwert;
        this.umsetzer = umsetzer;
        this.anfoFreigabeBeiModul = anfoFreigabeBeiModul;
        this.sensorCoc = sensorCoc;
        this.tteam = tteam;
        this.phasenbezug = phasenbezug;
        this.potentialStandardisierung = potentialStandardisierung;
        this.anhaenge = anhaenge;
        this.anhangForBild = anhangForBild;
        this.bild = bild;
        this.referenzSystemLinks = referenzSystemLinks;
        this.freigaben = freigaben;
        this.anforderungCopy = anforderungCopy;
        this.defaultVereinbarungType = VereinbarungType.ZAK;
    }

    public static AnforderungOhneMeldungEditViewData forAnforderung(Anforderung anforderung, Collection<ModulSeTeamId> configuredModulSeTeamsForAnforderung) {
        if (anforderung == null) {
            return forEmtyAnforderung(anforderung);
        }

        final List<AnforderungFreigabeDto> freigabeDtos = buildFreigabeDtos(anforderung.getAnfoFreigabeBeiModul(), configuredModulSeTeamsForAnforderung);

        return new AnforderungOhneMeldungEditViewData(anforderung.getAnforderungId(), anforderung.getFachId(),
                anforderung.getVersion(), anforderung.getStatus(),
                anforderung.getErsteller(), anforderung.getFestgestelltIn(),
                anforderung.getBeschreibungAnforderungDe(), anforderung.getBeschreibungAnforderungEn(),
                anforderung.getKommentarAnforderung(), anforderung.getZielwert(),
                AnforderungOhneMeldungEditViewData.getUmsetzerDtos(anforderung.getUmsetzer(), anforderung.getAnfoFreigabeBeiModul()),
                anforderung.getAnfoFreigabeBeiModul(), anforderung.getSensorCoc(), anforderung.getTteam(), anforderung.isPhasenbezug(), anforderung.isPotentialStandardisierung(),
                anforderung.getAnhaenge(),
                AnforderungOhneMeldungEditViewData.getAnhangForBildFromAnforderung(anforderung.getAnhaenge()),
                anforderung.getBild(), anforderung.getReferenzSystemLinks(), freigabeDtos, anforderung);
    }

    private static AnforderungOhneMeldungEditViewData forEmtyAnforderung(Anforderung anforderung) {
        return new AnforderungOhneMeldungEditViewData(null, "",
                1, Status.A_INARBEIT,
                "", new HashSet<>(),
                "", "",
                "", new Zielwert(),
                new HashSet<>(),
                new ArrayList<>(), null, null, true, true,
                null,
                null,
                null, new ArrayList<>(), new ArrayList<>(), anforderung);
    }

    public static AnforderungOhneMeldungEditViewData forNewAnforderung(Anforderung anforderung, Mitarbeiter user) {
        if (anforderung == null) {
            return forEmtyAnforderung(anforderung);
        }
        return new AnforderungOhneMeldungEditViewData(null, "",
                1, Status.A_INARBEIT,
                user.getName(), new HashSet<>(),
                "", "",
                "", new Zielwert(),
                new HashSet<>(),
                new ArrayList<>(), null, null, anforderung.isPhasenbezug(), true,
                anforderung.getAnhaenge(),
                null,
                null, new ArrayList<>(), new ArrayList<>(), anforderung);
    }

    private static Anhang getAnhangForBildFromAnforderung(List<Anhang> anhangList) {
        Anhang returnAnhang = null;
        for (Anhang a : anhangList) {
            if (a.isStandardBild()) {
                returnAnhang = a;
            }
        }
        return returnAnhang;
    }

    private static List<AnforderungFreigabeDto> buildFreigabeDtos(List<AnforderungFreigabeBeiModulSeTeam> freigabeList, Collection<ModulSeTeamId> configuredModulSeTeamsForAnforderung) {
        final List<AnforderungFreigabeDto> freigabeDtos = freigabeList.stream().map(AnforderungFreigabeBeiModulSeTeam::toDto).collect(Collectors.toList());
        updateFahrzeugmerkmalConfiguredStatus(configuredModulSeTeamsForAnforderung, freigabeDtos);
        return freigabeDtos;
    }

    private static void updateFahrzeugmerkmalConfiguredStatus(Collection<ModulSeTeamId> configuredModulSeTeamsForAnforderung, List<AnforderungFreigabeDto> freigabeDtos) {
        freigabeDtos.stream().filter(freigabeDto -> configuredModulSeTeamsForAnforderung.contains(freigabeDto.getModulSeTeamId())).forEach(AnforderungFreigabeDto::setConfiguredTrue);
    }

    private static Set<DialogUmsetzerDto> getUmsetzerDtos(Set<Umsetzer> umsetzerList, List<AnforderungFreigabeBeiModulSeTeam> freigabeList) {
        return umsetzerList.stream().map(umsetzer -> DialogUmsetzerDto.forUmsetzer(umsetzer, isUmsetzerFreigegeben(freigabeList, umsetzer))).collect(Collectors.toSet());
    }

    private static boolean isUmsetzerFreigegeben(List<AnforderungFreigabeBeiModulSeTeam> freigaben, Umsetzer umsetzer) {
        if (umsetzer != null && umsetzer.getSeTeam() != null && freigaben != null) {
            return freigaben.stream().anyMatch(f -> f.getModulSeTeam().equals(umsetzer.getSeTeam()) && f.isFreigabe());
        }
        return false;
    }

    @Override
    public String getFachId() {
        return fachId;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public List<Anhang> getAnhaenge() {
        return anhaenge;
    }

    @Override
    public void setAnhaenge(List<Anhang> anhaenge) {
        this.anhaenge = anhaenge;
    }

    @Override
    public String getBeschreibungAnforderungDe() {
        return beschreibungAnforderungDe;
    }

    public void setBeschreibungAnforderungDe(String beschreibungAnforderungDe) {
        this.beschreibungAnforderungDe = beschreibungAnforderungDe;
    }

    @Override
    public String getBeschreibungAnforderungEn() {
        return beschreibungAnforderungEn;
    }

    public void setBeschreibungAnforderungEn(String beschreibungAnforderungEn) {
        this.beschreibungAnforderungEn = beschreibungAnforderungEn;
    }

    @Override
    public String getKommentarAnforderung() {
        return kommentarAnforderung;
    }

    public void setKommentarAnforderung(String kommentarAnforderung) {
        this.kommentarAnforderung = kommentarAnforderung;
    }

    @Override
    public Zielwert getZielwert() {
        return zielwert;
    }

    public void setZielwert(Zielwert zielwert) {
        this.zielwert = zielwert;
    }

    public Set<DialogUmsetzerDto> getUmsetzer() {
        return umsetzer;
    }

    public void setUmsetzer(Set<DialogUmsetzerDto> umsetzer) {
        this.umsetzer = umsetzer;
    }

    @Override
    public List<AnforderungFreigabeBeiModulSeTeam> getAnfoFreigabeBeiModul() {
        return anfoFreigabeBeiModul;
    }

    @Override
    public SensorCoc getSensorCoc() {
        return sensorCoc;
    }

    public void setSensorCoc(SensorCoc sensorCoc) {
        this.sensorCoc = sensorCoc;
    }

    @Override
    public Tteam getTteam() {
        return tteam;
    }

    public void setTteam(Tteam tteam) {
        this.tteam = tteam;
    }

    @Override
    public boolean isPhasenbezug() {
        return phasenbezug;
    }

    public void setPhasenbezug(boolean phasenbezug) {
        this.phasenbezug = phasenbezug;
    }

    @Override
    public boolean isPotentialStandardisierung() {
        return potentialStandardisierung;
    }

    public void setPotentialStandardisierung(boolean potentialStandardisierung) {
        this.potentialStandardisierung = potentialStandardisierung;
    }

    @Override
    public Bild getBild() {
        return bild;
    }

    @Override
    public Set<FestgestelltIn> getFestgestelltIn() {
        return festgestelltIn;
    }

    public void setFestgestelltIn(Set<FestgestelltIn> festgestelltIn) {
        this.festgestelltIn = festgestelltIn;
    }

    public void setFestgestelltInList(List<FestgestelltIn> festgestelltInList) {
        if (festgestelltInList == null) {
            setFestgestelltIn(null);
        } else {
            Set<FestgestelltIn> festgestelltInSet = new HashSet<>(festgestelltInList);
            setFestgestelltIn(festgestelltInSet);
        }
    }

    public List<FestgestelltIn> getFestgestelltInList() {
        if (getFestgestelltIn() == null) {
            return Collections.emptyList();
        } else {
            return new ArrayList<>(getFestgestelltIn());
        }
    }

    @Override
    public String getErsteller() {
        return ersteller;
    }

    public void setErsteller(String ersteller) {
        this.ersteller = ersteller;
    }

    @Override
    public List<ReferenzSystemLink> getReferenzSystemLinks() {
        return referenzSystemLinks;
    }

    public Anhang getAnhangForBild() {
        return anhangForBild;
    }

    public void setAnhangForBild(Anhang anhangForBild) {
        this.anhangForBild = anhangForBild;
    }

    public Anhang getSelectedAnhang() {
        return selectedAnhang;
    }

    public void setSelectedAnhang(Anhang selectedAnhang) {
        this.selectedAnhang = selectedAnhang;
    }

    public void removeFreigabe(AnforderungFreigabeDto freigabe) {
        if (!getFreigaben().isEmpty() && getFreigaben().contains(freigabe)) {
            getFreigaben().remove(freigabe);
        }
    }

    public void addAnhang(Anhang anhang) {
        anhaenge.add(anhang);
    }

    public void removeAnhang(Anhang anhang) {
        if (anhaenge != null) {
            anhaenge.remove(anhang);
        }
    }

    public void removeVorschaubild() {
        this.anhangForBild = null;
    }

    public List<AnforderungFreigabeDto> getFreigaben() {
        return freigaben;
    }

    public void setFreigaben(List<AnforderungFreigabeDto> freigaben) {
        this.freigaben = freigaben;
    }

    public Anforderung getAnforderungCopy() {
        return anforderungCopy;
    }

    public List<ReferenzSystem> getReferenzSysteme() {
        return Arrays.asList(ReferenzSystem.values());
    }

    public void addReferenzSystem() {
        if (newReferenzSystem != null && newReferenzSystemId != null && !newReferenzSystemId.isEmpty()) {
            this.referenzSystemLinks.add(new ReferenzSystemLink(newReferenzSystem, newReferenzSystemId));
        }
    }

    public boolean isReferenzSystemLinksNotEmpty() {
        return newReferenzSystem != null && !referenzSystemLinks.isEmpty();
    }

    public String getNewReferenzSystemId() {
        return newReferenzSystemId;
    }

    public void setNewReferenzSystemId(String newReferenzSystemId) {
        this.newReferenzSystemId = newReferenzSystemId;
    }

    public ReferenzSystem getNewReferenzSystem() {
        return newReferenzSystem;
    }

    public void setNewReferenzSystem(ReferenzSystem newReferenzSystem) {
        this.newReferenzSystem = newReferenzSystem;
    }

    public AnforderungId getAnforderungId() {
        return anforderungId;
    }

    public VereinbarungType getDefaultVereinbarungType() {
        return defaultVereinbarungType;
    }

    public void setDefaultVereinbarungType(VereinbarungType defaultVereinbarungType) {
        this.defaultVereinbarungType = defaultVereinbarungType;
    }

}
