package de.interfaceag.bmw.pzbk.anforderungohnemeldungedit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungFreigabeDto;
import de.interfaceag.bmw.pzbk.controller.dialogs.UmsetzerDialogFields;
import de.interfaceag.bmw.pzbk.converters.ModulDtoConverter;
import de.interfaceag.bmw.pzbk.converters.ModulKomponenteDtoConverter;
import de.interfaceag.bmw.pzbk.converters.ModulSeTeamDtoConverter;
import de.interfaceag.bmw.pzbk.dialog.viewdata.DialogUmsetzerDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.ModulDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.ModulKomponenteDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.ModulSeTeamDto;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.entities.comparator.ModulKomponenteComparator;
import de.interfaceag.bmw.pzbk.entities.comparator.ModulSeTeamComparator;
import de.interfaceag.bmw.pzbk.shared.command.AddAnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.shared.command.AddDialogUmsetzerDto;
import de.interfaceag.bmw.pzbk.shared.command.AnforderungFreigabeDtoCommand;
import de.interfaceag.bmw.pzbk.shared.command.DialogUmsetzerCommand;
import de.interfaceag.bmw.pzbk.shared.command.RemoveAnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.shared.command.RemoveDialogUmsetzerDto;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * @author fn
 */
public class UmsetzerDialogViewData implements UmsetzerDialogFields, Serializable {

    private final List<ModulKomponenteDto> allKomponenten;
    private final List<ModulSeTeamDto> allSeTeams;
    private final List<ModulDto> allModule;

    private ModulDto selectedModul;
    private List<ModulDto> filteredModule;

    private ModulSeTeamDto selectedSeTeam;
    private List<ModulSeTeamDto> filteredModulSeTeams;

    private ModulKomponenteDto selectedKomponente;
    private List<ModulKomponenteDto> filteredModulKomponenten;

    private final Set<DialogUmsetzerDto> initialDialogDataSet;
    private Set<DialogUmsetzerDto> dialogUmsetzerViewDataSet;

    private boolean changed;

    private final List<DialogUmsetzerCommand> umsetzerChanges = new ArrayList<>();
    private final List<AnforderungFreigabeDtoCommand> freigabeChanges = new ArrayList<>();

    private final VereinbarungType defaultVereinbarungType;

    public UmsetzerDialogViewData(List<ModulDto> allModule, List<ModulSeTeamDto> allSeTeams,
            List<ModulKomponenteDto> allKomponenten, Set<DialogUmsetzerDto> dialogUmsetzerViewDataSet, VereinbarungType defaultVereinbarungType) {
        this.allKomponenten = allKomponenten;
        this.allSeTeams = allSeTeams;
        this.allModule = allModule;
        this.filteredModulKomponenten = new ArrayList<>(allKomponenten);
        this.filteredModulSeTeams = new ArrayList<>(allSeTeams);
        this.filteredModule = new ArrayList<>(allModule);
        this.dialogUmsetzerViewDataSet = dialogUmsetzerViewDataSet;
        this.initialDialogDataSet = new HashSet<>(dialogUmsetzerViewDataSet);
        this.defaultVereinbarungType = defaultVereinbarungType;
    }

    public void update(Set<DialogUmsetzerDto> dialogUmsetzerDtos) {
        this.dialogUmsetzerViewDataSet = new HashSet<>(dialogUmsetzerDtos);
    }

    @Override
    public ModulDto getSelectedModul() {
        return selectedModul;
    }

    @Override
    public void setSelectedModul(ModulDto selectedModul) {
        this.selectedModul = selectedModul;
    }

    @Override
    public ModulSeTeamDto getSelectedSeTeam() {
        return selectedSeTeam;
    }

    @Override
    public void setSelectedSeTeam(ModulSeTeamDto selectedSeTeam) {
        this.selectedSeTeam = selectedSeTeam;
        setSeTeamParent(selectedSeTeam);
    }

    private void setSeTeamParent(ModulSeTeamDto selectedSeTeam) {
        if (selectedSeTeam != null && selectedSeTeam.getModulId() != null) {
            Optional<ModulDto> modulDtoById = getModulDtoById(selectedSeTeam.getModulId());
            modulDtoById.ifPresent(this::setSelectedModul);
        }
    }

    @Override
    public ModulKomponenteDto getSelectedKomponente() {
        return selectedKomponente;
    }

    @Override
    public void setSelectedKomponente(ModulKomponenteDto selectedKomponente) {
        this.selectedKomponente = selectedKomponente;
        setKomponenteParent(selectedKomponente);
    }

    private void setKomponenteParent(ModulKomponenteDto selectedKomponente) {
        if (selectedKomponente != null && selectedKomponente.getSeTeamId() != null) {
            Optional<ModulSeTeamDto> seTeamDtoById = getSeTeamDtoById(selectedKomponente.getSeTeamId());
            seTeamDtoById.ifPresent(this::setSelectedSeTeam);
        }
    }

    private Optional<ModulDto> getModulDtoById(Long modulId) {
        return allModule.stream().filter(modul -> modul.getId().equals(modulId)).findAny();
    }

    private Optional<ModulSeTeamDto> getSeTeamDtoById(Long seTeamId) {
        return allSeTeams.stream().filter(seTeamDto -> seTeamDto.getId().equals(seTeamId)).findAny();
    }

    @Override
    public List<ModulKomponenteDto> getFilteredModulKomponenten() {
        return filteredModulKomponenten;
    }

    public void setFilteredModulKomponenten(List<ModulKomponenteDto> filteredModulKomponenten) {
        filteredModulKomponenten.sort(new ModulKomponenteComparator());
        this.filteredModulKomponenten = filteredModulKomponenten;
    }

    @Override
    public List<ModulSeTeamDto> getFilteredModulSeTeams() {
        return filteredModulSeTeams;
    }

    public void setFilteredModulSeTeams(List<ModulSeTeamDto> filteredModulSeTeams) {
        filteredModulSeTeams.sort(new ModulSeTeamComparator());
        this.filteredModulSeTeams = filteredModulSeTeams;
    }

    @Override
    public List<ModulDto> getFilteredModule() {
        return filteredModule;
    }

    public void setFilteredModule(List<ModulDto> filteredModule) {
        this.filteredModule = filteredModule;
    }

    @Override
    public void addUmsetzer() {
        DialogUmsetzerDto umsetzer = null;
        if (selectedKomponente != null) {
            umsetzer = DialogUmsetzerDto.forKomponenteDto(selectedKomponente);
        } else if (selectedSeTeam != null) {
            umsetzer = DialogUmsetzerDto.forSeTeamDto(selectedSeTeam);
        }
        if (umsetzer != null) {
            getDialogUmsetzerViewDataSet().add(umsetzer);

            if (!umsetzerChanges.contains(new AddDialogUmsetzerDto(umsetzer))) {
                umsetzerChanges.add(new AddDialogUmsetzerDto(umsetzer));
            }

            String seTeamName = umsetzer.getSeTeamName();

            if (initialDialogDataSet.stream().noneMatch(umsetzerDto -> umsetzerDto.getSeTeamName().equals(seTeamName))) {
                VereinbarungType vereinbarungType = getDefaultVereinbarungType();
                AnforderungFreigabeDto newFreigabe = new AnforderungFreigabeDto(umsetzer.getSeTeamId(), umsetzer.getSeTeamName(), umsetzer.getModulName(), vereinbarungType);
                if (!freigabeChanges.contains(new AddAnforderungFreigabeBeiModulSeTeam(newFreigabe))) {
                    freigabeChanges.add(new AddAnforderungFreigabeBeiModulSeTeam(newFreigabe));
                }
            }

            changed = Boolean.TRUE;
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Es wurde kein SE-Team ausgewählt!", "Bitte wählen Sie ein SE-Team oder eine Komponente."));
        }
    }

    @Override
    public void removeUmsetzer(DialogUmsetzerDto umsetzer) {
        if (umsetzer != null && dialogUmsetzerViewDataSet.contains(umsetzer)) {
            dialogUmsetzerViewDataSet.remove(umsetzer);
            umsetzerChanges.add(new RemoveDialogUmsetzerDto(umsetzer));

            String seTeamName = umsetzer.getSeTeamName();

            if (dialogUmsetzerViewDataSet.stream().noneMatch(umsetzerDto -> umsetzerDto.getSeTeamName().equals(seTeamName))) {
                VereinbarungType vereinbarungType = getDefaultVereinbarungType();
                AnforderungFreigabeDto newFreigabe = new AnforderungFreigabeDto(umsetzer.getSeTeamId(), umsetzer.getSeTeamName(), umsetzer.getModulName(), vereinbarungType);
                freigabeChanges.add(new RemoveAnforderungFreigabeBeiModulSeTeam(newFreigabe));
            }

            changed = Boolean.TRUE;
        }
    }

    @Override
    public Set<DialogUmsetzerDto> getDialogUmsetzerViewDataSet() {
        return dialogUmsetzerViewDataSet;
    }

    @Override
    public void setDialogUmsetzerViewDataSet(Set<DialogUmsetzerDto> dialogUmsetzerViewDataSet) {
        this.dialogUmsetzerViewDataSet = dialogUmsetzerViewDataSet;
    }

    @Override
    public boolean isChanged() {
        return changed;
    }

    @Override
    public void resetChangedBoolean() {
        this.changed = Boolean.FALSE;
    }

    public List<ModulSeTeamDto> getAllSeTeams() {
        return allSeTeams;
    }

    public List<ModulDto> getAllModule() {
        return allModule;
    }

    @Override
    public final List<ModulDto> completeModul(String query) {
        if (query.isEmpty()) {
            return allModule;
        }
        List<ModulDto> result = new ArrayList<>();
        Pattern pattern = Pattern.compile(".*" + query + ".*", Pattern.CASE_INSENSITIVE);
        allModule.stream().filter(modul -> pattern.matcher(modul.getName()).matches()).forEachOrdered(result::add);
        return result;
    }

    @Override
    public final List<ModulSeTeamDto> completeSeTeam(String query) {
        List<ModulSeTeamDto> result = new ArrayList<>();
        Pattern pattern = Pattern.compile(".*" + query + ".*", Pattern.CASE_INSENSITIVE);
        if (selectedModul == null) {
            allSeTeams.stream().filter(seTeam -> query.isEmpty()
                    || pattern.matcher(seTeam.getName()).matches())
                    .forEachOrdered(result::add);
        } else {
            allSeTeams.stream().filter(seTeam -> (query.isEmpty()
                    || pattern.matcher(seTeam.getName()).matches())
                    && seTeam.getModulName().equals(selectedModul.getName()))
                    .forEachOrdered(result::add);
        }
        return result;
    }

    @Override
    public final List<ModulKomponenteDto> completeKomponente(String query) {
        List<ModulKomponenteDto> result = new ArrayList<>();
        Pattern pattern = Pattern.compile(".*" + query + ".*", Pattern.CASE_INSENSITIVE);
        if (selectedSeTeam == null && selectedModul == null) {
            allKomponenten.stream().filter(modulKomponente -> query.isEmpty()
                    || pattern.matcher(modulKomponente.getName()).matches()
                    || pattern.matcher(modulKomponente.getPpg()).matches())
                    .forEachOrdered(result::add);
        } else if (selectedSeTeam == null && selectedModul != null) {
            selectedModul.getSeTeams().forEach(seTteam
                    -> seTteam.getKomponenten().stream().filter(modulKomponente -> query.isEmpty()
                    || pattern.matcher(modulKomponente.getName()).matches()
                    || pattern.matcher(modulKomponente.getPpg()).matches())
                            .forEachOrdered(result::add)
            );
        } else {
            allKomponenten.stream().filter(modulKomponente -> (query.isEmpty()
                    || pattern.matcher(modulKomponente.getName()).matches()
                    || pattern.matcher(modulKomponente.getPpg()).matches())
                    && modulKomponente.getSeTeamName().equals(selectedSeTeam.getName()))
                    .forEachOrdered(result::add);
        }
        return result;
    }

    @Override
    public Converter getModulConverter() {
        return new ModulDtoConverter(allModule);
    }

    @Override
    public Converter getModulKomponenteConverter() {
        return new ModulKomponenteDtoConverter(allKomponenten);
    }

    @Override
    public Converter getModulSeTeamConverter() {
        return new ModulSeTeamDtoConverter(allSeTeams);
    }

    @Override
    public void refreshModulSeTeamList() {
        setSelectedKomponente(null);
        setSelectedSeTeam(null);
        setFilteredModulSeTeams(this.completeSeTeam(""));
        refreshUmsetzerKompList();
    }

    @Override
    public void refreshUmsetzerKompList() {
        setSelectedKomponente(null);
        setFilteredModulKomponenten(this.completeKomponente(""));
    }

    public List<DialogUmsetzerCommand> getUmsetzerChanges() {
        return umsetzerChanges;
    }

    public void clearUmsetzerChanges() {
        this.umsetzerChanges.clear();
    }

    public List<AnforderungFreigabeDtoCommand> getFreigabeChanges() {
        return freigabeChanges;
    }

    public void clearFreigabeChanges() {
        this.freigabeChanges.clear();
    }

    public VereinbarungType getDefaultVereinbarungType() {
        return defaultVereinbarungType;
    }

}
