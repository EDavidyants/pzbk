package de.interfaceag.bmw.pzbk.anforderungohnemeldungedit;

import de.interfaceag.bmw.pzbk.controller.dialogs.SensorCocDialogFields;
import de.interfaceag.bmw.pzbk.converters.SensorCocConverter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author fn
 */
public class SensorCocDialogViewData implements SensorCocDialogFields, Serializable {

    private final List<SensorCoc> allSensorCocs;
    private SensorCoc sensorCocAutoComplete;

    private String inputSensorCocRessort;
    private String inputSensorCocOrtung;

    // left table
    private String inputSensorCocRessortOrtung;
    private List<String> sensorCocRessortOrtungList;

    // right table
    private String inputSensorCocTechnologie;
    private List<String> sensorCocTechnologieList;

    public SensorCocDialogViewData(List<SensorCoc> allSensorCocs) {
        if (allSensorCocs != null) {
            this.allSensorCocs = new ArrayList<>(allSensorCocs);
        } else {
            this.allSensorCocs = Collections.emptyList();
        }

        initRessortOrtungList();
        initTechnologieList();
    }

    public List<SensorCoc> getAllSensorCocs() {
        return allSensorCocs;
    }

    @Override
    public List<String> getSensorCocRessortOrtungList() {
        return sensorCocRessortOrtungList;
    }

    public void setSensorCocRessortOrtungList(List<String> sensorCocRessortOrtungList) {
        this.sensorCocRessortOrtungList = sensorCocRessortOrtungList;
    }

    public void setSensorCocTechnologieList(List<String> sensorCocTechnologieList) {
        this.sensorCocTechnologieList = sensorCocTechnologieList;
    }

    @Override
    public String getInputSensorCocRessortOrtung() {
        return inputSensorCocRessortOrtung;
    }

    public String getInputSensorCocRessort() {
        return inputSensorCocRessort;
    }

    public void setInputSensorCocRessort(String inputSensorCocRessort) {
        this.inputSensorCocRessort = inputSensorCocRessort;
    }

    public String getInputSensorCocOrtung() {
        return inputSensorCocOrtung;
    }

    public void setInputSensorCocOrtung(String inputSensorCocOrtung) {
        this.inputSensorCocOrtung = inputSensorCocOrtung;
    }

    @Override
    public void setInputSensorCocRessortOrtung(String inputSensorCocRessortOrtung) {
        if (inputSensorCocRessortOrtung != null) {
            this.inputSensorCocRessort = inputSensorCocRessortOrtung.split(">")[0];
            if (inputSensorCocRessortOrtung.split(">").length < 2) {
                this.inputSensorCocOrtung = "";
            } else {
                this.inputSensorCocOrtung = inputSensorCocRessortOrtung.split(">")[1];
            }
        }
        this.inputSensorCocRessortOrtung = inputSensorCocRessortOrtung;
    }

    @Override
    public List<String> getSensorCocTechnologieList() {
        return sensorCocTechnologieList;
    }

    @Override
    public String getInputSensorCocTechnologie() {
        return inputSensorCocTechnologie;
    }

    @Override
    public void setInputSensorCocTechnologie(String inputSensorCocTechnologie) {
        this.inputSensorCocTechnologie = inputSensorCocTechnologie;
        updateSelectedSensorCoc(inputSensorCocTechnologie);
    }

    private void updateSelectedSensorCoc(String technologie) {
        if (technologie != null && !technologie.isEmpty()) {

            Optional<SensorCoc> sensorCocByTechnologie = allSensorCocs.stream()
                    .filter(sensorCoc -> sensorCoc.getTechnologie().equals(technologie))
                    .findFirst();

            if (sensorCocByTechnologie.isPresent()) {
                this.sensorCocAutoComplete = sensorCocByTechnologie.get();
            }
        }
    }

    @Override
    public void setSensorCocAutoComplete(SensorCoc sensorCocAutoComplete) {
        if (sensorCocAutoComplete != null) {
            this.inputSensorCocRessort = sensorCocAutoComplete.getRessort();
            this.inputSensorCocOrtung = sensorCocAutoComplete.getOrtung();
            this.inputSensorCocTechnologie = sensorCocAutoComplete.getTechnologie();
            this.inputSensorCocRessortOrtung = sensorCocAutoComplete.getRessort() + ">" + sensorCocAutoComplete.getOrtung();
        }
        this.sensorCocAutoComplete = sensorCocAutoComplete;
    }

    @Override
    public SensorCoc getSensorCocAutoComplete() {
        return sensorCocAutoComplete;
    }

    @Override
    public SensorCocConverter getSensorCocConverter() {
        return new SensorCocConverter(allSensorCocs);
    }

    @Override
    public void refresh() {
        initTechnologieList(getInputSensorCocRessort(), getInputSensorCocOrtung());
    }

    private void initRessortOrtungList() {
        setSensorCocRessortOrtungList(computeSensorCocRessortOrtungList(allSensorCocs));
    }

    private static List<String> computeSensorCocRessortOrtungList(List<SensorCoc> allSensorCocs) {

        if (allSensorCocs == null) {
            return Collections.emptyList();
        }

        return allSensorCocs.stream().map(sensorCoc -> sensorCoc.getRessort() + ">" + sensorCoc.getOrtung()).distinct().sorted().collect(Collectors.toList());
    }

    private void initTechnologieList() {
        setSensorCocTechnologieList(computeSensorCocTechnologieList(null, null, allSensorCocs));
    }

    private void initTechnologieList(String inputRessort, String inputOrtung) {
        setSensorCocTechnologieList(computeSensorCocTechnologieList(inputRessort, inputOrtung, allSensorCocs));
    }

    private List<String> computeSensorCocTechnologieList(String inputRessort, String inputOrtung, List<SensorCoc> allSensorCocs) {

        if (allSensorCocs == null) {
            return Collections.emptyList();
        }

        Stream<SensorCoc> filteredSensorCocs = allSensorCocs.stream();

        if (inputRessort != null && !inputRessort.isEmpty()) {
            filteredSensorCocs = filteredSensorCocs.filter(sensorCoc -> sensorCoc.getRessort().contains(inputRessort));
        }

        if (inputOrtung != null && !inputOrtung.isEmpty()) {
            filteredSensorCocs = filteredSensorCocs.filter(sensorCoc -> sensorCoc.getOrtung().contains(inputOrtung));
        }

        return filteredSensorCocs.map(SensorCoc::getTechnologie).distinct().sorted().collect(Collectors.toList());
    }

    @Override
    public List<SensorCoc> completeSensorCoc(String query) {
        return completeSensorCoc(query, allSensorCocs);
    }

    private static List<SensorCoc> completeSensorCoc(String query, List<SensorCoc> allSensorCocs) {

        if (allSensorCocs == null) {
            return Collections.emptyList();
        }

        return allSensorCocs.stream()
                .filter(sensorCoc -> sensorCoc.pathToString().toLowerCase().contains(query.toLowerCase()))
                .collect(Collectors.toList());
    }
}
