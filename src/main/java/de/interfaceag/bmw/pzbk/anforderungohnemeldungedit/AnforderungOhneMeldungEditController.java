package de.interfaceag.bmw.pzbk.anforderungohnemeldungedit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungBeschreibungChangedValidator;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungModulChangedValidator;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.complete.FestgestelltInComplete;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungFreigabeDto;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalButtonActions;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalDialogActions;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalDialogData;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalData;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.validator.FahrzeugmerkmaleValidator;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.validator.SensorCocValidator;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.validator.UmsetzerValidator;
import de.interfaceag.bmw.pzbk.controller.dialogs.AnforderungChangedInStatusPlausiConfirmDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.SensorCocDialogMethods;
import de.interfaceag.bmw.pzbk.controller.dialogs.UmsetzerDialogMethods;
import de.interfaceag.bmw.pzbk.converters.FestgestelltInConverter;
import de.interfaceag.bmw.pzbk.converters.TteamConverter;
import de.interfaceag.bmw.pzbk.dialog.viewdata.DialogUmsetzerDto;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import org.apache.poi.util.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJBTransactionRolledbackException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author fn
 */
@ViewScoped
@Named
public class AnforderungOhneMeldungEditController implements Serializable, SensorCocDialogMethods, UmsetzerDialogMethods,
        AnforderungChangedInStatusPlausiConfirmDialog, AnforderungEditFahrzeugmerkmalButtonActions, AnforderungEditFahrzeugmerkmalDialogActions {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungOhneMeldungEditController.class.getName());

    @Inject
    private AnforderungOhneMeldungEditFacade facade;

    @Inject
    private Session session;

    private AnforderungOhneMeldungEditViewObjekt viewObjekt;

    @Inject
    private AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData;

    @PostConstruct
    public void init() {
        initViewData();
    }

    private void initViewData() {
        viewObjekt = facade.getViewData(UrlParameterUtils.getUrlParameter());
    }

    public AnforderungOhneMeldungEditViewPermission getViewPermission() {
        return viewObjekt.getViewPermission();
    }

    public AnforderungOhneMeldungEditViewData getViewData() {
        return viewObjekt.getViewData().orElse(null);
    }

    public AnforderungOhneMeldungEditWorkFields getWorkFields() {
        return viewObjekt.getWorkFields();
    }

    public String getFachId() {
        Optional<Prozessbaukasten> prozessbaukastenOptional = getWorkFields().getProzessbaukasten();
        if (prozessbaukastenOptional.isPresent()) {
            return prozessbaukastenOptional.get().getFachId();
        }
        return "";
    }

    public String getVersion() {
        Optional<Prozessbaukasten> prozessbaukastenOptional = getWorkFields().getProzessbaukasten();
        return prozessbaukastenOptional.map(prozessbaukasten -> String.valueOf(prozessbaukasten.getVersion())).orElse("");
    }

    public SensorCocDialogViewData getSensorCocDialogViewData() {
        return viewObjekt.getSensorCocDialogViewData();
    }

    public UmsetzerDialogViewData getUmsetzerDialogViewData() {
        return viewObjekt.getUmsetzerDialogViewData();
    }

    @Override
    public String saveFromDialog() {
        return saveValidatedData();
    }


    public String save() {

        boolean isValid = checkTitelbild();

        if (isValid) {
            boolean showKommentarChangedDialog = isAnforderungChangedInStatusPlausibilisiert();

            if (showKommentarChangedDialog) {
                return "";
            }
            return saveValidatedData();
        }

        return "";
    }

    private String saveValidatedData() {
        try {
            if (getWorkFields().isNewAnforderung()) {
                return facade.saveNewAnforderung(getWorkFields().getProzessbaukasten().orElse(null), getViewData(), getWorkFields().getStatusChangeKommentar(), anforderungEditFahrzeugmerkmalData);
            } else if (getWorkFields().isNewVersion()) {
                facade.saveNewAnforderungVersion(getViewData(), getWorkFields().getVersionKommentar(), anforderungEditFahrzeugmerkmalData);
            } else if (getWorkFields().isChangedInStatusPlausibilisiert()) {
                facade.saveChangesToAnforderungAndResetFreigabe(getViewData(), anforderungEditFahrzeugmerkmalData);
            } else {
                facade.saveChangesToAnforderung(getViewData(), anforderungEditFahrzeugmerkmalData);
            }
            return PageUtils.getUrlForPageWithFachIdAndVersion(Page.ANFORDERUNGVIEW, getViewData().getFachId(), getViewData().getVersion());
        } catch (EJBTransactionRolledbackException ex) {
            Throwable cause = ex.getCause();
            while ((cause != null) && !(cause instanceof ConstraintViolationException)) {
                cause = cause.getCause();
            }
            if (cause instanceof ConstraintViolationException) {
                ConstraintViolationException violationException = (ConstraintViolationException) cause;
                Set<ConstraintViolation<?>> constraintViolations = violationException.getConstraintViolations();
                LOG.warn("Validation errors: {}", constraintViolations);
                showViolations(constraintViolations);
            } else {
                LOG.error("Save of Anforderung failed!", ex);
            }
            return "";
        }
    }

    private void showViolations(Set<ConstraintViolation<?>> constraintViolations) {

        FacesContext context = FacesContext.getCurrentInstance();
        context.validationFailed();

        Iterator<ConstraintViolation<?>> iterator = constraintViolations.iterator();
        while (iterator.hasNext()) {
            ConstraintViolation<?> violation = iterator.next();
            String message = violation.getMessage();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, ""));
        }
    }

    private boolean checkTitelbild() {
        return AnforderungOhneMeldungTitelbildValidator.validate(getViewData(), getWorkFields());
    }

    private boolean isAnforderungChangedInStatusPlausibilisiert() {
        boolean isKommentarChangedInStatusPlausib = AnforderungBeschreibungChangedValidator.isKommentarChangedInStatusPlausibilisiert(getViewData(), getWorkFields().getBeschreibungAnforderungDe());

        boolean isModulChangedInStatusPlausib = AnforderungModulChangedValidator.isModulChangedInStatusPlausibilisiert(getViewData(), getWorkFields().getOriginalFreigabeBeiModulSeTeam());

        if (!getWorkFields().isChangedInStatusPlausibilisiert() && (isKommentarChangedInStatusPlausib || isModulChangedInStatusPlausib)) {
            getWorkFields().setChangedInStatusPlausibilisiert(true);
            RequestContext.getCurrentInstance().execute("PF('anforderungOhneMeldungPlausiChangedDialog').show()");
            return true;
        }
        getWorkFields().setChangedInStatusPlausibilisiert(false);
        return false;
    }

    public String cancel() {
        if (getWorkFields().isChangedInStatusPlausibilisiert()) {
            getWorkFields().setChangedInStatusPlausibilisiert(false);
        }
        if (getViewData().getFachId() != null && getViewData().getVersion() != null && !getViewData().getFachId().equals("")) {
            return PageUtils.getUrlForPageWithFachIdAndVersion(Page.ANFORDERUNGVIEW, getViewData().getFachId(), getViewData().getVersion());
        } else {
            return PageUtils.getRedirectUrlForPage(Page.DASHBOARD);
        }
    }

    public List<FestgestelltIn> completeFestgestelltIn(String query) {
        return FestgestelltInComplete.completeFestgestelltIn(query, getViewData().getFestgestelltIn(), getWorkFields().getFestgestelltIn());
    }

    public Converter getFestgestelltInConverter() {
        return new FestgestelltInConverter(getWorkFields().getFestgestelltIn());
    }

    public void validateSensorCoc(FacesContext context, UIComponent component, Object value) {
        SensorCocValidator.validateSensorCoc(context, component, value, getViewData().getSensorCoc());
    }

    public void validateFahrzeugmerkmale(FacesContext context, UIComponent component, Object value) {
        FahrzeugmerkmaleValidator.validateFahrzeugmerkmale(context, component, value, getViewData().getFreigaben());
    }

    public void validateUmsetzer(FacesContext context, UIComponent component, Object value) {
        UmsetzerValidator.validateUmsetzer(context, component, value, getViewData().getUmsetzer());
    }

    public TteamConverter getTteamConverter() {
        return new TteamConverter(getWorkFields().getAllTteams());
    }

    public void updateTteamleiter() {
        Tteam tteam = getViewData().getTteam();
        facade.getTteamleiterOfTteam(tteam)
                .ifPresent(t -> getViewData().getTteam().setTeamleiter(t));
    }

    public void removeModulFreigabe(AnforderungFreigabeDto anforderungFreigabeDto) {
        getViewData().removeFreigabe(anforderungFreigabeDto);
        getViewData().setUmsetzer(getViewData().getUmsetzer().stream().filter(umsetzer -> !umsetzer.getSeTeamName().equals(anforderungFreigabeDto.getSeTeamName())).collect(Collectors.toSet()));
        getUmsetzerDialogViewData().update(getViewData().getUmsetzer());
    }

    public List<DialogUmsetzerDto> getUmsetzerListFilteredByModulSeTeam(Long seTeamId) {

        if (seTeamId == null) {
            return Collections.emptyList();
        }

        Set<DialogUmsetzerDto> umsetzerList = getViewData().getUmsetzer();
        if (umsetzerList != null && !umsetzerList.isEmpty()) {
            return umsetzerList.stream()
                    .filter(umsetzer -> umsetzer.getKomponenteName() != null)
                    .filter(umsetzer -> umsetzer.getKomponenteName() != null)
                    .filter(umsetzer -> umsetzer.getSeTeamId().equals(seTeamId))
                    .collect(Collectors.toList());

        } else {
            return Collections.emptyList();
        }

    }

    public void uploadAnhang(FileUploadEvent event) {
        LOG.info("Upload started...");
        UploadedFile anhang = event.getFile();
        if (anhang != null) {
            LOG.info("Done uploading {}", anhang.getFileName());
            try {
                byte[] uploadedData = IOUtils.toByteArray(anhang.getInputstream());
                Anhang a = new Anhang(session.getUser(), anhang.getFileName(), anhang.getContentType(), uploadedData);
                getViewData().addAnhang(a);
            } catch (IOException ex) {
                LOG.error("upload failed", ex);
            }
        }
    }

    public void showAnhangGrafikDialog(Anhang anhang) {
        RequestContext context = RequestContext.getCurrentInstance();
        if (anhang != null) {
            getViewData().setSelectedAnhang(anhang);
        }

        context.update("dialog:anhangGrafikDialogForm");
        context.execute("PF('anhangGrafikDialog').show()");
    }

    public StreamedContent downloadAnhang(Anhang anhang) {
        return facade.downloadAnhang(anhang);
    }

    @Override
    public void selectSensorCoC() {
        Optional<Mitarbeiter> sensorCoCLeiter = facade.getSensorCoCLeiterForSensorCoc(getSensorCocDialogViewData().getSensorCocAutoComplete());
        getViewData().setSensorCoc(getSensorCocDialogViewData().getSensorCocAutoComplete());
        if (sensorCoCLeiter.isPresent()) {
            getViewData().getSensorCoc().setSensorCoCLeiter(sensorCoCLeiter.get());
        }
    }

    public void deselectSensorCoC() {
        getViewData().setSensorCoc(null);
    }

    @Override
    public void processUmsetzerChanges() {

        if (!getUmsetzerDialogViewData().isChanged()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Es wurde kein Umsetzer hinzugefügt!", ""));
        }

        getUmsetzerDialogViewData().resetChangedBoolean();

        getUmsetzerDialogViewData().getUmsetzerChanges().forEach(change -> change.apply(getViewData().getUmsetzer()));
        getUmsetzerDialogViewData().clearUmsetzerChanges();
        getUmsetzerDialogViewData().getFreigabeChanges().forEach(change -> change.apply(getViewData().getFreigaben()));
        getUmsetzerDialogViewData().clearFreigabeChanges();
        updateFahrzeugmerkmaleForFreigaben();

        RequestContext.getCurrentInstance().update("editAfo:anfoForm:anfoAccordion:modulsTable");

    }

    private void updateFahrzeugmerkmaleForFreigaben() {
        facade.updateFahrzeugmerkmaleForFreigaben(getViewData().getFreigaben(), anforderungEditFahrzeugmerkmalData, getViewData().getAnforderungId());
    }

    @Override
    public void resetUmsetzerList() {
        getUmsetzerDialogViewData().setDialogUmsetzerViewDataSet(getViewData().getUmsetzer());
    }

    public boolean isProzessbaukastenPresent() {
        return getWorkFields().getProzessbaukasten().isPresent();
    }

    public String getProzessbaukastenLinkName() {
        Optional<Prozessbaukasten> prozessbaukastenOptional = getWorkFields().getProzessbaukasten();
        if (prozessbaukastenOptional.isPresent()) {
            Prozessbaukasten prozessbaukasten = prozessbaukastenOptional.get();
            return prozessbaukasten.getFachId() + " | V" + prozessbaukasten.getVersion() + " : " + prozessbaukasten.getBezeichnung();
        } else {
            return "";
        }

    }

    @Override
    public void showFahrzeugmerkmalEditDialogForModulSeTeam(ModulSeTeamId modulSeTeamId) {
        final AnforderungId anforderungId = getViewData().getAnforderungId();

        if (anforderungEditFahrzeugmerkmalData.updateActiveData(modulSeTeamId)) {
            LOG.debug("found entry for {} in map", modulSeTeamId);
        } else {
            LOG.debug("load data for {} from database", modulSeTeamId);
            final AnforderungEditFahrzeugmerkmalDialogData anforderungEditFahrzeugmerkmalDialogData = facade.getAnforderungEditFahrzeugmerkmalDialogData(anforderungId, modulSeTeamId);
            this.anforderungEditFahrzeugmerkmalData.addDataForModulSeTeam(modulSeTeamId, anforderungEditFahrzeugmerkmalDialogData);
        }

        LOG.debug("show Fahrzeugmerkmal edit dialog for {} and {}", anforderungId, modulSeTeamId);
        final RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('fahrzeugmerkmalEditDialog').show()");
        requestContext.update("dialog:fahrzeugmerkmalEditDialogContent:editDialogForm");
    }

    @Override
    public void processFahrzeugmerkmalChanges() {
        for (AnforderungFreigabeDto anforderungFreigabeDto : getViewData().getFreigaben()) {
            final boolean allSelected = getAnforderungEditFahrzeugmerkmalDialogData().getFahrzeugmerkmale().stream().noneMatch(anforderungEditFahrzeugmerkmal -> anforderungEditFahrzeugmerkmal.getSelectedAuspraegungen().isEmpty());
            if (anforderungFreigabeDto.getModulSeTeamId().equals(anforderungEditFahrzeugmerkmalData.getActiveModulSeTeamId()) && allSelected) {
                LOG.debug("update {} to status configured = TRUE", anforderungFreigabeDto.getSeTeamName());
                anforderungFreigabeDto.setFahrzeugmerkmalConfigured(Boolean.TRUE);
            } else if (anforderungFreigabeDto.getModulSeTeamId().equals(anforderungEditFahrzeugmerkmalData.getActiveModulSeTeamId()) && !allSelected) {
                anforderungFreigabeDto.setFahrzeugmerkmalConfigured(Boolean.FALSE);
                LOG.debug("update {} to status configured = FALSE", anforderungFreigabeDto.getSeTeamName());
            }
        }
    }

    public AnforderungEditFahrzeugmerkmalDialogData getAnforderungEditFahrzeugmerkmalDialogData() {
        return anforderungEditFahrzeugmerkmalData.getActiveFahrzeugmerkmalDialogData();
    }
}
