package de.interfaceag.bmw.pzbk.anforderungohnemeldungedit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungFreigabeDto;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.AnforderungEditFahrzeugmerkmalDialogService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.AnforderungEditFahrzeugmerkmalSaveService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalDialogData;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalData;
import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.DialogUmsetzerDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.ModulDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.ModulKomponenteDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.ModulSeTeamDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Bild;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.entities.comparator.TteamComparator;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ImageService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author fn
 */
@Stateless
@Named
public class AnforderungOhneMeldungEditFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungOhneMeldungEditFacade.class.getName());
    public static final String CREATED = "created";

    @Inject
    Session session;
    @Inject
    AnforderungService anforderungService;
    @Inject
    TteamService tteamService;
    @Inject
    BerechtigungService berechtigungService;
    @Inject
    ImageService imageService;
    @Inject
    SensorCocService sensorCocService;
    @Inject
    ModulService modulService;
    @Inject
    ProzessbaukastenReadService prozessbaukastenReadService;
    @Inject
    ProzessbaukastenZuordnenService prozessbaukastenZuordnenService;
    @Inject
    AnforderungEditFahrzeugmerkmalDialogService anforderungEditFahrzeugmerkmalService;
    @Inject
    AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService anforderungModulSeTeamFahrzeugmerkmalAuspraegungService;
    @Inject
    AnforderungEditFahrzeugmerkmalSaveService anforderungEditFahrzeugmerkmalSaveService;

    public AnforderungOhneMeldungEditViewObjekt getViewData(UrlParameter urlParameter) {

        Anforderung anforderung = getAnforderungByUrlParameter(urlParameter);

        String createStatus = urlParameter.getValue(CREATED).orElse("");

        if (anforderung == null) {
            return AnforderungOhneMeldungEditViewObjekt.forNullAnforderung();
        }

        AnforderungOhneMeldungEditViewPermission viewPermission = getViewPermissionForAnforderung(anforderung);

        Anforderung anforderungCopy = anforderungService.getAnforderungWorkCopyByAnfoderung(anforderung);

        AnforderungOhneMeldungEditViewData anforderungData = getAnforderungOhneMeldungViewDataForAnforderung(anforderungCopy, createStatus);

        Prozessbaukasten prozessbaukasten = (getProzessbaukastenFromUrl(urlParameter)).orElse(null);

        if (isProzessbaukastenZugeordnet(prozessbaukasten, anforderungData)) {
            anforderungData.setDefaultVereinbarungType(VereinbarungType.STANDARD);
        }

        AnforderungOhneMeldungEditWorkFields workFields = getWorkFields(prozessbaukasten, anforderungCopy, anforderungData.getBeschreibungAnforderungDe(),
                urlParameter.getValue("comment").orElse(""),
                urlParameter.getValue(CREATED).orElse(""));

        SensorCocDialogViewData sensorCocDialogViewData = getSensorCocDialogViewData();

        UmsetzerDialogViewData umsetzerDialogViewData = getUmsetzerDialogViewData(anforderungData);

        return AnforderungOhneMeldungEditViewObjekt.getBuilder()
                .withViewData(anforderungData)
                .withViewPermission(viewPermission)
                .withWorkFields(workFields)
                .withSensorCocDialogViewData(sensorCocDialogViewData)
                .withUmsetzerDialogViewData(umsetzerDialogViewData)
                .build();
    }

    private static boolean isProzessbaukastenZugeordnet(Prozessbaukasten prozessbaukasten, AnforderungOhneMeldungEditViewData anforderungData) {
        return prozessbaukasten != null
                || anforderungData.getStatus() == Status.A_ANGELEGT_IN_PROZESSBAUKASTEN
                || anforderungData.getStatus() == Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN;
    }

    private Optional<Prozessbaukasten> getProzessbaukastenFromUrl(UrlParameter urlParameter) {

        String prozessbaukatenFachId = urlParameter.getValue("pzbkFachId").orElse("");
        String prozessbaukatenVersionString = urlParameter.getValue("pzbkVersion").orElse("");

        Integer prozessbaukastenVersion = 0;
        try {
            prozessbaukastenVersion = Integer.parseInt(prozessbaukatenVersionString);
        } catch (NumberFormatException numex) {
            LOG.warn("no valid Version for PZBK");
        }
        if (!"".equals(prozessbaukatenFachId) && prozessbaukastenVersion > 0) {
            return prozessbaukastenReadService.getByFachIdAndVersion(prozessbaukatenFachId, prozessbaukastenVersion);
        } else {
            return Optional.empty();
        }
    }

    private AnforderungOhneMeldungEditViewData getAnforderungOhneMeldungViewDataForAnforderung(Anforderung anforderung, String createStatus) {
        if (anforderung.getId() != null || isCreateNewVersion(createStatus)) {

            final Collection<ModulSeTeamId> configuredModulSeTeamsForAnforderung = anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getConfiguredModulSeTeamsForAnforderung(anforderung);

            return AnforderungOhneMeldungEditViewData.forAnforderung(anforderung, configuredModulSeTeamsForAnforderung);
        } else {
            return AnforderungOhneMeldungEditViewData.forNewAnforderung(anforderung, session.getUser());
        }
    }

    private AnforderungOhneMeldungEditWorkFields getWorkFields(Prozessbaukasten prozessbaukasten, Anforderung anforderung, String beschreibungDe,
                                                               String comment, String created) {

        Map<ModulSeTeam, Boolean> modulBerechtigungMapForAnforderung = getModulBerechtigungMapForAnforderung(anforderung);
        AnforderungOhneMeldungEditWorkFields workFields;
        if (anforderung.getAnfoFreigabeBeiModul() != null && anforderung.getId() != null && !anforderung.getFachId().equals("")) {
            workFields = new AnforderungOhneMeldungEditWorkFields(getAllFestgestelltIn(),
                    getTTeamsForUser(), beschreibungDe, anforderung.getAnfoFreigabeBeiModul(), modulBerechtigungMapForAnforderung);
        } else {
            workFields = new AnforderungOhneMeldungEditWorkFields(getAllFestgestelltIn(),
                    getTTeamsForUser(), beschreibungDe, new ArrayList<>(), modulBerechtigungMapForAnforderung);
            workFields.setNewAnforderung(true);
        }

        if (isCreateNewVersion(created)) {
            workFields.setNewAnforderung(false);
            workFields.setNewVersion(true);
        }

        workFields.setProzessbaukasten(prozessbaukasten);

        workFields.setVersionKommentar(comment);

        return workFields;
    }

    private Map<ModulSeTeam, Boolean> getModulBerechtigungMapForAnforderung(Anforderung anforderung) {
        Map<ModulSeTeam, Boolean> result = new HashMap<>();

        anforderung.getAnfoFreigabeBeiModul().stream()
                .map(AnforderungFreigabeBeiModulSeTeam::getModulSeTeam)
                .forEach(seTeam -> result.put(seTeam, isMyModulSeTeam(seTeam)));

        return result;
    }

    private SensorCocDialogViewData getSensorCocDialogViewData() {
        return new SensorCocDialogViewData(getAllSensorCocForUser());
    }

    private UmsetzerDialogViewData getUmsetzerDialogViewData(AnforderungOhneMeldungEditViewData anforderungData) {

        if (anforderungData.getUmsetzer() != null) {
            return new UmsetzerDialogViewData(getAllModule(), getAllSeTeams(), getAllKomponenten(),
                    anforderungData.getUmsetzer(), anforderungData.getDefaultVereinbarungType());
        } else {
            return new UmsetzerDialogViewData(getAllModule(), getAllSeTeams(), getAllKomponenten(),
                    new HashSet<>(), anforderungData.getDefaultVereinbarungType());
        }
    }

    private AnforderungOhneMeldungEditViewPermission getViewPermissionForAnforderung(Anforderung anforderung) {
        boolean berechtigt;
        if (anforderung.getId() != null) {
            berechtigt = anforderungService.isUserBerechtigtForAnforderung(anforderung.getId());
        } else {
            berechtigt = true;
        }
        Set<Rolle> rolesWithWritePermissions = getUserRolesWithWritePermissions(anforderung);

        return new AnforderungOhneMeldungEditViewPermission(berechtigt, anforderung.getStatus(), rolesWithWritePermissions);

    }

    protected Set<Rolle> getUserRolesWithWritePermissions(Anforderung anforderung) {
        if (anforderung.getSensorCoc() != null && anforderung.getTteam() != null
                && anforderung.getAnfoFreigabeBeiModul() != null) {
            Set<Rolle> rolesWithWritePermissions = session.getUserPermissions()
                    .getRolesWithWritePermissionsForAnforderung(anforderung.getSensorCoc(),
                            anforderung.getTteam().getId(),
                            anforderung.getAnfoFreigabeBeiModul().stream()
                                    .map(f -> f.getModulSeTeam().getId()).collect(Collectors.toSet())
                    );
            if (session.hasRole(Rolle.TTEAMMITGLIED)) {
                List<Long> tteamIdsForWrite = anforderungService.getTteamIdsforTteamMitgliedAndRechttype(session.getUser(), Rechttype.SCHREIBRECHT.getId());
                if (tteamIdsForWrite.contains(anforderung.getTteam().getId())) {
                    rolesWithWritePermissions.add(Rolle.TTEAMMITGLIED);
                }
            }
            return rolesWithWritePermissions;

        } else {
            return session.getUserPermissions().getRolesWithWritePermissionsForNewAnforderung();
        }

    }

    private Anforderung getAnforderungByUrlParameter(UrlParameter urlParameter) {

        String createStatus = urlParameter.getValue(CREATED).orElse("");
        String fachId = urlParameter.getValue("fachId").orElse("");
        String versionString = urlParameter.getValue("version").orElse("");
        String anforderungIdString = urlParameter.getValue("id").orElse("");

        if (!RegexUtils.matchesAnforderungFachId(fachId) || !RegexUtils.matchesVersion(versionString)) {
            return getAnforderungById(anforderungIdString, createStatus);
        } else {
            Integer version = parseAnforderungVersion(versionString);
            return getAnforderungByFachIdVersion(fachId, version, createStatus);
        }
    }

    private Anforderung getAnforderungById(String anforderungIdString, String createStatus) {
        if (RegexUtils.matchesId(anforderungIdString)) {
            Long longId = parseAnforderungId(anforderungIdString);
            if (isCreateNewVersion(createStatus)) {
                return getNewVersionForAnforderungById(longId);
            } else {
                return anforderungService.getAnforderungById(longId);
            }
        } else {
            return anforderungService.createNewAnforderung();
        }
    }

    private static boolean isCreateNewVersion(String createStatus) {
        return createStatus != null && "new".equals(createStatus);
    }

    private Anforderung getNewVersionForAnforderungById(Long id) {
        try {
            return anforderungService.createNewVersionForAnforderung(id);
        } catch (InvocationTargetException | IllegalAccessException ex) {
            LOG.error("getNewVersionForAnforderungId failed", ex);
            return null;
        }
    }

    private static Integer parseAnforderungVersion(String anforderungVersion) {
        try {
            return Integer.parseInt(anforderungVersion);
        } catch (NumberFormatException nfe) {
            LOG.warn("version is not valid. ", nfe);
            return 0;
        }
    }

    private static Long parseAnforderungId(String anforderungId) {
        try {
            return Long.parseLong(anforderungId);
        } catch (NumberFormatException nfe) {
            LOG.warn("ID is not valid. ", nfe);
            return null;
        }
    }

    private Anforderung getAnforderungByFachIdVersion(String fachId, Integer version, String createStatus) {
        if (isCreateNewVersion(createStatus)) {
            return getNewVersionForAnforderungByFachIdVersion(fachId, version);
        } else {
            return anforderungService.getAnforderungByFachIdVersion(fachId, version);
        }
    }

    private Anforderung getNewVersionForAnforderungByFachIdVersion(String fachId, Integer version) {
        try {
            return anforderungService.createNewVersionForAnforderung(fachId, version);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            LOG.error("getNewVersionForAnforderungByFachIdVersion failed", ex);
            return null;
        }
    }

    public void saveChangesToAnforderungAndResetFreigabe(AnforderungOhneMeldungEditViewData viewData, AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData) {
        Anforderung anforderung = viewData.getAnforderungCopy();
        anforderung.resetFreigabeBeiModulSeTeams();
        applyViewDataToAnforderung(anforderung, viewData);
        anforderungService.saveChangesToAnforderung(anforderung, viewData.getAnhangForBild(), anforderungEditFahrzeugmerkmalData);
        anforderungEditFahrzeugmerkmalSaveService.saveAnforderungFahrzeugmerkmalChanges(anforderung, anforderungEditFahrzeugmerkmalData);
    }

    public void saveChangesToAnforderung(AnforderungOhneMeldungEditViewData viewData, AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData) {
        Anforderung anforderung = viewData.getAnforderungCopy();
        applyViewDataToAnforderung(anforderung, viewData);
        anforderungService.saveChangesToAnforderung(anforderung, viewData.getAnhangForBild(), anforderungEditFahrzeugmerkmalData);
        anforderungEditFahrzeugmerkmalSaveService.saveAnforderungFahrzeugmerkmalChanges(anforderung, anforderungEditFahrzeugmerkmalData);
    }

    public String saveNewAnforderung(Prozessbaukasten prozessbaukasten, AnforderungOhneMeldungEditViewData viewData, String statusChangeKommentar, AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData) {
        Anforderung anforderung = viewData.getAnforderungCopy();
        applyViewDataToAnforderung(anforderung, viewData);
        anforderung.setErstellungsdatum(new Date());
        Mitarbeiter user = session.getUser();
        anforderung = anforderungService.saveNewAnforderungOhneMeldung(anforderung, statusChangeKommentar, user, viewData.getAnhangForBild());
        anforderungEditFahrzeugmerkmalSaveService.saveAnforderungFahrzeugmerkmalChanges(anforderung, anforderungEditFahrzeugmerkmalData);

        if (isProzessbaukastenValid(prozessbaukasten)) {
            prozessbaukastenZuordnen(prozessbaukasten, anforderung);
        }

        return PageUtils.getUrlForPageWithFachIdAndVersion(Page.ANFORDERUNGVIEW, anforderung.getFachId(), anforderung.getVersion());
    }

    private static boolean isProzessbaukastenValid(Prozessbaukasten prozessbaukasten) {
        return prozessbaukasten != null;
    }

    public void saveNewAnforderungVersion(AnforderungOhneMeldungEditViewData viewData, String statusChangeKommentar, AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData) {
        Anforderung anforderung = viewData.getAnforderungCopy();
        applyViewDataToAnforderung(anforderung, viewData);
        Mitarbeiter user = session.getUser();
        anforderung.getAnfoFreigabeBeiModul().forEach(freigabe -> freigabe.setId(null));
        anforderungService.saveNewAnforderungOhneMeldungVersion(anforderung, statusChangeKommentar, user, viewData.getAnhangForBild());
        anforderungEditFahrzeugmerkmalSaveService.saveAnforderungFahrzeugmerkmalChanges(anforderung, anforderungEditFahrzeugmerkmalData);
    }

    private Anforderung applyViewDataToAnforderung(Anforderung anforderung, AnforderungOhneMeldungEditViewData viewData) {
        applyFieldChangesToAnforderung(anforderung, viewData);
        applyBildChangesToAnforderung(anforderung, viewData.getBild());
        applyUmsetzerChangesToAnforderung(anforderung, viewData.getUmsetzer());
        applyFreigabeChangesToAnforderung(anforderung, viewData.getFreigaben());
        return anforderung;
    }

    private static Anforderung applyFieldChangesToAnforderung(Anforderung anforderung, AnforderungOhneMeldungEditViewData viewData) {
        anforderung.setBeschreibungAnforderungDe(viewData.getBeschreibungAnforderungDe());
        anforderung.setBeschreibungAnforderungEn(viewData.getBeschreibungAnforderungEn());
        anforderung.setKommentarAnforderung(viewData.getKommentarAnforderung());
        anforderung.setZielwert(viewData.getZielwert());
        anforderung.setSensorCoc(viewData.getSensorCoc());
        anforderung.setTteam(viewData.getTteam());
        anforderung.setPhasenbezug(viewData.isPhasenbezug());
        anforderung.setPotentialStandardisierung(viewData.isPotentialStandardisierung());
        anforderung.setFestgestelltIn(viewData.getFestgestelltIn());
        anforderung.setErsteller(viewData.getErsteller());
        return anforderung;
    }

    private Anforderung applyFreigabeChangesToAnforderung(Anforderung anforderung, List<AnforderungFreigabeDto> viewFreigabeDtos) {
        removeFreigaben(anforderung, viewFreigabeDtos);
        addNewFreigaben(anforderung, viewFreigabeDtos);
        changeExistingFreigabeen(anforderung, viewFreigabeDtos);
        return anforderung;
    }

    private static void changeExistingFreigabeen(Anforderung anforderung, List<AnforderungFreigabeDto> viewFreigabeDtos) {
        viewFreigabeDtos.stream()
                .filter(freigabeDto -> freigabeDto.getId() != null)
                .forEach(freigabeDto -> applyFreigabeDtoChangeToAnforderung(anforderung, freigabeDto));
    }

    private static void applyFreigabeDtoChangeToAnforderung(Anforderung anforderung, AnforderungFreigabeDto freigabeDto) {
        Optional<AnforderungFreigabeBeiModulSeTeam> freigabeFromAnforderungById = getFreigabeFromAnforderungById(anforderung, freigabeDto.getId());
        if (freigabeFromAnforderungById.isPresent()) {
            applyFreigabeDtoChangeToFreigabe(freigabeFromAnforderungById.get(), freigabeDto);
        }
    }

    private static void applyFreigabeDtoChangeToFreigabe(AnforderungFreigabeBeiModulSeTeam freigabe, AnforderungFreigabeDto freigabeDto) {
        freigabe.setVereinbarungType(freigabeDto.getVereinbarungType());
    }

    private static Optional<AnforderungFreigabeBeiModulSeTeam> getFreigabeFromAnforderungById(Anforderung anforderung, Long id) {
        return anforderung.getAnfoFreigabeBeiModul().stream().filter(freigabe -> freigabe.getId().equals(id)).findFirst();
    }

    private void addNewFreigaben(Anforderung anforderung, List<AnforderungFreigabeDto> viewFreigabeDtos) {
        Set<AnforderungFreigabeDto> freigabeDtosWithoutId = getFreigabeDtosWithoutId(viewFreigabeDtos);
        Set<AnforderungFreigabeBeiModulSeTeam> newFreigaben = generateNewFreigaben(anforderung, freigabeDtosWithoutId);
        newFreigaben.forEach(anforderung::addAnfoFreigabeBeiModul);
    }

    private void removeFreigaben(Anforderung anforderung, List<AnforderungFreigabeDto> viewFreigabeDtos) {
        Set<AnforderungFreigabeDto> freigabeDtosWithId = getFreigabeDtosWithId(viewFreigabeDtos);
        Set<AnforderungFreigabeBeiModulSeTeam> freigabenToRemove = getFreigabenToRemove(anforderung.getAnfoFreigabeBeiModul(), freigabeDtosWithId);
        freigabenToRemove.forEach(anforderung::removeAnfoFreigabeBeiModul);
    }

    private Anforderung applyUmsetzerChangesToAnforderung(Anforderung anforderung, Set<DialogUmsetzerDto> viewUmsetzerDtos) {
        removeUmsetzer(anforderung, viewUmsetzerDtos);
        addNewUmsetzer(anforderung, viewUmsetzerDtos);
        return anforderung;
    }

    private void removeUmsetzer(Anforderung anforderung, Set<DialogUmsetzerDto> viewUmsetzerDtos) {
        Set<DialogUmsetzerDto> umsetzerDtosWithId = getUmsetzerDtosWithId(viewUmsetzerDtos);
        Set<Umsetzer> umsetzerToRemove = getUmsetzerToRemove(anforderung.getUmsetzer(), umsetzerDtosWithId);
        umsetzerToRemove.forEach(anforderung::removeUmsetzer);
    }

    private void addNewUmsetzer(Anforderung anforderung, Set<DialogUmsetzerDto> viewUmsetzerDtos) {
        Set<DialogUmsetzerDto> umsetzerDtosWithoutId = getUmsetzerDtosWithoutId(viewUmsetzerDtos);
        Set<Umsetzer> newUmsetzer = generateNewUmsetzer(umsetzerDtosWithoutId);
        newUmsetzer.forEach(anforderung::addUmsetzer);
    }

    private static Set<Umsetzer> getUmsetzerToRemove(Collection<Umsetzer> currentUmsetzer, Collection<DialogUmsetzerDto> viewUmsetzerDtos) {
        Set<Long> remainingUmsetzerIds = viewUmsetzerDtos.stream().map(DialogUmsetzerDto::getUmsetzerId).collect(Collectors.toSet());
        return currentUmsetzer.stream().filter(umsetzer -> !remainingUmsetzerIds.contains(umsetzer.getId())).collect(Collectors.toSet());
    }

    private static Set<AnforderungFreigabeBeiModulSeTeam> getFreigabenToRemove(Collection<AnforderungFreigabeBeiModulSeTeam> currentFreigaben, Collection<AnforderungFreigabeDto> viewFreigabeDtos) {
        Set<Long> remainingFreigabeIds = getRemainingFreigabeIds(viewFreigabeDtos);
        return getFreigabenWithIdsNotInIdSet(currentFreigaben, remainingFreigabeIds);
    }

    private static Set<AnforderungFreigabeBeiModulSeTeam> getFreigabenWithIdsNotInIdSet(Collection<AnforderungFreigabeBeiModulSeTeam> freigaben, Set<Long> ids) {
        return freigaben.stream().filter(freigabe -> !ids.contains(freigabe.getId())).collect(Collectors.toSet());
    }

    private static Set<Long> getRemainingFreigabeIds(Collection<AnforderungFreigabeDto> anforderungFreigabeDtos) {
        return anforderungFreigabeDtos.stream().map(AnforderungFreigabeDto::getId).collect(Collectors.toSet());
    }

    private static Set<DialogUmsetzerDto> getUmsetzerDtosWithoutId(Collection<DialogUmsetzerDto> viewUmsetzerDtos) {
        return viewUmsetzerDtos.stream().filter(umsetzerDto -> umsetzerDto.getUmsetzerId() == null).collect(Collectors.toSet());
    }

    private static Set<DialogUmsetzerDto> getUmsetzerDtosWithId(Collection<DialogUmsetzerDto> viewUmsetzerDtos) {
        return viewUmsetzerDtos.stream().filter(umsetzerDto -> umsetzerDto.getUmsetzerId() != null).collect(Collectors.toSet());
    }

    private static Set<AnforderungFreigabeDto> getFreigabeDtosWithoutId(Collection<AnforderungFreigabeDto> viewFreigabeDtos) {
        return viewFreigabeDtos.stream().filter(freigabeDtos -> freigabeDtos.getId() == null).collect(Collectors.toSet());
    }

    private static Set<AnforderungFreigabeDto> getFreigabeDtosWithId(Collection<AnforderungFreigabeDto> viewFreigabeDtos) {
        return viewFreigabeDtos.stream().filter(freigabeDtos -> freigabeDtos.getId() != null).collect(Collectors.toSet());
    }

    private Set<Umsetzer> generateNewUmsetzer(Set<DialogUmsetzerDto> newUmsetzerDtos) {
        Set<Umsetzer> result = new HashSet<>();

        newUmsetzerDtos.stream()
                .filter(umsetzerDto -> umsetzerDto.getKomponenteId() != null)
                .map(umsetzerDto -> getNewUmsetzerForKomponenteId(umsetzerDto.getKomponenteId()))
                .filter(Objects::nonNull)
                .forEach(result::add);

        newUmsetzerDtos.stream()
                .filter(umsetzerDto -> umsetzerDto.getKomponenteId() == null)
                .filter(umsetzerDto -> umsetzerDto.getSeTeamId() != null)
                .map(umsetzerDto -> getNewUmsetzerForSeTeamId(umsetzerDto.getSeTeamId()))
                .filter(Objects::nonNull)
                .forEach(result::add);

        return result;
    }

    private Set<AnforderungFreigabeBeiModulSeTeam> generateNewFreigaben(Anforderung anforderung, Collection<AnforderungFreigabeDto> newFreigabeDtos) {
        Set<AnforderungFreigabeBeiModulSeTeam> result = new HashSet<>();

        newFreigabeDtos.stream()
                .map(freigabeDto -> getNewFreigabeForAnforderungIdSeTeamId(anforderung, freigabeDto))
                .filter(Objects::nonNull)
                .forEach(result::add);

        return result;
    }

    private AnforderungFreigabeBeiModulSeTeam getNewFreigabeForAnforderungIdSeTeamId(Anforderung anforderung, AnforderungFreigabeDto anforderungFreigabe) {
        ModulSeTeam seTeam = modulService.getSeTeamById(anforderungFreigabe.getSeTeamId());
        if (seTeam == null) {
            return null;
        }
        return new AnforderungFreigabeBeiModulSeTeam(anforderung, seTeam, anforderungFreigabe.getVereinbarungType());
    }

    private Umsetzer getNewUmsetzerForSeTeamId(Long seTeamId) {
        ModulSeTeam seTeam = modulService.getSeTeamById(seTeamId);

        if (seTeam == null) {
            return null;
        }

        return new Umsetzer(seTeam);
    }

    private Umsetzer getNewUmsetzerForKomponenteId(Long komponenteId) {
        ModulKomponente modulKomponenteById = modulService.getModulKomponenteById(komponenteId);

        if (modulKomponenteById == null) {
            return null;
        }

        ModulSeTeam seTeam = modulKomponenteById.getSeTeam();
        return new Umsetzer(seTeam, modulKomponenteById);
    }

    private static Anforderung applyBildChangesToAnforderung(Anforderung anforderung, Bild bild) {
        Bild existingBild = anforderung.getBild();

        if (existingBild == null || existingBild.getId() == null || (bild != null && !existingBild.getId().equals(bild.getId()))) {
            anforderung.setBild(bild);
        }

        return anforderung;
    }

    private List<FestgestelltIn> getAllFestgestelltIn() {
        return anforderungService.getAllFestgestelltIn();
    }

    private List<Tteam> getTTeamsForUser() {

        List<Tteam> result;

        if (session.hasRole(Rolle.TTEAMMITGLIED)) {
            UserPermissions<BerechtigungDto> userPermissions = session.getUserPermissions();
            List<Long> tteamIdsForTteamMitgliedSchreibend = userPermissions.getTteamIdsForTteamMitgliedSchreibend();
            result = tteamService.getTteamByIdList(tteamIdsForTteamMitgliedSchreibend);
        } else {
            result = tteamService.getAllTteams();
        }

        result.sort(new TteamComparator());
        return result;
    }

    public Optional<Mitarbeiter> getTteamleiterOfTteam(Tteam tteam) {
        return berechtigungService.getTteamleiterOfTteam(tteam);
    }

    private Boolean isMyModulSeTeam(ModulSeTeam modulSeTeam) {
        return berechtigungService.isMyModulSeTeam(modulSeTeam);
    }

    public StreamedContent downloadAnhang(Anhang anhang) {
        return imageService.downloadAnhang(anhang);
    }

    private List<SensorCoc> getAllSensorCocForUser() {
        return sensorCocService.getSensorCocsByRessortTechnologieOrtungByRole(null, null, null);
    }

    public Optional<Mitarbeiter> getSensorCoCLeiterForSensorCoc(SensorCoc sensorCoc) {
        return berechtigungService.getSensorCoCLeiterOfSensorCoc(sensorCoc);
    }

    private List<ModulDto> getAllModule() {
        return modulService.getAllModule().stream()
                .map(modul -> new ModulDto(modul.getId(), modul.getName(), getModulSeTeamDtoDtoListfromList(modul.getSeTeams())))
                .collect(Collectors.toList());
    }

    private List<ModulSeTeamDto> getModulSeTeamDtoDtoListfromList(Set<ModulSeTeam> modulSeTeams) {
        return modulSeTeams.stream().map(ModulSeTeamDto::forSeTeam).collect(Collectors.toList());
    }

    private List<ModulKomponenteDto> getModulKomponenteDtoListFromList(Set<ModulKomponente> modulKomponenten) {
        return modulKomponenten.stream().map(ModulKomponenteDto::forKomponente).collect(Collectors.toList());
    }

    private List<ModulSeTeamDto> getAllSeTeams() {
        return getModulSeTeamDtoDtoListfromList(new HashSet<>(modulService.getAllSeTeams()));
    }

    private List<ModulKomponenteDto> getAllKomponenten() {
        return getModulKomponenteDtoListFromList(new HashSet<>(modulService.getAllKomponenten()));

    }

    public String prozessbaukastenZuordnen(Prozessbaukasten prozessbaukasten, Anforderung anforderung) {
        return prozessbaukastenZuordnenService.prozessbaukastenZuordnenByProzessbaukatenAndAnforderung(prozessbaukasten, anforderung);
    }

    public AnforderungEditFahrzeugmerkmalDialogData getAnforderungEditFahrzeugmerkmalDialogData(AnforderungId anforderungId, ModulSeTeamId modulSeTeamId) {
        return anforderungEditFahrzeugmerkmalService.getDialogData(anforderungId, modulSeTeamId);
    }


    public void updateFahrzeugmerkmaleForFreigaben(List<AnforderungFreigabeDto> anforderungFreigabeDtoList, AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData, AnforderungId anforderungId) {
        anforderungFreigabeDtoList.forEach(anforderungFreigabeDto -> {
            AnforderungEditFahrzeugmerkmalDialogData anforderungEditFahrzeugmerkmalDialogData = getAnforderungEditFahrzeugmerkmalDialogData(anforderungId, anforderungFreigabeDto.getModulSeTeamId());
            anforderungEditFahrzeugmerkmalData.addDataForModulSeTeam(anforderungFreigabeDto.getModulSeTeamId(), anforderungEditFahrzeugmerkmalDialogData);
            if (anforderungEditFahrzeugmerkmalDialogData.getFahrzeugmerkmale().isEmpty()) {
                anforderungFreigabeDto.setFahrzeugmerkmalConfigured(Boolean.TRUE);
            }
        });
    }
}
