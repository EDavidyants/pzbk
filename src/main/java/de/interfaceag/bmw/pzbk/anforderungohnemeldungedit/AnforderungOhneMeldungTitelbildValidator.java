package de.interfaceag.bmw.pzbk.anforderungohnemeldungedit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungTitelbildValidator;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.Optional;

final class AnforderungOhneMeldungTitelbildValidator {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungOhneMeldungTitelbildValidator.class);

    private AnforderungOhneMeldungTitelbildValidator() {
    }

    static boolean validate(AnforderungOhneMeldungEditViewData viewData, AnforderungOhneMeldungEditWorkFields workFields) {
        Optional<Prozessbaukasten> prozessbaukastenOptional = workFields.getProzessbaukasten();
        if (prozessbaukastenOptional.isPresent()) {
            LOG.debug("Prozessbaukasten {} is selected. No Titelbild neccessary!", prozessbaukastenOptional);
            return true;
        } else {
            boolean isTitelbildValid = AnforderungTitelbildValidator.isValid(viewData, viewData.getAnhangForBild());
            if (!isTitelbildValid) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Es muss ein Anhang als Bild für die Anforderung ausgewählt werden.", ""));
                context.validationFailed();
            }
            return isTitelbildValid;
        }
    }
}
