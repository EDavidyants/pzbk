package de.interfaceag.bmw.pzbk.anforderungohnemeldungedit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.permission.AnforderungViewPermissionUtils;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.EditViewPermission;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.StatusPermission;
import de.interfaceag.bmw.pzbk.permissions.StatusToRole;
import de.interfaceag.bmw.pzbk.permissions.StatusToRolePermission;
import de.interfaceag.bmw.pzbk.permissions.TruePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

/**
 * @author fn
 */
public class AnforderungOhneMeldungEditViewPermission implements Serializable {

    private final ViewPermission page;
    private final EditPermission deleteSensorCoCButton;
    private final EditViewPermission tteamInput;
    private final EditPermission umsetzerInput;
    private final EditViewPermission potentialStandardisierungInput;
    private final EditViewPermission phasenbezugInput;
    private final EditPermission beschreibungDeInput;
    private final EditPermission beschreibungEnInput;
    private final EditPermission zielwertInput;
    private final EditPermission kommentarZielwertInput;
    private final EditPermission fahrzeugmerkmaleButton;

    public AnforderungOhneMeldungEditViewPermission() {
        this.page = FalsePermission.get();
        this.deleteSensorCoCButton = FalsePermission.get();
        this.tteamInput = FalsePermission.get();
        this.umsetzerInput = FalsePermission.get();
        this.potentialStandardisierungInput = FalsePermission.get();
        this.phasenbezugInput = FalsePermission.get();
        this.beschreibungDeInput = FalsePermission.get();
        this.beschreibungEnInput = FalsePermission.get();
        this.zielwertInput = FalsePermission.get();
        this.kommentarZielwertInput = FalsePermission.get();
        this.fahrzeugmerkmaleButton = FalsePermission.get();
    }

    public AnforderungOhneMeldungEditViewPermission(boolean pageBerechtigt,
            Status currentStatus, Set<Rolle> rolesWithWritePermission) {

        page = getPagePermission(pageBerechtigt);
        deleteSensorCoCButton = getDeleteSensorCocButtonPermission(currentStatus);
        tteamInput = getTteamInputPermission(currentStatus, rolesWithWritePermission);
        umsetzerInput = getUmsetzerInputPermission(currentStatus);
        potentialStandardisierungInput = getPotentialStandardisierungInputPermission(currentStatus);
        phasenbezugInput = getPhasenbezugInputPermission(currentStatus);
        beschreibungDeInput = getStatusFreigegebenDisabledPermission(currentStatus);
        beschreibungEnInput = getStatusFreigegebenDisabledPermission(currentStatus);
        zielwertInput = getStatusFreigegebenDisabledPermission(currentStatus);
        kommentarZielwertInput = getStatusFreigegebenDisabledPermission(currentStatus);
        fahrzeugmerkmaleButton = AnforderungViewPermissionUtils.buildPermissionForFahrzeugmerkmaleButton(rolesWithWritePermission, currentStatus);
    }

    private static EditPermission getStatusFreigegebenDisabledPermission(Status currentStatus) {
        return StatusPermission.builder()
                .authorizeRead(Status.A_FREIGEGEBEN)
                .authorizeWrite(Status.A_INARBEIT)
                .authorizeWrite(Status.A_FTABGESTIMMT)
                .authorizeWrite(Status.A_PLAUSIB)
                .authorizeWrite(Status.A_UNSTIMMIG)
                .authorizeWrite(Status.A_ANGELEGT_IN_PROZESSBAUKASTEN)
                .compareTo(currentStatus).get();
    }

    private static ViewPermission getPagePermission(boolean isBerechtigt) {
        if (isBerechtigt) {
            return TruePermission.get();
        } else {
            return FalsePermission.get();
        }
    }

    private static EditViewPermission getPhasenbezugInputPermission(Status currentStatus) {
        return StatusPermission.builder()
                .authorizeRead(Status.A_FREIGEGEBEN)
                .authorizeWrite(Status.A_INARBEIT)
                .authorizeWrite(Status.A_FTABGESTIMMT)
                .authorizeWrite(Status.A_PLAUSIB)
                .authorizeWrite(Status.A_UNSTIMMIG)
                .authorizeWrite(Status.A_ANGELEGT_IN_PROZESSBAUKASTEN)
                .compareTo(currentStatus).get();
    }

    private static EditViewPermission getPotentialStandardisierungInputPermission(Status currentStatus) {
        return StatusPermission.builder()
                .authorizeRead(Status.A_FREIGEGEBEN)
                .authorizeWrite(Status.A_FTABGESTIMMT)
                .authorizeWrite(Status.A_PLAUSIB)
                .authorizeWrite(Status.A_UNSTIMMIG)
                .authorizeWrite(Status.A_ANGELEGT_IN_PROZESSBAUKASTEN)
                .compareTo(currentStatus).get();
    }

    private static EditPermission getUmsetzerInputPermission(Status currentStatus) {
        return StatusPermission.builder()
                .authorizeWrite(Status.A_INARBEIT)
                .authorizeWrite(Status.A_FTABGESTIMMT)
                .authorizeWrite(Status.A_PLAUSIB)
                .authorizeWrite(Status.A_UNSTIMMIG)
                .authorizeWrite(Status.A_ANGELEGT_IN_PROZESSBAUKASTEN)
                .compareTo(currentStatus).get();
    }

    private static EditPermission getDeleteSensorCocButtonPermission(Status currentStatus) {
        return StatusPermission.builder()
                .authorizeWrite(Status.A_INARBEIT)
                .authorizeWrite(Status.A_FTABGESTIMMT)
                .authorizeWrite(Status.A_PLAUSIB)
                .authorizeWrite(Status.A_UNSTIMMIG)
                .authorizeWrite(Status.A_ANGELEGT_IN_PROZESSBAUKASTEN)
                .compareTo(currentStatus).get();
    }

    private static EditViewPermission getTteamInputPermission(Status currentStatus, Set<Rolle> rolesWithWritePermission) {
        StatusToRole statusAbgestimmt = StatusToRole.builder()
                .createMapFor(Status.A_FTABGESTIMMT)
                .addRole(Rolle.ADMIN).addRole(Rolle.T_TEAMLEITER).addRole(Rolle.TTEAM_VERTRETER).addRole(Rolle.TTEAMMITGLIED)
                .addRole(Rolle.SENSORCOCLEITER).addRole(Rolle.SCL_VERTRETER).get();

        StatusToRole statusInArbeit = StatusToRole.builder()
                .createMapFor(Status.A_INARBEIT)
                .addRole(Rolle.ADMIN).addRole(Rolle.T_TEAMLEITER).addRole(Rolle.TTEAM_VERTRETER).addRole(Rolle.TTEAMMITGLIED)
                .addRole(Rolle.SENSORCOCLEITER).addRole(Rolle.SCL_VERTRETER).get();

        StatusToRole statusPlausbilisiert = StatusToRole.builder()
                .createMapFor(Status.A_PLAUSIB)
                .addRole(Rolle.ADMIN).addRole(Rolle.T_TEAMLEITER).addRole(Rolle.TTEAM_VERTRETER).addRole(Rolle.TTEAMMITGLIED)
                .addRole(Rolle.SENSORCOCLEITER).addRole(Rolle.SCL_VERTRETER).get();

        StatusToRole statusUnstimmig = StatusToRole.builder()
                .createMapFor(Status.A_UNSTIMMIG)
                .addRole(Rolle.ADMIN).addRole(Rolle.T_TEAMLEITER).addRole(Rolle.TTEAM_VERTRETER).addRole(Rolle.TTEAMMITGLIED)
                .addRole(Rolle.SENSORCOCLEITER).addRole(Rolle.SCL_VERTRETER).get();

        StatusToRole statusFreigegeben = StatusToRole.builder()
                .createMapFor(Status.A_FREIGEGEBEN)
                .addRole(Rolle.ADMIN).addRole(Rolle.T_TEAMLEITER).addRole(Rolle.TTEAM_VERTRETER).addRole(Rolle.TTEAMMITGLIED)
                .addRole(Rolle.SENSORCOCLEITER).addRole(Rolle.SCL_VERTRETER).get();

        StatusToRole statusAngelegt = de.interfaceag.bmw.pzbk.permissions.StatusToRole.builder()
                .createMapFor(Status.A_ANGELEGT_IN_PROZESSBAUKASTEN)
                .addRole(Rolle.ADMIN).addRole(Rolle.T_TEAMLEITER).addRole(Rolle.TTEAM_VERTRETER).addRole(Rolle.TTEAMMITGLIED)
                .get();

        return StatusToRolePermission.builder()
                .authorizeWriteForStatusToRole(statusInArbeit)
                .authorizeWriteForStatusToRole(statusAbgestimmt)
                .authorizeWriteForStatusToRole(statusUnstimmig)
                .authorizeReadForStatusToRole(statusPlausbilisiert)
                .authorizeReadForStatusToRole(statusFreigegeben)
                .authorizeWriteForStatusToRole(statusAngelegt)
                .compareToRolesWithWritePermission(rolesWithWritePermission)
                .compareToStatus(currentStatus).get();
    }

    public static AnforderungOhneMeldungEditViewPermission forNullAnforderung() {
        return new AnforderungOhneMeldungEditViewPermission();
    }

    public boolean getPage() {
        return page.isRendered();
    }

    public boolean getDeleteSensorCoCButton() {
        return deleteSensorCoCButton.hasRightToEdit();
    }

    public EditViewPermission getTteamInput() {
        return tteamInput;
    }

    public boolean getUmsetzerInput() {
        return umsetzerInput.hasRightToEdit();
    }

    public EditViewPermission getPotentialStandardisierungInput() {
        return potentialStandardisierungInput;
    }

    public EditViewPermission getPhasenbezugInput() {
        return phasenbezugInput;
    }

    public boolean getBeschreibungDeInput() {
        return beschreibungDeInput.hasRightToEdit();
    }

    public boolean getBeschreibungEnInput() {
        return beschreibungEnInput.hasRightToEdit();
    }

    public boolean getZielwertInput() {
        return zielwertInput.hasRightToEdit();
    }

    public boolean getKommentarZielwertInput() {
        return kommentarZielwertInput.hasRightToEdit();
    }

    public boolean getFahrzeugmerkmaleButton() {
        return fahrzeugmerkmaleButton.hasRightToEdit();
    }

}
