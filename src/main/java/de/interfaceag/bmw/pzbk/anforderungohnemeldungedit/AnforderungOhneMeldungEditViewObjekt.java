package de.interfaceag.bmw.pzbk.anforderungohnemeldungedit;

import java.io.Serializable;
import java.util.Optional;

/**
 *
 * @author fn
 */
public final class AnforderungOhneMeldungEditViewObjekt implements Serializable {

    private final Optional<AnforderungOhneMeldungEditViewData> viewData;

    private final SensorCocDialogViewData sensorCocDialogViewData;

    private final UmsetzerDialogViewData umsetzerDialogViewData;

    private final AnforderungOhneMeldungEditWorkFields workFields;

    private final AnforderungOhneMeldungEditViewPermission viewPermission;

    private AnforderungOhneMeldungEditViewObjekt(Optional<AnforderungOhneMeldungEditViewData> viewData, SensorCocDialogViewData sensorCocDialogViewData, UmsetzerDialogViewData umsetzerDialogViewData, AnforderungOhneMeldungEditWorkFields workFields, AnforderungOhneMeldungEditViewPermission viewPermission) {
        this.viewData = viewData;
        this.sensorCocDialogViewData = sensorCocDialogViewData;
        this.umsetzerDialogViewData = umsetzerDialogViewData;
        this.workFields = workFields;
        this.viewPermission = viewPermission;
    }

    private AnforderungOhneMeldungEditViewObjekt(AnforderungOhneMeldungEditViewPermission anforderungOhneMeldungEditViewPermission) {
        this.viewData = Optional.empty();
        this.viewPermission = anforderungOhneMeldungEditViewPermission;
        this.sensorCocDialogViewData = null;
        this.umsetzerDialogViewData = null;
        this.workFields = null;
    }

    public static AnforderungOhneMeldungEditViewObjekt forNullAnforderung() {
        return new AnforderungOhneMeldungEditViewObjekt(AnforderungOhneMeldungEditViewPermission.forNullAnforderung());
    }

    public Optional<AnforderungOhneMeldungEditViewData> getViewData() {
        return viewData;
    }

    public AnforderungOhneMeldungEditViewPermission getViewPermission() {
        return viewPermission;
    }

    public SensorCocDialogViewData getSensorCocDialogViewData() {
        return sensorCocDialogViewData;
    }

    public UmsetzerDialogViewData getUmsetzerDialogViewData() {
        return umsetzerDialogViewData;
    }

    public AnforderungOhneMeldungEditWorkFields getWorkFields() {
        return workFields;
    }

    public static ViewObjectViewDataBuilder getBuilder() {
        return new Builder();
    }

    public static final class Builder implements ViewObjectViewDataBuilder, ViewObjectBuilder,
            ViewObjectSensorCocDialogViewDataBuilder, ViewObjectViewPermissionBuilder,
            ViewObjectWorkFieldsBuilder, ViewObjectUmsetzerDialogViewDataBuilder {

        private Optional<AnforderungOhneMeldungEditViewData> viewData;
        private SensorCocDialogViewData sensorCocDialogViewData;
        private UmsetzerDialogViewData umsetzerDialogViewData;
        private AnforderungOhneMeldungEditWorkFields workFields;
        private AnforderungOhneMeldungEditViewPermission viewPermission;

        private Builder() {
            viewData = Optional.empty();
        }

        @Override
        public ViewObjectViewPermissionBuilder withViewData(AnforderungOhneMeldungEditViewData viewData) {
            this.viewData = Optional.ofNullable(viewData);
            return this;
        }

        @Override
        public ViewObjectWorkFieldsBuilder withViewPermission(AnforderungOhneMeldungEditViewPermission viewPermission) {
            this.viewPermission = viewPermission;
            return this;
        }

        @Override
        public ViewObjectSensorCocDialogViewDataBuilder withWorkFields(AnforderungOhneMeldungEditWorkFields workFields) {
            this.workFields = workFields;
            return this;
        }

        @Override
        public ViewObjectUmsetzerDialogViewDataBuilder withSensorCocDialogViewData(SensorCocDialogViewData sensorCocDialogViewData) {
            this.sensorCocDialogViewData = sensorCocDialogViewData;
            return this;
        }

        public ViewObjectBuilder withUmsetzerDialogViewData(UmsetzerDialogViewData umsetzerDialogViewData) {
            this.umsetzerDialogViewData = umsetzerDialogViewData;
            return this;
        }

        @Override
        public AnforderungOhneMeldungEditViewObjekt build() {
            return new AnforderungOhneMeldungEditViewObjekt(viewData, sensorCocDialogViewData, umsetzerDialogViewData, workFields, viewPermission);
        }

    }

    public interface ViewObjectViewDataBuilder {

        ViewObjectViewPermissionBuilder withViewData(AnforderungOhneMeldungEditViewData viewData);
    }

    public interface ViewObjectViewPermissionBuilder {

        ViewObjectWorkFieldsBuilder withViewPermission(AnforderungOhneMeldungEditViewPermission viewPermission);
    }

    public interface ViewObjectWorkFieldsBuilder {

        ViewObjectSensorCocDialogViewDataBuilder withWorkFields(AnforderungOhneMeldungEditWorkFields workFields);
    }

    public interface ViewObjectSensorCocDialogViewDataBuilder {

        ViewObjectUmsetzerDialogViewDataBuilder withSensorCocDialogViewData(SensorCocDialogViewData sensorCocDialogViewData);
    }

    public interface ViewObjectUmsetzerDialogViewDataBuilder {

        ViewObjectBuilder withUmsetzerDialogViewData(UmsetzerDialogViewData umsetzerDialogViewData);
    }

    public interface ViewObjectBuilder {

        AnforderungOhneMeldungEditViewObjekt build();
    }

}
