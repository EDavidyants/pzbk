package de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten;

import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Sorting;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SingleValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.VonBisDateSearchFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
final class ProzessbaukastenFilterQueryBuilder {

    private ProzessbaukastenFilterQueryBuilder() {
    }

    static QueryPartDTO getTteamFilterQueryPart(IdSearchFilter filter) {
        QueryPartDTO result = new QueryPartDTO();
        if (filter.isActive()) {
            Collection<Long> ids = filter.getSelectedIdsAsSet();
            result.append(" AND p.tteam.id IN :tteamIds");
            result.put("tteamIds", ids);
        }
        return result;
    }

    static QueryPartDTO getProzessbaukastenFilterQueryPart(IdSearchFilter filter) {
        QueryPartDTO result = new QueryPartDTO();
        if (filter.isActive()) {
            Collection<Long> ids = filter.getSelectedIdsAsSet();
            result.append(" AND p.id IN :ids");
            result.put("ids", ids);
        }
        return result;
    }

    static QueryPartDTO getStatusFilterQueryPart(MultiValueEnumSearchFilter<ProzessbaukastenStatus> filter) {
        QueryPartDTO result = new QueryPartDTO();
        if (filter.isActive()) {
            Collection<ProzessbaukastenStatus> statusList = filter.getAsEnumList();
            Collection<Integer> ids = statusList.stream().map(status -> status.getId()).collect(Collectors.toList());
            result.append(" AND p.status IN :statusIds");
            result.put("statusIds", ids);
        }
        return result;
    }

    static QueryPartDTO getForSortingFilterQueryPart(SingleValueEnumSearchFilter<Sorting> filter) {
        StringBuilder sb = new StringBuilder();

        Sorting sorting = getSortingFromFilter(filter);

        switch (sorting) {
            case AENDERUNGSDATUMASC:
                sb.append(" ORDER BY p.aenderungsdatum");
                sb.append(" ASC");
                break;
            case AENDERUNGSDATUMDESC:
                sb.append(" ORDER BY p.aenderungsdatum");
                sb.append(" DESC");
                break;
            case NAMEASC:
                sb.append(" ORDER BY LENGTH(p.fachId)");
                sb.append(" ASC");
                sb.append(", p.fachId");
                sb.append(" ASC");
                break;
            case NAMEDESC:
            default:
                sb.append(" ORDER BY LENGTH(p.fachId)");
                sb.append(" DESC");
                sb.append(", p.fachId");
                sb.append(" DESC");
                break;
        }

        QueryPartDTO qp = new QueryPartDTO();
        qp.setSort(sb.toString());
        return qp;
    }

    private static Sorting getSortingFromFilter(SingleValueEnumSearchFilter<Sorting> filter) {
        Optional<Sorting> optionalSorting = filter.getAsEnum();
        Sorting sorting;
        if (optionalSorting.isPresent()) {
            sorting = optionalSorting.get();
        } else {
            sorting = Sorting.NAMEDESC;
        }
        return sorting;
    }

    static QueryPartDTO getErstellungsdatumFilterQueryPart(VonBisDateSearchFilter filter) {
        QueryPartDTO queryPart = new QueryPartDTO();
        if (filter.isActive()) {
            switch (filter.getCase()) {
                case VON:
                    queryPart.append(" AND p.erstellungsdatum >= :erstellungsdatumDateStart");
                    queryPart.put("erstellungsdatumDateStart", filter.getVon());
                    break;
                case BIS:
                    queryPart.append(" AND p.erstellungsdatum <= :erstellungsdatumDateEnd");
                    queryPart.put("erstellungsdatumDateEnd", filter.getBis());
                    break;
                case VONBIS:
                    queryPart.append(" AND p.erstellungsdatum >= :erstellungsdatumDateStart AND p.erstellungsdatum <= :erstellungsdatumDateEnd");
                    queryPart.put("erstellungsdatumDateStart", filter.getVon());
                    queryPart.put("erstellungsdatumDateEnd", filter.getBis());
                    break;
                case NONE:
                default:
                    break;
            }
        }
        return queryPart;
    }

    static QueryPartDTO getAenderungsdatumFilterQueryPart(VonBisDateSearchFilter filter) {
        QueryPartDTO queryPart = new QueryPartDTO();
        if (filter.isActive()) {
            switch (filter.getCase()) {
                case VON:
                    queryPart.append(" AND p.aenderungsdatum >= :aenderungsdatumDateStart");
                    queryPart.put("aenderungsdatumDateStart", filter.getVon());
                    break;
                case BIS:
                    queryPart.append(" AND p.aenderungsdatum <= :aenderungsdatumDateEnd");
                    queryPart.put("aenderungsdatumDateEnd", filter.getBis());
                    break;
                case VONBIS:
                    queryPart.append(" AND p.aenderungsdatum >= :aenderungsdatumDateStart AND p.aenderungsdatum <= :aenderungsdatumDateEnd");
                    queryPart.put("aenderungsdatumDateStart", filter.getVon());
                    queryPart.put("aenderungsdatumDateEnd", filter.getBis());
                    break;
                case NONE:
                default:
                    break;
            }
        }
        return queryPart;
    }

    static QueryPartDTO getDatumStatusaenderungFilterQueryPart(VonBisDateSearchFilter filter) {
        QueryPartDTO queryPart = new QueryPartDTO();
        if (filter.isActive()) {
            switch (filter.getCase()) {
                case VON:
                    queryPart.append(" AND p.datumStatuswechsel >= :datumStatusaenderungStart");
                    queryPart.put("datumStatusaenderungStart", filter.getVon());
                    break;
                case BIS:
                    queryPart.append(" AND p.datumStatuswechsel <= :datumStatusaenderungEnd");
                    queryPart.put("datumStatusaenderungEnd", filter.getBis());
                    break;
                case VONBIS:
                    queryPart.append(" AND p.datumStatuswechsel >= :datumStatusaenderungStart"
                            + " AND p.datumStatuswechsel <= :datumStatusaenderungEnd");
                    queryPart.put("datumStatusaenderungStart", filter.getVon());
                    queryPart.put("datumStatusaenderungEnd", filter.getBis());
                    break;
                case NONE:
                default:
                    break;
            }
        }
        return queryPart;
    }

}
