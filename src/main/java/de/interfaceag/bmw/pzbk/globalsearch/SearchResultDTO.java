package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.enums.Type;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sl
 */
public final class SearchResultDTO implements Serializable, AutocompleteSearchResult {

    private Long id;
    private String fachId;
    private final String name;
    private String typ;
    private String beschreibung;
    private String beschreibungStaerkeSchwaeche;
    private String status;
    private List<String> module;

    private String sensorCoc;
    private String version;

    private boolean selected;

    byte[] image;

    private Long numberOfMeldungen = -1L;

    public SearchResultDTO(String name) {
        this.name = name;
        this.selected = false;
    }

    private SearchResultDTO(Long id, String name, String typ, String beschreibung, String status, String version) {
        this.id = id;
        this.name = name;
        this.typ = typ;
        this.beschreibung = beschreibung;
        this.status = status;
        this.version = version;
        this.selected = false;
    }

    public SearchResultDTO(Long id, String name, String typ, String beschreibung, String status, String beschreibungStaerkeSchwaeche, String sensorCoc) {
        this.id = id;
        this.name = name;
        this.typ = typ;
        this.beschreibung = beschreibung;
        this.beschreibungStaerkeSchwaeche = beschreibungStaerkeSchwaeche;
        this.status = status;
        this.sensorCoc = sensorCoc;
        this.selected = false;
    }

    public SearchResultDTO(Long id, String name, String typ, String beschreibung, String status, String beschreibungStaerkeSchwaeche, String sensorCoc, byte[] image) {
        this.id = id;
        this.name = name;
        this.typ = typ;
        this.beschreibung = beschreibung;
        this.beschreibungStaerkeSchwaeche = beschreibungStaerkeSchwaeche;
        this.status = status;
        this.sensorCoc = sensorCoc;
        this.selected = false;
        this.image = image != null ? image.clone() : null;
    }

    public SearchResultDTO(Long id, String name, String typ, String beschreibung, String status, String beschreibungStaerkeSchwaeche, String sensorCoc, String version) {
        this.id = id;
        this.name = name;
        this.typ = typ;
        this.beschreibung = beschreibung;
        this.beschreibungStaerkeSchwaeche = beschreibungStaerkeSchwaeche;
        this.status = status;
        this.sensorCoc = sensorCoc;
        this.version = version;
        this.selected = false;
    }

    public SearchResultDTO(Long id, String name, String typ, String beschreibung, String status, String beschreibungStaerkeSchwaeche, String sensorCoc, String version, byte[] image, Long numberOfMeldungen) {
        this.id = id;
        this.name = name;
        this.typ = typ;
        this.beschreibung = beschreibung;
        this.beschreibungStaerkeSchwaeche = beschreibungStaerkeSchwaeche;
        this.status = status;
        this.sensorCoc = sensorCoc;
        this.version = version;
        this.selected = false;
        this.image = image != null ? image.clone() : null;
        this.numberOfMeldungen = numberOfMeldungen;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    @Override
    public String getFachId() {
        return fachId;
    }

    public void setFachId(String fachId) {
        this.fachId = fachId;
    }

    @Override
    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getBeschreibungStaerkeSchwaeche() {
        return beschreibungStaerkeSchwaeche;
    }

    public String getStatus() {
        return status;
    }

    public String getSensorCoc() {
        return sensorCoc;
    }

    private void setSensorCoc(String sensorCoc) {
        this.sensorCoc = sensorCoc;
    }

    @Override
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<String> getModule() {
        return module;
    }

    public void setModule(List<String> module) {
        this.module = module;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public byte[] getImage() {
        return image != null ? image.clone() : null;
    }

    public void setImage(byte[] image) {
        this.image = image != null ? image.clone() : null;
    }

    public StreamedContent getImageAsStreamedContent() {
        if (image != null) {
            return new DefaultStreamedContent(new ByteArrayInputStream(image), "image/jpeg");
        }
        return new DefaultStreamedContent();
    }

    public String getUrl() {
        switch (this.getTyp()) {
            case "Anforderung":
                return "anforderungView";
            case "Meldung":
                return "meldungView";
            case "Prozessbaukasten":
                return "prozessbaukastenView";
            default:
                return "";
        }
    }

    public String getDisplayName() {
        StringBuilder sb = new StringBuilder(name);
        if (version != null) {
            sb.append(" | ").append(version);
        }
        return sb.toString();
    }

    public static Builder forProzessbaukasten(String fachId, Integer version) {
        return new Builder().withTyp(Type.PROZESSBAUKASTEN.getBezeichnung())
                .withFachId(fachId).withVersion(version);
    }

    public static final class Builder {

        private Long id;
        private String fachId;
        private String name;
        private String typ;
        private String beschreibung;
        private String status;
        private String version;
        private List<String> module;
        private String sensorCoc;

        private Builder() {
            module = new ArrayList<>();
        }

        public Builder withTyp(String typ) {
            this.typ = typ;
            return this;
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withFachId(String fachId) {
            this.fachId = fachId;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withVersion(Integer version) {
            this.version = version.toString();
            return this;
        }

        public Builder withBeschreibung(String beschreibung) {
            this.beschreibung = beschreibung;
            return this;
        }

        public Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public Builder withModul(String modul) {
            if (modul != null) {
                this.module.add(modul);
            }
            return this;
        }

        public Builder withSensorCoc(String sensorCoc) {
            this.sensorCoc = sensorCoc;
            return this;
        }

        public SearchResultDTO get() {
            SearchResultDTO result = new SearchResultDTO(id, name, typ, beschreibung, status, version);
            result.setFachId(fachId);
            result.setModule(module);
            result.setSensorCoc(sensorCoc);
            return result;
        }

    }

}
