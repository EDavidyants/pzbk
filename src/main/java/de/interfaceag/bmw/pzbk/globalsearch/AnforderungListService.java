package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.entities.AbstractAnforderung;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.UrlEncoding;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.UrlEncodingService;
import de.interfaceag.bmw.pzbk.session.Session;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl
 */
@Stateless
@Named
public class AnforderungListService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungListService.class);

    @Inject
    private AnforderungService anforderungService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private ConfigService configService;
    @Inject
    private ModulService modulService;
    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private UrlEncodingService urlEncodingService;
    @Inject
    private LocalizationService localizationService;
    @Inject
    private Session session;
    @Inject
    private ProzessbaukastenService prozessbaukastenService;

    public List<Modul> getAllModule() {
        return modulService.getAllModule();
    }

    public List<ModulSeTeam> getAllSeTeams() {
        return modulService.getAllSeTeamsFetch();
    }

    public List<FestgestelltIn> getAllFestgestelltIn() {
        return anforderungService.getAllFestgestelltIn();
    }

    public List<SensorCoc> initSensorCocList() {
        if (session.hasRole(Rolle.ADMIN)
                || session.hasRole(Rolle.T_TEAMLEITER) || session.hasRole(Rolle.TTEAM_VERTRETER) || session.hasRole(Rolle.ANFORDERER)) {
            return sensorCocService.getAllSortByTechnologie();
        }
        return berechtigungService.getAuthorizedSensorCocObjectListForMitarbeiter(session.getUser());
    }

    public List<Derivat> initDerivatList() {
        return berechtigungService.getDerivateForCurrentUser();
    }

    private List<Anforderung> loadExcelExportAnforderungDataForSelectedSearchResult(
            List<SearchResultDTO> selectedSearchResult) {

        List<Long> anforderungIds = selectedSearchResult.stream()
                .filter(sr -> sr.getTyp().equals("Anforderung")).map(sr -> sr.getId())
                .collect(Collectors.toList());

        List<Anforderung> anforderungen = anforderungService.getAnforderungenByIdList(anforderungIds);

        return anforderungen;
    }

    private List<Meldung> loadExcelExportMeldungDataForSelectedSearchResult(List<SearchResultDTO> selectedSearchResult) {

        List<Long> meldungIds = selectedSearchResult.stream()
                .filter(sr -> sr.getTyp().equals("Meldung")).map(sr -> sr.getId())
                .collect(Collectors.toList());

        List<Meldung> meldungen = anforderungService.getMeldungenByIdList(meldungIds);

        return meldungen;
    }

    public Sheet createExcelExportForAnforderungListView(List<SearchResultDTO> selectedSearchResult, Sheet sheet) {
        sheet = createHeaderRow(sheet);
        List<Anforderung> anforderungResult = loadExcelExportAnforderungDataForSelectedSearchResult(selectedSearchResult);
        List<Meldung> meldungResult = loadExcelExportMeldungDataForSelectedSearchResult(selectedSearchResult);
        return createRowsForAnforderungenAndMeldungen(anforderungResult, meldungResult, sheet);
    }

    private Sheet createRowsForAnforderungenAndMeldungen(List<Anforderung> anforderungen,
            List<Meldung> meldungen, Sheet sheet) {
        int rownumber = 1;
        for (Anforderung anforderung : anforderungen) {
            Row row = sheet.createRow(rownumber++);
            createRowForAnforderung(anforderung, row);
        }
        for (Meldung meldung : meldungen) {
            Row row = sheet.createRow(rownumber++);
            createRowForMeldung(meldung, row);
        }
        return sheet;
    }

    private Sheet createHeaderRow(Sheet sheet) {

        Locale currentLocale = localizationService.getCurrentLocale();

        Row headerRow = sheet.createRow(0);
        headerRow.createCell(0).setCellValue(LocalizationService.getValue(currentLocale, "id"));
        headerRow.createCell(1).setCellValue(LocalizationService.getValue(currentLocale, "status"));
        headerRow.createCell(2).setCellValue(LocalizationService.getValue(currentLocale, "sensor"));
        headerRow.createCell(3).setCellValue(LocalizationService.getValue(currentLocale, "festgestelltIn"));
        headerRow.createCell(4).setCellValue(LocalizationService.getValue(currentLocale, "staerkeSchwaeche"));
        headerRow.createCell(5).setCellValue(LocalizationService.getValue(currentLocale, "beschreibungStaerkeSchwaeche"));
        headerRow.createCell(6).setCellValue(LocalizationService.getValue(currentLocale, "sensorCoc"));
        headerRow.createCell(7).setCellValue(LocalizationService.getValue(currentLocale, "tteam"));
        headerRow.createCell(8).setCellValue(LocalizationService.getValue(currentLocale, "beschreibung"));
        headerRow.createCell(9).setCellValue(LocalizationService.getValue(currentLocale, "erstellungsdatum"));
        headerRow.createCell(10).setCellValue(LocalizationService.getValue(currentLocale, "aenderungsdatum"));
        headerRow.createCell(11).setCellValue(LocalizationService.getValue(currentLocale, "zeitpunktStatusaenderung"));
        headerRow.createCell(12).setCellValue(LocalizationService.getValue(currentLocale, "referenz"));
        headerRow.createCell(13).setCellValue(LocalizationService.getValue(currentLocale, "zielwert"));
        headerRow.createCell(14).setCellValue(LocalizationService.getValue(currentLocale, "loesungsvorschlag"));
        headerRow.createCell(15).setCellValue(LocalizationService.getValue(currentLocale, "auswirkung"));
        headerRow.createCell(16).setCellValue(LocalizationService.getValue(currentLocale, "kommentar"));
        headerRow.createCell(17).setCellValue(LocalizationService.getValue(currentLocale, "zugeordneteMeldungen"));
        headerRow.createCell(18).setCellValue(LocalizationService.getValue(currentLocale, "module"));
        headerRow.createCell(19).setCellValue(LocalizationService.getValue(currentLocale, "werk"));
        headerRow.createCell(20).setCellValue(LocalizationService.getValue(currentLocale, "anforderungohnemeldungview_ersteller"));
        headerRow.createCell(21).setCellValue(LocalizationService.getValue(currentLocale, "anforderung_referenzSystemLinks"));
        headerRow.createCell(22).setCellValue(LocalizationService.getValue(currentLocale, "anforderung_kategorie"));
        return sheet;
    }

    public Row createRowForAnforderung(Anforderung anforderung, Row row) {
        createRowForAbstractAnforderung(anforderung, row);
        if (anforderung != null) {
            if (anforderung.getErsteller() != null) {
                row.createCell(20).setCellValue(anforderung.getErsteller());
            }
            if (anforderung.getReferenzSystemLinks() != null) {
                String referenzSystemLinksAsString = anforderung.getReferenzSystemLinks().stream()
                        .map(referenzSystemLink -> referenzSystemLink.getReferenzsystem().toString())
                        .collect(Collectors.joining(", "));
                row.createCell(21).setCellValue(referenzSystemLinksAsString);
            }
            if (anforderung.getKategorie() != null) {
                row.createCell(22).setCellValue(anforderung.getKategorie().toString());
            }
        }
        return row;
    }

    public Row createRowForMeldung(Meldung meldung, Row row) {
        return createRowForAbstractAnforderung(meldung, row);
    }

    public Row createRowForAbstractAnforderung(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung != null) {
            createFachIdCell(abstractAnforderung, row);
            row.createCell(1).setCellValue(abstractAnforderung.getStatus().toString());
            createSensorCell(abstractAnforderung, row);
            row.createCell(3).setCellValue(abstractAnforderung.getFestgestelltIn().stream().map(fi -> fi.getWert()).collect(Collectors.joining(", ")));
            row.createCell(4).setCellValue(abstractAnforderung.isStaerkeSchwaeche() ? "Stärke" : "Schwäche");
            row.createCell(5).setCellValue(abstractAnforderung.getBeschreibungStaerkeSchwaeche());
            createSensorCocCell(abstractAnforderung, row);
            createTteamCell(abstractAnforderung, row);
            row.createCell(8).setCellValue(abstractAnforderung.getBeschreibungAnforderungDe());
            createErstellungsdatumCell(abstractAnforderung, row);
            createAenderungsdatumCell(abstractAnforderung, row);
            createZeipunktStatusaenderungCell(abstractAnforderung, row);
            createReferenzenCell(abstractAnforderung, row);
            createZielwertCell(abstractAnforderung, row);
            createLoesungsvorschlagCell(abstractAnforderung, row);
            createAuswirkungenCell(abstractAnforderung, row);
            createKommentarAnforderungCell(abstractAnforderung, row);

            createUmsetzerCell(abstractAnforderung, row);
            createWerkCell(abstractAnforderung, row);
        }
        return row;
    }

    private void createFachIdCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung instanceof Anforderung) {
            row.createCell(0).setCellValue(abstractAnforderung.getFachId() + " | V" + ((Anforderung) abstractAnforderung).getVersion().toString());
            Set<Meldung> meldungenList = ((Anforderung) abstractAnforderung).getMeldungen();
            if (meldungenList != null && !meldungenList.isEmpty()) {
                row.createCell(17).setCellValue(meldungenList.stream().map(m -> m.getFachId()).collect(Collectors.joining(", ")));
            }

        } else {
            row.createCell(0).setCellValue(abstractAnforderung.getFachId());
        }
    }

    private void createWerkCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung.getWerk() != null) {
            row.createCell(19).setCellValue(abstractAnforderung.getWerk().getName());
        }
    }

    private void createUmsetzerCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung instanceof Anforderung) {
            Anforderung a = (Anforderung) abstractAnforderung;
            row.createCell(18).setCellValue(a.getUmsetzer().stream()
                    .map(u -> u.toString())
                    .collect(Collectors.joining(", ")));
        }
    }

    private void createKommentarAnforderungCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung.getKommentarAnforderung() != null) {
            row.createCell(16).setCellValue(abstractAnforderung.getKommentarAnforderung());
        }
    }

    private void createAuswirkungenCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung.getAuswirkungen() != null && !abstractAnforderung.getAuswirkungen().isEmpty()) {
            row.createCell(15).setCellValue(abstractAnforderung.getAuswirkungen().stream().map(ausw -> ausw.toString()).collect(Collectors.joining(", ")));
        }
    }

    private void createLoesungsvorschlagCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung.getLoesungsvorschlag() != null) {
            row.createCell(14).setCellValue(abstractAnforderung.getLoesungsvorschlag());
        }
    }

    private void createZielwertCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung.getZielwert() != null) {
            row.createCell(13).setCellValue(abstractAnforderung.getZielwert().toString());
        }
    }

    private void createReferenzenCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung.getReferenzen() != null) {
            row.createCell(12).setCellValue(abstractAnforderung.getReferenzen());
        }
    }

    private void createZeipunktStatusaenderungCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung.getZeitpunktStatusaenderung() != null) {
            row.createCell(11).setCellValue(new SimpleDateFormat("yyyy-MM-dd").format(abstractAnforderung.getZeitpunktStatusaenderung()));
        }
    }

    private void createAenderungsdatumCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung.getAenderungsdatum() != null) {
            row.createCell(10).setCellValue(new SimpleDateFormat("yyyy-MM-dd").format(abstractAnforderung.getAenderungsdatum()));
        }
    }

    private void createErstellungsdatumCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung.getErstellungsdatum() != null) {
            row.createCell(9).setCellValue(new SimpleDateFormat("yyyy-MM-dd").format(abstractAnforderung.getErstellungsdatum()));
        }
    }

    private void createTteamCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung instanceof Anforderung && ((Anforderung) abstractAnforderung).getTteam() != null) {
            row.createCell(7).setCellValue(((Anforderung) abstractAnforderung).getTteam().toString());
        }
    }

    private void createSensorCocCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung.getSensorCoc() != null) {
            row.createCell(6).setCellValue(abstractAnforderung.getSensorCoc().toString());
        }
    }

    private void createSensorCell(AbstractAnforderung abstractAnforderung, Row row) {
        if (abstractAnforderung.getSensor() != null) {
            row.createCell(2).setCellValue(abstractAnforderung.getSensor().toString());
        }
    }

    public String updateUrl(List<SearchResultDTO> selectedSearchResult) {
        String baseUrl = configService.getUrlPrefix() + "/";

        if (selectedSearchResult != null && !selectedSearchResult.isEmpty()) {
            String urlParameter = createUrlParamterForSelectedSearchResults(selectedSearchResult);
            UrlEncoding urlEncoding = urlEncodingService.createNewUrlEncodingForPageAndParameter(Page.SUCHE, urlParameter);
            return baseUrl + urlEncoding.getUrl();
        }
        return baseUrl + Page.SUCHE.getUrl();
    }

    private static String createUrlParamterForSelectedSearchResults(List<SearchResultDTO> selectedSearchResult) {
        if (selectedSearchResult != null && !selectedSearchResult.isEmpty()) {
            StringBuilder sb = new StringBuilder("?query=");
            selectedSearchResult.forEach(sr -> {
                sb.append(sr.getName());
                sb.append(",");
            });

            String urlParameterString = sb.toString();
            if (urlParameterString.contains(",")) {
                urlParameterString = urlParameterString.substring(0, urlParameterString.lastIndexOf(","));
            }

            return urlParameterString;
        }

        LOG.debug("AnforderungListService: createUrlParamterForSelectedSearchResults returned empty search result");
        return null;
    }

}
