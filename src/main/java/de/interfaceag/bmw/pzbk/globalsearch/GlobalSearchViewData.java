package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.entities.UserDefinedSearch;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class GlobalSearchViewData implements Serializable {

    private final GlobalSearchFilter searchFilter;

    private final GlobalSearchViewPermission viewPermission;

    private final GlobalSearchResult searchResult;

    private boolean allSelected;

    //user defined search list of user
    private List<UserDefinedSearch> userDefinedSearches;
    //new user defined search
    private UserDefinedSearch newUserDefinedSearch = new UserDefinedSearch();

    // list of active filters for display at the frontend
    //url for search results
    private String url;
    // sortierung
    private String sortBy = SearchFilterType.SORT_NAME_DESC.getBezeichnung();
    // anzahl treffer
    private final Integer anzahlTreffer = 25;

    public GlobalSearchViewData(GlobalSearchFilter searchFilter,
            GlobalSearchViewPermission viewPermission,
            GlobalSearchResult searchResult,
            List<UserDefinedSearch> userDefinedSearches) {
        this.searchResult = searchResult;
        this.searchFilter = searchFilter;
        this.viewPermission = viewPermission;
        this.userDefinedSearches = userDefinedSearches;
        this.allSelected = true;
    }

    // -------------- methods --------------------------------------------------
    public void updateSelectedSearchResult() {
        if (allSelected) {
            markAsSelectedSearchResults(getSearchResults());
        } else if (!allSelected) {
            setSelectedSearchResults(new ArrayList<>());
            markAsNotSelectedSearchResults(getSearchResults());
        }
    }

    private void markAsSelectedSearchResults(Collection<SearchResultDTO> searchResults) {
        searchResults.forEach(sDTO -> {
            sDTO.setSelected(true);
        });
    }

    private void markAsNotSelectedSearchResults(Collection<SearchResultDTO> searchResults) {
        searchResults.forEach(sDTO -> {
            sDTO.setSelected(false);
        });
    }

    public void updateSelectedSearchResult(SearchResultDTO searchResult) {
        if (getSelectedSearchResults().contains(searchResult)) {
            getSelectedSearchResults().remove(searchResult);
            allSelected = false;
        } else {
            getSelectedSearchResults().add(searchResult);
            allSelected = Integer.toUnsignedLong(getSelectedSearchResults().size()) == getNumberOfResults();
        }
    }

    // -------------- getter / setter ------------------------------------------
    public List<SearchResultDTO> getSearchResults() {
        return searchResult.getSearchResults();
    }

    public List<SearchResultDTO> getSelectedSearchResults() {
        return searchResult.getSelectedSearchResults();
    }

    public void setSelectedSearchResults(List<SearchResultDTO> selectedSearchResult) {
        this.searchResult.setSelectedSearchResults(selectedSearchResult);
    }

    public boolean isAllSelected() {
        return allSelected;
    }

    public void setAllSelected(boolean allSelected) {
        this.allSelected = allSelected;
    }

    public List<UserDefinedSearch> getUserDefinedSearches() {
        return userDefinedSearches;
    }

    public void setUserDefinedSearches(List<UserDefinedSearch> userDefinedSearches) {
        this.userDefinedSearches = userDefinedSearches;
    }

    public UserDefinedSearch getNewUserDefinedSearch() {
        return newUserDefinedSearch;
    }

    public void setNewUserDefinedSearch(UserDefinedSearch newUserDefinedSearch) {
        this.newUserDefinedSearch = newUserDefinedSearch;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public Integer getAnzahlTreffer() {
        return anzahlTreffer;
    }

    public long getNumberOfResults() {
        return searchResult.getCount();
    }

    public GlobalSearchFilter getSearchFilter() {
        return searchFilter;
    }

    public GlobalSearchViewPermission getViewPermission() {
        return viewPermission;
    }

}
