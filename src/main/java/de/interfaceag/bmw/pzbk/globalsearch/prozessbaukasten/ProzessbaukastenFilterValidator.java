package de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten;

import de.interfaceag.bmw.pzbk.globalsearch.GlobalSearchFilter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
final class ProzessbaukastenFilterValidator {

    private ProzessbaukastenFilterValidator() {
    }

    @SuppressWarnings("checkstyle:BooleanExpressionComplexity")
    static boolean isInvalidFilterActive(GlobalSearchFilter filter) {
        return filter.getReferenzSystemFilter().isActive()
                || filter.getKategorieFilter().isActive()
                || filter.getMeldungStatusFilter().isActive()
                || filter.getAnforderungStatusFilter().isActive()
                || filter.getMeldungIdFilter().isActive()
                || filter.getAnforderungIdFilter().isActive()
                || filter.getSensorCocFilter().isActive()
                || filter.getDerivatFilter().isActive()
                || filter.getPhasenrelevanzFilter().isActive()
                || filter.getFestgestelltInFilter().isActive()
                || filter.getModulSeTeamFilter().isActive()
                || filter.getDashboardFilter().isActive()
                || filter.getThemenklammerFilter().isActive()
                || filter.getModulFilter().isActive();
    }

}
