package de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten;

import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
final class ProzessbaukastenBaseQueryBuilder {

    private ProzessbaukastenBaseQueryBuilder() {
    }

    static QueryPartDTO getAutocompleteBaseQuery() {
        return new QueryPartDTO("SELECT DISTINCT p.id, p.fachId, "
                + "p.version, p.bezeichnung, p.beschreibung, p.status FROM "
                + "Prozessbaukasten p "
                + "WHERE 1=1 ");
    }

    static QueryPartDTO getSearchBaseQuery() {
        return new QueryPartDTO("SELECT DISTINCT p.id, p.fachId, "
                + "p.version, p.bezeichnung, p.beschreibung, p.status, p.tteam.teamName FROM "
                + "Prozessbaukasten p "
                + "WHERE 1=1 ");
    }

    static QueryPartDTO getCountBaseQuery() {
        return new QueryPartDTO("SELECT COUNT(DISTINCT p.id) FROM "
                + "Prozessbaukasten p "
                + "WHERE 1=1 ");
    }

}
