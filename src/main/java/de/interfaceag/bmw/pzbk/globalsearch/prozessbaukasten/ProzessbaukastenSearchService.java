package de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.globalsearch.AutocompleteSearchResult;
import de.interfaceag.bmw.pzbk.globalsearch.GlobalSearchFilter;
import de.interfaceag.bmw.pzbk.globalsearch.SearchResultDTO;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.services.ImageService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import static de.interfaceag.bmw.pzbk.globalsearch.SearchResultDtoConverter.parseProzessbaukastenAutocompleteResult;
import static de.interfaceag.bmw.pzbk.globalsearch.SearchResultDtoConverter.parseProzessbaukastenSearchResult;
import static de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenBaseQueryBuilder.getAutocompleteBaseQuery;
import static de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenBaseQueryBuilder.getCountBaseQuery;
import static de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenBaseQueryBuilder.getSearchBaseQuery;
import static de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenFilterQueryBuilder.getAenderungsdatumFilterQueryPart;
import static de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenFilterQueryBuilder.getDatumStatusaenderungFilterQueryPart;
import static de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenFilterQueryBuilder.getErstellungsdatumFilterQueryPart;
import static de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenFilterQueryBuilder.getForSortingFilterQueryPart;
import static de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenFilterQueryBuilder.getProzessbaukastenFilterQueryPart;
import static de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenFilterQueryBuilder.getStatusFilterQueryPart;
import static de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenFilterQueryBuilder.getTteamFilterQueryPart;
import static de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenPermissionQueryBuilder.getPermissionQueryPart;
import static de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenTextQueryBuilder.getAutocompleteQueryPart;
import static de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenTextQueryBuilder.getTextQueryPart;

/**
 *
 * @author sl
 */
@Stateless
public class ProzessbaukastenSearchService implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    @Inject
    private ImageService imageService;

    @Inject
    private Session session;

    public List<AutocompleteSearchResult> getProzessbaukastenAutocompleteResult(String query) {

        UserPermissions<BerechtigungDto> userPermissions = session.getUserPermissions();

        QueryPartDTO baseQuery = getAutocompleteBaseQuery();
        QueryPartDTO autocompleteConditions = getAutocompleteQueryPart(query);
        QueryPartDTO permissionQueryPart = getPermissionQueryPart(userPermissions);
        QueryPartDTO finalQuery = baseQuery.merge(autocompleteConditions).merge(permissionQueryPart);
        List<Object[]> queryResult = computeAutocompleteResult(finalQuery);
        return parseProzessbaukastenAutocompleteResult(queryResult);
    }

    private List<Object[]> computeAutocompleteResult(QueryPartDTO queryPart) {
        Query q = entityManager.createQuery(queryPart.getQueryWithSorting());
        q.setMaxResults(10);
        q.setFirstResult(0);
        queryPart.getParameterMap().forEach(q::setParameter);
        return q.getResultList();
    }

    private List<Object[]> computeSearchResult(QueryPartDTO queryPart, int offset, int limit) {
        Query q = entityManager.createQuery(queryPart.getQueryWithSorting());
        q.setMaxResults(limit);
        q.setFirstResult(offset);
        queryPart.getParameterMap().forEach(q::setParameter);
        return q.getResultList();
    }

    private Long computeCountResult(QueryPartDTO queryPart) {
        Query q = entityManager.createQuery(queryPart.getQuery());
        queryPart.getParameterMap().forEach(q::setParameter);
        return (Long) q.getResultList().get(0);
    }

    public List<SearchResultDTO> getSearchResult(GlobalSearchFilter filter, int offset, int limit) {

        if (ProzessbaukastenFilterValidator.isInvalidFilterActive(filter)) {
            return Collections.emptyList();
        }

        UserPermissions<BerechtigungDto> userPermissions = session.getUserPermissions();

        QueryPartDTO searchBaseQuery = getSearchBaseQuery();
        QueryPartDTO filterQueryPart = getFilterQueryPart(filter);
        QueryPartDTO permissionQueryPart = getPermissionQueryPart(userPermissions);
        QueryPartDTO finalQuery = searchBaseQuery.merge(filterQueryPart).merge(permissionQueryPart);
        List<Object[]> queryResult = computeSearchResult(finalQuery, offset, limit);
        List<SearchResultDTO> result = parseProzessbaukastenSearchResult(queryResult);
        return insertDefaultImage(result);
    }

    private List<SearchResultDTO> insertDefaultImage(List<SearchResultDTO> searchResult) {
        byte[] defaultImage = imageService.loadDefaultImage();
        searchResult.stream().filter(s -> s.getImage() == null).forEach(s -> s.setImage(defaultImage));
        return searchResult;
    }

    public long getSearchResultCount(GlobalSearchFilter filter) {

        if (ProzessbaukastenFilterValidator.isInvalidFilterActive(filter)) {
            return 0L;
        }

        UserPermissions<BerechtigungDto> userPermissions = session.getUserPermissions();

        QueryPartDTO countBaseQuery = getCountBaseQuery();
        QueryPartDTO filterQueryPart = getFilterQueryPart(filter);
        QueryPartDTO permissionQueryPart = getPermissionQueryPart(userPermissions);
        QueryPartDTO finalQuery = countBaseQuery.merge(filterQueryPart).merge(permissionQueryPart);
        return computeCountResult(finalQuery);
    }

    private static QueryPartDTO getFilterQueryPart(GlobalSearchFilter filter) {
        QueryPartDTO queryPart = new QueryPartDTO();
        queryPart.merge(getTextQueryPart(filter.getQueryFilter()));
        queryPart.merge(getTteamFilterQueryPart(filter.getTteamFilter()));
        queryPart.merge(getProzessbaukastenFilterQueryPart(filter.getProzessbaukastenFilter()));
        queryPart.merge(getStatusFilterQueryPart(filter.getProzessbaukastenStatusFilter()));
        queryPart.merge(getForSortingFilterQueryPart(filter.getSortingFilter()));
        queryPart.merge(getErstellungsdatumFilterQueryPart(filter.getErstellungsdatumFilter()));
        queryPart.merge(getAenderungsdatumFilterQueryPart(filter.getAenderungsdatumFilter()));
        queryPart.merge(getDatumStatusaenderungFilterQueryPart(filter.getDatumStatusaenderungFilter()));
        return queryPart;
    }

}
