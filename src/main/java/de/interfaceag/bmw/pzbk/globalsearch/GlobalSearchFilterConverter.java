package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.entities.SearchFilter;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class GlobalSearchFilterConverter {

    private GlobalSearchFilterConverter() {
    }

    public static void searchFilterListToGlobalSearchFilter(List<SearchFilter> searchFilters, GlobalSearchFilter globalSearchFilter) {
        Iterator<SearchFilter> iterator = searchFilters.iterator();
        while (iterator.hasNext()) {
            searchFilterToGloablSearchFilterConverter(globalSearchFilter, iterator);
        }
    }

    private static void searchFilterToGloablSearchFilterConverter(GlobalSearchFilter globalSearchFilter, Iterator<SearchFilter> iterator) {
        SearchFilter filter = iterator.next();
        switch (filter.getFilterType()) {
            case DASHBOARD_ANFORDERUNG:
                searchFilterListForDashbaordAnforderung(globalSearchFilter, filter);
                break;
            case DASHBOARD_MELDUNG:
                searchFilterListForDashbaordMeldung(globalSearchFilter, filter);
                break;
            case MELDUNG:
                searchFilterListForMeldung(globalSearchFilter);
                break;
            case ANFORDERUNG:
                searchFilterListForAnforderung(globalSearchFilter);
                break;
            case PZBK:
                searchFilterListForPZBK(globalSearchFilter);
                break;
            case RELEVANZ:
                searchFilterListForRelevanz(globalSearchFilter, filter);
                break;
            case AENDERUNGSDATUM:
                searchFilterListForAenderungsdatum(globalSearchFilter, filter);
                break;
            case AENDERUNGSDATUM_BIS:
                searchFilterListForAenderungsdatumBis(globalSearchFilter, filter);
                break;
            case AENDERUNGSDATUM_VON:
                searchFilterListForAenderungsdatumVon(globalSearchFilter, filter);
                break;
            case ERSTELLUNGSDATUM:
                searchFilterListForErstellungsdatum(globalSearchFilter, filter);
                break;
            case ERSTELLUNGSDATUM_BIS:
                searchFilterListForErstellungsdatumBis(globalSearchFilter, filter);
                break;
            case ERSTELLUNGSDATUM_VON:
                searchFilterListForErstellungsdatumVon(globalSearchFilter, filter);
                break;
            case STATUS_AENDERUNGSDATUM:
                searchFilterListForStatusaenderungdatum(globalSearchFilter, filter);
                break;
            case STATUSAENDERUNGSDATUM_VON:
                searchFilterListForStatusanederungdatumVon(globalSearchFilter, filter);
                break;
            case STATUS_AENDERUNGSDATUM_BIS:
                searchFilterListForStatusaenderungdatumBis(globalSearchFilter, filter);
                break;
            case SENSORCOC:
                searchFilterListForSensorCoc(globalSearchFilter, filter);
                break;
            case FESTGESTELLT_IN:
                searchFilterListForFestgestelltIn(globalSearchFilter, filter);
                break;
            case DERIVAT:
                searchFilterListForDerivat(globalSearchFilter, filter);
                break;
            case TTEAM:
                searchFilterListForTteam(globalSearchFilter, filter);
                break;
            case MODUL:
                searchFilterListForModul(globalSearchFilter, filter);
                break;
            case MODULSETEAM:
                searchFilterListForModulSeTeam(globalSearchFilter, filter);
                break;
            case STATUS_ANFORDERUNG:
                searchFilterListForStatusAnforderung(globalSearchFilter, filter);
                break;
            case STATUS_MELDUNG:
                searchFilterListForStatusMeldung(globalSearchFilter, filter);
                break;
            case URL_ANFORDERUNG:
                searchFilterListForUrlAnforderung(globalSearchFilter, filter);
                break;
            case URL_MELDUNG:
                searchFilterListForUrlMeldung(globalSearchFilter, filter);
                break;
            default:
                break;
        }
    }

    private static void searchFilterListForUrlMeldung(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        List<Long> meldungUrlIds = filter.getLongListValue();
        globalSearchFilter.getMeldungIdFilter().setAttributeValue(meldungUrlIds);
    }

    private static void searchFilterListForUrlAnforderung(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        List<Long> anforderungUrlIds = filter.getLongListValue();
        globalSearchFilter.getAnforderungIdFilter().setAttributeValue(anforderungUrlIds);
    }

    private static void searchFilterListForStatusMeldung(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Set<Long> meldungStatusValues = filter.getStatusListValue().stream().map(status -> Integer.toUnsignedLong(status.getStatusId())).collect(Collectors.toSet());
        Set<SearchFilterObject> selectedValues = globalSearchFilter.getMeldungStatusFilter().getAll().stream()
                .filter(sfo -> meldungStatusValues.contains(sfo.getId())).collect(Collectors.toSet());
        globalSearchFilter.getMeldungStatusFilter().setSelectedValues(selectedValues);
    }

    private static void searchFilterListForStatusAnforderung(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Set<Long> anforderungStatusValues = filter.getStatusListValue().stream().map(status -> Integer.toUnsignedLong(status.getStatusId())).collect(Collectors.toSet());
        Set<SearchFilterObject> selectedValues = globalSearchFilter.getAnforderungStatusFilter().getAll().stream()
                .filter(sfo -> anforderungStatusValues.contains(sfo.getId())).collect(Collectors.toSet());
        globalSearchFilter.getAnforderungStatusFilter().setSelectedValues(selectedValues);
    }

    private static void searchFilterListForModulSeTeam(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        List<Long> modulSeTeamIds = filter.getLongListValue();
        Set<SearchFilterObject> selectedValues = globalSearchFilter.getModulSeTeamFilter().getAll().stream().filter(sfo -> modulSeTeamIds.contains(sfo.getId())).collect(Collectors.toSet());
        globalSearchFilter.getModulSeTeamFilter().setSelectedValues(selectedValues);
    }

    private static void searchFilterListForModul(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        List<Long> modulIds = filter.getLongListValue();
        Set<SearchFilterObject> selectedValues = globalSearchFilter.getModulFilter().getAll().stream().filter(sfo -> modulIds.contains(sfo.getId())).collect(Collectors.toSet());
        globalSearchFilter.getModulFilter().setSelectedValues(selectedValues);
    }

    private static void searchFilterListForTteam(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        List<Long> tteamIds = filter.getLongListValue();
        Set<SearchFilterObject> selectedValues = globalSearchFilter.getTteamFilter().getAll().stream().filter(sfo -> tteamIds.contains(sfo.getId())).collect(Collectors.toSet());
        globalSearchFilter.getTteamFilter().setSelectedValues(selectedValues);
    }

    private static void searchFilterListForDerivat(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        List<Long> derivatIds = filter.getLongListValue();
        Set<SearchFilterObject> selectedValues = globalSearchFilter.getDerivatFilter().getAll().stream().filter(sfo -> derivatIds.contains(sfo.getId())).collect(Collectors.toSet());
        globalSearchFilter.getDerivatFilter().setSelectedValues(selectedValues);
    }

    private static void searchFilterListForFestgestelltIn(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        List<Long> festgestelltInIds = filter.getLongListValue();
        Set<SearchFilterObject> selectedValues = globalSearchFilter.getFestgestelltInFilter().getAll().stream().filter(sfo -> festgestelltInIds.contains(sfo.getId())).collect(Collectors.toSet());
        globalSearchFilter.getFestgestelltInFilter().setSelectedValues(selectedValues);
    }

    private static void searchFilterListForSensorCoc(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        List<Long> sensorCocIds = filter.getLongListValue();
        Set<SearchFilterObject> selectedValues = globalSearchFilter.getSensorCocFilter().getAll().stream().filter(sfo -> sensorCocIds.contains(sfo.getId())).collect(Collectors.toSet());
        globalSearchFilter.getSensorCocFilter().setSelectedValues(selectedValues);
    }

    private static void searchFilterListForStatusaenderungdatumBis(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Date startDateValue = filter.getStartDateValue();
        globalSearchFilter.getDatumStatusaenderungFilter().setVon(startDateValue);
    }

    private static void searchFilterListForStatusanederungdatumVon(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Date endDateValue = filter.getEndDateValue();
        globalSearchFilter.getDatumStatusaenderungFilter().setBis(endDateValue);
    }

    private static void searchFilterListForStatusaenderungdatum(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Date startDateValue = filter.getStartDateValue();
        Date endDateValue = filter.getEndDateValue();
        globalSearchFilter.getDatumStatusaenderungFilter().setVon(startDateValue);
        globalSearchFilter.getDatumStatusaenderungFilter().setBis(endDateValue);
    }

    private static void searchFilterListForErstellungsdatumVon(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Date startDateValue = filter.getStartDateValue();
        globalSearchFilter.getErstellungsdatumFilter().setVon(startDateValue);
    }

    private static void searchFilterListForErstellungsdatumBis(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Date endDateValue = filter.getEndDateValue();
        globalSearchFilter.getErstellungsdatumFilter().setBis(endDateValue);
    }

    private static void searchFilterListForErstellungsdatum(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Date startDateValue = filter.getStartDateValue();
        Date endDateValue = filter.getEndDateValue();
        globalSearchFilter.getErstellungsdatumFilter().setVon(startDateValue);
        globalSearchFilter.getErstellungsdatumFilter().setBis(endDateValue);
    }

    private static void searchFilterListForAenderungsdatumVon(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Date startDateValue = filter.getStartDateValue();
        globalSearchFilter.getAenderungsdatumFilter().setVon(startDateValue);
    }

    private static void searchFilterListForAenderungsdatumBis(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Date endDateValue = filter.getEndDateValue();
        globalSearchFilter.getAenderungsdatumFilter().setBis(endDateValue);
    }

    private static void searchFilterListForAenderungsdatum(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Date startDateValue = filter.getStartDateValue();
        Date endDateValue = filter.getEndDateValue();
        globalSearchFilter.getAenderungsdatumFilter().setVon(startDateValue);
        globalSearchFilter.getAenderungsdatumFilter().setBis(endDateValue);
    }

    private static void searchFilterListForRelevanz(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Optional<SearchFilterObject> value = globalSearchFilter.getPhasenrelevanzFilter().getAll().stream().filter(sfo -> sfo.getBooleanValue() == filter.getBooleanValue()).findAny();
        if (value.isPresent()) {
            Set<SearchFilterObject> selectedValues = new HashSet<>();
            selectedValues.add(value.get());
            globalSearchFilter.getPhasenrelevanzFilter().setSelectedValues(selectedValues);
        }
    }

    private static void searchFilterListForPZBK(GlobalSearchFilter globalSearchFilter) {
        Optional<SearchFilterObject> searchFilterObject = globalSearchFilter.getTypeFilter().getAll().stream().filter(sfo -> sfo.getName().equals("Pzbk")).findAny();
        searchFilterObject.ifPresent(filterObject -> globalSearchFilter.getTypeFilter().getSelectedValues().add(filterObject));
    }

    private static void searchFilterListForAnforderung(GlobalSearchFilter globalSearchFilter) {
        Optional<SearchFilterObject> searchFilterObject = globalSearchFilter.getTypeFilter().getAll().stream().filter(sfo -> sfo.getName().equals("Anforderung")).findAny();
        searchFilterObject.ifPresent(filterObject -> globalSearchFilter.getTypeFilter().getSelectedValues().add(filterObject));
    }

    private static void searchFilterListForMeldung(GlobalSearchFilter globalSearchFilter) {
        Optional<SearchFilterObject> searchFilterObject = globalSearchFilter.getTypeFilter().getAll().stream().filter(sfo -> sfo.getName().equals("Meldung")).findAny();
        searchFilterObject.ifPresent(filterObject -> globalSearchFilter.getTypeFilter().getSelectedValues().add(filterObject));
    }

    private static void searchFilterListForDashbaordMeldung(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Set<SearchFilterObject> meldungIds = filter.getLongListValue().stream().map(id -> new SearchFilterObject(id, "")).collect(Collectors.toSet());
        globalSearchFilter.getDashboardMeldungIdFilter().setSelectedValues(meldungIds);
    }

    private static void searchFilterListForDashbaordAnforderung(GlobalSearchFilter globalSearchFilter, SearchFilter filter) {
        Set<SearchFilterObject> anforderungIds = filter.getLongListValue().stream().map(id -> new SearchFilterObject(id, "")).collect(Collectors.toSet());
        globalSearchFilter.getDashboardAnforderungIdFilter().setSelectedValues(anforderungIds);
    }

    public static List<SearchFilter> toSearchFilters(GlobalSearchFilter filter) {
        List<SearchFilter> filterList = new ArrayList<>();

        addSearchFilterForDashboardFilter(filter, filterList);

        addSearchFilterForTypeFilter(filter, filterList);

        addSearchFilterForPhasenrelevanzFilter(filter, filterList);

        addSearchFilterForAenderungsdatumFilter(filter, filterList);

        addSearchFilterForErstellungsdatumFilter(filter, filterList);

        addSearchFilterForDatumStatunsaenderungFilter(filter, filterList);

        addSearchFilterForSensorCocFilter(filter, filterList);

        addSearchFilterForFestgestelltInFilter(filter, filterList);

        addSearchFilterForDerivatFilter(filter, filterList);

        addSearchFilterForTteamFilter(filter, filterList);

        addSearchFilterForModulFilter(filter, filterList);

        addSearchFilterForModulSeTeamFilter(filter, filterList);

        addSearchFilterForAnforderungStatusFilter(filter, filterList);

        addSearchFilterForForMeldungStatusFilter(filter, filterList);

        addSearchFilterForShowStatusGeloeschtFilter(filter, filterList);

        return filterList;
    }

    private static void addSearchFilterForDashboardFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getDashboardFilter().isActive()) {
            filterList.add(new SearchFilter(SearchFilterType.DASHBOARD_ANFORDERUNG, new ArrayList<>(filter.getDashboardAnforderungIdFilter().getSelectedIdsAsSet())));
            filterList.add(new SearchFilter(SearchFilterType.DASHBOARD_MELDUNG, new ArrayList<>(filter.getDashboardMeldungIdFilter().getSelectedIdsAsSet())));
        }
    }

    private static void addSearchFilterForShowStatusGeloeschtFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getShowStatusGeloeschtFilter().isActive()) {
            if (filter.getShowStatusGeloeschtFilter().getValue()) {
                List<Status> l = new ArrayList<>();
                l.add(Status.M_GELOESCHT);
                l.add(Status.A_GELOESCHT);
                SearchFilter sf = new SearchFilter(SearchFilterType.STATUS_GELOESCHT);
                sf.setStatusListValue(l);
                sf.setBooleanValue(true);
                filterList.add(sf);
            } else {
                List<Status> l = new ArrayList<>();
                l.add(Status.M_GELOESCHT);
                l.add(Status.A_GELOESCHT);
                SearchFilter sf = new SearchFilter(SearchFilterType.STATUS_GELOESCHT);
                sf.setStatusListValue(l);
                sf.setBooleanValue(false);
                filterList.add(sf);
            }
        }
    }

    private static void addSearchFilterForForMeldungStatusFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getMeldungStatusFilter().isActive()) {
            SearchFilter searchFilter = new SearchFilter(SearchFilterType.STATUS_MELDUNG);
            searchFilter.setStatusListValue(filter.getMeldungStatusFilter().getAsEnumList());
            filterList.add(searchFilter);
        }
    }

    private static void addSearchFilterForAnforderungStatusFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getAnforderungStatusFilter().isActive()) {
            SearchFilter searchFilter = new SearchFilter(SearchFilterType.STATUS_ANFORDERUNG);
            searchFilter.setStatusListValue(filter.getAnforderungStatusFilter().getAsEnumList());
            filterList.add(searchFilter);
        }
    }

    private static void addSearchFilterForModulSeTeamFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getModulSeTeamFilter().isActive()) {
            filterList.add(new SearchFilter(SearchFilterType.MODULSETEAM, new ArrayList(filter.getModulSeTeamFilter().getSelectedIdsAsSet())));
        }
    }

    private static void addSearchFilterForModulFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getModulFilter().isActive()) {
            filterList.add(new SearchFilter(SearchFilterType.MODUL, new ArrayList(filter.getModulFilter().getSelectedIdsAsSet())));
        }
    }

    private static void addSearchFilterForTteamFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getTteamFilter().isActive()) {
            filterList.add(new SearchFilter(SearchFilterType.TTEAM, new ArrayList(filter.getTteamFilter().getSelectedIdsAsSet())));
        }
    }

    private static void addSearchFilterForDerivatFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getDerivatFilter().isActive()) {
            filterList.add(new SearchFilter(SearchFilterType.DERIVAT, new ArrayList(filter.getDerivatFilter().getSelectedIdsAsSet())));
        }
    }

    private static void addSearchFilterForFestgestelltInFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getFestgestelltInFilter().isActive()) {
            filterList.add(new SearchFilter(SearchFilterType.FESTGESTELLT_IN, new ArrayList(filter.getFestgestelltInFilter().getSelectedIdsAsSet())));
        }
    }

    private static void addSearchFilterForSensorCocFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getSensorCocFilter().isActive()) {
            filterList.add(new SearchFilter(SearchFilterType.SENSORCOC, new ArrayList<>(filter.getSensorCocFilter().getSelectedIdsAsSet())));
        }
    }

    private static void addSearchFilterForDatumStatunsaenderungFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getDatumStatusaenderungFilter().isActive()) {
            if (filter.getDatumStatusaenderungFilter().getVonDateFilter().isActive() && filter.getDatumStatusaenderungFilter().getBisDateFilter().isActive()) {
                filterList.add(new SearchFilter(SearchFilterType.STATUS_AENDERUNGSDATUM, filter.getDatumStatusaenderungFilter().getVon(), filter.getDatumStatusaenderungFilter().getBis()));
            } else if (filter.getDatumStatusaenderungFilter().getVonDateFilter().isActive()) {
                filterList.add(new SearchFilter(SearchFilterType.STATUSAENDERUNGSDATUM_VON, filter.getDatumStatusaenderungFilter().getVon(), 0));
            } else {
                filterList.add(new SearchFilter(SearchFilterType.STATUS_AENDERUNGSDATUM_BIS, filter.getDatumStatusaenderungFilter().getBis(), 1));
            }
        }
    }

    private static void addSearchFilterForErstellungsdatumFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getErstellungsdatumFilter().isActive()) {
            if (filter.getErstellungsdatumFilter().getVonDateFilter().isActive() && filter.getErstellungsdatumFilter().getBisDateFilter().isActive()) {
                filterList.add(new SearchFilter(SearchFilterType.ERSTELLUNGSDATUM, filter.getErstellungsdatumFilter().getVon(), filter.getErstellungsdatumFilter().getBis()));
            } else if (filter.getErstellungsdatumFilter().getVonDateFilter().isActive()) {
                filterList.add(new SearchFilter(SearchFilterType.ERSTELLUNGSDATUM_VON, filter.getErstellungsdatumFilter().getVon(), 0));
            } else {
                filterList.add(new SearchFilter(SearchFilterType.ERSTELLUNGSDATUM_BIS, filter.getErstellungsdatumFilter().getBis(), 1));
            }
        }
    }

    private static void addSearchFilterForAenderungsdatumFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getAenderungsdatumFilter().isActive()) {
            if (filter.getAenderungsdatumFilter().getVonDateFilter().isActive() && filter.getAenderungsdatumFilter().getBisDateFilter().isActive()) {
                filterList.add(new SearchFilter(SearchFilterType.AENDERUNGSDATUM, filter.getAenderungsdatumFilter().getVon(), filter.getAenderungsdatumFilter().getBis()));
            } else if (filter.getAenderungsdatumFilter().getVonDateFilter().isActive()) {
                filterList.add(new SearchFilter(SearchFilterType.AENDERUNGSDATUM_VON, filter.getAenderungsdatumFilter().getVon(), 0));
            } else {
                filterList.add(new SearchFilter(SearchFilterType.AENDERUNGSDATUM_BIS, filter.getAenderungsdatumFilter().getBis(), 1));
            }
        }
    }

    private static void addSearchFilterForPhasenrelevanzFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getPhasenrelevanzFilter().isActive()) {
            SearchFilter searchFilter = new SearchFilter(SearchFilterType.RELEVANZ);
            List<Boolean> values = filter.getPhasenrelevanzFilter().getAsBoolean();
            if (values.size() == 1) {
                searchFilter.setBooleanValue(values.get(0));
                filterList.add(searchFilter);
            }
        }
    }

    private static void addSearchFilterForTypeFilter(GlobalSearchFilter filter, List<SearchFilter> filterList) {
        if (filter.getTypeFilter().isActive()) {
            Iterator<Type> types = filter.getTypeFilter().getAsEnumList().iterator();
            while (types.hasNext()) {
                Type next = types.next();
                switch (next) {
                    case MELDUNG:
                        filterList.add(new SearchFilter(SearchFilterType.MELDUNG));
                        break;
                    case ANFORDERUNG:
                        filterList.add(new SearchFilter(SearchFilterType.ANFORDERUNG));
                        break;
                    case PROZESSBAUKASTEN:
                        filterList.add(new SearchFilter(SearchFilterType.PZBK));
                        break;
                    default:
                        break;
                }
            }
        }
    }

}
