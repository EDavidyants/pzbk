package de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten;

import de.interfaceag.bmw.pzbk.filter.QueryUrlFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
final class ProzessbaukastenTextQueryBuilder {

    private ProzessbaukastenTextQueryBuilder() {
    }

    static QueryPartDTO getAutocompleteQueryPart(String query) {
        QueryPartDTO queryPart = new QueryPartDTO();
        queryPart.append("AND (");
        queryPart.append("(LOWER(p.fachId) LIKE LOWER(:query)) ");
        queryPart.append("OR (LOWER(p.bezeichnung) LIKE LOWER(:query)) ");
        queryPart.append("OR (LOWER(p.beschreibung) LIKE LOWER(:query)) ");
        queryPart.append(") ");
        queryPart.put("query", "%" + query.trim() + "%");
        return queryPart;
    }

    static QueryPartDTO getTextQueryPart(QueryUrlFilter filter) {
        QueryPartDTO result = new QueryPartDTO();
        if (filter.isActive()) {
            List<String> queryValues = filter.getAttributeValues();
            result = getQueryPartForQueryValues(queryValues);
        }
        return result;
    }

    private static QueryPartDTO getQueryPartForQueryValues(List<String> queryValues) {
        String query = getQueryForQueryValues(queryValues);
        Map<String, Object> parameterMap = getParameterMapForQueryValues(queryValues);
        QueryPartDTO result = new QueryPartDTO(query);
        result.putAll(parameterMap);
        return result;
    }

    private static Map<String, Object> getParameterMapForQueryValues(List<String> queryValues) {
        QueryPartDTO result = new QueryPartDTO();
        getQueryValueStream(queryValues).forEach(indexQueryPart -> result.putAll(indexQueryPart.getParameterMap()));
        return result.getParameterMap();
    }

    private static String getQueryForQueryValues(List<String> queryValues) {
        return getQueryValueStream(queryValues)
                .map(indexQueryPart -> indexQueryPart.getQuery())
                .collect(Collectors.joining(" OR ", "AND (", ")"));
    }

    private static Stream<QueryPartDTO> getQueryValueStream(List<String> queryValues) {
        if (queryValues == null || queryValues.isEmpty()) {
            return Stream.empty();
        }

        return IntStream.range(0, queryValues.size())
                .mapToObj(index -> {
                    String query = queryValues.get(index);
                    return getQueryPartForStringQuery(query, index);
                });
    }

    private static QueryPartDTO getQueryPartForStringQuery(String query, int index) {
        QueryPartDTO queryPart = new QueryPartDTO();
        queryPart.append("(LOWER(p.fachId) LIKE LOWER(:query").append(Integer.toString(index)).append(")) ");
        queryPart.append("OR (LOWER(p.bezeichnung) LIKE LOWER(:query").append(Integer.toString(index)).append(")) ");
        queryPart.append("OR (LOWER(p.beschreibung) LIKE LOWER(:query").append(Integer.toString(index)).append(")) ");
        queryPart.put("query" + Integer.toString(index), "%" + query.trim() + "%");
        return queryPart;
    }

}
