package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Stateless
public class QueryPartService implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager em;

    public List getResultList(String queryPart, Map<String, Object> parameterMap) {
        Query query = createQueryWithParameter(queryPart, parameterMap);
        return query.getResultList();
    }

    public List<Object[]> getResultListWithMaxResults(String queryPart, Map<String, Object> parameterMap, int limit, int offset) {
        TypedQuery<Object[]> query = createQueryWithParameter(queryPart, parameterMap);
        query.setMaxResults(limit);
        query.setFirstResult(offset);
        return query.getResultList();
    }

    private TypedQuery<Object[]> createQueryWithParameter(String queryPart, Map<String, Object> parameterMap) {
        TypedQuery query = em.createQuery(queryPart, Object[].class);
        if (parameterMap != null && !parameterMap.isEmpty()) {
            parameterMap.forEach(query::setParameter);
        }
        return query;
    }

    public List<Long> getQueryResultsAsLongList(QueryPartDTO queryPart) {
        Query q = queryPart.buildGenericQuery(em, Long.class);
        return (List<Long>) q.getResultList();
    }

    public Long getSingleResultAsLong(QueryPartDTO queryPart) {
        Query q = queryPart.buildGenericQuery(em, Long.class);
        return (Long) q.getSingleResult();
    }

}
