package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class SearchResultDtoConverter {

    private SearchResultDtoConverter() {
    }

    public static List<SearchResultDTO> parseAnforderungSearchResults(List<Object[]> searchResult, boolean withImage) {
        if (withImage) {
            return convertAnforderungSearchResultToDtoWithImage(searchResult);
        } else {
            return convertAnforderungSearchResultToDtoWithoutImage(searchResult);
        }
    }

    public static List<AutocompleteSearchResult> parseAnforderungAutocompleteSearchResults(List<Object[]> searchResult) {
        return searchResult.stream()
                .map(r -> new SearchResultDTO(r[0] != null ? (Long) r[0] : null,
                r[1] != null ? (String) r[1] : "", "Anforderung", r[2] != null ? (String) r[2] : "",
                r[3] != null ? ((Status) r[3]).toString() : "", r[5] != null ? (String) r[5] : "",
                r[4] != null ? ((SensorCoc) r[4]).getTechnologie() : "", "V" + r[6] != null ? Integer.toString((Integer) r[6]) : ""))
                .collect(Collectors.toList());
    }

    private static List<SearchResultDTO> convertAnforderungSearchResultToDtoWithoutImage(List<Object[]> searchResult) {
        return searchResult.stream()
                .map(r -> new SearchResultDTO(r[0] != null ? (Long) r[0] : null,
                r[1] != null ? (String) r[1] : "", "Anforderung", r[2] != null ? (String) r[2] : "",
                r[3] != null ? ((Status) r[3]).toString() : "", r[5] != null ? (String) r[5] : "",
                r[4] != null ? ((SensorCoc) r[4]).getTechnologie() : "", "V" + r[6] != null ? Integer.toString((Integer) r[6]) : ""))
                .collect(Collectors.toList());
    }

    private static List<SearchResultDTO> convertAnforderungSearchResultToDtoWithImage(List<Object[]> searchResult) {
        return searchResult.stream()
                .map(r -> new SearchResultDTO(r[0] != null ? (Long) r[0] : null,
                r[1] != null ? (String) r[1] : "", "Anforderung", r[2] != null ? (String) r[2] : "",
                r[3] != null ? ((Status) r[3]).toString() : "", r[5] != null ? (String) r[5] : "",
                r[4] != null ? ((SensorCoc) r[4]).getTechnologie() : "", "V" + r[6] != null ? Integer.toString((Integer) r[6]) : "",
                r[7] != null ? (byte[]) r[7] : null, r[8] != null ? (Long) r[8] : -1)) //, r[8] != null ? (String) r[8] : ""))
                .collect(Collectors.toList());
    }

    public static List<SearchResultDTO> parseMeldungSearchResults(List<Object[]> searchResult, boolean withImage) {
        if (withImage) {
            return convertMeldungSearchResultsToDtoWithImage(searchResult);
        } else {
            return convertMeldungSearchResultsToDtoWithoutImage(searchResult);
        }
    }

    private static List<SearchResultDTO> convertMeldungSearchResultsToDtoWithoutImage(List<Object[]> searchResult) {
        return searchResult.stream()
                .map(r -> new SearchResultDTO(r[0] != null ? (Long) r[0] : null,
                r[1] != null ? (String) r[1] : "", "Meldung", r[2] != null ? (String) r[2] : "",
                r[3] != null ? ((Status) r[3]).toString() : "", r[5] != null ? (String) r[5] : "",
                r[4] != null ? ((SensorCoc) r[4]).getTechnologie() : ""))
                .collect(Collectors.toList());
    }

    private static List<SearchResultDTO> convertMeldungSearchResultsToDtoWithImage(List<Object[]> searchResult) {
        return searchResult.stream()
                .map(r -> new SearchResultDTO(r[0] != null ? (Long) r[0] : null,
                r[1] != null ? (String) r[1] : "", "Meldung", r[2] != null ? (String) r[2] : "",
                r[3] != null ? ((Status) r[3]).toString() : "", r[5] != null ? (String) r[5] : "",
                r[4] != null ? ((SensorCoc) r[4]).getTechnologie() : "", r[6] != null ? (byte[]) r[6] : null))
                .collect(Collectors.toList());
    }

    public static List<AutocompleteSearchResult> parseMeldungAutocompleteSearchResults(List<Object[]> searchResult) {
        return searchResult.stream()
                .map(r -> new SearchResultDTO(r[0] != null ? (Long) r[0] : null,
                r[1] != null ? (String) r[1] : "", "Meldung", r[2] != null ? (String) r[2] : "",
                r[3] != null ? ((Status) r[3]).toString() : "", r[5] != null ? (String) r[5] : "",
                r[4] != null ? ((SensorCoc) r[4]).getTechnologie() : ""))
                .collect(Collectors.toList());
    }

    public static List<AutocompleteSearchResult> parseProzessbaukastenAutocompleteResult(List<Object[]> queryResults) {
        List<AutocompleteSearchResult> result = new ArrayList<>();
        Iterator<Object[]> iterator = queryResults.iterator();
        while (iterator.hasNext()) {
            Object[] queryResult = iterator.next();

            Long id = (Long) queryResult[0];
            String fachId = (String) queryResult[1];
            Integer version = (Integer) queryResult[2];
            String bezeichnung = (String) queryResult[3];
            String beschreibung = (String) queryResult[4];
            String status = parseProzessbaukastenStatus(queryResult);

            SearchResultDTO newResult = SearchResultDTO.forProzessbaukasten(fachId, version)
                    .withId(id)
                    .withName(fachId)
                    .withBeschreibung(beschreibung)
                    .withStatus(status)
                    .withModul(bezeichnung)
                    .get();

            result.add(newResult);
        }
        return result;
    }

    public static List<SearchResultDTO> parseProzessbaukastenSearchResult(List<Object[]> queryResults) {
        List<SearchResultDTO> result = new ArrayList<>();
        Iterator<Object[]> iterator = queryResults.iterator();
        while (iterator.hasNext()) {
            Object[] queryResult = iterator.next();

            Long id = (Long) queryResult[0];
            String fachId = (String) queryResult[1];
            Integer version = (Integer) queryResult[2];
            String bezeichnung = (String) queryResult[3];
            String beschreibung = (String) queryResult[4];
            String tteam = (String) queryResult[6];
            String status = parseProzessbaukastenStatus(queryResult);

            SearchResultDTO newResult = SearchResultDTO.forProzessbaukasten(fachId, version)
                    .withId(id)
                    .withName(fachId)
                    .withBeschreibung(beschreibung)
                    .withStatus(status)
                    .withSensorCoc(tteam)
                    .withModul(bezeichnung)
                    .get();

            newResult.setSelected(true);
            result.add(newResult);
        }
        return result;
    }

    private static String parseProzessbaukastenStatus(Object[] queryResult) {
        Integer statusId = (Integer) queryResult[5];
        ProzessbaukastenStatus status = ProzessbaukastenStatus.getById(statusId);
        return LocalizationService.getValue(Locale.GERMAN, status.getLocalizationKey());
    }

}
