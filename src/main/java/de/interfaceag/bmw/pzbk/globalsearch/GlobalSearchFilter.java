package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SearchFilter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Kategorie;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.ReferenzSystem;
import de.interfaceag.bmw.pzbk.enums.Sorting;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.filter.AenderungsdatumVonBisDateFilter;
import de.interfaceag.bmw.pzbk.filter.AnforderungStatusFilter;
import de.interfaceag.bmw.pzbk.filter.BooleanSearchFilter;
import de.interfaceag.bmw.pzbk.filter.DashboardAnforderungIdFilter;
import de.interfaceag.bmw.pzbk.filter.DashboardFilter;
import de.interfaceag.bmw.pzbk.filter.DashboardMeldungIdFilter;
import de.interfaceag.bmw.pzbk.filter.DatumStatusaenderungVonBisDateFilter;
import de.interfaceag.bmw.pzbk.filter.DerivatFilter;
import de.interfaceag.bmw.pzbk.filter.ErstellungsdatumVonBisDateFilter;
import de.interfaceag.bmw.pzbk.filter.FestgestelltInFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.IdUrlFilter;
import de.interfaceag.bmw.pzbk.filter.KategorieFilter;
import de.interfaceag.bmw.pzbk.filter.MeldungStatusFilter;
import de.interfaceag.bmw.pzbk.filter.ModulFilter;
import de.interfaceag.bmw.pzbk.filter.ModulSeTeamFilter;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.PageUrlFilter;
import de.interfaceag.bmw.pzbk.filter.PhasenrelevanzFilter;
import de.interfaceag.bmw.pzbk.filter.ProzessbaukastenFilter;
import de.interfaceag.bmw.pzbk.filter.ProzessbaukastenStatusFilter;
import de.interfaceag.bmw.pzbk.filter.QueryUrlFilter;
import de.interfaceag.bmw.pzbk.filter.ReferenzSystemFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilterRemoveObject;
import de.interfaceag.bmw.pzbk.filter.SensorCocFilter;
import de.interfaceag.bmw.pzbk.filter.ShowGeloeschteElementeFilter;
import de.interfaceag.bmw.pzbk.filter.SingleValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SortingFilter;
import de.interfaceag.bmw.pzbk.filter.ThemenklammerFilter;
import de.interfaceag.bmw.pzbk.filter.TteamFilter;
import de.interfaceag.bmw.pzbk.filter.TypeFilter;
import de.interfaceag.bmw.pzbk.filter.UserDefinedSearchUrlFilter;
import de.interfaceag.bmw.pzbk.filter.VonBisDateSearchFilter;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.globalsearch.dto.DashboardUrlParamterDto;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;
import de.interfaceag.bmw.pzbk.shared.utils.SearchUtil;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class GlobalSearchFilter implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(GlobalSearchFilter.class);

    private static final Page PAGE = Page.SUCHE;

    private final QueryUrlFilter queryFilter;

    private final MultiValueEnumSearchFilter<ReferenzSystem> referenzSystemFilter;

    private final MultiValueEnumSearchFilter<Kategorie> kategorieFilter;

    private final MultiValueEnumSearchFilter<Type> typeFilter;

    private final SingleValueEnumSearchFilter<Sorting> sortingFilter;

    private final ShowGeloeschteElementeFilter showStatusGeloeschtFilter;
    private final MultiValueEnumSearchFilter<Status> meldungStatusFilter;
    private final MultiValueEnumSearchFilter<Status> anforderungStatusFilter;
    private final MultiValueEnumSearchFilter<ProzessbaukastenStatus> prozessbaukastenStatusFilter;

    private final IdUrlFilter anforderungIdFilter;
    private final IdUrlFilter meldungIdFilter;

    private final IdSearchFilter sensorCocFilter;
    private final IdSearchFilter tteamFilter;
    private final IdSearchFilter derivatFilter;
    private final ThemenklammerFilter themenklammerFilter;
    private final BooleanSearchFilter phasenrelevanzFilter;
    private final IdSearchFilter festgestelltInFilter;

    private final IdSearchFilter modulSeTeamFilter;
    private final IdSearchFilter modulFilter;

    private final IdSearchFilter prozessbaukastenFilter;

    private final VonBisDateSearchFilter erstellungsdatumFilter;
    private final VonBisDateSearchFilter aenderungsdatumFilter;

    private final VonBisDateSearchFilter datumStatusaenderungFilter;

    private final PageUrlFilter pageUrlFilter;

    private final DashboardFilter dashboardFilter;
    private IdSearchFilter dashboardMeldungIdFilter;
    private IdSearchFilter dashboardAnforderungIdFilter;

    private final UserDefinedSearchUrlFilter userDefinedSearchUrlFilter;

    protected GlobalSearchFilter(
            UrlParameter urlParameter,
            List<SensorCoc> allSensorCocs,
            List<Tteam> allTteams,
            List<Modul> allModule,
            List<ModulSeTeam> allModulSeTeams,
            List<Derivat> allDerivate,
            List<ProzessbaukastenFilterDto> prozessbaukaesten,
            List<FestgestelltIn> allFestgestelltIn,
            Collection<ThemenklammerDto> themenklammern) {
        this.prozessbaukastenFilter = new ProzessbaukastenFilter(prozessbaukaesten, urlParameter);
        this.queryFilter = new QueryUrlFilter(urlParameter);

        this.typeFilter = new TypeFilter(urlParameter);

        this.referenzSystemFilter = new ReferenzSystemFilter(urlParameter);

        this.kategorieFilter = new KategorieFilter(urlParameter);

        this.sortingFilter = new SortingFilter(urlParameter);

        this.meldungStatusFilter = new MeldungStatusFilter(urlParameter);
        this.anforderungStatusFilter = new AnforderungStatusFilter(urlParameter);
        this.prozessbaukastenStatusFilter = new ProzessbaukastenStatusFilter(urlParameter);

        this.sensorCocFilter = new SensorCocFilter(allSensorCocs, urlParameter);
        this.tteamFilter = new TteamFilter(allTteams, urlParameter);
        this.derivatFilter = new DerivatFilter(allDerivate, urlParameter);
        this.phasenrelevanzFilter = new PhasenrelevanzFilter(urlParameter);
        this.festgestelltInFilter = new FestgestelltInFilter(allFestgestelltIn, urlParameter);

        this.modulFilter = new ModulFilter(allModule, urlParameter);
        this.modulSeTeamFilter = new ModulSeTeamFilter(allModulSeTeams, urlParameter);

        this.erstellungsdatumFilter = new ErstellungsdatumVonBisDateFilter(urlParameter);
        this.aenderungsdatumFilter = new AenderungsdatumVonBisDateFilter(urlParameter);
        this.datumStatusaenderungFilter = new DatumStatusaenderungVonBisDateFilter(urlParameter);

        this.pageUrlFilter = new PageUrlFilter(urlParameter);

        this.userDefinedSearchUrlFilter = new UserDefinedSearchUrlFilter(urlParameter);

        this.anforderungIdFilter = new IdUrlFilter(Type.ANFORDERUNG, urlParameter);
        this.meldungIdFilter = new IdUrlFilter(Type.MELDUNG, urlParameter);
        this.themenklammerFilter = new ThemenklammerFilter(themenklammern, urlParameter);

        this.dashboardFilter = new DashboardFilter(urlParameter);

        this.showStatusGeloeschtFilter = new ShowGeloeschteElementeFilter(urlParameter);
        this.dashboardAnforderungIdFilter = new DashboardAnforderungIdFilter();
        this.dashboardMeldungIdFilter = new DashboardMeldungIdFilter();

        Optional<String> pParameter = urlParameter.getValue("p");
        if (pParameter.isPresent()) {
            DashboardUrlParamterDto parseSearchParamter = parseSearchParamter(pParameter.get());
            dashboardAnforderungIdFilter = new DashboardAnforderungIdFilter(parseSearchParamter.getAnforderungIds());
            dashboardMeldungIdFilter = new DashboardMeldungIdFilter(parseSearchParamter.getMeldungIds());
        }

        Optional<String> anforderungIdParameter = urlParameter.getValue("aId");
        Optional<String> meldungIdParameter = urlParameter.getValue("mId");

        if (anforderungIdParameter.isPresent()) {
            Collection<Long> anforderungIds = parseAnforderungMeldungIdParamter(anforderungIdParameter.get());
            dashboardAnforderungIdFilter = new DashboardAnforderungIdFilter(anforderungIds);
        }

        if (meldungIdParameter.isPresent()) {
            Collection<Long> meldungIds = parseAnforderungMeldungIdParamter(meldungIdParameter.get());
            dashboardMeldungIdFilter = new DashboardMeldungIdFilter(meldungIds);
        }

    }

    public String getUrl() {
        return PAGE.getUrl() + getUrlParameter();
    }

    public String getResetUrl() {
        return PAGE.getUrl() + "?faces-redirect=true";
    }

    public IdSearchFilter getProzessbaukastenFilter() {
        return prozessbaukastenFilter;
    }

    public IdSearchFilter getSensorCocFilter() {
        return sensorCocFilter;
    }

    public IdSearchFilter getFestgestelltInFilter() {
        return festgestelltInFilter;
    }

    public BooleanSearchFilter getPhasenrelevanzFilter() {
        return phasenrelevanzFilter;
    }

    public MultiValueEnumSearchFilter<Status> getMeldungStatusFilter() {
        return meldungStatusFilter;
    }

    public MultiValueEnumSearchFilter<Status> getAnforderungStatusFilter() {
        return anforderungStatusFilter;
    }

    public IdSearchFilter getTteamFilter() {
        return tteamFilter;
    }

    public IdSearchFilter getDerivatFilter() {
        return derivatFilter;
    }

    public IdSearchFilter getModulSeTeamFilter() {
        return modulSeTeamFilter;
    }

    public IdSearchFilter getModulFilter() {
        return modulFilter;
    }

    public VonBisDateSearchFilter getErstellungsdatumFilter() {
        return erstellungsdatumFilter;
    }

    public VonBisDateSearchFilter getAenderungsdatumFilter() {
        return aenderungsdatumFilter;
    }

    public VonBisDateSearchFilter getDatumStatusaenderungFilter() {
        return datumStatusaenderungFilter;
    }

    public PageUrlFilter getPageUrlFilter() {
        return pageUrlFilter;
    }

    public MultiValueEnumSearchFilter<Type> getTypeFilter() {
        return typeFilter;
    }

    public MultiValueEnumSearchFilter<ReferenzSystem> getReferenzSystemFilter() {
        return referenzSystemFilter;
    }

    public MultiValueEnumSearchFilter<Kategorie> getKategorieFilter() {
        return kategorieFilter;
    }

    public QueryUrlFilter getQueryFilter() {
        return queryFilter;
    }

    public SingleValueEnumSearchFilter<Sorting> getSortingFilter() {
        return sortingFilter;
    }

    public UserDefinedSearchUrlFilter getUserDefinedSearchUrlFilter() {
        return userDefinedSearchUrlFilter;
    }

    public IdUrlFilter getAnforderungIdFilter() {
        return anforderungIdFilter;
    }

    public IdUrlFilter getMeldungIdFilter() {
        return meldungIdFilter;
    }

    public DashboardFilter getDashboardFilter() {
        return dashboardFilter;
    }

    public IdSearchFilter getDashboardMeldungIdFilter() {
        return dashboardMeldungIdFilter;
    }

    public IdSearchFilter getDashboardAnforderungIdFilter() {
        return dashboardAnforderungIdFilter;
    }

    public ThemenklammerFilter getThemenklammerFilter() {
        return themenklammerFilter;
    }

    public ShowGeloeschteElementeFilter getShowStatusGeloeschtFilter() {
        return showStatusGeloeschtFilter;
    }

    public void setDashboardMeldungIdFilter(IdSearchFilter dashboardMeldungIdFilter) {
        this.dashboardMeldungIdFilter = dashboardMeldungIdFilter;
    }

    public void setDashboardAnforderungIdFilter(IdSearchFilter dashboardAnforderungIdFilter) {
        this.dashboardAnforderungIdFilter = dashboardAnforderungIdFilter;
    }

    public MultiValueEnumSearchFilter<ProzessbaukastenStatus> getProzessbaukastenStatusFilter() {
        return prozessbaukastenStatusFilter;
    }

    private String getUrlParameter() {
        StringBuilder sb = new StringBuilder("?faces-redirect=true");

        sb.append(sensorCocFilter.getIndependentParameter());
        sb.append(festgestelltInFilter.getIndependentParameter());
        sb.append(phasenrelevanzFilter.getIndependentParameter());
        sb.append(meldungStatusFilter.getIndependentParameter());
        sb.append(anforderungStatusFilter.getIndependentParameter());
        sb.append(prozessbaukastenStatusFilter.getIndependentParameter());
        sb.append(tteamFilter.getIndependentParameter());
        sb.append(derivatFilter.getIndependentParameter());
        sb.append(modulSeTeamFilter.getIndependentParameter());
        sb.append(modulFilter.getIndependentParameter());
        sb.append(erstellungsdatumFilter.getIndependentParameter());
        sb.append(aenderungsdatumFilter.getIndependentParameter());
        sb.append(datumStatusaenderungFilter.getIndependentParameter());
        sb.append(pageUrlFilter.getIndependentParameter());
        sb.append(typeFilter.getIndependentParameter());
        sb.append(queryFilter.getIndependentParameter());
        sb.append(userDefinedSearchUrlFilter.getIndependentParameter());
        sb.append(dashboardFilter.getIndependentParameter());
        sb.append(sortingFilter.getIndependentParameter());
        sb.append(showStatusGeloeschtFilter.getIndependentParameter());
        sb.append(referenzSystemFilter.getIndependentParameter());
        sb.append(kategorieFilter.getIndependentParameter());
        sb.append(prozessbaukastenFilter.getIndependentParameter());
        sb.append(themenklammerFilter.getIndependentParameter());

        return sb.toString();
    }

    public List<SearchFilterRemoveObject> getActiveFilterSelectedValues() {
        return GlobalSearchFilterRemoveObjectHandler.getActiveFilterSelectedValues(this);
    }

    public String removeFilterObject(SearchFilterRemoveObject filterRemoveObject) {
        return GlobalSearchFilterRemoveObjectHandler.removeFilterObject(filterRemoveObject, this);
    }

    public void parseSearchFilter(List<SearchFilter> searchFilters) {
        GlobalSearchFilterConverter.searchFilterListToGlobalSearchFilter(searchFilters, this);
    }

    public List<SearchFilter> convertToSearchFilters() {
        return GlobalSearchFilterConverter.toSearchFilters(this);
    }

    private static Collection<Long> parseAnforderungMeldungIdParamter(String urlParameter) {

        try {
            urlParameter = URLDecoder.decode(urlParameter, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            LOG.error(null, ex);
        }

        Collection<String> input = new HashSet<>();
        if (urlParameter.contains(",")) {
            input = SearchUtil.splitQuery(urlParameter, ",");
        } else {
            input.add(urlParameter);
        }

        Collection<Long> result = new HashSet<>();

        Iterator<String> iterator = input.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            if (RegexUtils.matchesId(next)) {
                result.add(Long.parseLong(next));
            }
        }

        return result;
    }

    private static DashboardUrlParamterDto parseSearchParamter(String urlParameter) {

        try {
            urlParameter = URLDecoder.decode(urlParameter, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            LOG.error(null, ex);
        }

        List<String> input = new ArrayList<>();
        if (urlParameter.contains("~")) {
            input = SearchUtil.splitQuery(urlParameter, "~");
        } else {
            input.add(urlParameter);
        }

        DashboardUrlParamterDto result = new DashboardUrlParamterDto();

        input.forEach(i -> {
            if (i.contains("_id") && i.contains("_v")) { // case Anforderung
                String name = i.substring(0, i.indexOf("_"));
                String version = i.substring(i.indexOf("_v") + 2, i.indexOf("_id"));
                String id = i.substring(i.indexOf("_id") + 3);
                SearchResultDTO sr = new SearchResultDTO(name);
                sr.setId(Long.parseLong(id));
                sr.setVersion(version);
                result.addAnforderungId(Long.parseLong(id));
            } else if (i.contains("_id")) {
                String name = i.substring(0, i.indexOf("_"));
                String id = i.substring(i.indexOf("_id") + 3);
                SearchResultDTO sr = new SearchResultDTO(name);
                sr.setId(Long.parseLong(id));
                result.addMeldungId(Long.parseLong(id));
            } else {
                result.addQuery(new SearchResultDTO(i));
            }
        });

        return result;
    }

}
