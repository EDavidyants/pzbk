package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.dashboard.DashboardResultWithIDs;
import de.interfaceag.bmw.pzbk.dashboard.DashboardStatusService;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SearchFilter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.UrlEncoding;
import de.interfaceag.bmw.pzbk.entities.UserDefinedSearch;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import de.interfaceag.bmw.pzbk.filter.DashboardAnforderungIdFilter;
import de.interfaceag.bmw.pzbk.filter.DashboardMeldungIdFilter;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.UrlEncodingService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.session.UserDefinedSearchApi;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerService;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class GlobalSearchFacade implements Serializable {

    @Inject
    private Session session;

    @Inject
    private AnforderungListService anforderungListService;
    @Inject
    private ExcelDownloadService excelDownloadService;
    @Inject
    private DashboardStatusService dashboardStatusService;
    @Inject
    private TteamService tteamService;
    @Inject
    private GlobalSearchService globalSearchService;
    @Inject
    private UserDefinedSearchApi userDefinedSearchApi;
    @Inject
    private UrlEncodingService urlEncodingService;
    @Inject
    private ProzessbaukastenReadService prozessbaukastenReadService;
    @Inject
    private ThemenklammerService themenklammerService;

    // -------------- api methods ----------------------------------------------
    public GlobalSearchViewData getViewData(UrlParameter urlParameter) {

        List<SensorCoc> sensorCocForUser = getSensorCocs();
        List<Tteam> tteamForUser = getTteams();
        List<FestgestelltIn> festgestelltIns = getFestgestelltIns();
        List<Modul> module = getModule();
        List<ModulSeTeam> modulSeTeams = getModulSeTeams();
        List<Derivat> derivate = getDerivate();
        List<ProzessbaukastenFilterDto> prozessbaukaesten = prozessbaukastenReadService.getAllFilterDto();
        Collection<ThemenklammerDto> themenklammern = getThemenklammern();

        urlParameter = modifyUrlParameterForGeneratedLinks(urlParameter);

        GlobalSearchFilter searchFilter = new GlobalSearchFilter(
                urlParameter, sensorCocForUser, tteamForUser,
                module, modulSeTeams, derivate, prozessbaukaesten, festgestelltIns, themenklammern);

        Set<Rolle> userRoles = getUserRoles();

        GlobalSearchViewPermission viewPermission = new GlobalSearchViewPermission(userRoles);

        GlobalSearchResult searchResult = getSearchResultsForFilter(searchFilter);

        Optional<Long> id = searchFilter.getUserDefinedSearchUrlFilter().getId();
        if (id.isPresent()) {
            if (!loadUserDefinedSearch(id.get()).stream().filter(s -> s.getFilterType().equals(SearchFilterType.URL_ANFORDERUNG) || s.getFilterType().equals(SearchFilterType.URL_MELDUNG))
                    .findAny().isPresent()) {
                searchFilter.getUserDefinedSearchUrlFilter().setActive(Boolean.FALSE);
            }
        }

        List<UserDefinedSearch> userDefinedSearchByMitarbeiter = userDefinedSearchApi.getUserDefinedSearchByMitarbeiter();

        return new GlobalSearchViewData(searchFilter, viewPermission, searchResult, userDefinedSearchByMitarbeiter);
    }

    private Collection<ThemenklammerDto> getThemenklammern() {
        return themenklammerService.getAll();
    }

    public List<SearchResultDTO> getAllSearchResultsForFilter(GlobalSearchFilter filter) {
        return globalSearchService.getAllSearchResultsWithoutImage(filter, session.getUser());
    }

    // ----------- user defined search -----------------------------------------
    public List<UserDefinedSearch> addSelectionAsUserDefinedSearch(List<SearchResultDTO> selectedSearchResult, UserDefinedSearch newUserDefinedSearch) {
        if (newUserDefinedSearch.getName() != null && newUserDefinedSearch.getName().length() != 0) {
            if (userDefinedSearchApi.getUserDefinedSearchByNameAndMitarbeiter(newUserDefinedSearch.getName()).isEmpty()) {
                List<Long> anforderungIds = new ArrayList<>();
                List<Long> meldungIds = new ArrayList<>();
                selectedSearchResult.stream().filter(sr -> sr.getTyp().equals("Anforderung")).forEach(s -> anforderungIds.add(s.getId()));
                selectedSearchResult.stream().filter(sr -> sr.getTyp().equals("Meldung")).forEach(s -> meldungIds.add(s.getId()));
                List<SearchFilter> searchFilterList = new ArrayList<>();
                searchFilterList.add(new SearchFilter(SearchFilterType.URL_ANFORDERUNG, anforderungIds));
                searchFilterList.add(new SearchFilter(SearchFilterType.URL_MELDUNG, meldungIds));
                newUserDefinedSearch.setSearchFilterList(searchFilterList);
                userDefinedSearchApi.saveUserDefinedSearch(newUserDefinedSearch);
            } else {
                FacesContext.getCurrentInstance().addMessage("saveSearchError",
                        new FacesMessage(
                                FacesMessage.SEVERITY_ERROR,
                                "Es existiert bereits eine Suche mit diesem Namen.",
                                "")
                );
            }
        } else {
            FacesContext.getCurrentInstance().addMessage("saveSearchError",
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR,
                            "Bitte geben Sie einen Namen für die zu speichernde Suche ein.",
                            "")
            );
        }
        return userDefinedSearchApi.getUserDefinedSearchByMitarbeiter();

    }

    public void updateUserDefinedSearch(UserDefinedSearch userDefinedSearch) {
        userDefinedSearchApi.updateUserDefinedSearch(userDefinedSearch);
    }

    public void deleteUserDefinedSearch(UserDefinedSearch userDefinedSearch) {
        userDefinedSearchApi.removeUserDefinedSearch(userDefinedSearch);
    }

    private List<SearchFilter> loadUserDefinedSearch(Long userDefinedSearchId) {
        UserDefinedSearch userDefinedSearchById = userDefinedSearchApi.getUserDefinedSearchById(userDefinedSearchId);
        List<SearchFilter> searchFilterList = userDefinedSearchById.getSearchFilterList();
        return searchFilterList;
    }

    public List<UserDefinedSearch> addUserDefinedSearch(UserDefinedSearch newUserDefinedSearch) {
        if (newUserDefinedSearch.getName() != null && newUserDefinedSearch.getName().length() != 0) {
            if (userDefinedSearchApi.getUserDefinedSearchByNameAndMitarbeiter(newUserDefinedSearch.getName()).isEmpty()) {
                userDefinedSearchApi.saveUserDefinedSearch(newUserDefinedSearch);
            }

        }

        return userDefinedSearchApi.getUserDefinedSearchByMitarbeiter();
    }

    // ------------ private methods --------------------------------------------
    private GlobalSearchResult getSearchResultsForFilter(GlobalSearchFilter filter) {

        if (filter.getDashboardFilter().isActive()) {
            int step = filter.getDashboardFilter().getStep();
            DashboardResultWithIDs dashboardResultForStep = dashboardStatusService.getDashboardResultForStep(step);
            Set<Long> meldungIds = dashboardResultForStep.getMeldungIds();
            Set<Long> anforderungIds = dashboardResultForStep.getAnforderungIds();
            filter.setDashboardMeldungIdFilter(new DashboardMeldungIdFilter(meldungIds));
            filter.setDashboardAnforderungIdFilter(new DashboardAnforderungIdFilter(anforderungIds));
        }

        if (filter.getUserDefinedSearchUrlFilter().isActive()) {
            Optional<Long> id = filter.getUserDefinedSearchUrlFilter().getId();
            if (id.isPresent()) {
                List<SearchFilter> loadUserDefinedSearch = loadUserDefinedSearch(id.get());
                filter.parseSearchFilter(loadUserDefinedSearch);
            }
        }

        return globalSearchService.getGlobalSearchResultWithImage(filter, session.getUser());
    }

    private List<SensorCoc> getSensorCocs() {
        return anforderungListService.initSensorCocList();
    }

    private List<Tteam> getTteams() {
        return tteamService.getAllTteamsSortByName();
    }

    private List<FestgestelltIn> getFestgestelltIns() {
        return anforderungListService.getAllFestgestelltIn();
    }

    private List<Modul> getModule() {
        return anforderungListService.getAllModule();
    }

    private List<ModulSeTeam> getModulSeTeams() {
        return anforderungListService.getAllSeTeams();
    }

    private List<Derivat> getDerivate() {
        return anforderungListService.initDerivatList();
    }

    private Set<Rolle> getUserRoles() {
        return session.getUserPermissions().getRoles();
    }

    // ----------------- old methods from controller ---------------------------
    public void downloadExcelExport(GlobalSearchViewData viewData) {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Suchergebnisse");

        List<SearchResultDTO> searchResults = getSelectedSearchResults(viewData);

        anforderungListService.createExcelExportForAnforderungListView(searchResults, sheet);

        excelDownloadService.downloadExcelExport("Suchergebnisse", workbook);
    }

    public List<SearchResultDTO> getSelectedSearchResults(GlobalSearchViewData viewData) {
        List<SearchResultDTO> searchResults;

        if (viewData.isAllSelected()) {
            searchResults = getAllSearchResultsForFilter(viewData.getSearchFilter());
        } else {
            searchResults = viewData.getSearchResults().stream()
                    .filter(element -> element.isSelected())
                    .collect(Collectors.toList());
        }

        viewData.setSelectedSearchResults(searchResults);

        return searchResults;
    }

    public String updateUrl(GlobalSearchViewData viewData) {

        List<SearchResultDTO> selectedSearchResults = getSelectedSearchResults(viewData);

        return anforderungListService.updateUrl(selectedSearchResults);
    }

    private UrlParameter modifyUrlParameterForGeneratedLinks(UrlParameter urlParameter) {
        Optional<String> pUrlParameter = urlParameter.getValue("p");
        if (pUrlParameter.isPresent()) {
            UrlEncoding urlEncodingByUrlId = urlEncodingService.getUrlEncodingByUrlId(pUrlParameter.get());
            String urlParameter1 = urlEncodingByUrlId.getUrlParameter();
            String urlParameterReplacement = urlParameter1.replace("?query=", "");
            urlParameter.addOrReplaceParamter("query", urlParameterReplacement);
        }
        return urlParameter;
    }

}
