package de.interfaceag.bmw.pzbk.globalsearch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class GlobalSearchResult implements Serializable {

    private final long count;

    private final List<SearchResultDTO> searchResults;
    private List<SearchResultDTO> selectedSearchResults;

    public GlobalSearchResult(long count, List<SearchResultDTO> searchResults) {
        this.count = count;
        this.searchResults = searchResults;
        this.selectedSearchResults = new ArrayList<>();
    }

    public GlobalSearchResult(long count, List<SearchResultDTO> searchResults, List<SearchResultDTO> selectedSearchResults) {
        this.count = count;
        this.searchResults = searchResults;
        this.selectedSearchResults = selectedSearchResults;
    }

    public long getCount() {
        return count;
    }

    public List<SearchResultDTO> getSearchResults() {
        return searchResults;
    }

    public List<SearchResultDTO> getSelectedSearchResults() {
        return selectedSearchResults;
    }

    public void setSelectedSearchResults(List<SearchResultDTO> selectedSearchResults) {
        this.selectedSearchResults = selectedSearchResults;
    }

}
