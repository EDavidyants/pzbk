package de.interfaceag.bmw.pzbk.globalsearch.dto;

import de.interfaceag.bmw.pzbk.globalsearch.SearchResultDTO;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DashboardUrlParamterDto implements Serializable {

    private Set<Long> anforderungIds;
    private Set<Long> meldungIds;
    private Set<SearchResultDTO> queries;

    // ---------- constructors ----------
    public DashboardUrlParamterDto() {
        anforderungIds = new HashSet<>();
        meldungIds = new HashSet<>();
        queries = new HashSet<>();
    }

    // ---------- methods --------------------
    public void addMeldungId(Long newMeldungId) {
        if (newMeldungId != null) {
            meldungIds.add(newMeldungId);
        }
    }

    public void addAnforderungId(Long newAnforderungId) {
        if (newAnforderungId != null) {
            anforderungIds.add(newAnforderungId);
        }
    }

    public void addQuery(SearchResultDTO query) {
        if (query != null) {
            queries.add(query);
        }
    }

    public Set<Long> getAnforderungIds() {
        return anforderungIds;
    }

    public void setAnforderungIds(Set<Long> anforderungIds) {
        this.anforderungIds = anforderungIds;
    }

    public Set<Long> getMeldungIds() {
        return meldungIds;
    }

    public void setMeldungIds(Set<Long> meldungIds) {
        this.meldungIds = meldungIds;
    }

    public Set<SearchResultDTO> getQueries() {
        return queries;
    }


    public void setQueries(Set<SearchResultDTO> queries) {
        this.queries = queries;
    }

}
