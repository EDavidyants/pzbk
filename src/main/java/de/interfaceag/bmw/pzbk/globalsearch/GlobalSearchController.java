package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.entities.UserDefinedSearch;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.filter.SearchFilterRemoveObject;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Named
@ViewScoped
public class GlobalSearchController implements Serializable {

    @Inject
    private GlobalSearchFacade facade;

    private GlobalSearchViewData viewData;

    // ------------ init -------------------------------------------------------
    @PostConstruct
    public void init() {
        UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();
        viewData = facade.getViewData(urlParameter);

    } // end of init()

    // ------------  methods ---------------------------------------------------
    // TODO refactor service methods to directly receive a workbook instance instead of sheet
    public void downloadExcelExport() {
        facade.downloadExcelExport(viewData);
    }

    public String handleQueryInputAndFilter() {
        return filter();
    }

    public void updateSelectedSearchResult() {
        getViewData().updateSelectedSearchResult();
    }

    public void updateSelectedSearchResult(SearchResultDTO searchResult) {
        getViewData().updateSelectedSearchResult(searchResult);
    }

    public void updateUrl() {
        getViewData().setUrl(facade.updateUrl(getViewData()));
    }

    public String nextPage() {
        getFilter().getPageUrlFilter().incrementPageId();
        return filter();
    }

    public String previousPage() {
        getFilter().getPageUrlFilter().decrementPageId();
        return filter();
    }

    public int getPage() {
        return getFilter().getPageUrlFilter().getPageId();
    }

    public String deleteUserDefinedSearch(UserDefinedSearch uds) {
        facade.deleteUserDefinedSearch(uds);
        return filter();
    }

    public GlobalSearchFilter getFilter() {
        return getViewData().getSearchFilter();
    }

    public String filter() {
        return getFilter().getUrl();
    }

    public String reset() {
        return getFilter().getResetUrl();
    }

    public GlobalSearchViewData getViewData() {
        return viewData;
    }

    public void addSelectionAsUserDefinedSearch() {
        updateSelectedSearchResultAllSelected();
        UserDefinedSearch newUserDefinedSearch = getViewData().getNewUserDefinedSearch();
        getViewData().setUserDefinedSearches(facade.addSelectionAsUserDefinedSearch(getViewData().getSelectedSearchResults(), newUserDefinedSearch));
    }

    public String loadUserDefinedSearch(UserDefinedSearch userDefinedSearch) {
        if (userDefinedSearch == null) {
            return "";
        }
        getFilter().getUserDefinedSearchUrlFilter().setAttributeValue(userDefinedSearch.getId());
        return filter();
    }

    public void addUserDefinedSearch() {
        UserDefinedSearch newUserDefinedSearch = getViewData().getNewUserDefinedSearch();
        newUserDefinedSearch.setSearchFilterList(getFilter().convertToSearchFilters());
        getViewData().setUserDefinedSearches(facade.addUserDefinedSearch(newUserDefinedSearch));
    }

    public List<SearchResultDTO> getSearchResult() {
        return getViewData().getSearchResults();
    }

    public String getUrl() {
        return getViewData().getUrl();
    }

    public void setUrl(String url) {
        getViewData().setUrl(url);
    }

    public List<SearchResultDTO> getSelectedSearchResult() {
        return getViewData().getSelectedSearchResults();
    }

    public void setSelectedSearchResult(List<SearchResultDTO> selectedSearchResult) {
        getViewData().setSelectedSearchResults(selectedSearchResult);
    }

    public String getSortBy() {
        return getViewData().getSortBy();
    }

    public void setSortBy(String sortBy) {
        this.getViewData().setSortBy(sortBy);
    }

    public List<SearchResultDTO> getSearchQueries() {
        return getFilter().getQueryFilter().getQueries();
    }

    public void setSearchQueries(List<SearchResultDTO> searchQueries) {
        getFilter().getQueryFilter().setQueries(searchQueries);
    }

    public int getAnzahlTreffer() {
        return getViewData().getAnzahlTreffer();
    }

    public boolean isAllSelected() {
        return getViewData().isAllSelected();
    }

    public void setAllSelected(boolean allSelected) {
        getViewData().setAllSelected(allSelected);
    }

    public GlobalSearchViewPermission getViewPermission() {
        return getViewData().getViewPermission();
    }

    public String goToAnforderungDerivatZuordnenView() {
        updateSelectedSearchResultAllSelected();
        StringBuilder sb = new StringBuilder(Page.ZUORDNUNG.getUrl()).append("?faces-redirect=true");
        sb.append("&id=");
        getSelectedSearchResult().stream()
                .filter(sr -> sr.getStatus().equals(Status.A_FREIGEGEBEN.getStatusBezeichnung()))
                .forEach(sr -> {
                    if (sr.getId() != null) {
                        sb.append(sr.getId());
                        sb.append(" ");
                    }
                });
        return sb.toString();
    }

    public void updateSelectedSearchResultAllSelected() {
        if (getViewData().isAllSelected()) {
            getViewData().setSelectedSearchResults(facade.getAllSearchResultsForFilter(getFilter()));
        }
    }

    public String removeFilter(SearchFilterRemoveObject searchFilter) {
        return getFilter().removeFilterObject(searchFilter);
    }

    public boolean isFirstResultPage() {
        int currentPageId = getFilter().getPageUrlFilter().getPageId();
        return currentPageId == 0;
    }

    public boolean isLastResultPage() {
        int totalResults = (int) getViewData().getNumberOfResults();
        int numberPerPage = getViewData().getAnzahlTreffer();
        // pageId starts with 0
        int currentPageId = getFilter().getPageUrlFilter().getPageId();
        int resultsLeft = totalResults - numberPerPage * (currentPageId + 1);
        return resultsLeft <= 0;
    }

    public long getNumberOfResults() {
        if (getViewData().isAllSelected()) {
            return getViewData().getNumberOfResults();
        } else {
            return getViewData().getSearchResults().stream()
                    .filter(element -> element.isSelected())
                    .count();
        }
    }

}
