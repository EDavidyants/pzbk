package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class GlobalSearchViewPermission implements Serializable {

    // aktion menu 
    private final EditPermission anforderungZuordnenButton;

    public GlobalSearchViewPermission(Set<Rolle> userRoles) {

        this.anforderungZuordnenButton = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN)
                .authorizeWriteForRole(Rolle.ANFORDERER)
                .compareTo(userRoles).get();
    }

    public boolean getAnforderungZuordnenButton() {
        return anforderungZuordnenButton.hasRightToEdit();
    }

}
