package de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sl
 */
final class ProzessbaukastenPermissionQueryBuilder {

    private ProzessbaukastenPermissionQueryBuilder() {
    }

    static QueryPartDTO getPermissionQueryPart(UserPermissions<BerechtigungDto> userPermissions) {
        Set<Rolle> roles = userPermissions.getRoles();

        if (hasNoRestriction(roles)) {
            return getNoRestrictionQueryPart();
        } else if (isTteamMitglied(roles)) {
            List<Long> tteamIds = userPermissions.getTteamIdsForTteamMitgliedLesend();
            tteamIds.addAll(userPermissions.getTteamIdsForTteamMitgliedSchreibend());
            return getPermissionQueryForTteamMitglied(tteamIds);
        } else {
            return getAccessDeniedQueryPart();
        }
    }

    private static boolean hasNoRestriction(Collection<Rolle> roles) {
        return roles.contains(Rolle.ADMIN)
                || roles.contains(Rolle.T_TEAMLEITER)
                || roles.contains(Rolle.TTEAM_VERTRETER)
                || roles.contains(Rolle.ANFORDERER);
    }

    private static boolean isTteamMitglied(Collection<Rolle> roles) {
        return roles.contains(Rolle.TTEAMMITGLIED);
    }

    private static QueryPartDTO getNoRestrictionQueryPart() {
        return new QueryPartDTO();
    }

    private static QueryPartDTO getAccessDeniedQueryPart() {
        return new QueryPartDTO(" AND 1 = 0 ");
    }

    private static QueryPartDTO getPermissionQueryForTteamMitglied(Collection<Long> tteamIds) {

        if (tteamIds == null || tteamIds.isEmpty()) {
            return getAccessDeniedQueryPart();
        }

        QueryPartDTO result = new QueryPartDTO(" AND p.tteam.id IN :tteamMitgliedTteamIds ");
        result.put("tteamMitgliedTteamIds", tteamIds);
        return result;
    }

}
