package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Kategorie;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.ReferenzSystem;
import de.interfaceag.bmw.pzbk.enums.Sorting;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.filter.BooleanSearchFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.IdUrlFilter;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.QueryUrlFilter;
import de.interfaceag.bmw.pzbk.filter.ShowGeloeschteElementeFilter;
import de.interfaceag.bmw.pzbk.filter.SingleValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ThemenklammerFilter;
import de.interfaceag.bmw.pzbk.filter.VonBisDateSearchFilter;
import de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenSearchService;
import de.interfaceag.bmw.pzbk.services.ImageService;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import de.interfaceag.bmw.pzbk.shared.utils.SearchUtil;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author sl
 */
@Stateless
public class GlobalSearchService implements Serializable {

    private static final int NUMBEROFRESULTSPERPAGE = 25;
    private static final String AND_1_0 = " AND 1 = 0";
    private static final String DESC = " DESC";

    @Inject
    private ImageService imageService;

    @Inject
    private ProzessbaukastenSearchService prozessbaukastenSearchService;

    @Inject
    private QueryPartService queryPartService;

    public List<AutocompleteSearchResult> getGlobalSearchAutocompleteResult(String query, Mitarbeiter currentUser) {
        List<AutocompleteSearchResult> result = new ArrayList<>();
        for (Type type : Type.values()) {
            result.addAll(getAutocompleteSearchResultForType(query, currentUser, type));
        }
        return result;

    }

    public List<AutocompleteSearchResult> getAnforderungAutocompleteSearchResult(String query, Mitarbeiter currentUser) {
        return getAutocompleteSearchResultForType(query, currentUser, Type.ANFORDERUNG);
    }

    public GlobalSearchResult getGlobalSearchResultWithImage(GlobalSearchFilter filter,
                                                             Mitarbeiter currentUser) {

        int offset = filter.getPageUrlFilter().getPageId() * NUMBEROFRESULTSPERPAGE;

        Map<Type, Long> searchResultCountForTypes = getSearchResultCountForTypes(filter, currentUser);
        long searchResultCount = getSearchResultCount(searchResultCountForTypes);
        List<SearchResultDTO> result = getSearchResults(filter, currentUser, Boolean.TRUE, offset, searchResultCountForTypes, 25);
        result.forEach(dto -> dto.setSelected(true));

        return new GlobalSearchResult(searchResultCount, result);

    }

    private long getSearchResultCount(Map<Type, Long> searchResultCountForTypes) {
        return searchResultCountForTypes.values().stream().mapToLong(Long::longValue).sum();
    }

    private Map<Type, Long> getSearchResultCountForTypes(GlobalSearchFilter filter, Mitarbeiter currentUser) {
        List<Type> types = getTypesFromFilter(filter);
        EnumMap<Type, Long> result = new EnumMap<>(Type.class);
        types.forEach(type -> result.put(type, getSearchResultCountPerType(filter, currentUser, type)));
        return result;
    }

    private long getSearchResultCountPerType(GlobalSearchFilter filter, Mitarbeiter currentUser, Type type) {

        if (type.isProzessbaukasten()) {
            return prozessbaukastenSearchService.getSearchResultCount(filter);
        }

        QueryPartDTO fromStatement = SearchUtil.buildFromStatementForCount(type, filter.getDerivatFilter().isActive());
        QueryPartDTO conditionsStatement = buildConditionsStatement(currentUser, filter, type);

        QueryPartDTO qp = new QueryPartDTO("SELECT COUNT(DISTINCT a.id)").merge(fromStatement).merge(conditionsStatement);

        return queryPartService.getSingleResultAsLong(qp);
    }

    private List<SearchResultDTO> getSearchResults(GlobalSearchFilter filter, Mitarbeiter currentUser,
                                                   boolean withImage, int offset, Map<Type, Long> searchResultCountForTypes, int limit) {

        Map<Type, Integer> offsetForTypes = getOffsetForTypes(searchResultCountForTypes, offset, getTypesFromFilter(filter));

        List<SearchResultDTO> result = new ArrayList<>();

        Iterator<Map.Entry<Type, Integer>> iterator = offsetForTypes.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Type, Integer> entry = iterator.next();
            Type type = entry.getKey();
            Integer value = entry.getValue();
            result.addAll(getSearchResultForType(filter, currentUser, withImage, value, type, limit));
        }
        return result;
    }

    public List<SearchResultDTO> getAllSearchResultsWithoutImage(GlobalSearchFilter filter, Mitarbeiter currentUser) {
        List<SearchResultDTO> result = new ArrayList<>();
        List<Type> types = getTypesFromFilter(filter);
        for (Type type : types) {
            result.addAll(getSearchResultForType(filter, currentUser, Boolean.FALSE, 0, type, 0));
        }
        return result;
    }

    private List<AutocompleteSearchResult> getAutocompleteSearchResultForType(String query, Mitarbeiter currentUser, Type type) {

        if (type.isProzessbaukasten()) {
            return prozessbaukastenSearchService.getProzessbaukastenAutocompleteResult(query);
        }

        QueryUrlFilter queryUrlFilter = new QueryUrlFilter(query);

        QueryPartDTO selectStatement = SearchUtil.buildSelectStatementForAnforderungListQuery(type, Boolean.FALSE);
        QueryPartDTO fromStatement = SearchUtil.buildFromStatementForSearchResult(type, Boolean.FALSE, Boolean.FALSE);

        List<Long> anforderungIdsForZakId = getAnforderungIdsForZakId(query);
        QueryPartDTO queryConditions = getQueryForStringQuery(queryUrlFilter, type, anforderungIdsForZakId);

        QueryPartDTO permissionResult = SearchUtil.buildPermissionQuery(type, currentUser.getId(), currentUser.getQNumber());

        QueryPartDTO groupBy = SearchUtil.buildGroupByClause(type, false);

        QueryPartDTO qp = selectStatement.merge(fromStatement).merge(queryConditions).merge(permissionResult).merge(groupBy);

        List<Object[]> result = queryPartService.getResultListWithMaxResults(qp.getQueryWithSorting(), qp.getParameterMap(), 10, 0);
        return parseAutocompleteSearchResult(result, type);
    }

    private List<SearchResultDTO> getSearchResultForType(GlobalSearchFilter filter, Mitarbeiter currentUser,
                                                         boolean withImage, int offset, Type type, int limit) {

        if (type.isProzessbaukasten()) {
            return prozessbaukastenSearchService.getSearchResult(filter, offset, limit);
        }

        QueryPartDTO selectStatement = SearchUtil.buildSelectStatementForAnforderungListQuery(type, withImage);
        QueryPartDTO fromStatement = SearchUtil.buildFromStatementForSearchResult(type, filter.getDerivatFilter().isActive(), withImage);
        QueryPartDTO conditionsStatement = buildConditionsStatement(currentUser, filter, type, withImage);

        QueryPartDTO qp = selectStatement.merge(fromStatement).merge(conditionsStatement);

        List<Object[]> result = queryPartService.getResultListWithMaxResults(qp.getQueryWithSorting(), qp.getParameterMap(), limit, offset);
        return parseSearchResult(result, type, withImage);
    }

    private Map<Type, Integer> getOffsetForTypes(Map<Type, Long> searchResultCountForTypes, int offset, List<Type> types) {
        EnumMap<Type, Integer> result = new EnumMap<>(Type.class);
        List<Type> typeList = new ArrayList<>(types);

        if (!typeList.isEmpty()) {
            long remainingItemCount = NUMBEROFRESULTSPERPAGE + (long) offset;

            if (offset < 0) {
                offset = 0;
            }

            if (typeList.contains(Type.PROZESSBAUKASTEN)) {
                result.put(Type.PROZESSBAUKASTEN, offset);
                remainingItemCount = remainingItemCount - searchResultCountForTypes.get(Type.PROZESSBAUKASTEN);
                searchResultCountForTypes.remove(Type.PROZESSBAUKASTEN);
                typeList.remove(Type.PROZESSBAUKASTEN);
            } else if (typeList.contains(Type.ANFORDERUNG)) {
                result.put(Type.ANFORDERUNG, offset);
                remainingItemCount = remainingItemCount - searchResultCountForTypes.get(Type.ANFORDERUNG);
                searchResultCountForTypes.remove(Type.ANFORDERUNG);
                typeList.remove(Type.ANFORDERUNG);
            } else if (typeList.contains(Type.MELDUNG)) {
                result.put(Type.MELDUNG, offset);
                remainingItemCount = remainingItemCount - searchResultCountForTypes.get(Type.MELDUNG);
                searchResultCountForTypes.remove(Type.MELDUNG);
                typeList.remove(Type.MELDUNG);
            }

            // recursive call
            if (remainingItemCount > 0 && remainingItemCount < NUMBEROFRESULTSPERPAGE) {
                result.putAll(getOffsetForTypes(searchResultCountForTypes, 0, typeList));
            } else if (remainingItemCount > 0) {
                result.putAll(getOffsetForTypes(searchResultCountForTypes, (int) remainingItemCount - NUMBEROFRESULTSPERPAGE, typeList));
            }
        }
        return result;
    }

    private static List<Type> getTypesFromFilter(GlobalSearchFilter filter) {
        List<Type> types = filter.getTypeFilter().getAsEnumList();
        if (types.isEmpty()) {
            types = Arrays.asList(Type.values());
        }
        return types;
    }

    private List<SearchResultDTO> insertDefaultImage(List<SearchResultDTO> searchResult) {
        byte[] defaultImage = imageService.loadDefaultImage();
        searchResult.stream().filter(s -> s.getImage() == null).forEach(s -> s.setImage(defaultImage));
        return searchResult;
    }

    private List<SearchResultDTO> parseSearchResult(List<Object[]> searchResult, Type type, boolean withImage) {

        List<SearchResultDTO> result;

        switch (type) {
            case MELDUNG:
                result = SearchResultDtoConverter.parseMeldungSearchResults(searchResult, withImage);
                break;
            case ANFORDERUNG:
                if (withImage) {
                    List<SearchResultDTO> anforderungResult = SearchResultDtoConverter.parseAnforderungSearchResults(searchResult, withImage);
                    result = getDefaultImagesForAnforderungSearchResult(anforderungResult);
                } else {
                    result = SearchResultDtoConverter.parseAnforderungSearchResults(searchResult, withImage);
                }
                break;
            default:
                result = new ArrayList<>();
                break;
        }

        if (withImage && result != null) {
            return insertDefaultImage(result);
        }

        return result;
    }

    private List<AutocompleteSearchResult> parseAutocompleteSearchResult(List<Object[]> searchResult, Type type) {
        switch (type) {
            case MELDUNG:
                return SearchResultDtoConverter.parseMeldungAutocompleteSearchResults(searchResult);
            case ANFORDERUNG:
                return SearchResultDtoConverter.parseAnforderungAutocompleteSearchResults(searchResult);
            default:
                return Collections.emptyList();
        }
    }

    private List<SearchResultDTO> getDefaultImagesForAnforderungSearchResult(List<SearchResultDTO> anforderungResult) {
        if (anforderungResult != null && !anforderungResult.isEmpty()) {
            addDefaultImagesAsSearchResultDto(anforderungResult);
        }
        return anforderungResult;
    }

    private void addDefaultImagesAsSearchResultDto(List<SearchResultDTO> anforderungResult) {
        List<Long> idList = anforderungResult.stream().filter(sr -> sr.getImage() == null && sr.getId() != null).map(SearchResultDTO::getId).collect(Collectors.toList());
        if (!idList.isEmpty()) {
            Map<Long, byte[]> defaultImages = getDefaultImagesForAnforderungList(idList);
            //TODO refactor this!!
            defaultImages.forEach((key, value) -> anforderungResult.stream().filter(sr -> sr.getId().equals(key)).findFirst().ifPresent(searchResult -> searchResult.setImage(value)));
        }

        idList = anforderungResult.stream().map(SearchResultDTO::getId).collect(Collectors.toList());
        if (!idList.isEmpty()) {
            Map<Long, List<String>> modulNames = getModulStringAnforderungList(idList);
            anforderungResult.forEach(r -> r.setModule(modulNames.containsKey(r.getId()) ? modulNames.get(r.getId()) : new ArrayList<>()));
        }
    }

    private Map<Long, List<String>> getModulStringAnforderungList(List<Long> anforderungIds) {
        Map<Long, List<String>> result = new HashMap<>();
        if (anforderungIds == null || anforderungIds.isEmpty()) {
            return result;
        }

        QueryPartDTO qp = new QueryPartDTO("SELECT DISTINCT a.id, af.modulSeTeam.elternModul.name FROM Anforderung a ");
        qp.append("LEFT JOIN a.anfoFreigabeBeiModul af ");
        qp.append("WHERE a.id IN :idList");
        qp.put("idList", anforderungIds);

        List<Object[]> queryResult = (List<Object[]>) queryPartService.getResultList(qp.getQuery(), qp.getParameterMap());
        queryResult.forEach(r -> {
            Long key = (Long) r[0];
            if (result.containsKey(key)) {
                List<String> modulList = result.get(key);
                modulList.add((String) r[1]);
                result.replace(key, modulList);
            } else {
                List<String> modulList = new ArrayList<>();
                modulList.add((String) r[1]);
                result.put(key, modulList);
            }
        });
        return result;
    }

    private Map<Long, byte[]> getDefaultImagesForAnforderungList(List<Long> anforderungIds) {
        Map<Long, byte[]> thumbnail2AnfId = new HashMap<>();
        if (anforderungIds == null || anforderungIds.isEmpty()) {
            return thumbnail2AnfId;
        }

        QueryPartDTO qp = new QueryPartDTO("SELECT DISTINCT am.anforderung.id, b.thumbnailImage");

        qp.append(" FROM AnforderungFreigabeBeiModulSeTeam am ");
        qp.append("INNER JOIN am.modulSeTeam AS se ");
        qp.append("INNER JOIN se.bild AS b ");
        qp.append("WHERE am.anforderung.id IN :idList ");
        qp.append("ORDER BY b.id");
        qp.put("idList", anforderungIds);

        List<Object[]> results = (List<Object[]>) queryPartService.getResultList(qp.getQuery(), qp.getParameterMap());
        results.forEach(result -> thumbnail2AnfId.putIfAbsent((Long) result[0], (byte[]) result[1]));
        return thumbnail2AnfId;
    }

    private QueryPartDTO buildConditionsStatement(Mitarbeiter currentUser,
                                                  GlobalSearchFilter filter, Type type, boolean withImage) {

        List<Long> anforderungIdsForZakId = getAnforderungIdsForZakId(filter.getQueryFilter());
        QueryPartDTO queryConditions = getQueryForStringQuery(filter.getQueryFilter(), type, anforderungIdsForZakId);
        QueryPartDTO filterConditions = getQueryForGlobalSearchFilter(filter, type);
        QueryPartDTO datumStatusaenderungFilter = getQueryForDatumStatusaenderungFilter(filter);
        QueryPartDTO permissionResult = SearchUtil.buildPermissionQuery(type, currentUser.getId(), currentUser.getQNumber());
        QueryPartDTO groupBy = SearchUtil.buildGroupByClause(type, withImage);

        return new QueryPartDTO().merge(queryConditions).merge(permissionResult).merge(datumStatusaenderungFilter).merge(filterConditions).merge(groupBy);
    }

    private QueryPartDTO buildConditionsStatement(Mitarbeiter currentUser,
                                                  GlobalSearchFilter filter, Type type) {

        List<Long> anforderungIdsForZakId = getAnforderungIdsForZakId(filter.getQueryFilter());
        QueryPartDTO queryConditions = getQueryForStringQuery(filter.getQueryFilter(), type, anforderungIdsForZakId);
        QueryPartDTO filterConditions = getQueryForGlobalSearchFilter(filter, type);
        QueryPartDTO datumStatusaenderungFilter = getQueryForDatumStatusaenderungFilter(filter);
        QueryPartDTO permissionResult = SearchUtil.buildPermissionQuery(type, currentUser.getId(), currentUser.getQNumber());

        return new QueryPartDTO().merge(queryConditions).merge(permissionResult).merge(datumStatusaenderungFilter).merge(filterConditions);
    }

    private static QueryPartDTO getQueryForStringQuery(QueryUrlFilter queryUrlFilter, Type type, List<Long> anforderungIdsFromZakQuery) {
        QueryPartDTO qp = new QueryPartDTO();

        if (queryUrlFilter.isActive()) {

            List<String> queryItems = queryUrlFilter.getAttributeValues();

            if (queryItems.size() > 1) {
                qp.append(" AND (");
                queryItems.stream().map(String::trim).forEach(si -> {
                    String queryName = Integer.toString(Math.abs(si.hashCode()));
                    if (type.isProzessbaukasten()) {
                        qp.append("LOWER(a.fachId) LIKE LOWER(:query")
                                .append(queryName)
                                .append(") OR LOWER(a.beschreibung) LIKE LOWER(:query")
                                .append(queryName)
                                .append(") OR ");
                    } else {
                        qp.append("LOWER(a.fachId) LIKE LOWER(:query")
                                .append(queryName)
                                .append(") OR LOWER(a.beschreibungAnforderungDe) LIKE LOWER(:query")
                                .append(queryName)
                                .append(") OR LOWER(a.beschreibungStaerkeSchwaeche) LIKE LOWER(:query")
                                .append(queryName)
                                .append(") OR ");
                    }
                    qp.put("query" + queryName, "%" + si.toUpperCase() + "%");
                });
                qp.deleteFrom(qp.getQuery().lastIndexOf('O'));

                if (anforderungIdsFromZakQuery != null && !anforderungIdsFromZakQuery.isEmpty()) {
                    qp.append(" OR a.id IN :filterIdList");
                    qp.put("filterIdList", anforderungIdsFromZakQuery);
                }
                qp.append(")");
            } else {
                qp.append(" AND (LOWER(a.fachId) LIKE LOWER(:query) ");
                if (type.isProzessbaukasten()) {
                    qp.append("OR LOWER(a.beschreibung) LIKE LOWER(:query) ");
                } else {
                    qp.append("OR LOWER(a.beschreibungAnforderungDe) LIKE LOWER(:query)"
                            + " OR LOWER(a.beschreibungStaerkeSchwaeche) LIKE LOWER(:query)"
                    );
                }

                if (anforderungIdsFromZakQuery != null && !anforderungIdsFromZakQuery.isEmpty()) {
                    qp.append(" OR a.id IN :filterIdList");
                    qp.put("filterIdList", anforderungIdsFromZakQuery);
                }
                qp.append(")");
                qp.put("query", "%" + queryItems.get(0).trim().toLowerCase() + "%");
            }
        }

        return qp;
    }

    private static QueryPartDTO getQueryForAnforderungMeldungIdFilter(GlobalSearchFilter filter, Type type) {
        QueryPartDTO qp = new QueryPartDTO();

        IdUrlFilter anforderungIdFilter = filter.getAnforderungIdFilter();
        IdUrlFilter meldungIdFilter = filter.getMeldungIdFilter();

        if (anforderungIdFilter.isActive() && type.isAnforderung() && !anforderungIdFilter.getAttributeValues().isEmpty()) {
            qp.append(" AND a.id IN :anforderungUrlId");
            qp.put("anforderungUrlId", anforderungIdFilter.getAttributeValues());
        } else if (meldungIdFilter.isActive() && type.isMeldung() && !meldungIdFilter.getAttributeValues().isEmpty()) {
            qp.append(" AND a.id IN :meldungUrlId");
            qp.put("meldungUrlId", meldungIdFilter.getAttributeValues());
        } else if (anforderungIdFilter.isActive() || meldungIdFilter.isActive()) {
            qp.append(AND_1_0);
        }
        return qp;
    }

    private static QueryPartDTO getQueryForReferenzsystem(GlobalSearchFilter filter, Type type) {
        QueryPartDTO qp = new QueryPartDTO();

        MultiValueEnumSearchFilter<ReferenzSystem> referenzSystemFilter = filter.getReferenzSystemFilter();

        if (referenzSystemFilter.isActive() && type.isAnforderung() && !referenzSystemFilter.getAsEnumList().isEmpty()) {
            List<Integer> referenzSystemIdList = referenzSystemFilter.getAsEnumList().stream().map(ReferenzSystem::getId).collect(Collectors.toList());
            qp.append(" AND r.referenzSystem IN :referenzSystemList");
            qp.put("referenzSystemList", referenzSystemIdList);
        } else if (referenzSystemFilter.isActive() && !referenzSystemFilter.getAsEnumList().isEmpty()) {
            qp.append(AND_1_0);
        }
        return qp;
    }

    private static QueryPartDTO getQueryForKategorie(GlobalSearchFilter filter, Type type) {
        QueryPartDTO qp = new QueryPartDTO();

        MultiValueEnumSearchFilter<Kategorie> kategorieFilter = filter.getKategorieFilter();

        if (kategorieFilter.isActive() && type.isAnforderung() && !kategorieFilter.getAsEnumList().isEmpty()) {
            List<Integer> kategorieIdList = kategorieFilter.getAsEnumList().stream().map(Kategorie::getId).collect(Collectors.toList());
            qp.append(" AND a.kategorie IN :kategorieList");
            qp.put("kategorieList", kategorieIdList);
        } else if (kategorieFilter.isActive() && !kategorieFilter.getAsEnumList().isEmpty()) {
            qp.append(AND_1_0);
        }
        return qp;
    }

    private static QueryPartDTO getQueryForSensorCocFilter(GlobalSearchFilter globalSearchFilter) {
        QueryPartDTO qp = new QueryPartDTO();
        IdSearchFilter filter = globalSearchFilter.getSensorCocFilter();
        if (filter.isActive()) {
            qp.append(" AND a.sensorCoc.sensorCocId IN :sensorCocList");
            qp.put("sensorCocList", filter.getSelectedIdsAsSet());
        }
        return qp;
    }

    private static QueryPart getQueryForThemenklammerFilter(GlobalSearchFilter globalSearchFilter, Type type) {
        QueryPart queryPart = new QueryPartDTO();
        ThemenklammerFilter filter = globalSearchFilter.getThemenklammerFilter();
        if (filter.isActive() && type.isAnforderung()) {
            queryPart.append(" AND pt.themenklammer.id IN :themenklammerIds");
            queryPart.put("themenklammerIds", filter.getSelectedIdsAsSet());
        } else if (filter.isActive()) {
            queryPart.append(AND_1_0);
        }
        return queryPart;
    }

    private static QueryPartDTO getQueryForFestgestelltInFilter(GlobalSearchFilter globalSearchFilter, Type type) {
        QueryPartDTO qp = new QueryPartDTO();
        IdSearchFilter filter = globalSearchFilter.getFestgestelltInFilter();
        if (filter.isActive()) {
            if (type.isProzessbaukasten()) {
                qp.append(AND_1_0);
            } else {
                qp.append(" AND a.festgestelltIn IN :festgestelltInList");
                qp.put("festgestelltInList", filter.getSelectedIdsAsSet());
            }
        }
        return qp;
    }

    private static QueryPartDTO getQueryForTteamFilter(GlobalSearchFilter globalSearchFilter, Type type) {
        QueryPartDTO qp = new QueryPartDTO();
        IdSearchFilter filter = globalSearchFilter.getTteamFilter();
        if (filter.isActive()) {
            if (type.isMeldung()) {
                qp.append(AND_1_0);
            } else {
                qp.append(" AND a.tteam.id IN :tteamList");
                qp.put("tteamList", filter.getSelectedIdsAsSet());
            }
        }
        return qp;
    }

    private static QueryPartDTO getQueryForDerivatFilter(GlobalSearchFilter globalSearchFilter, Type type) {
        QueryPartDTO qp = new QueryPartDTO();
        IdSearchFilter filter = globalSearchFilter.getDerivatFilter();
        if (filter.isActive()) {
            if (type.isAnforderung()) {
                qp.append(" AND d.zuordnungAnforderungDerivat.derivat.id IN :derivatList");
                qp.put("derivatList", filter.getSelectedIdsAsSet());
            } else {
                qp.append(AND_1_0);
            }
        }
        return qp;
    }

    private static QueryPartDTO getQueryForProzessbaukastenFilter(GlobalSearchFilter globalSearchFilter, Type type) {
        QueryPartDTO qp = new QueryPartDTO();
        IdSearchFilter filter = globalSearchFilter.getProzessbaukastenFilter();
        if (filter.isActive()) {
            if (type.isAnforderung()) {
                qp.append(" AND p.id IN :prozessbaukastenList");
                qp.put("prozessbaukastenList", filter.getSelectedIdsAsSet());
            } else {
                qp.append(AND_1_0);
            }
        }
        return qp;
    }

    private static QueryPartDTO getQueryForPhasenrelevanzFilter(GlobalSearchFilter globalSearchFilter, Type type) {
        QueryPartDTO qp = new QueryPartDTO();
        BooleanSearchFilter filter = globalSearchFilter.getPhasenrelevanzFilter();
        if (filter.isActive()) {
            if (type.isAnforderung()) {
                qp.append(" AND a.phasenbezug IN :phasenbezug");
                qp.put("phasenbezug", filter.getAsBoolean());
            } else {
                qp.append(AND_1_0);
            }
        }
        return qp;
    }

    private static QueryPartDTO getQueryForStatusFilter(GlobalSearchFilter globalSearchFilter, Type type) {
        QueryPartDTO qp = new QueryPartDTO();
        MultiValueEnumSearchFilter<Status> meldungStatusFilter = globalSearchFilter.getMeldungStatusFilter();
        MultiValueEnumSearchFilter<Status> anforderungStatusFilter = globalSearchFilter.getAnforderungStatusFilter();
        MultiValueEnumSearchFilter<ProzessbaukastenStatus> prozessbaukastenStatusFilter = globalSearchFilter.getProzessbaukastenStatusFilter();
        ShowGeloeschteElementeFilter showGeloeschteElementeFilter = globalSearchFilter.getShowStatusGeloeschtFilter();

        if (prozessbaukastenStatusFilter.isActive()) {
            return qp.append(" AND 1=0 ");
        }

        List<Status> statusList = new ArrayList<>();

        if (meldungStatusFilter.isActive() && anforderungStatusFilter.isActive()) {
            statusList = meldungStatusFilter.getAsEnumList();
            statusList.addAll(anforderungStatusFilter.getAsEnumList());
        } else if (meldungStatusFilter.isActive()) {
            statusList = meldungStatusFilter.getAsEnumList();
        } else if (anforderungStatusFilter.isActive()) {
            statusList = anforderungStatusFilter.getAsEnumList();
        }

        if (meldungStatusFilter.isActive() || anforderungStatusFilter.isActive()) {
            qp.append(" AND a.status IN :statusList");
            qp.put("statusList", statusList);
        }

        if (!showGeloeschteElementeFilter.isActive() || !showGeloeschteElementeFilter.getAsBoolean().contains(Boolean.TRUE) && !type.isProzessbaukasten()) {
            List<Status> statusGeloeschtList = Arrays.asList(Status.M_GELOESCHT, Status.A_GELOESCHT);
            qp.append(" AND a.status NOT IN :statusGeloeschtList");
            qp.put("statusGeloeschtList", statusGeloeschtList);
        }

        return qp;
    }

    private static QueryPartDTO getQueryForAenderungsdatumFilter(GlobalSearchFilter globalSearchFilter) {
        QueryPartDTO qp = new QueryPartDTO();
        VonBisDateSearchFilter filter = globalSearchFilter.getAenderungsdatumFilter();
        if (filter.isActive()) {
            switch (filter.getCase()) {
                case VON:
                    qp.append(" AND a.aenderungsdatum >= :aenderungsdatumDateStart");
                    qp.put("aenderungsdatumDateStart", filter.getVon());
                    break;
                case BIS:
                    qp.append(" AND a.aenderungsdatum <= :aenderungsdatumDateEnd");
                    qp.put("aenderungsdatumDateEnd", filter.getBis());
                    break;
                case VONBIS:
                    qp.append(" AND a.aenderungsdatum >= :aenderungsdatumDateStart AND a.aenderungsdatum <= :aenderungsdatumDateEnd");
                    qp.put("aenderungsdatumDateStart", filter.getVon());
                    qp.put("aenderungsdatumDateEnd", filter.getBis());
                    break;
                case NONE:
                default:
                    break;
            }
        }
        return qp;
    }

    private static QueryPartDTO getQueryForErstellungsdatumFilter(GlobalSearchFilter globalSearchFilter) {
        QueryPartDTO qp = new QueryPartDTO();
        VonBisDateSearchFilter filter = globalSearchFilter.getErstellungsdatumFilter();
        if (filter.isActive()) {
            switch (filter.getCase()) {
                case VON:
                    qp.append(" AND a.erstellungsdatum >= :erstellungsdatumDateStart");
                    qp.put("erstellungsdatumDateStart", filter.getVon());
                    break;
                case BIS:
                    qp.append(" AND a.erstellungsdatum <= :erstellungsdatumDateEnd");
                    qp.put("erstellungsdatumDateEnd", filter.getBis());
                    break;
                case VONBIS:
                    qp.append(" AND a.erstellungsdatum >= :erstellungsdatumDateStart AND a.erstellungsdatum <= :erstellungsdatumDateEnd");
                    qp.put("erstellungsdatumDateStart", filter.getVon());
                    qp.put("erstellungsdatumDateEnd", filter.getBis());
                    break;
                case NONE:
                default:
                    break;
            }
        }
        return qp;
    }

    private static QueryPartDTO getQueryForSortingFilter(GlobalSearchFilter globalSearchFilter) {
        StringBuilder sb = new StringBuilder();
        SingleValueEnumSearchFilter<Sorting> filter = globalSearchFilter.getSortingFilter();

        Optional<Sorting> optionalSorting = filter.getAsEnum();
        Sorting sorting;
        if (optionalSorting.isPresent()) {
            sorting = optionalSorting.get();
        } else {
            sorting = Sorting.NAMEDESC;
        }

        switch (sorting) {
            case AENDERUNGSDATUMASC:
                sb.append(" ORDER BY a.aenderungsdatum");
                sb.append(" ASC");
                break;
            case AENDERUNGSDATUMDESC:
                sb.append(" ORDER BY a.aenderungsdatum");
                sb.append(DESC);
                break;
            case NAMEASC:
                sb.append(" ORDER BY LENGTH(a.fachId)");
                sb.append(" ASC");
                sb.append(", a.fachId");
                sb.append(" ASC");
                break;
            case NAMEDESC:
            default:
                sb.append(" ORDER BY LENGTH(a.fachId)");
                sb.append(DESC);
                sb.append(", a.fachId");
                sb.append(DESC);
                break;
        }

        QueryPartDTO qp = new QueryPartDTO();
        qp.setSort(sb.toString());
        return qp;
    }

    private static QueryPartDTO getQueryForDashboardAnforderungIdFilter(GlobalSearchFilter globalSearchFilter, Type type) {
        QueryPartDTO qp = new QueryPartDTO();
        IdSearchFilter filter = globalSearchFilter.getDashboardAnforderungIdFilter();
        IdSearchFilter meldungFilter = globalSearchFilter.getDashboardMeldungIdFilter();
        if (filter.isActive()) {
            if (type.isAnforderung()) {
                qp.append(" AND a.id IN :dashboardAnforderungIdList");
                qp.put("dashboardAnforderungIdList", filter.getSelectedIdsAsSet());
            } else if (!meldungFilter.isActive()) {
                qp.append(AND_1_0);
            }
        }
        return qp;
    }

    private static QueryPartDTO getQueryForDashboardMeldungIdFilter(GlobalSearchFilter globalSearchFilter, Type type) {
        QueryPartDTO qp = new QueryPartDTO();
        IdSearchFilter filter = globalSearchFilter.getDashboardMeldungIdFilter();
        IdSearchFilter anforderungFilter = globalSearchFilter.getDashboardAnforderungIdFilter();
        if (filter.isActive()) {
            if (type.isMeldung()) {
                qp.append(" AND a.id IN :dashboardMeldungdList");
                qp.put("dashboardMeldungdList", filter.getSelectedIdsAsSet());
            } else if (!anforderungFilter.isActive()) {
                qp.append(AND_1_0);
            }
        }
        return qp;
    }

    public static QueryPartDTO getQueryForModulAndSeTeamFilter(GlobalSearchFilter globalSearchFilter, Type type) {
        QueryPartDTO qp = new QueryPartDTO();

        IdSearchFilter modulFilter = globalSearchFilter.getModulFilter();
        IdSearchFilter modulSeTeamFilter = globalSearchFilter.getModulSeTeamFilter();

        if (modulFilter.isActive() || modulSeTeamFilter.isActive()) {
            if (type.isAnforderung()) {
                if (modulFilter.isActive() && modulSeTeamFilter.isActive()) {
                    qp.append(" AND (u.seTeam.elternModul IN :modulList");
                    qp.append(" OR u.seTeam IN :modulSeTeamList)");
                    qp.put("modulList", modulFilter.getSelectedIdsAsSet());
                    qp.put("modulSeTeamList", modulSeTeamFilter.getSelectedIdsAsSet());
                } else if (modulFilter.isActive()) {
                    qp.append(" AND u.seTeam.elternModul.id IN :modulList");
                    qp.put("modulList", modulFilter.getSelectedIdsAsSet());
                } else if (modulSeTeamFilter.isActive()) {
                    qp.append(" AND u.seTeam.id IN :modulSeTeamList");
                    qp.put("modulSeTeamList", modulSeTeamFilter.getSelectedIdsAsSet());
                }
            } else {
                qp.append(AND_1_0);
            }
        }

        return qp;
    }

    private QueryPartDTO getQueryForDatumStatusaenderungFilter(GlobalSearchFilter globalSearchFilter) {

        QueryPartDTO qp = new QueryPartDTO();
        VonBisDateSearchFilter datumStatusaenderungFilter = globalSearchFilter.getDatumStatusaenderungFilter();
        MultiValueEnumSearchFilter<Status> meldungStatusFilter = globalSearchFilter.getMeldungStatusFilter();
        MultiValueEnumSearchFilter<Status> anforderungStatusFilter = globalSearchFilter.getAnforderungStatusFilter();

        if (datumStatusaenderungFilter.isActive() && (meldungStatusFilter.isActive() || anforderungStatusFilter.isActive())) {

            List<Status> statusList = meldungStatusFilter.getAsEnumList();
            statusList.addAll(anforderungStatusFilter.getAsEnumList());

            List<Long> historyIdList = getAnforderungHistroyIdList(datumStatusaenderungFilter, statusList);

            if (historyIdList.isEmpty()) {
                qp.append(AND_1_0);
            } else {
                qp.append(" AND a.id IN :historyIdList");
                qp.put("historyIdList", historyIdList);
            }
        }

        return qp;
    }

    private List<Long> getAnforderungHistroyIdList(VonBisDateSearchFilter datumStatusaenderungFilter, List<Status> statusList) {
        QueryPartDTO qp = new QueryPartDTO("SELECT a.anforderungId FROM AnforderungHistory a WHERE 1=1");
        List<Long> result = new ArrayList<>();

        switch (datumStatusaenderungFilter.getCase()) {
            case VON:
                qp.append(" AND a.datum >= :dateStart");
                qp.put("dateStart", datumStatusaenderungFilter.getVon());
                break;
            case BIS:
                qp.append(" AND a.datum <= :dateEnd");
                qp.put("dateEnd", datumStatusaenderungFilter.getBis());
                break;
            case VONBIS:
                qp.append(" AND a.datum >= :dateStart AND a.datum <= :dateEnd");
                qp.put("dateStart", datumStatusaenderungFilter.getVon());
                qp.put("dateEnd", datumStatusaenderungFilter.getBis());
                break;
            case NONE:
            default:
                return result;
        }

        List<String> statusNameList = statusList.stream()
                .map(Status::getStatusBezeichnung)
                .collect(Collectors.toList());

        qp.append(" AND a.auf IN :statusNameList");
        qp.put("statusNameList", statusNameList);

        result.addAll(queryPartService.getResultList(qp.getQuery(), qp.getParameterMap()));
        return result;
    }

    private static QueryPartDTO getQueryForGlobalSearchFilter(GlobalSearchFilter filter, Type type) {
        QueryPartDTO qp = new QueryPartDTO();

        qp.merge(getQueryForSensorCocFilter(filter));
        qp.merge(getQueryForThemenklammerFilter(filter, type));
        qp.merge(getQueryForTteamFilter(filter, type));
        qp.merge(getQueryForStatusFilter(filter, type));
        qp.merge(getQueryForAenderungsdatumFilter(filter));
        qp.merge(getQueryForErstellungsdatumFilter(filter));
        qp.merge(getQueryForFestgestelltInFilter(filter, type));
        qp.merge(getQueryForDerivatFilter(filter, type));
        qp.merge(getQueryForPhasenrelevanzFilter(filter, type));
        qp.merge(getQueryForDashboardAnforderungIdFilter(filter, type));
        qp.merge(getQueryForDashboardMeldungIdFilter(filter, type));
        qp.merge(getQueryForModulAndSeTeamFilter(filter, type));
        qp.merge(getQueryForAnforderungMeldungIdFilter(filter, type));
        qp.merge(getQueryForReferenzsystem(filter, type));
        qp.merge(getQueryForKategorie(filter, type));
        qp.merge(getQueryForProzessbaukastenFilter(filter, type));

        qp.merge(getQueryForSortingFilter(filter));

        return qp;
    }

    private List<Long> getAnforderungIdsForZakId(String query) {
        List<String> searchItems = Collections.singletonList(query);
        return getAnforderungIdsForZakId(searchItems);
    }

    private List<Long> getAnforderungIdsForZakId(QueryUrlFilter queryUrlFilter) {
        List<String> searchItems = queryUrlFilter.getAttributeValues();
        return getAnforderungIdsForZakId(searchItems);
    }

    private List<Long> getAnforderungIdsForZakId(List<String> searchItems) {
        if (searchItems != null && !searchItems.isEmpty() && searchItems.stream().anyMatch(si -> si.matches("\\d+"))) {
            List<String> uebertragungIdsFromQuery = searchItems.stream().filter(si -> si.matches("\\d+")).map(si -> si + "%").collect(Collectors.toList());
            if (!uebertragungIdsFromQuery.isEmpty()) {
                List<Long> result = new ArrayList<>();
                uebertragungIdsFromQuery.forEach(ui -> {
                    QueryPartDTO qp = new QueryPartDTO("SELECT z.derivatAnforderungModul.zuordnungAnforderungDerivat.anforderung.id ");
                    qp.append(" FROM ZakUebertragung z WHERE z.zakId LIKE :uebertragungIds");
                    qp.put("uebertragungIds", ui);
                    result.addAll(queryPartService.getQueryResultsAsLongList(qp));
                });
                return result;
            }
        }
        return Collections.emptyList();
    }

}
