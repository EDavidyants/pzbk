package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.filter.AenderungsdatumVonBisDateFilter;
import de.interfaceag.bmw.pzbk.filter.AnforderungStatusFilter;
import de.interfaceag.bmw.pzbk.filter.BisDateFilter;
import de.interfaceag.bmw.pzbk.filter.DashboardFilter;
import de.interfaceag.bmw.pzbk.filter.DatumStatusaenderungVonBisDateFilter;
import de.interfaceag.bmw.pzbk.filter.DerivatFilter;
import de.interfaceag.bmw.pzbk.filter.ErstellungsdatumVonBisDateFilter;
import de.interfaceag.bmw.pzbk.filter.FestgestelltInFilter;
import de.interfaceag.bmw.pzbk.filter.MeldungStatusFilter;
import de.interfaceag.bmw.pzbk.filter.ModulFilter;
import de.interfaceag.bmw.pzbk.filter.ModulSeTeamFilter;
import de.interfaceag.bmw.pzbk.filter.PhasenrelevanzFilter;
import de.interfaceag.bmw.pzbk.filter.ProzessbaukastenStatusFilter;
import de.interfaceag.bmw.pzbk.filter.QueryUrlFilter;
import de.interfaceag.bmw.pzbk.filter.ReferenzSystemFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;
import de.interfaceag.bmw.pzbk.filter.SearchFilterRemoveObject;
import de.interfaceag.bmw.pzbk.filter.SensorCocFilter;
import de.interfaceag.bmw.pzbk.filter.TteamFilter;
import de.interfaceag.bmw.pzbk.filter.TypeFilter;
import de.interfaceag.bmw.pzbk.filter.UserDefinedSearchUrlFilter;
import de.interfaceag.bmw.pzbk.filter.VonDateFilter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class GlobalSearchFilterRemoveObjectHandler {

    private GlobalSearchFilterRemoveObjectHandler() {
    }

    public static List<SearchFilterRemoveObject> getActiveFilterSelectedValues(GlobalSearchFilter filter) {
        List<SearchFilterRemoveObject> result = new ArrayList<>();

        filter.getSensorCocFilter().getSelectedValues().forEach(value -> result.add(new SearchFilterRemoveObject(filter.getSensorCocFilter().getParameterName(), value)));
        filter.getFestgestelltInFilter().getSelectedValues().forEach(value -> result.add(new SearchFilterRemoveObject(filter.getFestgestelltInFilter().getParameterName(), value)));
        filter.getPhasenrelevanzFilter().getSelectedValues().forEach(value -> result.add(new SearchFilterRemoveObject(filter.getPhasenrelevanzFilter().getParameterName(), value)));
        filter.getMeldungStatusFilter().getSelectedValues().forEach(value -> result.add(new SearchFilterRemoveObject(filter.getMeldungStatusFilter().getParameterName(), value)));
        filter.getAnforderungStatusFilter().getSelectedValues().forEach(value -> result.add(new SearchFilterRemoveObject(filter.getAnforderungStatusFilter().getParameterName(), value)));
        filter.getProzessbaukastenStatusFilter().getSelectedValues().forEach(value -> result.add(new SearchFilterRemoveObject(filter.getProzessbaukastenStatusFilter().getParameterName(), value)));
        filter.getTteamFilter().getSelectedValues().forEach(value -> result.add(new SearchFilterRemoveObject(filter.getTteamFilter().getParameterName(), value)));
        filter.getDerivatFilter().getSelectedValues().forEach(value -> result.add(new SearchFilterRemoveObject(filter.getDerivatFilter().getParameterName(), value)));
        filter.getModulSeTeamFilter().getSelectedValues().forEach(value -> result.add(new SearchFilterRemoveObject(filter.getModulSeTeamFilter().getParameterName(), value)));
        filter.getModulFilter().getSelectedValues().forEach(value -> result.add(new SearchFilterRemoveObject(filter.getModulFilter().getParameterName(), value)));
        filter.getTypeFilter().getSelectedValues().forEach(value -> result.add(new SearchFilterRemoveObject(filter.getTypeFilter().getParameterName(), value)));
        filter.getProzessbaukastenFilter().getSelectedValues().forEach(value -> result.add(new SearchFilterRemoveObject(filter.getProzessbaukastenFilter().getParameterName(), value)));
        filter.getReferenzSystemFilter().getSelectedValues().forEach(value -> result.add(new SearchFilterRemoveObject(filter.getReferenzSystemFilter().getParameterName(), value)));

        VonDateFilter vonErstellungsdatumFilter = filter.getErstellungsdatumFilter().getVonDateFilter();
        BisDateFilter bisErstellungsdatumFilter = filter.getErstellungsdatumFilter().getBisDateFilter();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        addRemoveObjectForErstellungsdatumFilter(vonErstellungsdatumFilter, result, dateFormat);

        addRemoveObjectForErstellungsdatumFilter(bisErstellungsdatumFilter, result, dateFormat);

        VonDateFilter vonAenderungsdatumFilter = filter.getAenderungsdatumFilter().getVonDateFilter();
        BisDateFilter bisAenderungsdatumFilter = filter.getAenderungsdatumFilter().getBisDateFilter();

        addRemoveObjectForVonAenderungsdatumFilter(vonAenderungsdatumFilter, result, dateFormat);

        addRemoveObjectForBisAenderungsdatumFilter(bisAenderungsdatumFilter, result, dateFormat);

        VonDateFilter vonDatumStatusaenderungFilter = filter.getDatumStatusaenderungFilter().getVonDateFilter();
        BisDateFilter bisDatumStatusaenderungFilter = filter.getDatumStatusaenderungFilter().getBisDateFilter();

        addRemoveObjectForVonDatumStatusaenderungFilter(vonDatumStatusaenderungFilter, result, dateFormat);

        addRemoveObjectForBisDatumStatusaenderungFilter(bisDatumStatusaenderungFilter, result, dateFormat);

        addRemoveObjectForDashboardFilter(filter, result);

        if (filter.getUserDefinedSearchUrlFilter().isActive() && filter.getUserDefinedSearchUrlFilter().getId().isPresent()) {
            result.add(new SearchFilterRemoveObject(UserDefinedSearchUrlFilter.PARAMETERNAME, new SearchFilterObject(filter.getUserDefinedSearchUrlFilter().getId().orElse(null), "gespeichterte Auswahl")));

        }

        return result;
    }


    private static void addRemoveObjectForDashboardFilter(GlobalSearchFilter filter, List<SearchFilterRemoveObject> result) {
        if (filter.getDashboardFilter().isActive()) {
            result.add(new SearchFilterRemoveObject(DashboardFilter.PARAMETERNAME, new SearchFilterObject(Integer.toUnsignedLong(filter.getDashboardFilter().getStep()), "Dashboard")));
        }
    }

    private static void addRemoveObjectForBisDatumStatusaenderungFilter(BisDateFilter bisDatumStatusaenderungFilter, List<SearchFilterRemoveObject> result, SimpleDateFormat dateFormat) {
        if (bisDatumStatusaenderungFilter.isActive()) {
            result.add(new SearchFilterRemoveObject(bisDatumStatusaenderungFilter.getParameterName(), new SearchFilterObject(1L, "Datum Statusänderung bis " + dateFormat.format(bisDatumStatusaenderungFilter.getSelected()))));
        }
    }

    private static void addRemoveObjectForVonDatumStatusaenderungFilter(VonDateFilter vonDatumStatusaenderungFilter, List<SearchFilterRemoveObject> result, SimpleDateFormat dateFormat) {
        if (vonDatumStatusaenderungFilter.isActive()) {
            result.add(new SearchFilterRemoveObject(vonDatumStatusaenderungFilter.getParameterName(), new SearchFilterObject(0L, "Datum Statusänderung von " + dateFormat.format(vonDatumStatusaenderungFilter.getSelected()))));
        }
    }

    private static void addRemoveObjectForBisAenderungsdatumFilter(BisDateFilter bisAenderungsdatumFilter, List<SearchFilterRemoveObject> result, SimpleDateFormat dateFormat) {
        if (bisAenderungsdatumFilter.isActive()) {
            result.add(new SearchFilterRemoveObject(bisAenderungsdatumFilter.getParameterName(), new SearchFilterObject(1L, "Änderungsdatum bis " + dateFormat.format(bisAenderungsdatumFilter.getSelected()))));
        }
    }

    private static void addRemoveObjectForVonAenderungsdatumFilter(VonDateFilter vonAenderungsdatumFilter, List<SearchFilterRemoveObject> result, SimpleDateFormat dateFormat) {
        if (vonAenderungsdatumFilter.isActive()) {
            result.add(new SearchFilterRemoveObject(vonAenderungsdatumFilter.getParameterName(), new SearchFilterObject(0L, "Änderungsdatum von " + dateFormat.format(vonAenderungsdatumFilter.getSelected()))));
        }
    }

    private static void addRemoveObjectForErstellungsdatumFilter(BisDateFilter bisErstellungsdatumFilter, List<SearchFilterRemoveObject> result, SimpleDateFormat dateFormat) {
        if (bisErstellungsdatumFilter.isActive()) {
            result.add(new SearchFilterRemoveObject(bisErstellungsdatumFilter.getParameterName(), new SearchFilterObject(1L, "Erstellungsdatum bis " + dateFormat.format(bisErstellungsdatumFilter.getSelected()))));
        }
    }

    private static void addRemoveObjectForErstellungsdatumFilter(VonDateFilter vonErstellungsdatumFilter, List<SearchFilterRemoveObject> result, SimpleDateFormat dateFormat) {
        if (vonErstellungsdatumFilter.isActive()) {
            result.add(new SearchFilterRemoveObject(vonErstellungsdatumFilter.getParameterName(), new SearchFilterObject(0L, "Erstellungsdatum von " + dateFormat.format(vonErstellungsdatumFilter.getSelected()))));
        }
    }

    public static String removeFilterObject(SearchFilterRemoveObject filterRemoveObject, GlobalSearchFilter filter) {
        String filterName = filterRemoveObject.getFilter();
        SearchFilterObject searchFilterObject = filterRemoveObject.getSearchFilterObject();

        switch (filterName) {
            case SensorCocFilter.PARAMETERNAME:
                filter.getSensorCocFilter().getSelectedValues().remove(searchFilterObject);
                break;
            case FestgestelltInFilter.PARAMETERNAME:
                filter.getFestgestelltInFilter().getSelectedValues().remove(searchFilterObject);
                break;
            case PhasenrelevanzFilter.PARAMETERNAME:
                filter.getPhasenrelevanzFilter().getSelectedValues().remove(searchFilterObject);
                break;
            case MeldungStatusFilter.PARAMETERNAME:
                filter.getMeldungStatusFilter().getSelectedValues().remove(searchFilterObject);
                break;
            case AnforderungStatusFilter.PARAMETERNAME:
                filter.getAnforderungStatusFilter().getSelectedValues().remove(searchFilterObject);
                break;
            case ProzessbaukastenStatusFilter.PARAMETERNAME:
                filter.getProzessbaukastenStatusFilter().getSelectedValues().remove(searchFilterObject);
                break;
            case TteamFilter.PARAMETERNAME:
                filter.getTteamFilter().getSelectedValues().remove(searchFilterObject);
                break;
            case DerivatFilter.PARAMETERNAME:
                filter.getDerivatFilter().getSelectedValues().remove(searchFilterObject);
                break;
            case ModulSeTeamFilter.PARAMETERNAME:
                filter.getModulSeTeamFilter().getSelectedValues().remove(searchFilterObject);
                break;
            case ModulFilter.PARAMETERNAME:
                filter.getModulFilter().getSelectedValues().remove(searchFilterObject);
                break;
            case TypeFilter.PARAMETERNAME:
                filter.getTypeFilter().getSelectedValues().remove(searchFilterObject);
                break;
            case ErstellungsdatumVonBisDateFilter.PARAMETERPRAEFIX + VonDateFilter.PARAMETERNAME:
                filter.getErstellungsdatumFilter().getVonDateFilter().setActive(Boolean.FALSE);
                break;
            case ErstellungsdatumVonBisDateFilter.PARAMETERPRAEFIX + BisDateFilter.PARAMETERNAME:
                filter.getErstellungsdatumFilter().getBisDateFilter().setActive(Boolean.FALSE);
                break;
            case AenderungsdatumVonBisDateFilter.PARAMETERPRAEFIX + VonDateFilter.PARAMETERNAME:
                filter.getAenderungsdatumFilter().getVonDateFilter().setActive(Boolean.FALSE);
                break;
            case AenderungsdatumVonBisDateFilter.PARAMETERPRAEFIX + BisDateFilter.PARAMETERNAME:
                filter.getAenderungsdatumFilter().getBisDateFilter().setActive(Boolean.FALSE);
                break;
            case DatumStatusaenderungVonBisDateFilter.PARAMETERPRAEFIX + VonDateFilter.PARAMETERNAME:
                filter.getDatumStatusaenderungFilter().getVonDateFilter().setActive(Boolean.FALSE);
                break;
            case DatumStatusaenderungVonBisDateFilter.PARAMETERPRAEFIX + BisDateFilter.PARAMETERNAME:
                filter.getDatumStatusaenderungFilter().getBisDateFilter().setActive(Boolean.FALSE);
                break;
            case QueryUrlFilter.PARAMETERNAME:
                filter.getQueryFilter().getAttributeValues().remove(searchFilterObject.getName());
                break;
            case DashboardFilter.PARAMETERNAME:
                filter.getDashboardFilter().disable();
                break;
            case UserDefinedSearchUrlFilter.PARAMETERNAME:
                filter.getUserDefinedSearchUrlFilter().setActive(Boolean.FALSE);
                break;
            case ReferenzSystemFilter.PARAMETERNAME:
                filter.getReferenzSystemFilter().getSelectedValues().remove(searchFilterObject);
                break;
            default:
                break;
        }

        return filter.getUrl();
    }

}
