package de.interfaceag.bmw.pzbk.globalsearch;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface AutocompleteSearchResult extends Serializable {

    String getBeschreibung();

    String getFachId();

    Long getId();

    String getName();

    String getTyp();

    String getVersion();

}
