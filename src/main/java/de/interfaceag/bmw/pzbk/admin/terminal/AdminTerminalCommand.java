package de.interfaceag.bmw.pzbk.admin.terminal;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public enum AdminTerminalCommand {
    HELP(0), LINKMELDUNGANFORDERUNG(1), LISTANFORDERUNGWITHOUTMELDUNG(2),
    LISTMATCHINGMELDUNGENFORANFORDERUNG(3), SENDTOZAK(4), SHOWFILESFORFACHID(5),
    ADDFILETOFACHID(6), EXPORTFILESYSTEM(7);

    // ------------  fields ----------------------------------------------------
    private int id;
    private String bezeichnung;
    private String command;
    private int paramCount;

    // ------------  constructors ----------------------------------------------
    AdminTerminalCommand(int id) {
        this.id = id;

        switch (id) {
            case 0:
                this.bezeichnung = "help";
                this.command = "help";
                this.paramCount = 0;
                break;
            case 1:
                this.bezeichnung = "link #meldungId #anforderungId";
                this.command = "link";
                this.paramCount = 2;
                break;
            case 2:
                this.bezeichnung = "listAnforderungenWithoutMeldung";
                this.command = "listAnforderungenWithoutMeldung";
                this.paramCount = 0;
                break;
            case 3:
                this.bezeichnung = "listMatchingMeldungenForAnforderung #anforderungId";
                this.command = "listMatchingMeldungenForAnforderung";
                this.paramCount = 1;
                break;
            case 4:
                this.bezeichnung = "sendToZak #zakUebertragungId";
                this.command = "sendToZak";
                this.paramCount = 1;
                break;
            case 5:
                this.bezeichnung = "showFiles #fachId";
                this.command = "showFiles";
                this.paramCount = 1;
                break;
            case 6:
                this.bezeichnung = "addFileTo #fachId #fileName";
                this.command = "addFileTo";
                this.paramCount = 2;
                break;
            case 7:
                this.bezeichnung = "exportFileSystem";
                this.command = "exportFileSystem";
                this.paramCount = 0;
                break;
            default:
                this.bezeichnung = "";
                break;

        }
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return getBezeichnung();
    }

    // ------------  getter / setter -------------------------------------------
    public int getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public String getCommand() {
        return command;
    }

    public int getParamCount() {
        return paramCount;
    }

}
