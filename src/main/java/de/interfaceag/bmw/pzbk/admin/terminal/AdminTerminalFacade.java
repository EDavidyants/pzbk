package de.interfaceag.bmw.pzbk.admin.terminal;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface AdminTerminalFacade {

    String handleCommand(String command, String[] params);

}
