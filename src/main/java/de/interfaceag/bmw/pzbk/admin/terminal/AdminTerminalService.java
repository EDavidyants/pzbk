package de.interfaceag.bmw.pzbk.admin.terminal;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.enums.ContentType;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.FileExportService;
import de.interfaceag.bmw.pzbk.services.FileService;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class AdminTerminalService implements Serializable, AdminTerminalFacade {

    public static final String VORHANDEN = " vorhanden!";
    @Inject
    private ConfigService configService;
    @Inject
    private AnforderungService anforderungService;
    @Inject
    private FileService fileService;
    @Inject
    private FileExportService fileExportService;

    @Override
    public String handleCommand(String commandString, String[] params) {
        Optional<AdminTerminalCommand> command = AdminTerminalUtils.getCommand(commandString);
        if (command.isPresent()) {
            return handleCommand(command.get(), params);
        } else {
            return commandString + " wurde nicht gefunden!";
        }
    }

    protected String handleCommand(AdminTerminalCommand command, String[] params) {
        switch (command) {
            case LINKMELDUNGANFORDERUNG:
                if (AdminTerminalUtils.verifyParamCount(params, command)) {
                    String meldungId = params[0];
                    String anforderungId = params[1];
                    return configService.linkAnforderungWithMeldung(meldungId, anforderungId);
                } else {
                    return "Bitte genau zwei Parameter angeben (MeldungId, AnforderungId)";
                }
            case LISTANFORDERUNGWITHOUTMELDUNG:
                return configService.getAllAnforderungenWithoutMeldungAsString();
            case LISTMATCHINGMELDUNGENFORANFORDERUNG:
                if (AdminTerminalUtils.verifyParamCount(params, command)) {
                    String anforderungId = params[0];
                    return configService.getAllMatchingMeldungenForAnforderungAsString(anforderungId);
                } else {
                    return "Bitte genau einen Parameter angeben (AnforderungId)";
                }
            case HELP:
                return AdminTerminalUtils.getAllCommandsAsString();
            case SHOWFILESFORFACHID:
                return getFilesForFachId(params);
            case ADDFILETOFACHID:
                return addFileToFachId(params);
            case EXPORTFILESYSTEM:
                return exportFileSystem();
            default:
                return command + " not found";
        }
    }

    private String exportFileSystem() {
        fileExportService.downloadFileSystemExport();
        return "Export erfolgreich.";
    }

    private String getFilesForFachId(String[] params) {
        if (AdminTerminalUtils.verifyParamCount(params, AdminTerminalCommand.SENDTOZAK)) {
            String fachId = params[0];

            Long id;
            ContentType contentType;

            if (RegexUtils.matchesAnforderungFachId(fachId)) {
                Anforderung a = anforderungService.getUniqueAnforderungByFachId(fachId);
                if (a != null) {
                    id = a.getId();
                    contentType = ContentType.ANFORDERUNG;
                } else {
                    return "Fehler: Keine Anforderung mit FachId " + fachId + VORHANDEN;
                }
            } else if (RegexUtils.matchesMeldungFachId(fachId)) {
                Meldung m = anforderungService.getUniqueMeldungByFachId(fachId);
                if (m != null) {
                    id = m.getId();
                    contentType = ContentType.MELDUNG;
                } else {
                    return "Fehler: Keine Meldung mit FachId " + fachId + VORHANDEN;
                }
            } else {
                return "Fehler: Paramter ist keine FachId!";
            }
            return fileService.getFolderContentAsString(id, contentType);
        } else {
            return "Fehler: Es fehlt der Paramter!";
        }
    }

    private String addFileToFachId(String[] params) {
        if (AdminTerminalUtils.verifyParamCount(params, AdminTerminalCommand.ADDFILETOFACHID)) {
            String fachId = params[0];
            String fileName = params[1];

            if (RegexUtils.matchesAnforderungFachId(fachId)) {
                Anforderung a = anforderungService.getUniqueAnforderungByFachId(fachId);
                if (a != null) {
                    Path path = fileService.getFilePathForFachIdFilename(a.getId(), ContentType.ANFORDERUNG, fileName);
                    fileService.createAnhangForAnforderung(path, a);
                    return "Datei " + fileName + " wurde zur Anforderung " + fachId + " hinzugefügt";
                } else {
                    return "Fehler: Keine Anforderung mit FachId " + fachId + VORHANDEN;
                }
            } else if (RegexUtils.matchesMeldungFachId(fachId)) {
                Meldung m = anforderungService.getUniqueMeldungByFachId(fachId);
                if (m != null) {
                    Path path = fileService.getFilePathForFachIdFilename(m.getId(), ContentType.MELDUNG, fileName);
                    fileService.createAnhangForMeldung(path, m);
                    return "Datei " + fileName + " wurde zur Meldung " + fachId + " hinzugefügt";
                } else {
                    return "Fehler: Keine Meldung mit FachId " + fachId + VORHANDEN;
                }
            } else {
                return "Fehler: Paramter ist keine FachId!";
            }
        } else {
            return "Fehler: Es fehlt der Paramter!";
        }
    }

}
