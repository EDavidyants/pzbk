package de.interfaceag.bmw.pzbk.admin.terminal;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class AdminTerminalUtils {

    private AdminTerminalUtils() {
    }

    protected static String getAllCommandsAsString() {
        return getAllCommands().map(AdminTerminalCommand::getBezeichnung).collect(Collectors.joining(",\n"));
    }

    protected static Stream<AdminTerminalCommand> getAllCommands() {
        return Stream.of(AdminTerminalCommand.values());
    }

    protected static Optional<AdminTerminalCommand> getCommand(String command) {
        if (command != null) {
            return getAllCommands().filter(c -> c.getCommand().equalsIgnoreCase(command.trim().toLowerCase())).findAny();
        } else {
            return Optional.empty();
        }
    }

    protected static boolean verifyParamCount(String[] params, AdminTerminalCommand command) {
        if (params != null && command != null) {
            return params.length == command.getParamCount();
        }
        return false;
    }

}
