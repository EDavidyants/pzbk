package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import java.io.Serializable;

/**
 *
 * @author sl
 */
public final class ActiveIndex implements Serializable {

    private Integer index;

    private ActiveIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public static ActiveIndex forTab(ProzessbaukastenTab tab) {
        return new ActiveIndex(tab.getIndex());
    }

    public static ActiveIndex forUrlParameter(UrlParameter urlParameter) {
        String index = urlParameter.getValue("activeIndex").orElse("");
        if (RegexUtils.matchesId(index)) {
            Integer activeIndex = Integer.parseInt(index);
            return new ActiveIndex(activeIndex);
        }
        return new ActiveIndex(0);
    }

}
