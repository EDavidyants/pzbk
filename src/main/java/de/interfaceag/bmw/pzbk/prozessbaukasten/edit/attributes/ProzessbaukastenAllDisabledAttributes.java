package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes;

/**
 *
 * @author sl
 */
public class ProzessbaukastenAllDisabledAttributes implements ProzessbaukastenDisabledAttributes {

    protected ProzessbaukastenAllDisabledAttributes() {
    }

    @Override
    public boolean isBezeichnungDisabled() {
        return true;
    }

    @Override
    public boolean isBeschreibungDisabled() {
        return true;
    }

    @Override
    public boolean isTteamDisabled() {
        return true;
    }

    @Override
    public boolean isLeadTechnologieDisabled() {
        return true;
    }

    @Override
    public boolean isKonzeptDisabled() {
        return true;
    }

    @Override
    public boolean isStandardisierterFertigungsprozessDisabled() {
        return true;
    }

    @Override
    public boolean isErstanlaeuferDisabled() {
        return true;
    }

    @Override
    public boolean isAnhangStartbriefDisabled() {
        return true;
    }

    @Override
    public boolean isAnhangGrafikUmfangDisabled() {
        return true;
    }

    @Override
    public boolean isAnhangGrafikKonzeptbaumDisabled() {
        return true;
    }

    @Override
    public boolean isAnhangBeauftragungTPKDisabled() {
        return true;
    }

    @Override
    public boolean isAnhangGenehmigungTPKDisabled() {
        return true;
    }

    @Override
    public boolean isWeitereAnhangeDisabled() {
        return true;
    }

    @Override
    public boolean isAnhangeTabDisabled() {
        return true;
    }

    @Override
    public boolean isAnforderungenTabDisabled() {
        return true;
    }

    @Override
    public boolean isStammdatenDetailsPanelDisabled() {
        return true;
    }

}
