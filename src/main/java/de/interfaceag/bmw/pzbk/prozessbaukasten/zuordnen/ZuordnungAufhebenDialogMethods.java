package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

/**
 *
 * @author evda
 */
public interface ZuordnungAufhebenDialogMethods {

    String proceedWithZuordnungAufheben();

    void resetZuordnungAufhebenKommentar();

}
