package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Bild;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.services.ImageService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 *
 * @author evda
 */
@Stateless
@Named
public class ProzessbaukastenImageUpdateService implements Serializable {

    @Inject
    private ImageService imageService;

    public void updateProzessbaukastenBilder(Prozessbaukasten prozessbaukasten) {
        updateGrafikUmfangBild(prozessbaukasten);
        updateKonzeptbaumBild(prozessbaukasten);
        updateKonzeptBilder(prozessbaukasten);
    }

    private void updateGrafikUmfangBild(Prozessbaukasten prozessbaukasten) {
        Anhang grafikUmfang = prozessbaukasten.getGrafikUmfang();
        if (grafikUmfang != null) {
            Bild grafikUmfangBild = imageService.generateBildFromAnhang(grafikUmfang);
            prozessbaukasten.setGrafikUmfangBild(grafikUmfangBild);
        } else {
            prozessbaukasten.setGrafikUmfangBild(null);
        }
    }

    private void updateKonzeptbaumBild(Prozessbaukasten prozessbaukasten) {
        Anhang konzeptbaum = prozessbaukasten.getKonzeptbaum();
        if (konzeptbaum != null) {
            Bild konzeptbaumBild = imageService.generateBildFromAnhang(konzeptbaum);
            prozessbaukasten.setKonzeptbaumBild(konzeptbaumBild);
        } else {
            prozessbaukasten.setKonzeptbaumBild(null);
        }
    }

    private void updateKonzeptBilder(Prozessbaukasten prozessbaukasten) {
        for (Konzept konzept : prozessbaukasten.getKonzepte()) {
            if (konzept.getAnhang() != null) {
                Bild bild = imageService.generateBildFromAnhang(konzept.getAnhang());
                konzept.setBild(bild);
            } else {
                konzept.setBild(null);
            }

        }
    }

}
