package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import org.primefaces.event.ReorderEvent;

/**
 *
 * @author lomu
 */
public interface ProzessbaukastenAnforderungenTabReorder {

    void onRowReorder(ReorderEvent event);

    String apply();

}
