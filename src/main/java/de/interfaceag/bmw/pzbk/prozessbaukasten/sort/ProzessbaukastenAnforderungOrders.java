package de.interfaceag.bmw.pzbk.prozessbaukasten.sort;

/**
 *
 * @author sl
 */
public interface ProzessbaukastenAnforderungOrders {

    int getOrderForProzessbaukastenAnforderung(Long prozessbaukastenId, Long anforderungId);

}
