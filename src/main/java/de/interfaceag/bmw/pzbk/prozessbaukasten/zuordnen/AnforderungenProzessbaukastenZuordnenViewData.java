package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.globalsearch.AutocompleteSearchResult;
import de.interfaceag.bmw.pzbk.globalsearch.SearchResultDTO;
import org.primefaces.event.SelectEvent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *
 * @author fn
 */
public class AnforderungenProzessbaukastenZuordnenViewData implements AnforderungenProzessbaukastenZuordnenDialogFields, Serializable {

    private List<AnforderungenProzessbaukastenZuordnenDTO> zugeordneteAnforderungen = new ArrayList<>();
    private SearchResultDTO autocompleteSearchResult;

    public AnforderungenProzessbaukastenZuordnenViewData() {
    }

    @Override
    public List<AnforderungenProzessbaukastenZuordnenDTO> getZugeordneteAnforderungen() {
        return zugeordneteAnforderungen;
    }

    @Override
    public void setZugeordneteAnforderungen(List<AnforderungenProzessbaukastenZuordnenDTO> zugeordneteAnforderungen) {
        this.zugeordneteAnforderungen = zugeordneteAnforderungen;
    }

    public void setZugeordneteAnforderungenFromAnforderungList(List<Anforderung> anforderungen) {
        this.zugeordneteAnforderungen = anforderungen.stream().map(anforderung -> new AnforderungenProzessbaukastenZuordnenDTO(anforderung.getFachId(), anforderung.getVersion(), anforderung.getBeschreibungAnforderungDe()))
                .collect(Collectors.toList());
    }

    @Override
    public SearchResultDTO getAutocompleteSearchResult() {
        return autocompleteSearchResult;
    }

    @Override
    public void setAutocompleteSearchResult(SearchResultDTO autocompleteSearchResult) {
        this.autocompleteSearchResult = autocompleteSearchResult;
    }

    public void addAnforderung(SelectEvent event) {
        AutocompleteSearchResult autocompleteSearchResultSelection = (AutocompleteSearchResult) event.getObject();
        if (autocompleteSearchResultSelection != null && !isAnforderungInZuordnenList(zugeordneteAnforderungen, autocompleteSearchResultSelection)) {
            Integer version = Integer.parseInt(autocompleteSearchResultSelection.getVersion());
            zugeordneteAnforderungen.add(new AnforderungenProzessbaukastenZuordnenDTO(autocompleteSearchResultSelection.getName(), version, autocompleteSearchResultSelection.getBeschreibung()));
        }
    }

    private boolean isAnforderungInZuordnenList(List<AnforderungenProzessbaukastenZuordnenDTO> anforderungenDTOs, AutocompleteSearchResult autocompleteSearchResult) {
        for (AnforderungenProzessbaukastenZuordnenDTO anforderungDTO : anforderungenDTOs) {
            Integer version = Integer.parseInt(autocompleteSearchResult.getVersion());
            if (anforderungDTO.getFachId().equals(autocompleteSearchResult.getName()) && Objects.equals(anforderungDTO.getVersion(), version)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void removeAnforderung(AnforderungenProzessbaukastenZuordnenDTO anforderungDto) {
        zugeordneteAnforderungen.remove(anforderungDto);
    }

    public static AnforderungenProzessbaukastenZuordnenViewData forProzessbaukasten() {
        return new AnforderungenProzessbaukastenZuordnenViewData();
    }

    public void resetZugeordneteAnforderungen() {
        zugeordneteAnforderungen.clear();
    }

}
