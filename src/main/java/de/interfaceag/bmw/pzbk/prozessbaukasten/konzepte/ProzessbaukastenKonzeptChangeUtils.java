package de.interfaceag.bmw.pzbk.prozessbaukasten.konzepte;

import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.prozessbaukasten.dto.ProzessbaukastenKonzeptChangeDto;

import java.util.ArrayList;
import java.util.List;

final class ProzessbaukastenKonzeptChangeUtils {

    private static final String ANHANG = "Anhang";

    private ProzessbaukastenKonzeptChangeUtils() {

    }

    static List<ProzessbaukastenKonzeptChangeDto> getChangedKonzeptAttribute(Konzept konzeptNew, Konzept konzeptOld) {
        List<ProzessbaukastenKonzeptChangeDto> changedAttribute = new ArrayList<>();
        if (isKonzeptBezeichnungChanged(konzeptNew, konzeptOld)) {

            changedAttribute.add(new ProzessbaukastenKonzeptChangeDto(konzeptOld.getBezeichnung(), "Bezeichnung", konzeptOld.getBezeichnung(), konzeptNew.getBezeichnung()));
        }
        if (isKonzeptBeschreibungChanged(konzeptNew, konzeptOld)) {
            changedAttribute.add(new ProzessbaukastenKonzeptChangeDto(konzeptOld.getBezeichnung(), "Beschreibung", konzeptOld.getBeschreibung(), konzeptNew.getBeschreibung()));
        }
        if (isKonzeptAnhangHinzugefuegt(konzeptNew, konzeptOld)) {
            changedAttribute.add(new ProzessbaukastenKonzeptChangeDto(konzeptOld.getBezeichnung(), ANHANG, "kein", konzeptNew.getAnhang().getDateiname()));
        } else if (isKonzeptAnhangEntfernt(konzeptNew, konzeptOld)) {
            changedAttribute.add(new ProzessbaukastenKonzeptChangeDto(konzeptOld.getBezeichnung(), ANHANG, konzeptOld.getAnhang().getDateiname(), "kein"));
        } else if (isKonzeptAnhangChanged(konzeptNew, konzeptOld)) {
            changedAttribute.add(new ProzessbaukastenKonzeptChangeDto(konzeptOld.getBezeichnung(), ANHANG, konzeptOld.getAnhang().getDateiname(), konzeptNew.getAnhang().getDateiname()));
        }

        return changedAttribute;
    }

    private static boolean isKonzeptAnhangChanged(Konzept konzeptNew, Konzept konzeptOld) {
        if (konzeptNew.getAnhang() != null && konzeptOld.getAnhang() != null) {
            return !konzeptNew.getAnhang().equals(konzeptOld.getAnhang());
        } else {
            return false;
        }

    }

    private static boolean isKonzeptAnhangEntfernt(Konzept konzeptNew, Konzept konzeptOld) {
        return konzeptNew.getAnhang() == null && konzeptOld.getAnhang() != null;
    }

    private static boolean isKonzeptAnhangHinzugefuegt(Konzept konzeptNew, Konzept konzeptOld) {
        return konzeptNew.getAnhang() != null && konzeptOld.getAnhang() == null;
    }

    private static boolean isKonzeptBeschreibungChanged(Konzept konzeptNew, Konzept konzeptOld) {
        return !konzeptNew.getBeschreibung().equals(konzeptOld.getBeschreibung());
    }

    private static boolean isKonzeptBezeichnungChanged(Konzept konzeptNew, Konzept konzeptOld) {
        return !konzeptNew.getBezeichnung().equals(konzeptOld.getBezeichnung());
    }
}
