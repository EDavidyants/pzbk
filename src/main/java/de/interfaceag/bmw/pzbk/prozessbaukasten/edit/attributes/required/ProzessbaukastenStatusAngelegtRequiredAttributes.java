package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required;

/**
 *
 * @author sl
 */
public class ProzessbaukastenStatusAngelegtRequiredAttributes extends ProzessbaukastenNoRequiredAttributes {

    protected ProzessbaukastenStatusAngelegtRequiredAttributes() {
    }

    @Override
    public boolean isBezeichnungRequired() {
        return true;
    }

    @Override
    public boolean isBeschreibungRequired() {
        return true;
    }

    @Override
    public boolean isTteamRequired() {
        return true;
    }

    @Override
    public boolean isLeadTechnologieRequired() {
        return true;
    }

}
