package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author fn
 */
public interface ProzessbaukastenZuordnenDialogFields {

    List<ProzessbaukastenZuordnenDTO> getProzessbaukastenChoosable();

    void setProzessbaukastenChoosable(List<ProzessbaukastenZuordnenDTO> prozessbaukastenChoosable);

    void addProzessbaukasten(ProzessbaukastenZuordnenDTO prozessbaukasten);

    void removeProzessbaukasten();

    boolean zugeordneterProzessbaukastenIsPresent();

    Optional<ProzessbaukastenZuordnenDTO> getZugeordneterProzessbaukastenForView();

    List<ProzessbaukastenZuordnenDTO> getFilteredProzessbaukasten();

    void setFilteredProzessbaukasten(List<ProzessbaukastenZuordnenDTO> filteredProzessbaukasten);

    String getNameForView();

    String getBeschreibung();

}
