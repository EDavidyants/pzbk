package de.interfaceag.bmw.pzbk.prozessbaukasten.sort;

import de.interfaceag.bmw.pzbk.comparator.FachIdComparator;

import java.util.Comparator;
import java.util.Optional;

/**
 *
 * @author sl
 * @param <T>
 */
public class ProzessbaukastenAnforderungOptionalTupleComparator<T extends ProzessbaukastenAnforderungOptionalTuple> implements Comparator<T> {

    private final ProzessbaukastenAnforderungOrders anforderungOrders;

    public ProzessbaukastenAnforderungOptionalTupleComparator(ProzessbaukastenAnforderungOrders anforderungOrders) {
        this.anforderungOrders = anforderungOrders;
    }

    @Override
    public int compare(T zuordnung1, T zuordnung2) {
        Optional<Long> prozessbaukastendId1 = zuordnung1.getProzessbaukastenId();
        Optional<Long> prozessbaukastendId2 = zuordnung2.getProzessbaukastenId();

        if (prozessbaukastendId1.isPresent() && prozessbaukastendId2.isPresent()) {
            return compareProzessbaukastenAnforderungen(zuordnung1, zuordnung2);
        } else if (prozessbaukastendId1.isPresent()) {
            return 1;
        } else if (prozessbaukastendId2.isPresent()) {
            return -1;
        } else {
            return compareNormalAnforderugen(zuordnung1, zuordnung2);
        }
    }

    private int compareProzessbaukastenAnforderungen(T zuordnung1, T zuordnung2) {
        Long anforderungId1 = zuordnung1.getAnforderungId();
        Long anforderungId2 = zuordnung2.getAnforderungId();

        Long prozessbaukastendId1 = zuordnung1.getProzessbaukastenId().get();
        Long prozessbaukastendId2 = zuordnung2.getProzessbaukastenId().get();

        int compareProzessbaukastenId = compareProzessbaukastenId(prozessbaukastendId1, prozessbaukastendId2);

        if (compareProzessbaukastenId == 0) {
            // this implies that prozessbaukasten ids are identical
            return compareAnforderungPositionInProzessbaukasten(prozessbaukastendId1, anforderungId1, anforderungId2);
        } else {
            return compareProzessbaukastenId;
        }
    }

    private int compareAnforderungPositionInProzessbaukasten(Long prozessbaukastenId, Long anforderungId1, Long anforderungId2) {
        int order1 = anforderungOrders.getOrderForProzessbaukastenAnforderung(prozessbaukastenId, anforderungId1);
        int order2 = anforderungOrders.getOrderForProzessbaukastenAnforderung(prozessbaukastenId, anforderungId2);

        return Integer.compare(order1, order2);
    }

    private int compareProzessbaukastenId(Long id1, Long id2) {
        return id1.compareTo(id2);
    }

    private int compareNormalAnforderugen(T zuordnung1, T zuordnung2) {
        String fachId1 = zuordnung1.getAnforderungFachId();
        String fachId2 = zuordnung2.getAnforderungFachId();

        Comparator<String> fachIdComparator = new FachIdComparator().reversed();

        return fachIdComparator.compare(fachId1, fachId2);
    }

}
