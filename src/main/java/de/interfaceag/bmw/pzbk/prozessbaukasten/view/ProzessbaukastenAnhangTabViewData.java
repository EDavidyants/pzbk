package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.entities.Anhang;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ProzessbaukastenAnhangTabViewData implements ProzessbaukastenAnhangTabData, Serializable {

    private final Anhang startbrief;
    private final Anhang grafikUmfang;
    private final Anhang konzeptbaum;
    private final Anhang beauftragungTPK;
    private final Anhang genehmigungTPK;
    private final List<Anhang> weitereAnhaenge;

    private ProzessbaukastenAnhangTabViewData(Anhang startbrief, Anhang grafikUmfang, Anhang konzeptbaum, Anhang beauftragungTPK, Anhang genehmigungTPK, List<Anhang> weitereAnhaenge) {
        this.startbrief = startbrief;
        this.grafikUmfang = grafikUmfang;
        this.konzeptbaum = konzeptbaum;
        this.beauftragungTPK = beauftragungTPK;
        this.genehmigungTPK = genehmigungTPK;
        this.weitereAnhaenge = weitereAnhaenge;
    }

    @Override
    public Anhang getStartbrief() {
        return startbrief;
    }

    @Override
    public Anhang getGrafikUmfang() {
        return grafikUmfang;
    }

    @Override
    public Anhang getKonzeptbaum() {
        return konzeptbaum;
    }

    @Override
    public Anhang getBeauftragungTPK() {
        return beauftragungTPK;
    }

    @Override
    public Anhang getGenehmigungTPK() {
        return genehmigungTPK;
    }

    @Override
    public List<Anhang> getWeitereAnhaenge() {
        return weitereAnhaenge;
    }

    @Override
    public boolean isExistWeitereAnhange() {
        return weitereAnhaenge != null && !weitereAnhaenge.isEmpty();
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {

        private Anhang startbrief;
        private Anhang grafikUmfang;
        private Anhang konzeptbaum;
        private Anhang beauftragungTPK;
        private Anhang genehmigungTPK;
        private List<Anhang> weitereAnhaenge;

        private Builder() {
            weitereAnhaenge = new ArrayList<>();
        }

        public Builder withStartbrief(Anhang startbrief) {
            this.startbrief = startbrief;
            return this;
        }

        public Builder withGrafikUmfang(Anhang grafikUmfang) {
            this.grafikUmfang = grafikUmfang;
            return this;
        }

        public Builder withKonzeptbaum(Anhang konzeptbaum) {
            this.konzeptbaum = konzeptbaum;
            return this;
        }

        public Builder withBeauftragungTPK(Anhang beauftragungTPK) {
            this.beauftragungTPK = beauftragungTPK;
            return this;
        }

        public Builder withGenehmigungTPK(Anhang genehmigungTPK) {
            this.genehmigungTPK = genehmigungTPK;
            return this;
        }

        public Builder withWeitereAnhaenge(List<Anhang> weitereAnhaenge) {
            this.weitereAnhaenge = weitereAnhaenge;
            return this;
        }

        public ProzessbaukastenAnhangTabViewData build() {
            return new ProzessbaukastenAnhangTabViewData(startbrief, grafikUmfang, konzeptbaum, beauftragungTPK, genehmigungTPK, weitereAnhaenge);
        }

    }

    public static ProzessbaukastenAnhangTabViewData forLeerProzessbaukasten() {
        ProzessbaukastenAnhangTabViewData anhangTabViewData = ProzessbaukastenAnhangTabViewData.newBuilder()
                .withStartbrief(null)
                .withGrafikUmfang(null)
                .withKonzeptbaum(null)
                .withBeauftragungTPK(null)
                .withGenehmigungTPK(null)
                .withWeitereAnhaenge(new ArrayList<>())
                .build();
        return anhangTabViewData;
    }

}
