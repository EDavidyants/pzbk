package de.interfaceag.bmw.pzbk.prozessbaukasten.history;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungHistoryDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenAnforderungKonzept;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenHistory;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenHistoryBuilder;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.konzepte.ProzessbaukastenKonzeptHistoryService;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author evda
 */
@Stateless
@Named
public class ProzessbaukastenHistoryService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ProzessbaukastenHistoryService.class);

    private static final String ANFORDERUNGEN = "anforderungen";
    private static final String ZUGEORDNET = "zugeordnet";
    private static final String ADD_ANFORDERUNG_TO_PROZESSBAUKASTEN = "addAnforderungToProzessbaukasten";
    private static final String PROZESSBAUKASTENVERSION = "prozessbaukastenversion";
    private static final String KONZEPTE = "konzepte";
    private static final String STATUS = "status";

    @Inject
    private ProzessbaukastenHistoryDao prozessbaukastenHistoryDao;
    @Inject
    private ProzessbaukastenReadService prozessbaukastenReadService;
    @Inject
    private AnforderungMeldungHistoryService anforderungHistoryService;
    @Inject
    private ProzessbaukastenKonzeptHistoryService prozessbaukastenKonzeptHistoryService;

    private static final List<String> HISTORY_FIELDS;
    private static final Map<String, String> HISTORY_CHANGEDFIELD_MAP;

    static {
        HISTORY_FIELDS = new ArrayList<>();
        HISTORY_FIELDS.add("version");
        HISTORY_FIELDS.add("bezeichnung");
        HISTORY_FIELDS.add("beschreibung");
        HISTORY_FIELDS.add("tteam");
        HISTORY_FIELDS.add("leadTechnologie");
        HISTORY_FIELDS.add("standardisierterFertigungsprozess");
        HISTORY_FIELDS.add(ANFORDERUNGEN);
        HISTORY_FIELDS.add(STATUS);
        HISTORY_FIELDS.add("startbrief");
        HISTORY_FIELDS.add("grafikUmfang");
        HISTORY_FIELDS.add("konzeptbaum");
        HISTORY_FIELDS.add("beauftragungTPK");
        HISTORY_FIELDS.add("genehmigungTPK");
        HISTORY_FIELDS.add("weitereAnhaenge");
        HISTORY_FIELDS.add("erstanlaeufer");
        HISTORY_FIELDS.add(KONZEPTE);

        HISTORY_CHANGEDFIELD_MAP = new HashMap<>();
        HISTORY_CHANGEDFIELD_MAP.put("version", "Version");
        HISTORY_CHANGEDFIELD_MAP.put("bezeichnung", "Bezeichnung");
        HISTORY_CHANGEDFIELD_MAP.put("beschreibung", "Beschreibung");
        HISTORY_CHANGEDFIELD_MAP.put("tteam", "T-Team");
        HISTORY_CHANGEDFIELD_MAP.put("leadTechnologie", "Lead Technologie");
        HISTORY_CHANGEDFIELD_MAP.put("standardisierterFertigungsprozess", "Standardisierter Fertigungsprozess");
        HISTORY_CHANGEDFIELD_MAP.put(ANFORDERUNGEN, "Anforderung Zuordnung");
        HISTORY_CHANGEDFIELD_MAP.put(STATUS, "Status");
        HISTORY_CHANGEDFIELD_MAP.put("startbrief", "Startbrief");
        HISTORY_CHANGEDFIELD_MAP.put("grafikUmfang", "Grafik Umfang");
        HISTORY_CHANGEDFIELD_MAP.put("konzeptbaum", "Konzeptbaum");
        HISTORY_CHANGEDFIELD_MAP.put("beauftragungTPK", "Beauftragung TPK");
        HISTORY_CHANGEDFIELD_MAP.put("genehmigungTPK", "Genehmigung TPK");
        HISTORY_CHANGEDFIELD_MAP.put("weitereAnhaenge", "Weitere Anhaenge");
        HISTORY_CHANGEDFIELD_MAP.put(PROZESSBAUKASTENVERSION, "Version");
        HISTORY_CHANGEDFIELD_MAP.put("erstanlaeufer", "Erstanläufer");
        HISTORY_CHANGEDFIELD_MAP.put(KONZEPTE, "Konzepte");
        HISTORY_CHANGEDFIELD_MAP.put("konzept", "Konzept");
        HISTORY_CHANGEDFIELD_MAP.put("prozessbaukastenAnforderungKonzept", "Konzept Zuordnung");
    }

    public void save(ProzessbaukastenHistory history) {
        prozessbaukastenHistoryDao.save(history);
    }

    public List<AnforderungHistoryDto> getHistoryForProzessbaukastenByFachIdAndVersion(String fachId, int version) {
        Optional<Prozessbaukasten> prozessbaukastenToFind = prozessbaukastenReadService.getByFachIdAndVersion(fachId, version);
        if (prozessbaukastenToFind.isPresent()) {
            Prozessbaukasten prozessbaukasten = prozessbaukastenToFind.get();
            return getHistoryForProzessbaukasten(prozessbaukasten);
        }
        return Collections.emptyList();
    }

    private List<AnforderungHistoryDto> getHistoryForProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        List<ProzessbaukastenHistory> historyList = prozessbaukastenHistoryDao.getByFachIdAndVersion(prozessbaukasten.getFachId(), prozessbaukasten.getVersion());
        List<AnforderungHistoryDto> result = new ArrayList<>();

        historyList.forEach(history -> {
            Long prozessbaukastenId = prozessbaukasten.getId();
            Date zeitpunkt = history.getDatum();
            String benutzer = history.getBenutzer();
            String attribut = history.getChangedField();
            String von = history.getVon();
            String auf = history.getAuf();

            AnforderungHistoryDto dto = AnforderungHistoryDto.newBuilder()
                    .withAnforderungMeldungId(prozessbaukastenId)
                    .withKennzeichen("P")
                    .withZeitpunkt(zeitpunkt)
                    .withBenutzer(benutzer)
                    .withAttribut(attribut)
                    .withVon(von)
                    .withAuf(auf)
                    .build();

            if (!result.contains(dto)) {
                result.add(dto);
            }

        });

        return result;
    }

    public ProzessbaukastenHistory writeHistoryForNewProzessbaukasten(Mitarbeiter user, Prozessbaukasten prozessbaukasten) {
        final ProzessbaukastenHistoryBuilder builder = new ProzessbaukastenHistoryBuilder();

        builder.setProzessbaukasten(prozessbaukasten);
        builder.setDatum(prozessbaukasten.getErstellungsdatum());
        builder.setBenutzer(user.toString());
        builder.setChangedField("Prozessbaukasten");
        builder.setVon("");
        builder.setAuf("neu angelegt");

        ProzessbaukastenHistory newEntry = builder.build();
        save(newEntry);
        return newEntry;
    }

    public ProzessbaukastenHistory writeHistoryForProzessbaukastenNewVersion(Mitarbeiter user, Prozessbaukasten prozessbaukastenNewVersion, int baseVersion, String kommentar) {
        String von = prozessbaukastenNewVersion.getFachId() + " | V" + baseVersion;
        String auf = prozessbaukastenNewVersion.toString();

        return writeHistoryForChangedFieldWithKommentar(prozessbaukastenNewVersion, user, PROZESSBAUKASTENVERSION, von, auf, kommentar);
    }

    public void writeHistoryForChangedProzessbaukasten(Mitarbeiter user, Prozessbaukasten prozessbaukastenNew, Prozessbaukasten prozessbaukastenOld) {

        try {
            for (String fieldName : BeanUtils.describe(prozessbaukastenNew).keySet()) {
                Object oldValue = PropertyUtils.getProperty(prozessbaukastenOld, fieldName);
                Object newValue = PropertyUtils.getProperty(prozessbaukastenNew, fieldName);

                if (HISTORY_FIELDS.contains(fieldName) && isPropertyValueChanged(newValue, oldValue)) {

                    if (ANFORDERUNGEN.equals(fieldName)) {
                        writeHistoryForChangedAnforungList(user, prozessbaukastenNew, prozessbaukastenOld);

                    } else if (KONZEPTE.equals(fieldName)) {
                        prozessbaukastenKonzeptHistoryService.writeHistoryForKonzepte(prozessbaukastenOld, prozessbaukastenNew, user);

                    } else {
                        String newFieldValue = convertPropertyValueToString(newValue);
                        String oldFieldValue = convertPropertyValueToString(oldValue);
                        writeHistoryForChangedField(prozessbaukastenNew, user, fieldName, oldFieldValue, newFieldValue);
                    }
                }

            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            LOG.error(null, ex);
        }
    }

    private void writeHistoryForChangedAnforungList(Mitarbeiter user, Prozessbaukasten prozessbaukastenNew, Prozessbaukasten prozessbaukastenOld) {
        prozessbaukastenNew.getAnforderungen().stream().filter(anforderung -> (!prozessbaukastenOld.getAnforderungen().contains(anforderung))).forEachOrdered(anforderung -> {
            writeHistoryForChangedField(prozessbaukastenNew, user, ANFORDERUNGEN, anforderung.toString(), ZUGEORDNET);
            anforderungHistoryService.writeHistoryForChangedKeyValue(user, anforderung, prozessbaukastenNew.getNameCompleteName(), ZUGEORDNET, ADD_ANFORDERUNG_TO_PROZESSBAUKASTEN);

        });

        prozessbaukastenOld.getAnforderungen().stream().filter(anforderung -> (!prozessbaukastenNew.getAnforderungen().contains(anforderung))).forEachOrdered(anforderung -> {
            writeHistoryForChangedField(prozessbaukastenNew, user, ANFORDERUNGEN, anforderung.toString(), "Zuordnung entfernt");
            anforderungHistoryService.writeHistoryForChangedKeyValue(user, anforderung, "Zuordnung entfernt", prozessbaukastenNew.getNameCompleteName(), ADD_ANFORDERUNG_TO_PROZESSBAUKASTEN);
        });
    }

    private String convertPropertyValueToString(Object toConvert) {
        String valueAsString = "";

        if (!String.valueOf(toConvert).equals("null")) {
            valueAsString = String.valueOf(toConvert).replace("[", "").replace("]", "").replace("{", "").replace("}", "");

        }
        return valueAsString;
    }

    private boolean isPropertyValueChanged(Object newValue, Object oldValue) {
        if (oldValue == null && newValue != null) {
            return true;
        } else if (oldValue != null && newValue == null) {
            return true;
        } else if (oldValue != null && newValue != null) {
            return newValue != oldValue || !newValue.equals(oldValue);
        }

        return false;
    }

    public void writeHistoryByAnforderungenZuordnungToNewVersion(Prozessbaukasten prozessbaukasten, Mitarbeiter user, List<Anforderung> anforderungen) {
        anforderungen.forEach(anforderung -> {
            writeHistoryForChangedField(prozessbaukasten, user, ANFORDERUNGEN, ZUGEORDNET, anforderung.toString());
            anforderungHistoryService.writeHistoryForChangedKeyValue(user, anforderung, prozessbaukasten.getNameCompleteName(), ZUGEORDNET, ADD_ANFORDERUNG_TO_PROZESSBAUKASTEN);
        });
    }

    public void writeHistoryForAddedAnforderungKonzept(ProzessbaukastenAnforderungKonzept anforderungKonzept, Mitarbeiter user) {
        writeHistoryForAddedOrRemovedAnforderungKonzept(anforderungKonzept, user, ZUGEORDNET);
    }

    public void writeHistoryForRemovedAnforderungKonzept(ProzessbaukastenAnforderungKonzept anforderungKonzept, Mitarbeiter user) {
        writeHistoryForAddedOrRemovedAnforderungKonzept(anforderungKonzept, user, "entfernt");
    }

    private void writeHistoryForAddedOrRemovedAnforderungKonzept(ProzessbaukastenAnforderungKonzept anforderungKonzept, Mitarbeiter user, String ergebnisName) {
        String fieldNamePrefix = anforderungKonzept.getAnforderung().toString();
        String fieldNameSuffix = HISTORY_CHANGEDFIELD_MAP.get("prozessbaukastenAnforderungKonzept");
        String konzeptName = anforderungKonzept.getKonzept().getBezeichnung();
        writeHistoryForChangedField(anforderungKonzept.getProzessbaukasten(), user, fieldNamePrefix, fieldNameSuffix, konzeptName, ergebnisName);
    }

    public void writeHistoryForChangedKonzeptFields(Prozessbaukasten prozessbaukasten, Mitarbeiter user, String fieldName, String konzeptBezeichnung, String oldFieldValue, String newFieldValue) {
        writeHistoryForChangedField(prozessbaukasten, user, fieldName, konzeptBezeichnung, oldFieldValue, newFieldValue);
    }

    private void writeHistoryForChangedField(Prozessbaukasten prozessbaukasten, Mitarbeiter user, String fieldName, String oldFieldValue, String newFieldValue) {
        writeHistoryForChangedField(prozessbaukasten, user, HISTORY_CHANGEDFIELD_MAP.get(fieldName), "", oldFieldValue, newFieldValue);
    }

    private Optional<ProzessbaukastenHistory> writeHistoryForChangedField(Prozessbaukasten prozessbaukasten, Mitarbeiter user, String fieldNamePrefix, String fieldNameSuffix, String oldFieldValue, String newFieldValue) {
        if (!newFieldValue.equals(oldFieldValue) && !"IndirectList: not instantiated".equals(oldFieldValue)) {

            final ProzessbaukastenHistoryBuilder builder = new ProzessbaukastenHistoryBuilder();
            builder.setProzessbaukasten(prozessbaukasten);
            builder.setBenutzer(user.toString());
            builder.setChangedField(fieldNamePrefix.concat(" ").concat(fieldNameSuffix));
            builder.setVon(oldFieldValue);
            builder.setAuf(newFieldValue);

            ProzessbaukastenHistory newEntry = builder.build();
            save(newEntry);
            return Optional.of(newEntry);
        } else {
            return Optional.empty();
        }
    }

    public ProzessbaukastenHistory writeHistoryForChangedFieldWithKommentar(Prozessbaukasten prozessbaukasten, Mitarbeiter user, String fieldName, String oldFieldValue, String newFieldValue, String kommentar) {
        writeHistoryForChangedField(prozessbaukasten, user, fieldName, oldFieldValue, newFieldValue);
        String kommentarField = getKommentarNameForField(fieldName);
        return writeKommentarForChangedField(prozessbaukasten, user, kommentarField, kommentar);
    }

    public ProzessbaukastenHistory writeKommentarForChangedField(Prozessbaukasten prozessbaukasten, Mitarbeiter user, String fieldName, String kommentar) {

        final ProzessbaukastenHistoryBuilder builder = new ProzessbaukastenHistoryBuilder();

        builder.setProzessbaukasten(prozessbaukasten);
        builder.setBenutzer(user.toString());
        builder.setChangedField(fieldName);
        builder.setVon("");
        builder.setAuf(kommentar);

        ProzessbaukastenHistory newEntry = builder.build();
        save(newEntry);

        return newEntry;
    }

    private String getKommentarNameForField(String fieldName) {
        fieldName = fieldName.toLowerCase();
        switch (fieldName) {
            case STATUS:
                return LocalizationService.getGermanValue("kommentarStatuswechsel");
            case PROZESSBAUKASTENVERSION:
                return LocalizationService.getGermanValue("versionplusKommentar");
            default:
                return "";
        }

    }

}
