package de.interfaceag.bmw.pzbk.prozessbaukasten;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

final class ProzessbaukastenExcelExportGenerator {

    private ProzessbaukastenExcelExportGenerator() {
    }

    private static final Logger LOG = LoggerFactory.getLogger(ProzessbaukastenExcelExportGenerator.class);

    static void generateExcelExport(Workbook workbook, List<ProzessbaukastenAnforderung> prozessbaukastenAnforderungen) {
        LOG.debug("create excel export for {}", prozessbaukastenAnforderungen);

        Sheet sheet = workbook.createSheet("Anforderungen");
        createHeaderRow(sheet);
        createContent(sheet, prozessbaukastenAnforderungen);
    }

    private static void createContent(Sheet sheet, List<ProzessbaukastenAnforderung> prozessbaukastenAnforderungen) {

        int rowNumber = 0;

        for (ProzessbaukastenAnforderung prozessbaukastenAnforderung : prozessbaukastenAnforderungen) {

            rowNumber++;
            final Row row = sheet.createRow(rowNumber);

            addIdColumn(prozessbaukastenAnforderung, row);
            addBeschreibungColumn(prozessbaukastenAnforderung, row);
            addSensorCocColumn(prozessbaukastenAnforderung, row);
            addUmsetzerColumn(prozessbaukastenAnforderung, row);
            addKonzeptColumn(prozessbaukastenAnforderung, row);
            addThemenklammerColumn(prozessbaukastenAnforderung, row);
            addStatusColumn(prozessbaukastenAnforderung, row);

        }

    }

    private static void addIdColumn(ProzessbaukastenAnforderung prozessbaukastenAnforderung, Row row) {
        row.createCell(0).setCellValue(prozessbaukastenAnforderung.getFachIdAndVersion());
    }

    private static void addBeschreibungColumn(ProzessbaukastenAnforderung prozessbaukastenAnforderung, Row row) {
        row.createCell(1).setCellValue(prozessbaukastenAnforderung.getBeschreibung());
    }

    private static void addSensorCocColumn(ProzessbaukastenAnforderung prozessbaukastenAnforderung, Row row) {
        row.createCell(2).setCellValue(prozessbaukastenAnforderung.getSensorCoc());
    }

    private static void addUmsetzerColumn(ProzessbaukastenAnforderung prozessbaukastenAnforderung, Row row) {
        row.createCell(3).setCellValue(prozessbaukastenAnforderung.getUmsetzer());
    }

    private static void addKonzeptColumn(ProzessbaukastenAnforderung prozessbaukastenAnforderung, Row row) {
        row.createCell(4).setCellValue(prozessbaukastenAnforderung.getKonzept());
    }

    private static void addThemenklammerColumn(ProzessbaukastenAnforderung prozessbaukastenAnforderung, Row row) {
        row.createCell(5).setCellValue(prozessbaukastenAnforderung.getThemenklammer());
    }

    private static void addStatusColumn(ProzessbaukastenAnforderung prozessbaukastenAnforderung, Row row) {
        row.createCell(6).setCellValue(prozessbaukastenAnforderung.getStatus());
    }

    private static void createHeaderRow(Sheet sheet) {
        Row headerRow = sheet.createRow(0);
        headerRow.createCell(0).setCellValue("Id");
        headerRow.createCell(1).setCellValue("Beschreibung");
        headerRow.createCell(2).setCellValue("Anf.-Geber");
        headerRow.createCell(3).setCellValue("Umsetzer");
        headerRow.createCell(4).setCellValue("Konzept");
        headerRow.createCell(5).setCellValue("Themenklammer");
        headerRow.createCell(6).setCellValue("Status");
    }

}
