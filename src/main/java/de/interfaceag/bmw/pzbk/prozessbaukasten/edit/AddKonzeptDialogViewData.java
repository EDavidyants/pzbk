package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required.ProzessbaukastenRequiredAttributes;
import org.primefaces.context.RequestContext;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl
 */
public class AddKonzeptDialogViewData implements Serializable, AddKonzeptDialog {

    private Konzept konzept;
    private final List<Konzept> konzepte;
    private final ProzessbaukastenRequiredAttributes requiredAttributes;

    public AddKonzeptDialogViewData(ProzessbaukastenRequiredAttributes requiredAttributes, List<Konzept> konzepte) {
        this.requiredAttributes = requiredAttributes;
        this.konzepte = konzepte;
        resetKonzept();
    }

    @Override
    public Konzept getKonzept() {
        return konzept;
    }

    @Override
    public void setKonzept(Konzept konzept) {
        this.konzept = konzept;
    }

    @Override
    public void addKonzept() {
        this.konzepte.add(0, konzept);
        hideDialog();
        resetDialog();
    }

    private void hideDialog() {
        RequestContext requestContext = getRequestContext();
        if (requestContext != null) {
            requestContext.execute("PF('addKonzeptDialog').hide();");
        }
    }

    private static RequestContext getRequestContext() {
        return RequestContext.getCurrentInstance();
    }

    private void updateDialog() {
        RequestContext requestContext = getRequestContext();
        if (requestContext != null) {
            requestContext.update("dialog:addKonzeptDialogForm");
        }
    }

    private void resetKonzept() {
        this.konzept = new Konzept();
    }

    private void resetDialog() {
        resetKonzept();
        updateDialog();
    }

    @Override
    public KonzeptAnhangUploader getAnhangUploader() {
        return new KonzeptAnhangUploader(konzept);
    }

    @Override
    public ProzessbaukastenRequiredAttributes getRequiredAttributes() {
        return requiredAttributes;
    }

}
