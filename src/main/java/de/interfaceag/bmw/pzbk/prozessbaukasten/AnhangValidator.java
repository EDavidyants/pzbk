package de.interfaceag.bmw.pzbk.prozessbaukasten;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author sl
 */
public final class AnhangValidator {

    private AnhangValidator() {
    }

    public static boolean isAnhangValid(FileUploadEvent event) {
        UploadedFile file = event.getFile();
        return file != null;
    }

    public static boolean isAnhangValidImage(FileUploadEvent event) {
        UploadedFile file = event.getFile();
        if (file != null) {
            String contentType = file.getContentType();
            return isImage(contentType);
        } else {
            return false;

        }
    }

    private static boolean isImage(String contentType) {
        return "image/png".equals(contentType) || "image/jpeg".equals(contentType);
    }

}
