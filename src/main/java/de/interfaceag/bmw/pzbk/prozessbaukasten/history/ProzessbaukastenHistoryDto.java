package de.interfaceag.bmw.pzbk.prozessbaukasten.history;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author evda
 */
public final class ProzessbaukastenHistoryDto implements Serializable {

    private final Long prozessbaukastenId;
    private final String fachId;
    private final int version;
    private final String zeitpunkt;
    private final String attribut;
    private final String benutzer;
    private final String von;
    private final String auf;

    private ProzessbaukastenHistoryDto(Long prozessbaukastenId, String fachId, int version, String zeitpunkt,
            String benutzer, String attribut, String von, String auf) {

        this.prozessbaukastenId = prozessbaukastenId;
        this.fachId = fachId;
        this.version = version;
        this.zeitpunkt = zeitpunkt;
        this.benutzer = benutzer;
        this.attribut = attribut;
        this.von = von;
        this.auf = auf;
    }

    @Override
    public String toString() {
        return fachId + "| V" + version + ": " + zeitpunkt + " | " + benutzer + " | " + attribut + " | Von: " + von + " | Auf: " + auf;
    }

    public static ProzessbaukastenHistoryDtoBuilder newBuilder() {
        return new ProzessbaukastenHistoryDtoBuilder();
    }

    public static final class ProzessbaukastenHistoryDtoBuilder {

        Long prozessbaukastenId;
        String fachId;
        int version;
        String zeitpunkt;
        String benutzer;
        String attribut;
        String von;
        String auf;

        public ProzessbaukastenHistoryDtoBuilder() {

        }

        public ProzessbaukastenHistoryDtoBuilder withProzessbaukastenId(Long prozessbaukastenId) {
            this.prozessbaukastenId = prozessbaukastenId;
            return this;
        }

        public ProzessbaukastenHistoryDtoBuilder withFachId(String fachId) {
            this.fachId = fachId;
            return this;
        }

        public ProzessbaukastenHistoryDtoBuilder withVersion(int version) {
            this.version = version;
            return this;
        }

        public ProzessbaukastenHistoryDtoBuilder withZeitpunkt(Date datum) {
            if (datum == null) {
                return this;
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            this.zeitpunkt = sdf.format(datum);
            return this;
        }

        public ProzessbaukastenHistoryDtoBuilder withBenutzer(String benutzer) {
            this.benutzer = benutzer;
            return this;
        }

        public ProzessbaukastenHistoryDtoBuilder withAttribut(String attribut) {
            this.attribut = attribut;
            return this;
        }

        public ProzessbaukastenHistoryDtoBuilder withVon(String von) {
            this.von = von;
            return this;
        }

        public ProzessbaukastenHistoryDtoBuilder withAuf(String auf) {
            this.auf = auf;
            return this;
        }

        public ProzessbaukastenHistoryDto build() {
            return new ProzessbaukastenHistoryDto(prozessbaukastenId, fachId, version, zeitpunkt, benutzer, attribut, von, auf);
        }

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.prozessbaukastenId);
        hash = 29 * hash + Objects.hashCode(this.fachId);
        hash = 29 * hash + this.version;
        hash = 29 * hash + Objects.hashCode(this.zeitpunkt);
        hash = 29 * hash + Objects.hashCode(this.attribut);
        hash = 29 * hash + Objects.hashCode(this.benutzer);
        hash = 29 * hash + Objects.hashCode(this.von);
        hash = 29 * hash + Objects.hashCode(this.auf);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProzessbaukastenHistoryDto other = (ProzessbaukastenHistoryDto) obj;
        if (this.version != other.version) {
            return false;
        }
        if (!Objects.equals(this.fachId, other.fachId)) {
            return false;
        }
        if (!Objects.equals(this.zeitpunkt, other.zeitpunkt)) {
            return false;
        }
        if (!Objects.equals(this.attribut, other.attribut)) {
            return false;
        }
        if (!Objects.equals(this.benutzer, other.benutzer)) {
            return false;
        }
        if (!Objects.equals(this.von, other.von)) {
            return false;
        }
        if (!Objects.equals(this.auf, other.auf)) {
            return false;
        }
        if (!Objects.equals(this.prozessbaukastenId, other.prozessbaukastenId)) {
            return false;
        }
        return true;
    }

    //------------ Getter -------------
    public Long getProzessbaukastenId() {
        return prozessbaukastenId;
    }

    public String getFachId() {
        return fachId;
    }

    public int getVersion() {
        return version;
    }

    public String getZeitpunkt() {
        return zeitpunkt;
    }

    public String getAttribut() {
        return attribut;
    }

    public String getBenutzer() {
        return benutzer;
    }

    public String getVon() {
        return von;
    }

    public String getAuf() {
        return auf;
    }

}
