package de.interfaceag.bmw.pzbk.prozessbaukasten.sort;

public final class ProzessbaukastenAnforderungOrderBuilder {

    private ProzessbaukastenAnforderungTuple prozessbaukastenAnforderungTuple;
    private Long position;

    private ProzessbaukastenAnforderungOrderBuilder() {
    }

    public static ProzessbaukastenAnforderungOrderBuilder withProzessbaukastenAnforderungTuple(ProzessbaukastenAnforderungTuple prozessbaukastenAnforderungTuple) {
        return new ProzessbaukastenAnforderungOrderBuilder().setProzessbaukastenAnforderungTuple(prozessbaukastenAnforderungTuple);
    }

    private ProzessbaukastenAnforderungOrderBuilder setProzessbaukastenAnforderungTuple(ProzessbaukastenAnforderungTuple prozessbaukastenAnforderungTuple) {
        this.prozessbaukastenAnforderungTuple = prozessbaukastenAnforderungTuple;
        return this;
    }

    public ProzessbaukastenAnforderungOrderBuilder withPosition(Long position) {
        this.position = position;
        return this;
    }

    public ProzessbaukastenAnforderungOrder build() {
        return new ProzessbaukastenAnforderungOrderDto(prozessbaukastenAnforderungTuple, position);
    }

}
