package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.ContentType;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.prozessbaukasten.history.ProzessbaukastenHistoryService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenVersionMenuDto;
import de.interfaceag.bmw.pzbk.services.FileService;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;
import org.primefaces.model.menu.DefaultMenuItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * @author evda
 */
@Stateless
@Named
public class ProzessbaukastenService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ProzessbaukastenService.class.getName());

    @Inject
    private ProzessbaukastenDao prozessbaukastenDao;
    @Inject
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;
    @Inject
    private ProzessbaukastenImageUpdateService imageUpdateService;
    @Inject
    private FileService fileService;

    public Prozessbaukasten getProzessbaukastenForAnforderung(Anforderung anforderung) {
        if (anforderung != null) {
            return (prozessbaukastenDao.getProzessbaukastenByAnforderungId(anforderung.getId())).orElse(null);
        }
        return null;
    }

    public Prozessbaukasten save(Prozessbaukasten prozessbaukasten) {
        LOG.debug("Save Prozessbaukasten {}", prozessbaukasten);

        Prozessbaukasten savedProzessbaukasten = prozessbaukastenDao.save(prozessbaukasten);

        if (savedProzessbaukasten.getId() != null && prozessbaukasten.getFachId().equals("")) {
            prozessbaukasten.setFachId("P" + prozessbaukasten.getId());
        }
        return savedProzessbaukasten;
    }

    private void save(Prozessbaukasten prozessbaukasten, Set<Anhang> uploadedAnhaenge, Set<Long> removedAnhaenge, Date datum) {
        prozessbaukasten = save(prozessbaukasten);

        prozessbaukasten.setAenderungsdatum(datum);
        updateImages(prozessbaukasten, uploadedAnhaenge, removedAnhaenge);
        prozessbaukastenDao.save(prozessbaukasten);
    }

    private void updateImages(Prozessbaukasten prozessbaukasten, Set<Anhang> uploadedAnhaenge, Set<Long> removedAnhaenge) {
        removeAnhaenge(removedAnhaenge);
        saveAnhaenge(prozessbaukasten, uploadedAnhaenge);
        imageUpdateService.updateProzessbaukastenBilder(prozessbaukasten);
    }

    public void saveNewVersionWithHistory(Prozessbaukasten prozessbaukasten, Set<Anhang> uploadedAnhaenge, Set<Long> removedAnhaenge, Prozessbaukasten prozessbaukastenOrigin, List<Anforderung> zugeordneteAnforderungen, Mitarbeiter user, String kommentarOptional) {
        prozessbaukastenDao.save(prozessbaukasten);
        updateImages(prozessbaukasten, uploadedAnhaenge, removedAnhaenge);
        String kommentar = (kommentarOptional != null && !kommentarOptional.isEmpty()) ? kommentarOptional : "neue Version angelegt";
        writeHistoryForNewVersion(prozessbaukasten, prozessbaukastenOrigin, user, kommentar);

        if (zugeordneteAnforderungen != null) {
            prozessbaukastenHistoryService.writeHistoryByAnforderungenZuordnungToNewVersion(prozessbaukasten, user, zugeordneteAnforderungen);
        }

    }

    private void removeAnhaenge(Set<Long> removedAnhaenge) {
        removedAnhaenge.forEach(anhangId -> fileService.removeAnhangWithId(anhangId));
    }

    public void saveWithHistory(Prozessbaukasten prozessbaukastenNew, Prozessbaukasten prozessbaukastenOld, Mitarbeiter mitarbeiter, Date datum) {
        saveWithHistory(prozessbaukastenNew, prozessbaukastenOld, mitarbeiter, new HashSet<>(), new HashSet<>(), Optional.empty(), datum);
    }

    public void saveWithHistory(Prozessbaukasten prozessbaukastenNew, Prozessbaukasten prozessbaukastenOldOptional, Mitarbeiter mitarbeiter, Set<Anhang> uploadedAnhaenge, Set<Long> removedAnhaenge, Optional<String> kommentarOptional) {
        saveWithHistory(prozessbaukastenNew, prozessbaukastenOldOptional, mitarbeiter, uploadedAnhaenge, removedAnhaenge, kommentarOptional, new Date());
    }

    private void saveWithHistory(Prozessbaukasten prozessbaukastenNew, Prozessbaukasten prozessbaukastenOldOptional, Mitarbeiter mitarbeiter, Set<Anhang> uploadedAnhaenge, Set<Long> removedAnhaenge, Optional<String> kommentarOptional, Date datum) {
        save(prozessbaukastenNew, uploadedAnhaenge, removedAnhaenge, datum);
        writeHistoryForProzessbaukasten(prozessbaukastenNew, prozessbaukastenOldOptional, mitarbeiter, kommentarOptional);
    }

    private void writeHistoryForProzessbaukasten(Prozessbaukasten prozessbaukastenNew, Prozessbaukasten prozessbaukastenOldOptional, Mitarbeiter mitarbeiter, Optional<String> kommentarOptional) {
        if (isNewProzessbaukasten(prozessbaukastenOldOptional)) {
            prozessbaukastenHistoryService.writeHistoryForNewProzessbaukasten(mitarbeiter, prozessbaukastenNew);

        } else if (isNewVersion(prozessbaukastenOldOptional, prozessbaukastenNew)) {
            String kommentar = kommentarOptional.orElse("neue Version angelegt");
            writeHistoryForNewVersion(prozessbaukastenNew, prozessbaukastenOldOptional, mitarbeiter, kommentar);

        } else {
            writeHistoryForUpdatedProzessbaukasten(prozessbaukastenNew, prozessbaukastenOldOptional, mitarbeiter);
        }
    }

    private void writeHistoryForNewVersion(Prozessbaukasten prozessbaukastenNew, Prozessbaukasten prozessbaukastenOld, Mitarbeiter mitarbeiter, String kommentar) {
        if (Objects.nonNull(prozessbaukastenOld)) {
            prozessbaukastenHistoryService.writeHistoryForProzessbaukastenNewVersion(mitarbeiter, prozessbaukastenNew, prozessbaukastenOld.getVersion(), kommentar);
        }
    }

    private void writeHistoryForUpdatedProzessbaukasten(Prozessbaukasten prozessbaukastenNew, Prozessbaukasten prozessbaukastenOld, Mitarbeiter mitarbeiter) {
        if (Objects.nonNull(prozessbaukastenOld)) {
            prozessbaukastenHistoryService.writeHistoryForChangedProzessbaukasten(mitarbeiter, prozessbaukastenNew, prozessbaukastenOld);
        }
    }

    private static boolean isNewProzessbaukasten(Prozessbaukasten prozessbaukastenOld) {
        return Objects.isNull(prozessbaukastenOld);
    }

    private static boolean isNewVersion(Prozessbaukasten prozessbaukastenOld, Prozessbaukasten prozessbaukastenNew) {
        if (Objects.isNull(prozessbaukastenOld)) {
            return false;
        }

        return prozessbaukastenOld.getFachId().equals(prozessbaukastenNew.getFachId())
                && prozessbaukastenOld.getVersion() != prozessbaukastenNew.getVersion();

    }

    public Prozessbaukasten createCopyForNewVersion(Prozessbaukasten prozessbaukasten) {
        if (isPersisted(prozessbaukasten)) {
            Prozessbaukasten prozessbaukastenCopy = new Prozessbaukasten(prozessbaukasten.getFachId(), prozessbaukasten.getVersion());
            prozessbaukastenCopy.setVersion(getNextVersionNumberByFachId(prozessbaukasten.getFachId()));
            prozessbaukastenCopy.setBeschreibung(prozessbaukasten.getBeschreibung());
            prozessbaukastenCopy.setBezeichnung(prozessbaukasten.getBezeichnung());
            prozessbaukastenCopy.setLeadTechnologie(prozessbaukasten.getLeadTechnologie());
            prozessbaukastenCopy.setStandardisierterFertigungsprozess(prozessbaukasten.getStandardisierterFertigungsprozess());
            prozessbaukastenCopy.setTteam(prozessbaukasten.getTteam());
            prozessbaukastenCopy.setErstanlaeufer(prozessbaukasten.getErstanlaeufer());
            prozessbaukastenCopy.setAnforderungen(generateAnforderungListForNewVersion(prozessbaukasten));
            prozessbaukastenCopy.setKonzepte(createKonzepteCopy(prozessbaukasten));

            return prozessbaukastenCopy;
        }

        return new Prozessbaukasten();
    }

    private List<Anforderung> generateAnforderungListForNewVersion(Prozessbaukasten prozessbaukastenOrigin) {
        return new ArrayList<>(prozessbaukastenOrigin.getAnforderungen());
    }

    private List<Konzept> createKonzepteCopy(Prozessbaukasten prozessbaukasten) {

        List<Konzept> konzepte = prozessbaukasten.getKonzepte();
        List<Konzept> konzepteCopied = new ArrayList<>();

        konzepte.forEach(konzept -> {
            Konzept konzeptCopy = konzept.getCopy();
            konzepteCopied.add(konzeptCopy);
        });

        return konzepteCopied;
    }

    private int getNextVersionNumberByFachId(String fachId) {
        int lastVersionNumber = prozessbaukastenDao.getLastVersionNumberByFachId(fachId).orElse(0);
        return lastVersionNumber + 1;
    }

    public Optional<Prozessbaukasten> createRealCopy(Prozessbaukasten prozessbaukasten) {
        if (isPersisted(prozessbaukasten)) {
            Prozessbaukasten prozessbaukastenCopy = new Prozessbaukasten(prozessbaukasten.getFachId(), prozessbaukasten.getVersion());
            prozessbaukastenCopy.setAenderungsdatum(prozessbaukasten.getAenderungsdatum());
            prozessbaukastenCopy.setAnforderungen(new ArrayList<>(prozessbaukasten.getAnforderungen()));
            prozessbaukastenCopy.setBeschreibung(prozessbaukasten.getBeschreibung());
            prozessbaukastenCopy.setBezeichnung(prozessbaukasten.getBezeichnung());
            prozessbaukastenCopy.setErstellungsdatum(prozessbaukasten.getErstellungsdatum());
            prozessbaukastenCopy.setId(prozessbaukasten.getId());
            prozessbaukastenCopy.setLeadTechnologie(prozessbaukasten.getLeadTechnologie());
            prozessbaukastenCopy.setStandardisierterFertigungsprozess(prozessbaukasten.getStandardisierterFertigungsprozess());
            prozessbaukastenCopy.setStatus(prozessbaukasten.getStatus());
            prozessbaukastenCopy.setTteam(prozessbaukasten.getTteam());
            prozessbaukastenCopy.setErstanlaeufer(prozessbaukasten.getErstanlaeufer());
            prozessbaukastenCopy.setStartbrief(prozessbaukasten.getStartbrief());
            prozessbaukastenCopy.setGrafikUmfang(prozessbaukasten.getGrafikUmfang());
            prozessbaukastenCopy.setKonzeptbaum(prozessbaukasten.getKonzeptbaum());
            prozessbaukastenCopy.setBeauftragungTPK(prozessbaukasten.getBeauftragungTPK());
            prozessbaukastenCopy.setGenehmigungTPK(prozessbaukasten.getGenehmigungTPK());

            prozessbaukastenCopy.setKonzepte(createKonzepteCopyForHistory(prozessbaukasten));

            prozessbaukasten.getWeitereAnhaenge().forEach(anhang -> prozessbaukastenCopy.addWeiterenAnhang(anhang.getCopy()));

            return Optional.of(prozessbaukastenCopy);
        } else {
            return Optional.empty();
        }

    }

    private List<Konzept> createKonzepteCopyForHistory(Prozessbaukasten prozessbaukasten) {

        List<Konzept> konzepte = prozessbaukasten.getKonzepte();
        List<Konzept> konzepteCopied = new ArrayList<>();

        konzepte.forEach(konzept -> {
            Konzept konzeptCopy = konzept.createCopyForHistory();
            konzepteCopied.add(konzeptCopy);
        });

        return konzepteCopied;
    }

    private static boolean isPersisted(Prozessbaukasten prozessbaukasten) {
        return prozessbaukasten != null && prozessbaukasten.getId() != null;
    }

    public void saveChangesToAttributAndWriteHistoryWithKommentar(Prozessbaukasten prozessbaukasten, Mitarbeiter user, String attributName, String oldValue, String newValue, String kommentar, Date datum) {
        save(prozessbaukasten, new HashSet<>(), new HashSet<>(), datum);
        prozessbaukastenHistoryService.writeHistoryForChangedFieldWithKommentar(prozessbaukasten, user, attributName, oldValue, newValue, kommentar);
    }

    private void saveAnhaenge(Prozessbaukasten prozessbaukasten, Set<Anhang> uploadedAnhaenge) {
        uploadedAnhaenge.forEach(anhang -> fileService.writeAnhangToFile(anhang, ContentType.PZBK, prozessbaukasten.getId()));
    }

    public List<ProzessbaukastenVersionMenuDto> fetchVersionenForFachId(String fachId) {
        return prozessbaukastenDao.fetchVersionenForFachId(fachId);
    }

    public DefaultMenuItem generateVersionMenuItem(ProzessbaukastenVersionMenuDto version, String currentVersion) {
        Integer currentVersionNumber;
        DefaultMenuItem item = new DefaultMenuItem();

        if (isValidVersion(currentVersion)) {
            currentVersionNumber = Integer.parseInt(currentVersion);
            item.setUrl(Page.PROZESSBAUKASTEN_VIEW.getUrl() + "?fachId=" + version.getFachId() + "&version=" + version.getVersion() + "&faces-redirect=true");
            item.setIcon("ui-icon-arrow-1-e");
            String versionLabel = generateVersionLabel(version);
            item.setValue(versionLabel);
            if (version.getVersion().equals(currentVersionNumber)) {
                item.setStyleClass("current");
            }
        }

        return item;
    }

    private static boolean isValidVersion(String version) {
        return RegexUtils.matchesVersion(version);
    }

    private String generateVersionLabel(ProzessbaukastenVersionMenuDto versionData) {
        SimpleDateFormat toformat = new SimpleDateFormat("dd.MM.yyyy");
        String versionLabel;
        String versionString = versionData.getVersion().toString();

        if (versionData.getErstellungsdatum() == null) {
            versionLabel = 'V' + versionString;
        } else {
            String datum = toformat.format(versionData.getErstellungsdatum());
            versionLabel = "V" + versionString + " " + datum;
        }

        return versionLabel;
    }

}
