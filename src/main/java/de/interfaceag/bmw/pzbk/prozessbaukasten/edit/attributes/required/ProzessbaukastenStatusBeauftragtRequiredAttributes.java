package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required;

/**
 *
 * @author sl
 */
public class ProzessbaukastenStatusBeauftragtRequiredAttributes extends ProzessbaukastenStatusAngelegtRequiredAttributes {

    protected ProzessbaukastenStatusBeauftragtRequiredAttributes() {
    }

    @Override
    public boolean isAnhangBeauftragungTPKRequired() {
        return true;
    }

    @Override
    public boolean isAnhangStartbriefRequired() {
        return true;
    }

    @Override
    public boolean isErstanlaeuferRequired() {
        return true;
    }

    @Override
    public boolean isKonzeptBeschreibungRequired() {
        return true;
    }

    @Override
    public boolean isKonzeptBezeichnungRequired() {
        return true;
    }

}
