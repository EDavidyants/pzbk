package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ActiveIndex;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenTab;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import de.interfaceag.bmw.pzbk.staticvalidators.AnforderungValidator;
import de.interfaceag.bmw.pzbk.staticvalidators.ProzessbaukastenValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author fn
 */
@Stateless
@Named
public class ProzessbaukastenZuordnenService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ProzessbaukastenZuordnenService.class);

    @Inject
    protected AnforderungService anforderungService;
    @Inject
    protected Session session;
    @Inject
    protected ProzessbaukastenReadService prozessbaukastenReadService;
    @Inject
    protected LocalizationService localizationService;
    @Inject
    private ProzessbaukastenCreateAnforderungZuordnungService createAnforderungZuordnungService;
    @Inject
    private ProzessbaukastenRemoveAnforderungZuordnungService removeAnforderungZuordnungService;

    public ProzessbaukastenZuordnenDialogViewData getAddProzessbaukastenDialogViewData(Anforderung anforderung) {

        if (AnforderungValidator.validatAnforderung(anforderung)) {
            return getViewDataForAnforderung();
        } else {
            return ProzessbaukastenZuordnenDialogViewData.empty();
        }

    }

    private ProzessbaukastenZuordnenDialogViewData getViewDataForAnforderung() {
        ProzessbaukastenZuordnenDialogViewData viewData = new ProzessbaukastenZuordnenDialogViewData();

        this.setZuortenbareProzessbaukastenDTOtoViewData(viewData);

        return viewData;
    }

    private void setZuortenbareProzessbaukastenDTOtoViewData(ProzessbaukastenZuordnenDialogViewData viewData) {
        List<ProzessbaukastenZuordnenDTO> zuortenbareProzessbaukaesten;
        if (session.getUserPermissions().getRollen().contains(Rolle.TTEAMMITGLIED)) {
            zuortenbareProzessbaukaesten = prozessbaukastenReadService.getAllZuweisbareProzessbaukaesten().stream()
                    .filter(p -> session.getUserPermissions().getTteamIdsForTteamMitgliedSchreibend().contains(p.getTteam().getId()))
                    .map(prozessbaukasten -> new ProzessbaukastenZuordnenDTO(prozessbaukasten.getId(), prozessbaukasten.getFachId(), prozessbaukasten.getVersion(),
                    prozessbaukasten.getBezeichnung(), prozessbaukasten.getBeschreibung())).collect(Collectors.toList());
        } else {
            zuortenbareProzessbaukaesten = prozessbaukastenReadService.getAllZuweisbareProzessbaukaesten().stream()
                    .map(prozessbaukasten -> new ProzessbaukastenZuordnenDTO(prozessbaukasten.getId(), prozessbaukasten.getFachId(), prozessbaukasten.getVersion(),
                    prozessbaukasten.getBezeichnung(), prozessbaukasten.getBeschreibung())).collect(Collectors.toList());
        }
        viewData.setProzessbaukastenChoosable(zuortenbareProzessbaukaesten);
    }

    public String prozessbaukastenZuordnen(ProzessbaukastenZuordnenDTO prozessbaukastenZuordnenDTO, Long anforderungId) {
        Anforderung anforderung = anforderungService.getAnforderungById(anforderungId);

        if (prozessbaukastenZuordnenDTO != null) {
            Prozessbaukasten prozessbaukasten = prozessbaukastenReadService.getById(prozessbaukastenZuordnenDTO.getId());
            createAnforderungZuordnungService.assignAnforderungToProzessbaukasten(anforderung, prozessbaukasten, session.getUser());
        } else {
            LOG.warn("ProzessbaukastenZuordnenDTO is null");
        }

        return PageUtils.getUrlForPageWithFachIdAndVersionAndId(Page.ANFORDERUNGVIEW, anforderung.getFachId(), anforderung.getVersion(), anforderung.getId());
    }

    public String prozessbaukastenZuordnenByProzessbaukatenAndAnforderung(Prozessbaukasten prozessbaukasten, Anforderung anforderung) {

        if (!AnforderungValidator.validatAnforderung(anforderung) || !ProzessbaukastenValidator.validatProzessbaukasten(prozessbaukasten)) {
            LOG.warn("Anforderung or Przessbaukasten is not valid");
            return "";
        }

        createAnforderungZuordnungService.assignAnforderungToProzessbaukasten(anforderung, prozessbaukasten, session.getUser());

        return PageUtils.getUrlForPageWithFachIdAndVersionAndId(Page.ANFORDERUNGVIEW, anforderung.getFachId(), anforderung.getVersion(), anforderung.getId());
    }

    public String anforderungenProzessbaukastenZuordnen(List<AnforderungenProzessbaukastenZuordnenDTO> zugeordneteAnforderungenDTO, Prozessbaukasten prozessbaukasten) {
        if (!ProzessbaukastenValidator.validatProzessbaukasten(prozessbaukasten) || zugeordneteAnforderungenDTO == null) {
            return "";
        }
        List<Anforderung> zugeordneteAnforderungen = getAnforderungenFromDTO(zugeordneteAnforderungenDTO);

        List<Anforderung> bereitsZugeordneteAnforderungen = checkAnforderungenZugeordnet(zugeordneteAnforderungen);
        Map<Status, List<Anforderung>> nichtZuordenbareAnforderungen = checkAnforderungenStatus(zugeordneteAnforderungen);

        Optional<String> growlTitleOptional = generateGrowlTitle(bereitsZugeordneteAnforderungen, nichtZuordenbareAnforderungen);

        if (growlTitleOptional.isPresent()) {
            String growlTitle = growlTitleOptional.get();
            String growlMessage = generateGrowlMessage(bereitsZugeordneteAnforderungen, nichtZuordenbareAnforderungen);
            return showGrowl(FacesMessage.SEVERITY_ERROR, growlTitle, growlMessage);
        }

        return performAnforderungenZuordnung(prozessbaukasten, zugeordneteAnforderungen);
    }

    public String performAnforderungenZuordnung(Prozessbaukasten prozessbaukasten, List<Anforderung> newlyAddedAnforderungen) {

        for (Anforderung anforderung : newlyAddedAnforderungen) {
            createAnforderungZuordnungService.assignAnforderungToProzessbaukasten(anforderung, prozessbaukasten, session.getUser());
        }

        return PageUtils.getUrlForProzessbaukastenView(prozessbaukasten.getFachId(), prozessbaukasten.getVersion(), ActiveIndex.forTab(ProzessbaukastenTab.ANFORDERUNGEN));
    }

    private Optional<String> generateGrowlTitle(List<Anforderung> bereitsZugeordneteAnforderungen, Map<Status, List<Anforderung>> nichtZuordenbareAnforderungen) {
        String growlTitle;
        if (!bereitsZugeordneteAnforderungen.isEmpty() && !nichtZuordenbareAnforderungen.isEmpty()) {
            growlTitle = localizationService.getValue("prozessbaukasten_anforderungZuordnen_nichtMoeglich");
            return Optional.of(growlTitle);
        } else if (!bereitsZugeordneteAnforderungen.isEmpty()) {
            growlTitle = localizationService.getValue("prozessbaukasten_anforderungZuordnen_bereitsZugeordnetTitle");
            return Optional.of(growlTitle);
        } else if (!nichtZuordenbareAnforderungen.isEmpty()) {
            growlTitle = localizationService.getValue("prozessbaukasten_anforderungZuordnenNichtMoeglich_status");
            return Optional.of(growlTitle);
        }
        return Optional.empty();
    }

    private String generateGrowlMessage(List<Anforderung> bereitsZugeordneteAnforderungen, Map<Status, List<Anforderung>> nichtZuordenbareAnforderungen) {
        StringBuilder sb = new StringBuilder();

        String zugeordnetPrefix = "";
        String statusPrefix = "";
        boolean isSeparatorNeeded = false;

        if (!bereitsZugeordneteAnforderungen.isEmpty() && !nichtZuordenbareAnforderungen.isEmpty()) {
            zugeordnetPrefix = localizationService.getValue("prozessbaukasten_anforderungZuordnen_bereitsZugeordnetMessage") + ": ";
            statusPrefix = "In " + localizationService.getValue("status") + " ";
            isSeparatorNeeded = true;
        }

        sb.append(buildGrowlMessageSchonZugeordnet(zugeordnetPrefix, bereitsZugeordneteAnforderungen, isSeparatorNeeded))
                .append(buildGrowlMessageForStatus(statusPrefix, nichtZuordenbareAnforderungen));
        return sb.toString();
    }

    private String buildGrowlMessageSchonZugeordnet(String prefix, List<Anforderung> bereitsZugeordneteAnforderungen, boolean isSeparatorNeeded) {

        if (!bereitsZugeordneteAnforderungen.isEmpty()) {
            StringBuilder sb = new StringBuilder();

            if (prefix != null && !prefix.isEmpty()) {
                sb.append(prefix);
            }

            sb.append(buildAnforderungenAuflistung(bereitsZugeordneteAnforderungen));

            if (isSeparatorNeeded) {
                sb.append("; ").append(System.lineSeparator());
            }
            return sb.toString();
        }

        return "";
    }

    private List<Anforderung> checkAnforderungenZugeordnet(List<Anforderung> anforderungen) {
        List<Anforderung> zugeordneteAnforderungen = new ArrayList<>();
        anforderungen.stream().filter(Anforderung::isProzessbaukastenZugeordnet).forEachOrdered(anforderung -> {
            zugeordneteAnforderungen.add(anforderung);
        });
        return zugeordneteAnforderungen;
    }

    private String buildGrowlMessageForStatus(String prefix, Map<Status, List<Anforderung>> nichtZuordenbareAnforderungen) {

        if (!nichtZuordenbareAnforderungen.isEmpty()) {
            List<Anforderung> geloeschteAnforderungenList = nichtZuordenbareAnforderungen.get(Status.A_GELOESCHT);
            List<Anforderung> keineWeiterverfolgungAnforderungenList = nichtZuordenbareAnforderungen.get(Status.A_KEINE_WEITERVERFOLG);

            boolean isSeparatorNeeded = (geloeschteAnforderungenList != null && keineWeiterverfolgungAnforderungenList != null);

            StringBuilder sb = new StringBuilder();
            sb.append(buildAnforderungStatusReport(prefix, Status.A_GELOESCHT, geloeschteAnforderungenList, isSeparatorNeeded));
            sb.append(buildAnforderungStatusReport(prefix, Status.A_KEINE_WEITERVERFOLG, keineWeiterverfolgungAnforderungenList, false));
            return sb.toString();
        }

        return "";
    }

    private static String buildAnforderungenAuflistung(List<Anforderung> anforderungen) {

        return anforderungen.stream()
                .map(a -> a.toString())
                .collect(Collectors.joining(", "));

    }

    private String buildAnforderungStatusReport(String prefix, Status status, List<Anforderung> anforderungen, boolean isSeparatorNeeded) {
        StringBuilder sb = new StringBuilder();

        if (isAnforderungenInputValid(anforderungen)) {
            String anforderungenAuflistung = buildAnforderungenAuflistung(anforderungen);

            if (prefix != null && !prefix.isEmpty()) {
                sb.append(prefix);
            }

            sb.append(status.getStatusBezeichnung())
                    .append(": ").append(anforderungenAuflistung);

            if (isSeparatorNeeded) {
                sb.append("; ").append(System.lineSeparator());
            }

        }

        return sb.toString();
    }

    private String showGrowl(Severity severityLevel, String title, String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(severityLevel, title, message));
        return "";
    }

    private Map<Status, List<Anforderung>> checkAnforderungenStatus(List<Anforderung> anforderungen) {
        if (isAnforderungenInputValid(anforderungen)) {
            return anforderungen.stream()
                    .filter(a -> a.getStatus() == Status.A_GELOESCHT || a.getStatus() == Status.A_KEINE_WEITERVERFOLG)
                    .collect(Collectors.groupingBy(Anforderung::getStatus));
        }

        return new HashMap<>();
    }

    private static boolean isAnforderungenInputValid(List<Anforderung> anforderungen) {
        return anforderungen != null && !anforderungen.isEmpty();
    }

    private List<Anforderung> getAnforderungenFromDTO(List<AnforderungenProzessbaukastenZuordnenDTO> zugeordneteAnforderungen) {
        List<Anforderung> anforderungen = new ArrayList<>();
        zugeordneteAnforderungen.forEach(anforderungDTO -> {
            anforderungen.add(anforderungService.getAnforderungByFachIdVersion(anforderungDTO.getFachId(), anforderungDTO.getVersion()));
        });
        return anforderungen;
    }

    public void zuordnungAufheben(Anforderung anforderung, Prozessbaukasten prozessbaukasten, String kommentar) {
        if (isKommentarValid(kommentar)) {
            removeAnforderungZuordnungService.unassignAnforderungFromProzessbaukasten(anforderung, prozessbaukasten, kommentar);
        }
    }

    private static boolean isKommentarValid(String kommentar) {
        return kommentar != null && !kommentar.isEmpty();
    }

}
