package de.interfaceag.bmw.pzbk.prozessbaukasten.dto;

import java.util.Objects;

public class ProzessbaukastenKonzeptChangeDto {

    private final String konzeptBezeichnung;
    private final String fieldName;
    private final String oldValue;
    private final String newValue;

    public ProzessbaukastenKonzeptChangeDto(String konzeptBezeichnung, String fieldName, String oldValue, String newValue) {
        this.konzeptBezeichnung = konzeptBezeichnung;
        this.fieldName = fieldName;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        ProzessbaukastenKonzeptChangeDto that = (ProzessbaukastenKonzeptChangeDto) object;
        return Objects.equals(konzeptBezeichnung, that.konzeptBezeichnung)
                && Objects.equals(fieldName, that.fieldName)
                && Objects.equals(oldValue, that.oldValue)
                && Objects.equals(newValue, that.newValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(konzeptBezeichnung, fieldName, oldValue, newValue);
    }

    public String getKonzeptBezeichnung() {
        return konzeptBezeichnung;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getOldValue() {
        return oldValue;
    }

    public String getNewValue() {
        return newValue;
    }
}
