package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author sl
 */
@Stateless
@Named
public class ProzessbaukastenReadService implements Serializable {

    @Inject
    private ProzessbaukastenDao prozessbaukastenDao;

    public Prozessbaukasten getById(Long id) {
        return prozessbaukastenDao.getById(id);
    }

    public List<ProzessbaukastenFilterDto> getAllFilterDto() {
        return prozessbaukastenDao.getAllFilterDto();
    }

    public List<Prozessbaukasten> getAllZuweisbareProzessbaukaesten() {
        return prozessbaukastenDao.getAllZuweisbareProzessbaukaesten();
    }

    public Optional<Prozessbaukasten> getByFachIdAndVersion(String fachId, int version) {
        return prozessbaukastenDao.getByFachIdAndVersion(fachId, version);
    }

    public List<Prozessbaukasten> getProzessbaukaestenForAnforderungIds(Collection<Long> anforderungIds) {
        return prozessbaukastenDao.getByAnforderungIds(anforderungIds);
    }

    public Optional<Prozessbaukasten> getProzessbaukastenPreviousVersion(Prozessbaukasten prozessbaukasten) {
        String fachId = prozessbaukasten.getFachId();
        int previousVersionIndex = prozessbaukasten.getVersion() - 1;
        return getByFachIdAndVersion(fachId, previousVersionIndex);
    }

}
