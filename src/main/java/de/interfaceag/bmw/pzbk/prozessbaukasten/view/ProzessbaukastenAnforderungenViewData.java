package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenAnforderung;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author evda
 */
public final class ProzessbaukastenAnforderungenViewData implements Serializable {

    private final List<ProzessbaukastenAnforderung> anforderungen;
    private List<ProzessbaukastenAnforderung> filteredAnforderungen;

    private ProzessbaukastenAnforderungenViewData(List<ProzessbaukastenAnforderung> anforderungen, List<ProzessbaukastenAnforderung> filteredAnforderungen) {
        this.anforderungen = anforderungen;
        this.filteredAnforderungen = filteredAnforderungen;
    }

    public List<ProzessbaukastenAnforderung> getAnforderungen() {
        return anforderungen;
    }

    public List<ProzessbaukastenAnforderung> getFilteredAnforderungen() {
        return filteredAnforderungen;
    }

    public void setFilteredAnforderungen(List<ProzessbaukastenAnforderung> filteredAnforderungen) {
        this.filteredAnforderungen = filteredAnforderungen;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public static final class Builder {

        private List<ProzessbaukastenAnforderung> anforderungen;
        private List<ProzessbaukastenAnforderung> filteredAnforderungen;

        private Builder() {
            this.filteredAnforderungen = new ArrayList<>();
        }

        public Builder withAnforderungen(List<ProzessbaukastenAnforderung> anforderungen) {
            this.anforderungen = anforderungen;
            this.filteredAnforderungen = anforderungen;
            return this;
        }

        public Builder withFilteredAnforderungen(List<ProzessbaukastenAnforderung> filteredAnforderungen) {
            this.filteredAnforderungen = filteredAnforderungen;
            return this;
        }

        public ProzessbaukastenAnforderungenViewData build() {
            return new ProzessbaukastenAnforderungenViewData(anforderungen, filteredAnforderungen);
        }

    }

    public static ProzessbaukastenAnforderungenViewData forLeerProzessbaukasten() {
        ProzessbaukastenAnforderungenViewData anforderungenTabViewData = ProzessbaukastenAnforderungenViewData.getBuilder()
                .withAnforderungen(new ArrayList<>())
                .build();
        return anforderungenTabViewData;
    }

}
