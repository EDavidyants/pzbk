package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes;

import java.io.Serializable;

/**
 *
 * @author sl
 */
public interface ProzessbaukastenDisabledAttributes extends Serializable {

    boolean isBezeichnungDisabled();

    boolean isBeschreibungDisabled();

    boolean isTteamDisabled();

    boolean isLeadTechnologieDisabled();

    boolean isKonzeptDisabled();

    boolean isStandardisierterFertigungsprozessDisabled();

    boolean isErstanlaeuferDisabled();

    boolean isAnhangStartbriefDisabled();

    boolean isAnhangGrafikUmfangDisabled();

    boolean isAnhangGrafikKonzeptbaumDisabled();

    boolean isAnhangBeauftragungTPKDisabled();

    boolean isAnhangGenehmigungTPKDisabled();

    boolean isWeitereAnhangeDisabled();

    boolean isAnhangeTabDisabled();

    boolean isAnforderungenTabDisabled();

    boolean isStammdatenDetailsPanelDisabled();

}
