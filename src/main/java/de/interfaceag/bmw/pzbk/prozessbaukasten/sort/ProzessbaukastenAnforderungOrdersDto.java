package de.interfaceag.bmw.pzbk.prozessbaukasten.sort;

import java.util.Map;

/**
 *
 * @author sl
 */
public class ProzessbaukastenAnforderungOrdersDto implements ProzessbaukastenAnforderungOrders {

    private final Map<ProzessbaukastenAnforderungTuple, ProzessbaukastenAnforderungOrder> prozessbaukastenAnforderungOrders;

    public ProzessbaukastenAnforderungOrdersDto(Map<ProzessbaukastenAnforderungTuple, ProzessbaukastenAnforderungOrder> prozessbaukastenAnforderungOrders) {
        this.prozessbaukastenAnforderungOrders = prozessbaukastenAnforderungOrders;
    }

    @Override
    public int getOrderForProzessbaukastenAnforderung(Long prozessbaukastenId, Long anforderungId) {
        ProzessbaukastenAnforderungTuple prozessbaukastenAnforderungTuple = ProzessbaukastenAnforderungTupleBuilder.withAnforderungId(anforderungId).withProzessbaukastenId(prozessbaukastenId).build();

        ProzessbaukastenAnforderungOrder prozessbaukastenAnforderungOrder = prozessbaukastenAnforderungOrders.get(prozessbaukastenAnforderungTuple);

        Long position = prozessbaukastenAnforderungOrder.getPosition();

        return position != null ? position.intValue() : 0;
    }

}
