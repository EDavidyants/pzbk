package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import de.interfaceag.bmw.pzbk.globalsearch.SearchResultDTO;

import java.util.List;

/**
 *
 * @author fn
 */
public interface AnforderungenProzessbaukastenZuordnenDialogFields {

    void removeAnforderung(AnforderungenProzessbaukastenZuordnenDTO anforderungDto);

    List<AnforderungenProzessbaukastenZuordnenDTO> getZugeordneteAnforderungen();

    void setZugeordneteAnforderungen(List<AnforderungenProzessbaukastenZuordnenDTO> zugeordneteAnforderungen);

    SearchResultDTO getAutocompleteSearchResult();

    void setAutocompleteSearchResult(SearchResultDTO autocompleteSearchResult);

}
