package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required;

/**
 *
 * @author sl
 */
public class ProzessbaukastenStatusGueltigRequiredAttributes extends ProzessbaukastenStatusBeauftragtRequiredAttributes {

    protected ProzessbaukastenStatusGueltigRequiredAttributes() {
    }

    @Override
    public boolean isAnhangGenehmigungTPKRequired() {
        return true;
    }

    @Override
    public boolean isAnhangGrafikKonzeptbaumRequired() {
        return true;
    }

    @Override
    public boolean isAnhangGrafikUmfangRequired() {
        return true;
    }

    @Override
    public boolean isStandardisierterFertigungsprozessRequired() {
        return true;
    }

    @Override
    public boolean isKonzeptRequired() {
        return true;
    }

}
