package de.interfaceag.bmw.pzbk.prozessbaukasten.validator;

import de.interfaceag.bmw.pzbk.entities.Anhang;

import javax.faces.application.FacesMessage;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class AnhangPresentValidator {

    private AnhangPresentValidator() {
    }

    protected static void validateAnhang(Anhang anhang, String errorMessage) throws ValidatorException {
        if (!isAnhangPresent(anhang)) {
            showErrorMessage(errorMessage);
        }
    }

    private static void showErrorMessage(String message) throws ValidatorException {
        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, message, ""));
    }

    private static boolean isAnhangPresent(Anhang anhang) {
        return anhang != null;
    }

}
