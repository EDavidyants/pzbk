package de.interfaceag.bmw.pzbk.prozessbaukasten.themenklammer;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenThemenklammer;
import de.interfaceag.bmw.pzbk.entities.Themenklammer;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.ProzessbaukastenAnforderungEdit;
import de.interfaceag.bmw.pzbk.shared.objectIds.ThemenklammerId;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Stateless
@Named
public class AnforderungProzessbaukastenThemenklammerService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungProzessbaukastenThemenklammerService.class.getName());

    @Inject
    private ThemenklammerService themenklammerService;
    @Inject
    private AnforderungProzessbaukastenThemenklammerHistoryService anforderungProzessbaukastenThemenklammerHistoryService;

    public void updateAnforderungThemenklammern(Prozessbaukasten prozessbaukasten, List<ProzessbaukastenAnforderungEdit> prozessbaukastenAnforderungDtos) {
        LOG.debug("Update Themenklammern for prozessbaukasten {}", prozessbaukasten);
        final List<Anforderung> anforderungen = prozessbaukasten.getAnforderungen();
        for (ProzessbaukastenAnforderungEdit prozessbaukastenAnforderungDto : prozessbaukastenAnforderungDtos) {
            updateProzessbaukastenAnforderungForDto(prozessbaukasten, anforderungen, prozessbaukastenAnforderungDto);
        }
    }

    private void updateProzessbaukastenAnforderungForDto(Prozessbaukasten prozessbaukasten, List<Anforderung> anforderungen, ProzessbaukastenAnforderungEdit prozessbaukastenAnforderungDto) {
        LOG.debug("Update Themenklammern for anforderung {}", prozessbaukastenAnforderungDto.getFachIdAndVersion());
        final Optional<Anforderung> anforderungForId = getAnforderungForIdFromProzessbaukasten(anforderungen, prozessbaukastenAnforderungDto);
        if (anforderungForId.isPresent()) {
            final Anforderung anforderung = anforderungForId.get();
            updateProzessbaukastenAnforderung(prozessbaukasten, prozessbaukastenAnforderungDto, anforderung);
        } else {
            LOG.error("Anforderung {} not found in Prozessbaukasten {}", prozessbaukastenAnforderungDto.getFachIdAndVersion(), prozessbaukasten);
        }
    }

    private void updateProzessbaukastenAnforderung(Prozessbaukasten prozessbaukasten, ProzessbaukastenAnforderungEdit prozessbaukastenAnforderungDto, Anforderung anforderung) {
        LOG.debug("Update Anforderung {}", anforderung);

        final List<ThemenklammerId> currentlySelectedThemenklammerIds = getCurrentlySelectedThemenklammerIds(prozessbaukastenAnforderungDto);
        final List<ProzessbaukastenThemenklammer> savedThemenklammern = anforderung.getProzessbaukastenThemenklammernForProzessbaukasten(prozessbaukasten);

        removeThemenklammernFromAnforderung(anforderung, currentlySelectedThemenklammerIds, savedThemenklammern);
        addThemenklammernToAnforderung(anforderung, currentlySelectedThemenklammerIds, savedThemenklammern, prozessbaukasten);
    }

    private void addThemenklammernToAnforderung(Anforderung anforderung, List<ThemenklammerId> currentlySelectedThemenklammerIds, List<ProzessbaukastenThemenklammer> savedThemenklammern, Prozessbaukasten prozessbaukasten) {
        final List<ThemenklammerId> savedThemenklammerIds = savedThemenklammern.stream().map(ProzessbaukastenThemenklammer::getThemenklammer).map(Themenklammer::getThemenklammerId).collect(Collectors.toList());
        currentlySelectedThemenklammerIds.removeAll(savedThemenklammerIds);

        LOG.debug("Add Themenklammern from anforderung {}", currentlySelectedThemenklammerIds);

        for (ThemenklammerId themenklammerId : currentlySelectedThemenklammerIds) {
            addThemenklammerToProzessbaukastenAnforderung(anforderung, prozessbaukasten, themenklammerId);
        }
    }

    private void addThemenklammerToProzessbaukastenAnforderung(Anforderung anforderung, Prozessbaukasten prozessbaukasten, ThemenklammerId themenklammerId) {
        final Optional<Themenklammer> themenklammerById = themenklammerService.find(themenklammerId);
        if (themenklammerById.isPresent()) {
            final Themenklammer themenklammer = themenklammerById.get();
            ProzessbaukastenThemenklammer newProzessbaukastenThemenklammer = new ProzessbaukastenThemenklammer(prozessbaukasten, themenklammer);
            anforderung.addProzessbaukastenThemenklammer(newProzessbaukastenThemenklammer);
            LOG.debug("Add ProzessbaukastenThemenklammer {} to anforderung {}", newProzessbaukastenThemenklammer, anforderung);
            anforderungProzessbaukastenThemenklammerHistoryService.writeHistoryEntryForAddedThemenklammer(anforderung, newProzessbaukastenThemenklammer);
        } else {
            LOG.error("Themenklammer for id {} not found", themenklammerById);
        }
    }

    private void removeThemenklammernFromAnforderung(Anforderung anforderung, List<ThemenklammerId> currentlySelectedThemenklammerIds, List<ProzessbaukastenThemenklammer> savedThemenklammern) {
        final Stream<ProzessbaukastenThemenklammer> themenklammernToRemove = savedThemenklammern.stream().filter(prozessbaukastenThemenklammer -> !currentlySelectedThemenklammerIds.contains(prozessbaukastenThemenklammer.getThemenklammer().getThemenklammerId()));
        LOG.debug("Remove Themenklammern from anforderung {}", themenklammernToRemove);
        themenklammernToRemove.forEach(themenklammer -> removeThemenklammerFromAnforderungProzessbaukasten(anforderung, themenklammer));
    }

    private void removeThemenklammerFromAnforderungProzessbaukasten(Anforderung anforderung, ProzessbaukastenThemenklammer prozessbaukastenThemenklammer) {
        anforderung.removeProzessbaukastenThemenklammer(prozessbaukastenThemenklammer);
        LOG.debug("Remove Themenklammer {} from anforderung {}", prozessbaukastenThemenklammer, anforderung);
        anforderungProzessbaukastenThemenklammerHistoryService.writeHistoryEntryForRemovedThemenklammer(anforderung, prozessbaukastenThemenklammer);
    }

    private static List<ThemenklammerId> getCurrentlySelectedThemenklammerIds(ProzessbaukastenAnforderungEdit prozessbaukastenAnforderungDto) {
        return prozessbaukastenAnforderungDto.getThemenklammern().stream().map(ThemenklammerDto::getThemenklammerId).collect(Collectors.toList());
    }

    private static Optional<Anforderung> getAnforderungForIdFromProzessbaukasten(List<Anforderung> anforderungen, ProzessbaukastenAnforderungEdit prozessbaukastenAnforderungDto) {
        final String anforderungId = prozessbaukastenAnforderungDto.getId();

        return anforderungen.stream()
                .filter(anforderung -> anforderung.getId().toString().equals(anforderungId))
                .findFirst();
    }

}
