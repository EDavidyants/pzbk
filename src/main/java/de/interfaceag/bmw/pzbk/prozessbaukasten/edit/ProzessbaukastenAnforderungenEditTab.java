package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;

import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface ProzessbaukastenAnforderungenEditTab {

    List<ProzessbaukastenAnforderungEdit> getAnforderungen();

    void setAnforderungen(List<ProzessbaukastenAnforderungEdit> anforderungen);

    List<ProzessbaukastenAnforderungEdit> getFilteredAnforderungen();

    void setFilteredAnforderungen(List<ProzessbaukastenAnforderungEdit> filteredAnforderungen);

    List<Konzept> getAllKonzepte();

    List<ThemenklammerDto> getAllThemenklammern();

    Converter getKonzepteConverter();

    Converter getThemenklammerConverter();

}
