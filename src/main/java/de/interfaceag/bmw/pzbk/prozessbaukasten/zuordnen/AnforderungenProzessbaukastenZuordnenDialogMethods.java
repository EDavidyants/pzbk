package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

/**
 *
 * @author fn
 */
public interface AnforderungenProzessbaukastenZuordnenDialogMethods {

    String anforderungenZuordnen();

    void resetAnforderungenProzessbaukastenZuordnenDialog();
}
