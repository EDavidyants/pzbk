package de.interfaceag.bmw.pzbk.prozessbaukasten.themenklammer;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenHistory;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenThemenklammer;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenHistoryBuilder;
import de.interfaceag.bmw.pzbk.prozessbaukasten.history.ProzessbaukastenHistoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class AnforderungProzessbaukastenThemenklammerHistoryService {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungProzessbaukastenThemenklammerHistoryService.class);

    @Inject
    private Mitarbeiter currentUser;
    @Inject
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;

    public ProzessbaukastenHistory writeHistoryEntryForAddedThemenklammer(Anforderung anforderung, ProzessbaukastenThemenklammer prozessbaukastenThemenklammer) {

        LOG.debug("write history for added themenklammer {} to anforderung {}", prozessbaukastenThemenklammer, anforderung);

        ProzessbaukastenHistoryBuilder builder = new ProzessbaukastenHistoryBuilder();
        builder.setProzessbaukasten(prozessbaukastenThemenklammer.getProzessbaukasten());
        builder.setBenutzer(currentUser.toString());
        builder.setChangedField(prozessbaukastenThemenklammer.getThemenklammer().toString());
        builder.setVon(anforderung.toString());
        builder.setAuf("zugeordnet");

        final ProzessbaukastenHistory newEntry = builder.build();

        prozessbaukastenHistoryService.save(newEntry);

        return newEntry;
    }


    public ProzessbaukastenHistory writeHistoryEntryForRemovedThemenklammer(Anforderung anforderung, ProzessbaukastenThemenklammer prozessbaukastenThemenklammer) {

        LOG.debug("write history for removed themenklammer {} from anforderung {}", prozessbaukastenThemenklammer, anforderung);

        ProzessbaukastenHistoryBuilder builder = new ProzessbaukastenHistoryBuilder();
        builder.setProzessbaukasten(prozessbaukastenThemenklammer.getProzessbaukasten());
        builder.setBenutzer(currentUser.toString());
        builder.setChangedField(prozessbaukastenThemenklammer.getThemenklammer().toString());
        builder.setVon(anforderung.toString());
        builder.setAuf("entfernt");

        final ProzessbaukastenHistory newEntry = builder.build();

        prozessbaukastenHistoryService.save(newEntry);

        return newEntry;
    }
}
