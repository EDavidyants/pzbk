package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required.ProzessbaukastenRequiredAttributes;

import java.io.Serializable;

/**
 *
 * @author sl
 */
public interface AddKonzeptDialog extends Serializable {

    Konzept getKonzept();

    void setKonzept(Konzept konzept);

    KonzeptAnhangUploader getAnhangUploader();

    ProzessbaukastenRequiredAttributes getRequiredAttributes();

    void addKonzept();

}
