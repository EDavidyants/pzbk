package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenViewData;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Stateless
@Named
public class ProzessbaukastenStatusChangeValidatorService implements Serializable {

    @Inject
    private LocalizationService localizationService;
    @Inject
    private ProzessbaukastenReadService prozessbaukastenReadService;
    @Inject
    private ProzessbaukastenDialogAndGrowlService prozessbaukastenDialogAndGrowlService;

    private String checkRequiredAttributesAndProceedWithStatusChange(ProzessbaukastenStatus newStatus, ProzessbaukastenViewData viewData, ActiveIndex activeIndex) {
        if (hasRequiredAttributes(newStatus)) {
            return navigateToEditViewForStatusChange(newStatus, viewData, activeIndex);
        } else {
            return openStatusChangeDialog(newStatus, viewData);
        }
    }

    private boolean hasRequiredAttributes(ProzessbaukastenStatus status) {
        return !(status.equals(ProzessbaukastenStatus.ABGEBROCHEN) || status.equals(ProzessbaukastenStatus.GELOESCHT) || status.equals(ProzessbaukastenStatus.STILLGELEGT));
    }

    private String navigateToEditViewForStatusChange(ProzessbaukastenStatus newStatus, ProzessbaukastenViewData viewData, ActiveIndex activeIndex) {
        return PageUtils.getUrlForProzessbaukastenEditWithStatusChange(viewData.getFachId(), viewData.getVersion(), activeIndex, newStatus);
    }

    private String openStatusChangeDialog(ProzessbaukastenStatus newStatus, ProzessbaukastenViewData viewData) {
        viewData.setSelectedNextStatus(newStatus.getId());
        return prozessbaukastenDialogAndGrowlService.openStatusChangeDialog();
    }

    private boolean isAllAnforderungenFreigegeben(Prozessbaukasten prozessbaukastenToFind) {

        List<Anforderung> anforderungen = prozessbaukastenToFind.getAnforderungen();

        if (anforderungen == null || anforderungen.isEmpty()) {
            return true;
        }
        return anforderungen.stream()
                .allMatch(anforderung -> anforderung.getStatus() == Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
    }

    private boolean canProzessbaukastenBeGenehmigt(Prozessbaukasten prozessbaukasten) {
        Optional<Prozessbaukasten> previousVersionOptional = getProzessbaukastenPreviousVersion(prozessbaukasten);
        if (previousVersionOptional.isPresent()) {
            Prozessbaukasten previousVersion = previousVersionOptional.get();
            return isPreviousVersionStillgelegtOrGeloescht(previousVersion);
        }

        return true;
    }

    private boolean isPreviousVersionStillgelegtOrGeloescht(Prozessbaukasten previousVersion) {
        return previousVersion.getStatus() == ProzessbaukastenStatus.STILLGELEGT || previousVersion.getStatus() == ProzessbaukastenStatus.GELOESCHT;
    }


    private Optional<Prozessbaukasten> getProzessbaukastenPreviousVersion(Prozessbaukasten prozessbaukasten) {
        String fachId = prozessbaukasten.getFachId();
        int previousVersionIndex = prozessbaukasten.getVersion() - 1;
        return prozessbaukastenReadService.getByFachIdAndVersion(fachId, previousVersionIndex);
    }


    public String validateStatusChange(ProzessbaukastenStatus newStatus, ProzessbaukastenViewData viewData, ActiveIndex activeIndex, Prozessbaukasten prozessbaukasten) {
        if (newStatus != ProzessbaukastenStatus.GUELTIG) {
            return checkRequiredAttributesAndProceedWithStatusChange(newStatus, viewData, activeIndex);
        } else {

            boolean canBeGenehmigt = canProzessbaukastenBeGenehmigt(prozessbaukasten);

            if (!canBeGenehmigt) {
                String growlMessage = localizationService.getValue("prozessbaukasten_view_statusWechselToGenehmigtNichtMoeglichMessage");
                return prozessbaukastenDialogAndGrowlService.openStatusChangeNotAllowedGrowl(growlMessage);
            }

            boolean allAnforderungenFreigegeben = isAllAnforderungenFreigegeben(prozessbaukasten);

            if (!allAnforderungenFreigegeben) {
                String growlMessage = localizationService.getValue("prozessbaukasten_view_nichtAlleAnforderungenFreigegeben");
                return prozessbaukastenDialogAndGrowlService.openStatusChangeNotAllowedGrowl(growlMessage);
            }
            return checkRequiredAttributesAndProceedWithStatusChange(newStatus, viewData, activeIndex);
        }
    }


}
