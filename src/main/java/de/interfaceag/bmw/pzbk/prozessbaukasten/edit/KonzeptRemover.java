package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Konzept;

import java.util.List;

/**
 *
 * @author sl
 */
public class KonzeptRemover {

    private final List<Konzept> konzepte;
    private final Konzept konzept;

    public KonzeptRemover(List<Konzept> konzepte, Konzept konzept) {
        this.konzepte = konzepte;
        this.konzept = konzept;
    }

    public void remove() {
        konzepte.remove(konzept);
    }

}
