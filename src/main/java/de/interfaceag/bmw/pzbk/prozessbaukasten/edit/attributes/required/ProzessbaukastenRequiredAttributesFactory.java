package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required;

import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;

/**
 *
 * @author sl
 */
public final class ProzessbaukastenRequiredAttributesFactory {

    private ProzessbaukastenRequiredAttributesFactory() {
    }

    public static ProzessbaukastenRequiredAttributes getRequiredAttributesForProzessbaukasten(ProzessbaukastenStatus newProzessbaukastenStatus) {
        switch (newProzessbaukastenStatus) {
            case ANGELEGT:
                return new ProzessbaukastenStatusAngelegtRequiredAttributes();
            case BEAUFTRAGT:
                return new ProzessbaukastenStatusBeauftragtRequiredAttributes();
            case GUELTIG:
                return new ProzessbaukastenStatusGueltigRequiredAttributes();
            case GELOESCHT:
            case STILLGELEGT:
            case ABGEBROCHEN:
            default:
                return new ProzessbaukastenNoRequiredAttributes();
        }
    }
}
