package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenAnforderungKonzept;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.ProzessbaukastenAnforderungEdit;
import de.interfaceag.bmw.pzbk.prozessbaukasten.history.ProzessbaukastenHistoryService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class ProzessbaukastenAnforderungKonzeptService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ProzessbaukastenAnforderungKonzeptService.class.getName());

    @Inject
    private ProzessbaukastenAnforderungKonzeptDao anforderungKonzeptDao;

    @Inject
    private AnforderungService anforderungService;

    @Inject
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;

    @Inject
    private Session session;

    private ProzessbaukastenAnforderungKonzept save(ProzessbaukastenAnforderungKonzept anforderungKonzept) {
        LOG.info("Save {}", anforderungKonzept);
        return anforderungKonzeptDao.save(anforderungKonzept);
    }

    private void remove(ProzessbaukastenAnforderungKonzept anforderungKonzept) {
        LOG.info("Remove {}", anforderungKonzept);
        anforderungKonzeptDao.remove(anforderungKonzept);
    }

    public List<Anforderung> getAnforderungenListForProzessbaukasten(List<ProzessbaukastenAnforderungEdit> anforderungDtos) {
        List<Anforderung> anforderungen = new ArrayList<>();

        Iterator<ProzessbaukastenAnforderungEdit> iterator = anforderungDtos.iterator();
        while (iterator.hasNext()) {
            ProzessbaukastenAnforderungEdit anforderungDto = iterator.next();
            String id = anforderungDto.getId();
            Anforderung anforderung = anforderungService.getAnforderungById(Long.parseLong(id));

            if (anforderung != null) {
                anforderungen.add(anforderung);
            }
        }

        return anforderungen;
    }

    public void updateAnforderungKonzept(Prozessbaukasten prozessbaukasten, List<ProzessbaukastenAnforderungEdit> anforderungDtos) {
        List<ProzessbaukastenAnforderungKonzept> currentAnforderungKonzeptList = getByProzessbaukastenId(prozessbaukasten.getId());
        updateAnforderungKonzept(prozessbaukasten, anforderungDtos, currentAnforderungKonzeptList);
    }

    public void updateAnforderungKonzept(Prozessbaukasten prozessbaukasten, List<ProzessbaukastenAnforderungEdit> anforderungDtos, List<ProzessbaukastenAnforderungKonzept> currentAnforderungKonzeptList) {
        List<Anforderung> prozessbaukastenAnforderungen = prozessbaukasten.getAnforderungen();
        List<ProzessbaukastenAnforderungKonzept> persistedAnforderungKonzepte = getPersistedAnforderungKonzepte(prozessbaukasten, currentAnforderungKonzeptList);

        Iterator<ProzessbaukastenAnforderungEdit> iterator = anforderungDtos.iterator();
        while (iterator.hasNext()) {
            ProzessbaukastenAnforderungEdit anforderungDto = iterator.next();
            String id = anforderungDto.getId();
            Optional<Anforderung> anforderungById = getAnforderungById(id, prozessbaukastenAnforderungen);

            if (anforderungById.isPresent()) {
                Anforderung anforderung = anforderungById.get();
                addNewKonzepteToAnforderung(persistedAnforderungKonzepte, anforderungDto, anforderung, prozessbaukasten);
                removeKonzepteToAnforderung(persistedAnforderungKonzepte, anforderungDto);
            }
        }
    }

    private List<ProzessbaukastenAnforderungKonzept> getPersistedAnforderungKonzepte(Prozessbaukasten prozessbaukasten, List<ProzessbaukastenAnforderungKonzept> currentAnforderungKonzeptList) {
        List<ProzessbaukastenAnforderungKonzept> persistedAnforderungKonzepte = new ArrayList<>();

        currentAnforderungKonzeptList.forEach(anforderungKonzept -> {
            if (anforderungKonzept.getId() == null) {
                ProzessbaukastenAnforderungKonzept persistedKonzept = this.createAnforderungKonzept(anforderungKonzept.getAnforderung(), prozessbaukasten, anforderungKonzept.getKonzept());
                persistedAnforderungKonzepte.add(persistedKonzept);
            } else {
                persistedAnforderungKonzepte.add(anforderungKonzept);
            }
        });

        return persistedAnforderungKonzepte;
    }

    private void removeKonzepteToAnforderung(List<ProzessbaukastenAnforderungKonzept> existingAnforderungKonzept,
                                             ProzessbaukastenAnforderungEdit newAnforderungKonzept) {
        Set<ProzessbaukastenAnforderungKonzept> removeKonzeptList = getRemoveKonzeptList(existingAnforderungKonzept, newAnforderungKonzept);
        Iterator<ProzessbaukastenAnforderungKonzept> removeIterator = removeKonzeptList.iterator();
        while (removeIterator.hasNext()) {
            ProzessbaukastenAnforderungKonzept anforderungKonzept = removeIterator.next();
            prozessbaukastenHistoryService.writeHistoryForRemovedAnforderungKonzept(anforderungKonzept, session.getUser());
            remove(anforderungKonzept);
        }
    }

    private void addNewKonzepteToAnforderung(List<ProzessbaukastenAnforderungKonzept> existingAnforderungKonzept,
                                             ProzessbaukastenAnforderungEdit newAnforderungKonzept, Anforderung anforderung, Prozessbaukasten prozessbaukasten) {
        Set<Konzept> addKonzeptList = getAddKonzeptList(existingAnforderungKonzept, newAnforderungKonzept);
        Iterator<Konzept> addIterator = addKonzeptList.iterator();
        while (addIterator.hasNext()) {
            Konzept konzept = addIterator.next();
            createAnforderungKonzept(anforderung, prozessbaukasten, konzept);
        }
    }

    private Set<Konzept> getAddKonzeptList(List<ProzessbaukastenAnforderungKonzept> existingAnforderungKonzept, ProzessbaukastenAnforderungEdit newAnforderungKonzept) {
        if (newAnforderungKonzept.getAnforderungKonzept() == null) {
            return Collections.emptySet();
        }
        Long anforderungId = getAnforderungIdForAnforderungEdit(newAnforderungKonzept);

        Set<Konzept> existingKonzepteForAnforderung = existingAnforderungKonzept
                .stream()
                .filter(anforderungKonzept -> anforderungKonzept.getAnforderung().getId().equals(anforderungId))
                .map(ProzessbaukastenAnforderungKonzept::getKonzept)
                .collect(Collectors.toSet());
        return newAnforderungKonzept.getAnforderungKonzept().stream()
                .filter(konzept -> !existingKonzepteForAnforderung.contains(konzept))
                .collect(Collectors.toSet());
    }

    private static Long getAnforderungIdForAnforderungEdit(ProzessbaukastenAnforderungEdit anforderungEdit) {
        String anforderungIdString = anforderungEdit.getId();
        if (RegexUtils.matchesId(anforderungIdString)) {
            return Long.parseLong(anforderungIdString);
        } else {
            throw new IllegalArgumentException(anforderungIdString + " is no valid anforderung id!");
        }
    }

    private Set<ProzessbaukastenAnforderungKonzept> getRemoveKonzeptList(List<ProzessbaukastenAnforderungKonzept> existingAnforderungKonzept, ProzessbaukastenAnforderungEdit newAnforderungKonzept) {
        if (newAnforderungKonzept.getAnforderungKonzept() == null) {
            return new HashSet<>(existingAnforderungKonzept);
        }
        Long anforderungId = getAnforderungIdForAnforderungEdit(newAnforderungKonzept);

        List<Konzept> newKonzepteForAnforderung = newAnforderungKonzept.getAnforderungKonzept();
        return existingAnforderungKonzept.stream()
                .filter(anforderungKonzept -> anforderungKonzept.getAnforderung().getId().equals(anforderungId))
                .filter(anforderungKonzept -> !newKonzepteForAnforderung.contains(anforderungKonzept.getKonzept()))
                .collect(Collectors.toSet());
    }

    public ProzessbaukastenAnforderungKonzept createAnforderungKonzept(Anforderung anforderung, Prozessbaukasten prozessbaukasten, Konzept konzept, Mitarbeiter mitarbeiter) {
        LOG.info("Create ProzessbaukastenAnforderungKonzept with Anforderung {}, Prozessbaukasten {} and Konzept {}", anforderung, prozessbaukasten, konzept);
        ProzessbaukastenAnforderungKonzept newAnforderungKonzept = new ProzessbaukastenAnforderungKonzept(anforderung, konzept, prozessbaukasten);
        prozessbaukastenHistoryService.writeHistoryForAddedAnforderungKonzept(newAnforderungKonzept, mitarbeiter);
        return save(newAnforderungKonzept);
    }

    public ProzessbaukastenAnforderungKonzept createAnforderungKonzept(Anforderung anforderung, Prozessbaukasten prozessbaukasten, Konzept konzept) {
        final Mitarbeiter user = session.getUser();
        return createAnforderungKonzept(anforderung, prozessbaukasten, konzept, user);
    }

    private static Optional<Anforderung> getAnforderungById(String idString, List<Anforderung> anforderungen) {
        if (RegexUtils.matchesId(idString)) {
            long id = Long.parseLong(idString);
            return anforderungen.stream().filter(anforderung -> anforderung.getId() == id).findAny();
        } else {
            return Optional.empty();
        }
    }

    public List<ProzessbaukastenAnforderungKonzept> getByProzessbaukastenId(Long prozessbaukastenId) {
        if (prozessbaukastenId != null) {
            return anforderungKonzeptDao.getByProzessbaukastenId(prozessbaukastenId);
        } else {
            return Collections.emptyList();
        }
    }

    public List<ProzessbaukastenAnforderungKonzept> getByAnforderungId(long anforderungId) {
        return anforderungKonzeptDao.getByAnforderungId(anforderungId);
    }

    public List<ProzessbaukastenAnforderungKonzept> generateAnforderungKonzepteForNewVersion(Prozessbaukasten prozessbaukastenNew, Prozessbaukasten prozessbaukastenOrigin) {
        List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte = new ArrayList<>();
        List<ProzessbaukastenAnforderungKonzept> anforderungKonzepteFromOrigin = getByProzessbaukastenId(prozessbaukastenOrigin.getId());

        List<Konzept> konzepteNew = prozessbaukastenNew.getKonzepte();

        anforderungKonzepteFromOrigin.stream()
                .map(anforderungKonzept -> generateAnforderungKonzeptAufBasisVorhandenen(anforderungKonzept, konzepteNew, prozessbaukastenNew))
                .forEachOrdered(anforderungKonzepte::add);

        return anforderungKonzepte;
    }

    private ProzessbaukastenAnforderungKonzept generateAnforderungKonzeptAufBasisVorhandenen(ProzessbaukastenAnforderungKonzept anforderungKonzept, List<Konzept> konzepteNew, Prozessbaukasten prozessbaukastenNew) {
        Konzept konzeptOrigin = anforderungKonzept.getKonzept();

        Konzept konzeptNew = konzepteNew.stream()
                .filter(kn -> kn.getBezeichnung().equals(konzeptOrigin.getBezeichnung())
                        && kn.getBeschreibung().equals(konzeptOrigin.getBeschreibung()))
                .findAny().orElse(null);

        if (konzeptNew == null) {
            LOG.error("No Copy for the Konzept {} found in Prozessbaukasten {}", konzeptOrigin, prozessbaukastenNew);
        }

        Anforderung anforderung = anforderungKonzept.getAnforderung();
        return new ProzessbaukastenAnforderungKonzept(anforderung, konzeptNew, prozessbaukastenNew);
    }

}
