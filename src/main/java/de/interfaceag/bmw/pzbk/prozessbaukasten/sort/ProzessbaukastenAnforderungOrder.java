package de.interfaceag.bmw.pzbk.prozessbaukasten.sort;

/**
 *
 * @author sl
 */
public interface ProzessbaukastenAnforderungOrder {

    Long getProzessbaukastenId();

    Long getAnforderungId();

    Long getPosition();

}
