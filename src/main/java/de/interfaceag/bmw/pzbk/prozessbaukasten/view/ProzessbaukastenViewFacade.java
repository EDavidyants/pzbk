package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamMitgliedBerechtigung;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenAnforderungKonzept;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ActiveIndex;
import de.interfaceag.bmw.pzbk.prozessbaukasten.AnforderungenTabDTO;
import de.interfaceag.bmw.pzbk.prozessbaukasten.KonzeptDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenAnforderung;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenAnforderungKonzeptService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenExcelExportDownloadService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenStatusChangeService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenStatusChangeValidatorService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenStatusUebergang;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenTab;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.AnforderungenProzessbaukastenZuordnenDTO;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.AnforderungenProzessbaukastenZuordnenViewData;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.AnhangDownloadService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;
import org.primefaces.event.ReorderEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenStatusChangeService.isStatusChangeKommentarValid;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class ProzessbaukastenViewFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ProzessbaukastenViewData.class);

    @Inject
    private ProzessbaukastenService prozessbaukastenService;
    @Inject
    private ProzessbaukastenReadService prozessbaukastenReadService;
    @Inject
    private LocalizationService localizationService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private ProzessbaukastenStatusChangeService prozessbaukastenStatusChangeService;
    @Inject
    private AnhangDownloadService anhangDownloadService;
    @Inject
    private ProzessbaukastenZuordnenService prozessbaukastenZuordnenService;
    @Inject
    private AnforderungService anforderungService;
    @Inject
    private ProzessbaukastenAnforderungKonzeptService prozessbaukastenAnforderungKonzeptService;
    @Inject
    private ProzessbaukastenStatusChangeValidatorService prozessbaukastenStatusChangeValidatorService;
    @Inject
    private ProzessbaukastenExcelExportDownloadService prozessbaukastenExcelExportDownloadService;


    @Inject
    protected Session session;

    public ProzessbaukastenViewData getViewData(UrlParameter urlParameter) {

        String fachId = urlParameter.getValue("fachId").orElse(null);
        String version = urlParameter.getValue("version").orElse(null);

        return getProzessbaukastenViewDataByFachIdVersion(fachId, version);
    }

    private ProzessbaukastenViewData getProzessbaukastenViewDataByFachIdVersion(String fachId, String version) {
        Optional<Prozessbaukasten> prozessbaukastenToFind = prozessbaukastenReadService.getByFachIdAndVersion(fachId, Integer.parseInt(version));
        if (prozessbaukastenToFind.isPresent()) {
            Prozessbaukasten prozessbaukasten = prozessbaukastenToFind.get();
            return generateProzessbaukastenViewDataForProzessbaukasten(prozessbaukasten);
        } else {
            return generateViewDataForLeerProzessbaukasten();
        }
    }

    private static List<ProzessbaukastenStatus> getNaechstenProzessbaukastenStatus(Prozessbaukasten prozessbaukasten) {
        return ProzessbaukastenStatusUebergang.getNextStatus(prozessbaukasten.getStatus());
    }

    private ProzessbaukastenViewData generateViewDataForLeerProzessbaukasten() {
        ProzessbaukastenStammdatenViewData stammdaten = ProzessbaukastenStammdatenViewData.forLeerProzessbaukasten();
        ProzessbaukastenAnforderungenViewData anforderungenTabViewData = ProzessbaukastenAnforderungenViewData.forLeerProzessbaukasten();
        ProzessbaukastenAnhangTabViewData anhangTabViewData = ProzessbaukastenAnhangTabViewData.forLeerProzessbaukasten();
        AnforderungenProzessbaukastenZuordnenViewData anforderungenZuordnenViewData = AnforderungenProzessbaukastenZuordnenViewData.forProzessbaukasten();

        return ProzessbaukastenViewData.getBuilder()
                .withViewPermission(ProzessbaukastenViewPermission.denied())
                .withStatusLabel("")
                .withBezeichnung("")
                .withStammdaten(stammdaten)
                .withAnforderungenTabViewData(anforderungenTabViewData)
                .withStatus(ProzessbaukastenStatus.ANGELEGT)
                .withNaechsteStatus(new ArrayList<>())
                .withAnhangTabViewData(anhangTabViewData)
                .withAnforderungProzessbaukastenZuordnenViewData(anforderungenZuordnenViewData)
                .build();
    }

    private ProzessbaukastenViewData generateProzessbaukastenViewDataForProzessbaukasten(Prozessbaukasten prozessbaukasten) {

        ProzessbaukastenStammdatenViewData stammdaten = getProzessbaukastenStammdatenViewData(prozessbaukasten);
        ProzessbaukastenAnforderungenViewData anforderungenTabViewData = getProzessbaukastenAnforderungenViewData(prozessbaukasten);
        ProzessbaukastenAnhangTabViewData anhangTabViewData = getProzessbaukastenAnhangTabViewData(prozessbaukasten);
        AnforderungenProzessbaukastenZuordnenViewData anforderungenZuordnenViewData = AnforderungenProzessbaukastenZuordnenViewData.forProzessbaukasten();
        ProzessbaukastenViewPermission viewPermission = getViewPermission(prozessbaukasten);

        Optional<Prozessbaukasten> prozessbaukastenOriginOptional = prozessbaukastenService.createRealCopy(prozessbaukasten);

        return ProzessbaukastenViewData.getBuilder()
                .withProzessbaukastenOrigin(prozessbaukastenOriginOptional)
                .withId(String.valueOf(prozessbaukasten.getId()))
                .withFachId(prozessbaukasten.getFachId())
                .withVersion(String.valueOf(prozessbaukasten.getVersion()))
                .withStatusLabel(getStatusLabel(prozessbaukasten))
                .withBezeichnung(prozessbaukasten.getBezeichnung())
                .withStammdaten(stammdaten)
                .withViewPermission(viewPermission)
                .withAnforderungenTabViewData(anforderungenTabViewData)
                .withStatus(prozessbaukasten.getStatus())
                .withNaechsteStatus(getNaechstenProzessbaukastenStatus(prozessbaukasten))
                .withAnhangTabViewData(anhangTabViewData)
                .withAnforderungProzessbaukastenZuordnenViewData(anforderungenZuordnenViewData)
                .build();
    }


    private ProzessbaukastenViewPermission getViewPermission(Prozessbaukasten prozessbaukasten) {
        Set<Rolle> userRoles = session.getUserPermissions().getRoles();
        List<Long> tteamIds = session.getUserPermissions().getTteamAsTteamleiterSchreibend()
                .stream().map(BerechtigungDto::getId).collect(Collectors.toList());
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = session.getUserPermissions().getTteamMitgliedBerechtigung();
        return ProzessbaukastenViewPermission
                .forProzessbaukasten(prozessbaukasten)
                .forTteamIdsAsTteamleiter(tteamIds)
                .forUserRoles(userRoles)
                .forTteamMitgliedBerechtigung(tteamMitgliedBerechtigung)
                .forTteamIdsAsTteamVertreter(session.getUserPermissions().getTteamIdsForTteamVertreter())
                .build();
    }

    private static ProzessbaukastenAnhangTabViewData getProzessbaukastenAnhangTabViewData(Prozessbaukasten prozessbaukasten) {
        return ProzessbaukastenAnhangTabViewData.newBuilder()
                .withStartbrief(prozessbaukasten.getStartbrief())
                .withGrafikUmfang(prozessbaukasten.getGrafikUmfang())
                .withKonzeptbaum(prozessbaukasten.getKonzeptbaum())
                .withBeauftragungTPK(prozessbaukasten.getBeauftragungTPK())
                .withGenehmigungTPK(prozessbaukasten.getGenehmigungTPK())
                .withWeitereAnhaenge(prozessbaukasten.getWeitereAnhaenge())
                .build();
    }

    private ProzessbaukastenAnforderungenViewData getProzessbaukastenAnforderungenViewData(Prozessbaukasten prozessbaukasten) {
        List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte = prozessbaukastenAnforderungKonzeptService.getByProzessbaukastenId(prozessbaukasten.getId());
        List<ProzessbaukastenAnforderung> anforderungen = transformAnforderungenToAnforderungenDTO(prozessbaukasten.getAnforderungen(), anforderungKonzepte, prozessbaukasten);

        return ProzessbaukastenAnforderungenViewData.getBuilder()
                .withAnforderungen(anforderungen)
                .build();
    }

    private ProzessbaukastenStammdatenViewData getProzessbaukastenStammdatenViewData(Prozessbaukasten prozessbaukasten) {
        return ProzessbaukastenStammdatenViewData.getBuilder()
                .withBeschreibung(prozessbaukasten.getBeschreibung())
                .withTteam(getTteamNameForProzessbaukasten(prozessbaukasten))
                .withTteamleiter(getTteamLeiter(prozessbaukasten))
                .withLeadTechnologie(getLeadTechnologieNameForProzessbaukasten(prozessbaukasten))
                .withStandardisierterFertigungsprozess(prozessbaukasten.getStandardisierterFertigungsprozess())
                .withErstanlaeufer(getErstanlaeuferLabel(prozessbaukasten))
                .withKonzepte(getKonzepteForProzessbaukasten(prozessbaukasten))
                .build();
    }

    private String getErstanlaeuferLabel(Prozessbaukasten prozessbaukasten) {
        String label = "";
        if (prozessbaukasten != null && prozessbaukasten.getErstanlaeufer() != null) {
            Derivat erstanlaeufer = prozessbaukasten.getErstanlaeufer();
            label = erstanlaeufer.getLabel();
        }
        return label;
    }

    private static List<KonzeptDto> getKonzepteForProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        List<KonzeptDto> konzepte = KonzeptDto.forKonzepte(prozessbaukasten.getKonzepte());
        Collections.sort(konzepte);
        return konzepte;
    }

    private static String getTteamNameForProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        Tteam tteam = prozessbaukasten.getTteam();
        return tteam != null ? tteam.getTeamName() : "";
    }

    private static String getLeadTechnologieNameForProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        Mitarbeiter leadTechnologie = prozessbaukasten.getLeadTechnologie();
        return leadTechnologie != null ? leadTechnologie.getFullName() : "";
    }

    private static List<ProzessbaukastenAnforderung> transformAnforderungenToAnforderungenDTO(List<Anforderung> anforderungen, List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte, Prozessbaukasten prozessbaukasten) {
        return AnforderungenTabDTO.forAnforderungen(anforderungen, anforderungKonzepte, prozessbaukasten);
    }

    private static String getStatusLabel(Prozessbaukasten prozessbaukasten) {
        return LocalizationService.getValue(Locale.GERMAN, prozessbaukasten.getStatus().getLocalizationKey());
    }

    private String getTteamLeiter(Prozessbaukasten prozessbaukasten) {
        if (prozessbaukasten != null && prozessbaukasten.getTteam() != null) {
            Mitarbeiter tteamleiter = berechtigungService.getTteamleiterOfTteam(prozessbaukasten.getTteam()).orElse(null);
            if (tteamleiter != null) {
                return tteamleiter.getFullName();
            }
        }
        return "nicht vorhanden";
    }

    public String changeStatus(Integer newStatusId, ProzessbaukastenViewData viewData) {

        ProzessbaukastenStatus newStatus = ProzessbaukastenStatus.getById(newStatusId);

        if (!isChangeStatusInputValid(newStatus)) {
            LOG.error("Gewaehlter Prozessbaukasten Status nicht zulaessig");
            return "";
        }

        Optional<Prozessbaukasten> prozessbaukastenToFind = getProzessbaukastenByFachIdAndVersion(viewData);
        if (prozessbaukastenToFind.isPresent()) {
            Prozessbaukasten prozessbaukasten = prozessbaukastenToFind.get();
            String kommentar = viewData.getStatusChangeKommentar();

            if (isStatusChangeKommentarValid(kommentar)) {
                prozessbaukastenStatusChangeService.processStatusChange(newStatus, prozessbaukasten, kommentar);
                return redirectToProzessbaukastenView(viewData.getFachId(), Integer.parseInt(viewData.getVersion()));
            } else {
                showKommentarRequiredDialog(newStatus);
                return "";
            }
        }

        return "";

    }

    private static String redirectToProzessbaukastenView(String fachId, Integer version) {
        return PageUtils.getUrlForPageWithFachIdAndVersion(Page.PROZESSBAUKASTEN_VIEW, fachId, version);
    }

    private static boolean isChangeStatusInputValid(ProzessbaukastenStatus newStatus) {
        return newStatus != null;
    }

    private String getStatusChangeGrowlMessageTitle(ProzessbaukastenStatus newStatus) {
        StringBuilder sb = new StringBuilder();
        sb.append(localizationService.getValue("prozessbaukasten_view_beimWechselZumStatus")).append(" ");
        sb.append(localizationService.getValue(newStatus.getLocalizationKey())).append(" ");
        sb.append(localizationService.getValue("prozessbaukasten_view_istEinKommentarNotwendig")).append(".");
        String message = sb.toString();
        return message;
    }

    private void showKommentarRequiredDialog(ProzessbaukastenStatus newStatus) {
        FacesContext context = FacesContext.getCurrentInstance();
        String messageTitle = localizationService.getValue("prozessbaukasten_view_statusWechselKommentarFehlt") + "!";
        String message = getStatusChangeGrowlMessageTitle(newStatus);
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, messageTitle, message));
    }

    public StreamedContent downloadAnhang(Anhang anhang) {
        return anhangDownloadService.downloadAnhang(anhang);
    }

    public String anforderungenZuordnen(List<AnforderungenProzessbaukastenZuordnenDTO> zugeordneteAnforderungen, Prozessbaukasten prozessbaukastenOrigin) {
        return prozessbaukastenZuordnenService.anforderungenProzessbaukastenZuordnen(zugeordneteAnforderungen, prozessbaukastenOrigin);
    }

    public MenuModel getVersionsMenuItems(ProzessbaukastenViewData viewData) {
        DefaultMenuModel versionsMenuItems = new DefaultMenuModel();
        List<ProzessbaukastenVersionMenuDto> versionen = prozessbaukastenService.fetchVersionenForFachId(viewData.getFachId());

        versionen.stream().map((version) -> {
            return prozessbaukastenService.generateVersionMenuItem(version, viewData.getVersion());
        }).forEachOrdered(versionsMenuItems::addElement);

        if (isAuthorizedToCreateNewVersion(viewData)) {
            DefaultMenuItem addNewVersionItem = generateAddNewVersionMenuItem();
            versionsMenuItems.addElement(addNewVersionItem);
        }

        return versionsMenuItems;
    }

    private static boolean isAuthorizedToCreateNewVersion(ProzessbaukastenViewData viewData) {
        return viewData.getViewPermission().getNewVersionButton();
    }

    public DefaultMenuItem generateAddNewVersionMenuItem() {
        DefaultMenuItem item = new DefaultMenuItem();
        item.setOnclick("PF('versionPlusDialog').show()");
        item.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        item.setUrl("#");
        item.setIcon("ui-icon-plus");
        item.setStyleClass("add");
        item.setValue(localizationService.getValue("prozessbaukasten_view_neueVersion"));
        return item;
    }

    public String refreshViewDataWithAnforderungen(ProzessbaukastenViewData viewData) {
        String page = "";
        if (RegexUtils.matchesVersion(viewData.getVersion())) {
            page = PageUtils.getUrlForProzessbaukastenView(viewData.getFachId(),
                    Integer.parseInt(viewData.getVersion()), ActiveIndex.forTab(ProzessbaukastenTab.ANFORDERUNGEN));
        }

        return page;
    }

    private void doProceedWithZuordnungAufheben(Anforderung anforderung, ProzessbaukastenViewData viewData) {
        Optional<Prozessbaukasten> prozessbaukastenToFind = getProzessbaukastenByFachIdAndVersion(viewData);
        if (prozessbaukastenToFind.isPresent()) {
            Prozessbaukasten prozessbaukasten = prozessbaukastenToFind.get();
            String kommentar = viewData.getZuordnungAufhebenKommentar();
            prozessbaukastenZuordnenService.zuordnungAufheben(anforderung, prozessbaukasten, kommentar);
        }

    }

    private static boolean isAnforderungExist(Anforderung anforderung) {
        return anforderung != null;
    }

    public String proceedWithZuordnungAufheben(ProzessbaukastenViewData viewData) {

        AnforderungenTabDTO anforderungDto = viewData.getZugeordneteAnforderungToWithdraw();
        if (!isAnforderungDtoValid(anforderungDto)) {
            LOG.error("Gewaehlter Anforderung DTO nicht mehr aktuell");
        }

        Anforderung anforderung = getAnforderungByAnforderungDtoId(viewData);

        if (isAnforderungExist(anforderung)) {
            doProceedWithZuordnungAufheben(anforderung, viewData);
        } else {
            LOG.error("Gewaehlter Anforderung wurde in Datenbank nicht gefunden");
        }

        return refreshViewDataWithAnforderungen(viewData);
    }

    private static boolean isAnforderungDtoValid(AnforderungenTabDTO anforderungDto) {
        if (anforderungDto == null || anforderungDto.getId().isEmpty()) {
            return false;
        }

        String anforderungDtoId = anforderungDto.getId();
        if (!RegexUtils.matchesId(anforderungDtoId)) {
            return false;
        }

        return true;
    }

    private Anforderung getAnforderungByAnforderungDtoId(ProzessbaukastenViewData viewData) {
        String anforderungIdString = viewData.getZugeordneteAnforderungToWithdraw().getId();
        Long anforderungId = Long.parseLong(anforderungIdString);
        Anforderung anforderung = anforderungService.getAnforderungById(anforderungId);
        return anforderung;
    }

    public void reorderAnforderungen(Prozessbaukasten prozessbaukasten, ReorderEvent event) {
        int fromIndex = event.getFromIndex();
        int toIndex = event.getToIndex();
        LOG.debug("Swap anforderugen from {} to {}", fromIndex, toIndex);
        List<Anforderung> anforderungen = prozessbaukasten.getAnforderungen();
        LOG.debug("Anforderungen before swap: {}", anforderungen);
        if (toIndex >= fromIndex) {
            Collections.rotate(anforderungen.subList(fromIndex, toIndex + 1), -1);
        } else {
            Collections.rotate(anforderungen.subList(toIndex, fromIndex + 1), 1);
        }
        LOG.debug("Anforderungen after swap: {}", anforderungen);
        prozessbaukastenService.save(prozessbaukasten);
    }


    private Optional<Prozessbaukasten> getProzessbaukastenByFachIdAndVersion(ProzessbaukastenViewData viewData) throws NumberFormatException {
        String fachId = viewData.getFachId();
        int version = Integer.parseInt(viewData.getVersion());
        Optional<Prozessbaukasten> prozessbaukastenToFind = prozessbaukastenReadService.getByFachIdAndVersion(fachId, version);
        return prozessbaukastenToFind;
    }

    public String processStatusChange(ProzessbaukastenStatus newStatus, ProzessbaukastenViewData viewData, ActiveIndex activeIndex) {

        if (newStatus == null) {
            LOG.error("ProzessbaukastenViewFacade.processStatusChage: NewStatus is Null");
            return "";
        }

        Prozessbaukasten prozessbaukastenToFind = getProzessbaukastenByFachIdAndVersion(viewData).orElse(null);

        if (prozessbaukastenToFind == null) {
            LOG.error("ProzessbaukastenViewFacade.processStatusChage: Prozessbaukasten not find");
            return "";
        }

        return prozessbaukastenStatusChangeValidatorService.validateStatusChange(newStatus, viewData, activeIndex, prozessbaukastenToFind);

    }

    public void downloadExcelExport(ProzessbaukastenViewData viewData) {
        final String fileName = viewData.getProzessbaukastenOrigin().toString();
        final List<ProzessbaukastenAnforderung> anforderungen = viewData.getAnforderungen();
        prozessbaukastenExcelExportDownloadService.downloadExcelExport(fileName, anforderungen);
    }


}
