package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.converters.DerivatConverter;
import de.interfaceag.bmw.pzbk.converters.KonzeptConverter;
import de.interfaceag.bmw.pzbk.converters.ThemenklammerConverter;
import de.interfaceag.bmw.pzbk.converters.TteamConverter;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenAnforderungKonzept;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenStatusChangeDialogData;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.ProzessbaukastenDisabledAttributes;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required.ProzessbaukastenRequiredAttributes;
import de.interfaceag.bmw.pzbk.prozessbaukasten.validator.AnhangBeauftragungTPKValidator;
import de.interfaceag.bmw.pzbk.prozessbaukasten.validator.AnhangGenehmigungTPKValidator;
import de.interfaceag.bmw.pzbk.prozessbaukasten.validator.AnhangGrafikUmfangValidator;
import de.interfaceag.bmw.pzbk.prozessbaukasten.validator.AnhangKonzeptbaumValidator;
import de.interfaceag.bmw.pzbk.prozessbaukasten.validator.AnhangStartbriefValidator;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenAnhangTabData;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import org.primefaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.validator.ValidatorException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static de.interfaceag.bmw.pzbk.prozessbaukasten.AnhangValidator.isAnhangValid;
import static de.interfaceag.bmw.pzbk.prozessbaukasten.FileUploadToAnhangConverter.convertToAnhang;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ProzessbaukastenEditViewData implements ProzessbaukastenEditStammdatenTab,
        ProzessbaukastenAnforderungenEditTab, ProzessbaukastenAnhangTabData,
        ProzessbaukastenEditAnhangTab, ProzessbaukastenStatusChangeDialogData, Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ProzessbaukastenEditViewData.class.getName());

    private final Prozessbaukasten prozessbaukastenOrigin;
    private final Prozessbaukasten prozessbaukasten;

    private final String statusLabel;

    private String tteamLeiter;
    private Derivat erstanlaeufer;

    private final ProzessbaukastenStatus status;

    private final List<Tteam> allTteams;

    private final ProzessbaukastenEditViewPermission viewPermission;

    private List<ProzessbaukastenAnforderungEdit> anforderungen;
    private List<ProzessbaukastenAnforderungEdit> filteredAnforderungen;

    private final Locale currentLocale;

    private Set<Anhang> uploadedAnhaenge;
    private Set<Long> removedAnhaengeIds;

    private String newVersionKommentar;
    private boolean newVersion;

    private final ProzessbaukastenDisabledAttributes disabledAttributes;
    private final ProzessbaukastenRequiredAttributes requiredAttributes;

    private final boolean isStatusChange;
    private final ProzessbaukastenStatus newStatus;
    private final String newStatusLabel;
    private String statusChangeKommentar;

    private AddKonzeptDialog addKonzeptDialog;
    private final List<Derivat> allDerivate;

    private List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte;

    private final List<ThemenklammerDto> allThemenklammern;

    private ProzessbaukastenEditViewData(Prozessbaukasten prozessbaukastenOrigin, Prozessbaukasten prozessbaukasten,
            ProzessbaukastenStatus status, String statusLabel,
            String tteamLeiter, Derivat erstanlaeufer, ProzessbaukastenEditViewPermission viewPermission,
            List<Tteam> allTteams, List<Derivat> allDerivate, List<ProzessbaukastenAnforderungEdit> anforderungen, Set<Anhang> uploadedAnhaenge,
            Set<Long> removedAnhaengeIds, ProzessbaukastenDisabledAttributes disabledAttributes,
            ProzessbaukastenRequiredAttributes requiredAttributes, boolean isStatusChange,
            ProzessbaukastenStatus newStatus, String newStatusLabel, Locale currentLocale,
            List<ThemenklammerDto> allThemenklammern
    ) {
        this.prozessbaukastenOrigin = prozessbaukastenOrigin;
        this.prozessbaukasten = prozessbaukasten;
        this.status = status;
        this.statusLabel = statusLabel;
        this.tteamLeiter = tteamLeiter;
        this.erstanlaeufer = erstanlaeufer;
        this.viewPermission = viewPermission;
        this.allTteams = allTteams;
        this.allDerivate = allDerivate;
        this.anforderungen = anforderungen;
        this.filteredAnforderungen = anforderungen;

        this.uploadedAnhaenge = uploadedAnhaenge;
        this.removedAnhaengeIds = removedAnhaengeIds;

        this.currentLocale = currentLocale;
        this.disabledAttributes = disabledAttributes;
        this.requiredAttributes = requiredAttributes;
        this.isStatusChange = isStatusChange;
        this.newStatus = newStatus;
        this.newStatusLabel = newStatusLabel;
        this.addKonzeptDialog = new AddKonzeptDialogViewData(requiredAttributes, prozessbaukasten.getKonzepte());
        this.allThemenklammern = allThemenklammern;
    }

    public String getId() {
        return prozessbaukasten.getId() != null ? prozessbaukasten.getId().toString() : "";
    }

    public String getFachId() {
        return prozessbaukasten.getFachId();
    }

    public String getVersion() {
        return Integer.toString(prozessbaukasten.getVersion());
    }

    @Override
    public String getBezeichnung() {
        return prozessbaukasten.getBezeichnung();
    }

    @Override
    public void setBezeichnung(String bezeichnung) {
        prozessbaukasten.setBezeichnung(bezeichnung);
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    @Override
    public String getBeschreibung() {
        return prozessbaukasten.getBeschreibung();
    }

    @Override
    public void setBeschreibung(String beschreibung) {
        prozessbaukasten.setBeschreibung(beschreibung);
    }

    @Override
    public Tteam getTteam() {
        return prozessbaukasten.getTteam();
    }

    @Override
    public void setTteam(Tteam tteam) {
        prozessbaukasten.setTteam(tteam);
    }

    @Override
    public String getTteamleiter() {
        return tteamLeiter;
    }

    public void setTteamLeiter(String tteamLeiter) {
        this.tteamLeiter = tteamLeiter;
    }

    @Override
    public Mitarbeiter getLeadTechnologie() {
        return prozessbaukasten.getLeadTechnologie();
    }

    @Override
    public void setLeadTechnologie(Mitarbeiter leadTechnologie) {
        prozessbaukasten.setLeadTechnologie(leadTechnologie);
    }

    @Override
    public String getStandardisierterFertigungsprozess() {
        return prozessbaukasten.getStandardisierterFertigungsprozess();
    }

    @Override
    public void setStandardisierterFertigungsprozess(String standardisierterFertigungsprozess) {
        prozessbaukasten.setStandardisierterFertigungsprozess(standardisierterFertigungsprozess);
    }

    @Override
    public Derivat getErstanlaeufer() {
        return erstanlaeufer;
    }

    @Override
    public void setErstanlaeufer(Derivat derivat) {
        prozessbaukasten.setErstanlaeufer(derivat);
    }

    @Override
    public List<Tteam> getAllTteams() {
        return allTteams;
    }

    @Override
    public Converter getTteamConverter() {
        return new TteamConverter(allTteams);
    }

    public ProzessbaukastenStatus getStatus() {
        return status;
    }

    public ProzessbaukastenEditViewPermission getViewPermission() {
        return viewPermission;
    }

    public Prozessbaukasten getProzessbaukasten() {
        return prozessbaukasten;
    }

    public Prozessbaukasten getProzessbaukastenOrigin() {
        return prozessbaukastenOrigin;
    }

    public String getNewVersionKommentar() {
        return newVersionKommentar;
    }

    public void setNewVersionKommentar(String newVersionKommentar) {
        this.newVersionKommentar = newVersionKommentar;
    }

    public boolean isNewVersion() {
        return newVersion;
    }

    public void setNewVersion(boolean newVersion) {
        this.newVersion = newVersion;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    @Override
    public List<ProzessbaukastenAnforderungEdit> getAnforderungen() {
        return anforderungen;
    }

    @Override
    public void setAnforderungen(List<ProzessbaukastenAnforderungEdit> anforderungen) {
        this.anforderungen = anforderungen;
    }

    @Override
    public List<ProzessbaukastenAnforderungEdit> getFilteredAnforderungen() {
        return filteredAnforderungen;
    }

    @Override
    public void setFilteredAnforderungen(List<ProzessbaukastenAnforderungEdit> filteredAnforderungen) {
        this.filteredAnforderungen = filteredAnforderungen;
    }

    @Override
    public List<Konzept> getAllKonzepte() {
        return getKonzepte();
    }

    @Override
    public Converter getKonzepteConverter() {
        return new KonzeptConverter(getAllKonzepte());
    }

    public Set<Anhang> getUploadedAnhaenge() {
        return uploadedAnhaenge;
    }

    public void setUploadedAnhaenge(Set<Anhang> uploadedAnhaenge) {
        this.uploadedAnhaenge = uploadedAnhaenge;
    }

    public Set<Long> getRemovedAnhaengeIds() {
        return removedAnhaengeIds;
    }

    public void setRemovedAnhaengeIds(Set<Long> removedAnhaengeIds) {
        this.removedAnhaengeIds = removedAnhaengeIds;
    }

    @Override
    public List<Konzept> getKonzepte() {
        List<Konzept> konzepte = prozessbaukasten.getKonzepte();
        Collections.sort(konzepte);
        return konzepte;
    }

    @Override
    public void setKonzepte(List<Konzept> konzepte) {
        this.prozessbaukasten.setKonzepte(konzepte);
    }

    public List<ProzessbaukastenAnforderungKonzept> getAnforderungKonzepte() {
        return anforderungKonzepte;
    }

    public void setAnforderungKonzepte(List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte) {
        this.anforderungKonzepte = anforderungKonzepte;
    }

    @Override
    public KonzeptAnhangUploader getKonzeptAnhangUploader(Konzept konzept) {
        return new KonzeptAnhangUploader(konzept);
    }

    @Override
    public KonzeptRemover getKonzeptRemover(Konzept konzept) {
        return new KonzeptRemover(getKonzepte(), konzept);
    }

    @Override
    public KonzeptAnhangValidator getKonzeptAnhangValidator(Konzept konzept) {
        return new KonzeptAnhangValidator(konzept);
    }

    @Override
    @Deprecated
    public void addNewKonzept() {
        getKonzepte().add(0, new Konzept());
    }

    @Override
    public void uploadStartbrief(FileUploadEvent event) {
        if (isAnhangValid(event)) {
            try {
                Anhang startbriefAnhang = convertToAnhang(event);
                setStartbrief(startbriefAnhang);
                getUploadedAnhaenge().add(startbriefAnhang);
            } catch (IOException ex) {
                LOG.error(null, ex);
            }
        }
    }

    @Override
    public void uploadGrafikUmfang(FileUploadEvent event) {
        if (isAnhangValid(event)) {
            try {
                Anhang grafikUmfang = convertToAnhang(event);
                setGrafikUmfang(grafikUmfang);
                getUploadedAnhaenge().add(grafikUmfang);
            } catch (IOException ex) {
                LOG.error(null, ex);
            }
        }
    }

    @Override
    public void uploadKonzeptbaum(FileUploadEvent event) {
        if (isAnhangValid(event)) {
            try {
                Anhang konzeptbaum = convertToAnhang(event);
                setKonzeptbaum(konzeptbaum);
                getUploadedAnhaenge().add(konzeptbaum);
            } catch (IOException ex) {
                LOG.error(null, ex);
            }
        }
    }

    @Override
    public void uploadBeauftragungTPK(FileUploadEvent event) {
        if (isAnhangValid(event)) {
            try {
                Anhang beauftragungTPK = convertToAnhang(event);
                setBeauftragungTPK(beauftragungTPK);
                getUploadedAnhaenge().add(beauftragungTPK);
            } catch (IOException ex) {
                LOG.error(null, ex);
            }
        }
    }

    @Override
    public void uploadGenehmigungTPK(FileUploadEvent event) {
        if (isAnhangValid(event)) {
            try {
                Anhang genehmigungTPK = convertToAnhang(event);
                setGenehmigungTPK(genehmigungTPK);
                getUploadedAnhaenge().add(genehmigungTPK);
            } catch (IOException ex) {
                LOG.error(null, ex);
            }
        }
    }

    @Override
    public void uploadWeiterenAnhang(FileUploadEvent event) {
        if (isAnhangValid(event)) {
            try {
                Anhang weiterenAnhang = convertToAnhang(event);
                addWeiterenAnhang(weiterenAnhang);
                getUploadedAnhaenge().add(weiterenAnhang);
            } catch (IOException ex) {
                LOG.error(null, ex);
            }
        }
    }

    private void addWeiterenAnhang(Anhang anhang) {
        prozessbaukasten.addWeiterenAnhang(anhang);
    }

    private void setStartbrief(Anhang startbrief) {
        prozessbaukasten.setStartbrief(startbrief);
    }

    @Override
    public Anhang getStartbrief() {
        return prozessbaukasten.getStartbrief();
    }

    private void setGrafikUmfang(Anhang grafikUmfang) {
        prozessbaukasten.setGrafikUmfang(grafikUmfang);
    }

    @Override
    public Anhang getGrafikUmfang() {
        return prozessbaukasten.getGrafikUmfang();
    }

    private void setKonzeptbaum(Anhang konzeptbaum) {
        prozessbaukasten.setKonzeptbaum(konzeptbaum);
    }

    @Override
    public Anhang getKonzeptbaum() {
        return prozessbaukasten.getKonzeptbaum();
    }

    private void setBeauftragungTPK(Anhang beauftragungTPK) {
        prozessbaukasten.setBeauftragungTPK(beauftragungTPK);
    }

    @Override
    public Anhang getBeauftragungTPK() {
        return prozessbaukasten.getBeauftragungTPK();
    }

    private void setGenehmigungTPK(Anhang genehmigungTPK) {
        prozessbaukasten.setGenehmigungTPK(genehmigungTPK);
    }

    @Override
    public Anhang getGenehmigungTPK() {
        return prozessbaukasten.getGenehmigungTPK();
    }

    @Override
    public List<Anhang> getWeitereAnhaenge() {
        return prozessbaukasten.getWeitereAnhaenge();
    }

    @Override
    public boolean isExistWeitereAnhange() {
        return getWeitereAnhaenge() != null && !getWeitereAnhaenge().isEmpty();
    }

    @Override
    public void removeStartbrief() {
        getRemovedAnhaengeIds().add(prozessbaukasten.getStartbrief().getId());
        prozessbaukasten.setStartbrief(null);
    }

    @Override
    public void removeGrafikUmfang() {
        getRemovedAnhaengeIds().add(prozessbaukasten.getGrafikUmfang().getId());
        prozessbaukasten.setGrafikUmfang(null);
    }

    @Override
    public void removeKonzeptbaum() {
        getRemovedAnhaengeIds().add(prozessbaukasten.getKonzeptbaum().getId());
        prozessbaukasten.setKonzeptbaum(null);
    }

    @Override
    public void removeBeauftragungTPK() {
        getRemovedAnhaengeIds().add(prozessbaukasten.getBeauftragungTPK().getId());
        prozessbaukasten.setBeauftragungTPK(null);
    }

    @Override
    public void removeGenehmigungTPK() {
        getRemovedAnhaengeIds().add(prozessbaukasten.getGenehmigungTPK().getId());
        prozessbaukasten.setGenehmigungTPK(null);
    }

    @Override
    public void removeWeiterenAnhang(Anhang anhang) {
        getRemovedAnhaengeIds().add(anhang.getId());
        prozessbaukasten.removeWeiterenAnhang(anhang);
    }

    @Override
    public void validateStartbrief(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        AnhangStartbriefValidator.validate(prozessbaukasten, currentLocale, getRequiredAttributes());
    }

    @Override
    public void validateGrafikUmfang(FacesContext fc, UIComponent uic, Object object) throws ValidatorException {
        AnhangGrafikUmfangValidator.validate(prozessbaukasten, currentLocale, getRequiredAttributes());
    }

    @Override
    public void validateKonzeptbaum(FacesContext fc, UIComponent uic, Object object) throws ValidatorException {
        AnhangKonzeptbaumValidator.validate(prozessbaukasten, currentLocale, getRequiredAttributes());
    }

    @Override
    public void validateBeauftragungTPK(FacesContext fc, UIComponent uic, Object object) throws ValidatorException {
        AnhangBeauftragungTPKValidator.validate(prozessbaukasten, currentLocale, getRequiredAttributes());
    }

    @Override
    public void validateGenehmigungTPK(FacesContext fc, UIComponent uic, Object object) throws ValidatorException {
        AnhangGenehmigungTPKValidator.validate(prozessbaukasten, currentLocale, getRequiredAttributes());
    }

    @Override
    public ProzessbaukastenDisabledAttributes getProzessbaukastenDisabledAttributes() {
        return disabledAttributes;
    }

    @Override
    public ProzessbaukastenRequiredAttributes getRequiredAttributes() {
        return requiredAttributes;
    }

    public boolean isIsStatusChange() {
        return isStatusChange;
    }

    public ProzessbaukastenStatus getNewStatus() {
        return newStatus;
    }

    public String getNewStatusLabel() {
        return newStatusLabel;
    }

    @Override
    public String getStatusChangeKommentar() {
        return statusChangeKommentar;
    }

    @Override
    public void setStatusChangeKommentar(String statusChangeKommentar) {
        this.statusChangeKommentar = statusChangeKommentar;
    }

    public AddKonzeptDialog getAddKonzeptDialog() {
        return addKonzeptDialog;
    }

    @Override
    public List<Derivat> getAllDerivate() {
        return allDerivate;
    }

    @Override
    public Converter getDerivatConverter() {
        return new DerivatConverter(allDerivate);
    }

    @Override
    public List<ThemenklammerDto> getAllThemenklammern() {
        return allThemenklammern;
    }

    @Override
    public Converter getThemenklammerConverter() {
        return new ThemenklammerConverter(allThemenklammern);
    }

    public static final class Builder {

        private Prozessbaukasten prozessbaukastenOrigin;

        private Prozessbaukasten prozessbaukasten;
        private ProzessbaukastenStatus status;
        private String statusLabel;

        private String tteamLeiter;
        private Derivat erstanlaeufer;

        private ProzessbaukastenEditViewPermission viewPermission;

        private List<Tteam> allTteams;
        private List<Derivat> allDerivate;

        private List<ProzessbaukastenAnforderungEdit> anforderungen = new ArrayList<>();

        private Set<Anhang> uploadedAnhaenge;
        private Set<Long> removedAnhaengeIds;

        private ProzessbaukastenDisabledAttributes disabledAttributes;
        private ProzessbaukastenRequiredAttributes requiredAttributes;

        private boolean isStatusChange;
        private ProzessbaukastenStatus newStatus;
        private String newStatusLabel;

        private Locale currentLocale = Locale.GERMAN;

        private List<ThemenklammerDto> allThemenklammern;

        private Builder() {

        }

        public Builder withProzessbaukastenOrigin(Prozessbaukasten prozessbaukastenOriginOptional) {
            if (Objects.nonNull(prozessbaukastenOriginOptional)) {
                this.prozessbaukastenOrigin = prozessbaukastenOriginOptional;
            } else {
                this.prozessbaukastenOrigin = null;
            }

            return this;
        }

        public Builder withProzessbaukasten(Prozessbaukasten prozessbaukasten) {
            this.prozessbaukasten = prozessbaukasten;
            return this;
        }

        public Builder withStatus(ProzessbaukastenStatus status) {
            this.status = status;
            return this;
        }

        public Builder withStatusLabel(String statusLabel) {
            this.statusLabel = statusLabel;
            return this;
        }

        public Builder withTteamleiter(String tteamLeiter) {
            this.tteamLeiter = tteamLeiter;
            return this;
        }

        public Builder withErstanlaeufer(Derivat erstanlaeufer) {
            this.erstanlaeufer = erstanlaeufer;
            return this;
        }

        public Builder withViewPermission(ProzessbaukastenEditViewPermission viewPermission) {
            this.viewPermission = viewPermission;
            return this;
        }

        public Builder withAllTteams(List<Tteam> allTteams) {
            this.allTteams = allTteams;
            return this;
        }

        public Builder withAllDerivaten(List<Derivat> allDerivate) {
            this.allDerivate = allDerivate;
            return this;
        }

        public Builder withAnforderungen(List<ProzessbaukastenAnforderungEdit> anforderungen) {
            this.anforderungen = anforderungen;
            return this;
        }

        public Builder withUploadedAnhaenge(Set<Anhang> uploadedAnhaenge) {
            this.uploadedAnhaenge = Optional.ofNullable(uploadedAnhaenge).orElse(new HashSet<>());
            return this;
        }

        public Builder withRemovedAnhaengeIds(Set<Long> removedAnhaengeIds) {
            this.removedAnhaengeIds = Optional.ofNullable(removedAnhaengeIds).orElse(new HashSet<>());
            return this;
        }

        public Builder withDisabledAttributes(ProzessbaukastenDisabledAttributes disabledAttributes) {
            this.disabledAttributes = disabledAttributes;
            return this;
        }

        public Builder withRequiredAttributes(ProzessbaukastenRequiredAttributes requiredAttributes) {
            this.requiredAttributes = requiredAttributes;
            return this;
        }

        public Builder withStatusChange(ProzessbaukastenStatus newStatus, String newStatusLabel) {
            this.isStatusChange = true;
            this.newStatus = newStatus;
            this.newStatusLabel = newStatusLabel;
            return this;
        }

        public Builder withoutStatusChange() {
            this.isStatusChange = false;
            return this;
        }

        public Builder withCurrentLocale(Locale currentLocale) {
            this.currentLocale = currentLocale;
            return this;
        }

        public Builder withThemenklammern(List<ThemenklammerDto> allThemenklammern) {
            this.allThemenklammern = allThemenklammern;
            return this;
        }

        public ProzessbaukastenEditViewData build() {
            return new ProzessbaukastenEditViewData(prozessbaukastenOrigin, prozessbaukasten, status,
                    statusLabel, tteamLeiter, erstanlaeufer, viewPermission, allTteams, allDerivate,
                    anforderungen, uploadedAnhaenge, removedAnhaengeIds, disabledAttributes,
                    requiredAttributes, isStatusChange, newStatus, newStatusLabel, currentLocale,
                    allThemenklammern
            );
        }
    }

}
