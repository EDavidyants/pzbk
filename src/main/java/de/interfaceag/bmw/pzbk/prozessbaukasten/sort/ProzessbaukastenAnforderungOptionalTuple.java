package de.interfaceag.bmw.pzbk.prozessbaukasten.sort;

import java.util.Optional;

/**
 *
 * @author sl
 */
public interface ProzessbaukastenAnforderungOptionalTuple {

    Optional<Long> getProzessbaukastenId();

    Long getAnforderungId();

    String getAnforderungFachId();

}
