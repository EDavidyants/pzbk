package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

/**
 *
 * @author fn
 */
public interface ProzessbaukastenZuordnenDialogMethods {

    String prozessbaukastenZuordnen();

    void resetProzessbaukastenZuordnenDialog();

}
