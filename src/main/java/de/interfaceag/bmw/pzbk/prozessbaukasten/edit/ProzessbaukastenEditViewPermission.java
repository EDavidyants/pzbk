package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamMitgliedBerechtigung;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.ProzessbaukastenTteamRolePermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ProzessbaukastenEditViewPermission implements Serializable {

    private final ViewPermission page;

    private ProzessbaukastenEditViewPermission(Set<Rolle> userRoles, Prozessbaukasten prozessbaukasten, Collection<Long> tteamIdsWithWritePermissionsAsTteamleiter, TteamMitgliedBerechtigung tteamMitgliedBerechtigung) {
        Collection<Long> tteamIdsWithWritePermission = new HashSet<>();
        tteamIdsWithWritePermission.addAll(tteamIdsWithWritePermissionsAsTteamleiter);
        tteamIdsWithWritePermission.addAll(tteamMitgliedBerechtigung.getTteamIdsMitSchreibrechten());
        if (prozessbaukasten.getId() != null) {
            page = getViewPermissionForPage(userRoles, prozessbaukasten, tteamIdsWithWritePermission);
        } else {
            page = getPagePermission(userRoles);
        }
    }

    private ProzessbaukastenEditViewPermission() {
        page = new FalsePermission();
    }

    private ViewPermission getViewPermissionForPage(Set<Rolle> userRoles,
            Prozessbaukasten prozessbaukasten, Collection<Long> tteamIdsWithReadePermissions) {

        RolePermission rolePermission = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN)
                .authorizeWriteForRole(Rolle.T_TEAMLEITER)
                .authorizeWriteForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(userRoles)
                .get();

        return ProzessbaukastenTteamRolePermission.forProzessbaukasten(prozessbaukasten)
                .forUserRolesWithPermissions(userRoles)
                .forTteamIdsWithReadPermissions(tteamIdsWithReadePermissions)
                .withRolePermission(rolePermission)
                .build();
    }

    public static ProzessbaukastenEditViewPermission denied() {
        return new ProzessbaukastenEditViewPermission();
    }

    public boolean getPage() {
        return page.isRendered();
    }

    private static ViewPermission getPagePermission(Set<Rolle> userRoles) {
        return RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(userRoles)
                .get();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private Set<Rolle> userRoles;
        private Prozessbaukasten prozessbaukasten;
        private Collection<Long> tteamIdsWithWritePermissionsAsTteamleiter;
        private TteamMitgliedBerechtigung tteamMitgliedBerechtigung;

        public Builder() {
        }

        public Builder forUserRoles(Set<Rolle> userRoles) {
            this.userRoles = userRoles;
            return this;
        }

        public Builder forProzessbaukaten(Prozessbaukasten prozessbaukasten) {
            this.prozessbaukasten = prozessbaukasten;
            return this;
        }

        public Builder forTteamIdsWithWritePermissionAsTteamLeiter(Collection<Long> tteamIdsWithWritePermissionsAsTteamleiter) {
            this.tteamIdsWithWritePermissionsAsTteamleiter = tteamIdsWithWritePermissionsAsTteamleiter;
            return this;
        }

        public Builder forTteamMitgliedBerechtigung(TteamMitgliedBerechtigung tteamMitgliedBerechtigung) {
            this.tteamMitgliedBerechtigung = tteamMitgliedBerechtigung;
            return this;
        }

        public ProzessbaukastenEditViewPermission build() {
            return new ProzessbaukastenEditViewPermission(userRoles, prozessbaukasten, tteamIdsWithWritePermissionsAsTteamleiter, tteamMitgliedBerechtigung);
        }

    }
}
