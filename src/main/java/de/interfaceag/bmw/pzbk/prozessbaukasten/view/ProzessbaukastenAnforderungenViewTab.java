package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.prozessbaukasten.AnforderungenTabDTO;

import java.util.List;

/**
 *
 * @author evda
 */
public interface ProzessbaukastenAnforderungenViewTab {

    List<AnforderungenTabDTO> getAnforderungen();

    List<AnforderungenTabDTO> getFilteredAnforderungen();

    void setFilteredAnforderungen(List<AnforderungenTabDTO> filteredAnforderungen);

    String anforderungZuordnungAufheben(AnforderungenTabDTO anforderung);

}
