package de.interfaceag.bmw.pzbk.prozessbaukasten.sort;

import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;

/**
 *
 * @author sl
 */
public class ProzessbaukastenIdAnforderungIdTuple extends GenericTuple<Long, Long> implements ProzessbaukastenAnforderungTuple {

    public ProzessbaukastenIdAnforderungIdTuple(Long anforderungId, Long prozessbaukastenId) {
        super(anforderungId, prozessbaukastenId);
    }

    @Override
    public long getProzessbaukastenId() {
        return getY();
    }

    @Override
    public long getAnforderungId() {
        return getX();
    }

}
