package de.interfaceag.bmw.pzbk.prozessbaukasten.history;

import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenHistory;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Dependent
public class ProzessbaukastenHistoryDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public void save(ProzessbaukastenHistory history) {
        if (history.getId() != null) {
            entityManager.merge(history);
        } else {
            entityManager.persist(history);
        }
        entityManager.flush();
    }

    public List<ProzessbaukastenHistory> getByFachIdAndVersion(String fachId, int version) {
        if (Objects.isNull(fachId) || fachId.isEmpty()) {
            return Collections.emptyList();
        } else {
            TypedQuery<ProzessbaukastenHistory> query = entityManager.createNamedQuery(ProzessbaukastenHistory.BY_FACHID_VERSION, ProzessbaukastenHistory.class);
            query.setParameter("fachId", fachId);
            query.setParameter("version", version);
            return query.getResultList();
        }
    }

}
