package de.interfaceag.bmw.pzbk.prozessbaukasten.dto;

import java.io.Serializable;

public class ProzessbaukastenLabelDto implements Serializable {
    private final Long id;
    private final String fachId;
    private final int version;

    public ProzessbaukastenLabelDto(Long id, String fachId, int version) {
        this.id = id;
        this.fachId = fachId;
        this.version = version;
    }

    public String getFachId() {
        return fachId;
    }

    public int getVersion() {
        return version;
    }

    public long getId() {
        return id;
    }

    public String getLabelString() {
        return fachId + " | V" + version;
    }


}
