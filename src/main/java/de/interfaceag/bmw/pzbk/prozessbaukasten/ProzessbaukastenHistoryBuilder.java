package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenHistory;

import java.util.Date;

public class ProzessbaukastenHistoryBuilder {

    private Prozessbaukasten prozessbaukasten;
    private String benutzer;
    private Date datum;
    private String changedField;
    private String von;
    private String auf;

    public ProzessbaukastenHistoryBuilder() {
        this.datum = new Date();
    }

    public ProzessbaukastenHistoryBuilder setProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        this.prozessbaukasten = prozessbaukasten;
        return this;
    }

    public ProzessbaukastenHistoryBuilder setBenutzer(String benutzer) {
        this.benutzer = benutzer;
        return this;
    }

    public ProzessbaukastenHistoryBuilder setDatum(Date datum) {
        this.datum = datum;
        return this;
    }

    public ProzessbaukastenHistoryBuilder setChangedField(String changedField) {
        this.changedField = changedField;
        return this;
    }

    public ProzessbaukastenHistoryBuilder setVon(String von) {
        this.von = von;
        return this;
    }

    public ProzessbaukastenHistoryBuilder setAuf(String auf) {
        this.auf = auf;
        return this;
    }

    public ProzessbaukastenHistory build() {
        return new ProzessbaukastenHistory(prozessbaukasten, datum, benutzer, changedField, von, auf);
    }
}
