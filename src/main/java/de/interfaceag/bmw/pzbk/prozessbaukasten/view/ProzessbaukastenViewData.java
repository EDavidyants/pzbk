package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.controller.dialogs.StatusChangeNextStatusDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.VersionPlusKommentarDialog;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.prozessbaukasten.AnforderungenTabDTO;
import de.interfaceag.bmw.pzbk.prozessbaukasten.KonzeptDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenAnforderung;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.AnforderungenProzessbaukastenZuordnenViewData;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ZuordnungAufhebenDialogFields;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import static de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenFreigabeUtils.isAllProzessbaukastenAnforderungenGenehmigtImProzessbaukasten;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ProzessbaukastenViewData implements ProzessbaukastenStammdatenTab,
        StatusChangeNextStatusDialog, VersionPlusKommentarDialog, ProzessbaukastenAnforderungenTab,
        ZuordnungAufhebenDialogFields, Serializable {

    private final String id;
    private final String fachId;
    private final String version;
    private final String bezeichnung;
    private final String statusLabel;

    private final Prozessbaukasten prozessbaukastenOrigin;

    private final ProzessbaukastenStammdatenViewData stammdaten;

    private final ProzessbaukastenAnforderungenViewData anforderungenTabViewData;

    private final ProzessbaukastenStatus status;
    private final List<ProzessbaukastenStatus> naechsteStatus;
    private Integer selectedNextStatus;
    private String statusChangeKommentar;

    private final ProzessbaukastenAnhangTabViewData anhangTabViewData;

    private final ProzessbaukastenViewPermission viewPermission;

    private final AnforderungenProzessbaukastenZuordnenViewData anforderungenProzessbaukastenZuordnenViewData;

    private String versionKommentar = "";
    private String zuordnungAufhebenKommentar;
    private AnforderungenTabDTO zugeordneteAnforderungToWithdraw;

    private ProzessbaukastenViewData(Prozessbaukasten prozessbaukastenOrigin, String id, String fachId, String version, String bezeichnung,
            String statusLabel, ProzessbaukastenStammdatenViewData stammdaten, ProzessbaukastenViewPermission viewPermission,
            ProzessbaukastenAnforderungenViewData anforderungenTabViewData,
            ProzessbaukastenAnhangTabViewData anhangTabViewData, ProzessbaukastenStatus status,
            List<ProzessbaukastenStatus> naechsteStatus,
            AnforderungenProzessbaukastenZuordnenViewData anforderungenProzessbaukastenZuordnenViewData) {
        this.prozessbaukastenOrigin = prozessbaukastenOrigin;
        this.id = id;
        this.fachId = fachId;
        this.version = version;
        this.bezeichnung = bezeichnung;
        this.statusLabel = statusLabel;
        this.stammdaten = stammdaten;
        this.viewPermission = viewPermission;
        this.anforderungenTabViewData = anforderungenTabViewData;
        this.anhangTabViewData = anhangTabViewData;
        this.status = status;
        this.naechsteStatus = naechsteStatus;
        this.anforderungenProzessbaukastenZuordnenViewData = anforderungenProzessbaukastenZuordnenViewData;
        this.zuordnungAufhebenKommentar = null;
    }

    @Override
    public List<ProzessbaukastenAnforderung> getAnforderungen() {
        return anforderungenTabViewData.getAnforderungen();
    }

    @Override
    public List<ProzessbaukastenAnforderung> getFilteredAnforderungen() {
        return anforderungenTabViewData.getFilteredAnforderungen();
    }

    @Override
    public void setFilteredAnforderungen(List<ProzessbaukastenAnforderung> filteredAnforderungen) {
        anforderungenTabViewData.setFilteredAnforderungen(filteredAnforderungen);
    }

    public Prozessbaukasten getProzessbaukastenOrigin() {
        return prozessbaukastenOrigin;
    }

    public String getId() {
        return id;
    }

    public String getFachId() {
        return fachId;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public String getBezeichnung() {
        return bezeichnung;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    @Override
    public String getBeschreibung() {
        return stammdaten.getBeschreibung();
    }

    @Override
    public String getTteam() {
        return stammdaten.getTteam();
    }

    @Override
    public String getTteamleiter() {
        return stammdaten.getTteamLeiter();
    }

    @Override
    public String getLeadTechnologie() {
        return stammdaten.getLeadTechnologie();
    }

    @Override
    public String getStandardisierterFertigungsprozess() {
        return stammdaten.getStandardisierterFertigungsprozess();
    }

    @Override
    public String getErstanlaeufer() {
        return stammdaten.getErstanlaeufer();
    }

    public ProzessbaukastenStatus getStatus() {
        return status;
    }

    public List<ProzessbaukastenStatus> getNaechsteStatus() {
        return naechsteStatus;
    }

    @Override
    public Integer getSelectedNextStatus() {
        return selectedNextStatus;
    }

    @Override
    public void setSelectedNextStatus(Integer selectedNextStatus) {
        this.selectedNextStatus = selectedNextStatus;
    }

    @Override
    public String getStatusChangeKommentar() {
        return statusChangeKommentar;
    }

    @Override
    public void setStatusChangeKommentar(String statusChangeKommentar) {
        this.statusChangeKommentar = statusChangeKommentar;
    }

    @Override
    public String getVersionKommentar() {
        return versionKommentar;
    }

    @Override
    public void setVersionKommentar(String versionKommentar) {
        this.versionKommentar = versionKommentar;
    }

    @Override
    public String getZuordnungAufhebenKommentar() {
        return zuordnungAufhebenKommentar;
    }

    @Override
    public void setZuordnungAufhebenKommentar(String zuordnungAufhebenKommentar) {
        this.zuordnungAufhebenKommentar = zuordnungAufhebenKommentar;
    }

    @Override
    public AnforderungenTabDTO getZugeordneteAnforderungToWithdraw() {
        return zugeordneteAnforderungToWithdraw;
    }

    @Override
    public void setZugeordneteAnforderungToWithdraw(AnforderungenTabDTO zugeordneteAnforderungToWithdraw) {
        this.zugeordneteAnforderungToWithdraw = zugeordneteAnforderungToWithdraw;
    }

    public ProzessbaukastenAnhangTabViewData getAnhangTabViewData() {
        return anhangTabViewData;
    }

    public ProzessbaukastenViewPermission getViewPermission() {
        return viewPermission;
    }

    public AnforderungenProzessbaukastenZuordnenViewData getAnforderungenProzessbaukastenZuordnenViewData() {
        return anforderungenProzessbaukastenZuordnenViewData;
    }

    @Override
    public List<KonzeptDto> getKonzepte() {
        return stammdaten.getKonzepte();
    }

    public boolean isAlleAnforderungenFreigegeben() {
        return isAllProzessbaukastenAnforderungenGenehmigtImProzessbaukasten(getAnforderungen());
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public static final class Builder {

        private String id;
        private String fachId;
        private String version;
        private String bezeichnung;
        private String statusLabel;
        private Prozessbaukasten prozessbaukastenOrigin;
        private ProzessbaukastenStammdatenViewData stammdaten;
        private ProzessbaukastenAnforderungenViewData anforderungenTabViewData;
        private ProzessbaukastenViewPermission viewPermission;
        private ProzessbaukastenAnhangTabViewData anhangTabViewData;
        private ProzessbaukastenStatus status;
        private List<ProzessbaukastenStatus> naechsteStatus;
        private AnforderungenProzessbaukastenZuordnenViewData anforderungenProzessbaukastenZuordnenViewData;

        private Builder() {

        }

        public Builder withProzessbaukastenOrigin(Optional<Prozessbaukasten> prozessbaukastenOriginOptional) {
            if (prozessbaukastenOriginOptional.isPresent()) {
                this.prozessbaukastenOrigin = prozessbaukastenOriginOptional.get();
            } else {
                this.prozessbaukastenOrigin = null;
            }

            return this;
        }

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder withFachId(String fachId) {
            this.fachId = fachId;
            return this;
        }

        public Builder withVersion(String version) {
            this.version = version;
            return this;
        }

        public Builder withBezeichnung(String bezeichnung) {
            this.bezeichnung = bezeichnung;
            return this;
        }

        public Builder withStatusLabel(String statusLabel) {
            this.statusLabel = statusLabel;
            return this;
        }

        public Builder withStammdaten(ProzessbaukastenStammdatenViewData stammdaten) {
            this.stammdaten = stammdaten;
            return this;
        }

        public Builder withViewPermission(ProzessbaukastenViewPermission viewPermission) {
            this.viewPermission = viewPermission;
            return this;
        }

        public Builder withAnforderungenTabViewData(ProzessbaukastenAnforderungenViewData anforderungenTabViewData) {
            this.anforderungenTabViewData = anforderungenTabViewData;
            return this;
        }

        public Builder withAnhangTabViewData(ProzessbaukastenAnhangTabViewData anhangTabViewData) {
            this.anhangTabViewData = anhangTabViewData;
            return this;
        }

        public Builder withStatus(ProzessbaukastenStatus status) {
            this.status = status;
            return this;
        }

        public Builder withNaechsteStatus(List<ProzessbaukastenStatus> naechsteStatus) {
            this.naechsteStatus = naechsteStatus;
            return this;
        }

        public Builder withAnforderungProzessbaukastenZuordnenViewData(AnforderungenProzessbaukastenZuordnenViewData anforderungenProzessbaukastenZuordnenViewData) {
            this.anforderungenProzessbaukastenZuordnenViewData = anforderungenProzessbaukastenZuordnenViewData;
            return this;
        }

        public ProzessbaukastenViewData build() {
            return new ProzessbaukastenViewData(prozessbaukastenOrigin, id, fachId, version,
                    bezeichnung, statusLabel, stammdaten, viewPermission,
                    anforderungenTabViewData, anhangTabViewData, status,
                    naechsteStatus, anforderungenProzessbaukastenZuordnenViewData);
        }
    }
}
