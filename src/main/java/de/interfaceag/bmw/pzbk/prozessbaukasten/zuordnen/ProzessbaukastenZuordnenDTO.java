package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import java.io.Serializable;

/**
 *
 * @author fn
 */
public class ProzessbaukastenZuordnenDTO implements Serializable {

    private final Long id;
    private final String fachId;
    private final Integer version;
    private final String name;
    private final String beschreibung;

    public ProzessbaukastenZuordnenDTO(Long id, String fachId, Integer version, String name, String beschreibung) {
        this.id = id;
        this.fachId = fachId;
        this.version = version;
        this.name = name;
        this.beschreibung = beschreibung;
    }

    public String getFachId() {
        return fachId;
    }

    public Integer getVersion() {
        return version;
    }

    public Long getId() {
        return id;
    }

    public String getNameForView() {
        if ("".equals(fachId) && version == 0) {
            return "";
        } else {
            return fachId + " | V" + version + " : " + name;
        }
    }

    public String getBeschreibung() {
        return beschreibung;
    }

}
