package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenAnforderungKonzept;
import de.interfaceag.bmw.pzbk.entities.Themenklammer;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.ProzessbaukastenAnforderungEdit;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import org.apache.commons.lang.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lomu
 */
public final class AnforderungenTabDTO implements ProzessbaukastenAnforderungEdit {

    private final String id;
    private final String fachId;
    private final String version;
    private final String fachIdAndVersion;
    private final String beschreibung;
    private final String beschreibungShort;
    private final String sensorCoc;
    private final String umsetzer;
    private final String konzept;
    private final String themenklammer;
    private final Status status;
    private List<Konzept> anforderungKonzept;
    private List<ThemenklammerDto> themenklammern;

    private AnforderungenTabDTO(String id, String fachId, String version, String beschreibung, String sensorCoc,
                                String umsetzer, String konzept, List<Konzept> anforderungKonzept, Status status,
                                List<ThemenklammerDto> themenklammern, String themenklammer) {
        this.id = id;
        this.fachId = fachId;
        this.version = version;
        this.beschreibung = beschreibung;
        if (beschreibung.length() > 80) {
            this.beschreibungShort = beschreibung.substring(0, 80) + "...";
        } else {
            this.beschreibungShort = beschreibung;
        }
        this.sensorCoc = sensorCoc;
        this.umsetzer = umsetzer;
        this.konzept = konzept;
        this.anforderungKonzept = anforderungKonzept;
        this.status = status;
        this.fachIdAndVersion = fachId + " | V" + version;
        this.themenklammern = themenklammern;
        this.themenklammer = themenklammer;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getFachId() {
        return fachId;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public String getFachIdAndVersion() {
        return fachIdAndVersion;
    }

    @Override
    public String getBeschreibung() {
        return beschreibung;
    }

    @Override
    public String getSensorCoc() {
        return sensorCoc;
    }

    @Override
    public String getUmsetzer() {
        return umsetzer;
    }

    @Override
    public String getKonzept() {
        return konzept;
    }

    @Override
    public String getStatus() {
        return status.getStatusBezeichnung();
    }

    @Override
    public Status getStatusValue() {
        return status;
    }

    @Override
    public String getBeschreibungShort() {
        return beschreibungShort;
    }

    @Override
    public List<Konzept> getAnforderungKonzept() {
        Collections.sort(anforderungKonzept);
        return anforderungKonzept;
    }

    @Override
    public String getThemenklammer() {
        return themenklammer;
    }

    @Override
    public void setAnforderungKonzept(List<Konzept> anforderungKonzept) {
        this.anforderungKonzept = anforderungKonzept;
    }

    @Override
    public List<ThemenklammerDto> getThemenklammern() {
        return themenklammern;
    }

    @Override
    public void setThemenklammern(List<ThemenklammerDto> themenklammern) {
        this.themenklammern = themenklammern;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public static List<ProzessbaukastenAnforderung> forAnforderungen(List<Anforderung> anforderungen, List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte, Prozessbaukasten prozessbaukasten) {
        List<ProzessbaukastenAnforderung> result = anforderungen.stream().map(anforderung -> {
            String umsetzer = StringUtils.join(anforderung.getUmsetzer(), ",");
            String konzept = getKonzeptBezeichnungForAnforderung(anforderung.getId(), anforderungKonzepte);
            ProzessbaukastenAnforderung prozessbaukastenAnforderung = new Builder().withId(anforderung.getId().toString())
                    .withFachId(anforderung.getFachId())
                    .withVersion(anforderung.getVersion().toString())
                    .withBeschreibung(anforderung.getBeschreibungAnforderungDe())
                    .withSensorCoc(anforderung.getSensorCoc().getTechnologie())
                    .withUmsetzer(umsetzer)
                    .withKonzept(konzept)
                    .withThemenklammern(anforderung.getThemenklammernForProzessbaukasten(prozessbaukasten))
                    .withStatus(anforderung.getStatus())
                    .build();
            return prozessbaukastenAnforderung;
        }).collect(Collectors.toList());
        return result;
    }

    private static String getKonzeptBezeichnungForAnforderung(long anforderungId, List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte) {
        return anforderungKonzepte.stream()
                .filter(anforderungKonzept -> anforderungKonzept.getAnforderung().getId() == anforderungId)
                .filter(anforderungKonzept -> anforderungKonzept.getKonzept() != null)
                .map(anforderungKonzept -> anforderungKonzept.getKonzept().getBezeichnung())
                .collect(Collectors.joining(", "));
    }

    private static List<Konzept> getKonzeptForAnforderung(long anforderungId, List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte) {
        return anforderungKonzepte.stream()
                .filter(anforderungKonzept -> anforderungKonzept.getAnforderung().getId() == anforderungId)
                .filter(anforderungKonzept -> anforderungKonzept.getKonzept() != null)
                .map(ProzessbaukastenAnforderungKonzept::getKonzept)
                .collect(Collectors.toList());
    }

    public static List<ProzessbaukastenAnforderungEdit> forAnforderungenEdit(List<Anforderung> anforderungen, List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte, Prozessbaukasten prozessbaukasten) {
        return anforderungen.stream().map(anforderung -> {
            String umsetzer = StringUtils.join(anforderung.getUmsetzer(), ",");
            List<Konzept> konzepte = getKonzeptForAnforderung(anforderung.getId(), anforderungKonzepte);
            return new Builder().withId(anforderung.getId().toString())
                    .withFachId(anforderung.getFachId())
                    .withVersion(anforderung.getVersion().toString())
                    .withBeschreibung(anforderung.getBeschreibungAnforderungDe())
                    .withSensorCoc(anforderung.getSensorCoc().getTechnologie())
                    .withUmsetzer(umsetzer)
                    .withAnforderungKonzept(konzepte)
                    .withThemenklammern(anforderung.getThemenklammernForProzessbaukasten(prozessbaukasten))
                    .withStatus(anforderung.getStatus())
                    .build();
        }).collect(Collectors.toList());
    }

    public static class Builder {

        private String id;
        private String fachId;
        private String version;
        private String beschreibung;
        private String sensorCoc;
        private String umsetzer;
        private Status status;
        private String konzept;
        private List<Konzept> anforderungKonzept;
        private String themenklammer;
        private List<ThemenklammerDto> themenklammern;

        public Builder() {
            this.anforderungKonzept = Collections.emptyList();
        }

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder withFachId(String fachId) {
            this.fachId = fachId;
            return this;
        }

        public Builder withVersion(String version) {
            this.version = version;
            return this;
        }

        public Builder withBeschreibung(String beschreibung) {
            this.beschreibung = beschreibung;
            return this;
        }

        public Builder withSensorCoc(String sensorCoc) {
            this.sensorCoc = sensorCoc;
            return this;
        }

        public Builder withUmsetzer(String umsetzer) {
            this.umsetzer = umsetzer;
            return this;
        }

        public Builder withKonzept(String konzept) {
            this.konzept = konzept;
            return this;
        }

        public Builder withAnforderungKonzept(List<Konzept> konzept) {
            if (konzept != null) {
                this.anforderungKonzept = konzept;
                this.konzept = getKonzepteBezeichnung(konzept);
            }
            return this;
        }

        public Builder withThemenklammern(List<Themenklammer> themenklammern) {
            if (themenklammern != null) {
                this.themenklammern = themenklammern.stream()
                        .map(Themenklammer::getDto)
                        .collect(Collectors.toList());
                this.themenklammer = getThemenklammerBezeichnung(this.themenklammern);
            }
            return this;
        }

        public Builder withThemenklammer(String themenklammer) {
            this.themenklammer = themenklammer;
            return this;
        }

        private static String getThemenklammerBezeichnung(List<ThemenklammerDto> themenklammern) {
            return themenklammern.stream()
                    .map(ThemenklammerDto::getBezeichnung).sorted()
                    .collect(Collectors.joining(", "));
        }

        private static String getKonzepteBezeichnung(List<Konzept> konzepte) {
            return konzepte.stream()
                    .map(Konzept::getBezeichnung).sorted()
                    .collect(Collectors.joining(", "));
        }

        public Builder withStatus(Status status) {
            this.status = status;
            return this;
        }

        public AnforderungenTabDTO build() {
            return new AnforderungenTabDTO(id, fachId, version, beschreibung, sensorCoc, umsetzer, konzept, anforderungKonzept, status, themenklammern, themenklammer);
        }

    }

}
