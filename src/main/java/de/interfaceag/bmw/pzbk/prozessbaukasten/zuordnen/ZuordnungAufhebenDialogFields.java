package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import de.interfaceag.bmw.pzbk.prozessbaukasten.AnforderungenTabDTO;

/**
 *
 * @author evda
 */
public interface ZuordnungAufhebenDialogFields {

    AnforderungenTabDTO getZugeordneteAnforderungToWithdraw();

    void setZugeordneteAnforderungToWithdraw(AnforderungenTabDTO zugeordneteAnforderungToWithdraw);

    String getZuordnungAufhebenKommentar();

    void setZuordnungAufhebenKommentar(String zuordnungAufhebenKommentar);

}
