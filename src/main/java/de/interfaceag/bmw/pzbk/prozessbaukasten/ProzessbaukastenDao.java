package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenVersionMenuDto;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author evda
 */
@Dependent
public class ProzessbaukastenDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public Prozessbaukasten getById(Long id) {
        return entityManager.find(Prozessbaukasten.class, id);
    }

    public List<Prozessbaukasten> getByAnforderungIds(Collection<Long> anforderungIds) {
        TypedQuery<Prozessbaukasten> query = entityManager.createNamedQuery(Prozessbaukasten.GET_BY_ANFORDERUNG_IDS, Prozessbaukasten.class);
        query.setParameter("anforderungIds", anforderungIds);
        return anforderungIds != null && !anforderungIds.isEmpty() ? query.getResultList() : Collections.emptyList();
    }

    public Optional<Prozessbaukasten> getByFachIdAndVersion(String fachId, int version) {
        TypedQuery<Prozessbaukasten> query = entityManager.createNamedQuery(Prozessbaukasten.GET_BY_FACHID_VERSION, Prozessbaukasten.class);
        query.setParameter("fachId", fachId);
        query.setParameter("version", version);
        List<Prozessbaukasten> result = query.getResultList();
        return (result != null && !result.isEmpty()) ? Optional.ofNullable(result.get(0)) : Optional.empty();
    }

    public Prozessbaukasten save(Prozessbaukasten prozessbaukasten) {
        if (prozessbaukasten.getId() != null) {
            entityManager.merge(prozessbaukasten);
        } else {
            entityManager.persist(prozessbaukasten);
        }
        entityManager.flush();
        return prozessbaukasten;
    }

    public List<Prozessbaukasten> getAll() {
        TypedQuery<Prozessbaukasten> query = entityManager.createNamedQuery(Prozessbaukasten.ALL, Prozessbaukasten.class);
        List<Prozessbaukasten> result = query.getResultList();
        return Collections.unmodifiableList(result);
    }

    public List<ProzessbaukastenFilterDto> getAllFilterDto() {
        QueryPart queryPart = new QueryPartDTO("SELECT id, fachId, version, bezeichnung FROM Prozessbaukasten; ");
        Query query = queryPart.buildNativeQuery(entityManager);
        List<Object[]> queryList = query.getResultList();

        List<ProzessbaukastenFilterDto> result = queryList.stream().map(o -> new ProzessbaukastenFilterDto((Long) o[0], (String) o[1], (Integer) o[2], (String) o[3])).collect(Collectors.toList());

        return Collections.unmodifiableList(result);
    }

    public List<Prozessbaukasten> getAllZuweisbareProzessbaukaesten() {
        TypedQuery<Prozessbaukasten> query = entityManager.createNamedQuery(Prozessbaukasten.ALL_ZUORTENBARE, Prozessbaukasten.class);
        List<Integer> statusList = new ArrayList<>();
        statusList.add(ProzessbaukastenStatus.ANGELEGT.getId());
        statusList.add(ProzessbaukastenStatus.BEAUFTRAGT.getId());
        query.setParameter("statusList", statusList);
        return query.getResultList();
    }

    public Optional<Prozessbaukasten> getProzessbaukastenByAnforderungId(Long anforderungId) {
        TypedQuery<Prozessbaukasten> query = entityManager.createNamedQuery(Prozessbaukasten.GET_BY_ANFORDERUNG, Prozessbaukasten.class);
        query.setParameter("anforderungId", anforderungId);
        List<Prozessbaukasten> result = query.getResultList();
        return (result != null && !result.isEmpty()) ? Optional.ofNullable(result.get(0)) : Optional.empty();
    }

    public Optional<Integer> getLastVersionNumberByFachId(String fachId) {
        TypedQuery<Integer> query = entityManager.createNamedQuery(Prozessbaukasten.GET_LATEST_VERSION_NUMBER, Integer.class);
        query.setParameter("fachId", fachId);
        List<Integer> result = query.getResultList();
        return (result != null && !result.isEmpty()) ? Optional.ofNullable(result.get(0)) : Optional.empty();
    }

    public List<ProzessbaukastenVersionMenuDto> fetchVersionenForFachId(String fachId) {
        String escapedFachId = org.apache.commons.lang.StringEscapeUtils.escapeSql(fachId);
        String sqlQuery = "SELECT p.fachid, p.version, p.erstellungsdatum FROM prozessbaukasten p "
                + "WHERE p.fachid = '" + escapedFachId + "' "
                + "ORDER BY p.version";
        Query q = entityManager.createNativeQuery(sqlQuery);
        List<Object[]> queryResult = q.getResultList();
        return ProzessbaukastenVersionMenuDto.buildFromCollection(queryResult);
    }

}
