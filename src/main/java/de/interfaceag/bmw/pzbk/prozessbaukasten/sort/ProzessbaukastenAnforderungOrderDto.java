package de.interfaceag.bmw.pzbk.prozessbaukasten.sort;

/**
 *
 * @author sl
 */
public class ProzessbaukastenAnforderungOrderDto implements ProzessbaukastenAnforderungOrder {

    private final ProzessbaukastenAnforderungTuple prozessbaukastenAnforderungTuple;
    private final Long position;

    protected ProzessbaukastenAnforderungOrderDto(ProzessbaukastenAnforderungTuple prozessbaukastenAnforderungTuple, Long position) {
        this.prozessbaukastenAnforderungTuple = prozessbaukastenAnforderungTuple;
        this.position = position;
    }

    @Override
    public Long getProzessbaukastenId() {
        return prozessbaukastenAnforderungTuple.getProzessbaukastenId();
    }

    @Override
    public Long getAnforderungId() {
        return prozessbaukastenAnforderungTuple.getAnforderungId();
    }

    @Override
    public Long getPosition() {
        return position;
    }

}
