package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenAnforderungKonzept;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Dependent
public class ProzessbaukastenAnforderungKonzeptDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager em;

    public ProzessbaukastenAnforderungKonzept save(ProzessbaukastenAnforderungKonzept anforderungKonzept) {
        if (anforderungKonzept.getId() != null) {
            em.merge(anforderungKonzept);
        } else {
            em.persist(anforderungKonzept);
        }
        em.flush();
        return anforderungKonzept;
    }

    public void remove(ProzessbaukastenAnforderungKonzept anforderungKonzept) {
        em.remove(anforderungKonzept);
    }

    public List<ProzessbaukastenAnforderungKonzept> getByProzessbaukastenId(long prozessbaukastenId) {
        Query q = em.createNamedQuery(ProzessbaukastenAnforderungKonzept.BY_PROZESSBAUKASTEN, ProzessbaukastenAnforderungKonzept.class);
        q.setParameter("prozessbaukastenId", prozessbaukastenId);
        List<ProzessbaukastenAnforderungKonzept> result = q.getResultList();
        return result;
    }

    public List<ProzessbaukastenAnforderungKonzept> getByAnforderungId(long anforderungId) {
        Query q = em.createNamedQuery(ProzessbaukastenAnforderungKonzept.BY_ANFORDERUNG, ProzessbaukastenAnforderungKonzept.class);
        q.setParameter("anforderungId", anforderungId);
        List<ProzessbaukastenAnforderungKonzept> result = q.getResultList();
        return result;
    }

}
