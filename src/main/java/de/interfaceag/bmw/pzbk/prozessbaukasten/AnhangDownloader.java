package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author sl
 */
public interface AnhangDownloader {

    StreamedContent download(Anhang anhang);

}
