package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungStatusChangeService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.Kategorie;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.AnforderungReportingStatusTransitionAdapter;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;

@Stateless
@Named
public class ProzessbaukastenCreateAnforderungZuordnungService implements Serializable {

    @Inject
    private AnforderungMeldungHistoryService historyService;
    @Inject
    private AnforderungReportingStatusTransitionAdapter anforderungReportingStatusTransitionAdapter;
    @Inject
    private AnforderungStatusChangeService anforderungStatusChangeSerivce;
    @Inject
    private ProzessbaukastenService prozessbaukastenService;

    public void assignAnforderungToProzessbaukasten(Anforderung anforderung, Prozessbaukasten prozessbaukasten, Mitarbeiter currentUser) {
        updateAnforderungKategorie(anforderung, currentUser);
        addProzessbaukastenToAnforderung(anforderung, prozessbaukasten);

        final Status status = anforderung.getStatus();
        updateAnforderungStatusToAngelegt(anforderung, currentUser);

        saveZuordnung(prozessbaukasten, anforderung, status, currentUser);
    }

    private void saveZuordnung(Prozessbaukasten prozessbaukasten, Anforderung anforderung, Status status, Mitarbeiter currentUser) {
        Prozessbaukasten prozessbaukastenOriginOptional = prozessbaukastenService.createRealCopy(prozessbaukasten).orElse(null);
        addEntryToReportingLsDataStructure(anforderung, status);
        addAnforderungToProzessbaukasten(prozessbaukasten, anforderung);
        prozessbaukastenService.saveWithHistory(prozessbaukasten, prozessbaukastenOriginOptional, currentUser, new Date());
    }

    private void addEntryToReportingLsDataStructure(Anforderung anforderung, Status status) {
        anforderungReportingStatusTransitionAdapter.addAnforderungToProzessbaukasten(anforderung, status);
    }

    private static void addProzessbaukastenToAnforderung(Anforderung anforderung, Prozessbaukasten prozessbaukasten) {
        anforderung.getProzessbaukasten().add(prozessbaukasten);
    }

    private static void addAnforderungToProzessbaukasten(Prozessbaukasten prozessbaukasten, Anforderung anforderung) {
        prozessbaukasten.getAnforderungen().add(anforderung);
    }

    private void updateAnforderungKategorie(Anforderung anforderung, Mitarbeiter currentUser) {
        Kategorie oldKategorie = anforderung.getKategorie();
        anforderung.setKategorie(Kategorie.EINS);
        if (!oldKategorie.equals(Kategorie.EINS)) {
            historyService.writeHistoryForChangedKeyValue(currentUser, anforderung, Kategorie.EINS.name(), oldKategorie.name(), "kategorie");
        }
    }

    private void updateAnforderungStatusToAngelegt(Anforderung anforderung, Mitarbeiter currentUser) {
        anforderungStatusChangeSerivce.changeAnforderungStatus(anforderung, Status.A_ANGELEGT_IN_PROZESSBAUKASTEN, "Angelegt im Prozessbaukasten", currentUser);
    }

}
