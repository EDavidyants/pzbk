package de.interfaceag.bmw.pzbk.prozessbaukasten;

/**
 *
 * @author sl
 */
public interface ProzessbaukastenStatusChangeDialog {

    String processStatusChange();

    ProzessbaukastenStatusChangeDialogData getDialogData();

}
