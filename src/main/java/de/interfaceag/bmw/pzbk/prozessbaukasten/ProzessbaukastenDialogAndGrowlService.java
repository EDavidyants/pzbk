package de.interfaceag.bmw.pzbk.prozessbaukasten;


import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import org.primefaces.context.RequestContext;

import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Stateless
@Named
public class ProzessbaukastenDialogAndGrowlService {

    @Inject
    private LocalizationService localizationService;

    public String openStatusChangeDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialog:statusChangeForm");
        context.execute("PF('statusChangeDialog').show()");
        return "";
    }


    public String openStatusChangeNotAllowedGrowl(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        String messageTitle = getTitleForStatusChangeGrowl();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, messageTitle, message));
        return "";
    }

    private String getTitleForStatusChangeGrowl() {
        return localizationService.getValue("prozessbaukasten_view_statusWechselNichtMoeglich");
    }
}
