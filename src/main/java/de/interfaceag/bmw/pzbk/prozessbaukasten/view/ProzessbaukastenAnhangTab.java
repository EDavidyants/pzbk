package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.prozessbaukasten.AnhangDownloader;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface ProzessbaukastenAnhangTab extends AnhangDownloader {

    ProzessbaukastenAnhangTabData getProzessbaukastenAnhangTabData();

}
