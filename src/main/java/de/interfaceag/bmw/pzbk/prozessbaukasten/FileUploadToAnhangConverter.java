package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import org.apache.poi.util.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 *
 * @author sl
 */
public final class FileUploadToAnhangConverter {

    private static final Logger LOG = LoggerFactory.getLogger(FileUploadToAnhangConverter.class.getName());

    private FileUploadToAnhangConverter() {
    }

    public static Anhang convertToAnhang(FileUploadEvent event) throws IOException {
        UploadedFile file = event.getFile();
        byte[] uploadedData = IOUtils.toByteArray(file.getInputstream());
        Anhang anhang = new Anhang(null, file.getFileName(), file.getContentType(), uploadedData);
        LOG.info("Convert file {} to Anhang", file.getFileName());
        return anhang;
    }

}
