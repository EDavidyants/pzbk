package de.interfaceag.bmw.pzbk.prozessbaukasten.sort;

public final class ProzessbaukastenAnforderungTupleBuilder {

    private Long anforderungId;
    private Long prozessbaukastenId;

    private ProzessbaukastenAnforderungTupleBuilder() {
    }

    public static ProzessbaukastenAnforderungTupleBuilder withAnforderungId(Long anforderungId) {
        return new ProzessbaukastenAnforderungTupleBuilder().setAnforderungId(anforderungId);
    }

    private ProzessbaukastenAnforderungTupleBuilder setAnforderungId(Long anforderungId) {
        this.anforderungId = anforderungId;
        return this;
    }

    public ProzessbaukastenAnforderungTupleBuilder withProzessbaukastenId(Long prozessbaukastenId) {
        this.prozessbaukastenId = prozessbaukastenId;
        return this;
    }

    public ProzessbaukastenAnforderungTuple build() {
        return new ProzessbaukastenIdAnforderungIdTuple(anforderungId, prozessbaukastenId);
    }

}
