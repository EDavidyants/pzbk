package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.controller.dialogs.StatusChangeMethodChangeDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.VersionPlusCreateNewVersionDialog;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ActiveIndex;
import de.interfaceag.bmw.pzbk.prozessbaukasten.AnforderungenTabDTO;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.AnforderungenProzessbaukastenZuordnenDialogMethods;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ZuordnungAufhebenDialogMethods;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ZuordnungAufhebenDialogVerwaltung;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.ReorderEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.menu.MenuModel;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ViewScoped
@Named
public class ProzessbaukastenViewController implements StatusChangeMethodChangeDialog,
        ProzessbaukastenAnhangTab, VersionPlusCreateNewVersionDialog,
        AnforderungenProzessbaukastenZuordnenDialogMethods,
        ZuordnungAufhebenDialogVerwaltung, ZuordnungAufhebenDialogMethods,
        ProzessbaukastenAnforderungenTabReorder, Serializable {

    @Inject
    private ProzessbaukastenViewFacade facade;

    private ProzessbaukastenViewData viewData;

    private ActiveIndex activeIndex;

    @PostConstruct
    public void init() {
        initViewData();
        initActiveIndex();
    }

    private void initActiveIndex() {
        UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();
        activeIndex = ActiveIndex.forUrlParameter(urlParameter);
    }

    private void initViewData() {
        UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();
        viewData = facade.getViewData(urlParameter);
    }

    public ProzessbaukastenViewData getViewData() {
        return viewData;
    }

    public ProzessbaukastenViewPermission getViewPermission() {
        return viewData.getViewPermission();
    }

    public String processStatusChange(ProzessbaukastenStatus newStatus) {
        return facade.processStatusChange(newStatus, viewData, activeIndex);
    }

    @Override
    public String changeStatus(Integer newStatus) {
        return facade.changeStatus(newStatus, viewData);
    }

    @Override
    public ProzessbaukastenAnhangTabData getProzessbaukastenAnhangTabData() {
        return getViewData().getAnhangTabViewData();
    }

    @Override
    public StreamedContent download(Anhang anhang) {
        return facade.downloadAnhang(anhang);
    }

    public MenuModel getVersionsMenuItems() {
        return facade.getVersionsMenuItems(viewData);
    }

    @Override
    public String createNewVersion() throws IllegalAccessException, InvocationTargetException {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('versionPlusDialog').hide();");
        return navigateToProzessbaukastenEditViewForNewVersion();
    }

    private String navigateToProzessbaukastenEditViewForNewVersion() {
        String navigateToPage = Page.PROZESSBAUKASTEN_EDIT.getUrl()
                + "?fachId=" + viewData.getFachId()
                + "&version=" + viewData.getVersion()
                + "&created=new" + "&comment=" + viewData.getVersionKommentar()
                + "&faces-redirect=true";

        return navigateToPage;
    }

    @Override
    public String proceedWithZuordnungAufheben() {
        return facade.proceedWithZuordnungAufheben(viewData);
    }

    @Override
    public String anforderungenZuordnen() {
        return facade.anforderungenZuordnen(getViewData().getAnforderungenProzessbaukastenZuordnenViewData().getZugeordneteAnforderungen(), getViewData().getProzessbaukastenOrigin());
    }

    @Override
    public void resetAnforderungenProzessbaukastenZuordnenDialog() {
        viewData.getAnforderungenProzessbaukastenZuordnenViewData().resetZugeordneteAnforderungen();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('anforderungenProzessbaukatenZuordnenDialog').hide()");
    }

    public String openZuordnenDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('anforderungenProzessbaukatenZuordnenDialog').show()");
        return "";
    }

    public ActiveIndex getActiveIndex() {
        return activeIndex;
    }

    public String edit() {
        return PageUtils.getUrlForProzessbaukastenEdit(getViewData().getFachId(), getViewData().getVersion(), activeIndex);
    }

    @Override
    public void openZuordnungAufhebenDialog(AnforderungenTabDTO zugeordneteAnforderungToWithdraw) {
        viewData.setZugeordneteAnforderungToWithdraw(zugeordneteAnforderungToWithdraw);

        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('zuordnungAufhebenDialog').show()");
    }

    @Override
    public void resetZuordnungAufhebenKommentar() {
        viewData.setZuordnungAufhebenKommentar("");
    }

    @Override
    public boolean isProzessbaukastenGueltig() {
        return viewData.getStatus().equals(ProzessbaukastenStatus.GUELTIG);
    }

    @Override
    public void onRowReorder(ReorderEvent event) {
        Prozessbaukasten prozessbaukasten = getViewData().getProzessbaukastenOrigin();
        facade.reorderAnforderungen(prozessbaukasten, event);
    }

    @Override
    public String apply() {
        return PageUtils.getUrlForProzessbaukastenView(getViewData().getFachId(), getViewData().getVersion(), activeIndex);
    }

    public void downloadExcelExport() {
        facade.downloadExcelExport(viewData);
    }


}
