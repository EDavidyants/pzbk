package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenAnforderung;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;

import java.util.List;

/**
 *
 * @author sl
 */
public interface ProzessbaukastenAnforderungEdit extends ProzessbaukastenAnforderung {

    List<Konzept> getAnforderungKonzept();

    void setAnforderungKonzept(List<Konzept> konzept);

    List<ThemenklammerDto> getThemenklammern();

    void setThemenklammern(List<ThemenklammerDto> themenklammern);
}
