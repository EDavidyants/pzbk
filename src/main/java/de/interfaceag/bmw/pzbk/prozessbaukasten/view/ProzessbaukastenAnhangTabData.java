package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.entities.Anhang;

import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface ProzessbaukastenAnhangTabData {

    Anhang getStartbrief();

    Anhang getGrafikUmfang();

    Anhang getKonzeptbaum();

    Anhang getBeauftragungTPK();

    Anhang getGenehmigungTPK();

    List<Anhang> getWeitereAnhaenge();

    boolean isExistWeitereAnhange();

}
