package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.converters.MitarbeiterConverter;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenAnforderungKonzept;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.exceptions.ProzessbaukastenNotFoundException;
import de.interfaceag.bmw.pzbk.exceptions.ProzessbaukastenStatusNotFoundException;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ActiveIndex;
import de.interfaceag.bmw.pzbk.prozessbaukasten.AnforderungenTabDTO;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenAnforderungKonzeptService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenStatusChangeService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenTab;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.ProzessbaukastenDisabledAttributes;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.ProzessbaukastenDisabledAttributesFactory;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required.ProzessbaukastenRequiredAttributes;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required.ProzessbaukastenRequiredAttributesFactory;
import de.interfaceag.bmw.pzbk.prozessbaukasten.themenklammer.AnforderungProzessbaukastenThemenklammerService;
import de.interfaceag.bmw.pzbk.services.AnhangDownloadService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerService;
import org.primefaces.context.RequestContext;
import org.primefaces.model.StreamedContent;

import javax.ejb.Stateless;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenStatusChangeService.isStatusChangeKommentarValid;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class ProzessbaukastenEditViewFacade implements Serializable {

    @Inject
    protected ProzessbaukastenService prozessbaukastenService;
    @Inject
    protected ProzessbaukastenReadService prozessbaukastenReadService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    protected TteamService tteamService;
    @Inject
    private UserSearchService userSearchService;
    @Inject
    private AnhangDownloadService anhangDownloadService;
    @Inject
    private ProzessbaukastenStatusChangeService prozessbaukastenStatusChangeService;
    @Inject
    protected ProzessbaukastenAnforderungKonzeptService anforderungKonzeptService;
    @Inject
    private AnforderungProzessbaukastenThemenklammerService anforderungProzessbaukastenThemenklammerService;
    @Inject
    private ThemenklammerService themenklammerService;

    @Inject
    private DerivatService derivatService;

    @Inject
    protected Session session;

    public ProzessbaukastenEditViewData getViewData(UrlParameter urlParameter) {

        String fachId = urlParameter.getValue("fachId").orElse("");
        String version = urlParameter.getValue("version").orElse("1");
        Optional<String> comment = urlParameter.getValue("comment");
        Optional<String> created = urlParameter.getValue("created");
        Optional<String> newStatus = urlParameter.getValue("newStatus");

        ProzessbaukastenEditViewData viewData;
        if (isExistingProzessbaukasten(fachId, version, created.orElse(null))) {
            if (isStatusChange(newStatus.orElse(null))) {
                viewData = getProzessbaukastenViewDataByFachIdVersionForStatusChange(fachId, version, newStatus.orElse(null));
            } else {
                viewData = getProzessbaukastenViewDataByFachIdVersion(fachId, version);
            }
        } else if (isNewVersion(fachId, version, created.orElse(null))) {
            viewData = generateNewVersionViewData(fachId, version, comment.orElse(""));

        } else {
            viewData = generateNewProzessbaukastenViewData();
        }

        return viewData;
    }

    private static boolean isStatusChange(String newStatus) {
        return Objects.nonNull(newStatus) && RegexUtils.matchesId(newStatus);
    }

    private static boolean isExistingProzessbaukasten(String fachId, String version, String created) {
        return isFachIdValid(fachId) && isVersionValid(version) && !isToBeNewVersion(created);
    }

    private static boolean isNewVersion(String fachId, String version, String created) {
        return isFachIdValid(fachId) && isVersionValid(version) && isToBeNewVersion(created);
    }

    private static boolean isToBeNewVersion(String created) {
        return Objects.nonNull(created);
    }

    private static boolean isFachIdValid(String fachId) {
        return fachId != null && RegexUtils.matchesFachId(fachId);
    }

    private static boolean isVersionValid(String version) {
        return RegexUtils.matchesVersion(version);
    }

    private ProzessbaukastenEditViewData getProzessbaukastenViewDataByFachIdVersionForStatusChange(String fachId, String versionString, String statusChange) {
        int version = Integer.parseInt(versionString);
        Optional<Prozessbaukasten> optionalProzessbaukasten = prozessbaukastenReadService.getByFachIdAndVersion(fachId, version);
        if (optionalProzessbaukasten.isPresent()) {
            Prozessbaukasten prozessbaukasten = optionalProzessbaukasten.get();
            ProzessbaukastenStatus newStatus = getProzessbaukastenStatus(statusChange);
            return generateProzessbaukastenViewDataForProzessbaukastenStatusChange(prozessbaukasten, newStatus);
        } else {
            throw new ProzessbaukastenNotFoundException("No Prozessbaukasten found for fachId " + fachId + " version " + versionString);
        }
    }

    private ProzessbaukastenStatus getProzessbaukastenStatus(String statusId) {
        if (RegexUtils.matchesId(statusId)) {
            int id = Integer.parseInt(statusId);
            ProzessbaukastenStatus status = ProzessbaukastenStatus.getById(id);
            if (status == null) {
                throw new ProzessbaukastenStatusNotFoundException(statusId + " is no valid ProzessbaukastenStatus id!");
            } else {
                return status;
            }
        } else {
            throw new ProzessbaukastenStatusNotFoundException(statusId + " is no valid ProzessbaukastenStatus id!");
        }
    }

    private ProzessbaukastenEditViewData generateProzessbaukastenViewDataForProzessbaukastenStatusChange(Prozessbaukasten prozessbaukasten, ProzessbaukastenStatus newStatus) {
        ProzessbaukastenEditViewData.Builder builder = getProzessbaukastenViewDataBuilderForProzessbaukasten(prozessbaukasten);
        String newStatusLabel = getStatusLabel(newStatus);
        ProzessbaukastenRequiredAttributes requiredAttributes = getRequiredAttributesForStatus(newStatus);
        return builder.withStatusChange(newStatus, newStatusLabel)
                .withRequiredAttributes(requiredAttributes)
                .build();
    }

    private ProzessbaukastenEditViewData getProzessbaukastenViewDataByFachIdVersion(String fachId, String versionString) {
        int version = Integer.parseInt(versionString);
        Prozessbaukasten prozessbaukastenToFind = prozessbaukastenReadService.getByFachIdAndVersion(fachId, version).orElse(null);
        return generateProzessbaukastenViewDataForProzessbaukasten(prozessbaukastenToFind);
    }

    private ProzessbaukastenEditViewData.Builder getProzessbaukastenViewDataBuilderForProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        ProzessbaukastenStatus status = prozessbaukasten.getStatus();
        ProzessbaukastenDisabledAttributes disabledAttributes = getDisabledAttributes(status);
        ProzessbaukastenRequiredAttributes requiredAttributes = getRequiredAttributesForStatus(status);
        List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte = anforderungKonzeptService.getByProzessbaukastenId(prozessbaukasten.getId());
        List<ProzessbaukastenAnforderungEdit> anforderungen = transformAnforderungenToAnforderungenDTO(prozessbaukasten.getAnforderungen(), anforderungKonzepte, prozessbaukasten);
        ProzessbaukastenEditViewPermission viewPermission = getViewPermission(prozessbaukasten);
        List<ThemenklammerDto> allThemenklammern = getAllThemenklammern();

        Locale currentLocale = session.getLocale();

        return ProzessbaukastenEditViewData.getBuilder()
                .withProzessbaukastenOrigin(prozessbaukastenService.createRealCopy(prozessbaukasten).orElse(null))
                .withProzessbaukasten(prozessbaukasten)
                .withStatus(prozessbaukasten.getStatus())
                .withStatusLabel(getStatusLabel(prozessbaukasten.getStatus()))
                .withTteamleiter(getTteamLeiter(prozessbaukasten))
                .withErstanlaeufer(prozessbaukasten.getErstanlaeufer())
                .withViewPermission(viewPermission)
                .withAllTteams(getTteam())
                .withAllDerivaten(getDerivate())
                .withAnforderungen(anforderungen)
                .withUploadedAnhaenge(new HashSet<>())
                .withRemovedAnhaengeIds(new HashSet<>())
                .withDisabledAttributes(disabledAttributes)
                .withRequiredAttributes(requiredAttributes)
                .withCurrentLocale(currentLocale)
                .withThemenklammern(allThemenklammern);
    }

    private List<ThemenklammerDto> getAllThemenklammern() {
        return themenklammerService.getAll();
    }

    private List<Tteam> getTteam() {
        Collection<Long> tteamIdsForView = new HashSet<>();
        if (session.hasRole(Rolle.ADMIN)) {
            return tteamService.getAllTteams();
        }
        List<Long> tteamIdsForTteamLeiter = session.getUserPermissions().getTteamAsTteamleiterSchreibend()
                .stream().map(BerechtigungDto::getId).collect(Collectors.toList());
        tteamIdsForView.addAll(tteamIdsForTteamLeiter);
        List<Long> tteamIdsForTteamVertreter = session.getUserPermissions().getTteamIdsForTteamVertreter();
        tteamIdsForView.addAll(tteamIdsForTteamVertreter);
        List<Long> tteamIdsForTteamMitglied = session.getUserPermissions().getTteamIdsForTteamMitgliedSchreibend();
        tteamIdsForView.addAll(tteamIdsForTteamMitglied);
        List<Long> arrayList = new ArrayList<>(tteamIdsForView);
        return tteamService.getTteamByIdList(arrayList);
    }

    private List<Derivat> getDerivate() {
        if (isUserBerechtigt()) {
            return derivatService.getAllErstanlaeufer();
        }

        return new ArrayList<>();
    }

    private boolean isUserBerechtigt() {
        return session.hasRole(Rolle.ADMIN) || session.hasRole(Rolle.T_TEAMLEITER)
                || session.hasRole(Rolle.TTEAM_VERTRETER) || session.hasRole(Rolle.TTEAMMITGLIED);
    }

    private ProzessbaukastenEditViewData generateProzessbaukastenViewDataForProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        if (Objects.nonNull(prozessbaukasten)) {
            ProzessbaukastenEditViewData.Builder builder = getProzessbaukastenViewDataBuilderForProzessbaukasten(prozessbaukasten);
            return builder.withoutStatusChange().build();
        } else {
            return generateNewProzessbaukastenViewData();
        }

    }

    private ProzessbaukastenRequiredAttributes getRequiredAttributesForStatus(ProzessbaukastenStatus status) {
        return ProzessbaukastenRequiredAttributesFactory.getRequiredAttributesForProzessbaukasten(status);
    }

    private ProzessbaukastenDisabledAttributes getDisabledAttributesForNewProzessbaukasten() {
        return ProzessbaukastenDisabledAttributesFactory.getDisabledAttributesForNewProzessbaukasten();
    }

    private ProzessbaukastenDisabledAttributes getDisabledAttributes(ProzessbaukastenStatus status) {
        boolean userIsAdmin = session.hasRole(Rolle.ADMIN);
        return ProzessbaukastenDisabledAttributesFactory.getDisabledAttributesForProzessbaukasten(status, userIsAdmin);
    }

    private static List<ProzessbaukastenAnforderungEdit> transformAnforderungenToAnforderungenDTO(List<Anforderung> anforderungen, List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte, Prozessbaukasten prozessbaukasten) {
        return AnforderungenTabDTO.forAnforderungenEdit(anforderungen, anforderungKonzepte, prozessbaukasten);
    }

    private ProzessbaukastenEditViewData generateNewVersionViewData(String fachId, String versionString, String comment) {
        int version = Integer.parseInt(versionString);
        Prozessbaukasten prozessbaukastenToFind = prozessbaukastenReadService.getByFachIdAndVersion(fachId, version).orElse(null);
        return generateViewDataForNewVersionProzessbaukasten(prozessbaukastenToFind, comment);
    }

    private ProzessbaukastenEditViewData generateViewDataForNewVersionProzessbaukasten(Prozessbaukasten baseProzessbaukasten, String comment) {
        if (Objects.nonNull(baseProzessbaukasten)) {
            if (Objects.isNull(comment)) {
                comment = "";
            }
            return createViewDataForProzessbaukastenVersion(baseProzessbaukasten, comment);

        } else {
            return generateNewProzessbaukastenViewData();
        }
    }

    private ProzessbaukastenEditViewData createViewDataForProzessbaukastenVersion(Prozessbaukasten baseProzessbaukasten, String newVersionKommentar) {
        Prozessbaukasten prozessbaukasten = prozessbaukastenService.createCopyForNewVersion(baseProzessbaukasten);
        ProzessbaukastenStatus status = prozessbaukasten.getStatus();

        List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte = anforderungKonzeptService.generateAnforderungKonzepteForNewVersion(prozessbaukasten, baseProzessbaukasten);
        List<ProzessbaukastenAnforderungEdit> anforderungen = transformAnforderungenToAnforderungenDTO(prozessbaukasten.getAnforderungen(), anforderungKonzepte, prozessbaukasten);

        ProzessbaukastenDisabledAttributes disabledAttributes = getDisabledAttributes(status);
        ProzessbaukastenRequiredAttributes requiredAttributes = getRequiredAttributesForStatus(status);
        ProzessbaukastenEditViewPermission viewPermission = getViewPermission(baseProzessbaukasten);
        Locale locale = session.getLocale();

        ProzessbaukastenEditViewData viewData = ProzessbaukastenEditViewData.getBuilder()
                .withProzessbaukastenOrigin(baseProzessbaukasten)
                .withProzessbaukasten(prozessbaukasten)
                .withStatus(prozessbaukasten.getStatus())
                .withStatusLabel(getStatusLabel(prozessbaukasten.getStatus()))
                .withTteamleiter(getTteamLeiter(prozessbaukasten))
                .withErstanlaeufer(prozessbaukasten.getErstanlaeufer())
                .withAnforderungen(anforderungen)
                .withViewPermission(viewPermission)
                .withAllTteams(getTteam())
                .withAllDerivaten(getDerivate())
                .withUploadedAnhaenge(new HashSet<>())
                .withRemovedAnhaengeIds(new HashSet<>())
                .withDisabledAttributes(disabledAttributes)
                .withRequiredAttributes(requiredAttributes)
                .withCurrentLocale(locale)
                .build();

        viewData.setAnforderungKonzepte(anforderungKonzepte);
        viewData.setTteam(prozessbaukasten.getTteam());
        viewData.setNewVersionKommentar(newVersionKommentar);
        viewData.setNewVersion(true);
        return viewData;

    }

    private ProzessbaukastenEditViewData generateNewProzessbaukastenViewData() {
        Prozessbaukasten newProzessbaukasten = new Prozessbaukasten();
        ProzessbaukastenDisabledAttributes disabledAttributes = getDisabledAttributesForNewProzessbaukasten();
        ProzessbaukastenRequiredAttributes requiredAttributes = getRequiredAttributesForStatus(ProzessbaukastenStatus.ANGELEGT);
        ProzessbaukastenEditViewPermission viewPermission = getViewPermission(newProzessbaukasten);
        Locale locale = session.getLocale();

        return ProzessbaukastenEditViewData.getBuilder()
                .withProzessbaukasten(newProzessbaukasten)
                .withStatusLabel(LocalizationService.getValue(Locale.GERMAN, "prozessbaukasten_edit_statusLabel"))
                .withTteamleiter("")
                .withViewPermission(viewPermission)
                .withAllTteams(getTteam())
                .withUploadedAnhaenge(new HashSet<>())
                .withRemovedAnhaengeIds(new HashSet<>())
                .withDisabledAttributes(disabledAttributes)
                .withRequiredAttributes(requiredAttributes)
                .withCurrentLocale(locale)
                .build();
    }

    private ProzessbaukastenEditViewPermission getViewPermission(Prozessbaukasten prozessbaukasten) {
        Set<Rolle> userRoles = session.getUserPermissions().getRoles();
        Collection<Long> tteamIds = session.getUserPermissions().getTteamAsTteamleiterSchreibend()
                .stream().map(BerechtigungDto::getId).collect(Collectors.toList());
        return ProzessbaukastenEditViewPermission.builder()
                .forProzessbaukaten(prozessbaukasten)
                .forTteamIdsWithWritePermissionAsTteamLeiter(tteamIds)
                .forTteamMitgliedBerechtigung(session.getUserPermissions().getTteamMitgliedBerechtigung())
                .forUserRoles(userRoles).build();
    }

    private static String getStatusLabel(ProzessbaukastenStatus status) {
        return LocalizationService.getValue(Locale.GERMAN, status.getLocalizationKey());
    }

    private String getTteamLeiter(Prozessbaukasten prozessbaukasten) {
        if (prozessbaukasten != null && prozessbaukasten.getTteam() != null) {
            Mitarbeiter tteamleiter = berechtigungService.getTteamleiterOfTteam(prozessbaukasten.getTteam()).orElse(null);
            if (tteamleiter != null) {
                return tteamleiter.getFullName();
            }
        }
        return "";
    }

    public String save(ProzessbaukastenEditViewData viewData, ActiveIndex activeIndex) {
        if (viewData.isNewVersion()) {
            return saveNewVersion(viewData, activeIndex);
        } else {
            return saveChangedProzessbaukasten(viewData, activeIndex);
        }
    }

    private String saveNewVersion(ProzessbaukastenEditViewData viewData, ActiveIndex activeIndex) {
        Prozessbaukasten prozessbaukastenOrigin = viewData.getProzessbaukastenOrigin();
        Prozessbaukasten prozessbaukasten = viewData.getProzessbaukasten();
        List<ProzessbaukastenAnforderungEdit> anforderungen = viewData.getAnforderungen();
        List<Anforderung> zugeordneteAnforderungen = anforderungKonzeptService.getAnforderungenListForProzessbaukasten(anforderungen);
        String versionKommentar = viewData.getNewVersionKommentar();
        Set<Anhang> uploadedAnhaenge = viewData.getUploadedAnhaenge();
        Set<Long> removedAnhaengeIds = viewData.getRemovedAnhaengeIds();

        prozessbaukastenService.saveNewVersionWithHistory(prozessbaukasten, uploadedAnhaenge, removedAnhaengeIds, prozessbaukastenOrigin, zugeordneteAnforderungen, session.getUser(), versionKommentar);
        List<ProzessbaukastenAnforderungKonzept> currentAnforderungKonzepte = viewData.getAnforderungKonzepte();
        anforderungKonzeptService.updateAnforderungKonzept(prozessbaukasten, anforderungen, currentAnforderungKonzepte);
        anforderungProzessbaukastenThemenklammerService.updateAnforderungThemenklammern(prozessbaukasten, anforderungen);

        return redirectToNextViewBySave(viewData, activeIndex);
    }

    private String saveChangedProzessbaukasten(ProzessbaukastenEditViewData viewData, ActiveIndex activeIndex) {

        Prozessbaukasten prozessbaukasten = viewData.getProzessbaukasten();
        List<ProzessbaukastenAnforderungEdit> anforderungen = viewData.getAnforderungen();
        anforderungKonzeptService.updateAnforderungKonzept(prozessbaukasten, anforderungen);
        anforderungProzessbaukastenThemenklammerService.updateAnforderungThemenklammern(prozessbaukasten, anforderungen);

        if (isStatusChange(viewData)) {
            return openStatusChangeDialog();
        } else {
            return saveChanges(viewData, activeIndex);
        }

    }

    private String saveChanges(ProzessbaukastenEditViewData viewData, ActiveIndex activeIndex) {
        Prozessbaukasten prozessbaukastenOrigin = viewData.getProzessbaukastenOrigin();
        Prozessbaukasten prozessbaukasten = viewData.getProzessbaukasten();
        Set<Anhang> uploadedAnhaenge = viewData.getUploadedAnhaenge();
        Set<Long> removedAnhaengeIds = viewData.getRemovedAnhaengeIds();
        Optional<String> kommentar = Optional.ofNullable(viewData.getNewVersionKommentar());
        prozessbaukastenService.saveWithHistory(prozessbaukasten, prozessbaukastenOrigin, session.getUser(), uploadedAnhaenge, removedAnhaengeIds, kommentar);
        return redirectToNextViewBySave(viewData, activeIndex);
    }

    public String saveStatusChange(ProzessbaukastenEditViewData viewData) {
        ProzessbaukastenStatus newStatus = viewData.getNewStatus();

        String kommentar = viewData.getStatusChangeKommentar();

        if (isStatusChangeKommentarValid(kommentar)) {
            return handleStatusChange(viewData);
        } else {
            prozessbaukastenStatusChangeService.showKommentarRequiredDialog(newStatus);
            return "";
        }
    }

    private String handleStatusChange(ProzessbaukastenEditViewData viewData) {
        Prozessbaukasten prozessbaukastenOrigin = viewData.getProzessbaukastenOrigin();
        ProzessbaukastenStatus newStatus = viewData.getNewStatus();
        Prozessbaukasten prozessbaukasten = viewData.getProzessbaukasten();
        String kommentar = viewData.getStatusChangeKommentar();

        Set<Anhang> uploadedAnhaenge = viewData.getUploadedAnhaenge();
        Set<Long> removedAnhaengeIds = viewData.getRemovedAnhaengeIds();
        prozessbaukastenService.saveWithHistory(prozessbaukasten, prozessbaukastenOrigin, session.getUser(), uploadedAnhaenge, removedAnhaengeIds, Optional.ofNullable(viewData.getNewVersionKommentar()));
        prozessbaukastenStatusChangeService.processStatusChange(newStatus, prozessbaukasten, kommentar);

        return redirectToProzessbaukastenView(prozessbaukasten.getFachId(), Integer.toString(prozessbaukasten.getVersion()), ActiveIndex.forTab(ProzessbaukastenTab.STAMMDATEN));
    }

    private boolean isStatusChange(ProzessbaukastenEditViewData viewData) {
        return viewData.isIsStatusChange();
    }

    public static String cancel(ProzessbaukastenEditViewData viewData, ActiveIndex activeIndex) {
        return redirectToNextViewByCancel(viewData, activeIndex);
    }

    private static String redirectToNextViewBySave(ProzessbaukastenEditViewData viewData, ActiveIndex activeIndex) {
        String fachId = viewData.getFachId();
        String version = viewData.getVersion();

        if (isFachIdAndVersionValid(fachId, version)) {
            return redirectToProzessbaukastenView(fachId, version, activeIndex);
        } else {
            return redirectToDashboard();
        }
    }

    private static String redirectToNextViewByCancel(ProzessbaukastenEditViewData viewData, ActiveIndex activeIndex) {
        String fachId = viewData.getFachId();
        String version = viewData.getVersion();
        boolean isNewVersion = viewData.isNewVersion();

        if (isFachIdAndVersionValid(fachId, version)) {
            if (isNewVersion) {
                version = getBaseVersionForCancel(viewData);
            }
            return redirectToProzessbaukastenView(fachId, version, activeIndex);
        } else {
            return redirectToDashboard();
        }
    }

    private static boolean isFachIdAndVersionValid(String fachId, String version) {
        return fachId != null && !"".equals(fachId) && version != null;
    }

    private static String redirectToProzessbaukastenView(String fachId, String version, ActiveIndex activeIndex) {
        return PageUtils.getUrlForProzessbaukastenView(fachId, Integer.parseInt(version), activeIndex);
    }

    private static String redirectToDashboard() {
        return PageUtils.getRedirectUrlForPage(Page.DASHBOARD);
    }

    private static String getBaseVersionForCancel(ProzessbaukastenEditViewData viewData) {
        int currentVersion = viewData.getProzessbaukasten().getVersion();

        if (viewData.getProzessbaukastenOrigin() != null) {
            int baseVersion = viewData.getProzessbaukastenOrigin().getVersion();
            if (isVersionChanged(baseVersion, currentVersion)) {
                return Integer.toString(baseVersion);
            }
        }

        return Integer.toString(currentVersion);
    }

    private static boolean isVersionChanged(int baseVersion, int currentVersion) {
        return currentVersion != baseVersion;
    }

    public Optional<Mitarbeiter> getTteamleiterOfTteam(Tteam tteam) {
        return berechtigungService.getTteamleiterOfTteam(tteam);
    }

    public List<Mitarbeiter> completeLeadTechnologie(String query) {
        return userSearchService.getMitarbeiterByVornameOrNachname(query);
    }

    public Converter getMitarbeiterConverter() {
        return new MitarbeiterConverter(userSearchService);
    }

    public StreamedContent downloadAnhang(Anhang anhang) {
        return anhangDownloadService.downloadAnhang(anhang);
    }

    public String openStatusChangeDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialog:statusChangeForm");
        context.execute("PF('statusChangeDialog').show()");
        return "";
    }

}
