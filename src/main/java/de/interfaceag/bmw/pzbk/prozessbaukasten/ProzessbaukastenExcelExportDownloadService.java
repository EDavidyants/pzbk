package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

@Stateless
public class ProzessbaukastenExcelExportDownloadService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ProzessbaukastenExcelExportDownloadService.class);

    @Inject
    private ExcelDownloadService excelDownloadService;

    public void downloadExcelExport(String fileName, List<ProzessbaukastenAnforderung> prozessbaukastenAnforderungen) {
        LOG.debug("download excel export {}", fileName);
        try (Workbook workbook = new XSSFWorkbook()) {
            ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, prozessbaukastenAnforderungen);
            excelDownloadService.downloadExcelExport(fileName, workbook);
        } catch (IOException exception) {
            LOG.error("workbook creation failed", exception);
        }
    }

}
