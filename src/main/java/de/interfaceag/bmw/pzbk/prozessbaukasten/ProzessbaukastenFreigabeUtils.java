package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;

import java.util.Collection;

public final class ProzessbaukastenFreigabeUtils {

    private ProzessbaukastenFreigabeUtils() {
    }

    public static boolean isAllAnforderungenGenehmigtImProzessbaukasten(Collection<Anforderung> anforderungen) {
        if (anforderungen != null && !anforderungen.isEmpty()) {
            for (Anforderung anforderung : anforderungen) {
                if (!Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN.equals(anforderung.getStatus())) {
                    return false;
                }
            }
            return true;
        } else {
            return true;
        }
    }

    public static boolean isAllProzessbaukastenAnforderungenGenehmigtImProzessbaukasten(Collection<ProzessbaukastenAnforderung> anforderungen) {
        if (anforderungen != null && !anforderungen.isEmpty()) {
            for (ProzessbaukastenAnforderung anforderung : anforderungen) {
                if (!Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN.equals(anforderung.getStatusValue())) {
                    return false;
                }
            }
            return true;
        } else {
            return true;
        }
    }


}
