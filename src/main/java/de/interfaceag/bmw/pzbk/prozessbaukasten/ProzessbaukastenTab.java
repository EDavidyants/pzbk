package de.interfaceag.bmw.pzbk.prozessbaukasten;

import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author sl
 */
public enum ProzessbaukastenTab {

    STAMMDATEN(0),
    ANFORDERUNGEN(1),
    FESTGESCHRIEBENE_GROESSEN(2),
    ANHANGE(3),
    DERIVATE(4);

    private final Integer index;

    ProzessbaukastenTab(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public static Optional<ProzessbaukastenTab> getByIndex(Integer index) {
        return Stream.of(ProzessbaukastenTab.values()).filter(tab -> tab.getIndex().equals(index)).findAny();
    }

}
