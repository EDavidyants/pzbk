package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenAnforderung;

import java.util.List;

/**
 *
 * @author lomu
 */
public interface ProzessbaukastenAnforderungenTab {

    List<ProzessbaukastenAnforderung> getAnforderungen();

    List<ProzessbaukastenAnforderung> getFilteredAnforderungen();

    void setFilteredAnforderungen(List<ProzessbaukastenAnforderung> filteredAnforderungen);

}
