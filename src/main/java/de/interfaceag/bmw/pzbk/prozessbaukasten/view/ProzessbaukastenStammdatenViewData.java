package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.prozessbaukasten.KonzeptDto;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author evda
 */
public final class ProzessbaukastenStammdatenViewData implements Serializable {

    private final String beschreibung;
    private final String tteam;
    private final String tteamLeiter;
    private final String leadTechnologie;
    private final String standardisierterFertigungsprozess;
    private final String erstanlaeufer;

    private final List<KonzeptDto> konzepte;

    private ProzessbaukastenStammdatenViewData(String beschreibung, String tteam, String tteamLeiter,
            String leadTechnologie, String standardisierterFertigungsprozess, String erstanlaufer,
            List<KonzeptDto> konzepte) {
        this.beschreibung = beschreibung;
        this.tteam = tteam;
        this.tteamLeiter = tteamLeiter;
        this.leadTechnologie = leadTechnologie;
        this.standardisierterFertigungsprozess = standardisierterFertigungsprozess;
        this.erstanlaeufer = erstanlaufer;
        this.konzepte = konzepte;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public String getTteam() {
        return tteam;
    }

    public String getTteamLeiter() {
        return tteamLeiter;
    }

    public String getLeadTechnologie() {
        return leadTechnologie;
    }

    public String getStandardisierterFertigungsprozess() {
        return standardisierterFertigungsprozess;
    }

    public String getErstanlaeufer() {
        return erstanlaeufer;
    }

    public List<KonzeptDto> getKonzepte() {
        return konzepte;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public static final class Builder {

        private String beschreibung;
        private String tteam;
        private String tteamLeiter;
        private String leadTechnologie;
        private String standardisierterFertigungsprozess;
        private String erstanlaeufer;
        private List<KonzeptDto> konzepte = Collections.EMPTY_LIST;

        private Builder() {

        }

        public Builder withBeschreibung(String beschreibung) {
            this.beschreibung = beschreibung;
            return this;
        }

        public Builder withTteam(String tteam) {
            this.tteam = tteam;
            return this;
        }

        public Builder withTteamleiter(String tteamLeiter) {
            this.tteamLeiter = tteamLeiter;
            return this;
        }

        public Builder withLeadTechnologie(String leadTechnologie) {
            this.leadTechnologie = leadTechnologie;
            return this;
        }

        public Builder withStandardisierterFertigungsprozess(String standardisierterFertigungsprozess) {
            this.standardisierterFertigungsprozess = standardisierterFertigungsprozess;
            return this;
        }

        public Builder withErstanlaeufer(String erstanlaeufer) {
            this.erstanlaeufer = erstanlaeufer;
            return this;
        }

        public Builder withKonzepte(List<KonzeptDto> konzepte) {
            this.konzepte = konzepte;
            return this;
        }

        public ProzessbaukastenStammdatenViewData build() {
            return new ProzessbaukastenStammdatenViewData(beschreibung, tteam, tteamLeiter, leadTechnologie,
                    standardisierterFertigungsprozess, erstanlaeufer, konzepte);
        }

    }

    public static ProzessbaukastenStammdatenViewData forLeerProzessbaukasten() {
        ProzessbaukastenStammdatenViewData stammdaten = ProzessbaukastenStammdatenViewData.getBuilder()
                .withBeschreibung("")
                .withTteam("")
                .withTteamleiter("")
                .withLeadTechnologie("")
                .withStandardisierterFertigungsprozess("")
                .withErstanlaeufer("")
                .build();
        return stammdaten;
    }

}
