package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Anhang;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * This Converter should only be used to verify that a Anhang is not null!
 *
 * @author sl
 */
@FacesConverter("KonzeptAnhangConverter")
public class KonzeptAnhangConverter implements Converter<Anhang> {

    @Override
    public Anhang getAsObject(FacesContext context, UIComponent component, String value) {
        return value != null ? new Anhang() : null;
    }

    public Anhang getAsObject(FacesContext context, UIComponent component, String value, Anhang anhang) {
        return value != null ? new Anhang() : null;
    }

    public Anhang getAsObject(Anhang anhang) {
        return anhang.getDateiname() != null ? new Anhang() : null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Anhang value) {
        if (value != null) {
            if (value.getId() != null) {
                return value.getId().toString();
            } else {
                return "";
            }
        } else {
            return null;
        }
    }

}
