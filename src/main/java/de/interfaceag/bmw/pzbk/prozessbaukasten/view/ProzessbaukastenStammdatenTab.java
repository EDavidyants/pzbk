package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.prozessbaukasten.KonzeptDto;

import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface ProzessbaukastenStammdatenTab {

    String getBezeichnung();

    String getBeschreibung();

    String getTteam();

    String getTteamleiter();

    String getLeadTechnologie();

    String getStandardisierterFertigungsprozess();

    String getErstanlaeufer();

    List<KonzeptDto> getKonzepte();

}
