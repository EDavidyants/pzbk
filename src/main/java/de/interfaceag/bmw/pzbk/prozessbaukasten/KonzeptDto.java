package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Konzept;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * @author sl
 */
public class KonzeptDto implements Serializable, Comparable {

    private final Long id;
    private final String bezeichnung;
    private final String beschreibung;

    public KonzeptDto(Long id, String bezeichnung, String beschreibung) {
        this.id = id;
        this.bezeichnung = bezeichnung;
        this.beschreibung = beschreibung;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public String getBeschreibungFull() {
        return beschreibung;
    }

    public String getBeschreibungPreview() {
        if (beschreibung != null && beschreibung.length() > 60) {
            return beschreibung.substring(0, 59).concat("...");
        } else {
            return beschreibung;
        }
    }

    public Long getId() {
        return id;
    }

    public static List<KonzeptDto> forKonzepte(List<Konzept> konzepte) {
        List<KonzeptDto> result = new ArrayList<>();
        Iterator<Konzept> iterator = konzepte.iterator();
        while (iterator.hasNext()) {
            Konzept konzept = iterator.next();
            KonzeptDto newDto = new KonzeptDto(konzept.getId(), konzept.getBezeichnung(), konzept.getBeschreibung());
            result.add(newDto);
        }
        return result;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        KonzeptDto that = (KonzeptDto) object;
        return Objects.equals(id, that.id) &&
                Objects.equals(bezeichnung, that.bezeichnung) &&
                Objects.equals(beschreibung, that.beschreibung);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, bezeichnung, beschreibung);
    }

    @Override
    public int compareTo(Object other) {
        KonzeptDto otherKonzept = (KonzeptDto) other;
        return this.bezeichnung.compareToIgnoreCase(otherKonzept.bezeichnung);
    }

}
