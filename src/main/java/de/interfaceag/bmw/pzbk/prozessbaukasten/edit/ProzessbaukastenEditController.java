package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ActiveIndex;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenStatusChangeDialog;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenStatusChangeDialogData;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenAnhangTab;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenAnhangTabData;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.convert.Converter;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ViewScoped
@Named
public class ProzessbaukastenEditController implements ProzessbaukastenAnhangTab, ProzessbaukastenStatusChangeDialog, Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ProzessbaukastenEditController.class.getName());

    @Inject
    private ProzessbaukastenEditViewFacade facade;

    private ProzessbaukastenEditViewData viewData;

    private ActiveIndex activeIndex;

    @PostConstruct
    public void init() {
        initViewData();
        initActiveIndex();
    }

    private void initActiveIndex() {
        UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();
        activeIndex = ActiveIndex.forUrlParameter(urlParameter);
    }

    private void initViewData() {
        UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();
        try {
            viewData = facade.getViewData(urlParameter);
        } catch (IllegalArgumentException exception) {
            LOG.error("view data could not be loaded!", exception);
        }
    }

    public ProzessbaukastenEditViewData getViewData() {
        return viewData;
    }

    public ProzessbaukastenEditViewPermission getViewPermission() {
        return viewData.getViewPermission();
    }

    public void updateTteamleiter(SelectEvent event) {
        Tteam tteam = (Tteam) event.getObject();
        Optional<Mitarbeiter> tteamleiter = facade.getTteamleiterOfTteam(tteam);
        if (tteamleiter.isPresent()) {
            getViewData().setTteamLeiter(tteamleiter.get().toString());
        } else {
            getViewData().setTteamLeiter("");
        }
    }

    public String save() {
        return facade.save(viewData, activeIndex);
    }

    public String cancel() {
        return ProzessbaukastenEditViewFacade.cancel(viewData, activeIndex);
    }

    public List<Mitarbeiter> completeLeadTechnologie(String query) {
        return facade.completeLeadTechnologie(query);
    }

    public Converter getMitarbeiterConverter() {
        return facade.getMitarbeiterConverter();
    }

    @Override
    public ProzessbaukastenAnhangTabData getProzessbaukastenAnhangTabData() {
        return getViewData();
    }

    @Override
    public StreamedContent download(Anhang anhang) {
        return facade.downloadAnhang(anhang);
    }

    public ActiveIndex getActiveIndex() {
        return activeIndex;
    }

    @Override
    public String processStatusChange() {
        return facade.saveStatusChange(viewData);
    }

    @Override
    public ProzessbaukastenStatusChangeDialogData getDialogData() {
        return getViewData();
    }

}
