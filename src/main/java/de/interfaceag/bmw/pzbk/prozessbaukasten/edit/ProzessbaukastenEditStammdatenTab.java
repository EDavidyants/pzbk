package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.ProzessbaukastenDisabledAttributes;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required.ProzessbaukastenRequiredAttributes;

import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface ProzessbaukastenEditStammdatenTab {

    String getBezeichnung();

    void setBezeichnung(String bezeichnung);

    String getBeschreibung();

    void setBeschreibung(String beschreibung);

    Tteam getTteam();

    void setTteam(Tteam tteam);

    String getTteamleiter();

    Mitarbeiter getLeadTechnologie();

    void setLeadTechnologie(Mitarbeiter leadTechnologie);

    String getStandardisierterFertigungsprozess();

    void setStandardisierterFertigungsprozess(String standardisierterFertigungsprozess);

    Derivat getErstanlaeufer();

    void setErstanlaeufer(Derivat derivat);

    List<Tteam> getAllTteams();

    Converter getTteamConverter();

    List<Konzept> getKonzepte();

    void setKonzepte(List<Konzept> konzepte);

    KonzeptAnhangUploader getKonzeptAnhangUploader(Konzept konzept);

    KonzeptRemover getKonzeptRemover(Konzept konzept);

    KonzeptAnhangValidator getKonzeptAnhangValidator(Konzept konzept);

    void addNewKonzept();

    ProzessbaukastenDisabledAttributes getProzessbaukastenDisabledAttributes();

    ProzessbaukastenRequiredAttributes getRequiredAttributes();

    List<Derivat> getAllDerivate();

    Converter getDerivatConverter();

}
