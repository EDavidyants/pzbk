package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes;

/**
 *
 * @author sl
 */
public class ProzessbaukastenStatusBeauftragtDisabledAttributes extends ProzessbaukastenStatusAngelegtDisabledAttributes {

    private final boolean isUserAdmin;

    protected ProzessbaukastenStatusBeauftragtDisabledAttributes(boolean isUserAdmin) {
        this.isUserAdmin = isUserAdmin;
    }

    @Override
    public boolean isTteamDisabled() {
        return !isUserAdmin;
    }

    @Override
    public boolean isAnhangGenehmigungTPKDisabled() {
        return false;
    }

    @Override
    public boolean isErstanlaeuferDisabled() {
        return false;
    }

}
