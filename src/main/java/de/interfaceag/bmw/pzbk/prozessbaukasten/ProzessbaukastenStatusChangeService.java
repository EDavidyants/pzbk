package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungStatusChangeService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author evda
 */
@Stateless
@Named
public class ProzessbaukastenStatusChangeService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(ProzessbaukastenStatusChangeService.class);

    @Inject
    private ProzessbaukastenService prozessbaukastenService;
    @Inject
    private LocalizationService localizationService;
    @Inject
    private Session session;
    @Inject
    private AnforderungStatusChangeService anforderungStatusChangeService;

    public Prozessbaukasten processStatusChange(ProzessbaukastenStatus newStatus, Prozessbaukasten prozessbaukasten, String kommentar) {
        return processStatusChange(newStatus, prozessbaukasten, kommentar, session.getUser(), new Date());
    }

    public Prozessbaukasten processStatusChange(ProzessbaukastenStatus newStatus, Prozessbaukasten prozessbaukasten, String kommentar, Mitarbeiter mitarbeiter, Date datum) {
        if (isInputValid(prozessbaukasten, newStatus)) {
            ProzessbaukastenStatus oldStatus = prozessbaukasten.getStatus();
            return changeProzessbaukastenStatus(prozessbaukasten, newStatus, oldStatus, mitarbeiter, kommentar, datum);
        }

        return prozessbaukasten;
    }

    private Prozessbaukasten changeProzessbaukastenStatus(Prozessbaukasten prozessbaukasten, ProzessbaukastenStatus newStatus, ProzessbaukastenStatus oldStatus, Mitarbeiter mitarbeiter, String kommentar, Date datum) {
        prozessbaukasten.setStatus(newStatus);
        prozessbaukasten.setAenderungsdatum(datum);
        prozessbaukasten.setDatumStatuswechsel(datum);
        prozessbaukastenService.saveChangesToAttributAndWriteHistoryWithKommentar(prozessbaukasten, mitarbeiter, "status", oldStatus.getNameForHistory(), newStatus.getNameForHistory(), kommentar, datum);

        if (isNewStatusGueltig(newStatus)) {
            updateAllAnforderungenToStatusGueltigImProzessbaukasten(prozessbaukasten, mitarbeiter);
        } else if (isNewStatusStillgelegt(newStatus)) {
            resetAnforderungStatusIfNecessary(prozessbaukasten);
        }
        return prozessbaukasten;
    }

    private boolean isNewStatusGueltig(ProzessbaukastenStatus newStatus) {
        return newStatus == ProzessbaukastenStatus.GUELTIG;
    }

    private boolean isNewStatusStillgelegt(ProzessbaukastenStatus newStatus) {
        return newStatus == ProzessbaukastenStatus.STILLGELEGT;
    }

    private void resetAnforderungStatusIfNecessary(Prozessbaukasten prozessbaukasten) {

        List<Anforderung> anforderungen = prozessbaukasten.getAnforderungen();

        LOG.debug("Prozessbaukasten {} wurde stillgelegt -> update all Anforderungen!", prozessbaukasten);

        for (Anforderung anforderung : anforderungen) {
            if (anforderung.getStatus() == Status.A_GENEHMIGT_IN_GUELTIGEM_PROZESSBAUKASTEN) {
                anforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN, "Prozessbaukasten " + prozessbaukasten.toString() + " wurde stillgelegt.", session.getUser());
            }
        }

    }

    private void updateAllAnforderungenToStatusGueltigImProzessbaukasten(Prozessbaukasten prozessbaukasten, Mitarbeiter currentUser) {
        LOG.debug("Prozessbaukasten {} wurde gültig gesetzt -> update all Anforderungen!", prozessbaukasten);
        final List<Anforderung> anforderungen = prozessbaukasten.getAnforderungen();
        for (Anforderung anforderung : anforderungen) {
            anforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_GENEHMIGT_IN_GUELTIGEM_PROZESSBAUKASTEN, "Prozessbaukasten " + prozessbaukasten.toString() + " wurde gültig gesetzt.", currentUser);
        }
    }

    private boolean isInputValid(Prozessbaukasten prozessbaukasten, ProzessbaukastenStatus newStatus) {
        return prozessbaukasten != null && prozessbaukasten.getId() != null && newStatus != null;
    }

    public static boolean isStatusChangeKommentarValid(String kommentar) {
        kommentar = kommentar.trim();
        return !"".equals(kommentar);
    }

    public void showKommentarRequiredDialog(ProzessbaukastenStatus newStatus) {
        FacesContext context = FacesContext.getCurrentInstance();
        String messageTitle = localizationService.getValue("prozessbaukasten_view_statusWechselKommentarFehlt");
        String message = getStatusChangeGrowlMessageTitle(newStatus);
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, messageTitle, message));
    }

    private String getStatusChangeGrowlMessageTitle(ProzessbaukastenStatus newStatus) {
        StringBuilder sb = new StringBuilder();
        sb.append(localizationService.getValue("prozessbaukasten_view_beimWechselZumStatus")).append(" ");
        sb.append(localizationService.getValue(newStatus.getLocalizationKey())).append(" ");
        sb.append(localizationService.getValue("prozessbaukasten_view_istEinKommentarNotwendig")).append(".");
        String message = sb.toString();
        return message;
    }


}
