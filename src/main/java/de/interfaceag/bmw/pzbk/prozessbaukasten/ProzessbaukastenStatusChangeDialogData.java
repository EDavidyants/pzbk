package de.interfaceag.bmw.pzbk.prozessbaukasten;

/**
 *
 * @author sl
 */
public interface ProzessbaukastenStatusChangeDialogData {

    String getStatusChangeKommentar();

    void setStatusChangeKommentar(String statusChangeKommentar);

}
