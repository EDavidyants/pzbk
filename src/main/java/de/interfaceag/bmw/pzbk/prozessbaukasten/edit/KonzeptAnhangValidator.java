package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Konzept;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author sl
 */
public class KonzeptAnhangValidator {

    private final Konzept konzept;

    public KonzeptAnhangValidator(Konzept konzept) {
        this.konzept = konzept;
    }

    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        Anhang anhang = konzept.getAnhang();
        if (isAnhangMissing(anhang)) {
            showValidationMessage();
        }
    }

    private static boolean isAnhangMissing(Anhang anhang) {
        return anhang == null;
    }

    private static void showValidationMessage() throws ValidatorException {
        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "", ""));
    }

}
