package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes;

import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;

/**
 *
 * @author sl
 */
public final class ProzessbaukastenDisabledAttributesFactory {

    private ProzessbaukastenDisabledAttributesFactory() {
    }

    public static ProzessbaukastenDisabledAttributes getDisabledAttributesForNewProzessbaukasten() {
        return new ProzessbaukastenNeuDisabledAttributes();
    }

    public static ProzessbaukastenDisabledAttributes getDisabledAttributesForProzessbaukasten(
            ProzessbaukastenStatus status, boolean isUserAdmin) {
        switch (status) {
            case BEAUFTRAGT:
                return new ProzessbaukastenStatusBeauftragtDisabledAttributes(isUserAdmin);
            case GUELTIG:
                return new ProzessbaukastenStatusGultigDisabledAttributes(isUserAdmin);
            case ABGEBROCHEN:
            case GELOESCHT:
            case STILLGELEGT:
                return new ProzessbaukastenAllDisabledAttributes();
            case ANGELEGT:
                return new ProzessbaukastenStatusAngelegtDisabledAttributes();
            default:
                return new ProzessbaukastenNeuDisabledAttributes();
        }
    }

}
