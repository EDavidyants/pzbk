package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

/**
 *
 * @author sl
 */
public interface ProzessbaukastenDerivat {

    String getBezeichnung();

    String getStatus();

}
