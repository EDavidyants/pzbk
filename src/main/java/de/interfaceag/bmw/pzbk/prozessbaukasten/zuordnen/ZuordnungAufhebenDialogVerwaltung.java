package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import de.interfaceag.bmw.pzbk.prozessbaukasten.AnforderungenTabDTO;

/**
 *
 * @author evda
 */
public interface ZuordnungAufhebenDialogVerwaltung {

    void openZuordnungAufhebenDialog(AnforderungenTabDTO zugeordneteAnforderungToWithdraw);

    boolean isProzessbaukastenGueltig();

}
