package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author fn
 */
public class AnforderungenProzessbaukastenZuordnenDTO implements Serializable {

    private final String fachId;

    private final Integer version;

    private final String beschreibung;

    public AnforderungenProzessbaukastenZuordnenDTO(String fachId, Integer version, String beschreibung) {
        this.fachId = fachId;
        this.version = version;
        this.beschreibung = beschreibung;
    }

    public String getFachId() {
        return fachId;
    }

    public Integer getVersion() {
        return version;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public String getName() {
        return fachId + " | V" + version;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.fachId);
        hash = 59 * hash + Objects.hashCode(this.version);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnforderungenProzessbaukastenZuordnenDTO other = (AnforderungenProzessbaukastenZuordnenDTO) obj;
        if (!Objects.equals(this.fachId, other.fachId)) {
            return false;
        }
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        return true;
    }

}
