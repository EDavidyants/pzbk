package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungStatusChangeService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.history.ProzessbaukastenHistoryService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.AnforderungReportingStatusTransitionAdapter;
import de.interfaceag.bmw.pzbk.session.Current;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;

@Stateless
public class ProzessbaukastenRemoveAnforderungZuordnungService implements Serializable {

    @Inject
    private AnforderungReportingStatusTransitionAdapter anforderungReportingStatusTransitionAdapter;
    @Inject
    private AnforderungStatusChangeService anforderungStatusChangeSerivce;
    @Inject
    private ProzessbaukastenService prozessbaukastenService;
    @Inject
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;

    @Inject
    @Current
    private Mitarbeiter currentUser;

    public void unassignAnforderungFromProzessbaukasten(Anforderung anforderung, Prozessbaukasten prozessbaukasten, String kommentar) {
        unassignAnforderungFromProzessbaukasten(anforderung, prozessbaukasten);
        writeKommentarToHistory(prozessbaukasten, kommentar);
    }

    private void unassignAnforderungFromProzessbaukasten(Anforderung anforderung, Prozessbaukasten prozessbaukasten) {
        Prozessbaukasten prozessbaukastenInitial = prozessbaukastenService.createRealCopy(prozessbaukasten).orElse(null);
        updateAnforderungAndProzessbaukasten(anforderung, prozessbaukasten);
        addEntryToReportingLsDataStructure(anforderung);
        prozessbaukastenService.saveWithHistory(prozessbaukasten, prozessbaukastenInitial, currentUser, new Date());
    }

    private void updateAnforderungAndProzessbaukasten(Anforderung anforderung, Prozessbaukasten prozessbaukasten) {
        removeAnfordernugFromProzessbaukasten(anforderung, prozessbaukasten);
        removeProzessbaukastenFromAnforderung(anforderung, prozessbaukasten);
        if (anforderung.getProzessbaukasten().isEmpty()) {
            changeAnforderungStatusToInArbeit(anforderung);
        }
    }

    private void addEntryToReportingLsDataStructure(Anforderung anforderung) {
        anforderungReportingStatusTransitionAdapter.removeAnforderungFromProzessbaukasten(anforderung, anforderung.getStatus());
    }

    private void changeAnforderungStatusToInArbeit(Anforderung anforderung) {
        anforderungStatusChangeSerivce.changeAnforderungStatus(anforderung, Status.A_INARBEIT, "");
    }

    private static void removeProzessbaukastenFromAnforderung(Anforderung anforderung, Prozessbaukasten prozessbaukasten) {
        anforderung.getProzessbaukasten().remove(prozessbaukasten);
    }

    private static void removeAnfordernugFromProzessbaukasten(Anforderung anforderung, Prozessbaukasten prozessbaukasten) {
        prozessbaukasten.removeAnforderung(anforderung);
    }

    private void writeKommentarToHistory(Prozessbaukasten prozessbaukasten, String kommentar) {
        prozessbaukastenHistoryService.writeKommentarForChangedField(prozessbaukasten, currentUser, "Kommentar zum Aufheben der Zuordnung", kommentar);
    }

}
