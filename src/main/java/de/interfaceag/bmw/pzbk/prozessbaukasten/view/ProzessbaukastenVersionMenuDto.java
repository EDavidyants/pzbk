package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author evda
 */
public class ProzessbaukastenVersionMenuDto implements Serializable, Comparable<ProzessbaukastenVersionMenuDto> {

    private final String fachId;
    private final Integer version;
    private final Date erstellungsdatum;

    public ProzessbaukastenVersionMenuDto(String fachId, Integer version, Date erstellungsdatum) {
        this.fachId = fachId;
        this.version = version;
        if (erstellungsdatum != null) {
            this.erstellungsdatum = new Date(erstellungsdatum.getTime());
        } else {
            this.erstellungsdatum = null;
        }
    }

    public String getFachId() {
        return fachId;
    }

    public Integer getVersion() {
        return version;
    }

    public Date getErstellungsdatum() {
        if (this.erstellungsdatum != null) {
            return new Date(this.erstellungsdatum.getTime());
        } else {
            return null;
        }
    }

    public static List<ProzessbaukastenVersionMenuDto> buildFromCollection(Collection<Object[]> queryResultCollection) {
        List<ProzessbaukastenVersionMenuDto> result = new ArrayList<>();

        Iterator<Object[]> iterator = queryResultCollection.iterator();

        while (iterator.hasNext()) {
            Object[] record = iterator.next();
            String fachId = (String) record[0];
            Integer version = (Integer) record[1];
            Date erstellungsdatum = (Date) record[2];
            result.add(new ProzessbaukastenVersionMenuDto(fachId, version, erstellungsdatum));
        }

        Collections.sort(result);
        return result;
    }

    @Override
    public int compareTo(ProzessbaukastenVersionMenuDto other) {
        if (this.getVersion().equals(other.getVersion())) {
            return compareToByDate(other);
        } else {
            return this.getVersion() - other.getVersion();
        }
    }

    private int compareToByDate(ProzessbaukastenVersionMenuDto other) {
        Date date1 = this.getErstellungsdatum();
        Date date2 = other.getErstellungsdatum();
        if (date1.after(date2)) {
            return 1;
        } else if (date1.before(date2)) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.fachId);
        hash = 31 * hash + Objects.hashCode(this.version);
        hash = 31 * hash + Objects.hashCode(this.erstellungsdatum);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProzessbaukastenVersionMenuDto other = (ProzessbaukastenVersionMenuDto) obj;
        if (!Objects.equals(this.fachId, other.fachId)) {
            return false;
        }
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        if (!Objects.equals(this.erstellungsdatum, other.erstellungsdatum)) {
            return false;
        }
        return true;
    }

}
