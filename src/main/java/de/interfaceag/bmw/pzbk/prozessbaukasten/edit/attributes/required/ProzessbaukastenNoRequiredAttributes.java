package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required;

/**
 *
 * @author sl
 */
public class ProzessbaukastenNoRequiredAttributes implements ProzessbaukastenRequiredAttributes {

    protected ProzessbaukastenNoRequiredAttributes() {
    }

    @Override
    public boolean isBezeichnungRequired() {
        return false;
    }

    @Override
    public boolean isBeschreibungRequired() {
        return false;
    }

    @Override
    public boolean isTteamRequired() {
        return false;
    }

    @Override
    public boolean isLeadTechnologieRequired() {
        return false;
    }

    @Override
    public boolean isKonzeptRequired() {
        return false;
    }

    @Override
    public boolean isStandardisierterFertigungsprozessRequired() {
        return false;
    }

    @Override
    public boolean isErstanlaeuferRequired() {
        return false;
    }

    @Override
    public boolean isAnhangStartbriefRequired() {
        return false;
    }

    @Override
    public boolean isAnhangGrafikUmfangRequired() {
        return false;
    }

    @Override
    public boolean isAnhangGrafikKonzeptbaumRequired() {
        return false;
    }

    @Override
    public boolean isAnhangBeauftragungTPKRequired() {
        return false;
    }

    @Override
    public boolean isAnhangGenehmigungTPKRequired() {
        return false;
    }

    @Override
    public boolean isWeitereAnhangeRequired() {
        return false;
    }

    @Override
    public boolean isKonzeptBezeichnungRequired() {
        return false;
    }

    @Override
    public boolean isKonzeptBeschreibungRequired() {
        return false;
    }

    @Override
    public boolean isKonzeptAnhangRequired() {
        return false;
    }

}
