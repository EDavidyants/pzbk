package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import org.primefaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static de.interfaceag.bmw.pzbk.prozessbaukasten.AnhangValidator.isAnhangValidImage;
import static de.interfaceag.bmw.pzbk.prozessbaukasten.FileUploadToAnhangConverter.convertToAnhang;

/**
 *
 * @author sl
 */
public class KonzeptAnhangUploader {

    private static final Logger LOG = LoggerFactory.getLogger(KonzeptAnhangUploader.class.getName());

    private final Konzept konzept;

    public KonzeptAnhangUploader(Konzept konzept) {
        this.konzept = konzept;
    }

    public Konzept getKonzept() {
        return konzept;
    }

    public void uploadAnhang(FileUploadEvent event) {
        if (isAnhangValidImage(event)) {
            try {
                Anhang weiterenAnhang = convertToAnhang(event);
                konzept.setAnhang(weiterenAnhang);
            } catch (IOException ex) {
                LOG.error(null, ex);
            }
        }
    }

    public static List<KonzeptAnhangUploader> forKonzepte(List<Konzept> konzepte) {
        List<KonzeptAnhangUploader> result = new ArrayList<>();
        Iterator<Konzept> iterator = konzepte.iterator();
        while (iterator.hasNext()) {
            Konzept konzept = iterator.next();
            KonzeptAnhangUploader newDto = new KonzeptAnhangUploader(konzept);
            result.add(newDto);
        }
        return result;
    }

}
