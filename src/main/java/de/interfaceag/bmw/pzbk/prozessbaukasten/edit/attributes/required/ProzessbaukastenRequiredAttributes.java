package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required;

import java.io.Serializable;

/**
 *
 * @author sl
 */
public interface ProzessbaukastenRequiredAttributes extends Serializable {

    boolean isBezeichnungRequired();

    boolean isBeschreibungRequired();

    boolean isTteamRequired();

    boolean isLeadTechnologieRequired();

    boolean isKonzeptRequired();

    boolean isStandardisierterFertigungsprozessRequired();

    boolean isErstanlaeuferRequired();

    boolean isAnhangStartbriefRequired();

    boolean isAnhangGrafikUmfangRequired();

    boolean isAnhangGrafikKonzeptbaumRequired();

    boolean isAnhangBeauftragungTPKRequired();

    boolean isAnhangGenehmigungTPKRequired();

    boolean isWeitereAnhangeRequired();

    boolean isKonzeptBezeichnungRequired();

    boolean isKonzeptBeschreibungRequired();

    boolean isKonzeptAnhangRequired();

}
