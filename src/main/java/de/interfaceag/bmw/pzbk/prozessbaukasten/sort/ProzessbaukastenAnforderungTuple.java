package de.interfaceag.bmw.pzbk.prozessbaukasten.sort;

/**
 *
 * @author sl
 */
public interface ProzessbaukastenAnforderungTuple {

    long getProzessbaukastenId();

    long getAnforderungId();

}
