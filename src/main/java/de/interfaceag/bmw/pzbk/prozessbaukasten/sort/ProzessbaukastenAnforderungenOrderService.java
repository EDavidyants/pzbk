package de.interfaceag.bmw.pzbk.prozessbaukasten.sort;

import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author sl
 */
@Named
@Stateless
public class ProzessbaukastenAnforderungenOrderService implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public ProzessbaukastenAnforderungOrders getAnforderungenOrderForProzessbaukasten(Collection<Long> prozessbaukaestenIds) {
        List<Object[]> queryResult = getAnforderungenOrderQueryResultForProzessbaukasten(prozessbaukaestenIds);
        ProzessbaukastenAnforderungOrders result = convertToProzessbaukastenAnforderungOrders(queryResult);
        return result;
    }

    private static ProzessbaukastenAnforderungOrders convertToProzessbaukastenAnforderungOrders(List<Object[]> queryResults) {
        Map<ProzessbaukastenAnforderungTuple, ProzessbaukastenAnforderungOrder> result = new HashMap<>();

        Iterator<Object[]> iterator = queryResults.iterator();
        while (iterator.hasNext()) {
            Object[] queryResult = iterator.next();
            ProzessbaukastenAnforderungTuple prozessbaukastenAnforderungTuple = convertToProzessbaukastenAnforderungTuple(queryResult);

            ProzessbaukastenAnforderungOrder prozessbaukastenAnforderungOrder = convertToProzessbaukastenAnforderungOrder(queryResult, prozessbaukastenAnforderungTuple);
            result.put(prozessbaukastenAnforderungTuple, prozessbaukastenAnforderungOrder);
        }
        return new ProzessbaukastenAnforderungOrdersDto(result);
    }

    private static ProzessbaukastenAnforderungTuple convertToProzessbaukastenAnforderungTuple(Object[] queryResult) {
        Long prozessbaukastenId = (Long) queryResult[0];
        Long anforderungId = (Long) queryResult[1];

        return ProzessbaukastenAnforderungTupleBuilder
                .withAnforderungId(anforderungId)
                .withProzessbaukastenId(prozessbaukastenId)
                .build();
    }

    private static ProzessbaukastenAnforderungOrder convertToProzessbaukastenAnforderungOrder(Object[] queryResult, ProzessbaukastenAnforderungTuple prozessbaukastenAnforderungTuple) {
        Long position = (Long) queryResult[2];

        return ProzessbaukastenAnforderungOrderBuilder
                .withProzessbaukastenAnforderungTuple(prozessbaukastenAnforderungTuple)
                .withPosition(position).build();
    }

    private List<Object[]> getAnforderungenOrderQueryResultForProzessbaukasten(Collection<Long> prozessbaukaestenIds) {
        QueryPart queryPart = new QueryPartDTO("SELECT * from prozessbaukasten_anforderung");
        queryPart.append(" WHERE prozessbaukasten_id IN ").append(getProzessbaukastenIdsForQuery(prozessbaukaestenIds));
        queryPart.append(" ORDER BY prozessbaukasten_id, anforderungen_order");

        Query query = entityManager.createNativeQuery(queryPart.getQuery());
        List<Object[]> result = query.getResultList();
        return result;
    }

    private static String getProzessbaukastenIdsForQuery(Collection<Long> prozessbaukaestenIds) {
        return prozessbaukaestenIds.stream()
                .map(id -> id.toString())
                .collect(Collectors.joining(",", "(", ")"));
    }

}
