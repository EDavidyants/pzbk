package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;

/**
 *
 * @author evda
 */
public final class ProzessbaukastenStatusUebergang implements Serializable {

    private static final EnumMap<ProzessbaukastenStatus, List<ProzessbaukastenStatus>> STATUS_UEBERGANG_MAP = new EnumMap<>(ProzessbaukastenStatus.class);

    static {
        STATUS_UEBERGANG_MAP.put(ProzessbaukastenStatus.ANGELEGT, Arrays.asList(ProzessbaukastenStatus.BEAUFTRAGT, ProzessbaukastenStatus.ABGEBROCHEN));
        STATUS_UEBERGANG_MAP.put(ProzessbaukastenStatus.BEAUFTRAGT, Arrays.asList(ProzessbaukastenStatus.ABGEBROCHEN, ProzessbaukastenStatus.GUELTIG));
        STATUS_UEBERGANG_MAP.put(ProzessbaukastenStatus.ABGEBROCHEN, Collections.singletonList(ProzessbaukastenStatus.GELOESCHT));
        STATUS_UEBERGANG_MAP.put(ProzessbaukastenStatus.GUELTIG, Collections.singletonList(ProzessbaukastenStatus.STILLGELEGT));
    }

    private ProzessbaukastenStatusUebergang() {
    }

    public static List<ProzessbaukastenStatus> getNextStatus(ProzessbaukastenStatus status) {
        return STATUS_UEBERGANG_MAP.get(status);
    }

}
