package de.interfaceag.bmw.pzbk.prozessbaukasten.validator;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required.ProzessbaukastenRequiredAttributes;

import javax.faces.validator.ValidatorException;
import java.util.Locale;

import static de.interfaceag.bmw.pzbk.prozessbaukasten.validator.AnhangPresentValidator.validateAnhang;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class AnhangStartbriefValidator {

    private AnhangStartbriefValidator() {
    }

    public static void validate(Prozessbaukasten prozessbaukasten, Locale locale, ProzessbaukastenRequiredAttributes requiredAttributes) throws ValidatorException {
        boolean isRequired = requiredAttributes.isAnhangStartbriefRequired();
        if (isRequired) {
            Anhang startbrief = prozessbaukasten.getStartbrief();
            String errorMessage = getValidationErrorMessage(locale);
            validateAnhang(startbrief, errorMessage);
        }
    }

    private static String getValidationErrorMessage(Locale locale) {
        return LocalizationService.getValue(locale, "prozessbaukasten_startbrief_validaton_error_detail");
    }

}
