package de.interfaceag.bmw.pzbk.prozessbaukasten.konzepte;

import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.prozessbaukasten.dto.ProzessbaukastenKonzeptChangeDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.history.ProzessbaukastenHistoryService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Stateless
public class ProzessbaukastenKonzeptHistoryService implements Serializable {

    private static final String KONZEPT = "Konzept";
    @Inject
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;

    public void writeHistoryForKonzepte(Prozessbaukasten prozessbaukastenOld, Prozessbaukasten prozessbaukastenNew, Mitarbeiter user) {

        prozessbaukastenOld.getKonzepte().forEach(konzeptOld -> {
            Optional<Konzept> konzeptNewOptional = prozessbaukastenNew.getKonzepte().stream().filter(konzeptNew -> konzeptNew.getId() == konzeptOld.getId()).findAny();
            if (konzeptNewOptional.isPresent()) {
                checkAndWriteHistoryForChangedKonzept(prozessbaukastenNew, user, konzeptOld, konzeptNewOptional.get());
            } else {
                prozessbaukastenHistoryService.writeHistoryForChangedKonzeptFields(prozessbaukastenNew, user, KONZEPT, konzeptOld.getBezeichnung(), "zugeordnet", "entfernt");
            }
        });

        prozessbaukastenNew.getKonzepte().forEach(konzeptNew -> {
            Optional<Konzept> konzeptOldOptional = prozessbaukastenOld.getKonzepte().stream().filter(konzeptOld -> konzeptOld.getId() == konzeptNew.getId()).findAny();
            if (!konzeptOldOptional.isPresent()) {
                prozessbaukastenHistoryService.writeHistoryForChangedKonzeptFields(prozessbaukastenNew, user, KONZEPT, konzeptNew.getBezeichnung(), "neu", "zugeordnet");
            }
        });
    }

    private void checkAndWriteHistoryForChangedKonzept(Prozessbaukasten prozessbaukastenNew, Mitarbeiter user, Konzept konzeptOld, Konzept konzeptNew) {

        List<ProzessbaukastenKonzeptChangeDto> changedKonzeptAttribute = ProzessbaukastenKonzeptChangeUtils.getChangedKonzeptAttribute(konzeptNew, konzeptOld);
        if (!changedKonzeptAttribute.isEmpty()) {
            changedKonzeptAttribute.forEach(change
                    -> writeHistoryForKonzeptChange(prozessbaukastenNew, user, change)
            );

        }
    }

    private void writeHistoryForKonzeptChange(Prozessbaukasten prozessbaukastenNew, Mitarbeiter user, ProzessbaukastenKonzeptChangeDto change) {
        String konzeptBezeichnung = change.getKonzeptBezeichnung().concat(" ").concat(change.getFieldName());
        String oldFieldValue = change.getOldValue();
        String newFieldValue = change.getNewValue();

        prozessbaukastenHistoryService.writeHistoryForChangedKonzeptFields(prozessbaukastenNew, user, KONZEPT, konzeptBezeichnung, oldFieldValue, newFieldValue);
    }

}
