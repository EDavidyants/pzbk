package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes;

/**
 *
 * @author sl
 */
public class ProzessbaukastenStatusGultigDisabledAttributes extends ProzessbaukastenAllDisabledAttributes {

    private final boolean isUserAdmin;

    protected ProzessbaukastenStatusGultigDisabledAttributes(boolean isUserAdmin) {
        this.isUserAdmin = isUserAdmin;
    }

    @Override
    public boolean isTteamDisabled() {
        return !isUserAdmin;
    }

}
