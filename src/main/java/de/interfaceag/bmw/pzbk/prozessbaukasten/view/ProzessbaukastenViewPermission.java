package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamMitgliedBerechtigung;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.ProzessbaukastenFalsePermission;
import de.interfaceag.bmw.pzbk.permissions.ProzessbaukastenRolePermission;
import de.interfaceag.bmw.pzbk.permissions.ProzessbaukastenTteamRolePermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ProzessbaukastenViewPermission implements Serializable {

    private final ViewPermission page;

    private final EditPermission statusChangeButton;
    private final EditPermission newVersionButton;
    private final ProzessbaukastenRolePermission newAnforderungButton;
    private final ProzessbaukastenRolePermission addAnforderungButton;
    private final ProzessbaukastenRolePermission editButton;

    private ProzessbaukastenViewPermission(Set<Rolle> userRoles,
            Prozessbaukasten prozessbaukasten, Collection<Long> tteamIdsWithWritePermissionsAsTteamleiter,
            TteamMitgliedBerechtigung tteamMitgliedBerechtigung, Collection<Long> tteamIdsWithWritePermissonsAsTteamVertrter) {

        Collection<Long> tteamIdsWithWritePermission = new HashSet<>();
        tteamIdsWithWritePermission.addAll(tteamIdsWithWritePermissionsAsTteamleiter);
        tteamIdsWithWritePermission.addAll(tteamMitgliedBerechtigung.getTteamIdsMitSchreibrechten());
        tteamIdsWithWritePermission.addAll(tteamIdsWithWritePermissonsAsTteamVertrter);

        Collection<Long> tteamIdsWithReadPermission = new HashSet<>();
        tteamIdsWithReadPermission.addAll(tteamMitgliedBerechtigung.getTteamIdsMitLeserechten());
        tteamIdsWithReadPermission.addAll(tteamMitgliedBerechtigung.getTteamIdsMitSchreibrechten());

        RolePermission permissionToRead = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN)
                .authorizeWriteForRole(Rolle.T_TEAMLEITER)
                .authorizeWriteForRole(Rolle.TTEAM_VERTRETER)
                .authorizeWriteForRole(Rolle.TTEAMMITGLIED)
                .authorizeWriteForRole(Rolle.ANFORDERER)
                .compareTo(userRoles)
                .get();
        page = getViewPermissionForPage(userRoles, prozessbaukasten, tteamIdsWithReadPermission, permissionToRead);

        RolePermission permissionToWrite = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN)
                .compareTo(userRoles)
                .get();

        Collection<Long> tteamsIdsForStatusChangeAndNewVersion = new HashSet<>();
        tteamsIdsForStatusChangeAndNewVersion.addAll(tteamIdsWithWritePermissionsAsTteamleiter);
        tteamsIdsForStatusChangeAndNewVersion.addAll(tteamIdsWithWritePermissonsAsTteamVertrter);

        statusChangeButton = getEditButtonViewPermission(prozessbaukasten, userRoles, tteamsIdsForStatusChangeAndNewVersion, permissionToWrite);
        newVersionButton = getEditButtonViewPermission(prozessbaukasten, userRoles, tteamsIdsForStatusChangeAndNewVersion, permissionToWrite);

        RolePermission permissionToWriteWithTTeam = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN)
                .compareTo(userRoles)
                .get();
        newAnforderungButton = getEditButtonForTteamViewPermission(prozessbaukasten, userRoles, tteamIdsWithWritePermission, permissionToWriteWithTTeam);
        editButton = getEditButtonForTteamViewPermission(prozessbaukasten, userRoles, tteamIdsWithWritePermission, permissionToWriteWithTTeam);
        addAnforderungButton = getEditButtonForTteamViewPermission(prozessbaukasten, userRoles, tteamIdsWithWritePermission, permissionToWriteWithTTeam);
    }

    private EditPermission getEditButtonViewPermission(Prozessbaukasten prozessbaukasten,
            Collection<Rolle> userRoles, Collection<Long> tteamIdsWithWritePermissionsAsTteamleiter, RolePermission rolePermission) {
        return ProzessbaukastenTteamRolePermission.forProzessbaukasten(prozessbaukasten)
                .forUserRolesWithPermissions(userRoles)
                .forTteamIdsWithWritePermissions(tteamIdsWithWritePermissionsAsTteamleiter)
                .withRolePermission(rolePermission)
                .build();
    }

    private ProzessbaukastenRolePermission getEditButtonForTteamViewPermission(Prozessbaukasten prozessbaukasten,
            Collection<Rolle> userRoles, Collection<Long> tteamIdsWithWritePermissions, RolePermission rolePermission) {

        Collection<ProzessbaukastenStatus> invalidStatus = Arrays.asList(ProzessbaukastenStatus.GUELTIG,
                ProzessbaukastenStatus.ABGEBROCHEN, ProzessbaukastenStatus.GELOESCHT, ProzessbaukastenStatus.STILLGELEGT);

        if (invalidStatus.contains(prozessbaukasten.getStatus())) {
            return ProzessbaukastenTteamRolePermission.getFalsePermission();
        }

        return ProzessbaukastenTteamRolePermission.forProzessbaukasten(prozessbaukasten)
                .forUserRolesWithPermissions(userRoles)
                .forTteamIdsWithWritePermissions(tteamIdsWithWritePermissions)
                .withRolePermission(rolePermission)
                .build();
    }

    private ViewPermission getViewPermissionForPage(Set<Rolle> userRoles,
            Prozessbaukasten prozessbaukasten, Collection<Long> tteamIdsWithReadePermissions, RolePermission rolePermission) {

        return ProzessbaukastenTteamRolePermission.forProzessbaukasten(prozessbaukasten)
                .forUserRolesWithPermissions(userRoles)
                .forTteamIdsWithReadPermissions(tteamIdsWithReadePermissions)
                .withRolePermission(rolePermission)
                .build();
    }

    private ProzessbaukastenViewPermission() {
        page = new FalsePermission();
        statusChangeButton = new FalsePermission();
        newVersionButton = new FalsePermission();
        newAnforderungButton = new ProzessbaukastenFalsePermission();
        addAnforderungButton = new ProzessbaukastenFalsePermission();
        editButton = new ProzessbaukastenFalsePermission();
    }

    public static ProzessbaukastenViewPermission denied() {
        return new ProzessbaukastenViewPermission();
    }

    public boolean getPage() {
        return page.isRendered();
    }

    public boolean getStatusChangeButton() {
        return statusChangeButton.hasRightToEdit();
    }

    public boolean getNewVersionButton() {
        return newVersionButton.hasRightToEdit();
    }

    public boolean getNewAnforderungButton() {
        return newAnforderungButton.hasRightToEditWithTteamMitglied();
    }

    public boolean getAddAnforderungButton() {
        return addAnforderungButton.hasRightToEditWithTteamMitglied();
    }

    public boolean getEditButton() {
        return editButton.hasRightToEditWithTteamMitglied();
    }

    public static Builder forProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        return new Builder().forProzessbaukasten(prozessbaukasten);
    }

    public static final class Builder {

        private Prozessbaukasten prozessbaukasten;
        private Set<Rolle> userRoles;
        private Collection<Long> tteamIdsWithWritePermissionsAsTteamleiter;
        private TteamMitgliedBerechtigung tteamMitgliedBerechtigung;
        private Collection<Long> tteamIdsWithWritePermissonsAsTteamVertrter;

        private Builder() {
        }

        private Builder forProzessbaukasten(Prozessbaukasten prozessbaukasten) {
            this.prozessbaukasten = prozessbaukasten;
            return this;
        }

        public Builder forUserRoles(Set<Rolle> userRoles) {
            this.userRoles = userRoles;
            return this;
        }

        public Builder forTteamIdsAsTteamleiter(Collection<Long> tteamIdsWithWritePermissionsAsTteamleiter) {
            this.tteamIdsWithWritePermissionsAsTteamleiter = tteamIdsWithWritePermissionsAsTteamleiter;
            return this;
        }

        public Builder forTteamMitgliedBerechtigung(TteamMitgliedBerechtigung tteamMitgliedBerechtigung) {
            this.tteamMitgliedBerechtigung = tteamMitgliedBerechtigung;
            return this;
        }

        public Builder forTteamIdsAsTteamVertreter(Collection<Long> tteamIdsWithWritePermissonsAsTteamVertrter) {
            this.tteamIdsWithWritePermissonsAsTteamVertrter = tteamIdsWithWritePermissonsAsTteamVertrter;
            return this;
        }

        public ProzessbaukastenViewPermission build() {
            return new ProzessbaukastenViewPermission(userRoles, prozessbaukasten,
                    tteamIdsWithWritePermissionsAsTteamleiter, tteamMitgliedBerechtigung,
                    tteamIdsWithWritePermissonsAsTteamVertrter);
        }

    }
}
