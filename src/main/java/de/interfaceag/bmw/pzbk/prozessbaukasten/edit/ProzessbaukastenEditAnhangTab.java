package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import org.primefaces.event.FileUploadEvent;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface ProzessbaukastenEditAnhangTab {

    void uploadStartbrief(FileUploadEvent event);

    void uploadGrafikUmfang(FileUploadEvent event);

    void uploadKonzeptbaum(FileUploadEvent event);

    void uploadBeauftragungTPK(FileUploadEvent event);

    void uploadGenehmigungTPK(FileUploadEvent event);

    void uploadWeiterenAnhang(FileUploadEvent event);

    void removeStartbrief();

    void removeGrafikUmfang();

    void removeKonzeptbaum();

    void removeBeauftragungTPK();

    void removeGenehmigungTPK();

    void removeWeiterenAnhang(Anhang anhang);

    void validateStartbrief(FacesContext fc, UIComponent uic, Object object) throws ValidatorException;

    void validateGrafikUmfang(FacesContext fc, UIComponent uic, Object object) throws ValidatorException;

    void validateKonzeptbaum(FacesContext fc, UIComponent uic, Object object) throws ValidatorException;

    void validateBeauftragungTPK(FacesContext fc, UIComponent uic, Object object) throws ValidatorException;

    void validateGenehmigungTPK(FacesContext fc, UIComponent uic, Object object) throws ValidatorException;

}
