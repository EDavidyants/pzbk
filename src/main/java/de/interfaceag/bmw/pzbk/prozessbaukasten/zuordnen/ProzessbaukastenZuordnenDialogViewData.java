package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author fn
 */
public class ProzessbaukastenZuordnenDialogViewData implements Serializable, ProzessbaukastenZuordnenDialogFields {

    private List<ProzessbaukastenZuordnenDTO> choosableProzessbaukasten;
    private ProzessbaukastenZuordnenDTO zugeordneterProzessbaukasten;
    private List<ProzessbaukastenZuordnenDTO> filteredProzessbaukasten;

    public static ProzessbaukastenZuordnenDialogViewData empty() {
        return new ProzessbaukastenZuordnenDialogViewData();
    }

    public ProzessbaukastenZuordnenDialogViewData() {
        this.choosableProzessbaukasten = new ArrayList<>();
        this.zugeordneterProzessbaukasten = null;

    }

    @Override
    public List<ProzessbaukastenZuordnenDTO> getProzessbaukastenChoosable() {
        return choosableProzessbaukasten;
    }

    @Override
    public void setProzessbaukastenChoosable(List<ProzessbaukastenZuordnenDTO> prozessbaukastenChoosable) {
        this.choosableProzessbaukasten = prozessbaukastenChoosable;
    }

    @Override
    public void addProzessbaukasten(ProzessbaukastenZuordnenDTO prozessbaukasten) {
        if (prozessbaukasten != null) {
            this.zugeordneterProzessbaukasten = prozessbaukasten;
        }
    }

    @Override
    public void removeProzessbaukasten() {
        if (getZugeordneterProzessbaukastenForView().isPresent()) {
            zugeordneterProzessbaukasten = null;
        }
    }

    @Override
    public Optional<ProzessbaukastenZuordnenDTO> getZugeordneterProzessbaukastenForView() {
        return Optional.ofNullable(zugeordneterProzessbaukasten);
    }

    @Override
    public boolean zugeordneterProzessbaukastenIsPresent() {
        return getZugeordneterProzessbaukastenForView().isPresent();
    }

    @Override
    public List<ProzessbaukastenZuordnenDTO> getFilteredProzessbaukasten() {
        return filteredProzessbaukasten;
    }

    @Override
    public void setFilteredProzessbaukasten(List<ProzessbaukastenZuordnenDTO> filteredProzessbaukasten) {
        this.filteredProzessbaukasten = filteredProzessbaukasten;
    }

    @Override
    public String getNameForView() {
        if (getZugeordneterProzessbaukastenForView().isPresent()) {
            return getZugeordneterProzessbaukastenForView().get().getNameForView();
        }
        return "";
    }

    @Override
    public String getBeschreibung() {
        if (getZugeordneterProzessbaukastenForView().isPresent()) {
            return getZugeordneterProzessbaukastenForView().get().getBeschreibung();
        }
        return "";
    }

}
