package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes;

/**
 *
 * @author sl
 */
public class ProzessbaukastenStatusAngelegtDisabledAttributes extends ProzessbaukastenNeuDisabledAttributes {

    protected ProzessbaukastenStatusAngelegtDisabledAttributes() {
    }

    @Override
    public boolean isKonzeptDisabled() {
        return false;
    }

    @Override
    public boolean isStandardisierterFertigungsprozessDisabled() {
        return false;
    }

    @Override
    public boolean isErstanlaeuferDisabled() {
        return false;
    }

    @Override
    public boolean isAnhangStartbriefDisabled() {
        return false;
    }

    @Override
    public boolean isAnhangGrafikUmfangDisabled() {
        return false;
    }

    @Override
    public boolean isAnhangGrafikKonzeptbaumDisabled() {
        return false;
    }

    @Override
    public boolean isAnhangBeauftragungTPKDisabled() {
        return false;
    }

    @Override
    public boolean isWeitereAnhangeDisabled() {
        return false;
    }

    @Override
    public boolean isAnhangeTabDisabled() {
        return false;
    }

    @Override
    public boolean isAnforderungenTabDisabled() {
        return false;
    }

    @Override
    public boolean isStammdatenDetailsPanelDisabled() {
        return false;
    }

}
