package de.interfaceag.bmw.pzbk.prozessbaukasten.dto;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenDerivat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author sl
 */
public class ProzessbaukastenDerivatDto implements ProzessbaukastenDerivat {

    private final String bezeichnung;
    private final String status;

    public ProzessbaukastenDerivatDto(String bezeichnung, String status) {
        this.bezeichnung = bezeichnung;
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.bezeichnung);
        hash = 97 * hash + Objects.hashCode(this.status);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProzessbaukastenDerivatDto other = (ProzessbaukastenDerivatDto) obj;
        if (!Objects.equals(this.bezeichnung, other.bezeichnung)) {
            return false;
        }
        if (!Objects.equals(this.status, other.status)) {
            return false;
        }

        return true;
    }

    @Override
    public String getBezeichnung() {
        return bezeichnung;
    }

    @Override
    public String getStatus() {
        return status;
    }

    public static List<ProzessbaukastenDerivat> forDerivate(List<Derivat> derivate, DerivatStatusNames derivatStatusNames) {
        Iterator<Derivat> iterator = derivate.iterator();
        List<ProzessbaukastenDerivat> result = new ArrayList<>();
        while (iterator.hasNext()) {
            Derivat derivat = iterator.next();
            ProzessbaukastenDerivat newProzessbaukastenDerivat = forDerivat(derivat, derivatStatusNames);
            result.add(newProzessbaukastenDerivat);
        }
        return result;
    }

    public static ProzessbaukastenDerivat forDerivat(Derivat derivat, DerivatStatusNames derivatStatusNames) {
        String bezeichnung = derivat.getName();
        DerivatStatus status = derivat.getStatus();
        String statusLabel = derivatStatusNames.getNameForStatus(status);
        return new ProzessbaukastenDerivatDto(bezeichnung, statusLabel);
    }

    public static ProzessbaukastenDerivat empty() {
        return new ProzessbaukastenDerivatDto("", "");
    }

}
