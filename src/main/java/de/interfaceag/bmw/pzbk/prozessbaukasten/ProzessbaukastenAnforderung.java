package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.enums.Status;

import java.io.Serializable;

/**
 *
 * @author sl
 */
public interface ProzessbaukastenAnforderung extends Serializable {

    String getBeschreibung();

    String getBeschreibungShort();

    String getFachId();

    String getFachIdAndVersion();

    String getId();

    String getKonzept();

    String getThemenklammer();

    String getSensorCoc();

    String getStatus();

    Status getStatusValue();

    String getUmsetzer();

    String getVersion();

}
