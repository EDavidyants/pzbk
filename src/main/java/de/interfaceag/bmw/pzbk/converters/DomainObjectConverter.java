package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.shared.objectIds.DomainObject;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.Collections;
import java.util.List;

public class DomainObjectConverter<T extends DomainObject> implements Converter<T> {

    private final List<T> domainObjects;

    public DomainObjectConverter(List<T> domainObjects) {
        if (domainObjects != null) {
            this.domainObjects = domainObjects;
        } else {
            this.domainObjects = Collections.emptyList();
        }
    }

    @Override
    public T getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty() || !RegexUtils.matchesId(value)) {
            return null;
        } else {
            long id = Long.parseLong(value);
            return domainObjects.stream().filter(domainObject -> domainObject.getId() == id).findAny().orElse(null);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, T domainObject) {
        if (domainObject == null) {
            return "";
        } else {
            return Long.toString(domainObject.getId());
        }
    }

}
