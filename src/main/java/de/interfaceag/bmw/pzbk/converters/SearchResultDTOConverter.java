package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.globalsearch.SearchResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author sl
 */
@FacesConverter("searchResultDtoConverter")
public class SearchResultDTOConverter implements Converter {

    private static final Logger LOG = LoggerFactory.getLogger(SearchResultDTOConverter.class.getName());

    public SearchResultDTOConverter() {
    }

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && value.trim().length() > 0) {
            try {
                if (value.contains(";Beschreibung")) {
                    String name = value.substring(0, value.indexOf(';'));
                    value = value.substring(value.indexOf(';') + 1);
                    String version = value.substring(0, value.indexOf(';'));
                    value = value.substring(value.indexOf(';') + 1);
                    String id = value.substring(0, value.indexOf(';'));
                    value = value.substring(value.indexOf(";Beschreibung") + 1);
                    String beschreibung = value;
                    SearchResultDTO result = new SearchResultDTO(name);
                    result.setVersion(version);
                    result.setId(Long.parseLong(id));
                    result.setBeschreibung(beschreibung);
                    return result;
                } else if (value.contains(";")) {
                    String name = value.substring(0, value.indexOf(';'));
                    value = value.substring(value.indexOf(';') + 1);
                    String version = value.substring(0, value.indexOf(';'));
                    value = value.substring(value.indexOf(';') + 1);
                    String id = value;
                    SearchResultDTO result = new SearchResultDTO(name);
                    result.setVersion(version);
                    result.setId(Long.parseLong(id));
                    return result;
                } else if (value.contains("_id")) {
                    String name = value.substring(0, value.indexOf("_id"));
                    value = value.substring(value.indexOf("_id") + 3);
                    String id = value;
                    SearchResultDTO result = new SearchResultDTO(name);
                    result.setId(Long.parseLong(id));
                    return result;
                } else {
                    return new SearchResultDTO(value);
                }
            } catch (NumberFormatException exception) {
                LOG.error(exception.getMessage(), exception);
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid SearchResultDTO."));
            }
        }

        return null;

    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object instanceof SearchResultDTO) {
            SearchResultDTO searchResultDTO = (SearchResultDTO) object;
            if (searchResultDTO.getBeschreibung() != null) {
                return searchResultDTO.getName() + ";" + searchResultDTO.getVersion() + ";" + searchResultDTO.getId() + ";Beschreibung " + searchResultDTO.getBeschreibung();
            } else if (searchResultDTO.getVersion() != null) {
                return searchResultDTO.getName() + ";" + searchResultDTO.getVersion() + ";" + searchResultDTO.getId();
            } else if (searchResultDTO.getId() != null) {
                return searchResultDTO.getName() + "_id" + searchResultDTO.getId();
            } else {
                return searchResultDTO.getName();
            }
        } else {
            return null;
        }
    }
}
