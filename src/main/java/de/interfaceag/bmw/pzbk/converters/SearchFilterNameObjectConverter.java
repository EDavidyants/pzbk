package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.filter.SearchFilterNameObject;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class SearchFilterNameObjectConverter implements Converter {

    private final List<SearchFilterNameObject> allSearchFilterObjects;

    public SearchFilterNameObjectConverter(List<SearchFilterNameObject> allSearchFilterObjects) {
        this.allSearchFilterObjects = allSearchFilterObjects;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return allSearchFilterObjects.stream()
                .filter(t -> t.getName().equals(value))
                .findAny().orElse(null);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof SearchFilterNameObject) {
            SearchFilterNameObject searchFilterNameObject = (SearchFilterNameObject) value;
            return searchFilterNameObject.getName();
        }
        return "";
    }

}
