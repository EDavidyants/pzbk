package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.dialog.viewdata.ModulKomponenteDto;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author fn
 */
public class ModulKomponenteDtoConverter implements Converter {

    private final List<ModulKomponenteDto> allKomponenten;

    public ModulKomponenteDtoConverter(List<ModulKomponenteDto> allKomponenten) {
        this.allKomponenten = allKomponenten;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return allKomponenten.stream().filter(modulKomponente -> modulKomponente.getPpg().equals(value)).findAny().orElse(null);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof ModulKomponenteDto) {
            ModulKomponenteDto modulKomponente = (ModulKomponenteDto) value;
            return modulKomponente.getPpg();
        }
        return "";
    }

}
