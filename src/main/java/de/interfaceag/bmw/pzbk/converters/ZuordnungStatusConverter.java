package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import static de.interfaceag.bmw.pzbk.shared.utils.RegexUtils.matchesId;

/**
 * @author sl
 */
@FacesConverter("zuordnungStatusConverter")
public class ZuordnungStatusConverter implements Converter<ZuordnungStatus> {

    @Override
    public ZuordnungStatus getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (matchesId(value)) {
            final int id = Integer.parseInt(value);
            return ZuordnungStatus.getById(id).orElse(null);
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, ZuordnungStatus status) {
        if (status == null) {
            return "";
        } else {
            return Integer.toString(status.getId());
        }
    }

}
