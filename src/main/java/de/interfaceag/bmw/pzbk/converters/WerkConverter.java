package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.entities.Werk;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author fn
 */
public class WerkConverter implements Converter {

    List<Werk> allWerk;

    public WerkConverter(List<Werk> allWerk) {
        this.allWerk = allWerk;
    }

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        return allWerk.stream().filter(t -> t.getName().equals(value)).findAny().orElse(null);
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object instanceof Werk) {
            return String.valueOf(((Werk) object).getName());
        }
        return null;
    }
}
