package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.enums.Phasenbezug;
import de.interfaceag.bmw.pzbk.shared.utils.PhasenbezugUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@FacesConverter("phasenbezugDtoConverter")
public class PhasenbezugConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return PhasenbezugUtils.getPhasenbezugForString(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Phasenbezug) {
            return ((Phasenbezug) value).getBeschreibung();
        }
        return "";
    }
}
