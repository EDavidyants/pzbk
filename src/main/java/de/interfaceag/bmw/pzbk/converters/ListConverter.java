package de.interfaceag.bmw.pzbk.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@FacesConverter("ListValidationConverter")
public class ListConverter implements Converter<List> {

    @Override
    public List getAsObject(FacesContext context, UIComponent component, String value) {
        if (Objects.nonNull(value) && !value.isEmpty()) {
            return Collections.singletonList("");
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, List list) {
        if (Objects.nonNull(list) && !list.isEmpty()) {
            return list.toString();
        } else {
            return null;
        }
    }
}
