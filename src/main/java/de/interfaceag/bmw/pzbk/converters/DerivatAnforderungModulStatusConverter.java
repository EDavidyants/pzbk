package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author sl
 */
@FacesConverter("derivatAnforderungModulStatusConverter")
public class DerivatAnforderungModulStatusConverter implements Converter {

    public DerivatAnforderungModulStatusConverter() {

    }

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && value.trim().length() > 0) {
            return DerivatAnforderungModulStatus.getStatusByBezeichnung(value);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object instanceof DerivatAnforderungModulStatus) {
            return object.toString();
        } else {
            return null;
        }
    }

}
