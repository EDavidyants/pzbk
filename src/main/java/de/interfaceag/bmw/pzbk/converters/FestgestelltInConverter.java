package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author sl (Stefan.Luchs@Interface-ag.de)
 */
public class FestgestelltInConverter implements Converter {

    List<FestgestelltIn> allFestgestelltIn;

    public FestgestelltInConverter(List<FestgestelltIn> allFestgestelltIn) {
        this.allFestgestelltIn = allFestgestelltIn;
    }

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        Optional<FestgestelltIn> result = allFestgestelltIn.stream().filter(t -> t.getWert().equals(value)).findAny();
        return result.isPresent() ? result.get() : null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object instanceof FestgestelltIn) {
            return String.valueOf(((FestgestelltIn) object).getWert());
        }
        return null;
    }
}
