package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.entities.Modul;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author Christian Schauer
 */
public class ModulConverter implements Converter {

    private final List<Modul> module;

    public ModulConverter(List<Modul> module) {
        this.module = module;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return module.stream().filter(m -> m.getName().equals(value)).findAny().orElse(null);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Modul) {
            Modul m = (Modul) value;
            return m.getName();
        }
        return null;
    }

}
