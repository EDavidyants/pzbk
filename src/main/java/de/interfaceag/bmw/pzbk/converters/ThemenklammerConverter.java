package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.Collections;
import java.util.List;

public class ThemenklammerConverter implements Converter<ThemenklammerDto> {

    private final List<ThemenklammerDto> themenklammern;

    public ThemenklammerConverter(List<ThemenklammerDto> themenklammern) {
        if (themenklammern != null) {
            this.themenklammern = themenklammern;
        } else {
            this.themenklammern = Collections.emptyList();
        }
    }

    @Override
    public ThemenklammerDto getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty() || !RegexUtils.matchesId(value)) {
            return null;
        } else {
            long id = Long.parseLong(value);
            return themenklammern.stream().filter(themenklammerDto -> themenklammerDto.getId() == id).findAny().orElse(null);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, ThemenklammerDto themenklammerDto) {
        if (themenklammerDto == null) {
            return "";
        } else {
            return Long.toString(themenklammerDto.getId());
        }
    }

}
