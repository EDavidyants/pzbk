package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import org.primefaces.convert.ClientConverter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.Map;

/**
 *
 * @author evda
 */
@FacesConverter("vereinbarungTypeConverter")
public class VereinbarungTypeConverter implements Converter, ClientConverter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        boolean forZak = Boolean.parseBoolean(value);
        VereinbarungType vt = VereinbarungType.getVereinbarungTypeForZAK(forZak);
        return vt;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof VereinbarungType) {
            VereinbarungType vt = (VereinbarungType) value;
            return vt.toString();
        } else {
            return "";
        }
    }

    @Override
    public Map<String, Object> getMetadata() {
        return null;
    }

    @Override
    public String getConverterId() {
        return "vereinbarungTypeConverter";
    }

} // end of class
