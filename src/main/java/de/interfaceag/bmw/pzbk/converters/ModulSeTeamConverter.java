package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author Christian Schauer
 */
public class ModulSeTeamConverter implements Converter {

    private final List<ModulSeTeam> modulSeTeams;

    public ModulSeTeamConverter(List<ModulSeTeam> modulSeTeams) {
        this.modulSeTeams = modulSeTeams;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return modulSeTeams.stream().filter(m -> m.getName().equals(value)).findAny().orElse(null);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof ModulSeTeam) {
            ModulSeTeam se = (ModulSeTeam) value;
            return se.getName();
        }
        return "";
    }
}
