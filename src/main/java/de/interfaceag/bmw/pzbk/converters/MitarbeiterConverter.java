package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Christian Schauer
 */
public class MitarbeiterConverter implements Converter {

    private final UserSearchService uss;
    private final List<Mitarbeiter> mitarbeiter;

    public MitarbeiterConverter(UserSearchService uss) {
        this.uss = uss;
        this.mitarbeiter = null;
    }

    public MitarbeiterConverter(List<Mitarbeiter> mitarbeiter) {
        this.uss = null;
        this.mitarbeiter = mitarbeiter;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && value.trim().length() >= 1) {
            if (mitarbeiter != null) {
                Optional result = mitarbeiter.stream().filter(m -> m.getId().equals(Long.parseLong(value))).findFirst();
                return result.isPresent() ? result.get() : null;
            }

            if (RegexUtils.matchesId(value)) {
                return uss.getUniqueMitarbeiterById(Long.parseLong(value));
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Mitarbeiter) {
            Mitarbeiter user = (Mitarbeiter) value;
            return user.getId().toString();
        } else {
            return null;
        }
    }
}
