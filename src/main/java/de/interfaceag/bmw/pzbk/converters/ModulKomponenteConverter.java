package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.services.ModulService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

/**
 * @author Christian Schauer
 */
public class ModulKomponenteConverter implements Converter {

    private ModulService modulService;

    private List<ModulKomponente> allKomponenten;

    public ModulKomponenteConverter(ModulService modulService) {
        this.modulService = modulService;
    }

    public ModulKomponenteConverter(List<ModulKomponente> allKomponenten) {
        this.allKomponenten = allKomponenten;
    }

    private String createName(ModulKomponente mk) {
        return mk.getName() + "," + mk.getPpg();
    }

    private String extractName(String input) {
        int pos = input.lastIndexOf(',');
        if (pos > 0) {
            return input.substring(0, pos).trim();
        } else {
            return input;
        }
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        List<ModulKomponente> mks;

        if (allKomponenten == null) {
            mks = modulService.getModulKomponenteByName(extractName(value));
        } else {
            mks = allKomponenten;
        }
        if (mks != null && !mks.isEmpty()) {
            return mks.get(0);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof ModulKomponente) {
            ModulKomponente mk = (ModulKomponente) value;
            return createName(mk);
        }
        return "";
    }

}
