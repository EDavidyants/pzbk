package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author fn
 */
public class SearchFilterObjectStringConverter implements Converter {

    private final List<SearchFilterObject> allSearchFilterObjects;

    public SearchFilterObjectStringConverter(List<SearchFilterObject> allSearchFilterObjects) {
        this.allSearchFilterObjects = allSearchFilterObjects;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return allSearchFilterObjects.stream()
                .filter(t -> t.getName().equals(value))
                .findAny().orElse(null);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof SearchFilterObject) {
            SearchFilterObject searchFilterObject = (SearchFilterObject) value;
            return searchFilterObject.getName();
        }
        return "";
    }

}
