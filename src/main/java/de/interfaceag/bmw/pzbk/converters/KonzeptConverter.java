package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.Collections;
import java.util.List;

public class KonzeptConverter implements Converter<Konzept> {

    private final List<Konzept> konzepte;

    public KonzeptConverter(List<Konzept> konzepte) {
        if (konzepte != null) {
            this.konzepte = konzepte;
        } else {
            this.konzepte = Collections.emptyList();
        }
    }

    @Override
    public Konzept getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        } else if (RegexUtils.matchesId(value)) {
            long id = Long.parseLong(value);
            return konzepte.stream().filter(konzept -> konzept.getId() == id).findAny().orElse(null);
        } else {
            return konzepte.stream().filter(konzept -> konzept.getBezeichnung().equals(value)).findAny().orElse(null);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Konzept konzept) {
        if (konzept == null) {
            return "";
        } else if (konzept.getId() > 0) {
            return Long.toString(konzept.getId());
        } else {
            return konzept.getBezeichnung();
        }
    }

}
