
package de.interfaceag.bmw.pzbk.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;
import java.time.LocalDate;

/**
 * DIESE KLASSE AUF KEINEN FALL LOESCHEN, SIE WIRD BEIM STARTEN DER APPLIKATION FUER DIE LOCALDATES IN DER DATENBANK BENUTZT
 *
 * @author evda
 */
@Converter(autoApply = true)
public class LocalDateAttributeConverter implements AttributeConverter<LocalDate, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDate locDate) {
        return (locDate == null ? null : Date.valueOf(locDate));
    }

    @Override
    public LocalDate convertToEntityAttribute(Date sqlDate) {
        return (sqlDate == null ? null : sqlDate.toLocalDate());
    }

}
