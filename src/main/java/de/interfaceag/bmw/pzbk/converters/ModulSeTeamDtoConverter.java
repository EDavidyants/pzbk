package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.dialog.viewdata.ModulSeTeamDto;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author fn
 */
public class ModulSeTeamDtoConverter implements
        Converter {

    private final List<ModulSeTeamDto> modulSeTeams;

    public ModulSeTeamDtoConverter(List<ModulSeTeamDto> modulSeTeams) {
        this.modulSeTeams = modulSeTeams;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return modulSeTeams.stream().filter(m -> m.getName().equals(value)).findAny().orElse(null);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof ModulSeTeamDto) {
            ModulSeTeamDto se = (ModulSeTeamDto) value;
            return se.getName();
        }
        return "";
    }
}
