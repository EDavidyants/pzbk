package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author Stefan Luchs sl
 *
 */
public class DerivatConverter implements Converter {

    private static final Logger LOG = LoggerFactory.getLogger(DerivatConverter.class.getName());

    private final List<Derivat> derivatList;

    public DerivatConverter(List<Derivat> derivatList) {
        this.derivatList = derivatList;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Long id;
        Derivat result = null;
        try {
            id = Long.parseLong(value);
            result = derivatList.stream().filter(d -> d.getId().equals(id)).findFirst().orElse(null);
        } catch (NumberFormatException exception) {
            LOG.error(exception.getMessage());
        }
        return result;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof Derivat)) {
            return null;
        } else {
            Derivat d = (Derivat) value;
            return d.getId().toString();
        }
    }
}
