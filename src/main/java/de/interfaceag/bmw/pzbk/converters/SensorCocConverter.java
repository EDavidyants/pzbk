package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author fp
 */
public class SensorCocConverter implements Converter {

    private static final Logger LOG = LoggerFactory.getLogger(SensorCocConverter.class.getName());

    private final SensorCocService sensorCocService;
    private final Map<String, SensorCoc> allScocs;

    public SensorCocConverter(SensorCocService sensorCocService) {
        this.sensorCocService = sensorCocService;
        this.allScocs = new HashMap<>();
    }

    public SensorCocConverter(Collection<SensorCoc> scocs) {
        this.sensorCocService = null;
        this.allScocs = new HashMap<>();
        Iterator<SensorCoc> iter = scocs.iterator();
        while (iter.hasNext()) {
            SensorCoc tmpScoc = iter.next();
            allScocs.put(tmpScoc.getSensorCocId().toString(), tmpScoc);
        }
    }

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && value.trim().length() > 0) {
            try {
                if (this.sensorCocService != null) {
                    return this.sensorCocService.getSensorCocById(Long.parseLong(value));
                } else {
                    return this.allScocs.get(value);
                }
            } catch (NumberFormatException exception) {
                LOG.error(exception.getMessage(), exception);
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid SensorCoC."));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object instanceof SensorCoc) {
            return String.valueOf(((SensorCoc) object).getSensorCocId());
        } else {
            return null;
        }
    }
}
