package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/**
 * @author sl
 */
public class UmsetzungsBestaetigungStatusConverter implements Converter {

    private static final Logger LOG = LoggerFactory.getLogger(UmsetzungsBestaetigungStatusConverter.class.getName());

    public UmsetzungsBestaetigungStatusConverter() {

    }

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && value.trim().length() > 0) {
            try {
                return UmsetzungsBestaetigungStatus.getStatusByName(value);
            } catch (NumberFormatException exception) {
                LOG.error(exception.getMessage(), exception);
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid KovAPhase."));
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object instanceof UmsetzungsBestaetigungStatus) {
            return (object).toString();
        } else {
            return null;
        }
    }

}
