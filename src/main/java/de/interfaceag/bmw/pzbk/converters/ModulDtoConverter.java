package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.dialog.viewdata.ModulDto;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author fn
 */
public class ModulDtoConverter implements Converter {

    private final List<ModulDto> module;

    public ModulDtoConverter(List<ModulDto> module) {
        this.module = module;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return module.stream().filter(m -> m.getName().equals(value)).findAny().orElse(null);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof ModulDto) {
            ModulDto m = (ModulDto) value;
            return m.getName();
        }
        return null;
    }

}
