package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.entities.Tteam;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class TteamConverter implements Converter {

    private final List<Tteam> tteams;

    public TteamConverter(List<Tteam> tteams) {
        this.tteams = tteams;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return tteams.stream().filter(t -> t.getId().toString().equals(value)).findAny().orElse(null);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Tteam) {
            Tteam tt = (Tteam) value;
            return tt.getId().toString();
        }
        return "";
    }

}
