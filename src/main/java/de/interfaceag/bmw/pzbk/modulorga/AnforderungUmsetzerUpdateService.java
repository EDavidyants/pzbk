package de.interfaceag.bmw.pzbk.modulorga;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.Umsetzungsbestaetigung;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungService;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static de.interfaceag.bmw.pzbk.modulorga.ModulOrgaUtils.anforderungAlreadyHasNewModul;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class AnforderungUmsetzerUpdateService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungUmsetzerUpdateService.class);

    private static final String RESULTSHEETNAME = "ApplyAnforderungUmsetzerChanges";
    private static final String RESULTSHEETNAMEAPPLYCHANGES = "Modifizierte Anforderungen";

    @Inject
    private AnforderungService anforderungService;
    @Inject
    private ModulService modulService;
    @Inject
    private DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;
    @Inject
    private ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;

    private final Set<Tuple<Long, Long>> anforderungSeTeamIdList = new HashSet<>();

    public Workbook evaluateAnforderungUmsetzerChanges(Sheet sheet) {

        anforderungSeTeamIdList.clear();

        List<ModuleStructureUpdateDto> moduleStructureUpdateList = ModuleStructureUpdateDtoConverter.convertToDto(sheet);

        Workbook resultWorkbook = createAnforderungResultWorkbook(RESULTSHEETNAME);
        Sheet resultSheet = resultWorkbook.getSheet(RESULTSHEETNAME);

        for (ModuleStructureUpdateDto moduleStructureUpdate : moduleStructureUpdateList) {
            evaluateChangesForKomponente(moduleStructureUpdate, resultSheet);
        }

        for (ModuleStructureUpdateDto moduleStructureUpdate : moduleStructureUpdateList) {
            evaluateChangesForSeTeam(moduleStructureUpdate, resultSheet);
        }

        LOG.info("finished");

        return resultWorkbook;
    }

    /**
     * Apply changes to anforderung and all related entities based in input
     * Excel Sheet. Return result Excel Sheet with log of all changes. Only
     * writes changes if save is true!
     *
     * @param sheet input Excel sheet.
     * @param persist defined if the changes are persisted or only log is
     * generated.
     * @return Workbook with the results.
     */
    public Workbook applyAnforderungUmsetzerChanges(Sheet sheet, boolean persist) {

        anforderungSeTeamIdList.clear();

        List<AnforderungUmsetzerUpdateDto> anforderungUmsetzerUpdateList = AnforderungUmsetzerUpdateDtoConverter.convertToDto(sheet);

        Workbook resultWorkbook = createAnforderungUmsetzerResultWorkbook(RESULTSHEETNAMEAPPLYCHANGES);
        Sheet resultSheet = resultWorkbook.getSheet(RESULTSHEETNAMEAPPLYCHANGES);

        for (AnforderungUmsetzerUpdateDto anforderungUmsetzerUpdate : anforderungUmsetzerUpdateList) {
            applyChanges(anforderungUmsetzerUpdate, resultSheet, persist);
        }

        LOG.info("finished");

        return resultWorkbook;
    }

    private static Workbook createAnforderungResultWorkbook(String sheetName) {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet(sheetName);
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("Anforderung");
        row.createCell(1).setCellValue("SeTeam alt");
        row.createCell(2).setCellValue("SeTeam neu");
        row.createCell(3).setCellValue("eindeutig");
        return workbook;
    }

    private static void addAnforderungResult(Sheet resultSheet, String anforderung, String seTeamAlt, String seTeamNeu, String eindeutig) {
        int lastRowNum = resultSheet.getLastRowNum() + 1;
        Row row = resultSheet.createRow(lastRowNum);
        row.createCell(0).setCellValue(anforderung);
        row.createCell(1).setCellValue(seTeamAlt);
        row.createCell(2).setCellValue(seTeamNeu);
        row.createCell(3).setCellValue(eindeutig);
    }

    private static Workbook createAnforderungUmsetzerResultWorkbook(String sheetName) {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet(sheetName);
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("Anforderung");
        row.createCell(1).setCellValue("SeTeam alt");
        row.createCell(2).setCellValue("SeTeam neu");
        row.createCell(3).setCellValue("eindeutig");
        row.createCell(4).setCellValue("Änderung durchgeführt");
        row.createCell(5).setCellValue("Kommentar");
        return workbook;
    }

    private static void addAnforderungUmsetzerResult(Sheet resultSheet, String anforderung, String seTeamAlt, String seTeamNeu, String eindeutig, String umgesetzt, String message) {
        int lastRowNum = resultSheet.getLastRowNum() + 1;
        Row row = resultSheet.createRow(lastRowNum);
        row.createCell(0).setCellValue(anforderung);
        row.createCell(1).setCellValue(seTeamAlt);
        row.createCell(2).setCellValue(seTeamNeu);
        row.createCell(3).setCellValue(eindeutig);
        row.createCell(4).setCellValue(umgesetzt);
        row.createCell(5).setCellValue(message);
    }

    private void evaluateChangesForKomponente(ModuleStructureUpdateDto moduleStructureUpdate, Sheet resultSheet) {

        String ppg = moduleStructureUpdate.getPpg();

        ModulKomponente modulKomponente = modulService.getModulKomponenteByPpg(ppg);

        if (modulKomponente == null) {
            String message = "Komponente  mit PPG Nummer " + ppg + " nicht gefunden.";
            LOG.warn(message);
            return;
        }

        List<Anforderung> anforderungenByPpg = anforderungService.getAnforderungenByKomponenteId(modulKomponente.getId());

        if (anforderungenByPpg.isEmpty()) {
            String message = "Es wurde keine Anforderung für PPG " + ppg + " gefunden";
            LOG.info(message);
        } else {
            String message = "Es wurden " + anforderungenByPpg.size() + " Anforderungen für PPG " + ppg + " gefunden";
            LOG.info(message);
        }

        for (Anforderung anforderung : anforderungenByPpg) {

            Iterator<Umsetzer> umsetzerIterator = anforderung.getUmsetzer().iterator();

            while (umsetzerIterator.hasNext()) {
                Umsetzer umsetzer = umsetzerIterator.next();
                if (umsetzer.getKomponente() != null && umsetzer.getKomponente().getPpg().equals(ppg)) {

                    Tuple anforderungModulTuple = new GenericTuple<>(anforderung.getId(), umsetzer.getSeTeam().getId());

                    if (!anforderungSeTeamIdList.contains(anforderungModulTuple)) {
                        anforderungSeTeamIdList.add(anforderungModulTuple);
                        addAnforderungResult(resultSheet, anforderung.toString(), umsetzer.getSeTeam().getName(), moduleStructureUpdate.getNewModulSeTeamName(), "JA");
                    } else {
                        LOG.info("Anforderung {} und Submodul {} bereits in Liste vorhanden.", new Object[]{anforderung.toString(), umsetzer.getSeTeam().getName()});
                    }
                }
            }

        }
    }

    private void evaluateChangesForSeTeam(ModuleStructureUpdateDto moduleStructureUpdate, Sheet resultSheet) {

        String ppg = moduleStructureUpdate.getPpg();
        String oldModulSeTeamName = moduleStructureUpdate.getOldModulSeTeamName();

        ModulKomponente modulKomponente = modulService.getModulKomponenteByPpg(ppg);

        if (modulKomponente == null) {
            String message = "Komponente  mit PPG Nummer " + ppg + " nicht gefunden.";
            LOG.warn(message);
            return;
        }

        List<Anforderung> anforderungenBySeTeamName = anforderungService.getAnforderungenBySeTeamName(oldModulSeTeamName);

        if (anforderungenBySeTeamName.isEmpty()) {
            String message = "Es wurde keine Anforderung für SeTeam " + oldModulSeTeamName + " gefunden";
            LOG.info(message);
        } else {
            String message = "Es wurden " + anforderungenBySeTeamName.size() + " Anforderungen für SeTeam " + oldModulSeTeamName + " gefunden";
            LOG.info(message);
        }

        for (Anforderung anforderung : anforderungenBySeTeamName) {

            Iterator<Umsetzer> umsetzerIterator = anforderung.getUmsetzer().iterator();

            while (umsetzerIterator.hasNext()) {
                Umsetzer umsetzer = umsetzerIterator.next();

                if (umsetzer.getKomponente() != null && umsetzer.getSeTeam().getName().equals(oldModulSeTeamName)) {

                    Tuple anforderungModulTuple = new GenericTuple<>(anforderung.getId(), umsetzer.getSeTeam().getId());

                    if (!anforderungSeTeamIdList.contains(anforderungModulTuple)) {
                        anforderungSeTeamIdList.add(anforderungModulTuple);
                        addAnforderungResult(resultSheet, anforderung.toString(), umsetzer.getSeTeam().getName(), moduleStructureUpdate.getNewModulSeTeamName(), "NEIN");
                    } else {
                        LOG.info("Anforderung {} und Submodul {} bereits in Liste vorhanden.", new Object[]{anforderung.toString(), umsetzer.getSeTeam().getName()});
                    }
                }
            }

            Iterator<AnforderungFreigabeBeiModulSeTeam> freigabeIterator = anforderung.getAnfoFreigabeBeiModul().iterator();

            while (freigabeIterator.hasNext()) {
                AnforderungFreigabeBeiModulSeTeam freigabe = freigabeIterator.next();

                if (freigabe.getModulSeTeam() != null && freigabe.getModulSeTeam().getName().equals(oldModulSeTeamName)) {

                    Tuple anforderungModulTuple = new GenericTuple<>(anforderung.getId(), freigabe.getModulSeTeam().getId());

                    if (!anforderungSeTeamIdList.contains(anforderungModulTuple)) {
                        anforderungSeTeamIdList.add(anforderungModulTuple);
                        addAnforderungResult(resultSheet, anforderung.toString(), freigabe.getModulSeTeam().getName(), moduleStructureUpdate.getNewModulSeTeamName(), "NEIN");
                    } else {
                        LOG.info("Anforderung {} und Submodul {} bereits in Liste vorhanden.", new Object[]{anforderung.toString(), freigabe.getModulSeTeam().getName()});
                    }
                }
            }
        }

    }

    private void applyChanges(AnforderungUmsetzerUpdateDto anforderungUmsetzerUpdate, Sheet resultSheet, boolean persist) {

        if (!anforderungUmsetzerUpdate.isEindeutig()) {
            String message = "Change SeTeam for Anforderung " + anforderungUmsetzerUpdate.getFachIdVersion() + " from " + anforderungUmsetzerUpdate.getOldModulSeTeamName() + " to " + anforderungUmsetzerUpdate.getNewModulSeTeamName();
            LOG.info(message);
            addAnforderungUmsetzerResult(resultSheet, anforderungUmsetzerUpdate.getFachIdVersion(), anforderungUmsetzerUpdate.getOldModulSeTeamName(), anforderungUmsetzerUpdate.getNewModulSeTeamName(),
                    anforderungUmsetzerUpdate.getEindeutig(), "NEIN", "Nicht eindeutig. Änderung wurde NICHT angewendet.");
            return;
        }

        ModulSeTeam oldModulSeTeam;

        if (anforderungUmsetzerUpdate.getOldModulSeTeamName().isEmpty()) {

            oldModulSeTeam = null;

        } else {

            oldModulSeTeam = modulService.getModulSeTeamByName(anforderungUmsetzerUpdate.getOldModulSeTeamName());

            if (oldModulSeTeam == null) {
                String message = "ModulSeTeam mit Name " + anforderungUmsetzerUpdate.getOldModulSeTeamName() + " nicht gefunden.";
                LOG.warn(message);
                return;
            }

        }

        ModulSeTeam newModulSeTeam = modulService.getModulSeTeamByName(anforderungUmsetzerUpdate.getNewModulSeTeamName());

        if (newModulSeTeam == null) {
            String message = "ModulSeTeam mit Name " + anforderungUmsetzerUpdate.getNewModulSeTeamName() + " nicht gefunden.";
            LOG.warn(message);
            return;
        }

        Anforderung anforderung = anforderungService.getAnforderungByFachIdVersion(anforderungUmsetzerUpdate.getFachId(), anforderungUmsetzerUpdate.getVersion());

        if (anforderung == null) {
            String message = "Es wurde keine Anforderung für FachId " + anforderungUmsetzerUpdate.getFachId() + " Version " + anforderungUmsetzerUpdate.getVersion() + " gefunden";
            LOG.warn(message);
            return;
        }

        if (anforderung.getUmsetzer() == null) {
            String message = "Anforderung " + anforderung.toString() + " enthält keine Umsetzer!";
            LOG.warn(message);
            return;
        }

        if (oldModulSeTeam != null) {

            // old logic
            changeSeTeamForAnforderung(anforderung, oldModulSeTeam, newModulSeTeam, resultSheet, persist);

        } else {

            //new logic for creating new data
            addSeTeamForAnforderung(anforderung, newModulSeTeam, resultSheet, persist);

        }

    }

    private void addSeTeamForAnforderung(Anforderung anforderung, ModulSeTeam newModulSeTeam, Sheet resultSheet, boolean persist) {

        StringBuilder message = new StringBuilder("Add SeTeam " + newModulSeTeam.getName() + " for Anforderung ").append(anforderung.toString())
                .append(". ");

        createUmsetzer(anforderung, newModulSeTeam, message, persist);

        createFreigabe(anforderung, newModulSeTeam, message, persist);

        addModulForAnforderungIfNecessary(anforderung, newModulSeTeam, message, persist);

        LOG.info(message.toString());
        addAnforderungUmsetzerResult(resultSheet, anforderung.toString(), "", newModulSeTeam.getName(), "", "JA", message.toString());
    }

    private void changeSeTeamForAnforderung(Anforderung anforderung, ModulSeTeam oldModulSeTeam, ModulSeTeam newModulSeTeam, Sheet resultSheet, boolean persist) {

        StringBuilder message = new StringBuilder("Change SeTeam for Anforderung ").append(anforderung.toString())
                .append(" from ").append(oldModulSeTeam.getName()).append(" to ").append(newModulSeTeam.getName()).append(". ");

        updateUmsetzer(anforderung, oldModulSeTeam, newModulSeTeam, message, persist);

        updateFreigabe(anforderung, oldModulSeTeam, newModulSeTeam, message, persist);

        changeModulForAnforderungIfNecessary(anforderung, oldModulSeTeam, newModulSeTeam, message, persist);

        LOG.info(message.toString());
        addAnforderungUmsetzerResult(resultSheet, anforderung.toString(), oldModulSeTeam.getName(), newModulSeTeam.getName(), "", "JA", message.toString());
    }

    private void updateUmsetzer(Anforderung anforderung, ModulSeTeam oldModulSeTeam, ModulSeTeam newModulSeTeam, StringBuilder message, boolean persist) {
        Iterator<Umsetzer> umsetzerIterator = anforderung.getUmsetzer().iterator();

        while (umsetzerIterator.hasNext()) {
            Umsetzer umsetzer = umsetzerIterator.next();
            if (umsetzer.getSeTeam() != null && umsetzer.getSeTeam().getName().equals(oldModulSeTeam.getName())) {
                if (persist) {
                    umsetzer.setSeTeam(newModulSeTeam);
                }
                message.append("Change Umsetzer (id: ").append(umsetzer.getId()).append("); ");
            }
        }
    }

    private void createUmsetzer(Anforderung anforderung, ModulSeTeam newModulSeTeam, StringBuilder message, boolean persist) {

        Umsetzer newUmsetzer = new Umsetzer(newModulSeTeam);

        if (persist) {
            anforderung.addUmsetzer(newUmsetzer);
        }

        message.append("Add Umsetzer (id: ").append(newUmsetzer.getSeTeam().toString()).append("); ");

    }

    private void updateFreigabe(Anforderung anforderung, ModulSeTeam oldModulSeTeam, ModulSeTeam newModulSeTeam, StringBuilder message, boolean persist) {
        Iterator<AnforderungFreigabeBeiModulSeTeam> freigabeIterator = anforderung.getAnfoFreigabeBeiModul().iterator();

        while (freigabeIterator.hasNext()) {
            AnforderungFreigabeBeiModulSeTeam freigabe = freigabeIterator.next();

            if (freigabe.getModulSeTeam().getId().equals(oldModulSeTeam.getId())) {
                if (persist) {
                    freigabe.setModulSeTeam(newModulSeTeam);
                }
                message.append("Change Freigabe (id: ").append(freigabe.getId()).append("); ");
            }
        }
    }

    private void createFreigabe(Anforderung anforderung, ModulSeTeam newModulSeTeam, StringBuilder message, boolean persist) {

        boolean isFreigegeben = isAnforderungFreigegeben(anforderung);

        AnforderungFreigabeBeiModulSeTeam newFreigabe = new AnforderungFreigabeBeiModulSeTeam(anforderung, newModulSeTeam, isFreigegeben, "", VereinbarungType.ZAK);

        if (persist) {
            anforderung.addAnfoFreigabeBeiModul(newFreigabe);
        }

        message.append("Add Freigabe (").append(newFreigabe.toString()).append("); ");

    }

    private static boolean isAnforderungFreigegeben(Anforderung anforderung) {
        return anforderung.getStatus().equals(Status.A_FREIGEGEBEN);
    }

    private void changeModulForAnforderungIfNecessary(Anforderung anforderung, ModulSeTeam oldModulSeTeam, ModulSeTeam newModulSeTeam, StringBuilder message, boolean persist) {
        if (oldModulSeTeam.getElternModul().getId().equals(newModulSeTeam.getElternModul().getId())) {
            message.append("Keine Moduländerung!");
        } else {
            message.append("Moduländerung! Alt: ").append(oldModulSeTeam.getElternModul().getName()).append(" Neu: ").append(newModulSeTeam.getElternModul().getName());

            Modul oldModul = oldModulSeTeam.getElternModul();
            Modul newModul = newModulSeTeam.getElternModul();

            /**
             * check for 'Vereinigung' Fall. In this case the modul already
             * exists in the anforderung and therefore the data for vereinbarung
             * (DerivatAnforderungModul) and umsetzungsbestaetigung of the
             * anforderung needs to be manually removed
             */
            if (anforderungAlreadyHasNewModul(newModul, anforderung)) {
                message.append("Modul ").append(newModulSeTeam.getElternModul().getName()).append(" bereits in der Anforderung vorhanden! 'Vereinigungsfall!' Manuelle Anpassungen notwendig.");
            } else {
                updateVereinbarung(anforderung, oldModul, newModul, message, persist);
                updateUmsetzungsbestaetigung(anforderung, oldModul, newModul, message, persist);
            }
        }
    }

    private void addModulForAnforderungIfNecessary(Anforderung anforderung, ModulSeTeam newModulSeTeam, StringBuilder message, boolean persist) {

        if (anforderungHasModulOfSeTeam(anforderung, newModulSeTeam)) {

            // do nothing as anforderung already considers the module
            message.append("Keine Moduländerung!");

        } else {

            message.append("Moduländerung! Neu: ").append(newModulSeTeam.getElternModul().getName());

            Modul newModul = newModulSeTeam.getElternModul();

            createNewVereinbarungForZuordnungAnforderungDerivat(anforderung, newModul, message, persist);

        }
    }

    private static boolean anforderungHasModulOfSeTeam(Anforderung anforderung, ModulSeTeam newModulSeTeam) {
        return anforderung.getUmsetzer().stream()
                .map(umsetzer -> umsetzer.getModul())
                .anyMatch(modul -> modul.getId().equals(newModulSeTeam.getId()));
    }

    private void updateVereinbarung(Anforderung anforderung, Modul oldModul, Modul newModul, StringBuilder message, boolean persist) {
        // Vereinbarung AND ZakUebertragung
        // ZakUebertragung ist changed at it always references a DerivatAnforderungModul instance and does not hold the modul itself!
        List<DerivatAnforderungModul> vereinbarungForAnforderungModul = derivatAnforderungModulService.getDerivatAnforderungModulByAnforderungAndModul(anforderung.getId(), oldModul.getId());

        if (vereinbarungForAnforderungModul == null || vereinbarungForAnforderungModul.isEmpty()) {
            String logMessage = "Es wurde keine Vereinbarungen für Anforderung " + anforderung.toString() + " und Modul " + oldModul.getName() + " gefunden";
            LOG.info(logMessage);
            return;
        } else {
            String logMessage = "Es wurden " + vereinbarungForAnforderungModul.size() + " Vereinbarungen für Anforderung " + anforderung.toString() + " und Modul " + oldModul.getName() + " gefunden";
            LOG.info(logMessage);
        }

        Iterator<DerivatAnforderungModul> vereinbarungIterator = vereinbarungForAnforderungModul.iterator();

        while (vereinbarungIterator.hasNext()) {
            DerivatAnforderungModul vereinbarung = vereinbarungIterator.next();
            if (persist) {
                vereinbarung.setModul(newModul);
            }
            message.append("Vereinbarung ").append(vereinbarung.toString()).append(" (id: ").append(vereinbarung.getId()).append(") modified; ");
        }
    }

    private void createNewVereinbarungForZuordnungAnforderungDerivat(Anforderung anforderung, Modul newModul, StringBuilder message, boolean persist) {

        List<ZuordnungAnforderungDerivat> zuordnungenForAnforderung = zuordnungAnforderungDerivatService.getZuordnungAnforderungDerivatForAnforderung(anforderung);

        if (zuordnungenForAnforderung == null || zuordnungenForAnforderung.isEmpty()) {

            String logMessage = "Es wurde keine Zuordnungen für Anforderung " + anforderung.toString() + " gefunden";
            LOG.info(logMessage);
            return; // nothing to do here

        } else {

            String logMessage = "Es wurden " + zuordnungenForAnforderung.size() + " Zuordnungen für Anforderung " + anforderung.toString() + " gefunden";
            LOG.info(logMessage);

        }

        Iterator<ZuordnungAnforderungDerivat> zuordnungIterator = zuordnungenForAnforderung.iterator();

        while (zuordnungIterator.hasNext()) {

            ZuordnungAnforderungDerivat zuordnung = zuordnungIterator.next();

            DerivatAnforderungModul newVereinbarung = new DerivatAnforderungModul(zuordnung, newModul);
            newVereinbarung.setStatusZV(DerivatAnforderungModulStatus.ABZUSTIMMEN);
            newVereinbarung.setStatusVKBG(DerivatAnforderungModulStatus.ABZUSTIMMEN);

            if (persist) {

                derivatAnforderungModulService.persistDerivatAnforderungModul(newVereinbarung);

            }

            message.append("Vereinbarung ").append(zuordnung.toString()).append(" (id: ").append(zuordnung.getId()).append(") created; ");

        }
    }

    private void updateUmsetzungsbestaetigung(Anforderung anforderung, Modul oldModul, Modul newModul, StringBuilder message, boolean persist) {
        List<Umsetzungsbestaetigung> umsetzungsbestaetigungForAnforderungModul = umsetzungsbestaetigungService.getUmsetzungsbestaetigungByAnforderungModul(anforderung, oldModul);

        if (umsetzungsbestaetigungForAnforderungModul == null || umsetzungsbestaetigungForAnforderungModul.isEmpty()) {
            String logMessage = "Es wurde keine Umsetzungsbestätigung für Anforderung " + anforderung.toString() + " und Modul " + oldModul.getName() + " gefunden";
            LOG.info(logMessage);
            return;
        } else {
            String logMessage = "Es wurden " + umsetzungsbestaetigungForAnforderungModul.size() + " Umsetzungsbestätigungen für Anforderung " + anforderung.toString() + " und Modul " + oldModul.getName() + " gefunden";
            LOG.info(logMessage);
        }

        Iterator<Umsetzungsbestaetigung> umsetzungsbestaetigungIterator = umsetzungsbestaetigungForAnforderungModul.iterator();

        while (umsetzungsbestaetigungIterator.hasNext()) {
            Umsetzungsbestaetigung umsetzungsbestaetigung = umsetzungsbestaetigungIterator.next();
            if (persist) {
                umsetzungsbestaetigung.setModul(newModul);
            }
            message.append("Umsetzungsbestätigung ").append(umsetzungsbestaetigung.toString()).append(" (id: ").append(umsetzungsbestaetigung.getId()).append(") modified; ");
        }
    }

}
