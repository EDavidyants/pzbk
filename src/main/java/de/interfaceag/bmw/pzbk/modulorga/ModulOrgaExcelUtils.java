package de.interfaceag.bmw.pzbk.modulorga;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ModulOrgaExcelUtils {

    private ModulOrgaExcelUtils() {
    }

    protected static int getResultColumnIndex(Sheet sheet) {
        if (sheet.getLastRowNum() > 0) {
            Row row = sheet.getRow(0);
            return row.getLastCellNum();
        }
        return -1;
    }

    protected static void addResult(Sheet sheet, String result, int rowNumber, int resultColumnIndex) {
        sheet.getRow(rowNumber).createCell(resultColumnIndex).setCellValue(result);
    }
}
