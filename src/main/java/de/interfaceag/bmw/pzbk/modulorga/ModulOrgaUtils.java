package de.interfaceag.bmw.pzbk.modulorga;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Modul;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ModulOrgaUtils {

    private ModulOrgaUtils() {
    }

    protected static boolean anforderungAlreadyHasNewModul(Modul newModul, Anforderung anforderung) {
        if (newModul != null && newModul.getId() != null && anforderung != null) {
            return anforderung.getUmsetzer().stream().anyMatch(umsetzer -> umsetzer.getModul().getId().equals(newModul.getId()));
        } else {
            return false;
        }
    }

}
