package de.interfaceag.bmw.pzbk.modulorga;

import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AnforderungUmsetzerUpdateDto {

    private final int rowNumber;

    private final String fachIdVersion;

    private final String oldModulSeTeamName;

    private final String newModulSeTeamName;

    private final String eindeutig;

    public AnforderungUmsetzerUpdateDto(int rowNumber, String fachIdVersion, String oldModulSeTeamName, String newModulSeTeamName, String eindeutig) {
        this.rowNumber = rowNumber;
        this.fachIdVersion = fachIdVersion;
        this.oldModulSeTeamName = oldModulSeTeamName;
        this.newModulSeTeamName = newModulSeTeamName;
        this.eindeutig = eindeutig;
    }

    @Override
    public String toString() {
        return "AnforderungUmsetzerUpdateDto{" + "rowNumber=" + rowNumber + ", fachIdVersion=" + fachIdVersion + ", oldModulSeTeamName=" + oldModulSeTeamName + ", newModulSeTeamName=" + newModulSeTeamName + ", eindeutig=" + eindeutig + '}';
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public String getFachId() {
        return parseFachIdVersionToFachId();
    }

    private String parseFachIdVersionToFachId() {
        int indexOfSeparator = fachIdVersion.indexOf(" | ");
        if (indexOfSeparator > 0) {
            return fachIdVersion.substring(0, indexOfSeparator);
        }
        return "";
    }

    private int parseFachIdVersionToVersion() {
        int indexOfSeparator = fachIdVersion.indexOf("V");
        if (indexOfSeparator > 0) {
            String versionString = fachIdVersion.substring(indexOfSeparator + 1).trim();
            if (RegexUtils.matchesId(versionString)) {
                return Integer.parseInt(versionString);
            }
        }
        return -1;
    }

    public String getFachIdVersion() {
        return fachIdVersion;
    }

    public int getVersion() {
        return parseFachIdVersionToVersion();
    }

    public String getOldModulSeTeamName() {
        return oldModulSeTeamName;
    }

    public String getNewModulSeTeamName() {
        return newModulSeTeamName;
    }

    public boolean isEindeutig() {
        return "JA".equals(eindeutig);
    }

    public String getEindeutig() {
        return eindeutig;
    }

}
