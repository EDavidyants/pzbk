package de.interfaceag.bmw.pzbk.modulorga;

import de.interfaceag.bmw.pzbk.excel.ExcelUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ModuleStructureUpdateDtoConverter {

    private static final Logger LOG = LoggerFactory.getLogger(ModuleStructureUpdateDtoConverter.class.getName());

    private ModuleStructureUpdateDtoConverter() {
    }

    /**
     * Convert Excel Sheet to Dto Structure. First line will be ignored!
     *
     * @param sheet Excel Sheet of expected structure.
     * @return List of Dto objects.
     */
    public static List<ModuleStructureUpdateDto> convertToDto(Sheet sheet) {

        if (!sheetMatchesStructure(sheet)) {
            LOG.error("Sheet does not match expected structure");
            return new ArrayList<>();
        }

        List<ModuleStructureUpdateDto> result = new ArrayList<>();

        Row row;
        String oldModulSeTeamName;
        String newModulSeTeamName;
        String ppg;

        for (int rowNumber = sheet.getFirstRowNum() + 1; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            row = sheet.getRow(rowNumber);

            if (row.getLastCellNum() < 3) {
                LOG.error("Row does not match expected structure");
                break;
            }

            oldModulSeTeamName = ExcelUtils.cellToString(row.getCell(0));
            newModulSeTeamName = ExcelUtils.cellToString(row.getCell(1));
            ppg = ExcelUtils.cellToString(row.getCell(2));

            result.add(new ModuleStructureUpdateDto(oldModulSeTeamName, newModulSeTeamName, ppg, rowNumber));

        }

        return result;

    }

    private static boolean sheetMatchesStructure(Sheet sheet) {

        int lastRowNum = sheet.getLastRowNum();

        if (lastRowNum < 1) {
            LOG.error("Sheet is empty");
            return Boolean.FALSE;
        }

        Row row = sheet.getRow(0);

        short lastCellNum = row.getLastCellNum();

        if (lastCellNum != 3) {
            LOG.error("Row does not have correct length");
            return Boolean.FALSE;
        }

        return Boolean.TRUE;

    }

}
