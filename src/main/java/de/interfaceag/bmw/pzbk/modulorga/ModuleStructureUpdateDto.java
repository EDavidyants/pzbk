package de.interfaceag.bmw.pzbk.modulorga;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ModuleStructureUpdateDto {

    private final int rowNumber;

    private final String oldModulSeTeamName;

    private final String newModulSeTeamName;

    private final String ppg;

    public ModuleStructureUpdateDto(String oldModulSeTeamName, String newModulSeTeamName, String ppg, int rowNumber) {
        this.oldModulSeTeamName = oldModulSeTeamName;
        this.newModulSeTeamName = newModulSeTeamName;
        this.ppg = ppg;
        this.rowNumber = rowNumber;
    }

    @Override
    public String toString() {
        return "ModuleStructureUpdateDto{" + "rowNumber=" + rowNumber + ", oldModulSeTeamName=" + oldModulSeTeamName + ", newModulSeTeamName=" + newModulSeTeamName + ", ppg=" + ppg + '}';
    }

    public String getOldModulSeTeamName() {
        return oldModulSeTeamName;
    }

    public String getNewModulSeTeamName() {
        return newModulSeTeamName;
    }

    public String getPpg() {
        return ppg;
    }

    public int getRowNumber() {
        return rowNumber;
    }

}
