package de.interfaceag.bmw.pzbk.modulorga;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class ModulOrgaService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ModulOrgaService.class);

    private static final String MODULESTRUCTUREUPDATESHEET = "ModulUpdate";
    private static final String MODULESTRUCTUREUPDATEPERSISTSHEET = "ModulUpdatePersist";
    private static final String ANFORDERUNGUMSETZERUPDATESHEET = "AnforderungUmsetzer";
    private static final String ANFORDERUNGUMSETZERAPPLYCHANGESSHEET = "ApplyAnforderungUmsetzerChanges";
    private static final String ANFORDERUNGUMSETZERAPPLYANDPERSISTCHANGESSHEET = "ApplyAndPersistAnforderung";
    private static final String NOT_FOUND = " not found";
    private static final String FINISHED = " finished";
    private static final String FOUND = " found";

    @Inject
    private ModuleStructureUpdateService moduleStructureUpdateService;
    @Inject
    private AnforderungUmsetzerUpdateService anforderungUmsetzerUpdateService;

    /**
     * Handle all modulOrgExcelUploads.
     *
     * @param uploadedFile File from Excel Upload.
     * @return Workbook with results based on case.
     */
    public Workbook handleModulOrgaUploadedFile(UploadedFile uploadedFile) {

        if (uploadedFile == null) {
            LOG.error("uploadedFile is null");
            return new HSSFWorkbook();
        }

        Workbook workbook;
        try {
            workbook = WorkbookFactory.create(uploadedFile.getInputstream());
            return handleModulOrgaExcelSheet(workbook);
        } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
            LOG.error(ex.getMessage());
        }

        return new HSSFWorkbook();
    }

    private Workbook handleModulOrgaExcelSheet(Workbook workbook) {

        Sheet moduleStructureUpdateSheet = workbook.getSheet(MODULESTRUCTUREUPDATESHEET);
        if (moduleStructureUpdateSheet != null) {
            LOG.info(MODULESTRUCTUREUPDATESHEET + FOUND);
            moduleStructureUpdateService.updateModuleStructure(moduleStructureUpdateSheet, Boolean.FALSE);
            LOG.info(MODULESTRUCTUREUPDATESHEET + FINISHED);
        } else {
            LOG.info(MODULESTRUCTUREUPDATESHEET + NOT_FOUND);
        }

        Sheet moduleStructureUpdatePersistSheet = workbook.getSheet(MODULESTRUCTUREUPDATEPERSISTSHEET);
        if (moduleStructureUpdatePersistSheet != null) {
            LOG.info(MODULESTRUCTUREUPDATEPERSISTSHEET + FOUND);
            moduleStructureUpdateService.updateModuleStructure(moduleStructureUpdatePersistSheet, Boolean.TRUE);
            LOG.info(MODULESTRUCTUREUPDATEPERSISTSHEET + FINISHED);
        } else {
            LOG.info(MODULESTRUCTUREUPDATEPERSISTSHEET + NOT_FOUND);
        }

        Sheet anforderungUmsetzerUpdateSheet = workbook.getSheet(ANFORDERUNGUMSETZERUPDATESHEET);
        if (anforderungUmsetzerUpdateSheet != null) {
            LOG.info(ANFORDERUNGUMSETZERUPDATESHEET + FOUND);
            return anforderungUmsetzerUpdateService.evaluateAnforderungUmsetzerChanges(anforderungUmsetzerUpdateSheet);
        } else {
            LOG.info(ANFORDERUNGUMSETZERUPDATESHEET + NOT_FOUND);
        }

        Sheet applyAnforderungUmsetzerUpdateSheet = workbook.getSheet(ANFORDERUNGUMSETZERAPPLYCHANGESSHEET);
        if (applyAnforderungUmsetzerUpdateSheet != null) {
            LOG.info(ANFORDERUNGUMSETZERAPPLYCHANGESSHEET + FOUND);
            return anforderungUmsetzerUpdateService.applyAnforderungUmsetzerChanges(applyAnforderungUmsetzerUpdateSheet, Boolean.FALSE);
        } else {
            LOG.info(ANFORDERUNGUMSETZERAPPLYCHANGESSHEET + NOT_FOUND);
        }

        Sheet applyAndPersistAnforderungUmsetzerUpdateSheet = workbook.getSheet(ANFORDERUNGUMSETZERAPPLYANDPERSISTCHANGESSHEET);
        if (applyAndPersistAnforderungUmsetzerUpdateSheet != null) {
            LOG.info(ANFORDERUNGUMSETZERAPPLYANDPERSISTCHANGESSHEET + FOUND);
            return anforderungUmsetzerUpdateService.applyAnforderungUmsetzerChanges(applyAndPersistAnforderungUmsetzerUpdateSheet, Boolean.TRUE);
        } else {
            LOG.info(ANFORDERUNGUMSETZERAPPLYANDPERSISTCHANGESSHEET + NOT_FOUND);
        }

        return workbook;

    }

}
