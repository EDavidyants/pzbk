package de.interfaceag.bmw.pzbk.modulorga;

import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.services.ModulService;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class ModuleStructureUpdateService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ModuleStructureUpdateService.class.getName());

    @Inject
    private ModulService modulService;

    public void updateModuleStructure(Sheet sheet, boolean persistChanges) {

        List<ModuleStructureUpdateDto> moduleStructureUpdateList = ModuleStructureUpdateDtoConverter.convertToDto(sheet);

        int resultColumnIndex = ModulOrgaExcelUtils.getResultColumnIndex(sheet);

        if (resultColumnIndex == -1) {
            LOG.error("Excel Sheet is corrupt");
            return;
        }

        for (ModuleStructureUpdateDto moduleStructureUpdate : moduleStructureUpdateList) {
            applyChange(moduleStructureUpdate, sheet, resultColumnIndex, persistChanges);
        }

    }

    private void applyChange(ModuleStructureUpdateDto moduleStructureUpdate, Sheet sheet, int resultColumnIndex, boolean persistChanges) {

        int rowNumber = moduleStructureUpdate.getRowNumber();
        String ppg = moduleStructureUpdate.getPpg();
        String newModulSeTeamName = moduleStructureUpdate.getNewModulSeTeamName();

        ModulKomponente modulKomponente = modulService.getModulKomponenteByPpg(ppg);

        if (modulKomponente == null) {
            String message = "Komponente  mit PPG Nummer " + ppg + " nicht gefunden.";
            LOG.warn(message);
            ModulOrgaExcelUtils.addResult(sheet, message, rowNumber, resultColumnIndex);
            return;
        }

        ModulSeTeam newModulSeTeam = modulService.getModulSeTeamByName(newModulSeTeamName);

        if (newModulSeTeam == null) {
            String message = "ModulSeTeam mit Name " + newModulSeTeamName + " nicht gefunden.";
            LOG.warn(message);
            ModulOrgaExcelUtils.addResult(sheet, message, rowNumber, resultColumnIndex);
            return;
        }

        ModulSeTeam oldSeTeam = modulKomponente.getSeTeam();

        if (persistChanges) {
            LOG.info("Persist changes");
            applyAndPersistChanges(newModulSeTeam, modulKomponente);
        }

        String message = "Komponente mit PPG " + ppg + " wurde von " + oldSeTeam.getName() + " zu " + newModulSeTeam.getName() + " umgehängt.";
        LOG.info(message);
        ModulOrgaExcelUtils.addResult(sheet, message, rowNumber, resultColumnIndex);

    }

    private void applyAndPersistChanges(ModulSeTeam newModulSeTeam, ModulKomponente modulKomponente) {
        // remove komponente from seteam
        ModulSeTeam oldSeTeam = modulKomponente.getSeTeam();
        oldSeTeam.getKomponenten().remove(modulKomponente);

        // replace seteam in komponente
        modulKomponente.setSeTeam(newModulSeTeam);

        // add komponente to new seteam
        newModulSeTeam.addKomponente(modulKomponente);

        persistChanges(oldSeTeam, newModulSeTeam, modulKomponente);
    }

    private void persistChanges(ModulSeTeam oldSeTeam, ModulSeTeam newModulSeTeam, ModulKomponente modulKomponente) {
        modulService.persistModulKomponente(modulKomponente);
        modulService.persistModulSeTeam(newModulSeTeam);
        modulService.persistModulSeTeam(oldSeTeam);
    }

}
