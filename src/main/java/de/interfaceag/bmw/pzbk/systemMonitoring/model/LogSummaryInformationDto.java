package de.interfaceag.bmw.pzbk.systemMonitoring.model;

import java.util.Date;
import java.util.List;

public final class LogSummaryInformationDto {

    private final Date lastZakPost;
    private final Date lastZakGet;
    private final String zakSummary;

    private LogSummaryInformationDto(Builder builder) {
        lastZakPost = builder.lastZakPost;
        lastZakGet = builder.lastZakGet;
        zakSummary = builder.zakSummary;
    }

    public Date getLastZakPost() {
        return lastZakPost;
    }

    public Date getLastZakGet() {
        return lastZakGet;
    }

    public String getZakSummary() {
        return zakSummary;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private Date lastZakPost;
        private Date lastZakGet;
        private List<String> changedDerivates;
        private String zakSummary;

        private Builder() {
        }

        public Builder withLastZakPost(Date val) {
            lastZakPost = val;
            return this;
        }

        public Builder withLastZakGet(Date val) {
            lastZakGet = val;
            return this;
        }

        public Builder withZakSummary(String val) {
            zakSummary = val;
            return this;
        }

        public LogSummaryInformationDto build() {
            return new LogSummaryInformationDto(this);
        }
    }
}
