package de.interfaceag.bmw.pzbk.systemMonitoring;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.LogLevel;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.SystemType;
import de.interfaceag.bmw.pzbk.filter.SingleDerivatFilter;
import de.interfaceag.bmw.pzbk.filter.VonBisDateSearchFilter;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.DateUtils;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.SystemMonitoringViewData;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.SystemMonitoringViewFilter;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.inputtext.InputText;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author sf (stefan.fruhwirth@interface-ag.de)
 */
@ViewScoped
@Named
public class SystemMonitoringController implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SystemMonitoringController.class.getName());

    @Inject
    private Session session;

    @Inject
    private SystemMonitoringFacade systemMonitoringFacade;

    private boolean userIsAdmin;
    private SystemMonitoringViewData monitoringViewData;
    private SystemMonitoringViewFilter systemMonitoringViewFilter;

    @PostConstruct
    public void init() {
        this.handleUserSession();
        this.initMonitoringData();
        this.session.setLocationForView();
    }

    public boolean isUserIsAdmin() {
        return userIsAdmin;
    }

    public SystemMonitoringViewData getMonitoringViewData() {
        return this.monitoringViewData;
    }

    /**
     * init default view data.
     * --> default filter is the actual date.
     */
    private void initMonitoringData() {
        UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();
        List<Derivat> derivatList = this.systemMonitoringFacade.getAllDerivate();

        this.systemMonitoringViewFilter = new SystemMonitoringViewFilter(urlParameter, derivatList);

        Date startDate = new Date();
        this.systemMonitoringViewFilter.getDatumFilter().setVon(startDate);
        this.systemMonitoringViewFilter.getDatumFilter().setBis(startDate);
        this.changeFilterEvent();
    }

    public VonBisDateSearchFilter getFilter() {
        return this.systemMonitoringViewFilter.getDatumFilter();
    }

    public SingleDerivatFilter getDerivatFilter() {
        return this.systemMonitoringViewFilter.getDerivatFilter();
    }

    /**
     * handling the filter events
     * --> load new zak-uebertragung for table
     * --> load new log-entries for table
     */
    public void changeFilterEvent() {
        resetZakFilters();
        // note: not both datum-filter must be set.
        Date startDate = systemMonitoringViewFilter.getDatumFilter().getVon();
        Date endDate = systemMonitoringViewFilter.getDatumFilter().getBis();

        startDate = DateUtils.removeTime(startDate);
        endDate = DateUtils.removeTime(endDate);

        if (startDate.getTime() == endDate.getTime()) {
            endDate = DateUtils.addDays(endDate, 1);
        }

        String selectedDerivatName = this.getSelectedDerivatName();
        LOGGER.debug("Changed filter for startDate = {}, endDate = {}, selectedDerivat = {}", startDate, endDate, selectedDerivatName);
        this.monitoringViewData = this.systemMonitoringFacade.changeFilterSetting(startDate, endDate, selectedDerivatName);
    }

    /**
     * Handling the user session and check user authorization.
     */
    private void handleUserSession() {
        Mitarbeiter currentUser = session.getUser();
        if (!session.hasRole(Rolle.ADMIN)) {
            String message = "Versuchter Zugriff auf SystemMonitoringDashboard ohne ausreichende Berechtigung durch " + currentUser.toString();
            this.systemMonitoringFacade.addLogEntry(LogLevel.INFO, SystemType.PERMISSION, message);
            userIsAdmin = false;
            LOGGER.debug(message);
        } else {
            userIsAdmin = true;
        }
    }

    /**
     * Derivat-Filter is an optional filter
     */
    private String getSelectedDerivatName() {
        SingleDerivatFilter derivatFilter = this.systemMonitoringViewFilter.getDerivatFilter();
        if (!derivatFilter.isActive()) {
            return null;
        }
        return derivatFilter.getSelectedValue().getName();
    }

    private void resetZakFilters() {
        // reset global filter value for table
        final FacesContext context = FacesContext.getCurrentInstance();
        final UIViewRoot viewRoot = context.getViewRoot();

        UIComponent globalFilter = viewRoot.findComponent(":systemMonitoringDashboardContent:monitoring-form-table:zak-table:globalFilter");
        if (globalFilter != null) {
            InputText globalFilterInput = (InputText) globalFilter;
            globalFilterInput.setValue("");
        }

        // reset filter values for columns
        DataTable dataTable = (DataTable) viewRoot.findComponent(":systemMonitoringDashboardContent:monitoring-form-table:zak-table");
        if (dataTable != null) {
            dataTable.resetValue();
            dataTable.reset();
            dataTable.setFilters(null);
        }
    }
}
