package de.interfaceag.bmw.pzbk.systemMonitoring.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sf (stefan.fruhwirth@interface-ag.de)
 */
public final class SystemMonitoringViewData implements Serializable {

    private List<SystemMonitoringDto> monitoringDtoList;
    private List<LogEntryDto> logEntryDtosList;
    private LogSummaryInformationDto logSummaryInformationDto;

    private SystemMonitoringViewData(Builder builder) {
        monitoringDtoList = builder.monitoringDtoList;
        logEntryDtosList = builder.logEntryDtosList;
        logSummaryInformationDto = builder.logSummaryInformationDto;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public List<SystemMonitoringDto> getMonitoringDtoList() {
        return monitoringDtoList;
    }

    public List<LogEntryDto> getLogEntryDtosList() {
        return logEntryDtosList;
    }

    public LogSummaryInformationDto getLogSummaryInformationDto() {
        return logSummaryInformationDto;
    }

    public static final class Builder {
        private List<SystemMonitoringDto> monitoringDtoList;
        private List<LogEntryDto> logEntryDtosList;
        private LogSummaryInformationDto logSummaryInformationDto;

        private Builder() {
        }

        public Builder withMonitoringDtoList(List<SystemMonitoringDto> val) {
            monitoringDtoList = new ArrayList<>(val);
            return this;
        }

        public Builder withLogEntryDtosList(List<LogEntryDto> val) {
            logEntryDtosList = new ArrayList<>(val);
            return this;
        }

        public Builder withLogSummaryInformationDto(LogSummaryInformationDto val) {
            logSummaryInformationDto = val;
            return this;
        }

        public SystemMonitoringViewData build() {
            return new SystemMonitoringViewData(this);
        }
    }
}
