package de.interfaceag.bmw.pzbk.systemMonitoring;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.LogEntry;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.enums.LogLevel;
import de.interfaceag.bmw.pzbk.enums.SystemType;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.LogService;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.LogEntryDto;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.LogEntryDtoMapper;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.SystemMonitoringDto;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.SystemMonitoringMapper;
import de.interfaceag.bmw.pzbk.zak.ZakUebertragungService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sf (stefan.fruhwirth@interface-ag.de)
 */
@Stateless
public class SystemMonitoringService implements Serializable {

    @Inject
    private LogService logService;

    @Inject
    private ZakUebertragungService zakUebertragungService;

    @Inject
    private DerivatService derivatService;

    public void addLogEntry(LogLevel info, SystemType permission, String message) {
        this.logService.addLogEntry(info, permission, message);
    }

    public List<LogEntryDto> getZAKLogEntriesForTimeRange(Date startDate, Date endDate) {
        List<LogEntry> logEntries = this.logService.getZAKLogEntriesForTimeRange(startDate, endDate);
        return logEntries.stream().map(LogEntryDtoMapper::buildLogEntryDto).collect(Collectors.toList());
    }

    public List<SystemMonitoringDto> getAllZakUebertragungForTimeRange(Date startDate, Date endDate, String derivatName) {
        List<ZakUebertragung> allZakUebertragungForTimeRange = this.zakUebertragungService.getAllZakUebertragungForTimeRange(startDate, endDate, derivatName);

        return allZakUebertragungForTimeRange.stream()
                .map(SystemMonitoringMapper::buildSystemMonitoringDto).collect(Collectors.toList());
    }

    public List<Derivat> getAllDerivate() {
        return this.derivatService.getAllDerivate();
    }
}
