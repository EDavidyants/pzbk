package de.interfaceag.bmw.pzbk.systemMonitoring.model;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;

/**
 * Helper class for mapping SystemMonitoringDto
 */
public final class SystemMonitoringMapper {

    private SystemMonitoringMapper() { }

    public static SystemMonitoringDto buildSystemMonitoringDto(ZakUebertragung zakUebertragung) {
        if (zakUebertragung == null) {
            return SystemMonitoringDto.newBuilder().build();
        }

        // Note all sub-object from zakUebragung can not be null in case of db config. (null checks not needed)
        DerivatAnforderungModul derivatAnforderungModul = zakUebertragung.getDerivatAnforderungModul();
        Anforderung anforderung = derivatAnforderungModul.getAnforderung();
        Derivat derivat = derivatAnforderungModul.getDerivat();
        ZuordnungAnforderungDerivat zuordnungAnforderungDerivat = derivatAnforderungModul.getZuordnungAnforderungDerivat();

        return SystemMonitoringDto.newBuilder()
                .withAenderungsdatum(zakUebertragung.getAenderungsdatum())
                .withUebertragungId(zakUebertragung.getUebertragungId())
                .withZakRepsonse(zakUebertragung.getZakResponse())
                .withZakId(zakUebertragung.getZakId())
                .withZakstatus(zakUebertragung.getZakStatus())

                .withNachZakUebertragen(zuordnungAnforderungDerivat.isNachZakUebertragen())
                .withZakUebertragungErfolgreich(zuordnungAnforderungDerivat.isZakUebertragungErfolgreich())

                .withAnforderungStatus(anforderung.getStatus())
                .withAnforderungFachId(anforderung.getFachId())

                .withDerivatId(derivat.getId())
                .withDerivatName(derivat.getName())
                .withDerivatProduktlinie(derivat.getProduktlinie())
                .withDerivatStatus(derivat.getStatus())
                .withPreviousDerivatStatus(derivat.getLastStatus())

                .build();
    }
}
