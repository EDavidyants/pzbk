package de.interfaceag.bmw.pzbk.systemMonitoring;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.LogLevel;
import de.interfaceag.bmw.pzbk.enums.SystemType;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.LogEntryDto;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.LogSummaryInformationDto;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.SystemMonitoringDto;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.SystemMonitoringViewData;
import de.interfaceag.bmw.pzbk.systemMonitoring.util.LogEntriesInformationUtil;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author sf (stefan.fruhwirth@interface-ag.de)
 */
@Stateless
public class SystemMonitoringFacade implements Serializable {

    static final String DERIVAT_FILTER_MATCHER = "Derivat ";

    @Inject
    private SystemMonitoringService systemMonitoringService;

    public void addLogEntry(LogLevel info, SystemType permission, String message) {
        this.systemMonitoringService.addLogEntry(info, permission, message);
    }

    /**
     * Hanlding filter changes
     * --> load new ViewData for Uebertragungstable and LogEntryTable.
     */
    public SystemMonitoringViewData changeFilterSetting(Date startDate, Date endDate, String selectedDerivatName) {
        List<LogEntryDto> logEntries = this.getViewDataLogEntries(startDate, endDate);
        LogSummaryInformationDto logSummaryInformation = this.getLogSummaryInformation(logEntries);

        logEntries = LogEntriesInformationUtil.filterByString(logEntries, getSelectedDerivatMatcher(selectedDerivatName));

        return SystemMonitoringViewData.newBuilder()
                .withMonitoringDtoList(this.getViewDataZakUebertragung(startDate, endDate, selectedDerivatName))
                .withLogEntryDtosList(logEntries)
                .withLogSummaryInformationDto(logSummaryInformation)
                .build();
    }

    public List<Derivat> getAllDerivate() {
        return this.systemMonitoringService.getAllDerivate();
    }

    private List<SystemMonitoringDto> getViewDataZakUebertragung(Date startDate, Date endDate, String selectedDerivatName) {
        return this.systemMonitoringService.getAllZakUebertragungForTimeRange(startDate, endDate, selectedDerivatName);
    }

    private List<LogEntryDto> getViewDataLogEntries(Date startDate, Date endDate) {
        return this.systemMonitoringService.getZAKLogEntriesForTimeRange(startDate, endDate);
    }

    private LogSummaryInformationDto getLogSummaryInformation(List<LogEntryDto> logEntryDtos) {
        return LogEntriesInformationUtil.getInformationForLogEntries(logEntryDtos);
    }

    /**
     * get the filter-matcher for derivat name.
     * filter-matcher is a string in case of regex filtering.
     */
    private String getSelectedDerivatMatcher(String derivatName) {
        if (derivatName == null) {
            return null;
        }

        return DERIVAT_FILTER_MATCHER + derivatName;
    }
}
