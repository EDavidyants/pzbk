package de.interfaceag.bmw.pzbk.systemMonitoring.model;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;

import java.io.Serializable;
import java.util.Date;

/**
 * @author sf (stefan.fruhwirth@interface-ag.de)
 */
public final class SystemMonitoringDto implements Serializable {

    private final Date aenderungsdatum;
    private final ZakStatus zakstatus;
    private final Long uebertragungId;
    private final String zakResponse;
    private final String zakId;

    private final Boolean nachZakUebertragen;
    private final Boolean zakUebertragungErfolgreich;

    private final DerivatStatus derivatStatus;
    private final Long derivatId;
    private final String derivatName;
    private final String derivatProduktlinie;
    private final DerivatStatus previousDerivatStatus;

    private final String anforderungFachId;
    private final Status anforderungStatus;

    private SystemMonitoringDto(Builder builder) {
        aenderungsdatum = builder.aenderungsdatum;
        zakstatus = builder.zakstatus;
        uebertragungId = builder.uebertragungId;
        zakResponse = builder.zakRepsonse;
        zakId = builder.zakId;
        derivatStatus = builder.derivatStatus;
        derivatId = builder.derivatId;
        nachZakUebertragen = builder.nachZakUebertragen;
        zakUebertragungErfolgreich = builder.zakUebertragungErfolgreich;
        derivatName = builder.derivatName;
        derivatProduktlinie = builder.derivatProduktlinie;
        previousDerivatStatus = builder.previousDerivatStatus;
        anforderungFachId = builder.anforderungFachId;
        anforderungStatus = builder.anforderungStatus;
    }

    public Date getAenderungsdatum() {
        return aenderungsdatum;
    }

    public ZakStatus getZakstatus() {
        return zakstatus;
    }

    public Long getUebertragungId() {
        return uebertragungId;
    }

    public String getZakResponse() {
        return zakResponse;
    }

    public String getZakId() {
        return zakId;
    }

    public Boolean isNachZakUebertragen() {
        return nachZakUebertragen;
    }

    public Boolean isZakUebertragungErfolgreich() {
        return zakUebertragungErfolgreich;
    }

    public DerivatStatus getDerivatStatus() {
        return derivatStatus;
    }

    public Long getDerivatId() {
        return derivatId;
    }

    public String getDerivatName() {
        return derivatName;
    }

    public String getDerivatProduktlinie() {
        return derivatProduktlinie;
    }

    public DerivatStatus getPreviousDerivatStatus() {
        return previousDerivatStatus;
    }

    public String getAnforderungFachId() {
        return anforderungFachId;
    }

    public Status getAnforderungStatus() {
        return anforderungStatus;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private Date aenderungsdatum;
        private ZakStatus zakstatus;
        private Long uebertragungId;
        private String zakRepsonse;
        private String zakId;
        private DerivatStatus derivatStatus;
        private Long derivatId;
        private Boolean nachZakUebertragen;
        private Boolean zakUebertragungErfolgreich;
        private String derivatName;
        private String derivatProduktlinie;
        private DerivatStatus previousDerivatStatus;
        private String anforderungFachId;
        private Status anforderungStatus;

        private Builder() {
        }

        public Builder withAenderungsdatum(Date aenderungsdatum) {
            this.aenderungsdatum = aenderungsdatum;
            return this;
        }

        public Builder withZakstatus(ZakStatus zakstatus) {
            this.zakstatus = zakstatus;
            return this;
        }

        public Builder withUebertragungId(Long uebertragungId) {
            this.uebertragungId = uebertragungId;
            return this;
        }

        public Builder withZakRepsonse(String zakRepsonse) {
            this.zakRepsonse = zakRepsonse;
            return this;
        }

        public Builder withZakId(String zakId) {
            this.zakId = zakId;
            return this;
        }

        public Builder withDerivatStatus(DerivatStatus derivatStatus) {
            this.derivatStatus = derivatStatus;
            return this;
        }

        public Builder withDerivatId(Long derivatId) {
            this.derivatId = derivatId;
            return this;
        }

        public Builder withNachZakUebertragen(Boolean nachZakUebertragen) {
            this.nachZakUebertragen = nachZakUebertragen;
            return this;
        }

        public Builder withZakUebertragungErfolgreich(Boolean zakUebertragungErfolgreich) {
            this.zakUebertragungErfolgreich = zakUebertragungErfolgreich;
            return this;
        }

        public Builder withDerivatName(String derivatName) {
            this.derivatName = derivatName;
            return this;
        }

        public Builder withDerivatProduktlinie(String derivatProduktlinie) {
            this.derivatProduktlinie = derivatProduktlinie;
            return this;
        }

        public Builder withPreviousDerivatStatus(DerivatStatus previousDerivatStatus) {
            this.previousDerivatStatus = previousDerivatStatus;
            return this;
        }

        public Builder withAnforderungFachId(String anforderungFachId) {
            this.anforderungFachId = anforderungFachId;
            return this;
        }

        public Builder withAnforderungStatus(Status anforderungStatus) {
            this.anforderungStatus = anforderungStatus;
            return this;
        }

        public SystemMonitoringDto build() {
            return new SystemMonitoringDto(this);
        }
    }
}
