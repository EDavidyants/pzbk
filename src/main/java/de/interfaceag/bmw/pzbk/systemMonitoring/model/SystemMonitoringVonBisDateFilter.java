package de.interfaceag.bmw.pzbk.systemMonitoring.model;

import de.interfaceag.bmw.pzbk.filter.AbstractVonBisDateFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

/**
 * @author sf (stefan.fruhwirth@interface-ag.de)
 */
public class SystemMonitoringVonBisDateFilter extends AbstractVonBisDateFilter {

    public static final String PARAMETERPRAEFIX = "zak-uebertragungen";

    public SystemMonitoringVonBisDateFilter(UrlParameter urlParameter) {
        super(PARAMETERPRAEFIX, urlParameter);
    }
}
