package de.interfaceag.bmw.pzbk.systemMonitoring.model;

import de.interfaceag.bmw.pzbk.entities.LogEntry;

public final class LogEntryDtoMapper {

    private LogEntryDtoMapper() {
    }

    public static LogEntryDto buildLogEntryDto(LogEntry logEntry) {

        if (logEntry == null) {
            return null;
        }

        return LogEntryDto.newBuilder()
                .withId(logEntry.getId())
                .withDate(logEntry.getDatum())
                .withLogLevel(logEntry.getLogLevel())
                .withLogValue(logEntry.getLogValue())
                .build();
    }
}
