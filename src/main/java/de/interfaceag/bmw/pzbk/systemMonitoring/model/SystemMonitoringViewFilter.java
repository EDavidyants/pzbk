package de.interfaceag.bmw.pzbk.systemMonitoring.model;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.filter.Filter;
import de.interfaceag.bmw.pzbk.filter.FilterToUrlConverter;
import de.interfaceag.bmw.pzbk.filter.QueryUrlFilter;
import de.interfaceag.bmw.pzbk.filter.SingleDerivatFilter;
import de.interfaceag.bmw.pzbk.filter.VonBisDateSearchFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author sf (stefan.fruhwirth@interface-ag.de)
 */
public class SystemMonitoringViewFilter implements Serializable {

    private static final Page PAGE = Page.SYSTEM_MONITORING;

    @Filter
    private final VonBisDateSearchFilter datumFilter;

    @Filter
    private final SingleDerivatFilter derivatFilter;

    @Filter
    private final QueryUrlFilter queryUrlFilter;

    public SystemMonitoringViewFilter(UrlParameter urlParameter, Collection<Derivat> allDerivate) {
        this.queryUrlFilter = new QueryUrlFilter(urlParameter);
        this.datumFilter = new SystemMonitoringVonBisDateFilter(urlParameter);
        this.derivatFilter = new SingleDerivatFilter(allDerivate, urlParameter);
    }

    public QueryUrlFilter getQueryUrlFilter() {
        return queryUrlFilter;
    }

    public VonBisDateSearchFilter getDatumFilter() {
        return datumFilter;
    }

    public SingleDerivatFilter getDerivatFilter() {
        return this.derivatFilter;
    }

    public String getUrl() {
        return PAGE.getUrl() + getUrlParameter();
    }

    public String getResetUrl() {
        return PAGE.getUrl();
    }

    private String getUrlParameter() {
        FilterToUrlConverter urlConverter = FilterToUrlConverter.forClass(this);
        return urlConverter.getUrl();
    }


}
