package de.interfaceag.bmw.pzbk.systemMonitoring.util;

import de.interfaceag.bmw.pzbk.systemMonitoring.model.LogEntryDto;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.LogSummaryInformationDto;

import java.util.ArrayList;
import java.util.List;

public final class LogEntriesInformationUtil {

    private LogEntriesInformationUtil() {
    }

    public static LogSummaryInformationDto getInformationForLogEntries(List<LogEntryDto> logEntries) {

        LogEntryDto logLastZakPost = null;
        LogEntryDto logLastZakGet = null;

        for (LogEntryDto logEntry : logEntries) {
            if (logEntry.getLogValue().matches("(.*)POST abgeschlossen(.*)")) {
                logLastZakPost = logEntry;
            }
            if (logEntry.getLogValue().matches("(.*)Starte ZAK Update für Derivat (.*)")) {
                logLastZakGet = logEntry;
            }
        }

        return LogSummaryInformationDto.newBuilder()
                .withLastZakPost(logLastZakPost == null ? null : logLastZakPost.getDate())
                .withZakSummary(logLastZakPost == null ? null : logLastZakPost.getLogValue())
                .withLastZakGet(logLastZakGet == null ? null : logLastZakGet.getDate())
                .build();
    }

    public static List<LogEntryDto> filterByString(List<LogEntryDto> logEntries, String matcher) {
        if (matcher == null) {
            return logEntries;
        }

        List<LogEntryDto> result = new ArrayList<>();

        for (LogEntryDto logEntry : logEntries) {
            if (logEntry.getLogValue().matches("(.*)" + matcher + "(.*)")) {
                result.add(logEntry);
            }
        }
        return result;
    }
}
