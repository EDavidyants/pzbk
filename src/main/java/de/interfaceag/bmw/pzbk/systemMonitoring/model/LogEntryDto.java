package de.interfaceag.bmw.pzbk.systemMonitoring.model;

import de.interfaceag.bmw.pzbk.enums.LogLevel;

import java.io.Serializable;
import java.util.Date;

public final class LogEntryDto implements Serializable {

    private final Long id;
    private final Date date;
    private final String logValue;
    private final LogLevel logLevel;

    private LogEntryDto(Builder builder) {
        id = builder.id;
        date = builder.date;
        logValue = builder.logValue;
        logLevel = builder.logLevel;
    }

    public Long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getLogValue() {
        return logValue;
    }

    public LogLevel getLogLevel() {
        return logLevel;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private Long id;
        private Date date;
        private String logValue;
        private LogLevel logLevel;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withDate(Date val) {
            date = val;
            return this;
        }

        public Builder withLogValue(String val) {
            logValue = val;
            return this;
        }

        public Builder withLogLevel(LogLevel val) {
            logLevel = val;
            return this;
        }

        public LogEntryDto build() {
            return new LogEntryDto(this);
        }
    }
}
