package de.interfaceag.bmw.pzbk.testdata;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Auswirkung;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.entities.Werk;
import de.interfaceag.bmw.pzbk.entities.Zielwert;
import de.interfaceag.bmw.pzbk.enums.Status;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class AnforderungBuilder {

    private final Anforderung anforderung;

    private AnforderungBuilder() {
        this.anforderung = new Anforderung();
        anforderung.setErstellungsdatum(new Date());
        anforderung.setAenderungsdatum(new Date());
        anforderung.setZeitpunktStatusaenderung(new Date());
    }

    public static AnforderungBuilder newAnforderung() {
        AnforderungBuilder builder = new AnforderungBuilder();
        return builder;
    }

    public Anforderung build() {
        return anforderung;
    }

    public AnforderungBuilder basedOnMeldung(Meldung meldung) {

        return newAnforderung()
                .withSensorCoc(meldung.getSensorCoc())
                .withSensor(meldung.getSensor())
                .withFestgestelltIn(meldung.getFestgestelltIn())
                .withAuswirkung(meldung.getAuswirkungen())
                .withZielwert(meldung.getZielwert())
                .withBeschreibungStaerkeSchwaeche(meldung.getBeschreibungStaerkeSchwaeche())
                .withBeschreibungDe(meldung.getBeschreibungAnforderungDe())
                .withBeschreibungEn(meldung.getBeschreibungAnforderungEn())
                .withStaerkeSchwaeche(meldung.isStaerkeSchwaeche())
                .withReferenzen(meldung.getReferenzen())
                .withUrsache(meldung.getUrsache())
                .withKommentar(meldung.getKommentarAnforderung())
                .withLoesungsvorschlag(meldung.getLoesungsvorschlag())
                .withWerk(meldung.getWerk())
                .withAnhaenge(meldung.getAnhaenge())
                .withMeldung(meldung);

    }

    public AnforderungBuilder withSensorCoc(SensorCoc sensorCoc) {
        anforderung.setSensorCoc(sensorCoc);
        return this;
    }

    public AnforderungBuilder withSensor(Mitarbeiter sensor) {
        anforderung.setSensor(sensor);
        return this;
    }

    public AnforderungBuilder withFestgestelltIn(FestgestelltIn festgestelltIn) {
        anforderung.addFestgestelltIn(festgestelltIn);
        return this;
    }

    public AnforderungBuilder withFestgestelltIn(Set<FestgestelltIn> festgestelltIn) {
        anforderung.setFestgestelltIn(festgestelltIn);
        return this;
    }

    public AnforderungBuilder withAuswirkung(Auswirkung auswirkung) {
        anforderung.addAuswirkung(auswirkung);
        return this;
    }

    public AnforderungBuilder withAuswirkung(List<Auswirkung> auswirkung) {
        anforderung.setAuswirkungen(auswirkung);
        return this;
    }

    public AnforderungBuilder withZielwert(Zielwert zielwert) {
        anforderung.setZielwert(zielwert);
        return this;
    }

    public AnforderungBuilder withBeschreibungStaerkeSchwaeche(String beschreibungStaerkeSchwaeche) {
        anforderung.setBeschreibungStaerkeSchwaeche(beschreibungStaerkeSchwaeche);
        return this;
    }

    public AnforderungBuilder withStaerkeSchwaeche(boolean starkeSchwaeche) {
        anforderung.setStaerkeSchwaeche(starkeSchwaeche);
        return this;
    }

    public AnforderungBuilder withReferenzen(String referenzen) {
        anforderung.setReferenzen(referenzen);
        return this;
    }

    public AnforderungBuilder withBeschreibungDe(String beschreibungDe) {
        anforderung.setBeschreibungAnforderungDe(beschreibungDe);
        return this;
    }

    public AnforderungBuilder withBeschreibungEn(String beschreibungEn) {
        anforderung.setBeschreibungAnforderungEn(beschreibungEn);
        return this;
    }

    public AnforderungBuilder withUrsache(String ursache) {
        anforderung.setUrsache(ursache);
        return this;
    }

    public AnforderungBuilder withKommentar(String kommentar) {
        anforderung.setKommentarAnforderung(kommentar);
        return this;
    }

    public AnforderungBuilder withLoesungsvorschlag(String loesungsvorschlag) {
        anforderung.setLoesungsvorschlag(loesungsvorschlag);
        return this;
    }

    public AnforderungBuilder withWerk(Werk werk) {
        anforderung.setWerk(werk);
        return this;
    }

    public AnforderungBuilder withAnhang(Anhang anhang) {
        anforderung.addAnhang(anhang);
        return this;
    }

    public AnforderungBuilder withAnhaenge(List<Anhang> anhang) {
        anforderung.setAnhaenge(anhang);
        return this;
    }

    public AnforderungBuilder withFachId(String fachId) {
        anforderung.setFachId(fachId);
        return this;
    }

    public AnforderungBuilder withStatus(Status status) {
        anforderung.setStatus(status);
        return this;
    }

    public AnforderungBuilder withMeldung(Meldung meldung) {
        anforderung.addMeldung(meldung);
        return this;
    }

    public AnforderungBuilder withVersion(int version) {
        anforderung.setVersion(version);
        return this;
    }

    public AnforderungBuilder withPhasenbezug(boolean phasenbezug) {
        anforderung.setPhasenbezug(phasenbezug);
        return this;
    }

    public AnforderungBuilder withZeitpunktUmsetzungsbestaetigung(boolean zeitpunktUmsetzungsbestaetigung) {
        anforderung.setZeitpktUmsetzungsbest(zeitpunktUmsetzungsbestaetigung);
        return this;
    }

    public AnforderungBuilder withPotentialStandardisierung(boolean potentialStandardisierung) {
        anforderung.setPotentialStandardisierung(potentialStandardisierung);
        return this;
    }

    public AnforderungBuilder withTteam(Tteam tteam) {
        anforderung.setTteam(tteam);
        return this;
    }

    /**
     * also automatically creates the matching freigabe if not present!
     *
     * @param umsetzer
     * @return
     */
    public AnforderungBuilder withUmsetzerAndZakFreigabe(Umsetzer umsetzer) {
        anforderung.addUmsetzer(umsetzer);
        return generateFreigabeForUmsetzerIfNecessary(umsetzer, VereinbarungType.ZAK);
    }

    public AnforderungBuilder withUmsetzerAndStdFreigabe(Umsetzer umsetzer) {
        anforderung.addUmsetzer(umsetzer);
        return generateFreigabeForUmsetzerIfNecessary(umsetzer, VereinbarungType.STANDARD);
    }

    private AnforderungBuilder generateFreigabeForUmsetzerIfNecessary(Umsetzer umsetzer, VereinbarungType vereinbarungType) {
        if (!anforderung.getAnfoFreigabeBeiModul().stream()
                .anyMatch(f -> f.getModulSeTeam().getId().equals(umsetzer.getSeTeam().getId()))) {
            AnforderungFreigabeBeiModulSeTeam freigabe = new AnforderungFreigabeBeiModulSeTeam(anforderung, umsetzer.getSeTeam());
            freigabe.setVereinbarungType(vereinbarungType);
            anforderung.addAnfoFreigabeBeiModul(freigabe);
        }
        return this;
    }
}
