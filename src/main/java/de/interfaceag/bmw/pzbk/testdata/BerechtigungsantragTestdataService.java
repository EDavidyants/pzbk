package de.interfaceag.bmw.pzbk.testdata;

import de.interfaceag.bmw.pzbk.berechtigungantrag.BerechtigungAntragService;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author rohit
 */
@Stateless
public class BerechtigungsantragTestdataService implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(BerechtigungsantragTestdataService.class);

    private static final Integer MAX_ANZAHL_MITARBEITER = 6;

    private static final Integer MAX_ANTRAEGE_PRO_MITARBEITER = 6;

    @Inject
    private BerechtigungAntragService berechtigungAntragService;

    @Inject
    private SensorCocService sensorCocService;

    @Inject
    private TteamService tteamService;

    @Inject
    private ModulService modulService;

    @Inject
    private UserSearchService userSearchService;

    @Inject
    private BerechtigungService berechtigungService;


    public void generateTestdata() {
        LOGGER.debug("Generating test daten for BerechtigungsAntrag");
        List<Mitarbeiter> mitarbeiterOhneBerechtigung = getMitarbeiterGroupOhneBerechtigung();
        mitarbeiterOhneBerechtigung.forEach(this::generateMultipleAntraegeProMitarbeiter);

    }


    private List<Mitarbeiter> getMitarbeiterGroupOhneBerechtigung() {
        List<Mitarbeiter> alleMitarbeiter = userSearchService.getAllMitarbeiter();
        return getRandomMitarbeitersOhneRolle(alleMitarbeiter, MAX_ANZAHL_MITARBEITER);

    }

    private void generateMultipleAntraegeProMitarbeiter(Mitarbeiter mitarbeiter) {
        int anzahlRandomAntraegeProMitarbiter = RandomUtils.getRandomForSize(MAX_ANTRAEGE_PRO_MITARBEITER);
        for (int antragIndex = 0; antragIndex < anzahlRandomAntraegeProMitarbiter; antragIndex++) {
            generateRandomBerechtigungsAntrag(mitarbeiter);
        }
    }


    private void generateRandomBerechtigungsAntrag(Mitarbeiter mitarbeiter) {
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag();
        berechtigungsantrag.setAntragsteller(mitarbeiter);
        berechtigungsantrag.setRolle(getRandomRolle());
        berechtigungsantrag.setRechttype(getRandomRechttype(berechtigungsantrag));

        switch (berechtigungsantrag.getRolle()) {
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
                berechtigungsantrag.setSensorCoc(getRandomSensorCoc());
                break;
            case E_COC:
                berechtigungsantrag.setModulSeTeam(getRandomModulSeTeam());
                break;
            case TTEAMMITGLIED:
                berechtigungsantrag.setTteam(getRandomTteam());
                break;
            default:
                LOGGER.error("Nicht erlaubter rolle fuer berechtigungsantrag!");
                break;

        }

        berechtigungAntragService.persistBerechtigungAntrag(berechtigungsantrag);

    }

    private Rolle getRandomRolle() {
        List<Rolle> erlaubteAntragErstellerRolle = Arrays.asList(Rolle.SENSOR, Rolle.SENSOR_EINGESCHRAENKT, Rolle.E_COC, Rolle.TTEAMMITGLIED);
        int index = RandomUtils.getRandomForSize(erlaubteAntragErstellerRolle.size());
        return erlaubteAntragErstellerRolle.get(index);
    }

    private Rechttype getRandomRechttype(Berechtigungsantrag berechtigungsAntrag) {
        List<Rechttype> erlaubteAntragErstellerRechttype = Arrays.asList(Rechttype.LESERECHT, Rechttype.SCHREIBRECHT);
        int index = RandomUtils.getRandomForSize(erlaubteAntragErstellerRechttype.size());
        if (berechtigungsAntrag.getRolle().equals(Rolle.E_COC)) {
            return Rechttype.SCHREIBRECHT;
        }
        return erlaubteAntragErstellerRechttype.get(index);
    }

    private SensorCoc getRandomSensorCoc() {
        List<SensorCoc> sensorCocs = sensorCocService.getAllSortByTechnologie();
        int index = RandomUtils.getRandomForSize(sensorCocs.size());
        return sensorCocs.get(index);
    }

    private Tteam getRandomTteam() {
        List<Tteam> allTteams = tteamService.getAllTteams();
        int index = RandomUtils.getRandomForSize(allTteams.size());
        return allTteams.get(index);
    }

    private ModulSeTeam getRandomModulSeTeam() {
        List<ModulSeTeam> allModulSeTeam = modulService.getAllSeTeams();
        int index = RandomUtils.getRandomForSize(allModulSeTeam.size());
        return allModulSeTeam.get(index);
    }

    private List<Mitarbeiter> getRandomMitarbeitersOhneRolle(List<Mitarbeiter> mitarbeiters, Integer anzahlMitarbeiter) {
        Set<Mitarbeiter> uniqueMitarbeiterOhneRolle = mitarbeiters.stream()
                .filter(mitarbeiter -> berechtigungService.getRolesForUser(mitarbeiter).isEmpty())
                .collect(Collectors.toSet());

        List<Mitarbeiter> mitarbeiterOhneRolle = new ArrayList<>(uniqueMitarbeiterOhneRolle);
        Collections.shuffle(mitarbeiterOhneRolle);
        return mitarbeiterOhneRolle.subList(0, anzahlMitarbeiter);
    }

}
