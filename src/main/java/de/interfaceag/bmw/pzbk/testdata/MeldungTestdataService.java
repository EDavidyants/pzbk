package de.interfaceag.bmw.pzbk.testdata;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.Auswirkung;
import de.interfaceag.bmw.pzbk.entities.Berechtigung;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Werk;
import de.interfaceag.bmw.pzbk.entities.Zielwert;
import de.interfaceag.bmw.pzbk.enums.Attribut;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.WerkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class MeldungTestdataService implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MeldungTestdataService.class.getName());
    
    private static final String STATUS = "Status";
    private static final String ENTWURF = "Entwurf";
    private static final String MAX_ADMIN = "Max Admin";
    private static final String GEMELDET = "gemeldet";
    private static final String UNSTIMMIG = "unstimmig";

    private Random random = new Random();
    
    @Inject
    private AnforderungService anforderungService;
    @Inject
    private ConfigService configService;
    @Inject
    private WerkService werkService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private AnforderungMeldungHistoryService anforderungMeldungHistoryService;

    public void generateMeldungTestdata() {
        LOGGER.debug("start Meldung testdata generation");
        int count = 0;
        for (int i = 0; i < 30; i++) {
            if (generateMeldungWithRandomValues()) {
                count++;
            }
        }
        LOGGER.debug("{} Meldungen generated", count);
    }

    private Mitarbeiter getSensorForSensorCoc(SensorCoc sensorCoc) {
        Mitarbeiter mitarbeiter = null;
        String sensorCocId = sensorCoc.getSensorCocId().toString();
        Berechtigung bere = berechtigungService.getByFuerIdFuerRolleRechttype(sensorCocId, Rolle.SENSOR, BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT);
        if (bere != null) {
            mitarbeiter = bere.getMitarbeiter();
        } else {
            bere = berechtigungService.getByFuerIdFuerRolleRechttype(sensorCocId, Rolle.SENSORCOCLEITER, BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT);
            if (bere != null) {
                mitarbeiter = bere.getMitarbeiter();
            }
        }
        return mitarbeiter;
    }

    private boolean generateMeldungWithRandomValues() {
        SensorCoc sensorCoc = getRandomSensorCoc();
        if (sensorCoc == null) {
            LOGGER.error("sensorCoc is null");
            return false;
        }
        Mitarbeiter sensor = getSensorForSensorCoc(sensorCoc);
        if (sensor == null) {
            LOGGER.error("no sensor found for SensorCoc {}", sensorCoc);
            return false;
        }
        Status status = getRandomMeldungStatus();
        if (status.equals(Status.M_ZUGEORDNET)) {
            status = Status.M_GEMELDET;
        }
        generateMeldung(sensorCoc, sensor, status);
        return true;
    }

    private Date generatePastDateFromDateInWeeks(Date baseDate, Long weeks) {
        LocalDate currentLocal = Instant.ofEpochMilli(baseDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate datumWithDelta = currentLocal.minusWeeks(weeks);
        return Date.from(datumWithDelta.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    private Date generateDateFromDateInWeeks(Date baseDate, Long weeks) {
        LocalDate currentLocal = Instant.ofEpochMilli(baseDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate datumWithDelta = currentLocal.plusWeeks(weeks);
        return Date.from(datumWithDelta.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    private Date generateDateFromDateInDays(Date baseDate, Long days) {
        LocalDate currentLocal = Instant.ofEpochMilli(baseDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate datumWithDelta = currentLocal.plusDays(days);
        return Date.from(datumWithDelta.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public Meldung generateMeldung(SensorCoc sensorCoc, Mitarbeiter sensor, Status status) {

        Meldung newMeldung = MeldungBuilder.newMeldung()
                .withSensorCoc(sensorCoc)
                .withSensor(sensor)
                .withFestgestelltIn(getRandomFestgestelltIn())
                .withFestgestelltIn(getRandomFestgestelltIn())
                .withAuswirkung(generateAuswirkung())
                .withAuswirkung(generateAuswirkung())
                .withZielwert(generateZielwert())
                .withBeschreibungStaerkeSchwaeche(getRandomBeschreibungStaerkeSchwaeche())
                .withBeschreibungDe(getRandomBeschreibungDe())
                .withBeschreibungEn(getRandomBeschreibungDe())
                .withStaerkeSchwaeche(generateStaerkeSchwaeche())
                .withReferenzen(getRandomReferenzen())
                .withUrsache(getRandomUrsache())
                .withKommentar(getRandomKommentar())
                .withLoesungsvorschlag(getRandomLoesungsvorschlag())
                .withWerk(getRandomWerk())
                .withStatus(status)
                .build();
        //.withAnhang(getRandomAnhang(sensor))

        Date generatedErstellungsDatum = generatePastDateFromDateInWeeks(newMeldung.getErstellungsdatum(), 10L);
        newMeldung.setErstellungsdatum(generatedErstellungsDatum);
        Long id = anforderungService.saveTestMeldung(newMeldung);
        newMeldung.setFachId("M" + id);
        newMeldung.setStatus(status);
        anforderungService.saveTestMeldung(newMeldung);
        generatHistoryForMeldung(newMeldung, generatedErstellungsDatum);
        LOGGER.debug("created new Meldung {} with status {}", newMeldung, status);

        return newMeldung;
    }

    private void generatHistoryForMeldung(Meldung meldung, Date baseDate) {
        Status status = meldung.getStatus();

        int randomNum = random.nextInt(2) + 1;
        switch (status) {
            case M_ENTWURF:
                break;
            case M_GEMELDET:
                if (randomNum == 1) {
                    writeHistoryForGemeldet(meldung, baseDate);
                } else if (randomNum == 2) {
                    writeHistoryForGemeldetOverUnstimmig(meldung, baseDate);
                }
                break;
            case M_UNSTIMMIG:
                if (randomNum == 1) {
                    writeHistoryForGemeldetToUnstimmig(meldung, baseDate);
                } else if (randomNum == 2) {
                    writeHistoryForGemeldetToUnstimmigWithLoop(meldung, baseDate);
                }

                break;
            case M_ZUGEORDNET:
                if (randomNum == 1) {
                    writeHistoryForDirektZugeordnet(meldung, baseDate);
                } else if (randomNum == 2) {
                    writeHistoryForZugeordnetOverUnstimmig(meldung, baseDate);
                }

                break;
            case M_GELOESCHT:
                writeHistoryForGeloeschtFromUnstimmig(meldung, baseDate);
                break;
            default:
                break;
        }
    }

    private void writeHistoryForDirektZugeordnet(Meldung meldung, Date baseDate) {
        Long meldungId = meldung.getId();

        Date halfAWeekFromNow = generateDateFromDateInDays(baseDate, 3L);
        Date threeAndAHalfWeeksFromNow = generateDateFromDateInDays(baseDate, 24L);

        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", halfAWeekFromNow, STATUS, MAX_ADMIN, ENTWURF, GEMELDET));
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", threeAndAHalfWeeksFromNow, STATUS, MAX_ADMIN, GEMELDET, "zugeordnet"));
    }

    private void writeHistoryForZugeordnetOverUnstimmig(Meldung meldung, Date baseDate) {
        Long meldungId = meldung.getId();

        Date halfAWeekFromNow = generateDateFromDateInDays(baseDate, 3L);
        Date weekFromNow = generateDateFromDateInWeeks(baseDate, 1L);
        Date twoWeeksFromNow = generateDateFromDateInWeeks(baseDate, 2L);
        Date threeAndAHalfWeeksFromNow = generateDateFromDateInDays(baseDate, 24L);

        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", halfAWeekFromNow, STATUS, MAX_ADMIN, ENTWURF, GEMELDET));
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", weekFromNow, STATUS, MAX_ADMIN, GEMELDET, UNSTIMMIG));
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", twoWeeksFromNow, STATUS, MAX_ADMIN, UNSTIMMIG, GEMELDET));
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", threeAndAHalfWeeksFromNow, STATUS, MAX_ADMIN, GEMELDET, "zugeordnet"));
    }

    private void writeHistoryForGeloeschtFromUnstimmig(Meldung meldung, Date baseDate) {

        Long meldungId = meldung.getId();

        Date halfAWeekFromNow = generateDateFromDateInDays(baseDate, 3L);
        Date weekFromNow = generateDateFromDateInWeeks(baseDate, 1L);
        Date fourWeeksFromNow = generateDateFromDateInWeeks(baseDate, 4L);

        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", halfAWeekFromNow, STATUS, MAX_ADMIN, ENTWURF, GEMELDET));
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", weekFromNow, STATUS, MAX_ADMIN, GEMELDET, UNSTIMMIG));
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", fourWeeksFromNow, STATUS, MAX_ADMIN, UNSTIMMIG, "gelöscht"));

    }

    private void writeHistoryForGemeldetToUnstimmigWithLoop(Meldung meldung, Date baseDate) {
        Long meldungId = meldung.getId();

        Date halfAWeekFromNow = generateDateFromDateInDays(baseDate, 3L);
        Date weekFromNow = generateDateFromDateInWeeks(baseDate, 1L);
        Date twoWeeksFromNow = generateDateFromDateInWeeks(baseDate, 2L);
        Date twoAndAHalfWeekFromNow = generateDateFromDateInDays(baseDate, 17L);

        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", halfAWeekFromNow, STATUS, MAX_ADMIN, ENTWURF, GEMELDET));
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", weekFromNow, STATUS, MAX_ADMIN, GEMELDET, UNSTIMMIG));
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", twoWeeksFromNow, STATUS, MAX_ADMIN, UNSTIMMIG, GEMELDET));
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", twoAndAHalfWeekFromNow, STATUS, MAX_ADMIN, GEMELDET, UNSTIMMIG));

    }

    private void writeHistoryForGemeldetToUnstimmig(Meldung meldung, Date baseDate) {
        Long meldungId = meldung.getId();

        Date halfAWeekFromNow = generateDateFromDateInDays(baseDate, 3L);
        Date weekFromNow = generateDateFromDateInWeeks(baseDate, 1L);

        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", halfAWeekFromNow, STATUS, MAX_ADMIN, ENTWURF, GEMELDET));
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", weekFromNow, STATUS, MAX_ADMIN, GEMELDET, UNSTIMMIG));

    }

    private void writeHistoryForGemeldetOverUnstimmig(Meldung meldung, Date baseDate) {
        Long meldungId = meldung.getId();

        Date halfAWeekFromNow = generateDateFromDateInDays(baseDate, 3L);
        Date weekFromNow = generateDateFromDateInWeeks(baseDate, 1L);
        Date twoWeeksFromNow = generateDateFromDateInWeeks(baseDate, 2L);

        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", halfAWeekFromNow, STATUS, MAX_ADMIN, ENTWURF, GEMELDET));
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", weekFromNow, STATUS, MAX_ADMIN, GEMELDET, UNSTIMMIG));
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", twoWeeksFromNow, STATUS, MAX_ADMIN, UNSTIMMIG, GEMELDET));
    }

    private void writeHistoryForGemeldet(Meldung meldung, Date baseDate) {
        Long meldungId = meldung.getId();
        Date halfAWeekFromNow = generateDateFromDateInDays(baseDate, 3L);
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(meldungId, "M", halfAWeekFromNow, STATUS, MAX_ADMIN, ENTWURF, GEMELDET));
    }

    public SensorCoc getRandomSensorCoc() {
        List<SensorCoc> sensorCocList = sensorCocService.getAllSortByTechnologie();
        int index = RandomUtils.getRandomForSize(sensorCocList.size());
        return sensorCocList.get(index);
    }

    public static Status getRandomMeldungStatus() {
        List<Status> allStatus = Status.selectStatusesOfMeldung();
        int index = RandomUtils.getRandomForSize(allStatus.size());
        return allStatus.get(index);
    }

    public FestgestelltIn getRandomFestgestelltIn() {
        List<FestgestelltIn> allFestgestelltIn = anforderungService.getAllFestgestelltIn();
        int index = RandomUtils.getRandomForSize(allFestgestelltIn.size());
        return allFestgestelltIn.get(index);
    }

    public Auswirkung generateAuswirkung() {
        List<String> auswirkungen = configService.getWertStringByAttribut(Attribut.AUSWIRKUNG);

        int index = RandomUtils.getRandomForSize(auswirkungen.size());

        Auswirkung auswirkung = new Auswirkung();
        auswirkung.setAuswirkung(auswirkungen.get(index));
        auswirkung.setWert(getRandomAuswirkungWert());
        return auswirkung;

    }

    public Werk getRandomWerk() {
        List<Werk> allWerke = werkService.getAllWerk();
        int index = RandomUtils.getRandomForSize(allWerke.size());
        return allWerke.get(index);
    }

    public static boolean generateStaerkeSchwaeche() {
        return RandomUtils.getRandomBoolean();
    }

    public Zielwert generateZielwert() {
        Zielwert zielwert = new Zielwert();
        zielwert.setWert(getRandomZielwertWert());
        zielwert.setKommentar(getRandomZielwertKommentar());
        return zielwert;
    }

    public static String getRandomUrsache() {
        List<String> values = Arrays.asList(
                "Das Dach ist undicht!",
                "Es wurde vergessen den Motor klein genug zu bauen.",
                "Der Lack wurde nicht entsprechend angemischt",
                "Vögel fliegen frei in der Halle.");
        int index = RandomUtils.getRandomForSize(values.size());
        return values.get(index);
    }

    public static String getRandomBeschreibungDe() {
        List<String> values = Arrays.asList(
                "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
                "Gallia est omnis divisa in partes tres, quarum unam incolunt Belgae, aliam Aquitani, tertiam qui ipsorum lingua Celtae, nostra Galli appellantur. Hi omnes lingua, institutis, legibus inter se differunt. Gallos ab Aquitanis Garumna flumen, a Belgis Matrona et Sequana dividit. Horum omnium fortissimi sunt Belgae, propterea quod a cultu atque humanitate provinciae longissime absunt, minimeque ad eos mercatores saepe commeant atque ea quae ad effeminandos animos pertinent important, proximique sunt Germanis, qui trans Rhenum incolunt, quibuscum continenter bellum gerunt.",
                "Wir begleiten Unternehmen und Behörden auf IHREM WEG der Digitalen Transformation mit Blick auf unsere Fokusthemen Softwareentwicklung, DevOps und ...",
                "In nova fert animus mutatas dicere formas\n"
                + "corpora; di, coeptis (nam vos mutastis et illas)\n"
                + "adspirate meis primaque ab origine mundi\n"
                + "ad mea perpetuum deducite tempora carmen!",
                "Begonnen hat der Weg der InterFace AG vor über 30 Jahren als Produktanbieter im Bereich Unix-Textverarbeitung. Daraus hat sich über die Jahre ein modernes Software-Projekthaus mit den Schwerpunkten Java/JEE und .NET entwickelt. Der Anspruch des Unternehmens war es von Anfang an, einen engen Bezug zwischen Entwicklung, Implementierung und Betrieb herzustellen – und so auch in dieser frühen Phase der Digitalisierung den Begriff “Digitale Transformation” noch vor seiner “Erfindung” aktiv zu leben.",
                "Durch eine iterative Softwareentwicklung in agilen Projektteams (SCRUM-Methodik) sowie ein automatisiertes Testing realisiert die InterFace AG eine kontinuierliche An­pas­sung und Qua­li­täts­­si­che­rung der entwickelten Softwarelösungen – über den Rollout hinaus. Die Be­reit­stel­lung al­ler not­wen­di­gen Re­ssour­cen bei en­ger, me­tho­di­scher Zu­sam­men­ar­beit zw­ischen Kun­den und Dienst­­leis­ter gewährleistet Performance, Sicherheit und Anwenderfreundlichkeit im täglichen Betrieb. Am Ende steht ein re­li­ables System, das das ge­sam­te An­wen­dungs­­spek­trum der eingesetzten Software berücksichtigt."
        );
        int index = RandomUtils.getRandomForSize(values.size());
        return values.get(index);
    }

    public static String getRandomReferenzen() {
        List<String> values = Arrays.asList(
                "Wir sind die Referenz!",
                "Tritt unter Anderem beim Fahrrad meines Kollegen auf.",
                "Betrifft alle Cabrios mit Stoffdach.");
        int index = RandomUtils.getRandomForSize(values.size());
        return values.get(index);
    }

    public static String getRandomBeschreibungStaerkeSchwaeche() {
        List<String> values = Arrays.asList(
                "Diese Stärke ist sehr stark.",
                "Wenn jemand Zeit hat für sinnvolle Text kann er sie hier einfügen.",
                "Das haben wir uns bei Audi abgeschaut.",
                "So ein Anfängerfehler sollte uns nicht passieren.");
        int index = RandomUtils.getRandomForSize(values.size());
        return values.get(index);
    }

    public static String getRandomKommentar() {
        List<String> values = Arrays.asList(
                "do it or do not there is no try",
                "No, I am your father!");
        int index = RandomUtils.getRandomForSize(values.size());
        return values.get(index);
    }

    public static String getRandomLoesungsvorschlag() {
        List<String> values = Arrays.asList(
                "Lass uns Kleben statt Bohren!",
                "Das Dach muss größer werden.",
                "Wir haben noch keinen Plan ...",
                "456");
        int index = RandomUtils.getRandomForSize(values.size());
        return values.get(index);
    }

    public static String getRandomZielwertWert() {
        List<String> values = Arrays.asList("42", "12€", "bessere Leistung");
        int index = RandomUtils.getRandomForSize(values.size());
        return values.get(index);
    }

    public static String getRandomZielwertKommentar() {
        List<String> values = Arrays.asList(
                "Das ist der Kommentar zum Zielwert",
                "Das ist ein anderer Kommentar zum Zielwert"
        );
        int index = RandomUtils.getRandomForSize(values.size());
        return values.get(index);
    }

    public static String getRandomAuswirkungWert() {
        List<String> values = Arrays.asList(
                "42",
                "DER Wert der Auswirkung");
        int index = RandomUtils.getRandomForSize(values.size());
        return values.get(index);
    }

}
