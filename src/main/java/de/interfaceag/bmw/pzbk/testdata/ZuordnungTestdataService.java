package de.interfaceag.bmw.pzbk.testdata;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class ZuordnungTestdataService implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZuordnungTestdataService.class.getName());
    private static final int MAX = 50;

    @Inject
    private AnforderungService anforderungService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private UserSearchService userSearchService;
    @Inject
    private ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;
    @Inject
    private DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    private ZakTestDataService zakTestDataService;

    public void generateZuordnungTestdata() {
        LOGGER.debug("start Zuordnung testdata generation for E20");

        Optional<Mitarbeiter> maxAdmin = Optional.ofNullable(userSearchService.getMitarbeiterByQnumber("q2222222"));

        if (!maxAdmin.isPresent()) {
            LOGGER.warn("user q2222222 is not present");
            return;
        }

        Mitarbeiter mitarbeiter = maxAdmin.get();

        generateZuordnungTestdataForDerivatByName("E20", mitarbeiter);
        generateZuordnungTestdataForDerivatByName("E21", mitarbeiter);
        generateZuordnungTestdataForDerivatByName("E22", mitarbeiter);
        generateZuordnungTestdataForDerivatByName("E23", mitarbeiter);
        generateZuordnungTestdataForDerivatByName("E24", mitarbeiter);

        LOGGER.debug("Zuordnung testdata generated");
    }

    private void generateZuordnungTestdataForDerivatByName(String derivatName, Mitarbeiter user) {
        Optional<Derivat> derivat = Optional.ofNullable(derivatService.getDerivatByName(derivatName));
        if (!derivat.isPresent()) {
            LOGGER.warn("derivat {} is not present", derivatName);
        } else {
            generateZuordnungTestdataForDerivat(derivat.get(), user);
        }
    }

    private void generateZuordnungTestdataForDerivat(Derivat derivat, Mitarbeiter user) {
        int count = 0;

        derivatService.updateDerivatStatus(derivat, DerivatStatus.VEREINARBUNG_VKBG);
        List<Anforderung> anforderungen = anforderungService.getAllAnforderungenByStatus(Status.A_FREIGEGEBEN);
        Collections.reverse(anforderungen);
        for (Anforderung anforderung : anforderungen) {

            generateZuordnungWithStatus(ZuordnungStatus.ZUGEORDNET, anforderung, derivat, user);
            count++;
            if (count >= MAX) {
                break;
            }
        }

        updateSelectedDerivatForUser(derivat, user);
        konfigureAndUpdateKovaPhaseForDerivat(derivat);
    }

    private void generateZuordnungWithStatus(ZuordnungStatus status,
                                             Anforderung anforderung, Derivat derivat, Mitarbeiter user) {
        if (anforderung == null) {
            LOGGER.warn("anforderung is not present");
            return;
        }

        Optional<ZuordnungAnforderungDerivat> result = Optional.ofNullable(
                zuordnungAnforderungDerivatService.assignAnforderungToDerivat(
                        status, anforderung, derivat, "", user));

        if (result.isPresent()) {
            ZuordnungAnforderungDerivat anforderungDerivat = result.get();
            LOGGER.debug("{} was created", anforderungDerivat);
            updateZakStatusForVereinbarungen(anforderungDerivat);
        } else {
            LOGGER.warn("nothing was created");
        }
    }

    private void updateZakStatusForVereinbarungen(ZuordnungAnforderungDerivat anforderungDerivat) {
        Anforderung anforderung = anforderungDerivat.getAnforderung();
        Derivat derivat = anforderungDerivat.getDerivat();
        List<DerivatAnforderungModul> vereinbarungen = derivatAnforderungModulService.getDerivatAnforderungModulByAnforderungAndDerivat(anforderung, derivat);
        Iterator<DerivatAnforderungModul> iterator = vereinbarungen.iterator();
        while (iterator.hasNext()) {
            DerivatAnforderungModul vereinbarung = iterator.next();
            ZakStatus zakStatus = getRandomZakStatus();
            DerivatAnforderungModulStatus vereinbarungStatus = getRandomVereinbarungStatus(zakStatus);
            vereinbarung.setStatusVKBG(vereinbarungStatus);
            vereinbarung.setStatusZV(vereinbarungStatus);
            LOGGER.debug("Update ZakStatus for {} to {} with VereinbarungZV Status {}", vereinbarung, zakStatus, vereinbarungStatus);
            derivatAnforderungModulService.updateDerivatAnforderungModulZakStatus(zakStatus, "", vereinbarung);
            zakTestDataService.generateZakUebertragung(vereinbarung, zakStatus, new Date());
        }
    }

    private static ZakStatus getRandomZakStatus() {
        int index = getRandomZakStatusIndex();
        ZakStatus result = ZakStatus.values()[index];
        return result;
    }

    private static int getRandomZakStatusIndex() {
        int length = ZakStatus.values().length;
        return getRandomForLength(length);
    }

    private static int getRandomForLength(int length) {
        double random = Math.random();
        Double floor = Math.floor(random * length);
        return floor.intValue();
    }

    private static DerivatAnforderungModulStatus getRandomVereinbarungStatus(ZakStatus zakStatus) {
        if (zakStatus == ZakStatus.PENDING) {
            return DerivatAnforderungModulStatus.ANGENOMMEN;
        }

        return getRandomVereinbarungStatus();
    }

    private static DerivatAnforderungModulStatus getRandomVereinbarungStatus() {
        List<Integer> vorhandenenStatus = new ArrayList<>();
        for (DerivatAnforderungModulStatus status : DerivatAnforderungModulStatus.values()) {
            vorhandenenStatus.add(status.getStatusId());
        }

        int randomStatusIndex = getRandomForLength(vorhandenenStatus.size());
        final Integer randomStatusId = vorhandenenStatus.get(randomStatusIndex);
        return DerivatAnforderungModulStatus.getStatusById(randomStatusId);
    }

    private void updateSelectedDerivatForUser(Derivat derivat, Mitarbeiter user) {
        List<Derivat> selectedDerivate = Arrays.asList(derivat);
        derivatService.setUserSelectedDerivate(user, selectedDerivate);
    }

    private void konfigureAndUpdateKovaPhaseForDerivat(Derivat derivat) {
        List<KovAPhaseImDerivat> kovAPhasen = derivat.getKovAPhasen();

        if (kovAPhasen != null && kovAPhasen.size() > 1) {
            KovAPhaseImDerivat kovAPhaseImDerivat = kovAPhasen.get(0);
            LocalDate now = LocalDate.now();
            kovAPhaseImDerivat.setStartDate(now);
            LocalDate end = LocalDate.ofYearDay(now.getYear(), now.getDayOfYear() + 1);
            kovAPhaseImDerivat.setEndDate(end);
            kovAPhaseImDerivat.setKovaStatus(KovAStatus.AKTIV);
            derivatService.persistKovAPhaseImDerivat(kovAPhaseImDerivat);

            kovAPhaseImDerivat = kovAPhasen.get(1);
            now = LocalDate.now();
            now = LocalDate.ofYearDay(now.getYear(), now.getDayOfYear() + 1);
            kovAPhaseImDerivat.setStartDate(now);
            end = LocalDate.ofYearDay(now.getYear(), now.getDayOfYear() + 1);
            kovAPhaseImDerivat.setEndDate(end);
            kovAPhaseImDerivat.setKovaStatus(KovAStatus.AKTIV);
            derivatService.persistKovAPhaseImDerivat(kovAPhaseImDerivat);
        }
    }
}
