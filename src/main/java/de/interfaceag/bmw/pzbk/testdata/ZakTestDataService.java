package de.interfaceag.bmw.pzbk.testdata;

import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.zak.ZakVereinbarungService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author fn
 */
@Stateless
@Named
public class ZakTestDataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZakTestDataService.class.getName());

    @Inject
    private ZakVereinbarungService zakVereinbarungService;

    private Random random = new Random();

    public void generateZakUebertragung(DerivatAnforderungModul derivatAnforderungModul, ZakStatus zakStatus, Date erstellungsdatum) {

        Long uebertragungsId = zakVereinbarungService.generateZakUebertragungId(derivatAnforderungModul.getAnforderung(), derivatAnforderungModul.getModul());
        ZakUebertragung zakUebertragung = new ZakUebertragung(derivatAnforderungModul, zakStatus, uebertragungsId, erstellungsdatum);
        Integer rand = random.nextInt(10000);
        String zakId = rand.toString() + derivatAnforderungModul.getId().toString();
        zakUebertragung.setZakId(zakId);
        zakVereinbarungService.persistZakUebertragung(zakUebertragung);
        LOGGER.debug("Create ZakUebertragung for {} to {}", derivatAnforderungModul, zakStatus);

    }
}
