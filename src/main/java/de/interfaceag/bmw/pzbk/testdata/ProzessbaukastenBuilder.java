package de.interfaceag.bmw.pzbk.testdata;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.Tteam;

import java.util.List;

/**
 *
 * @author evda
 */
public final class ProzessbaukastenBuilder {

    private final Prozessbaukasten prozessbaukasten;

    private ProzessbaukastenBuilder() {
        this.prozessbaukasten = new Prozessbaukasten();
    }

    public static ProzessbaukastenBuilder newProzessbaukasten() {
        return new ProzessbaukastenBuilder();
    }

    public Prozessbaukasten build() {
        return prozessbaukasten;
    }

    public ProzessbaukastenBuilder withFachId(String fachId) {
        prozessbaukasten.setFachId(fachId);
        return this;
    }

    public ProzessbaukastenBuilder withVersion(int version) {
        prozessbaukasten.setVersion(version);
        return this;
    }

    public ProzessbaukastenBuilder withBezeichnung(String bezeichnung) {
        prozessbaukasten.setBezeichnung(bezeichnung);
        return this;
    }

    public ProzessbaukastenBuilder withBeschreibung(String beschreibung) {
        prozessbaukasten.setBeschreibung(beschreibung);
        return this;
    }

    public ProzessbaukastenBuilder withTteam(Tteam tteam) {
        prozessbaukasten.setTteam(tteam);
        return this;
    }

    public ProzessbaukastenBuilder withLeadTechnologie(Mitarbeiter leadTechnologie) {
        prozessbaukasten.setLeadTechnologie(leadTechnologie);
        return this;
    }

    public ProzessbaukastenBuilder withStandardisierterFertigungsprozess(String standardisierterFertigungsprozess) {
        prozessbaukasten.setStandardisierterFertigungsprozess(standardisierterFertigungsprozess);
        return this;
    }

    public ProzessbaukastenBuilder withStartbrief(Anhang startbrief) {
        prozessbaukasten.setStartbrief(startbrief);
        return this;
    }

    public ProzessbaukastenBuilder withGrafikUmfang(Anhang grafikUmfang) {
        prozessbaukasten.setGrafikUmfang(grafikUmfang);
        return this;
    }

    public ProzessbaukastenBuilder withKonzeptbaum(Anhang konzeptbaum) {
        prozessbaukasten.setKonzeptbaum(konzeptbaum);
        return this;
    }

    public ProzessbaukastenBuilder withBeauftragungTPK(Anhang beauftragungTPK) {
        prozessbaukasten.setBeauftragungTPK(beauftragungTPK);
        return this;
    }

    public ProzessbaukastenBuilder withGenehmigungTPK(Anhang genehmigungTPK) {
        prozessbaukasten.setGenehmigungTPK(genehmigungTPK);
        return this;
    }

    public ProzessbaukastenBuilder withWeitereAnhaenge(List<Anhang> weitereAnhaenge) {
        prozessbaukasten.setWeitereAnhaenge(weitereAnhaenge);
        return this;
    }

    public ProzessbaukastenBuilder withWeiteremAnhang(Anhang weitererAnhang) {
        prozessbaukasten.addWeiterenAnhang(weitererAnhang);
        return this;
    }

    public ProzessbaukastenBuilder withKonzept(Konzept konzept) {
        prozessbaukasten.addKonzept(konzept);
        return this;
    }

    public ProzessbaukastenBuilder withErstanlaeufer(Derivat derivat) {
        prozessbaukasten.setErstanlaeufer(derivat);
        return this;
    }

}
