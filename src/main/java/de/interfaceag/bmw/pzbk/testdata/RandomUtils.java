package de.interfaceag.bmw.pzbk.testdata;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class RandomUtils {

    private RandomUtils() {
    }

    public static int getRandomForSize(int size) {
        return (int) Math.floor(Math.random() * size);
    }

    public static boolean getRandomBoolean() {
        return Math.random() > 0.5 ? Boolean.TRUE : Boolean.FALSE;
    }
}
