package de.interfaceag.bmw.pzbk.testdata;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenAnforderungKonzeptService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenStatusChangeService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenCreateAnforderungZuordnungService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author evda
 */
@Stateless
public class ProzessbaukastenTestdataService implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProzessbaukastenTestdataService.class.getName());

    @Inject
    private TteamService tteamService;
    @Inject
    private ProzessbaukastenService prozessbaukastenService;
    @Inject
    private UserSearchService userSearchService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private AnforderungService anforderungService;
    @Inject
    private ProzessbaukastenAnforderungKonzeptService prozessbaukastenAnforderungKonzeptService;
    @Inject
    private MeldungTestdataService meldungTestdataService;
    @Inject
    private AnforderungTestdataService anforderungTestdataService;
    @Inject
    private ProzessbaukastenCreateAnforderungZuordnungService createAnforderungZuordnungService;
    @Inject
    private ProzessbaukastenStatusChangeService prozessbaukastenStatusChangeService;

    public List<Prozessbaukasten> generateProzessbaukastenWithRandomDataForAdmin() {
        LOGGER.debug("start Prozessbaukasten testdata generation");
        List<Prozessbaukasten> result = new ArrayList<>();
        Mitarbeiter leadTechnologie = userSearchService.getMitarbeiterByQnumber("q2222222");
        if (leadTechnologie == null) {
            LOGGER.error("LeadTechnologie Mitarbeiter mit q2222222 nicht gefunden");
            return result;
        }

        Tteam tteam;
        Prozessbaukasten newProzessbaukasten;
        Integer count = 0;

        for (int i = 1; i <= 20; i++) {
            count = i;
            tteam = getRandomTteam();
            if (tteam == null) {
                LOGGER.error("Tteam nicht gefunden");
                return result;
            }

            newProzessbaukasten = ProzessbaukastenBuilder.newProzessbaukasten()
                    .withBezeichnung("Prozessbaukasten " + i)
                    .withBeschreibung(getRandomBeschreibung())
                    .withTteam(tteam)
                    .withLeadTechnologie(leadTechnologie)
                    .withStandardisierterFertigungsprozess(getRandomStandardisierterFertigungsprozess())
                    .withKonzept(getRandomKonzept())
                    .withKonzept(getRandomKonzept())
                    .withKonzept(getRandomKonzept())
                    .withKonzept(getRandomKonzept())
                    .build();

            newProzessbaukasten.setStatus(ProzessbaukastenStatus.ANGELEGT);
            Date generationDate = generatePastDateWithDeltaToDate(new Date());
            newProzessbaukasten.setErstellungsdatum(generationDate);
            prozessbaukastenService.saveWithHistory(newProzessbaukasten, null, leadTechnologie, generationDate);

            // the last five prozessbaukasten in status 'gueltig'
            if (i >= 15) {
                ordneFreigegebeneAnforderungenZu(newProzessbaukasten, leadTechnologie);
                ordneKonzepteZuProzessbaukastenAnforderung(newProzessbaukasten, leadTechnologie, generationDate);

                Date changeDate = generateFutureDateWithDeltaToDate(generationDate);
                setStatusBeauftragt(newProzessbaukasten, leadTechnologie, changeDate);
                setStatusGueltig(newProzessbaukasten, leadTechnologie);
            }

            result.add(newProzessbaukasten);

        }

        LOGGER.debug("{} Prozessbaukasten generated", count);
        return result;

    }

    private Date generatePastDateWithDeltaToDate(Date baseDate) {
        LocalDate currentLocal = Instant.ofEpochMilli(baseDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate datumWithDelta = currentLocal.minusDays(10);
        return Date.from(datumWithDelta.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    private Date generateFutureDateWithDeltaToDate(Date baseDate) {
        LocalDate currentLocal = Instant.ofEpochMilli(baseDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate datumWithDelta = currentLocal.plusDays(5);
        return Date.from(datumWithDelta.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    private Prozessbaukasten ordneFreigegebeneAnforderungenZu(Prozessbaukasten prozessbaukasten, Mitarbeiter leadTechnologie) {
        Meldung newMeldung;

        for (int i = 1; i <= 2; i++) {
            newMeldung = meldungTestdataService.generateMeldung(meldungTestdataService.getRandomSensorCoc(), leadTechnologie, Status.M_ZUGEORDNET);
            AnforderungId anforderungId = anforderungTestdataService.generateAnforderung(newMeldung, leadTechnologie, Status.A_FREIGEGEBEN);
            Anforderung anforderung = anforderungService.getAnforderungById(anforderungId.getId());
            createAnforderungZuordnungService.assignAnforderungToProzessbaukasten(anforderung, prozessbaukasten, leadTechnologie);
        }

        return prozessbaukasten;
    }

    private Prozessbaukasten ordneKonzepteZuProzessbaukastenAnforderung(Prozessbaukasten prozessbaukasten, Mitarbeiter leadTechnologie, Date datum) {
        Prozessbaukasten origin = prozessbaukastenService.createRealCopy(prozessbaukasten).orElse(null);
        Konzept konzept;
        int i = 0;
        int konzepteNumber = prozessbaukasten.getKonzepte().size();
        for (Anforderung anforderung : prozessbaukasten.getAnforderungen()) {
            if (prozessbaukasten.getKonzepte() != null && !prozessbaukasten.getKonzepte().isEmpty()) {
                konzept = prozessbaukasten.getKonzepte().get(i % konzepteNumber);
                i++;
                prozessbaukastenAnforderungKonzeptService.createAnforderungKonzept(anforderung, prozessbaukasten, konzept, leadTechnologie);
            }
        }

        prozessbaukastenService.saveWithHistory(prozessbaukasten, origin, leadTechnologie, datum);
        return prozessbaukasten;
    }

    private Prozessbaukasten setStatusBeauftragt(Prozessbaukasten prozessbaukasten, Mitarbeiter leadTechnologie, Date datum) {
        prozessbaukasten.setErstanlaeufer(getRandomErstanlaeufer());
        return prozessbaukastenStatusChangeService.processStatusChange(ProzessbaukastenStatus.BEAUFTRAGT, prozessbaukasten, "", leadTechnologie, datum);
    }

    private Prozessbaukasten setStatusGueltig(Prozessbaukasten prozessbaukasten, Mitarbeiter leadTechnologie) {
        return prozessbaukastenStatusChangeService.processStatusChange(ProzessbaukastenStatus.GUELTIG, prozessbaukasten, "", leadTechnologie, new Date());
    }

    private Konzept getRandomKonzept() {
        Konzept konzept = new Konzept();
        konzept.setBezeichnung(getRandomBezeichnung());
        konzept.setBeschreibung(getRandomBeschreibung());
        return konzept;
    }

    private Tteam getRandomTteam() {
        List<Tteam> allTteams = tteamService.getAllTteams();
        if (allTteams != null && !allTteams.isEmpty()) {
            int index = RandomUtils.getRandomForSize(allTteams.size());
            return allTteams.get(index);
        }

        return new Tteam("Unterboden");
    }

    private Derivat getRandomErstanlaeufer() {
        List<Derivat> allDerivate = derivatService.getAllErstanlaeufer();
        if (allDerivate != null && !allDerivate.isEmpty()) {
            int index = RandomUtils.getRandomForSize(allDerivate.size());
            return allDerivate.get(index);
        }

        return new Derivat("E20", "Bezeichnung");
    }

    private static String getRandomBezeichnung() {
        List<String> values = Arrays.asList(
                "SYLTKAKA",
                "ÖRESUND",
                "KLIPPAN",
                "KACKLING",
                "STÄNKA"
        );
        int index = RandomUtils.getRandomForSize(values.size());
        return values.get(index);
    }

    private static String getRandomBeschreibung() {
        List<String> values = Arrays.asList(
                "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
                "Gallia est omnis divisa in partes tres, quarum unam incolunt Belgae, aliam Aquitani, tertiam qui ipsorum lingua Celtae, nostra Galli appellantur. Hi omnes lingua, institutis, legibus inter se differunt. Gallos ab Aquitanis Garumna flumen, a Belgis Matrona et Sequana dividit. Horum omnium fortissimi sunt Belgae, propterea quod a cultu atque humanitate provinciae longissime absunt, minimeque ad eos mercatores saepe commeant atque ea quae ad effeminandos animos pertinent important, proximique sunt Germanis, qui trans Rhenum incolunt, quibuscum continenter bellum gerunt.",
                "Wir begleiten Unternehmen und Behörden auf IHREM WEG der Digitalen Transformation mit Blick auf unsere Fokusthemen Softwareentwicklung, DevOps und ...",
                "In nova fert animus mutatas dicere formas\n"
                        + "corpora; di, coeptis (nam vos mutastis et illas)\n"
                        + "adspirate meis primaque ab origine mundi\n"
                        + "ad mea perpetuum deducite tempora carmen!",
                "Begonnen hat der Weg der InterFace AG vor über 30 Jahren als Produktanbieter im Bereich Unix-Textverarbeitung. Daraus hat sich über die Jahre ein modernes Software-Projekthaus mit den Schwerpunkten Java/JEE und .NET entwickelt. Der Anspruch des Unternehmens war es von Anfang an, einen engen Bezug zwischen Entwicklung, Implementierung und Betrieb herzustellen – und so auch in dieser frühen Phase der Digitalisierung den Begriff “Digitale Transformation” noch vor seiner “Erfindung” aktiv zu leben.",
                "Durch eine iterative Softwareentwicklung in agilen Projektteams (SCRUM-Methodik) sowie ein automatisiertes Testing realisiert die InterFace AG eine kontinuierliche An­pas­sung und Qua­li­täts­­si­che­rung der entwickelten Softwarelösungen – über den Rollout hinaus. Die Be­reit­stel­lung al­ler not­wen­di­gen Re­ssour­cen bei en­ger, me­tho­di­scher Zu­sam­men­ar­beit zw­ischen Kun­den und Dienst­­leis­ter gewährleistet Performance, Sicherheit und Anwenderfreundlichkeit im täglichen Betrieb. Am Ende steht ein re­li­ables System, das das ge­sam­te An­wen­dungs­­spek­trum der eingesetzten Software berücksichtigt."
        );
        int index = RandomUtils.getRandomForSize(values.size());
        return values.get(index);
    }

    private static String getRandomStandardisierterFertigungsprozess() {
        List<String> values = Arrays.asList(
                "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
                "Gallia est omnis divisa in partes tres, quarum unam incolunt Belgae, aliam Aquitani, tertiam qui ipsorum lingua Celtae, nostra Galli appellantur. Hi omnes lingua, institutis, legibus inter se differunt. Gallos ab Aquitanis Garumna flumen, a Belgis Matrona et Sequana dividit. Horum omnium fortissimi sunt Belgae, propterea quod a cultu atque humanitate provinciae longissime absunt, minimeque ad eos mercatores saepe commeant atque ea quae ad effeminandos animos pertinent important, proximique sunt Germanis, qui trans Rhenum incolunt, quibuscum continenter bellum gerunt.",
                "Wir begleiten Unternehmen und Behörden auf IHREM WEG der Digitalen Transformation mit Blick auf unsere Fokusthemen Softwareentwicklung, DevOps und ...",
                "paar Informationen",
                "wichtige Hinweis",
                "as soon as possible",
                "Begonnen hat der Weg der InterFace AG vor über 30 Jahren als Produktanbieter im Bereich Unix-Textverarbeitung. Daraus hat sich über die Jahre ein modernes Software-Projekthaus mit den Schwerpunkten Java/JEE und .NET entwickelt. Der Anspruch des Unternehmens war es von Anfang an, einen engen Bezug zwischen Entwicklung, Implementierung und Betrieb herzustellen – und so auch in dieser frühen Phase der Digitalisierung den Begriff “Digitale Transformation” noch vor seiner “Erfindung” aktiv zu leben.",
                "Durch eine iterative Softwareentwicklung in agilen Projektteams (SCRUM-Methodik) sowie ein automatisiertes Testing realisiert die InterFace AG eine kontinuierliche An­pas­sung und Qua­li­täts­­si­che­rung der entwickelten Softwarelösungen – über den Rollout hinaus. Die Be­reit­stel­lung al­ler not­wen­di­gen Re­ssour­cen bei en­ger, me­tho­di­scher Zu­sam­men­ar­beit zw­ischen Kun­den und Dienst­­leis­ter gewährleistet Performance, Sicherheit und Anwenderfreundlichkeit im täglichen Betrieb. Am Ende steht ein re­li­ables System, das das ge­sam­te An­wen­dungs­­spek­trum der eingesetzten Software berücksichtigt."
        );
        int index = RandomUtils.getRandomForSize(values.size());
        return values.get(index);
    }

}
