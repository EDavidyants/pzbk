package de.interfaceag.bmw.pzbk.testdata;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Auswirkung;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Werk;
import de.interfaceag.bmw.pzbk.entities.Zielwert;
import de.interfaceag.bmw.pzbk.enums.Status;

import java.util.Date;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class MeldungBuilder {

    private final Meldung meldung;

    private MeldungBuilder() {
        this.meldung = new Meldung();
        meldung.setErstellungsdatum(new Date());
        meldung.setAenderungsdatum(new Date());
        meldung.setZeitpunktStatusaenderung(new Date());
        meldung.setStatus(Status.M_ENTWURF);
    }

    public static MeldungBuilder newMeldung() {
        MeldungBuilder builder = new MeldungBuilder();
        return builder;
    }

    public Meldung build() {
        return meldung;
    }

    public MeldungBuilder withSensorCoc(SensorCoc sensorCoc) {
        meldung.setSensorCoc(sensorCoc);
        return this;
    }

    public MeldungBuilder withSensor(Mitarbeiter sensor) {
        meldung.setSensor(sensor);
        return this;
    }

    public MeldungBuilder withFestgestelltIn(FestgestelltIn festgestelltIn) {
        meldung.addFestgestelltIn(festgestelltIn);
        return this;
    }

    public MeldungBuilder withAuswirkung(Auswirkung auswirkung) {
        meldung.addAuswirkung(auswirkung);
        return this;
    }

    public MeldungBuilder withZielwert(Zielwert zielwert) {
        meldung.setZielwert(zielwert);
        return this;
    }

    public MeldungBuilder withBeschreibungStaerkeSchwaeche(String beschreibungStaerkeSchwaeche) {
        meldung.setBeschreibungStaerkeSchwaeche(beschreibungStaerkeSchwaeche);
        return this;
    }

    public MeldungBuilder withStaerkeSchwaeche(boolean starkeSchwaeche) {
        meldung.setStaerkeSchwaeche(starkeSchwaeche);
        return this;
    }

    public MeldungBuilder withReferenzen(String referenzen) {
        meldung.setReferenzen(referenzen);
        return this;
    }

    public MeldungBuilder withBeschreibungDe(String beschreibungDe) {
        meldung.setBeschreibungAnforderungDe(beschreibungDe);
        return this;
    }

    public MeldungBuilder withBeschreibungEn(String beschreibungEn) {
        meldung.setBeschreibungAnforderungEn(beschreibungEn);
        return this;
    }

    public MeldungBuilder withUrsache(String ursache) {
        meldung.setUrsache(ursache);
        return this;
    }

    public MeldungBuilder withKommentar(String kommentar) {
        meldung.setKommentarAnforderung(kommentar);
        return this;
    }

    public MeldungBuilder withLoesungsvorschlag(String loesungsvorschlag) {
        meldung.setLoesungsvorschlag(loesungsvorschlag);
        return this;
    }

    public MeldungBuilder withWerk(Werk werk) {
        meldung.setWerk(werk);
        return this;
    }

    public MeldungBuilder withAnhang(Anhang anhang) {
        meldung.addAnhang(anhang);
        return this;
    }

    public MeldungBuilder withFachId(String fachId) {
        meldung.setFachId(fachId);
        return this;
    }

    public MeldungBuilder withStatus(Status status) {
        meldung.setStatus(status);
        return this;
    }
}
