package de.interfaceag.bmw.pzbk.testdata;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusChangeService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author lomu
 */
@Stateless
public class DerivatTestdataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DerivatTestdataService.class.getName());

    @Inject
    private DerivatStatusChangeService derivatStatusChangeService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private AnforderungService anforderungService;
    @Inject
    private ZuordnungAnforderungDerivatService anforderungDerivatService;
    @Inject
    private UserSearchService userSearchService;
    @Inject
    private DerivatAnforderungModulService derivatAnforderungModulService;

    public void generateDerivatTestData() {
        List<Derivat> derivate = derivatService.getAllDerivate();
        boolean derivatTestdataAlreadyChanged = false;

        for (Derivat derivat : derivate) {
            derivatTestdataAlreadyChanged = derivat.getStatus() != DerivatStatus.OFFEN;
        }

        if (!derivatTestdataAlreadyChanged && !derivate.isEmpty()) {
            changeStatusOfAllDerivate(derivate);
        }

    }

    private void changeStatusOfAllDerivate(List<Derivat> derivate) {
        LOGGER.debug("start DervatStatus testdata generation");
        Mitarbeiter testUser = userSearchService.getMitarbeiterByQnumber("q1111111");
        if (testUser != null) {

            derivate.forEach(derivat -> {
                while (shouldDerivatStatusBeChanged(derivat)) {
                    changeStatusOfDerivat(derivat);
                    if (isDerivatInStatusToAddAnforderung(derivat)) {
                        List<ZuordnungAnforderungDerivat> zuordnungen = addAnforderungenToDerivat(derivat, testUser);
                        changeAnforderungDerivatStatus(zuordnungen, testUser);
                    }

                }
            });
            LOGGER.debug("finished DervatStatus testdata generation");
        } else {
            LOGGER.debug("Couldn't find requested User in DB!");
        }
    }

    private void changeAnforderungDerivatStatus(List<ZuordnungAnforderungDerivat> zuordnungen, Mitarbeiter testUser) {
        zuordnungen.stream()
                .filter(zuordnung -> (zuordnung != null && testUser != null))
                .forEachOrdered(zuordnung ->
                        zuordnung.getAnforderung()
                                .getUmsetzer().stream().
                                forEach(umsetzer -> derivatAnforderungModulService.updateDerivatAnforderungModulStatus(
                                        getRandomStatus(),
                                        "Changed by Testfactory",
                                        zuordnung.getDerivat(),
                                        zuordnung.getAnforderung(),
                                        umsetzer.getModul(),
                                        testUser))
                );
    }

    private DerivatAnforderungModulStatus getRandomStatus() {
        List<DerivatAnforderungModulStatus> possibleStates = new ArrayList<>();
        possibleStates.add(DerivatAnforderungModulStatus.ANGENOMMEN);
        possibleStates.add(DerivatAnforderungModulStatus.NICHT_BEWERTBAR);
        possibleStates.add(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG);
        possibleStates.add(DerivatAnforderungModulStatus.IN_KLAERUNG);
        possibleStates.add(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET);

        ThreadLocalRandom random = ThreadLocalRandom.current();
        return possibleStates.get(random.nextInt(0, possibleStates.size()));
    }

    private boolean shouldDerivatStatusBeChanged(Derivat derivat) {
        return (derivat.getStatus() != DerivatStatus.ZV || derivat.getStatus() != DerivatStatus.INAKTIV)
                && new Random().nextBoolean();
    }

    private boolean changeStatusOfDerivat(Derivat derivat) {
        Collection<DerivatStatus> newStatus = derivat.getStatus().getNextStatus();
        if (!newStatus.isEmpty()) {
            derivatStatusChangeService.changeStatus(derivat, newStatus.iterator().next());
            return true;
        }
        return false;
    }

    private List<ZuordnungAnforderungDerivat> addAnforderungenToDerivat(Derivat derivat, Mitarbeiter testUser) {
        List<Anforderung> anforderungen = anforderungService.getAllAnforderungenByStatus(Status.A_FREIGEGEBEN);
        ThreadLocalRandom random = ThreadLocalRandom.current();

        List<ZuordnungAnforderungDerivat> results = new ArrayList<>();
        int amountOfAnforderungen = 0;
        if (isDerivatInStatusToAddAnforderung(derivat)) {
            if (!anforderungen.isEmpty()) {
                amountOfAnforderungen = random.nextInt(1, anforderungen.size());
            }

            for (int i = 0; i < amountOfAnforderungen; i++) {
                int anforderungPosition = random.nextInt(0, anforderungen.size());

                ZuordnungAnforderungDerivat result = anforderungDerivatService.assignAnforderungToDerivat(ZuordnungStatus.ZUGEORDNET, anforderungen.get(anforderungPosition), derivat, "Changed by Testfactory", testUser);
                if (result != null) {
                    results.add(result);
                }
            }
        }
        return results;
    }

    private boolean isDerivatInStatusToAddAnforderung(Derivat derivat) {
        return derivat.getStatus() == DerivatStatus.VEREINARBUNG_VKBG
                || derivat.getStatus() == DerivatStatus.VEREINBARUNG_ZV;
    }

}
