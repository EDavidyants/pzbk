package de.interfaceag.bmw.pzbk.testdata;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.Berechtigung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.ReferenzSystemLink;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Kategorie;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.ReferenzSystem;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static de.interfaceag.bmw.pzbk.shared.utils.DateUtils.addDays;
import static de.interfaceag.bmw.pzbk.shared.utils.DateUtils.subtractDays;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class AnforderungTestdataService implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnforderungTestdataService.class.getName());

    private static final String ANFORDERUNG = "Anforderung";
    private static final String MAX_ADMIN = "Max Admin";
    private static final String NEU_ANGELEGT = "neu angelegt";
    private static final String STATUS = "Status";
    private static final String UNSTIMMIG = "unstimmig";
    private static final String GELOESCHT = "gelöscht";
    private static final String FREIGEGEBEN = "freigegeben";
    private static final String PLAUSIBILISIERT = "plausibilisiert";
    private static final String IN_ARBEIT = "in Arbeit";
    private static final String ABGESTIMMT = "abgestimmt";
    public static final String KEINE_WEITERVERFOLGUNG = "Keine Weiterverfolgung";

    private final Random random = new Random();

    @Inject
    private AnforderungService anforderungService;
    @Inject
    private ModulService modulService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private TteamService tteamService;
    @Inject
    private AnforderungMeldungHistoryService anforderungMeldungHistoryService;

    @Inject
    private MeldungTestdataService meldungTestdataService;

    public void generateAnforderungOhneMeldungTestdata() {
        LOGGER.debug("start Anforderung ohne Meldung testdata generation");

        int count = 0;
        for (int i = 0; i < 50; i++) {
            if (generateAnforderungOhneMeldungWithRandomValues() != null) {
                count++;
            }
        }

        LOGGER.debug("{} Anforderungen ohne Meldung generated", count);
    }

    public AnforderungId generateAnforderungOhneMeldungWithRandomValues() {
        return generateAnforderungOhneMeldungWithRandomValues(getRandomAnforderungStatus());
    }

    public AnforderungId generateAnforderungOhneMeldungWithRandomValues(Status status) {
        SensorCoc sensorCoc = getRandomSensorCoc();
        if (sensorCoc == null) {
            LOGGER.error("sensorCoc is null");
            return null;
        }
        Mitarbeiter sensor = getSensorCocLeiterForSensorCoc(sensorCoc);
        if (sensor == null) {
            LOGGER.error("no SensocCoc-Leiter found for SensorCoc {}", sensorCoc);
            return null;
        }

        Anforderung newAnforderung = AnforderungBuilder.newAnforderung()
                .withSensor(sensor)
                .withStatus(status)
                .withVersion(1)
                .withPhasenbezug(RandomUtils.getRandomBoolean())
                .withZeitpunktUmsetzungsbestaetigung(RandomUtils.getRandomBoolean())
                .withPotentialStandardisierung(RandomUtils.getRandomBoolean())
                .withUmsetzerAndStdFreigabe(getRandomUmsetzer())
                .withUmsetzerAndStdFreigabe(getRandomUmsetzer())
                .withTteam(getRandomTteam())
                .withSensorCoc(sensorCoc)
                .withSensor(sensor)
                .withFestgestelltIn(meldungTestdataService.getRandomFestgestelltIn())
                .withAuswirkung(meldungTestdataService.generateAuswirkung())
                .withAuswirkung(meldungTestdataService.generateAuswirkung())
                .withZielwert(meldungTestdataService.generateZielwert())
                .withBeschreibungStaerkeSchwaeche(MeldungTestdataService.getRandomBeschreibungStaerkeSchwaeche())
                .withBeschreibungDe(MeldungTestdataService.getRandomBeschreibungDe())
                .withBeschreibungEn(MeldungTestdataService.getRandomBeschreibungDe())
                .withStaerkeSchwaeche(MeldungTestdataService.generateStaerkeSchwaeche())
                .withReferenzen(MeldungTestdataService.getRandomReferenzen())
                .withUrsache(MeldungTestdataService.getRandomUrsache())
                .withKommentar(MeldungTestdataService.getRandomKommentar())
                .withLoesungsvorschlag(MeldungTestdataService.getRandomLoesungsvorschlag())
                .withWerk(meldungTestdataService.getRandomWerk())
                .build();

        //.withAnhang(meldungTestdataService.getRandomAnhang(sensor))
        newAnforderung.setErsteller("Max von Mustermann");
        newAnforderung.setKategorie(Kategorie.EINS);
        newAnforderung.setPhasenbezug(true);

        ReferenzSystemLink refsyslink = new ReferenzSystemLink(ReferenzSystem.DOORS, "");
        newAnforderung.setReferenzSystemLinks(Arrays.asList(refsyslink));

        Long id = anforderungService.saveTestAnforderung(newAnforderung);
        newAnforderung.setFachId("A" + id);

        newAnforderung.setStatus(status);

        Date generatedErstellungsDatum = addDays(newAnforderung.getErstellungsdatum(), 49);
        newAnforderung.setErstellungsdatum(generatedErstellungsDatum);
        generateHistoryForAnforderung(newAnforderung, generatedErstellungsDatum);
        anforderungService.saveTestAnforderung(newAnforderung);

        return new AnforderungId(newAnforderung.getId());
    }

    public List<AnforderungId> generateAnforderungTestdata() {
        LOGGER.debug("start Anforderung testdata generation");

        int count = 0;
        List<AnforderungId> anforderungs = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            AnforderungId anforderungId = generateAnforderungWithRandomValues();
            if (anforderungId != null) {
                anforderungs.add(anforderungId);
                count++;
            }
        }

        LOGGER.debug("{} Anforderungen generated", count);

        return anforderungs;
    }

    private Mitarbeiter getSensorCocLeiterForSensorCoc(SensorCoc sensorCoc) {
        Mitarbeiter mitarbeiter = null;
        String sensorCocId = sensorCoc.getSensorCocId().toString();
        Berechtigung bere = berechtigungService.getByFuerIdFuerRolleRechttype(sensorCocId, Rolle.SENSORCOCLEITER, BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT);
        if (bere != null) {
            mitarbeiter = bere.getMitarbeiter();
        }
        return mitarbeiter;
    }

    private AnforderungId generateAnforderungWithRandomValues() {


        SensorCoc sensorCoc = getRandomSensorCoc();
        if (sensorCoc == null) {
            LOGGER.error("sensorCoc is null");
            return null;
        }

        Mitarbeiter sensor = getSensorCocLeiterForSensorCoc(sensorCoc);

        if (sensor == null) {
            LOGGER.error("no SensocCoc-Leiter found for SensorCoc {}", sensorCoc);
            return null;
        }


        Meldung newMeldung = meldungTestdataService.generateMeldung(sensorCoc, sensor, Status.M_ZUGEORDNET);

        Status status = getRandomAnforderungStatus();
        return generateAnforderung(newMeldung, sensor, status);

    }

    public AnforderungId generateAnforderung(Meldung meldung, Mitarbeiter sensorCocLeiter, Status status) {

        Anforderung newAnforderung = AnforderungBuilder.newAnforderung()
                .basedOnMeldung(meldung)
                .withSensor(sensorCocLeiter)
                .withStatus(status)
                .withVersion(1)
                .withPhasenbezug(RandomUtils.getRandomBoolean())
                .withZeitpunktUmsetzungsbestaetigung(RandomUtils.getRandomBoolean())
                .withPotentialStandardisierung(RandomUtils.getRandomBoolean())
                .withUmsetzerAndZakFreigabe(getRandomUmsetzer())
                .withUmsetzerAndZakFreigabe(getRandomUmsetzer())
                .withTteam(getRandomTteam())
                .build();

        Long id = anforderungService.saveTestAnforderung(newAnforderung);
        newAnforderung.setFachId("A" + id);
        newAnforderung.setStatus(status);

        Date erstellungsdatum = subtractDays(newAnforderung.getErstellungsdatum(), 60);
        newAnforderung.setErstellungsdatum(erstellungsdatum);
        generateHistoryForAnforderung(newAnforderung, erstellungsdatum);
        LOGGER.debug("created new Anforderung {} with status {}", newAnforderung, status);
        Long newAnforderungId = anforderungService.saveTestAnforderung(newAnforderung);
        return new AnforderungId(newAnforderungId);
    }

    private void generateHistoryForAnforderung(Anforderung anforderung, Date erstellungsdatum) {
        int randomNum = random.nextInt(3) + 1;
        int randomNum2 = random.nextInt(2) + 1;

        Status status = anforderung.getStatus();
        switch (status) {
            case A_INARBEIT:
                generateHistoryForStatusInArbeit(anforderung, erstellungsdatum);
                break;
            case A_FTABGESTIMMT:
                generateHistoryForStatusAbgestimmt(anforderung, erstellungsdatum, randomNum2);
                break;
            case A_UNSTIMMIG:
                generateHistoryForStatusUnstimmig(anforderung, erstellungsdatum, randomNum2);
                break;
            case A_GELOESCHT:
                generateHistoryForStatusGeloescht(anforderung, erstellungsdatum, randomNum);
                break;
            case A_PLAUSIB:
                generateHistoryForStatusPlausibilisiert(anforderung, erstellungsdatum, randomNum2);
                break;
            case A_FREIGEGEBEN:
                generateHistoryForStatusFreigegeben(anforderung, erstellungsdatum);
                break;
            case A_KEINE_WEITERVERFOLG:
                generateHistoryForStatusKeineWeiterverfolgung(anforderung, erstellungsdatum);
                break;
            default:
                break;
        }
    }

    private void generateHistoryForStatusPlausibilisiert(Anforderung anforderung, Date erstellungsdatum, int randomNum2) {
        if (randomNum2 == 1) {
            writeHistoryForPlausibilisiert(anforderung, erstellungsdatum);
        } else if (randomNum2 == 2) {
            writeHistoryForPlausibilisiertOverUnstimmig(anforderung, erstellungsdatum);
        }
    }

    private void generateHistoryForStatusGeloescht(Anforderung anforderung, Date erstellungsdatum, int randomNum) {
        switch (randomNum) {
            case 1:
                writeHistoryForGeloeschtFromArbeit(anforderung, erstellungsdatum);
                break;
            case 2:
                writeHistoryForGeloeschtFromAbgestimmt(anforderung, erstellungsdatum);
                break;
            case 3:
                writeHistoryForGeloeschtFromPlausibilisiert(anforderung, erstellungsdatum);
                break;
            default:
                break;
        }
    }

    private void generateHistoryForStatusUnstimmig(Anforderung anforderung, Date erstellungsdatum, int randomNum2) {
        if (randomNum2 == 1) {
            writeHistoryForUnstimmigFromAbgestimmt(anforderung, erstellungsdatum);
        } else if (randomNum2 == 2) {
            writeHistoryForUnstimmigFromPlausibilisiert(anforderung, erstellungsdatum);
        }
    }

    private void generateHistoryForStatusAbgestimmt(Anforderung anforderung, Date erstellungsdatum, int randomNum2) {
        if (randomNum2 == 1) {
            writeHistoryForAbgestimmtOverUnstimmig(anforderung, erstellungsdatum);
        } else if (randomNum2 == 2) {
            writeHistoryForAbgestimmt(anforderung, erstellungsdatum);
        }
    }

    private void generateHistoryForStatusInArbeit(Anforderung anforderung, Date erstellungsdatum) {
        Long anforderungId = anforderung.getId();
        anforderungMeldungHistoryService.persistAnforderungHistory(new AnforderungHistory(anforderungId, "A", erstellungsdatum, ANFORDERUNG, MAX_ADMIN, "", NEU_ANGELEGT));
        anforderung.setZeitpunktStatusaenderung(erstellungsdatum);
    }

    private void writeHistoryForAbgestimmtOverUnstimmig(Anforderung anforderung, Date erstellungsdatum) {
        Long anforderungId = anforderung.getId();
        Date lastStatusChange = addDays(erstellungsdatum, 43);
        anforderung.setZeitpunktStatusaenderung(lastStatusChange);

        final AnforderungHistory newAngelegt = getNewHistoryEntry(erstellungsdatum, anforderungId, 0, ANFORDERUNG, "", NEU_ANGELEGT);
        final AnforderungHistory abgestimmt1 = getNewHistoryEntry(erstellungsdatum, anforderungId, 12, STATUS, IN_ARBEIT, ABGESTIMMT);
        final AnforderungHistory unstimmig = getNewHistoryEntry(erstellungsdatum, anforderungId, 20, STATUS, ABGESTIMMT, UNSTIMMIG);
        final AnforderungHistory abgestimmt2 = new AnforderungHistory(anforderungId, "A", lastStatusChange, STATUS, MAX_ADMIN, UNSTIMMIG, ABGESTIMMT);

        persistHistoryEntries(newAngelegt, abgestimmt1, unstimmig, abgestimmt2);
    }

    private void writeHistoryForAbgestimmt(Anforderung anforderung, Date erstellungsdatum) {
        Long anforderungId = anforderung.getId();
        Date lastStatusChange = addDays(erstellungsdatum, 30);
        anforderung.setZeitpunktStatusaenderung(lastStatusChange);

        final AnforderungHistory newAngelegt = getNewHistoryEntry(erstellungsdatum, anforderungId, 0, ANFORDERUNG, "", NEU_ANGELEGT);
        final AnforderungHistory abgestimmt = new AnforderungHistory(anforderungId, "A", lastStatusChange, STATUS, MAX_ADMIN, IN_ARBEIT, ABGESTIMMT);

        persistHistoryEntries(newAngelegt, abgestimmt);
    }

    private void writeHistoryForUnstimmigFromAbgestimmt(Anforderung anforderung, Date erstellungsdatum) {
        Long anforderungId = anforderung.getId();
        Date lastStatusChange = addDays(erstellungsdatum, 50);
        anforderung.setZeitpunktStatusaenderung(lastStatusChange);

        final AnforderungHistory newAngelegt = getNewHistoryEntry(erstellungsdatum, anforderungId, 0, ANFORDERUNG, "", NEU_ANGELEGT);
        final AnforderungHistory abgestimmt = getNewHistoryEntry(erstellungsdatum, anforderungId, 35, STATUS, IN_ARBEIT, ABGESTIMMT);
        final AnforderungHistory unstimmig = new AnforderungHistory(anforderungId, "A", lastStatusChange, STATUS, MAX_ADMIN, ABGESTIMMT, UNSTIMMIG);

        persistHistoryEntries(newAngelegt, abgestimmt, unstimmig);
    }

    private void writeHistoryForUnstimmigFromPlausibilisiert(Anforderung anforderung, Date erstellungsdatum) {
        Long anforderungId = anforderung.getId();
        Date lastStatusChange = addDays(erstellungsdatum, 35);
        anforderung.setZeitpunktStatusaenderung(lastStatusChange);

        final AnforderungHistory newAngelegt = getNewHistoryEntry(erstellungsdatum, anforderungId, 0, ANFORDERUNG, "", NEU_ANGELEGT);
        final AnforderungHistory abgestimmt = getNewHistoryEntry(erstellungsdatum, anforderungId, 14, STATUS, IN_ARBEIT, ABGESTIMMT);
        final AnforderungHistory plausibilisiert = getNewHistoryEntry(erstellungsdatum, anforderungId, 32, STATUS, ABGESTIMMT, PLAUSIBILISIERT);
        final AnforderungHistory unstimmig = new AnforderungHistory(anforderungId, "A", lastStatusChange, STATUS, MAX_ADMIN, PLAUSIBILISIERT, UNSTIMMIG);

        persistHistoryEntries(newAngelegt, abgestimmt, plausibilisiert, unstimmig);
    }

    private void writeHistoryForGeloeschtFromArbeit(Anforderung anforderung, Date erstellungsdatum) {
        Long anforderungId = anforderung.getId();
        Date lastStatusChange = addDays(erstellungsdatum, 5);
        anforderung.setZeitpunktStatusaenderung(lastStatusChange);

        final AnforderungHistory newAngelegt = getNewHistoryEntry(erstellungsdatum, anforderungId, 0, ANFORDERUNG, "", NEU_ANGELEGT);
        final AnforderungHistory geloescht = new AnforderungHistory(anforderungId, "A", lastStatusChange, STATUS, MAX_ADMIN, IN_ARBEIT, GELOESCHT);

        persistHistoryEntries(newAngelegt, geloescht);
    }

    private void writeHistoryForGeloeschtFromAbgestimmt(Anforderung anforderung, Date erstellungsdatum) {
        Long anforderungId = anforderung.getId();
        Date lastStatusChange = addDays(erstellungsdatum, 23);
        anforderung.setZeitpunktStatusaenderung(lastStatusChange);

        final AnforderungHistory newAngelegt = getNewHistoryEntry(erstellungsdatum, anforderungId, 0, ANFORDERUNG, "", NEU_ANGELEGT);
        final AnforderungHistory abgestimmt = getNewHistoryEntry(erstellungsdatum, anforderungId, 15, STATUS, IN_ARBEIT, ABGESTIMMT);
        final AnforderungHistory geloescht = new AnforderungHistory(anforderungId, "A", lastStatusChange, STATUS, MAX_ADMIN, ABGESTIMMT, GELOESCHT);

        persistHistoryEntries(newAngelegt, abgestimmt, geloescht);
    }

    private void writeHistoryForGeloeschtFromPlausibilisiert(Anforderung anforderung, Date erstellungsdatum) {
        Long anforderungId = anforderung.getId();
        Date lastStatusChange = addDays(erstellungsdatum, 60);
        anforderung.setZeitpunktStatusaenderung(lastStatusChange);

        final AnforderungHistory newAngelegt = getNewHistoryEntry(erstellungsdatum, anforderungId, 0, ANFORDERUNG, "", NEU_ANGELEGT);
        final AnforderungHistory abgestimmt = getNewHistoryEntry(erstellungsdatum, anforderungId, 25, STATUS, IN_ARBEIT, ABGESTIMMT);
        final AnforderungHistory plausibilisiert = getNewHistoryEntry(erstellungsdatum, anforderungId, 40, STATUS, ABGESTIMMT, PLAUSIBILISIERT);
        final AnforderungHistory geloescht = new AnforderungHistory(anforderungId, "A", lastStatusChange, STATUS, MAX_ADMIN, PLAUSIBILISIERT, GELOESCHT);

        persistHistoryEntries(newAngelegt, abgestimmt, plausibilisiert, geloescht);
    }

    private void writeHistoryForPlausibilisiert(Anforderung anforderung, Date erstellungsdatum) {
        Long anforderungId = anforderung.getId();
        Date lastStatusChange = addDays(erstellungsdatum, 12);
        anforderung.setZeitpunktStatusaenderung(lastStatusChange);

        final AnforderungHistory newAngelegt = getNewHistoryEntry(erstellungsdatum, anforderungId, 0, ANFORDERUNG, "", NEU_ANGELEGT);
        final AnforderungHistory abgestimmt = getNewHistoryEntry(erstellungsdatum, anforderungId, 7, STATUS, IN_ARBEIT, ABGESTIMMT);
        final AnforderungHistory plausibilisiert = new AnforderungHistory(anforderungId, "A", lastStatusChange, STATUS, MAX_ADMIN, ABGESTIMMT, PLAUSIBILISIERT);

        persistHistoryEntries(newAngelegt, abgestimmt, plausibilisiert);
    }

    private void writeHistoryForPlausibilisiertOverUnstimmig(Anforderung anforderung, Date erstellungsdatum) {
        Long anforderungId = anforderung.getId();
        Date lastStatusChange = addDays(erstellungsdatum, 55);
        anforderung.setZeitpunktStatusaenderung(lastStatusChange);

        final AnforderungHistory newAngelegt = getNewHistoryEntry(erstellungsdatum, anforderungId, 0, ANFORDERUNG, "", NEU_ANGELEGT);
        final AnforderungHistory abgestimmt1 = getNewHistoryEntry(erstellungsdatum, anforderungId, 23, STATUS, IN_ARBEIT, ABGESTIMMT);
        final AnforderungHistory plausibilisiert1 = getNewHistoryEntry(erstellungsdatum, anforderungId, 30, STATUS, ABGESTIMMT, PLAUSIBILISIERT);
        final AnforderungHistory unstimig = getNewHistoryEntry(erstellungsdatum, anforderungId, 35, STATUS, PLAUSIBILISIERT, UNSTIMMIG);
        final AnforderungHistory abgestimmt2 = getNewHistoryEntry(erstellungsdatum, anforderungId, 45, STATUS, UNSTIMMIG, ABGESTIMMT);
        final AnforderungHistory plausibilisiert2 = new AnforderungHistory(anforderungId, "A", lastStatusChange, STATUS, MAX_ADMIN, ABGESTIMMT, PLAUSIBILISIERT);

        persistHistoryEntries(newAngelegt, abgestimmt1, plausibilisiert1, unstimig, abgestimmt2, plausibilisiert2);
    }

    private void generateHistoryForStatusFreigegeben(Anforderung anforderung, Date erstellungsdatum) {
        Long anforderungId = anforderung.getId();
        Date lastStatusChange = addDays(erstellungsdatum, 42);
        anforderung.setZeitpunktStatusaenderung(lastStatusChange);

        final AnforderungHistory newAngelegt = getNewHistoryEntry(erstellungsdatum, anforderungId, 0, ANFORDERUNG, "", NEU_ANGELEGT);
        final AnforderungHistory abgestimmt1 = getNewHistoryEntry(erstellungsdatum, anforderungId, 17, STATUS, IN_ARBEIT, ABGESTIMMT);
        final AnforderungHistory plausibilisiert = getNewHistoryEntry(erstellungsdatum, anforderungId, 23, STATUS, ABGESTIMMT, PLAUSIBILISIERT);
        final AnforderungHistory freigegeben = new AnforderungHistory(anforderungId, "A", lastStatusChange, STATUS, MAX_ADMIN, PLAUSIBILISIERT, FREIGEGEBEN);

        persistHistoryEntries(newAngelegt, abgestimmt1, plausibilisiert, freigegeben);
    }

    private void generateHistoryForStatusKeineWeiterverfolgung(Anforderung anforderung, Date erstellungsdatum) {
        Long anforderungId = anforderung.getId();
        Date lastStatusChange = addDays(erstellungsdatum, 54);
        anforderung.setZeitpunktStatusaenderung(lastStatusChange);

        final AnforderungHistory newAngelegt = getNewHistoryEntry(erstellungsdatum, anforderungId, 0, ANFORDERUNG, "", NEU_ANGELEGT);
        final AnforderungHistory abgestimmt1 = getNewHistoryEntry(erstellungsdatum, anforderungId, 19, STATUS, IN_ARBEIT, ABGESTIMMT);
        final AnforderungHistory plausibilisiert = getNewHistoryEntry(erstellungsdatum, anforderungId, 28, STATUS, ABGESTIMMT, PLAUSIBILISIERT);
        final AnforderungHistory freigegeben = getNewHistoryEntry(erstellungsdatum, anforderungId, 42, STATUS, PLAUSIBILISIERT, FREIGEGEBEN);
        final AnforderungHistory keineWeiterverfolgung = new AnforderungHistory(anforderungId, "A", lastStatusChange, STATUS, MAX_ADMIN, FREIGEGEBEN, KEINE_WEITERVERFOLGUNG);

        persistHistoryEntries(newAngelegt, abgestimmt1, plausibilisiert, freigegeben, keineWeiterverfolgung);
    }

    private Umsetzer getRandomUmsetzer() {
        if (RandomUtils.getRandomBoolean()) {
            List<ModulKomponente> allKomponenten = modulService.getAllKomponenten();
            int index = RandomUtils.getRandomForSize(allKomponenten.size());
            ModulKomponente komponente = allKomponenten.get(index);
            return new Umsetzer(komponente.getSeTeam(), komponente);
        } else {
            List<ModulSeTeam> allSeTeams = modulService.getAllSeTeams();
            int index = RandomUtils.getRandomForSize(allSeTeams.size());
            ModulSeTeam seTeam = allSeTeams.get(index);
            return new Umsetzer(seTeam);
        }
    }

    private SensorCoc getRandomSensorCoc() {
        List<SensorCoc> sensorCocList = sensorCocService.getAllSortByTechnologie();
        int index = RandomUtils.getRandomForSize(sensorCocList.size());
        return sensorCocList.get(index);
    }

    private Tteam getRandomTteam() {
        List<Tteam> allTteams = tteamService.getAllTteams();
        int index = RandomUtils.getRandomForSize(allTteams.size());
        return allTteams.get(index);
    }

    private static Status getRandomAnforderungStatus() {
        List<Status> allStatus = Status.selectStatusesOfAnforderung();
        int index = RandomUtils.getRandomForSize(allStatus.size());
        return allStatus.get(index);
    }

    private void persistHistoryEntry(AnforderungHistory historyEntry) {
        anforderungMeldungHistoryService.persistAnforderungHistory(historyEntry);
    }

    private void persistHistoryEntries(AnforderungHistory historyEntry1, AnforderungHistory historyEntry2) {
        persistHistoryEntry(historyEntry1);
        persistHistoryEntry(historyEntry2);
    }

    private void persistHistoryEntries(AnforderungHistory historyEntry1, AnforderungHistory historyEntry2, AnforderungHistory historyEntry3) {
        persistHistoryEntries(historyEntry1, historyEntry2);
        persistHistoryEntry(historyEntry3);
    }

    private void persistHistoryEntries(AnforderungHistory historyEntry1, AnforderungHistory historyEntry2, AnforderungHistory historyEntry3, AnforderungHistory historyEntry4) {
        persistHistoryEntries(historyEntry1, historyEntry2, historyEntry3);
        persistHistoryEntry(historyEntry4);
    }

    private void persistHistoryEntries(AnforderungHistory historyEntry1, AnforderungHistory historyEntry2, AnforderungHistory historyEntry3, AnforderungHistory historyEntry4, AnforderungHistory historyEntry5) {
        persistHistoryEntries(historyEntry1, historyEntry2, historyEntry3, historyEntry4);
        persistHistoryEntry(historyEntry5);
    }

    private void persistHistoryEntries(AnforderungHistory historyEntry1, AnforderungHistory historyEntry2, AnforderungHistory historyEntry3, AnforderungHistory historyEntry4, AnforderungHistory historyEntry5, AnforderungHistory historyEntry6) {
        persistHistoryEntries(historyEntry1, historyEntry2, historyEntry3, historyEntry4, historyEntry5);
        persistHistoryEntry(historyEntry6);
    }

    private AnforderungHistory getNewHistoryEntry(Date erstellungsdatum, Long anforderungId, int daysAfterErstellungsdatum, String objektName, String vonStatus, String aufStatus) {
        return new AnforderungHistory(anforderungId, "A", addDays(erstellungsdatum, daysAfterErstellungsdatum), objektName, MAX_ADMIN, vonStatus, aufStatus);
    }
}
