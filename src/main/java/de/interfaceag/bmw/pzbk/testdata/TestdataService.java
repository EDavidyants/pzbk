package de.interfaceag.bmw.pzbk.testdata;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.testdata.FahrzeugmerkmalAnforderungModulSeTeamAuspraegungTestDataService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.testdata.FahrzeugmerkmalDerivatZuordnungTestdataService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.testdata.FahrzeugmerkmalTestdataService;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerTestdataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class TestdataService implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestdataService.class.getName());

    @Inject
    private MeldungTestdataService meldungTestdataService;
    @Inject
    private AnforderungTestdataService anforderungTestdataService;
    @Inject
    private ZuordnungTestdataService zuordnungTestdataService;
    @Inject
    private ProzessbaukastenTestdataService prozessbaukastenTestdataService;
    @Inject
    private DerivatTestdataService derivatTestdataSerivce;
    @Inject
    private ThemenklammerTestdataService themenklammerTestdataService;
    @Inject
    private FahrzeugmerkmalTestdataService fahrzeugmerkmalTestdataService;
    @Inject
    private FahrzeugmerkmalDerivatZuordnungTestdataService fahrzeugmerkmalDerivatZuordnungTestdataService;
    @Inject
    private FahrzeugmerkmalAnforderungModulSeTeamAuspraegungTestDataService fahrzeugmerkmalAnforderungModulSeTeamAuspraegungTestDataService;
    @Inject
    private BerechtigungsantragTestdataService berechtigungsantragTestdataService;

    public void generateTestdata() {
        LOGGER.debug("start testdata generation");

        meldungTestdataService.generateMeldungTestdata();
        final List<AnforderungId> newGeneratedAnforderungen = anforderungTestdataService.generateAnforderungTestdata();

        anforderungTestdataService.generateAnforderungOhneMeldungTestdata();
        zuordnungTestdataService.generateZuordnungTestdata();

        derivatTestdataSerivce.generateDerivatTestData();

        prozessbaukastenTestdataService.generateProzessbaukastenWithRandomDataForAdmin();

        themenklammerTestdataService.generateTestdata();

        fahrzeugmerkmalTestdataService.generateTestdata();
        fahrzeugmerkmalDerivatZuordnungTestdataService.generateTestdata();
        fahrzeugmerkmalAnforderungModulSeTeamAuspraegungTestDataService.generateTestData(newGeneratedAnforderungen);
        berechtigungsantragTestdataService.generateTestdata();


        LOGGER.debug("testdata generation finished");
    }

}
