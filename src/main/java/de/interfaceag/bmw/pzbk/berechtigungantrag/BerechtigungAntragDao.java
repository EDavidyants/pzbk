package de.interfaceag.bmw.pzbk.berechtigungantrag;

import de.interfaceag.bmw.pzbk.dao.GenericDao;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.persistence.TypedQuery;
import java.util.List;

@Dependent
public class BerechtigungAntragDao extends GenericDao<Berechtigungsantrag> {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(BerechtigungAntragDao.class);

    private static final String ANTRAGSTELLER = "antragsteller";
    private static final String ROLLE = "rolle";
    private static final String RECHTTYPE = "rechttype";

    List<Berechtigungsantrag> getAllOpenBerechtigungAntraegeForView() {
        TypedQuery<Berechtigungsantrag> query = getEntityManager().createNamedQuery(Berechtigungsantrag.ALL_FOR_VIEW, Berechtigungsantrag.class);
        return query.getResultList();
    }

    List<Berechtigungsantrag> getAllNotOffenBerechtigungAntraege() {
        TypedQuery<Berechtigungsantrag> typedQuery = getEntityManager().createNamedQuery(Berechtigungsantrag.GET_ALL_NOT_OFFEN, Berechtigungsantrag.class);
        return typedQuery.getResultList();
    }

    List<Berechtigungsantrag> getAntraegeByAntragstellerRolleRechttypSensorCoc(Mitarbeiter antragsteller, Rolle rolle, Rechttype rechttype, SensorCoc sensorCoc) {
        TypedQuery<Berechtigungsantrag> typedQuery = getEntityManager().createNamedQuery(Berechtigungsantrag.GET_ANTRAEGE_BY_ANTRAGSTELLER_ROLLE_RECHTTYP_SENSOR_COC, Berechtigungsantrag.class);
        typedQuery.setParameter(ANTRAGSTELLER, antragsteller);
        typedQuery.setParameter(ROLLE, rolle.getId());
        typedQuery.setParameter(RECHTTYPE, rechttype.getId());
        typedQuery.setParameter("sensorCoc", sensorCoc);
        return typedQuery.getResultList();
    }

    List<Berechtigungsantrag> getAntraegeByAntragstellerRolleRechttypTteam(Mitarbeiter antragsteller, Rolle rolle, Rechttype rechttype, Tteam tteam) {
        TypedQuery<Berechtigungsantrag> typedQuery = getEntityManager().createNamedQuery(Berechtigungsantrag.GET_ANTRAEGE_BY_ANTRAGSTELLER_ROLLE_RECHTTYP_TTEAM, Berechtigungsantrag.class);
        typedQuery.setParameter(ANTRAGSTELLER, antragsteller);
        typedQuery.setParameter(ROLLE, rolle.getId());
        typedQuery.setParameter(RECHTTYPE, rechttype.getId());
        typedQuery.setParameter("tteam", tteam);
        return typedQuery.getResultList();
    }

    List<Berechtigungsantrag> getAntraegeByAntragstellerRolleRechttypModulSeTeam(Mitarbeiter antragsteller, Rolle rolle, Rechttype rechttype, ModulSeTeam modulSeTeam) {
        TypedQuery<Berechtigungsantrag> typedQuery = getEntityManager().createNamedQuery(Berechtigungsantrag.GET_ANTRAEGE_BY_ANTRAGSTELLER_ROLLE_RECHTTYP_MODUL_SE_TEAM, Berechtigungsantrag.class);
        typedQuery.setParameter(ANTRAGSTELLER, antragsteller);
        typedQuery.setParameter(ROLLE, rolle.getId());
        typedQuery.setParameter(RECHTTYPE, rechttype.getId());
        typedQuery.setParameter("modulSeTeam", modulSeTeam);
        return typedQuery.getResultList();
    }

    Berechtigungsantrag getAntraegByAntragId(Long id) {
        TypedQuery<Berechtigungsantrag> typedQuery = getEntityManager().createNamedQuery(Berechtigungsantrag.BY_ID, Berechtigungsantrag.class);
        typedQuery.setParameter("id", id);
        List<Berechtigungsantrag> resultList = typedQuery.getResultList();

        if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
            LOG.error("Couldn't find an entry for BerechtigungAntrag with id {}", id);
            return null;
        }

        return resultList.get(0);

    }


}
