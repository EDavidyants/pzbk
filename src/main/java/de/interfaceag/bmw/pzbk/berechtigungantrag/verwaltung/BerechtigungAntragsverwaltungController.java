package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung;

import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiter;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterAntrag;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragsverwaltungViewData;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.permission.BerechtigungAntragsverwaltungViewPermission;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class BerechtigungAntragsverwaltungController implements Serializable {

    @Inject
    private BerechtigungAntragsverwaltungViewData viewData;

    @Inject
    private BerechtigungAntragsverwaltungViewPermission viewPermission;

    @Inject
    private BerechtigungAntragsverwaltungFacade berechtigungAntragsverwaltungFacade;
    @Inject
    private LocalizationService localizationService;

    private List<BerechtigungAntragMitarbeiterAntrag> selectedAntraege;

    private BerechtigungAntragMitarbeiterAntrag antragToChange;

    public BerechtigungAntragMitarbeiterAntrag getAntragToChange() {
        return antragToChange;
    }

    public void setAntragToChange(BerechtigungAntragMitarbeiterAntrag antragToChange) {
        this.antragToChange = antragToChange;
    }

    public void openAntraege(BerechtigungAntragMitarbeiter selectedMitarbeiterAntraege) {
        setSelectedMitarbeiter(selectedMitarbeiterAntraege);
        selectedAntraege = selectedMitarbeiterAntraege.getAntraege();
    }

    public List<BerechtigungAntragMitarbeiterAntrag> getSelectedAntraege() {
        return selectedAntraege;
    }

    public void setSelectedAntraege(List<BerechtigungAntragMitarbeiterAntrag> selectedAntraege) {
        this.selectedAntraege = selectedAntraege;
    }

    public BerechtigungAntragsverwaltungViewData getViewData() {
        return viewData;
    }

    public BerechtigungAntragsverwaltungViewPermission getViewPermission() {
        return viewPermission;
    }

    public void openAnnehmenDialog(BerechtigungAntragMitarbeiterAntrag antragToChange) {
        setAntragToChange(antragToChange);
        antragToChange.setKommentar("");

        RequestContext.getCurrentInstance().update("dialogComponent:dialogAngenommenForm:angenommenDialog");
        RequestContext.getCurrentInstance().execute("PF('angenommenDialog').show();");
        
    }

    public void openAbgelehntDialog(BerechtigungAntragMitarbeiterAntrag antragToChange) {

        setAntragToChange(antragToChange);
        antragToChange.setKommentar("");

        RequestContext.getCurrentInstance().update("dialogComponent:dialogAbgelehntForm:abgelehntDialog");
        RequestContext.getCurrentInstance().execute("PF('abgelehntDialog').show();");

    }

    public void antragAngenommenClicked() {
        berechtigungAntragsverwaltungFacade.antragAnnehmen(antragToChange);
        antragToChange.setKommentar("");

        selectedAntraege.remove(antragToChange);
    }

    public void antragAbgelehntClicked() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (antragToChange.getKommentar().trim().isEmpty()) {
            updateGrowlMessage(context);
        } else {
            berechtigungAntragsverwaltungFacade.antragAblehnen(antragToChange);
            antragToChange.setKommentar("");

            selectedAntraege.remove(antragToChange);

            hideDialog();

        }

    }

    private void hideDialog() {
        RequestContext.getCurrentInstance().update("authorizedContent:berechtigungsantragForm");
        RequestContext.getCurrentInstance().execute("PF('abgelehntDialog').hide();");
    }

    private void updateGrowlMessage(FacesContext context) {
        String header = localizationService.getValue("berechtigungsworkflow_verwaltung_missing_kommentar_header");
        String body = localizationService.getValue("berechtigungsworkflow_verwaltung_missing_kommentar_body");
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, header, body));
        context.validationFailed();
        RequestContext.getCurrentInstance().update("authorizedContent:berechtigungsantragForm:growl");
    }

    public String getSelectedMitarbeiter() {
        BerechtigungAntragMitarbeiter mitarbeiter = viewData.getSelectedMitarbeiter();
        return mitarbeiter != null ? mitarbeiter.getMitarbeiterLabel() : "";
    }

    public void setSelectedMitarbeiter(BerechtigungAntragMitarbeiter mitarbeiter) {
        viewData.setSelectedMitarbeiter(mitarbeiter);
    }
}
