package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto;

import java.io.Serializable;
import java.util.List;

public class BerechtigungAntragsverwaltungViewData implements Serializable {

    private final List<BerechtigungAntragMitarbeiter> berechtigungAntragMitarbeiter;
    private BerechtigungAntragMitarbeiter selectedMitarbeiter;

    public BerechtigungAntragsverwaltungViewData(List<BerechtigungAntragMitarbeiter> berechtigungAntragMitarbeiter) {
        this.berechtigungAntragMitarbeiter = berechtigungAntragMitarbeiter;
    }

    public List<BerechtigungAntragMitarbeiter> getBerechtigungAntragMitarbeiter() {
        return berechtigungAntragMitarbeiter;
    }

    public BerechtigungAntragMitarbeiter getSelectedMitarbeiter() {
        return selectedMitarbeiter;
    }

    public void setSelectedMitarbeiter(BerechtigungAntragMitarbeiter selectedMitarbeiter) {
        this.selectedMitarbeiter = selectedMitarbeiter;
    }

}
