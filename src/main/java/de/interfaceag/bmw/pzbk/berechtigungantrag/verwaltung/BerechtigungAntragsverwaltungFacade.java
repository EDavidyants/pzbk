package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung;

import de.interfaceag.bmw.pzbk.berechtigungantrag.BerechtigungAntragService;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterAntrag;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

@Stateless
public class BerechtigungAntragsverwaltungFacade implements Serializable {

    @Inject
    private BerechtigungAntragService berechtigungAntragService;

    public List<Berechtigungsantrag> antragAnnehmen(BerechtigungAntragMitarbeiterAntrag antrag) {
        berechtigungAntragService.antragAnnehmen(antrag);

        return berechtigungAntragService.getAllOpenBerechtigungAntraegeForView();

    }

    public List<Berechtigungsantrag> antragAblehnen(BerechtigungAntragMitarbeiterAntrag antrag) {
        berechtigungAntragService.antragAblehnen(antrag);

       return berechtigungAntragService.getAllOpenBerechtigungAntraegeForView();
    }



}
