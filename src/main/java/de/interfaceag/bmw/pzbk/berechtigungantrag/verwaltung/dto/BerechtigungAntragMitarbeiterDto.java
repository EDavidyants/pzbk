package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class BerechtigungAntragMitarbeiterDto implements BerechtigungAntragMitarbeiter {

    private final Long mitarbeiterId;
    private final String mitarbeiterLabel;

    private List<BerechtigungAntragMitarbeiterAntrag> antraege;

    public BerechtigungAntragMitarbeiterDto(Long mitarbeiterId, String mitarbeiterLabel) {
        this.mitarbeiterId = mitarbeiterId;
        this.mitarbeiterLabel = mitarbeiterLabel;
        this.antraege = new ArrayList<>();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BerechtigungAntragMitarbeiterDto.class.getSimpleName() + "[", "]")
                .add("mitarbeiterId=" + mitarbeiterId)
                .add("mitarbeiterLabel='" + mitarbeiterLabel + "'")
                .add("antraege=" + antraege)
                .toString();
    }

    @Override
    public String getMitarbeiterLabel() {
        return mitarbeiterLabel;
    }

    @Override
    public Long getMitarbeiterId() {
        return mitarbeiterId;
    }

    @Override
    public List<BerechtigungAntragMitarbeiterAntrag> getAntraege() {
        return antraege;
    }

    public void addAntrag(BerechtigungAntragMitarbeiterAntrag antrag) {
        this.antraege.add(antrag);
    }

}
