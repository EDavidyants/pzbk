package de.interfaceag.bmw.pzbk.berechtigungantrag.history;

import de.interfaceag.bmw.pzbk.berechtigungantrag.BerechtigungAntragService;
import de.interfaceag.bmw.pzbk.berechtigungantrag.BerechtigungAntragUtils;
import de.interfaceag.bmw.pzbk.berechtigungantrag.history.dto.BerechtigungAntragVerwaltungHistoryDto;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Stateless
public class BerechtigungAntragVerwaltungHistoryService {

    @Inject
    private BerechtigungAntragService berechtigungAntragService;

    public List<BerechtigungAntragVerwaltungHistoryDto> getAllNotOffenBerechtigungAntraege() {

        List<Berechtigungsantrag> berechtigungsantragList = berechtigungAntragService.getAllNotOffenBerechtigungAntraege();

        return convertBerechtigungAntragToDto(berechtigungsantragList);
    }

    private List<BerechtigungAntragVerwaltungHistoryDto> convertBerechtigungAntragToDto(List<Berechtigungsantrag> berechtigungsantragList) {
        return berechtigungsantragList.stream()
                .map(berechtigungsantrag -> new BerechtigungAntragVerwaltungHistoryDto(
                        berechtigungsantrag.getAntragsteller().toString(),
                        berechtigungsantrag.getStatus(),
                        Optional.ofNullable(berechtigungsantrag.getBearbeiter()).map(Mitarbeiter::toString).orElse(""),
                        berechtigungsantrag.getKommentar(),
                        berechtigungsantrag.getRolle(),
                        berechtigungsantrag.getRechttype(),
                        BerechtigungAntragUtils.getBerechtigungLabel(berechtigungsantrag),
                        berechtigungsantrag.getErstellungsdatum(),
                        berechtigungsantrag.getAenderungsdatum())).collect(Collectors.toList());
    }

}
