package de.interfaceag.bmw.pzbk.berechtigungantrag.mail;

import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.AntragStatus;
import de.interfaceag.bmw.pzbk.enums.Attribut;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.mail.MailArgs;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.mail.MailGeneratorService;
import de.interfaceag.bmw.pzbk.shared.mail.MailSendService;
import de.interfaceag.bmw.pzbk.shared.mail.MailTemplatesService;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author evda
 */
@Stateless
@Named
public class BerechtigungsantragMailService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(BerechtigungsantragMailService.class);

    @Inject
    private MailSendService mailSendService;

    @Inject
    private MailGeneratorService mailGeneratorService;

    @Inject
    private MailTemplatesService mailTemplatesService;

    @Inject
    private LocalizationService localizationService;

    @Inject
    private Session session;

    @Inject
    private Date date;

    public void sendMailBasedOnAntragStatus(List<Berechtigungsantrag> berechtigungen) {
        AntragStatus status = BerechtigungsantragMailUtils.getStatus(berechtigungen);
        if (!checkDataForStatus(berechtigungen, status)) {
            LOG.error("sendMailBasedOnAntragStatus: Could not send email due to corrupt data");
            return;
        }

        if (status == AntragStatus.OFFEN) {
            LOG.debug("Sending receipt confirmation for authorization application ..");
        } else {
            LOG.debug("Sending status change mail for single authorization application ..");
        }

        try {
            Message message = buildMailMessage(berechtigungen);
            mailSendService.sendMail(message);

        } catch (MessagingException ex) {
            LOG.error("BerechtigungsantragMailService MessagingException by sendConfirmationMailForBerechtigungen");
        }

    }

    private boolean checkDataForStatus(List<Berechtigungsantrag> berechtigungen, AntragStatus status) {
        switch (status) {
            case OFFEN: {
                return isDataValid(berechtigungen);
            }
            case ANGENOMMEN:
            case ABGELEHNT:
                return isCurrentUserSet() && isBerechtigungenExisting(berechtigungen);

            default:
                return false;
        }
    }

    private Message buildMailMessage(List<Berechtigungsantrag> berechtigungen) throws MessagingException {
        AntragStatus status = BerechtigungsantragMailUtils.getStatus(berechtigungen);
        String template = getTemplate(status);
        MailArgs mailValues = getValuesMapForMail(berechtigungen);
        MimeBodyPart content = mailGeneratorService.buildContentFromTemplate(template, mailValues);

        String subject = null;
        String recipientMail = null;

        switch (status) {
            case OFFEN: {
                subject = localizationService.getValue("mail_berechtigungsantrag_eingangsbestaetigung");
                recipientMail = session.getUser().getEmail();
                break;
            }
            case ANGENOMMEN:
            case ABGELEHNT: {
                subject = localizationService.getValue("mail_berechtigungsantrag_status_change");
                Berechtigungsantrag berechtigungsantrag = berechtigungen.get(0);
                Mitarbeiter antragsteller = berechtigungsantrag.getAntragsteller();
                recipientMail = antragsteller.getEmail();
                break;
            }
            default:
                break;
        }

        if (recipientMail == null) {
            LOG.error("buildMailMessage: RecipientMail resolved to null");
        }

        LOG.debug("Sending Email to {}..", recipientMail);
        Message message = mailGeneratorService.createNoReplyMailMessage(subject, recipientMail, new MimeBodyPart[]{content});
        return message;
    }

    private MailArgs getValuesMapForMail(List<Berechtigungsantrag> berechtigungen) {
        MailArgs valuesMap = new MailArgs();
        valuesMap.putValue("datum", BerechtigungsantragMailUtils.getFormattedDate(date));
        valuesMap.putValue("user", BerechtigungsantragMailUtils.getAntragsteller(berechtigungen));
        valuesMap.putValue("rolle", BerechtigungsantragMailUtils.getRolle(berechtigungen));
        valuesMap.putValue("schreibberechtigungen", BerechtigungsantragMailUtils.getSchreibberechtigungen(berechtigungen));
        valuesMap.putValue("leseberechtigungen", BerechtigungsantragMailUtils.getLeseberechtigungen(berechtigungen));
        valuesMap.putValue("schreibberechtigungenAnzahl", BerechtigungsantragMailUtils.getSchreibberechtigungenAnzahl(berechtigungen));
        valuesMap.putValue("leseberechtigungenAnzahl", BerechtigungsantragMailUtils.getLeseberechtigungenAnzahl(berechtigungen));
        valuesMap.putValue("schreibberechtigungenAnzahlEn", BerechtigungsantragMailUtils.getSchreibberechtigungenAnzahlEn(berechtigungen));
        valuesMap.putValue("leseberechtigungenAnzahlEn", BerechtigungsantragMailUtils.getLeseberechtigungenAnzahlEn(berechtigungen));
        valuesMap.putValue("headerlogos", mailGeneratorService.generateLogoHtml());

        AntragStatus status = BerechtigungsantragMailUtils.getStatus(berechtigungen);
        switch (status) {
            case OFFEN:
                valuesMap.putValue("title", localizationService.getValue("mail_berechtigungsantrag_eingangsbestaetigung"));
                break;
            case ANGENOMMEN:
            case ABGELEHNT:
                Mitarbeiter bearbeiter = session.getUser();
                valuesMap.putValue("title", localizationService.getValue("mail_berechtigungsantrag_status"));
                valuesMap.putValue("bearbeiter", bearbeiter.toString());
                valuesMap.putValue("bearbeitermail", bearbeiter.getEmail());
                valuesMap.putValue("bearbeitertel", bearbeiter.getTel());
                valuesMap.putValue("statusUpdate", BerechtigungsantragMailUtils.getStatusUpdateMessage(berechtigungen));
                valuesMap.putValue("statusUpdateEn", BerechtigungsantragMailUtils.getStatusUpdateMessageEn(berechtigungen));
                break;
            default:
                break;
        }

        return valuesMap;
    }

    private String getTemplate(AntragStatus status) {
        switch (status) {
            case OFFEN:
                return mailTemplatesService.getMailTemplate(Attribut.BERECHTIGUNGSANTRAG_EINGANGSBESTAETIGUNG);
            case ANGENOMMEN:
            case ABGELEHNT:
                return mailTemplatesService.getMailTemplate(Attribut.BERECHTIGUNGSANTRAG_STATUS_UPDATE);
            default:
                return "";
        }
    }

    private boolean isDataValid(List<Berechtigungsantrag> berechtigungen) {
        if (!isCurrentUserSet()) {
            LOG.warn("User is null");
            return false;
        }

        if (!isBerechtigungenExisting(berechtigungen)) {
            LOG.warn("Error by saving authorization applications!");
            return false;
        }

        if (berechtigungen.stream()
                .map(berechtigung -> berechtigung.getAntragsteller())
                .anyMatch(user -> !user.equals(session.getUser()))) {

            LOG.warn("Berechtigung Antragsteller is not the current user!");
            return false;
        }

        Berechtigungsantrag berechtigungantragFirst = berechtigungen.get(0);
        Rolle rolleFirst = berechtigungantragFirst.getRolle();

        if (berechtigungen.stream()
                .map(berechtigung -> berechtigung.getRolle())
                .anyMatch(rolle -> rolle != rolleFirst)) {

            LOG.warn("Berechtigung Rolle vary throughout one authorization application!");
            return false;
        }

        return true;
    }

    private boolean isCurrentUserSet() {
        return session.getUser() != null;
    }

    private static boolean isBerechtigungenExisting(List<Berechtigungsantrag> berechtigungen) {
        return berechtigungen != null && !berechtigungen.isEmpty();
    }

}
