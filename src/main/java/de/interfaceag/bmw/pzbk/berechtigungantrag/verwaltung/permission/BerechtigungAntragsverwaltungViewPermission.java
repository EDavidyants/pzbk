package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.permission;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

public class BerechtigungAntragsverwaltungViewPermission implements Serializable {

    private final ViewPermission page;

    BerechtigungAntragsverwaltungViewPermission(Set<Rolle> userRoles) {
        this.page = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(userRoles).get();
    }

    public boolean getPage() {
        return page.isRendered();
    }

}
