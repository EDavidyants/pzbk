package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung;

import de.interfaceag.bmw.pzbk.berechtigungantrag.BerechtigungAntragService;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiter;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterAntragDto;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterDto;
import de.interfaceag.bmw.pzbk.berechtigungantrag.permission.BerechtigungAntragVerwaltungPermissionService;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
public class BerechtigungAntragVerwaltungService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(BerechtigungAntragVerwaltungService.class);

    @Inject
    private BerechtigungAntragService berechtigungAntragService;
    @Inject
    private BerechtigungAntragVerwaltungPermissionService berechtigungAntragVerwaltungPermissionService;


    public List<BerechtigungAntragMitarbeiter> getAllOpenBerechtigungAntraegeForView() {
        final List<Berechtigungsantrag> allOpenBerechtigungAntraegeForView = berechtigungAntragService.getAllOpenBerechtigungAntraegeForView();
        LOG.debug("Loaded {} entries from database for view", allOpenBerechtigungAntraegeForView.size());

        final Collection<Berechtigungsantrag> restrictedBerechtigungsAntraege = berechtigungAntragVerwaltungPermissionService.restrictToCurrentUser(allOpenBerechtigungAntraegeForView);
        LOG.debug("Restricted to {} entries for current user", restrictedBerechtigungsAntraege.size());

        final Map<Long, BerechtigungAntragMitarbeiterDto> mitarbeiterIdResultMap = new HashMap<>();
        for (Berechtigungsantrag berechtigungsantrag : restrictedBerechtigungsAntraege) {
            addBerechtigungAntragToMap(mitarbeiterIdResultMap, berechtigungsantrag);
        }

        LOG.debug("Created {}", mitarbeiterIdResultMap.values());
        return new ArrayList<>(mitarbeiterIdResultMap.values());
    }

    private static void addBerechtigungAntragToMap(Map<Long, BerechtigungAntragMitarbeiterDto> mitarbeiterIdResultMap, Berechtigungsantrag berechtigungsantrag) {
        final Mitarbeiter antragsteller = berechtigungsantrag.getAntragsteller();
        final Long antragstellerId = antragsteller.getId();

        if (mitarbeiterIdResultMap.containsKey(antragstellerId)) {
            updateMapEntryWithNewAntrag(mitarbeiterIdResultMap, berechtigungsantrag, antragstellerId);
        } else {
            createNewMapEntry(mitarbeiterIdResultMap, berechtigungsantrag, antragsteller, antragstellerId);
        }
    }

    private static void updateMapEntryWithNewAntrag(Map<Long, BerechtigungAntragMitarbeiterDto> mitarbeiterIdResultMap, Berechtigungsantrag berechtigungsantrag, Long antragstellerId) {
        final BerechtigungAntragMitarbeiterDto berechtigungAntragMitarbeiter = mitarbeiterIdResultMap.get(antragstellerId);
        LOG.debug("Update {}", berechtigungAntragMitarbeiter);
        addNewAntrag(berechtigungsantrag, berechtigungAntragMitarbeiter);
        mitarbeiterIdResultMap.replace(antragstellerId, berechtigungAntragMitarbeiter);
    }

    private static void createNewMapEntry(Map<Long, BerechtigungAntragMitarbeiterDto> mitarbeiterIdResultMap, Berechtigungsantrag berechtigungsantrag, Mitarbeiter antragsteller, Long antragstellerId) {
        final BerechtigungAntragMitarbeiterDto berechtigungAntragMitarbeiter = new BerechtigungAntragMitarbeiterDto(antragstellerId, antragsteller.toString());
        LOG.debug("Create {}", berechtigungAntragMitarbeiter);
        addNewAntrag(berechtigungsantrag, berechtigungAntragMitarbeiter);
        mitarbeiterIdResultMap.put(antragstellerId, berechtigungAntragMitarbeiter);
    }

    private static void addNewAntrag(Berechtigungsantrag berechtigungsantrag, BerechtigungAntragMitarbeiterDto berechtigungAntragMitarbeiter) {
        final BerechtigungAntragMitarbeiterAntragDto newAntrag = BerechtigungAntragMitarbeiterConverter.convertToBerechtigungAntragMitarbeiter(berechtigungsantrag);
        berechtigungAntragMitarbeiter.addAntrag(newAntrag);
    }


}
