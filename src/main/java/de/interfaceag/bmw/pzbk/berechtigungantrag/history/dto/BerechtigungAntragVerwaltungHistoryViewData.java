package de.interfaceag.bmw.pzbk.berechtigungantrag.history.dto;

import java.io.Serializable;
import java.util.List;

public class BerechtigungAntragVerwaltungHistoryViewData implements Serializable {

    private final List<BerechtigungAntragVerwaltungHistoryDto> berechtigungAntragVerwaltungHistoryDtoList;

    public BerechtigungAntragVerwaltungHistoryViewData(List<BerechtigungAntragVerwaltungHistoryDto> berechtigungAntragVerwaltungHistoryDtoList) {
        this.berechtigungAntragVerwaltungHistoryDtoList = berechtigungAntragVerwaltungHistoryDtoList;
    }

    public List<BerechtigungAntragVerwaltungHistoryDto> getBerechtigungAntragVerwaltungHistoryDtoList() {
        return berechtigungAntragVerwaltungHistoryDtoList;
    }
}
