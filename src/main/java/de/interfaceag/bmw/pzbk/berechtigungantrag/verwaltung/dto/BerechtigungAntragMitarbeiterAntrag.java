package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto;

import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.io.Serializable;

public interface BerechtigungAntragMitarbeiterAntrag extends Serializable {

    Long getBerechtigungAntragId();

    String getKommentar();

    void setKommentar(String kommentar);

    Rolle getRolle();

    Rechttype getRechttype();

    String getBerechtigungLabel();

    String getErstellungsdatumForAusgabe();

}
