package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.permission;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Set;

@Stateless
public class BerechtigungAntragsverwaltungViewPermissionFactory implements Serializable {

    @Inject
    private Session session;

    @Produces
    public BerechtigungAntragsverwaltungViewPermission getViewPermission() {
        final UserPermissions<BerechtigungDto> userPermissions = session.getUserPermissions();
        final Set<Rolle> roles = userPermissions.getRoles();
        return new BerechtigungAntragsverwaltungViewPermission(roles);
    }

}
