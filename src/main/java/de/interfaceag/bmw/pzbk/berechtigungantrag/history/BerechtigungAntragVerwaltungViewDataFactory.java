package de.interfaceag.bmw.pzbk.berechtigungantrag.history;

import de.interfaceag.bmw.pzbk.berechtigungantrag.history.dto.BerechtigungAntragVerwaltungHistoryDto;
import de.interfaceag.bmw.pzbk.berechtigungantrag.history.dto.BerechtigungAntragVerwaltungHistoryViewData;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;


@Stateless
public class BerechtigungAntragVerwaltungViewDataFactory implements Serializable {

    @Inject
    private BerechtigungAntragVerwaltungHistoryService berechtigungAntragVerwaltungHistoryService;

    @Produces
    public BerechtigungAntragVerwaltungHistoryViewData initViewData() {
        List<BerechtigungAntragVerwaltungHistoryDto> berechtigungAntragVerwaltungHistoryDtoList = berechtigungAntragVerwaltungHistoryService.getAllNotOffenBerechtigungAntraege();
        return new BerechtigungAntragVerwaltungHistoryViewData(berechtigungAntragVerwaltungHistoryDtoList);
    }
}
