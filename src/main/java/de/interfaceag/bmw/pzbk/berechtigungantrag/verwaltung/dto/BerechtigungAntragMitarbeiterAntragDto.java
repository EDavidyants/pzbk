package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto;

import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringJoiner;

public class BerechtigungAntragMitarbeiterAntragDto implements BerechtigungAntragMitarbeiterAntrag {

    private final Long berechtigungAntragId;
    private final Rolle rolle;
    private final Rechttype rechttype;
    private final String berechtigungLabel;
    private final Date erstellungsdatum;

    private String kommentar;

    public BerechtigungAntragMitarbeiterAntragDto(Long berechtigungAntragId, Rolle rolle, Rechttype rechttype, String berechtigungLabel, Date erstellungdatum) {
        this.berechtigungAntragId = berechtigungAntragId;
        this.rolle = rolle;
        this.rechttype = rechttype;
        this.berechtigungLabel = berechtigungLabel;
        this.erstellungsdatum = erstellungdatum;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BerechtigungAntragMitarbeiterAntragDto.class.getSimpleName() + "[", "]")
                .add("berechtigungAntragId=" + berechtigungAntragId)
                .add("rolle=" + rolle)
                .add("rechttype=" + rechttype)
                .add("berechtigungLabel='" + berechtigungLabel + "'")
                .add("kommentar='" + kommentar + "'")
                .toString();
    }

    @Override
    public Long getBerechtigungAntragId() {
        return this.berechtigungAntragId;
    }

    @Override
    public String getKommentar() {
        return this.kommentar;
    }

    @Override
    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    @Override
    public Rolle getRolle() {
        return this.rolle;
    }

    @Override
    public Rechttype getRechttype() {
        return this.rechttype;
    }

    @Override
    public String getBerechtigungLabel() {
        return this.berechtigungLabel;
    }

    @Override
    public String getErstellungsdatumForAusgabe() {
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(erstellungsdatum);
    }


    public Date getErstellungsdatum() {
        return erstellungsdatum;
    }

    //Never ever change this equals. Is needed for removing the right entry out of Viewdata in BerechtigungsAntragVerwaltung
    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        BerechtigungAntragMitarbeiterAntragDto that = (BerechtigungAntragMitarbeiterAntragDto) object;

        return new EqualsBuilder()
                .append(berechtigungAntragId, that.berechtigungAntragId)
                .append(rolle, that.rolle)
                .append(rechttype, that.rechttype)
                .append(berechtigungLabel, that.berechtigungLabel)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(berechtigungAntragId)
                .append(rolle)
                .append(rechttype)
                .append(berechtigungLabel)
                .toHashCode();
    }
}
