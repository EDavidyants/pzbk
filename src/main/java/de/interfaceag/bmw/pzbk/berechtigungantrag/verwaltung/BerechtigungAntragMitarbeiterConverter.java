package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung;

import de.interfaceag.bmw.pzbk.berechtigungantrag.BerechtigungAntragUtils;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterAntragDto;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.Date;

final class BerechtigungAntragMitarbeiterConverter {


    private BerechtigungAntragMitarbeiterConverter() {
    }

    static BerechtigungAntragMitarbeiterAntragDto convertToBerechtigungAntragMitarbeiter(Berechtigungsantrag berechtigungsantrag) {

        final Long berechtigungsantragId = berechtigungsantrag.getId();
        final Rolle rolle = berechtigungsantrag.getRolle();
        final Rechttype rechttype = berechtigungsantrag.getRechttype();
        String berechtigungLabel = BerechtigungAntragUtils.getBerechtigungLabel(berechtigungsantrag);

        Date erstellunsdatum = berechtigungsantrag.getErstellungsdatum();
        return new BerechtigungAntragMitarbeiterAntragDto(berechtigungsantragId, rolle, rechttype, berechtigungLabel, erstellunsdatum);
    }

}
