package de.interfaceag.bmw.pzbk.berechtigungantrag.history.dto;

import de.interfaceag.bmw.pzbk.enums.AntragStatus;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BerechtigungAntragVerwaltungHistoryDto implements Serializable {

    private final String antragsteller;
    private final AntragStatus antragStatus;
    private final String bearbeiter;
    private final String kommentar;
    private final Rolle rolle;
    private final Rechttype rechttype;
    private final String berechtigung;
    private final Date erstelungsdatum;
    private final Date aenderungsdatum;

    public BerechtigungAntragVerwaltungHistoryDto(String antragsteller, AntragStatus antragStatus, String bearbeiter, String kommentar, Rolle rolle, Rechttype rechttype, String berechtigung, Date erstelungsdatum, Date aenderungsdatum) {
        this.antragsteller = antragsteller;
        this.antragStatus = antragStatus;
        this.bearbeiter = bearbeiter;
        this.kommentar = kommentar;
        this.rolle = rolle;
        this.rechttype = rechttype;
        this.berechtigung = berechtigung;
        this.erstelungsdatum = erstelungsdatum;
        this.aenderungsdatum = aenderungsdatum;
    }

    public String getAntragsteller() {
        return antragsteller;
    }

    public AntragStatus getAntragStatus() {
        return antragStatus;
    }

    public String getBearbeiter() {
        return bearbeiter;
    }

    public String getKommentar() {
        return kommentar;
    }

    public Rolle getRolle() {
        return rolle;
    }

    public Rechttype getRechttype() {
        return rechttype;
    }

    public String getBerechtigung() {
        return berechtigung;
    }

    public String getErstelungsdatum() {
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(erstelungsdatum);
    }

    public String getAenderungsdatum() {
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(aenderungsdatum);
    }
}
