package de.interfaceag.bmw.pzbk.berechtigungantrag.history;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Stateless
public class BerechtigungAntragVerwaltungHistoryFilter {

    private static final Logger LOG = LoggerFactory.getLogger(BerechtigungAntragVerwaltungHistoryFilter.class);

    public boolean customDateFilter(Object value, Object filter, Locale locale) {

        if (filter == null) {
            return true;
        }

        if (value == null) {
            return false;
        }

        Date filterdate = (Date) filter;
        Date entryDate = null;
        try {
            entryDate = new SimpleDateFormat("dd.MM.yyyy").parse(String.valueOf(value));
        } catch (ParseException ex) {
            LOG.error("Error parsing {}", value);
        }

        return filterdate.equals(entryDate);
    }
}
