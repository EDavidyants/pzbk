package de.interfaceag.bmw.pzbk.berechtigungantrag.mail;

import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.enums.AntragStatus;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author evda
 */
public final class BerechtigungsantragMailUtils implements Serializable {

    private BerechtigungsantragMailUtils() {

    }

    public static String getFormattedDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        return format.format(date);
    }

    public static String getAntragsteller(List<Berechtigungsantrag> berechtigungen) {
        Berechtigungsantrag berechtigungantragFirst = berechtigungen.get(0);
        return berechtigungantragFirst.getAntragsteller().getName();
    }

    public static String getRolle(List<Berechtigungsantrag> berechtigungen) {
        Berechtigungsantrag berechtigungantragFirst = berechtigungen.get(0);
        return berechtigungantragFirst.getRolle().toString();
    }

    public static String getSchreibberechtigungen(List<Berechtigungsantrag> berechtigungen) {
        List<Berechtigungsantrag> relevantAntraege = getRelevantAntraegeForRechttype(berechtigungen, Rechttype.SCHREIBRECHT);
        if (relevantAntraege.isEmpty()) {
            return "";
        }

        String prefix = "<ul style='margin-top: 0px;'><li>";
        String suffix = "</li></ul>";
        return getListedZieleForAntraege(relevantAntraege, prefix, suffix);

    }

    public static String getLeseberechtigungen(List<Berechtigungsantrag> berechtigungen) {
        List<Berechtigungsantrag> relevantAntraege = getRelevantAntraegeForRechttype(berechtigungen, Rechttype.LESERECHT);
        if (relevantAntraege.isEmpty()) {
            return "";
        }

        String prefix = "<ul style='list-style-type: circle; margin-top: 0px;'><li>";
        String suffix = "</li></ul>";
        return getListedZieleForAntraege(relevantAntraege, prefix, suffix);

    }

    private static String getListedZieleForAntraege(List<Berechtigungsantrag> relevantAntraege, String prefix, String suffix) {
        String berechtigungZiel = getBerechtigungZiel(relevantAntraege);

        switch (berechtigungZiel) {
            case "SensorCoC": {
                return prefix + relevantAntraege.stream()
                        .map(berechtigung -> getBerechtigungsantragForSensorCocAsListItem(berechtigung))
                        .collect(Collectors.joining("</li><li>")) + suffix;

            }
            case "T-Team": {
                return prefix + relevantAntraege.stream()
                        .map(berechtigung -> getBerechtigungsantragForTteamAsListItem(berechtigung))
                        .collect(Collectors.joining("</li><li>")) + suffix;
            }
            case "ModulSeTeam": {
                return prefix + relevantAntraege.stream()
                        .map(berechtigung -> getBerechtigungsantragForModulSeTeamAsListItem(berechtigung))
                        .collect(Collectors.joining("</li><li>")) + suffix;
            }
            default:
                return "";
        }
    }

    private static String getBerechtigungsantragForSensorCocAsListItem(Berechtigungsantrag berechtigungsantrag) {
        AntragStatus status = berechtigungsantrag.getStatus();
        switch (status) {
            case OFFEN:
                return berechtigungsantrag.getSensorCoc().getTechnologie();
            case ANGENOMMEN: {
                String kommentar = getKommentar(berechtigungsantrag);
                String suffix = kommentar.isEmpty() ? "" : "<br/><div style='font-style: italic;'>" + kommentar + "</div>";
                return berechtigungsantrag.getSensorCoc().getTechnologie() + suffix;
            }
            case ABGELEHNT: {
                String kommentar = getKommentar(berechtigungsantrag);
                String suffix = "<br/><div style='font-style: italic;'>" + kommentar + "</div>";
                return berechtigungsantrag.getSensorCoc().getTechnologie() + suffix;
            }
            default:
                return "";
        }

    }

    private static String getBerechtigungsantragForTteamAsListItem(Berechtigungsantrag berechtigungsantrag) {
        AntragStatus status = berechtigungsantrag.getStatus();
        switch (status) {
            case OFFEN:
                return berechtigungsantrag.getTteam().getTeamName();
            case ANGENOMMEN: {
                String kommentar = getKommentar(berechtigungsantrag);
                String suffix = kommentar.isEmpty() ? "" : "<br/><div style='font-style: italic;'>" + kommentar + "</div>";
                return berechtigungsantrag.getTteam().getTeamName() + suffix;
            }
            case ABGELEHNT: {
                String kommentar = getKommentar(berechtigungsantrag);
                String suffix = "<br/><div style='font-style: italic;'>" + kommentar + "</div>";
                return berechtigungsantrag.getTteam().getTeamName() + suffix;
            }
            default:
                return "";
        }

    }

    private static String getBerechtigungsantragForModulSeTeamAsListItem(Berechtigungsantrag berechtigungsantrag) {
        AntragStatus status = berechtigungsantrag.getStatus();
        switch (status) {
            case OFFEN:
                return berechtigungsantrag.getModulSeTeam().toString();
            case ANGENOMMEN: {
                String kommentar = getKommentar(berechtigungsantrag);
                String suffix = kommentar.isEmpty() ? "" : "<br/><div style='font-style: italic;'>" + kommentar + "</div>";
                return berechtigungsantrag.getModulSeTeam().toString() + suffix;
            }
            case ABGELEHNT: {
                String kommentar = getKommentar(berechtigungsantrag);
                String suffix = "<br/><div style='font-style: italic;'>" + kommentar + "</div>";
                return berechtigungsantrag.getModulSeTeam().toString() + suffix;
            }
            default:
                return "";
        }

    }

    private static List<Berechtigungsantrag> getRelevantAntraegeForRechttype(List<Berechtigungsantrag> berechtigungen, Rechttype rechttype) {
        return berechtigungen.stream()
                .filter(berechtigung -> berechtigung.getRechttype() == rechttype)
                .collect(Collectors.toList());
    }

    private static String getBerechtigungZiel(List<Berechtigungsantrag> berechtigungen) {
        Berechtigungsantrag berechtigungantragFirst = berechtigungen.get(0);
        Rolle rolle = berechtigungantragFirst.getRolle();
        switch (rolle) {
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
                return "SensorCoC";
            case TTEAMMITGLIED:
                return "T-Team";
            case E_COC:
                return "ModulSeTeam";
            default:
                return "Unrecognized";
        }
    }

    public static String getSchreibberechtigungenAnzahl(List<Berechtigungsantrag> berechtigungen) {
        List<Berechtigungsantrag> relevantAntraege = getRelevantAntraegeForRechttype(berechtigungen, Rechttype.SCHREIBRECHT);

        if (!relevantAntraege.isEmpty()) {
            String message = "<label>Schreibberechtigungen für " + getBerechtigungZiel(relevantAntraege) + ":" + "</label>";
            return message;
        }

        return "";
    }

    public static String getSchreibberechtigungenAnzahlEn(List<Berechtigungsantrag> berechtigungen) {
        List<Berechtigungsantrag> relevantAntraege = getRelevantAntraegeForRechttype(berechtigungen, Rechttype.SCHREIBRECHT);

        if (!relevantAntraege.isEmpty()) {
            String message = "<label>Write Authorisations for " + getBerechtigungZiel(relevantAntraege) + ":" + "</label>";
            return message;
        }

        return "";
    }

    public static String getLeseberechtigungenAnzahl(List<Berechtigungsantrag> berechtigungen) {
        List<Berechtigungsantrag> relevantAntraege = getRelevantAntraegeForRechttype(berechtigungen, Rechttype.LESERECHT);

        if (!relevantAntraege.isEmpty()) {
            String message = "<label>Leseberechtigungen für " + getBerechtigungZiel(relevantAntraege) + ":" + "</label>";
            return message;
        }

        return "";
    }

    public static String getLeseberechtigungenAnzahlEn(List<Berechtigungsantrag> berechtigungen) {
        List<Berechtigungsantrag> relevantAntraege = getRelevantAntraegeForRechttype(berechtigungen, Rechttype.LESERECHT);

        if (!relevantAntraege.isEmpty()) {
            String message = "<label>Read Authorisations for " + getBerechtigungZiel(relevantAntraege) + ":" + "</label>";
            return message;
        }

        return "";
    }

    public static String getStatusUpdateMessage(List<Berechtigungsantrag> berechtigungen) {
        AntragStatus status = getStatus(berechtigungen);
        String statusUpdateMessage = status.getBezeichnung().toLowerCase();
        return statusUpdateMessage;
    }

    public static String getStatusUpdateMessageEn(List<Berechtigungsantrag> berechtigungen) {
        AntragStatus status = getStatus(berechtigungen);
        String statusUpdateMessage = "";
        switch (status) {
            case ANGENOMMEN: {
                statusUpdateMessage = "granted to You";
                break;
            }
            case ABGELEHNT: {
                statusUpdateMessage = "denied";
                break;
            }

            default:
                break;
        }

        return statusUpdateMessage;
    }

    public static AntragStatus getStatus(List<Berechtigungsantrag> berechtigungen) {
        Berechtigungsantrag berechtigungantragFirst = berechtigungen.get(0);
        AntragStatus status = berechtigungantragFirst.getStatus();
        return status;
    }

    public static String getKommentar(Berechtigungsantrag berechtigungsantrag) {
        String kommentar = berechtigungsantrag.getKommentar();
        return kommentar != null ? kommentar : "";
    }

}
