package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung;

import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiter;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragsverwaltungViewData;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

@Stateless
public class BerechtigungAntragsverwaltungViewDataFactory implements Serializable {

    @Inject
    private BerechtigungAntragVerwaltungService berechtigungAntragService;

    @Produces
    public BerechtigungAntragsverwaltungViewData getViewData() {
        final List<BerechtigungAntragMitarbeiter> allOpenBerechtigungAntraegeForView = berechtigungAntragService.getAllOpenBerechtigungAntraegeForView();
        return new BerechtigungAntragsverwaltungViewData(allOpenBerechtigungAntraegeForView);
    }

}
