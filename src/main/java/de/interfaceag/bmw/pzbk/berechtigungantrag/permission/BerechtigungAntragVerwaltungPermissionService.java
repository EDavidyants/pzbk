package de.interfaceag.bmw.pzbk.berechtigungantrag.permission;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Stateless
public class BerechtigungAntragVerwaltungPermissionService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(BerechtigungAntragVerwaltungPermissionService.class);

    @Inject
    private Session session;

    public Collection<Berechtigungsantrag> restrictToCurrentUser(List<Berechtigungsantrag> allBerechtigungsAntraege) {

        final UserPermissions<BerechtigungDto> userPermissions = session.getUserPermissions();
        final Set<Rolle> roles = userPermissions.getRoles();

        Set<Berechtigungsantrag> restrictedAntraege = new HashSet<>();
        for (Rolle role : roles) {
            LOG.debug("Restrict Berechtigungsantraege for role {}", role);
            final Collection<Berechtigungsantrag> antraegeForRole = restrictForRole(role, userPermissions, allBerechtigungsAntraege);
            restrictedAntraege.addAll(antraegeForRole);
        }

        LOG.info("Restrict {} BerechtigungAntraege for roles {} to {} BerechtigungAntraege", allBerechtigungsAntraege.size(), roles, restrictedAntraege.size());

        return restrictedAntraege;
    }

    private static Collection<Berechtigungsantrag> restrictForRole(Rolle role, UserPermissions<BerechtigungDto> userPermissions, List<Berechtigungsantrag> allBerechtigungsAntraege) {
        switch (role) {
            case ADMIN:
                LOG.debug("Return all antraege for role {}", role);
                return allBerechtigungsAntraege;
            case SENSORCOCLEITER:
                return restrictForRoleSensorCocLeiter(userPermissions, allBerechtigungsAntraege);
            case T_TEAMLEITER:
                return restrictForRoleTteamLeiter(userPermissions, allBerechtigungsAntraege);
            case SCL_VERTRETER:
                return restrictForRoleSensorCocVertreter(userPermissions, allBerechtigungsAntraege);
            case TTEAM_VERTRETER:
                return restrictForRoleTteamVertreter(userPermissions, allBerechtigungsAntraege);
            default:
                LOG.debug("Return no antraege for role {}", role);
                return Collections.emptyList();
        }
    }

    private static Collection<Berechtigungsantrag> restrictForRoleSensorCocLeiter(UserPermissions<BerechtigungDto> userPermissions, Collection<Berechtigungsantrag> allBerechtigungsAntraege) {
        final List<BerechtigungDto> sensorCocAsSensorCocLeiterSchreibend = userPermissions.getSensorCocAsSensorCocLeiterSchreibend();
        LOG.debug("Found {} sensorCoc permissions for role SensorCoc Leiter", sensorCocAsSensorCocLeiterSchreibend.size());
        return restrictToSensorCoc(sensorCocAsSensorCocLeiterSchreibend, allBerechtigungsAntraege);
    }

    private static Collection<Berechtigungsantrag> restrictForRoleSensorCocVertreter(UserPermissions<BerechtigungDto> userPermissions, Collection<Berechtigungsantrag> allBerechtigungsAntraege) {
        final List<BerechtigungDto> sensorCocAsSensorCocVertreterSchreibend = userPermissions.getSensorCocAsVertreterSCLSchreibend();
        LOG.debug("Found {} sensorCoc permissions for role SensorCoc Vertreter", sensorCocAsSensorCocVertreterSchreibend.size());
        return restrictToSensorCoc(sensorCocAsSensorCocVertreterSchreibend, allBerechtigungsAntraege);
    }

    private static Collection<Berechtigungsantrag> restrictForRoleTteamLeiter(UserPermissions<BerechtigungDto> userPermissions, Collection<Berechtigungsantrag> allBerechtigungsAntraege) {
        final List<BerechtigungDto> tteamAsTteamleiterSchreibend = userPermissions.getTteamAsTteamleiterSchreibend();
        LOG.debug("Found {} tteam permissions for role T-Team Leiter", tteamAsTteamleiterSchreibend.size());
        return restrictToTteam(tteamAsTteamleiterSchreibend, allBerechtigungsAntraege);
    }

    private static Collection<Berechtigungsantrag> restrictForRoleTteamVertreter(UserPermissions<BerechtigungDto> userPermissions, Collection<Berechtigungsantrag> allBerechtigungsAntraege) {
        final List<Long> tteamIdsForTteamVertreter = userPermissions.getTteamIdsForTteamVertreter();
        LOG.debug("Found {} tteam permissions for role T-Team Vertreter", tteamIdsForTteamVertreter.size());
        return restrictToTteamIds(tteamIdsForTteamVertreter, allBerechtigungsAntraege);
    }

    private static Collection<Berechtigungsantrag> restrictToSensorCoc(Collection<BerechtigungDto> sensorCocPermissions, Collection<Berechtigungsantrag> allBerechtigungsAntraege) {
        final Set<Long> sensorCocIds = sensorCocPermissions.stream().map(BerechtigungDto::getId).collect(Collectors.toSet());
        return restrictToSensorCocIds(sensorCocIds, allBerechtigungsAntraege);
    }

    private static Collection<Berechtigungsantrag> restrictToSensorCocIds(Collection<Long> sensorCocIds, Collection<Berechtigungsantrag> allBerechtigungsAntraege) {
        return allBerechtigungsAntraege.stream()
                .filter(antrag -> Objects.nonNull(antrag.getSensorCoc()))
                .filter(antrag -> sensorCocIds.contains(antrag.getSensorCoc().getSensorCocId()))
                .collect(Collectors.toSet());
    }

    private static Collection<Berechtigungsantrag> restrictToTteam(Collection<BerechtigungDto> sensorCocPermissions, Collection<Berechtigungsantrag> allBerechtigungsAntraege) {
        final Set<Long> sensorCocIds = sensorCocPermissions.stream().map(BerechtigungDto::getId).collect(Collectors.toSet());
        return restrictToTteamIds(sensorCocIds, allBerechtigungsAntraege);
    }

    private static Collection<Berechtigungsantrag> restrictToTteamIds(Collection<Long> tteamIds, Collection<Berechtigungsantrag> allBerechtigungsAntraege) {
        return allBerechtigungsAntraege.stream()
                .filter(antrag -> Objects.nonNull(antrag.getTteam()))
                .filter(antrag -> tteamIds.contains(antrag.getTteam().getId()))
                .collect(Collectors.toSet());
    }

}
