package de.interfaceag.bmw.pzbk.berechtigungantrag;

import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public final class BerechtigungAntragUtils {

    public static final Logger LOG = LoggerFactory.getLogger(BerechtigungAntragUtils.class);

    private BerechtigungAntragUtils() {
    }

    public static String getBerechtigungLabel(Berechtigungsantrag berechtigungsantrag) {
        final Optional<SensorCoc> sensorCoc = Optional.ofNullable(berechtigungsantrag.getSensorCoc());
        final Optional<Tteam> tteam = Optional.ofNullable(berechtigungsantrag.getTteam());
        final Optional<ModulSeTeam> modulSeTeam = Optional.ofNullable(berechtigungsantrag.getModulSeTeam());

        if (sensorCoc.isPresent()) {
            return sensorCoc.get().pathToString();
        } else if (tteam.isPresent()) {
            return tteam.get().getTeamName();
        } else if (modulSeTeam.isPresent()) {
            return modulSeTeam.get().getName();
        } else {
            LOG.error("No SensorCoc, T-Team or ModulSeTeam present!");
            return "";
        }
    }
    

}
