package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto;

import java.io.Serializable;
import java.util.List;

public interface BerechtigungAntragMitarbeiter extends Serializable {

    String getMitarbeiterLabel();

    Long getMitarbeiterId();

    List<BerechtigungAntragMitarbeiterAntrag> getAntraege();

}
