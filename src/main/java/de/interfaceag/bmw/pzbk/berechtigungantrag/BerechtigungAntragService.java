package de.interfaceag.bmw.pzbk.berechtigungantrag;

import de.interfaceag.bmw.pzbk.berechtigungantrag.mail.BerechtigungsantragMailService;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterAntrag;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.AntragStatus;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.session.Session;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class BerechtigungAntragService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(BerechtigungAntragService.class);

    @Inject
    private BerechtigungAntragDao berechtigungAntragDao;
    @Inject
    private Session session;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private TteamService tteamService;
    @Inject
    private BerechtigungsantragMailService berechtigungsantragMailService;

    public void persistBerechtigungAntrag(Berechtigungsantrag berechtigungsantrag) {
        berechtigungAntragDao.save(berechtigungsantrag);
    }

    public List<Berechtigungsantrag> getAllNotOffenBerechtigungAntraege() {
        return berechtigungAntragDao.getAllNotOffenBerechtigungAntraege();
    }

    public List<Berechtigungsantrag> getAllOpenBerechtigungAntraegeForView() {
        return berechtigungAntragDao.getAllOpenBerechtigungAntraegeForView();
    }

    public List<Berechtigungsantrag> getAntraegeByAntragstellerRolleRechttypSensorCoc(Mitarbeiter antragsteller, Rolle rolle, Rechttype rechttype, SensorCoc sensorCoc) {
        return berechtigungAntragDao.getAntraegeByAntragstellerRolleRechttypSensorCoc(antragsteller, rolle, rechttype, sensorCoc);
    }

    public List<Berechtigungsantrag> getAntraegeByAntragstellerRolleRechttypTteam(Mitarbeiter antragsteller, Rolle rolle, Rechttype rechttype, Tteam tteam) {
        return berechtigungAntragDao.getAntraegeByAntragstellerRolleRechttypTteam(antragsteller, rolle, rechttype, tteam);
    }

    public List<Berechtigungsantrag> getAntraegeByAntragstellerRolleRechttypModulSeTeam(Mitarbeiter antragsteller, Rolle rolle, Rechttype rechttype, ModulSeTeam modulSeTeam) {
        return berechtigungAntragDao.getAntraegeByAntragstellerRolleRechttypModulSeTeam(antragsteller, rolle, rechttype, modulSeTeam);
    }

    public void antragAnnehmen(BerechtigungAntragMitarbeiterAntrag antrag) {
        Berechtigungsantrag berechtigungsantrag = saveAntragWithStatus(antrag, AntragStatus.ANGENOMMEN);
        addBerechtigungToUser(berechtigungsantrag);
        List<Berechtigungsantrag> berechtigungsantraege = new ArrayList<>();
        berechtigungsantraege.add(berechtigungsantrag);
        berechtigungsantragMailService.sendMailBasedOnAntragStatus(berechtigungsantraege);
    }

    private void addBerechtigungToUser(Berechtigungsantrag berechtigungsantrag) {
        if (berechtigungsantrag.getRolle().equals(Rolle.SENSOR)) {
            LOG.debug("Adding berechtigung for {}", berechtigungsantrag);
            grantRoleAndBerechtigungForSensor(berechtigungsantrag);
        }

        if (berechtigungsantrag.getRolle().equals(Rolle.SENSOR_EINGESCHRAENKT)) {
            LOG.debug("Adding berechtigung for {}", berechtigungsantrag);
            grantRoleAndBerechtigungForSensorEingeschraenkt(berechtigungsantrag);
        }

        if (berechtigungsantrag.getRolle().equals(Rolle.TTEAMMITGLIED)) {
            LOG.debug("Adding berechtigung for {}", berechtigungsantrag);
            grantRoleAndBerechtigungForTteammitglied(berechtigungsantrag);
        }

        if (berechtigungsantrag.getRolle().equals(Rolle.E_COC)) {
            LOG.debug("Adding berechtigung for {}", berechtigungsantrag);
            grantRoleAndBerechtigungForEcoc(berechtigungsantrag);
        }
    }

    private void grantRoleAndBerechtigungForSensor(Berechtigungsantrag berechtigungsantrag) {
        List<SensorCoc> cocsForMitarbeiter = berechtigungService.getSensorCocForMitarbeiterAndRolle(berechtigungsantrag.getAntragsteller(), berechtigungsantrag.getRolle(), berechtigungsantrag.getRechttype().equals(Rechttype.SCHREIBRECHT));
        if (!cocsForMitarbeiter.contains(berechtigungsantrag.getSensorCoc())) {
            berechtigungService.grantRoleSensor(berechtigungsantrag.getAntragsteller(), berechtigungsantrag.getSensorCoc().getSensorCocId(), berechtigungsantrag.getRechttype());
        } else {
            LOG.debug("Berechtigung already present for {}", berechtigungsantrag);
        }
    }

    private void grantRoleAndBerechtigungForSensorEingeschraenkt(Berechtigungsantrag berechtigungsantrag) {
        List<SensorCoc> cocsForMitarbeiter = berechtigungService.getSensorCocForMitarbeiterAndRolle(berechtigungsantrag.getAntragsteller(), berechtigungsantrag.getRolle(), berechtigungsantrag.getRechttype().equals(Rechttype.SCHREIBRECHT));
        if (!cocsForMitarbeiter.contains(berechtigungsantrag.getSensorCoc())) {
            berechtigungService.grantRoleSensorEingeschraenkt(berechtigungsantrag.getAntragsteller(), berechtigungsantrag.getSensorCoc().getSensorCocId(), berechtigungsantrag.getRechttype());
        } else {
            LOG.debug("Berechtigung already present for {}", berechtigungsantrag);
        }
    }

    private void grantRoleAndBerechtigungForTteammitglied(Berechtigungsantrag berechtigungsantrag) {
        List<Long> tteamIdsForMitarbeiter = tteamService.getTteamIdsforTteamMitgliedAndRechttype(berechtigungsantrag.getAntragsteller(), berechtigungsantrag.getRechttype().getId());

        if (!tteamIdsForMitarbeiter.contains(berechtigungsantrag.getTteam().getId())) {
            berechtigungService.grantRoleTteamMitglied(berechtigungsantrag.getAntragsteller(), Collections.singletonList(berechtigungsantrag.getTteam()), berechtigungsantrag.getRechttype());
        } else {
            LOG.debug("Berechtigung already present for {}", berechtigungsantrag);
        }
    }

    private void grantRoleAndBerechtigungForEcoc(Berechtigungsantrag berechtigungsantrag) {
        List<ModulSeTeam> seTeamListForMitarbeiter = berechtigungService.getAuthorizedModulSeTeamListForMitarbeiter(berechtigungsantrag.getAntragsteller());
        if (!seTeamListForMitarbeiter.contains(berechtigungsantrag.getModulSeTeam())) {
            berechtigungService.grantRoleEcoc(berechtigungsantrag.getAntragsteller(), berechtigungsantrag.getModulSeTeam(), berechtigungsantrag.getRechttype());
        } else {
            LOG.debug("Berechtigung already present for {}", berechtigungsantrag);
        }
    }

    public void antragAblehnen(BerechtigungAntragMitarbeiterAntrag antrag) {
        saveAntragWithStatus(antrag, AntragStatus.ABGELEHNT);
        Berechtigungsantrag berechtigungsantrag = getAntragById(antrag.getBerechtigungAntragId());
        List<Berechtigungsantrag> berechtigungsantraege = new ArrayList<>();
        berechtigungsantraege.add(berechtigungsantrag);
        berechtigungsantragMailService.sendMailBasedOnAntragStatus(berechtigungsantraege);
    }

    private Berechtigungsantrag saveAntragWithStatus(BerechtigungAntragMitarbeiterAntrag antrag, AntragStatus status) {
        Berechtigungsantrag berechtigungAntrag = berechtigungAntragDao.getAntraegByAntragId(antrag.getBerechtigungAntragId());
        berechtigungAntrag.setStatus(status);
        berechtigungAntrag.setBearbeiter(session.getUser());
        berechtigungAntrag.setAenderungsdatum(new Date());
        berechtigungAntrag.setKommentar(antrag.getKommentar());
        persistBerechtigungAntrag(berechtigungAntrag);

        return berechtigungAntrag;
    }

    public Berechtigungsantrag getAntragById(Long id) {
        return berechtigungAntragDao.getAntraegByAntragId(id);
    }

}
