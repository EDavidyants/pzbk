package de.interfaceag.bmw.pzbk.berechtigungantrag.history;

import de.interfaceag.bmw.pzbk.berechtigungantrag.history.dto.BerechtigungAntragVerwaltungHistoryViewData;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class BerechtigungAntragVerwaltungHistoryController implements Serializable {

    @Inject
    private BerechtigungAntragVerwaltungHistoryViewData viewData;

    @Inject
    private BerechtigungAntragVerwaltungHistoryFilter filter;

    public BerechtigungAntragVerwaltungHistoryViewData getViewData() {
        return viewData;
    }

    public BerechtigungAntragVerwaltungHistoryFilter getFilter() {
        return filter;
    }


}
