package de.interfaceag.bmw.pzbk.berechtigungantrag.arbeitsvorrat;

import de.interfaceag.bmw.pzbk.berechtigungantrag.BerechtigungAntragService;
import de.interfaceag.bmw.pzbk.berechtigungantrag.permission.BerechtigungAntragVerwaltungPermissionService;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Stateless
public class BerechtigungAntragArbeitsvorratService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(BerechtigungAntragArbeitsvorratService.class);

    @Inject
    private BerechtigungAntragService berechtigungAntragService;
    @Inject
    private BerechtigungAntragVerwaltungPermissionService berechtigungAntragVerwaltungPermissionService;

    public int getArbeitsvorratForCurrentUser() {
        final List<Berechtigungsantrag> allOpenBerechtigungAntraegeForView = berechtigungAntragService.getAllOpenBerechtigungAntraegeForView();
        LOG.debug("Loaded {} entries from database for view", allOpenBerechtigungAntraegeForView.size());

        final Collection<Berechtigungsantrag> restrictedBerechtigungsAntraege = berechtigungAntragVerwaltungPermissionService.restrictToCurrentUser(allOpenBerechtigungAntraegeForView);
        LOG.debug("Restricted Arbeitsvorrat for user: {}", restrictedBerechtigungsAntraege.size());

        return restrictedBerechtigungsAntraege.size();
    }

}
