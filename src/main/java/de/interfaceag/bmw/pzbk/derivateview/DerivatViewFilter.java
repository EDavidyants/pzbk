package de.interfaceag.bmw.pzbk.derivateview;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;

/**
 *
 * @author fn
 */
public class DerivatViewFilter implements Serializable {

    private static final Page PAGE = Page.PROJEKTVERWALTUNG;

    private final TextSearchFilter derivateFilter;
    private final TextSearchFilter produktlinieFilter;

    protected DerivatViewFilter(UrlParameter urlParameter) {
        this.derivateFilter = new TextSearchFilter("derivate", urlParameter);
        this.produktlinieFilter = new TextSearchFilter("produktlinie", urlParameter);
    }

    public String getUrl() {
        return PAGE.getUrl() + getUrlParameter();
    }

    private String getUrlParameter() {
        StringBuilder sb = new StringBuilder("?faces-redirect=true");
        sb.append(derivateFilter.getIndependentParameter());
        sb.append(produktlinieFilter.getIndependentParameter());
        return sb.toString();
    }

    public TextSearchFilter getDerivateFilter() {
        return derivateFilter;
    }

    public TextSearchFilter getProduktlinieFilter() {
        return produktlinieFilter;
    }

    public String getResetUrl() {
        return PAGE.getUrl() + "?faces-redirect=true";
    }

}
