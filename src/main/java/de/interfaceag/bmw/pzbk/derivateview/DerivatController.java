package de.interfaceag.bmw.pzbk.derivateview;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusChangeController;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusChangeDTO;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusChangePermission;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.TimerType;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author ig
 */
@ViewScoped
@Named
public class DerivatController implements DerivatDeleteDialogMethods, Serializable, DerivatStatusChangeController {

    private static final Logger LOG = LoggerFactory.getLogger(DerivatController.class);

    @Inject
    private Session session;

    @Inject
    private DerivatViewFacade derivatViewFacade;

    @Inject
    private DerivatStatusNames derivatStatusNames;

    private DerivatViewData derivatViewData;

    private DerivateViewPermission viewPermission;

    private DerivatStatusChangeDTO derivatStatusChangeDTO;

    @PostConstruct
    public void init() {

        initViewData();
        session.setLocationForView();
        viewPermission = new DerivateViewPermission(session.getUserPermissions().getRoles());
    }

    private void initViewData() {
        derivatViewData = derivatViewFacade.getInitData(getUrlParameter());
    }

    UrlParameter getUrlParameter() {
        return UrlParameterUtils.getUrlParameter();
    }

    public DerivatViewFilter getFilter() {
        return derivatViewData.getFilter();
    }

    public String reset() {
        return getFilter().getResetUrl();
    }

    public String filter() {
        return getFilter().getUrl();
    }

    public void aktualisierePhasen() {
        FacesContext context = getCurrentFacesInstance();
        Map<TimerType, Integer> status = derivatViewFacade.aktualisierePhasen();
        StringBuilder stringBuilder = new StringBuilder();
        status.entrySet().stream().forEach(entry -> {
            stringBuilder.append(entry.getKey().getAction());
            stringBuilder.append(": ");
            stringBuilder.append(entry.getValue().toString());
            stringBuilder.append("\n");
        });
        context.addMessage(null,
                new FacesMessage(
                        FacesMessage.SEVERITY_INFO,
                        "Aktualisierung abgeschlossen!",
                        stringBuilder.toString())
        );
    }

    FacesContext getCurrentFacesInstance() {
        return FacesContext.getCurrentInstance();
    }

    public String saveNewDerivat() {
        derivatViewFacade.createNewDerivat(derivatViewData.getDerivat());
        return filter();
    }

    public void openDeleteDerivatDialog(Derivat derivat) {
        derivatViewData.setSelectedDerivat(derivat);
        RequestContext context = getCurrentRequestInstance();
        context.update("dialog:derivatDeleteConfirmDialogForm");
        context.execute("PF('derivatDeleteConfirmDialog').show()");
    }

    RequestContext getCurrentRequestInstance() {
        return RequestContext.getCurrentInstance();
    }

    public void openChangeDerivatStatusDialog(Derivat derivat) {
        initChangeStatusDialogDTO(derivat);
        derivatViewData.setSelectedDerivat(derivat);
        RequestContext context = getCurrentRequestInstance();
        context.update("dialog:derivatChangeStatusDialogForm");
        context.execute("PF('derivatChangeStatusDialog').show()");
    }

    private void initChangeStatusDialogDTO(Derivat derivat) {
        derivatStatusChangeDTO = new DerivatStatusChangeDTO(derivat, derivatStatusNames);
    }

    public DerivatStatusChangeDTO getDerivatStatusChangeDTO() {
        return derivatStatusChangeDTO;
    }

    @Override
    public String getDerivatDeleteMessage() {
        return derivatViewFacade.getDerivatDeleteMessage(derivatViewData.getSelectedDerivat());
    }

    @Override
    public String getDerivatZuordnungen() {
        return derivatViewFacade.getDerivatZuordnungen(derivatViewData.getSelectedDerivat());
    }

    @Override
    public void proceedWithDerivatRemoval() {
        derivatViewFacade.changeStatusToInactive(derivatViewData.getSelectedDerivat());
        reloadDerivate(getUrlParameter());
    }

    public void restoreDeletedDerivat(Derivat derivat) {
        derivatViewData.setSelectedDerivat(derivat);
        derivatViewFacade.restoreDeletedDerivat(derivat);
        reloadDerivate(getUrlParameter());
    }

    private void reloadDerivate(UrlParameter urlParameter) {
        List<Derivat> currentlySelectedDerivate = derivatViewData.getFilteredDerivate();
        List<Derivat> updatedDerivate = derivatViewFacade.getUpdatedDerivate(urlParameter);
        List<Derivat> updatedSelectedDerivate = derivatViewFacade.getUpdatedSelectedDerivate(updatedDerivate, currentlySelectedDerivate);

        derivatViewData.setDerivate(updatedDerivate);
        derivatViewData.setFilteredDerivate(updatedSelectedDerivate);

        getCurrentRequestInstance().update("derivatAccordionPanel:derivateForm:derivateTable");
    }

    public void onRowEdit(RowEditEvent event) {
        Derivat derivat = (Derivat) event.getObject();
        derivatViewFacade.persistDerivat(derivat);
    }

    public void onKovAPhaseEdit(RowEditEvent event) {
        KovAPhaseImDerivat kova = (KovAPhaseImDerivat) event.getObject();
        if (kova.getStartDate() != null && kova.getEndDate() != null) {
            derivatViewFacade.persistKovAPhaseImDerivat(kova);
            derivatViewData.setStartDate(LocalDate.now());
            derivatViewData.setEndDate(null);
        } else {
            kova.setStartDate(null);
            kova.setEndDate(null);
        }
    }

    public void onKovAPhaseClear(RowEditEvent event) {
        KovAPhaseImDerivat kova = (KovAPhaseImDerivat) event.getObject();
        derivatViewFacade.clearKovAPhaseImDerivat(kova);
        derivatViewData.setStartDate(LocalDate.now());
        derivatViewData.setEndDate(null);
    }

    public DerivateViewPermission getViewPermission() {
        return viewPermission;
    }

    public DerivatViewData getDerivatViewData() {
        return derivatViewData;
    }

    public void filterByAktiveStatus() {
        Boolean isAktiveStatusFilterOn = derivatViewData.getOnlyActiveFilter();
        List<Derivat> filteredDerivate = derivatViewFacade.getUpdatedFilteredDerivate(derivatViewData.getDerivate(), isAktiveStatusFilterOn);
        derivatViewData.setFilteredDerivate(filteredDerivate);
        getCurrentRequestInstance().update("derivatAccordionPanel:derivateForm:derivateTable");
    }

    @Override
    public String changeDerivatStatus(DerivatStatus nextStatus) {
        Derivat selectedDerivat = derivatViewData.getSelectedDerivat();
        LOG.debug("Changing Derivat {} to Status {}", selectedDerivat, nextStatus);
        derivatViewFacade.changeDerivatStatus(selectedDerivat, nextStatus);
        return PageUtils.getRedirectUrlForPage(Page.PROJEKTVERWALTUNG);
    }

    public String convertDerivatStatusToString(Derivat derivat) {
        DerivatStatus status = derivat.getStatus();
        return derivatStatusNames.getNameForStatus(status);
    }

    public boolean isStatusChangeEnabled(Derivat derivat) {
        Set<Rolle> roles = session.getUserPermissions().getRoles();
        DerivatStatus status = derivat.getStatus();
        return DerivatStatusChangePermission.isStatusChangeEnabled(roles, status);
    }

}
