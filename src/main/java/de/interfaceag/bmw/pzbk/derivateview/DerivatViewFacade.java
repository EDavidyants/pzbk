package de.interfaceag.bmw.pzbk.derivateview;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusChangeService;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusService;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.TimerType;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.FahrzeugmerkmaleProgressService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmaleProgressDto;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author fn
 */
@Stateless
public class DerivatViewFacade implements Serializable {

    @Inject
    private ConfigService configService;
    @Inject
    private DerivatStatusNames derivatStatusNames;
    @Inject
    private DerivatStatusService derivatStatusService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private DerivatSearchService derivatSearchService;
    @Inject
    private DerivatStatusChangeService derivatStatusChangeSerivce;
    @Inject
    private FahrzeugmerkmaleProgressService derivatAuspraegungProgressService;

    @Inject
    private LocalizationService localizationService;

    private List<String> getAllProduktlinienAsStringList() {
        return configService.getAllProduklinienAsStringList();
    }

    public void clearKovAPhaseImDerivat(KovAPhaseImDerivat kova) {
        derivatService.clearKovAPhaseImDerivat(kova);
    }

    public void persistKovAPhaseImDerivat(KovAPhaseImDerivat kova) {
        derivatService.persistKovAPhaseImDerivat(kova);
    }

    public void persistDerivat(Derivat deri) {
        derivatService.persistDerivat(deri);
    }

    public void changeStatusToInactive(Derivat derivat) {
        derivatStatusChangeSerivce.changeStatus(derivat, DerivatStatus.INAKTIV);
    }

    public void createNewDerivat(Derivat derivat) {
        derivatService.createNewDerivat(derivat);
    }

    public Map<TimerType, Integer> aktualisierePhasen() {
        return derivatService.aktualisierePhasen();
    }

    public DerivatViewData getInitData(UrlParameter urlParameter) {

        DerivatViewFilter viewFilter = getViewFilter(urlParameter);

        LocalDate currentDate = LocalDate.now();
        LocalDate startDate = LocalDate.now();

        Derivat derivat = new Derivat();

        List<Derivat> derivate = getDerivateByFilterList(viewFilter);

        List<String> allProduktlinien = getAllProduktlinienAsStringList();

        int totalNumberOfFahrzeugmerkmale = derivatAuspraegungProgressService.getTotalNumberOfFahrzeugmerkmale();
        Collection<FahrzeugmerkmaleProgressDto> fahrzeugmerkmaleProgressDtos = derivatAuspraegungProgressService.generateFahrzeugmerkmaleProgressDtosForDerivate(derivate);

        String derivatName = "";


        return new DerivatViewData(derivat, derivate,
                totalNumberOfFahrzeugmerkmale, fahrzeugmerkmaleProgressDtos,
                allProduktlinien,
                derivatName,
                currentDate, startDate, viewFilter);
    }

    private List<Derivat> getDerivateByFilterList(DerivatViewFilter viewFilter) {
        return derivatSearchService.getSeachResultForCurrentUser(viewFilter.getDerivateFilter(),
                viewFilter.getProduktlinieFilter());
    }

    private DerivatViewFilter getViewFilter(UrlParameter urlParameter) {
        return new DerivatViewFilter(urlParameter);
    }

    public String getDerivatDeleteMessage(Derivat derivat) {
        StringBuilder sb = new StringBuilder();
        sb.append(localizationService.getValue("derivat_delete_meldungTeilEins")).append(" ")
                .append(derivat.getName()).append(" ")
                .append(localizationService.getValue("derivat_delete_meldungTeilZwei"));
        return sb.toString();
    }

    public String getDerivatZuordnungen(Derivat derivat) {
        String resultEmpty = localizationService.getValue("derivat_zuordnungen_keine");
        String result = derivatService.findZuordnungenForDerivat(derivat);
        return !result.isEmpty() ? result : resultEmpty;
    }

    public void restoreDeletedDerivat(Derivat derivat) {
        derivatStatusChangeSerivce.restoreToLastStatus(derivat);
    }

    public List<Derivat> getUpdatedFilteredDerivate(List<Derivat> alleDerivate, Boolean activeStatusFilter) {
        if (isInputValid(alleDerivate)) {
            List<Derivat> filteredDerivate = new ArrayList<>();
            if (activeStatusFilter) {
                alleDerivate.stream()
                        .filter(d -> !d.isInaktiv())
                        .forEach(filteredDerivate::add);
            } else {
                filteredDerivate.addAll(alleDerivate);
            }

            return filteredDerivate;
        }

        return new ArrayList<>();
    }

    private static boolean isInputValid(List<Derivat> derivate) {
        return derivate != null && !derivate.isEmpty();
    }

    public List<Derivat> getUpdatedDerivate(UrlParameter urlParameter) {
        DerivatViewFilter viewFilter = getViewFilter(urlParameter);
        return getDerivateByFilterList(viewFilter);
    }

    public List<Derivat> getUpdatedSelectedDerivate(List<Derivat> alleDerivate, List<Derivat> selectedDerivate) {

        if (isVorhanden(alleDerivate) && isVorhanden(selectedDerivate)) {
            List idsSelected = selectedDerivate.stream().map(Derivat::getId).collect(Collectors.toList());
            return alleDerivate.stream()
                    .filter(d -> idsSelected.contains(d.getId()))
                    .collect(Collectors.toList());
        }

        return isVorhanden(alleDerivate) ? alleDerivate : new ArrayList<>();
    }

    private static boolean isVorhanden(List<Derivat> derivate) {
        return derivate != null && !derivate.isEmpty();
    }

    public String convertDerivatStatusToString(Derivat derivat) {
        DerivatStatus status = derivat.getStatus();
        String localizationName = status.getLocalizationName();
        return localizationService.getValue(localizationName);
    }

    public String convertDerivatStatusToString(DerivatStatus status) {
        return derivatStatusNames.getNameForStatus(status);

    }

    public DerivatViewFacade getDerivatViewFacade() {
        return this;
    }

    public String getStatusChangeNameForStatus(DerivatStatus status) {
        return derivatStatusService.getStatusChangeNameForStatus(status);
    }

    public Collection<DerivatStatus> getPossibleNextStatus(DerivatViewData derivatViewData) {
        return derivatViewData.getSelectedDerivat().getStatus().getNextStatus();
    }

    public void changeDerivatStatus(Derivat derivat, DerivatStatus selectedNextStatus) {
        derivatService.updateDerivatStatus(derivat, selectedNextStatus);
    }
}
