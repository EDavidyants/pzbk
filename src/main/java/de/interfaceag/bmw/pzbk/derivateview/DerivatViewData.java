package de.interfaceag.bmw.pzbk.derivateview;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.api.FahrzeugmerkmaleProgressBarMethods;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmaleProgressDto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * @author fn
 */
public class DerivatViewData implements DerivatDeleteDialogFields,
        FahrzeugmerkmaleProgressBarMethods, Serializable {

    private final DerivatViewFilter derivatViewFilter;
    private Derivat derivat;
    private Derivat selectedDerivat;
    private List<Derivat> derivate;
    private List<String> allProduktlinien;

    private String derivatName;


    private LocalDate currentDate;
    private LocalDate startDate;
    private LocalDate endDate;

    private Boolean onlyActiveFilter;
    private List<Derivat> filteredDerivate;

    private final int totalNumberOfFahrzeugmerkmale;
    private Collection<FahrzeugmerkmaleProgressDto> fahrzeugmerkmaleProgressDtos;

    public DerivatViewData(Derivat derivat, List<Derivat> derivate,
                           int totalNumberOfFahrzeugmerkmale, Collection<FahrzeugmerkmaleProgressDto> fahrzeugmerkmaleProgressDtos,
                           List<String> allProduktlinien, String derivatName,
                           LocalDate currentDate, LocalDate startDate, DerivatViewFilter derivatViewFilter) {
        this.derivat = derivat;
        this.derivate = derivate;
        this.totalNumberOfFahrzeugmerkmale = totalNumberOfFahrzeugmerkmale;
        this.fahrzeugmerkmaleProgressDtos = fahrzeugmerkmaleProgressDtos;
        this.allProduktlinien = allProduktlinien;
        this.derivatName = derivatName;
        this.currentDate = currentDate;
        this.startDate = startDate;
        this.derivatViewFilter = derivatViewFilter;
        this.onlyActiveFilter = Boolean.TRUE;
        this.filteredDerivate = filterOutActiveOnly(derivate);
    }

    public Derivat getDerivat() {
        return derivat;
    }

    public void setDerivat(Derivat derivat) {
        this.derivat = derivat;
    }

    @Override
    public Derivat getSelectedDerivat() {
        return selectedDerivat;
    }

    @Override
    public void setSelectedDerivat(Derivat selectedDerivat) {
        this.selectedDerivat = selectedDerivat;
    }

    public List<Derivat> getDerivate() {
        return derivate;
    }

    public void setDerivate(List<Derivat> derivate) {
        this.derivate = derivate;
    }

    public List<String> getAllProduktlinien() {
        return allProduktlinien;
    }

    public void setAllProduktlinien(List<String> allProduktlinien) {
        this.allProduktlinien = allProduktlinien;
    }

    public String getDerivatName() {
        return derivatName;
    }

    public void setDerivatName(String derivatName) {
        this.derivatName = derivatName;
    }

    public LocalDate getCurrentDate() {
        if (currentDate != null) {
            return currentDate;
        }
        return null;
    }

    public void setCurrentDate(LocalDate currentDate) {
        this.currentDate = currentDate;
    }

    public LocalDate getStartDate() {
        if (startDate != null) {
            return startDate;
        }
        return null;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        if (endDate != null) {
            return endDate;
        }
        return null;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public DerivatViewFilter getFilter() {
        return derivatViewFilter;
    }

    public Boolean getOnlyActiveFilter() {
        return onlyActiveFilter;
    }

    public void setOnlyActiveFilter(Boolean onlyActiveFilter) {
        this.onlyActiveFilter = onlyActiveFilter;
    }

    public List<Derivat> getFilteredDerivate() {
        return filteredDerivate;
    }

    public void setFilteredDerivate(List<Derivat> filteredDerivate) {
        this.filteredDerivate = filteredDerivate;
    }

    private List<Derivat> filterOutActiveOnly(List<Derivat> derivate) {
        if (derivate != null && !derivate.isEmpty()) {
            return derivate.stream().filter(d -> !d.isInaktiv()).collect(Collectors.toList());
        }

        return new ArrayList<>();
    }

    public int getTotalNumberOfFahrzeugmerkmale() {
        return totalNumberOfFahrzeugmerkmale;
    }

    public Collection<FahrzeugmerkmaleProgressDto> getFahrzeugmerkmaleProgressDtos() {
        return fahrzeugmerkmaleProgressDtos;
    }

    @Override
    public long getProgressValueForDerivat(Derivat derivat) {
        long abgeschlossen = getAbgeschlossenForDerivatId(derivat.getId());
        if (totalNumberOfFahrzeugmerkmale > 0) {
            return abgeschlossen * 100 / totalNumberOfFahrzeugmerkmale;
        } else {
            return 100L;
        }
    }

    @Override
    public String getProgressBarLabelForDerivat(Derivat derivat) {
        long abgeschlossen = getAbgeschlossenForDerivatId(derivat.getId());
        return abgeschlossen + " / " + getTotalNumberOfFahrzeugmerkmale();
    }

    @Override
    public boolean isAbgeschlossenForDerivat(Derivat derivat) {
        long abgeschlossen = getAbgeschlossenForDerivatId(derivat.getId());
        return abgeschlossen == getTotalNumberOfFahrzeugmerkmale();
    }

    private long getAbgeschlossenForDerivatId(Long derivatId) {
        Optional<Long> abgeschlossenFound = getFahrzeugmerkmaleProgressDtos().stream()
                .filter(dto -> dto.getDerivatId().equals(derivatId))
                .map(FahrzeugmerkmaleProgressDto::getAbgeschlossen).findAny();
        return abgeschlossenFound.orElse(0L);
    }

}
