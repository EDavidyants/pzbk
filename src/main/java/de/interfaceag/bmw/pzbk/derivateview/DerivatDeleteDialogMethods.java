package de.interfaceag.bmw.pzbk.derivateview;

/**
 *
 * @author evda
 */
public interface DerivatDeleteDialogMethods {

    String getDerivatDeleteMessage();

    String getDerivatZuordnungen();

    void proceedWithDerivatRemoval();

}
