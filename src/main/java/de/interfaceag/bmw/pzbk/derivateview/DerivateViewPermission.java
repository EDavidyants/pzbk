package de.interfaceag.bmw.pzbk.derivateview;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

/**
 * @author fn
 */
public class DerivateViewPermission implements Serializable {

    private final ViewPermission page;

    private final EditPermission aktualisierungButton;
    private final EditPermission derivatBearbeitenButton;
    private final EditPermission kovaPhasenBearbeitenButton;

    private final ViewPermission neuesDerivatPanel;

    public DerivateViewPermission(Set<Rolle> rolesWithWritePermissions) {

        page = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(rolesWithWritePermissions).get();

        neuesDerivatPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .compareTo(rolesWithWritePermissions).get();

        aktualisierungButton = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN)
                .compareTo(rolesWithWritePermissions).get();

        derivatBearbeitenButton = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN)
                .compareTo(rolesWithWritePermissions).get();

        kovaPhasenBearbeitenButton = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN)
                .authorizeWriteForRole(Rolle.ANFORDERER)
                .compareTo(rolesWithWritePermissions).get();

    }

    public boolean getPage() {
        return page.isRendered();
    }

    public boolean getNeuesDerivatPanel() {
        return neuesDerivatPanel.isRendered();
    }

    public boolean getAktualisierungButton() {
        return aktualisierungButton.hasRightToEdit();
    }

    public boolean getDerivatBearbeitenButton() {
        return derivatBearbeitenButton.hasRightToEdit();
    }

    public boolean getKovaPhasenBearbeitenButton() {
        return kovaPhasenBearbeitenButton.hasRightToEdit();
    }

}
