package de.interfaceag.bmw.pzbk.derivateview;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fn
 */
@Stateless
public class DerivatSearchService implements Serializable {

    @Inject
    private BerechtigungService berechtigungService;

    public List<Derivat> getSeachResultForCurrentUser(TextSearchFilter derivatFilter, TextSearchFilter produktlinieFilter) {

        List<Derivat> searchList = berechtigungService.getAllExistingDerivateForCurrentUser();
        if (derivatFilter.isActive()) {
            searchList = getSearchByDerivatString(searchList, derivatFilter.getAsString());
        }
        if (produktlinieFilter.isActive()) {
            searchList = getSearchByProduktlinieFilter(searchList, produktlinieFilter.getAsString());
        }
        return searchList;
    }

    private List<Derivat> getSearchByDerivatString(List<Derivat> derivate, String filterString) {
        List<Derivat> returnList = new ArrayList<>();
        derivate.stream()
                .filter(derivat -> derivat.getName().toLowerCase().contains(filterString.toLowerCase()))
                .forEachOrdered(returnList::add);
        return returnList;
    }

    private List<Derivat> getSearchByProduktlinieFilter(List<Derivat> derivate, String filterString) {
        List<Derivat> returnList = new ArrayList<>();
        derivate.stream()
                .filter(derivat -> derivat.getProduktlinie().toLowerCase().contains(filterString.toLowerCase()))
                .forEachOrdered(returnList::add);
        return returnList;
    }

}
