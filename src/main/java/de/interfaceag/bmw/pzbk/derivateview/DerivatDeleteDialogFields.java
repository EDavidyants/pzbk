package de.interfaceag.bmw.pzbk.derivateview;

import de.interfaceag.bmw.pzbk.entities.Derivat;

/**
 *
 * @author evda
 */
public interface DerivatDeleteDialogFields {

    Derivat getSelectedDerivat();

    void setSelectedDerivat(Derivat selectedDerivat);

}
