package de.interfaceag.bmw.pzbk.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public enum Type {
    MELDUNG(0), ANFORDERUNG(1), PROZESSBAUKASTEN(2);

    // ------------  fields ----------------------------------------------------
    private final int id;
    private final String bezeichnung;
    private final String kennzeichen;

    // ------------  constructors ----------------------------------------------
    Type(int id) {
        this.id = id;

        switch (id) {
            case 0:
                this.bezeichnung = "Meldung";
                this.kennzeichen = "M";
                break;
            case 1:
                this.bezeichnung = "Anforderung";
                this.kennzeichen = "A";
                break;
            case 2:
                this.bezeichnung = "Prozessbaukasten";
                this.kennzeichen = "P";
                break;
            default:
                this.bezeichnung = "";
                this.kennzeichen = null;
                break;

        }
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return getBezeichnung();
    }

    public static Optional<Type> getById(int id) {
        return Stream.of(Type.values()).filter(t -> t.getId() == id).findAny();
    }

    public boolean isAnforderung() {
        return this.equals(Type.ANFORDERUNG);
    }

    public boolean isMeldung() {
        return this.equals(Type.MELDUNG);
    }

    public boolean isProzessbaukasten() {
        return this.equals(Type.PROZESSBAUKASTEN);
    }

    public static List<Type> getActiveFilters() {
        return Collections.unmodifiableList(Arrays.asList(Type.ANFORDERUNG, Type.MELDUNG, Type.PROZESSBAUKASTEN));
    }

    // ------------  getter / setter -------------------------------------------
    public int getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public String getKennzeichen() {
        return kennzeichen;
    }

    public static Type getByKennzeichen(String kennzeichen) {
        switch (kennzeichen) {
            case "A":
                return Type.ANFORDERUNG;
            case "M":
                return Type.MELDUNG;
            default:
                return null;
        }
    }

}
