package de.interfaceag.bmw.pzbk.enums;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author fn
 */
public enum KovAPhase {

    VABG0(1, "VABG0"),
    VABG1(2, "VABG1"), VABG2(3, "VABG2"), VA_VKBG(4, "VA VKBG"), VKBG(5, "VKBG"),
    VA_VBBG(6, "VA VBBG"), VBBG(7, "VBBG"), BBG(8, "BBG");

    private final int id;
    private final String bezeichnung;

    KovAPhase(int id, String bezeichnung) {
        this.id = id;
        this.bezeichnung = bezeichnung;
    }

    @Override
    public String toString() {
        return this.name();
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public int getId() {
        return id;
    }

    public static List<KovAPhase> getAllKovAPhasen() {
        return Arrays.asList(KovAPhase.values());
    }

    public static Optional<KovAPhase> getPreviousKovaphase(KovAPhase kovAPhase) {
        if (kovAPhase == null || kovAPhase.getId() <= 1) {
            return Optional.empty();
        } else {
            return getById(kovAPhase.getId() - 1);
        }
    }

    public static Optional<KovAPhase> getById(int id) {
        return Stream.of(KovAPhase.values())
                .filter(kovaphase -> kovaphase.getId() == id)
                .findAny();
    }
}
