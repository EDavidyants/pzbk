package de.interfaceag.bmw.pzbk.enums;

import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author evda
 */
public enum DashboardStep {
    MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG(1, "Entwurf | unstimmig"),
    GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG(2, "Gemeldet | in Arbeit | unstimmig"),
    ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT(3, "Abgestimmt | plausibilisiert"),
    FREIGEGEBENEN_ANFORDERUNGEN(4, "Freigegeben"),
    ANFORDERUNGEN_VEREINBART(5, "Vereinbart"),
    ANFORDERUNGEN_UMGESETZT(6, "Umgesetzt"),
    BESTAETIGER_ZUORDNEN(8, "Bestaetiger zuordnen"),
    UMSETZUNG_BESTAETIGEN(9, "Umsetzung bestaetigen"),
    ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET(10, "Unzureichende Anforderungsqualitaet"),
    NICHT_UMGESETZTEN_ANFORDERUNGEN(11, "Nicht umgesetzt"),
    ANFORDERUNGEN_OFFEN_IN_ZAK(12, "ZAK | offen"),
    ANFORDERUNGEN_ABGELEHNT_IN_ZAK(13, "ZAK | abgelehnt"),
    LANGLAEUFER(14, "Langlaeufer"),
    BERECHTIGUNG_ANTRAG(15, "Berechtigungsantrag");

    private final int stepNumber;
    private final String stepLabel;

    DashboardStep(int stepNumber, String stepLabel) {
        this.stepNumber = stepNumber;
        this.stepLabel = stepLabel;
    }

    public static Optional<DashboardStep> getByStepNumber(int stepNumber) {
        return Stream.of(DashboardStep.values()).filter(step -> step.getStepNumber() == stepNumber).findAny();
    }

    public int getStepNumber() {
        return stepNumber;
    }

    public String getStepLabel() {
        return stepLabel;
    }

    @Override
    public String toString() {
        return "Dashboard Step " + this.stepNumber + ": " + this.stepLabel;
    }

}
