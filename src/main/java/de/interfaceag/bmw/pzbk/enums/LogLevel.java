package de.interfaceag.bmw.pzbk.enums;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public enum LogLevel {
    INFO(0, "Information"),
    WARNING(1, "Warnung"),
    ERROR(2, "Fehler"),
    SEVERE(3, "kritischer Fehler"),
    ALL(4, "All"),
    CONFIG(5, "Konfiguration"),
    FINE(6, "FINE");

    private final int id;
    private final String bezeichnung;

    LogLevel(int id, String bezeichnung) {
        this.id = id;
        this.bezeichnung = bezeichnung;
    }

    @Override
    public String toString() {
        return getBezeichnung();
    }

    public int getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public static Optional<LogLevel> getByBezeichnung(String bezeichnung) {
        if (bezeichnung != null) {
            return Stream.of(LogLevel.values()).filter(e -> e.getBezeichnung().equals(bezeichnung)).findAny();
        }
        return Optional.empty();
    }
}
