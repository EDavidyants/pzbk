package de.interfaceag.bmw.pzbk.enums;

import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public enum Kategorie {

    EINS(1),
    ZWEI(2);

    private final int id; // id to store in database

    Kategorie(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.name();
    }

    public int getId() {
        return id;
    }

    public static Kategorie getById(int id) {
        return Stream.of(Kategorie.values()).filter(referenzSystem -> referenzSystem.getId() == id).findAny().orElse(null);
    }

}
