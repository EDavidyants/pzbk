package de.interfaceag.bmw.pzbk.enums;

/**
 * @author evda
 */
public enum SearchFilterType {

    STATUS(0), SENSORCOC(1), TTEAM(2), DERIVAT(3), DASHBOARD(4), MELDUNG(5), ANFORDERUNG(6), ID(7),
    ERSTELLUNGSDATUM(8), AENDERUNGSDATUM(9), STATUS_ANFORDERUNG(10), STATUS_MELDUNG(11),
    AENDERUNGSDATUM_VON(12), AENDERUNGSDATUM_BIS(13), ERSTELLUNGSDATUM_VON(14), ERSTELLUNGSDATUM_BIS(15),
    STATUS_AENDERUNGSDATUM(16), STATUSAENDERUNGSDATUM_VON(17), STATUS_AENDERUNGSDATUM_BIS(18),
    SORT_NAME_ASC(19), SORT_NAME_DESC(20), SORT_AENDERUNGSDATUM_ASC(21), SORT_AENDERUNGSDATUM_DESC(22),
    HISTORIE(23), STATUS_DERIVAT(24), STATUS_GELOESCHT(25), URL(26), MODUL(27), MODULSETEAM(28), SUCHTEXT(29),
    PZBK(30), FESTGESTELLT_IN(31), UMSETZUNGSBESTAETIGUNG_STATUS(32), ZAK_STATUS(33), ZAK_BOOLEAN(34), STATUS_OFFEN(35),
    DASHBOARD_MELDUNG(36), DASHBOARD_ANFORDERUNG(37), URL_MELDUNG(38), URL_ANFORDERUNG(39), KOVA_STATUS(40),
    KOVA_PHASE(41), VONDATE(42), BIS_DATE(43), RELEVANZ(44), ZUORDNUNG_ZAK_STATUS(45), VON_BIS_DATE(46), TYPE(47),
    SORTING(48), ROLLE(49), ABTEILUNG(50), ZAK_EINORDNUNG(51), SENSORCOC_SCHREIBEND(52), SENSORCOC_LESEND(53),
    NURBERECHTIGTE(54), WERK(55), TECHNOLOGIE(56), PROZESSBAUKASTEN_STATUS(57), SNAPSHOT(58), PROZESSBAUKASTEN(59),
    IN_PROZESSBAUKASTEN(60), FACHBEREICH(61), THEMENKLAMMER(62);

    // ------------  fields ----------------------------------------------------
    private int filterTypeId;
    private String bezeichnung;
    private String className;
    private String propertyName;

    // ------------  constructors ----------------------------------------------
    SearchFilterType(int filterTypeId) {
        this.filterTypeId = filterTypeId;

        switch (filterTypeId) {
            case 0:
                this.setAttributes("Status", "searchFilterType_propertyName_status", "Status");
                break;
            case 1:
                this.setAttributes("Sensor Coc", "searchFilterType_propertyName_sensorcoc", "SensorCoc");
                break;
            case 2:
                this.setAttributes("T-Team", "searchFilterType_propertyName_tteam", "Tteam");
                break;
            case 3:
                this.setAttributes("Derivat", "searchFilterType_propertyName_derivat", "Derivat");
                break;
            case 4:
                this.setAttributes("Dashboard", "searchFilterType_propertyName_dashboard", "Long");
                break;
            case 5:
                this.setAttributes("Meldung", "searchFilterType_propertyName_meldung", "Meldung");
                break;
            case 6:
                this.setAttributes("Anforderung", "searchFilterType_propertyName_anforderung", "Anforderung");
                break;
            case 7:
                this.setAttributes("Id", "searchFilterType_propertyName_id", "Long");
                break;
            case 8:
                this.setAttributes("Erstellungdatum: von - bis", "searchFilterType_propertyName_erstellungDatum", "Erstellungdatum");
                break;
            case 9:
                this.setAttributes("Änderungsdatum: von - bis", "searchFilterType_propertyName_aenderungsDatum", "Änderungsdatum");
                break;
            case 10:
                this.setAttributes("Status Anforderung", "searchFilterType_propertyName_satusAnforderung", "Status");
                break;
            case 11:
                this.setAttributes("Status Meldung", "searchFilterType_propertyName_statusMeldung", "Status");
                break;
            case 12:
                this.setAttributes("Änderungsdatum: von", "searchFilterType_propertyName_aenderungsdatumVon", "Änderungsdatum");
                break;
            case 13:
                this.setAttributes("Änderungsdatum: bis", "searchFilterType_propertyName_aenderungsdatumBis", "Änderungsdatum");
                break;
            case 14:
                this.setAttributes("Erstellungdatum: von", "searchFilterType_propertyName_erstellungdatumVon", "Erstellungdatum");
                break;
            case 15:
                this.setAttributes("Erstellungdatum: bis", "searchFilterType_propertyName_erstellungdatumBis", "Erstellungdatum");
                break;
            case 16:
                this.setAttributes("Status Änderungsdatum: von - bis", "searchFilterType_propertyName_statusaenderungdatum", "Status Änderungsdatum");
                break;
            case 17:
                this.setAttributes("Status Änderungsdatum: von", "searchFilterType_propertyName_statusaenderungsdatumVon", "Status Änderungsdatum");
                break;
            case 18:
                this.setAttributes("Status Änderungsdatum: bis", "searchFilterType_propertyName_statusaenderungsdatumBis", "Status Änderungsdatum");
                break;
            case 19:
                this.setAttributes("Sortierung: FachId (aufsteigend)", "searchFilterType_propertyName_sortierungFachIdAufsteigend", "Sortierung");
                break;
            case 20:
                this.setAttributes("Sortierung: FachId (absteigend)", "searchFilterType_propertyName_sortierungFachIdAbsteigend", "Sortierung");
                break;
            case 21:
                this.setAttributes("Sortierung: Änderungsdatum (aufsteigend)", "searchFilterType_propertyName_sortierungAenderungsdatumAufsteigend", "Sortierung");
                break;
            case 22:
                this.setAttributes("Sortierung: Änderungsdatum (absteigend)", "searchFilterType_propertyName_sortierungAenderungsdatumAbsteigend", "Sortierung");
                break;
            case 23:
                this.setAttributes("Historie", "searchFilterType_propertyName_historie", "Historie");
                break;
            case 24:
                this.setAttributes("Status Derivat", "searchFilterType_propertyName_statusDerivat", "Derivat");
                break;
            case 25:
                this.setAttributes("Gelöschte Elemente anzeigen", "searchFilterType_propertyName_geloeschteElementeAnzeigen", "Status");
                break;
            case 26:
                this.setAttributes("URL", "searchFilterType_propertyName_url", "Long");
                break;
            case 27:
                this.setAttributes("Modul", "searchFilterType_propertyName_modul", "Modul");
                break;
            case 28:
                this.setAttributes("ModulSeTeam", "searchFilterType_propertyName_modulSeTeam", "ModulSeTeam");
                break;
            case 29:
                this.setAttributes("Suchtext", "searchFilterType_propertyName_suchtext", "String");
                break;
            case 30:
                this.setAttributes("Pzbk", "searchFilterType_propertyName_pzbk", "Pzbk");
                break;
            case 31:
                this.setAttributes("Festgestellt In", "searchFilterType_propertyName_festgestelltIn", "Long");
                break;
            case 36:
                this.setAttributes("Dashboard Meldung", "searchFilterType_propertyName_dashboardMeldung", "Long");
                break;
            case 37:
                this.setAttributes("Dashboard Anforderung", "searchFilterType_propertyName_dashboardAnforderung", "Long");
                break;
            case 38:
                this.setAttributes("URL Meldung", "searchFilterType_propertyName_urlMeldung", "Long");
                break;
            case 39:
                this.setAttributes("URL Anforderung", "searchFilterType_propertyName_urlAnforderung", "Long");
                break;
            case 40:
                this.setAttributes("KovAStatus", "searchFilterType_propertyName_kovastatus");
                break;
            case 41:
                this.setAttributes("KovAPhase", "searchFilterType_propertyName_kovaphase");
                break;
            case 42:
                this.setAttributes("von", "searchFilterType_propertyName_von");
                break;
            case 43:
                this.setAttributes("bis", "searchFilterType_propertyName_bis");
                break;
            case 44:
                this.setAttributes("Phasenrelevanz", "searchFilterType_propertyName_phasenrelevanz");
                break;
            case 45:
                this.setAttributes("ZAK Status", "searchFilterType_propertyName_zakStatus");
                break;
            case 49:
                this.setAttributes("Rolle", "searchFilterType_propertyName_rolle");
                break;
            case 50:
                this.setAttributes("Abteilung", "searchFilterType_propertyName_abteilung");
                break;
            case 51:
                this.setAttributes("Zak Einordnung", "searchFilterType_propertyName_zakEinordung");
                break;
            case 52:
                this.setAttributes("SensorCoc schreibend", "searchFilterType_propertyName_sensorcocschreibend");
                break;
            case 53:
                this.setAttributes("SensorCoc lesend", "searchFilterType_propertyName_sensorcoclesend");
                break;
            case 54:
                this.setAttributes("Nur Berechtigte Benutzer anzeigen", "searchFilterType_propertyName_berechtigteBenutzer");
                break;
            case 55:
                this.setAttributes("Werk", "searchFilterType_propertyName_werk");
                break;
            case 56:
                this.setAttributes("Technologie", "searchFilterType_propertyName_technologie");
                break;
            case 57:
                this.setAttributes("Prozessbaukasten Status", "searchFilterType_propertyName_technologie");
                break;
            case 58:
                this.setAttributes("Snapshot", "searchFilterType_propertyName_snapshot");
                break;
            case 59:
                this.setAttributes("Prozessbaukasten", "searchFilterType_propertyName_prozessbaukasten");
                break;
            case 60:
                this.setAttributes("In Prozessbaukasten", "searchFilterType_propertyName_in_prozessbaukasten");
                break;
            case 61:
                this.setAttributes("Fachbereich", "searchFilterType_propertyName_fachbereich");
                break;
            case 62:
                this.setAttributes("Themenklammer", "searchFilterType_propertyName_themenklammer");
                break;
            case 32:
            case 33:
            case 34:
            case 35:
            case 46:
            case 47:
            case 48:
                this.setPropertyName("");
                break;
            default:
                break;
        }
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return getBezeichnung();
    }

    // ------------  getter / setter -------------------------------------------
    public int getFilterTypeId() {
        return filterTypeId;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public String getClassName() {
        return className;
    }

    public String getPropertyName() {
        return propertyName;
    }

    private void setAttributes(String bezeichnung, String propertyName) {
        this.bezeichnung = bezeichnung;
        this.propertyName = propertyName;
    }

    private void setAttributes(String bezeichnung, String propertyName, String className) {
        this.bezeichnung = bezeichnung;
        this.propertyName = propertyName;
        this.className = className;
    }

    private void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
}
