package de.interfaceag.bmw.pzbk.enums;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author fn
 */
public enum ExcelSheetForTechnikalIssus {
    BERECHTIGUNGEN(0, "Berechtigungen"),
    ROLLEN(1, "Rollen"),
    TESTNUTZER(2, "Testnutzer"),
    DERIVATE(3, "Derivate"),
    DATENMIGRATION(4, "Datenmigration"),
    SENSORCOCTTEAM(5, "SensorCocTteam"),
    ANFORDERUNGSHISTORIE(6, "Anforderungshistorie"),
    ZAKUEBERTRAGUNG(7, "ZAK"),
    FAHRZEUGMERKMAL(8, "Fahrzeugmerkmale"),
    KOVAPHASEN(9, "KovAPhasen"),
    LINKANFORDERUNGMELDUNG(10, "AnforderungMeldungLink"),
    DERIVATANFORDERUNGMODUL(11, "DerivatAnforderung"),
    ANHAENGE(12, "Anhaenge"),
    MIGRATIONSDATUM(13, "MigrationDatum"),
    WERKMIGRATION(14, "WerkMigration");

    private final int id;
    private final String bezeichnung;

    ExcelSheetForTechnikalIssus(int id, String bezeichnung) {
        this.id = id;
        this.bezeichnung = bezeichnung;
    }

    @Override
    public String toString() {
        return getBezeichnung();
    }

    public int getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public static Optional<ExcelSheetForTechnikalIssus> getByBezeichnung(String bezeichnung) {
        if (bezeichnung != null) {
            return Stream.of(ExcelSheetForTechnikalIssus.values()).filter(e -> e.getBezeichnung().equals(bezeichnung)).findAny();
        }
        return Optional.empty();
    }

}
