package de.interfaceag.bmw.pzbk.enums;

import de.interfaceag.bmw.pzbk.entities.AbstractAnforderung;
import de.interfaceag.bmw.pzbk.mail.IPotentialMailParticipants;
import de.interfaceag.bmw.pzbk.mail.MailParticipants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author evda
 */
public enum Status implements Serializable {
    M_ENTWURF(0), M_GEMELDET(1), M_GELOESCHT(2), M_ZUGEORDNET(3),

    A_INARBEIT(4), A_UNSTIMMIG(5), A_FTABGESTIMMT(6),
    A_GELOESCHT(7), A_PLAUSIB(8), A_FREIGEGEBEN(9), A_KEINE_WEITERVERFOLG(10),

    M_UNSTIMMIG(11), M_ANGELEGT(12),

    A_ANGELEGT_IN_PROZESSBAUKASTEN(13),
    A_GENEHMIGT_IN_PROZESSBAUKASTEN(14),
    A_GENEHMIGT_IN_GUELTIGEM_PROZESSBAUKASTEN(15);

    private static final String ANFORDERUNG = "Anforderung";
    private static final String ANFORDERUNG_IN_PROZESSBAUKASTEN = "Anforderung im Prozessbaukasten";
    private static final String MELDUNG = "Meldung";
    private static final String ENTWURF = "Entwurf";
    private static final String SPEICHERN = "speichern";
    private static final String GELOESCHT = "gelöscht";
    private static final String LOESCHEN = "löschen";
    private static final String ANGELEGT = "angelegt";
    // ------------  fields ----------------------------------------------------
    private int statusId;
    private String statusBezeichnung;
    private String lebenszyklus;
    private String statusMarker;
    private String aktion;
    private String propertyName;

    // ------------  constructors ----------------------------------------------
    Status(int statusId) {
        this.statusId = statusId;

        switch (statusId) {
            case 0: {
                this.statusBezeichnung = ENTWURF;
                this.lebenszyklus = MELDUNG;
                this.statusMarker = "M";
                this.aktion = SPEICHERN;
                this.propertyName = "status_action_enum_MeldungEntwurf";
                break;
            }
            case 1: {
                this.statusBezeichnung = "gemeldet";
                this.lebenszyklus = MELDUNG;
                this.statusMarker = "M";
                this.aktion = "melden";
                this.propertyName = "status_action_enum_MeldungGemeldet";
                break;
            }
            case 2: {
                this.statusBezeichnung = GELOESCHT;
                this.lebenszyklus = MELDUNG;
                this.statusMarker = "M";
                this.aktion = LOESCHEN;
                this.propertyName = "status_action_enum_Meldunggeloescht";
                break;
            }
            case 3: {
                this.statusBezeichnung = "zugeordnet";
                this.lebenszyklus = MELDUNG;
                this.statusMarker = "M";
                this.aktion = "ableiten";
                this.propertyName = "status_action_enum_MeldungZugeordnet";
                break;
            }
            case 4: {
                this.statusBezeichnung = "in Arbeit";
                this.lebenszyklus = ANFORDERUNG;
                this.statusMarker = "A";
                this.aktion = "in Arbeit schicken";
                this.propertyName = "status_action_enum_AnforderungInArbeit";
                break;
            }
            case 5: {
                this.statusBezeichnung = "unstimmig";
                this.lebenszyklus = ANFORDERUNG;
                this.statusMarker = "A";
                this.aktion = "als unstimmig markieren";
                this.propertyName = "status_action_enum_AnforderungUnstimmig";
                break;
            }
            case 6: {
                this.statusBezeichnung = "abgestimmt";
                this.lebenszyklus = ANFORDERUNG;
                this.statusMarker = "A";
                this.aktion = "abstimmen";
                this.propertyName = "status_action_enum_AnforderungAbgestimmt";
                break;
            }
            case 7: {
                this.statusBezeichnung = GELOESCHT;
                this.lebenszyklus = ANFORDERUNG;
                this.statusMarker = "A";
                this.aktion = LOESCHEN;
                this.propertyName = "status_action_enum_AnforderungGeloescht";
                break;
            }
            case 8: {
                this.statusBezeichnung = "plausibilisiert";
                this.lebenszyklus = ANFORDERUNG;
                this.statusMarker = "A";
                this.aktion = "plausibilisieren";
                this.propertyName = "status_action_enum_AnforderungPlausibilisiert";
                break;
            }
            case 9: {
                this.statusBezeichnung = "freigegeben";
                this.lebenszyklus = ANFORDERUNG;
                this.statusMarker = "A";
                this.aktion = "freigeben";
                this.propertyName = "status_action_enum_AnforderungFreigegeben";
                break;
            }
            case 10: {
                this.statusBezeichnung = "Keine Weiterverfolgung";
                this.lebenszyklus = ANFORDERUNG;
                this.statusMarker = "A";
                this.aktion = "nicht weiter verfolgen";
                this.propertyName = "status_action_enum_AnforderungKeineWeiterverfolgung";
                break;
            }
            case 11: {
                this.statusBezeichnung = "unstimmig";
                this.lebenszyklus = MELDUNG;
                this.statusMarker = "M";
                this.aktion = "zurückweisen";
                this.propertyName = "status_action_enum_MeldungUnstimmig";
                break;
            }
            case 12: {
                this.statusBezeichnung = ANGELEGT;
                this.lebenszyklus = MELDUNG;
                this.statusMarker = "M";
                this.aktion = "anlegen";
                this.propertyName = "status_action_enum_MeldungAngelegt";
                break;
            }
            case 13: {
                this.statusBezeichnung = ANGELEGT;
                this.lebenszyklus = ANFORDERUNG_IN_PROZESSBAUKASTEN;
                this.statusMarker = "PA";
                this.aktion = "anlegen";
                this.propertyName = "status_action_enum_AnforderungAngelegt";
                break;
            }
            case 14: {
                this.statusBezeichnung = "PZBK genehmigt";
                this.lebenszyklus = ANFORDERUNG_IN_PROZESSBAUKASTEN;
                this.statusMarker = "PA";
                this.aktion = "genehmigen";
                this.propertyName = "status_action_enum_AnforderungGenehmigt";
                break;
            }
            case 15: {
                this.statusBezeichnung = "PZBK gültig";
                this.lebenszyklus = ANFORDERUNG_IN_PROZESSBAUKASTEN;
                this.statusMarker = "PA";
                this.aktion = "genehmigen";
                this.propertyName = "status_action_enum_AnforderungGenehmigtGueltig";
                break;
            }
            default: {
                this.statusBezeichnung = ENTWURF;
                this.lebenszyklus = MELDUNG;
                this.statusMarker = "M";
                this.aktion = SPEICHERN;
                this.propertyName = "status_action_enum_MeldungEntwurf";
                break;
            }
        }
    }

    public static Status getStatusById(int statusId) {
        for (Status st : Status.values()) {
            if (st.statusId == statusId) {
                return st;
            }
        }
        throw new IllegalArgumentException("Invalid statusId: " + statusId);
    }

    public static List<Status> selectStatusesOfMeldung() {
        List<Status> lst = new ArrayList<>();
        for (Status st : Status.values()) {
            if (st.statusMarker.equals("M") && !st.statusBezeichnung.equals(ANGELEGT)) {
                lst.add(st);
            }
        }
        return lst;
    }

    public static List<Status> getStatusValuesForGlobalAnforderungFilter() {
        return Stream.of(values())
                .filter(status -> "A".equals(status.getStatusMarker()) || "PA".equals(status.getStatusMarker()))
                .collect(Collectors.toList());
    }

    public static List<Status> selectStatusesOfAnforderung() {
        List<Status> lst = new ArrayList<>();
        for (Status st : Status.values()) {
            if (st.statusMarker.equals("A")) {
                lst.add(st);
            }
        }
        return lst;
    }

    public static Status getStatusByBezeichnung(String bezeichnung) {
        for (Status st : Status.values()) {
            if (st.statusBezeichnung.equals(bezeichnung)) {
                return st;
            }
        }
        return null;
    }

    public static Status getAnforderungStatusByBezeichnung(String bezeichnung) {
        for (Status st : Status.values()) {
            if ("A".equals(st.getStatusMarker()) && st.statusBezeichnung.equals(bezeichnung)) {
                return st;
            }
        }
        return null;
    }

    public static Status getMeldungStatusByBezeichnung(String bezeichnung) {
        for (Status st : Status.values()) {
            if ("M".equals(st.getStatusMarker()) && st.statusBezeichnung.equals(bezeichnung)) {
                return st;
            }
        }
        return null;
    }

    public static Status getStatusByBezeichnungAndMarker(String bezeichnung, String marker) {
        for (Status s : Status.values()) {
            if (s.getStatusBezeichnung().equals(bezeichnung) && s.getStatusMarker().equals(marker)) {
                return s;
            }
        }
        return null;
    }

    public static void setMailParticipantsForEditedByStatus(Status currentStatus, MailParticipants mp, IPotentialMailParticipants pmp) {
        switch (currentStatus) {
            case A_FTABGESTIMMT:
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSORCOCLEITER));
                break;
            case A_PLAUSIB:
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSORCOCLEITER));
                mp.addRecipientMail(pmp.getAllEmailsByRolle(Rolle.T_TEAMLEITER));
                break;
            default:
                break;
        }
    }

    public static void setMailParticipantsByStatusChange(Status oldStatus, Status newStatus, AbstractAnforderung abstractAnforderung, MailParticipants mp, IPotentialMailParticipants pmp) {
        if (oldStatus == null || newStatus == null) {
            return;
        }

        if (abstractAnforderung.getStatus().getStatusMarker().equals("M")) {
            switch (oldStatus.getStatusId()) {
                case 0:
                    setMailParticipantsForMEntwurf(newStatus, mp, pmp);
                    break;
                case 1:
                    setMailParticipantsForMGemeldet(newStatus, mp, pmp);
                    break;
                case 11:
                    setMailParticipantsForMUnstimmig(newStatus, mp, pmp);
                    break;
                default:
                    break;
            }
        }
        if (abstractAnforderung.getStatus().getStatusMarker().equals("A")) {
            switch (oldStatus) {

                case A_INARBEIT:
                    setMailParticipantsForAInArbeit(newStatus, mp, pmp);
                    break;
                case A_UNSTIMMIG:
                    setMailParticipantsForAUnstimmig(newStatus, mp, pmp);
                    break;
                case A_FTABGESTIMMT:
                    setMailParticipantsForAAbgestimmt(newStatus, mp, pmp);
                    break;
                case A_PLAUSIB:
                    setMailParticipantsForAPlausibilisiert(newStatus, mp, pmp);
                    break;
                case A_FREIGEGEBEN:
                    setMailParticipantsForAFreigegeben(newStatus, mp, pmp);
                    break;
                case A_KEINE_WEITERVERFOLG:
                    setMailParticipantsForAKeineWeiterverfolgung(newStatus, mp, pmp);
                    break;
                default:
                    break;
            }
        }

    }

    public static void setMailParticipantsForMEntwurf(Status newStatus, MailParticipants mp, IPotentialMailParticipants pmp) {
        switch (newStatus) {
            case M_GEMELDET:
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSORCOCLEITER));
                mp.addRecipientMail(pmp.getDetektoren());
                break;
            case M_GELOESCHT:
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSOR));
                mp.addRecipientMail(pmp.getDetektoren());
                break;
            default:
                break;
        }
    }

    public static void setMailParticipantsForMGemeldet(Status newStatus, MailParticipants mp, IPotentialMailParticipants pmp) {
        switch (newStatus) {
            case M_ZUGEORDNET:
            case M_UNSTIMMIG:
            case M_GELOESCHT:
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSOR));
                mp.addCC(pmp.getDetektoren());
                break;
            default:
                break;
        }
    }

    public static void setMailParticipantsForMUnstimmig(Status newStatus, MailParticipants mp, IPotentialMailParticipants pmp) {
        switch (newStatus) {
            case M_GEMELDET:
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSORCOCLEITER));
                break;
            case M_GELOESCHT:
                mp.addCC(pmp.getEmailByRolle(Rolle.SENSOR));
                mp.addCC(pmp.getDetektoren());
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSORCOCLEITER));
                break;
            default:
                break;
        }
    }

    public static void setMailParticipantsForAInArbeit(Status newStatus, MailParticipants mp, IPotentialMailParticipants pmp) {
        switch (newStatus) {
            case A_FTABGESTIMMT:
                mp.addRecipient(pmp.getEmailByRolle(Rolle.T_TEAMLEITER));
                break;
            case A_GELOESCHT:
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSOR));
                mp.addCC(pmp.getDetektoren());
                break;
            default:
                break;
        }
    }

    public static void setMailParticipantsForAUnstimmig(Status newStatus, MailParticipants mp, IPotentialMailParticipants pmp) {
        switch (newStatus) {
            case A_GELOESCHT:
                mp.addCC(pmp.getEmailByRolle(Rolle.SENSOR));
                mp.addCC(pmp.getDetektoren());
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSORCOCLEITER));
                mp.addCC(pmp.getEmailByRolle(Rolle.T_TEAMLEITER));
                break;
            case A_FTABGESTIMMT:
                mp.addRecipient(pmp.getEmailByRolle(Rolle.T_TEAMLEITER));
                break;
            default:
                break;
        }
    }

    public static void setMailParticipantsForAAbgestimmt(Status newStatus, MailParticipants mp, IPotentialMailParticipants pmp) {
        switch (newStatus) {
            case A_UNSTIMMIG:
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSORCOCLEITER));
                break;
            case A_PLAUSIB:
                mp.addCC(pmp.getEmailByRolle(Rolle.SENSORCOCLEITER));
                mp.addRecipientMail(pmp.getAllEmailsByRolle(Rolle.E_COC)); //??
                break;
            case A_GELOESCHT:
                mp.addCC(pmp.getEmailByRolle(Rolle.SENSOR));
                mp.addCC(pmp.getDetektoren());
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSORCOCLEITER));
                mp.addCC(pmp.getEmailByRolle(Rolle.T_TEAMLEITER)); //??
                break;
            default:
                break;
        }
    }

    public static void setMailParticipantsForAPlausibilisiert(Status newStatus, MailParticipants mp, IPotentialMailParticipants pmp) {
        switch (newStatus) {
            case A_UNSTIMMIG:
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSORCOCLEITER));
                break;
            case A_FREIGEGEBEN:
                mp.addCC(pmp.getEmailByRolle(Rolle.SENSOR));
                mp.addCC(pmp.getDetektoren());
                mp.addCC(pmp.getEmailByRolle(Rolle.SENSORCOCLEITER));
                mp.addCC(pmp.getEmailByRolle(Rolle.T_TEAMLEITER));
                mp.addCC(pmp.getAllEmailsByRolle(Rolle.E_COC));
                mp.addRecipient(pmp.getEmailByRolle(Rolle.ANFORDERER));
                break;
            case A_GELOESCHT:
                mp.addCC(pmp.getEmailByRolle(Rolle.SENSOR));
                mp.addCC(pmp.getDetektoren());
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSORCOCLEITER));
                mp.addCC(pmp.getEmailByRolle(Rolle.T_TEAMLEITER));
                break;
            default:
                break;
        }
    }

    public static void setMailParticipantsForAFreigegeben(Status newStatus, MailParticipants mp, IPotentialMailParticipants pmp) {
        if (newStatus == Status.A_KEINE_WEITERVERFOLG) {
            mp.addRecipient(pmp.getEmailByRolle(Rolle.T_TEAMLEITER));
        }
    }

    public static void setMailParticipantsForAKeineWeiterverfolgung(Status newStatus, MailParticipants mp, IPotentialMailParticipants pmp) {
        switch (newStatus) {
            case A_PLAUSIB:
                mp.addRecipient(pmp.getEmailByRolle(Rolle.T_TEAMLEITER));
                break;
            case A_GELOESCHT:
                mp.addCC(pmp.getEmailByRolle(Rolle.SENSOR));
                mp.addCC(pmp.getDetektoren());
                mp.addRecipient(pmp.getEmailByRolle(Rolle.SENSORCOCLEITER));
                mp.addCC(pmp.getEmailByRolle(Rolle.T_TEAMLEITER));
                break;
            default:
                break;
        }
    }

    @Override
    public String toString() {
        return getStatusBezeichnung();
    }

    // statusId can potentially differ from ordinal (default statusId value)
    public int getStatusId() {
        return this.statusId;
    }

    public String getStatusBezeichnung() {
        return this.statusBezeichnung;
    }

    public String getStatusMarker() {
        return statusMarker;
    }

    public String getAktion() {
        return aktion;
    }

    public String getPropertyName() {
        return propertyName;
    }

}
