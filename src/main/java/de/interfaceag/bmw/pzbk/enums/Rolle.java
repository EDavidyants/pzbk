package de.interfaceag.bmw.pzbk.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * @author evda
 */
public enum Rolle {

    ADMIN(0),
    SENSOR(1),
    SENSOR_EINGESCHRAENKT(2),
    SENSORCOCLEITER(3),
    T_TEAMLEITER(4),
    E_COC(5),
    ANFORDERER(6),
    UMSETZUNGSBESTAETIGER(7),
    LESER(8),
    SCL_VERTRETER(9),
    TTEAMMITGLIED(10),
    TTEAM_VERTRETER(11);

    private final int rolleId;
    private String rolleBezeichnung;
    private String rolleRawString;

    Rolle(int rolleId) {
        this.rolleId = rolleId;

        switch (rolleId) {
            case 0:
                this.rolleBezeichnung = "Admin";
                this.rolleRawString = "ADMIN";
                break;
            case 1:
                this.rolleBezeichnung = "Sensor";
                this.rolleRawString = "SENSOR";
                break;
            case 2:
                this.rolleBezeichnung = "Sensor eingeschränkt";
                this.rolleRawString = "SENSOR_EINGESCHRAENKT";
                break;
            case 3:
                this.rolleBezeichnung = "SensorCoC-Leiter";
                this.rolleRawString = "SENSORCOCLEITER";
                break;
            case 4:
                this.rolleBezeichnung = "T-Teamleiter";
                this.rolleRawString = "T_TEAMLEITER";
                break;
            case 5:
                this.rolleBezeichnung = "E-CoC";
                this.rolleRawString = "E_COC";
                break;
            case 6:
                this.rolleBezeichnung = "Anforderer";
                this.rolleRawString = "ANFORDERER";
                break;
            case 7:
                this.rolleBezeichnung = "Umsetzungsbestätiger";
                this.rolleRawString = "UMSETZUNGSBESTAETIGER";
                break;
            case 8:
                this.rolleBezeichnung = "Leser";
                this.rolleRawString = "LESER";
                break;
            case 9:
                this.rolleBezeichnung = "SensorCoc Vertreter";
                this.rolleRawString = "SCL_VERTRETER";
                break;
            case 10:
                this.rolleBezeichnung = "T-Team Mitglied";
                this.rolleRawString = "TTEAMMITGLIED";
                break;
            case 11:
                this.rolleBezeichnung = "T-Team Vertreter";
                this.rolleRawString = "TTEAM_VERTRETER";
                break;
            default:
                this.rolleBezeichnung = "";
                this.rolleRawString = "";
                break;
        }

    }

    @Override
    public String toString() {
        return getBezeichnung();
    }

    public static Rolle getRolleByName(String bezeichnung) {
        for (Rolle r : Rolle.values()) {
            String name = r.name();
            if (name.equals(bezeichnung)) {
                return r;
            }
        }
        return null;
    }

    public static Rolle getRolleByRawString(String rawString) {
        for (Rolle r : Rolle.values()) {
            String name = r.getRolleRawString();
            if (name.equals(rawString)) {
                return r;
            }
        }
        return null;
    }

    public static Rolle getRolleById(int rolleId) {
        for (Rolle r : Rolle.values()) {
            if (r.rolleId == rolleId) {
                return r;
            }
        }
        throw new IllegalArgumentException("Invalid rolleId: " + rolleId);
    }

    public static List<Rolle> getAllRolles() {
        List<Rolle> rolles = new ArrayList<>();
        for (Rolle r : Rolle.values()) {
            rolles.add(r);
        }
        return rolles;
    }

    public int getId() {
        return rolleId;
    }

    public String getBezeichnung() {
        return rolleBezeichnung;
    }

    public String getRolleRawString() {
        return rolleRawString;
    }

}
