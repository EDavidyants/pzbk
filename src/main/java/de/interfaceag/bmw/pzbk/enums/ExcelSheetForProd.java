package de.interfaceag.bmw.pzbk.enums;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public enum ExcelSheetForProd {
    SENSORCOC(0, "SensorCoCs"),
    FAHRZEUGTYPEN(1, "Fahrzeugtypen"),
    AUSWIRKUNGEN(2, "Auswirkungen"),
    INFORMATIONSTEXTE(3, "Informationstexte"),
    PRODUKTLINIEN(4, "Produktlinien"),
    UMSETZERLISTE(5, "Umsetzerliste"),
    TTEAM(6, "TTeam"),
    WERK(7, "Werk"),
    DOORS(8, "DOORS Migration");

    private final int id;
    private final String bezeichnung;

    ExcelSheetForProd(int id, String bezeichnung) {
        this.id = id;
        this.bezeichnung = bezeichnung;
    }

    @Override
    public String toString() {
        return getBezeichnung();
    }

    public int getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public static Optional<ExcelSheetForProd> getByBezeichnung(String bezeichnung) {
        if (bezeichnung != null) {
            return Stream.of(ExcelSheetForProd.values()).filter(e -> e.getBezeichnung().equals(bezeichnung)).findAny();
        }
        return Optional.empty();
    }

}
