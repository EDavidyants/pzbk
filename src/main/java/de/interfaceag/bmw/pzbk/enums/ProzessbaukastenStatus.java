package de.interfaceag.bmw.pzbk.enums;

import java.util.stream.Stream;

/**
 *
 * @author evda
 */
public enum ProzessbaukastenStatus {

    ANGELEGT(0, "prozessbaukasten_angelegt", ""),
    BEAUFTRAGT(1, "prozessbaukasten_beauftragt", "prozessbaukasten_beauftragen"),
    ABGEBROCHEN(2, "prozessbaukasten_abgebrochen", "prozessbaukasten_abbrechen"),
    GELOESCHT(3, "prozessbaukasten_geloescht", "prozessbaukasten_loeschen"),
    GUELTIG(4, "prozessbaukasten_gueltig", "prozessbaukasten_genehmigen"),
    STILLGELEGT(5, "prozessbaukasten_stillgelegt", "prozessbaukasten_stilllegen");

    private final int id; // id to store in database
    private final String localizationKey; // for localization
    private final String actionName; // command button label for status change
    private final String nameForHistory;

    ProzessbaukastenStatus(int id, String localizationKey, String actionName) {
        this.id = id;
        this.localizationKey = localizationKey;
        this.actionName = actionName;
        this.nameForHistory = generateNameForHistory(id);
    }

    @Override
    public String toString() {
        return nameForHistory;
    }

    public int getId() {
        return id;
    }

    public String getLocalizationKey() {
        return localizationKey;
    }

    public String getActionName() {
        return actionName;
    }

    private String generateNameForHistory(int id) {
        switch (id) {
            case 0: {
                return "angelegt";
            }
            case 1: {
                return "beauftragt";
            }
            case 2: {
                return "abgebrochen";
            }
            case 3: {
                return "gelöscht";
            }
            case 4: {
                return "gültig";
            }
            case 5: {
                return "stillgelegt";
            }
            default:
                return "";
        }
    }

    public static ProzessbaukastenStatus getById(int id) {
        return Stream.of(ProzessbaukastenStatus.values()).filter(status -> status.getId() == id).findAny().orElse(null);
    }

    public String getNameForHistory() {
        return nameForHistory;
    }

}
