package de.interfaceag.bmw.pzbk.enums;

import java.util.stream.Stream;

/**
 * @author evda
 */
public enum Rechttype {
    KEIN(0), LESERECHT(1), SCHREIBRECHT(2);

    // ------------  fields ----------------------------------------------------
    private int id;
    private String bezeichnung;

    Rechttype(int rechttypeId) {
        this.id = rechttypeId;

        switch (rechttypeId) {
            case 0:
                this.bezeichnung = "KEIN";
                break;

            case 1:
                this.bezeichnung = "LESERECHT";
                break;

            case 2:
                this.bezeichnung = "SCHREIBRECHT";
                break;
            default:
                break;
        }
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        switch (this) {
            case KEIN:
                return "kein Recht";
            case LESERECHT:
                return "Leserecht";
            case SCHREIBRECHT:
                return "Schreibrecht";
            default:
                return "Illegible value";
        }
    }

    public static Rechttype getById(Integer id) {
        return Stream.of(Rechttype.values()).filter(rechttype -> rechttype.getId() == id).findAny().orElse(null);
    }

    public int getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

} // end of class
