package de.interfaceag.bmw.pzbk.enums;

public enum Phasenbezug {
    ARCHITEKTURRELEVANT("Architekturrelevant", false),
    KONZEPTRELEVANT("Konzeptrelevant", true);

    private final boolean value;
    private final String beschreibung;

    Phasenbezug(String beschreibung, boolean value) {
        this.value = value;
        this.beschreibung = beschreibung;
    }

    public boolean getValue() {
        return value;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

}
