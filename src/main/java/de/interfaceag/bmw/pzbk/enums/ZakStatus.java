package de.interfaceag.bmw.pzbk.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author evda
 */
public enum ZakStatus {
    EMPTY(0),
    PENDING(1), DONE(2), NICHT_BEWERTBAR(3), NICHT_UMSETZBAR(4),
    KEINE_ZAKUEBERTRAGUNG(5), UEBERTRAGUNG_FEHLERHAFT(6),
    ZURUECKGEZOGEN_DURCH_ANFORDERER(7), BESTAETIGT_AUS_VORPROZESS(8);

    // ------------  fields ----------------------------------------------------
    private int statusId;
    private String statusBezeichnung;

    // ------------  constructors ----------------------------------------------
    ZakStatus(int statusId) {
        this.statusId = statusId;

        switch (statusId) {
            case 0:
                this.statusBezeichnung = "zu übertragen";
                break;
            case 1:
                this.statusBezeichnung = "offen";
                break;
            case 2:
                this.statusBezeichnung = "bestätigt";
                break;
            case 3:
                this.statusBezeichnung = "nicht bewertbar";
                break;
            case 4:
                this.statusBezeichnung = "nicht umsetzbar";
                break;
            case 5:
                this.statusBezeichnung = "Standardvereinbarung";
                break;
            case 6:
                this.statusBezeichnung = "Übertragung fehlerhaft";
                break;
            case 7:
                this.statusBezeichnung = "Zurückgezogen durch Anforderer";
                break;
            case 8:
                this.statusBezeichnung = "bestätigt aus Vorprozess";
                break;
            default:
                break;

        }
    }

    // ------------  methods ---------------------------------------------------
    public static List<ZakStatus> getAllStatusListForFilter() {
        List<ZakStatus> result = new ArrayList<>();
        result.addAll(Arrays.asList(ZakStatus.values()));
        result.remove(ZakStatus.EMPTY);
        return result;
    }

    public static List<ZakStatus> getAllStatusList() {
        List<ZakStatus> result = new ArrayList<>();
        result.addAll(Arrays.asList(ZakStatus.values()));
        return result;
    }

    public static List<ZakStatus> getInverseList(List<ZakStatus> statusList) {
        List<ZakStatus> inverseStatusList = new ArrayList<>();
        for (ZakStatus s : getAllStatusList()) {
            if (!statusList.contains(s)) {
                inverseStatusList.add(s);
            }
        }

        return inverseStatusList;
    }

    public static ZakStatus getStatusById(int statusId) {
        for (ZakStatus st : ZakStatus.values()) {
            if (st.statusId == statusId) {
                return st;
            }
        }
        throw new IllegalArgumentException("Invalid statusId: " + statusId);
    }

    public static ZakStatus getStatusByBezeichnung(String bezeichnung) {
        for (ZakStatus zak : ZakStatus.values()) {
            if (zak.statusBezeichnung.equals(bezeichnung)) {
                return zak;
            }
        }
        throw new IllegalArgumentException("No match with " + bezeichnung);
    }

    @Override
    public String toString() {
        return getStatusBezeichnung();
    }

    // ------------  getter / setter -------------------------------------------
    public int getStatusId() {
        return this.statusId;
    }

    public String getStatusBezeichnung() {
        return this.statusBezeichnung;
    }

} // end of class
