package de.interfaceag.bmw.pzbk.enums;

import java.util.stream.Stream;

/**
 *
 * @author evda
 */
public enum ReferenzSystem {

    DOORS(1, "referenzSystem_Doors");

    private final int id; // id to store in database
    private final String localizationKey; // for localization

    ReferenzSystem(int id, String localizationKey) {
        this.id = id;
        this.localizationKey = localizationKey;
    }

    @Override
    public String toString() {
        return this.name();
    }

    public int getId() {
        return id;
    }

    public String getLocalizationKey() {
        return localizationKey;
    }

    public static ReferenzSystem getById(int id) {
        return Stream.of(ReferenzSystem.values()).filter(referenzSystem -> referenzSystem.getId() == id).findAny().orElse(null);
    }

}
