package de.interfaceag.bmw.pzbk.enums;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public enum ContentType {
    ANFORDERUNG(0), MELDUNG(1), PZBK(3);

    // ------------  fields ----------------------------------------------------
    private final int id;
    private final String bezeichnung;

    // ------------  constructors ----------------------------------------------
    ContentType(int id) {
        this.id = id;

        switch (id) {
            case 0:
                this.bezeichnung = "Anforderung";
                break;
            case 1:
                this.bezeichnung = "Meldung";
                break;
            case 2:
                this.bezeichnung = "Pzbk";
                break;
            default:
                this.bezeichnung = "";
                break;

        }
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return getBezeichnung();
    }

    // ------------  getter / setter -------------------------------------------
    public int getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

}
