package de.interfaceag.bmw.pzbk.enums;

/**
 *
 * @author evda
 */
public enum ReportingDurchlaufzeitType {
    UNDEFINED, FACHTEAM, TTEAM, VEREINBARUNG;
}
