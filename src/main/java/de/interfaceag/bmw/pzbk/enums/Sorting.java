package de.interfaceag.bmw.pzbk.enums;

import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public enum Sorting {
    AENDERUNGSDATUMASC(0), AENDERUNGSDATUMDESC(1), NAMEASC(2), NAMEDESC(3);

    // ------------  fields ----------------------------------------------------
    private final int id;
    private final String bezeichnung;
    private final String propertyName;

    // ------------  constructors ----------------------------------------------
    Sorting(int id) {
        this.id = id;

        switch (id) {
            case 0:
                this.bezeichnung = "Sortierung: Änderungsdatum (aufsteigend)";
                this.propertyName = "searchFilterType_propertyName_sortierungAenderungsdatumAufsteigend";
                break;
            case 1:
                this.bezeichnung = "Sortierung: Änderungsdatum (absteigend)";
                this.propertyName = "searchFilterType_propertyName_sortierungAenderungsdatumAbsteigend";
                break;
            case 2:
                this.bezeichnung = "Sortierung: FachId (aufsteigend)";
                this.propertyName = "searchFilterType_propertyName_sortierungFachIdAufsteigend";
                break;
            case 3:
                this.bezeichnung = "Sortierung: FachId (absteigend)";
                this.propertyName = "searchFilterType_propertyName_sortierungFachIdAbsteigend";
                break;
            default:
                this.bezeichnung = "";
                this.propertyName = "";
                break;

        }
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return getBezeichnung();
    }

    public static Optional<Sorting> getById(int id) {
        return Stream.of(Sorting.values()).filter(t -> t.getId() == id).findAny();
    }

    // ------------  getter / setter -------------------------------------------
    public int getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public String getPropertyName() {
        return propertyName;
    }

}
