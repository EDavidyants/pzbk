package de.interfaceag.bmw.pzbk.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author fp
 */
public enum Attribut {
    PRIOBI,
    AUSWIRKUNG,
    PRODUKTLINIE,
    MAILTEMPLATE_STATUS_CHANGED, MAILTEMPLATE_ATTR_CHANGED,
    MAILTEMPLATE_BEWERTUNGSAUFTRAGS_VORLAGE, MAILTEMPLATE_REMINDER_VORLAGE, MAILTEMPLATE_LASTCALL_VORLAGE,
    MAILTEMPLATE_MAIL_TO_FTS, MAILTEMPLATE_MAIL_TO_ANFORDERER_ZAK, MAILTEMPLATE_MAIL_TO_ANFORDERER_UB,
    REPORTING_START_DATE,
    BERECHTIGUNGSANTRAG_EINGANGSBESTAETIGUNG,
    BERECHTIGUNGSANTRAG_STATUS_UPDATE;

    public static List<Attribut> getAllAttribute() {
        List<Attribut> result = new ArrayList<>();
        result.addAll(Arrays.asList(Attribut.values()));
        return result;
    }
}
