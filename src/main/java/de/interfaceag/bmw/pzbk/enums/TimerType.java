package de.interfaceag.bmw.pzbk.enums;

/**
 *
 * @author evda
 */
public enum TimerType {

    FIRST_NOTIFICATION_FOR_FTS(0), BEWERTUNGSAUFTRAG(1), PHASE_AKTIV(2),
    REMINDER(3), LAST_CALL(4), PHASE_ABGESCHLOSSEN(5);

    // ------------  fields ----------------------------------------------------
    private int timerId;
    private String timerName;
    private String triggerSchedule;
    private String propName; // in properties file timer_settings.properties
    private String action;

    // ------------  constructors ----------------------------------------------
    TimerType(int timerId) {
        this.timerId = timerId;

        switch (timerId) {
            case 0: {
                this.timerName = "firstNotificationForFTS";
                this.triggerSchedule = "Vier Wochen vor dem KovAPhase-Startdatum";
                this.propName = "firstNotificationForFTS";
                this.action = "KovAPhase-Information nach FTS geschickt";
                break;
            }
            case 1: {
                this.timerName = "bewertungsauftrag";
                this.triggerSchedule = "Eine Woche vor dem KovAPhase-Startdatum";
                this.propName = "bewertungsauftrag";
                this.action = "Bewertungsauftrag geschick";
                break;
            }
            case 2: {
                this.timerName = "phaseAktiv";
                this.triggerSchedule = "Beim Erreichen des KovAPhase-Startdatums";
                this.propName = ""; // no property setting for this timerType
                this.action = "KovAPhasen aktiv gesetzt";
                break;
            }
            case 3: {
                this.timerName = "reminder";
                this.triggerSchedule = "In der Mitte der Periode zwischen KovAPhaseStart- und Enddatums (in der Regel eine Woche nach dem KovAPhase-Startdatum)";
                this.propName = "reminder";
                this.action = "Reminder geschickt";
                break;
            }
            case 4: {
                this.timerName = "lastCall";
                this.triggerSchedule = "Ein Tag vor dem KovAPhase-Enddatum";
                this.propName = "lastCall";
                this.action = "Last-Call geschickt";
                break;
            }
            case 5: {
                this.timerName = "phaseAbgeschlossen";
                this.triggerSchedule = "Eine Woche nach dem KovAPhase-Enddatum";
                this.propName = "phaseAbgeschlossen";
                this.action = "KovAPhasen deaktiviert";
                break;
            }
            default:
                break;
        }
    }

    // ------------  methods ---------------------------------------------------
    public static TimerType getTimerTypeById(int timerId) {
        for (TimerType tt : TimerType.values()) {
            if (tt.timerId == timerId) {
                return tt;
            }
        }
        throw new IllegalArgumentException("Invalid timerId: " + timerId);
    }

    public static TimerType getTimerTypeByName(String timerName) {
        for (TimerType tt : TimerType.values()) {
            if (tt.timerName.equals(timerName)) {
                return tt;
            }
        }
        throw new IllegalArgumentException("Invalid timerName: " + timerName);
    }

    @Override
    public String toString() {
        return getTimerName();
    }

    // ------------  getter / setter -------------------------------------------
    public int getTimerId() {
        return timerId;
    }

    public String getTimerName() {
        return timerName;
    }

    public String getTriggerSchedule() {
        return triggerSchedule;
    }

    public String getPropName() {
        return propName;
    }

    public String getAction() {
        return action;
    }

} // end of class
