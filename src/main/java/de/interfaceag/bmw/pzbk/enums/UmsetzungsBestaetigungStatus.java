package de.interfaceag.bmw.pzbk.enums;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author evda
 */
public enum UmsetzungsBestaetigungStatus {

    OFFEN(0), UMGESETZT(1), NICHT_UMGESETZT(2), BEWERTUNG_NICHT_MOEGLICH(3), NICHT_RELEVANT(4), KEINE_WEITERVERFOLGUNG(5),
    NICHT_UMGESETZT_KEINE_WEITERVERFOLGUNG(6), NICHT_UMGESETZT_WEITERVERFOLGUNG(7), NICHT_RELEVANT_KEINE_WEITERVERFOLGUNG(8);

    // ------------  fields ----------------------------------------------------
    private int id;
    private String statusBezeichnung;
    private String localizationName;

    // ------------  constructors ----------------------------------------------
    UmsetzungsBestaetigungStatus(int id) {
        this.id = id;
        switch (id) {
            case 0: {
                statusBezeichnung = "offen";
                localizationName = "umsetzungsbestaetigung_status_offen";
                break;
            }
            case 1: {
                statusBezeichnung = "umgesetzt";
                localizationName = "umsetzungsbestaetigung_status_umgesetzt";
                break;
            }
            case 2: {
                statusBezeichnung = "nicht umgesetzt";
                localizationName = "umsetzungsbestaetigung_status_nichtUmgesetzt";
                break;
            }
            case 3: {
                statusBezeichnung = "Bewertung nicht möglich";
                localizationName = "umsetzungsbestaetigung_status_bewertungNichtMoeglich";
                break;
            }
            case 4: {
                statusBezeichnung = "nicht relevant";
                localizationName = "umsetzungsbestaetigung_status_nichtRelevant";
                break;
            }
            case 5: {
                statusBezeichnung = "keine Weiterverfolgung";
                localizationName = "umsetzungsbestaetigung_status_keineWeiterverfolgung";
                break;
            }
            case 6: {
                statusBezeichnung = "nicht umgesetzt und keine Weiterverfolgung";
                localizationName = "umsetzungsbestaetigung_status_nichtUmgesetztUndKeineWeiterverfolgung";
                break;
            }
            case 7: {
                statusBezeichnung = "nicht umgesetzt und Vereinbarung der Umsetzung";
                localizationName = "umsetzungsbestaetigung_status_nichtUmgesetztUndVereinbarungDerUmsetzung";
                break;
            }
            case 8: {
                statusBezeichnung = "nicht relevant und keine Weiterverfolgung";
                localizationName = "umsetzungsbestaetigung_status_nichtRelevantUndKeineWeiterverfolgung";
                break;
            }
            default: {
                statusBezeichnung = "Illegible value";
                localizationName = "umsetzungsbestaetigung_status_nichtZulaessig";
                break;
            }
        }
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return getStatusBezeichnung();
    }

    public static List<UmsetzungsBestaetigungStatus> getSelectableStatusList() {
        List<UmsetzungsBestaetigungStatus> result = new ArrayList<>();
        for (UmsetzungsBestaetigungStatus umsetzungsBestaetigungStatus : UmsetzungsBestaetigungStatus.values()) {
            if (umsetzungsBestaetigungStatus.getId() < 5) {
                result.add(umsetzungsBestaetigungStatus);
            }
        }
        return result;
    }

    public static List<UmsetzungsBestaetigungStatus> getFilterStatusList() {
        List<UmsetzungsBestaetigungStatus> result = new ArrayList<>();
        for (UmsetzungsBestaetigungStatus umsetzungsBestaetigungStatus : UmsetzungsBestaetigungStatus.values()) {
            if (umsetzungsBestaetigungStatus.getId() < 6) {
                result.add(umsetzungsBestaetigungStatus);
            }
        }
        return result;
    }

    public static UmsetzungsBestaetigungStatus getStatusByName(String statusName) {
        for (UmsetzungsBestaetigungStatus st : UmsetzungsBestaetigungStatus.values()) {
            if (st.toString().equals(statusName)) {
                return st;
            }
        }
        throw new IllegalArgumentException("No match with " + statusName);
    }

    public static UmsetzungsBestaetigungStatus getStatusById(int id) {
        for (UmsetzungsBestaetigungStatus st : UmsetzungsBestaetigungStatus.values()) {
            if (st.id == id) {
                return st;
            }
        }
        throw new IllegalArgumentException("Invalid status Id: " + id);
    }

    public static List<UmsetzungsBestaetigungStatus> getInverseList(List<UmsetzungsBestaetigungStatus> statusList) {
        List<UmsetzungsBestaetigungStatus> inverseStatusList = new ArrayList<>();
        if (statusList != null) {
            for (UmsetzungsBestaetigungStatus s : getSelectableStatusList()) {
                if (!statusList.contains(s)) {
                    inverseStatusList.add(s);
                }
            }
        }
        return inverseStatusList;
    }

    // ------------  getter / setter -------------------------------------------
    public int getId() {
        return this.id;
    }

    public String getStatusBezeichnung() {
        return this.statusBezeichnung;
    }

    public String getLocalizationName() {
        return this.localizationName;
    }

} // end of class
