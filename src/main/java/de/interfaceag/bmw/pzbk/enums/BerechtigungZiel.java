package de.interfaceag.bmw.pzbk.enums;

/**
 *
 * @author evda
 */
public enum BerechtigungZiel {
    SYSTEM(0), SENSOR_COC(1), MODULSETEAM(2), ZAK_EINORDNUNG(3), DERIVAT(4), TTEAM(5);

    // ------------  fields ----------------------------------------------------
    private int berZielId;
    private String berZielBezeichnung;

    BerechtigungZiel(int berZielId) {
        this.berZielId = berZielId;

        switch (berZielId) {
            case 0: {
                this.berZielBezeichnung = "SYSTEM";
                break;
            }
            case 1: {
                this.berZielBezeichnung = "SENSOR_COC";
                break;
            }
            case 2: {
                this.berZielBezeichnung = "MODULSETEAM";
                break;
            }
            case 3: {
                this.berZielBezeichnung = "ZAK_EINORDNUNG";
                break;
            }
            case 4: {
                this.berZielBezeichnung = "DERIVAT";
                break;
            }
            case 5: {
                this.berZielBezeichnung = "TTEAM";
                break;
            }
            default: {
                break;
            }
        }
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        switch (this) {
            case SYSTEM:
                return "System";
            case SENSOR_COC:
                return "SensorCoC";
            case MODULSETEAM:
                return "ModulSeTeam";
            case ZAK_EINORDNUNG:
                return "ZakEinordnung";
            case DERIVAT:
                return "Derivat";
            case TTEAM:
                return "T-Team";
            default:
                return "";
        }
    }

    public int getBerZielId() {
        return berZielId;
    }

    public String getBerZielBezeichnung() {
        return berZielBezeichnung;
    }

} //end of class
