package de.interfaceag.bmw.pzbk.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author fp
 */
public enum DerivatAnforderungModulStatus {

    ABZUSTIMMEN(1),
    ANGENOMMEN(3),
    NICHT_BEWERTBAR(5),
    KEINE_WEITERVERFOLGUNG(6),
    IN_KLAERUNG(8),
    UNZUREICHENDE_ANFORDERUNGSQUALITAET(9);

    // ------------  fields ----------------------------------------------------
    private int statusId;
    private String statusBezeichnung;
    private List<Integer> nextStatus;

    // ------------  constructors ----------------------------------------------
    DerivatAnforderungModulStatus(int statusId) {
        this.statusId = statusId;

        switch (statusId) {
            case 0: {
                this.statusBezeichnung = "offen";
                this.nextStatus = new ArrayList<>();
                this.nextStatus.add(7);
                this.nextStatus.add(2);
                break;
            }
            case 1: {
                this.statusBezeichnung = "abzustimmen";
                this.nextStatus = new ArrayList<>();
                this.nextStatus.add(3);
                this.nextStatus.add(5);
                this.nextStatus.add(6);
                this.nextStatus.add(8);
                this.nextStatus.add(9);
                break;
            }
            case 2: {
                this.statusBezeichnung = "keine Zuordnung";
                this.nextStatus = new ArrayList<>();
                this.nextStatus.add(7);
                break;
            }
            case 3: {
                this.statusBezeichnung = "angenommen";
                this.nextStatus = new ArrayList<>();
                this.nextStatus.add(1);
                this.nextStatus.add(5);
                this.nextStatus.add(6);
                this.nextStatus.add(8);
                this.nextStatus.add(9);
                break;
            }
            case 4: {
                this.statusBezeichnung = "umgesetzt";
                this.nextStatus = new ArrayList<>();
                this.nextStatus.add(1);
                break;
            }
            case 5: {
                this.statusBezeichnung = "nicht bewertbar";
                this.nextStatus = new ArrayList<>();
                this.nextStatus.add(6);
                this.nextStatus.add(8);
                this.nextStatus.add(9);
                break;
            }
            case 6: {
                this.statusBezeichnung = "keine Weiterverfolgung";
                this.nextStatus = new ArrayList<>();
                this.nextStatus.add(1);
                this.nextStatus.add(9);
                break;
            }
            case 7: {
                this.statusBezeichnung = "zugeordnet";
                this.nextStatus = new ArrayList<>();
                this.nextStatus.add(0);
                break;
            }
            case 8: {
                this.statusBezeichnung = "in Klärung";
                this.nextStatus = new ArrayList<>();
                this.nextStatus.add(3);
                this.nextStatus.add(5);
                this.nextStatus.add(6);
                this.nextStatus.add(9);
                break;
            }
            case 9: {
                this.statusBezeichnung = "unzureichende Anforderungsqualität";
                this.nextStatus = new ArrayList<>();
                this.nextStatus.add(1);
                this.nextStatus.add(6);
                break;
            }
            default: {
                this.statusBezeichnung = "-";
                break;
            }
        }
    }

    public static List<DerivatAnforderungModulStatus> getNextStatusListByStatus(DerivatAnforderungModulStatus status) {
        List<DerivatAnforderungModulStatus> result = new ArrayList<>();
        for (Integer statusId : status.getNextStatus()) {
            result.add(getStatusById(statusId));
        }

        return result;
    }

    public static List<DerivatAnforderungModulStatus> getAllStatusListForVereinbarung() {
        return getAllStatusList();
    }

    public static List<DerivatAnforderungModulStatus> getAllStatusList() {
        return Arrays.asList(DerivatAnforderungModulStatus.values());
    }

    public static DerivatAnforderungModulStatus getStatusById(Integer statusId) {
        for (DerivatAnforderungModulStatus status : DerivatAnforderungModulStatus.values()) {
            Integer id = status.getStatusId();
            if (id.equals(statusId)) {
                return status;
            }
        }
        return null;
    }

    public static DerivatAnforderungModulStatus getStatusByBezeichnung(String bezeichnung) {
        DerivatAnforderungModulStatus[] status = DerivatAnforderungModulStatus.values();
        for (DerivatAnforderungModulStatus s : status) {
            if (s.getStatusBezeichnung().equals(bezeichnung)) {
                return s;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return getStatusBezeichnung();
    }

    public int getStatusId() {
        return this.statusId;
    }

    public String getStatusBezeichnung() {
        return this.statusBezeichnung;
    }

    public List<Integer> getNextStatus() {
        return nextStatus;
    }

}
