package de.interfaceag.bmw.pzbk.enums;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public enum SystemType {
    ZAKSST(0, "ZAK SST"),
    PERMISSION(1, "Berechtigung"),
    ALL(2, "ALL"),
    MIGRATION(3, "MIgration"),
    REPORTING(4, "Reporting");

    private final int id;
    private final String bezeichnung;

    SystemType(int id, String bezeichnung) {
        this.id = id;
        this.bezeichnung = bezeichnung;
    }

    @Override
    public String toString() {
        return getBezeichnung();
    }

    public int getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

}
