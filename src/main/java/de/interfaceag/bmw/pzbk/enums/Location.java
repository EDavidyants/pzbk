package de.interfaceag.bmw.pzbk.enums;

/**
 *
 * @author fn
 */
public enum Location {
    DEUTSCH(0), ENGLISCH(1);

    // ------------  fields ----------------------------------------------------
    private final int id;
    private final String location;

    // ------------  constructors ----------------------------------------------
    Location(int id) {
        this.id = id;

        switch (id) {
            case 0:
                this.location = "DE";
                break;
            case 1:
                this.location = "EN";
                break;
            default:
                this.location = "";
                break;

        }
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return getLocation();
    }

    // ------------  getter / setter -------------------------------------------
    public int getId() {
        return id;
    }

    public String getLocation() {
        return location;
    }

}
