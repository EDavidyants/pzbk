package de.interfaceag.bmw.pzbk.enums;

import java.util.stream.Stream;

/**
 *
 * @author evda
 */
public enum AntragStatus {

    OFFEN(0, "Offen"), ANGENOMMEN(1, "Angenommen"), ABGELEHNT(2, "Abgelehnt");

    private final int id;
    private final String bezeichnung;

    AntragStatus(int id, String bezeichnung) {
        this.id = id;
        this.bezeichnung = bezeichnung;
    }

    public static AntragStatus getById(int id) {
        return Stream.of(AntragStatus.values()).filter(status -> status.getId() == id).findAny().orElse(null);
    }

    @Override
    public String toString() {
        return getBezeichnung();
    }

    public int getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

}
