package de.interfaceag.bmw.pzbk.enums;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public enum Page {
    SUCHE(0), BERICHTSWESEN(1), VEREINBARUNG(2), UMSETZUNGSBESTAETIGUNG(3),
    ANFORDERUNGVIEW(4), ANFORDERUNGEDIT(5), MELDUNGVIEW(6), MELDUNGEDIT(7),
    DASHBOARD(8), ZUORDNUNG(9), BERECHTIGUNG(10), PROJEKTVERWALTUNG(11),
    REPORTING_DASHBOARD(12), REPORTING_PROCESS_OVERVIEW_SIMPLE(13),
    REPORTING_PROCESS_OVERVIEW_DETAIL(14), REPORTING_FACHTEAM(15), REPORTING_TTEAM(16),
    PROJEKTREPORTING_PROZESS(17), PROJEKTREPORTING_WERK(18), PROJEKTREPORTING_STATUSABGLEICH(19),
    PROJEKTREPORTING_DASHBOARD(20),
    PROZESSBAUKASTEN_VIEW(21), PROZESSBAUKASTEN_EDIT(22),
    REPORTING_PROJECT_PUBLIC_DASHBOARD(23), REPORTING_PROJECT_PUBLIC_PROCESS(24), REPORTING_PROJECT_PUBLIC_STATUSABGLEICH(25),
    SYSTEM_MONITORING(26), KONFIGURATION(27), FAHRZEUGMERKMAL_KONFIGURATION(28),
    FAHRZEUGMERMAL_DERIVAT_ZUORDNUNG(29);

    // ------------  fields ----------------------------------------------------
    private int id;
    private String bezeichnung;
    private String url;

    // ------------  constructors ----------------------------------------------
    Page(int id) {
        this.id = id;

        switch (id) {
            case 0:
                this.bezeichnung = "Suche";
                this.url = "anforderungList.xhtml";
                break;
            case 1:
                this.bezeichnung = "Berichtswesen";
                this.url = "zakStatus.xhtml";
                break;
            case 2:
                this.bezeichnung = "Vereinbarung";
                this.url = "addAnfoView.xhtml";
                break;
            case 3:
                this.bezeichnung = "Umsetzungsbestätigung";
                this.url = "umsetzungsbestaetigung.xhtml";
                break;
            case 4:
                this.bezeichnung = "AnforderungView";
                this.url = "anforderungView.xhtml";
                break;
            case 5:
                this.bezeichnung = "AnforderungEdit";
                this.url = "anforderungEdit.xhtml";
                break;
            case 6:
                this.bezeichnung = "MeldungView";
                this.url = "meldungView.xhtml";
                break;
            case 7:
                this.bezeichnung = "MeldungEdit";
                this.url = "meldungEdit.xhtml";
                break;
            case 8:
                this.bezeichnung = "Dashboard";
                this.url = "dashboard.xhtml";
                break;
            case 9:
                this.bezeichnung = "Zuordnung";
                this.url = "zuordnung.xhtml";
                break;
            case 10:
                this.bezeichnung = "Berechtigung";
                this.url = "adminView.xhtml";
                break;
            case 11:
                this.bezeichnung = "Projektverwaltung";
                this.url = "derivateView.xhtml";
                break;
            case 12:
                this.bezeichnung = "Reporting";
                this.url = "reporting.xhtml";
                break;
            case 13:
                this.bezeichnung = "Prozessübersicht Simple";
                this.url = "reportingProcessOverviewSimple.xhtml";
                break;
            case 14:
                this.bezeichnung = "Prozessübersicht Detail";
                this.url = "reportingProcessOverviewDetail.xhtml";
                break;
            case 15:
                this.bezeichnung = "Reporting Fachteam";
                this.url = "reportingFachteam.xhtml";
                break;
            case 16:
                this.bezeichnung = "Reporting Tteam";
                this.url = "reportingTteam.xhtml";
                break;
            case 17:
                this.bezeichnung = "Projektreporting Prozess";
                this.url = "projektReportingProzess.xhtml";
                break;
            case 18:
                this.bezeichnung = "Projektreporting Werk";
                this.url = "projektReportingWerk.xhtml";
                break;
            case 19:
                this.bezeichnung = "Projektreporting Statusabgleich";
                this.url = "projektReportingStatusabgleich.xhtml";
                break;
            case 20:
                this.bezeichnung = "Projektreporting Dashboard";
                this.url = "projektReporting.xhtml";
                break;
            case 21:
                this.bezeichnung = "Prozessbaukasten";
                this.url = "prozessbaukastenView.xhtml";
                break;
            case 22:
                this.bezeichnung = "Prozessbaukasten Edit";
                this.url = "prozessbaukastenEdit.xhtml";
                break;
            case 23:
                this.bezeichnung = "Reporting Prozess Public Dashboard";
                this.url = "dashboard.xhtml";
                break;
            case 24:
                this.bezeichnung = "Reporting Prozess Public";
                this.url = "process.xhtml";
                break;
            case 25:
                this.bezeichnung = "Reporting Prozess Public Statusabgleich";
                this.url = "statusabgleich.xhtml";
                break;
            case 26:
                this.bezeichnung = "System Monitoring";
                this.url = "systemMonitoring.xhtml";
                break;
            case 27:
                this.bezeichnung = "Konfiguration";
                this.url = "configView.xhtml";
                break;
            case 28:
                this.bezeichnung = "Fahrzeugmerkmal Konfiguration";
                this.url = "fahrzeugmerkmalVerwaltung.xhtml";
                break;
            case 29:
                this.bezeichnung = "Fahrzeugmerkmal Derivat Zuordnung";
                this.url = "fahrzeugmerkmalDerivatzuordnung.xhtml";
                break;
            default:
                this.bezeichnung = "";
                break;

        }
    }

    // ------------  methods ---------------------------------------------------
    @Override
    public String toString() {
        return getBezeichnung();
    }

    // ------------  getter / setter -------------------------------------------
    public int getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public String getUrl() {
        return url;
    }

}
