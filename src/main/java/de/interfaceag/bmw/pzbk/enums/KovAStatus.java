package de.interfaceag.bmw.pzbk.enums;

/**
 *
 * @author evda
 */
public enum KovAStatus {

    OFFEN(0), KONFIGURIERT(1), AKTIV(2), ABGESCHLOSSEN(3);

    // ------------  fields ----------------------------------------------------
    private int statusId;
    private String statusBezeichnung;

    // ------------  constructors ----------------------------------------------
    KovAStatus(int statusId) {
        this.statusId = statusId;

        switch (statusId) {
            case 0: {
                this.statusBezeichnung = "offen";
                break;
            }
            case 1: {
                this.statusBezeichnung = "konfiguriert";
                break;
            }
            case 2: {
                this.statusBezeichnung = "aktiv";
                break;
            }
            case 3: {
                this.statusBezeichnung = "abgeschlossen";
                break;
            }
            default: {
                break;
            }
        }
    }

    // ------------  methods ---------------------------------------------------
    public static KovAStatus getStatusById(int statusId) {
        for (KovAStatus kv : KovAStatus.values()) {
            if (kv.statusId == statusId) {
                return kv;
            }
        }
        throw new IllegalArgumentException("Invalid statusId: " + statusId);
    }

    public static KovAStatus getStatusByBezeichnung(String bezeichnung) {
        for (KovAStatus kv : KovAStatus.values()) {
            if (kv.statusBezeichnung.equals(bezeichnung)) {
                return kv;
            }
        }
        throw new IllegalArgumentException("No match with " + bezeichnung);
    }

    @Override
    public String toString() {
        return getStatusBezeichnung();
    }

    // ------------  getter / setter -------------------------------------------
    public int getStatusId() {
        return this.statusId;
    }

    public String getStatusBezeichnung() {
        return this.statusBezeichnung;
    }

} // end of class
