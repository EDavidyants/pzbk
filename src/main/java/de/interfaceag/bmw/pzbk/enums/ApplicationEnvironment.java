package de.interfaceag.bmw.pzbk.enums;

import java.util.Objects;

public enum ApplicationEnvironment {

    DEVELOPMENT,
    PRODUCTION;

    private static final String PROP_NAME = "applicationEnvironment";

    public boolean isProduction() {
        return this.equals(PRODUCTION);
    }

    public boolean isDevelopment() {
        return this.equals(DEVELOPMENT);
    }

    public static String getPropertyName() {
        return PROP_NAME;
    }

    /**
     * Default (no match) = DEVELOPMENT
     *
     * @param input PRODUCTION or DEVELOPMENT
     * @return current application environment (development or production)
     */
    public static ApplicationEnvironment getByName(String input) {
        if (Objects.nonNull(input)) {
            input = input.toUpperCase();
            if ("PRODUCTION".equals(input)) {
                return ApplicationEnvironment.PRODUCTION;
            }
        }
        return ApplicationEnvironment.DEVELOPMENT;
    }
}
