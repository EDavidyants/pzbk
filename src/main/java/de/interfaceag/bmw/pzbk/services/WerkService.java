package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.WerkDao;
import de.interfaceag.bmw.pzbk.entities.Werk;
import de.interfaceag.bmw.pzbk.enums.LogLevel;
import de.interfaceag.bmw.pzbk.enums.SystemType;
import de.interfaceag.bmw.pzbk.excel.ExcelUtils;
import de.interfaceag.bmw.pzbk.exceptions.ExcelInputFileException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author fn
 */
@Stateless
@Named
public class WerkService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(WerkService.class);

    @Inject
    LogService logService;

    @Inject
    WerkDao werkDao;

    public List<Werk> getAllWerk() {
        return werkDao.getAllWerk();
    }

    private void persistWerk(Werk werk) {
        werkDao.persistWerk(werk);
    }

    public List<Werk> getWerkByName(String name) {
        return werkDao.getWerkByName(name);
    }

    public void createAndPersistDummyWerk() {
        loadAndPersistWerkFromExcelSheet();
    }

    public void persistOnlyNotExistingWerk(Werk werkToSave) {
        List<Werk> allWerks = getAllWerk();
        boolean isWerkInDB = false;
        for (Werk werkInDB : allWerks) {
            if (werkInDB.getName().trim().equals(werkToSave.getName().trim())) {
                isWerkInDB = true;
            }
        }
        if (!isWerkInDB) {
            persistWerk(werkToSave);
            logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "The Werk with the Name " + werkToSave.getName() + "is saved in DB", WerkService.class.getName());

        } else {
            logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "The Werk with the Name " + werkToSave.getName() + "is allready in DB", WerkService.class.getName());
        }
    }

    private void loadAndPersistWerkFromExcelSheet() {
        try {
            XSSFWorkbook wb = ExcelUtils.createXSSFWorkbookForFile(ConfigService.getExcelConfigFile());
            List<List<String>> rows = ExcelUtils.loadRowsBySheet(0, "Werk", wb);

            for (List<String> row : rows) {
                Werk werk = new Werk();
                werk.setName(row.get(0));
                persistOnlyNotExistingWerk(werk);
            }
        } catch (ExcelInputFileException ex) {
            LOG.error(null, ex);
        }
    }

    public void loadNewWerkFromExcelSheet(Sheet sheet) {

        logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "Start of Werk load...", WerkService.class.getName());

        int i = 0;
        for (Integer rowNumber = sheet.getFirstRowNum() + 1; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
            Row row = sheet.getRow(rowNumber);
            i++;
            if (row.getPhysicalNumberOfCells() == 1) {
                String werkName = ExcelUtils.cellToString(row.getCell(0));
                Werk we = new Werk();
                we.setName(werkName);
                persistOnlyNotExistingWerk(we);
                logService.logWithLogEntry(LogLevel.INFO, SystemType.MIGRATION, "The Werk with the Name " + werkName + "is saved in DB", WerkService.class.getName());

            } else {
                logService.logWithLogEntry(LogLevel.WARNING, SystemType.MIGRATION, "The Row " + i + " has not the length 1 ", WerkService.class.getName());
            }
        }

    }

}
