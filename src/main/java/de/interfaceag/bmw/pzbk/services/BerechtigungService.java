package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.berechtigung.BerechtigungsExcelRealm;
import de.interfaceag.bmw.pzbk.berechtigung.BerechtigungsUtils;
import de.interfaceag.bmw.pzbk.berechtigung.UserPermissionsService;
import de.interfaceag.bmw.pzbk.berechtigung.dao.TteamMitgliedDAO;
import de.interfaceag.bmw.pzbk.berechtigung.dao.TteamVertreterDAO;
import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungEditDto;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamMitgliedBerechtigung;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamVertreterBerechtigung;
import de.interfaceag.bmw.pzbk.dao.BerechtigungDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Berechtigung;
import de.interfaceag.bmw.pzbk.entities.BerechtigungDerivat;
import de.interfaceag.bmw.pzbk.entities.BerechtigungHistory;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.UserToRole;
import de.interfaceag.bmw.pzbk.entities.rollen.TTeamMitglied;
import de.interfaceag.bmw.pzbk.entities.rollen.TteamVertreter;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.exceptions.UserNotFoundException;
import de.interfaceag.bmw.pzbk.session.Session;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author evda
 */
@Stateless
@Named
public class BerechtigungService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(BerechtigungService.class);
    private static final String T_TEAM = "TTeam";

    @Inject
    BerechtigungDao berechtigungDao;
    @Inject
    private Session session;
    @Inject
    private ConfigService configService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private ModulService modulService;
    @Inject
    SensorCocService sensorCocService;
    @Inject
    private TteamService tteamService;
    @Inject
    private UserService userService;
    @Inject
    private UserSearchService userSearchService;
    @Inject
    private UserPermissionsService userPermissionsService;
    @Inject
    private TteamMitgliedDAO tteamMitgliedDao;
    @Inject
    private TteamVertreterDAO tteamVertreterDao;
    @Inject
    private SensorCocAuthorizedService sensorCocAuthorizedService;
    @Inject
    private TteamAuthorizedService tteamAuthorizedService;

    private Boolean startup = false;

    public void persistBerechtigungHistory(BerechtigungHistory berechtigung) {
        berechtigungDao.persistBerechtigungHistory(berechtigung);
    }

    public void loadBerechtigungenFromExcelSheet(Sheet sheet) {
        Collection<BerechtigungsExcelRealm> berechtigungsRealm = BerechtigungsUtils.loadBerechtigungenFromExcelSheet(sheet, "Q-Nummer");
        berechtigeRealms(berechtigungsRealm);
    }

    private void berechtigeRealms(Collection<BerechtigungsExcelRealm> berechtigungsRealm) {
        for (BerechtigungsExcelRealm realm : berechtigungsRealm) {
            berechtigeRealm(realm);
        }
    }

    private void berechtigeRealm(BerechtigungsExcelRealm realm) {

        Mitarbeiter mitarbeiter = null;
        String nachname = null;
        String vorname = null;
        if (realm.getName() != null && realm.getName().contains(", ")) {
            nachname = realm.getName().substring(0, realm.getName().indexOf(", "));
            vorname = realm.getName().substring(realm.getName().indexOf(", ") + 2);
            mitarbeiter = getMitarbeiterFromUserServiceWithNameAndApteilung(vorname, nachname, realm, mitarbeiter);
        }

        if (mitarbeiter == null) {
            mitarbeiter = getMitarbeiterFromUserServiceWithEmail(realm, mitarbeiter);
        }

        if (mitarbeiter == null) {
            mitarbeiter = getMitarbeiterForBerechtigung(mitarbeiter, realm, vorname, nachname);
        }

        if (mitarbeiter != null) {
            berechtigeMitarbeiter(mitarbeiter, realm);
        } else {
            LOG.warn("Kein Mitarbeiter mit qNumber {} gefunden", realm.getQnumber());
        }
    }

    public List<Berechtigung> getBerechtigungForRolle(Rolle rolle) {
        return berechtigungDao.getBerechtigungByRolle(rolle);
    }

    private Mitarbeiter getMitarbeiterForBerechtigung(Mitarbeiter mitarbeiter, BerechtigungsExcelRealm realm, String vorname, String nachname) {
        if (configService.isProductionEnvironment()) {
            mitarbeiter = getMitarbeiterFromLdap(mitarbeiter, realm, vorname, nachname);
        } else if (vorname != null && nachname != null && (!configService.isProductionEnvironment())) {
            mitarbeiter = new Mitarbeiter();
            mitarbeiter.setVorname(vorname);
            mitarbeiter.setNachname(nachname);
            mitarbeiter.setAbteilung(realm.getAbteilung());
            mitarbeiter.setEmail(realm.getMail());
            mitarbeiter.setQNumber("Q" + vorname.substring(0, 1) + nachname.substring(0, 1) + "1234");
            mitarbeiter.setLocation("de");
        }
        if (mitarbeiter == null) {
            mitarbeiter = userSearchService.getMitarbeiterByQnumber(realm.getQnumber());
        }
        return mitarbeiter;
    }

    private Mitarbeiter getMitarbeiterFromLdap(Mitarbeiter mitarbeiter, BerechtigungsExcelRealm realm, String vorname, String nachname) {
        try {
            // first by mail
            mitarbeiter = userSearchService.getMitarbeiterFromLdapByMail(realm.getMail());

            if (mitarbeiter == null) {
                // then by attributes
                List<Mitarbeiter> result = userSearchService.getMitarbeiterFromLdap(vorname, nachname, realm.getAbteilung());
                if (result != null && result.size() == 1) {
                    mitarbeiter = result.get(0);
                }
            }
        } catch (UserNotFoundException ex) {
            LOG.error(null, ex);
        }
        return mitarbeiter;
    }

    private void berechtigeMitarbeiter(Mitarbeiter mitarbeiter, BerechtigungsExcelRealm realm) {
        if (mitarbeiter.getId() == null && mitarbeiter.getName() != null && mitarbeiter.getNachname() != null
                && mitarbeiter.getAbteilung() != null && mitarbeiter.getEmail() != null) {
            if (mitarbeiter.getLocation().isEmpty()) {
                mitarbeiter.setLocation("de");
            }
            userService.saveMitarbeiter(mitarbeiter);
        }

        berechtigeSensorCoc(realm, mitarbeiter);

        berechtigeZakEinordnung(realm, mitarbeiter);

        berechtigeDerivat(realm, mitarbeiter);

        berechtigeModul(realm, mitarbeiter);

        berechtigeTteam(realm, mitarbeiter);

        berechtigeTteamVertreter(realm, mitarbeiter);

        berechtigeTteamMitglied(realm, mitarbeiter);

    }

    private void berechtigeTteam(BerechtigungsExcelRealm realm, Mitarbeiter mitarbeiter) {
        // berechtige fuer Tteams
        if (realm.getTteam() != null && !realm.getTteam().isEmpty()) {
            List<Long> ttlTteams = tteamService.getTteamsByNameList(realm.getTteam()).stream().map(Tteam::getId).collect(Collectors.toList());
            List<Long> tteams = berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZiel(mitarbeiter, Rolle.T_TEAMLEITER, BerechtigungZiel.TTEAM)
                    .stream().map(b -> Long.parseLong(b.getFuerId())).collect(Collectors.toList());
            ttlTteams.removeAll(tteams);
            ttlTteams.forEach(id -> grantRoleTeamleiter(mitarbeiter, id, Rechttype.SCHREIBRECHT)
            );
        }
    }

    private void berechtigeModul(BerechtigungsExcelRealm realm, Mitarbeiter mitarbeiter) {
        // berechtige fuer Modulen
        if (realm.getPlueUmsetzer() != null && !realm.getPlueUmsetzer().isEmpty()) {
            List<ModulSeTeam> modulSeTeamList = modulService.getSeTeamsByNameList(realm.getPlueUmsetzer());
            List<Long> berechtigteModulSeTeamIdList = berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZiel(mitarbeiter, Rolle.E_COC, BerechtigungZiel.MODULSETEAM)
                    .stream().map(b -> Long.parseLong(b.getFuerId())).collect(Collectors.toList());

            List<ModulSeTeam> grantRoleList = modulSeTeamList.stream().filter(se -> !berechtigteModulSeTeamIdList.contains(se.getId())).collect(Collectors.toList());
            grantRoleList.forEach(modulSeTeam -> grantRoleEcoc(mitarbeiter, modulSeTeam, Rechttype.SCHREIBRECHT)
            );

        }
    }

    private void berechtigeDerivat(BerechtigungsExcelRealm realm, Mitarbeiter mitarbeiter) {
        // berechtige fuer Derivaten
        if (realm.getPlVorschlaege() != null && !realm.getPlVorschlaege().isEmpty()) {
            List<Long> anfordererDerivate = derivatService.getDerivateByNameList(realm.getPlVorschlaege()).stream().map(Derivat::getId).collect(Collectors.toList());
            List<Long> derivate = berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZiel(mitarbeiter, Rolle.ANFORDERER, BerechtigungZiel.DERIVAT)
                    .stream().map(b -> Long.parseLong(b.getFuerId())).collect(Collectors.toList());
            anfordererDerivate.removeAll(derivate);
            List<Derivat> derivatList = derivatService.getDerivatByIdList(anfordererDerivate);
            derivatList.forEach(derivat ->
                    grantRoleAnforderer(mitarbeiter, derivat, Rechttype.SCHREIBRECHT, BerechtigungZiel.DERIVAT)
            );
        }
    }

    private void berechtigeZakEinordnung(BerechtigungsExcelRealm realm, Mitarbeiter mitarbeiter) {
        // berechtige fuer ZAK-Einordnungen
        if (realm.getZakEinordnungen() != null && !realm.getZakEinordnungen().isEmpty()) {
            List<String> zakEinordnungen = berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZiel(mitarbeiter, Rolle.ANFORDERER, BerechtigungZiel.ZAK_EINORDNUNG)
                    .stream().map(Berechtigung::getFuerId).collect(Collectors.toList());
            List<String> anfordererZakEinordnungen = new ArrayList<>(realm.getZakEinordnungen());
            anfordererZakEinordnungen.removeAll(zakEinordnungen);
            anfordererZakEinordnungen.forEach(zak ->
                    authorizeAnfordererToSensorCoc(mitarbeiter, zak, Rechttype.SCHREIBRECHT)
            );
        }
    }

    private void berechtigeTteamVertreter(BerechtigungsExcelRealm realm, Mitarbeiter mitarbeiter) {
        if (realm.getTteamVertreter() != null && !realm.getTteamVertreter().isEmpty()) {
            List<Long> ttlTteams = getTteamsByNameList(realm.getTteamVertreter());
            List<Long> tteams = tteamVertreterDao.getTteamIdsforTteamVertreter(mitarbeiter);

            ttlTteams.removeAll(tteams);
            grantRoleTteamVertreter(mitarbeiter, tteamService.getTteamByIdList(ttlTteams));
        }
    }

    private void berechtigeTteamMitglied(BerechtigungsExcelRealm realm, Mitarbeiter mitarbeiter) {
        if (realm.getTteamMitgliedSchreibend() != null && !realm.getTteamMitgliedSchreibend().isEmpty()) {
            List<Long> ttlTteams = getTteamsByNameList(realm.getTteamMitgliedSchreibend());
            List<Long> tteams = tteamMitgliedDao.getTteamIdsforTteamMitgliedAndRechttype(mitarbeiter, Rechttype.SCHREIBRECHT.getId());

            ttlTteams.removeAll(tteams);
            grantRoleTteamMitglied(mitarbeiter, tteamService.getTteamByIdList(ttlTteams), Rechttype.SCHREIBRECHT);
        }
    }

    private void berechtigeSensorCoc(BerechtigungsExcelRealm realm, Mitarbeiter mitarbeiter) {
        // berechtige fuer SensorCocs
        List<SensorCoc> sensorCoCLeiterSensorCoc = realm.getSensorCocs().isEmpty() || realm.getSensorCocs() == null
                ? null : sensorCocService.getSensorCocsByTechnologieList(realm.getSensorCocs());
        List<SensorCoc> sensorSensorCoc = realm.getSensors().isEmpty() || realm.getSensors() == null
                ? null : sensorCocService.getSensorCocsByTechnologieList(realm.getSensors());
        List<SensorCoc> leserSensorCoc = realm.getLeserechte().isEmpty() || realm.getLeserechte() == null
                ? null : sensorCocService.getSensorCocsByTechnologieList(realm.getLeserechte());

        if (sensorCoCLeiterSensorCoc != null && !sensorCoCLeiterSensorCoc.isEmpty()) {
            // berechtige fuer rolle sensorCoCLeiter
            LOG.debug("Berechtige Mitarbeiter {} als SensorCoC-Leiter", mitarbeiter);
            List<Long> sensorCocIds = berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZiel(mitarbeiter, Rolle.SENSORCOCLEITER, BerechtigungZiel.SENSOR_COC)
                    .stream().map(b -> Long.parseLong(b.getFuerId())).collect(Collectors.toList());
            List<Long> sensorCoCLeiterSensorCocIds = sensorCoCLeiterSensorCoc.stream().map(SensorCoc::getSensorCocId).collect(Collectors.toList());
            sensorCoCLeiterSensorCocIds.removeAll(sensorCocIds);
            sensorCoCLeiterSensorCocIds.forEach(id ->
                    grantRoleSensorCoCLeiter(mitarbeiter, id, Rechttype.SCHREIBRECHT)
            );
        } else if (sensorSensorCoc != null && !sensorSensorCoc.isEmpty()) {
            // berechtige fuer rolle sensor (eingeschraenkt)
            List<Long> sensorSensorCocIds = sensorSensorCoc.stream().map(SensorCoc::getSensorCocId).collect(Collectors.toList());
            if (realm.isEingeschraenkt()) {
                LOG.debug("Berechtige Mitarbeiter {} als Sensor eingeschränkt", mitarbeiter);
                List<Long> sensorCocIds = berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZiel(mitarbeiter, Rolle.SENSOR_EINGESCHRAENKT, BerechtigungZiel.SENSOR_COC)
                        .stream().map(b -> Long.parseLong(b.getFuerId())).collect(Collectors.toList());
                sensorSensorCocIds.removeAll(sensorCocIds);
                sensorSensorCocIds.forEach(id ->
                        grantRoleSensorEingeschraenkt(mitarbeiter, id, Rechttype.SCHREIBRECHT)
                );
            } else {
                LOG.debug("Berechtige Mitarbeiter {} als Sensor", mitarbeiter);
                List<Long> sensorCocIds = berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZiel(mitarbeiter, Rolle.SENSOR, BerechtigungZiel.SENSOR_COC)
                        .stream().map(b -> Long.parseLong(b.getFuerId())).collect(Collectors.toList());
                sensorSensorCocIds.removeAll(sensorCocIds);
                sensorSensorCocIds.forEach(id ->
                        grantRoleSensor(mitarbeiter, id, Rechttype.SCHREIBRECHT)
                );
            }
        } else if (leserSensorCoc != null && !leserSensorCoc.isEmpty()) {
            // berechtige fuer rolle Sensor mit Leserechten fuer SensorCoc
            LOG.debug("Berechtige Mitarbeiter {} mit Leserrechten fuer SensorCoCs", mitarbeiter);
            List<Long> leserSensorCocIds = leserSensorCoc.stream().map(SensorCoc::getSensorCocId).collect(Collectors.toList());
            List<Long> sensorCocIds = berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZiel(mitarbeiter, Rolle.LESER, BerechtigungZiel.SENSOR_COC)
                    .stream().map(b -> Long.parseLong(b.getFuerId())).collect(Collectors.toList());
            leserSensorCocIds.removeAll(sensorCocIds);
            leserSensorCocIds.forEach(id ->
                    grantRoleSensor(mitarbeiter, id, Rechttype.LESERECHT)
            );
        }
    }

    private Mitarbeiter getMitarbeiterFromUserServiceWithEmail(BerechtigungsExcelRealm realm, Mitarbeiter mitarbeiter) {
        List<Mitarbeiter> result = userSearchService.getMitarbeiterByMail(realm.getMail());
        if (result != null && !result.isEmpty()) {
            mitarbeiter = result.get(0);
        }
        return mitarbeiter;
    }

    private Mitarbeiter getMitarbeiterFromUserServiceWithNameAndApteilung(String vorname, String nachname, BerechtigungsExcelRealm realm, Mitarbeiter mitarbeiter) {
        List<Mitarbeiter> result = userSearchService.getMitarbeiterByCriteria(vorname, nachname, realm.getAbteilung());
        if (result != null && result.size() == 1) {
            mitarbeiter = result.get(0);
        }
        return mitarbeiter;
    }

    private List<Long> getTteamsByNameList(List<String> tteams) {
        return tteamService.getTteamsByNameList(tteams).stream().map(Tteam::getId).collect(Collectors.toList());
    }

    private Boolean isAuthorized(Rolle rolle, Long sensorCocId) {
        if (startup) {
            return true;
        }
        Mitarbeiter currentUser = session.getUser();
        List<Long> userSensorCocIds = getAuthorizedSensorCocListForMitarbeiter(currentUser);
        if ((rolle == Rolle.SENSOR || rolle == Rolle.SENSOR_EINGESCHRAENKT || rolle == Rolle.UMSETZUNGSBESTAETIGER)
                && userSensorCocIds.stream().anyMatch(sid -> Objects.equals(sid, sensorCocId))) {
            return session.hasRole(Rolle.SENSORCOCLEITER) || session.hasRole(Rolle.ADMIN);
        }
        return session.hasRole(Rolle.ADMIN);
    }

    private Boolean isAuthorized(Rolle rolle) {

        if (startup) {
            return true;
        }
        if (rolle == Rolle.SENSOR || rolle == Rolle.SENSOR_EINGESCHRAENKT
                || rolle == Rolle.SCL_VERTRETER || rolle == Rolle.UMSETZUNGSBESTAETIGER) {
            return session.hasRole(Rolle.SENSORCOCLEITER) || session.hasRole(Rolle.ADMIN);
        }
        return session.hasRole(Rolle.ADMIN);
    }

    private boolean authorizeForSensorCoc(Mitarbeiter mitarbeiter, Rolle role, Long sensorcocId, Rechttype rechttype, BerechtigungZiel berechtigungsZiel) {
        boolean success = false;
        if (mitarbeiter != null) {
            String fuerId = String.valueOf(sensorcocId);
            success = processRechtZuweisungToMitarbeiter(mitarbeiter, role, fuerId, berechtigungsZiel, rechttype);
        }
        return success;
    }

    private boolean authorizeTteamleiterForTteam(Mitarbeiter mitarbeiter, Long tteamId, Rechttype rechttype) {
        boolean success = false;
        Rolle role = Rolle.T_TEAMLEITER;
        BerechtigungZiel fuer = BerechtigungZiel.TTEAM;

        if (mitarbeiter != null) {
            String fuerId = String.valueOf(tteamId);
            success = processRechtZuweisungToMitarbeiter(mitarbeiter, role, fuerId, fuer, rechttype);
        }
        return success;
    }

    private void historiciseAuthorization(Mitarbeiter userEdited, Rolle role, String zielId, String fuer, boolean permissionAdded, Rechttype rechttype) {
        Mitarbeiter currentUser = session.getUser();
        BerechtigungHistory berechtigung = new BerechtigungHistory(currentUser.getQNumber(), currentUser.getName(), userEdited.getQNumber(), userEdited.getName(), role.name(), zielId, fuer, permissionAdded, rechttype);
        persistBerechtigungHistory(berechtigung);
    }

    private boolean authorizeForRole(Mitarbeiter mitarbeiter, Rolle role, Rechttype rechttype) {
        boolean success = false;
        if (mitarbeiter != null) {
            if (!berechtigungDao.userHasRole(mitarbeiter, role)) {
                UserToRole userToRole = new UserToRole(mitarbeiter.getQNumber(), role.getRolleRawString());
                berechtigungDao.persistNewRolleForUser(userToRole);
            }

            berechtigungDao.addBerechtigung(mitarbeiter, role, "all", BerechtigungZiel.SYSTEM, rechttype);
            success = true;
        }
        return success;
    }

    private boolean authorizeForModulSeTeam(Mitarbeiter mitarbeiter, ModulSeTeam modulSeTeam, Rolle role, Rechttype rechttype) {
        boolean sucess = false;
        if (mitarbeiter != null) {
            sucess = processRechtZuweisungToMitarbeiter(mitarbeiter, role, modulSeTeam.getId().toString(), BerechtigungZiel.MODULSETEAM, rechttype);
        }
        return sucess;
    }

    private boolean authorizeForDerivat(Mitarbeiter mitarbeiter, Derivat derivat, Rolle role, Rechttype rechttype) {
        boolean sucess = false;
        if (mitarbeiter != null) {
            BerechtigungZiel fuer = BerechtigungZiel.DERIVAT;
            String fuerId = String.valueOf(derivat.getId());
            sucess = processRechtZuweisungToMitarbeiter(mitarbeiter, role, fuerId, fuer, rechttype);
        }
        return sucess;
    }

    private boolean authorizeForZak(Mitarbeiter mitarbeiter, String zakEinordnung, Rolle role, Rechttype rechttype) {
        return mitarbeiter != null && processRechtZuweisungToMitarbeiter(mitarbeiter, role, zakEinordnung, BerechtigungZiel.ZAK_EINORDNUNG, rechttype);
    }

    private void stripRole(Mitarbeiter mitarbeiterForBerechtigung, BerechtigungZiel fuer, String fuerId, Rolle role, Rechttype rechttype) {
        boolean authorized;

        if (fuer.equals(BerechtigungZiel.SENSOR_COC) && !role.equals(Rolle.ANFORDERER) && !role.equals(Rolle.SENSORCOCLEITER) && !role.equals(Rolle.SCL_VERTRETER)) {
            authorized = isAuthorized(role, Long.parseLong(fuerId));
        } else {
            authorized = isAuthorized(role);
        }

        if (authorized) {
            boolean sucess = false;
            if (mitarbeiterForBerechtigung != null) {
                sucess = processRechtEntziehungFromMitarbeiter(mitarbeiterForBerechtigung, role, fuerId, fuer);
            }
            if (sucess && !startup) {
                historiciseAuthorization(mitarbeiterForBerechtigung, role, fuerId, fuer.name(), false, rechttype);
            }
        }
    }

    private void grantRoleSensorCoc(Mitarbeiter mitarbeiter, Rolle role, Long sensorcocId, Rechttype rechttype) {
        grandRoleForSensor(mitarbeiter, sensorcocId, rechttype, role, isAuthorized(role));
    }

    public void grantRoleSensor(Mitarbeiter mitarbeiter, Long sensorcocId, Rechttype rechttype) {
        Rolle role = Rolle.SENSOR;
        grandRoleForSensor(mitarbeiter, sensorcocId, rechttype, role, isAuthorized(role, sensorcocId));
    }

    private void grandRoleForSensor(Mitarbeiter mitarbeiter, Long sensorcocId, Rechttype rechttype, Rolle role, Boolean authorized) {
        if (authorized) {
            boolean success = authorizeForSensorCoc(mitarbeiter, role, sensorcocId, rechttype, BerechtigungZiel.SENSOR_COC);
            if (success && !startup) {
                String fuer = BerechtigungZiel.SENSOR_COC.name();
                historiciseAuthorization(mitarbeiter, role, String.valueOf(sensorcocId), fuer, true, rechttype);
            }
        }
    }

    public void stripRoleSensor(Mitarbeiter mitarbeiter, Long sensorcocId, Rechttype rechttype) {
        if (sensorCocAuthorizedService.isUserAuthorizedToEditSensorCocPermission(sensorcocId)) {
            stripRole(mitarbeiter, BerechtigungZiel.SENSOR_COC, String.valueOf(sensorcocId), Rolle.SENSOR, rechttype);
        }
    }

    public void grantRoleSensorEingeschraenkt(Mitarbeiter mitarbeiter, Long sensorcocId, Rechttype rechttype) {
        Rolle role = Rolle.SENSOR_EINGESCHRAENKT;
        grandRoleForSensor(mitarbeiter, sensorcocId, rechttype, role, isAuthorized(role, sensorcocId));
    }

    public void stripRoleSensorEingeschraenkt(Mitarbeiter mitarbeiter, Long sensorcocId, Rechttype rechttype) {
        if (sensorCocAuthorizedService.isUserAuthorizedToEditSensorCocPermission(sensorcocId)) {
            stripRole(mitarbeiter, BerechtigungZiel.SENSOR_COC, String.valueOf(sensorcocId), Rolle.SENSOR_EINGESCHRAENKT, rechttype);
        }
    }

    public void grantRoleAdmin(Mitarbeiter mitarbeiter) {
        Rolle role = Rolle.ADMIN;
        Rechttype rechttype = Rechttype.SCHREIBRECHT;
        if (isAuthorized(role)) {
            boolean success = authorizeForRole(mitarbeiter, role, rechttype);
            if (success && !startup) {
                String fuer = BerechtigungZiel.SYSTEM.name();
                historiciseAuthorization(mitarbeiter, role, "all", fuer, true, rechttype);
            }
        }
    }

    public void stripRoleAdmin(Mitarbeiter mitarbeiter) {
        Rolle role = Rolle.ADMIN;
        if (isAuthorized(role)) {
            boolean sucess = false;
            if (mitarbeiter != null) {
                if (berechtigungDao.userHasRole(mitarbeiter, role)) {
                    berechtigungDao.removeRoleFromUser(mitarbeiter, Rolle.ADMIN);
                }
                berechtigungDao.removeBerechtigung(mitarbeiter, role, "all", BerechtigungZiel.SYSTEM);
                sucess = true;
            }
            if (sucess && !startup) {
                String fuer = BerechtigungZiel.SYSTEM.name();
                historiciseAuthorization(mitarbeiter, role, "all", fuer, false, Rechttype.SCHREIBRECHT);
            }
        }
    }

    public void grantRoleSensorCoCLeiter(Mitarbeiter mitarbeiter, Long sensorcocId, Rechttype rechttype) {
        grantRoleSensorCoc(mitarbeiter, Rolle.SENSORCOCLEITER, sensorcocId, rechttype);
    }

    public void stripRoleSensorCoCLeiter(Mitarbeiter mitarbeiter, Long sensorcocId, Rechttype rechttype) {
        if (sensorCocAuthorizedService.isUserAuthorizedToEditSensorCocPermission(sensorcocId)) {
            stripRole(mitarbeiter, BerechtigungZiel.SENSOR_COC, String.valueOf(sensorcocId), Rolle.SENSORCOCLEITER, rechttype);
        }
    }

    public void grantRoleVertreterSCL(Mitarbeiter mitarbeiter, Long sensorcocId, Rechttype rechttype) {
        grantRoleSensorCoc(mitarbeiter, Rolle.SCL_VERTRETER, sensorcocId, rechttype);
    }

    public void stripRoleVertreterSCL(Mitarbeiter mitarbeiter, Long sensorcocId, Rechttype rechttype) {
        if (sensorCocAuthorizedService.isUserAuthorizedToEditSensorCocPermission(sensorcocId)) {
            stripRole(mitarbeiter, BerechtigungZiel.SENSOR_COC, String.valueOf(sensorcocId), Rolle.SCL_VERTRETER, rechttype);
        }
    }


    public void grantRoleTeamleiter(Mitarbeiter mitarbeiter, Long tteamId, Rechttype rechttype) {
        Rolle role = Rolle.T_TEAMLEITER;
        if (isAuthorized(role)) {
            boolean success = authorizeTteamleiterForTteam(mitarbeiter, tteamId, rechttype);
            if (success && !startup) {
                String fuer = BerechtigungZiel.TTEAM.name();
                String fuerId = String.valueOf(tteamId);
                historiciseAuthorization(mitarbeiter, role, fuerId, fuer, true, rechttype);
            }
        }
    }

    public void stripRoleTeamleiter(Mitarbeiter mitarbeiter, Long tteamId, Rechttype rechttype) {
        if (tteamAuthorizedService.isUserAuthorizedToEditTteamPermission(tteamId)) {
            Rolle role = Rolle.T_TEAMLEITER;
            stripRole(mitarbeiter, BerechtigungZiel.TTEAM, String.valueOf(tteamId), role, rechttype);
        }
    }

    public void grantRoleEcoc(Mitarbeiter mitarbeiter, ModulSeTeam modulSeTeam, Rechttype rechttype) {
        Rolle role = Rolle.E_COC;
        if (isAuthorized(role)) {
            boolean success = authorizeForModulSeTeam(mitarbeiter, modulSeTeam, role, rechttype);
            if (success && !startup) {
                historiciseAuthorization(mitarbeiter, role, modulSeTeam.getId().toString(), BerechtigungZiel.MODULSETEAM.name(), true, rechttype);
            }
        }
    }

    public void stripRoleEcoc(Mitarbeiter mitarbeiter, ModulSeTeam modulSeTeam, Rechttype rechttype) {
        stripRole(mitarbeiter, BerechtigungZiel.MODULSETEAM, modulSeTeam.getId().toString(), Rolle.E_COC, rechttype);
    }

    public void stripRoleEcoc(Mitarbeiter mitarbeiter, Long modulSeTeamId, Rechttype rechttype) {
        stripRole(mitarbeiter, BerechtigungZiel.MODULSETEAM, modulSeTeamId.toString(), Rolle.E_COC, rechttype);
    }

    public void authorizeAnfordererToSensorCoc(Mitarbeiter mitarbeiter, String zakEinordnung, Rechttype rechttype) {
        Rolle role = Rolle.ANFORDERER;
        if (isAuthorized(role)) {
            boolean sucess = tryAuthorizeAnfordererToSensorCoc(mitarbeiter, zakEinordnung, rechttype);
            if (sucess && !startup) {
                String fuer = BerechtigungZiel.ZAK_EINORDNUNG.name();
                historiciseAuthorization(mitarbeiter, role, zakEinordnung, fuer, true, rechttype);
            }
        }
    }

    public void grantRoleAnforderer(Mitarbeiter mitarbeiter, String zakEinordnung, Rechttype rechttype) {
        Rolle role = Rolle.ANFORDERER;
        if (isAuthorized(role)) {
            Boolean success = authorizeForZak(mitarbeiter, zakEinordnung, role, rechttype);
            if (success && !startup) {
                String fuer = BerechtigungZiel.ZAK_EINORDNUNG.name();
                historiciseAuthorization(mitarbeiter, role, zakEinordnung, fuer, true, rechttype);
            }
        }
    }

    public void grantRoleAnforderer(Mitarbeiter mitarbeiter, Derivat derivat, Rechttype rechttype, BerechtigungZiel ziel) {
        Rolle role = Rolle.ANFORDERER;
        if (isAuthorized(role)) {
            boolean success = authorizeForDerivat(mitarbeiter, derivat, role, rechttype);
            if (success && !startup) {
                String fuer = ziel.name();
                historiciseAuthorization(mitarbeiter, role, String.valueOf(derivat.getId()), fuer, true, rechttype);
            }
        }
    }

    public void stripRoleAnforderer(Mitarbeiter mitarbeiter, String zakEinordnung, Rechttype rechttype) {
        stripRole(mitarbeiter, BerechtigungZiel.ZAK_EINORDNUNG, zakEinordnung, Rolle.ANFORDERER, rechttype);
    }

    public void stripRoleAnforderer(Mitarbeiter mitarbeiter, Derivat derivat, Rechttype rechttype) {
        stripRole(mitarbeiter, BerechtigungZiel.DERIVAT, String.valueOf(derivat.getId()), Rolle.ANFORDERER, rechttype);
    }

    public void stripRoleAnforderer(Mitarbeiter mitarbeiter, Long derivatId, Rechttype rechttype) {
        stripRole(mitarbeiter, BerechtigungZiel.DERIVAT, String.valueOf(derivatId), Rolle.ANFORDERER, rechttype);
    }

    public void grantRoleUmsetzungsbestaetiger(Mitarbeiter mitarbeiter, Derivat derivat) {
        Rolle role = Rolle.UMSETZUNGSBESTAETIGER;
        Rechttype rechttype = Rechttype.SCHREIBRECHT;
        if (isAuthorized(role)) {
            boolean sucess = authorizeForDerivat(mitarbeiter, derivat, role, rechttype);
            if (sucess && !startup) {
                String fuer = BerechtigungZiel.DERIVAT.name();
                historiciseAuthorization(mitarbeiter, role, String.valueOf(derivat.getId()), fuer, true, rechttype);
            }
        }
    }

    public void stripRoleUmsetzungsbestaetiger(Mitarbeiter mitarbeiter, Derivat derivat) {
        Rolle role = Rolle.UMSETZUNGSBESTAETIGER;
        stripRole(mitarbeiter, BerechtigungZiel.DERIVAT, String.valueOf(derivat.getId()), role, Rechttype.SCHREIBRECHT);
    }

    public void grantRoleUmsetzungsbestaetigerForSCoC(Mitarbeiter mitarbeiter, Long sensorCoCId) {
        Rolle role = Rolle.UMSETZUNGSBESTAETIGER;
        Rechttype rechttype = Rechttype.SCHREIBRECHT;
        grandRoleForSensor(mitarbeiter, sensorCoCId, rechttype, role, isAuthorized(role, sensorCoCId));
    }

    private Mitarbeiter getUserDependingOnStartup() {
        if (!startup) {
            return session.getUser();
        } else {
            return userSearchService.getMitarbeiterByQnumber("q1111111");
        }
    }

    public void grantRoleTteamVertreter(Mitarbeiter selectedMitarbeiter, List<Tteam> tteamIds) {
        Mitarbeiter currentUser = getUserDependingOnStartup();
        if (startup || session.hasRole(Rolle.ADMIN) || session.hasRole(Rolle.T_TEAMLEITER)) {
            if (!berechtigungDao.userHasRole(selectedMitarbeiter, Rolle.TTEAM_VERTRETER)) {
                UserToRole userToRole = new UserToRole(selectedMitarbeiter.getQNumber(), Rolle.TTEAM_VERTRETER.getRolleRawString());
                berechtigungDao.persistNewRolleForUser(userToRole);
            }
            tteamIds.stream().map(tteam -> {
                TteamVertreter tteamVertreter = new TteamVertreter(tteam, selectedMitarbeiter);
                tteamVertreterDao.persistTteamVertreter(tteamVertreter);
                BerechtigungHistory berechtigungHistory = new BerechtigungHistory(currentUser.getQNumber(), currentUser.getName(), selectedMitarbeiter.getQNumber(),
                        selectedMitarbeiter.getName(), Rolle.TTEAM_VERTRETER.getBezeichnung(),
                        tteam.getTeamName(), T_TEAM, true, Rechttype.SCHREIBRECHT);
                return berechtigungHistory;
            }).forEachOrdered(this::persistBerechtigungHistory);
        }
    }

    public void grantRoleTteamMitglied(Mitarbeiter selectedMitarbeiter, List<Tteam> tteamIds, Rechttype rechttype) {
        Mitarbeiter currentUser = getUserDependingOnStartup();
        if (startup || session.hasRole(Rolle.ADMIN) || session.hasRole(Rolle.T_TEAMLEITER)) {
            if (!berechtigungDao.userHasRole(selectedMitarbeiter, Rolle.TTEAMMITGLIED)) {
                UserToRole userToRole = new UserToRole(selectedMitarbeiter.getQNumber(), Rolle.TTEAMMITGLIED.getRolleRawString());
                berechtigungDao.persistNewRolleForUser(userToRole);
            }
            tteamIds.stream().map(tteam -> {
                TTeamMitglied tteamMitglied = new TTeamMitglied(tteam, selectedMitarbeiter, rechttype);
                tteamMitgliedDao.persistTteamMitglied(tteamMitglied);
                BerechtigungHistory berechtigungHistory = new BerechtigungHistory(currentUser.getQNumber(), currentUser.getName(), selectedMitarbeiter.getQNumber(),
                        selectedMitarbeiter.getName(), Rolle.TTEAMMITGLIED.getBezeichnung(),
                        tteam.getTeamName(), T_TEAM, true, rechttype);
                return berechtigungHistory;
            }).forEachOrdered(this::persistBerechtigungHistory);
        }
    }

    private void stripRoleTteamMitglied(Mitarbeiter selectedMitarbeiter) {
        if (session.hasRole(Rolle.ADMIN) || session.hasRole(Rolle.T_TEAMLEITER)) {
            List<TTeamMitglied> tteamMitglieds = tteamMitgliedDao.getTteamMitgliedsForMitarbeiter(selectedMitarbeiter);
            tteamMitglieds.forEach(tteamMitglied -> tteamMitgliedDao.removeTteamMitgliedBerechtigung(tteamMitglied));
            berechtigungDao.removeRoleFromUser(selectedMitarbeiter, Rolle.TTEAMMITGLIED);
        }
    }

    public void stripRoleTteamMitglied(Mitarbeiter selectedMitarbeiter, List<Tteam> tteamIds, Rechttype rechttype) {
        Mitarbeiter currentUser = session.getUser();
        if (session.hasRole(Rolle.ADMIN) || session.hasRole(Rolle.T_TEAMLEITER)) {

            tteamIds.forEach(tteam -> handleStripRoleTteamMitglied(selectedMitarbeiter, rechttype, currentUser, tteam));
            List<TTeamMitglied> tteamMitglieds = tteamMitgliedDao.getTteamMitgliedsForMitarbeiter(selectedMitarbeiter);
            if (berechtigungDao.userHasRole(selectedMitarbeiter, Rolle.TTEAMMITGLIED) && tteamMitglieds.isEmpty()) {
                berechtigungDao.removeRoleFromUser(selectedMitarbeiter, Rolle.TTEAMMITGLIED);
            }
        }
    }

    private void handleStripRoleTteamMitglied(Mitarbeiter selectedMitarbeiter, Rechttype rechttype, Mitarbeiter currentUser, Tteam tteam) {
        if (tteamAuthorizedService.isUserAuthorizedToEditTteamPermission(tteam)) {
            Optional<TTeamMitglied> tteamMitglied = tteamMitgliedDao.getTTeamMitgliedByTteamAndMitarbeiter(tteam, selectedMitarbeiter);
            if (tteamMitglied.isPresent()) {
                tteamMitgliedDao.removeTteamMitgliedBerechtigung(tteamMitglied.get());
                writeBerechtigungHistory(selectedMitarbeiter, rechttype, currentUser, tteam, Rolle.TTEAMMITGLIED);
            }
        }
    }

    private void writeBerechtigungHistory(Mitarbeiter selectedMitarbeiter, Rechttype rechttype, Mitarbeiter currentUser, Tteam tteam, Rolle tteammitglied) {
        BerechtigungHistory berechtigungHistory = new BerechtigungHistory(currentUser.getQNumber(), currentUser.getName(), selectedMitarbeiter.getQNumber(),
                selectedMitarbeiter.getName(), tteammitglied.getBezeichnung(),
                tteam.getTeamName(), T_TEAM, false, rechttype);
        persistBerechtigungHistory(berechtigungHistory);
    }

    public void stripRoleTteamVertreter(Mitarbeiter selectedMitarbeiter, List<Tteam> tteamIds) {
        Mitarbeiter currentUser = session.getUser();
        if (session.hasRole(Rolle.ADMIN) || session.hasRole(Rolle.T_TEAMLEITER)) {
            tteamIds.forEach(tteam -> handleStripRoleTteamVertreter(selectedMitarbeiter, currentUser, tteam));
            List<TteamVertreter> tteamVertreter = tteamVertreterDao.getTteamVertreterForMitarbeiter(selectedMitarbeiter);
            if (berechtigungDao.userHasRole(selectedMitarbeiter, Rolle.TTEAM_VERTRETER) && tteamVertreter.isEmpty()) {
                berechtigungDao.removeRoleFromUser(selectedMitarbeiter, Rolle.TTEAM_VERTRETER);
            }
        }
    }

    private void handleStripRoleTteamVertreter(Mitarbeiter selectedMitarbeiter, Mitarbeiter currentUser, Tteam tteam) {
        if (tteamAuthorizedService.isUserAuthorizedToEditTteamPermission(tteam)) {
            Optional<TteamVertreter> tteamVertreter = tteamVertreterDao.getTTeamVertreterByTteamAndMitarbeiter(tteam, selectedMitarbeiter);
            if (tteamVertreter.isPresent()) {
                tteamVertreterDao.removeTteamVertreterBerechtigung(tteamVertreter.get());
                writeBerechtigungHistory(selectedMitarbeiter, Rechttype.SCHREIBRECHT, currentUser, tteam, Rolle.TTEAM_VERTRETER);
            }
        }
    }

    public void stripRoleUmsetzungsbestaetigerForSCoC(Mitarbeiter mitarbeiter, Long sensorCoCId) {
        if (sensorCocAuthorizedService.isUserAuthorizedToEditSensorCocPermission(sensorCoCId)) {
            Rolle role = Rolle.UMSETZUNGSBESTAETIGER;
            stripRole(mitarbeiter, BerechtigungZiel.SENSOR_COC, String.valueOf(sensorCoCId), role, Rechttype.SCHREIBRECHT);
        }
    }

    public void stripRoleLeser(Mitarbeiter mitarbeiter, String fuerId, BerechtigungZiel fuer) {
        Rolle role = Rolle.LESER;
        stripRole(mitarbeiter, fuer, fuerId, role, Rechttype.LESERECHT);
    }

    public Boolean isMyModulSeTeam(ModulSeTeam modulSeTeam) {
        if (session.hasRole(Rolle.ADMIN)) {
            return true;
        } else {
            List<ModulSeTeam> moduls = getAuthorizedModulSeTeamsForEcoc(session.getUser());
            return moduls.contains(modulSeTeam);
        }
    }

    public boolean tryAuthorizeAnfordererToSensorCoc(Mitarbeiter mitarbeiter, String zakEinordnung, Rechttype rechttype) {
        boolean sucess = false;
        if (mitarbeiter != null) {
            List<SensorCoc> sensorCocList = sensorCocService.getSensorCocByZakEinordnung(zakEinordnung);
            if (sensorCocList != null && !sensorCocList.isEmpty()) {
                berechtigungDao.addBerechtigung(mitarbeiter, Rolle.ANFORDERER, zakEinordnung, BerechtigungZiel.ZAK_EINORDNUNG, rechttype);
                sucess = true;
            }
        }
        return sucess;
    }

    private boolean processRechtZuweisungToMitarbeiter(Mitarbeiter mitarbeiter, Rolle role, String fuerId, BerechtigungZiel fuer, Rechttype rechttype) {
        boolean sucess = false;
        switch (fuer) {
            case SENSOR_COC:
                sucess = processRechtZuweisenSensorCoc(fuerId, mitarbeiter, role, fuer, rechttype, sucess);
                break;
            case TTEAM:
                sucess = processRechtZuweisenTteam(fuerId, mitarbeiter, role, fuer, rechttype, sucess);
                break;
            case MODULSETEAM:
                sucess = processRechtZuweisenModulSeTeam(fuerId, mitarbeiter, role, fuer, rechttype, sucess);
                break;
            case DERIVAT:
                sucess = processRechtZuweisenDerivat(fuerId, mitarbeiter, role, fuer, rechttype, sucess);
                break;
            case ZAK_EINORDNUNG:
                sucess = processRechtZuweisenZakEinordnung(mitarbeiter, role, fuerId, fuer, rechttype, sucess);
                break;
            default:
                break;

        }
        return sucess;
    }

    private boolean processRechtZuweisenZakEinordnung(Mitarbeiter mitarbeiter, Rolle role, String fuerId, BerechtigungZiel fuer, Rechttype rechttype, boolean sucess) {
        if (mitarbeiter != null) {
            if (!berechtigungDao.userHasRole(mitarbeiter, role)) {
                UserToRole userToRole = new UserToRole(mitarbeiter.getQNumber(), role.getRolleRawString());
                berechtigungDao.persistNewRolleForUser(userToRole);
            }
            userService.saveMitarbeiter(mitarbeiter);
            berechtigungDao.addBerechtigung(mitarbeiter, role, fuerId, fuer, rechttype);
            sucess = true;
        }
        return sucess;
    }

    private boolean processRechtZuweisenDerivat(String fuerId, Mitarbeiter mitarbeiter, Rolle role, BerechtigungZiel fuer, Rechttype rechttype, boolean sucess) {
        Long derivatId = Long.parseLong(fuerId);
        Derivat derivat = derivatService.getDerivatById(derivatId);
        if (mitarbeiter != null && derivat != null) {
            if (!berechtigungDao.userHasRole(mitarbeiter, role)) {
                UserToRole userToRole = new UserToRole(mitarbeiter.getQNumber(), role.getRolleRawString());
                berechtigungDao.persistNewRolleForUser(userToRole);
            }

            userService.saveMitarbeiter(mitarbeiter);
            derivatService.persistDerivat(derivat);

            berechtigungDao.addBerechtigung(mitarbeiter, role, fuerId, fuer, rechttype);
            sucess = true;
        }
        return sucess;
    }

    private boolean processRechtZuweisenModulSeTeam(String fuerId, Mitarbeiter mitarbeiter, Rolle role, BerechtigungZiel fuer, Rechttype rechttype, boolean sucess) {
        ModulSeTeam modulSeTeam = modulService.getSeTeamById(Long.parseLong(fuerId));
        if (mitarbeiter != null && modulSeTeam != null) {
            sucess = persistRoleAndAddBerechtigung(fuerId, mitarbeiter, role, fuer, rechttype);
        }
        return sucess;
    }

    private boolean persistRoleAndAddBerechtigung(String fuerId, Mitarbeiter mitarbeiter, Rolle role, BerechtigungZiel fuer, Rechttype rechttype) {
        if (!berechtigungDao.userHasRole(mitarbeiter, role)) {
            UserToRole userToRole = new UserToRole(mitarbeiter.getQNumber(), role.getRolleRawString());
            berechtigungDao.persistNewRolleForUser(userToRole);
        }
        berechtigungDao.addBerechtigung(mitarbeiter, role, fuerId, fuer, rechttype);
        return true;
    }

    private boolean processRechtZuweisenTteam(String fuerId, Mitarbeiter mitarbeiter, Rolle role, BerechtigungZiel fuer, Rechttype rechttype, boolean sucess) {
        Long tteamId = Long.parseLong(fuerId);
        Tteam tteam = tteamService.getTteamById(tteamId);
        if (mitarbeiter != null && tteam != null) {
            sucess = persistRoleAndAddBerechtigung(fuerId, mitarbeiter, role, fuer, rechttype);
        }
        return sucess;
    }

    private boolean processRechtZuweisenSensorCoc(String fuerId, Mitarbeiter mitarbeiter, Rolle role, BerechtigungZiel fuer, Rechttype rechttype, boolean sucess) {
        Long sensorcocId = Long.parseLong(fuerId);
        SensorCoc sensorCoc = sensorCocService.getSensorCocById(sensorcocId);
        if (mitarbeiter != null && sensorCoc != null && role != null) {

            switch (role) {
                case SENSORCOCLEITER:
                case SCL_VERTRETER:
                    processRechtZuweisenForDerivatAsUmsetungsbestaetiger(fuerId, mitarbeiter, fuer, rechttype);
                    break;
                default:
                    break;

            }

            if (!berechtigungDao.userHasRole(mitarbeiter, role)) {
                UserToRole userToRole = new UserToRole(mitarbeiter.getQNumber(), role.getRolleRawString());
                berechtigungDao.persistNewRolleForUser(userToRole);
            }
            berechtigungDao.addBerechtigung(mitarbeiter, role, fuerId, fuer, rechttype);
            sucess = true;

        }
        return sucess;
    }

    private void processRechtZuweisenForDerivatAsUmsetungsbestaetiger(String fuerId, Mitarbeiter mitarbeiter, BerechtigungZiel fuer, Rechttype rechttype) {
        berechtigungDao.addBerechtigung(mitarbeiter, Rolle.UMSETZUNGSBESTAETIGER, fuerId, fuer, rechttype);
        for (Derivat dd : derivatService.getAllDerivate()) {
            List<Berechtigung> beriList = berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZielById(mitarbeiter, Rolle.UMSETZUNGSBESTAETIGER, String.valueOf(dd.getId()), BerechtigungZiel.DERIVAT);
            if (beriList == null || beriList.isEmpty()) {
                this.grantRoleUmsetzungsbestaetiger(mitarbeiter, dd);
            }
        }
    }

    private boolean processRechtEntziehungFromMitarbeiter(Mitarbeiter mitarbeiter, Rolle role, String fuerId, BerechtigungZiel fuer) {
        boolean sucess = false;
        switch (fuer) {
            case SENSOR_COC:
                sucess = processRechtEntziehenSensorCoc(fuerId, mitarbeiter, role, fuer, sucess);
                break;
            case TTEAM:
                sucess = processRechtEntziehenTteam(fuerId, mitarbeiter, role, fuer, sucess);
                break;
            case MODULSETEAM:
                sucess = processRechtEntziehenModulSeTeam(fuerId, mitarbeiter, role, fuer, sucess);
                break;
            case DERIVAT:
                sucess = processRechtEntziehenDerivat(fuerId, mitarbeiter, role, fuer, sucess);
                break;
            case ZAK_EINORDNUNG:
                sucess = processRechtEntziehenZakEinordnung(mitarbeiter, role, fuerId, fuer, sucess);
                break;
            default:
                break;
        }
        return sucess;
    }

    private boolean processRechtEntziehenZakEinordnung(Mitarbeiter mitarbeiter, Rolle role, String fuerId, BerechtigungZiel fuer, boolean sucess) {
        if (mitarbeiter != null) {
            this.removeBerechtigung(mitarbeiter, role, fuerId, fuer);
            sucess = true;
        }
        return sucess;
    }

    private boolean processRechtEntziehenDerivat(String fuerId, Mitarbeiter mitarbeiter, Rolle role, BerechtigungZiel fuer, boolean sucess) {
        Long derivatId = Long.parseLong(fuerId);
        Derivat derivat = derivatService.getDerivatById(derivatId);
        if (mitarbeiter != null && derivat != null) {
            switch (role) {
                case ANFORDERER:
                case UMSETZUNGSBESTAETIGER:
                case LESER:
                    removeBerechtigung(mitarbeiter, role, fuerId, fuer);
                    break;
                default:
                    break;
            }

            // update selected derivate
            List<Derivat> userSelectedDerivate = mitarbeiter.getSelectedDerivate();
            List<Derivat> updatedSelectedDerivate = new ArrayList<>(userSelectedDerivate);

            List<Derivat> userSelectedZakDerivate = mitarbeiter.getSelectedZakDerivate();
            List<Derivat> updatedSelectedZakDerivate = new ArrayList<>(userSelectedZakDerivate);

            if (userSelectedDerivate != null && !userSelectedDerivate.isEmpty()) {
                userSelectedDerivate.stream()
                        .filter(d -> d.getId().equals(derivat.getId()))
                        .forEachOrdered(updatedSelectedDerivate::remove);
            }

            if (userSelectedZakDerivate != null && !userSelectedZakDerivate.isEmpty()) {
                userSelectedZakDerivate.stream()
                        .filter(d -> d.getId().equals(derivat.getId()))
                        .forEachOrdered(updatedSelectedZakDerivate::remove);
            }

            mitarbeiter.setSelectedDerivate(updatedSelectedDerivate);
            mitarbeiter.setSelectedZakDerivate(updatedSelectedZakDerivate);
            userService.saveMitarbeiter(mitarbeiter);

            sucess = true;
        }
        return sucess;
    }

    private boolean processRechtEntziehenModulSeTeam(String fuerId, Mitarbeiter mitarbeiter, Rolle role, BerechtigungZiel fuer, boolean sucess) {
        ModulSeTeam modulSeTeam = modulService.getSeTeamById(Long.parseLong(fuerId));
        if (mitarbeiter != null && modulSeTeam != null) {
            this.removeBerechtigung(mitarbeiter, role, fuerId, fuer);
            sucess = true;
        }
        return sucess;
    }

    private boolean processRechtEntziehenTteam(String fuerId, Mitarbeiter mitarbeiter, Rolle role, BerechtigungZiel fuer, boolean sucess) {
        Long tteamId = Long.parseLong(fuerId);
        Tteam tteam = tteamService.getTteamById(tteamId);
        if (mitarbeiter != null && tteam != null) {
            berechtigungDao.removeBerechtigung(mitarbeiter, role, fuerId, fuer);
            List<Tteam> teams = tteamService.getTteamByTeamleiter(mitarbeiter);
            if (teams.isEmpty() && berechtigungDao.userHasRole(mitarbeiter, role)) {
                berechtigungDao.removeRoleFromUser(mitarbeiter, role);
            }
            sucess = true;
        }
        return sucess;
    }

    private boolean processRechtEntziehenSensorCoc(String fuerId, Mitarbeiter mitarbeiter, Rolle role, BerechtigungZiel fuer, boolean sucess) {
        SensorCoc sensorCoc = sensorCocService.getSensorCocById(Long.parseLong(fuerId));
        if (mitarbeiter != null && sensorCoc != null) {
            berechtigungDao.removeBerechtigung(mitarbeiter, role, fuerId, fuer);

            if (null != role) {
                switch (role) {
                    case SENSORCOCLEITER:
                    case SCL_VERTRETER:
                        processRechtEntziehenForSensorCocLeiterUndVertreter(fuerId, mitarbeiter, role, fuer);
                        break;
                    case SENSOR:
                    case SENSOR_EINGESCHRAENKT:
                    case LESER:
                    case UMSETZUNGSBESTAETIGER:
                        Integer diff = getNumberOfBerechtigungen(mitarbeiter, role);
                        if (diff == 0 && berechtigungDao.userHasRole(mitarbeiter, role)) {
                            berechtigungDao.removeRoleFromUser(mitarbeiter, role);
                        }
                        break;
                    default:
                        break;
                }
            }
            sucess = true;
        }
        return sucess;
    }

    private void processRechtEntziehenForSensorCocLeiterUndVertreter(String fuerId, Mitarbeiter mitarbeiter, Rolle role, BerechtigungZiel fuer) {
        berechtigungDao.removeBerechtigung(mitarbeiter, Rolle.UMSETZUNGSBESTAETIGER, fuerId, fuer);

        Integer diff = getNumberOfBerechtigungen(mitarbeiter, role);
        if (berechtigungDao.userHasRole(mitarbeiter, role) && diff == 0) {
            berechtigungDao.removeRoleFromUser(mitarbeiter, role);

        }

        Integer count = getNumberOfBerechigungenByZiel(mitarbeiter, Rolle.UMSETZUNGSBESTAETIGER, BerechtigungZiel.SENSOR_COC);
        if (count == 0 && berechtigungDao.userHasRole(mitarbeiter, Rolle.UMSETZUNGSBESTAETIGER)) {
            for (Derivat dd : derivatService.getAllDerivate()) {
                List<Berechtigung> beriList = berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZielById(mitarbeiter, Rolle.UMSETZUNGSBESTAETIGER, String.valueOf(dd.getId()), BerechtigungZiel.DERIVAT);
                if (beriList != null && !beriList.isEmpty()) {
                    stripRoleUmsetzungsbestaetiger(mitarbeiter, dd);
                }
            }
        }
    }

    private void removeBerechtigung(Mitarbeiter mitarbeiter, Rolle role, String fuerId, BerechtigungZiel fuer) {
        berechtigungDao.removeBerechtigung(mitarbeiter, role, fuerId, fuer);
        List<Berechtigung> berechtigungen = berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZiel(mitarbeiter, role, fuer);
        if (berechtigungen.isEmpty() && berechtigungDao.userHasRole(mitarbeiter, role)) {
            berechtigungDao.removeRoleFromUser(mitarbeiter, role);
        }
    }

    private Integer getNumberOfBerechtigungen(Mitarbeiter mitarbeiter, Rolle rolle) {
        return berechtigungDao.getBerechtigungByMitarbeiterAndRolle(mitarbeiter, rolle).size();
    }

    private Integer getNumberOfBerechigungenByZiel(Mitarbeiter mitarbeiter, Rolle rolle, BerechtigungZiel berechtigungZiel) {
        return berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZiel(mitarbeiter, rolle, berechtigungZiel).size();
    }

    public List<Rolle> getRolesForUser(Mitarbeiter mitarbeiter) {
        return berechtigungDao.getRolesByQnumber(mitarbeiter.getQNumber());
    }

    public List<Rolle> getAllRoles() {
        List<Rolle> roles = new ArrayList<>();
        roles.addAll(Arrays.asList(Rolle.values()));
        return roles;
    }

    private List<ModulSeTeam> getAuthorizedModulSeTeamsForEcoc(Mitarbeiter mitarbeiter) {
        List<Long> modulSeTeamIdList = berechtigungDao.getAuthorizedModulnamesForEcoc(mitarbeiter)
                .stream()
                .map(Long::parseLong)
                .collect(Collectors.toList());
        List<ModulSeTeam> result = new ArrayList<>();
        result.addAll(modulService.getSeTeamsByIdList(modulSeTeamIdList));
        return result;
    }

    public List<SensorCoc> getAuthorizedSensorCocObjectListForMitarbeiter(Mitarbeiter mitarbeiter) {
        return berechtigungDao.getAuthorizedSensorCocListForMitarbeiter(mitarbeiter);
    }

    public List<SensorCoc> getSensorCocForMitarbeiterForRoleEdit(Mitarbeiter mitarbeiter, Boolean withSchreibrecht) {
        Set<SensorCoc> result = new HashSet<>();
        getRolesForUser(mitarbeiter).forEach(r -> {
            if (!r.equals(Rolle.ANFORDERER)) {
                result.addAll(getSensorCocForMitarbeiterAndRolle(mitarbeiter, r, withSchreibrecht));
            }
        });
        return !result.isEmpty() ? new ArrayList<>(result) : new ArrayList<>();
    }

    public List<SensorCoc> getSensorCocForMitarbeiter(Mitarbeiter mitarbeiter, Boolean withSchreibrecht) {
        Set<SensorCoc> result = new HashSet<>();
        getRolesForUser(mitarbeiter).forEach(r ->
                result.addAll(getSensorCocForMitarbeiterAndRolle(mitarbeiter, r, withSchreibrecht)));
        return !result.isEmpty() ? new ArrayList<>(result) : new ArrayList<>();
    }

    public List<SensorCoc> getSensorCocForMitarbeiterAndRolle(Mitarbeiter mitarbeiter, Rolle rolle, Boolean withSchreibrecht) {
        Rechttype rechttype = withSchreibrecht ? Rechttype.SCHREIBRECHT : Rechttype.LESERECHT;
        if (rolle.equals(Rolle.ADMIN) || rolle.equals(Rolle.T_TEAMLEITER) || rolle.equals(Rolle.ANFORDERER)) {
            return sensorCocService.getAllSortByTechnologie();
        }
        List<Long> sensorCocIds = berechtigungDao
                .getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype(mitarbeiter, rolle, BerechtigungZiel.SENSOR_COC, rechttype)
                .stream()
                .map(Long::parseLong)
                .collect(Collectors.toList());
        if (!sensorCocIds.isEmpty()) {
            return sensorCocService.getSensorCocsByIdList(sensorCocIds);
        }
        return new ArrayList<>();
    }

    public List<Derivat> getDerivateForCurrentUser() {
        if (session.hasRole(Rolle.ADMIN) || session.hasRole(Rolle.T_TEAMLEITER)) {
            return derivatService.getAllDerivate();
        }
        List<Derivat> returnList = new ArrayList<>();
        if (session.hasRole(Rolle.ANFORDERER)) {
            returnList.addAll(getAuthorizedDerivatListByMitarbeiterAndRolle(session.getUser(), Rolle.ANFORDERER));
        }
        if (session.hasRole(Rolle.UMSETZUNGSBESTAETIGER)) {
            returnList.addAll(getAuthorizedDerivatListByMitarbeiterAndRolle(session.getUser(), Rolle.UMSETZUNGSBESTAETIGER));
        }
        return returnList;
    }

    public List<Derivat> getAllExistingDerivateForCurrentUser() {
        if (session.hasRole(Rolle.ADMIN) || session.hasRole(Rolle.T_TEAMLEITER)) {
            return derivatService.findAllExistingDerivate();
        }
        return getDerivateForCurrentUser();
    }

    public List<Derivat> getAuthorizedDerivatListByMitarbeiterAndRolle(Mitarbeiter mitarbeiter, Rolle rolle) {
        List<Long> derivatIdList = berechtigungDao.getDerivateByMitarbeiterAndRolle(mitarbeiter, rolle);
        List<Derivat> result = new ArrayList<>();
        result.addAll(derivatService.getDerivatByIdList(derivatIdList));
        return result;
    }

    public List<Tteam> getTteamsForMitarbeiterForZuordnenDialog(Mitarbeiter mitarbeiter) {
        return getAuthorizedTteamsForMitarbeiter(mitarbeiter);

    }

    public List<Tteam> getTteamsForMitarbeiter(Mitarbeiter mitarbeiter) {
        if (berechtigungDao.userHasRole(mitarbeiter, Rolle.ADMIN) || berechtigungDao.userHasRole(mitarbeiter, Rolle.ANFORDERER)) {
            return tteamService.getAllTteams();
        }
        return getAuthorizedTteamsForMitarbeiter(mitarbeiter);
    }

    /**
     * Does not work for Role ADMIN! Use getTteamsForMitarbeiter instead.
     *
     * @param mitarbeiter
     * @return
     * @deprecated
     */
    @Deprecated
    public List<Tteam> getAuthorizedTteamsForMitarbeiter(Mitarbeiter mitarbeiter) {
        return berechtigungDao.getAuthorizedAsSchreiberTteamListForMitarbeiter(mitarbeiter);
    }

    public List<String> getAuthorizedZakEinordnungenForMitarbeiter(Mitarbeiter mitarbeiter) {
        return berechtigungDao.getAuthorizedZakEinordnungListForMitarbeiter(mitarbeiter);
    }

    public List<ModulSeTeam> getAuthorizedModulSeTeamListForMitarbeiter(Mitarbeiter mitarbeiter) {
        return berechtigungDao.getAuthorizedModulSeTeamListForMitarbeiter(mitarbeiter);
    }

    public List<Long> getAuthorizedSensorCocListForMitarbeiter(Mitarbeiter mitarbeiter) {
        return berechtigungDao.getAuthorizedSensorCocIdListForMitarbeiter(mitarbeiter);
    }

    public void setStartup(Boolean startup) {
        this.startup = startup;
    }

    public List<Mitarbeiter> getUmsetzungsbestaetigetListForSensorCoc(SensorCoc scoc) {
        List<Mitarbeiter> result = new ArrayList<>();
        if (scoc != null) {
            String fuerId = String.valueOf(scoc.getSensorCocId());
            result.addAll(berechtigungDao.getMitarbeiterForDefinierteBerechtigungByRolleAndZielId(Rolle.UMSETZUNGSBESTAETIGER, fuerId, BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT));
        }
        return result;
    }

    public List<Berechtigung> getBerechtigungByMitarbeiterAndRolleAndZielById(Mitarbeiter mitarbeiter, Rolle rolle, String fuerId, BerechtigungZiel fuer) {
        return berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZielById(mitarbeiter, rolle, fuerId, fuer);
    }

    public Optional<Mitarbeiter> getSensorCoCLeiterOfSensorCoc(SensorCoc sensorCoc) {
        return berechtigungDao.getSensorCoCLeiterForSensorCoc(sensorCoc);
    }

    public Collection<Mitarbeiter> getSensorCoCLeiterAndVertreterOfSensorCoc(SensorCoc sensorCoc) {
        Set<Mitarbeiter> result = new HashSet<>();
        getSensorCoCLeiterOfSensorCoc(sensorCoc).ifPresent(result::add);
        result.addAll(berechtigungDao.getSensorCoCVertreter(sensorCoc));
        return result;
    }

    public Mitarbeiter getAnfordererForSensorCocDerivat(SensorCoc sensorCoc, Derivat derivat) {
        List<Derivat> derivatList = new ArrayList<>();
        derivatList.add(derivat);
        List<Mitarbeiter> anfordererForDerivat = getMitarbeiterByDerivatenInListRolleAndRechttype(derivatList, Rolle.ANFORDERER, Rechttype.SCHREIBRECHT);
        if (anfordererForDerivat != null && !anfordererForDerivat.isEmpty()) {
            List<Mitarbeiter> anfordererForSensorCoc = getAnfordererForSensorCoc(sensorCoc);
            if (anfordererForSensorCoc != null && !anfordererForSensorCoc.isEmpty()) {
                anfordererForSensorCoc.retainAll(anfordererForDerivat);
                if (!anfordererForSensorCoc.isEmpty()) {
                    return anfordererForSensorCoc.get(0);
                }
            }
        }
        return null;
    }

    public List<Mitarbeiter> getAnfordererForSensorCoc(SensorCoc sensorCoc) {
        return berechtigungDao.getAnfordererForSensorCoc(sensorCoc);
    }

    public Collection<Mitarbeiter> getSensorCoCLeiterAndVertreterForSensorCocs(List<SensorCoc> sensorCocList) {
        if (sensorCocList == null || sensorCocList.isEmpty()) {
            return Collections.emptyList();
        } else {
            return sensorCocList.stream()
                    .filter(Objects::nonNull)
                    .flatMap(sensorCoc -> getSensorCoCLeiterAndVertreterOfSensorCoc(sensorCoc).stream())
                    .collect(Collectors.toSet());
        }
    }

    public Berechtigung getByFuerIdFuerRolleRechttype(String fuerId, Rolle rolle, BerechtigungZiel fuer, Rechttype rechttype) {
        return berechtigungDao.getByFuerIdFuerRolleRechttype(fuerId, rolle, fuer, rechttype);
    }

    public Optional<Mitarbeiter> getTteamleiterOfTteam(Tteam tteam) {
        return berechtigungDao.getTteamleiterOfTteam(tteam);
    }

    public List<Mitarbeiter> getEcocForAnforderung(Anforderung anforderung) {
        List<ModulSeTeam> seteams = anforderung.getUmsetzer().stream().map(Umsetzer::getSeTeam).distinct().collect(Collectors.toList());
        return getEcocForSeTeamList(seteams);
    }

    public List<Mitarbeiter> getEcocForSeTeamList(List<ModulSeTeam> modulSeTeamList) {
        List<String> modulSeTeamIdList = modulSeTeamList.stream().map(se -> se.getId().toString()).collect(Collectors.toList());
        return berechtigungDao.getMitarbeiterByRolleBerechtigungZielRechttypeAndFuerIdList(Rolle.E_COC, modulSeTeamIdList, BerechtigungZiel.MODULSETEAM, Rechttype.SCHREIBRECHT);
    }

    public List<String> getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype(Mitarbeiter mitarbeiter, Rolle rolle, BerechtigungZiel fuer, Rechttype rechttype) {
        return berechtigungDao.getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype(mitarbeiter, rolle, fuer, rechttype);
    }

    public List<Long> getAuthorizedSensorCocForSensor(Mitarbeiter mitarbeiter) {
        return berechtigungDao.getAuthorizedSensorCocForSensor(mitarbeiter);
    }

    public void persistNewRolleForUser(UserToRole userToRole) {
        berechtigungDao.persistNewRolleForUser(userToRole);
    }

    public BerechtigungDerivat getBerechtigungDerivatByMitarbeiterAndRolleForDerivat(Long derivatId, Mitarbeiter mitarbeiter, Rolle rolle) {
        return berechtigungDao.getBerechtigungDerivatByMitarbeiterAndRolleForDerivat(derivatId, mitarbeiter, rolle);
    }

    public List<Mitarbeiter> getMitarbeiterByDerivatenInListRolleAndRechttype(List<Derivat> derivatList, Rolle rolle, Rechttype rechttype) {
        return berechtigungDao.getMitarbeiterByDerivatenInListRolleAndRechttype(derivatList, rolle, rechttype);
    }

    public Boolean isAnfordererForDerivat(Derivat derivat, Mitarbeiter mitarbeiter) {
        if (berechtigungDao.userHasRole(mitarbeiter, Rolle.ADMIN)) {
            return true;
        }
        BerechtigungDerivat berechtigungDerivat = getBerechtigungDerivatByMitarbeiterAndRolleForDerivat(derivat.getId(), mitarbeiter, Rolle.ANFORDERER);
        return berechtigungDerivat != null;
    }

    public List<Berechtigung> getBerechtigungByMitarbeiter(Mitarbeiter mitarbeiter) {
        return berechtigungDao.getBerechtigungByMitarbeiter(mitarbeiter);
    }

    public void removeBerechtigungen(List<BerechtigungEditDto> berechtigungen, Mitarbeiter mitarbeiter) {
        for (BerechtigungEditDto b : berechtigungen) {
            switch (b.getRolle()) {
                case SENSOR:
                    stripRoleSensor(mitarbeiter, b.getId(), b.getRechttype());
                    break;
                case SENSOR_EINGESCHRAENKT:
                    stripRoleSensorEingeschraenkt(mitarbeiter, b.getId(), b.getRechttype());
                    break;
                case SENSORCOCLEITER:
                    stripRoleSensorCoCLeiter(mitarbeiter, b.getId(), b.getRechttype());
                    break;
                case SCL_VERTRETER:
                    this.stripRoleVertreterSCL(mitarbeiter, b.getId(), b.getRechttype());
                    break;
                case ANFORDERER:
                    switch (b.getType()) {
                        case DERIVAT:
                            stripRoleAnforderer(mitarbeiter, b.getId(), b.getRechttype());
                            break;
                        case ZAK_EINORDNUNG:
                            stripRoleAnforderer(mitarbeiter, b.getName(), b.getRechttype());
                            break;
                        default:
                            break;
                    }
                    break;
                case ADMIN:
                    stripRoleAdmin(mitarbeiter);
                    break;
                case E_COC:
                    stripRoleEcoc(mitarbeiter, b.getId(), b.getRechttype());
                    break;
                case T_TEAMLEITER:
                    stripRoleTeamleiter(mitarbeiter, b.getId(), b.getRechttype());
                    break;
                case UMSETZUNGSBESTAETIGER:
                    stripRoleUmsetzungsbestaetigerForSCoC(mitarbeiter, b.getId());
                    break;
                case LESER:
                    stripRoleLeser(mitarbeiter, b.getId().toString(), b.getType());
                    break;
                case TTEAMMITGLIED:
                    stripRoleTteamMitglied(mitarbeiter);
                    break;
                default:
                    break;
            }
        }
    }

    public List<String> getBerechtigungIdsForUserAndBerechtigungZiel(Mitarbeiter user, BerechtigungZiel fuer) {
        return berechtigungDao.getBerechtigungIdsForUserAndBerechtigungZiel(user, fuer);
    }

    public Optional<TteamMitgliedBerechtigung> getTteamMitgliedBerechtigungForUser(Mitarbeiter mitarbeiter) {
        return userPermissionsService.getTteamMitgliedBerechtigung(mitarbeiter);
    }

    public Optional<TteamVertreterBerechtigung> getTteamVertreterBerechtigungForUser(Mitarbeiter mitarbeiter) {
        return userPermissionsService.getTeamVertreterBerechtigung(mitarbeiter);
    }

    public void removeTteamMitgliedBerechtigungen(Mitarbeiter mitarbeiter, List<Long> tteamLesend, List<Long> tteamSchreibend) {

        tteamLesend.stream().map(tteamId -> tteamService.getTteamById(tteamId)).map(tteam -> tteamMitgliedDao.getTTeamMitgliedByTteamAndMitarbeiter(tteam, mitarbeiter)).filter(Optional::isPresent).forEachOrdered(tteamMitglied ->
                tteamMitgliedDao.removeTteamMitgliedBerechtigung(tteamMitglied.get())
        );

        tteamSchreibend.stream().map(tteamId -> tteamService.getTteamById(tteamId)).
                map(tteam -> tteamMitgliedDao.getTTeamMitgliedByTteamAndMitarbeiter(tteam, mitarbeiter)).filter(Optional::isPresent).forEachOrdered(tteamMitglied ->
                tteamMitgliedDao.removeTteamMitgliedBerechtigung(tteamMitglied.get())
        );

        List<TTeamMitglied> tteamMitglieds = tteamMitgliedDao.getTteamMitgliedsForMitarbeiter(mitarbeiter);
        if (berechtigungDao.userHasRole(mitarbeiter, Rolle.TTEAMMITGLIED) && tteamMitglieds.isEmpty()) {
            berechtigungDao.removeRoleFromUser(mitarbeiter, Rolle.TTEAMMITGLIED);

        }

    }

    public void removeTteamVertreterdBerechtigungen(Mitarbeiter mitarbeiter, List<Long> tteamIds) {
        tteamIds.stream().map(tteamId -> tteamService.getTteamById(tteamId)).map(tteam -> tteamVertreterDao.getTTeamVertreterByTteamAndMitarbeiter(tteam, mitarbeiter)).filter(Optional::isPresent).forEachOrdered(ttemVertreter ->
                tteamVertreterDao.removeTteamVertreterBerechtigung(ttemVertreter.get())
        );

        List<TteamVertreter> tteamVertreter = tteamVertreterDao.getTteamVertreterForMitarbeiter(mitarbeiter);
        if (berechtigungDao.userHasRole(mitarbeiter, Rolle.TTEAM_VERTRETER) && tteamVertreter.isEmpty()) {
            berechtigungDao.removeRoleFromUser(mitarbeiter, Rolle.TTEAM_VERTRETER);
        }
    }

}
