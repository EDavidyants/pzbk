package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.MitarbeiterDao;
import de.interfaceag.bmw.pzbk.dao.UserDefinedSearchDao;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.LocalGroup;
import de.interfaceag.bmw.pzbk.entities.LocalUser;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.UserDefinedSearch;
import de.interfaceag.bmw.pzbk.excel.ExcelUtils;
import de.interfaceag.bmw.pzbk.exceptions.ExcelInputFileException;
import de.interfaceag.bmw.pzbk.exceptions.UserNotFoundException;
import de.interfaceag.bmw.pzbk.shared.utils.SecurityUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Christian Schauer
 */
@Stateless
@Named
public class UserService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    @Inject
    private ConfigService cs;
    @Inject
    private UserSearchService uss;
    @Inject
    MitarbeiterDao mitarbeiterDao;
    @Inject
    private UserDefinedSearchDao userDefinedSearchDao;

    public void createAndPersistDummyUsers() {

        loadAndPersistLocalUsersFromExcelSheet();
        Collection<Mitarbeiter> users = cs.loadMitarbeiterFromExcelSheet();
        users.forEach(u -> registerDummyUsers(u));
    }

    private void saveLocalUser(LocalUser localUser) {
        mitarbeiterDao.saveLocalUser(localUser);
    }

    public void saveMitarbeiter(Mitarbeiter mitarbeiter) {
        mitarbeiterDao.persistMitarbeiter(mitarbeiter);
    }

    private void saveGruppe(LocalGroup gruppe) {
        if (isExistsGruppe(gruppe)) {
            mitarbeiterDao.registerGroup(gruppe);
        } else {
            mitarbeiterDao.registerNewGroup(gruppe);
        }
    }

    private Boolean isExistsGruppe(LocalGroup gruppe) {
        return mitarbeiterDao.isExistsGruppe(gruppe);
    }

    private List<LocalGroup> getGruppenByName(String gruppenname) {
        return mitarbeiterDao.getGruppenByName(gruppenname);
    }

    public Mitarbeiter updateUserData(Mitarbeiter currentUser, Mitarbeiter ldapUser) {
        if (currentUser != null && ldapUser != null && currentUser.getQNumber().equals(ldapUser.getQNumber())) {
            Boolean change = false;
            if (!currentUser.getNachname().equals(ldapUser.getNachname())) {
                currentUser.setNachname(ldapUser.getNachname());
                change = true;
            }
            if (!currentUser.getVorname().equals(ldapUser.getVorname())) {
                currentUser.setVorname(ldapUser.getVorname());
                change = true;
            }
            if (!currentUser.getAbteilung().equals(ldapUser.getAbteilung())) {
                currentUser.setAbteilung(ldapUser.getAbteilung());
                change = true;
            }
            if (change) {
                saveMitarbeiter(currentUser);
            }
        }
        return currentUser;
    }

    private void loadAndPersistLocalUsersFromExcelSheet() {
        try {
            XSSFWorkbook wb = ExcelUtils.createXSSFWorkbookForFile(ConfigService.getExcelConfigFile());
            List<List<String>> rows = ExcelUtils.loadRowsBySheet(1, "Testnutzer", wb);

            for (List<String> row : rows) {
                LocalUser lu = new LocalUser();
                lu.setQnumber(row.get(0));
                lu.setPassword(SecurityUtils.hashStringSha256(row.get(6)));
                saveLocalUser(lu);
                String[] gruppen = row.get(8).split(";");
                for (String lg : gruppen) {
                    if (!lg.trim().isEmpty()) {
                        LocalGroup g = new LocalGroup(lg.trim());
                        if (!isExistsGruppe(g)) {
                            saveGruppe(g);
                        } else {
                            g = getGruppenByName(g.getGruppenname()).get(0);
                        }
                        g.addMitglied(lu);
                        saveGruppe(g);
                    }
                }
            }
        } catch (ExcelInputFileException ex) {
            LOG.error(null, ex);
        }
    }

    private void registerDummyUsers(Mitarbeiter mitarbeiter) {
        try {
            Mitarbeiter m = uss.getMitarbeiterFromLdap(mitarbeiter.getQNumber());
            if (m != null && !uss.isExistsMitarbeiter(mitarbeiter.getQNumber())) {
                LOG.info("Register Mitarbeiter {}", m.toString());
                mitarbeiterDao.registerNewMitarbeiter(m);
            } else {
                LOG.info("Register Mitarbeiter {}", mitarbeiter.toString());
                mitarbeiterDao.registerNewMitarbeiter(mitarbeiter);
            }
        } catch (UserNotFoundException ex) {
            LOG.error("Couldn't register qNumber {}; Reason: {}", new Object[]{mitarbeiter.getQNumber(), ex.getMessage()});
        }
    }

    public UserDefinedSearch getUserDefinedSeachById(Long id) {
        return userDefinedSearchDao.getUserDefinedSearchById(id);
    }

    public List<UserDefinedSearch> getUserDefinedSearchByMitarbeiter(Mitarbeiter ma) {
        return userDefinedSearchDao.getUserDefinedSearchByMitarbeiter(ma);
    }

    public List<UserDefinedSearch> getUserDefinedSearchByNameAndMitarbeiter(String udsName, Mitarbeiter ma) {
        return userDefinedSearchDao.getUserDefinedSearchByNameAndMitarbeiter(udsName, ma);
    }

    public void saveUserDefinedSearch(UserDefinedSearch uds) {
        userDefinedSearchDao.persistUserDefinedSearch(uds);
    }

    public void updateUserDefinedSearch(UserDefinedSearch uds) {
        userDefinedSearchDao.persistUserDefinedSearch(uds);
    }

    public void removeUserDefinedSearch(UserDefinedSearch uds) {
        userDefinedSearchDao.removeUserDefinedSearch(uds);
    }

    public List<Derivat> getUpdatedSelectedDerivateForMitarbeiter(Mitarbeiter mitarbeiter) {
        return mitarbeiterDao.getUpdatedSelectedDerivateForMitarbeiter(mitarbeiter);
    }

}
