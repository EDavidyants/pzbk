package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.ldap.LdapFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;

@Stateless
public class LDAPReaderService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(LDAPReaderService.class);

    public boolean isLdapAvailable() {
        return getContextFromEnv() != null;
    }

    public Mitarbeiter getCustomerData(String qNumber) {
        LOG.debug("Search for Data for User {}", qNumber);
        return readUserDataFromLDAP(qNumber);
    }

    public Collection<Mitarbeiter> searchUserInLdap(String vorname, String nachname, String abteilung) {

        final String ldapFilterQuery = LdapFilter
                .newInstance()
                .withGivenname(vorname)
                .withSurname(nachname)
                .withDepartmentNumber(abteilung)
                .getLdapFilterQuery();

        return searchInLdap(ldapFilterQuery);
    }

    public Mitarbeiter readUserDataFromLDAPByMail(String mail) {
        String searchfilter = "(mail=" + mail + ")";
        final Collection<Mitarbeiter> persons = searchInLdap(searchfilter);
        Mitarbeiter person = null;
        if (persons != null && !persons.isEmpty()) {
            person = persons.iterator().next();
        }
        return person;
    }

    private Mitarbeiter readUserDataFromLDAP(String qNumber) {
        String searchfilter = "(uid=" + qNumber + ")";
        final Collection<Mitarbeiter> persons = searchInLdap(searchfilter);
        Mitarbeiter person = null;
        if (persons != null && !persons.isEmpty()) {
            person = persons.iterator().next();
        }
        return person;
    }

    @SuppressWarnings("checkstyle:IllegalCatch")
    private Collection<Mitarbeiter> searchInLdap(String searchfilter) {

        Collection<Mitarbeiter> result = new HashSet<>();

        try {
            DirContext ctx = getContextFromEnv();
            if (ctx == null) {
                LOG.warn("Context from Env are null");
                return null;
            }
            LOG.debug("Context are successfully loaded");
            SearchControls ctls = new SearchControls();
            ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            NamingEnumeration answer = ctx.search("", searchfilter, ctls);

            if (answer.hasMore()) {
                while (answer.hasMore()) {
                    Mitarbeiter person = new Mitarbeiter();
                    SearchResult entry = (SearchResult) answer.next();
                    Attributes attrs = entry.getAttributes();

                    // fill data into user object
                    String qNumber = readAttribute(attrs.get("uid"));
                    person.setQNumber(qNumber);

                    String givenname = readAttribute(attrs.get("givenname"));
                    person.setVorname(givenname);

                    String surname = readAttribute(attrs.get("sn"));
                    person.setNachname(surname);

                    String tel = readAttribute(attrs.get("telephonenumber"));
                    person.setTel(tel);

                    String email = readAttribute(attrs.get("mail"));
                    String externalMail = readAttribute(attrs.get("externalMail"));
                    if ("".equals(email)) {
                        person.setEmail(externalMail);
                    } else {
                        person.setEmail(email);
                    }

                    String department = readAttribute(attrs.get("departmentnumber"));
                    person.setAbteilung(department);
                    if (qNumber != null && !"".equals(qNumber)) {
                        result.add(person);
                        LOG.debug("User Attributes for {}; {}; {}; {}; {}; {}", new Object[] {qNumber, givenname, surname, department, email, externalMail});
                    } else {
                        LOG.debug("User without qNumber found {}; {}; {}; {}; {}; {}", new Object[] {qNumber, givenname, surname, department, email, externalMail});
                    }
                }
                // liste der qnummer
                StringBuilder stringBuilder = new StringBuilder("Gefundene Mitarbeiter: ");
                result.stream().map(r -> r.getQNumber()).forEach(q -> stringBuilder.append(q).append("; "));
                LOG.info(stringBuilder.toString());
            } else {
                LOG.warn("Coud not find Ldap User {}", searchfilter);
            }

            ctx.close();

        } catch (NamingException exception) {
            LOG.error("Error while reading data From Ldap", exception);
            return null;
        } catch (Exception exception) {
            LOG.error("Error while reading data From Ldap", exception);
            return null;
        }
        return result;
    }

    private DirContext getContextFromEnv() {
        String ldapURLProtocol = System.getProperty("com.bmw.ldap.url.protocol");
        LOG.debug("Protocoll for LDAP Connection {}", ldapURLProtocol);
        String ldapURL = System.getProperty("com.bmw.ldap.url");
        LOG.debug("Url for LDAP Connection {}", ldapURL);
        String ldapUser = System.getProperty("com.bmw.ldap.principal");
        LOG.debug("Principal for LDAP Connection {}", ldapUser);
        String ldapUserPwd = System.getProperty("com.bmw.ldap.credentials");
        LOG.debug("Credentials for LDAP Connection successfully loaded");
        if (ldapURL == null || ldapUser == null || ldapUserPwd == null || ldapURL.isEmpty() || ldapUser.isEmpty() || ldapUserPwd.isEmpty()) {
            LOG.error("Propertys for LDAP not loaded.");
            return null;
        }

        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapURLProtocol + "://" + ldapURL);
        env.put(Context.SECURITY_PRINCIPAL, ldapUser);
        env.put(Context.SECURITY_CREDENTIALS, ldapUserPwd);

        DirContext dirContext = null;

        try {
            dirContext = new InitialDirContext(env);
            LOG.debug(dirContext.toString());
        } catch (NamingException exception) {
            LOG.warn("Error while accesing LDAP Context ENV", exception);
        }
        return dirContext;
    }

    private String readAttribute(Attribute attr) {
        if (attr != null) {
            try {
                return (String) attr.get();
            } catch (NamingException exception) {
                LOG.warn("Error while reading Attributes {}", exception);
                return "";
            }
        }
        LOG.warn("Attribute null");
        return "";

    }
}
