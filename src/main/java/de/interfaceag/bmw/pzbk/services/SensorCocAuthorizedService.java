package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.session.Session;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Stateless
@Named
public class SensorCocAuthorizedService implements Serializable {

    @Inject
    private BerechtigungService berechtigungService;

    @Inject
    private SensorCocService sensorCocService;

    @Inject
    private Session session;

    public boolean isUserAuthorizedToEditSensorCocPermission(Long sensorcocId) {
        Mitarbeiter user = session.getUser();
        if (session.hasRole(Rolle.SENSORCOCLEITER)) {
            List<SensorCoc> sensorCocsUserIsAuthorizedToEdit = berechtigungService.getSensorCocForMitarbeiter(user, Boolean.TRUE);
            return sensorCocService.getSensorCocIdsFromSensorCocList(sensorCocsUserIsAuthorizedToEdit).contains(sensorcocId);
        }
        return true;
    }

}
