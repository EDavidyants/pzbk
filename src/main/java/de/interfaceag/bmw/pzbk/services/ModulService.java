package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.ModulDao;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl
 */
@Stateless
public class ModulService implements Serializable {

    @Inject
    private ModulDao modulDao;

    public void persistModule(List<Modul> module) {
        modulDao.persistModule(module);
    }

    public void persistModulSeTeam(ModulSeTeam modulSeTeam) {
        modulDao.persistSeTeam(modulSeTeam);
    }

    public void persistModulKomponente(ModulKomponente modulKomponente) {
        modulDao.persistModulKomponente(modulKomponente);
    }

    public ModulSeTeam getModulSeTeamByName(String name) {
        return modulDao.getSeTeamByName(name);
    }

    public Modul getModulByName(String name) {
        return modulDao.getModulByName(name);
    }

    public List<Modul> getModuleByIdList(List<Long> id) {
        return modulDao.getModuleByIdList(id);
    }

    public ModulKomponente getModulKomponenteByPpg(String name) {
        return modulDao.getModulKomponenteByPpg(name);
    }

    public ModulKomponente getModulKomponenteById(Long id) {
        return modulDao.getModulKomponenteById(id);
    }

    public List<ModulSeTeam> getAllSeTeams() {
        return modulDao.getAllSeTeams();
    }

    public List<ModulSeTeam> getAllSeTeamsFetch() {
        return modulDao.getAllSeTeamsFetch();
    }

    public List<Modul> getAllModule() {
        return modulDao.getAllModule();
    }

    public ModulSeTeam getSeTeamByName(String name) {
        return modulDao.getSeTeamByName(name);
    }

    public List<ModulSeTeam> getSeTeamsByIdList(List<Long> idList) {
        return modulDao.getSeTeamsByIdList(idList);
    }

    public ModulSeTeam getSeTeamById(Long id) {
        return modulDao.getSeTeamById(id);
    }

    public List<ModulKomponente> getAllKomponenten() {
        return modulDao.getAllModulKomponenten();
    }

    public List<ModulKomponente> getModulKomponenteByName(String name) {
        return modulDao.getModulKomponenteByName(name);
    }

    public List<ModulSeTeam> getSeTeamsByNameList(List<String> nameList) {
        return modulDao.getSeTeamsByNameList(nameList);
    }

    public List<ModulSeTeam> getSeTeamsThatStartWith(String start, Modul modul) {
        return modulDao.getSeTeamsThatStartWith(start, modul);
    }

    public List<String> getAllFachbereiche() {
        return modulDao.getAllFachbereiche();
    }
}
