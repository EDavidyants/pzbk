package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class FileExportService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(FileExportService.class.getName());

    @Inject
    private ConfigService configService;
    @Inject
    private ExcelDownloadService excelDownloadService;

    public void downloadFileSystemExport() {
        Workbook workbook = generateFileSystemExport();
        excelDownloadService.downloadExcelExport("filesystem", workbook);
    }

    public Workbook generateFileSystemExport() {
        try (Workbook result = new XSSFWorkbook()) {
            Sheet sheet = result.createSheet("Export");

            List<String> fileList = getFileList(configService.getBasePath());

            Iterator<String> iterator = fileList.iterator();
            int index = 1;

            while (iterator.hasNext()) {
                String cellValue = iterator.next();
                Row row = sheet.createRow(index);
                row.createCell(0).setCellValue(cellValue);
                index++;
            }
            LOG.info("FileExportSheet generated");
            return result;
        } catch (IOException ex) {
            LOG.error("FileSzstemExport could not be generated", ex);
            return null;
        }
    }

    private static List<String> getFileList(String directory) {
        try (Stream<Path> pathStream = Files.walk(Paths.get(directory))) {
            return pathStream.filter(p -> !p.toFile().isDirectory()).map(Path::toString).collect(Collectors.toList());
        } catch (IOException ex) {
            LOG.warn(ex.toString());
        }
        return new ArrayList<>();
    }
}
