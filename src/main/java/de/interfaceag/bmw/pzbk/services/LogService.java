package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.LogEntryDao;
import de.interfaceag.bmw.pzbk.entities.LogEntry;
import de.interfaceag.bmw.pzbk.enums.LogLevel;
import de.interfaceag.bmw.pzbk.enums.SystemType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class LogService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(LogService.class.getName());


    @Inject
    private LogEntryDao logEntryDao;

    // --------- methods -------------------------------------------------------
    public void addLogEntry(LogLevel level, SystemType systemType, String value) {
        LogEntry entry = new LogEntry(level, systemType, value);
        saveLogEntry(entry);
    }

    /**
     * Diese Methode dient zum Loggen in der gesamten Anwendung. Dabei wir ein
     * Log Entry erstellt der in der Admin Ansicht mit angezigt wird.
     * Gleichzeitig wird ein Logger erzeugt der zu dem Passenden Level auch Log
     * einträge erstellt.
     *
     * @param level Mögliche Level ALL, CONFIG, ERROR, FINE, INFO, SEVERE,
     * WARNING
     * @param systemType Bei dem SystemType handelt es sich um eine
     * Anzeigerweiterung im Frontend. Um Log einträger schneller etwas zuordnen
     * zu können. Wenn man den Logger Benutzt sollte man sich für die
     * verschiedenen Aufgabenbereichen einen SystemType generieren.
     * @param message Nachricht für den Log
     * @param className Für die erstellung des Loggers
     */
    public void logWithLogEntry(LogLevel level, SystemType systemType, String message, String className) {
        Logger logger = LoggerFactory.getLogger(className);
        saveLogEntry(new LogEntry(level, systemType, message));

        Optional<LogLevel> loglevelMap = LogLevel.getByBezeichnung(level.getBezeichnung());

        if (loglevelMap.isPresent()) {
            switch (loglevelMap.get()) {
                case ALL:
                case CONFIG:
                case INFO:
                    logger.info(message);
                    break;
                case ERROR:
                    logger.error("ERROR: {}", message);
                    break;
                case FINE:
                    logger.debug(message);
                    break;
                case SEVERE:
                    logger.error(message);
                    break;
                case WARNING:
                    logger.warn(message);
                    break;
                default:

            }
        }
    }

    public void logWithLogEntryDefaultSystemType(LogLevel level, String message, String className) {
        logWithLogEntry(level, SystemType.ALL, message, className);
    }

    /**
     * Diese Methode dient zum Loggen in der gesamten Anwendung. Dabei wir ein
     * Log Entry erstellt der in der Admin Ansicht mit angezigt wird.
     * Gleichzeitig wird ein Logger erzeugt der zu dem Passenden Level auch Log
     * einträge erstellt.
     *
     * @param level Mögliche Level ALL, CONFIG, ERROR, FINE, INFO, SEVERE,
     * WARNING
     * @param systemType Bei dem SystemType handelt es sich um eine
     * Anzeigerweiterung im Frontend. Um Log einträger schneller etwas zuordnen
     * zu können. Wenn man den Logger Benutzt sollte man sich für die
     * verschiedenen Aufgabenbereichen einen SystemType generieren.
     * @param message Nachricht für den Log
     * @param className Für die erstellung des Loggers
     * @param exception Die Exception um den Stacktrace mit auszugeben
     */
    public void logWithLogEntry(LogLevel level, SystemType systemType, String message, String className, Object exception) {
        Logger logger = LoggerFactory.getLogger(className);
        LogEntry entry = new LogEntry(level, systemType, message);
        saveLogEntry(entry);

        Optional<LogLevel> loglevelMap = LogLevel.getByBezeichnung(level.getBezeichnung());

        if (loglevelMap.isPresent()) {
            switch (loglevelMap.get()) {
                case ALL:
                case CONFIG:
                case INFO:
                    logger.info(message, exception);
                    break;
                case ERROR:
                    logger.error("ERROR: " + message, exception);
                    break;
                case FINE:
                    logger.debug(message, exception);
                    break;
                case SEVERE:
                    logger.error(message, exception);
                    break;
                case WARNING:
                    logger.warn(message, exception);
                    break;
                default:

            }
        }
    }

    // --------- CRUD ----------------------------------------------------------
    private void saveLogEntry(LogEntry logEntry) {
        logEntryDao.saveLogEntry(logEntry);
    }

    // ---------- named queries ------------------------------------------------
    public List<LogEntry> getAllLogEntries() {
        return logEntryDao.getAllLogEntries();
    }

    public List<LogEntry> getAllLogEntriesForZAKSST() {
        return this.logEntryDao.getAllLogEntriesBySystemType(SystemType.ZAKSST);
    }

    public List<LogEntry> getZAKLogEntriesForTimeRange(Date startDate, Date endDate) {
        LOG.info("Search LogEntries for Time-Range {} bis {}", startDate, endDate);
        List<LogEntry> logEntriesForTimeRange = this.logEntryDao.getZAKLogEntriesForTimeRange(startDate, endDate);
        LOG.info("Found LogEntries {} Ergebnisse for Time-Range", logEntriesForTimeRange.size());
        return logEntriesForTimeRange;
    }
}
