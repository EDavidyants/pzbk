package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.DbFileDao;
import de.interfaceag.bmw.pzbk.entities.AbstractAnfoMgmtObject;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.DbFile;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.enums.ContentType;
import de.interfaceag.bmw.pzbk.excel.ExcelUtils;
import de.interfaceag.bmw.pzbk.shared.utils.ObjectTypeValidationUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class FileService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(FileService.class);

    @Inject
    private DbFileDao dbFileDao;

    @Inject
    private AnforderungService anforderungService;
    @Inject
    private ConfigService configService;

    public Anhang getAnhangById(Long id) {
        return dbFileDao.getAnhangById(id);
    }

    public void removeAnhang(Anhang anh) {
        dbFileDao.removeAnhang(anh);
    }

    public void removeAnhangWithId(Long id) {
        Anhang anhang = getAnhangById(id);
        if (anhang != null) {
            removeAnhang(anhang);
        }
    }

    public void saveAnhang(Anhang anhang) {
        dbFileDao.saveAnhang(anhang);
    }

    private ContentType getContentTypeToWriteAnhaengeTo(Object object) {
        if (ObjectTypeValidationUtils.isAnforderung(object)) {
            return ContentType.ANFORDERUNG;

        } else if (ObjectTypeValidationUtils.isMeldung(object)) {
            return ContentType.MELDUNG;

        } else if (ObjectTypeValidationUtils.isProzessbaukasten(object)) {
            return ContentType.PZBK;

        } else {
            return null;
        }
    }

    public void writeAnhaengeToFile(AbstractAnfoMgmtObject abstractAnfoMgmtObject) {
        if (configService.getWriteToFileSystemBoolean()) {
            Map<Anhang, Boolean> resultMap = new HashMap<>();

            ContentType contentType = getContentTypeToWriteAnhaengeTo(abstractAnfoMgmtObject);
            Long contentTypeId = abstractAnfoMgmtObject.getId();

            if (isContentTypeValid(contentType) && contentTypeId != null) {
                abstractAnfoMgmtObject.getAnhaenge().stream()
                        .filter(a -> a == null || a.getContent() == null || a.getContent().getFilePath() == null || a.getContent().getFilePath().isEmpty())
                        .forEach(a -> resultMap.put(a, writeAnhangToFile(a, contentType, contentTypeId)));
                resultMap.entrySet().stream().filter(e -> !e.getValue()).map(Map.Entry::getKey).forEach(a -> {
                    LOG.warn("Anhang {} konnte nicht geschrieben werden!", a);
                    abstractAnfoMgmtObject.removeAnhang(a);
                });
            }

        }
    }

    private static boolean isContentTypeValid(ContentType contentType) {
        return contentType != null;
    }

    public Boolean writeAnhangToFile(Anhang anhang, ContentType contentType, Long contentTypeId) {

        Boolean result = Boolean.FALSE;

        String relativeFilePath = buildTargetFilePath(contentTypeId, anhang.getDateiname(), contentType);

        if (isRelativeFilePathValid(relativeFilePath) && anhangContentExists(anhang)) {
            writeAnhangToFileSystem(anhang, relativeFilePath);
            result = Boolean.TRUE;

        } else {
            LOG.warn("Anhang {} wird nicht in FileSystem gespeichert!", anhang);
        }

        return result;
    }

    private static boolean isRelativeFilePathValid(String relativeFilePath) {
        return relativeFilePath != null && !relativeFilePath.isEmpty();
    }

    private static boolean anhangContentExists(Anhang anhang) {
        return anhang != null && anhang.getContent() != null && anhang.getContent().getContent() != null;
    }

    private void writeAnhangToFileSystem(Anhang anhang, String relativeFilePath) {
        String fullFilePath = configService.getBasePath() + relativeFilePath;
        File outputFile = new File(fullFilePath);
        outputFile.getParentFile().mkdirs();

        try {
            boolean createNewFile = outputFile.createNewFile();
            if (!createNewFile) {
                LOG.error("Failed to create File");
            }
        } catch (IOException ex) {
            LOG.error("Failed to create File", ex);
        }

        try (FileOutputStream output = new FileOutputStream(outputFile)) {
            output.write(anhang.getContent().getContent());
        } catch (IOException ex) {
            LOG.error("Failed to create FileOutputStream", ex);
        }

        anhang.getContent().setFilePath(relativeFilePath);
        anhang.getContent().setContent(null);
        saveAnhang(anhang);
    }

    private static boolean isInputForAnhangFilePathValid(Long id, ContentType contentType) {
        return isContentTypeValid(contentType) && id != null;
    }

    // more abstract path generation which is also applicable in the data migration
    private static String buildTargetFilePath(Long id, String filename, ContentType contentType) {
        if (!isInputForAnhangFilePathValid(id, contentType)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        String separator = FileSystems.getDefault().getSeparator();
        sb.append(separator);
        switch (contentType) {
            case ANFORDERUNG:
                sb.append("A").append(separator);
                break;
            case MELDUNG:
                sb.append("M").append(separator);
                break;
            case PZBK:
                sb.append("P").append(separator);
                break;
            default:
                return "";
        }
        sb.append(id).append(separator).append(filename);
        return sb.toString();
    }

    public byte[] readFileData(String relativeFilePath) {
        if (relativeFilePath != null && !relativeFilePath.isEmpty()) {
            String fullFilePath = configService.getBasePath() + relativeFilePath;
            try {
                return Files.readAllBytes(new File(fullFilePath).toPath());
            } catch (IOException ex) {
                LOG.error(null, ex);
            }
        } else {
            LOG.warn("{} ist kein g\u00fcltiger Dateipfad!", relativeFilePath);
        }
        return null;
    }

    @SuppressWarnings("checkstyle:IllegalCatch")
    public void migrateAnhaengeFromFileSystem(Sheet sheet) {
        try {
            Row row = sheet.getRow(0);
            if (row.getLastCellNum() > 0) {
                String basefolder = ExcelUtils.cellToString(row.getCell(0));
                copyAnhaengeFromBaseFolderToTargetFolder(basefolder);
            }
        } catch (Exception exception) {
            LOG.warn(exception.toString());
        }

    }

    private void copyAnhaengeFromBaseFolderToTargetFolder(String basefolder) {
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(basefolder))) {
            for (Path entry : directoryStream) {
                copyAnhaengeForPath(entry);
            }
        } catch (IOException ex) {
            LOG.error(ex.toString());
        }
    }

    private void copyAnhaengeForPath(Path entry) throws IOException {
        if (entry.toFile().isDirectory()) {
            String name = entry.getFileName().toString();
            List<Anforderung> anforderungen = anforderungService.fetchVersionDataForFachId("A" + name);
            List<Meldung> meldungen = anforderungService.getMeldungByFachIdStartingWith("M" + name);
            if (!anforderungen.isEmpty() || !meldungen.isEmpty()) {
                try (DirectoryStream<Path> innerDirectoryStream = Files.newDirectoryStream(entry)) {
                    for (Path file : innerDirectoryStream) {
                        if (file.toFile().isFile() && !file.endsWith("Thumbs.db")) {
                            anforderungen.forEach(a -> {
                                String relativePathAsString = buildTargetFilePath(a.getId(), file.getFileName().toString(), ContentType.ANFORDERUNG);
                                copyFile(relativePathAsString, file);
                                createAnhangForAnforderung(a, relativePathAsString, file);
                            });
                            meldungen.forEach(m -> {

                                String relativePathAsString = buildTargetFilePath(m.getId(), file.getFileName().toString(), ContentType.MELDUNG);
                                copyFile(relativePathAsString, file);
                                createAnhangForMeldung(m, relativePathAsString, file);
                            });
                        }
                    }
                }
            }
        }
    }

    public void createAnhangForAnforderung(Path path, Anforderung anforderung) {
        Anhang anhang = createAnhang(path);
        anforderung.addAnhang(anhang);
        anforderungService.saveAnforderung(anforderung);
    }

    public void createAnhangForMeldung(Path path, Meldung meldung) {
        Anhang anhang = createAnhang(path);
        meldung.addAnhang(anhang);
        anforderungService.saveMeldung(meldung);
    }

    private void createAnhangForAnforderung(Anforderung anforderung, String pathAsString, Path path) {
        Anhang anhang = createAnhang(pathAsString, path);
        anforderung.addAnhang(anhang);
        anforderungService.saveAnforderung(anforderung);
    }

    private void createAnhangForMeldung(Meldung meldung, String pathAsString, Path path) {
        Anhang anhang = createAnhang(pathAsString, path);
        meldung.addAnhang(anhang);
        anforderungService.saveMeldung(meldung);
    }

    private Anhang createAnhang(String relativeFilePathAsString, Path path) {
        try {
            String separator = FileSystems.getDefault().getSeparator();
            String fileName = relativeFilePathAsString.substring(relativeFilePathAsString.lastIndexOf(separator) + 1);
            String fileType = Files.probeContentType(path);
            DbFile dbFile = new DbFile(fileName, fileType, relativeFilePathAsString);
            Anhang anhang = new Anhang(fileName, dbFile);
            anhang.setDateityp(fileType);
            return anhang;
        } catch (IOException ex) {
            LOG.error(null, ex);
            return null;
        }
    }

    private Anhang createAnhang(Path path) {
        try {
            String fileName = path.getFileName().toString();
            String fileType = Files.probeContentType(path);
            String canonicalPath = reduceToRelativePath(path.toString());

            DbFile dbFile = new DbFile(fileName, fileType, canonicalPath);
            Anhang anhang = new Anhang(fileName, dbFile);
            anhang.setDateityp(fileType);
            return anhang;
        } catch (IOException ex) {
            LOG.error(null, ex);
            return null;
        }
    }

    protected static String reduceToRelativePath(String fullPath) {
        if (fullPath.contains("/A/")) {
            return fullPath.substring(fullPath.indexOf("/A/"));
        } else if (fullPath.contains("/M/")) {
            return fullPath.substring(fullPath.indexOf("/M/"));
        } else if (fullPath.contains("/P/")) {
            return fullPath.substring(fullPath.indexOf("/P/"));
        }
        return fullPath;
    }

    private void copyFile(String relativePathAsString, Path file) {
        try {
            String fullPathAsString = configService.getBasePath() + relativePathAsString;
            Path filePath = Paths.get(fullPathAsString);
            Files.createDirectories(filePath);
            Files.copy(file, filePath, REPLACE_EXISTING);
        } catch (IOException ex) {
            LOG.error(null, ex);
        }
    }

    public String getFolderContentAsString(Long id, ContentType contentType) {
        String pathAsString = configService.getBasePath() + buildTargetFilePath(id, "", contentType);
        Path dirPath = Paths.get(pathAsString);
        File dir = dirPath.toFile();
        if (dir != null && dir.exists()) {
            if (dir.isDirectory() && dir.listFiles() != null) {
                return "Gefundene Dateien im Verzeichnis " + pathAsString + ": " + Stream.of(dir.listFiles()).map(File::getName).collect(Collectors.joining(", "));
            }
            return "No File found at " + pathAsString;
        }
        return "Verzeichnis " + pathAsString + " nicht vorhanden!";
    }

    public Path getFilePathForFachIdFilename(Long id, ContentType contentType, String fileName) {
        String pathAsString = configService.getBasePath() + buildTargetFilePath(id, fileName, contentType);
        Path dirPath = Paths.get(pathAsString);
        File dir = dirPath.toFile();
        if (dir != null && dir.exists() && !dir.isDirectory()) {
            LOG.debug("Datei {} gefunden.", pathAsString);
        } else {
            LOG.warn("Datei {} nicht vorhanden!", pathAsString);
        }
        return dirPath;
    }

}
