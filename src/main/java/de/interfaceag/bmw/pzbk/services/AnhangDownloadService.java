package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.DbFile;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class AnhangDownloadService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AnhangDownloadService.class.getName());

    @Inject
    private FileService fileService;

    public StreamedContent downloadAnhang(Anhang anhang) {

        if (anhang == null) {
            LOG.error("Anhang is null.");
            return null;
        }

        DbFile dbFile = anhang.getContent();
        byte[] src = dbFile.getContent();
        if (src == null && dbFile.getFilePath() != null) { // file is not stored in the db
            src = fileService.readFileData(dbFile.getFilePath());
        }
        if (src != null) {
            ByteArrayInputStream byteStream = new ByteArrayInputStream(src);
            StreamedContent file = new DefaultStreamedContent(byteStream, dbFile.getType(), dbFile.getName());
            return file;
        }
        LOG.warn("Datei f\u00fcr Anhang {} nicht gefunden.", anhang);
        return null;
    }
}
