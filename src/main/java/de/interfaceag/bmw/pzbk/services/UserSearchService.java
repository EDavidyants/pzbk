package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.MitarbeiterDao;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.exceptions.UserNotFoundException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
@Stateless
@Named
public class UserSearchService implements Serializable {

    private static final String COULDN_T_LOAD_USER_FROM_LDAP = "Couldn't load user from ldap";
    @Inject
    private ConfigService configService;
    @Inject
    private LDAPReaderService ldap;
    @Inject
    private MitarbeiterDao mitarbeiterDao;

    public boolean isExistsMitarbeiter(String qNumber) {
        return mitarbeiterDao.getMitarbeiterByQnumber(qNumber).isPresent();
    }

    public Mitarbeiter getMitarbeiterByQnumber(String qNumber) {
        return mitarbeiterDao.getMitarbeiterByQnumber(qNumber).orElse(null);
    }

    public List<Mitarbeiter> getAllMitarbeiter() {
        return mitarbeiterDao.getAllMitarbeiter();
    }

    public List<String> getAllAbteilung() {
        return mitarbeiterDao.getAllAbteilung();
    }

    public Mitarbeiter getMitarbeiterFromLdapByMail(String mail) throws UserNotFoundException {
        Mitarbeiter m = null;
        if (Boolean.TRUE.equals(ldap.isLdapAvailable())) {
            m = ldap.readUserDataFromLDAPByMail(mail);
        } else if (configService.isDevelopmentEnvironment()) {
            Collection<Mitarbeiter> mitarbeiters = configService.loadMitarbeiterFromExcelSheet();
            for (Mitarbeiter tmpMitarbeiter : mitarbeiters) {
                if (tmpMitarbeiter.getEmail().equals(mail)) {
                    m = tmpMitarbeiter;
                    break;
                }
            }
        }
        if (m == null) {
            throw new UserNotFoundException(COULDN_T_LOAD_USER_FROM_LDAP);
        }
        return m;
    }

    public Mitarbeiter getMitarbeiterFromLdap(String qNumber) throws UserNotFoundException {
        Mitarbeiter mitarbeiter = null;
        if (Boolean.TRUE.equals(ldap.isLdapAvailable())) {
            mitarbeiter = ldap.getCustomerData(qNumber);
            if (mitarbeiter != null) {
                mitarbeiter.setQNumber(qNumber);
            }
        } else {
            // Wenn kein Ldap und im Status DEVELOPMENT dann nutze Testdaten
            if (Boolean.TRUE.equals(configService.isDevelopmentEnvironment())) {
                Collection<Mitarbeiter> mitarbeiters = configService.loadMitarbeiterFromExcelSheet();
                for (Mitarbeiter tmpMitarbeiter : mitarbeiters) {
                    if (tmpMitarbeiter.getQNumber().equals(qNumber)) {
                        mitarbeiter = tmpMitarbeiter;
                        break;
                    }
                }
            }
        }

        if (mitarbeiter == null) {
            throw new UserNotFoundException(COULDN_T_LOAD_USER_FROM_LDAP);
        }
        return mitarbeiter;
    }

    public Mitarbeiter getUniqueMitarbeiterById(Long id) {
        return mitarbeiterDao.getUniqueMitarbeiterById(id).orElse(null);
    }

    public List<Mitarbeiter> getMitarbeiterByCriteria(String vorname, String nachname, String abteilung) {
        return mitarbeiterDao.getMitarbeiterByCriteria(vorname, nachname, abteilung);
    }

    public List<Mitarbeiter> getMitarbeiterByMail(String email) {
        return mitarbeiterDao.getMitarbeiterByMail(email);
    }

    public List<Mitarbeiter> getMitarbeiterByVornameOrNachname(String query) {
        return mitarbeiterDao.getMitarbeiterByVornameOrNachname(query);
    }

    public List<Mitarbeiter> getMitarbeiterFromLdap(String vorname, String nachname, String abteilung) throws UserNotFoundException {
        Collection<Mitarbeiter> result = null;
        if (Boolean.TRUE.equals(ldap.isLdapAvailable())) {
            result = ldap.searchUserInLdap(vorname, nachname, abteilung);
        } else if (Boolean.TRUE.equals(configService.isDevelopmentEnvironment())) { // Wenn kein Ldap und im Status DEVELOPMENT dann nutze Testdaten
            result = getMitarbeiterByCriteria(vorname, nachname, abteilung);
        }

        if (result == null) {
            throw new UserNotFoundException(COULDN_T_LOAD_USER_FROM_LDAP);
        }

        return new ArrayList<>(result);
    }

}
