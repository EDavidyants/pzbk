package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.session.Session;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Stateless
@Named
public class TteamAuthorizedService implements Serializable {

    @Inject
    private BerechtigungService berechtigungService;

    @Inject
    private TteamService tteamService;

    @Inject
    private Session session;

    public boolean isUserAuthorizedToEditTteamPermission(Long tteamId) {
        if (session.hasRole(Rolle.T_TEAMLEITER)) {
            List<Tteam> tteamsUserIsAuthorizedToEdit = berechtigungService.getTteamsForMitarbeiter(session.getUser());
            return tteamService.getTteamIdsByTteamList(tteamsUserIsAuthorizedToEdit).contains(tteamId);
        }
        return true;
    }

    public boolean isUserAuthorizedToEditTteamPermission(Tteam tteam) {
        if (session.hasRole(Rolle.T_TEAMLEITER)) {
            List<Tteam> tteamsUserIsAuthorizedToEdit = berechtigungService.getTteamsForMitarbeiter(session.getUser());
            return tteamsUserIsAuthorizedToEdit.contains(tteam);
        }
        return true;
    }
}
