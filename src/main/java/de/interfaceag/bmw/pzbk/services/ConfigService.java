package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.WertDao;
import de.interfaceag.bmw.pzbk.doors.migration.DoorsAnforderungMigrationService;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.UserToRole;
import de.interfaceag.bmw.pzbk.entities.Wert;
import de.interfaceag.bmw.pzbk.enums.ApplicationEnvironment;
import de.interfaceag.bmw.pzbk.enums.Attribut;
import de.interfaceag.bmw.pzbk.enums.ExcelSheetForProd;
import de.interfaceag.bmw.pzbk.enums.ExcelSheetForTechnikalIssus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.excel.ExcelExportService;
import de.interfaceag.bmw.pzbk.excel.ExcelUtils;
import de.interfaceag.bmw.pzbk.exceptions.ExcelInputFileException;
import de.interfaceag.bmw.pzbk.migration.AnforderungMeldungMigrationService;
import de.interfaceag.bmw.pzbk.migration.MigrationService;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author fp
 */
@Stateless
@Named
public class ConfigService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigService.class);

    private static String excelConfigPath = "/excelSheets/InputAttributeBMW.xlsx";
    public static final String EXCEL_WERK_PATH = "/excelSheets/InputWerk.xlsx";

    public static final String ANFOREDERUNG_MELDUNG_MIGRATION = "/excelSheets/AnforderungenMeldungen.xlsx";

    @Inject
    private WertDao wertDao;

    @Inject
    private AnforderungService anforderungService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private DerivatService derivateService;
    @Inject
    private InfoTextService infoTextService;
    @Inject
    private FileService fileService;
    @Inject
    private MigrationService migrationService;
    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private TteamService tteamService;
    @Inject
    private AnforderungMeldungMigrationService anforderungMeldungMigrationService;
    @Inject
    private ExcelExportService excelExportService;
    @Inject
    private WerkService werkService;
    @Inject
    private DoorsAnforderungMigrationService doorsAnforderungMigrationService;

    private Properties settings;

    private Properties versionSettings;

    private Sheet sensorCocTteamSheet;

    // ------------  init ------------------------------------------------------
    @PostConstruct
    public void init() {
        InputStream istr = null;
        try {
            istr = ConfigService.class.getResourceAsStream("/de/interfaceag/settings.properties");
            settings = new Properties();
            settings.load(istr);
        } catch (IOException ex) {
            LOG.error(null, ex);
        }

        try (InputStream input = ConfigService.class.getResourceAsStream("/version.properties")) {
            versionSettings = new Properties();
            versionSettings.load(input);
        } catch (IOException exception) {
            LOG.error(null, exception);
        }
        try {
            if (istr != null) {
                istr.close();
            }
        } catch (IOException ex) {
            LOG.error(null, ex);
        }

        if (!isProductionEnvironment()) {
            excelConfigPath = "/excelSheets/InputAttributeIF.xlsx";
        }
    } // end of init

    // ------------  methods ---------------------------------------------------
    public void migrateZipFile(InputStream inputStream) {
        try {
            migrationService.migrateModulBilder(inputStream);
        } catch (IOException ex) {
            LOG.error(null, ex);
        }
    }

    public void migrateAnhaenge(InputStream inputStream) {
        try {
            migrationService.migrateAnhaenge(inputStream);
        } catch (IOException ex) {
            LOG.error(null, ex);
        }
    }

    public void insertFromExcel(Workbook wb) {
        Iterator<Sheet> iterSheet = wb.sheetIterator();
        while (iterSheet.hasNext()) {
            Sheet sheet = iterSheet.next();
            insertFromExcel(sheet, null);
        }
    }

    public Tuple<Boolean, Workbook> insertFromExcel(Sheet sheet, Workbook resultWorkbook) {
        String sheetName = sheet.getSheetName();
        Optional<ExcelSheetForProd> excelSheet = ExcelSheetForProd.getByBezeichnung(sheetName);

        if (excelSheet.isPresent()) {
            switch (excelSheet.get()) {
                case SENSORCOC:
                    this.insertSensorCocsFromExcel(sheet);
                    break;
                case FAHRZEUGTYPEN:
                    this.persistFestgestelltIn(sheet);
                    break;
                case AUSWIRKUNGEN:
                    this.persistAuswirkungenFromExcelSheet(sheet);
                    break;
                case INFORMATIONSTEXTE:
                    infoTextService.persistInfoTexteFromExcelSheet(sheet);
                    break;
                case PRODUKTLINIEN:
                    this.persistProduktlinienFromExelSheet(sheet);
                    break;
                case UMSETZERLISTE:
                    migrationService.persistUmsetzerlisteFromExcelSheet(sheet);
                    break;
                case TTEAM:
                    tteamService.checkAndPersistNewTteamsFromExcelSheet(sheet);
                    break;
                case WERK:
                    werkService.loadNewWerkFromExcelSheet(sheet);
                    break;
                case DOORS:
                    resultWorkbook = doorsAnforderungMigrationService.migrateDoorsAnforderungen(sheet);
                    break;
                default:
                    return new GenericTuple<>(false, resultWorkbook);

            }
        }

        Optional<ExcelSheetForTechnikalIssus> excelSheetTechnikal = ExcelSheetForTechnikalIssus.getByBezeichnung(sheetName);
        if (excelSheetTechnikal.isPresent()) {
            switch (excelSheetTechnikal.get()) {
                case BERECHTIGUNGEN:
                    berechtigungService.loadBerechtigungenFromExcelSheet(sheet);
                    break;
                case ROLLEN:
                    break;
                case TESTNUTZER:
                    break;
                case DERIVATE:
                    derivateService.persistDerivateFromExcelSheet(sheet);
                    break;
                case DATENMIGRATION:
                    migrationService.persistDatenmigrationFromExcelSheet(sheet, sensorCocTteamSheet, resultWorkbook);
                    break;
                case SENSORCOCTTEAM:
                    break;
                case ANFORDERUNGSHISTORIE:
                    migrationService.persistAnforderungshistorieDatenFromExcelSheet(sheet);
                    break;
                case ZAKUEBERTRAGUNG:
                    migrationService.persistZakUebertragungDatenFromExcelSheet(sheet);
                    break;
                case LINKANFORDERUNGMELDUNG:
                    migrationService.linkMeldungenToAnforderung(sheet);
                    break;
                case DERIVATANFORDERUNGMODUL:
                    migrationService.generateAnforderungDerivatZuordnung(sheet);
                    break;
                case ANHAENGE:
                    fileService.migrateAnhaengeFromFileSystem(sheet);
                    break;
                case MIGRATIONSDATUM:
                    migrationService.migrateDateValuesForAnforderungenAndMeldungen(sheet, resultWorkbook);
                    break;
                case WERKMIGRATION:
                    migrationService.migrationOldAnforderungAndMeldungAddWerkFromExcelSheet(sheet);
                    break;
                default:
                    return new GenericTuple<>(false, resultWorkbook);

            }
        }

        return new GenericTuple<>(true, resultWorkbook);
    }

    public void insertSensorCocsFromExcel(Sheet sheet) {
        LOG.info("insert SensorCoC data into database");
        DataFormatter fmt = new DataFormatter();
        String ortung = "";
        String zakEinordnung = "";
        String ressort;
        String technologie;

        for (int rn = sheet.getFirstRowNum(); rn <= sheet.getLastRowNum(); rn++) {
            Row row = sheet.getRow(rn);
            if (row != null) {
                if (row.getCell(0) != null && row.getCell(2) != null) {
                    ortung = fmt.formatCellValue(row.getCell(0));
                    zakEinordnung = fmt.formatCellValue(row.getCell(2));
                } else if (row.getCell(0) != null) {
                    ortung = fmt.formatCellValue(row.getCell(0));
                    zakEinordnung = "";
                } else if (row.getCell(0) == null && row.getCell(1) != null) {
                    ressort = "T-Ressort";
                    technologie = fmt.formatCellValue(row.getCell(1));
                    List<SensorCoc> slist = sensorCocService.getSensorCocsByRessortTechnologieOrtung(ressort, ortung, technologie);
                    SensorCoc s = null;
                    if (slist != null && !slist.isEmpty()) {
                        s = slist.get(0);
                    }

                    // new SensorCoc is found
                    if (s == null) {
                        s = new SensorCoc();
                        s.setRessort("T-Ressort");
                        s.setOrtung(ortung);
                        s.setTechnologie(technologie);
                        s.setZakEinordnung(zakEinordnung);
                        sensorCocService.persistSensorCoc(s);
                    }

                } // next SensorCoc
            } // row is not null
        } // iterating through rows in the sheet
    }

    private List<String> loadFahrzeugeFromExcelSheet(Sheet sheet) {
        return ExcelUtils.loadColumn(0, 0, sheet);
    }

    private List<String> loadAuswirkungenFromExcelSheet(Sheet sheet) {
        return ExcelUtils.loadColumn(0, 0, sheet);
    }

    private List<String> loadProduktlinienFromExcelSheet(Sheet sheet) {
        return ExcelUtils.loadColumn(0, 0, sheet);
    }

    // ---------- save data -------------------------------------------------
    public void persistWert(Wert wert) {
        Attribut attribut = wert.getAttribut();
        // case mail template
        if (attribut.equals(Attribut.MAILTEMPLATE_STATUS_CHANGED) || attribut.equals(Attribut.MAILTEMPLATE_BEWERTUNGSAUFTRAGS_VORLAGE)
                || attribut.equals(Attribut.MAILTEMPLATE_REMINDER_VORLAGE) || attribut.equals(Attribut.MAILTEMPLATE_LASTCALL_VORLAGE)
                || attribut.equals(Attribut.MAILTEMPLATE_MAIL_TO_FTS)) {
            List<Wert> werteList = getWertByAttribut(wert.getAttribut());
            if (werteList != null && werteList.size() == 1) { // update current wert if necessary
                Wert currentWert = werteList.get(0);
                if (!currentWert.getWert().equals(wert.getWert())) {
                    currentWert.setWert(wert.getWert());
                    wertDao.persistWert(currentWert);
                }
            } else if (werteList != null && werteList.size() > 1) {
                werteList.forEach(w -> removeWert(w));
                wertDao.persistWert(wert);
            } else {
                wertDao.persistWert(wert);
            }
        } else { // all other attributes
            List<Wert> werteList = wertDao.getWertByAttributAndWert(wert.getAttribut(), wert.getWert());
            if (werteList.isEmpty()) {
                wertDao.persistWert(wert);
            }
        }
    }

    public void removeWert(Wert wert) {
        wertDao.removeWert(wert);
    }

    public void persistFestgestelltIn(Sheet sheet) {
        List<String> fahrzeugtypen = loadFahrzeugeFromExcelSheet(sheet);
        List<String> existingFestgestelltIn = anforderungService.getAllFestgestelltIn()
                .stream().map(fi -> fi.getWert()).collect(Collectors.toList());
        fahrzeugtypen.removeAll(existingFestgestelltIn);
        fahrzeugtypen.forEach(wert ->
                anforderungService.persistFestgestelltIn(new FestgestelltIn(wert))
        );
    }

    public void persistAuswirkungenFromExcelSheet(Sheet sheet) {
        List<String> auswirkungen = loadAuswirkungenFromExcelSheet(sheet);
        auswirkungen.forEach((tmp) -> {
            persistWert(new Wert(Attribut.AUSWIRKUNG, tmp));
        });
    }

    public void persistProduktlinienFromExelSheet(Sheet sheet) {
        List<String> produktlinien = loadProduktlinienFromExcelSheet(sheet);
        produktlinien.forEach((tmp) -> {
            persistWert(new Wert(Attribut.PRODUKTLINIE, tmp));
        });
    }

    public List<String> getAllProduklinienAsStringList() {
        List<String> result = this.getWertStringByAttribut(Attribut.PRODUKTLINIE);
        Collections.sort(result);
        return result;
    }

    public List<Wert> getAllAuswirkungen() {
        return getWertByAttribut(Attribut.AUSWIRKUNG);
    }

    public List<Attribut> getAllAttribute() {
        return Attribut.getAllAttribute();
    }

    public List<String> getWertStringByAttribut(Attribut attribut) {
        return wertDao.getWertStringByAttribut(attribut);
    }

    private static File getExcelConfigFile(String filePath) {
        InputStream input = ConfigService.class.getResourceAsStream(filePath);
        if (input == null) {
            LOG.warn("File {} does not exist", filePath);
        }
        File file = null;
        try {
            file = File.createTempFile("/myExcelFile", ".tmp");
            try (FileOutputStream output = new FileOutputStream(file)) {
                if (input != null) {
                    int c;
                    while ((c = input.read()) > -1) {
                        output.write(c);
                    }
                }
            }
            file.deleteOnExit();
        } catch (IOException ex) {
            LOG.error(null, ex);
        }

        return file;
    }

    public static File getExcelConfigFile() {
        return getExcelConfigFile(excelConfigPath);
    }

    private ApplicationEnvironment getApplicationEnvironment() {
        final String applicationEnvironmentPropertyName = ApplicationEnvironment.getPropertyName();
        final String applicationEnvironmentProperty = settings.getProperty(applicationEnvironmentPropertyName);
        return ApplicationEnvironment.getByName(applicationEnvironmentProperty);
    }

    public boolean isSendEmailsActivated() {
        String result = settings.getProperty("sendMail").toLowerCase();
        return "on".equals(result);
    }

    public String getVersionFromProperty() {
        return versionSettings.getProperty("version");
    }

    public Boolean isProductionEnvironment() {
        return getApplicationEnvironment().isProduction();
    }

    public Boolean isDevelopmentEnvironment() {
        return getApplicationEnvironment().isDevelopment();
    }

    public String getUrlPrefix() {
        String result = System.getProperty("com.bmw.pzbk.urlprefix");
        if (result == null || result.isEmpty()) {
            LOG.warn("System Property 'com.bmw.pzbk.urlprefix' not found. Use settings.property fallback option!");
            return getApplicationUrl();
        }
        return "https://" + result;
    }

    private String getApplicationUrl() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request;
        String requestURL;
        if (context != null) {
            ExternalContext externalContext = context.getExternalContext();
            if (externalContext != null) {
                request = (HttpServletRequest) externalContext.getRequest();
                requestURL = request.getRequestURL().toString();
            } else {
                return settings.getProperty("urlprefix");
            }
        } else {
            return settings.getProperty("urlprefix");
        }

        if (requestURL.contains("integration")) {
            return "https://integration.if-lab.de/pzbk";
        } else if (requestURL.contains("localhost")) {
            return "http://localhost:8080/pzbk";
        } else {
            return settings.getProperty("urlprefix");
        }
    }

    public String getUserPassword() {
        String result = System.getProperty("com.bmw.pzbk.userpassword");
        if (result == null || result.isEmpty()) {
            LOG.error("System Property 'com.bmw.pzbk.userpassword' not found!");
            return "";
        }
        return result;
    }

    public String getZakUrl() {
        String result = System.getProperty("com.bmw.pzbk.zakurl");
        if (result == null || result.isEmpty()) {
            LOG.error("System Property 'com.bmw.pzbk.zakurl' not found!");
            return "https://zaktest.bmwgroup.net/zielkat/"; // for test use only
        }
        return "https://" + result;
    }

    public String getBasePath() {
        String result = System.getProperty("com.bmw.pzbk.basepath");
        if (result == null || result.isEmpty()) {
            LOG.error("System Property 'com.bmw.pzbk.basepath' not found!");
            // for test use only
            // change path for local testing
            return File.separator + "var" + File.separator + "pzbk";
        }
        return result;
    }

    public boolean getWriteToFileSystemBoolean() {
        String result = System.getProperty("com.bmw.pzbk.writetofilesystem");
        if (result == null || result.isEmpty()) {
            LOG.error("System Property 'com.bmw.pzbk.writetofilesystem' not found!");
            return Boolean.FALSE; // for test use only
        } else {
            return "true".equalsIgnoreCase(result);
        }
    }

    private Collection<Mitarbeiter> loadMitarbeiterFromExcelSheet(Sheet sheet) {
        List<List<String>> rows = ExcelUtils.loadRowsBySheet(1, sheet);
        Collection<Mitarbeiter> result = new ArrayList<>();
        String cellDelimiter = ";";
        rows.stream().map(row -> {
            Mitarbeiter mitarbeiter = new Mitarbeiter();
            mitarbeiter.setQNumber(row.get(0));
            mitarbeiter.setVorname(row.get(1));
            mitarbeiter.setNachname(row.get(2));
            mitarbeiter.setKurzzeichen(row.get(3));
            mitarbeiter.setTel(row.get(4));
            mitarbeiter.setEmail(row.get(5));
            mitarbeiter.setLocation("de");
            String[] rollen = row.get(7).split(cellDelimiter);
            for (String rolle : rollen) {
                Rolle r = Rolle.getRolleByName(rolle);
                if (r != null) {
                    UserToRole userToRole = new UserToRole(mitarbeiter.getQNumber(), r.getRolleRawString());
                    berechtigungService.persistNewRolleForUser(userToRole);
                }
            }
            mitarbeiter.setAbteilung(row.get(9));
            return mitarbeiter;
        }).forEachOrdered(result::add);
        return result;
    }

    public Collection<Mitarbeiter> loadMitarbeiterFromExcelSheet() {
        Sheet sheet = null;
        try (XSSFWorkbook wb = ExcelUtils.createXSSFWorkbookForFile(ConfigService.getExcelConfigFile())) {
            sheet = wb.getSheet("Testnutzer");
        } catch (ExcelInputFileException | IOException ex) {
            LOG.error(null, ex);
        }
        return loadMitarbeiterFromExcelSheet(sheet);
    }

    public List<Wert> getWertByAttribut(Attribut attribut) {
        return wertDao.getWertByAttribut(attribut);
    }

    public void setSensorCocTteamSheet(Sheet sensorCocTteamSheet) {
        this.sensorCocTteamSheet = sensorCocTteamSheet;
    }

    public void updateLastStatusChangeDateFromHistory() {
        migrationService.updateLastStatusChangeDateFromHistory();
    }

    public void updateLatestDerivatAnforderungModulHistory() {
        migrationService.updateLatestDerivatAnforderungModulHistory();
    }

    public void linkAnforderungenWithoutMeldungenWithMatchingMeldung() {
        anforderungMeldungMigrationService.linkMeldungenWithAnforderungenWithoutMeldungen();
    }

    public String linkAnforderungWithMeldung(String meldungId, String anforderungId) {
        return anforderungMeldungMigrationService.linkMeldungWithAnforderung(meldungId, anforderungId);
    }

    public String getAllMatchingMeldungenForAnforderungAsString(String anforderungId) {
        return anforderungMeldungMigrationService.getAllMatchingMeldungenForAnforderungAsString(anforderungId);
    }

    public String getAllAnforderungenWithoutMeldungAsString() {
        return anforderungMeldungMigrationService.getAllAnforderungenWithoutMeldungAsString();
    }

    public Workbook getExampleWithExistingData() {
        return excelExportService.getExampleWithExistingData();

    }

    public Date getReportingStartDate() {

        Calendar calendar = Calendar.getInstance();
        calendar.set(2018, 2, 1);
        Date startDate = calendar.getTime();

        List<Wert> wertList = getWertByAttribut(Attribut.REPORTING_START_DATE);
        if (wertList != null && !wertList.isEmpty()) {
            String startDateStr = wertList.get(0).getWert();
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            try {
                startDate = formatter.parse(startDateStr);
            } catch (ParseException ex) {
                LOG.error(null, ex);
            }
        }

        return startDate;
    }

    public Wert getReportingStartDateWert() {

        List<Wert> wertList = getWertByAttribut(Attribut.REPORTING_START_DATE);
        if (wertList != null && !wertList.isEmpty()) {
            return wertList.get(0);
        }
        return null;
    }

}
