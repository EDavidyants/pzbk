package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.InfoTextDao;
import de.interfaceag.bmw.pzbk.entities.InfoText;
import de.interfaceag.bmw.pzbk.excel.ExcelUtils;
import org.apache.poi.ss.usermodel.Sheet;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author ig
 */
@Stateless
@Named
public class InfoTextService implements Serializable {

    @Inject
    private InfoTextDao infoTextDao;

    public void persistInfoText(InfoText infoText) {
        infoTextDao.persistInfoText(infoText);
    }

    /**
     *
     * @param id id of requested InfoText
     * @return InfoText with ID id
     */
    public InfoText getInfoTextById(Long id) {
        return infoTextDao.getInfoTextById(id);
    }

    /**
     *
     * @param feldName id of requested InfoText
     * @return InfoText with name of field feldName
     */
    public Optional<InfoText> getInfoTextByFeldName(String feldName) {
        return Optional.ofNullable(infoTextDao.getInfoTextByFeldName(feldName));
    }

    public Collection<InfoText> getInfoTextsByKeys(Collection<String> keys) {
        return infoTextDao.getInfoTextByFeldNames(keys);
    }

    public List<InfoText> getAllInfoTexte() {
        return infoTextDao.getAllInfoTexte();
    }

    public void persistInfoTexteFromExcelSheet(Sheet sheet) {
        List<List<String>> rows = ExcelUtils.loadRowsBySheet(1, sheet);

        String feldname;
        InfoText infotext;

        for (List<String> row : rows) {
            feldname = row.get(0);
            infotext = infoTextDao.getInfoTextByFeldName(feldname);

            // new infotext is found
            if (infotext == null) {
                infotext = new InfoText();
                infotext.setFeldName(feldname);
                infotext.setBeschreibungsText(row.get(1));
                infotext.setFeldBeschreibungLang(row.get(2));
                persistInfoText(infotext);

            } // only unique values to be persisted
        } // iterate through rows
    }
}
