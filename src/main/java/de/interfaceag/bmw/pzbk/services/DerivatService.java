package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.DerivatDao;
import de.interfaceag.bmw.pzbk.dao.KovAPhaseImDerivatDao;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusChangeService;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.entities.comparator.DerivatComparator;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.TimerType;
import de.interfaceag.bmw.pzbk.excel.ExcelUtils;
import de.interfaceag.bmw.pzbk.timer.KovaPhaseImDerivatTimerService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * @author ig
 */
@Stateless(name = "I_DerivatService")
@Named
public class DerivatService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(DerivatService.class);

    @Inject
    private DerivatDao derivatDao;
    @Inject
    private KovAPhaseImDerivatDao kovAPhaseImDerivatDao;

    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private KovaPhaseImDerivatTimerService kovaPhaseImDerivatTimerService;
    @Inject
    private UserService userService;
    @Inject
    private UserSearchService userSearchService;
    @Inject
    private ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;
    @Inject
    private DerivatStatusChangeService derivatStatusChangeService;

    public void createNewDerivat(Derivat derivat) {
        List<KovAPhaseImDerivat> kovAPhasenImDerivat = new ArrayList<>();
        KovAPhase.getAllKovAPhasen().forEach(phase
                -> kovAPhasenImDerivat.add(new KovAPhaseImDerivat(derivat, phase)));
        derivat.setKovAPhasen(kovAPhasenImDerivat);
        persistDerivat(derivat);
    }

    public void persistDerivat(Derivat derivat) {
        derivatDao.persistDerivat(derivat);
    }

    public Derivat getDerivatById(Long id) {
        return derivatDao.getDerivatById(id);
    }

    public List<Derivat> getDerivatByIdList(List<Long> idList) {
        List<Derivat> derivate = derivatDao.getDerivateByIdList(idList);
        derivate.sort(new DerivatComparator());
        return derivate;
    }

    public Derivat getDerivatByName(String name) {
        return derivatDao.getDerivatByName(name);
    }

    public List<Derivat> getDerivateByNameList(List<String> names) {
        List<Derivat> derivate = derivatDao.getDerivateByNameList(names);
        derivate.sort(new DerivatComparator());
        return derivate;
    }

    public List<Derivat> getAllDerivate() {
        List<Derivat> derivate = derivatDao.getAllDerivate();
        derivate.sort(new DerivatComparator());
        return derivate;
    }

    public List<Long> getAllDerivatIds() {
        return derivatDao.getDerivatIds();
    }

    public List<Derivat> getAllDerivateWithZielvereinbarungFalse() {
        return derivatDao.getAllDerivateWithZielvereinbarungFalse();
    }

    public List<Derivat> getFilteredDerivate(String derivatName, String derivatProduktlinie) {
        return derivatDao.getFilteredDerivate(derivatName, derivatProduktlinie);
    }

    public List<Derivat> getAuthorizedDerivateForAnforderer(Mitarbeiter mitarbeiter) {
        List<Derivat> result = new ArrayList<>();
        Rolle rolle = Rolle.ANFORDERER;
        BerechtigungZiel fuer = BerechtigungZiel.DERIVAT;
        Rechttype rechttype = Rechttype.SCHREIBRECHT;

        List<String> deriIdAsStringList = berechtigungService.getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype(mitarbeiter, rolle, fuer, rechttype);
        Long deriId;
        for (String idstr : deriIdAsStringList) {
            deriId = Long.parseLong(idstr);
            Derivat deri = getDerivatById(deriId);
            if (deri != null && !result.contains(deri)) {
                result.add(deri);
            }
        }

        result.sort(new DerivatComparator());
        return result;
    }

    public List<Long> getAuthorizedDerivateIdsForAnforderer(Mitarbeiter mitarbeiter) {
        List<Long> result = new ArrayList<>();
        Rolle rolle = Rolle.ANFORDERER;
        BerechtigungZiel fuer = BerechtigungZiel.DERIVAT;
        Rechttype rechttype = Rechttype.SCHREIBRECHT;

        List<String> deriIdAsStringList = berechtigungService.getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype(mitarbeiter, rolle, fuer, rechttype);
        Long deriId;
        for (String idstr : deriIdAsStringList) {
            deriId = Long.parseLong(idstr);

            result.add(deriId);

        }
        return result;
    }

    public void clearKovAPhaseImDerivat(KovAPhaseImDerivat kova) {
        kova.setStartDate(null);
        kova.setEndDate(null);
        kova.setKovaStatus(KovAStatus.OFFEN);
        kovAPhaseImDerivatDao.persistKovAPhaseImDerivat(kova);
    }

    public void persistKovAPhaseImDerivat(KovAPhaseImDerivat kova) {
        KovAStatus kovaStatus = kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(kova.getId()).getKovaStatus();
        kova.setKovaStatus(kovaStatus);

        if (isUserInputValid(kova)) {
            LocalDate startOld = kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(kova.getId()).getStartDate();
            LocalDate endOld = kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(kova.getId()).getEndDate();
            kovAPhaseImDerivatDao.persistKovAPhaseImDerivat(kova);

            // Set Timer if needed
            LocalDate startCur = kova.getStartDate();
            LocalDate endCur = kova.getEndDate();
            if ((dateIsModified(startOld, startCur) || dateIsModified(endOld, endCur)) && isKonfiguriert(kova)) { // phase turned to be configured (first time set)
                kova.setKovaStatus(KovAStatus.KONFIGURIERT);
                kovAPhaseImDerivatDao.persistKovAPhaseImDerivat(kova);
            } // date (both dates) was set/modified
        } else {
            LOG.warn("Modifications on {} of Derivat {} were not persisted due to incorrect user input. ",
                    new Object[]{kova.getKovAPhase(), kova.getDerivat().getName()});
        }
    }

    public void persistDerivateFromExcelSheet(Sheet sheet) {
        List<List<String>> rows = ExcelUtils.loadRowsBySheet(1, sheet);
        for (List<String> row : rows) {
            if (derivatDao.getDerivatByName(row.get(0)) == null) {
                Derivat derivat = new Derivat();
                derivat.setName(row.get(0));
                if (row.size() > 1) {
                    derivat.setProduktlinie(row.get(1));
                }

                createNewDerivat(derivat);
            }
        }
    }

    public List<String> getDerivatNamenWithEndDatumOfPhase(KovAPhase phase, LocalDate endDatum) {
        return kovAPhaseImDerivatDao.getDerivatNamenWithEndDatumOfPhase(phase, endDatum);
    }

    private boolean dateIsModified(LocalDate old, LocalDate current) {
        boolean sucess = false;
        if (old == null && current == null) {
            return sucess;
        } else if (old == null || current == null) {
            sucess = true;
            return sucess;
        } else {
            if (!old.equals(current)) {
                sucess = true;
            }
        }
        return sucess;
    }

    private boolean isKonfiguriert(KovAPhaseImDerivat kova) {
        boolean sucess = false;

        KovAStatus kovaStatusOld = kova.getKovaStatus();
        LocalDate start = kova.getStartDate();
        LocalDate end = kova.getEndDate();

        if (kovaStatusOld.equals(KovAStatus.OFFEN) && start != null && end != null) {
            sucess = true;
        }

        return sucess;
    }

    private boolean isUserInputValid(KovAPhaseImDerivat kova) {
        LocalDate start = kova.getStartDate();
        LocalDate end = kova.getEndDate();
        return !end.isBefore(start);
    }

    public Map<TimerType, Integer> aktualisierePhasen() {
        return kovaPhaseImDerivatTimerService.updateKovaPhasenImDerivat();
    }

    public List<Derivat> getUserSelectedDerivate(Mitarbeiter user) {
        List<Derivat> result = new ArrayList<>();

        List<Derivat> selectedDerivate = userService.getUpdatedSelectedDerivateForMitarbeiter(user);

        if (selectedDerivate != null) {
            result.addAll(selectedDerivate);
        } else {
            result.addAll(getAllDerivate());
        }

        result.sort(new DerivatComparator());
        return result;
    }

    public void setUserSelectedDerivate(Mitarbeiter user, List<Derivat> derivate) {
        Set<Derivat> selectedDeri = new HashSet<>();
        selectedDeri.addAll(derivate);
        List<Derivat> deriList = new ArrayList<>();
        deriList.addAll(selectedDeri);
        user.setSelectedDerivate(deriList);
        userService.saveMitarbeiter(user);
    }

    public void clearFKConstraintForMitarbeiter(Derivat deri) {
        // update selectedzakderivate field in mitarbeiter for all mitarbeiter
        boolean toUpdate;
        boolean check = false;
        boolean check2 = false;
        boolean check3 = false;

        List<Mitarbeiter> allMitarbeiterList = userSearchService.getAllMitarbeiter();

        for (Mitarbeiter me : allMitarbeiterList) {
            toUpdate = false;

            List<Derivat> deriSelected = me.getSelectedDerivate();
            if (deriSelected != null && !deriSelected.isEmpty()) {
                check = deriSelected.remove(deri);
            }

            List<Derivat> deriSelectedZak = me.getSelectedZakDerivate();
            if (deriSelectedZak != null && !deriSelectedZak.isEmpty()) {
                check2 = deriSelectedZak.remove(deri);
            }

            if (check || check2 || check3) {
                toUpdate = true;
            }

            if (toUpdate) {
                me.setSelectedDerivate(deriSelected);
                me.setSelectedZakDerivate(deriSelectedZak);
                userService.saveMitarbeiter(me);
            }

        }

    }

    public Optional<KovAPhaseImDerivat> getKovAPhaseImDerivatById(Long kovAPhaseId) {
        return kovAPhaseImDerivatDao.getKovAPhaseImDerivatByIdFetchDerivat(kovAPhaseId);
    }

    public List<KovAPhaseImDerivat> getKovAPhasenImDerivatByDerivat(Derivat derivat) {
        return kovAPhaseImDerivatDao.getKovAPhasenImDerivatByDerivatFetch(derivat);
    }

    public KovAPhaseImDerivat getKovAPhaseImDerivatByDerivatAndPhase(Derivat derivat, KovAPhase kovAPhase) {
        return kovAPhaseImDerivatDao.getKovAPhaseImDerivatByDerivatAndPhase(derivat, kovAPhase);
    }

    public String findZuordnungenForDerivat(Derivat derivat) {
        List<ZuordnungAnforderungDerivat> zuordnungen = zuordnungAnforderungDerivatService.getZuordnungAnforderungDerivatForDerivat(derivat);

        String result = "";

        if (zuordnungen != null && !zuordnungen.isEmpty()) {
            Long anzahl = zuordnungen.stream()
                    .filter(z -> z.getDerivat().getId().equals(derivat.getId()))
                    .map(z -> z.getAnforderung().toString()).count();
            return anzahl.toString();
        }

        return result;
    }

    public List<Derivat> findAllExistingDerivate() {
        return derivatDao.findAllExistingDerivate();
    }

    public List<Derivat> getAllErstanlaeufer() {
        return derivatDao.getAllErstanlaeufer();
    }

    public void updateDerivatStatus(Derivat derivat, DerivatStatus newStatus) {
        derivatStatusChangeService.changeStatus(derivat, newStatus);
    }

    public List<Long> getDerivatIdsToDerivatAnforderungModulIds(Collection<Long> derivatAnforderungModulIds) {
        return derivatDao.getDerivatIdsToDerivatAnforderungModulIds(derivatAnforderungModulIds);
    }

}
