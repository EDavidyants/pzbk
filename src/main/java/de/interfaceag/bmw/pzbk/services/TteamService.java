package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.berechtigung.dao.TteamMitgliedDAO;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamVertreterBerechtigung;
import de.interfaceag.bmw.pzbk.dao.TteamDao;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.excel.ExcelUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
@Stateless
@Named
public class TteamService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(TteamService.class);

    @Inject
    private TteamDao tteamDao;
    @Inject
    private TteamMitgliedDAO tteamMitgliedDAO;
    @Inject
    private BerechtigungService berechtigungService;

    public List<Tteam> getAllTteams() {
        return tteamDao.getAllTteams();
    }

    public List<Tteam> getAllTteamsSortByName() {
        return tteamDao.getAllTteamsSortByName();
    }

    public void saveTteam(Tteam tt) {
        tteamDao.saveTteam(tt);
    }

    public List<Long> getTteamIdsforTteamMitgliedAndRechttype(Mitarbeiter mitarbeiter, Integer rechttype) {
        return tteamMitgliedDAO.getTteamIdsforTteamMitgliedAndRechttype(mitarbeiter, rechttype);
    }

    public List<Tteam> getTteamsByNameList(List<String> names) {
        return tteamDao.getTteamsByNameList(names);
    }

    public Tteam getTteamByName(String name) {
        return tteamDao.getTteamByName(name);
    }

    public List<Tteam> getTteamByTeamleiter(Mitarbeiter mitarbeiter) {
        List<Long> tteamIds = berechtigungService.getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype(mitarbeiter, Rolle.T_TEAMLEITER, BerechtigungZiel.TTEAM, Rechttype.SCHREIBRECHT)
                .stream().map(Long::parseLong).collect(Collectors.toList());
        Optional<TteamVertreterBerechtigung> tteamVertreterBerechtigung = berechtigungService.getTteamVertreterBerechtigungForUser(mitarbeiter);
        if (tteamVertreterBerechtigung.isPresent()) {
            tteamVertreterBerechtigung.get().getTteamIdsMitSchreibrechten().stream().filter((ids) -> (!tteamIds.contains(ids))).forEachOrdered((ids) -> {
                tteamIds.add(ids);
            });
        }
        return getTteamByIdList(tteamIds);
    }

    public Tteam getTteamById(Long id) {
        Tteam tteam = tteamDao.getTteamById(id);
        if (tteam != null) {
            berechtigungService.getTteamleiterOfTteam(tteam)
                    .ifPresent(tteam::setTeamleiter);
        }
        return tteam;
    }

    public List<Tteam> getTteamByIdList(List<Long> idList) {
        return tteamDao.getTteamsByIdList(idList);
    }

    public List<Long> getTteamIdsByTteamList(List<Tteam> tteams) {
        return tteams
                .stream()
                .map(Tteam::getId)
                .collect(Collectors.toList());
    }

    public void checkAndPersistNewTteamsFromExcelSheet(Sheet sheet) {
        List<List<String>> rows = ExcelUtils.loadRowsBySheet(1, sheet);
        List<String> newlyPersisted = new ArrayList<>();
        String tteamName;
        Tteam tteam;

        for (List<String> row : rows) {
            tteamName = row.get(0).trim();
            tteam = tteamDao.getTteamByName(tteamName);

            // new tteam is found: create new Tteam instance and save it
            if (tteam == null && !"".equals(tteamName)) {
                tteam = new Tteam(tteamName);
                saveTteam(tteam);
                newlyPersisted.add(tteamName);
            }
        }

        if (!newlyPersisted.isEmpty()) {
            LOG.info("{} neue T-Teams wurden in der Datenbank gespeichert:", newlyPersisted.size());
            for (String tn : newlyPersisted) {
                LOG.info("Tteam: {}", tn);
            }
        }
    }
}
