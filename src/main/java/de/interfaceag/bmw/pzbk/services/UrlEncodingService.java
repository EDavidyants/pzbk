package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.UrlEncodingDao;
import de.interfaceag.bmw.pzbk.entities.UrlEncoding;
import de.interfaceag.bmw.pzbk.enums.LogLevel;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.shared.utils.SecurityUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 *
 * @author sl (Stefan.Luchs@Interface-ag.de)
 */
@Stateless
@Named
public class UrlEncodingService implements Serializable {

    @Inject
    UrlEncodingDao urlEncodingDao;

    @Inject
    private LogService logService;

    // ---------- methods ------------------------------------------------------
    public UrlEncoding createNewUrlEncodingForPageAndParameter(Page page, String urlParameter) {
        String urlId = generateUrlId(page, urlParameter);
        UrlEncoding urlEncoding = getUrlEncodingByUrlId(urlId);
        if (urlEncoding == null) {
            if (urlParameter.length() < 10000) {
                urlEncoding = new UrlEncoding(page, urlId, urlParameter);
                persistUrlEncoding(urlEncoding);
            } else {
                urlEncoding = new UrlEncoding(page, "", "");
                logService.logWithLogEntryDefaultSystemType(LogLevel.SEVERE, "urlParamater is too long", UrlEncodingService.class.getName());
            }
        }
        return urlEncoding;
    }

    private static String generateUrlId(Page page, String urlParameter) {
        String s = Integer.toString(page.getId()) + urlParameter;
        return SecurityUtils.hashStringSha256(s);
    }

    // ---------- interface implementation -------------------------------------
    private void persistUrlEncoding(UrlEncoding urlEncoding) {
        urlEncodingDao.persistUrlEncoding(urlEncoding);
    }

    public UrlEncoding getUrlEncodingByUrlId(String urlId) {
        return urlEncodingDao.getUrlEncodingByUrlId(urlId);
    }

}
