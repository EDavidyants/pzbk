package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.SensorCocDao;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.session.Session;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
@Stateless
public class SensorCocService implements Serializable {

    @Inject
    private SensorCocDao sensorCocDao;

    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private Session session;

    public List<SensorCoc> getAllSortByTechnologie() {
        return sensorCocDao.getAllSensorCoCsSortByTechnologie();
    }

    public SensorCoc getSensorCocById(Long id) {
        SensorCoc sensorCoc = sensorCocDao.getSensorCocById(id);
        if (sensorCoc != null) {
            Optional<Mitarbeiter> sensorCoCLeiter = berechtigungService.getSensorCoCLeiterOfSensorCoc(sensorCoc);
            if (sensorCoCLeiter.isPresent()) {
                sensorCoc.setSensorCoCLeiter(sensorCoCLeiter.get());
            }
        }
        return sensorCoc;
    }

    public List<SensorCoc> getSensorCocsByIdList(List<Long> idList) {
        return sensorCocDao.getSensorCocsByIdList(idList);
    }

    public List<SensorCoc> getSensorCocByZakEinordnung(String zakEinordnung) {
        return sensorCocDao.getSensorCocByZakEinordnung(zakEinordnung);
    }

    public List<SensorCoc> getSensorCocsByTechnologie(String technologie) {
        return sensorCocDao.getSensorCocsByTechnologie(technologie);
    }

    public Collection<SensorCoc> getSensorCocsByOrtung(Collection<String> ortung) {
        return sensorCocDao.getSensorCocsByOrtung(ortung);
    }

    public SensorCoc getSensorCocByTechnologie(String technologie) {
        List<SensorCoc> result = sensorCocDao.getSensorCocsByTechnologie(technologie);
        return result.size() == 1 ? result.get(0) : null;
    }

    public List<SensorCoc> getSensorCocsByTechnologieList(List<String> technologieList) {
        return sensorCocDao.getSensorCocsByTechnologieList(technologieList);
    }

    public void persistSensorCoc(SensorCoc sensorCoc) {
        sensorCocDao.persistSensorCoc(sensorCoc);
    }

    public List<SensorCoc> getSensorCocsByRessortTechnologieOrtung(String ressort, String ortung, String technologie) {
        return sensorCocDao.getSensorCocsByRessortTechnologieOrtung(ressort, ortung, technologie);
    }

    public List<SensorCoc> getSensorCocsByRessortTechnologieOrtungByRole(String ressort, String ortung, String technologie) {
        List<SensorCoc> result;
        Mitarbeiter user = session.getUser();
        List<Rolle> userRoles = session.getUserPermissions().getRollen();
        if (userRoles.contains(Rolle.ADMIN)
                || userRoles.contains(Rolle.T_TEAMLEITER)
                || userRoles.contains(Rolle.TTEAM_VERTRETER)
                || userRoles.contains(Rolle.TTEAMMITGLIED)) {
            result = sensorCocDao.getSensorCocsByRessortTechnologieOrtung(ressort, ortung, technologie);
        } else { // not Admin
            List<Long> sensorcocidList = berechtigungService.getAuthorizedSensorCocForSensor(user);
            if (sensorcocidList.isEmpty()) {
                sensorcocidList.add(0L);
            }
            result = sensorCocDao.getSensorCocsByRessortTechnologieOrtungInList(ressort, ortung, technologie, sensorcocidList);
        }
        return result;
    }

    public List<String> getAllZakEinordnung() {
        List<String> zaks = sensorCocDao.getAllZakEinordnung();
        List<String> l = new ArrayList<>();
        zaks.stream()
                .filter(zak -> !l.contains(zak))
                .forEachOrdered(l::add);
        return l;
    }

    public List<Long> getAllSensorCocIds() {
        return sensorCocDao.getAllSensorCocIds();
    }

    public List<Long> getSensorCocIdsFromSensorCocList(List<SensorCoc> sensorCocs) {
        return sensorCocs.stream().map(SensorCoc::getSensorCocId).collect(Collectors.toList());
    }

}
