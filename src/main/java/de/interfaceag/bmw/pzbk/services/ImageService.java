package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.BildDao;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Bild;
import de.interfaceag.bmw.pzbk.exceptions.InvalidAnhangException;
import org.apache.poi.util.IOUtils;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.inject.Named;
import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

/**
 * @author Stefan Luchs sl
 */
@Stateless
@Named
public class ImageService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ImageService.class);

    @Inject
    private BildDao bildDao;

    @Inject
    private AnhangDownloadService anhangDownloadService;
    @Inject
    private FileService fileService;

    public byte[] getImageFromBildByObjectIdAndParameter(Long objectId, Integer parameter, String type) {
        switch (parameter) {
            case 1:
                return bildDao.getImageFromAnforderungBildByObjectId(objectId, false, type);
            case 2:
                return bildDao.getImageFromAnforderungBildByObjectId(objectId, true, type);
            default:
                return null;
        }
    }

    public byte[] getProzessbaukastenKonzeptbaumBild(String fachId, int version, Boolean largeImage) {
        return bildDao.getProzessbaukastenKonzeptbaumBild(fachId, version, largeImage);
    }

    public byte[] getProzessbaukastenGrafikUmfangBild(String fachId, int version, Boolean largeImage) {
        return bildDao.getProzessbaukastenGrafikUmfangBild(fachId, version, largeImage);
    }

    public byte[] getDefaultImageForModulByAnforderungIdAndParameter(Long anforderungId, Integer parameter) {
        switch (parameter) {
            case 1:
                return bildDao.getDefaultImageForModulByAnforderungId(anforderungId, false);
            case 2:
                return bildDao.getDefaultImageForModulByAnforderungId(anforderungId, true);
            default:
                return null;
        }
    }

    public byte[] loadDefaultImage() {
        try {
            InputStream input = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/images/draft/cadExample.jpg");
            BufferedImage image = ImageIO.read(input);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(toBufferedImage(image), "jpg", byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        } catch (IOException ex) {
            LOG.error(null, ex);
        }
        return null;
    }

    public Bild generateBildFromInputStream(InputStream inputStream, String fileType) {
        try {
            byte[] input = IOUtils.toByteArray(inputStream);
            byte[] thumbnail = resizeInputStreamImage(input, 90, fileType);
            byte[] normal = resizeInputStreamImage(input, 283, fileType);
            byte[] large = resizeInputStreamImage(input, 910, fileType);
            Bild bild = new Bild(thumbnail, normal, large);
            return bild;
        } catch (IOException ex) {
            LOG.error(null, ex);
        }
        return null;
    }

    public Bild generateBildFromAnhang(Anhang anhang) {
        try {
            byte[] thumbnail = resizeAnhangImage(anhang, 90);
            byte[] normal = resizeAnhangImage(anhang, 283);
            byte[] large = resizeAnhangImage(anhang, 910);
            if (thumbnail == null || normal == null || large == null) {
                throw new InvalidAnhangException("Anhang " + anhang + " is invalid");
            }
            Bild bild = new Bild(thumbnail, normal, large);
            return bild;
        } catch (IOException ex) {
            LOG.error(null, ex);
        } catch (InvalidAnhangException exception) {
            LOG.warn(null, exception);
        }
        return null;
    }

    private byte[] resizeInputStreamImage(byte[] input, Integer size, String fileType) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(input);
        BufferedImage image = ImageIO.read(bais);
        BufferedImage croppedImage = resizeAnhangImage(image, size);
        return getByteArrayFromBufferedImage(croppedImage, fileType);
    }

    private byte[] resizeAnhangImage(Anhang anhang, Integer size) throws IOException {
        BufferedImage image;
        if (anhang.getContent().getContent() != null) {
            image = ImageIO.read(new ByteArrayInputStream(anhang.getContent().getContent()));
        } else if (anhang.getContent().getFilePath() != null) {
            ByteArrayInputStream bais = new ByteArrayInputStream(fileService.readFileData(anhang.getContent().getFilePath()));
            try {
                image = ImageIO.read(bais);
            } catch (IOException exception) {
                LOG.error(exception.toString());
                return null;
            }
        } else {
            return null;
        }
        BufferedImage croppedImage = resizeAnhangImage(image, size);
        return getByteArrayFromBufferedImage(croppedImage, anhang.getDateityp());
    }

    private byte[] getByteArrayFromBufferedImage(BufferedImage image, String fileType) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        switch (fileType) {
            case "image/jpeg":
            case "jpeg":
                ImageIO.write(image, "jpeg", byteArrayOutputStream);
                break;
            case "image/png":
            case "png":
                ImageIO.write(image, "png", byteArrayOutputStream);
                break;
            case "jpg":
                ImageIO.write(image, "jpg", byteArrayOutputStream);
                break;
            default:
                break;
        }
        return byteArrayOutputStream.toByteArray();
    }

    private BufferedImage resizeAnhangImage(BufferedImage image, Integer size) throws IOException {
        int widthToScale;
        int heightToScale;
        if (image.getWidth() > image.getHeight()) {

            heightToScale = (int) (1.1 * size);
            widthToScale = (int) ((heightToScale * 1.0) / image.getHeight() * image.getWidth());
        } else {
            widthToScale = (int) (1.1 * size);
            heightToScale = (int) ((widthToScale * 1.0) / image.getWidth() * image.getHeight());
        }

        BufferedImage resizedImage = new BufferedImage(widthToScale, heightToScale, image.getType());
        Graphics2D g = resizedImage.createGraphics();
        g.setComposite(AlphaComposite.Src);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.drawImage(image, 0, 0, widthToScale, heightToScale, null);
        g.dispose();

        int x = (resizedImage.getWidth() - size) / 2;
        int y = (resizedImage.getHeight() - size) / 2;

        if (x < 0 || y < 0) {
            throw new IllegalArgumentException("Width of resized image is bigger than original image");
        }
        return resizedImage.getSubimage(x, y, size, size);
    }

    private static BufferedImage toBufferedImage(Image image) {
        if (image instanceof BufferedImage) {
            return (BufferedImage) image;
        }
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.drawImage(image, 0, 0, null);
        graphics2D.dispose();
        return bufferedImage;
    }

    public StreamedContent downloadAnhang(Anhang anhang) {
        return anhangDownloadService.downloadAnhang(anhang);
    }

}
