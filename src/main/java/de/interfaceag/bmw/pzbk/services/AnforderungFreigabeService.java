package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.AnforderungFreigabeBeiModuSeTeamDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl
 */
@Stateless
public class AnforderungFreigabeService implements Serializable {

    @Inject
    private AnforderungFreigabeBeiModuSeTeamDao anforderungFreigabeBeiModuSeTeamDao;

    public void persistAnforderungFreigabe(AnforderungFreigabeBeiModulSeTeam anforderungFreigabeBeiModulSeTeam) {
        anforderungFreigabeBeiModuSeTeamDao.persistAnforderungFreigabeBeiModul(anforderungFreigabeBeiModulSeTeam);
    }

    public AnforderungFreigabeBeiModulSeTeam getAnfoFreigabeByAnforderungAndModulSeTeam(Anforderung anforderung, ModulSeTeam modulSeTeam) {
        return anforderungFreigabeBeiModuSeTeamDao.getAnfoFreigabeByAnforderungAndModulSeTeam(anforderung, modulSeTeam);
    }

    public List<AnforderungFreigabeBeiModulSeTeam> getAnfoFreigabeByAnforderung(Anforderung anforderung) {
        return anforderungFreigabeBeiModuSeTeamDao.getAnfoFreigabeByAnforderung(anforderung);
    }

    public Integer countAllModulSeTeamsForAnforderung(Anforderung anforderung) {
        return anforderungFreigabeBeiModuSeTeamDao.countAllModulSeTeamsForAnforderung(anforderung);
    }

    public Integer countFreigegebeneModulSeTeamsForAnforderung(Anforderung anforderung) {
        return anforderungFreigabeBeiModuSeTeamDao.countFreigegebeneModulSeTeamsForAnforderung(anforderung);
    }

    public List<AnforderungFreigabeBeiModulSeTeam> getFreigegebeneModulSeTeamsForAnforderung(Anforderung anforderung) {
        return anforderungFreigabeBeiModuSeTeamDao.getFreigegebeneModulSeTeamsForAnforderung(anforderung);
    }

}
