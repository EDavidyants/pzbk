package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungZuordnungAdapter;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungVersionMenuViewData;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.AnforderungEditFahrzeugmerkmalSaveService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalData;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.folgeprozessttl.ZuordnungAnforderungDerivatDTO;
import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.dao.FestgestelltInDao;
import de.interfaceag.bmw.pzbk.entities.AbstractAnfoMgmtObject;
import de.interfaceag.bmw.pzbk.entities.AbstractAnforderung;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Berechtigung;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ReferenzSystemLink;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.ReferenzSystem;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.mail.MailService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ActiveIndex;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenTab;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.AnforderungReportingStatusTransitionAdapter;
import de.interfaceag.bmw.pzbk.reporting.statustransition.meldung.MeldungReportingStatusTransitionAdapter;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.ProzessbaukastenLinkData;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.primefaces.context.RequestContext;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuItem;
import org.primefaces.model.menu.MenuModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author fp
 */
@Stateless
public class AnforderungService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungService.class.getName());

    @Inject
    Session session;

    @Inject
    protected AnforderungDao anforderungDao;
    @Inject
    private FestgestelltInDao festgestelltInDao;

    @Inject
    private AnforderungFreigabeService anforderungFreigabeService;
    @Inject
    protected FileService fileService;
    @Inject
    private ModulService modulService;
    @Inject
    private DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    protected AnforderungMeldungHistoryService historyService;
    @Inject
    private ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;
    @Inject
    protected MailService mailService;
    @Inject
    private ImageService imageService;
    @Inject
    private LocalizationService localizationService;
    @Inject
    private TteamService tteamService;
    @Inject
    private AnforderungReportingStatusTransitionAdapter reportingStatusTransitionAdapter;
    @Inject
    private MeldungReportingStatusTransitionAdapter meldungReportingStatusTransitionAdapter;
    @Inject
    protected AnforderungReportingStatusTransitionAdapter anforderungReportingStatusTransitionAdapter;
    @Inject
    protected MeldungReportingStatusTransitionAdapter changeMeldungStatusService;
    @Inject
    private AnforderungEditFahrzeugmerkmalSaveService anforderungEditFahrzeugmerkmalSaveService;
    @Inject
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService anforderungModulSeTeamFahrzeugmerkmalAuspraegungService;

    @Inject
    private AnforderungMeldungZuordnungAdapter anforderungMeldungZuordnungAdapter;

    public void saveChangesToAnforderungAndResetFreigabe(Anforderung anforderung, Anhang anhangForBild, AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData) {
        anforderung.resetFreigabeBeiModulSeTeams();
        saveChangesToAnforderung(anforderung, anhangForBild, anforderungEditFahrzeugmerkmalData);
    }

    public Anforderung saveChangesToAnforderung(Anforderung anforderung, Anhang anhangForBild) {
        Mitarbeiter user = session.getUser();

        if (anforderung != null && user != null && isUserBerechtigtForAnforderung(anforderung.getId())) {
            updateStandardBild(anforderung, anhangForBild);
            anforderung.setAenderungsdatum(new Date());
            List<AnforderungFreigabeBeiModulSeTeam> anfoModuls = anforderung.getAnfoFreigabeBeiModul();
            anforderung.setAnfoFreigabeBeiModul(anfoModuls);
            if (anforderung.getId() != null) {
                historyService.writeHistoryForChangedValuesOfAnfoMgmtObject(user, anforderung, this.getAnforderungById(anforderung.getId()));
                this.saveAnforderung(anforderung);
            } else {
                this.saveAnforderung(anforderung);
                historyService.writeHistoryForNewAnfoMgmtObject(user, anforderung);
            }
            mailService.sendMailAnforderungChanged(anforderung, user);
            return anforderung;
        }
        return null;
    }

    public Anforderung saveChangesToAnforderung(Anforderung anforderung, Anhang anhangForBild, AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData) {
        final Anforderung returnValue = saveChangesToAnforderung(anforderung, anhangForBild);

        if (anforderung != null) {
            anforderungEditFahrzeugmerkmalSaveService.saveAnforderungFahrzeugmerkmalChanges(anforderung, anforderungEditFahrzeugmerkmalData);
        }

        return returnValue;
    }

    public List<Long> getTteamIdsforTteamMitgliedAndRechttype(Mitarbeiter mitarbeiter, Integer rechttype) {
        return tteamService.getTteamIdsforTteamMitgliedAndRechttype(mitarbeiter, rechttype);
    }

    public Anforderung saveAbgeleiteteNewAnforderung(Anforderung anforderungDraft, Meldung baseMeldung, String statusChangeKommentar, Anhang anhangForBild, AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData) {

        Mitarbeiter currentUser = session.getUser();
        if (isDataForAnforderungAbleitungValid(anforderungDraft, baseMeldung, currentUser)) {
            saveNewAbgeleiteteAnforderung(anforderungDraft, anhangForBild, baseMeldung, currentUser);
            saveChangesToBaseMeldung(baseMeldung, anforderungDraft, statusChangeKommentar, currentUser);

            mailService.sendMailAnforderungChanged(baseMeldung, currentUser);
            mailService.sendMailAnforderungChanged(anforderungDraft, currentUser);

            anforderungReportingStatusTransitionAdapter.createAnforderungFromMeldung(anforderungDraft);
            meldungReportingStatusTransitionAdapter.addMeldungToNewAnforderung(baseMeldung);

            anforderungEditFahrzeugmerkmalSaveService.saveAnforderungFahrzeugmerkmalChanges(anforderungDraft, anforderungEditFahrzeugmerkmalData);

            return anforderungDraft;
        }

        return null;
    }

    private static boolean isDataForAnforderungAbleitungValid(Anforderung anforderungDraft, Meldung baseMeldung, Mitarbeiter currentUser) {
        return anforderungDraft != null && baseMeldung != null && currentUser != null;
    }

    private void saveNewAbgeleiteteAnforderung(Anforderung anforderungDraft, Anhang anhangForBild, Meldung baseMeldung, Mitarbeiter currentUser) {
        updateStandardBild(anforderungDraft, anhangForBild);
        anforderungDraft.setAenderungsdatum(new Date());
        anforderungDraft.setZeitpunktStatusaenderung(new Date());
        List<AnforderungFreigabeBeiModulSeTeam> anfoModuls = anforderungDraft.getAnfoFreigabeBeiModul();
        anforderungDraft.setAnfoFreigabeBeiModul(anfoModuls);
        anforderungDraft.addMeldung(baseMeldung);
        this.saveAnforderung(anforderungDraft);
        historyService.writeHistoryForNewAnfoMgmtObject(currentUser, anforderungDraft);
        String historyKommentar = "Basis Meldung " + baseMeldung.getFachId() + " wurde zugeordnet";
        historyService.writeHistoryForChangedKeyValue(currentUser, anforderungDraft, historyKommentar, "", "meldungZuordnung");
    }

    private void saveChangesToBaseMeldung(Meldung baseMeldung, Anforderung anforderungDraft, String statusChangeKommentar, Mitarbeiter currentUser) {
        baseMeldung.setAssignedToAnforderung(true);
        baseMeldung.getAnforderung().add(anforderungDraft);
        baseMeldung.setStatus(Status.M_ZUGEORDNET);
        baseMeldung.setAenderungsdatum(new Date());
        baseMeldung.setZeitpunktStatusaenderung(new Date());
        baseMeldung.setStatusWechselKommentar(statusChangeKommentar == null ? "Status Aenderung" : statusChangeKommentar);
        historyService.writeHistoryForChangedValuesOfAnfoMgmtObject(currentUser, baseMeldung, this.getMeldungById(baseMeldung.getId()));
        this.saveMeldung(baseMeldung);
    }

    public Anforderung saveNewAnforderungOhneMeldung(Anforderung anforderungDraft, String statusChangeKommentar, Mitarbeiter user, Anhang anhangForBild) {

        if (anforderungDraft != null && user != null) {

            updateStandardBild(anforderungDraft, anhangForBild);

            anforderungDraft.setAenderungsdatum(new Date());
            anforderungDraft.setZeitpunktStatusaenderung(new Date());

            List<AnforderungFreigabeBeiModulSeTeam> anfoModuls = anforderungDraft.getAnfoFreigabeBeiModul();

            anforderungDraft.setAnfoFreigabeBeiModul(anfoModuls);

            this.saveAnforderung(anforderungDraft);

            historyService.writeHistoryForNewAnfoMgmtObject(user, anforderungDraft);

            mailService.sendMailAnforderungChanged(anforderungDraft, user);

            return anforderungDraft;
        }
        return null;
    }

    public Anforderung saveNewAnforderungOhneMeldungVersion(Anforderung baseAnforderungCopy, String versionKommentar, Mitarbeiter user, Anhang anhangForBild) {

        if (user != null) {

            updateStandardBild(baseAnforderungCopy, anhangForBild);

            baseAnforderungCopy.setAenderungsdatum(new Date());
            baseAnforderungCopy.setZeitpunktStatusaenderung(new Date());
            List<ReferenzSystemLink> referenzSystemLinks = copyReferenzSystemLink(baseAnforderungCopy.getReferenzSystemLinks());
            baseAnforderungCopy.setReferenzSystemLinks(referenzSystemLinks);
            saveAnforderung(baseAnforderungCopy);

            historyService.writeHistoryForNewAnfoMgmtObjectVersion(user, baseAnforderungCopy, (baseAnforderungCopy.getVersion() - 1),
                    baseAnforderungCopy.getVersion(), versionKommentar == null ? "neue Version der Anforderung" : versionKommentar);

            mailService.sendMailAnforderungChanged(baseAnforderungCopy, user);

            return baseAnforderungCopy;
        }
        return null;
    }

    private List<ReferenzSystemLink> copyReferenzSystemLink(List<ReferenzSystemLink> referenzSystemLinks) {
        List<ReferenzSystemLink> referenzSystemLinksCopy = new ArrayList<>();
        referenzSystemLinks.forEach((referenzSystemlink) -> {
            referenzSystemLinksCopy.add(new ReferenzSystemLink(ReferenzSystem.DOORS, referenzSystemlink.getReferenzsystemId()));
        });
        return referenzSystemLinksCopy;
    }

    public Anforderung saveNewAnforderungVersion(Anforderung baseAnforderungCopy, Long baseAnforderungId, String versionKommentar, Anhang anhangForBild, AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData) {
        Mitarbeiter user = session.getUser();
        if (baseAnforderungCopy != null && baseAnforderungId != null && user != null && isUserBerechtigtForAnforderung(baseAnforderungId)) {
            updateStandardBild(baseAnforderungCopy, anhangForBild);
            Map<ModulSeTeam, VereinbarungType> vereinbarungBeiModulMap = this.createVereinbarungMapForNewVersion(baseAnforderungCopy);
            baseAnforderungCopy.setAenderungsdatum(new Date());
            baseAnforderungCopy.setZeitpunktStatusaenderung(new Date());
            List<AnforderungFreigabeBeiModulSeTeam> anfoModuls = this.copyModuleZumFreigeben(baseAnforderungCopy, vereinbarungBeiModulMap);
            baseAnforderungCopy.setAnfoFreigabeBeiModul(anfoModuls);
            this.saveAnforderung(baseAnforderungCopy);

            reportingStatusTransitionAdapter.createAnforderungAsNewVersion(baseAnforderungCopy);

            historyService.writeHistoryForNewAnfoMgmtObjectVersion(user, baseAnforderungCopy, this.getAnforderungById(baseAnforderungId).getVersion(),
                    baseAnforderungCopy.getVersion(), versionKommentar == null ? "neue Version der Anforderung" : versionKommentar);

            mailService.sendMailAnforderungChanged(baseAnforderungCopy, user);

            anforderungEditFahrzeugmerkmalSaveService.saveAnforderungFahrzeugmerkmalChanges(baseAnforderungCopy, anforderungEditFahrzeugmerkmalData);

            return baseAnforderungCopy;
        }
        return null;
    }

    public boolean isUserBerechtigtForAnforderung(Long originalObjectId) {
        boolean berechtigt;

        Mitarbeiter currentUser = session.getUser();
        List<Rolle> mitarbeiterRollen = session.getUserPermissions().getRollen();

        if (originalObjectId != null) {
            berechtigt = this.isAnforderungForUserRole(originalObjectId, currentUser, mitarbeiterRollen);
            if (berechtigt == false && mitarbeiterRollen.contains(Rolle.TTEAMMITGLIED)) {
                berechtigt = checkForTteamMitglied(originalObjectId, session.getUser());
            }
        } else {
            berechtigt = true;
        }

        return berechtigt;
    }

    private boolean checkForTteamMitglied(Long originalObjectID, Mitarbeiter user) {
        List<Long> tteamIdsForWrite = getTteamIdsforTteamMitgliedAndRechttype(user, Rechttype.SCHREIBRECHT.getId());
        List<Long> tteamIdsForRead = getTteamIdsforTteamMitgliedAndRechttype(user, Rechttype.LESERECHT.getId());

        return tteamIdsForWrite.contains(getAnforderungById(originalObjectID).getTteam().getId())
                || tteamIdsForRead.contains(getAnforderungById(originalObjectID).getTteam().getId());
    }

    public Long saveAnforderung(Anforderung anforderung) {
        Long savedAnfoId = anforderungDao.saveAnforderung(anforderung);
        fileService.writeAnhaengeToFile(anforderung);
        return savedAnfoId;
    }

    public Long saveTestAnforderung(Anforderung anforderung) {
        Long savedAnfoId = anforderungDao.saveAnforderung(anforderung);
        return savedAnfoId;
    }

    public Meldung saveChangesToMeldung(Meldung meldung, String statusChangeKommentar, Mitarbeiter user, Anhang anhangForBild) {
        Long originalObjectId = meldung.getId();
        meldung.setAenderungsdatum(new Date());
        boolean isMeldungStatusAboutToChange = isMeldungStatusAboutToChange(meldung);
        if (isMeldungStatusAboutToChange) {
            changeMeldungStatusService.changeMeldungStatus(meldung, meldung.getStatus(), meldung.getNextStatus());
            meldung.setStatus(meldung.getNextStatus());
            meldung.setZeitpunktStatusaenderung(new Date());
            meldung.setStatusWechselKommentar(statusChangeKommentar);
        }

        updateStandardBild(meldung, anhangForBild);

        if (originalObjectId != null) {
            historyService.writeHistoryForChangedValuesOfAnfoMgmtObject(user, meldung, this.getMeldungById(originalObjectId));
            this.saveMeldung(meldung);

        } else {
            this.saveMeldung(meldung);
            historyService.writeHistoryForNewAnfoMgmtObject(user, meldung);
        }

        mailService.sendMailAnforderungChanged(meldung, user);
        return meldung;
    }

    private boolean isMeldungStatusAboutToChange(Meldung meldung) {
        if (meldung.getNextStatus() == null) {
            return false;
        }

        return meldung.getStatus() != meldung.getNextStatus();
    }

    public Meldung changeMeldungStatus(Meldung meldung, int newStatus, String statusChangeKommentar) {
        meldung.setNextStatus(Status.getStatusById(newStatus));
        meldung.setStatusChangeRequested(true);
        meldung.setAenderungsdatum(new Date());
        meldung.setZeitpunktStatusaenderung(new Date());
        switch (newStatus) {
            case 3: {
                meldung.setStatusWechselKommentar(statusChangeKommentar);
                this.saveMeldung(meldung);
                break;
            }
            case 1: {
                this.saveMeldung(meldung);
                break;
            }
            default:
                break;
        }

        return meldung;
    }

    public String restoreMeldung(Meldung meldung, Mitarbeiter user) {

        if (meldung == null || user == null) {
            return "meldung or user is null";
        }
        Status s = historyService.getLastStatusByObjectnameAnforderungId(meldung.getId(), meldung.getKennzeichen());
        if (s != null) {
            changeMeldungStatusService.changeMeldungStatus(meldung, meldung.getStatus(), s);
            meldung = restoreMeldungStatus(meldung, s);
            historyService.writeHistoryForChangedValuesOfAnfoMgmtObject(user, meldung, this.getMeldungById(meldung.getId()));
            this.saveMeldung(meldung);
            mailService.sendMailAnforderungChanged(meldung, user);
            return "";
        }
        return "Die Meldung kann nicht zurückgesetzt werden.";
    }

    public String restoreAnforderung(Anforderung anforderung, Mitarbeiter user) {

        if (anforderung == null || user == null) {
            return "anforderung or user is null";
        }

        Anforderung anforderungWorkCopy = this.getAnforderungWorkCopyByIdOrNewAnforderung(anforderung.getId());
        Status s = historyService.getLastStatusByObjectnameAnforderungId(anforderung.getId(), anforderung.getKennzeichen());

        if (s != null && s.equals(Status.A_KEINE_WEITERVERFOLG)) {
            return "Die Anforderung kann nicht zurückgesetzt werden, da sie schon im Status 'Keine Weiterverfolgung' war. Bitte eine neue Version erstellen.";
        }

        anforderung = restoreAnforderungStatus(anforderung);
        anforderungReportingStatusTransitionAdapter.changeAnforderungStatus(anforderung, Status.A_GELOESCHT, Status.A_INARBEIT);
        historyService.writeHistoryForChangedValuesOfAnfoMgmtObject(user, anforderung, anforderungWorkCopy);
        this.saveAnforderung(anforderung);
        for (Meldung meldung : anforderung.getMeldungen()) {
            if (meldung.getStatus() == Status.M_GELOESCHT) {
                restoreAndPersistMeldungStatus(meldung, Status.M_ZUGEORDNET, user);
            }
        }
        mailService.sendMailAnforderungChanged(anforderung, user);

        return "";
    }

    protected Anforderung restoreAnforderungStatus(Anforderung anforderung) {
        anforderung.setStatus(Status.A_INARBEIT);
        anforderung.setStatusWechselKommentar("Anforderung wurde wiederhergestellt");
        Date restoreDate = new Date();
        anforderung.setZeitpunktStatusaenderung(restoreDate);
        anforderung.setAenderungsdatum(restoreDate);

        anforderungReportingStatusTransitionAdapter.restoreAnforderung(anforderung);

        return anforderung;
    }

    protected static Meldung restoreMeldungStatus(Meldung meldung, Status status) {
        meldung.setStatus(status);
        meldung.setStatusWechselKommentar("Meldung wurde wiederhergestellt");
        Date restoreDate = new Date();
        meldung.setZeitpunktStatusaenderung(restoreDate);
        meldung.setAenderungsdatum(restoreDate);
        return meldung;
    }

    protected String restoreAndPersistMeldungStatus(Meldung meldung, Status status, Mitarbeiter user) {
        if (meldung == null || user == null) {
            return "meldung or user is null";
        }
        if (status != null) {
            meldung = restoreMeldungStatus(meldung, status);
            historyService.writeHistoryForChangedValuesOfAnfoMgmtObject(user, meldung, this.getMeldungById(meldung.getId()));
            this.saveMeldung(meldung);
            mailService.sendMailAnforderungChanged(meldung, user);
            return "";
        }
        return "Die Meldung kann nicht zurückgesetzt werden.";

    }

    public void clearEntityManager() {
        anforderungDao.clearEntityManager();
    }

    public void persistMeldung(Meldung meldung) {
        anforderungDao.persistMeldung(meldung);
    }

    public Long persistAnforderung(Anforderung anforderung) {
        return anforderungDao.persistAnforderung(anforderung);
    }

    public Long saveMeldung(Meldung meldung) {
        Long savedMeldungId = anforderungDao.saveMeldung(meldung);
        fileService.writeAnhaengeToFile(meldung);
        return savedMeldungId;
    }

    public Long saveTestMeldung(Meldung meldung) {
        Long savedMeldungId = anforderungDao.saveMeldung(meldung);
        return savedMeldungId;
    }

    public List<Anforderung> getAllAnforderung() {
        return anforderungDao.getAllAnforderung();
    }

    public boolean isMyMeldungByIdAndSensorCocList(Long id, List<Long> sensorcocList) {
        return anforderungDao.getMeldungByIdAndSensorCocList(id, sensorcocList) != null;
    }

    public boolean isMyMeldungByFachIdAndSensorCocList(String fachId, List<Long> sensorcocList) {
        return anforderungDao.getMeldungIdByFachIdAndSensorCocList(fachId, sensorcocList) != null;
    }

    public List<Meldung> getAllMeldungen() {
        return anforderungDao.getAllMeldungen();
    }

    public List<Anforderung> getAllAnforderungenWithThisMeldung(Long meldungId) {
        Meldung meldung = getMeldungById(meldungId);
        return anforderungDao.getAllAnforderungenWithThisMeldung(meldung);
    }

    private Anforderung modulSeTeamAblehnen(Mitarbeiter user, AnforderungFreigabeBeiModulSeTeam freigabeBeiModulSeTeam, String kommentar) {
        freigabeBeiModulSeTeam.setBearbeiter(user);
        freigabeBeiModulSeTeam.setAbgelehnt(true);
        freigabeBeiModulSeTeam.setKommentar(kommentar);
        freigabeBeiModulSeTeam.setDatum(new Date());
        anforderungFreigabeService.persistAnforderungFreigabe(freigabeBeiModulSeTeam);
        historyService.writeHistoryForAnforderungFreigabeBeiModulSeTeam(user, freigabeBeiModulSeTeam);
        return freigabeBeiModulSeTeam.getAnforderung();
    }

    private Anforderung anforderungForSelectedModulFreigeben(Mitarbeiter user, AnforderungFreigabeBeiModulSeTeam freigabeBeiModulSeTeam, String kommentar) {
        freigabeBeiModulSeTeam.setBearbeiter(user);
        freigabeBeiModulSeTeam.setFreigabe(true);
        if (kommentar == null) {
            freigabeBeiModulSeTeam.setKommentar("");
        } else {
            freigabeBeiModulSeTeam.setKommentar(kommentar);
        }

        freigabeBeiModulSeTeam.setDatum(new Date());
        freigabeBeiModulSeTeam.setAbgelehnt(false);
        anforderungFreigabeService.persistAnforderungFreigabe(freigabeBeiModulSeTeam);
        historyService.writeHistoryForAnforderungFreigabeBeiModulSeTeam(user, freigabeBeiModulSeTeam);
        return freigabeBeiModulSeTeam.getAnforderung();
    }

    public boolean isAuthorizedForModulSeTeam(ModulSeTeam modulSeTeam, Status status, Tteam tteam) {
        Mitarbeiter user = session.getUser();
        if (modulSeTeam != null && user != null) {
            if (session.hasRole(Rolle.ADMIN)) {
                return true;
            } else if (session.hasRole(Rolle.E_COC)) {
                List<Berechtigung> berechtigungen = berechtigungService.getBerechtigungByMitarbeiterAndRolleAndZielById(user, Rolle.E_COC, modulSeTeam.getId().toString(), BerechtigungZiel.MODULSETEAM);
                if (berechtigungen != null && !berechtigungen.isEmpty()) {
                    Berechtigung berechtigung = berechtigungen.get(0);
                    return berechtigung.getRechttype() == Rechttype.SCHREIBRECHT;
                }
            } else if (Status.A_ANGELEGT_IN_PROZESSBAUKASTEN.equals(status) && (session.hasRole(Rolle.T_TEAMLEITER) || session.hasRole(Rolle.TTEAM_VERTRETER) || session.hasRole(Rolle.TTEAMMITGLIED))) {
                if (tteam != null && userIsAuthorizedForTteam(tteam)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean userIsAuthorizedForTteam(Tteam tteam) {
        if (session.getUserPermissions().getTteamIdsForTteamMitgliedSchreibend().contains(tteam.getId())) {
            return true;
        }
        if (session.getUserPermissions().getTteamIdsForTteamVertreter().contains(tteam.getId())) {
            return true;
        }
        if (session.getUserPermissions().getTteamAsTteamleiterSchreibend().stream().anyMatch(berechtigungDto -> berechtigungDto.getId().equals(tteam.getId()))) {
            return true;
        }
        return false;
    }

    public List<AnforderungFreigabeBeiModulSeTeam> anforderungForModulSeTeamFreigeben(Anforderung anforderung, List<ModulSeTeam> modulSeTeams, String kommentar) {
        List<AnforderungFreigabeBeiModulSeTeam> anfoFreigabeList = anforderungFreigabeService.getAnfoFreigabeByAnforderung(anforderung);
        return anfoFreigabeList.stream()
                .filter(af -> modulSeTeams.contains(af.getModulSeTeam()))
                .peek(af -> {
                    af.setFreigabe(true);
                    af.setKommentar(kommentar);
                    af.setDatum(new Date());
                }).collect(Collectors.toList());
    }

    public Anforderung anforderungForAllMyModulSeTeamsFreigeben(Anforderung anforderung, Mitarbeiter currentUser, String kommentar) {
        List<ModulSeTeam> modulSeTeams;
        if (session.hasRole(Rolle.ADMIN)) {
            modulSeTeams = modulService.getAllSeTeams();
        } else {
            modulSeTeams = berechtigungService.getAuthorizedModulSeTeamListForMitarbeiter(currentUser);
        }

        List<AnforderungFreigabeBeiModulSeTeam> freigegebeneModuls = anforderungForModulSeTeamFreigeben(anforderung, modulSeTeams, kommentar);
        anforderung.setAnfoFreigabeBeiModul(freigegebeneModuls);

        int modulsTotal = anforderungFreigabeService.countAllModulSeTeamsForAnforderung(anforderung);
        int modulsFreigegeben = anforderungFreigabeService.countFreigegebeneModulSeTeamsForAnforderung(anforderung);

        if (modulsTotal > 0 && modulsTotal == modulsFreigegeben) {
            anforderung = setzeAnforderungAufFreigegeben(anforderung, kommentar);
        }
        return anforderung;
    }

    public Boolean hasPreviousVersion(Anforderung anforderung) {
        Anforderung previousVersion = getAnforderungByFachIdVersion(anforderung.getFachId(), anforderung.getVersion() - 1);
        return previousVersion != null;
    }

    public Boolean checkFreizugeben(Anforderung anforderung, AnforderungFreigabeBeiModulSeTeam anforderungModul) {
        return hasPreviousVersion(anforderung) && anforderungHasLastModulFreizugeben(anforderung, anforderungModul);
    }

    private boolean anforderungHasLastModulFreizugeben(Anforderung anforderung, AnforderungFreigabeBeiModulSeTeam anforderungModul) {
        List<AnforderungFreigabeBeiModulSeTeam> freigegebeneAnforderungModule = anforderungFreigabeService.getFreigegebeneModulSeTeamsForAnforderung(anforderung);
        int modulsTotal = anforderungFreigabeService.countAllModulSeTeamsForAnforderung(anforderung);

        return modulsTotal > 0 && modulsTotal == freigegebeneAnforderungModule.size() + 1 && !freigegebeneAnforderungModule.contains(anforderungModul);

    }

    public Boolean checkForFreigegeben(Anforderung anforderung) {
        int modulsTotal = anforderungFreigabeService.countAllModulSeTeamsForAnforderung(anforderung);
        int modulsFreigegeben = anforderungFreigabeService.countFreigegebeneModulSeTeamsForAnforderung(anforderung);
        return modulsTotal > 0 && modulsTotal == modulsFreigegeben;
    }

    public Boolean isDemGueltigenProzessbaukastenZugeordnet(Anforderung anforderung) {
        List<Anforderung> previousVersionsWithGueltigemProzessbaukasten = anforderungDao.getPreviousVersionsWithGueltigenProzessbaukasten(anforderung);
        return !previousVersionsWithGueltigemProzessbaukasten.isEmpty();
    }

    public Anforderung getAnforderungById(Long id) {
        return anforderungDao.getAnforderungById(id);
    }

    public List<Anforderung> getAllAnforderungenByStatus(Status status) {
        return anforderungDao.getAllAnforderungenByStatus(status);
    }

    public Anforderung getAnforderungByFachIdVersion(String fachId, Integer version) {
        return anforderungDao.getAnforderungByFachIdVersion(fachId, version);
    }

    public List<Anforderung> getVorigeVersionenForAnforderung(String fachId, Integer version) {
        return new ArrayList<>();
    }

    private Anforderung getAnforderungWorkCopy(Anforderung anforderung) {
        Anforderung original = anforderung;
        Anforderung workCopy = new Anforderung();
        if (original != null) {
            try {
                original.copy(workCopy);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }
            if (workCopy.getAnhaenge() == null) {
                workCopy.setAnhaenge(new ArrayList<>());
            }
            workCopy.setDetektoren(original.getDetektoren());
            Optional<Mitarbeiter> sensorCoCLeiter = berechtigungService
                    .getSensorCoCLeiterOfSensorCoc(original.getSensorCoc());
            Optional<Mitarbeiter> tteamleiter = berechtigungService.getTteamleiterOfTteam(original.getTteam());

            Set<Prozessbaukasten> prozessbaukasten = new HashSet<>();
            if (anforderung.isProzessbaukastenZugeordnet()) {
                prozessbaukasten.addAll(anforderung.getProzessbaukasten());
            }

            workCopy.setProzessbaukasten(prozessbaukasten);

            sensorCoCLeiter.ifPresent(mitarbeiter -> workCopy.getSensorCoc().setSensorCoCLeiter(mitarbeiter));
            tteamleiter.ifPresent(mitarbeiter -> workCopy.getTteam().setTeamleiter(mitarbeiter));

            workCopy.setProzessbaukasten(prozessbaukasten);

            final Collection<ModulSeTeamId> configuredModulSeTeamsForAnforderung = anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getConfiguredModulSeTeamsForAnforderung(anforderung);

            workCopy.getAnfoFreigabeBeiModul().stream()
                    .filter(freigabe -> configuredModulSeTeamsForAnforderung.contains(freigabe.getModulSeTeam().getModulSeTeamId()))
                    .forEach(freigabe -> freigabe.setFahrzeugmerkmalConfigured(Boolean.TRUE));

        }

        return workCopy;
    }

    public Anforderung getAnforderungWorkCopyByIdOrNewAnforderung(Long id) {
        Anforderung original = anforderungDao.getAnforderungById(id);
        return getAnforderungWorkCopy(original);
    }

    public Anforderung getAnforderungWorkCopyByAnfoderung(Anforderung anforderung) {
        return getAnforderungWorkCopy(anforderung);
    }

    public List<Meldung> getMeldungByFachIdStartingWith(String fachId) {
        return anforderungDao.getMeldungByFachIdStartingWith(fachId);
    }

    public List<Meldung> getAllMeldungenBySensorCocAndStatus(List<SensorCoc> scoc, List<Status> status) {
        return anforderungDao.getMeldungenBySensorCocAndStatus(scoc, status);
    }

    public List<Meldung> getMeldungenBySensorCocAndStatusNotInIdList(List<SensorCoc> sensorCocList, List<Status> statusList, List<Long> idList) {
        return anforderungDao.getMeldungenBySensorCocAndStatusNotInIdList(sensorCocList, statusList, idList);
    }

    public Meldung getMeldungById(Long id) {
        return anforderungDao.getMeldungById(id);
    }

    public Meldung getMeldungWorkCopyByFachIdOrNewMeldung(String fachId) {
        Meldung original = getUniqueMeldungByFachId(fachId);
        return getMeldungWorkCopyForMeldung(original);
    }

    public Meldung getMeldungWorkCopyByIdOrNewMeldung(Long id) {
        Meldung original = anforderungDao.getMeldungById(id);
        return getMeldungWorkCopyForMeldung(original);
    }

    private Meldung getMeldungWorkCopyForMeldung(Meldung original) {
        Meldung workCopy = new Meldung();
        if (original != null) {
            try {
                original.copy(workCopy);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

            if (original.getSensorCoc() != null) {
                Optional<Mitarbeiter> sensorCoCLeiter = berechtigungService
                        .getSensorCoCLeiterOfSensorCoc(original.getSensorCoc());
                if (sensorCoCLeiter.isPresent()) {
                    workCopy.getSensorCoc().setSensorCoCLeiter(sensorCoCLeiter.get());
                }
            }

            workCopy.setDetektoren(original.getDetektoren());
        }
        return workCopy;
    }

    public Meldung getMeldungWorkCopyByIdOrNewMeldung(Meldung original) {
        Meldung workCopy = new Meldung();
        if (original != null) {
            try {
                original.copy(workCopy);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }
        }
        return workCopy;
    }

    public List<ZuordnungAnforderungDerivat> getDeriAnforderungenForAnforderung(Anforderung anforderung) {
        return zuordnungAnforderungDerivatService.getZuordnungAnforderungDerivatForAnforderung(anforderung);
    }

    public List<ZuordnungAnforderungDerivatDTO> getAnforderungeDerivatListForAnforderung(Anforderung anforderung, Set<Rolle> rolesWithWritePermissions) {
        List<ZuordnungAnforderungDerivatDTO> result = new ArrayList<>();
        List<ZuordnungAnforderungDerivat> anforderungDerivatList = zuordnungAnforderungDerivatService.getZuordnungAnforderungDerivatForAnforderung(anforderung);

        anforderungDerivatList.forEach(ad -> result.add(new ZuordnungAnforderungDerivatDTO(ad.getId(), ad.getDerivat().getId(), ad.getDerivat().getName(), ad.getStatus(), ad.isNachZakUebertragen(), ad.isZurKenntnisGenommen(), rolesWithWritePermissions)));

        derivatAnforderungModulService.getDerivatAnforderungModulByAnforderungStatus(anforderung, DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET)
                .stream()
                .map(vereinbarung -> vereinbarung.getDerivat().getId())
                .forEach((Long derivatId) -> {
                    Optional<ZuordnungAnforderungDerivatDTO> zuordnungDto = result.stream()
                            .filter(zuordnungAnforderungDerivat -> zuordnungAnforderungDerivat.getDerivatId().equals(derivatId))
                            .findAny();
                    if (zuordnungDto.isPresent()) {
                        zuordnungDto.get().setUnzureichendeAnforderungsqualitaet(Boolean.TRUE);
                        zuordnungDto.get().updateViewPermissions();
                    }
                });

        return result;
    }

    public void acceptZurKenntnisGenommen(ZuordnungAnforderungDerivatDTO anforderungDerivatDTO) {
        ZuordnungAnforderungDerivat anforderungDerivat = zuordnungAnforderungDerivatService.getZuordnungAnforderungDerivatById(anforderungDerivatDTO.getId());
        anforderungDerivatDTO.setZurKenntnisGenommen(true);
        anforderungDerivat.setZurKenntnisGenommen(true);
        zuordnungAnforderungDerivatService.persistZuordnungAnforderungDerivat(anforderungDerivat);

        AnforderungHistory anforderungHistory = new AnforderungHistory(anforderungDerivat.getAnforderung().getId(), anforderungDerivat.getAnforderung().getKennzeichen(),
                new Date(), "Zuordnung", session.getUser().toString(), "", "Zuordnung zu " + anforderungDerivat.getDerivat().toString()
                + " im Status unzureichende Anforderungsqualität wurde zur Kenntnis genommen.");
        historyService.persistAnforderungHistory(anforderungHistory);
    }

    public Anforderung createAnforderungForMeldung(String fachId) throws IllegalAccessException, InvocationTargetException {
        Meldung m = getUniqueMeldungByFachId(fachId);
        Anforderung a = new Anforderung(1);
        m.cloneAbstractAnforderung(a);
        a.setId(null);
        a.setFachId("");
        a.setVersion(1);
        a.setBild(null);
        a.setStatus(Status.A_INARBEIT);
        a.setPhasenbezug(true);
        return a;
    }

    public Anforderung createNewAnforderung() {
        Anforderung a = new Anforderung(1);

        a.setId(null);
        a.setFachId("");
        a.setVersion(1);
        a.setBild(null);
        a.setStatus(Status.A_INARBEIT);
        a.setPhasenbezug(true);
        return a;
    }

    public Anforderung createAnforderungForMeldung(Long mid) throws IllegalAccessException, InvocationTargetException {
//        Meldung m = getMeldungByIdWithAllDependencies(mid);
        Meldung m = getMeldungById(mid);
        Anforderung a = new Anforderung(1);
        m.cloneAbstractAnforderung(a);
        a.setId(null);
        a.setFachId("");
        a.setVersion(1);
        a.setBild(null);
        a.setStatus(Status.A_INARBEIT);
        return a;
    }

    public Anforderung createNewVersionForAnforderung(Long aid) throws IllegalAccessException, InvocationTargetException {
        Anforderung original = getAnforderungById(aid);
        Anforderung clone = new Anforderung();
        original.cloneForNewAnforderung(clone);
        clone.setId(null);
        clone.setVersion(anforderungDao.getNextVersionNumber(original.getFachId()));
        return clone;
    }

    public Anforderung createNewVersionForAnforderung(String fachId, Integer version) throws IllegalAccessException, InvocationTargetException {
        Anforderung original = getAnforderungByFachIdVersion(fachId, version);
        Anforderung clone = new Anforderung();
        original.cloneForNewAnforderung(clone);
        clone.setId(null);
        clone.setVersion(anforderungDao.getNextVersionNumber(original.getFachId()));
        return clone;
    }

    public List<Anforderung> fetchVersionDataForFachId(String fachId) {
        return anforderungDao.fetchVersionDataForFachId(fachId);
    }

    public List<AnforderungVersionMenuViewData> fetchVersionViewDataForFachId(String fachId) {
        return anforderungDao.fetchVersionViewDataForFachId(fachId);
    }

    public Anforderung getUniqueAnforderungByFachId(String fachId) {
        return anforderungDao.getUniqueAnforderungByFachId(fachId);
    }

    public Meldung getUniqueMeldungByFachId(String fachId) {
        return anforderungDao.getUniqueMeldungByFachId(fachId);
    }

    public AbstractAnforderung getUniqueAnforderungOrMeldungByFachIdEndingWith(String fachId) {
        return fachId.matches("\\d+")
                ? anforderungDao.getUniqueAnforderungOrMeldungByFachIdEndingWith(Long.parseLong(fachId))
                : null;
    }

    public AbstractAnforderung getUniqueAnforderungOrMeldungByFachIdEndingWith(Long fachIdLong) {
        return anforderungDao.getUniqueAnforderungOrMeldungByFachIdEndingWith(fachIdLong);
    }

    public Map<ModulSeTeam, VereinbarungType> createVereinbarungMapForNewVersion(Anforderung anforderung) {
        Map<ModulSeTeam, VereinbarungType> vereinbarungMap = new HashMap<>();

        if (anforderung != null) {
            List<AnforderungFreigabeBeiModulSeTeam> anforderungFreigabeBeiModuls = anforderung.getAnfoFreigabeBeiModul();
            anforderungFreigabeBeiModuls.forEach((anforderungFreigabeBeiModul) -> {
                vereinbarungMap.put(anforderungFreigabeBeiModul.getModulSeTeam(), anforderungFreigabeBeiModul.getVereinbarungType());
            });
        }

        return vereinbarungMap;

    }

    public Map<ModulSeTeam, VereinbarungType> createVereinbarungMap(Anforderung anforderung) {
        Map<ModulSeTeam, VereinbarungType> modulSeTeamToVereinbarungTypeMap = new HashMap<>();
        // Insert AnforderungFreigabeBeiModul
        if (anforderung != null) {
            Set<Umsetzer> umsetzerList = anforderung.getUmsetzer();
            if (umsetzerList != null && !umsetzerList.isEmpty()) {
                ModulSeTeam modulSeTeam;
                for (Umsetzer umsetzer : umsetzerList) {
                    modulSeTeam = umsetzer.getSeTeam();
                    if (modulSeTeam != null) {
                        modulSeTeamToVereinbarungTypeMap.put(modulSeTeam, getVereinbarungTypForModulSeTeam(anforderung, modulSeTeam));
                    }
                }
            }
        }
        return modulSeTeamToVereinbarungTypeMap;
    }

    private VereinbarungType getVereinbarungTypForModulSeTeam(Anforderung anforderung, ModulSeTeam modulSeTeam) {
        AnforderungFreigabeBeiModulSeTeam anforderungFreigabeBeiModulSeTeam = anforderungFreigabeService.getAnfoFreigabeByAnforderungAndModulSeTeam(anforderung, modulSeTeam);
        if (anforderungFreigabeBeiModulSeTeam == null) {

            Set<Meldung> meldungen = anforderung.getMeldungen();
            if (meldungen != null && !meldungen.isEmpty()) {
                return VereinbarungType.ZAK;
            } else {
                return VereinbarungType.STANDARD;
            }

        } else {
            return anforderungFreigabeBeiModulSeTeam.getVereinbarungType();
        }
    }

    public List<AnforderungFreigabeBeiModulSeTeam> copyModuleZumFreigeben(Anforderung anfo, Map<ModulSeTeam, VereinbarungType> vtmap) {
        List<AnforderungFreigabeBeiModulSeTeam> copy = new ArrayList<>();
        ModulSeTeam modulSeTeam;
        // Insert AnforderungFreigabeBeiModul
        Set<Umsetzer> umsetzerList = anfo.getUmsetzer();
        if (umsetzerList != null && !umsetzerList.isEmpty()) {
            for (Umsetzer umsetzer : umsetzerList) {
                modulSeTeam = umsetzer.getSeTeam();
                if (modulSeTeam != null) {
                    AnforderungFreigabeBeiModulSeTeam anfoModul = anforderungFreigabeService.getAnfoFreigabeByAnforderungAndModulSeTeam(anfo, umsetzer.getSeTeam());
                    if (anfoModul == null) {
                        anfoModul = new AnforderungFreigabeBeiModulSeTeam();
                        anfoModul.setAnforderung(anfo);
                        anfoModul.setModulSeTeam(modulSeTeam);
                    }

                    anfoModul.setFreigabe(false);
                    anfoModul.setKommentar("");

                    if (vtmap.keySet().contains(modulSeTeam)) {
                        anfoModul.setVereinbarungType(vtmap.get(modulSeTeam));
                    } else {
                        anfoModul.setVereinbarungType(VereinbarungType.ZAK);
                    }

                    if (!copy.contains(anfoModul)) {
                        copy.add(anfoModul);
                    }
                }
            }
        }
        return copy;
    }

    public List<FestgestelltIn> getAllFestgestelltIn() {
        return festgestelltInDao.getAllFestgestelltIn();
    }

    public void persistFestgestelltIn(FestgestelltIn festgestelltIn) {
        festgestelltInDao.persistFestgestelltIn(festgestelltIn);
    }

    public List<FestgestelltIn> getFestgestelltInByWert(String wert) {
        return festgestelltInDao.getFestgestelltInByWert(wert);
    }

    public List<FestgestelltIn> getFestgestelltInByIdList(List<Long> idList) {
        return festgestelltInDao.getFestgestelltInByIdList(idList);
    }

    public String tryToRemoveFestgestelltIn(FestgestelltIn festgestelltIn) {
        Long anforderungen = countAnforderungenForFestgestelltIn(festgestelltIn);
        Long meldungen = countMeldungenForFestgestelltIn(festgestelltIn);
        if (anforderungen == 0L && meldungen == 0L) {
            removeFestgestelltIn(festgestelltIn);
            return festgestelltIn.getWert() + " gelöscht";
        }
        return festgestelltIn.getWert() + " wird in Anforderungen oder Meldungen verwendet und kann nicht gelöscht werden!";
    }

    private void removeFestgestelltIn(FestgestelltIn festgestelltIn) {
        festgestelltInDao.removeFestgestelltIn(festgestelltIn);
    }

    private Long countAnforderungenForFestgestelltIn(FestgestelltIn festgestelltIn) {
        return anforderungDao.countAnforderungenForFestgestelltIn(festgestelltIn);
    }

    private Long countMeldungenForFestgestelltIn(FestgestelltIn festgestelltIn) {
        return anforderungDao.countAnforderungenForFestgestelltIn(festgestelltIn);
    }

    public boolean isMeldungForUserRole(Long id) {
        Mitarbeiter currentUser = session.getUser();
        List<Rolle> mitarbeiterRollen = session.getUserPermissions().getRollen();
        if (getMeldungById(id) != null) {
            if (mitarbeiterRollen.contains(Rolle.ADMIN) || mitarbeiterRollen.contains(Rolle.T_TEAMLEITER) || mitarbeiterRollen.contains(Rolle.TTEAM_VERTRETER) || mitarbeiterRollen.contains(Rolle.ANFORDERER)) {
                return Boolean.TRUE;
            } else if (mitarbeiterRollen.contains(Rolle.SENSOR_EINGESCHRAENKT)) {
                return anforderungDao.getMeldungForIdAndSensor(id, currentUser) != null;
            } else {
                return anforderungDao.getMeldungForIdAndSensorCoc(id, currentUser) != null;
            }
        }
        return Boolean.FALSE;
    }

    private boolean isAnforderungForUserRole(Long id, Mitarbeiter currentUser, List<Rolle> mitarbeiterRollen) {
        if (getAnforderungById(id) != null) {
            if (mitarbeiterRollen.contains(Rolle.ADMIN) || mitarbeiterRollen.contains(Rolle.T_TEAMLEITER) || mitarbeiterRollen.contains(Rolle.TTEAM_VERTRETER) || mitarbeiterRollen.contains(Rolle.ANFORDERER)) {
                return Boolean.TRUE;
            } else {
                Anforderung anforderung = anforderungDao.getAnforderungForIdAndBerechtigung(id, currentUser, mitarbeiterRollen);
                return anforderung != null;
            }
        }
        return Boolean.FALSE;
    }

    public List<Anforderung> getAnforderungenByIdList(List<Long> idList) {
        return anforderungDao.getAnforderungenByIdList(idList);
    }

    public List<Meldung> getMeldungenByIdList(List<Long> idList) {
        return anforderungDao.getMeldungenByIdList(idList);
    }

    public ZuordnungAnforderungDerivatService getZuordnungAnforderungDerivatService() {
        return zuordnungAnforderungDerivatService;
    }

    public void setZuordnungAnforderungDerivatService(ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService) {
        this.zuordnungAnforderungDerivatService = zuordnungAnforderungDerivatService;
    }

    public Anforderung addMeldungenToAnforderung(Anforderung anforderung, Collection<Meldung> meldungen, Mitarbeiter user) {
        return anforderungMeldungZuordnungAdapter.addMeldungenToAnforderung(anforderung, meldungen, user);
    }

    public Anforderung removeMeldungenFromAnforderung(Anforderung anforderung, Collection<Meldung> meldungen, Mitarbeiter user) {
        return anforderungMeldungZuordnungAdapter.removeMeldungenFromAnforderung(anforderung, meldungen, user);
    }

    public void updateStandardBild(AbstractAnfoMgmtObject aamo, Anhang selectedAnhangForBild) {
        if (selectedAnhangForBild != null && selectedAnhangForBild.getContent().getContent() != null) {
            for (Anhang anhang : aamo.getAnhaenge()) {
                if (anhang.getContent().getContent() != null && Arrays.equals(anhang.getContent().getContent(), selectedAnhangForBild.getContent().getContent())) {
                    anhang.setStandardBild(true);
                    aamo.setBild(imageService.generateBildFromAnhang(anhang));
                } else {
                    anhang.setStandardBild(false);
                }
            }
        } else if (selectedAnhangForBild != null && selectedAnhangForBild.getContent().getFilePath() != null) {
            byte[] data = fileService.readFileData(selectedAnhangForBild.getContent().getFilePath());
            for (Anhang anhang : aamo.getAnhaenge()) {
                if (anhang.getContent().getFilePath() != null) {
                    byte[] aData = fileService.readFileData(anhang.getContent().getFilePath());
                    if (Arrays.equals(aData, data)) {
                        anhang.setStandardBild(true);
                        aamo.setBild(imageService.generateBildFromAnhang(anhang));
                    } else {
                        anhang.setStandardBild(false);
                    }
                } else {
                    anhang.setStandardBild(false);
                }
            }
        } else {
            aamo.setBild(null);
            aamo.getAnhaenge().forEach(anhang -> anhang.setStandardBild(Boolean.FALSE));
        }
    }

    public List<Anforderung> getAnforderungenByKomponenteId(Long komponenteId) {
        return anforderungDao.getAnforderungenByKomponenteId(komponenteId);
    }

    public List<Anforderung> getAnforderungenBySeTeamName(String seTeamName) {
        return anforderungDao.getAnforderungenBySeTeamName(seTeamName);
    }

    public boolean modulAblehnen(AnforderungFreigabeBeiModulSeTeam anfoModul, String modulFreigabeKommentar) {

        if (modulFreigabeKommentar == null || "".equals(modulFreigabeKommentar)) {
            showGrowlByFehlendenKommentarForModulAblehnen();
            return false;

        } else {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            requestContext.execute("PF('modulFreigabeCommentDialog').hide()");

            lehneModulAbAndWriteHistory(anfoModul, modulFreigabeKommentar);
            return true;
        }
    }

    private void showGrowlByFehlendenKommentarForModulAblehnen() {
        String summary = localizationService.getValue("anforderungservice_modulablehnen_dialog_kommentarfehlt");
        String detail = localizationService.getValue("anforderungservice_modulablehnen_dialog_detail");
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
    }

    private void lehneModulAbAndWriteHistory(AnforderungFreigabeBeiModulSeTeam anfoModul, String modulFreigabeKommentar) {
        Mitarbeiter currentUser = session.getUser();

        Anforderung anforderung = modulSeTeamAblehnen(currentUser, anfoModul, modulFreigabeKommentar);

        anforderung.setAenderungsdatum(new Date());
        historyService.writeHistoryForChangedValuesOfAnfoMgmtObject(currentUser, anforderung, getAnforderungById(anforderung.getId()));
        saveAnforderung(anforderung);
    }

    public void anforderungForSelectedModulFreigeben(AnforderungFreigabeBeiModulSeTeam anfoModul, String modulFreigabeKommentar) {
        Mitarbeiter currentUser = session.getUser();

        if (modulFreigabeKommentar == null) {
            modulFreigabeKommentar = "";
        }

        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('modulFreigabeCommentDialog').hide()");

        Anforderung anforderung = anforderungForSelectedModulFreigeben(currentUser, anfoModul, modulFreigabeKommentar);

        if (checkForFreigegeben(anforderung)) {
            if (Status.A_ANGELEGT_IN_PROZESSBAUKASTEN.equals(anforderung.getStatus())) {
                updateAnforderungStatusToGenehmigtImProzessbaukastenAndWriteHistory(anforderung, modulFreigabeKommentar, currentUser);
            } else {
                setzeAnforderungAufFreigebenAndWriteHistory(anforderung, modulFreigabeKommentar, currentUser);

            }
        }
    }

    private void updateAnforderungStatusToGenehmigtImProzessbaukastenAndWriteHistory(Anforderung anforderung, String modulFreigabeKommentar, Mitarbeiter currentUser) {
        updateAnforderungStatusToGenehmigtImProzessbaukasten(anforderung, modulFreigabeKommentar);
        historyService.writeHistoryForChangedValuesOfAnfoMgmtObject(currentUser, anforderung, getAnforderungById(anforderung.getId()));
        saveAnforderung(anforderung);
    }

    protected void setzeAnforderungAufFreigebenAndWriteHistory(Anforderung anforderung, String modulFreigabeKommentar, Mitarbeiter currentUser) {

        reportingStatusTransitionAdapter.changeAnforderungStatus(anforderung, anforderung.getStatus(), Status.A_FREIGEGEBEN);

        anforderung = setzeAnforderungAufFreigegeben(anforderung, modulFreigabeKommentar);
        historyService.writeHistoryForChangedValuesOfAnfoMgmtObject(currentUser, anforderung, getAnforderungById(anforderung.getId()));
        saveAnforderung(anforderung);

        List<Anforderung> vorigeVersionen = anforderungDao.getVorigeVersionenForAnforderung(anforderung);
        vorigeVersionen.forEach(version -> {
            setzeAnforderungAufKeineWeiterverfolgungAndWriteHistory(version, currentUser);
        });

        mailService.sendMailAnforderungChanged(anforderung, currentUser);
    }

    private void setzeAnforderungAufKeineWeiterverfolgungAndWriteHistory(Anforderung anforderung, Mitarbeiter user) {

        Status anforderungStatus = anforderung.getStatus();
        if (isStatusWechselZumKeineWeiterverfolgenErlaubt(anforderungStatus)) {
            reportingStatusTransitionAdapter.changeAnforderungStatus(anforderung, anforderung.getStatus(), Status.A_KEINE_WEITERVERFOLG);

            String oldStatus = anforderung.getStatus().getStatusBezeichnung();
            String newStatus = Status.A_KEINE_WEITERVERFOLG.getStatusBezeichnung();
            anforderung = setzeAnforderungAufKeineWeiterverfolgung(anforderung);
            saveAnforderung(anforderung);
            historyService.writeHistoryForChangedKeyValue(user, anforderung, newStatus, oldStatus, "status");
            String statusWechselKommentar = "Status Wechsel wegen der Freigabe der neuen Version";
            historyService.writeHistoryForChangedKeyValue(user, anforderung, statusWechselKommentar, "", "statusWechselKommentar");
        }

    }

    private static boolean isStatusWechselZumKeineWeiterverfolgenErlaubt(Status anforderungStatus) {
        return anforderungStatus != Status.A_KEINE_WEITERVERFOLG && anforderungStatus != Status.A_GELOESCHT;
    }

    private Anforderung updateAnforderungStatusToGenehmigtImProzessbaukasten(Anforderung anforderung, String kommentarZumStatusWechsel) {
        anforderung.setStatus(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
        anforderung.setNextStatus(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
        anforderung.setStatusChangeRequested(true);
        anforderung.setZeitpunktStatusaenderung(new Date());
        anforderung.setStatusWechselKommentar(kommentarZumStatusWechsel);
        anforderung.setAenderungsdatum(new Date());
        return anforderung;
    }

    private Anforderung setzeAnforderungAufFreigegeben(Anforderung anforderung, String kommentarZumStatusWechsel) {
        anforderung.setStatus(Status.A_FREIGEGEBEN);
        anforderung.setNextStatus(Status.A_FREIGEGEBEN);
        anforderung.setStatusChangeRequested(true);
        anforderung.setZeitpunktStatusaenderung(new Date());
        anforderung.setStatusWechselKommentar(kommentarZumStatusWechsel);
        anforderung.setAenderungsdatum(new Date());
        return anforderung;
    }

    private Anforderung setzeAnforderungAufKeineWeiterverfolgung(Anforderung anforderung) {
        anforderung.setStatus(Status.A_KEINE_WEITERVERFOLG);
        anforderung.setAenderungsdatum(new Date());
        anforderung.setZeitpunktStatusaenderung(new Date());
        return anforderung;
    }

    public List<ProzessbaukastenLinkData> getProzessbaukastenLinksForAnforderung(Anforderung anforderung) {
        List<Object[]> queryResult = anforderungDao.getProzessbaukastenLinksForAnforderung(anforderung.getId());
        List<ProzessbaukastenLinkData> result = parseToProzessbaukastenLinkData(queryResult);
        return result;
    }

    private List<ProzessbaukastenLinkData> parseToProzessbaukastenLinkData(List<Object[]> queryResult) {
        List<ProzessbaukastenLinkData> result = new ArrayList<>();

        Iterator<Object[]> iterator = queryResult.iterator();
        while (iterator.hasNext()) {
            Object[] row = iterator.next();
            String fachId = (String) row[0];
            Integer version = (Integer) row[1];
            String bezeichnung = (String) row[2];
            result.add(new ProzessbaukastenLinkData(fachId, version, bezeichnung));
        }

        return result;
    }

    public MenuModel getVersionsMenuItemsForAnforderung(Anforderung anforderung, boolean hasRightToCreateNewVersion) {
        MenuModel versionsMenuItems = new DefaultMenuModel();
        List<Anforderung> versionen = fetchVersionDataForFachId(anforderung.getFachId());

        versionen.stream().map((version) -> {
            return buildMenuItemForAnforderungVersion(version, anforderung);
        }).forEachOrdered(versionsMenuItems::addElement);

        if (hasRightToCreateNewVersion) {
            MenuItem item = buildMenuItemForNewVersion();
            versionsMenuItems.addElement(item);
        }

        return versionsMenuItems;
    }

    private MenuItem buildMenuItemForAnforderungVersion(Anforderung version, Anforderung anforderung) {
        DefaultMenuItem item = new DefaultMenuItem();
        item.setUrl("anforderungView.xhtml?id=" + version.getId() + "&faces-redirect=true");
        item.setIcon("ui-icon-arrow-1-e");
        SimpleDateFormat toformat = new SimpleDateFormat("dd.MM.yyyy");
        String vl;
        if (version.getErstellungsdatum() == null) {
            vl = 'V' + version.getVersion().toString();
        } else {
            vl = 'V' + version.getVersion().toString() + " " + toformat.format(version.getErstellungsdatum());
        }
        item.setValue(vl);
        if (version.getVersion().equals(anforderung.getVersion())) {
            item.setStyleClass("current");
        }
        return item;
    }

    private MenuItem buildMenuItemForNewVersion() {
        DefaultMenuItem item = new DefaultMenuItem();
        item.setOnclick("PF('versionPlusDialog').show()");
        item.setId(FacesContext.getCurrentInstance().getViewRoot().createUniqueId());
        item.setUrl("#");
        item.setIcon("ui-icon-plus");
        item.setStyleClass("add");
        item.setValue("Neue Version");
        return item;
    }

    public MenuModel getZugeordneteProzessbaukastenMenuItems(List<ProzessbaukastenLinkData> zugeordneteProzessbaukasten) {
        DefaultMenuModel zugeordneteProzessbaukastenMenuItems = new DefaultMenuModel();

        zugeordneteProzessbaukasten.stream().map(linkData -> {

            DefaultMenuItem item = new DefaultMenuItem();
            item.setUrl(getUrlForProzessbaukasten(linkData));
            item.setIcon("ui-icon-arrow-1-e");
            item.setValue(linkData.toString());
            return item;

        }).forEachOrdered(zugeordneteProzessbaukastenMenuItems::addElement);

        return zugeordneteProzessbaukastenMenuItems;
    }

    public MenuModel getZugeordneteMeldungenMenuItemsForAnforderung(Anforderung anforderung, boolean hasRightToZuordnen) {
        MenuModel zugeordneteMeldungenMenuItems = new DefaultMenuModel();

        if (anforderung == null) {
            return zugeordneteMeldungenMenuItems;
        }

        Set<Meldung> zugeordneteMeldungen = anforderung.getMeldungen();

        if (zugeordneteMeldungen != null && !zugeordneteMeldungen.isEmpty()) {
            zugeordneteMeldungen.stream().map(this::buildMenuItemForZugeordneteMeldung).forEachOrdered(zugeordneteMeldungenMenuItems::addElement);
        }

        if (hasRightToZuordnen) {
            MenuItem item = buildMenuItemForMeldungZuordnenLink();
            zugeordneteMeldungenMenuItems.addElement(item);
        }

        return zugeordneteMeldungenMenuItems;
    }

    private MenuItem buildMenuItemForZugeordneteMeldung(Meldung meldung) {
        String technologie = "<unbekannt>";
        DefaultMenuItem item = new DefaultMenuItem();
        item.setUrl("meldungView.xhtml?id=" + meldung.getId() + "&faces-redirect=true");
        item.setIcon("ui-icon-arrow-1-e");
        if (meldung.getSensorCoc() != null) {
            item.setValue(" " + meldung.getFachId() + " | " + meldung.getSensorCoc().getTechnologie());
        } else {
            item.setValue(" " + meldung.getFachId() + " | " + technologie);
        }
        return item;
    }

    private MenuItem buildMenuItemForMeldungZuordnenLink() {
        DefaultMenuItem item = new DefaultMenuItem();
        item.setId("0");
        item.setOnclick("showMeldungenZuordnenDialog()");
        item.setIcon("ui-icon-plus");
        item.setStyleClass("add");
        item.setValue("Neue Meldung zuordnen");
        return item;
    }

    protected String getUrlForProzessbaukasten(ProzessbaukastenLinkData prozessbaukastenLink) {
        return PageUtils.getUrlForProzessbaukastenView(prozessbaukastenLink.getFachId(), prozessbaukastenLink.getVersion(), ActiveIndex.forTab(ProzessbaukastenTab.STAMMDATEN));
    }

    public String generateGrowlMessageForFreigabeNeuerVersion() {
        StringBuilder sb = new StringBuilder();
        sb.append(localizationService.getValue("anforderung_view_demGueltigenProzessbaukastenZugeordnet_header"))
                .append("\n").append(localizationService.getValue("anforderung_view_demGueltigenProzessbaukastenZugeordnet_messageOne"))
                .append("\n").append(localizationService.getValue("anforderung_view_demGueltigenProzessbaukastenZugeordnet_messageTwo"))
                .append("\n").append(localizationService.getValue("anforderung_view_demGueltigenProzessbaukastenZugeordnet_messageThree"))
                .append("\n").append(localizationService.getValue("anforderung_view_demGueltigenProzessbaukastenZugeordnet_messageFour"));

        String message = sb.toString();
        return message;
    }

    public List<Anforderung> getAllAnforderungenWithMeldung() {
        return anforderungDao.getAllAnforderungenWithMeldung();
    }

}
