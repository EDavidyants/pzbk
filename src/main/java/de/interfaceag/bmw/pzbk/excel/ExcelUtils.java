package de.interfaceag.bmw.pzbk.excel;

import de.interfaceag.bmw.pzbk.exceptions.ExcelInputFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @author chsc
 */
public final class ExcelUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ExcelUtils.class.getName());

    private ExcelUtils() {
    }

    /**
     * Concatenates the elements of a row to a String. If includeEmptyCells ==
     * true, the will be added as an empty String "" to the result.
     *
     * @param row               ...
     * @param delimiter         ...
     * @param includeEmptyCells ...
     * @return ...
     */
    public static String rowToString(Row row, String delimiter, boolean includeEmptyCells) {
        if (delimiter == null) {
            delimiter = "";
        }

        StringBuilder result = new StringBuilder();
        String tmp;

        for (int i = 0; i < row.getLastCellNum(); i++) {
            tmp = cellToString(row.getCell(i, Row.CREATE_NULL_AS_BLANK));

            if (!"".equals(tmp) || includeEmptyCells) {
                result.append(tmp);
                result.append(delimiter);
            }
        }

        if (!"".equals(delimiter)) {
            result.deleteCharAt(result.length() - 1);
        }

        return result.toString();
    }

    /**
     * Puts the elements of a row in a list of strings. If includeEmptyCells is
     * true, blank cells that appear before the last populated cell will be
     * interptreted as an empty String "". Otherwise the list will just contain
     * the populated cells.
     *
     * @param row               ...
     * @param includeEmptyCells ...
     * @return ...
     */
    public static List<String> rowToStringList(Row row, boolean includeEmptyCells) {
        ArrayList<String> result = new ArrayList<>();

        String tmp;

        for (int i = 0; i < row.getLastCellNum(); i++) {
            tmp = cellToString(row.getCell(i, Row.CREATE_NULL_AS_BLANK));

            if (!"".equals(tmp) || includeEmptyCells) {
                result.add(tmp);
            }
        }

        return result;
    }

    /**
     * Reads a cell of an excel-sheet and converts it to a string value.
     *
     * @param cell ...
     * @return string value of the cell
     */
    public static String cellToString(Cell cell) {
        String result = cellToString(cell, false);
        return normalizeString(result);
    }

    private static String normalizeString(String string) {
        if (string.contains("\r\n")) {
            string = string.replaceAll("\r\n", " ");
            string = string.replaceAll(" +", " ");
        }
        return string.trim();
    }

    /**
     * Reads a cell of an excel-sheet and converts it to a string value.
     *
     * @param cell         ...
     * @param showFormula if true the formular will be returned instead of its
     *                     "result" value
     * @return string value of the cell
     */
    public static String cellToString(Cell cell, boolean showFormula) {
        String result = "";

        if (cell == null) {
            return result;
        }

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                result = cell.getStringCellValue();
                break;
            case Cell.CELL_TYPE_NUMERIC:
                Double d = cell.getNumericCellValue();
                boolean isWhole = d % 1 == 0;
                if (isWhole) {
                    result = "" + d.longValue();
                } else {
                    result = d.toString();
                }
                break;
            case Cell.CELL_TYPE_BLANK:
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                result = "" + cell.getBooleanCellValue();
                break;
            case Cell.CELL_TYPE_FORMULA:
                if (showFormula) {
                    result = cell.getCellFormula();
                } else {
                    result = cell.toString();
                }
                break;
            default:
                result = cell.toString();
                break;
        }

        return result;
    }

    public static XSSFWorkbook createXSSFWorkbookForFile(File file) throws ExcelInputFileException {
        XSSFWorkbook wb;
        try (InputStream inputStream = new FileInputStream(file);) {
            wb = new XSSFWorkbook(inputStream);
        } catch (IOException ex) {
            LOG.error(null, ex);
            throw new ExcelInputFileException(ex.getMessage());
        }

        return wb;
    }

    public static List<String> loadColumn(int col, int offset, Sheet sheet) {
        return loadColumn(col, offset, 0, sheet);
    }

    public static List<String> loadColumn(int col, int offsetStart, int offsetEnd, Sheet sheet) {
        List<String> result = new ArrayList<>();
        Iterator<Row> iter = sheet.iterator();
        for (int i = 0; i < offsetStart; i++) {
            iter.next();
        }

        if (offsetEnd <= 0) {
            while (iter.hasNext()) {
                Row row = iter.next();
                String typ = ExcelUtils.cellToString(row.getCell(col));
                result.add(typ);
            }
        } else {
            int cntEnd = 0;
            while (iter.hasNext() && cntEnd < offsetEnd) {
                Row row = iter.next();
                String typ = ExcelUtils.cellToString(row.getCell(col));
                result.add(typ);
                cntEnd++;
            }

        }

        return result;
    }

    /**
     * @param start     Start at line
     * @param sheetName ...
     * @param wb        ...
     * @return ...
     */
    public static List<List<String>> loadRowsBySheet(int start, String sheetName, Workbook wb) {
        Sheet sheet = wb.getSheet(sheetName);
        return loadRowsBySheet(start, sheet);
    }

    /**
     * @param start Start at line
     * @param sheet ...
     * @return ...
     */
    public static List<List<String>> loadRowsBySheet(int start, Sheet sheet) {
        List<List<String>> result = new ArrayList<>();
        Iterator<Row> iter = sheet.iterator();
        for (int i = 0; i < start; i++) {
            iter.next();
        }
        while (iter.hasNext()) {
            Row row = iter.next();
            result.add(ExcelUtils.rowToStringList(row, true));
        }
        return result;
    }

    public static List<List<String>> getColByTag(Sheet sheet, int rowNum, String tag, int offsetStart) {
        return getColByTag(sheet, rowNum, tag, offsetStart, 0);
    }

    /**
     * Sucht eine Spalte in einem Sheet anhand des darin enthaltenen Textes.
     * Eine Spalte wird duch 1 Stringliste repräsentiert.
     *
     * @param sheet       Das Excelblatt welches dursucht werden soll
     * @param rowNum      Position der Zeile im Sheet in der gesucht werden soll,
     *                    beginnend bei 0
     * @param tag         Der text welche in den Zellen stehen soll, zu welchen die
     *                    entsprechenen Spalten gesucht werden.
     * @param offsetStart Offset ab welcher Zeile die zurückzugebende Spalte
     *                    befüllt werden soll beginnen mit 0
     * @param offsetEnd   Falls ein Offset von 0 angegeben wurde wird die
     *                    komplette Spalte zurückgeliefert
     * @return Eine Liste von Stringlisten welche je eine gefundene Spalte
     * enthalten.
     */
    public static List<List<String>> getColByTag(Sheet sheet, int rowNum, String tag, int offsetStart, int offsetEnd) {
        List<List<String>> result = new ArrayList<>();
        Row row = sheet.getRow(rowNum);
        Iterator<Cell> iter = row.iterator();

        int posIter = 0;

        while (iter.hasNext()) {
            Cell cell = iter.next();
            String cellStr = cellToString(cell);
            if (cellStr.equals(tag)) {
                List<String> tmpColumn = loadColumn(posIter, offsetStart, offsetEnd, sheet);
                result.add(tmpColumn);
            }
            posIter++;
        }

        return result;
    }

    /**
     * Suche die spalte welche in der Zeilennummer rowNum das entsprechende Tag
     * enthalten.
     *
     * @param sheet  ...
     * @param rowNum ...
     * @param tag    ...
     * @return Alle Spaltennummern an denen das Tag gefunden wurde
     */
    public static List<Integer> getColNumByTag(Sheet sheet, int rowNum, String tag) {
        List<Integer> result = new ArrayList<>();
        Row row = sheet.getRow(rowNum);
        List<String> strRow = rowToStringList(row, true);
        Iterator<String> iter = strRow.iterator();

        int posIter = 0;
        while (iter.hasNext()) {
            String cellStr = iter.next();
            if (cellStr.equals(tag)) {
                result.add(posIter);
            }
            posIter++;
        }
        return result;
    }

    /**
     * Liefert für eine durch Namen definierte Spalte die zum daraus
     * resultierenden Tag (Wert in der Spalte) dazugehörigen Zeilen (also alle
     * folgenden Zeilen in denen kein Inhalt in der definierten Spalte zu finden
     * ist)
     *
     * @param sheet       ...
     * @param columnName ...
     * @param rowToBegin  ...
     * @return ExcelSheetRealms auf Zeilenbasis
     */
    public static Collection<ExcelSheetRealm> getRealmsForColumn(Sheet sheet, String columnName, int rowToBegin) {
        List<Integer> posList = ExcelUtils.getColNumByTag(sheet, 0, columnName);
        int posTag;
        if (!posList.isEmpty()) {
            posTag = posList.get(0);
        } else {
            posList = ExcelUtils.getColNumByTag(sheet, 0, "Mail");
            posTag = posList.get(0);
        }

        return getRealmsForColumn(sheet, posTag, rowToBegin);
    }

    /**
     * Liefert für eine durch die Spaltennummer beginnend bei 0 definierte
     * Spalte die zum daraus resultierenden Tag (Wert in der Spalte)
     * dazugehörigen Zeilen (also alle folgenden Zeilen in denen kein Inhalt in
     * der definierten Spalte zu finden ist)
     *
     * @param sheet      ...
     * @param pos        ...
     * @param rowToBegin ...
     * @return ExcelSheetRealms auf Zeilenbasis
     */
    public static Collection<ExcelSheetRealm> getRealmsForColumn(Sheet sheet, int pos, int rowToBegin) {
        List<ExcelSheetRealm> result = new ArrayList<>();

        Iterator<Row> iterRows = sheet.iterator();
        ExcelSheetRealm esr = null;

        // Ignore headline
        for (int i = 0; i < rowToBegin; i++) {
            if (iterRows.hasNext()) {
                iterRows.next();
            }
        }

        while (iterRows.hasNext()) {
            List<String> strRow = ExcelUtils.rowToStringList(iterRows.next(), true);
            String currentTag = strRow.get(pos).trim();

            if (!"".equals(currentTag)) {
                if (esr != null) {
                    result.add(esr);
                }
                esr = new ExcelSheetRealm(currentTag);
                esr.addChain(strRow);
            } else {
                if (esr != null) {
                    esr.addChain(strRow);
                }
            }

        }
        // No das letzte einfügen
        if (esr != null) {
            result.add(esr);
        }

        return result;
    }
}
