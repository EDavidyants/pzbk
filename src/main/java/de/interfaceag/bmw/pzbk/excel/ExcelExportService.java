package de.interfaceag.bmw.pzbk.excel;

import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.InfoText;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Werk;
import de.interfaceag.bmw.pzbk.enums.Attribut;
import de.interfaceag.bmw.pzbk.enums.ExcelSheetForProd;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.InfoTextService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.WerkService;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Named
@Stateless
public class ExcelExportService implements Serializable {

    @Inject
    protected AnforderungService anforderungService;
    @Inject
    protected SensorCocService sensorCocService;
    @Inject
    protected InfoTextService infoTextService;
    @Inject
    protected TteamService tteamService;
    @Inject
    protected ModulService modulService;
    @Inject
    protected ConfigService configService;
    @Inject
    protected WerkService werkService;

    public Workbook getExampleWithExistingData() {
        Workbook wb = new XSSFWorkbook();
        createRowsForSensorCoCs(wb);
        createRowsForFahrzeugtypen(wb);
        createRowsForAuswirkungen(wb);
        createRowsForInformationstexte(wb);
        createRowsForTTeam(wb);
        createRowsForProduktlinien(wb);
        createRowsForUmsetzerliste(wb);
        createRowsForWerk(wb);
        return wb;
    }

    protected Workbook createRowsForSensorCoCs(Workbook wb) {
        Sheet sheet = wb.createSheet(ExcelSheetForProd.SENSORCOC.getBezeichnung());

        List<SensorCoc> sensorCoCs = sensorCocService.getAllSortByTechnologie();

        int rownumber = 0;
        String ortung = "";
        if (sensorCoCs.get(0) != null) {
            ortung = sensorCoCs.get(0).getOrtung();
            Row rowInit = sheet.createRow(rownumber++);
            rowInit.createCell(0).setCellValue(ortung);
            rowInit.createCell(2).setCellValue(sensorCoCs.get(0).getZakEinordnung());
        }
        for (SensorCoc sensorCoC : sensorCoCs) {
            if (!ortung.equals(sensorCoC.getOrtung())) {
                Row row = sheet.createRow(rownumber++);
                ortung = sensorCoC.getOrtung();
                row.createCell(0).setCellValue(ortung);
                row.createCell(2).setCellValue(sensorCoC.getZakEinordnung());
            }
            Row rowTechnologie = sheet.createRow(rownumber++);
            rowTechnologie.createCell(1).setCellValue(sensorCoC.getTechnologie());
        }
        return wb;
    }

    protected Workbook createRowsForFahrzeugtypen(Workbook wb) {
        Sheet sheet = wb.createSheet(ExcelSheetForProd.FAHRZEUGTYPEN.getBezeichnung());

        List<FestgestelltIn> fahrzeugtypen = anforderungService.getAllFestgestelltIn();

        int rownumber = 0;
        for (FestgestelltIn fahrzeugtyp : fahrzeugtypen) {
            Row row = sheet.createRow(rownumber++);
            row.createCell(0).setCellValue(fahrzeugtyp.getWert());
        }
        return wb;
    }

    protected Workbook createRowsForWerk(Workbook wb) {
        Sheet sheet = wb.createSheet(ExcelSheetForProd.WERK.getBezeichnung());

        List<Werk> werke = werkService.getAllWerk();
        int rownumber = 0;
        for (Werk werk : werke) {
            Row row = sheet.createRow(rownumber++);
            row.createCell(0).setCellValue(werk.getName());
        }
        return wb;
    }

    protected Workbook createRowsForAuswirkungen(Workbook wb) {
        Sheet sheet = wb.createSheet(ExcelSheetForProd.AUSWIRKUNGEN.getBezeichnung());

        List<String> auswirkungen = configService.getWertStringByAttribut(Attribut.AUSWIRKUNG);

        int rownumber = 0;
        for (String auswirkung : auswirkungen) {
            Row row = sheet.createRow(rownumber++);
            row.createCell(0).setCellValue(auswirkung);
        }
        return wb;
    }

    protected Workbook createRowsForInformationstexte(Workbook wb) {
        Sheet sheet = wb.createSheet(ExcelSheetForProd.INFORMATIONSTEXTE.getBezeichnung());

        List<InfoText> infotexte = infoTextService.getAllInfoTexte();

        Row headerRow = sheet.createRow(0);
        headerRow.createCell(0).setCellValue("Feldname");
        headerRow.createCell(1).setCellValue("Beschreibungstext");
        headerRow.createCell(2).setCellValue("Sensor");

        int rownumber = 1;
        for (InfoText infotext : infotexte) {
            Row row = sheet.createRow(rownumber++);
            row.createCell(0).setCellValue(infotext.getFeldName());
            row.createCell(1).setCellValue(infotext.getBeschreibungsText());
            row.createCell(2).setCellValue(infotext.getFeldBeschreibungLang());
        }
        return wb;
    }

    protected Workbook createRowsForTTeam(Workbook wb) {
        Sheet sheet = wb.createSheet(ExcelSheetForProd.TTEAM.getBezeichnung());

        List<Tteam> tteams = tteamService.getAllTteams();

        Row headerRow = sheet.createRow(0);
        headerRow.createCell(0).setCellValue("team_name");

        int rownumber = 1;
        for (Tteam tteam : tteams) {
            Row row = sheet.createRow(rownumber++);
            row.createCell(0).setCellValue(tteam.getTeamName());
        }
        return wb;
    }

    protected Workbook createRowsForProduktlinien(Workbook wb) {
        Sheet sheet = wb.createSheet(ExcelSheetForProd.PRODUKTLINIEN.getBezeichnung());

        List<String> produktlinien = configService.getWertStringByAttribut(Attribut.PRODUKTLINIE);

        int rownumber = 0;
        for (String produktlinie : produktlinien) {
            Row row = sheet.createRow(rownumber++);
            row.createCell(0).setCellValue(produktlinie);
        }
        return wb;
    }

    protected Workbook createRowsForUmsetzerliste(Workbook wb) {
        Sheet sheet = wb.createSheet(ExcelSheetForProd.UMSETZERLISTE.getBezeichnung());

        Row headerRow = sheet.createRow(0);
        headerRow.createCell(0).setCellValue("FB");
        headerRow.createCell(1).setCellValue("EGV/EV, ML");
        headerRow.createCell(2).setCellValue("Beschreibung");
        headerRow.createCell(3).setCellValue("SE-Team");
        headerRow.createCell(4).setCellValue("Komponente");
        headerRow.createCell(5).setCellValue("PPG");

        int rownumber = 1;
        for (Modul modul : modulService.getAllModule()) {
            rownumber = createRowsForModul(sheet, rownumber, modul);
        }
        return wb;
    }

    private int createRowsForModul(Sheet sheet, int rownumber, Modul modul) {
        for (ModulSeTeam seTeam : modul.getSeTeams()) {
            rownumber = createRowsForModulSeTeam(sheet, rownumber, modul, seTeam);
        }
        return rownumber;
    }

    private static int createRowsForModulSeTeam(Sheet sheet, int rownumber, Modul modul, ModulSeTeam seTeam) {
        for (ModulKomponente komponente : seTeam.getKomponenten()) {
            rownumber = createRowForModulKomponente(sheet, rownumber, modul, seTeam, komponente);
        }
        return rownumber;
    }

    private static int createRowForModulKomponente(Sheet sheet, int rownumber, Modul modul, ModulSeTeam seTeam, ModulKomponente komponente) {
        Row row = sheet.createRow(rownumber++);
        row.createCell(0).setCellValue(modul.getFachBereich());
        row.createCell(1).setCellValue(modul.getName());
        row.createCell(2).setCellValue(modul.getBeschreibung());
        row.createCell(3).setCellValue(seTeam.getName());
        row.createCell(4).setCellValue(komponente.getName());
        row.createCell(5).setCellValue(komponente.getPpg());
        return rownumber;
    }

}
