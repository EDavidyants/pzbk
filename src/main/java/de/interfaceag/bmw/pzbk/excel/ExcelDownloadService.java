package de.interfaceag.bmw.pzbk.excel;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Named
@Stateless
public class ExcelDownloadService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ExcelDownloadService.class);

    public boolean downloadExcelExport(String fileName, Workbook workbook) {
        LOG.info("Start Excel Export Download");

        if (fileName == null) {
            LOG.warn("fileName is null!");
            return Boolean.FALSE;
        } else if (workbook == null) {
            LOG.warn("resultWorkbook is null!");
            return Boolean.FALSE;
        } else {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            externalContext.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + ".xlsx\"");

            try {
                workbook.write(externalContext.getResponseOutputStream());
                externalContext.getResponseOutputStream().flush();
                externalContext.getResponseOutputStream().close();
            } catch (IOException ex) {
                LOG.error(ex.toString(), ex);
                return Boolean.FALSE;
            }

            facesContext.responseComplete();
        }

        LOG.info("Finish Excel Export Download");
        return Boolean.TRUE;
    }

}
