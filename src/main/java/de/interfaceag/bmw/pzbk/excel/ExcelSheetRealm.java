package de.interfaceag.bmw.pzbk.excel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Der Realm dient dazu einen vertikalen oder horizontalen bereich in einer
 * Exceltabelle zu definieren. Ursache dieses Konstrukts ist der Umstand, dass
 * in den Excelsheets von BMW in welchen die Rechte der Nutzer geregelt werden
 * die Rechte pro benutzer in vertikal aufeinanderfolgenden Zellen definiert
 * werden. Da die Nutzer nun aber selbst wieder reihenweise aufgeführt werden
 * erschien es sinnvoll diese Hilfsklasse zu implementieren welche Werteketten
 * (chains) speichert, welche je nach belieben Spalten oder Zeilen darstellen
 * können.
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class ExcelSheetRealm {

    private String tag;
    private final List<List<String>> chains;

    public ExcelSheetRealm(String tag) {
        this.tag = tag;
        this.chains = new ArrayList<>();
    }

    /**
     * @return the tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    public void addChain(List<String> chainToAdd) {
        List<String> tmpChain = new ArrayList<>();
        for (String str : chainToAdd) {
            tmpChain.add(str);
        }
        this.chains.add(tmpChain);
    }

    /**
     * @return the chains
     */
    public List<List<String>> getChains() {
        return Collections.unmodifiableList(chains);
    }

    /**
     * Liefert beispielsweise für einen Realm aus Reihen eine Spalte oder
     * andersrum
     *
     * @param pos ...
     * @param includeEmptyCells Legt fest ob Elemente mit Inhalt "" ignoriert
     * werden
     * @return ...
     */
    public List<String> getOrthogonalChain(int pos, boolean includeEmptyCells) {
        List<String> result = new ArrayList<>();
        //String chainElement;
        if (!chains.isEmpty()) {
            for (List<String> chain : chains) {
                String element = "";
                if (pos < chain.size()) {
                    element = chain.get(pos);
                }
                if (includeEmptyCells) {
                    result.add(element);
                } else if (!element.isEmpty()) {
                    result.add(element);
                }
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.tag);
        hash = 89 * hash + Objects.hashCode(this.chains);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ExcelSheetRealm)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExcelSheetRealm other = (ExcelSheetRealm) obj;
        if (!Objects.equals(this.tag, other.tag)) {
            return false;
        }
        if (!Objects.equals(this.chains, other.chains)) {
            return false;
        }

        List<List<String>> otherChains = other.getChains();
        if (otherChains.size() != chains.size()) {
            return false;
        }

        Iterator<List<String>> otherIter = otherChains.iterator();
        Iterator<List<String>> thisIter = chains.iterator();

        while (thisIter.hasNext()) {
            if (!equalsStringList(otherIter.next(), thisIter.next())) {
                return false;
            }
        }

        return true;
    }

    private boolean equalsStringList(List<String> l1, List<String> l2) {
        if (l1.size() != l2.size()) {
            return false;
        }

        Iterator<String> iter1 = l1.iterator();
        Iterator<String> iter2 = l2.iterator();

        while (iter1.hasNext()) {
            if (!iter1.next().equals(iter2.next())) {
                return false;
            }
        }
        return true;
    }

    public int getSize() {
        return this.chains.size();
    }

}
