package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.filter.Filter;
import de.interfaceag.bmw.pzbk.filter.FilterToUrlConverter;
import de.interfaceag.bmw.pzbk.filter.SelectedDerivatUrlFilter;
import de.interfaceag.bmw.pzbk.filter.UrlFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;

public class FahrzeugmerkmalDerivatZuordnungFilter implements Serializable {

    private static final Page PAGE = Page.FAHRZEUGMERMAL_DERIVAT_ZUORDNUNG;

    @Filter
    private final UrlFilter derivatFilter;

    public FahrzeugmerkmalDerivatZuordnungFilter(UrlParameter urlParameter) {
        this.derivatFilter = SelectedDerivatUrlFilter.withParamterName("derivat").withUrlParameter(urlParameter).build();
    }

    public UrlFilter getDerivatFilter() {
        return derivatFilter;
    }

    public String filter() {
        return PAGE.getUrl() + getUrlParameter();
    }

    public String reset() {
        String derivatParameter = derivatFilter.getIndependentParameter();
        return PAGE.getUrl() + "?faces-redirect=true&" + derivatParameter;
    }

    private String getUrlParameter() {
        FilterToUrlConverter urlConverter = FilterToUrlConverter.forClass(this);
        return urlConverter.getUrl();
    }

}
