package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

public class FahrzeugmerkmalViewPermission implements Serializable {

    private final ViewPermission page;

    public FahrzeugmerkmalViewPermission(Set<Rolle> roles) {
        page = buildPageViewPermissions(roles);
    }

    private ViewPermission buildPageViewPermissions(Set<Rolle> rolesWithPermission) {
        return RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .compareTo(rolesWithPermission).get();
    }

    public boolean getPage() {
        return page.isRendered();
    }

}
