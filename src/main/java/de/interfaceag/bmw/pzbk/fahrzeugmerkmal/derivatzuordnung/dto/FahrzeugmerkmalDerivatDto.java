package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.FahrzeugmerkmalDerivatZuordnungStatus;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

public class FahrzeugmerkmalDerivatDto implements Serializable {

    private final FahrzeugmerkmalId fahrzeugmerkmalId;
    private final String fahrzeugmerkmalMerkmal;
    private final DerivatId derivatId;
    private final FahrzeugmerkmalDerivatZuordnungStatus status; // for icons
    private Boolean isNotRelevant;

    public FahrzeugmerkmalDerivatDto(FahrzeugmerkmalId fahrzeugmerkmalId, String fahrzeugmerkmalMerkmal, DerivatId derivatId, FahrzeugmerkmalDerivatZuordnungStatus status, Boolean isNotRelevant) {
        this.fahrzeugmerkmalId = fahrzeugmerkmalId;
        this.fahrzeugmerkmalMerkmal = fahrzeugmerkmalMerkmal;
        this.derivatId = derivatId;
        this.status = status;
        this.isNotRelevant = isNotRelevant;
    }

    public FahrzeugmerkmalId getFahrzeugmerkmalId() {
        return fahrzeugmerkmalId;
    }

    public String getFahrzeugmerkmalMerkmal() {
        return fahrzeugmerkmalMerkmal;
    }

    public DerivatId getDerivatId() {
        return derivatId;
    }

    public FahrzeugmerkmalDerivatZuordnungStatus getStatus() {
        return status;
    }

    public boolean isStatusNichtBewertet() {
        return status != FahrzeugmerkmalDerivatZuordnungStatus.NICHT_BEWERTET;
    }

    public void setNotRelevant(Boolean notRelevant) {
        isNotRelevant = notRelevant;
    }

    public Boolean getNotRelevant() {
        return isNotRelevant;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        FahrzeugmerkmalDerivatDto that = (FahrzeugmerkmalDerivatDto) object;

        return new EqualsBuilder()
                .append(fahrzeugmerkmalId, that.fahrzeugmerkmalId)
                .append(fahrzeugmerkmalMerkmal, that.fahrzeugmerkmalMerkmal)
                .append(derivatId, that.derivatId)
                .append(status, that.status)
                .append(isNotRelevant, that.isNotRelevant)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(fahrzeugmerkmalId)
                .append(fahrzeugmerkmalMerkmal)
                .append(derivatId)
                .append(status)
                .append(isNotRelevant)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "FahrzeugmerkmalDerivatDto{" +
                "fahrzeugmerkmalId=" + fahrzeugmerkmalId +
                ", fahrzeugmerkmalMerkmal='" + fahrzeugmerkmalMerkmal + '\'' +
                ", derivatId=" + derivatId +
                ", status=" + status +
                ", isNotRelevant=" + isNotRelevant +
                '}';
    }
}
