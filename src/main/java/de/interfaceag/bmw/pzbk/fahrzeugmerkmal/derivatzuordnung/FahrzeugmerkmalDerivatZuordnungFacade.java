package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalAuspraegungDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.SelectedFahrzeugmerkmalData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

@Stateless
public class FahrzeugmerkmalDerivatZuordnungFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(FahrzeugmerkmalDerivatZuordnungFacade.class);

    @Inject
    private FahrzeugmerkmalDerivatZuordnungService fahrzeugmerkmalDerivatZuordnungService;

    public SelectedFahrzeugmerkmalData getSelectedFahrzeugmerkmalDataForFahrzeugmerkmal(FahrzeugmerkmalDerivatDto selectedFahrzeugmerkmal) {
        LOG.debug("Get SelectedFahrzeugmerkmalData for {}", selectedFahrzeugmerkmal);
        final List<FahrzeugmerkmalAuspraegungDerivatDto> allAuspraegungen = fahrzeugmerkmalDerivatZuordnungService.getAllAuspraegungenForFahrzeugmerkmal(selectedFahrzeugmerkmal.getFahrzeugmerkmalId());
        final List<FahrzeugmerkmalAuspraegungDerivatDto> selectedAuspraegungen = fahrzeugmerkmalDerivatZuordnungService.getAlreadySelectedAuspraegungenForFahrzeugmerkmalAndDerivat(selectedFahrzeugmerkmal);
        return new SelectedFahrzeugmerkmalData(selectedFahrzeugmerkmal, allAuspraegungen, selectedAuspraegungen);
    }

    public void persistSelectedAuspraegung(SelectedFahrzeugmerkmalData selectedFahrzeugmerkmalData) {
        LOG.debug("Persist SelectedFahrzeugmerkmalData for {}", selectedFahrzeugmerkmalData);
        fahrzeugmerkmalDerivatZuordnungService.persistAuspraegung(selectedFahrzeugmerkmalData);
    }
}
