package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto;

import de.interfaceag.bmw.pzbk.converters.DomainObjectConverter;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalEditDialogData;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.apache.commons.lang.StringUtils;

import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class FahrzeugmerkmalEditDialogDataDto implements FahrzeugmerkmalEditDialogData, Serializable {

    private final boolean newObject;

    private final Long fahrzeugmerkmalId;
    private final String originalFahrzeugmerkmalLabel;
    private String fahrzeugmerkmalLabel;

    private final List<FahrzeugmerkmalModulSeTeamDto> allModulSeTeams;
    private List<FahrzeugmerkmalModulSeTeamDto> originalFahrzeugmerkmalModulSeTeams;
    private List<FahrzeugmerkmalModulSeTeamDto> fahrzeugmerkmalModulSeTeams;

    private String newAuspraegung;
    private List<FahrzeugmerkmalAuspraegungDto> originalAuspraegungen;
    private List<FahrzeugmerkmalAuspraegungDto> auspraegungen;


    public FahrzeugmerkmalEditDialogDataDto(List<FahrzeugmerkmalModulSeTeamDto> allModulSeTeams) {
        this.originalFahrzeugmerkmalLabel = StringUtils.EMPTY;
        this.fahrzeugmerkmalLabel = StringUtils.EMPTY;
        this.fahrzeugmerkmalId = null;
        this.allModulSeTeams = allModulSeTeams;
        this.fahrzeugmerkmalModulSeTeams = new ArrayList<>();
        this.originalFahrzeugmerkmalModulSeTeams = new ArrayList<>();
        this.originalAuspraegungen = new ArrayList<>();
        this.auspraegungen = new ArrayList<>();
        this.newObject = true;
    }


    public FahrzeugmerkmalEditDialogDataDto(FahrzeugmerkmalDto fahrzeugmerkmal, List<FahrzeugmerkmalModulSeTeamDto> allModulSeTeams, List<FahrzeugmerkmalModulSeTeamDto> fahrzeugmerkmalModulSeTeams, List<FahrzeugmerkmalAuspraegungDto> auspraegungen) {
        this.originalFahrzeugmerkmalLabel = fahrzeugmerkmal.getMerkmal();
        this.fahrzeugmerkmalLabel = fahrzeugmerkmal.getMerkmal();
        this.fahrzeugmerkmalId = fahrzeugmerkmal.getId();
        this.allModulSeTeams = allModulSeTeams;
        this.fahrzeugmerkmalModulSeTeams = fahrzeugmerkmalModulSeTeams;
        this.originalFahrzeugmerkmalModulSeTeams = new ArrayList<>(fahrzeugmerkmalModulSeTeams);
        this.originalAuspraegungen = auspraegungen.stream().map(FahrzeugmerkmalAuspraegungDto::new).collect(Collectors.toList());
        this.auspraegungen = auspraegungen;
        this.newObject = false;
    }

    @Override
    public String toString() {
        return "FahrzeugmerkmalEditDialogDataDto{" +
                "newObject=" + newObject +
                ", fahrzeugmerkmalId=" + fahrzeugmerkmalId +
                ", originalFahrzeugmerkmalLabel='" + originalFahrzeugmerkmalLabel + '\'' +
                ", fahrzeugmerkmalLabel='" + fahrzeugmerkmalLabel + '\'' +
                ", allModulSeTeams=" + allModulSeTeams +
                ", originalFahrzeugmerkmalModulSeTeams=" + originalFahrzeugmerkmalModulSeTeams +
                ", fahrzeugmerkmalModulSeTeams=" + fahrzeugmerkmalModulSeTeams +
                ", newAuspraegung='" + newAuspraegung + '\'' +
                ", originalAuspraegungen=" + originalAuspraegungen +
                ", auspraegungen=" + auspraegungen +
                '}';
    }

    @Override
    public FahrzeugmerkmalId getFahrzeugmerkmalId() {
        return new FahrzeugmerkmalId(fahrzeugmerkmalId);
    }

    @Override
    public String getFahrzeugmerkmalLabel() {
        return fahrzeugmerkmalLabel;
    }

    @Override
    public void setFahrzeugmerkmalLabel(String fahrzeugmerkmalLabel) {
        this.fahrzeugmerkmalLabel = fahrzeugmerkmalLabel;
    }

    @Override
    public List<FahrzeugmerkmalModulSeTeamDto> getModulSeTeams() {
        return fahrzeugmerkmalModulSeTeams;
    }

    @Override
    public void setModulSeTeams(List<FahrzeugmerkmalModulSeTeamDto> modulSeTeams) {
        this.fahrzeugmerkmalModulSeTeams = modulSeTeams;
    }

    @Override
    public Converter getModulConverter() {
        return new DomainObjectConverter<>(allModulSeTeams);
    }

    @Override
    public List<FahrzeugmerkmalModulSeTeamDto> completeModule(String query) {
        if (query == null || query.isEmpty()) {
            return Collections.emptyList();
        } else {
            return allModulSeTeams.stream()
                    .filter(modulSeTeamDto -> modulSeTeamDto.getBezeichnung().toLowerCase().contains(query.toLowerCase()))
                    .limit(10)
                    .collect(Collectors.toList());
        }
    }

    @Override
    public boolean isModuleChanged() {
        return !Objects.equals(originalFahrzeugmerkmalModulSeTeams, fahrzeugmerkmalModulSeTeams);
    }

    @Override
    public String getNewAuspraegung() {
        return newAuspraegung;
    }

    @Override
    public void setNewAuspraegung(String newAuspraegung) {
        this.newAuspraegung = newAuspraegung;
    }

    @Override
    public void addAuspraegung() {
        if (StringUtils.isNotBlank(newAuspraegung)) {
            final FahrzeugmerkmalAuspraegungDto newAuspraegungDto = new FahrzeugmerkmalAuspraegungDto(this.newAuspraegung);
            auspraegungen.add(newAuspraegungDto);
        }
        newAuspraegung = StringUtils.EMPTY;
    }

    @Override
    public List<FahrzeugmerkmalAuspraegungDto> getAuspraegungen() {
        return auspraegungen;
    }

    @Override
    public List<String> getAuspraegungenForValidation() {
        if (Objects.isNull(auspraegungen)) {
            return null;
        } else {
            return auspraegungen.stream().map(FahrzeugmerkmalAuspraegungDto::getAuspraegung).collect(Collectors.toList());
        }
    }

    @Override
    public void setAuspraegungenForValidation(List<String> auspraegungenForValidation) {
        // do nothing. do not delete. property auspraegungForValidation needs to be writeable!
    }

    @Override
    public void setAuspraegungen(List<FahrzeugmerkmalAuspraegungDto> auspraegungen) {
        this.auspraegungen = auspraegungen;
    }

    @Override
    public boolean isFahrzeugmerkmalLabelChanged() {
        return !Objects.equals(originalFahrzeugmerkmalLabel, fahrzeugmerkmalLabel);
    }

    @Override
    public boolean isAuspraegungenChanged() {
        return !Objects.equals(originalAuspraegungen, auspraegungen);
    }

    @Override
    public boolean isNewObject() {
        return newObject;
    }
}
