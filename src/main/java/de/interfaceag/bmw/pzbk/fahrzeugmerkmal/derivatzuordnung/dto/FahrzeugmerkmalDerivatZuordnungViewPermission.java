package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.TruePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

public class FahrzeugmerkmalDerivatZuordnungViewPermission implements Serializable {

    private final ViewPermission page;

    public FahrzeugmerkmalDerivatZuordnungViewPermission() {
        page = FalsePermission.get();
    }

    public FahrzeugmerkmalDerivatZuordnungViewPermission(Set<Rolle> roles, Collection<DerivatId> derivatIdsAsAnforderer, DerivatId currentDerivatId) {
        page = buildPageViewPermissions(roles, derivatIdsAsAnforderer, currentDerivatId);
    }

    private ViewPermission buildPageViewPermissions(Set<Rolle> rolesWithPermission, Collection<DerivatId> derivatIdsAsAnforderer, DerivatId currentDerivatId) {

        if (rolesWithPermission.contains(Rolle.ADMIN)) {
            return TruePermission.get();
        } else if (rolesWithPermission.contains(Rolle.ANFORDERER) && derivatIdsAsAnforderer.contains(currentDerivatId)) {
            return TruePermission.get();
        } else {
            return FalsePermission.get();
        }
    }

    public boolean getPage() {
        return page.isRendered();
    }

}
