package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung;

import de.interfaceag.bmw.pzbk.dao.GenericDao;
import de.interfaceag.bmw.pzbk.entities.AnforderungModulSeTeamFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;

import javax.enterprise.context.Dependent;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Dependent
public class AnforderungModulSeTeamFahrzeugmerkmalAuspraegungDao extends GenericDao<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> {

    public Collection<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> getByAnforderung(AnforderungId anforderungId) {
        TypedQuery<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> query = getEntityManager().createNamedQuery(AnforderungModulSeTeamFahrzeugmerkmalAuspraegung.BY_ANFORDERUNG, AnforderungModulSeTeamFahrzeugmerkmalAuspraegung.class);
        query.setParameter("anforderungId", anforderungId.getId());
        return query.getResultList();
    }

    public List<FahrzeugmerkmalAuspraegung> getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(AnforderungId anforderungId, ModulSeTeamId modulSeTeamId) {
        TypedQuery<FahrzeugmerkmalAuspraegung> query = getEntityManager().createNamedQuery(AnforderungModulSeTeamFahrzeugmerkmalAuspraegung.AUSPRAEGUNGEN_BY_ANFORDERUNG_AND_MODULSETEAM, FahrzeugmerkmalAuspraegung.class);
        query.setParameter("anforderungId", anforderungId.getId());
        query.setParameter("modulSeTeamId", modulSeTeamId.getId());
        return query.getResultList();
    }

    public Optional<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> getSelectedFahrzeugmerkmalAuspraegungenForAnforderungModulSeTeamAndFahrzeugmerkmalAuspraegung(AnforderungId anforderungId,
                                                                                                                                                                    ModulSeTeamId modulSeTeamId,
                                                                                                                                                                    FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId) {
        TypedQuery<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> query = getEntityManager().createNamedQuery(AnforderungModulSeTeamFahrzeugmerkmalAuspraegung.AUSPRAEGUNGEN_BY_ANFORDERUNG_MODULSETEAM_AND_FAHRZEUGMERKMAL_AUSPRAEGUNG, AnforderungModulSeTeamFahrzeugmerkmalAuspraegung.class);
        query.setParameter("anforderungId", anforderungId.getId());
        query.setParameter("modulSeTeamId", modulSeTeamId.getId());
        query.setParameter("fahrzeugmerkmalAuspraegungId", fahrzeugmerkmalAuspraegungId.getId());
        final List<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> result = query.getResultList();
        return result.isEmpty() ? Optional.empty() : Optional.ofNullable(result.get(0));
    }
}
