package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatZuordnungViewPermission;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

@Stateless
public class FahrzeugmerkmalDerivatZuordnungViewPermissionFactory implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(FahrzeugmerkmalDerivatZuordnungViewPermissionFactory.class);

    @Inject
    private Session session;

    public FahrzeugmerkmalDerivatZuordnungViewPermission buildFahrzeugmerkmalDerivatZuordnungViewPermissionForDerivatNotPresent() {
        return new FahrzeugmerkmalDerivatZuordnungViewPermission();
    }

    public FahrzeugmerkmalDerivatZuordnungViewPermission buildFahrzeugmerkmalDerivatZuordnungViewPermissionForDerivat(DerivatId currentDerivatId) {

        final UserPermissions<BerechtigungDto> userPermissions = session.getUserPermissions();
        final Set<Rolle> roles = userPermissions.getRoles();
        final Set<DerivatId> derivatIdsAsAnforderer = getDerivatIdsAsAnforderer(userPermissions);

        return new FahrzeugmerkmalDerivatZuordnungViewPermission(roles, derivatIdsAsAnforderer, currentDerivatId);
    }

    private static Set<DerivatId> getDerivatIdsAsAnforderer(UserPermissions<BerechtigungDto> userPermissions) {
        return userPermissions.getDerivatAsAnfordererSchreibend().stream().map(BerechtigungDto::getId).map(DerivatId::new).collect(Collectors.toSet());
    }
}
