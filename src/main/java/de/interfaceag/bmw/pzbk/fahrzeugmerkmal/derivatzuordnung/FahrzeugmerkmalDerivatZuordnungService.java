package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.converters.FahrzeugmerkmalAuspraegungDerivatDtoConverter;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalAuspraegungDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.SelectedFahrzeugmerkmalData;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Stateless
public class FahrzeugmerkmalDerivatZuordnungService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(FahrzeugmerkmalDerivatZuordnungService.class);

    @Inject
    private FahrzeugmerkmalDerivatZuordnungDao fahrzeugmerkmalDerivatZuordnungDao;
    @Inject
    private FahrzeugmerkmalService fahrzeugmerkmalService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private DerivatFahrzeugmerkmalService derivatFahrzeugmerkmalService;


    public void save(DerivatFahrzeugmerkmal derivatFahrzeugmerkmal) {
        fahrzeugmerkmalDerivatZuordnungDao.save(derivatFahrzeugmerkmal);
    }

    public List<FahrzeugmerkmalDerivatDto> getAllFahrzeugmerkmaleForDerivat(Derivat derivat) {
        List<FahrzeugmerkmalDerivatDto> alreadySetFahrzeugmerkmale = fahrzeugmerkmalDerivatZuordnungDao.getAllFahrzeugmerkmaleForDerivat(derivat.getDerivatId());

        List<Fahrzeugmerkmal> allFahrzeugmerkmale = fahrzeugmerkmalService.getAllFahrzeugmerkmaleNotBewertetForDerivat(derivat);

        List<FahrzeugmerkmalDerivatDto> result = allFahrzeugmerkmale.stream()
                .map(fahrzeugmerkmal ->
                        new FahrzeugmerkmalDerivatDto(
                                fahrzeugmerkmal.getFahrzeugmerkmalId(),
                                fahrzeugmerkmal.getMerkmal(),
                                derivat.getDerivatId(),
                                FahrzeugmerkmalDerivatZuordnungStatus.NICHT_BEWERTET,
                                null))
                .collect(Collectors.toList());

        result.addAll(alreadySetFahrzeugmerkmale);

        return result;
    }

    public List<FahrzeugmerkmalAuspraegungDerivatDto> getAlreadySelectedAuspraegungenForFahrzeugmerkmalAndDerivat(FahrzeugmerkmalDerivatDto selectedFahrzeugmerkmal) {
        final FahrzeugmerkmalId fahrzeugmerkmalId = selectedFahrzeugmerkmal.getFahrzeugmerkmalId();
        final DerivatId derivatId = selectedFahrzeugmerkmal.getDerivatId();
        return fahrzeugmerkmalDerivatZuordnungDao.getAlreadySelectedAuspraegungenForFahrzeugmerkmalAndDerivat(fahrzeugmerkmalId, derivatId);
    }

    public List<FahrzeugmerkmalAuspraegungDerivatDto> getAllAuspraegungenForFahrzeugmerkmal(FahrzeugmerkmalId fahrzeugmerkmalId) {
        final List<FahrzeugmerkmalAuspraegung> allAuspraegungenForFahrzeugmerkmal = fahrzeugmerkmalService.getAllAuspraegungenForFahrzeugmerkmal(fahrzeugmerkmalId);
        return FahrzeugmerkmalAuspraegungDerivatDtoConverter.forFahrzeugmerkmalAuspraeungen(allAuspraegungenForFahrzeugmerkmal);
    }

    public void persistAuspraegung(SelectedFahrzeugmerkmalData selectedFahrzeugmerkmalData) {
        Boolean isNotRelevant = selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto().getNotRelevant();
        if (isNotRelevant) {
            persistDerivatFahrzeugmerkmalNotRelevant(selectedFahrzeugmerkmalData);
        } else {
            persistSelectedAuspraegung(selectedFahrzeugmerkmalData);
        }
    }

    private void persistDerivatFahrzeugmerkmalNotRelevant(SelectedFahrzeugmerkmalData selectedFahrzeugmerkmalData) {
        LOG.debug("persist DerivatFahrzeugmerkmalNotRelevant");
        Derivat derivatFromSelectedFahrzeugmerkmal = getDerivatFromSelectedFahrzeugmerkmal(selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto().getDerivatId());

        final DerivatFahrzeugmerkmal derivatFahrzeugmerkmal = buildDerivatFahrzeugmerkmalFromSelectedFahrzeugmerkmalData(selectedFahrzeugmerkmalData);

        if (derivatFahrzeugmerkmal == null) {
            LOG.error("Couldn't find DerivatFahrzeugmerkmal and could not save for selectedFahrzeugmerkmalData {}", selectedFahrzeugmerkmalData);
            return;
        }

        Optional<Fahrzeugmerkmal> fahrzeugmerkmalOptional = findFahrzeugmerkmalFromSelectedFahrzeugmerkmal(selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto().getFahrzeugmerkmalId());
        if (!fahrzeugmerkmalOptional.isPresent()) {
            LOG.error("Couldn't find Fahrzeugmerkmal for {}", selectedFahrzeugmerkmalData);
        } else {
            derivatFahrzeugmerkmal.setFahrzeugmerkmal(fahrzeugmerkmalOptional.get());
        }
        derivatFahrzeugmerkmal.setDerivat(derivatFromSelectedFahrzeugmerkmal);
        derivatFahrzeugmerkmal.setAuspraegungen(Collections.emptyList());
        derivatFahrzeugmerkmal.setNotRelevant(Boolean.TRUE);

        fahrzeugmerkmalDerivatZuordnungDao.save(derivatFahrzeugmerkmal);
    }

    private void persistSelectedAuspraegung(SelectedFahrzeugmerkmalData selectedFahrzeugmerkmalData) {
        final DerivatFahrzeugmerkmal derivatFahrzeugmerkmal = buildDerivatFahrzeugmerkmalFromSelectedFahrzeugmerkmalData(selectedFahrzeugmerkmalData);
        if (derivatFahrzeugmerkmal == null) {
            LOG.error("Couldn't build a valid DerivatFahrzeugmerkmal for {}", selectedFahrzeugmerkmalData);
        } else {
            LOG.debug("persist SelectedAuspraegung");
            fahrzeugmerkmalDerivatZuordnungDao.save(derivatFahrzeugmerkmal);
        }
    }

    private DerivatFahrzeugmerkmal buildDerivatFahrzeugmerkmalFromSelectedFahrzeugmerkmalData(SelectedFahrzeugmerkmalData selectedFahrzeugmerkmalData) {

        Optional<Fahrzeugmerkmal> fahrzeugmerkmalOptional = findFahrzeugmerkmalFromSelectedFahrzeugmerkmal(selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto().getFahrzeugmerkmalId());
        List<FahrzeugmerkmalAuspraegung> fahrzeugmerkmalAuspraegungs = fahrzeugmerkmalService.findFahrzeugmerkmalAuspraegungFromSelectedFahrzeugmerkmal(selectedFahrzeugmerkmalData);

        if (!fahrzeugmerkmalOptional.isPresent()) {
            return null;
        }

        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal = derivatFahrzeugmerkmalService.getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId(
                selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto().getDerivatId(),
                selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto().getFahrzeugmerkmalId());

        if (derivatFahrzeugmerkmal != null) {
            // override saved auspraegung with the settings from the user
            derivatFahrzeugmerkmal.setAuspraegungen(findFahrzeugmerkmalAuspraegung(selectedFahrzeugmerkmalData.getSelectedAuspraegungen()));
            derivatFahrzeugmerkmal.setNotRelevant(Boolean.FALSE);
            return derivatFahrzeugmerkmal;
        }

        // init a new one
        return new DerivatFahrzeugmerkmal(getDerivatFromSelectedFahrzeugmerkmal(selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto().getDerivatId()),
                fahrzeugmerkmalOptional.get(),
                fahrzeugmerkmalAuspraegungs,
                Boolean.FALSE);

    }

    private Derivat getDerivatFromSelectedFahrzeugmerkmal(DerivatId derivatId) {
        return derivatService.getDerivatById(derivatId.getId());
    }

    private Optional<Fahrzeugmerkmal> findFahrzeugmerkmalFromSelectedFahrzeugmerkmal(FahrzeugmerkmalId fahrzeugmerkmalId) {
        return fahrzeugmerkmalService.findFahrzeugmerkmal(fahrzeugmerkmalId);
    }

    private List<FahrzeugmerkmalAuspraegung> findFahrzeugmerkmalAuspraegung(List<FahrzeugmerkmalAuspraegungDerivatDto> fahrzeugmerkmalAuspraegungDerivatDtos) {
        return fahrzeugmerkmalAuspraegungDerivatDtos.stream()
                .map(fahrzeugmerkmalAuspraegungDerivatDto ->
                        findFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungDerivatDto.getFahrzeugmerkmalAuspraegungId()).get())
                .collect(Collectors.toList());
    }

    private Optional<FahrzeugmerkmalAuspraegung> findFahrzeugmerkmalAuspraegung(FahrzeugmerkmalAuspraegungId auspraegungId) {
        return fahrzeugmerkmalService.findFahrzeugmerkmalAuspraegung(auspraegungId);
    }

}
