package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api;

import java.io.Serializable;

public interface FahrzeugmerkmalEditDialog extends Serializable {

    FahrzeugmerkmalEditDialogData getDialogData();

    String save();

    String hide();

}
