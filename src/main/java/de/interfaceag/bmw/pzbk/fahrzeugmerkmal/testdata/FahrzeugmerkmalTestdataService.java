package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.testdata;

import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;

@Stateless
public class FahrzeugmerkmalTestdataService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(FahrzeugmerkmalTestdataService.class);
    @Inject
    private ModulService modulService;
    @Inject
    private FahrzeugmerkmalService fahrzeugmerkmalService;

    public void generateTestdata() {
        if (fahrzeugmerkmalService.getAllFahrzeugmerkmalAuspraegungen().isEmpty()) {
            LOG.debug("Create Fahrzeugmerkmal Testdata");

            generateFahrzeugmerkmalHeckleuchte();
            generateFahrzeugmerkmalDach();
            generateFahrzeugmerkmalLenkrad();
            generateFahrzeugmalBlinker();
        }
    }

    private void generateFahrzeugmalBlinker() {
        Fahrzeugmerkmal fahrzeugmerkmal = new Fahrzeugmerkmal("Blinker");

        ModulSeTeam modulSeTeam = getModulSeTeam();
        if (modulSeTeam != null) {
            fahrzeugmerkmal.addModulSeTeam(modulSeTeam);
        }
        fahrzeugmerkmalService.saveFahrzeugmerkmal(fahrzeugmerkmal);

        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegungMitHupe = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "Hat einen");
        fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungMitHupe);

        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegungohneHupe = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "ist ein BMW");
        fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungohneHupe);

    }

    private void generateFahrzeugmerkmalLenkrad() {
        Fahrzeugmerkmal fahrzeugmerkmal = new Fahrzeugmerkmal("Lenkrad");

        ModulSeTeam modulSeTeam = getModulSeTeam();
        if (modulSeTeam != null) {
            fahrzeugmerkmal.addModulSeTeam(modulSeTeam);
        }
        fahrzeugmerkmalService.saveFahrzeugmerkmal(fahrzeugmerkmal);

        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegungMitHupe = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "Mit Hupe");
        fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungMitHupe);

        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegungohneHupe = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "ohne Hupe");
        fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungohneHupe);

        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegungKannSingen = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "Kann singen");
        fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungKannSingen);
    }

    private void generateFahrzeugmerkmalDach() {
        Fahrzeugmerkmal fahrzeugmerkmal = new Fahrzeugmerkmal("Dach");

        ModulSeTeam modulSeTeam = getModulSeTeam();
        if (modulSeTeam != null) {
            fahrzeugmerkmal.addModulSeTeam(modulSeTeam);
        }
        fahrzeugmerkmalService.saveFahrzeugmerkmal(fahrzeugmerkmal);

        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegungStoff = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "Stoff");
        fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungStoff);

        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegungMetall = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "Metall");
        fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungMetall);

        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegungGlas = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "Glas");
        fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungGlas);

        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegungKeins = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "Keins");
        fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungKeins);
    }

    private void generateFahrzeugmerkmalHeckleuchte() {
        Fahrzeugmerkmal fahrzeugmerkmal = new Fahrzeugmerkmal("Heckleuchten");

        ModulSeTeam modulSeTeam = getModulSeTeam();
        if (modulSeTeam != null) {
            fahrzeugmerkmal.addModulSeTeam(modulSeTeam);
        }
        fahrzeugmerkmalService.saveFahrzeugmerkmal(fahrzeugmerkmal);

        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegungRund = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "Rund");
        fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungRund);

        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegungEckig = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "Eckig");
        fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungEckig);
    }

    private ModulSeTeam getModulSeTeam() {
        return modulService.getSeTeamById(1L);
    }

}
