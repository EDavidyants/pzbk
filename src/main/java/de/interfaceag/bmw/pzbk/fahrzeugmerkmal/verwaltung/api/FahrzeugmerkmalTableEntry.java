package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api;

import java.io.Serializable;

public interface FahrzeugmerkmalTableEntry extends Serializable {

    String getMerkmal();

    String getAuspraegungen();

    String getModule();

}
