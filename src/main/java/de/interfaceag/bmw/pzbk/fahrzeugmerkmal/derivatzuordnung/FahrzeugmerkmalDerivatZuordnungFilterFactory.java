package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import java.io.Serializable;

@Stateless
public class FahrzeugmerkmalDerivatZuordnungFilterFactory implements Serializable {

    @Produces
    public FahrzeugmerkmalDerivatZuordnungFilter buildFahrzeugmerkmalDerivatZuordnungFilter() {
        UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();
        return new FahrzeugmerkmalDerivatZuordnungFilter(urlParameter);
    }

}
