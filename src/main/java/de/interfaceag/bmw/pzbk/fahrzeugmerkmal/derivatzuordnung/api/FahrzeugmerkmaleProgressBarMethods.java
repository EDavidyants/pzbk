package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.api;

import de.interfaceag.bmw.pzbk.entities.Derivat;

/**
 *
 * @author evda
 */
public interface FahrzeugmerkmaleProgressBarMethods {

    long getProgressValueForDerivat(Derivat derivat);

    String getProgressBarLabelForDerivat(Derivat derivat);

    boolean isAbgeschlossenForDerivat(Derivat derivat);

}
