package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto;

import de.interfaceag.bmw.pzbk.shared.objectIds.DomainObject;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class FahrzeugmerkmalAuspraegungDto implements DomainObject {

    private final Long id;
    private String auspraegung;
    private final String originalAuspraegung;


    public FahrzeugmerkmalAuspraegungDto(String auspraegung) {
        this.id = null;
        this.auspraegung = auspraegung;
        this.originalAuspraegung = null;
    }

    public FahrzeugmerkmalAuspraegungDto(Long id, String auspraegung) {
        this.id = id;
        this.auspraegung = auspraegung;
        this.originalAuspraegung = auspraegung;
    }

    public FahrzeugmerkmalAuspraegungDto(FahrzeugmerkmalAuspraegungDto original) {
        this.id = original.getId();
        this.auspraegung = original.getAuspraegung();
        this.originalAuspraegung = original.getAuspraegung();
    }

    @Override
    public String toString() {
        return "FahrzeugmerkmalModulSeTeamDto{" +
                "id=" + id +
                ", auspraegung='" + auspraegung + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        FahrzeugmerkmalAuspraegungDto that = (FahrzeugmerkmalAuspraegungDto) object;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(auspraegung, that.auspraegung)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(auspraegung)
                .toHashCode();
    }

    @Override
    public Long getId() {
        return id;
    }

    public FahrzeugmerkmalAuspraegungId getFahrzeugmerkmalAuspraegungId() {
        return new FahrzeugmerkmalAuspraegungId(id);
    }

    public String getAuspraegung() {
        return auspraegung;
    }

    public void setAuspraegung(String auspraegung) {
        this.auspraegung = auspraegung;
    }

    public boolean isChanged() {
        return originalAuspraegung != null && !originalAuspraegung.equals(auspraegung);
    }
}
