package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalTableData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalTableEntry;

import java.io.Serializable;
import java.util.List;

public class FahrzeugmerkmalViewData implements Serializable, FahrzeugmerkmalTableData {

    private final List<FahrzeugmerkmalTableEntry> fahrzeugmerkmalTableEntries;

    public FahrzeugmerkmalViewData(List<FahrzeugmerkmalTableEntry> fahrzeugmerkmalTableEntries) {
        this.fahrzeugmerkmalTableEntries = fahrzeugmerkmalTableEntries;
    }

    @Override
    public List<FahrzeugmerkmalTableEntry> getFahrzeugmerkmalTableEntries() {
        return fahrzeugmerkmalTableEntries;
    }

}
