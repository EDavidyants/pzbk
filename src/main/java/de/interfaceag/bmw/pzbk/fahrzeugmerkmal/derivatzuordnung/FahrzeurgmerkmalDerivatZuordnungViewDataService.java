package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatZuordnungViewData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatZuordnungViewPermission;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Stateless
public class FahrzeurgmerkmalDerivatZuordnungViewDataService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(FahrzeurgmerkmalDerivatZuordnungViewDataService.class);

    @Inject
    private DerivatService derivatService;
    @Inject
    private FahrzeugmerkmalDerivatZuordnungService fahrzeugmerkmalDerivatZuordnungService;
    @Inject
    private FahrzeugmerkmalDerivatZuordnungViewPermissionFactory fahrzeugmerkmalDerivatZuordnungViewPermissionFactory;

    @Produces
    public FahrzeugmerkmalDerivatZuordnungViewData getFahrzeugmerkmalDerivatZuordnungViewData() {
        UrlParameter urlParameter = UrlParameterUtils.getUrlParameter();
        return buildFahrzeugmerkmalDerivatZuordnungViewDataForUrlParameter(urlParameter);
    }

    FahrzeugmerkmalDerivatZuordnungViewData buildFahrzeugmerkmalDerivatZuordnungViewDataForUrlParameter(UrlParameter urlParameter) {

        final Optional<Derivat> derivatFromUrlParameter = getDerivatFromUrlParameter(urlParameter);

        if (derivatFromUrlParameter.isPresent()) {
            final Derivat derivat = derivatFromUrlParameter.get();
            return buildFahrzeugmerkmalDerivatZuordnungViewDataForDerivat(derivat);
        } else {
            return buildFahrzeugmerkmalDerivatZuordnungViewDataWithoutDerivat();
        }
    }

    private FahrzeugmerkmalDerivatZuordnungViewData buildFahrzeugmerkmalDerivatZuordnungViewDataForDerivat(Derivat derivat) {
        final List<FahrzeugmerkmalDerivatDto> allFahrzeugmerkmaleForDerivat = fahrzeugmerkmalDerivatZuordnungService.getAllFahrzeugmerkmaleForDerivat(derivat);
        final FahrzeugmerkmalDerivatZuordnungViewPermission viewPermission = fahrzeugmerkmalDerivatZuordnungViewPermissionFactory.buildFahrzeugmerkmalDerivatZuordnungViewPermissionForDerivat(derivat.getDerivatId());
        return new FahrzeugmerkmalDerivatZuordnungViewData(allFahrzeugmerkmaleForDerivat, viewPermission);
    }

    private FahrzeugmerkmalDerivatZuordnungViewData buildFahrzeugmerkmalDerivatZuordnungViewDataWithoutDerivat() {
        final List<FahrzeugmerkmalDerivatDto> allFahrzeugmerkmaleForDerivat = Collections.emptyList();
        final FahrzeugmerkmalDerivatZuordnungViewPermission viewPermission = fahrzeugmerkmalDerivatZuordnungViewPermissionFactory.buildFahrzeugmerkmalDerivatZuordnungViewPermissionForDerivatNotPresent();
        return new FahrzeugmerkmalDerivatZuordnungViewData(allFahrzeugmerkmaleForDerivat, viewPermission);
    }

    private Optional<Derivat> getDerivatFromUrlParameter(UrlParameter urlParameter) {
        final Optional<String> derivatName = urlParameter.getValue("derivat");
        if (derivatName.isPresent()) {
            return Optional.ofNullable(derivatService.getDerivatByName(derivatName.get()));
        } else {
            LOG.warn("No derivat parameter found in urlParameter");
            return Optional.empty();
        }
    }

}
