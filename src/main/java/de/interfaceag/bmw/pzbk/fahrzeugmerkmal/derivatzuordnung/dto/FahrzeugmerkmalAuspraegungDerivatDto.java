package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto;

import de.interfaceag.bmw.pzbk.shared.objectIds.DomainObject;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

public class FahrzeugmerkmalAuspraegungDerivatDto implements Serializable, DomainObject {

    private final FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId;
    private final String auspraegung;

    public FahrzeugmerkmalAuspraegungDerivatDto(FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId, String auspraegung) {
        this.fahrzeugmerkmalAuspraegungId = fahrzeugmerkmalAuspraegungId;
        this.auspraegung = auspraegung;
    }

    @Override
    public String toString() {
        return "FahrzeugmerkmalAuspraegungDerivatDto{" +
                "fahrzeugmerkmalAuspraegungId=" + fahrzeugmerkmalAuspraegungId +
                ", auspraegung='" + auspraegung + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        FahrzeugmerkmalAuspraegungDerivatDto that = (FahrzeugmerkmalAuspraegungDerivatDto) object;

        return new EqualsBuilder()
                .append(fahrzeugmerkmalAuspraegungId, that.fahrzeugmerkmalAuspraegungId)
                .append(auspraegung, that.auspraegung)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(fahrzeugmerkmalAuspraegungId)
                .append(auspraegung)
                .toHashCode();
    }

    @Override
    public Long getId() {
        return getFahrzeugmerkmalAuspraegungId().getId();
    }

    public FahrzeugmerkmalAuspraegungId getFahrzeugmerkmalAuspraegungId() {
        return fahrzeugmerkmalAuspraegungId;
    }

    public String getAuspraegung() {
        return auspraegung;
    }

}
