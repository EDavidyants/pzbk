package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalTableEntry;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalViewData;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class FahrzeugmerkmalViewDataService implements Serializable {

    @Inject
    private FahrzeugmerkmalService fahrzeugmerkmalService;

    @Produces
    public FahrzeugmerkmalViewData getViewData() {
        List<FahrzeugmerkmalTableEntry> entries = getFahrzeugmerkmalTableEntries();
        return new FahrzeugmerkmalViewData(entries);
    }

    private List<FahrzeugmerkmalTableEntry> getFahrzeugmerkmalTableEntries() {
        final Collection<FahrzeugmerkmalDto> allFahrzeugmerkmale = fahrzeugmerkmalService.getAllFahrzeugmerkmalTableEntries();
        return sortDtos(allFahrzeugmerkmale);
    }

    private static List<FahrzeugmerkmalTableEntry> sortDtos(Collection<FahrzeugmerkmalDto> allFahrzeugmerkmale) {
        return allFahrzeugmerkmale.stream().sorted().collect(Collectors.toList());
    }

}
