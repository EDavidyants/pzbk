package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.testdata;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.FahrzeugmerkmalDerivatZuordnungService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.FahrzeugmerkmalDerivatZuordnungStatus;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;

@Stateless
public class FahrzeugmerkmalDerivatZuordnungTestdataService implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(FahrzeugmerkmalDerivatZuordnungTestdataService.class);
    Random random = new Random();

    @Inject
    private FahrzeugmerkmalService fahrzeugmerkmalService;
    @Inject
    private FahrzeugmerkmalDerivatZuordnungService fahrzeugmerkmalDerivatZuordnungService;
    @Inject
    private DerivatService derivatService;

    public void generateTestdata() {
        final Derivat derivatE24 = derivatService.getDerivatByName("E24");
        if (!Objects.nonNull(derivatE24)) {
            LOG.error("Derivat E24 not found");
            return;
        }
        if (!isE24AlreadyConfigured(derivatE24)) {

            generateTestdataForDerivatE24(derivatE24);
            generateTestDataForRemainingDerivate(derivatE24);
        }
    }

    private void generateTestDataForRemainingDerivate(Derivat derivatE24) {
        List<Derivat> derivate = derivatService.getAllDerivate();
        derivate.remove(derivatE24);
        if (!derivate.isEmpty()) {
            for (Derivat derivat : derivate) {
                generateFahrzeugmerkmaleTestDataForDerivat(derivat);
            }
        }
    }

    private void generateFahrzeugmerkmaleTestDataForDerivat(Derivat derivat) {

        List<FahrzeugmerkmalDerivatDto> allFahrzeugmerkmaleForDerivat = fahrzeugmerkmalDerivatZuordnungService.getAllFahrzeugmerkmaleForDerivat(derivat);

        for (FahrzeugmerkmalDerivatDto fahrzeugmerkmalDerivatDto : allFahrzeugmerkmaleForDerivat) {
            Optional<Fahrzeugmerkmal> fahrzeugmerkmal = fahrzeugmerkmalService.findFahrzeugmerkmal(fahrzeugmerkmalDerivatDto.getFahrzeugmerkmalId());

            if (!fahrzeugmerkmal.isPresent()) {
                LOG.error("Couldn't find a valid fahrzeugmerkmal for {}", fahrzeugmerkmalDerivatDto);
                return;
            }

            // 0 = nothing, 1 = auspraegungen, 2 = notRelevant
            int actionToDo = random.nextInt(3);

            if (actionToDo == 1) {
                randomlySetAuspraegungenForDerivatAndSave(derivat, fahrzeugmerkmal.get());
            }

            if (actionToDo == 2) {
                DerivatFahrzeugmerkmal derivatFahrzeugmerkmal = new DerivatFahrzeugmerkmal(derivat, fahrzeugmerkmal.get(), Collections.emptyList(), true);
                LOG.debug("Creating new Derivat Fahrzeugmerkmale {}", derivatFahrzeugmerkmal);
                fahrzeugmerkmalDerivatZuordnungService.save(derivatFahrzeugmerkmal);
            }
        }
    }

    private void randomlySetAuspraegungenForDerivatAndSave(Derivat derivat, Fahrzeugmerkmal fahrzeugmerkmal) {
        List<FahrzeugmerkmalAuspraegung> allAuspraegungenForFahrzeugmerkmal = fahrzeugmerkmalService.getAllAuspraegungenForFahrzeugmerkmal(new FahrzeugmerkmalId(fahrzeugmerkmal.getId()));

        for (int i = 0; i < allAuspraegungenForFahrzeugmerkmal.size(); i++) {
            // 0 = don't save, 1 = save For DerivatFahrzeugmerkmal
            int action = random.nextInt(2);

            if (action == 0) {
                allAuspraegungenForFahrzeugmerkmal.remove(0);
            }
        }
        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal = new DerivatFahrzeugmerkmal(derivat, fahrzeugmerkmal, allAuspraegungenForFahrzeugmerkmal, false);
        LOG.debug("Creating new Derivat Fahrzeugmerkmale {}", derivatFahrzeugmerkmal);
        fahrzeugmerkmalDerivatZuordnungService.save(new DerivatFahrzeugmerkmal(derivat, fahrzeugmerkmal, allAuspraegungenForFahrzeugmerkmal, false));

    }

    private boolean isE24AlreadyConfigured(Derivat derivatE24) {
        List<FahrzeugmerkmalDerivatDto> allFahrzeugmerkmaleForDerivat = fahrzeugmerkmalDerivatZuordnungService.getAllFahrzeugmerkmaleForDerivat(derivatE24);
        for (FahrzeugmerkmalDerivatDto fahrzeugmerkmalDerivatDto : allFahrzeugmerkmaleForDerivat) {
            if (fahrzeugmerkmalDerivatDto.getStatus() != FahrzeugmerkmalDerivatZuordnungStatus.NICHT_BEWERTET) {
                return true;
            }
        }
        return false;
    }


    private void generateTestdataForDerivatE24(Derivat derivat) {
        if (!getAllFahrzeugmerkmaleForDerivat(derivat).isEmpty()) {
            generateTestdataForDerivat(derivat);
        } else {
            LOG.debug("Data for E24 found. No testdata will be created");
        }
    }

    private List<FahrzeugmerkmalDerivatDto> getAllFahrzeugmerkmaleForDerivat(Derivat derivat) {
        return fahrzeugmerkmalDerivatZuordnungService.getAllFahrzeugmerkmaleForDerivat(derivat);
    }

    private void generateTestdataForDerivat(Derivat derivat) {
        final List<FahrzeugmerkmalAuspraegung> allFahrzeugmerkmalAuspraegungen = fahrzeugmerkmalService.getAllFahrzeugmerkmalAuspraegungen();

        if (!allFahrzeugmerkmalAuspraegungen.isEmpty()) {
            generateTestdataForFahrzeugmerkmalAuspraegungen(derivat, allFahrzeugmerkmalAuspraegungen);
        } else {
            LOG.warn("Not FahrzeugmerkmalAuspraegung found");
        }
    }

    private void generateTestdataForFahrzeugmerkmalAuspraegungen(Derivat derivat, List<FahrzeugmerkmalAuspraegung> allFahrzeugmerkmalAuspraegungen) {
        final FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung = allFahrzeugmerkmalAuspraegungen.get(0);
        generateTestdataForFahrzeugmerkmalAuspraegung(derivat, fahrzeugmerkmalAuspraegung);
    }

    private void generateTestdataForFahrzeugmerkmalAuspraegung(Derivat derivat, FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung) {
        List<FahrzeugmerkmalAuspraegung> auspraegungen = Collections.singletonList(fahrzeugmerkmalAuspraegung);
        Fahrzeugmerkmal fahrzeugmerkmal = fahrzeugmerkmalAuspraegung.getFahrzeugmerkmal();
        DerivatFahrzeugmerkmal newDerivatFahrzeugmerkmal = new DerivatFahrzeugmerkmal(derivat, fahrzeugmerkmal, auspraegungen);
        LOG.debug("Create new {}", newDerivatFahrzeugmerkmal);
        fahrzeugmerkmalDerivatZuordnungService.save(newDerivatFahrzeugmerkmal);
    }

}
