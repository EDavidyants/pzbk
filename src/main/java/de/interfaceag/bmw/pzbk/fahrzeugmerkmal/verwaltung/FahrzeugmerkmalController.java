package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalEditDialog;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalEditDialogData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalTableMethods;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalViewData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalViewPermission;
import org.primefaces.context.RequestContext;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class FahrzeugmerkmalController implements Serializable, FahrzeugmerkmalEditDialog,
        FahrzeugmerkmalTableMethods {

    @Inject
    private FahrzeugmerkmalViewFacade facade;

    @Inject
    private FahrzeugmerkmalViewData viewData;

    @Inject
    private FahrzeugmerkmalViewPermission viewPermission;

    private FahrzeugmerkmalEditDialogData editDialogData;

    public FahrzeugmerkmalViewData getViewData() {
        return viewData;
    }

    public void updateEditDialog(FahrzeugmerkmalDto fahrzeugmerkmal) {
        editDialogData = facade.getEditDialogData(fahrzeugmerkmal);
        showEditDialog();
    }

    public void showNewMerkmalDialog() {
        editDialogData = facade.getEditDialogDataForNewFahrzeugmerkmal();
        showEditDialog();
    }

    private void showEditDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialogs:editDialogContent:editDialogForm");
        context.execute("PF('fahrzeugmerkmalEditDialog').show();");
    }

    @Override
    public FahrzeugmerkmalEditDialogData getDialogData() {
        return editDialogData;
    }

    @Override
    public String save() {
        return facade.saveEditDialog(editDialogData);
    }

    @Override
    public String hide() {
        return Page.FAHRZEUGMERKMAL_KONFIGURATION.getUrl() + "?faces-redirect=true";
    }

    public FahrzeugmerkmalViewPermission getViewPermission() {
        return viewPermission;
    }
}
