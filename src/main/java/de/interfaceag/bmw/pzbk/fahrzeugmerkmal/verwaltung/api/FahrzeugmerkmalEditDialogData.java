package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalAuspraegungDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalModulSeTeamDto;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;

import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.List;

public interface FahrzeugmerkmalEditDialogData extends Serializable {

    boolean isNewObject();

    FahrzeugmerkmalId getFahrzeugmerkmalId();

    String getFahrzeugmerkmalLabel();

    void setFahrzeugmerkmalLabel(String fahrzeugmerkmalLabel);

    boolean isFahrzeugmerkmalLabelChanged();

    // modules

    List<FahrzeugmerkmalModulSeTeamDto> getModulSeTeams();

    void setModulSeTeams(List<FahrzeugmerkmalModulSeTeamDto> modulSeTeams);

    Converter getModulConverter();

    List<FahrzeugmerkmalModulSeTeamDto> completeModule(String query);

    boolean isModuleChanged();

    // auspraegungen

    String getNewAuspraegung();

    void setNewAuspraegung(String newAuspraegung);

    void addAuspraegung();

    List<FahrzeugmerkmalAuspraegungDto> getAuspraegungen();

    List<String> getAuspraegungenForValidation();

    void setAuspraegungenForValidation(List<String> auspraegungenForValidation);

    void setAuspraegungen(List<FahrzeugmerkmalAuspraegungDto> auspraegungen);

    boolean isAuspraegungenChanged();

}
