package de.interfaceag.bmw.pzbk.fahrzeugmerkmal;

import de.interfaceag.bmw.pzbk.dao.GenericDao;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.persistence.TypedQuery;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Dependent
public class FahrzeugmerkmalAuspraegungDao extends GenericDao<FahrzeugmerkmalAuspraegung> {

    public static final Logger LOG = LoggerFactory.getLogger(FahrzeugmerkmalAuspraegungDao.class);

    public List<FahrzeugmerkmalAuspraegung> getAll() {
        TypedQuery<FahrzeugmerkmalAuspraegung> query = getEntityManager().createNamedQuery(FahrzeugmerkmalAuspraegung.ALL, FahrzeugmerkmalAuspraegung.class);
        return query.getResultList();
    }

    public List<FahrzeugmerkmalAuspraegung> getAllAuspraegungenForFahrzeugmerkmal(FahrzeugmerkmalId fahrzeugmerkmalId) {
        if (Objects.isNull(fahrzeugmerkmalId)) {
            LOG.warn("No value for fahrzeugmerkmalId set. Return empty list!");
            return Collections.emptyList();
        } else {
            TypedQuery<FahrzeugmerkmalAuspraegung> query = getEntityManager().createNamedQuery(FahrzeugmerkmalAuspraegung.ALL_FOR_FAHRZEUGMERKMAL, FahrzeugmerkmalAuspraegung.class);
            query.setParameter("fahrzeugmerkmalId", fahrzeugmerkmalId.getId());
            return query.getResultList();
        }
    }

}
