package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.history;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.AnforderungModulSeTeamFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.session.Session;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;

@Stateless
public class AnforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService implements Serializable {

    @Inject
    private AnforderungMeldungHistoryService anforderungMeldungHistoryService;
    @Inject
    private Session session;

    public void writeAnforderungFahrzeugmerkmalAuspraegungHinzugefuegtToHistory(AnforderungModulSeTeamFahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegung) {
        AnforderungHistory anforderungHistory = buildAnforderungHistory(anforderungModulSeTeamFahrzeugmerkmalAuspraegung, true, session.getUser().toString());
        anforderungMeldungHistoryService.persistAnforderungHistory(anforderungHistory);
    }


    public void writeAnforderungFahrzeugmerkmalAuspraegungGeloeschtToHistory(AnforderungModulSeTeamFahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegung) {
        AnforderungHistory anforderungHistory = buildAnforderungHistory(anforderungModulSeTeamFahrzeugmerkmalAuspraegung, false, session.getUser().toString());
        anforderungMeldungHistoryService.persistAnforderungHistory(anforderungHistory);
    }

    AnforderungHistory buildAnforderungHistory(AnforderungModulSeTeamFahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegung, boolean auspraegungHinzugefuegt, String benutzer) {
        AnforderungHistory anforderungHistory = new AnforderungHistory();
        anforderungHistory.setKennzeichen("A");
        anforderungHistory.setObjektName(anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getAttributeStringForHistory());
        anforderungHistory.setAnforderungId(anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getAnforderung().getId());
        anforderungHistory.setBenutzer(benutzer);
        anforderungHistory.setVon("Ausprägung: " + anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegung().getAuspraegung());

        if (auspraegungHinzugefuegt) {
            anforderungHistory.setAuf("hinzugefügt");
        } else {
            anforderungHistory.setAuf("entfernt");
        }

        return anforderungHistory;


    }

}
