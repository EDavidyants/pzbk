package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api;

import java.io.Serializable;
import java.util.List;

public interface FahrzeugmerkmalTableData extends Serializable {

    List<FahrzeugmerkmalTableEntry> getFahrzeugmerkmalTableEntries();

}
