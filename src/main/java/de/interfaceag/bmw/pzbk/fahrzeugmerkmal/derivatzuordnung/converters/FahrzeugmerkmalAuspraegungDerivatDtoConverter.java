package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.converters;

import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalAuspraegungDerivatDto;

import java.util.List;
import java.util.stream.Collectors;

public final class FahrzeugmerkmalAuspraegungDerivatDtoConverter {

    private FahrzeugmerkmalAuspraegungDerivatDtoConverter() {
    }

    public static List<FahrzeugmerkmalAuspraegungDerivatDto> forFahrzeugmerkmalAuspraeungen(List<FahrzeugmerkmalAuspraegung> fahrzeugmerkmalAuspraegungen) {
        return fahrzeugmerkmalAuspraegungen.stream()
                .map(fahrzeugmerkmalAuspraegung -> new FahrzeugmerkmalAuspraegungDerivatDto(fahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegungId(), fahrzeugmerkmalAuspraegung.getAuspraegung()))
                .collect(Collectors.toList());
    }


}
