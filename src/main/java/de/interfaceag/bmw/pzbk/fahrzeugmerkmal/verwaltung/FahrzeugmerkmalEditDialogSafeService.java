package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung;

import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalEditDialogData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalAuspraegungDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalModulSeTeamDto;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Stateless
public class FahrzeugmerkmalEditDialogSafeService {

    public static final Logger LOG = LoggerFactory.getLogger(FahrzeugmerkmalEditDialogSafeService.class);

    @Inject
    private FahrzeugmerkmalService fahrzeugmerkmalService;
    @Inject
    private ModulService modulService;

    public Fahrzeugmerkmal save(FahrzeugmerkmalEditDialogData editDialogData) {
        LOG.info("save edit dialog {}", editDialogData);

        if (editDialogData.isNewObject()) {
            return createNewFahrzeugmerkmal(editDialogData);
        } else {
            return updateExistingFahrzeugmerkmal(editDialogData);
        }
    }

    private Fahrzeugmerkmal createNewFahrzeugmerkmal(FahrzeugmerkmalEditDialogData editDialogData) {
        LOG.debug("Start Create new Fahrzeugmerkmal");

        if (Objects.isNull(editDialogData.getAuspraegungen()) || editDialogData.getAuspraegungen().isEmpty()) {
            LOG.error("No Auspraegung selected for new Fahrzeugmerkmal. Do not safe data to ensure consistency");
            return null;
        }

        final String merkmal = editDialogData.getFahrzeugmerkmalLabel();

        Fahrzeugmerkmal newFahrzeugmerkmal = new Fahrzeugmerkmal(merkmal);
        fahrzeugmerkmalService.saveFahrzeugmerkmal(newFahrzeugmerkmal);
        LOG.debug("Save new {}", newFahrzeugmerkmal);

        updateChangesForFahrzeugmerkmal(editDialogData, newFahrzeugmerkmal);

        LOG.info("New {} created", newFahrzeugmerkmal);

        return newFahrzeugmerkmal;
    }

    private Fahrzeugmerkmal updateExistingFahrzeugmerkmal(FahrzeugmerkmalEditDialogData editDialogData) {
        LOG.debug("Update exising Fahrzeugmerkmal");

        final FahrzeugmerkmalId fahrzeugmerkmalId = editDialogData.getFahrzeugmerkmalId();
        final Optional<Fahrzeugmerkmal> fahrzeugmerkmalById = fahrzeugmerkmalService.findFahrzeugmerkmal(fahrzeugmerkmalId);

        if (fahrzeugmerkmalById.isPresent()) {
            final Fahrzeugmerkmal fahrzeugmerkmal = fahrzeugmerkmalById.get();
            updateChangesForFahrzeugmerkmal(editDialogData, fahrzeugmerkmal);
            return fahrzeugmerkmal;
        } else {
            LOG.error("No Fahrzeugmerkmal found for {}", fahrzeugmerkmalId);
            return null;
        }
    }

    private void updateChangesForFahrzeugmerkmal(FahrzeugmerkmalEditDialogData editDialogData, Fahrzeugmerkmal fahrzeugmerkmal) {
        LOG.debug("Update changes for {}", fahrzeugmerkmal);

        if (editDialogData.isFahrzeugmerkmalLabelChanged()) {
            updateFahrzeugmerkmalMerkmal(editDialogData, fahrzeugmerkmal);
        }

        if (editDialogData.isModuleChanged()) {
            updateModuleForFahrzeugmerkmal(editDialogData, fahrzeugmerkmal);
        }

        if (editDialogData.isAuspraegungenChanged()) {
            createOrUpdateAuspraegungen(editDialogData, fahrzeugmerkmal);
        }
    }

    private void updateModuleForFahrzeugmerkmal(FahrzeugmerkmalEditDialogData editDialogData, Fahrzeugmerkmal fahrzeugmerkmal) {
        LOG.debug("Update module for {}", fahrzeugmerkmal);

        final List<ModulSeTeamId> selectedModulSeTeamIds = getSelectedModulSeTeamids(editDialogData);
        final List<ModulSeTeamId> fahrzeugmerkmalModulSeTeamIds = getFahrzeugmerkmalModulSeTeamIds(fahrzeugmerkmal);

        addNewModulSeTeams(fahrzeugmerkmal, selectedModulSeTeamIds, fahrzeugmerkmalModulSeTeamIds);
        removeRemovedModulSeTeams(fahrzeugmerkmal, selectedModulSeTeamIds, fahrzeugmerkmalModulSeTeamIds);
    }

    private static List<ModulSeTeamId> getFahrzeugmerkmalModulSeTeamIds(Fahrzeugmerkmal fahrzeugmerkmal) {
        final List<ModulSeTeam> fahrzeugmerkmalModulSeTeams = fahrzeugmerkmal.getModulSeTeams();
        return fahrzeugmerkmalModulSeTeams.stream().map(ModulSeTeam::getModulSeTeamId).collect(Collectors.toList());
    }

    private static List<ModulSeTeamId> getSelectedModulSeTeamids(FahrzeugmerkmalEditDialogData editDialogData) {
        final List<FahrzeugmerkmalModulSeTeamDto> selectedModulSeTeams = editDialogData.getModulSeTeams();
        if (Objects.isNull(selectedModulSeTeams)) {
            return Collections.emptyList();
        } else {
            return selectedModulSeTeams.stream().map(FahrzeugmerkmalModulSeTeamDto::getModulSeTeamId).collect(Collectors.toList());
        }
    }

    private void removeRemovedModulSeTeams(Fahrzeugmerkmal fahrzeugmerkmal, List<ModulSeTeamId> selectedModulSeTeamIds, List<ModulSeTeamId> fahrzeugmerkmalModulSeTeamIds) {
        List<ModulSeTeamId> removedModulSeTeamIds = new ArrayList<>(fahrzeugmerkmalModulSeTeamIds);
        removedModulSeTeamIds.removeAll(selectedModulSeTeamIds);

        for (ModulSeTeamId removedModulSeTeamId : removedModulSeTeamIds) {
            final Optional<ModulSeTeam> modulSeTeamById = Optional.ofNullable(modulService.getSeTeamById(removedModulSeTeamId.getId()));
            if (modulSeTeamById.isPresent()) {
                final ModulSeTeam modulSeTeam = modulSeTeamById.get();
                LOG.debug("Remove {} from {}", modulSeTeam, fahrzeugmerkmal);
                fahrzeugmerkmal.removeModulSeTeam(modulSeTeam);
            } else {
                LOG.error("No ModulSeTeam found for {}", removedModulSeTeamId);
            }
        }
    }

    private void addNewModulSeTeams(Fahrzeugmerkmal fahrzeugmerkmal, List<ModulSeTeamId> selectedModulSeTeamIds, List<ModulSeTeamId> fahrzeugmerkmalModulSeTeamIds) {
        List<ModulSeTeamId> newModulSeTeamIds = new ArrayList<>(selectedModulSeTeamIds);
        newModulSeTeamIds.removeAll(fahrzeugmerkmalModulSeTeamIds);

        for (ModulSeTeamId newModulSeTeamId : newModulSeTeamIds) {
            final Optional<ModulSeTeam> modulSeTeamById = Optional.ofNullable(modulService.getSeTeamById(newModulSeTeamId.getId()));
            if (modulSeTeamById.isPresent()) {
                final ModulSeTeam modulSeTeam = modulSeTeamById.get();
                LOG.debug("Add {} to {}", modulSeTeam, fahrzeugmerkmal);
                fahrzeugmerkmal.addModulSeTeam(modulSeTeam);
            } else {
                LOG.error("No ModulSeTeam found for {}", newModulSeTeamId);
            }
        }
    }

    private void updateFahrzeugmerkmalMerkmal(FahrzeugmerkmalEditDialogData editDialogData, Fahrzeugmerkmal fahrzeugmerkmal) {
        final String merkmal = editDialogData.getFahrzeugmerkmalLabel();
        LOG.debug("Update merkmal value for {} to {}", fahrzeugmerkmal, merkmal);
        fahrzeugmerkmal.setMerkmal(merkmal);
    }

    private void createOrUpdateAuspraegungen(FahrzeugmerkmalEditDialogData editDialogData, Fahrzeugmerkmal fahrzeugmerkmal) {
        final List<FahrzeugmerkmalAuspraegungDto> auspraegungen = editDialogData.getAuspraegungen();
        addNewAuspraegungenToFahrzeugmerkmal(fahrzeugmerkmal, auspraegungen);
        updateChangedAuspraegungen(auspraegungen);
    }

    private void updateChangedAuspraegungen(List<FahrzeugmerkmalAuspraegungDto> auspraegungen) {
        final Iterator<FahrzeugmerkmalAuspraegungDto> changedAuspraegungen = auspraegungen.stream().filter(FahrzeugmerkmalAuspraegungDto::isChanged).iterator();
        while (changedAuspraegungen.hasNext()) {
            final FahrzeugmerkmalAuspraegungDto changedAuspraegung = changedAuspraegungen.next();
            updateChangedAuspraegungIfPresent(changedAuspraegung);
        }
    }

    private void updateChangedAuspraegungIfPresent(FahrzeugmerkmalAuspraegungDto changedAuspraegung) {
        final FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId = changedAuspraegung.getFahrzeugmerkmalAuspraegungId();
        final Optional<FahrzeugmerkmalAuspraegung> fahrzeugmerkmalAuspraegungById = fahrzeugmerkmalService.findFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungId);
        if (fahrzeugmerkmalAuspraegungById.isPresent()) {
            final FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung = fahrzeugmerkmalAuspraegungById.get();
            updateChangedAuspraegung(changedAuspraegung, fahrzeugmerkmalAuspraegung);
        } else {
            LOG.error("No FahrzeugmerkmalAuspraegung found for {}", fahrzeugmerkmalAuspraegungId);
        }
    }

    private void updateChangedAuspraegung(FahrzeugmerkmalAuspraegungDto changedAuspraegung, FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung) {
        final String auspraegung = changedAuspraegung.getAuspraegung();
        LOG.info("Update changed {} to {}", fahrzeugmerkmalAuspraegung, auspraegung);
        fahrzeugmerkmalAuspraegung.setAuspraegung(auspraegung);
        fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegung);
    }

    private void addNewAuspraegungenToFahrzeugmerkmal(Fahrzeugmerkmal fahrzeugmerkmal, List<FahrzeugmerkmalAuspraegungDto> auspraegungen) {
        final Iterator<FahrzeugmerkmalAuspraegungDto> newAuspraegungen = auspraegungen.stream().filter(auspraegung -> Objects.isNull(auspraegung.getId())).iterator();
        while (newAuspraegungen.hasNext()) {
            final FahrzeugmerkmalAuspraegungDto newAuspraegung = newAuspraegungen.next();
            FahrzeugmerkmalAuspraegung newFahrzeugmerkmalAuspraegung = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, newAuspraegung.getAuspraegung());
            fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(newFahrzeugmerkmalAuspraegung);
            LOG.info("Create new {}", newFahrzeugmerkmalAuspraegung);
        }
    }

}
