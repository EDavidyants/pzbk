package de.interfaceag.bmw.pzbk.fahrzeugmerkmal;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.SelectedFahrzeugmerkmalData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalAuspraegungDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalModulSeTeamDto;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Stateless
public class FahrzeugmerkmalService {

    @Inject
    private FahrzeugmerkmalDao fahrzeugmerkmalDao;
    @Inject
    private FahrzeugmerkmalAuspraegungDao fahrzeugmerkmalAuspraegungDao;


    public List<Fahrzeugmerkmal> getAllFahrzeugmerkmaleNotBewertetForDerivat(Derivat derivat) {
        return fahrzeugmerkmalDao.getAllFahrzeugmerkmaleNotBewertetForDerivat(derivat);
    }

    public Optional<Fahrzeugmerkmal> findFahrzeugmerkmal(FahrzeugmerkmalId fahrzeugmerkmalId) {
        return fahrzeugmerkmalDao.find(fahrzeugmerkmalId);
    }

    public Optional<FahrzeugmerkmalAuspraegung> findFahrzeugmerkmalAuspraegung(FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId) {
        return fahrzeugmerkmalAuspraegungDao.find(fahrzeugmerkmalAuspraegungId);

    }

    public void saveFahrzeugmerkmal(Fahrzeugmerkmal fahrzeugmerkmal) {
        fahrzeugmerkmalDao.save(fahrzeugmerkmal);
    }

    public void saveFahrzeugmerkmalAuspraegung(FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung) {
        fahrzeugmerkmalAuspraegungDao.save(fahrzeugmerkmalAuspraegung);
    }

    public List<FahrzeugmerkmalAuspraegung> getAllFahrzeugmerkmalAuspraegungen() {
        return fahrzeugmerkmalAuspraegungDao.getAll();
    }

    public List<FahrzeugmerkmalAuspraegung> getAllAuspraegungenForFahrzeugmerkmal(FahrzeugmerkmalId fahrzeugmerkmalId) {
        return fahrzeugmerkmalAuspraegungDao.getAllAuspraegungenForFahrzeugmerkmal(fahrzeugmerkmalId);
    }

    public Collection<FahrzeugmerkmalDto> getAllFahrzeugmerkmalTableEntries() {
        List<FahrzeugmerkmalAuspraegung> allFahrzeugmerkmalAuspraegungen = fahrzeugmerkmalAuspraegungDao.getAll();

        Map<Fahrzeugmerkmal, FahrzeugmerkmalDto> fahrzeugmerkmalDtoMap = new HashMap<>();
        for (FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung : allFahrzeugmerkmalAuspraegungen) {
            addFahrzeugmerkmalAuspraegungToMap(fahrzeugmerkmalDtoMap, fahrzeugmerkmalAuspraegung);
        }

        return fahrzeugmerkmalDtoMap.values();
    }

    public List<FahrzeugmerkmalAuspraegung> findFahrzeugmerkmalAuspraegungFromSelectedFahrzeugmerkmal(SelectedFahrzeugmerkmalData selectedFahrzeugmerkmalData) {
        Optional<Fahrzeugmerkmal> fahrzeugmerkmal = findFahrzeugmerkmal(selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto().getFahrzeugmerkmalId());

        if (!fahrzeugmerkmal.isPresent()) {
            Collections.emptyList();
        }

        return selectedFahrzeugmerkmalData.getSelectedAuspraegungen().stream().
                map(fahrzeugmerkmalAuspraegungDerivatDto ->
                        this.findFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungDerivatDto.getFahrzeugmerkmalAuspraegungId()).get()
                ).
                collect(Collectors.toList());
    }

    private static void addFahrzeugmerkmalAuspraegungToMap(Map<Fahrzeugmerkmal, FahrzeugmerkmalDto> fahrzeugmerkmalDtoMap, FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung) {
        final Fahrzeugmerkmal fahrzeugmerkmal = fahrzeugmerkmalAuspraegung.getFahrzeugmerkmal();
        final FahrzeugmerkmalAuspraegungDto auspraegung = fahrzeugmerkmalAuspraegung.getDto();

        if (mapContainsFahrzeugmerkmal(fahrzeugmerkmalDtoMap, fahrzeugmerkmal)) {
            updateEntryForFahrzeugmerkmal(fahrzeugmerkmalDtoMap, fahrzeugmerkmal, auspraegung);
        } else {
            createNewEntryForFahrzeugmerkmal(fahrzeugmerkmalDtoMap, fahrzeugmerkmal, auspraegung);
        }
    }

    private static boolean mapContainsFahrzeugmerkmal(Map<Fahrzeugmerkmal, FahrzeugmerkmalDto> fahrzeugmerkmalDtoMap, Fahrzeugmerkmal fahrzeugmerkmal) {
        return fahrzeugmerkmalDtoMap.containsKey(fahrzeugmerkmal);
    }

    private static void createNewEntryForFahrzeugmerkmal(Map<Fahrzeugmerkmal, FahrzeugmerkmalDto> fahrzeugmerkmalDtoMap, Fahrzeugmerkmal fahrzeugmerkmal, FahrzeugmerkmalAuspraegungDto auspraegung) {
        final List<FahrzeugmerkmalModulSeTeamDto> modulSeTeams = fahrzeugmerkmal.getModulSeTeams().stream().map(modulSeTeam -> new FahrzeugmerkmalModulSeTeamDto(modulSeTeam.getId(), modulSeTeam.getName())).collect(Collectors.toList());
        final FahrzeugmerkmalDto newFahrzeugmerkmalDto = new FahrzeugmerkmalDto(fahrzeugmerkmal.getId(), fahrzeugmerkmal.getMerkmal(), modulSeTeams);
        newFahrzeugmerkmalDto.addAuspraegung(auspraegung);
        fahrzeugmerkmalDtoMap.put(fahrzeugmerkmal, newFahrzeugmerkmalDto);
    }

    private static void updateEntryForFahrzeugmerkmal(Map<Fahrzeugmerkmal, FahrzeugmerkmalDto> fahrzeugmerkmalDtoMap, Fahrzeugmerkmal fahrzeugmerkmal, FahrzeugmerkmalAuspraegungDto auspraegung) {
        final FahrzeugmerkmalDto fahrzeugmerkmalDto = fahrzeugmerkmalDtoMap.get(fahrzeugmerkmal);
        fahrzeugmerkmalDto.addAuspraegung(auspraegung);
        fahrzeugmerkmalDtoMap.replace(fahrzeugmerkmal, fahrzeugmerkmalDto);
    }

    public List<Fahrzeugmerkmal> getFahrzeugmerkmaleForModulSeTeam(ModulSeTeamId modulSeTeamId) {
        return fahrzeugmerkmalDao.getForModulSeTeam(modulSeTeamId);
    }
}
