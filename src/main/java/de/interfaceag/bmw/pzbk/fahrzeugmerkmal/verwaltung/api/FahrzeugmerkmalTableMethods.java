package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalDto;

import java.io.Serializable;

public interface FahrzeugmerkmalTableMethods extends Serializable {

    void updateEditDialog(FahrzeugmerkmalDto fahrzeugmerkmal);

}
