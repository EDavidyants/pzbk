package de.interfaceag.bmw.pzbk.fahrzeugmerkmal;

import de.interfaceag.bmw.pzbk.dao.GenericDao;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;

import javax.enterprise.context.Dependent;
import javax.persistence.TypedQuery;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Dependent
public class FahrzeugmerkmalDao extends GenericDao<Fahrzeugmerkmal> {

    public List<Fahrzeugmerkmal> getAll() {
        TypedQuery<Fahrzeugmerkmal> query = getEntityManager().createNamedQuery(Fahrzeugmerkmal.ALL, Fahrzeugmerkmal.class);
        return query.getResultList();
    }

    public List<Fahrzeugmerkmal> getAllFahrzeugmerkmaleNotBewertetForDerivat(Derivat derivat) {
        TypedQuery<Fahrzeugmerkmal> query = getEntityManager().createQuery("SELECT f FROM Fahrzeugmerkmal f WHERE f.id NOT IN (SELECT dm.fahrzeugmerkmal.id FROM DerivatFahrzeugmerkmal dm WHERE dm.derivat.id = :derivatId )", Fahrzeugmerkmal.class);
        query.setParameter("derivatId", derivat.getId());
        return query.getResultList();
    }


    public List<Fahrzeugmerkmal> getForModulSeTeam(ModulSeTeamId modulSeTeamId) {

        if (Objects.isNull(modulSeTeamId)) {
            return Collections.emptyList();
        }

        TypedQuery<Fahrzeugmerkmal> query = getEntityManager().createNamedQuery(Fahrzeugmerkmal.BY_SE_TEAM, Fahrzeugmerkmal.class);
        query.setParameter("modulSeTeamId", modulSeTeamId.getId());
        return query.getResultList();
    }
}
