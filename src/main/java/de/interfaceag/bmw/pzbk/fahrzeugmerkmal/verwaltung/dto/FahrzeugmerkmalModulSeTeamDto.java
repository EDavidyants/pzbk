package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto;

import de.interfaceag.bmw.pzbk.shared.objectIds.DomainObject;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class FahrzeugmerkmalModulSeTeamDto implements DomainObject {

    final Long id;
    final String bezeichnung;

    public FahrzeugmerkmalModulSeTeamDto(long id, String bezeichnung) {
        this.id = id;
        this.bezeichnung = bezeichnung;
    }

    @Override
    public String toString() {
        return "FahrzeugmerkmalModulSeTeamDto{" +
                "id=" + id +
                ", bezeichnung='" + bezeichnung + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        FahrzeugmerkmalModulSeTeamDto that = (FahrzeugmerkmalModulSeTeamDto) object;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(bezeichnung, that.bezeichnung)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(bezeichnung)
                .toHashCode();
    }

    public ModulSeTeamId getModulSeTeamId() {
        return new ModulSeTeamId(id);
    }

    public Long getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }
}
