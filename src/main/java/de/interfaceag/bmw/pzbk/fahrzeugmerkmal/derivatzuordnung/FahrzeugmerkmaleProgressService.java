package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalDao;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmaleProgressDto;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author evda
 */
@Stateless
@Named
public class FahrzeugmerkmaleProgressService implements Serializable {

    @Inject
    private FahrzeugmerkmalDao fahrzeugmerkmalDao;

    @Inject
    private DerivatFahrzeugmerkmalDao derivatFahrzeugmerkmalDao;

    public int getTotalNumberOfFahrzeugmerkmale() {
        List<Fahrzeugmerkmal> allFahrzeugmerkmale = fahrzeugmerkmalDao.getAll();
        return allFahrzeugmerkmale.size();
    }

    public Collection<FahrzeugmerkmaleProgressDto> generateFahrzeugmerkmaleProgressDtosForDerivate(Collection<Derivat> derivate) {
        Collection<FahrzeugmerkmaleProgressDto> result = new HashSet<>();

        if (derivate == null || derivate.isEmpty()) {
            return result;
        }

        List<Long> derivatIds = derivate.stream().map(derivat -> derivat.getId()).collect(Collectors.toList());
        result = derivatFahrzeugmerkmalDao.getNumberOfPersistedFahrzeugmerkmaleForDerivatIds(derivatIds);

        return result;
    }

}
