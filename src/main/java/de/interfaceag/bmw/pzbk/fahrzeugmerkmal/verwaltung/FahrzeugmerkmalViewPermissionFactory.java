package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalViewPermission;
import de.interfaceag.bmw.pzbk.session.Session;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Set;

@Stateless
public class FahrzeugmerkmalViewPermissionFactory implements Serializable {

    @Inject
    private Session session;

    @Produces
    public FahrzeugmerkmalViewPermission getFahrzeugmerkmalViewPermission() {
        final Set<Rolle> roles = session.getUserPermissions().getRoles();
        return new FahrzeugmerkmalViewPermission(roles);
    }

}
