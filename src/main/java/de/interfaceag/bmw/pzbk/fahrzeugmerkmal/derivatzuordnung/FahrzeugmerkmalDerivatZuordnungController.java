package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.api.FahrzeugmerkmalDerivatZuordnungTableMethods;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.api.FahrzeugmerkmaleDerivatZuordnungMerkmalData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatZuordnungViewData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatZuordnungViewPermission;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.SelectedFahrzeugmerkmalData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class FahrzeugmerkmalDerivatZuordnungController implements FahrzeugmerkmaleDerivatZuordnungMerkmalData, FahrzeugmerkmalDerivatZuordnungTableMethods, Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(FahrzeugmerkmalDerivatZuordnungController.class);

    @Inject
    private FahrzeugmerkmalDerivatZuordnungFacade facade;

    @Inject
    private FahrzeugmerkmalDerivatZuordnungViewData viewData;

    @Inject
    private FahrzeugmerkmalDerivatZuordnungFilter filter;

    private SelectedFahrzeugmerkmalData selectedFahrzeugmerkmalData;

    @Override
    public void showDetailForFahrzeugmerkmal(FahrzeugmerkmalDerivatDto fahrzeugmerkmalDerivatDto) {
        LOG.debug("Show detail for {}", fahrzeugmerkmalDerivatDto);
        this.selectedFahrzeugmerkmalData = facade.getSelectedFahrzeugmerkmalDataForFahrzeugmerkmal(fahrzeugmerkmalDerivatDto);
    }

    public SelectedFahrzeugmerkmalData getSelectedFahrzeugmerkmalData() {
        return selectedFahrzeugmerkmalData;
    }

    @Override
    public List<FahrzeugmerkmalDerivatDto> getAllFahrzeugmerkmaleForDerivat() {
        return viewData.getAllFahrzeugmerkmaleForDerivat();
    }

    public void checkboxClicked() {
        if (Boolean.TRUE.equals(this.selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto().getNotRelevant())) {
            this.selectedFahrzeugmerkmalData.setSelectedAuspraegungen(new ArrayList<>());
        }
    }

    public String persistSelectedAuspraegung() {
        facade.persistSelectedAuspraegung(selectedFahrzeugmerkmalData);
        return reload();
    }

    private String reload() {
        return filter.filter();
    }

    public FahrzeugmerkmalDerivatZuordnungViewPermission getViewPermission() {
        return viewData.getViewPermission();
    }

}
