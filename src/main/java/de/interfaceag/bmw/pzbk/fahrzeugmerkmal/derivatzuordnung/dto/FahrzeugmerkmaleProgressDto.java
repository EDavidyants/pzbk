package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * @author evda
 */
public class FahrzeugmerkmaleProgressDto implements Serializable {

    private final Long derivatId;
    private final Long abgeschlossen;

    public FahrzeugmerkmaleProgressDto(Long derivatId, Long abgeschlossen) {
        this.derivatId = derivatId;
        this.abgeschlossen = abgeschlossen;
    }

    @Override
    public String toString() {
        return "Derivat ID " + derivatId + ": " + abgeschlossen + " Fahrzeugmerkmale abgeschlossen";
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(71, 691);
        hb.append(derivatId);
        hb.append(abgeschlossen);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof FahrzeugmerkmaleProgressDto)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        FahrzeugmerkmaleProgressDto other = (FahrzeugmerkmaleProgressDto) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(derivatId, other.derivatId);
        eb.append(abgeschlossen, other.abgeschlossen);
        return eb.isEquals();

    }

    public Long getDerivatId() {
        return derivatId;
    }

    public Long getAbgeschlossen() {
        return abgeschlossen;
    }

}
