package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto;

import java.io.Serializable;
import java.util.List;

public class FahrzeugmerkmalDerivatZuordnungViewData implements Serializable {

    private final List<FahrzeugmerkmalDerivatDto> allFahrzeugmerkmaleForDerivat;

    private final FahrzeugmerkmalDerivatZuordnungViewPermission viewPermission;

    public FahrzeugmerkmalDerivatZuordnungViewData(List<FahrzeugmerkmalDerivatDto> allFahrzeugmerkmaleForDerivat, FahrzeugmerkmalDerivatZuordnungViewPermission viewPermission) {
        this.allFahrzeugmerkmaleForDerivat = allFahrzeugmerkmaleForDerivat;
        this.viewPermission = viewPermission;
    }

    public List<FahrzeugmerkmalDerivatDto> getAllFahrzeugmerkmaleForDerivat() {
        return allFahrzeugmerkmaleForDerivat;
    }

    public FahrzeugmerkmalDerivatZuordnungViewPermission getViewPermission() {
        return viewPermission;
    }
}
