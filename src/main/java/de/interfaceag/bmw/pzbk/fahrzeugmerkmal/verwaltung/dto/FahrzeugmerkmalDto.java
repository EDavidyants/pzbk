package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalTableEntry;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FahrzeugmerkmalDto implements FahrzeugmerkmalTableEntry, Comparable<FahrzeugmerkmalDto> {

    private final Long id;
    private final String merkmal;
    private final List<FahrzeugmerkmalAuspraegungDto> auspraegungen;
    private final List<FahrzeugmerkmalModulSeTeamDto> module;

    public FahrzeugmerkmalDto(Long id, String merkmal, List<FahrzeugmerkmalModulSeTeamDto> modulSeTeams) {
        this.id = id;
        this.merkmal = merkmal;
        auspraegungen = new ArrayList<>();
        module = modulSeTeams;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        FahrzeugmerkmalDto that = (FahrzeugmerkmalDto) object;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(merkmal, that.merkmal)
                .append(auspraegungen, that.auspraegungen)
                .append(module, that.module)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(merkmal)
                .append(auspraegungen)
                .append(module)
                .toHashCode();
    }

    @Override
    public int compareTo(FahrzeugmerkmalDto fahrzeugmerkmalDto) {
        return this.merkmal.compareTo(fahrzeugmerkmalDto.merkmal);
    }

    @Override
    public String getMerkmal() {
        return merkmal;
    }

    @Override
    public String getAuspraegungen() {
        return auspraegungen.stream().map(FahrzeugmerkmalAuspraegungDto::getAuspraegung).collect(Collectors.joining(", "));
    }

    public List<FahrzeugmerkmalAuspraegungDto> getAuspraegungenForFahrzeugmerkmal() {
        return auspraegungen;
    }

    @Override
    public String getModule() {
        return module.stream().map(FahrzeugmerkmalModulSeTeamDto::getBezeichnung).collect(Collectors.joining(", "));
    }

    public Long getId() {
        return id;
    }

    public List<FahrzeugmerkmalModulSeTeamDto> getModuleForFahrzeugmerkmal() {
        return module;
    }

    public void addAuspraegung(FahrzeugmerkmalAuspraegungDto auspraegung) {
        this.auspraegungen.add(auspraegung);
    }

}
