package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.api;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;

import java.util.List;


public interface FahrzeugmerkmaleDerivatZuordnungMerkmalData {

    List<FahrzeugmerkmalDerivatDto> getAllFahrzeugmerkmaleForDerivat();

}
