package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;

@Stateless
public class DerivatFahrzeugmerkmalService implements Serializable {

    @Inject
    private DerivatFahrzeugmerkmalDao derivatFahrzeugmerkmalDao;

    public DerivatFahrzeugmerkmal getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId(DerivatId derivatId, FahrzeugmerkmalId fahrzeugmerkmalId) {
        return derivatFahrzeugmerkmalDao.getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId(derivatId, fahrzeugmerkmalId);
    }

}
