package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.api;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;

import java.io.Serializable;

public interface FahrzeugmerkmalDerivatZuordnungTableMethods extends Serializable {

    void showDetailForFahrzeugmerkmal(FahrzeugmerkmalDerivatDto fahrzeugmerkmalDerivatDto);

}
