package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmaleProgressDto;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * @author evda
 */
@Dependent
public class DerivatFahrzeugmerkmalDao implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(DerivatFahrzeugmerkmalDao.class);

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public Collection<FahrzeugmerkmaleProgressDto> getNumberOfPersistedFahrzeugmerkmaleForDerivatIds(Collection<Long> derivatIds) {
        LOG.debug("getNumberOfPersistedFahrzeugmerkmaleForDerivatIds By derivatIds {}", derivatIds);

        Query query = entityManager.createQuery("SELECT dfm.derivat.id, COUNT(dfm.id) "
                + "FROM DerivatFahrzeugmerkmal dfm GROUP BY dfm.derivat HAVING dfm.derivat.id IN :derivatIds "
                + "ORDER BY dfm.derivat.id");

        query.setParameter("derivatIds", derivatIds);
        List<Object[]> tuples = query.getResultList();

        Collection<FahrzeugmerkmaleProgressDto> result = new HashSet<>();
        tuples.forEach(tuple -> result.add(new FahrzeugmerkmaleProgressDto((Long) tuple[0], (Long) tuple[1])));

        LOG.debug("getNumberOfPersistedFahrzeugmerkmaleForDerivatIds found {} results", result.size());

        return result;
    }

    /**
     * Get all DerivatFahrzeugmerkmal for a derivat id and fahrzeugmerkmal id.
     *
     * @param derivatId         the id of a derivat.
     * @param fahrzeugmerkmalId the id of the fahrzeugmerkmal.
     * @return the DerivatFahrzeugmerkmal
     */
    public DerivatFahrzeugmerkmal getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId(DerivatId derivatId, FahrzeugmerkmalId fahrzeugmerkmalId) {
        LOG.debug("getDerivatFahrzeugmerkmal By DerivatID {} And FahrzeugmerkmalId {}", derivatId.getId(), fahrzeugmerkmalId.getId());

        TypedQuery<DerivatFahrzeugmerkmal> query = entityManager.createQuery("SELECT dm FROM DerivatFahrzeugmerkmal dm WHERE dm.derivat.id = :derivatId AND dm.fahrzeugmerkmal.id = :fahrzeugmerkmalId", DerivatFahrzeugmerkmal.class);
        query.setParameter("derivatId", derivatId.getId());
        query.setParameter("fahrzeugmerkmalId", fahrzeugmerkmalId.getId());

        List<DerivatFahrzeugmerkmal> result = query.getResultList();

        if (result.isEmpty()) {
            LOG.debug("getDerivatFahrzeugmerkmal, no result is found");
            return null;
        }

        return result.get(0);
    }

}
