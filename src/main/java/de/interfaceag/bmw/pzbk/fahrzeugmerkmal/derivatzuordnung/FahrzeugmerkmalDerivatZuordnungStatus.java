package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;

import java.io.Serializable;

public enum FahrzeugmerkmalDerivatZuordnungStatus implements Serializable {

    NICHT_BEWERTET("icon-kreis-blau.png"),
    RELEVANT("icon-check-green.png"),
    NICHT_RELEVANT("icon-cancel-red.png");

    private final String icon;

    FahrzeugmerkmalDerivatZuordnungStatus(String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public static FahrzeugmerkmalDerivatZuordnungStatus getStatusFromDerivatFahrzeugmerkmal(DerivatFahrzeugmerkmal derivatFahrzeugmerkmal) {
        if (derivatFahrzeugmerkmal.isNotRelevant()) {
            return FahrzeugmerkmalDerivatZuordnungStatus.NICHT_RELEVANT;
        } else if (!derivatFahrzeugmerkmal.getAuspraegungen().isEmpty()) {
            return FahrzeugmerkmalDerivatZuordnungStatus.RELEVANT;
        } else {
            return FahrzeugmerkmalDerivatZuordnungStatus.NICHT_BEWERTET;
        }
    }
}
