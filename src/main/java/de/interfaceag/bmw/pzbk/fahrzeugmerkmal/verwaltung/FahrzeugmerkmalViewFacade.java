package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung;

import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalEditDialogData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Objects;

@Stateless
public class FahrzeugmerkmalViewFacade implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(FahrzeugmerkmalViewFacade.class);

    @Inject
    private FahrzeugmerkmalEditDialogService fahrzeugmerkmalEditDialogService;
    @Inject
    private FahrzeugmerkmalEditDialogSafeService fahrzeugmerkmalEditDialogSafeService;

    public FahrzeugmerkmalEditDialogData getEditDialogData(FahrzeugmerkmalDto fahrzeugmerkmal) {
        LOG.debug("get edit dialog data for fahrzeugmerkmal {}", fahrzeugmerkmal);
        return fahrzeugmerkmalEditDialogService.getEditDialogDataForFahrzeugmerkmal(fahrzeugmerkmal);
    }

    public String saveEditDialog(FahrzeugmerkmalEditDialogData editDialogData) {
        final Fahrzeugmerkmal fahrzeugmerkmal = fahrzeugmerkmalEditDialogSafeService.save(editDialogData);
        if (Objects.nonNull(fahrzeugmerkmal)) {
            return Page.FAHRZEUGMERKMAL_KONFIGURATION.getUrl() + "?faces-redirect=true";
        } else {
            return "";
        }
    }

    public FahrzeugmerkmalEditDialogData getEditDialogDataForNewFahrzeugmerkmal() {
        return fahrzeugmerkmalEditDialogService.getEditDialogDataForNewFahrzeugmerkmal();
    }
}
