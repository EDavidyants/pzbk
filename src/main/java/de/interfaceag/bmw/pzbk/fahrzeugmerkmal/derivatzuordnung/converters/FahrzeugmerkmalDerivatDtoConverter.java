package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.converters;

import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.FahrzeugmerkmalDerivatZuordnungStatus;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;

import java.util.List;
import java.util.stream.Collectors;

public final class FahrzeugmerkmalDerivatDtoConverter {

    private FahrzeugmerkmalDerivatDtoConverter() {
    }

    public static List<FahrzeugmerkmalDerivatDto> forDerivatFahrzeugmerkmale(List<DerivatFahrzeugmerkmal> derivatFahrzeugmerkmale) {
        return derivatFahrzeugmerkmale.stream()
                .map(derivatFahrzeugmerkmal -> new FahrzeugmerkmalDerivatDto(
                        derivatFahrzeugmerkmal.getFahrzeugmerkmal().getFahrzeugmerkmalId(),
                        derivatFahrzeugmerkmal.getFahrzeugmerkmal().getMerkmal(),
                        derivatFahrzeugmerkmal.getDerivat().getDerivatId(),
                        FahrzeugmerkmalDerivatZuordnungStatus.getStatusFromDerivatFahrzeugmerkmal(derivatFahrzeugmerkmal),
                        derivatFahrzeugmerkmal.isNotRelevant()))
                .collect(Collectors.toList());
    }
}
