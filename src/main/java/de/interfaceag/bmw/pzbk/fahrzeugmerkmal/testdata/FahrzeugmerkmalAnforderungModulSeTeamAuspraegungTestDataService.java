package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.testdata;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungModulSeTeamFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import de.interfaceag.bmw.pzbk.testdata.AnforderungTestdataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

@Stateless
public class FahrzeugmerkmalAnforderungModulSeTeamAuspraegungTestDataService implements Serializable {

    private static final int ANFORDERUNGEN_TO_CONFIGURE = 10;
    private Logger log = LoggerFactory.getLogger(FahrzeugmerkmalAnforderungModulSeTeamAuspraegungTestDataService.class);

    @Inject
    private ModulService modulService;
    @Inject
    private FahrzeugmerkmalService fahrzeugmerkmalService;
    @Inject
    private AnforderungService anforderungService;
    @Inject
    private AnforderungTestdataService anforderungTestdataService;
    @Inject
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService anforderungModulSeTeamFahrzeugmerkmalAuspraegungService;

    public void generateTestData(List<AnforderungId> newGeneratedAnforderungen) {
        if (newGeneratedAnforderungen != null && !newGeneratedAnforderungen.isEmpty()) {
            for (int i = 0; i < ANFORDERUNGEN_TO_CONFIGURE; i++) {
                setAuspraegungForAnforderung(newGeneratedAnforderungen.get(i));
            }
        }

        for (int i = 0; i < ANFORDERUNGEN_TO_CONFIGURE; i++) {
            AnforderungId newGeneratedAnforderung = anforderungTestdataService.generateAnforderungOhneMeldungWithRandomValues(Status.A_FREIGEGEBEN);
            setAuspraegungForAnforderung(newGeneratedAnforderung);
        }

    }

    private void setAuspraegungForAnforderung(AnforderungId anforderungId) {
        log.debug("Testdata: Writing Auspraegung for {}", anforderungId);
        Anforderung anforderung = anforderungService.getAnforderungById(anforderungId.getId());
        ModulSeTeam modulSeTeam = modulService.getSeTeamById(1L);
        anforderung = verifyAnforderungHasMatchingModulSeTeam(modulSeTeam, anforderung);
        List<FahrzeugmerkmalAuspraegung> fahrzeugmerkmalAuspraegungen = getRandomFahrzeugmerkmalAuspraegung();

        if (anforderung != null && modulSeTeam != null && !fahrzeugmerkmalAuspraegungen.isEmpty()) {
            persistRandomAmountOfAuspraegungen(anforderung, modulSeTeam, fahrzeugmerkmalAuspraegungen);
        }

    }

    private void persistRandomAmountOfAuspraegungen(Anforderung anforderung, ModulSeTeam modulSeTeam, List<FahrzeugmerkmalAuspraegung> fahrzeugmerkmalAuspraegungen) {
        List<FahrzeugmerkmalAuspraegung> alreadySetAuspraegungen = anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(new AnforderungId(anforderung.getId()), new ModulSeTeamId(modulSeTeam.getId()));
        for (FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung: fahrzeugmerkmalAuspraegungen) {
            if (!alreadySetAuspraegungen.contains(fahrzeugmerkmalAuspraegung)) {
                AnforderungModulSeTeamFahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegung = new AnforderungModulSeTeamFahrzeugmerkmalAuspraegung(anforderung, modulSeTeam, fahrzeugmerkmalAuspraegung);
                log.debug("Writing Anforderung ModulSe Team Auspraegung {}", anforderungModulSeTeamFahrzeugmerkmalAuspraegung.toString());
                anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.save(anforderungModulSeTeamFahrzeugmerkmalAuspraegung, true);
            }
        }
    }

    private Anforderung verifyAnforderungHasMatchingModulSeTeam(ModulSeTeam modulSeTeam, Anforderung anforderung) {
        Set<Umsetzer> umsetzers = anforderung.getUmsetzer();

        for (Umsetzer umsetzer : umsetzers) {
            if (umsetzer.getSeTeam().equals(modulSeTeam)) {
                return anforderung;
            }
        }
        umsetzers.add(new Umsetzer(modulSeTeam));
        anforderung.setUmsetzer(umsetzers);
        AnforderungFreigabeBeiModulSeTeam freigabe = new AnforderungFreigabeBeiModulSeTeam(anforderung, modulSeTeam);
        freigabe.setVereinbarungType(VereinbarungType.ZAK);
        anforderung.addAnfoFreigabeBeiModul(freigabe);
        anforderungService.saveAnforderung(anforderung);

        return anforderung;
    }

    private List<FahrzeugmerkmalAuspraegung> getRandomFahrzeugmerkmalAuspraegung() {
        List<FahrzeugmerkmalAuspraegung> allFahrzeugmerkmalAuspraegungen = fahrzeugmerkmalService.getAllFahrzeugmerkmalAuspraegungen();
        List<FahrzeugmerkmalAuspraegung> result = new ArrayList<>();
        if (allFahrzeugmerkmalAuspraegungen != null && !allFahrzeugmerkmalAuspraegungen.isEmpty()) {
            int amountOfAuspraegungen = ThreadLocalRandom.current().nextInt(allFahrzeugmerkmalAuspraegungen.size());

            for (int i = 0; i < amountOfAuspraegungen; i++) {
                result.add(allFahrzeugmerkmalAuspraegungen.get(ThreadLocalRandom.current().nextInt(allFahrzeugmerkmalAuspraegungen.size())));
            }

        }
        return result;
    }

}
