package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung;

import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalEditDialogData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalAuspraegungDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalEditDialogDataDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalModulSeTeamDto;
import de.interfaceag.bmw.pzbk.services.ModulService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class FahrzeugmerkmalEditDialogService implements Serializable {

    @Inject
    private ModulService modulService;

    public FahrzeugmerkmalEditDialogData getEditDialogDataForFahrzeugmerkmal(FahrzeugmerkmalDto fahrzeugmerkmal) {
        final List<FahrzeugmerkmalModulSeTeamDto> allModule = getAllModule();
        final List<FahrzeugmerkmalModulSeTeamDto> moduleForFahrzeugmerkmal = fahrzeugmerkmal.getModuleForFahrzeugmerkmal();
        final List<FahrzeugmerkmalAuspraegungDto> auspraegungenForFahrzeugmerkmal = fahrzeugmerkmal.getAuspraegungenForFahrzeugmerkmal();

        return new FahrzeugmerkmalEditDialogDataDto(fahrzeugmerkmal, allModule, moduleForFahrzeugmerkmal, auspraegungenForFahrzeugmerkmal);
    }

    private List<FahrzeugmerkmalModulSeTeamDto> getAllModule() {
        final List<ModulSeTeam> allSeTeams = modulService.getAllSeTeams();
        return allSeTeams.stream()
                .map(modulSeTeam -> new FahrzeugmerkmalModulSeTeamDto(modulSeTeam.getId(), modulSeTeam.getName()))
                .collect(Collectors.toList());
    }

    public FahrzeugmerkmalEditDialogData getEditDialogDataForNewFahrzeugmerkmal() {
        final List<FahrzeugmerkmalModulSeTeamDto> allModule = getAllModule();
        return new FahrzeugmerkmalEditDialogDataDto(allModule);
    }
}
