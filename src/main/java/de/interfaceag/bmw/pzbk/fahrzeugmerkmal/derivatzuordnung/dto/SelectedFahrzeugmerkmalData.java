package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto;

import de.interfaceag.bmw.pzbk.converters.DomainObjectConverter;

import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.List;

public class SelectedFahrzeugmerkmalData implements Serializable {

    private final FahrzeugmerkmalDerivatDto fahrzeugmerkmalDerivatDto;

    private final List<FahrzeugmerkmalAuspraegungDerivatDto> allAuspraegungen;
    private List<FahrzeugmerkmalAuspraegungDerivatDto> selectedAuspraegungen;

    public SelectedFahrzeugmerkmalData(FahrzeugmerkmalDerivatDto selectedFahrzeugmerkmal, List<FahrzeugmerkmalAuspraegungDerivatDto> allAuspraegungen, List<FahrzeugmerkmalAuspraegungDerivatDto> selectedAuspraegungen) {
        this.fahrzeugmerkmalDerivatDto = selectedFahrzeugmerkmal;
        this.allAuspraegungen = allAuspraegungen;
        this.selectedAuspraegungen = selectedAuspraegungen;
    }

    public FahrzeugmerkmalDerivatDto getFahrzeugmerkmalDerivatDto() {
        return fahrzeugmerkmalDerivatDto;
    }

    public List<FahrzeugmerkmalAuspraegungDerivatDto> getSelectedAuspraegungen() {
        return selectedAuspraegungen;
    }

    public void setSelectedAuspraegungen(List<FahrzeugmerkmalAuspraegungDerivatDto> selectedAuspraegungen) {
        this.selectedAuspraegungen = selectedAuspraegungen;
    }

    public List<FahrzeugmerkmalAuspraegungDerivatDto> getAllAuspraegungen() {
        return allAuspraegungen;
    }

    public Converter getAuspraegungConverter() {
        return new DomainObjectConverter<>(allAuspraegungen);
    }
}
