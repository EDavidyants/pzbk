package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.dao.GenericDao;
import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.converters.FahrzeugmerkmalAuspraegungDerivatDtoConverter;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.converters.FahrzeugmerkmalDerivatDtoConverter;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalAuspraegungDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;

import javax.enterprise.context.Dependent;
import javax.persistence.TypedQuery;
import java.util.List;

@Dependent
public class FahrzeugmerkmalDerivatZuordnungDao extends GenericDao<DerivatFahrzeugmerkmal> {

    /**
     * Get all DerivatFahrzeugmerkmal as dto for a derivat id. This results in all entries for which a user has already defined something for the derivat.
     *
     * @param derivatId the id of a derivat.
     * @return all entries for a derivat id.
     */
    public List<FahrzeugmerkmalDerivatDto> getAllFahrzeugmerkmaleForDerivat(DerivatId derivatId) {
        TypedQuery<DerivatFahrzeugmerkmal> query = getEntityManager().createNamedQuery(DerivatFahrzeugmerkmal.BY_DERIVAT, DerivatFahrzeugmerkmal.class);
        query.setParameter("derivatId", derivatId.getId());
        List<DerivatFahrzeugmerkmal> queryResultList = query.getResultList();
        return FahrzeugmerkmalDerivatDtoConverter.forDerivatFahrzeugmerkmale(queryResultList);
    }
    
    public List<FahrzeugmerkmalAuspraegungDerivatDto> getAlreadySelectedAuspraegungenForFahrzeugmerkmalAndDerivat(FahrzeugmerkmalId fahrzeugmerkmalId, DerivatId derivatId) {
        TypedQuery<FahrzeugmerkmalAuspraegung> query = getEntityManager().createNamedQuery(DerivatFahrzeugmerkmal.AUSPRAEGUNG_BY_DERIVAT_AND_MERKMAL, FahrzeugmerkmalAuspraegung.class);
        query.setParameter("fahrzeugmerkmalId", fahrzeugmerkmalId.getId());
        query.setParameter("derivatId", derivatId.getId());
        List<FahrzeugmerkmalAuspraegung> queryResultList = query.getResultList();
        return FahrzeugmerkmalAuspraegungDerivatDtoConverter.forFahrzeugmerkmalAuspraeungen(queryResultList);
    }

}
