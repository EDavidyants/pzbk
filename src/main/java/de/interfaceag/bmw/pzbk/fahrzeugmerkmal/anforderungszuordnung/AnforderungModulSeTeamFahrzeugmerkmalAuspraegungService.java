package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungModulSeTeamFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.history.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Stateless
public class AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService implements Serializable {

    @Inject
    private FahrzeugmerkmalService fahrzeugmerkmalService;
    @Inject
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungDao anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao;
    @Inject
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService anforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService;

    public void save(AnforderungModulSeTeamFahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegung, boolean isStartupService) {
        anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.save(anforderungModulSeTeamFahrzeugmerkmalAuspraegung);
        if (!isStartupService) {
            anforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService.writeAnforderungFahrzeugmerkmalAuspraegungHinzugefuegtToHistory(anforderungModulSeTeamFahrzeugmerkmalAuspraegung);
        }
    }

    public Optional<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> find(AnforderungModulSeTeamFahrzeugmerkmalAuspraegungId domainObject) {
        return anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.find(domainObject);
    }

    public void remove(AnforderungModulSeTeamFahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegung) {
        anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.remove(anforderungModulSeTeamFahrzeugmerkmalAuspraegung);
        anforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService.writeAnforderungFahrzeugmerkmalAuspraegungGeloeschtToHistory(anforderungModulSeTeamFahrzeugmerkmalAuspraegung);
    }

    public Collection<ModulSeTeamId> getConfiguredModulSeTeamsForAnforderung(Anforderung anforderung) {
        if (Objects.nonNull(anforderung) && Objects.nonNull(anforderung.getAnforderungId()) && Objects.nonNull(anforderung.getAnforderungId().getId())) {
            return getConfiguredModulSeTeamsForValidAnforderung(anforderung);
        } else {
            return Collections.emptyList();
        }
    }

    private Collection<ModulSeTeamId> getConfiguredModulSeTeamsForValidAnforderung(Anforderung anforderung) {
        final Set<ModulSeTeamId> configuredModulSeTeamIds = new HashSet<>();

        final Iterator<ModulSeTeam> modulSeTeamIterator = anforderung.getAnfoFreigabeBeiModul().stream().map(AnforderungFreigabeBeiModulSeTeam::getModulSeTeam).iterator();
        while (modulSeTeamIterator.hasNext()) {
            final ModulSeTeamId modulSeTeamId = modulSeTeamIterator.next().getModulSeTeamId();
            checkIfModulSeTeamIsConfigured(anforderung, modulSeTeamId, configuredModulSeTeamIds);
        }

        return configuredModulSeTeamIds;
    }

    private void checkIfModulSeTeamIsConfigured(Anforderung anforderung, ModulSeTeamId modulSeTeamId, Set<ModulSeTeamId> configuredModulSeTeamIds) {
        final Set<FahrzeugmerkmalId> requiredFahrzeugmerkmalIds = getRequiredFahrzeugmerkmale(modulSeTeamId);
        final Set<FahrzeugmerkmalId> selectedFahrzeugmerkmalIds = getSelectedFahrzeugmerkmale(anforderung, modulSeTeamId);

        if (selectedFahrzeugmerkmalIds.equals(requiredFahrzeugmerkmalIds)) {
            configuredModulSeTeamIds.add(modulSeTeamId);
        }
    }

    private Set<FahrzeugmerkmalId> getSelectedFahrzeugmerkmale(Anforderung anforderung, ModulSeTeamId modulSeTeamId) {
        final AnforderungId anforderungId = anforderung.getAnforderungId();
        final List<FahrzeugmerkmalAuspraegung> selectedAuspraegungenForModulSeTeam = anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(anforderungId, modulSeTeamId);
        return selectedAuspraegungenForModulSeTeam.stream()
                .map(FahrzeugmerkmalAuspraegung::getFahrzeugmerkmal)
                .map(Fahrzeugmerkmal::getFahrzeugmerkmalId)
                .collect(Collectors.toSet());
    }

    private Set<FahrzeugmerkmalId> getRequiredFahrzeugmerkmale(ModulSeTeamId modulSeTeamId) {
        final List<Fahrzeugmerkmal> fahrzeugmerkmaleForModulSeTeam = fahrzeugmerkmalService.getFahrzeugmerkmaleForModulSeTeam(modulSeTeamId);
        return fahrzeugmerkmaleForModulSeTeam.stream()
                .map(Fahrzeugmerkmal::getFahrzeugmerkmalId)
                .collect(Collectors.toSet());
    }


    public List<FahrzeugmerkmalAuspraegung> getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(AnforderungId anforderungId, ModulSeTeamId modulSeTeamId) {
        if (Objects.nonNull(anforderungId) && Objects.nonNull(modulSeTeamId)) {
            return anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(anforderungId, modulSeTeamId);
        } else {
            return Collections.emptyList();
        }
    }

    public Optional<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> getSelectedFahrzeugmerkmalAuspraegungenForAnforderungModulSeTeamAndFahrzeugmerkmalAuspraegung(AnforderungId anforderungId,
                                                                                                                                                                    ModulSeTeamId modulSeTeamId,
                                                                                                                                                                    FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId) {
        return anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungModulSeTeamAndFahrzeugmerkmalAuspraegung(anforderungId, modulSeTeamId, fahrzeugmerkmalAuspraegungId);
    }
}
