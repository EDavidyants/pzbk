package de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto;

import java.io.Serializable;
import java.util.List;

public class BerechtigungDto implements Serializable {

    private final BerechtigungTyp berechtigungTyp;
    private final List<BerechtigungIdTuple> berechtigungValue;

    public BerechtigungDto(BerechtigungTyp berechtigungTyp, List<BerechtigungIdTuple> berechtigungValue) {
        this.berechtigungTyp = berechtigungTyp;
        this.berechtigungValue = berechtigungValue;
    }

    public BerechtigungTyp getBerechtigungTyp() {
        return berechtigungTyp;
    }

    public List<BerechtigungIdTuple> getBerechtigungValue() {
        return berechtigungValue;
    }
}
