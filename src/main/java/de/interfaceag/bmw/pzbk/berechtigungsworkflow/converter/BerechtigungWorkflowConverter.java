package de.interfaceag.bmw.pzbk.berechtigungsworkflow.converter;

import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungIdTuple;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

public class BerechtigungWorkflowConverter implements Converter<BerechtigungIdTuple> {

    private final List<BerechtigungIdTuple> berechtigung;

    public BerechtigungWorkflowConverter(List<BerechtigungIdTuple> berechtigung) {
        this.berechtigung = berechtigung;
    }

    @Override
    public BerechtigungIdTuple getAsObject(FacesContext context, UIComponent component, String value) {
        return berechtigung.stream().filter(m -> m.getValue().equals(value)).findAny().orElse(null);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, BerechtigungIdTuple value) {
        BerechtigungIdTuple m = value;
        return m.getValue();

    }

}
