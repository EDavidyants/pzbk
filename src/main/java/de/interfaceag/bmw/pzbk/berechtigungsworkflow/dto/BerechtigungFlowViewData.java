package de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto;

import de.interfaceag.bmw.pzbk.berechtigungsworkflow.converter.BerechtigungWorkflowConverter;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.List;

public class BerechtigungFlowViewData implements Serializable {

    private final List<Rolle> chooseableRoles;
    private Rolle selectedRole;

    private BerechtigungDto berechtigungenForRoleWithSchreibrecht;
    private List<BerechtigungIdTuple> selectedBerechtigungenWithSchreibrecht;

    private BerechtigungDto berechtigungenForRoleWithLeserecht;
    private List<BerechtigungIdTuple> selectedBerechtigungenWithLeserecht;

    private boolean datenschutzAngenommen;

    public BerechtigungFlowViewData(List<Rolle> rollen, Rolle selectedRolle) {
        this.chooseableRoles = rollen;
        this.selectedRole = selectedRolle;
        this.datenschutzAngenommen = false;
    }

    public boolean isDatenschutzAngenommen() {
        return datenschutzAngenommen;
    }

    public void setDatenschutzAngenommen(boolean datenschutzAngenommen) {
        this.datenschutzAngenommen = datenschutzAngenommen;
    }

    public Converter getBerechtigungWorkflowConverterForSchreibreccht() {
        return new BerechtigungWorkflowConverter(berechtigungenForRoleWithSchreibrecht.getBerechtigungValue());
    }

    public Converter getBerechtigungWorkflowConverterForLesereccht() {
        return new BerechtigungWorkflowConverter(berechtigungenForRoleWithLeserecht.getBerechtigungValue());
    }

    public List<Rolle> getChooseableRoles() {
        return chooseableRoles;
    }

    public Rolle getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(Rolle selectedRole) {
        this.selectedRole = selectedRole;
    }

    public BerechtigungDto getBerechtigungenForRoleWithSchreibrecht() {
        return berechtigungenForRoleWithSchreibrecht;
    }

    public void setBerechtigungenForRoleWithSchreibrecht(BerechtigungDto berechtigungenForRoleWithSchreibrecht) {
        this.berechtigungenForRoleWithSchreibrecht = berechtigungenForRoleWithSchreibrecht;
    }

    public List<BerechtigungIdTuple> getSelectedBerechtigungenWithSchreibrecht() {
        return selectedBerechtigungenWithSchreibrecht;
    }

    public void setSelectedBerechtigungenWithSchreibrecht(List<BerechtigungIdTuple> selectedBerechtigungenWithSchreibrecht) {
        this.selectedBerechtigungenWithSchreibrecht = selectedBerechtigungenWithSchreibrecht;
    }

    public BerechtigungDto getBerechtigungenForRoleWithLeserecht() {
        return berechtigungenForRoleWithLeserecht;
    }

    public void setBerechtigungenForRoleWithLeserecht(BerechtigungDto berechtigungenForRoleWithLeserecht) {
        this.berechtigungenForRoleWithLeserecht = berechtigungenForRoleWithLeserecht;
    }

    public List<BerechtigungIdTuple> getSelectedBerechtigungenWithLeserecht() {
        return selectedBerechtigungenWithLeserecht;
    }

    public void setSelectedBerechtigungenWithLeserecht(List<BerechtigungIdTuple> selectedBerechtigungenWithLeserecht) {
        this.selectedBerechtigungenWithLeserecht = selectedBerechtigungenWithLeserecht;
    }

    public boolean isLeseberechtigungSelectedInSchreibrecht(BerechtigungIdTuple leserecht) {
        if (selectedBerechtigungenWithSchreibrecht == null) {
            return false;
        }
        return this.selectedBerechtigungenWithSchreibrecht.contains(leserecht);
    }

    public boolean isSchreibberechtigungSelectedInSLeseecht(BerechtigungIdTuple schreibrecht) {
        if (selectedBerechtigungenWithLeserecht == null) {
            return false;
        }
        return this.selectedBerechtigungenWithLeserecht.contains(schreibrecht);
    }
}
