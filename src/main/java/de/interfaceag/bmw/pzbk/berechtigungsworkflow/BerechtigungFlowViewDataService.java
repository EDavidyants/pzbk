package de.interfaceag.bmw.pzbk.berechtigungsworkflow;

import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungFlowViewData;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class BerechtigungFlowViewDataService implements Serializable {

    @Produces
    public BerechtigungFlowViewData buildDefaultViewData () {
        final List<Rolle> choosableRoles = getChoosableRoles();
        return new BerechtigungFlowViewData(choosableRoles, Rolle.SENSOR);
    }

    private static List<Rolle> getChoosableRoles() {
        List<Rolle> rollen = new ArrayList<>();
        rollen.add(Rolle.SENSOR);
        rollen.add(Rolle.SENSOR_EINGESCHRAENKT);
        rollen.add(Rolle.TTEAMMITGLIED);
        rollen.add(Rolle.E_COC);
        return rollen;
    }

}
