package de.interfaceag.bmw.pzbk.berechtigungsworkflow;

import de.interfaceag.bmw.pzbk.berechtigungantrag.BerechtigungAntragService;
import de.interfaceag.bmw.pzbk.berechtigungantrag.mail.BerechtigungsantragMailService;
import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungFlowViewData;
import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungIdTuple;
import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungTyp;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.services.UserService;
import de.interfaceag.bmw.pzbk.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Stateless
public class BerechtigungFlowService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(BerechtigungFlowService.class);

    @Inject
    private Session session;
    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private ModulService modulService;
    @Inject
    private TteamService tteamService;
    @Inject
    private BerechtigungAntragService berechtigungAntragService;
    @Inject
    private UserSearchService userSearchService;
    @Inject
    private UserService userService;
    @Inject
    private BerechtigungsantragMailService berechtigungsantragMailService;

    public void getBerechtigungForRolle(BerechtigungFlowViewData viewData) {
        Optional<BerechtigungTyp> optionalBerechtigungTyp = BerechtigungTyp.getBerechtigungTypByRolle(viewData.getSelectedRole());
        if (!optionalBerechtigungTyp.isPresent()) {
            return;
        }
        final BerechtigungTyp berechtigungTyp = optionalBerechtigungTyp.get();
        List<BerechtigungIdTuple> berechtigungIdTuples = getBerechtigungIdTuplesByBerechtigungTyp(berechtigungTyp);

        restrictBerechtigungBySession(viewData, berechtigungTyp, berechtigungIdTuples);
    }

    private void restrictBerechtigungBySession(BerechtigungFlowViewData viewData, BerechtigungTyp berechtigungTyp, List<BerechtigungIdTuple> berechtigungIdTuples) {
        UserPermissions<de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto> userPermissions = session.getUserPermissions();

        if (userPermissions == null) {
            viewData.setBerechtigungenForRoleWithSchreibrecht(new BerechtigungDto(berechtigungTyp, berechtigungIdTuples));
            viewData.setBerechtigungenForRoleWithLeserecht(new BerechtigungDto(berechtigungTyp, berechtigungIdTuples));
            return;
        }

        restrictBerechtigungByBerechtigungTyp(viewData, berechtigungTyp, berechtigungIdTuples, userPermissions);
    }

    private void restrictBerechtigungByBerechtigungTyp(BerechtigungFlowViewData viewData,
                                                       BerechtigungTyp berechtigungTyp,
                                                       List<BerechtigungIdTuple> berechtigungIdTuples,
                                                       UserPermissions<de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto> userPermissions) {

        switch (berechtigungTyp) {
            case SENSORCOC:
                setViewDataBerechtigungForSensorCoc(viewData, berechtigungTyp, berechtigungIdTuples, userPermissions);
                break;

            case SETEAM:
                setViewDataBerechtigungForSeTeam(viewData, berechtigungTyp, berechtigungIdTuples, userPermissions);
                break;

            case TTEAM:
                setViewDataBerechtigungForTTeam(viewData, berechtigungTyp, berechtigungIdTuples, userPermissions);
                break;

            default:
                viewData.setBerechtigungenForRoleWithLeserecht(new BerechtigungDto(berechtigungTyp, Collections.emptyList()));
                viewData.setBerechtigungenForRoleWithLeserecht(new BerechtigungDto(berechtigungTyp, Collections.emptyList()));
                break;
        }
    }

    private void setViewDataBerechtigungForTTeam(BerechtigungFlowViewData viewData, BerechtigungTyp berechtigungTyp, List<BerechtigungIdTuple> berechtigungIdTuples, UserPermissions<de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto> userPermissions) {
        List<BerechtigungIdTuple> filterResult;
        filterResult = berechtigungIdTuples
                .stream()
                .filter(data -> userPermissions.getTteamMitgliedBerechtigung().getTteamIdsMitSchreibrechten().stream().noneMatch(permission -> permission == data.getId()))
                .collect(Collectors.toList());
        viewData.setBerechtigungenForRoleWithSchreibrecht(new BerechtigungDto(berechtigungTyp, filterResult));

        filterResult = berechtigungIdTuples
                .stream()
                .filter(data -> userPermissions.getTteamMitgliedBerechtigung().getTteamIdsMitLeserechten().stream().noneMatch(permission -> permission == data.getId()))
                .filter(data -> userPermissions.getTteamMitgliedBerechtigung().getTteamIdsMitSchreibrechten().stream().noneMatch(permission -> permission == data.getId()))
                .collect(Collectors.toList());
        viewData.setBerechtigungenForRoleWithLeserecht(new BerechtigungDto(berechtigungTyp, filterResult));
    }

    private void setViewDataBerechtigungForSeTeam(BerechtigungFlowViewData viewData, BerechtigungTyp berechtigungTyp, List<BerechtigungIdTuple> berechtigungIdTuples, UserPermissions<de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto> userPermissions) {
        List<BerechtigungIdTuple> filterResult;
        filterResult = berechtigungIdTuples
                .stream()
                .filter(data -> userPermissions.getModulSeTeamAsEcocSchreibend().stream().noneMatch(permission -> permission.getId() == data.getId()))
                .collect(Collectors.toList());
        viewData.setBerechtigungenForRoleWithSchreibrecht(new BerechtigungDto(berechtigungTyp, filterResult));

        viewData.setBerechtigungenForRoleWithLeserecht(new BerechtigungDto(berechtigungTyp, Collections.emptyList()));
    }

    private void setViewDataBerechtigungForSensorCoc(BerechtigungFlowViewData viewData, BerechtigungTyp berechtigungTyp, List<BerechtigungIdTuple> berechtigungIdTuples, UserPermissions<de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto> userPermissions) {
        List<BerechtigungIdTuple> filterResult;
        filterResult = berechtigungIdTuples
                .stream()
                .filter(data -> userPermissions.getSensorCocAsSensorSchreibend().stream().noneMatch(permission -> permission.getId() == data.getId()))
                .collect(Collectors.toList());
        viewData.setBerechtigungenForRoleWithSchreibrecht(new BerechtigungDto(berechtigungTyp, filterResult));

        if (viewData.getSelectedRole() != Rolle.SENSOR_EINGESCHRAENKT) {
            filterResult = berechtigungIdTuples
                    .stream()
                    .filter(data -> userPermissions.getSensorCocAsSensorLesend().stream().noneMatch(permission -> permission.getId() == data.getId()))
                    .filter(data -> userPermissions.getSensorCocAsSensorSchreibend().stream().noneMatch(permission -> permission.getId() == data.getId()))
                    .collect(Collectors.toList());
            viewData.setBerechtigungenForRoleWithLeserecht(new BerechtigungDto(berechtigungTyp, filterResult));
        } else {
            viewData.setBerechtigungenForRoleWithLeserecht(new BerechtigungDto(berechtigungTyp, new ArrayList<>()));
        }
    }

    private List<BerechtigungIdTuple> getBerechtigungIdTuplesByBerechtigungTyp(BerechtigungTyp berechtigungTyp) {
        switch (berechtigungTyp) {
            case SENSORCOC:
                return getAllSensorCocAsBerechtigungIdTuple();
            case SETEAM:
                return getAllSeTeamAsBerechtigungIdTuple();
            case TTEAM:
                return getAllTteamCocAsBerechtigungIdTuple();
            default:
                return Collections.emptyList();
        }
    }

    private List<BerechtigungIdTuple> getAllTteamCocAsBerechtigungIdTuple() {
        return tteamService.getAllTteams().stream()
                .map(tteam -> new BerechtigungIdTuple(tteam.getId(), tteam.getTeamName()))
                .collect(Collectors.toList());
    }

    private List<BerechtigungIdTuple> getAllSeTeamAsBerechtigungIdTuple() {
        return modulService.getAllSeTeams().stream()
                .map(modulSeTeam -> new BerechtigungIdTuple(modulSeTeam.getId(), modulSeTeam.getName()))
                .collect(Collectors.toList());
    }

    private List<BerechtigungIdTuple> getAllSensorCocAsBerechtigungIdTuple() {
        return sensorCocService.getAllSortByTechnologie().stream()
                .map(sensorCoc -> new BerechtigungIdTuple(sensorCoc.getSensorCocId(), sensorCoc.getTechnologie()))
                .collect(Collectors.toList());
    }

    public List<Berechtigungsantrag> persistAntrag(BerechtigungFlowViewData viewData) {
        removeDoppelteAntraegeMitSchreibUndLeseRecht(viewData);
        return createListOfBerechtigungsAntragFromViewData(viewData);
    }

    private void removeDoppelteAntraegeMitSchreibUndLeseRecht(BerechtigungFlowViewData viewData) {
        if (viewData.getSelectedBerechtigungenWithLeserecht() == null) {
            return;
        }

        for (BerechtigungIdTuple berechtigungIdTuple : viewData.getSelectedBerechtigungenWithLeserecht()) {
            if (viewData.getSelectedBerechtigungenWithSchreibrecht().contains(berechtigungIdTuple)) {
                viewData.getSelectedBerechtigungenWithLeserecht().remove(berechtigungIdTuple);
            }
        }
    }

    private List<Berechtigungsantrag> createListOfBerechtigungsAntragFromViewData(BerechtigungFlowViewData viewData) {
        List<Berechtigungsantrag> result = new ArrayList<>();

        LOG.debug("Starting with BerechtigungAntraege for Schreibrecht");
        if (viewData.getSelectedBerechtigungenWithSchreibrecht() == null || viewData.getSelectedBerechtigungenWithSchreibrecht().isEmpty()) {
            LOG.debug("No Data selected for BerechtigungAntraege with Schreibrecht");
        } else {
            result.addAll(createAndPersistBerechtigungsAntrag(viewData, Rechttype.SCHREIBRECHT));
        }

        LOG.debug("Starting with BerechtigungAntraege for Leserecht");
        if (viewData.getSelectedBerechtigungenWithLeserecht() == null || viewData.getSelectedBerechtigungenWithLeserecht().isEmpty()) {
            LOG.debug("No Data selected for BerechtigungAntraege with Leserecht");
        } else {
            result.addAll(createAndPersistBerechtigungsAntrag(viewData, Rechttype.LESERECHT));
        }

        berechtigungsantragMailService.sendMailBasedOnAntragStatus(result);

        return result;
    }

    private List<Berechtigungsantrag> createAndPersistBerechtigungsAntrag(BerechtigungFlowViewData viewData, Rechttype rechttype) {

        Iterator<BerechtigungIdTuple> berechtigungIterator = getIteratorBasedOnRechttyp(viewData, rechttype);

        List<Berechtigungsantrag> result = new ArrayList<>();
        Mitarbeiter user = session.getUser();
        user.setDatenschutzAngenommen(viewData.isDatenschutzAngenommen());

        while (berechtigungIterator.hasNext()) {
            BerechtigungIdTuple berechtigung = berechtigungIterator.next();

            Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag();

            berechtigungsantrag.setRechttype(rechttype);
            berechtigungsantrag.setRolle(viewData.getSelectedRole());

            berechtigungsantrag.setAntragsteller(user);

            BerechtigungTyp berechtigungTyp = getBerechtigungTyp(viewData, rechttype);

            boolean antragAlreadyExit = false;

            switch (berechtigungTyp) {
                case SENSORCOC:
                    SensorCoc sensorCocToSave = sensorCocService.getSensorCocByTechnologie(berechtigung.getValue());
                    antragAlreadyExit = setSensorCocIfAntragNotExist(viewData, rechttype, user, berechtigungsantrag, antragAlreadyExit, sensorCocToSave);
                    break;
                case TTEAM:
                    Tteam tteamToSave = tteamService.getTteamByName(berechtigung.getValue());
                    antragAlreadyExit = setTteamIfAntragNotExist(viewData, rechttype, user, berechtigungsantrag, antragAlreadyExit, tteamToSave);
                    break;
                case SETEAM:
                    antragAlreadyExit = setModulSeTeamIfAntragNotExist(viewData, rechttype, user, berechtigung, berechtigungsantrag, antragAlreadyExit);
                    break;
                default:
                    break;
            }
            if (antragAlreadyExit) {
                LOG.warn("Antrag allready exist User:{} Rolle:{} Berechtigung:{} Rechttyp{}", user, viewData.getSelectedRole(), berechtigung, rechttype);
                continue;
            }
            LOG.debug("Trying to persist {}", berechtigungsantrag);
            persistUserIfNotExist(user);
            berechtigungAntragService.persistBerechtigungAntrag(berechtigungsantrag);
            result.add(berechtigungsantrag);
        }

        return result;
    }

    private boolean setModulSeTeamIfAntragNotExist(BerechtigungFlowViewData viewData, Rechttype rechttype, Mitarbeiter user, BerechtigungIdTuple berechtigung, Berechtigungsantrag berechtigungsantrag, boolean antragAlreadyExit) {
        ModulSeTeam modulSeTeam = modulService.getSeTeamByName(berechtigung.getValue());
        if (modulSeTeamAntragAlreadyExit(user, viewData.getSelectedRole(), rechttype, modulSeTeam)) {
            antragAlreadyExit = true;
        } else {
            berechtigungsantrag.setModulSeTeam(modulSeTeam);
        }
        return antragAlreadyExit;
    }

    private boolean setTteamIfAntragNotExist(BerechtigungFlowViewData viewData, Rechttype rechttype, Mitarbeiter user, Berechtigungsantrag berechtigungsantrag, boolean antragAlreadyExit, Tteam tteamToSave) {
        if (tteamAntragAlreadyExit(user, viewData.getSelectedRole(), rechttype, tteamToSave)) {
            antragAlreadyExit = true;
        } else {
            berechtigungsantrag.setTteam(tteamToSave);
        }
        return antragAlreadyExit;
    }

    private boolean setSensorCocIfAntragNotExist(BerechtigungFlowViewData viewData, Rechttype rechttype, Mitarbeiter user, Berechtigungsantrag berechtigungsantrag, boolean antragAlreadyExit, SensorCoc sensorCocToSave) {
        if (sensorCocAntragAlreadyExit(user, viewData.getSelectedRole(), rechttype, sensorCocToSave)) {
            antragAlreadyExit = true;
        } else {
            berechtigungsantrag.setSensorCoc(sensorCocToSave);
        }
        return antragAlreadyExit;
    }


    private boolean sensorCocAntragAlreadyExit(Mitarbeiter antragsteller, Rolle rolle, Rechttype rechttype, SensorCoc sensorCoc) {
        return !berechtigungAntragService.getAntraegeByAntragstellerRolleRechttypSensorCoc(antragsteller, rolle, rechttype, sensorCoc).isEmpty();
    }

    private boolean tteamAntragAlreadyExit(Mitarbeiter antragsteller, Rolle rolle, Rechttype rechttype, Tteam tteam) {
        return !berechtigungAntragService.getAntraegeByAntragstellerRolleRechttypTteam(antragsteller, rolle, rechttype, tteam).isEmpty();
    }

    private boolean modulSeTeamAntragAlreadyExit(Mitarbeiter antragsteller, Rolle rolle, Rechttype rechttype, ModulSeTeam modulSeTeam) {
        return !berechtigungAntragService.getAntraegeByAntragstellerRolleRechttypModulSeTeam(antragsteller, rolle, rechttype, modulSeTeam).isEmpty();
    }

    private BerechtigungTyp getBerechtigungTyp(BerechtigungFlowViewData viewData, Rechttype rechttype) {
        if (Rechttype.LESERECHT.equals(rechttype)) {
            return viewData.getBerechtigungenForRoleWithLeserecht().getBerechtigungTyp();
        } else {
            return viewData.getBerechtigungenForRoleWithSchreibrecht().getBerechtigungTyp();
        }
    }

    private void persistUserIfNotExist(Mitarbeiter user) {
        if (!userSearchService.isExistsMitarbeiter(user.getQNumber())) {
            userService.saveMitarbeiter(user);
        }

    }

    private Iterator<BerechtigungIdTuple> getIteratorBasedOnRechttyp(BerechtigungFlowViewData viewData, Rechttype rechttype) {
        if (rechttype.equals(Rechttype.SCHREIBRECHT)) {
            return viewData.getSelectedBerechtigungenWithSchreibrecht().iterator();
        } else {
            return viewData.getSelectedBerechtigungenWithLeserecht().iterator();
        }
    }

    public boolean isDatenschutzAngenommen() {
        Mitarbeiter user = session.getUser();
        if (user != null) {
            return user.isDatenschutzAngenommen();
        } else {
            return false;
        }
    }
}
