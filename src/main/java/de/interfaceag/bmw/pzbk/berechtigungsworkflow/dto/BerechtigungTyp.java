package de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto;

import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.Optional;

public enum BerechtigungTyp {

    SENSORCOC,
    TTEAM,
    SETEAM;

    BerechtigungTyp() {
    }

    public static Optional<BerechtigungTyp> getBerechtigungTypByRolle(Rolle rolle) {
        switch (rolle) {
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
                return Optional.of(BerechtigungTyp.SENSORCOC);
            case E_COC:
                return Optional.of(BerechtigungTyp.SETEAM);
            case TTEAMMITGLIED:
                return Optional.of(BerechtigungTyp.TTEAM);
            default:
                return Optional.empty();
        }
    }
}
