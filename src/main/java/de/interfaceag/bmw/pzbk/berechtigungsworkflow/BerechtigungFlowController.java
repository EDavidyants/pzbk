package de.interfaceag.bmw.pzbk.berechtigungsworkflow;

import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungFlowViewData;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.flow.FlowScoped;
import javax.faces.lifecycle.ClientWindow;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;

@Named
@FlowScoped("berechtigung")
public class BerechtigungFlowController implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(BerechtigungFlowController.class);

    @Inject
    private BerechtigungFlowViewData viewData;
    @Inject
    private BerechtigungFlowFacade berechtigungFlowFacade;

    @PostConstruct
    public void init() {
        viewData.setDatenschutzAngenommen(berechtigungFlowFacade.isDatenschutzAngenommen());
    }

    public String getReturnValue() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        ClientWindow clientWindow = externalContext.getClientWindow();
        clientWindow.disableClientWindowRenderMode(facesContext);

        return "/dashboard.xhtml?faces-redirect=true";
    }

    public void updateBerechtigungenForRole() {
        LOG.debug("update berechtigung for selected role {}", viewData.getSelectedRole());
        berechtigungFlowFacade.getBerechtigungForRolle(viewData);
    }

    public void persistAntrag() {
        if (viewData.isDatenschutzAngenommen()) {
            LOG.debug("Persist new application");
            berechtigungFlowFacade.persistAntrag(viewData);
        } else {
            LOG.error("Data protection is not accepted. Do not persist data!");
        }
    }

    public String displayNoneWhenEcoc() {
        if (viewData.getSelectedRole() == Rolle.E_COC) {
            return "display: none";
        }
        return "";
    }

    public BerechtigungFlowViewData getViewData() {
        return viewData;
    }

    public boolean isDatenschutzAngenommen() {
        return berechtigungFlowFacade.isDatenschutzAngenommen();
    }

    public boolean isButtonDisabled() {
        if (viewData.getSelectedRole() == Rolle.E_COC &&
                (this.viewData.getSelectedBerechtigungenWithSchreibrecht() == null || this.viewData.getSelectedBerechtigungenWithSchreibrecht().isEmpty())) {
            return true;
        }
        if (viewData.getSelectedRole() != Rolle.E_COC &&
                (this.viewData.getSelectedBerechtigungenWithLeserecht() == null || this.viewData.getSelectedBerechtigungenWithLeserecht().isEmpty()) &&
                (this.viewData.getSelectedBerechtigungenWithSchreibrecht() == null || this.viewData.getSelectedBerechtigungenWithSchreibrecht().isEmpty())) {
            return true;
        }

        return false;
    }

    public void resetSelectedBerechtigungen() {
        this.viewData.setSelectedBerechtigungenWithLeserecht(new ArrayList<>());
        this.viewData.setSelectedBerechtigungenWithSchreibrecht(new ArrayList<>());
    }
}
