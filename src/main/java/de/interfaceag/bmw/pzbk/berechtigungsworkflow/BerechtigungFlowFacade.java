package de.interfaceag.bmw.pzbk.berechtigungsworkflow;

import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungFlowViewData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;

@Stateless
public class BerechtigungFlowFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(BerechtigungFlowFacade.class);

    @Inject
    private BerechtigungFlowService berechtigungFlowService;

    public void getBerechtigungForRolle(BerechtigungFlowViewData viewData) {
        berechtigungFlowService.getBerechtigungForRolle(viewData);
    }

    public void persistAntrag(BerechtigungFlowViewData viewData) {
        berechtigungFlowService.persistAntrag(viewData);
    }

    public boolean isDatenschutzAngenommen() {
        return berechtigungFlowService.isDatenschutzAngenommen();
    }
}
