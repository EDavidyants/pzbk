package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.shared.objectIds.AbstractDomainObject;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Dependent
public class ZuordnungAutomaticProcessDao implements Serializable {

    public static final Logger LOG = LoggerFactory.getLogger(ZuordnungAutomaticProcessDao.class);

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public List<Anforderung> getAnforderungForFahrzeugmerkmalAndAuspraegungen(DerivatId derivatId, List<AnforderungId> anforderungIds) {
        List<FahrzeugmerkmalAuspraegung> auspraegungenForDerivat = getAuspraegungenForDerivat(derivatId);
        List<Long> auspraegungIds = getAuspraegungIdsForDerivat(auspraegungenForDerivat);
        return getAnforderungForAutomatischZuordnenWithAuspraegungIds(anforderungIds, auspraegungIds);
    }

    private List<Anforderung> getAnforderungForAutomatischZuordnenWithAuspraegungIds(List<AnforderungId> anforderungIds, List<Long> auspraegungIds) {

        if (Objects.isNull(anforderungIds) || anforderungIds.isEmpty()) {
            LOG.error("parameter anforderungIds is null or empty");
            return Collections.emptyList();
        }

        if (Objects.isNull(auspraegungIds) || auspraegungIds.isEmpty()) {
            LOG.error("parameter auspraegungIds is null or empty");
            return Collections.emptyList();
        }

        List<Long> anforderungsIdsAsLong = anforderungIds.stream().map(AbstractDomainObject::getId).collect(Collectors.toList());

        TypedQuery<Anforderung> query = entityManager.createQuery(
                "SELECT anforderungAuspraegung.anforderung from AnforderungModulSeTeamFahrzeugmerkmalAuspraegung anforderungAuspraegung " +
                        "WHERE anforderungAuspraegung.fahrzeugmerkmalAuspraegung.id in :auspraegungIds " +
                        "AND anforderungAuspraegung.anforderung.id IN :anforderungIds", Anforderung.class);

        query.setParameter("anforderungIds", anforderungsIdsAsLong);
        query.setParameter("auspraegungIds", auspraegungIds);

        return query.getResultList();
    }

    private static List<Long> getAuspraegungIdsForDerivat(List<FahrzeugmerkmalAuspraegung> auspraegungenForDerivat) {
        return auspraegungenForDerivat.stream()
                .filter(Objects::nonNull)
                .map(FahrzeugmerkmalAuspraegung::getId)
                .collect(Collectors.toList());
    }

    private List<FahrzeugmerkmalAuspraegung> getAuspraegungenForDerivat(DerivatId derivatId) {
        TypedQuery<FahrzeugmerkmalAuspraegung> query = entityManager.createQuery("SELECT der.auspraegungen from DerivatFahrzeugmerkmal der " +
                "WHERE der.derivat.id = :derivatId", FahrzeugmerkmalAuspraegung.class);

        query.setParameter("derivatId", derivatId.getId());
        return query.getResultList();
    }
}
