package de.interfaceag.bmw.pzbk.zuordnung;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author sl
 */
public final class ZuordnungZakStatusUtils {

    private ZuordnungZakStatusUtils() {
    }

    public static Stream<ZuordnungZakStatus> getAllZuordnungZakStatus() {
        return Stream.of(ZuordnungZakStatus.values());
    }

    public static List<ZuordnungZakStatus> getAllgetAllZuordnungZakStatusAsList() {
        return getAllZuordnungZakStatus().collect(Collectors.toList());
    }

    public static ZuordnungZakStatus getZuordnungZakStatusForBoolean(boolean booleanToMap) {
        return getAllZuordnungZakStatus().filter(p -> p.getValue() == booleanToMap).findAny().orElse(null);
    }

    public static ZuordnungZakStatus getZuordnungZakStatusForString(String stringToMap) {
        if (stringToMap != null) {
            switch (stringToMap) {
                case "noch nicht übertragen":
                    return getZuordnungZakStatusForBoolean(false);
                case "nach ZAK übertragen":
                    return getZuordnungZakStatusForBoolean(true);
                default:
                    break;
            }
        }
        return null;
    }

}
