package de.interfaceag.bmw.pzbk.zuordnung;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public enum ZuordnungZakStatus {
    OFFEN(0), ZAK(1);

    private boolean value;
    private String beschreibung;
    private String key;

    // ------------  constructors ----------------------------------------------
    ZuordnungZakStatus(int id) {
        switch (id) {
            case 0:
                this.value = false;
                this.beschreibung = "noch nicht übertragen";
                this.key = "nochNichtUebertragen";
                break;
            case 1:
                this.value = true;
                this.beschreibung = "nach ZAK übertragen";
                this.key = "nachZakUebertragen";
                break;
            default:
                break;
        }
    }

    public boolean getValue() {
        return value;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    /**
     * get the key for the localization file.
     *
     * @return key
     */
    public String getKey() {
        return key;
    }

}
