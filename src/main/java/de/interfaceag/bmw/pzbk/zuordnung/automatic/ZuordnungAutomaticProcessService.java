package de.interfaceag.bmw.pzbk.zuordnung.automatic;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAutomaticProcessDao;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungProcessChangeService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungViewData;
import de.interfaceag.bmw.pzbk.zuordnung.automatic.util.ZuordnungToDtoConverter;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Named
@Stateless
public class ZuordnungAutomaticProcessService {

    public static final Logger LOG = LoggerFactory.getLogger(ZuordnungAutomaticProcessService.class);

    @Inject
    private DerivatService derivatService;
    @Inject
    private ZuordnungProcessChangeService zuordnungProcessChangeService;

    @Inject
    private ZuordnungAutomaticProcessDao automaticProcessDao;

    public AutomaticZuordnenDialogViewData assignAnforderungenAutomatic(ZuordnungViewData viewData) {
        DerivatDto selectedDerivat = viewData.getSelectedDerivat();
        DerivatId derivatId = new DerivatId(selectedDerivat.getId());

        List<AnforderungId> anforderungIds = viewData.getAnforderungen().stream().map(anforderung -> new AnforderungId(anforderung.getId())).collect(Collectors.toList());

        List<Anforderung> anforderungsList = automaticProcessDao.getAnforderungForFahrzeugmerkmalAndAuspraegungen(derivatId, anforderungIds);
        if (anforderungsList.isEmpty()) {
            LOG.error("result of automatic zuordnung is empty");
            return null;
        } else {
            return assignAnforderungenToDerivat(viewData, selectedDerivat, derivatId, anforderungsList);
        }
    }

    private AutomaticZuordnenDialogViewData assignAnforderungenToDerivat(ZuordnungViewData viewData, DerivatDto selectedDerivat, DerivatId derivatId, List<Anforderung> anforderungsList) {
        Derivat derivat = findDerivatForId(derivatId);
        List<Zuordnung> openZuordnungen = getOpenZuordnungen(viewData.getTableData(), selectedDerivat);
        List<Zuordnung> restrictedZuordnungen = getZuordungForAnforderungen(anforderungsList, openZuordnungen);
        final Collection<ZuordnungAnforderungDerivat> actualChanges = zuordnungProcessChangeService.processZuordnungChangesInBulkMode(ZuordnungStatus.ZUGEORDNET, derivat, restrictedZuordnungen);
        return ZuordnungToDtoConverter.buildAutomaticZuordnenDialogViewData(actualChanges);
    }

    private List<Zuordnung> getZuordungForAnforderungen(List<Anforderung> anforderungForAutomatischZuordnen, List<Zuordnung> zuordnungen) {
        List<Zuordnung> restrictedList = new ArrayList<>();

        zuordnungen.forEach(zuordnung -> {
            Optional<Anforderung> foundAnforderung = anforderungForAutomatischZuordnen.stream()
                    .filter(anforderung -> anforderung.getId().equals(zuordnung.getAnforderungId()))
                    .findFirst();
            if (foundAnforderung.isPresent()) {
                restrictedList.add(zuordnung);
            }
        });

        return restrictedList;
    }

    private List<Zuordnung> getOpenZuordnungen(List<Zuordnung> zuordnungList, DerivatDto derivatDto) {
        return zuordnungList.stream()
                .filter(zuordnung -> zuordnung.getZuordnungForDerivat(derivatDto).getStatus() == ZuordnungStatus.OFFEN)
                .collect(Collectors.toList());
    }

    private Derivat findDerivatForId(DerivatId derivatId) {
        return derivatService.getDerivatById(derivatId.getId());
    }
}
