package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @author sl
 */
@Named
@Stateless
public class ZuordnungProcessService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ZuordnungProcessService.class);

    @Inject
    private DerivatService derivatService;

    @Inject
    private ZuordnungProcessChangeService zuordnungProcessChangeService;
    @Inject
    private ZuordnungProcessZakService zuordnungProcessZakService;

    public void processChanges(ZuordnungViewData viewData) {

        boolean zuordnenMode = viewData.isZuordnenMode();
        boolean zakMode = viewData.isZakMode();

        if (zuordnenMode) {
            processZuordnungChanges(viewData);
        } else if (zakMode) {
            processZakChanged(viewData);
        }
    }

    private void processZakChanged(ZuordnungViewData viewData) {
        List<Zuordnung> selectedTableData = viewData.getSelectedTableData();
        DerivatDto selectedDerivat = viewData.getSelectedDerivat();
        zuordnungProcessZakService.processZakChanges(selectedTableData, selectedDerivat);
    }

    private void processZuordnungChanges(ZuordnungViewData viewData) {

        ZuordnungStatus groupStatus = viewData.getGroupStatus();
        List<Zuordnung> selectedTableData = viewData.getSelectedTableData();
        DerivatDto selectedDerivat = viewData.getSelectedDerivat();
        Derivat derivat = getDerivat(selectedDerivat.getId());

        if (isBulkMode(groupStatus, selectedTableData)) {
            processZuordnungenInBulkMode(viewData);
        } else {
            List<Zuordnung> zuordnungen = viewData.getTableData();
            zuordnungProcessChangeService.processZuordnungChangesInSingleMode(derivat, zuordnungen);
        }
    }

    private void processZuordnungenInBulkMode(ZuordnungViewData viewData) {
        ZuordnungStatus groupStatus = viewData.getGroupStatus();
        DerivatDto selectedDerivat = viewData.getSelectedDerivat();
        Derivat derivat = getDerivat(selectedDerivat.getId());

        List<Zuordnung> selectedDataToProcess;
        if (viewData.isAllSelected()) {
            selectedDataToProcess = viewData.getTableData();
            LOG.info("Zuordnung: Alle Ausgewaehlt Option for Derivat {} aktiviert. Total processing: {}", selectedDerivat.getName(), selectedDataToProcess.size());
        } else {
            selectedDataToProcess = viewData.getSelectedTableData();
            LOG.info("Zuordnung: nicht alle Elemente for Derivat {} wurden ausgewaehlt. Total ausgewaehlt: {}", selectedDerivat.getName(), selectedDataToProcess.size());
        }

        zuordnungProcessChangeService.processZuordnungChangesInBulkMode(groupStatus, derivat, selectedDataToProcess);
    }

    private static boolean isBulkMode(ZuordnungStatus groupStatus, Collection selectedTableData) {
        return selectedTableData != null && !selectedTableData.isEmpty() && groupStatus != null;
    }

    private Derivat getDerivat(Long id) {
        return derivatService.getDerivatById(id);
    }
}
