package de.interfaceag.bmw.pzbk.zuordnung.dto;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.prozessbaukasten.dto.ProzessbaukastenLabelDto;
import de.interfaceag.bmw.pzbk.shared.utils.AnforderungUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author sl
 */
public final class ZuordnungBuilder {

    private Long anforderungId;
    private String anforderungLabel;
    private String anforderungFachId;
    private Integer anforderungVersion;
    private String anforderungBeschreibung;
    private boolean standardvereinbarung;
    private final Map<DerivatDto, DerivatZuordnung> derivatZuordnungen = new HashMap<>();
    private ProzessbaukastenLabelDto prozessbaukastenLabel;

    public static ZuordnungBuilder forAnforderung(Anforderung anforderung) {
        return new ZuordnungBuilder().withAnforderung(anforderung);
    }

    private ZuordnungBuilder() {
    }

    private void setStandardvereinbarung(Anforderung anforderung) {
        boolean isStandardvereinbarung = AnforderungUtils.isStandardvereinbarung(anforderung);
        this.standardvereinbarung = isStandardvereinbarung;
    }

    private ZuordnungBuilder withAnforderung(Anforderung anforderung) {
        this.anforderungId = anforderung.getId();
        this.anforderungFachId = anforderung.getFachId();
        this.anforderungVersion = anforderung.getVersion();
        this.anforderungLabel = anforderung.toString();
        this.anforderungBeschreibung = anforderung.getBeschreibungAnforderungDe();
        this.prozessbaukastenLabel = buildProzessbaukastenLabelDtoFromAnforderung(anforderung).orElse(null);
        setStandardvereinbarung(anforderung);
        return this;
    }

    private Optional<ProzessbaukastenLabelDto> buildProzessbaukastenLabelDtoFromAnforderung(Anforderung anforderung) {
        return anforderung.getGueltigerProzessbaukasten().map(prozessbaukasten -> new ProzessbaukastenLabelDto(prozessbaukasten.getId(), prozessbaukasten.getFachId(), prozessbaukasten.getVersion()));
    }

    public ZuordnungBuilder withDerivatZuordnung(DerivatZuordnung derivatZuordnung, DerivatDto derivat) {
        this.derivatZuordnungen.put(derivat, derivatZuordnung);
        return this;
    }

    public Zuordnung build() {
        return new ZuordnungDto(anforderungId, anforderungLabel, anforderungFachId, anforderungVersion, anforderungBeschreibung, standardvereinbarung, derivatZuordnungen, prozessbaukastenLabel);
    }
}
