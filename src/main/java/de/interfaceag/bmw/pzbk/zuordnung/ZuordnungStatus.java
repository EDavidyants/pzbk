package de.interfaceag.bmw.pzbk.zuordnung;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public enum ZuordnungStatus {

    OFFEN(0),
    KEINE_ZUORDNUNG(2),
    ZUGEORDNET(7);

    private final int id;

    ZuordnungStatus(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static Optional<ZuordnungStatus> getById(Integer id) {
        if (id == null) {
            return Optional.empty();
        } else {
            return Stream.of(ZuordnungStatus.values()).filter(status -> id.equals(status.getId())).findAny();
        }
    }

    @Override
    public String toString() {
        return getStatusBezeichnung();
    }

    public String getStatusBezeichnung() {
        switch (this) {
            case OFFEN:
                return "offen";
            case KEINE_ZUORDNUNG:
                return "keine Zuordnung";
            case ZUGEORDNET:
                return "zugeordnet";
            default:
                return "";
        }
    }

    public static List<ZuordnungStatus> getAll() {
        return Arrays.asList(values());
    }

    public List<ZuordnungStatus> getNextStatus() {
        switch (this) {
            case OFFEN:
                return Arrays.asList(ZuordnungStatus.KEINE_ZUORDNUNG, ZuordnungStatus.ZUGEORDNET);
            case KEINE_ZUORDNUNG:
                return Collections.singletonList(ZuordnungStatus.ZUGEORDNET);
            case ZUGEORDNET:
                return Collections.singletonList(ZuordnungStatus.OFFEN);
            default:
                return Collections.emptyList();
        }
    }

}
