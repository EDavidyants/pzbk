package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.filter.UrlFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ZuordnungViewData implements Serializable {

    private final ZuordnungViewFilter filter;

    private final ZuordnungViewPermission viewPermission;

    private final List<Anforderung> anforderungen;

    private final List<DerivatDto> derivate;
    private List<DerivatDto> filteredDerivate;
    private DerivatDto selectedDerivat;

    private final List<Zuordnung> tableData;
    private List<Zuordnung> selectedTableData = new ArrayList<>();

    private ZuordnungStatus groupStatus;
    private boolean allSelected;

    private boolean zuordnenMode;
    private boolean zakMode;

    private final DerivatStatusNames derivatStatusNames;

    protected ZuordnungViewData(
            Set<Rolle> userRoles,
            UrlParameter urlParameter,
            ZuordnungViewFilter filter,
            List<DerivatDto> derivate,
            List<Anforderung> anforderungen,
            List<Zuordnung> tableData,
            List<DerivatDto> filteredDerivate,
            DerivatStatusNames derivatStatusNames
    ) {
        this.filter = filter;
        this.anforderungen = anforderungen;
        this.derivate = derivate;
        this.filteredDerivate = filteredDerivate;
        this.tableData = tableData;
        parseUrlParameter(urlParameter, filter);
        parseSelectedDerivat(urlParameter, filter, derivate);
        setSelectedDerivatIfListIsNotEmpty();
        this.viewPermission = ZuordnungViewPermissionUtils.getViewPermission(userRoles, zakMode, zuordnenMode, selectedDerivat);
        this.derivatStatusNames = derivatStatusNames;
        this.allSelected = false;
    }

    private void setSelectedDerivatIfListIsNotEmpty() {
        if (!filteredDerivate.isEmpty() && selectedDerivat == null) {
            this.selectedDerivat = filteredDerivate.get(0);
        }
    }

    private void parseUrlParameter(UrlParameter urlParameter, ZuordnungViewFilter viewFilter) {
        setZakMode(ZuordnungUrlParameterUtils.getModeValue(urlParameter, viewFilter.getZakModeFilter()));
        setZuordnenMode(ZuordnungUrlParameterUtils.getModeValue(urlParameter, viewFilter.getZuordnenModeFilter()));
    }

    private void parseSelectedDerivat(UrlParameter urlParameter, ZuordnungViewFilter viewFilter, List<DerivatDto> allDerivate) {
        UrlFilter selectedDerivatFilter = viewFilter.getSelectedDerivatFilter();
        ZuordnungUrlParameterUtils.getSelectedDerivat(urlParameter, selectedDerivatFilter, allDerivate)
                .ifPresent(d -> setSelectedDerivat(d));
    }

    public ZuordnungViewFilter getFilter() {
        return filter;
    }

    public List<DerivatDto> getDerivate() {
        return derivate;
    }

    public List<DerivatDto> getFilteredDerivate() {
        return filteredDerivate;
    }

    public DerivatDto getSelectedDerivat() {
        return selectedDerivat;
    }

    public void setSelectedDerivat(DerivatDto selectedDerivat) {
        if (selectedDerivat != null) {
            this.selectedDerivat = selectedDerivat;
            this.getFilter().getSelectedDerivatFilter().setAttributeValue(selectedDerivat.getId().toString());
        }
    }

    public void setFilteredDerivate(List<DerivatDto> filteredDerivate) {
        this.filteredDerivate = filteredDerivate;
    }

    public List<Anforderung> getAnforderungen() {
        return anforderungen;
    }

    public List<Zuordnung> getTableData() {
        return tableData;
    }

    public List<Zuordnung> getSelectedTableData() {
        return selectedTableData;
    }

    public void setSelectedTableData(List<Zuordnung> selectedTableData) {
        this.selectedTableData = selectedTableData;
    }

    public ZuordnungStatus getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(ZuordnungStatus groupStatus) {
        this.groupStatus = groupStatus;
    }

    public boolean isAllSelected() {
        return allSelected;
    }

    public void setAllSelected(boolean allSelected) {
        if (allSelected) {
            selectedTableData = getTableData();
        } else {
            selectedTableData = new ArrayList<>();
        }
        this.allSelected = allSelected;
    }

    public boolean isZuordnenMode() {
        return zuordnenMode;
    }

    public void setZuordnenMode(boolean zuordnenMode) {
        this.zuordnenMode = zuordnenMode;
    }

    public boolean isZakMode() {
        return zakMode;
    }

    public void setZakMode(boolean zakMode) {
        this.zakMode = zakMode;
    }

    public ZuordnungViewPermission getViewPermission() {
        return viewPermission;
    }

    public DerivatStatusNames getDerivatStatusNames() {
        return derivatStatusNames;
    }

}
