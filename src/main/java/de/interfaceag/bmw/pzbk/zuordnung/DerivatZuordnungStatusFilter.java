package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.filter.BooleanSearchFilter;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ZuordnungStatusFilter;
import de.interfaceag.bmw.pzbk.filter.ZuordnungZakStatusFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DerivatZuordnungStatusFilter implements Serializable {

    private final Long derivatId;
    private final String derivatName;

    private final MultiValueEnumSearchFilter<ZuordnungStatus> zuordnungStatusFilter;
    private final BooleanSearchFilter zuordnungZakStatusFilter;

    protected DerivatZuordnungStatusFilter(
            Long derivatId,
            String derivatName,
            UrlParameter urlParameter) {
        this.derivatName = derivatName;
        this.derivatId = derivatId;

        zuordnungStatusFilter = new ZuordnungStatusFilter(urlParameter, derivatName);
        zuordnungZakStatusFilter = new ZuordnungZakStatusFilter(urlParameter, derivatName);
    }

    public String getParameter() {
        StringBuilder sb = new StringBuilder();
        sb.append(zuordnungStatusFilter.getIndependentParameter());
        sb.append(zuordnungZakStatusFilter.getIndependentParameter());
        return sb.toString();
    }

    public MultiValueEnumSearchFilter<ZuordnungStatus> getZuordnungStatusFilter() {
        return zuordnungStatusFilter;
    }

    public BooleanSearchFilter getZuordnungZakStatusFilter() {
        return zuordnungZakStatusFilter;
    }

    public String getDerivatName() {
        return derivatName;
    }

    public Long getDerivatId() {
        return derivatId;
    }

}
