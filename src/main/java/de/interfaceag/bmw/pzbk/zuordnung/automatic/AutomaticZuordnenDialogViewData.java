package de.interfaceag.bmw.pzbk.zuordnung.automatic;

import java.io.Serializable;
import java.util.List;

public final class AutomaticZuordnenDialogViewData implements Serializable {

    private List<AutomaticZuordnenDto> automaticZuordnenDtoList;

    private AutomaticZuordnenDialogViewData(Builder builder) {
        automaticZuordnenDtoList = builder.automaticZuordnenDtoList;
    }

    public List<AutomaticZuordnenDto> getAutomaticZuordnenDtoList() {
        return automaticZuordnenDtoList;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private List<AutomaticZuordnenDto> automaticZuordnenDtoList;

        private Builder() {
        }

        public Builder withAutomaticZuordnenDtoList(List<AutomaticZuordnenDto> val) {
            automaticZuordnenDtoList = val;
            return this;
        }

        public AutomaticZuordnenDialogViewData build() {
            return new AutomaticZuordnenDialogViewData(this);
        }
    }
}
