package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOptionalTupleComparator;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOrders;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungenOrderService;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author sl
 */
@Stateless
@Named
public class ZuordnungTableDataSortService {

    private static final Logger LOG = LoggerFactory.getLogger(ZuordnungTableDataSortService.class.getName());

    @Inject
    private ProzessbaukastenAnforderungenOrderService prozessbaukastenAnforderungenOrderService;

    public List<Zuordnung> sortTableData(List<Zuordnung> zuordnungen) {
        LOG.debug("Sort table data");
        LOG.debug("Order before sorting: {}", zuordnungen);

        Collection<Long> prozessbaukastenIdsForZuordnungen = getProzessbaukastenIdsForZuordnungen(zuordnungen);

        if (prozessbaukastenIdsForZuordnungen.isEmpty()) {
            return zuordnungen;
        }

        ProzessbaukastenAnforderungOrders anforderungenOrders = prozessbaukastenAnforderungenOrderService.getAnforderungenOrderForProzessbaukasten(prozessbaukastenIdsForZuordnungen);

        List<Zuordnung> prozessbaukastenAnforderungen = sortProzessbaukastenAnforderungen(zuordnungen, anforderungenOrders);
        List<Zuordnung> otherAnforderungen = sortNormalAnforderungen(zuordnungen, anforderungenOrders);

        prozessbaukastenAnforderungen.addAll(otherAnforderungen);

        LOG.debug("Order after sorting: {}", prozessbaukastenAnforderungen);

        return prozessbaukastenAnforderungen;
    }

    private static List<Zuordnung> sortProzessbaukastenAnforderungen(List<Zuordnung> zuordnungen, ProzessbaukastenAnforderungOrders anforderungenOrders) {
        ProzessbaukastenAnforderungOptionalTupleComparator<Zuordnung> zuordnungProzessbaukastenAnforderungComparator = new ProzessbaukastenAnforderungOptionalTupleComparator<>(anforderungenOrders);

        return zuordnungen.stream()
                .filter(Zuordnung::isProzessbaukastenZugeordnet)
                .sorted(zuordnungProzessbaukastenAnforderungComparator)
                .collect(Collectors.toList());
    }

    private static List<Zuordnung> sortNormalAnforderungen(List<Zuordnung> zuordnungen, ProzessbaukastenAnforderungOrders anforderungenOrders) {
        ProzessbaukastenAnforderungOptionalTupleComparator<Zuordnung> zuordnungProzessbaukastenAnforderungComparator = new ProzessbaukastenAnforderungOptionalTupleComparator<>(anforderungenOrders);

        return zuordnungen.stream()
                .filter(zuordnung -> !zuordnung.isProzessbaukastenZugeordnet())
                .sorted(zuordnungProzessbaukastenAnforderungComparator)
                .collect(Collectors.toList());
    }

    private static Collection<Long> getProzessbaukastenIdsForZuordnungen(Collection<Zuordnung> zuordnungen) {
        return zuordnungen.stream()
                .map(Zuordnung::getProzessbaukastenId)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

}
