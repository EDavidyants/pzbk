package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusService;
import de.interfaceag.bmw.pzbk.entities.AbstractAnfoMgmtObject;
import de.interfaceag.bmw.pzbk.entities.AbstractAnforderung;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.comparator.DerivatComparator;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.PersistedObject;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.LogUtils;
import de.interfaceag.bmw.pzbk.shared.utils.SearchUtil;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerService;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import de.interfaceag.bmw.pzbk.zuordnung.automatic.AutomaticZuordnenDialogViewData;
import de.interfaceag.bmw.pzbk.zuordnung.automatic.ZuordnungAutomaticProcessService;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class ZuordnungViewFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ZuordnungViewFacade.class);

    @Inject
    private Session session;

    @Inject
    private AnforderungService anforderungService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    private ZuordnungSearchService zuordnungSearchService;
    @Inject
    private DerivatStatusService derivatStatusService;

    @Inject
    private ProzessbaukastenReadService prozessbaukastenReadService;
    @Inject
    private ZuordnungTableDataService zuordnungTableDataService;
    @Inject
    private ZuordnungProcessService zuordnungProcessService;
    @Inject
    private ThemenklammerService themenklammerService;
    @Inject
    private ZuordnungAutomaticProcessService zuordnungAutomaticProcessService;

    public void updateUserDerivate(List<DerivatDto> selectedDerivate) {
        Mitarbeiter currentUser = session.getUser();
        List<Long> derivatIds = selectedDerivate.stream().map(PersistedObject::getId).collect(Collectors.toList());
        List<Derivat> derivate = derivatService.getDerivatByIdList(derivatIds);
        derivatService.setUserSelectedDerivate(currentUser, derivate);
    }

    public String process(ZuordnungViewData viewData) {
        zuordnungProcessService.processChanges(viewData);
        viewData.getFilter().getZuordnenModeFilter().setInactive();
        viewData.getFilter().getZakModeFilter().setInactive();
        return viewData.getFilter().getUrl();
    }

    private List<Anforderung> getAnforderungenByFilterList(ZuordnungViewFilter viewFilter) {

        LOG.info("Start getAnforderungenByFilterList");
        Date startDate = new Date();

        //Hier auch viele Queries
        List<Anforderung> seachResult = zuordnungSearchService.getSeachResult(
                viewFilter.getDerivatFilter(),
                viewFilter.getFestgestelltInFilter(), viewFilter.getPhasenrelevanzFilter(),
                viewFilter.getSensorCocFilter(), viewFilter.getAnforderungIdFilter(),
                viewFilter.getFreigegebenSeitFilter(), viewFilter.getAnforderungFilter(),
                viewFilter.getBeschreibungFilter(), viewFilter.getDerivatVereinbarungStatusFilter(), viewFilter.getZakModeFilter(),
                viewFilter.getProzessbaukastenFilter(), viewFilter.getInProzessbaukastenFilter(),
                viewFilter.getNotInProzessbaukastenFilter(), viewFilter.getThemenklammerFilter());

        LOG.info("End getAnforderungenByFilterList");
        Date endDate = new Date();
        LogUtils.logDiff(startDate, endDate);

        return seachResult;
    }

    public ZuordnungViewData getViewData(UrlParameter urlParameter) {

        Mitarbeiter currentUser = session.getUser();

        LOG.debug("currentUser: {}", currentUser);
        LOG.debug("urlParameter: {}", urlParameter);

        LOG.info("Start getLiveKeyFigures");
        Date startDate = new Date();

        List<DerivatDto> selectedDerivate = derivatService.getUserSelectedDerivate(currentUser).stream().map(derivat -> (DerivatDto) derivat).collect(Collectors.toList());

        ZuordnungViewFilter viewFilter = getViewFilter(urlParameter, currentUser, selectedDerivate);

        List<Anforderung> anforderungen;
        List<ProzessbaukastenFilterDto> prozessbaukaesten = new ArrayList<>();
        List<Zuordnung> tableData;

        if (selectedDerivate.isEmpty()) {
            anforderungen = new ArrayList<>();
            tableData = new ArrayList<>();
        } else {
            //Modul Se Team Fetch Prozessbaukasten Fetch
            anforderungen = getAnforderungenByFilterList(viewFilter);

            anforderungen.stream().filter(Anforderung::isProzessbaukastenZugeordnet)
                    .forEach(anforderung ->
                            anforderung.getProzessbaukasten().stream()
                                    .filter(prozessbaukasten -> !isProzessbaukastenDtoInFilterList(prozessbaukaesten, prozessbaukasten))
                                    .forEach(prozessbaukasten -> prozessbaukaesten.add(ProzessbaukastenFilterDto.forProzessbaukasten(prozessbaukasten)))
                    );
            //Anforderung Freigabe bei Modul Fetch
            tableData = generateZuordnungTableData(selectedDerivate, anforderungen);
        }

        LOG.info("End getLiveKeyFigures");
        Date endDate = new Date();
        LogUtils.logDiff(startDate, endDate);

        Collection<SensorCoc> sensorCocs = getSensorCocsForAnforderungen(anforderungen);
        //Festgestellt In Fetch
        Collection<FestgestelltIn> festgestelltIns = getFestgestelltInForAnforderungen(anforderungen);

        viewFilter.restrictSelectableValuesBasedOnSearchResult(sensorCocs, festgestelltIns, prozessbaukaesten, urlParameter);

        DerivatStatusNames derivatStatusNames = getDerivatStatusNames();

        List<Long> derivatAnforderungModulIdSet = getDerivatAnforderungModulIdsFromUrl(urlParameter);
        Set<Long> derivatIdSet = getDerivatIdSet(derivatAnforderungModulIdSet);
        return new ZuordnungViewData(getUserRoles(currentUser), urlParameter, viewFilter, getDerivate(derivatIdSet),
                anforderungen, tableData, selectedDerivate, derivatStatusNames);
    }

    private static boolean isProzessbaukastenDtoInFilterList(List<ProzessbaukastenFilterDto> filterProzessbaukasten, Prozessbaukasten prozessbaukasten) {
        return filterProzessbaukasten.stream().anyMatch(prozessbaukastenDto -> (prozessbaukastenDto.getId().equals(prozessbaukasten.getId())));
    }

    private static Collection<FestgestelltIn> getFestgestelltInForAnforderungen(Collection<Anforderung> anforderungen) {
        return anforderungen.stream().map(AbstractAnforderung::getFestgestelltIn).flatMap(Collection::stream).collect(Collectors.toSet());
    }

    private static Collection<SensorCoc> getSensorCocsForAnforderungen(Collection<Anforderung> anforderungen) {
        return anforderungen.stream().map(AbstractAnfoMgmtObject::getSensorCoc).collect(Collectors.toSet());
    }

    private DerivatStatusNames getDerivatStatusNames() {
        return derivatStatusService.getDerivatStatusNames();
    }

    private Set<Rolle> getUserRoles(Mitarbeiter currentUser) {
        return new HashSet<>(berechtigungService.getRolesForUser(currentUser));
    }

    private List<Zuordnung> generateZuordnungTableData(List<DerivatDto> derivate, List<Anforderung> anforderungen) {
        return zuordnungTableDataService.getTableData(derivate, anforderungen);
    }

    private Set<Long> getDerivatIdSet(List<Long> derivatAnforderungModulIdList) {

        LOG.info("Start getDerivatIdSet");
        Date startDate = new Date();

        if (derivatAnforderungModulIdList != null && !derivatAnforderungModulIdList.isEmpty()) {
            List<DerivatAnforderungModul> derivatAnforderungModulen = derivatAnforderungModulService.getDerivatAnforderungModulByIdList(derivatAnforderungModulIdList);

            LOG.info("End getDerivatIdSet");
            Date endDate = new Date();
            LogUtils.logDiff(startDate, endDate);

            return derivatAnforderungModulen.stream().map(da -> da.getAnforderung().getId()).collect(Collectors.toSet());
        }
        return new HashSet<>();
    }

    private static List<Long> getDerivatAnforderungModulIdsFromUrl(UrlParameter urlParameter) {
        Optional<String> paramter = urlParameter.getValue("daid");
        if (paramter.isPresent()) {
            List<String> input = SearchUtil.splitQuery(paramter.get(), " ");
            return input.stream()
                    .filter(SearchUtil::patternMatchNumber)
                    .map(Long::parseLong).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    private ZuordnungViewFilter getViewFilter(UrlParameter urlParameter, Mitarbeiter currentUser, List<DerivatDto> selectedDerivate) {

        LOG.info("Start getViewFilter");
        Date startDate = new Date();

        List<SensorCoc> allSensorCocs = berechtigungService.getSensorCocForMitarbeiter(currentUser, true);
        LOG.debug("{} sensorCocs f\u00fcr {} gefunden", allSensorCocs.size(), currentUser);

        List<FestgestelltIn> allFestgestelltIn = anforderungService.getAllFestgestelltIn();

        //Hier viele Queries
        List<ProzessbaukastenFilterDto> prozessbaukaesten = prozessbaukastenReadService.getAllFilterDto();

        final List<ThemenklammerDto> themenklammern = themenklammerService.getAll();

        LOG.info("End getViewFilter");
        Date endDate = new Date();
        LogUtils.logDiff(startDate, endDate);

        return new ZuordnungViewFilter(urlParameter, allSensorCocs, selectedDerivate, allFestgestelltIn, prozessbaukaesten, themenklammern);
    }

    private List<DerivatDto> getDerivate(Set<Long> derivatIdSet) {

        LOG.info("Start getDerivate");
        Date startDate = new Date();

        List<Derivat> derivate;

        if (session.hasRole(Rolle.ADMIN)) { // show all derivate for admin
            derivate = derivatService.getAllDerivate();
        } else if (session.hasRole(Rolle.ANFORDERER)) { // only show subset for anforderer
            derivate = berechtigungService.getDerivateForCurrentUser();
        } else {
            derivate = new ArrayList<>();
        }

        if (derivatIdSet != null && !derivatIdSet.isEmpty()) {
            derivate = derivate.stream().filter(derivat -> derivatIdSet.contains(derivat.getId())).collect(Collectors.toList());
        }

        LOG.info("End getDerivate");
        Date endDate = new Date();
        LogUtils.logDiff(startDate, endDate);

        derivate.sort(new DerivatComparator());

        return derivate.stream().map(derivat -> (DerivatDto) derivat).collect(Collectors.toList());

    }

    public AutomaticZuordnenDialogViewData automatischZuordnen(ZuordnungViewData viewData) {
        return zuordnungAutomaticProcessService.assignAnforderungenAutomatic(viewData);
    }
}
