package de.interfaceag.bmw.pzbk.zuordnung.dto;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.shared.utils.AnforderungUtils;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungZakStatus;

/**
 *
 * @author sl
 */
public final class DerivatZuordnungBuilder {

    private ZuordnungStatus status;
    private DerivatDto derivatDto;
    private ZuordnungZakStatus zakStatus;
    private Boolean zakUebertragungSuccessful;
    private Boolean nachZakUebertragen;
    private Boolean standardvereinbarung;
    private DerivatStatus zeitpunktZuordnung;
    private String zakLabelTooltip = "";

    public static DerivatZuordnungBuilder forZuordnung(ZuordnungAnforderungDerivat anforderungDerivat) {
        return new DerivatZuordnungBuilder().withZuordnung(anforderungDerivat);
    }

    public static DerivatZuordnung forNotExistingZuordnung(DerivatDto derivat) {
        return new DerivatZuordnungBuilder()
                .withStatus(ZuordnungStatus.OFFEN)
                .withZakStatus(ZuordnungZakStatus.OFFEN)
                .withDerivat(derivat)
                .build();
    }

    private DerivatZuordnungBuilder() {
    }

    private DerivatZuordnungBuilder withZuordnung(ZuordnungAnforderungDerivat anforderungDerivat) {
        this.status = anforderungDerivat.getStatus();
        this.zakUebertragungSuccessful = anforderungDerivat.isZakUebertragungErfolgreich();
        this.nachZakUebertragen = anforderungDerivat.isNachZakUebertragen();
        this.standardvereinbarung = AnforderungUtils.isStandardvereinbarung(anforderungDerivat.getAnforderung());
        this.zeitpunktZuordnung = anforderungDerivat.getZuordnungDerivatStatus();
        return this;
    }

    private DerivatZuordnungBuilder withStatus(ZuordnungStatus status) {
        this.status = status;
        return this;
    }

    public DerivatZuordnungBuilder withDerivat(DerivatDto derivat) {
        this.derivatDto = derivat;
        return this;
    }

    public DerivatZuordnungBuilder withZakStatus(ZuordnungZakStatus zakStatus) {
        this.zakStatus = zakStatus;
        return this;
    }

    public DerivatZuordnungBuilder withZakLabelTooltip(String zakLabelTooltip) {
        this.zakLabelTooltip = zakLabelTooltip;
        return this;
    }

    public DerivatZuordnung build() {
        return new DerivatZuordnungDto(status, derivatDto, zakStatus, zakUebertragungSuccessful, nachZakUebertragen, standardvereinbarung, zeitpunktZuordnung, zakLabelTooltip);
    }

}
