package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class DerivatZuordnungStatusFilterUtils {

    private DerivatZuordnungStatusFilterUtils() {
    }

    protected static List<DerivatZuordnungStatusFilter> getFilterForDerivatListAndUrlParamter(
            List<DerivatDto> derivate, UrlParameter urlParameter) {

        if (derivate != null) {
            return derivate.stream()
                    .map(derivat -> getFilterForDerivatAndUrlParamter(derivat, urlParameter))
                    .collect(Collectors.toList());
        }

        return new ArrayList<>();
    }

    private static DerivatZuordnungStatusFilter getFilterForDerivatAndUrlParamter(
            DerivatDto derivat, UrlParameter urlParameter) {
        return new DerivatZuordnungStatusFilter(derivat.getId(), derivat.getName(), urlParameter);
    }
}
