package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.filter.BooleanSearchFilter;
import de.interfaceag.bmw.pzbk.filter.BooleanUrlFilter;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.filter.DerivatFilter;
import de.interfaceag.bmw.pzbk.filter.FachIdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.FestgestelltInFilter;
import de.interfaceag.bmw.pzbk.filter.Filter;
import de.interfaceag.bmw.pzbk.filter.FilterToUrlConverter;
import de.interfaceag.bmw.pzbk.filter.IdListUrlFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.InProzessbaukastenFilter;
import de.interfaceag.bmw.pzbk.filter.NotInProzessbaukastenFilter;
import de.interfaceag.bmw.pzbk.filter.PhasenrelevanzFilter;
import de.interfaceag.bmw.pzbk.filter.ProzessbaukastenFilter;
import de.interfaceag.bmw.pzbk.filter.SelectedDerivatUrlFilter;
import de.interfaceag.bmw.pzbk.filter.SensorCocFilter;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ThemenklammerFilter;
import de.interfaceag.bmw.pzbk.filter.VonDateFilter;
import de.interfaceag.bmw.pzbk.filter.ZakModeUrlFilter;
import de.interfaceag.bmw.pzbk.filter.ZuordnenModeUrlFilter;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ZuordnungViewFilter implements Serializable {

    private static final Page PAGE = Page.ZUORDNUNG;

    @Filter
    private final FachIdSearchFilter anforderungFilter;
    @Filter
    private final TextSearchFilter beschreibungFilter;

    @Filter
    private final IdSearchFilter derivatFilter;

    @Filter
    private IdSearchFilter sensorCocFilter;
    @Filter
    private IdSearchFilter festgestelltInFilter;
    @Filter
    private final ThemenklammerFilter themenklammerFilter;
    @Filter
    private final BooleanSearchFilter phasenrelevanzFilter;

    @Filter
    private final DateSearchFilter freigegebenSeitFilter;

    private final List<DerivatZuordnungStatusFilter> derivatVereinbarungStatusFilter;

    @Filter
    private final BooleanUrlFilter zakModeFilter;
    @Filter
    private final BooleanUrlFilter zuordnenModeFilter;
    @Filter
    private final SelectedDerivatUrlFilter selectedDerivatFilter;
    @Filter
    private final IdListUrlFilter anforderungIdFilter;

    @Filter
    private IdSearchFilter prozessbaukastenFilter;
    @Filter
    private final BooleanUrlFilter inProzessbaukastenFilter;
    @Filter
    private final BooleanUrlFilter notInProzessbaukastenFilter;

    ZuordnungViewFilter(
            UrlParameter urlParameter,
            List<SensorCoc> allSensorCocs,
            List<DerivatDto> selectedDerivate,
            List<FestgestelltIn> allFestgestelltIn,
            List<ProzessbaukastenFilterDto> prozessbaukaesten,
            Collection<ThemenklammerDto> themenklammern) {
        derivatFilter = DerivatFilter.forDerivatDtos(selectedDerivate, urlParameter);
        derivatFilter.setSelectedValues(new HashSet<>(derivatFilter.getAll()));

        anforderungFilter = new FachIdSearchFilter(urlParameter);

        beschreibungFilter = new TextSearchFilter("beschreibung", urlParameter);

        sensorCocFilter = new SensorCocFilter(allSensorCocs, urlParameter);

        festgestelltInFilter = new FestgestelltInFilter(allFestgestelltIn, urlParameter);

        phasenrelevanzFilter = new PhasenrelevanzFilter(urlParameter);

        this.freigegebenSeitFilter = new VonDateFilter("freigabe")
                .parseParameter(urlParameter.getValue("freigabe" + VonDateFilter.PARAMETERNAME));

        derivatVereinbarungStatusFilter = DerivatZuordnungStatusFilterUtils
                .getFilterForDerivatListAndUrlParamter(selectedDerivate, urlParameter);

        zakModeFilter = new ZakModeUrlFilter(urlParameter);

        zuordnenModeFilter = new ZuordnenModeUrlFilter(urlParameter);

        selectedDerivatFilter = new SelectedDerivatUrlFilter(urlParameter);

        anforderungIdFilter = new IdListUrlFilter(urlParameter);

        prozessbaukastenFilter = new ProzessbaukastenFilter(prozessbaukaesten, urlParameter);

        inProzessbaukastenFilter = new InProzessbaukastenFilter(urlParameter);

        notInProzessbaukastenFilter = new NotInProzessbaukastenFilter(urlParameter);

        themenklammerFilter = new ThemenklammerFilter(themenklammern, urlParameter);
    }

    public String getUrl() {
        return PAGE.getUrl() + getUrlParameter();
    }

    public String getResetUrl() {
        return PAGE.getUrl() + "?faces-redirect=true";
    }

    public FachIdSearchFilter getAnforderungFilter() {
        return anforderungFilter;
    }

    public TextSearchFilter getBeschreibungFilter() {
        return beschreibungFilter;
    }

    public IdSearchFilter getSensorCocFilter() {
        return sensorCocFilter;
    }

    public IdSearchFilter getFestgestelltInFilter() {
        return festgestelltInFilter;
    }

    public BooleanSearchFilter getPhasenrelevanzFilter() {
        return phasenrelevanzFilter;
    }

    public IdSearchFilter getDerivatFilter() {
        return derivatFilter;
    }

    public DateSearchFilter getFreigegebenSeitFilter() {
        return freigegebenSeitFilter;
    }

    public List<DerivatZuordnungStatusFilter> getDerivatVereinbarungStatusFilter() {
        return derivatVereinbarungStatusFilter;
    }

    public BooleanUrlFilter getZakModeFilter() {
        return zakModeFilter;
    }

    public BooleanUrlFilter getZuordnenModeFilter() {
        return zuordnenModeFilter;
    }

    public SelectedDerivatUrlFilter getSelectedDerivatFilter() {
        return selectedDerivatFilter;
    }

    public IdListUrlFilter getAnforderungIdFilter() {
        return anforderungIdFilter;
    }

    public IdSearchFilter getProzessbaukastenFilter() {
        return prozessbaukastenFilter;
    }

    public BooleanUrlFilter getInProzessbaukastenFilter() {
        return inProzessbaukastenFilter;
    }

    public BooleanUrlFilter getNotInProzessbaukastenFilter() {
        return notInProzessbaukastenFilter;
    }

    public ThemenklammerFilter getThemenklammerFilter() {
        return themenklammerFilter;
    }

    private String getUrlParameter() {

        FilterToUrlConverter urlConverter = FilterToUrlConverter.forClass(this);
        final StringBuilder stringBuilder = new StringBuilder(urlConverter.getUrl());

        derivatVereinbarungStatusFilter.forEach(filter -> stringBuilder.append(filter.getParameter()));

        return stringBuilder.toString();
    }

    public void restrictSelectableValuesBasedOnSearchResult(Collection<SensorCoc> sensorCocs, Collection<FestgestelltIn> festgestelltIns, List<ProzessbaukastenFilterDto> prozessbaukaesten, UrlParameter urlParameter) {
        sensorCocFilter = new SensorCocFilter(sensorCocs, urlParameter);
        festgestelltInFilter = new FestgestelltInFilter(festgestelltIns, urlParameter);
        prozessbaukastenFilter = new ProzessbaukastenFilter(prozessbaukaesten, urlParameter);
    }
}
