package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ZuordnungViewPermission implements Serializable {

    // page
    private final ViewPermission page;

    private final EditPermission edit;

    // header
    private EditPermission editButton;
    private EditPermission automaticAssignemntButton;
    private EditPermission zakButton;
    private EditPermission processChangesButton;
    private EditPermission cancelButton;

    private final boolean editMode;
    private final Set<Rolle> userRoles;

    public ZuordnungViewPermission(Set<Rolle> userRoles, boolean editMode, DerivatDto selectedDerivat) {
        this.editMode = editMode;
        this.userRoles = userRoles;

        this.page = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(userRoles).get();

        this.edit = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN)
                .authorizeWriteForRole(Rolle.ANFORDERER)
                .compareTo(userRoles).get();

        if (editMode) {
            initEditButtonsForEditMode(selectedDerivat);
        } else {
            initEditButtonsForViewMode(selectedDerivat);
        }
    }

    private void initEditButtonsForViewMode(DerivatDto selectedDerivat) {
        this.processChangesButton = new FalsePermission();
        this.cancelButton = new FalsePermission();

        if (selectedDerivat != null && selectedDerivat.isVereinbarungActive()) {
            this.editButton = RolePermission.builder()
                    .authorizeWriteForRole(Rolle.ADMIN)
                    .authorizeWriteForRole(Rolle.ANFORDERER)
                    .compareTo(userRoles).get();
            this.zakButton = RolePermission.builder()
                    .authorizeWriteForRole(Rolle.ADMIN)
                    .authorizeWriteForRole(Rolle.ANFORDERER)
                    .compareTo(userRoles).get();
            this.automaticAssignemntButton = RolePermission.builder()
                    .authorizeWriteForRole(Rolle.ADMIN)
                    .authorizeWriteForRole(Rolle.ANFORDERER)
                    .compareTo(userRoles).get();
        } else {
            this.editButton = new FalsePermission();
            this.zakButton = new FalsePermission();
            this.automaticAssignemntButton = new FalsePermission();
        }
    }

    private void initEditButtonsForEditMode(DerivatDto selectedDerivat) {
        this.editButton = new FalsePermission();
        this.zakButton = new FalsePermission();
        this.automaticAssignemntButton = new FalsePermission();

        if (selectedDerivat != null && selectedDerivat.isVereinbarungActive()) {
            this.processChangesButton = RolePermission.builder()
                    .authorizeWriteForRole(Rolle.ADMIN)
                    .authorizeWriteForRole(Rolle.ANFORDERER)
                    .compareTo(userRoles).get();

            this.cancelButton = RolePermission.builder()
                    .authorizeWriteForRole(Rolle.ADMIN)
                    .authorizeWriteForRole(Rolle.ANFORDERER)
                    .compareTo(userRoles).get();
        } else {
            this.processChangesButton = new FalsePermission();
            this.cancelButton = new FalsePermission();
        }
    }

    public void updateSelectedDerivat(DerivatDto selectedDerivat) {
        if (editMode) {
            initEditButtonsForEditMode(selectedDerivat);
        } else {
            initEditButtonsForViewMode(selectedDerivat);
        }
    }

    @Deprecated
    public boolean canEdit() {
        return edit.hasRightToEdit();
    }

    public boolean isPage() {
        return page.isRendered();
    }

    public boolean isEditButton() {
        return editButton.hasRightToEdit();
    }

    public boolean isAutomaticAssignment() {
        return editButton.hasRightToEdit();
    }

    public boolean isProcessChangesButton() {
        return processChangesButton.hasRightToEdit();
    }

    public boolean isCancelButton() {
        return cancelButton.hasRightToEdit();
    }

    public boolean isZakButton() {
        return zakButton.hasRightToEdit();
    }

}
