package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.dao.ZuordnungAnforderungDerivatDao;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.dialog.viewdata.DerivatZuordnenDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.DerivatenAnzeigenDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.utils.InputValidationUtils;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungService;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class ZuordnungAnforderungDerivatService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ZuordnungAnforderungDerivatService.class.getName());

    @Inject
    Session session;

    @Inject
    public ZuordnungAnforderungDerivatDao zuordnungAnforderungDerivatDao;

    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;
    @Inject
    private AnforderungMeldungHistoryService anforderungMeldungHistoryService;

    public ZuordnungAnforderungDerivat updateZuordnungAnforderungDerivatStatus(ZuordnungStatus status, Anforderung anforderung, Derivat derivat, Mitarbeiter user) {
        ZuordnungAnforderungDerivat anforderungDerivat = getZuordnungAnforderungDerivatForAnforderungDerivat(anforderung, derivat);
        if (anforderungDerivat != null) {
            return updateZuordnungAnforderungDerivatStatus(status, anforderungDerivat, user);
        } else {
            String kommentar = "(" + LocalizationService.getGermanValue(derivat.getStatus().getLocalizationName()) + ") " + "Die Anforderung " + anforderung.toString() + " wurde dem Derivat " + derivat.getName() + " zugeordnet.";
            return assignAnforderungToDerivat(status, anforderung, derivat, kommentar, user);
        }
    }

    public ZuordnungAnforderungDerivat assignAnforderungToDerivat(ZuordnungStatus status, Anforderung anforderung, Derivat derivat, String kommentar, Mitarbeiter user) {
        if (status == null) {
            LOG.warn("ZuordnungStatus for Anforderung {} zum Derivat {} is null", anforderung, derivat);
            return null;
        }

        if (InputValidationUtils.anyObjectIsNull(anforderung, derivat)) {
            LOG.error("Anforderung or derivat is null");
            return null;
        }

        if (isNull(anforderung.getStatus()) || !(Status.A_FREIGEGEBEN.equals(anforderung.getStatus()) || Status.A_GENEHMIGT_IN_GUELTIGEM_PROZESSBAUKASTEN.equals(anforderung.getStatus()))) {
            LOG.error("Anforderung is not freigegeben");
            return null;
        }

        if (!berechtigungService.isAnfordererForDerivat(derivat, user)) {
            LOG.error("assignAnforderungToDerivat: insufficient authorization");
            return null;
        }

        if (getZuordnungAnforderungDerivatForAnforderungDerivat(anforderung, derivat) != null) {
            LOG.warn("assignAnforderungToDerivat: zuordnung already exists in database");
            return null;
        }

        if (!derivat.isVereinbarungActive()) {
            LOG.warn("Derivat has invalid statis {}. No ZuordnungAnforderungDerivat is created!", status);
            return null;
        }

        switch (status) {
            case ZUGEORDNET:
                return processAssinmentForZugeordnet(status, anforderung, derivat, kommentar, user);
            case KEINE_ZUORDNUNG:
                return processAssignmentForKeineZuordnung(status, anforderung, derivat, user);
            default:
                LOG.warn("assignAnforderungToDerivat: invalid status for derivat anforderung zuordnung");
                return null;
        }

    }

    private ZuordnungAnforderungDerivat processAssignmentForKeineZuordnung(ZuordnungStatus status, Anforderung anforderung, Derivat derivat, Mitarbeiter user) {
        ZuordnungAnforderungDerivat anforderungDerivat = new ZuordnungAnforderungDerivat(anforderung, derivat, status);
        persistZuordnungAnforderungDerivat(anforderungDerivat);
        createAnforderungHistoryForZuordnung(anforderungDerivat, user);
        return anforderungDerivat;
    }

    private ZuordnungAnforderungDerivat processAssinmentForZugeordnet(ZuordnungStatus status, Anforderung anforderung, Derivat derivat, String kommentar, Mitarbeiter user) {
        ZuordnungAnforderungDerivat anforderungDerivat = new ZuordnungAnforderungDerivat(anforderung, derivat, status);
        anforderungDerivat.setZuordnungDerivatStatus(derivat.getStatus());
        persistZuordnungAnforderungDerivat(anforderungDerivat);
        createAnforderungHistoryForZuordnung(anforderungDerivat, user);
        derivatAnforderungModulService.createDerivatAnforderungModulForAnforderungDerivatZuordnung(anforderungDerivat, DerivatAnforderungModulStatus.ABZUSTIMMEN, kommentar, user);
        return anforderungDerivat;
    }

    private void createAnforderungHistoryForZuordnung(ZuordnungAnforderungDerivat anforderungDerivat, Mitarbeiter user) {
        StringBuilder messageBuilder = new StringBuilder();

        switch (anforderungDerivat.getStatus()) {
            case ZUGEORDNET:
                messageBuilder.append("(").append(LocalizationService.getGermanValue(anforderungDerivat.getDerivat().getStatus().getLocalizationName())).append(") ");
                messageBuilder.append(anforderungDerivat.getDerivat().toString());
                messageBuilder.append(" wurde der Anforderung zugeordnet.");
                break;
            case KEINE_ZUORDNUNG:
                messageBuilder.append("(").append(LocalizationService.getGermanValue(anforderungDerivat.getDerivat().getStatus().getLocalizationName())).append(") ");
                messageBuilder.append("Die Anforderung wurde für ");
                messageBuilder.append(anforderungDerivat.getDerivat().toString());
                messageBuilder.append(" auf keine Zuordnung gesetzt.");
                break;
            case OFFEN:
                messageBuilder.append("(").append(LocalizationService.getGermanValue(anforderungDerivat.getDerivat().getStatus().getLocalizationName())).append(") ");
                messageBuilder.append("Die Zuordnung zu ");
                messageBuilder.append(anforderungDerivat.getDerivat().toString());
                messageBuilder.append(" wurde aufgehoben.");
                break;
            default:
                break;
        }

        AnforderungHistory anforderungHistory = new AnforderungHistory(anforderungDerivat.getAnforderung().getId(),
                "A", new Date(), "Zuordnung", user.toString(), "", messageBuilder.toString());
        anforderungMeldungHistoryService.persistAnforderungHistory(anforderungHistory);
    }

    public ZuordnungAnforderungDerivat updateZuordnungAnforderungDerivatStatus(ZuordnungStatus status, ZuordnungAnforderungDerivat anforderungDerivat, Mitarbeiter currentUser) {
        if (anforderungDerivat != null && status != null) {
            if (status.equals(ZuordnungStatus.OFFEN)) {
                LOG.debug("remove {}", anforderungDerivat);
                umsetzungsbestaetigungService.removeUmsetzungsbestaetigungenForAnforderungDerivat(anforderungDerivat);
                derivatAnforderungModulService.removeDerivatAnforderungModulForZuordnungAnforderungDerivat(anforderungDerivat);
                zuordnungAnforderungDerivatDao.removeZuordnungAnforderungDerivat(anforderungDerivat);
                anforderungDerivat.setStatus(ZuordnungStatus.OFFEN);
                createAnforderungHistoryForZuordnung(anforderungDerivat, currentUser);
                return null;
            } else {
                if (anforderungDerivat.getStatus().equals(ZuordnungStatus.KEINE_ZUORDNUNG)
                        && status.equals(ZuordnungStatus.ZUGEORDNET)) {
                    derivatAnforderungModulService.createDerivatAnforderungModulForAnforderungDerivatZuordnung(anforderungDerivat, DerivatAnforderungModulStatus.ABZUSTIMMEN, "", currentUser);
                    return performeStatusChange(status, anforderungDerivat, currentUser);
                } else if (anforderungDerivat.getStatus().equals(ZuordnungStatus.ZUGEORDNET)
                        && ZuordnungStatus.KEINE_ZUORDNUNG.equals(status)) {
                    umsetzungsbestaetigungService.removeUmsetzungsbestaetigungenForAnforderungDerivat(anforderungDerivat);
                    derivatAnforderungModulService.removeDerivatAnforderungModulForZuordnungAnforderungDerivat(anforderungDerivat);
                    return performeStatusChange(status, anforderungDerivat, currentUser);
                } else if (!status.equals(anforderungDerivat.getStatus())) {
                    return performeStatusChange(status, anforderungDerivat, currentUser);
                } else {
                    LOG.error("invalid status for anforderungDerivat {}", anforderungDerivat);
                    return null;
                }
            }
        } else {
            LOG.warn("updateZuordnungAnforderungDerivatStatus: invalid input data");
            return null;
        }
    }

    private ZuordnungAnforderungDerivat performeStatusChange(ZuordnungStatus status, ZuordnungAnforderungDerivat anforderungDerivat, Mitarbeiter currentUser) {
        anforderungDerivat.setStatus(status);
        updateZuordnungDerivatStatusIfStatusIsChangedToZuogeordnet(anforderungDerivat, anforderungDerivat.getDerivat().getStatus());
        createAnforderungHistoryForZuordnung(anforderungDerivat, currentUser);
        persistZuordnungAnforderungDerivat(anforderungDerivat);
        return anforderungDerivat;
    }

    private static void updateZuordnungDerivatStatusIfStatusIsChangedToZuogeordnet(ZuordnungAnforderungDerivat anforderungDerivat, DerivatStatus derivatStatus) {
        if (anforderungDerivat.getStatus().equals(ZuordnungStatus.ZUGEORDNET)) {
            anforderungDerivat.setZuordnungDerivatStatus(derivatStatus);
        }
    }

    public void setDerivatAnforderungAsNachZakUebertragen(ZuordnungAnforderungDerivat anforderungDerivat, boolean isZakUebertragungFehlerhaft) {
        anforderungDerivat.setNachZakUebertragen(Boolean.TRUE);

        if (isZakUebertragungFehlerhaft) {
            anforderungDerivat.setZakUebertragungErfolgreich(Boolean.FALSE);
        } else {
            anforderungDerivat.setZakUebertragungErfolgreich(Boolean.TRUE);
        }

        persistZuordnungAnforderungDerivat(anforderungDerivat);
    }

    public List<ZuordnungAnforderungDerivat> getAllZuordnungAnforderungDerivatForAnforderungListDerivatListFetch(Collection<Long> anforderungen, Collection<Long> derivate) {
        return zuordnungAnforderungDerivatDao.getAllZuordnungAnforderungDerivatForAnforderungListDerivatListFetch(anforderungen, derivate);
    }

    public ZuordnungAnforderungDerivat getZuordnungAnforderungDerivatForAnforderungDerivat(Anforderung anforderung, Derivat derivat) {
        return zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatForAnforderungDerivat(anforderung, derivat);
    }

    public List<ZuordnungAnforderungDerivat> getZuordnungAnforderungDerivatForAnforderung(Anforderung anforderung) {
        return zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatForAnforderung(anforderung);
    }

    public List<ZuordnungAnforderungDerivat> getDerivatZuordnungenWithBerechtigungForAnforderung(Anforderung anforderung) {
        if (session.hasRole(Rolle.ADMIN)) {
            return zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatForAnforderung(anforderung);
        }

        List<Long> derivatIds = session.getUserPermissions().getDerivatAsAnfordererSchreibend().stream()
                .map(BerechtigungDto::getId).collect(Collectors.toList());

        return zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatForAnforderungAndDerivatIds(anforderung, derivatIds);
    }

    public List<DerivatenAnzeigenDto> getDerivatAnzeigenDialogViewDataForAnforderungId(Long anforderungId) {
        return zuordnungAnforderungDerivatDao.getDerivatAnzeigenDialogViewDataForAnforderungId(anforderungId);
    }

    public List<Derivat> getDerivateForAnforderung(Anforderung anforderung) {
        return zuordnungAnforderungDerivatDao.getDerivateForAnforderung(anforderung);
    }

    public void persistZuordnungAnforderungDerivat(List<ZuordnungAnforderungDerivat> anforderungDerivat) {
        zuordnungAnforderungDerivatDao.persistZuordnungAnforderungDerivat(anforderungDerivat);
    }

    public void persistZuordnungAnforderungDerivat(ZuordnungAnforderungDerivat anforderungDerivat) {
        zuordnungAnforderungDerivatDao.persistZuordnungAnforderungDerivat(anforderungDerivat);
    }

    public ZuordnungAnforderungDerivat getZuordnungAnforderungDerivatById(Long id) {
        return zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatById(id);
    }

    public List<ZuordnungAnforderungDerivat> getZuordnungAnforderungDerivatForDerivat(Derivat derivat) {
        return zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatForDerivat(derivat);
    }

    public List<DerivatZuordnenDto> getDerivateViewDataForAnforderung(Long anforderungId) {
        return zuordnungAnforderungDerivatDao.getDerivateZuordnenViewDataForAnforderung(anforderungId);
    }

    public List<DerivatZuordnenDto> getDerivateVerfuegbarViewDataForAnforderung(Long anforderungId, List<Long> derivatIds) {
        return zuordnungAnforderungDerivatDao.getDerivateZuordnenViewDataForAnforderung(anforderungId, derivatIds);
    }

    public List<ZuordnungAnforderungDerivat> getZuordnungAnforderungDerivatByIdList(List<Long> idList) {
        return zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatByIdList(idList);
    }

}
