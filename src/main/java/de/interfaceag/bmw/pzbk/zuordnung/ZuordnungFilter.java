package de.interfaceag.bmw.pzbk.zuordnung;

/**
 *
 * @author sl
 */
public interface ZuordnungFilter {

    String reset();

    String filter();

    ZuordnungViewFilter getFilter();

}
