package de.interfaceag.bmw.pzbk.zuordnung.dto;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungZakStatus;

import java.util.Collections;
import java.util.List;

/**
 *
 * @author sl
 */
public class DerivatZuordnungDto implements DerivatZuordnung {

    private ZuordnungStatus status;
    private final DerivatDto derivatDto;
    private final ZuordnungZakStatus zakStatus;
    private final Boolean zakUebertragungSuccessful;
    private final Boolean nachZakUebertragen;
    private final Boolean standardvereinbarung;
    private boolean changed;
    private final DerivatStatus zeitpunktZuordnung;
    private final String zakLabelTooltip;

    DerivatZuordnungDto(ZuordnungStatus status, DerivatDto derivatDto, ZuordnungZakStatus zakStatus,
                        Boolean zakUebertragungSuccessful, Boolean nachZakUebertragen, Boolean standardvereinbarung,
                        DerivatStatus zeitpunktZuordnung, String zakLabelTooltip) {
        this.status = status;
        this.derivatDto = derivatDto;
        this.zakStatus = zakStatus;
        this.zakUebertragungSuccessful = zakUebertragungSuccessful;
        this.nachZakUebertragen = nachZakUebertragen;
        this.standardvereinbarung = standardvereinbarung;
        this.zeitpunktZuordnung = zeitpunktZuordnung;
        this.zakLabelTooltip = zakLabelTooltip;
    }

    @Override
    public ZuordnungStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(ZuordnungStatus status) {
        this.status = status;
        this.changed = Boolean.TRUE;
    }

    @Override
    public DerivatDto getDerivat() {
        return derivatDto;
    }

    @Override
    public boolean isStatusChangeEnabled(DerivatDto selectedDerivat, boolean zuordnenMode) {
        return derivatDto.getId().equals(selectedDerivat.getId()) && zuordnenMode && derivatVereinbarungIsActive(derivatDto);
    }

    private static boolean derivatVereinbarungIsActive(DerivatDto derivat) {
        switch (derivat.getStatus()) {
            case VEREINARBUNG_VKBG:
            case VEREINBARUNG_ZV:
                return Boolean.TRUE;
            default:
                return Boolean.FALSE;
        }
    }

    @Override
    public List<ZuordnungStatus> getNextStatus() {
        if (status == null) {
            return Collections.emptyList();
        } else {
            return status.getNextStatus();
        }
    }

    @Override
    public boolean isZakStatusRendered() {
        return nachZakUebertragen != null && nachZakUebertragen || standardvereinbarung != null && standardvereinbarung;
    }

    @Override
    public boolean isZakUebertragungSuccessful() {
        return zakUebertragungSuccessful != null && zakUebertragungSuccessful || standardvereinbarung != null && standardvereinbarung;
    }

    @Override
    public boolean isNachZakUebertragen() {
        return nachZakUebertragen != null && nachZakUebertragen;
    }

    @Override
    public ZuordnungZakStatus getZakStatus() {
        return zakStatus;
    }

    @Override
    public String getZakStatusTooltip() {
        return zakLabelTooltip;
    }

    @Override
    public boolean isChanged() {
        return changed;
    }

    @Override
    public String getZeitpunktZuordnung() {
        if (zeitpunktZuordnung != null) {
            switch (zeitpunktZuordnung) {
                case VEREINARBUNG_VKBG:
                    return "VKBG";
                case VEREINBARUNG_ZV:
                    return "ZV";
                default:
                    return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public boolean isZeitpunktZuordnungRendered() {
        return !getZeitpunktZuordnung().isEmpty();
    }

}
