package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.derivat.DerivatDtoConverter;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.shared.utils.LogUtils;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import de.interfaceag.bmw.pzbk.zuordnung.automatic.AutomaticZuordnenDialogViewData;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author sl
 */
@ViewScoped
@Named
public class ZuordnungController implements Serializable, ZuordnungNavbar, ZuordnungView, ZuordnungFilter {

    private static final Logger LOG = LoggerFactory.getLogger(ZuordnungController.class);

    @Inject
    private ZuordnungViewFacade zuordnungViewFacade;

    private ZuordnungViewData zuordnungViewData;
    private AutomaticZuordnenDialogViewData automaticZuordnenDialogViewData;

    @PostConstruct
    public void init() {
        LOG.info("Start init");
        Date startDate = new Date();

        initViewData();

        LOG.info("End init");
        Date endDate = new Date();
        LogUtils.logDiff(startDate, endDate);
    }

    private void initViewData() {
        zuordnungViewData = zuordnungViewFacade.getViewData(UrlParameterUtils.getUrlParameter());
    }

    @Override
    public String updateDerivatList() {
        updateUserDerivate();
        return filter();
    }

    private void updateUserDerivate() {
        zuordnungViewFacade.updateUserDerivate(getFilteredDerivate());
    }

    @Override
    public void enableDerivat(DerivatDto derivat) {
        if (derivat != null) {
            setSelectedDerivat(derivat);
        }
    }

    private void setSingleDerivatSelected() {
        if (getFilteredDerivate() != null && getFilteredDerivate().size() == 1) {
            setSelectedDerivat(getFilteredDerivate().get(0));
        }
    }

    @Override
    public List<ZuordnungStatus> getAllNextStatusList() {
        return ZuordnungStatus.getAll();
    }

    @Override
    public String toggleZuordnenMode() {
        setSingleDerivatSelected();
        if (getSelectedDerivat() != null) {
            getFilter().getZuordnenModeFilter().setActive();
            return filter();
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("zuordnen",
                    "Bitte wählen Sie ein Derivat aus."));
        }
        return "";
    }

    @Override
    public String toggleZakMode() {
        setSingleDerivatSelected();
        if (getSelectedDerivat() != null) {
            getFilter().getZakModeFilter().setActive();
            return filter();
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("ZAK",
                    "Bitte wählen Sie ein Derivat aus."));
        }
        return "";
    }

    @Override
    public String process() {
        return zuordnungViewFacade.process(getViewData());
    }

    @Override
    public String cancel() {
        getFilter().getZuordnenModeFilter().setInactive();
        getFilter().getZakModeFilter().setInactive();
        return filter();
    }

    @Override
    public Converter getDerivatConverter() {
        return new DerivatDtoConverter(getDerivate());
    }

    @Override
    public List<DerivatDto> getDerivate() {
        return getViewData().getDerivate();
    }

    @Override
    public boolean isZuordnenMode() {
        return getViewData().isZuordnenMode();
    }

    @Override
    public boolean isZakMode() {
        return getViewData().isZakMode();
    }

    @Override
    public List<DerivatDto> getFilteredDerivate() {
        return getViewData().getFilteredDerivate();
    }

    @Override
    public void setFilteredDerivate(List<DerivatDto> filteredDerivate) {
        this.getViewData().setFilteredDerivate(filteredDerivate);
    }

    @Override
    public List<Zuordnung> getTableData() {
        return getViewData().getTableData();
    }

    @Override
    public List<Zuordnung> getSelectedTableData() {
        return getViewData().getSelectedTableData();
    }

    @Override
    public void setSelectedTableData(List<Zuordnung> selectedTableData) {
        this.getViewData().setSelectedTableData(selectedTableData);
    }

    @Override
    public DerivatDto getSelectedDerivat() {
        return getViewData().getSelectedDerivat();
    }

    public void setSelectedDerivat(DerivatDto selectedDerivat) {
        this.getViewData().setSelectedDerivat(selectedDerivat);
        this.getViewPermission().updateSelectedDerivat(selectedDerivat);
    }

    @Override
    public ZuordnungStatus getGroupStatus() {
        return getViewData().getGroupStatus();
    }

    @Override
    public void setGroupStatus(ZuordnungStatus groupStatus) {
        this.getViewData().setGroupStatus(groupStatus);
    }

    @Override
    public boolean isAllSelected() {
        return getViewData().isAllSelected();
    }

    @Override
    public void setAllSelected(boolean allSelected) {
        this.getViewData().setAllSelected(allSelected);
    }

    @Override
    public ZuordnungViewData getViewData() {
        return zuordnungViewData;
    }

    @Override
    public ZuordnungViewFilter getFilter() {
        return zuordnungViewData.getFilter();
    }

    @Override
    public String reset() {
        return getFilter().getResetUrl();
    }

    @Override
    public String filter() {
        return getFilter().getUrl();
    }

    @Override
    public ZuordnungViewPermission getViewPermission() {
        return getViewData().getViewPermission();
    }

    @Override
    public void automatischZuordnen() {
        if (!isOnlyOneDerivatFiltered()) {
            LOG.error("No or more than one derivat was selected");
            return;
        }

        LOG.debug("starting automatische Zuordnung");
        automaticZuordnenDialogViewData = zuordnungViewFacade.automatischZuordnen(zuordnungViewData);

        LOG.debug("show Fahrzeugmerkmal automatische Zuordnung result Dialog with viewData {}", automaticZuordnenDialogViewData);

        final RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('automatischZuordnenDialog').show()");
        requestContext.update("zuordnungNavBar:automatischZuordnenDialogForm:automatischZuordnenDialogContent");
    }

    @Override
    public AutomaticZuordnenDialogViewData getAutomaticZuordnenDialogViewData() {
        return automaticZuordnenDialogViewData;
    }

    public DerivatDto getDerivat() {
        return getViewData().getSelectedDerivat();
    }

    @Override
    public String getNumberOfResults() {
        return Integer.toString(getTableData().size());
    }

    @Override
    public DerivatStatusNames getDerivatStatusNames() {
        return getViewData().getDerivatStatusNames();
    }

    public boolean isOnlyOneDerivatFiltered() {
        final List<DerivatDto> filteredDerivate = zuordnungViewData.getFilteredDerivate();
        return Objects.nonNull(filteredDerivate) && filteredDerivate.size() == 1;
    }

    public String reloadPage() {
        return getFilter().getUrl();
    }
}
