package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.Set;

/**
 *
 * @author sl
 */
public final class ZuordnungViewPermissionUtils {

    private ZuordnungViewPermissionUtils() {
    }

    public static ZuordnungViewPermission getViewPermission(Set<Rolle> userRoles, boolean zakMode, boolean zuordnenMode, DerivatDto selectedDerivat) {
        boolean editMode = zakMode || zuordnenMode;
        return new ZuordnungViewPermission(userRoles, editMode, selectedDerivat);
    }
}
