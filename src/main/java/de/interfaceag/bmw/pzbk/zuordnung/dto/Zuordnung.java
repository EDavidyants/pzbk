package de.interfaceag.bmw.pzbk.zuordnung.dto;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.dto.ProzessbaukastenLabelDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOptionalTuple;

import java.io.Serializable;
import java.util.Optional;

/**
 *
 * @author sl
 */
public interface Zuordnung extends Serializable, ProzessbaukastenAnforderungOptionalTuple {

    @Override
    Long getAnforderungId();

    String getAnforderungLabel();

    @Override
    String getAnforderungFachId();

    @Override
    Optional<Long> getProzessbaukastenId();

    Integer getAnforderungVersion();

    String getAnforderungBeschreibungFull();

    String getAnforderungBeschreibungShort();

    boolean isProzessbaukastenZugeordnet();

    ProzessbaukastenLabelDto getProzessbaukastenLabel();

    DerivatZuordnung getZuordnungForDerivat(DerivatDto derivat);

    boolean isStandardvereinbarung();

}
