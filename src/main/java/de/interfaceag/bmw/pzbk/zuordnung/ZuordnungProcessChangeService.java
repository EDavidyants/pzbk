package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.zuordnung.dto.DerivatZuordnung;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * @author sl
 */
@Named
@Stateless
public class ZuordnungProcessChangeService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ZuordnungProcessChangeService.class);

    @Inject
    private Session session;

    @Inject
    private AnforderungService anforderungService;
    @Inject
    private ZuordnungAnforderungDerivatService anforderungDerivatService;

    public Collection<ZuordnungAnforderungDerivat> processZuordnungChangesInBulkMode(ZuordnungStatus groupStatus, Derivat derivat, List<Zuordnung> zuordnungen) {
        Mitarbeiter user = session.getUser();
        Iterator<Zuordnung> iterator = zuordnungen.iterator();

        Collection<ZuordnungAnforderungDerivat> result = new ArrayList<>();
        while (iterator.hasNext()) {
            Zuordnung zuordnung = iterator.next();
            Long anforderungId = zuordnung.getAnforderungId();
            Anforderung anforderung = getAnforderung(anforderungId);
            final ZuordnungAnforderungDerivat zuordnungAnforderungDerivat = processZuordnungChange(groupStatus, anforderung, derivat, user);

            if (Objects.nonNull(zuordnungAnforderungDerivat)) {
                result.add(zuordnungAnforderungDerivat);
            } else {
                LOG.warn("processZuordnungChange retured null for group status {} anforderung {} derivat {} user {}", groupStatus, anforderung, derivat, user);
            }
        }

        return result;
    }

    public void processZuordnungChangesInSingleMode(Derivat derivat, List<Zuordnung> zuordnungen) {
        Mitarbeiter user = session.getUser();

        Iterator<Zuordnung> iterator = zuordnungen.iterator();
        while (iterator.hasNext()) {
            Zuordnung zuordnung = iterator.next();
            DerivatZuordnung zuordnungForDerivat = zuordnung.getZuordnungForDerivat(derivat);

            if (zuordnungForDerivat.isChanged()) {
                ZuordnungStatus status = zuordnungForDerivat.getStatus();
                Long anforderungId = zuordnung.getAnforderungId();
                Anforderung anforderung = getAnforderung(anforderungId);
                processZuordnungChange(status, anforderung, derivat, user);
            }
        }
    }

    private ZuordnungAnforderungDerivat processZuordnungChange(ZuordnungStatus status, Anforderung anforderung, Derivat derivat, Mitarbeiter user) {
        return anforderungDerivatService.updateZuordnungAnforderungDerivatStatus(status, anforderung, derivat, user);
    }

    private Anforderung getAnforderung(Long id) {
        return anforderungService.getAnforderungById(id);
    }

}
