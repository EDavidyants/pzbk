package de.interfaceag.bmw.pzbk.zuordnung.dto;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.dto.ProzessbaukastenLabelDto;

import java.util.Map;
import java.util.Optional;

/**
 * @author sl
 */
public class ZuordnungDto implements Zuordnung {

    private final Long anforderungId;
    private final String anforderungLabel;
    private final String anforderungFachId;
    private final Integer anforderungVersion;
    private final String anforderungBeschreibung;
    private final ProzessbaukastenLabelDto prozessbaukastenLabel;
    private final boolean standardvereinbarung;
    private final Map<DerivatDto, DerivatZuordnung> derivatZuordnungen;


    protected ZuordnungDto(Long anforderungId, String anforderungLabel, String anforderungFachId, Integer anforderungVersion, String anforderungBeschreibung,
                           boolean standardvereinbarung, Map<DerivatDto, DerivatZuordnung> derivatZuordnungen, ProzessbaukastenLabelDto prozessbaukastenLabel) {
        this.anforderungId = anforderungId;
        this.anforderungLabel = anforderungLabel;
        this.anforderungFachId = anforderungFachId;
        this.anforderungVersion = anforderungVersion;
        this.anforderungBeschreibung = anforderungBeschreibung;
        this.standardvereinbarung = standardvereinbarung;
        this.derivatZuordnungen = derivatZuordnungen;
        this.prozessbaukastenLabel = prozessbaukastenLabel;
    }


    @Override
    public Long getAnforderungId() {
        return anforderungId;
    }

    @Override
    public String getAnforderungLabel() {
        return anforderungLabel;
    }

    @Override
    public String getAnforderungFachId() {
        return anforderungFachId;
    }

    @Override
    public Integer getAnforderungVersion() {
        return anforderungVersion;
    }

    @Override
    public String getAnforderungBeschreibungFull() {
        return anforderungBeschreibung;
    }

    @Override
    public String getAnforderungBeschreibungShort() {
        if (anforderungBeschreibung.length() > 67) {
            return anforderungBeschreibung.substring(0, 67).concat("...");
        } else {
            return anforderungBeschreibung;
        }
    }

    @Override
    public boolean isProzessbaukastenZugeordnet() {
        return prozessbaukastenLabel != null;
    }

    @Override
    public ProzessbaukastenLabelDto getProzessbaukastenLabel() {
        return prozessbaukastenLabel;
    }

    @Override
    public DerivatZuordnung getZuordnungForDerivat(DerivatDto derivat) {
        return derivatZuordnungen.get(derivat);
    }

    @Override
    public boolean isStandardvereinbarung() {
        return standardvereinbarung;
    }

    @Override
    public Optional<Long> getProzessbaukastenId() {
        if (prozessbaukastenLabel != null) {
            return Optional.of(prozessbaukastenLabel.getId());
        }
        return Optional.empty();
    }

}
