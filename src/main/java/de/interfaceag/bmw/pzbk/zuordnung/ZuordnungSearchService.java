package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.comparator.AnforderungFachIdComparator;
import de.interfaceag.bmw.pzbk.dao.SearchDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.filter.BooleanSearchFilter;
import de.interfaceag.bmw.pzbk.filter.BooleanUrlFilter;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.filter.FachIdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.IdListUrlFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ThemenklammerFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import de.interfaceag.bmw.pzbk.shared.dto.SearchFilterDTO;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl
 */
@Stateless
public class ZuordnungSearchService implements Serializable {

    @Inject
    private SearchDao searchDao;

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager em;

    public List<Anforderung> getSeachResult(IdSearchFilter derivatFilter,
                                            IdSearchFilter festgestelltInFilter,
                                            BooleanSearchFilter phasenrelevanzFilter,
                                            IdSearchFilter sensorCocFilter,
                                            IdListUrlFilter anforderungIdFilter,
                                            DateSearchFilter freigegebenSeitFilter,
                                            FachIdSearchFilter fachIdFilter,
                                            TextSearchFilter beschreibungFilter,
                                            List<DerivatZuordnungStatusFilter> derivatVereinbarungStatusFilter,
                                            BooleanUrlFilter zakModeFilter,
                                            IdSearchFilter prozessbaukastenFilter,
                                            BooleanUrlFilter inProzessbaukastenFilter,
                                            BooleanUrlFilter notInProzessbaukastenFilter, ThemenklammerFilter themenklammerFilter) {

        List<Anforderung> result = new ArrayList<>();
        List<Anforderung> existingZuordnungAnforderungen = getExistingZuordnungSearchResult(derivatFilter,
                festgestelltInFilter, phasenrelevanzFilter, sensorCocFilter,
                anforderungIdFilter, freigegebenSeitFilter, fachIdFilter,
                beschreibungFilter, derivatVereinbarungStatusFilter, zakModeFilter,
                prozessbaukastenFilter, inProzessbaukastenFilter, notInProzessbaukastenFilter, themenklammerFilter);

        List<Long> existingZuordnungAnforderungIds = existingZuordnungAnforderungen.stream().map(Anforderung::getId).collect(Collectors.toList());

        result.addAll(existingZuordnungAnforderungen);
        result.addAll(getAnforderungSearchResult(festgestelltInFilter, phasenrelevanzFilter,
                sensorCocFilter, anforderungIdFilter, freigegebenSeitFilter, fachIdFilter,
                beschreibungFilter, derivatVereinbarungStatusFilter, existingZuordnungAnforderungIds, zakModeFilter,
                prozessbaukastenFilter, inProzessbaukastenFilter, notInProzessbaukastenFilter, themenklammerFilter));

        result.sort(new AnforderungFachIdComparator().reversed());

        return result;
    }

    private List<Anforderung> getAnforderungSearchResult(
            IdSearchFilter festgestelltInFilter,
            BooleanSearchFilter phasenrelevanzFilter,
            IdSearchFilter sensorCocFilter,
            IdListUrlFilter anforderungIdFilter,
            DateSearchFilter freigegebenSeitFilter,
            FachIdSearchFilter fachIdFilter,
            TextSearchFilter beschreibungFilter,
            List<DerivatZuordnungStatusFilter> derivatVereinbarungStatusFilter,
            List<Long> existingZuordnungAnforderungIds, BooleanUrlFilter zakModeFilter,
            IdSearchFilter prozessbaukastenFilter,
            BooleanUrlFilter inProzessbaukastenFilter, BooleanUrlFilter notInProzessbaukastenFilter, ThemenklammerFilter themenklammerFilter) {

        QueryPartDTO qp = ZuordnungSearchUtils.getBaseQuery(existingZuordnungAnforderungIds);

        if (fachIdFilter.isActive()) {
            ZuordnungSearchUtils.getAnforderungQuery(qp, fachIdFilter);
        }
        if (beschreibungFilter.isActive()) {
            ZuordnungSearchUtils.getBeschreibungQuery(qp, beschreibungFilter);
        }
        if (festgestelltInFilter.isActive()) {
            ZuordnungSearchUtils.getFestgestlltInQuery(qp, festgestelltInFilter.getSelectedIdsAsSet());
        }
        if (phasenrelevanzFilter.isActive()) {
            ZuordnungSearchUtils.getPhasenrelevanzQuery(qp, phasenrelevanzFilter);
        }
        if (sensorCocFilter.isActive()) {
            ZuordnungSearchUtils.getSensorCocQuery(qp, sensorCocFilter);
        }
        if (anforderungIdFilter.isActive()) {
            ZuordnungSearchUtils.getAnforderungIdUrlQuery(qp, anforderungIdFilter);
        }
        if (freigegebenSeitFilter.isActive()) {
            getHistoryQueryPart(qp, freigegebenSeitFilter);
        }

        if (zakModeFilter.isActive() && zakModeFilter.getAttributeValue().equals("true")) {
            ZuordnungSearchUtils.getAnforderungWithoutProzessbaukatenZuordnung(qp);
            ZuordnungSearchUtils.getAnforderungWithoutVereinbarungtypStd(qp);
        } else {
            ZuordnungSearchUtils.getAnforderungForProzessbaukatenGueltig(qp);
        }

        if (prozessbaukastenFilter.isActive()) {
            ZuordnungSearchUtils.getProzessbaukastenQuery(qp, prozessbaukastenFilter);
        }

        ZuordnungSearchUtils.getThemenklammerQuery(qp, themenklammerFilter);

        if (inProzessbaukastenFilter.isActive()) {
            ZuordnungSearchUtils.getInProzessbaukastenQuery(qp, inProzessbaukastenFilter);
        }

        if (notInProzessbaukastenFilter.isActive()) {
            ZuordnungSearchUtils.getNotInProzessbaukastenQuery(qp, notInProzessbaukastenFilter);
        }

        getAnforderungIdQueryPart(qp, derivatVereinbarungStatusFilter);

        ZuordnungSearchUtils.getSortQueryPart(qp);

        Query q = buildQuery(qp);
        return q.getResultList();

    }

    private List<Anforderung> getExistingZuordnungSearchResult(
            IdSearchFilter derivatFilter,
            IdSearchFilter festgestelltInFilter,
            BooleanSearchFilter phasenrelevanzFilter,
            IdSearchFilter sensorCocFilter,
            IdListUrlFilter anforderungIdFilter,
            DateSearchFilter freigegebenSeitFilter,
            FachIdSearchFilter fachIdFilter,
            TextSearchFilter beschreibungFilter,
            List<DerivatZuordnungStatusFilter> derivatVereinbarungStatusFilter, BooleanUrlFilter zakModeFilter,
            IdSearchFilter prozessbaukastenFilter,
            BooleanUrlFilter inProzessbaukastenFilter, BooleanUrlFilter notInProzessbaukastenFilter, ThemenklammerFilter themenklammerFilter) {

        QueryPartDTO qp = ZuordnungSearchUtils.getExistingZuordnungBaseQuery(derivatFilter.getSelectedIdsAsSet());

        if (fachIdFilter.isActive()) {
            ZuordnungSearchUtils.getAnforderungQuery(qp, fachIdFilter);
        }
        if (beschreibungFilter.isActive()) {
            ZuordnungSearchUtils.getBeschreibungQuery(qp, beschreibungFilter);
        }
        if (festgestelltInFilter.isActive()) {
            ZuordnungSearchUtils.getFestgestlltInQuery(qp, festgestelltInFilter.getSelectedIdsAsSet());
        }
        if (phasenrelevanzFilter.isActive()) {
            ZuordnungSearchUtils.getPhasenrelevanzQuery(qp, phasenrelevanzFilter);
        }
        if (sensorCocFilter.isActive()) {
            ZuordnungSearchUtils.getSensorCocQuery(qp, sensorCocFilter);
        }
        if (anforderungIdFilter.isActive()) {
            ZuordnungSearchUtils.getAnforderungIdUrlQuery(qp, anforderungIdFilter);
        }
        if (freigegebenSeitFilter.isActive()) {
            getHistoryQueryPart(qp, freigegebenSeitFilter);
        }

        if (zakModeFilter.isActive() && zakModeFilter.getAttributeValue().equals("true")) {
            ZuordnungSearchUtils.getAnforderungWithoutProzessbaukatenZuordnung(qp);
            ZuordnungSearchUtils.getAnforderungWithoutVereinbarungtypStd(qp);
        } else {
            ZuordnungSearchUtils.getAnforderungForProzessbaukatenGueltig(qp);
        }

        if (prozessbaukastenFilter.isActive()) {
            ZuordnungSearchUtils.getProzessbaukastenQuery(qp, prozessbaukastenFilter);
        }

        if (inProzessbaukastenFilter.isActive()) {
            ZuordnungSearchUtils.getInProzessbaukastenQuery(qp, inProzessbaukastenFilter);
        }

        if (notInProzessbaukastenFilter.isActive()) {
            ZuordnungSearchUtils.getNotInProzessbaukastenQuery(qp, notInProzessbaukastenFilter);
        }

        ZuordnungSearchUtils.getThemenklammerQuery(qp, themenklammerFilter);

        getAnforderungIdQueryPart(qp, derivatVereinbarungStatusFilter);

        ZuordnungSearchUtils.getSortQueryPart(qp);

        Query q = buildQuery(qp);
        return q.getResultList();
    }

    private Query buildQuery(QueryPartDTO qp) {
        Query q = em.createQuery(qp.getQuery(), Anforderung.class);
        qp.getParameterMap().entrySet().forEach(p -> {
            q.setParameter(p.getKey(), p.getValue());
        });
        return q;
    }

    private void getAnforderungIdQueryPart(QueryPartDTO qp, List<DerivatZuordnungStatusFilter> derivatZuordnungStatusFilters) {
        if (derivatZuordnungStatusFilters.stream().anyMatch(f -> f.getZuordnungStatusFilter().isActive()
                || f.getZuordnungZakStatusFilter().isActive())) {
            List<Long> idList = getAnforderungIdListForDerivatAnforderungFilter(derivatZuordnungStatusFilters);
            ZuordnungSearchUtils.getAnforderungIdQuery(qp, idList);
        }
    }

    private List<Long> getAnforderungIdListForDerivatAnforderungFilter(
            List<DerivatZuordnungStatusFilter> derivatZuordnungStatusFilters) {

        Set<Long> columns = new HashSet<>(); // derivatIds
        Map<Long, List<ZuordnungStatus>> derivatAnforderungModulStatus = new HashMap<>();
        Map<Long, List<Boolean>> zakStatus = new HashMap<>();

        for (DerivatZuordnungStatusFilter f : derivatZuordnungStatusFilters) {
            if (f.getZuordnungStatusFilter().isActive()) {
                derivatAnforderungModulStatus.put(f.getDerivatId(), f.getZuordnungStatusFilter().getAsEnumList());
                columns.add(f.getDerivatId());
            }
            if (f.getZuordnungZakStatusFilter().isActive()) {
                zakStatus.put(f.getDerivatId(), f.getZuordnungZakStatusFilter().getAsBoolean());
                columns.add(f.getDerivatId());
            }
        }

        List<List<Long>> columnResults = new ArrayList<>();
        for (Long derivatId : columns) {
            columnResults.add(filterDerivatAnforderungenForDerivatColumn(derivatId,
                    derivatAnforderungModulStatus.get(derivatId), zakStatus.get(derivatId)));
        }

        List<Long> result = new ArrayList<>();
        columnResults.forEach(l -> result.addAll(l));
        columnResults.forEach(l -> result.retainAll(l));
        return result;
    }

    private void getHistoryQueryPart(QueryPartDTO qp, DateSearchFilter freigegebenSeitFilter) {
        if (freigegebenSeitFilter.isActive()) {
            List<Long> historyIdList = getHistoryIdList(freigegebenSeitFilter);
            ZuordnungSearchUtils.getHistoryQuery(qp, historyIdList);
        }
    }

    private List<Long> getHistoryIdList(DateSearchFilter freigegebenSeitFilter) {
        SearchFilterDTO statusFilter = new SearchFilterDTO(SearchFilterType.STATUS);
        List<Status> statusList = new ArrayList<>();
        statusList.add(Status.A_FREIGEGEBEN);
        statusFilter.setStatusListValue(statusList);

        SearchFilterDTO dateSearchFilter = new SearchFilterDTO(SearchFilterType.HISTORIE);
        dateSearchFilter.setStartDateValue(freigegebenSeitFilter.getSelected());

        List<Long> historyIdList = searchDao.getAnforderungHistroyIdList(dateSearchFilter, statusFilter);
        return historyIdList;

    }

    private List<Long> filterDerivatAnforderungenForDerivatColumn(Long derivatId,
                                                                  List<ZuordnungStatus> zuordnungStatus,
                                                                  List<Boolean> zakStatus) {
        Map<String, Object> parameterMap = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        List<Long> offenIdList = null;

        Boolean statusOffen = zuordnungStatus != null && zuordnungStatus.contains(ZuordnungStatus.OFFEN);

        if (statusOffen) {
            String query = "SELECT DISTINCT a.id FROM Anforderung a "
                    + "WHERE (a.status = :statusFreigegeben "
                    + "OR a.status = :statusKeineWeiterverfolgung "
                    + "OR a.status = :statusgeloescht) "
                    + "AND a.id NOT IN "
                    + "(SELECT DISTINCT da.anforderung.id FROM ZuordnungAnforderungDerivat da "
                    + "WHERE da.derivat.id = :derivatId)";

            Query q = em.createQuery(query, Long.class)
                    .setParameter("derivatId", derivatId)
                    .setParameter("statusFreigegeben", Status.A_FREIGEGEBEN)
                    .setParameter("statusKeineWeiterverfolgung", Status.A_KEINE_WEITERVERFOLG)
                    .setParameter("statusgeloescht", Status.A_GELOESCHT);
            offenIdList = q.getResultList();
        }

        sb.append("SELECT DISTINCT a.id FROM Anforderung a "
                + "WHERE (a.status = :statusFreigegeben "
                + "OR a.status = :statusKeineWeiterverfolgung "
                + "OR a.status = :statusgeloescht) "
                + "AND ( a.id IN "
                + "(SELECT DISTINCT da.anforderung.id FROM ZuordnungAnforderungDerivat da "
                + "WHERE da.derivat.id = :derivatId");

        parameterMap.put("statusFreigegeben", Status.A_FREIGEGEBEN);
        parameterMap.put("statusKeineWeiterverfolgung", Status.A_KEINE_WEITERVERFOLG);
        parameterMap.put("statusgeloescht", Status.A_GELOESCHT);
        parameterMap.put("derivatId", derivatId);

        if (zuordnungStatus != null && zuordnungStatus.size() < 5) {
            if (zuordnungStatus.isEmpty()) {
                sb.append(" AND 1 = 0");
            } else {
                String statusParameter = "status";
                sb.append(" AND da.status IN :").append(statusParameter);
                parameterMap.put(statusParameter, zuordnungStatus.stream().map(ZuordnungStatus::getId).collect(Collectors.toList()));
            }
        }

        if (zakStatus != null && zakStatus.size() < 2) {
            if (zakStatus.isEmpty()) {
                sb.append(" AND 1 = 0");
            } else {
                String zakStatusParameter = "zakStatus";
                sb.append(" AND da.nachZakUebertragen IN :").append(zakStatusParameter);
                parameterMap.put(zakStatusParameter, zakStatus);
            }
        }

        if (statusOffen && offenIdList != null && !offenIdList.isEmpty() && (zakStatus == null || zakStatus.contains(Boolean.FALSE))) {
            sb.append(")) OR a.id IN :offenIdList");
            parameterMap.put("offenIdList", offenIdList);
        } else {
            sb.append("))");
        }

        TypedQuery<Long> query = em.createQuery(sb.toString(), Long.class);
        parameterMap.forEach(query::setParameter);
        return query.getResultList();
    }

}
