package de.interfaceag.bmw.pzbk.zuordnung.dto;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungZakStatus;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl
 */
public interface DerivatZuordnung extends Serializable {

    ZuordnungStatus getStatus();

    void setStatus(ZuordnungStatus status);

    DerivatDto getDerivat();

    boolean isStatusChangeEnabled(DerivatDto selectedDerivat, boolean zuordnenMode);

    List<ZuordnungStatus> getNextStatus();

    boolean isZakStatusRendered();

    boolean isZakUebertragungSuccessful();

    boolean isNachZakUebertragen();

    ZuordnungZakStatus getZakStatus();

    String getZakStatusTooltip();

    boolean isChanged();

    String getZeitpunktZuordnung();

    boolean isZeitpunktZuordnungRendered();

}
