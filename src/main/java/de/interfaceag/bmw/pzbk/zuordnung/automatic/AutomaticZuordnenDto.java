package de.interfaceag.bmw.pzbk.zuordnung.automatic;

import java.io.Serializable;

public final class AutomaticZuordnenDto implements Serializable {

    private final Long anforderungId;
    private final String anforderungFachId;
    private final Integer anforderungVersion;
    private final String anforderungDescriptionLong;
    private final String anforderungDescriptionShort;

    private AutomaticZuordnenDto(Builder builder) {
        anforderungId = builder.anforderungId;
        anforderungFachId = builder.anforderungFachId;
        anforderungVersion = builder.anforderungVersion;
        anforderungDescriptionLong = builder.anforderungDescriptionLong;
        anforderungDescriptionShort = builder.anforderungDescriptionShort;
    }

    public Long getAnforderungId() {
        return anforderungId;
    }

    public String getAnforderungFachId() {
        return anforderungFachId;
    }

    public Integer getAnforderungVersion() {
        return anforderungVersion;
    }

    public String getAnforderungDescriptionLong() {
        return anforderungDescriptionLong;
    }

    public String getAnforderungDescriptionShort() {
        return anforderungDescriptionShort;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private Long anforderungId;
        private String anforderungFachId;
        private Integer anforderungVersion;
        private String anforderungDescriptionLong;
        private String anforderungDescriptionShort;

        private Builder() {
        }

        public Builder withAnforderungId(Long val) {
            anforderungId = val;
            return this;
        }

        public Builder withAnforderungFachId(String val) {
            anforderungFachId = val;
            return this;
        }

        public Builder withAnforderungVersion(Integer val) {
            anforderungVersion = val;
            return this;
        }

        public Builder withAnforderungDescriptionLong(String val) {
            anforderungDescriptionLong = val;
            return this;
        }

        public Builder withAnforderungDescriptionShort(String val) {
            anforderungDescriptionShort = val;
            return this;
        }

        public AutomaticZuordnenDto build() {
            return new AutomaticZuordnenDto(this);
        }
    }
}
