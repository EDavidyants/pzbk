package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.shared.utils.AnforderungUtils;
import de.interfaceag.bmw.pzbk.shared.utils.CollectionUtils;
import de.interfaceag.bmw.pzbk.zuordnung.dto.DerivatZuordnung;
import de.interfaceag.bmw.pzbk.zuordnung.dto.DerivatZuordnungBuilder;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import de.interfaceag.bmw.pzbk.zuordnung.dto.ZuordnungBuilder;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author sl
 */
@Stateless
@Named
public class ZuordnungTableDataService {

    @Inject
    private ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;
    @Inject
    private ZuordnungTableDataSortService zuordnungTableDataSortService;

    @Inject
    private LocalizationService localizationService;

    public List<Zuordnung> getTableData(List<DerivatDto> derivate, List<Anforderung> anforderungen) {
        List<Zuordnung> result = new ArrayList<>();
        Collection<ZuordnungAnforderungDerivat> existingZuordnungen = getExistingZuordnungen(derivate, anforderungen);

        Iterator<Anforderung> anforderungIterator = anforderungen.iterator();

        while (anforderungIterator.hasNext()) {
            Anforderung anforderung = anforderungIterator.next();
            Zuordnung zuordnung = getZuordnungForAnforderung(anforderung, derivate, existingZuordnungen);
            result.add(zuordnung);
        }

        return zuordnungTableDataSortService.sortTableData(result);
    }

    private Collection<ZuordnungAnforderungDerivat> getExistingZuordnungen(List<DerivatDto> derivate, List<Anforderung> anforderungen) {
        Collection<Long> derivatIds = new CollectionUtils<DerivatDto>().getIds(derivate);
        Collection<Long> anforderungIds = new CollectionUtils<Anforderung>().getIds(anforderungen);
        return zuordnungAnforderungDerivatService.getAllZuordnungAnforderungDerivatForAnforderungListDerivatListFetch(anforderungIds, derivatIds);
    }

    private Zuordnung getZuordnungForAnforderung(Anforderung anforderung, Collection<DerivatDto> derivate, Collection<ZuordnungAnforderungDerivat> existingZuordnungen) {
        ZuordnungBuilder zuordnungBuilder = ZuordnungBuilder.forAnforderung(anforderung);

        for (DerivatDto derivat : derivate) {
            Long derivatId = derivat.getId();
            Long anforderungId = anforderung.getId();

            Optional<ZuordnungAnforderungDerivat> zuordnung = getZuordnungForAnforderungAndDerivat(existingZuordnungen, anforderungId, derivatId);

            DerivatZuordnung derivatZuordnung;
            if (zuordnung.isPresent()) {
                boolean isStandardvereinbarung = AnforderungUtils.isStandardvereinbarung(anforderung);
                derivatZuordnung = getDerivatZuordnungForExistingZuordnung(zuordnung.get(), derivat, isStandardvereinbarung);
            } else {
                derivatZuordnung = getDerivatZuordnungForNotExistingZuordnung(derivat);
            }

            zuordnungBuilder.withDerivatZuordnung(derivatZuordnung, derivat);
        }

        return zuordnungBuilder.build();
    }

    private DerivatZuordnung getDerivatZuordnungForNotExistingZuordnung(DerivatDto derivat) {
        return DerivatZuordnungBuilder.forNotExistingZuordnung(derivat);
    }

    private DerivatZuordnung getDerivatZuordnungForExistingZuordnung(ZuordnungAnforderungDerivat anforderungDerivat, DerivatDto derivat, boolean isStandardvereinbarung) {
        return DerivatZuordnungBuilder
                .forZuordnung(anforderungDerivat)
                .withDerivat(derivat)
                .withZakStatus(ZuordnungZakStatus.ZAK)
                .withZakLabelTooltip(getZakLabelTooltip(isStandardvereinbarung, anforderungDerivat.isZakUebertragungErfolgreich()))
                .build();
    }

    private String getZakLabelTooltip(Boolean isStandvereinbarung, Boolean isZakUebertragungSuccessful) {
        if (isStandvereinbarung != null && isStandvereinbarung) {
            return localizationService.getValue("zuordnung_zak_tooltip_standardvereinbarung");
        } else if (isZakUebertragungSuccessful != null && isZakUebertragungSuccessful) {
            return localizationService.getValue("zuordnung_zak_tooltip_uebertragung_erfolgreich");
        } else {
            return localizationService.getValue("zuordnung_zak_tooltip_uebertragung_fehlerhaft");
        }
    }

    private static Optional<ZuordnungAnforderungDerivat> getZuordnungForAnforderungAndDerivat(Collection<ZuordnungAnforderungDerivat> existingZuordnungen, Long anforderungId, Long derivatId) {
        return existingZuordnungen.stream()
                .filter(da -> da.getDerivat().getId().equals(derivatId)
                && da.getAnforderung().getId().equals(anforderungId))
                .findFirst();
    }

}
