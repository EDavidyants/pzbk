package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;

import java.util.List;

/**
 *
 * @author sl
 */
public interface ZuordnungView {

    List<Zuordnung> getTableData();

    List<Zuordnung> getSelectedTableData();

    void setSelectedTableData(List<Zuordnung> zuordnung);

    boolean isZuordnenMode();

    boolean isZakMode();

    ZuordnungViewData getViewData();

    List<ZuordnungStatus> getAllNextStatusList();

    ZuordnungStatus getGroupStatus();

    void setGroupStatus(ZuordnungStatus groupStatus);

    boolean isAllSelected();

    void setAllSelected(boolean allSelected);

    void enableDerivat(DerivatDto derivat);

    DerivatDto getSelectedDerivat();

    List<DerivatDto> getFilteredDerivate();

    void setFilteredDerivate(List<DerivatDto> filteredDerivate);

}
