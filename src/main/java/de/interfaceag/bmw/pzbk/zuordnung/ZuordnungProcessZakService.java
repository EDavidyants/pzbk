package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import de.interfaceag.bmw.pzbk.zak.ZakVereinbarungService;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author sl
 */
@Named
@Stateless
public class ZuordnungProcessZakService {

    private static final Logger LOG = LoggerFactory.getLogger(ZuordnungProcessZakService.class);
    @Inject
    private ZakVereinbarungService zakVereinbarungService;
    @Inject
    private DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    private AnforderungService anforderungService;
    @Inject
    private DerivatService derivatService;

    public void processZakChanges(List<Zuordnung> selectedTableData, DerivatDto selectedDerivat) {
        Derivat derivat = getDerivat(selectedDerivat.getId());

        Iterator<Long> anforderungIterator = selectedTableData.stream().map(Zuordnung::getAnforderungId).iterator();

        while (anforderungIterator.hasNext()) {
            Long anforderungId = anforderungIterator.next();
            processZakChange(anforderungId, derivat);
        }
    }

    private void processZakChange(Long anforderungId, Derivat derivat) {
        Anforderung anforderung = getAnforderung(anforderungId);

        if (anforderung.isProzessbaukastenZugeordnet()) {
            LOG.error("Anforderung {} aus Prozessbaukasten. Wird nicht nach ZAK geschickt!", anforderung);
            return;
        }

        List<DerivatAnforderungModul> vereinbarungen = derivatAnforderungModulService.getDerivatAnforderungModulByAnforderungAndDerivat(anforderung, derivat);
        zakVereinbarungService.sendModuleNachZak(vereinbarungen);
    }

    private Anforderung getAnforderung(Long id) {
        return anforderungService.getAnforderungById(id);
    }

    private Derivat getDerivat(Long id) {
        return derivatService.getDerivatById(id);
    }
}
