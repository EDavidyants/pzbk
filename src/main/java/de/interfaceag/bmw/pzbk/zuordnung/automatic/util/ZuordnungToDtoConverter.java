package de.interfaceag.bmw.pzbk.zuordnung.automatic.util;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.zuordnung.automatic.AutomaticZuordnenDialogViewData;
import de.interfaceag.bmw.pzbk.zuordnung.automatic.AutomaticZuordnenDto;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class ZuordnungToDtoConverter {

    private ZuordnungToDtoConverter() {
    }

    public static AutomaticZuordnenDialogViewData buildAutomaticZuordnenDialogViewData(Collection<ZuordnungAnforderungDerivat> zuordnungList) {
        return AutomaticZuordnenDialogViewData.newBuilder()
                .withAutomaticZuordnenDtoList(buildAutomaticZuordnenDto(zuordnungList))
                .build();
    }

    private static List<AutomaticZuordnenDto> buildAutomaticZuordnenDto(Collection<ZuordnungAnforderungDerivat> zuordnungList) {
        if (zuordnungList == null || zuordnungList.isEmpty()) {
            return Collections.emptyList();
        }

        return zuordnungList.stream().map(ZuordnungToDtoConverter::buildAutomaticZuordnung).collect(Collectors.toList());
    }

    private static AutomaticZuordnenDto buildAutomaticZuordnung(ZuordnungAnforderungDerivat zuordnung) {
        final Anforderung anforderung = zuordnung.getAnforderung();
        final String beschreibung = anforderung.getBeschreibungAnforderungDe() != null ? anforderung.getBeschreibungAnforderungDe() : "";
        final String beschreibungShort;

        if (beschreibung.length() > 30) {
            beschreibungShort = beschreibung.substring(0, 29).concat("...");
        } else {
            beschreibungShort = beschreibung;
        }

        return AutomaticZuordnenDto.newBuilder()
                .withAnforderungDescriptionShort(beschreibungShort)
                .withAnforderungDescriptionLong(beschreibung)
                .withAnforderungFachId(anforderung.getFachId())
                .withAnforderungId(anforderung.getId())
                .withAnforderungVersion(anforderung.getVersion())
                .build();
    }
}
