package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.zuordnung.automatic.AutomaticZuordnenDialogViewData;

import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author sl
 */
public interface ZuordnungNavbar {

    String updateDerivatList();

    List<DerivatDto> getFilteredDerivate();

    void setFilteredDerivate(List<DerivatDto> filteredDerivate);

    List<DerivatDto> getDerivate();

    Converter getDerivatConverter();

    String getNumberOfResults();

    DerivatStatusNames getDerivatStatusNames();

    String toggleZuordnenMode();

    String toggleZakMode();

    String process();

    String cancel();

    ZuordnungViewPermission getViewPermission();

    void automatischZuordnen();

    AutomaticZuordnenDialogViewData getAutomaticZuordnenDialogViewData();
}
