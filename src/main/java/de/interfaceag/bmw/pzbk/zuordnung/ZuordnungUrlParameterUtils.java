package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.filter.BooleanUrlFilter;
import de.interfaceag.bmw.pzbk.filter.UrlFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author sl
 */
public final class ZuordnungUrlParameterUtils {

    private ZuordnungUrlParameterUtils() {
    }

    public static boolean getModeValue(UrlParameter urlParameter, BooleanUrlFilter urlFilter) {
        Optional<String> parameterValue = urlParameter.getValue(urlFilter.getParameterName());
        if (parameterValue.isPresent()) {
            boolean result = parseParameterValue(parameterValue.get());
            if (result) {
                urlFilter.setActive();
            }
            return result;
        }
        return false;
    }

    public static Optional<DerivatDto> getSelectedDerivat(UrlParameter urlParameter, UrlFilter urlFilter, List<DerivatDto> allDerivate) {
        Optional<String> parameterValue = urlParameter.getValue(urlFilter.getParameterName());
        if (parameterValue.isPresent()) {
            String value = parameterValue.get();
            if (RegexUtils.matchesId(value)) {
                Long derivatId = Long.parseLong(value);
                return allDerivate.stream().filter(d -> d.getId().equals(derivatId)).findAny();
            }
        }
        return Optional.empty();
    }

    private static boolean parseParameterValue(String value) {
        return "true".equals(value);
    }
}
