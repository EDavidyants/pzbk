package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.filter.BooleanSearchFilter;
import de.interfaceag.bmw.pzbk.filter.BooleanUrlFilter;
import de.interfaceag.bmw.pzbk.filter.FachIdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.IdListUrlFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ThemenklammerFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * @author sl
 */
final class ZuordnungSearchUtils {

    private ZuordnungSearchUtils() {
    }

    static QueryPartDTO getExistingZuordnungBaseQuery(Set<Long> derivatIds) {
        QueryPartDTO qp;
        if (derivatIds == null || derivatIds.isEmpty()) {
            qp = new QueryPartDTO("SELECT DISTINCT a FROM ZuordnungAnforderungDerivat zad "
                    + "INNER JOIN zad.anforderung a "
                    + "INNER JOIN a.anfoFreigabeBeiModul AS afbm"
                    + "LEFT JOIN FETCH a.prozessbaukasten "
                    + "LEFT JOIN FETCH a.festgestelltIn "
                    + "LEFT JOIN a.prozessbaukastenThemenklammern pt "
                    + "LEFT JOIN FETCH afbm.modulSeTeam "
                    + "WHERE 1=0");
        } else {
            qp = new QueryPartDTO("SELECT DISTINCT a FROM ZuordnungAnforderungDerivat zad "
                    + "INNER JOIN zad.anforderung a "
                    + "INNER JOIN a.anfoFreigabeBeiModul AS afbm "
                    + "LEFT JOIN FETCH a.prozessbaukasten "
                    + "LEFT JOIN FETCH a.festgestelltIn "
                    + "LEFT JOIN a.prozessbaukastenThemenklammern pt "
                    + "LEFT JOIN FETCH afbm.modulSeTeam "
                    + "WHERE zad.derivat.id IN :derivatIdList");
            qp.put("derivatIdList", derivatIds);
        }

        return qp;
    }

    static QueryPartDTO getBaseQuery(List<Long> existingZuordnungAnforderungIds) {
        QueryPartDTO qp = new QueryPartDTO("SELECT DISTINCT a FROM Anforderung a "
                + "INNER JOIN a.anfoFreigabeBeiModul afbm "
                + "LEFT JOIN FETCH a.prozessbaukasten "
                + "LEFT JOIN FETCH a.festgestelltIn "
                + "LEFT JOIN a.prozessbaukastenThemenklammern pt "
                + "WHERE a.status IN :statusFreigegeben ");
        qp.put("statusFreigegeben", Arrays.asList(Status.A_FREIGEGEBEN, Status.A_GENEHMIGT_IN_GUELTIGEM_PROZESSBAUKASTEN));
        if (existingZuordnungAnforderungIds != null && !existingZuordnungAnforderungIds.isEmpty()) {
            qp.append("AND a.id NOT IN :existingZuordnungAnforderungIds ");
            qp.put("existingZuordnungAnforderungIds", existingZuordnungAnforderungIds);
        }
        return qp;
    }

    static void getSortQueryPart(QueryPartDTO qp) {
        qp.append(" ORDER BY LENGTH(a.fachId) DESC, a.fachId DESC");
    }

    static void getFestgestlltInQuery(QueryPartDTO qp, Set<Long> festgestelltInIds) {
        qp.append(" AND a.festgestelltIn IN :festgestelltInList");
        qp.put("festgestelltInList", festgestelltInIds);
    }

    static void getSensorCocQuery(QueryPartDTO qp, IdSearchFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND a.sensorCoc.sensorCocId IN :sensorCocs");
            qp.put("sensorCocs", filter.getSelectedIdsAsSet());
        }
    }

    static void getAnforderungQuery(QueryPartDTO qp, FachIdSearchFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND (");
            int i = 0;
            for (SearchFilterObject selectedValue : filter.getSelectedValues()) {
                qp.append(" LOWER(a.fachId) LIKE LOWER(:fachId" + i + ") OR");
                qp.put("fachId" + i, selectedValue.getName());
                i++;
            }
            qp.append(" 1=0)");
        }
    }

    static void getProzessbaukastenQuery(QueryPartDTO qp, IdSearchFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND a.prozessbaukasten IN :prozessbaukaesten");
            qp.put("prozessbaukaesten", filter.getSelectedIdsAsSet());
        }
    }

    static void getThemenklammerQuery(QueryPart qp, ThemenklammerFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND pt.themenklammer.id IN :themenklammerIds");
            qp.put("themenklammerIds", filter.getSelectedIdsAsSet());
        }
    }

    static void getInProzessbaukastenQuery(QueryPartDTO qp, BooleanUrlFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND a.prozessbaukasten IS NOT EMPTY");
        }
    }

    static void getNotInProzessbaukastenQuery(QueryPartDTO qp, BooleanUrlFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND a.prozessbaukasten IS EMPTY");
        }
    }

    static void getBeschreibungQuery(QueryPartDTO qp, TextSearchFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND LOWER(a.beschreibungAnforderungDe) LIKE LOWER(:beschreibung)");
            qp.put("beschreibung", "%" + filter.getAsString() + "%");
        }
    }

    static void getPhasenrelevanzQuery(QueryPartDTO qp, BooleanSearchFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND a.phasenbezug IN :phasenbezug");
            qp.put("phasenbezug", filter.getAsBoolean());
        }
    }

    static void getHistoryQuery(QueryPartDTO qp, List<Long> historyIdList) {
        if (historyIdList == null || historyIdList.isEmpty()) {
            qp.append(" AND 1=0");
        } else {
            qp.append(" AND a.id IN :historyIdList");
            qp.put("historyIdList", historyIdList);
        }
    }

    static void getAnforderungIdQuery(QueryPartDTO qp, List<Long> idList) {
        if (idList == null || idList.isEmpty()) {
            qp.append(" AND 1=0");
        } else {
            qp.append(" AND a.id IN :idList");
            qp.put("idList", idList);
        }
    }

    static void getAnforderungIdUrlQuery(QueryPartDTO qp, IdListUrlFilter filter) {
        if (filter.isActive()) {
            Set<Long> idList = filter.getIds();
            if (idList == null || idList.isEmpty()) {
                qp.append(" AND 1 = 0");
            } else {
                qp.append(" AND a.id IN :anforderungIdUrlList");
                qp.put("anforderungIdUrlList", idList);
            }
        }
    }

    static void getAnforderungWithoutProzessbaukatenZuordnung(QueryPartDTO qp) {
        qp.append(" AND a.id NOT IN (SELECT a.id FROM Prozessbaukasten p INNER JOIN p.anforderungen AS a )");
    }

    static void getAnforderungWithoutVereinbarungtypStd(QueryPartDTO qp) {
        qp.append(" AND afbm.vereinbarung  != :vereinbarungTyp ");
        qp.put("vereinbarungTyp", VereinbarungType.STANDARD);
    }

    static void getAnforderungForProzessbaukatenGueltig(QueryPartDTO qp) {
        qp.append(" AND ((a.id NOT IN (SELECT a.id FROM Prozessbaukasten p INNER JOIN p.anforderungen AS a )) ");
        qp.append(" OR (a.id IN (SELECT a.id FROM Prozessbaukasten p INNER JOIN p.anforderungen AS a WHERE p.status = 4)))");
    }
}
