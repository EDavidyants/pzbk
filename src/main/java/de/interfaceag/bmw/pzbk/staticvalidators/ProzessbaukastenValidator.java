package de.interfaceag.bmw.pzbk.staticvalidators;

import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;

/**
 *
 * @author fn
 */
public final class ProzessbaukastenValidator {

    private ProzessbaukastenValidator() {
    }

    public static boolean validatProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        if (prozessbaukasten == null) {
            return false;
        } else if (prozessbaukasten.getId() != null
                && prozessbaukasten.getFachId() != null
                && prozessbaukasten.getVersion() != 0) {
            return true;
        }
        return false;
    }
}
