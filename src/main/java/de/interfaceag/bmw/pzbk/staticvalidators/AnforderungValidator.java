package de.interfaceag.bmw.pzbk.staticvalidators;

import de.interfaceag.bmw.pzbk.entities.Anforderung;

/**
 *
 * @author fn
 */
public final class AnforderungValidator {

    private AnforderungValidator() {
    }

    public static boolean validatAnforderung(Anforderung anforderung) {
        if (anforderung == null) {
            return false;
        } else if (anforderung.getId() != null && anforderung.getFachId() != null && anforderung.getVersion() != null) {
            return true;
        }
        return false;
    }
}
