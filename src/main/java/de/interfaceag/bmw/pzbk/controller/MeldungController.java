package de.interfaceag.bmw.pzbk.controller;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.MeldungInlineEditViewPermission;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.StatusUtils;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungViewPermission;
import de.interfaceag.bmw.pzbk.controller.dialogs.SensorCocDialogController;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Auswirkung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 * @author fp
 */
@ViewScoped
@Named
public class MeldungController extends AbstractAnforderungController {

    public static final String FACES_REDIRECT_TRUE = "&faces-redirect=true";
    private Meldung anfoMgmtObject;
    MenuModel anforderungListMenuModel;
    private List<Anforderung> anforderungList = new ArrayList<>();

    private AnforderungViewPermission viewPermission;

    @PostConstruct
    @Override
    public void init() {
        String idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");

        if (idstr != null && !"null".equals(idstr)) {
            originalObjectId = Long.parseLong(idstr);
        }

        if (originalObjectId != null) {
            berechtigt = anforderungService.isMeldungForUserRole(originalObjectId);
        } else {
            berechtigt = true;
        }

        setUserRollen(getBerechtigungService().getRolesForUser(session.getUser()));

        if (originalObjectId != null && berechtigt) {
            anfoMgmtObject = getAnforderungService().getMeldungWorkCopyByIdOrNewMeldung(originalObjectId);
            anforderungList = getAnforderungService().getAllAnforderungenWithThisMeldung(originalObjectId);
        }

        if (anfoMgmtObject == null) {
            anfoMgmtObject = new Meldung();
            anfoMgmtObject.setSensor(session.getUser());
            anfoMgmtObject.setStatus(Status.M_ENTWURF);
            anfoMgmtObject.setStaerkeSchwaeche(true);
            anfoMgmtObject.setAuswirkungen(new ArrayList<>());
        } else {
            nextStatusList = StatusUtils.getAllNextStatusAsList(anfoMgmtObject.getStatus());
        }

        initAuswirkungsarten();
        auswirkung = new Auswirkung();
        initStandardBild();

        this.setRequiredByMelden(isRequierdByMeldung());

        this.sensorCocDialogController = new SensorCocDialogController(this.sensorCocService, this.getBerechtigungService(), this.anfoMgmtObject, session.getUser());

        setAllFestgestelltIn(anforderungService.getAllFestgestelltIn());

        Set<Rolle> rolesWithWritePermissions;
        if (anfoMgmtObject.getSensorCoc() != null) {
            rolesWithWritePermissions = session.getUserPermissions()
                    .getRolesWithWritePermissionsForMeldung(anfoMgmtObject.getSensorCoc().getSensorCocId());
        } else {
            rolesWithWritePermissions = session.getUserPermissions()
                    .getRolesWithWritePermissionsForNewMeldung();
        }

        viewPermission = new AnforderungViewPermission(rolesWithWritePermissions,
                anfoMgmtObject.getStatus(), berechtigt, false);
        session.setLocationForView();
        setAllWerk(werkService.getAllWerk());
        super.initDetektoren(getAnfoMgmtObject().getDetektoren());
    }

    private boolean isRequierdByMeldung() {
        return anfoMgmtObject.getNextStatus() == Status.M_GEMELDET && anfoMgmtObject.isStatusChangeRequested() || anfoMgmtObject.getStatus() == Status.M_GEMELDET;
    }

    @Override
    public String processGeloeschtStatusChange() {
        return super.processStatusChange(Status.M_GELOESCHT);
    }

    @Override
    public Meldung getAnfoMgmtObject() {
        return anfoMgmtObject;
    }

    public void setAnfoMgmtObject(Meldung anfoMgmtObject) {
        this.anfoMgmtObject = anfoMgmtObject;
    }

    @Override
    public String edit() {
        anfoMgmtObject.setStatusChangeRequested(false);
        getAnforderungService().saveMeldung(anfoMgmtObject);
        return "meldungEdit.xhtml?id=" + anfoMgmtObject.getId() + FACES_REDIRECT_TRUE;
    }

    @Override
    public String save() {
        getAnfoMgmtObject().setDetektoren(super.getDetektorenValues());
        Mitarbeiter currentUser = session.getUser();
        this.anfoMgmtObject = anforderungService.saveChangesToMeldung(anfoMgmtObject, statusChangeKommentar, currentUser, getAnhangForBild());
        return "meldungView.xhtml?id=" + anfoMgmtObject.getId() + FACES_REDIRECT_TRUE;
    }

    public String saveFromView() {
        return save();
    }

    public String saveValidatedData() {
        return save();
    }

    @Override
    public String cancel() {
        if (anfoMgmtObject.getId() != null) {
            return "meldungView.xhtml?id=" + anfoMgmtObject.getId() + FACES_REDIRECT_TRUE;
        } else {
            return "dashboard?faces-redirect=true";
        }
    }

    @Override
    public String changeStatus(Integer newStatus) {
        if (newStatus != null) {
            if ((newStatus == 11 || newStatus == 2) && (statusChangeKommentar == null || statusChangeKommentar.equals(""))) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Kommentar zum Statuswechsel fehlt!", "Beim Wechsel zum Status " + Status.getStatusById(newStatus).toString() + " ist ein Kommentar notwendig."));
            } else {
                anfoMgmtObject = anforderungService.changeMeldungStatus(anfoMgmtObject, newStatus, statusChangeKommentar);

                switch (newStatus) {
                    case 3:
                        return "anforderungEdit.xhtml?fid=" + anfoMgmtObject.getFachId() + FACES_REDIRECT_TRUE;
                    case 1:
                        return "meldungEdit.xhtml?id=" + anfoMgmtObject.getId() + FACES_REDIRECT_TRUE;
                    default:
                        return save();
                }
            }
        }
        return "";
    }

    public boolean isViewDisableEdit() {
        Status status = anfoMgmtObject.getStatus();
        boolean hasRightToEdit = viewPermission.getEditButton();
        return !MeldungInlineEditViewPermission.isInlineEdit(status, hasRightToEdit);
    }

    @Override
    public String restore() {
        Mitarbeiter currentUser = session.getUser();
        anforderungService.restoreMeldung(anfoMgmtObject, currentUser);
        return PageUtils.getUrlForPageWithId(Page.MELDUNGVIEW, anfoMgmtObject.getId());
    }

    public boolean isNeueMeldung() {
        return getAnfoMgmtObject().getFachId() == null || getAnfoMgmtObject().getFachId().length() == 0;
    }

    public MenuModel getAnforderungListMenuModel() {
        anforderungListMenuModel = new DefaultMenuModel();

        anforderungList.stream().map(anfo -> {
            DefaultMenuItem menuitem = new DefaultMenuItem();
            menuitem.setUrl("anforderungView.xhtml?id=" + anfo.getId() + FACES_REDIRECT_TRUE);
            menuitem.setIcon("ui-icon-arrow-1-e");
            menuitem.setValue(" " + anfo.getFachId() + " | V" + anfo.getVersion().toString());
            return menuitem;
        }).forEachOrdered(menuitem ->
                 anforderungListMenuModel.addElement(menuitem)
        );
        return anforderungListMenuModel;
    }

    public void setAnforderungListMenuModel(MenuModel anforderungListMenuModel) {
        this.anforderungListMenuModel = anforderungListMenuModel;
    }

    public List<Anforderung> getAnforderungList() {
        return anforderungList;
    }

    public void setAnforderungList(List<Anforderung> anforderungList) {
        this.anforderungList = anforderungList;
    }

    @Override
    public AnforderungViewPermission getViewPermission() {
        return viewPermission;
    }

    @Override
    public boolean isAfoMgmtObjectInstanceOfAnforderung() {
        return false;
    }

} //end of class
