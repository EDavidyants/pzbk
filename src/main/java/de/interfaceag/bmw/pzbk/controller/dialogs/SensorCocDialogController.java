package de.interfaceag.bmw.pzbk.controller.dialogs;

import de.interfaceag.bmw.pzbk.converters.SensorCocConverter;
import de.interfaceag.bmw.pzbk.entities.AbstractAnfoMgmtObject;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import org.primefaces.context.RequestContext;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author fp
 */
public class SensorCocDialogController implements Serializable, SensorCocDialogFields, SensorCocDialogMethods {

    private final SensorCocService sensorCocService;
    private final BerechtigungService berechtigungService;

    private SensorCoc sensorCoc;
    private String inputSensorCocRessortOrtung;
    private String inputSensorCocTechnologie;

    private String inputScocRessort;
    private String inputScocOrtung;

    private List<String> sensorCocRoList;
    private List<String> sensorCocTechnologieList;

    private Mitarbeiter user;

    private final AbstractAnfoMgmtObject anf;

    public SensorCocDialogController(SensorCocService sensorCocService, BerechtigungService berechtigungService, AbstractAnfoMgmtObject anf, Mitarbeiter user) {
        this.sensorCocService = sensorCocService;
        this.berechtigungService = berechtigungService;
        this.anf = anf;
        this.user = user;
        init();
    }


    private void init() {
        initSensorCocROList();
        initSensorCocTechnologieList();
    }

    @Override
    public void refresh() {
        init();
    }

    public void clearSelection() {
        this.sensorCoc = null;
        this.inputSensorCocRessortOrtung = null;
        this.inputScocRessort = null;
        this.inputScocOrtung = null;
        this.inputSensorCocTechnologie = null;
    }

    public void initSensorCocROList() {
        List<String> ros = new ArrayList<>();
        List<SensorCoc> allScocs = sensorCocService.getSensorCocsByRessortTechnologieOrtungByRole(inputScocRessort, null, null);
        for (int i = 0; i < allScocs.size(); i++) {
            SensorCoc tmp = allScocs.get(i);
            String roTmp = tmp.getRessort() + ">" + tmp.getOrtung();
            if (!ros.contains(roTmp)) {
                ros.add(roTmp);
            }
        }
        Collections.sort(ros);
        this.sensorCocRoList = ros;
    }

    public void initSensorCocTechnologieList() {
        List<String> technologieen = new ArrayList<>();
        if (inputScocRessort != null && !inputScocRessort.isEmpty()) {
            List<SensorCoc> allScocs = sensorCocService.getSensorCocsByRessortTechnologieOrtungByRole(inputScocRessort, inputScocOrtung, null);
            for (int i = 0; i < allScocs.size(); i++) {
                if (allScocs.get(i).getTechnologie() != null && !allScocs.get(i).getTechnologie().isEmpty() && !technologieen.contains(allScocs.get(i).getTechnologie())) {
                    technologieen.add(allScocs.get(i).getTechnologie());
                }
            }
        }
        Collections.sort(technologieen);
        this.sensorCocTechnologieList = technologieen;
    }

    @Override
    public List<SensorCoc> completeSensorCoc(String query) {

        if (query == null || query.isEmpty()) {
            return Collections.emptyList();
        }

        String lowerCaseQuery = query.toLowerCase();

        List<SensorCoc> allScocs = sensorCocService.getSensorCocsByRessortTechnologieOrtungByRole(null, null, null);
        List<SensorCoc> filteredScocs = new ArrayList<>();
        for (int i = 0; i < allScocs.size(); i++) {
            SensorCoc scoc = allScocs.get(i);
            if (scoc.pathToString().toLowerCase().contains(lowerCaseQuery)) {
                filteredScocs.add(scoc);
            }
        }
        return filteredScocs;
    }

    @Override
    public SensorCocConverter getSensorCocConverter() {
        return new SensorCocConverter(sensorCocService);
    }

    @Override
    public List<String> getSensorCocRessortOrtungList() {
        return Collections.unmodifiableList(sensorCocRoList);
    }

    @Override
    public List<String> getSensorCocTechnologieList() {
        return Collections.unmodifiableList(this.sensorCocTechnologieList);
    }

    public void showSensorCocDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('sensorCocDialog').show()");
    }

    /**
     * @return the inputSensorCocTechnologie
     */
    @Override
    public String getInputSensorCocTechnologie() {
        return inputSensorCocTechnologie;
    }

    /**
     * @param inputScocTechnologie the inputSensorCocTechnologie to set
     */
    @Override
    public void setInputSensorCocTechnologie(String inputScocTechnologie) {
        this.inputSensorCocTechnologie = inputScocTechnologie;
    }

    /**
     * @return the sensorCoc
     */
    @Override
    public SensorCoc getSensorCocAutoComplete() {
        if (inputScocOrtung != null && !inputScocOrtung.isEmpty()
                || inputSensorCocTechnologie != null && !inputSensorCocTechnologie.isEmpty()) {
            return sensorCocService.getSensorCocsByRessortTechnologieOrtungByRole(null, inputScocOrtung, inputSensorCocTechnologie).get(0);
        }
        return sensorCoc;
    }

    /**
     * @param sensorCoc the sensorCoc to set
     */
    @Override
    public void setSensorCocAutoComplete(SensorCoc sensorCoc) {
        if (sensorCoc != null) {
            this.inputScocRessort = sensorCoc.getRessort();
            this.inputScocOrtung = sensorCoc.getOrtung();
            this.inputSensorCocTechnologie = sensorCoc.getTechnologie();
            this.inputSensorCocRessortOrtung = sensorCoc.getRessort() + ">" + sensorCoc.getOrtung();
        }
        this.sensorCoc = sensorCoc;
    }

    /**
     * @return the inputSensorCocRessortOrtung
     */
    @Override
    public String getInputSensorCocRessortOrtung() {
        return inputSensorCocRessortOrtung;
    }

    /**
     * @param inputSensorCocRessortOrtung the inputSensorCocRessortOrtung to set
     */
    @Override
    public void setInputSensorCocRessortOrtung(String inputSensorCocRessortOrtung) {
        if (inputSensorCocRessortOrtung != null) {
            this.inputScocRessort = inputSensorCocRessortOrtung.split(">")[0];
            if (inputSensorCocRessortOrtung.split(">").length < 2) {
                this.inputScocOrtung = "";
            } else {
                this.inputScocOrtung = inputSensorCocRessortOrtung.split(">")[1];
            }
        }
        this.inputSensorCocRessortOrtung = inputSensorCocRessortOrtung;
    }

    @Override
    public void selectSensorCoC() {
        Optional<Mitarbeiter> sensorCocLeiter = berechtigungService.getSensorCoCLeiterOfSensorCoc(this.getSensorCocAutoComplete());
        this.anf.setSensorCoc(this.getSensorCocAutoComplete());
        if (sensorCocLeiter.isPresent()) {
            this.anf.getSensorCoc().setSensorCoCLeiter(sensorCocLeiter.get());
        }
    }

    public void deselectSensorCoC() {
        this.anf.setSensorCoc(null);
        this.clearSelection();
    }

    public Mitarbeiter getUser() {
        return user;
    }

    public void setUser(Mitarbeiter user) {
        this.user = user;
    }

}
