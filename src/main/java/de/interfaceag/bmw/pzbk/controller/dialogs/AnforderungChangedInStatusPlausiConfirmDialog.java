package de.interfaceag.bmw.pzbk.controller.dialogs;

/**
 *
 * @author fn
 */
public interface AnforderungChangedInStatusPlausiConfirmDialog {

    String saveFromDialog();
}
