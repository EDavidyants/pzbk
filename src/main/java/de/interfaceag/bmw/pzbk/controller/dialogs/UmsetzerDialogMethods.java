package de.interfaceag.bmw.pzbk.controller.dialogs;

/**
 *
 * @author fn
 */
public interface UmsetzerDialogMethods {

    void processUmsetzerChanges();

    void resetUmsetzerList();

}
