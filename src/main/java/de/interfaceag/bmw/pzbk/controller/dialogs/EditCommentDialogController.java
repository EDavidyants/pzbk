package de.interfaceag.bmw.pzbk.controller.dialogs;

import de.interfaceag.bmw.pzbk.shared.dto.Commentable;
import org.primefaces.context.RequestContext;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class EditCommentDialogController implements Serializable {

    private String header;

    private Commentable commentable;

    public EditCommentDialogController() {
        header = "";
    }

    public EditCommentDialogController(String header) {
        this.header = header;
    }

    // ---------- methods ------------------------------------------------------
    public void show(Commentable commentable) {
        this.commentable = commentable;
        show();
    }

    public void show() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialog:editCommendDialogForm");
        context.execute("PF('editCommendDialog').show();");
    }

    public void hide() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('editCommendDialog').hide();");
    }

    // ---------- getter setter ------------------------------------------------
    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getKommentar() {
        return commentable.getComment();
    }

    public void setKommentar(String kommentar) {
        commentable.setComment(kommentar);
    }

    public Commentable getCommentable() {
        return commentable;
    }

}
