package de.interfaceag.bmw.pzbk.controller;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungEditDtoImpl;
import de.interfaceag.bmw.pzbk.converters.MitarbeiterConverter;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.permissions.pages.EinstellungenViewPermission;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.services.UserService;
import de.interfaceag.bmw.pzbk.session.SessionEdit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
@Named
@RequestScoped
public class PersonalSettingsController implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(PersonalSettingsController.class);

    @Inject
    private UserSearchService uss;
    @Inject
    private UserService userService;
    @Inject
    private SessionEdit session;
    @Inject
    private ConfigService configService;
    @Inject
    private TteamService tteamService;

    private Mitarbeiter user;

    private UserPermissions userPermissions;

    private EinstellungenViewPermission viewPermission;

    @PostConstruct
    public void init() {
        this.userPermissions = session.getUserPermissions();

        this.user = session.getUser();

        viewPermission = new EinstellungenViewPermission(session.getUserPermissions().getRollen());

        session.setLocationForView();
    }

    public void changeUserLocation() {
        saveUser();
        FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(user.getLocation()));
        refreshAfterUserChange();
    }

    public void refreshAfterUserChange() {
        session.setUser(user);

        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
        } catch (IOException ex) {
            LOG.error(null, ex);
        }
    }

    public List<Mitarbeiter> getAllMitarbeiter() {
        return uss.getAllMitarbeiter();
    }

    public List<String> getAllLocations() {
        List<String> result = new ArrayList<>();
        result.add("DE");
        result.add("EN");
        return result;
    }

    public boolean isMailReceiptEnabled() {
        return user.isMailReceiptEnabled();
    }

    public Boolean isBmwUmgebung() {
        return configService.isProductionEnvironment();
    }

    public void setMailReceiptEnabled(boolean input) {
        user.setMailReceiptEnabled(input);
        userService.saveMitarbeiter(user);
    }

    public Converter getMitarbeiterConverter() {
        return new MitarbeiterConverter(uss);
    }

    public void saveUser() {
        userService.saveMitarbeiter(user);
    }

    public Mitarbeiter getUser() {
        return user;
    }

    public void setUser(Mitarbeiter user) {
        this.user = user;
    }

    // ---------- new methods --------------------------------------------------
    public List<Rolle> getRollen() {
        return userPermissions.getRollen();
    }

    public List<BerechtigungEditDtoImpl> getSensorCocAsSensorSchreibend() {
        return userPermissions.getSensorCocAsSensorSchreibend();
    }

    public List<BerechtigungEditDtoImpl> getSensorCocAsSensorLesend() {
        return userPermissions.getSensorCocAsSensorLesend();
    }

    public List<BerechtigungEditDtoImpl> getSensorCocAsSensorEingSchreibend() {
        return userPermissions.getSensorCocAsSensorEingSchreibend();
    }

    public List<BerechtigungEditDtoImpl> getSensorCocAsSensorCocLeiterSchreibend() {
        return userPermissions.getSensorCocAsSensorCocLeiterSchreibend();
    }

    public List<BerechtigungEditDtoImpl> getSensorCocAsSensorCocLeiterLesend() {
        return userPermissions.getSensorCocAsSensorCocLeiterLesend();
    }

    public List<BerechtigungEditDtoImpl> getSensorCocAsVertreterSCLSchreibend() {
        return userPermissions.getSensorCocAsVertreterSCLSchreibend();
    }

    public List<BerechtigungEditDtoImpl> getSensorCocAsVertreterSCLLesend() {
        return userPermissions.getSensorCocAsVertreterSCLLesend();
    }

    public List<BerechtigungEditDtoImpl> getSensorCocAsUmsetzungsbestaetigerSchreibend() {
        return userPermissions.getSensorCocAsUmsetzungsbestaetigerSchreibend();
    }

    public List<BerechtigungEditDtoImpl> getTteamAsTteamleiterSchreibend() {
        return userPermissions.getTteamAsTteamleiterSchreibend();
    }

    public List<BerechtigungEditDtoImpl> getDerivatAsAnfordererSchreibend() {
        return userPermissions.getDerivatAsAnfordererSchreibend();
    }

    public List<BerechtigungEditDtoImpl> getModulSeTeamAsEcocSchreibend() {
        return userPermissions.getModulSeTeamAsEcocSchreibend();
    }

    public List<BerechtigungEditDtoImpl> getZakEinordnungAsAnfordererSchreibend() {
        return userPermissions.getZakEinordnungAsAnfordererSchreibend();
    }

    public List<BerechtigungEditDtoImpl> getSensorCocAsLeser() {
        return userPermissions.getSensorCocAsLeser();
    }

    public List<String> getTteamMitgliedLeser() {
        List<Tteam> tteamByIdList = tteamService.getTteamByIdList(userPermissions.getTteamIdsForTteamMitgliedLesend());
        return tteamByIdList.stream().map(Tteam::getTeamName).collect(Collectors.toList());
    }

    public List<String> getTteamMitgliedSchreibend() {
        List<Tteam> tteamByIdList = tteamService.getTteamByIdList(userPermissions.getTteamIdsForTteamMitgliedSchreibend());
        return tteamByIdList.stream().map(Tteam::getTeamName).collect(Collectors.toList());
    }

    public List<String> getTteamVertreter() {
        List<Tteam> tteamByIdList = tteamService.getTteamByIdList(userPermissions.getTteamIdsForTteamVertreter());
        return tteamByIdList.stream().map(Tteam::getTeamName).collect(Collectors.toList());
    }

    public EinstellungenViewPermission getViewPermission() {
        return viewPermission;
    }

} // end of class
