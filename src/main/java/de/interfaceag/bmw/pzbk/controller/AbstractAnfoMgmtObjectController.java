package de.interfaceag.bmw.pzbk.controller;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.controller.dialogs.SensorCocDialogController;
import de.interfaceag.bmw.pzbk.controller.dialogs.StatusChangeMethodChangeDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.StatusChangeNextStatusDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.UmsetzerController;
import de.interfaceag.bmw.pzbk.converters.MitarbeiterConverter;
import de.interfaceag.bmw.pzbk.converters.TteamConverter;
import de.interfaceag.bmw.pzbk.entities.AbstractAnfoMgmtObject;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.comparator.TteamComparator;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.mail.MailService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.FileService;
import de.interfaceag.bmw.pzbk.services.ImageService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.session.Session;
import org.apache.poi.util.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.menu.MenuModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author fp
 */
public abstract class AbstractAnfoMgmtObjectController implements Serializable, StatusChangeMethodChangeDialog,
        StatusChangeNextStatusDialog {

    protected static final Logger LOG = LoggerFactory.getLogger(AbstractAnfoMgmtObjectController.class);

    @Inject
    protected Session session;

    @Inject
    SensorCocService sensorCocService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private MailService mailService;
    @Inject
    private FileService fileService;
    @Inject
    private ImageService imageService;
    @Inject
    private TteamService tteamService;
    @Inject
    ModulService modulService;
    @Inject
    private AnforderungMeldungHistoryService historyService;
    @Inject
    private UserSearchService uss;

    protected UmsetzerController umsetzerController;
    protected SensorCocDialogController sensorCocDialogController;

    boolean berechtigt;
    boolean created;
    Long originalObjectId;
    String originalObjectFachId;

    List<Status> nextStatusList = new ArrayList<>();
    Integer selectedNextStatus;
    String statusChangeKommentar;

    UploadedFile anhang;
    private Anhang anhangForBild;
    Anhang selectedAnhang;

    String versionKommentar;
    protected MenuModel versionsMenuItems;
    String vsLabel;

    public abstract void init();

    public void initStandardBild() {
        getAnfoMgmtObject().getAnhaenge().forEach(a -> {
            if (a.isStandardBild()) {
                setAnhangForBild(a);
            }
        });
    }

    public abstract AbstractAnfoMgmtObject getAnfoMgmtObject();

    public abstract String edit();

    public abstract String save();

    public abstract String cancel();

    public abstract String processStatusChange(Status newStatus);

    public abstract String restore();

    public Boolean isStatusChangeKommentarRequired() {
        Status s = null;
        if (getSelectedNextStatus() != null) {
            s = Status.getStatusById(getSelectedNextStatus());
        }

        return s != null && (s == Status.A_GELOESCHT || s == Status.M_GELOESCHT || s == Status.A_UNSTIMMIG || s == Status.M_UNSTIMMIG);
    }

    @Override
    public abstract String changeStatus(Integer newStatus);

    public List<Status> getNextStatusList() {
        return nextStatusList;
    }

    public void setNextStatusList(List<Status> nextStatusList) {
        this.nextStatusList = nextStatusList;
    }

    @Override
    public Integer getSelectedNextStatus() {
        return selectedNextStatus;
    }

    @Override
    public void setSelectedNextStatus(Integer selectedNextStatus) {
        this.selectedNextStatus = selectedNextStatus;
    }

    @Override
    public String getStatusChangeKommentar() {
        return statusChangeKommentar;
    }

    @Override
    public void setStatusChangeKommentar(String statusChangeKommentar) {
        this.statusChangeKommentar = statusChangeKommentar;
    }

    public void uploadAnhang(FileUploadEvent event) {
        LOG.info("Upload started...");
        this.anhang = event.getFile();
        if (anhang != null) {
            LOG.info("Done uploading {}", anhang.getFileName());
            try {
                byte[] uploadedData = IOUtils.toByteArray(anhang.getInputstream());
                Anhang a = new Anhang(session.getUser(), anhang.getFileName(), anhang.getContentType(), uploadedData);
                getAnfoMgmtObject().addAnhang(a);
            } catch (IOException ex) {
                LOG.error(null, ex);
            }
        }
    }

    public StreamedContent downloadAnhang(Anhang anhang) {
        return getImageService().downloadAnhang(anhang);
    }

    public void removeVorschaubild() {
        this.anhangForBild = null;
    }

    public void showAnfoMgmtObjectEditAnhangGrafikDialog(Anhang anhang) {
        RequestContext context = RequestContext.getCurrentInstance();
        if (anhang != null) {
            setSelectedAnhang(anhang);
        }

        context.update("dialog:anfoMgmtObjectEditAnhangGrafikForm");
        context.execute("PF('anfoMgmtObjectEditAnhangGrafikDialog').show()");
    }

    public UploadedFile getAnhang() {
        return anhang;
    }

    public void setAnhang(UploadedFile anhang) {
        this.anhang = anhang;
    }

    public Anhang getSelectedAnhang() {
        return selectedAnhang;
    }

    public void setSelectedAnhang(Anhang selectedAnhang) {
        this.selectedAnhang = selectedAnhang;
    }

    public Anhang getAnhangForBild() {
        return anhangForBild;
    }

    public void setAnhangForBild(Anhang anhangForBild) {
        this.anhangForBild = anhangForBild;
    }

    public Long getOriginalObjectID() {
        return originalObjectId;
    }

    public void setOriginalObjectID(Long originalObjectID) {
        this.originalObjectId = originalObjectID;
    }

    public String getVersionKommentar() {
        return versionKommentar;
    }

    public void setVersionKommentar(String versionKommentar) {
        this.versionKommentar = versionKommentar;
    }

    public String getVsLabel() {
        return vsLabel;
    }

    public void setVsLabel(String vsLabel) {
        this.vsLabel = vsLabel;
    }

    public void setVersionsMenuItems(MenuModel versionsMenuItems) {
        this.versionsMenuItems = versionsMenuItems;
    }

    public TteamConverter getTteamConverter() {
        return new TteamConverter(getAllTTeams());
    }

    public List<Tteam> getAllTTeams() {
        List<Tteam> tteams = getTteamService().getAllTteams();
        tteams.sort(new TteamComparator());
        return tteams;
    }

    public boolean isSensorCoCSelected() {
        return getAnfoMgmtObject().getSensorCoc() != null;
    }

    public void validateSensorCoc(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String summary = "SensorCoc: no message defined";
        if (value instanceof String) {
            summary = (String) value;
        }
        String detail = "----------kein SensorCoC-----------";
        if (getAnfoMgmtObject().getSensorCoc() == null || getAnfoMgmtObject().getSensorCoc().equals(new SensorCoc())) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
        }
    }

    public Converter getMitarbeiterConverter() {
        return new MitarbeiterConverter(uss);
    }

    public String getFormattedDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm");
        return simpleDateFormat.format(date);
    }

    public boolean isBerechtigt() {
        return berechtigt;
    }

    public void setBerechtigt(boolean berechtigt) {
        this.berechtigt = berechtigt;
    }

    public boolean isCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }

    public BerechtigungService getBerechtigungService() {
        return berechtigungService;
    }

    public MailService getMailService() {
        return mailService;
    }

    public FileService getFileService() {
        return fileService;
    }

    public ImageService getImageService() {
        return imageService;
    }

    public TteamService getTteamService() {
        return tteamService;
    }

    public AnforderungMeldungHistoryService getHistoryService() {
        return historyService;
    }

    public void setHistoryService(AnforderungMeldungHistoryService historyService) {
        this.historyService = historyService;
    }

    public UmsetzerController getUmsetzerController() {
        return this.umsetzerController;
    }

    public SensorCocDialogController getSensorCocDialogController() {
        return this.sensorCocDialogController;
    }
}
