package de.interfaceag.bmw.pzbk.controller.dialogs;

import de.interfaceag.bmw.pzbk.dialog.viewdata.DialogUmsetzerDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.ModulDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.ModulKomponenteDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.ModulSeTeamDto;

import javax.faces.convert.Converter;
import java.util.List;
import java.util.Set;

/**
 *
 * @author fn
 */
public interface UmsetzerDialogFields {

    List<ModulDto> getFilteredModule();

    List<ModulSeTeamDto> getFilteredModulSeTeams();

    List<ModulKomponenteDto> getFilteredModulKomponenten();

    Set<DialogUmsetzerDto> getDialogUmsetzerViewDataSet();

    void setDialogUmsetzerViewDataSet(Set<DialogUmsetzerDto> dialogUmsetzerSet);

    void addUmsetzer();

    void removeUmsetzer(DialogUmsetzerDto dialogUmsetzerDto);

    ModulDto getSelectedModul();

    void setSelectedModul(ModulDto selectedModul);

    ModulSeTeamDto getSelectedSeTeam();

    void setSelectedSeTeam(ModulSeTeamDto selectedSeTeam);

    ModulKomponenteDto getSelectedKomponente();

    void setSelectedKomponente(ModulKomponenteDto selectedKomponente);

    boolean isChanged();

    void resetChangedBoolean();

    List<ModulDto> completeModul(String query);

    List<ModulSeTeamDto> completeSeTeam(String query);

    List<ModulKomponenteDto> completeKomponente(String query);

    Converter getModulConverter();

    Converter getModulKomponenteConverter();

    Converter getModulSeTeamConverter();

    void refreshModulSeTeamList();

    void refreshUmsetzerKompList();
}
