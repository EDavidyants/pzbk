package de.interfaceag.bmw.pzbk.controller;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungBasisDataDto;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungHistoryDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.history.ProzessbaukastenHistoryService;
import de.interfaceag.bmw.pzbk.session.Session;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author evda
 */
@ViewScoped
@Named
public class AnforderungHistoryController implements Serializable {

    @Inject
    protected Session session;
    @Inject
    private AnforderungMeldungHistoryService historyService;
    @Inject
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;

    private AnforderungBasisDataDto basisData;

    private List<AnforderungHistoryDto> historyForAnforderung = new ArrayList<>();
    private List<AnforderungHistoryDto> filteredHistory = new ArrayList<>();

    private String benutzerFilter;
    private String attributFilter;
    private String vonFilter;
    private String aufFilter;

    @PostConstruct
    public void init() {
        String idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        String kennzeichen = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("kennzeichen");
        String fachid = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fachId");
        String version = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("version");

        Long id;

        if (kennzeichen != null && kennzeichen.matches("A|M|P") && idstr != null && idstr.matches("\\d+")) {
            id = Long.parseLong(idstr.trim());

            if (kennzeichen.matches("P")) {
                historyForAnforderung = prozessbaukastenHistoryService.getHistoryForProzessbaukastenByFachIdAndVersion(fachid, Integer.parseInt(version));
            } else {
                historyForAnforderung = historyService.getHistoryForAnforderung(id, kennzeichen);
            }
            this.basisData = historyService.getAnforderungBasisInfo(id, kennzeichen);
        }
        session.setLocationForView();
    }

    public List<AnforderungHistoryDto> getHistoryForAnforderung() {
        return historyForAnforderung;
    }

    public void setHistoryForAnforderung(List<AnforderungHistoryDto> historyForAnforderung) {
        this.historyForAnforderung = historyForAnforderung;
    }

    public List<AnforderungHistoryDto> getFilteredHistory() {
        return filteredHistory;
    }

    public void setFilteredHistory(List<AnforderungHistoryDto> filteredHistory) {
        this.filteredHistory = filteredHistory;
    }

    public AnforderungBasisDataDto getBasisData() {
        return basisData;
    }

    public void setBasisData(AnforderungBasisDataDto basisData) {
        this.basisData = basisData;
    }

    public String getVsLabel() {
        if (this.basisData == null) {
            return "";
        }
        String kennzeichen = getBasisData().getFachId().substring(0, 1);
        return ("A".equals(kennzeichen) || "P".equals(kennzeichen)) ? ("V" + getBasisData().getVersion()) : "";
    }

    // Filters
    public String getBenutzerFilter() {
        return benutzerFilter;
    }

    public void setBenutzerFilter(String benutzerFilter) {
        this.benutzerFilter = benutzerFilter;
    }

    public String getAttributFilter() {
        return attributFilter;
    }

    public void setAttributFilter(String attributFilter) {
        this.attributFilter = attributFilter;
    }

    public String getVonFilter() {
        return vonFilter;
    }

    public void setVonFilter(String vonFilter) {
        this.vonFilter = vonFilter;
    }

    public String getAufFilter() {
        return aufFilter;
    }

    public void setAufFilter(String aufFilter) {
        this.aufFilter = aufFilter;
    }

} // end of class
