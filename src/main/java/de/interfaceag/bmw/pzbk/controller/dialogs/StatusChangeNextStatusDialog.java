package de.interfaceag.bmw.pzbk.controller.dialogs;

/**
 *
 * @author fn
 */
public interface StatusChangeNextStatusDialog {

    Integer getSelectedNextStatus();

    void setSelectedNextStatus(Integer selectedNextStatus);

    String getStatusChangeKommentar();

    void setStatusChangeKommentar(String statusChangeKommentar);
}
