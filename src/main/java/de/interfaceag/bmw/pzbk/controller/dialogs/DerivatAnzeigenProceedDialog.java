package de.interfaceag.bmw.pzbk.controller.dialogs;

import de.interfaceag.bmw.pzbk.dialog.viewdata.DerivatenAnzeigenDto;

/**
 *
 * @author fn
 */
public interface DerivatAnzeigenProceedDialog {

    void onAnfoDeriEdit(DerivatenAnzeigenDto derivatenAnzeigenDialogViewData);

    void toggleNachZak(DerivatenAnzeigenDto derivatenAnzeigenDialogViewData);

    String sendNachZak();

    String reloadPage();

}
