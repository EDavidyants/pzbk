package de.interfaceag.bmw.pzbk.controller.dialogs;

import de.interfaceag.bmw.pzbk.converters.ModulConverter;
import de.interfaceag.bmw.pzbk.converters.ModulKomponenteConverter;
import de.interfaceag.bmw.pzbk.converters.ModulSeTeamConverter;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.comparator.ModulComparator;
import de.interfaceag.bmw.pzbk.entities.comparator.ModulKomponenteComparator;
import de.interfaceag.bmw.pzbk.entities.comparator.ModulSeTeamComparator;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.shared.utils.UserInput;
import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * @author Christian Schauer
 */
public class UmsetzerController implements Serializable {

    private Modul selectedModul;
    private ModulSeTeam selectedSe;
    private ModulKomponente selectedKomponente;
    private List<ModulKomponente> allKomponenten;
    private List<ModulSeTeam> allSeTeams;
    private List<Modul> allModule;

    private List<ModulKomponente> filteredListKomponente = new ArrayList<>();
    private List<ModulSeTeam> filteredListSeTeams = new ArrayList<>();
    private List<Modul> filteredListModule = new ArrayList<>();

    private UserInput inputUmsetzerModul = new UserInput();
    private UserInput inputUmsetzerTeam = new UserInput();
    private UserInput inputUmsetzerKomponente = new UserInput();

    private Set<Umsetzer> dialogUmsetzerSet = new HashSet<>();

    private List<AnforderungFreigabeBeiModulSeTeam> freigabeList;

    private final ModulService modulService;

    private boolean changed;

    public UmsetzerController() {
        this.modulService = new ModulService();
        this.freigabeList = new ArrayList<>();
    }

    public UmsetzerController(ModulService modulService, List<AnforderungFreigabeBeiModulSeTeam> freigabeList) {
        this.modulService = modulService;
        this.freigabeList = freigabeList;
        init();
    }

    private void init() {
        this.allKomponenten = modulService.getAllKomponenten();
        this.allSeTeams = modulService.getAllSeTeams();
        this.allModule = modulService.getAllModule();
        this.allModule.sort(new ModulComparator());
        this.allSeTeams.sort(new ModulSeTeamComparator());
        this.allKomponenten.sort(new ModulKomponenteComparator());
        filteredListKomponente = allKomponenten;
        filteredListSeTeams = allSeTeams;
        filteredListModule = allModule;
        inputUmsetzerKomponente.reset();
        inputUmsetzerModul.reset();
        inputUmsetzerTeam.reset();
        selectedKomponente = null;
        selectedModul = null;
        selectedSe = null;
    }

    public void addUmsetzer() {
        Umsetzer umsetzer = getUmsetzer();

        if (umsetzer != null) {

            // check if Umsetzer has Komponente
            boolean hasSelectedUmsetzerKomponenete = hasUmsetzerKomponente(umsetzer);
            if (hasSelectedUmsetzerKomponenete) {
                handleSelectedUmsetzerWithKomponente(umsetzer);

            } else {
                handleSelectedUmsetzerWithoutKomponente(umsetzer);
            }

        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Es wurde kein SE-Team ausgewählt!", "Bitte wählen Sie ein SE-Team oder eine Komponente."));
        }
    }

    public void removeUmsetzer(Umsetzer umsetzer) {
        if (umsetzer != null && dialogUmsetzerSet.contains(umsetzer)) {
            dialogUmsetzerSet.remove(umsetzer);
            changed = Boolean.TRUE;
        }
    }

    private boolean hasUmsetzerKomponente(Umsetzer umsetzer) {
        return umsetzer.getKomponente() != null;
    }

    private Optional<Umsetzer> findUmsetzerWithSameModulSeTeamAndKomponente(Set<Umsetzer> umsetzerPresent, Umsetzer selectedNewUmsetzer) {
        if (umsetzerPresent != null && !umsetzerPresent.isEmpty()) {
            ModulSeTeam modulSeTeam = selectedNewUmsetzer.getSeTeam();

            return umsetzerPresent.stream().filter(u -> u.getSeTeam().equals(modulSeTeam) && u.getKomponente() != null)
                    .findAny();
        }

        return Optional.empty();
    }

    private Optional<Umsetzer> findUmsetzerWithSameModulWithoutKomponente(Set<Umsetzer> umsetzerPresent, Umsetzer selectedNewUmsetzer) {
        if (umsetzerPresent != null && !umsetzerPresent.isEmpty()) {
            ModulSeTeam modulSeTeam = selectedNewUmsetzer.getSeTeam();

            return umsetzerPresent.stream().filter(u -> u.getSeTeam().equals(modulSeTeam) && u.getKomponente() == null)
                    .findAny();
        }

        return Optional.empty();
    }

    private void handleSelectedUmsetzerWithKomponente(Umsetzer umsetzer) {
        Optional<Umsetzer> umsetzerToRemove = findUmsetzerWithSameModulWithoutKomponente(dialogUmsetzerSet, umsetzer);
        if (umsetzerToRemove.isPresent()) {
            removeUmsetzer(umsetzerToRemove.get());
        }

        dialogUmsetzerSet.add(umsetzer);
        changed = Boolean.TRUE;
    }

    private void handleSelectedUmsetzerWithoutKomponente(Umsetzer umsetzer) {
        Optional<Umsetzer> existedUmsetzerMitKomponente = findUmsetzerWithSameModulSeTeamAndKomponente(dialogUmsetzerSet, umsetzer);
        if (!existedUmsetzerMitKomponente.isPresent()) {
            dialogUmsetzerSet.add(umsetzer);
            changed = Boolean.TRUE;
        }
    }

    //======= MODULE ========
    public List<Modul> getUmsetzerModulList() {
        return filteredListModule;
    }

    public Modul getSelectedModul() {
        return selectedModul;
    }

    public void setSelectedModul(Modul selectedModul) {
        if (Modul.areModulesEqual(selectedModul, this.selectedModul)) {
            //No changes
            return;
        }
        this.selectedModul = selectedModul;
        inputUmsetzerKomponente.reset();
        inputUmsetzerTeam.reset();
        refreshUmsetzerTeamList();
    }

    public List<Modul> completeModul(String query) {
        inputUmsetzerModul.setValue(query);
        if (query.isEmpty()) {
            return allModule;
        }
        List<Modul> result = new ArrayList<>();
        Pattern p = Pattern.compile(".*" + query + ".*", Pattern.CASE_INSENSITIVE);
        allModule.stream().filter(modul -> p.matcher(modul.getName()).matches()).forEachOrdered(result::add
        );
        return result;
    }

    //======= SE TEAMS ==========
    public List<ModulSeTeam> getUmsetzerTeamList() {
        return filteredListSeTeams;
    }

    public void refreshUmsetzerTeamList() {
        filteredListSeTeams = completeSeTeam(inputUmsetzerTeam.getValue());
        filteredListSeTeams.sort(new ModulSeTeamComparator());
        setSelectedKomponente(null);
        setSelectedSe(null);
        inputUmsetzerKomponente.reset();
        refreshUmsetzerKompList();
    }

    public ModulSeTeam getSelectedSe() {
        return selectedSe;
    }

    public void setSelectedSe(ModulSeTeam selectedSe) {
        if (ModulSeTeam.areSeTeamsEqual(selectedSe, this.selectedSe)) {
            //No changes
            return;
        }
        this.selectedSe = selectedSe;
        if (selectedSe != null) {
            filteredListSeTeams.remove(selectedSe);
            filteredListSeTeams.add(0, selectedSe);
            selectedModul = selectedSe.getElternModul();
            filteredListModule.remove(selectedModul);
            filteredListModule.add(0, selectedModul);
        }
        inputUmsetzerKomponente.reset();
        refreshUmsetzerKompList();
    }

    public List<ModulSeTeam> completeSeTeam(String query) {
        inputUmsetzerTeam.setValue(query);
        List<ModulSeTeam> result = new ArrayList<>();

        Pattern p = Pattern.compile(".*" + query + ".*", Pattern.CASE_INSENSITIVE);
        if (selectedModul == null) {
            searchInAllSeTeams(query, result, p);
        } else {
            searchSeTeamsForSelectedModul(query, result, p);
        }
        return result;
    }

    private void searchSeTeamsForSelectedModul(String query, List<ModulSeTeam> result, Pattern pattern) {
        allSeTeams.stream().filter(modulSeTeam -> (query.isEmpty() || pattern.matcher(modulSeTeam.getName()).matches()) && modulSeTeam.getElternModul().getName().equals(selectedModul.getName())).forEachOrdered(result::add);
    }

    private void searchInAllSeTeams(String query, List<ModulSeTeam> result, Pattern pattern) {
        allSeTeams.stream().filter(modulSeTeam -> query.isEmpty() || pattern.matcher(modulSeTeam.getName()).matches()).forEachOrdered(result::add);
    }

    //======= KOMPONENTEN =======
    public List<ModulKomponente> getUmsetzerKompList() {
        return filteredListKomponente;
    }

    public void refreshUmsetzerKompList() {
        filteredListKomponente = completeKomponente(inputUmsetzerKomponente.getValue());
        filteredListKomponente.sort(new ModulKomponenteComparator());
        setSelectedKomponente(null);
    }

    public List<ModulKomponente> completeKomponente(String query) {
        inputUmsetzerKomponente.setValue(query);
        List<ModulKomponente> result = new ArrayList<>();
        Pattern p = Pattern.compile(".*" + query + ".*", Pattern.CASE_INSENSITIVE);
        if (this.selectedSe == null && this.selectedModul == null) {
            allKomponenten.stream().filter(modulKomponente -> query.isEmpty() || p.matcher(modulKomponente.getName()).matches() || p.matcher(modulKomponente.getPpg()).matches()).forEachOrdered(result::add
            );
        } else if (this.selectedSe == null) {
            selectedModul.getSeTeams().forEach(team ->
                    team.getKomponenten().stream().filter(modulKomponente -> query.isEmpty() || p.matcher(modulKomponente.getName()).matches() || p.matcher(modulKomponente.getPpg()).matches()).forEachOrdered(result::add)
            );
        } else {
            allKomponenten.stream().filter(modulKomponente -> (query.isEmpty() || p.matcher(modulKomponente.getName()).matches() || p.matcher(modulKomponente.getPpg()).matches()) && modulKomponente.getSeTeam().getName().equals(selectedSe.getName())).forEachOrdered(result::add);
        }
        return result;
    }

    public ModulKomponente getSelectedKomponente() {
        return selectedKomponente;
    }

    public void setSelectedKomponente(ModulKomponente selectedKomponente) {
        this.selectedKomponente = selectedKomponente;

        if (selectedKomponente != null) {
            filteredListKomponente.remove(selectedKomponente);
            filteredListKomponente.add(0, selectedKomponente);
            ModulSeTeam selectedSeTeam = selectedKomponente.getSeTeam();
            filteredListSeTeams.remove(selectedSeTeam);
            filteredListSeTeams.add(0, selectedSeTeam);
            this.selectedSe = selectedSeTeam;
            Modul elternModul = selectedSeTeam.getElternModul();
            filteredListModule.remove(elternModul);
            filteredListModule.add(0, elternModul);
            this.selectedModul = elternModul;
        }
    }

    public Umsetzer getUmsetzer() {
        if (selectedKomponente != null) {
            return new Umsetzer(selectedKomponente.getSeTeam(), selectedKomponente);
        } else if (selectedSe != null) {
            return new Umsetzer(selectedSe);
        }
        return null;
    }

    public Boolean isNotFreigegeben(Umsetzer umsetzer) {
        if (umsetzer != null && umsetzer.getSeTeam() != null && freigabeList != null) {
            return freigabeList.stream().noneMatch(f -> f.getModulSeTeam().equals(umsetzer.getSeTeam()) && f.isFreigabe());
        }
        return true;
    }

    public void resetChangedBoolean() {
        this.changed = Boolean.FALSE;
    }

    public void showUmsetzerDialog() {
        inputUmsetzerKomponente.reset();
        inputUmsetzerModul.reset();
        inputUmsetzerTeam.reset();
        setSelectedKomponente(null);
        setSelectedModul(null);
        setSelectedSe(null);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('umsetzerDialog').show()");
    }

    public Converter getModulConverter() {
        return new ModulConverter(allModule);
    }

    public Converter getModulKomponenteConverter() {
        return new ModulKomponenteConverter(modulService);
    }

    public Converter getModulSeTeamConverter() {
        return new ModulSeTeamConverter(allSeTeams);
    }

    public Set<Umsetzer> getDialogUmsetzerSet() {
        return dialogUmsetzerSet;
    }

    public void setDialogUmsetzerSet(Set<Umsetzer> dialogUmsetzerSet) {
        this.dialogUmsetzerSet = dialogUmsetzerSet;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setFreigabeList(List<AnforderungFreigabeBeiModulSeTeam> freigabeList) {
        this.freigabeList = freigabeList;
    }

}
