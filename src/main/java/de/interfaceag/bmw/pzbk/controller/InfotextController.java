package de.interfaceag.bmw.pzbk.controller;

import de.interfaceag.bmw.pzbk.entities.InfoText;
import de.interfaceag.bmw.pzbk.services.InfoTextService;
import org.primefaces.context.RequestContext;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

/**
 * @author ig
 */
@ViewScoped
@Named
public class InfotextController implements Serializable {

    @EJB
    private InfoTextService infoTextService;

    private static final String DEFAULT = "Es liegen keine weitere Informationen zu diesem Attribut vor.";

    private String infoText = DEFAULT;

    public String getInfoText() {
        return infoText;
    }

    public void setInfoText(String infoText) {
        if (infoText == null) {
            this.infoText = DEFAULT;
        } else {
            this.infoText = infoText;
        }
    }

    public void showInfoTextDialog(String infoToShow) {
        updateInfoText(infoTextService.getInfoTextByFeldName(infoToShow));
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('infoAnzeigenDialog').show()");
    }

    public void showInfoTextDialog() {
        Map<String, String> parameterMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String infoToShow = parameterMap.get("infoToShow");

        updateInfoText(infoTextService.getInfoTextByFeldName(infoToShow));

        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('infoAnzeigenDialog').show()");
    }

    private void updateInfoText(Optional<InfoText> infoText) {
        if (infoText.isPresent()) {
            setInfoText(infoText.get().getBeschreibungsText());
        } else {
            setInfoText(getDefaultMessage());
        }
    }

    private String getDefaultMessage() {
        return DEFAULT;
    }

}
