package de.interfaceag.bmw.pzbk.controller.dialogs;

import org.primefaces.context.RequestContext;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class CommentDialogController implements Serializable {

    private String header;
    private String text;

    public CommentDialogController() {
        header = "";
        text = "";
    }

    public CommentDialogController(String header) {
        this.header = header;
    }

    // ---------- methods ------------------------------------------------------
    public void show(String value) {
        this.text = value;
        show();
    }

    public void show() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialog:displayCommendDialogForm");
        context.execute("PF('displayCommendDialog').show();");
    }

    public void hide() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('displayCommendDialog').hide();");
    }

    // ---------- getter setter ------------------------------------------------
    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
