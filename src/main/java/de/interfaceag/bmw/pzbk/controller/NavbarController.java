package de.interfaceag.bmw.pzbk.controller;

import de.interfaceag.bmw.pzbk.converters.SearchResultDTOConverter;
import de.interfaceag.bmw.pzbk.entities.UserDefinedSearch;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.filter.UserDefinedSearchUrlFilter;
import de.interfaceag.bmw.pzbk.globalsearch.AutocompleteSearchResult;
import de.interfaceag.bmw.pzbk.globalsearch.GlobalSearchService;
import de.interfaceag.bmw.pzbk.globalsearch.SearchResultDTO;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.session.UserDefinedSearchApi;
import de.interfaceag.bmw.pzbk.shared.utils.SearchUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

@Named
@RequestScoped
public class NavbarController implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(NavbarController.class);

    @Inject
    private UserDefinedSearchApi userDefinedSearchApi;
    @Inject
    private Session session;

    @Inject
    private GlobalSearchService globalSearchService;

    //user defined search list of user
    private List<UserDefinedSearch> udss;
    //new user defined search
    private UserDefinedSearch newUds;

    private List<AutocompleteSearchResult> searchQueries; // list of selected queries
    private List<AutocompleteSearchResult> completeSearchResult; // list of last autocomplete result
    private Boolean isAdmin; // true -> user has role admin
    private String searchQuery; // current input

    @PostConstruct
    public void init() {
        isAdmin = session.hasRole(Rolle.ADMIN);
        List<UserDefinedSearch> userDefinedSearchByMitarbeiter = userDefinedSearchApi.getUserDefinedSearchByMitarbeiter();
        if (userDefinedSearchByMitarbeiter != null && !userDefinedSearchByMitarbeiter.isEmpty()) {
            udss = userDefinedSearchByMitarbeiter;
        } else {
            udss = new ArrayList<>();
        }
        newUds = new UserDefinedSearch(session.getUser());
        session.setLocationForView();
    }

    public List<AutocompleteSearchResult> completeSearch(String query) {
        List<AutocompleteSearchResult> result = new ArrayList<>();
        if (!query.isEmpty()) {
            this.searchQuery = query;
            completeSearchResult = globalSearchService.getGlobalSearchAutocompleteResult(query, session.getUser());
            result.addAll(completeSearchResult);
        }
        return result;
    }

    public List<AutocompleteSearchResult> completeAnforderungSearch(String query) {
        List<AutocompleteSearchResult> result = new ArrayList<>();
        if (!query.isEmpty()) {
            this.searchQuery = query;
            completeSearchResult = globalSearchService.getAnforderungAutocompleteSearchResult(query, session.getUser());
            result.addAll(completeSearchResult);
        }
        return result;
    }

    private void checkForCustomSearch() {
        // one element is selected and the autocomplete result was empty
        if (searchQueries != null && searchQueries.size() == 1) {
            AutocompleteSearchResult query = searchQueries.get(0);
            if (query.getId() == null) {
                List<String> queries = SearchUtil.splitQueryWithTextRecognition(query.getName());
                searchQueries.clear();
                queries.forEach(q -> searchQueries.add(new SearchResultDTO(q)));
            }
        }
    }

    public void doSearch() {
        checkForCustomSearch();
    }

    public void doSearchOrGoToResult() throws IOException {
        checkForCustomSearch();
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        if (searchQueries != null && searchQueries.size() == 1 && searchQueries.get(0).getId() != null) {
            AutocompleteSearchResult result = searchQueries.get(0);
            if (result.getName().startsWith("A")) {
                context.redirect(context.getRequestContextPath() + "/anforderungView.xhtml?faces-redirect=true&id=" + result.getId().toString() + "&fachId=" + result.getName() + "&version=" + result.getVersion());
            } else if (result.getName().startsWith("M")) {
                context.redirect(context.getRequestContextPath() + "/meldungView.xhtml?faces-redirect=true&id=" + result.getId().toString());
            } else if (result.getName().startsWith("P")) {
                context.redirect(context.getRequestContextPath() + "/prozessbaukastenView.xhtml?faces-redirect=true&fachId=" + result.getName() + "&version=" + result.getVersion());
            }
        } else {
            context.redirect(context.getRequestContextPath() + "/anforderungList.xhtml" + getSearchURLProperties());
        }
    }

    private String getSearchURLProperties() {
        if (searchQueries == null || searchQueries.isEmpty()) {
            return "?faces-redirect=true";
        } else {
            return buildSearchUrlProperties();
        }
    }

    private String buildSearchUrlProperties() {
        StringBuilder stringBuilder = new StringBuilder();
        searchQueries.forEach(searchQuerie -> {
            if (!searchQuerie.getName().isEmpty()) {
                buildSearchQuerie(stringBuilder, searchQuerie);
            }
        });
        String result = stringBuilder.substring(0, stringBuilder.length() - 1);

        while (result.endsWith(" ")) {
            result = result.substring(0, result.length() - 1);
        }

        try {
            return "?query=" + URLEncoder.encode(result, "UTF-8") + "&faces-redirect=true";
        } catch (UnsupportedEncodingException ex) {
            LOG.error(null, ex);
        }
        return "";
    }

    private void buildSearchQuerie(StringBuilder stringBuilder, AutocompleteSearchResult searchQuerie) {
        stringBuilder.append(searchQuerie.getName());
        if (searchQuerie.getVersion() != null) {
            stringBuilder.append("_v").append(searchQuerie.getVersion());
        }
        if (searchQuerie.getId() != null) {
            stringBuilder.append("_id").append(searchQuerie.getId());
        }
        stringBuilder.append(",");
    }

    public Integer[] querySubstring(String beschreibung) {
        Integer[] result = new Integer[4];
        if (beschreibung != null && searchQuery != null && !beschreibung.isEmpty()) {
            String beschreibungLower = beschreibung.toLowerCase();
            String queryLower = searchQuery.toLowerCase();
            int visiblcharacters = Math.round((float) (50 - queryLower.length()) / 2);

            if (beschreibungLower.contains(queryLower)) {

                Integer queryIndex = beschreibungLower.indexOf(queryLower);
                Integer queryEndIndex = queryIndex + queryLower.length();
                Integer startIndex = Math.max(0, beschreibungLower.indexOf(queryLower) - visiblcharacters);
                Integer endIndex = Math.min(beschreibung.length(), beschreibungLower.indexOf(queryLower) + queryLower.length() + visiblcharacters);
                result[0] = queryIndex;
                result[1] = queryEndIndex;
                result[2] = startIndex;
                result[3] = endIndex;
            }
        }
        return result;
    }

    public String loadUserDefinedSearch(UserDefinedSearch userDefinedSearch) {
        return "anforderungList.xhtml?faces-redirect=true&" + UserDefinedSearchUrlFilter.PARAMETERNAME + "=" + userDefinedSearch.getId();
    }

    public void updateUserDefinedSearch(UserDefinedSearch uds) {
        userDefinedSearchApi.updateUserDefinedSearch(uds);
    }

    public void deleteUserDefinedSearch(UserDefinedSearch uds) {
        userDefinedSearchApi.removeUserDefinedSearch(uds);
        getUdss().remove(uds);
    }

    public void addUserDefinedSearch() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (newUds.getName() != null && newUds.getName().length() != 0) {
            if (userDefinedSearchApi.getUserDefinedSearchByNameAndMitarbeiter(newUds.getName()).isEmpty()) {
                //TODO save searchqueries
                userDefinedSearchApi.saveUserDefinedSearch(newUds);
                udss.add(newUds);
                newUds = new UserDefinedSearch(session.getUser());
            } else {
                context.addMessage("saveSearchError",
                        new FacesMessage(
                                FacesMessage.SEVERITY_ERROR,
                                "Es existiert bereits eine Suche mit diesem Namen.",
                                "")
                );
            }
        } else {
            context.addMessage("saveSearchError",
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR,
                            "Bitte geben Sie einen Namen für die zu speichernde Suche ein.",
                            "")
            );
        }
    }

    public void redirectNeueMeldung() throws IOException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        context.redirect(context.getRequestContextPath() + "/meldungEdit.xhtml");
    }

    public void redirectUmsetzungsbestaetigung() throws IOException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        context.redirect(context.getRequestContextPath() + "/kovaPhasen.xhtml");
    }

    public void redirectAddAnforderung() throws IOException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        context.redirect(context.getRequestContextPath() + "/addAnfoView.xhtml");
    }

    public void redirectZakStatus() throws IOException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        context.redirect(context.getRequestContextPath() + "/zakStatus.xhtml");
    }

    public void redirectFahrzeugprojekt() throws IOException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        context.redirect(context.getRequestContextPath() + "/derivateView.xhtml");
    }


    public SearchResultDTOConverter getSearchResultDTOConverter() {
        return new SearchResultDTOConverter();
    }

    public List<AutocompleteSearchResult> getSearchQueries() {
        return searchQueries;
    }

    public void setSearchQueries(List<AutocompleteSearchResult> searchQueries) {
        this.searchQueries = searchQueries;
    }

    public List<UserDefinedSearch> getUdss() {
        return udss;
    }

    public void setUdss(List<UserDefinedSearch> udss) {
        this.udss = udss;
    }

    public UserDefinedSearch getNewUds() {
        return newUds;
    }

    public void setNewUds(UserDefinedSearch newUds) {
        this.newUds = newUds;
    }
}
