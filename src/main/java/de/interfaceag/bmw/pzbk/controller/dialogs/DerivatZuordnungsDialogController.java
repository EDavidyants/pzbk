package de.interfaceag.bmw.pzbk.controller.dialogs;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class DerivatZuordnungsDialogController implements Serializable {

    private final DerivatService derivatService;
    private final ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;
    private final Anforderung anforderung;
    private final Mitarbeiter user;
    private final Boolean isAdmin;

    private List<Derivat> allDerivate;
    private List<Derivat> derivateChoosable = new ArrayList<>();
    private Map<Derivat, Boolean> canBeRemovedMap = new HashMap<>();
    private List<Derivat> zugeordneteDerivate = new ArrayList<>();
    private List<Derivat> zugeordneteDerivateToRemove = new ArrayList<>();

    public DerivatZuordnungsDialogController(Anforderung anforderung, Mitarbeiter user, Boolean isAdmin, DerivatService derivatService,
            ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService) {
        this.anforderung = anforderung;
        this.derivatService = derivatService;
        this.zuordnungAnforderungDerivatService = zuordnungAnforderungDerivatService;
        this.isAdmin = isAdmin;
        this.user = user;
        this.allDerivate = derivatService.getAllDerivate();
        initZugeordneteDerivate();
        initVerfuegbareDerivate();
    }

    private void initZugeordneteDerivate() {
        this.zugeordneteDerivate = zuordnungAnforderungDerivatService.getDerivateForAnforderung(anforderung);
        zugeordneteDerivate.forEach(d -> canBeRemovedMap.put(d, Boolean.FALSE));
    }

    private void initVerfuegbareDerivate() {
        this.derivateChoosable = new ArrayList<>();
        List<Derivat> alleDerivate;

        if (isAdmin) {
            alleDerivate = derivatService.getAllDerivate();
        } else {
            alleDerivate = derivatService.getAuthorizedDerivateForAnforderer(user);
        }

        List<Derivat> anforderungDerivate = zuordnungAnforderungDerivatService.getDerivateForAnforderung(anforderung);

        this.derivateChoosable = generateListeChoosableDerivate(alleDerivate, anforderungDerivate);

    }

    protected List<Derivat> generateListeChoosableDerivate(List<Derivat> vorhandeneDerivate, List<Derivat> zugeordneteDerivate) {
        List<Derivat> choosableDerivate = new ArrayList<>();

        if (vorhandeneDerivate == null) {
            return choosableDerivate;
        }

        choosableDerivate = vorhandeneDerivate.stream()
                .filter(derivat -> derivat.isVereinbarungActive() && !zugeordneteDerivate.contains(derivat))
                .collect(Collectors.toList());

        return choosableDerivate;
    }

    public void addDerivat(Derivat derivat) {
        derivateChoosable.remove(derivat);
        this.zugeordneteDerivate.add(derivat);
        if (zugeordneteDerivateToRemove.contains(derivat)) {
            zugeordneteDerivateToRemove.remove(derivat);
        }
    }

    public void addToZugeordneteDerivateToRemove(Derivat derivat) {
        if (zugeordneteDerivateToRemove == null) {
            zugeordneteDerivateToRemove = new ArrayList<>();
        }
        if (!zugeordneteDerivateToRemove.contains(derivat)) {
            zugeordneteDerivateToRemove.add(derivat);
        }
        if (!getDerivatChoosable().contains(derivat)) {
            getDerivatChoosable().add(derivat);
        }
        zugeordneteDerivate.remove(derivat);
    }

    public void addDerivateToAnforderung() {
        zugeordneteDerivate.forEach(derivat
                -> zuordnungAnforderungDerivatService.assignAnforderungToDerivat(ZuordnungStatus.ZUGEORDNET, anforderung, derivat,
                        "Die Anforderung " + anforderung.toString() + " wurde dem Derivat " + derivat.getName() + " zugeordnet.", user)
        );
    }

    public void persistChanges() {
        addDerivateToAnforderung();
        initZugeordneteDerivate();
    }

    public void discardChanges() {
        initZugeordneteDerivate();
        initVerfuegbareDerivate();
    }

    public List<Derivat> getDerivatChoosable() {
        return derivateChoosable;
    }

    public void setDerivateChoosable(List<Derivat> derivateChoosable) {
        this.derivateChoosable = derivateChoosable;
    }

    public Boolean canBeRemoved(Derivat derivat) {
        return !canBeRemovedMap.containsKey(derivat);
    }

    public List<Derivat> getAllDerivate() {
        return allDerivate;
    }

    public void setAllDerivate(List<Derivat> allDerivate) {
        this.allDerivate = allDerivate;
    }

    public Map<Derivat, Boolean> getCanBeRemovedMap() {
        return canBeRemovedMap;
    }

    public void setCanBeRemovedMap(Map<Derivat, Boolean> canBeRemovedMap) {
        this.canBeRemovedMap = canBeRemovedMap;
    }

    public List<Derivat> getZugeordneteDerivate() {
        return zugeordneteDerivate;
    }

    public void setZugeordneteDerivate(List<Derivat> zugeordneteDerivate) {
        this.zugeordneteDerivate = zugeordneteDerivate;
    }

    public List<Derivat> getZugeordneteDerivateToRemove() {
        return zugeordneteDerivateToRemove;
    }

    public void setZugeordneteDerivateToRemove(List<Derivat> zugeordneteDerivateToRemove) {
        this.zugeordneteDerivateToRemove = zugeordneteDerivateToRemove;
    }

} // end of class
