package de.interfaceag.bmw.pzbk.controller.dialogs;

/**
 *
 * @author fn
 */
public interface ChangeVersionProceedDialog {

    void proceedWithFullFreigabe();
}
