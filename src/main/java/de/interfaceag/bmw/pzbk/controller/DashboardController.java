package de.interfaceag.bmw.pzbk.controller;

import de.interfaceag.bmw.pzbk.dashboard.DashboardFacade;
import de.interfaceag.bmw.pzbk.dashboard.DashboardResultWithIDs;
import de.interfaceag.bmw.pzbk.dashboard.DashboardViewData;
import de.interfaceag.bmw.pzbk.dashboard.DashboardViewPermission;
import de.interfaceag.bmw.pzbk.enums.DashboardStep;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@RequestScoped
@Named
public class DashboardController implements Serializable {

    private static final String FACES_REDIRECT_TRUE = "&faces-redirect=true";

    @Inject
    private DashboardFacade facade;

    private DashboardViewData viewData;

    @PostConstruct
    public void init() {
        this.viewData = facade.getViewData();
        facade.setLocationForDashboardView();
    }

    public String stepOneClicked() {
        return getStepOneAmount() == 0 ? "" : "anforderungList.xhtml?dashboard=1&faces-redirect=true";
    }

    public String stepTwoClicked() {
        return getStepTwoAmount() == 0 ? "" : "anforderungList.xhtml?dashboard=2&faces-redirect=true";
    }

    public String stepThreeClicked() {
        return getStepThreeAmount() == 0 ? "" : "anforderungList.xhtml?dashboard=3&faces-redirect=true";
    }

    public String stepFourClicked() {
        return getStepFourAmount() == 0 ? "" : "anforderungList.xhtml?dashboard=4&faces-redirect=true";
    }

    public String stepFiveClicked() {
        return getStepFiveAmount() == 0 ? "" : "anforderungList.xhtml?dashboard=5&faces-redirect=true";
    }

    public String stepSixClicked() {
        return getStepSixAmount() == 0 ? "" : "anforderungList.xhtml?dashboard=6&faces-redirect=true";
    }

    public String stepEightClicked() {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("currentStep", "eight");
        return getStepEightAmount() == 0 ? "" : "kovaPhasen.xhtml?kovaStatusFilter=1&faces-redirect=true";
    }

    public String stepNineClicked() {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("currentStep", "nine");
        return getStepNineAmount() == 0 ? "" : "kovaPhasen.xhtml?kovaStatusFilter=2&faces-redirect=true";
    }

    public String stepTenClicked() {
        return getStepTenAmount() == 0 ? "" : "anforderungList.xhtml?dashboard=10&faces-redirect=true";
    }

    public String stepElevenClicked() {
        if (getStepElevenAmount() > 0) {
            DashboardResultWithIDs dashboardResult = viewData.getStepData(DashboardStep.NICHT_UMGESETZTEN_ANFORDERUNGEN);
            StringBuilder stringBuilder = new StringBuilder("addAnfoView.xhtml?daid=");
            dashboardResult.getDerivatAnforderungModulIds().forEach(s -> stringBuilder.append(s.toString()).append(" "));
            stringBuilder.append(FACES_REDIRECT_TRUE);
            return stringBuilder.toString();
        }
        return "";
    }

    public String stepTwelveClicked() {
        if (getStepTwelveAmount() > 0) {
            DashboardResultWithIDs dashboardResult = viewData.getStepData(DashboardStep.ANFORDERUNGEN_OFFEN_IN_ZAK);
            StringBuilder stringBuilder = new StringBuilder("addAnfoView.xhtml?daid=");
            dashboardResult.getDerivatAnforderungModulIds().forEach(s -> stringBuilder.append(s.toString()).append(" "));
            stringBuilder.append(FACES_REDIRECT_TRUE);
            return stringBuilder.toString();
        }
        return "";
    }

    public String stepThirteenClicked() {
        if (getStepThirteenAmount() > 0) {
            DashboardResultWithIDs dashboardResult = viewData.getStepData(DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK);
            StringBuilder stringBuilder = new StringBuilder("zakStatus.xhtml?daid=");
            dashboardResult.getDerivatAnforderungModulIds().forEach(s -> stringBuilder.append(s.toString()).append(","));
            stringBuilder.append(FACES_REDIRECT_TRUE);
            return stringBuilder.toString();
        }
        return "";
    }

    public String stepFourteenClicked() {
        return getStepFourteenAmount() == 0 ? "" : "anforderungList.xhtml?dashboard=14&faces-redirect=true";
    }

    public String clickArbeitsvorratBerechtigungAntrag() {
        return getArbeitsvorratBerechtigungAntrag() == 0 ? "" : "berechtigungAntragVerwaltung.xhtml?faces-redirect=true";
    }

    public int getStepOneAmount() {
        return viewData.getStepCount(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG);
    }

    public int getStepTwoAmount() {
        return viewData.getStepCount(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG);
    }

    public int getStepThreeAmount() {
        return viewData.getStepCount(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT);
    }

    public int getStepFourAmount() {
        return viewData.getStepCount(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN);
    }

    public int getStepFiveAmount() {
        return viewData.getStepCount(DashboardStep.ANFORDERUNGEN_VEREINBART);
    }

    public int getStepSixAmount() {
        return viewData.getStepCount(DashboardStep.ANFORDERUNGEN_UMGESETZT);
    }

    public int getStepEightAmount() {
        return viewData.getStepCount(DashboardStep.BESTAETIGER_ZUORDNEN);
    }

    public int getStepNineAmount() {
        return viewData.getStepCount(DashboardStep.UMSETZUNG_BESTAETIGEN);
    }

    public int getStepTenAmount() {
        return viewData.getStepCount(DashboardStep.ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET);
    }

    public int getStepElevenAmount() {
        return viewData.getStepCount(DashboardStep.NICHT_UMGESETZTEN_ANFORDERUNGEN);
    }

    public int getStepTwelveAmount() {
        return viewData.getStepCount(DashboardStep.ANFORDERUNGEN_OFFEN_IN_ZAK);
    }

    public int getStepThirteenAmount() {
        return viewData.getStepCount(DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK);
    }

    public int getStepFourteenAmount() {
        return viewData.getStepCount(DashboardStep.LANGLAEUFER);
    }

    public int getArbeitsvorratBerechtigungAntrag() {
        return viewData.getStepCount(DashboardStep.BERECHTIGUNG_ANTRAG);
    }

    public String getVersion() {
        return viewData.getVersion();
    }

    public DashboardViewPermission getViewPermission() {
        return viewData.getViewPermission();
    }

    public DashboardViewData getViewData() {
        return viewData;
    }

    public String isAnforderungMenuDisabled() {
        if (!this.viewData.getViewPermission().getNeueMeldungMenuItem()
                && !this.viewData.getViewPermission().getNeueAnforderungMenuItem()
                && !this.viewData.getViewPermission().getReportingAnforderungMenuItem()) {
            return this.getCssStyleForDisabledMenu();
        }

        return "";
    }

    public String isProjekteMenuDisabled() {
        if (!this.viewData.getViewPermission().getProjektverwaltungMenuItem()
                && !this.viewData.getViewPermission().getAnforderungZuordnenMenuItem()
                && !this.viewData.getViewPermission().getAnforderungVereinbarenMenuItem()
                && !this.viewData.getViewPermission().getBerichtswesenMenuItem()
                && !this.viewData.getViewPermission().getUmsetzungsbestaetigungMenuItem()
                && !this.viewData.getViewPermission().getReportingProjektMenuItem()) {
            return this.getCssStyleForDisabledMenu();
        }
        return "";
    }

    public String isProzessbaukastenMenuDisabled() {
        if (!this.viewData.getViewPermission().getNeuerProzessbaukastenMenuItem()) {
            return this.getCssStyleForDisabledMenu();
        }
        return "";
    }

    /**
     * styling for disabled menu-item
     */
    private String getCssStyleForDisabledMenu() {
        return "pointer-events: none; background: rgba(181, 181, 181, 0.3); opacity: 0.4;";
    }
}
