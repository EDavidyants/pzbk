package de.interfaceag.bmw.pzbk.controller.dialogs;

import de.interfaceag.bmw.pzbk.converters.SensorCocConverter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;

import java.util.List;

/**
 *
 * @author fn
 */
public interface SensorCocDialogFields {

    List<SensorCoc> completeSensorCoc(String query);

    SensorCocConverter getSensorCocConverter();

    void refresh();

    // ------------ getter / setter -----------------------------
    void setSensorCocAutoComplete(SensorCoc sensorCoc);

    SensorCoc getSensorCocAutoComplete();

    List<String> getSensorCocRessortOrtungList();

    String getInputSensorCocRessortOrtung();

    void setInputSensorCocRessortOrtung(String inputSensorCocRessortOrtung);

    List<String> getSensorCocTechnologieList();

    String getInputSensorCocTechnologie();

    void setInputSensorCocTechnologie(String inputSensorCocTechnologie);

}
