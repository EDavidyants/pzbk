package de.interfaceag.bmw.pzbk.controller.dialogs;

import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author fn
 */
public interface VersionPlusCreateNewVersionDialog {

    String createNewVersion() throws IllegalAccessException, InvocationTargetException;
}
