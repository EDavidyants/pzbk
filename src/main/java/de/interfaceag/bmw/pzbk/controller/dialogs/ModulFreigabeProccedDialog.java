package de.interfaceag.bmw.pzbk.controller.dialogs;

import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;

/**
 *
 * @author fn
 */
public interface ModulFreigabeProccedDialog {

    String proceedWithModulFreigabe(AnforderungFreigabeBeiModulSeTeam anfoModul);
}
