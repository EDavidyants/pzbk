package de.interfaceag.bmw.pzbk.controller.dialogs;

/**
 *
 * @author fn
 */
public interface DerivatZuordnenDialogPersistDerivat {

    String persistDerivatChanges();
    
    String discardChanges();

}
