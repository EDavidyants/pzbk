package de.interfaceag.bmw.pzbk.controller;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungViewPermission;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.DetektorDto;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.validator.EmailValidator;
import de.interfaceag.bmw.pzbk.converters.FestgestelltInConverter;
import de.interfaceag.bmw.pzbk.converters.WerkConverter;
import de.interfaceag.bmw.pzbk.entities.AbstractAnforderung;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Auswirkung;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Werk;
import de.interfaceag.bmw.pzbk.entities.Wert;
import de.interfaceag.bmw.pzbk.enums.Attribut;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.services.AnforderungFreigabeService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.WerkService;
import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 *
 * @author fp
 */
public abstract class AbstractAnforderungController extends AbstractAnfoMgmtObjectController implements Serializable {

    @Inject
    AnforderungService anforderungService;
    @Inject
    AnforderungFreigabeService anforderungFreigabeService;
    @Inject
    DerivatService derivatService;
    @Inject
    SensorCocService scocService;
    @Inject
    private ConfigService configService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    WerkService werkService;
    @Inject
    LocalizationService localizationService;

    boolean kommentarRequired;
    boolean requiredByMelden;
    private List<FestgestelltIn> allFestgestelltIn = new ArrayList<>();
    private List<Werk> allWerk = new ArrayList<>();

    private List<Rolle> userRollen;

    private List<String> auswirkungsArten = new ArrayList<>();
    Auswirkung auswirkung;

    private String newDetektor;
    private List<DetektorDto> detektoren;

    public boolean isMyMeldung() {
        if (!session.hasRole(Rolle.ADMIN)) {
            List<Long> sensorcocList = getBerechtigungService().getAuthorizedSensorCocListForMitarbeiter(session.getUser());
            if (sensorcocList != null && !sensorcocList.isEmpty()) {
                return getAnforderungService().isMyMeldungByIdAndSensorCocList(originalObjectId, sensorcocList);
            }
        }
        return true;
    }

    public boolean isMyMeldung(String fachId) {
        Mitarbeiter currentUser = session.getUser();
        if (!session.hasRole(Rolle.ADMIN)) {
            List<Long> sensorcocList = getBerechtigungService().getAuthorizedSensorCocListForMitarbeiter(currentUser);
            if (sensorcocList != null && !sensorcocList.isEmpty()) {
                return getAnforderungService().isMyMeldungByFachIdAndSensorCocList(fachId, sensorcocList);
            }
        }
        return true;
    }

    @Override
    public abstract AbstractAnforderung getAnfoMgmtObject();

    public abstract boolean isAfoMgmtObjectInstanceOfAnforderung();

    public boolean toBeOpenedInEditMode() {
        return getAnfoMgmtObject().isStatusChangeRequested()
                && (getAnfoMgmtObject().getNextStatus() == Status.M_GEMELDET
                || getAnfoMgmtObject().getNextStatus() == Status.M_ZUGEORDNET);
    }

    public void proceedWithStatusChange() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('statusChangeConfirmDialog').show()");
    }

    public boolean openAnforderungInEditMode(Status newStatus) {
        if (newStatus.equals(Status.M_GELOESCHT) || newStatus.equals(Status.M_UNSTIMMIG)) {
            return false;
        }

        // SensorCoc and BeschreibungDe are always required
        if (getAnfoMgmtObject().getBeschreibungAnforderungDe() == null
                || getAnfoMgmtObject().getBeschreibungAnforderungDe().equals("")
                || getAnfoMgmtObject().getSensorCoc() == null) {
            return true;
        }

        return newStatus == Status.M_GEMELDET || newStatus == Status.M_ZUGEORDNET
                || newStatus == Status.A_INARBEIT;
    }

    public abstract String processGeloeschtStatusChange();

    @Override
    public String processStatusChange(Status newStatus) {
        if (newStatus != null) {
            setSelectedNextStatus(newStatus.getStatusId());

            if (openAnforderungInEditMode(newStatus)) {
                return changeStatus(newStatus.ordinal());
            } else {
                setStatusChangeKommentar("");
                RequestContext context = RequestContext.getCurrentInstance();
                context.update("dialog:statusChangeForm");
                context.execute("PF('statusChangeDialog').show()");
                return "";
            }
        }
        return "";
    }

    public String updateAnhaengeOnMeldungZuordnungDialogHide() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('meldungenZuordnenDialog').hide()");
        context.update("viewAfo:anfoOverviewForm:anhaengePanel");
        return "";
    }

    public List<FestgestelltIn> completeFestgestelltIn(String query) {
        if (allFestgestelltIn.isEmpty()) {
            allFestgestelltIn = anforderungService.getAllFestgestelltIn();
        }

        Pattern pattern = Pattern.compile(".*" + query + ".*", Pattern.CASE_INSENSITIVE);

        AbstractAnforderung abstractAnforderung = getAnfoMgmtObject();
        List<FestgestelltIn> festgestelltIn = abstractAnforderung.getFestgestelltInList();

        List<FestgestelltIn> result = new ArrayList<>();
        allFestgestelltIn.forEach((festgestelltInListElelement) -> {
            if (festgestelltIn == null) {
                if (pattern.matcher(festgestelltInListElelement.getWert()).matches()) {
                    result.add(festgestelltInListElelement);
                }
            } else {
                if (pattern.matcher(festgestelltInListElelement.getWert()).matches() && !festgestelltIn.contains(festgestelltInListElelement)) {
                    result.add(festgestelltInListElelement);
                }
            }
        });

        return result;
    }

    public Converter getFestgestelltInConverter() {
        return new FestgestelltInConverter(anforderungService.getAllFestgestelltIn());
    }

    public Converter getWerkConverter() {
        return new WerkConverter(werkService.getAllWerk());
    }

    public void addAuswirkung() {
        if (this.auswirkung != null && this.auswirkung.getWert() != null && this.auswirkung.getAuswirkung() != null
                && !this.auswirkung.getWert().isEmpty() && !this.auswirkung.getAuswirkung().isEmpty()) {
            getAnfoMgmtObject().addAuswirkung(this.auswirkung);
            this.auswirkung = new Auswirkung();
        }
    }

    public boolean isAuswirkungAssigned() {
        return getAnfoMgmtObject().getAuswirkungen().size() > 0;
    }

    public void validateAuswirkungen(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String summary = "Auswirkungen: no message defined";
        if (value instanceof String) {
            summary = (String) value;
        }
        String detail = "-----------Auswirkungen leer----------";
        boolean req;
        Integer curst = 0;
        if (getAnfoMgmtObject() != null && getAnfoMgmtObject().getStatus() != null) {
            curst = getAnfoMgmtObject().getStatus().ordinal();
        }
        Status s = getAnfoMgmtObject().getNextStatus();
        boolean requ = getAnfoMgmtObject().isStatusChangeRequested();

        if (s == Status.M_GEMELDET && requ || getAnfoMgmtObject().getStatus() == Status.M_GEMELDET
                || curst >= 4 && curst < 10) {
            req = true;
        } else {
            req = false;
        }

        if (req && (getAnfoMgmtObject().getAuswirkungen() == null || getAnfoMgmtObject().getAuswirkungen().isEmpty())) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
        }
    }

    public void initAuswirkungsarten() {
        List<Wert> tmp = configService.getAllAuswirkungen();
        auswirkungsArten = new ArrayList<>();
        tmp.forEach((wert) -> {
            auswirkungsArten.add(wert.getWert());
        });
    }

    public List<String> getAuswirkungsArten() {
        return auswirkungsArten;
    }

    public Auswirkung getAuswirkung() {
        return auswirkung;
    }

    public void setAuswirkung(Auswirkung auswirkung) {
        this.auswirkung = auswirkung;
    }

    public List<String> getPrioBIs() {
        List<Wert> tmp = configService.getWertByAttribut(Attribut.PRIOBI);
        List<String> pbis = new ArrayList<>();
        tmp.forEach((wert) -> {
            pbis.add(wert.getWert());
        });
        return pbis;
    }

    public boolean hasRightToSetTteam() {
        boolean statusIsAbgestimmt = false;
        if (this.getAnforderungService().getAnforderungById(originalObjectId) != null) {
            statusIsAbgestimmt = this.getAnforderungService().getAnforderungById(originalObjectId).getStatus().equals(Status.A_FTABGESTIMMT);
        }
        return userRollen.contains(Rolle.SENSORCOCLEITER) || userRollen.contains(Rolle.SCL_VERTRETER)
                || userRollen.contains(Rolle.ADMIN) || userRollen.contains(Rolle.T_TEAMLEITER) || userRollen.contains(Rolle.TTEAM_VERTRETER) && statusIsAbgestimmt;
    }

    public boolean isRequiredByMelden() {
        return requiredByMelden;
    }

    public void setRequiredByMelden(boolean requiredByMelden) {
        this.requiredByMelden = requiredByMelden;
    }

    public boolean isKommentarRequired() {
        return kommentarRequired;
    }

    public void setKommentarRequired(boolean kommentarRequired) {
        this.kommentarRequired = kommentarRequired;
    }

    public AnforderungService getAnforderungService() {
        return anforderungService;
    }

    public AnforderungFreigabeService getAnforderungFreigabeService() {
        return anforderungFreigabeService;
    }

    public void setAnforderungService(AnforderungService anforderungService) {
        this.anforderungService = anforderungService;
    }

    public DerivatService getDerivatService() {
        return derivatService;
    }

    public void setDerivatService(DerivatService derivatService) {
        this.derivatService = derivatService;
    }

    public SensorCocService getScocService() {
        return scocService;
    }

    public void setScocService(SensorCocService scocService) {
        this.scocService = scocService;
    }

    public List<FestgestelltIn> getAllFestgestelltIn() {
        return allFestgestelltIn;
    }

    public void setAllFestgestelltIn(List<FestgestelltIn> allFestgestelltIn) {
        this.allFestgestelltIn = allFestgestelltIn;
    }

    public List<Rolle> getUserRollen() {
        return userRollen;
    }

    public void setUserRollen(List<Rolle> userRollen) {
        this.userRollen = userRollen;
    }

    public List<Werk> getAllWerk() {
        return allWerk;
    }

    public void setAllWerk(List<Werk> allWerk) {
        this.allWerk = allWerk;
    }

    public boolean getDerivatPanelRenderCondition() {
        return getAnfoMgmtObject() != null && getAnfoMgmtObject().getStatus() != null
                ? getAnfoMgmtObject().getStatus().equals(Status.A_FREIGEGEBEN)
                || getAnfoMgmtObject().getStatus().equals(Status.A_KEINE_WEITERVERFOLG)
                || getAnfoMgmtObject().getStatus().equals(Status.A_GELOESCHT)
                : Boolean.FALSE;
    }

    public abstract AnforderungViewPermission getViewPermission();

    public boolean isAnforderungFreigabeBeiModulRendered() {
        return getAnfoMgmtObject() instanceof Anforderung;
    }

    public List<AnforderungFreigabeBeiModulSeTeam> getAnforderungFreigabeBeiModul() {
        return getAnfoMgmtObject() instanceof Anforderung ? ((Anforderung) getAnfoMgmtObject()).getAnfoFreigabeBeiModul() : new ArrayList<>();
    }

    public void setAnforderungFreigabeBeiModul(List<AnforderungFreigabeBeiModulSeTeam> freigaben) {

        // do nothing
    }

    public String getNewDetektor() {
        return newDetektor;
    }

    public void setNewDetektor(String newDetektor) {
        this.newDetektor = newDetektor;
    }

    public String addNewDetektor() {

        boolean isValid = EmailValidator.validate(newDetektor);
        if (!isValid) {
            String message = localizationService.getValue("anforderung_Controller_email_Validation_Message");
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    message, ""));
            context.validationFailed();
            return "";
        } else {

            if (this.newDetektor != null && !this.newDetektor.isEmpty()) {
                getDetektoren().add(new DetektorDto(newDetektor));
                this.newDetektor = "";
            }
            return "";
        }

    }

    public void initDetektoren(List<String> values) {
        if (values != null) {
            setDetektoren(values.stream().distinct().map(value -> new DetektorDto(value)).collect(Collectors.toList()));
        } else {
            setDetektoren(new ArrayList<>());
        }
    }

    public List<String> getDetektorenValues() {
        if (detektoren != null) {
            return detektoren.stream().map(detektor -> detektor.getValue()).distinct().collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    public List<DetektorDto> getDetektoren() {
        return detektoren;
    }

    public void setDetektoren(List<DetektorDto> detektoren) {
        this.detektoren = detektoren;
    }

    public boolean isAnforderungZuordnungErlaubt() {
        AbstractAnforderung abstractAnforderung = this.getAnfoMgmtObject();
        if (abstractAnforderung == null) {
            return false;
        }

        if (!(abstractAnforderung instanceof Anforderung)) {
            return false;
        }

        Anforderung anforderung = (Anforderung) abstractAnforderung;

        Status status = anforderung.getStatus();
        if (status == Status.A_GELOESCHT || status == Status.A_KEINE_WEITERVERFOLG) {
            return false;
        }

        return !anforderung.isProzessbaukastenZugeordnet();
    }

}
