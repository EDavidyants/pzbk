package de.interfaceag.bmw.pzbk.controller.dialogs;

import org.primefaces.context.RequestContext;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 *
 */
@Named
@RequestScoped
public class CopyLinkDialogController implements Serializable {

    private static final String DIALOGNAME = "copyLinkDialog";
    private static final String FORMNAME = "dialog:copyLinkDialogForm";

    private String header = "Link kopieren";

    private String message;

    public void show(String message) {
        this.message = message;
        RequestContext context = RequestContext.getCurrentInstance();
        context.update(FORMNAME);
        context.execute("PF('" + DIALOGNAME + "').show();");
    }

    public void showGrowl() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Link wurde in das Clipboard kopiert", ""));
    }

    public String getHeader() {
        return header;
    }

    public String getMessage() {
        return message;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
