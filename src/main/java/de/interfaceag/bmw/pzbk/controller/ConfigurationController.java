package de.interfaceag.bmw.pzbk.controller;

import de.interfaceag.bmw.pzbk.admin.terminal.AdminTerminalFacade;
import de.interfaceag.bmw.pzbk.doors.migration.DoorsAnforderungMigrationService;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.InfoText;
import de.interfaceag.bmw.pzbk.entities.Wert;
import de.interfaceag.bmw.pzbk.enums.Attribut;
import de.interfaceag.bmw.pzbk.enums.ExcelSheetForTechnikalIssus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import de.interfaceag.bmw.pzbk.modulorga.ModulOrgaService;
import de.interfaceag.bmw.pzbk.reporting.config.ReportingConfigService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.migration.ReportingStatusTransitionMigrationAdapter;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.FileExportService;
import de.interfaceag.bmw.pzbk.services.InfoTextService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;
import de.interfaceag.bmw.pzbk.zak.ZakPostService;
import de.interfaceag.bmw.pzbk.zak.ZakUpdateService;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author ig
 */
@ViewScoped
@Named
public class ConfigurationController implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationController.class);
    public static final String PF_EXCEL_UPLOAD_RESULT_DLG_SHOW = "PF('excelUploadResultDlg').show();";

    @Inject
    private Session session;

    @Inject
    private AnforderungService anforderungService;
    @Inject
    private ConfigService configService;
    @Inject
    private InfoTextService infoTextService;
    @Inject
    private ZakPostService zakPostService;
    @Inject
    private ZakUpdateService zakUpdateService;
    @Inject
    private ExcelDownloadService excelDownloadService;
    @Inject
    private DoorsAnforderungMigrationService doorsAnforderungMigrationService;
    @Inject
    private ReportingStatusTransitionMigrationAdapter reportingStatusTransitionMigrationAdapter;
    @Inject
    private ReportingConfigService reportingConfigService;

    @Inject
    private AdminTerminalFacade adminTerminal;
    @Inject
    private FileExportService fileExportService;

    @Inject
    private ModulOrgaService modulOrgService;

    private List<SelectItem> rollen;
    private List<Attribut> attribute;
    private Attribut attribut;
    private List<Wert> werte;
    private List<InfoText> infoTexte;
    private String beschreibungsText;
    private Long editedId;

    private List<FestgestelltIn> festgestelltIn;
    private final Set<FestgestelltIn> festgestelltInToRemove = new HashSet<>();
    private final Set<FestgestelltIn> festgestelltInToSave = new HashSet<>();

    private List<String> uploadSheetsDone;
    private List<String> uploadSheetsFailures;

    private Workbook result;

    private Boolean testMode = false;

    private String zakUpdateId;

    private String langlauferTteam;

    private String langlauferFachteam;

    private Date reportingStartDate;

    @PostConstruct
    public void init() {
        Map<String, String> requestParameters = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (requestParameters.get("test") != null) {
            String test = requestParameters.get("test");
            if ("true".equals(test)) {
                testMode = true;
            }
        }

        attribute = configService.getAllAttribute();
        infoTexte = infoTextService.getAllInfoTexte();
        festgestelltIn = anforderungService.getAllFestgestelltIn();

        langlauferTteam = String.valueOf(reportingConfigService.getLanglauferTteamThreshold());
        langlauferFachteam = String.valueOf(reportingConfigService.getLanglauferFachteamThreshold());

        session.setLocationForView();

    }

    public void setLanglauferTteam(String langlauferTteam) {
        this.langlauferTteam = langlauferTteam;
    }

    public void setLanglauferFachteam(String langlauferFachteam) {
        this.langlauferFachteam = langlauferFachteam;
    }

    public void downloadFileSystemExport() {
        result = fileExportService.generateFileSystemExport();
        RequestContext.getCurrentInstance().execute(PF_EXCEL_UPLOAD_RESULT_DLG_SHOW);
    }

    public void testZakGet() {
        zakUpdateService.getZakUpdate();
    }

    public void testZakPost() {
        zakPostService.sendQueuedZakUebertragungen();
    }

    public void linkAnforderungenWithoutMeldungenWithMatchingMeldung() {
        configService.linkAnforderungenWithoutMeldungenWithMatchingMeldung();
    }

    public void updateZeitpunktStatusaenderung() {
        configService.updateLastStatusChangeDateFromHistory();
    }

    public void updateLatestDerivatAnforderungModulHistory() {
        configService.updateLatestDerivatAnforderungModulHistory();
    }

    public void migrateAnforderungen() {
        reportingStatusTransitionMigrationAdapter.migrateAnforderungen();
    }

    public void migrateMeldungen() {
        reportingStatusTransitionMigrationAdapter.migrateMeldungen();
    }

    public String handleCommand(String command, String[] params) {
        return adminTerminal.handleCommand(command, params);
    }

    public boolean isAdmin() {
        return session.hasRole(Rolle.ADMIN);
    }

    public void removeWert(Wert wert) {
        werte.remove(wert);
        configService.removeWert(wert);
    }

    public void persistWert(Wert wert) {
        configService.persistWert(wert);
    }

    public void persistWerte() {
        werte.forEach(wert ->
                configService.persistWert(wert)
        );
    }

    public void persistFestgestelltIn() {
        StringBuilder removeResultStringBuilder = new StringBuilder();
        festgestelltInToRemove.forEach(fi -> removeResultStringBuilder
                .append(anforderungService.tryToRemoveFestgestelltIn(fi))
                .append("\n"));
        festgestelltInToSave.stream()
                .filter(fi -> !fi.getWert().isEmpty())
                .forEach(fi -> anforderungService.persistFestgestelltIn(fi));
        if (!festgestelltInToRemove.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "FestgestelltIn Löschen", removeResultStringBuilder.toString()));
        }
        if (!festgestelltInToSave.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "FestgestelltIn Speicher",
                            Integer.toString(festgestelltInToSave.size())
                                    + " FestgestelltIn Werte gespeichert."));
        }
        festgestelltInToRemove.clear();
        festgestelltInToSave.clear();
    }

    public void dropdownChangeListener(Attribut attribut) {
        if (attribut != null) {
            this.setAttribut(attribut);
            this.setWerte(configService.getWertByAttribut(attribut));
        }
    }

    public void addWert(Attribut attribut) {
        Wert w = new Wert();
        w.setAttribut(attribut);
        werte.add(w);
    }

    public void addFestgestlltIn() {
        festgestelltIn.add(new FestgestelltIn());
    }

    public void addFestgestelltInToSaveList(FestgestelltIn fi) {
        festgestelltInToSave.add(fi);
    }

    public void removeFestgestelltIn(FestgestelltIn fi) {
        festgestelltInToRemove.add(fi);
        festgestelltIn.remove(fi);
    }

    //Getters and Setters
    public List<SelectItem> getRollen() {
        return rollen;
    }

    public void setRollen(List<SelectItem> rollen) {
        this.rollen = rollen;
    }

    public List<Attribut> getAttribute() {
        return attribute;
    }

    public void setAttribute(List<Attribut> attribute) {
        this.attribute = attribute;
    }

    public Attribut getAttribut() {
        return attribut;
    }

    public void setAttribut(Attribut attribut) {
        if (attribut != null) {
            this.attribut = attribut;
            setWerte(configService.getWertByAttribut(attribut));
        }
    }

    public List<Wert> getWerte() {
        return werte;
    }

    public void setWerte(List<Wert> werte) {
        this.werte = werte;
    }

    public List<InfoText> getInfoTexte() {
        return infoTexte;
    }

    public void setInfoTexte(List<InfoText> infoTexte) {
        this.infoTexte = infoTexte;
    }

    public String getBeschreibungsText() {
        return beschreibungsText;
    }

    public void setBeschreibungsText(String beschreibungsText) {
        this.beschreibungsText = beschreibungsText;
    }

    public boolean existSucessfulUploads() {
        if (uploadSheetsDone == null) {
            return false;
        }
        return !this.uploadSheetsDone.isEmpty();
    }

    public boolean existFailedUploads() {
        if (uploadSheetsFailures == null) {
            return false;
        }
        return !this.uploadSheetsFailures.isEmpty();
    }

    public void uploadExcelSheet(FileUploadEvent event) {
        UploadedFile excelFile = event.getFile();
        if (excelFile != null) {
            this.uploadSheetsDone = new ArrayList<>();
            this.uploadSheetsFailures = new ArrayList<>();

            if (excelFile.getContentType().equals("application/x-zip-compressed")) {
                migrateZipFile(excelFile);
            } else {
                importExcelSheet(excelFile);
            }

            LOG.info("... open dialog");
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('excelUploadDlg').hide();");
            context.execute(PF_EXCEL_UPLOAD_RESULT_DLG_SHOW);
        }
    }

    private void importExcelSheet(UploadedFile excelFile) {
        Workbook resultWorkbook = new HSSFWorkbook();
        try (InputStream inputstream = excelFile.getInputstream()) {
            try (Workbook workBook = WorkbookFactory.create(inputstream)) {
                Sheet sensorCocTteamSheet = workBook.getSheet(ExcelSheetForTechnikalIssus.SENSORCOCTTEAM.getBezeichnung());
                configService.setSensorCocTteamSheet(sensorCocTteamSheet);

                resultWorkbook = importSheets(workBook, resultWorkbook);
            } catch (IOException ex) {
                LOG.error(null, ex);
            }
        } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
            LOG.error(ex.getMessage());
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            resultWorkbook.write(baos);
        } catch (IOException ex) {
            LOG.error(null, ex);
        }
        result = resultWorkbook;
    }

    private Workbook importSheets(final Workbook workBook, Workbook resultWorkbook) {
        Sheet currentSheet;
        Iterator<Sheet> iter = workBook.iterator();
        while (iter.hasNext()) {
            currentSheet = iter.next();
            if (currentSheet != null) {
                resultWorkbook = importSheet(currentSheet, resultWorkbook);
            }
        }
        return resultWorkbook;
    }

    private Workbook importSheet(Sheet currentSheet, Workbook resultWorkbook) {
        String sname = currentSheet.getSheetName();
        Tuple<Boolean, Workbook> sheetResult = configService.insertFromExcel(currentSheet, resultWorkbook);
        if (sheetResult.getX()) {
            uploadSheetsDone.add(sname);
        } else {
            uploadSheetsFailures.add(sname);
        }
        resultWorkbook = sheetResult.getY();
        return resultWorkbook;
    }

    private void migrateZipFile(UploadedFile excelFile) {
        try (InputStream inputstream = excelFile.getInputstream()) {
            configService.migrateZipFile(inputstream);
        } catch (IOException ex) {
            LOG.error(null, ex);
        }
    }


    public void uploadAnhaenge(FileUploadEvent event) {
        UploadedFile anhaenge = event.getFile();
        if (anhaenge != null) {

            LOG.info("Start Upload Anhaenge");

            try {
                configService.migrateAnhaenge(anhaenge.getInputstream());

            } catch (IOException ex) {
                LOG.error(null, ex);
            }

            LOG.info("Upload Anhaenge finished");
            LOG.info("Show dialog ...");
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('excelUploadDlg').hide();");
            context.execute(PF_EXCEL_UPLOAD_RESULT_DLG_SHOW);
        } else {
            LOG.warn("Anhaenge is null");
        }
    }

    public void uploadModulOrgaSheets(FileUploadEvent event) {
        UploadedFile excelFile = event.getFile();
        Workbook handleModulOrgaUploadedFile = modulOrgService.handleModulOrgaUploadedFile(excelFile);
        result = handleModulOrgaUploadedFile;
    }

    public void getExampleWithExistingData() {
        Workbook workbook = configService.getExampleWithExistingData();
        excelDownloadService.downloadExcelExport("upload_example_existing_data", workbook);
    }

    public void getResult() {
        excelDownloadService.downloadExcelExport("result", result);
    }

    public void showInfoBearbeitenDialog(String feldName, Long id) {
        editedId = id;
        infoTextService.getInfoTextByFeldName(feldName)
                .ifPresent(i -> beschreibungsText = i.getBeschreibungsText());
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialog:infoBearbeitenForm:infoBearbeitenDialog");
        context.execute("PF('infoBearbeitenDialog').show()");
    }

    public void onTextEdit(String neuerBeschreibungsText) {
        InfoText editedText = infoTextService.getInfoTextById(editedId);
        editedText.setBeschreibungsText(neuerBeschreibungsText);
        infoTextService.persistInfoText(editedText);
        infoTexte = infoTextService.getAllInfoTexte();
    }

    public List<String> getUploadSheetsDone() {
        return uploadSheetsDone;
    }

    public List<String> getUploadSheetsFailures() {
        return uploadSheetsFailures;
    }

    public List<FestgestelltIn> getFestgestelltIn() {
        return festgestelltIn;
    }

    public void setFestgestelltIn(List<FestgestelltIn> festgestelltIn) {
        this.festgestelltIn = festgestelltIn;
    }

    public Boolean getTestMode() {
        return testMode;
    }

    public StreamedContent downloadZakUpdate() {

        byte[] src = zakUpdateService.getZakResponseForZakUpdateId(zakUpdateId);

        if (src != null) {
            ByteArrayInputStream byteStream = new ByteArrayInputStream(src);
            return new DefaultStreamedContent(byteStream, "application/zip", "zakUpdate.zip");
        }

        LOG.info("zakResponse for {} is null!", zakUpdateId);
        return null;
    }

    public String getZakUpdateId() {
        return zakUpdateId;
    }

    public void setZakUpdateId(String zakUpdateId) {
        this.zakUpdateId = zakUpdateId;
    }

    public void setReportingStartDate(Date defaultReportingStartDate) {
        this.reportingStartDate = defaultReportingStartDate;
    }

    public void persistReportingStartDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        reportingConfigService.persistReportingConfigValue(formatter.format(this.reportingStartDate));
    }

    public Date getReportingStartDate() {
        return reportingConfigService.getStartDateValue();
    }

    public String getLanglauferTteam() {
        return String.valueOf(reportingConfigService.getLanglauferTteamThreshold());
    }

    public void persistLanglauferTteam() {
        reportingConfigService.persistLanglauferTteam(langlauferTteam);
    }

    public String getLanglauferFachteam() {
        return String.valueOf(reportingConfigService.getLanglauferFachteamThreshold());
    }

    public void persistLanglauferFachteam() {
        reportingConfigService.persistLanglauferFachteam(langlauferFachteam);
    }
}
