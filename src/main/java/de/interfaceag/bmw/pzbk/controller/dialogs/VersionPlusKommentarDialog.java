package de.interfaceag.bmw.pzbk.controller.dialogs;

/**
 *
 * @author fn
 */
public interface VersionPlusKommentarDialog {

    String getVersionKommentar();

    void setVersionKommentar(String versionKommentar);
}
