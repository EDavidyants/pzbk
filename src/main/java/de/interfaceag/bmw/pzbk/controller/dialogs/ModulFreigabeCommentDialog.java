package de.interfaceag.bmw.pzbk.controller.dialogs;

import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;

/**
 *
 * @author fn
 */
public interface ModulFreigabeCommentDialog {

    String getModulFreigabeKommentar();

    void setModulFreigabeKommentar(String modulFreigabeKommentar);

    boolean isKommentarRequired();

    void setKommentarRequired(boolean kommentarRequired);

    AnforderungFreigabeBeiModulSeTeam getAnfoModulZumFreigeben();

    void setAnfoModulZumFreigeben(AnforderungFreigabeBeiModulSeTeam anfoModulZumFreigeben);
}
