package de.interfaceag.bmw.pzbk.controller.dialogs;

import de.interfaceag.bmw.pzbk.dialog.viewdata.DerivatenAnzeigenDto;

import java.util.List;

/**
 *
 * @author fn
 */
public interface DerivatAnzeigenDialogDerivatForAnforderungList {

    List<DerivatenAnzeigenDto> getZuordnungAnforderungenDerivatList();

    void setZuordnungAnforderungenDerivatList(List<DerivatenAnzeigenDto> zuordnungAnforderungDerivatList);

    List<Long> getAnfoNachZakIDList();

    void setAnfoNachZakList(List<Long> anfoNachZakIDList);
}
