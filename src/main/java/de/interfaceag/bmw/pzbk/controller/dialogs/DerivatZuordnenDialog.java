package de.interfaceag.bmw.pzbk.controller.dialogs;

import de.interfaceag.bmw.pzbk.dialog.viewdata.DerivatZuordnenDto;

import java.util.List;

/**
 *
 * @author fn
 */
public interface DerivatZuordnenDialog {

    List<DerivatZuordnenDto> getDerivateChoosable();

    List<DerivatZuordnenDto> getZugeordneteDerivate();

    void setDerivateChoosable(List<DerivatZuordnenDto> derivateChoosable);

    void setZugeordneteDerivate(List<DerivatZuordnenDto> zugeordneteDerivate);

    void addDerivat(DerivatZuordnenDto zugeordneteDerivate);

    void removeDerivat(DerivatZuordnenDto zugeordneteDerivate);

    Boolean canBeRemoved(DerivatZuordnenDto zugeordneteDerivate);
}
