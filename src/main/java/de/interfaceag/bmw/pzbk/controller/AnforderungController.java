package de.interfaceag.bmw.pzbk.controller;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungBeschreibungChangedValidator;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungInlineEditViewPermission;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMitMeldungFacade;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMitMeldungViewData;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungModulChangedValidator;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungStatusChangeService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungTitelbildValidator;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.StatusUtils;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungEditViewPermission;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungViewPermission;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalButtonActions;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalDialogActions;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalDialogData;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalData;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.folgeprozessttl.ZuordnungAnforderungDerivatDTO;
import de.interfaceag.bmw.pzbk.berichtswesen.ZakStatusService;
import de.interfaceag.bmw.pzbk.controller.dialogs.AnforderungChangedInStatusPlausiConfirmDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.ChangeVersionProceedDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.DerivatZuordnungsDialogController;
import de.interfaceag.bmw.pzbk.controller.dialogs.ModulFreigabeCommentDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.ModulFreigabeProccedDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.SensorCocDialogController;
import de.interfaceag.bmw.pzbk.controller.dialogs.UmsetzerController;
import de.interfaceag.bmw.pzbk.controller.dialogs.VersionPlusCreateNewVersionDialog;
import de.interfaceag.bmw.pzbk.controller.dialogs.VersionPlusKommentarDialog;
import de.interfaceag.bmw.pzbk.converters.VereinbarungTypeConverter;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Auswirkung;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDTO;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDialogMethods;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDialogViewData;
import de.interfaceag.bmw.pzbk.shared.dto.ProcessResultDto;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.ProzessbaukastenLinkData;
import de.interfaceag.bmw.pzbk.zak.ZakVereinbarungService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import org.primefaces.context.RequestContext;
import org.primefaces.model.menu.MenuModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Christian Schauer
 * @author fp
 */
@ViewScoped
@Named
public class AnforderungController extends AbstractAnforderungController
        implements ModulFreigabeCommentDialog, ModulFreigabeProccedDialog,
        ChangeVersionProceedDialog, VersionPlusCreateNewVersionDialog,
        VersionPlusKommentarDialog, AnforderungChangedInStatusPlausiConfirmDialog,
        ProzessbaukastenZuordnenDialogMethods, AnforderungEditFahrzeugmerkmalButtonActions,
        AnforderungEditFahrzeugmerkmalDialogActions {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungController.class);
    public static final String FACES_REDIRECT_TRUE = "&faces-redirect=true";
    public static final String PF_MODUL_FREIGABE_COMMENT_DIALOG_SHOW = "PF('modulFreigabeCommentDialog').show()";

    @Inject
    private DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    private ZakVereinbarungService zakService;
    @Inject
    private ZakStatusService zakStatusService;
    @Inject
    private ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;
    @Inject
    private AnforderungMitMeldungFacade facade;
    @Inject
    private AnforderungStatusChangeService anforderungStatusChangeService;

    @Inject
    private AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData;

    private DerivatZuordnungsDialogController derivatZuordnungsDialogController;

    private Anforderung anforderung;
    private Meldung baseMeldung;

    private Map<ModulSeTeam, VereinbarungType> vereinbarungBeiModulMap = new HashMap<>();
    protected VereinbarungType vereinbarung;
    private Map<ModulSeTeam, Boolean> modulAuthorization = new HashMap<>();
    AnforderungFreigabeBeiModulSeTeam anfoModulZumFreigeben;
    private String modulFreigabeKommentar;

    private List<ZuordnungAnforderungDerivatDTO> anforderungDerivatList = new ArrayList<>();
    private List<ZuordnungAnforderungDerivat> deriAnforderungen = new ArrayList<>();
    private List<ZuordnungAnforderungDerivat> deriAnfoToRemoveAsList = new ArrayList<>();
    private List<ZuordnungAnforderungDerivat> anfoNachZakList = new ArrayList<>();

    private AnforderungViewPermission viewPermission;
    private AnforderungEditViewPermission editViewPermission;
    private AnforderungMitMeldungViewData viewData;

    private boolean newVersion;
    private boolean justAbgeleitet;
    private boolean changedInPlausi;

    // for comparison with the current beschreibungDe
    private String originalBeschreibungDe;
    private List<AnforderungFreigabeBeiModulSeTeam> originalFreigabeBeiModulSeTeam;

    @PostConstruct
    @Override
    public void init() {
        String idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        String fachIdString = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fid");
        String createStatus = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("created");
        String vsKomment = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("comment");

        checkRequestParameter(idstr, fachIdString, createStatus);

        berechtigt = anforderungService.isUserBerechtigtForAnforderung(originalObjectId);

        initAnforderungData(fachIdString, createStatus, vsKomment);

        changedInPlausi = false;

        setUserRollen(getBerechtigungService().getRolesForUser(session.getUser()));

        vereinbarung = VereinbarungType.ZAK;

        vsLabel = "V" + anforderung.getVersion();
        initAuswirkungsarten();
        auswirkung = new Auswirkung();
        initStandardBild();

        initViewData();

        isRequierdByMeldung();

        this.umsetzerController = new UmsetzerController(modulService, anforderung.getAnfoFreigabeBeiModul());
        umsetzerController.setDialogUmsetzerSet(anforderung.getUmsetzer());

        Map<ModulSeTeam, Boolean> modulAuthorizationMap = new HashMap<>();

        getModulAuthorizationMapFromAnforderung(modulAuthorizationMap);

        setModulAuthorization(modulAuthorizationMap);

        this.derivatZuordnungsDialogController = new DerivatZuordnungsDialogController(anforderung,
                session.getUser(), session.hasRole(Rolle.ADMIN), getDerivatService(),
                zuordnungAnforderungDerivatService);

        this.sensorCocDialogController = new SensorCocDialogController(this.sensorCocService, this.getBerechtigungService(), this.anforderung, session.getUser());
        this.deriAnforderungen = getAnforderungService().getDeriAnforderungenForAnforderung(anforderung);
        setAllFestgestelltIn(anforderungService.getAllFestgestelltIn());

        Set<Rolle> rolesWithWritePermissions;
        rolesWithWritePermissions = setRolesWithWritePermissions();

        viewPermission = new AnforderungViewPermission(rolesWithWritePermissions,
                anforderung.getStatus(), berechtigt, viewData.hasZuordnungZuProzessbaukasten());

        this.editViewPermission = new AnforderungEditViewPermission(rolesWithWritePermissions, anforderung.getStatus());

        this.originalBeschreibungDe = anforderung.getBeschreibungAnforderungDe();
        this.originalFreigabeBeiModulSeTeam = new ArrayList<>(anforderung.getAnfoFreigabeBeiModul());

        this.setAnforderungDerivatList(anforderungService.getAnforderungeDerivatListForAnforderung(anforderung, rolesWithWritePermissions));
        session.setLocationForView();
        setAllWerk(werkService.getAllWerk());

        super.initDetektoren(getAnfoMgmtObject().getDetektoren());
    }

    private void initAnforderungData(String fachIdString, String createStatus, String vsKomment) {
        if (this.isJustAbgeleitet() && isMyMeldung(fachIdString)) {
            initJustAbgeleiteteAnforderung(fachIdString);
        } else {
            initNewOrExistingAnforderung(createStatus, vsKomment);
        }
    }

    private void initNewOrExistingAnforderung(String createStatus, String vsKomment) {
        if (originalObjectId != null && berechtigt) {
            if (createStatus != null && "new".equals(createStatus)) {
                initForNewVersionAnforderung(vsKomment);
            } else {
                initAnforderungForExistingAnforderung();
            }
            vereinbarungBeiModulMap = getAnforderungService().createVereinbarungMap(anforderung);
        } else if (originalObjectId != null && anforderungService.isMeldungForUserRole(originalObjectId)) {
            berechtigt = true;
            baseMeldung = getAnforderungService().getMeldungWorkCopyByIdOrNewMeldung(originalObjectId);
            try {
                anforderung = getAnforderungService().createAnforderungForMeldung(originalObjectId);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }
        } else {
            initForEmptyView();
        }
    }

    private void initForEmptyView() {
        anforderung = new Anforderung(1);
        anforderung.setStatus(Status.A_INARBEIT);
        anforderung.setStaerkeSchwaeche(true);
        anforderung.setAuswirkungen(new ArrayList<>());
    }

    private void initJustAbgeleiteteAnforderung(String fachIdString) {
        originalObjectFachId = fachIdString;
        baseMeldung = getAnforderungService().getMeldungWorkCopyByFachIdOrNewMeldung(fachIdString);
        try {
            anforderung = getAnforderungService().createAnforderungForMeldung(fachIdString);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            LOG.error(null, ex);
        }
    }

    private void initAnforderungForExistingAnforderung() {
        anforderung = getAnforderungService().getAnforderungWorkCopyByIdOrNewAnforderung(originalObjectId);
        if (anforderung.getSensorCoc() != null && anforderung.getTteam() != null) {
            nextStatusList = StatusUtils.getAllNextStatusAsList(anforderung.getStatus());
        }
    }

    private void initForNewVersionAnforderung(String vsKomment) {
        try {
            if (vsKomment != null) {
                setVersionKommentar(vsKomment);
            }
            anforderung = getAnforderungService().createNewVersionForAnforderung(originalObjectId);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            LOG.error(null, ex);
        }
        this.created = true;
    }

    private void isRequierdByMeldung() {
        if (anforderung.getStatus().ordinal() >= 1 && anforderung.getStatus().ordinal() != 2 && anforderung.getStatus().ordinal() < 11) {
            this.setRequiredByMelden(true);
        } else {
            this.setRequiredByMelden(false);
        }
    }

    private void getModulAuthorizationMapFromAnforderung(Map<ModulSeTeam, Boolean> modulAuthorizationMap) {
        anforderung.getAnfoFreigabeBeiModul().forEach(mf -> {
            boolean isAuthorized;
            if (mf != null && mf.getModulSeTeam() != null) {
                isAuthorized = getAnforderungService().isAuthorizedForModulSeTeam(mf.getModulSeTeam(), anforderung.getStatus(), anforderung.getTteam());
                modulAuthorizationMap.put(mf.getModulSeTeam(), isAuthorized);
            }
        });
    }

    private Set<Rolle> setRolesWithWritePermissions() {
        Set<Rolle> rolesWithWritePermissions;
        if (anforderung.getSensorCoc() != null && anforderung.getTteam() != null
                && anforderung.getAnfoFreigabeBeiModul() != null) {
            rolesWithWritePermissions = session.getUserPermissions()
                    .getRolesWithWritePermissionsForAnforderung(anforderung.getSensorCoc(),
                            anforderung.getTteam().getId(),
                            anforderung.getAnfoFreigabeBeiModul().stream()
                                    .map(f -> f.getModulSeTeam().getId()).collect(Collectors.toSet())
                    );
            if (session.hasRole(Rolle.TTEAMMITGLIED)) {
                List<Long> tteamIdsForWrite = anforderungService.getTteamIdsforTteamMitgliedAndRechttype(session.getUser(), Rechttype.SCHREIBRECHT.getId());
                if (tteamIdsForWrite.contains(anforderung.getTteam().getId())) {
                    rolesWithWritePermissions.add(Rolle.TTEAMMITGLIED);
                }
            }
            if (session.hasRole(Rolle.TTEAM_VERTRETER)) {
                List<Long> tteamIdsForWrite = session.getUserPermissions().getTteamIdsForTteamVertreter();
                if (tteamIdsForWrite.contains(anforderung.getTteam().getId())) {
                    rolesWithWritePermissions.add(Rolle.TTEAM_VERTRETER);
                }
            }
        } else {
            rolesWithWritePermissions = session.getUserPermissions()
                    .getRolesWithWritePermissionsForNewAnforderung();
        }
        return rolesWithWritePermissions;
    }

    private void checkRequestParameter(String idstr, String fachIdString, String createStatus) throws NumberFormatException {
        if (idstr != null && !"null".equals(idstr)) {
            originalObjectId = Long.parseLong(idstr);
        }
        this.justAbgeleitet = fachIdString != null && !fachIdString.isEmpty() && fachIdString.startsWith("M");
        this.newVersion = "new".equals(createStatus);
    }

    private void initViewData() {
        viewData = facade.initViewData(anforderung);
    }

    public AnforderungMitMeldungViewData getViewData() {
        return viewData;
    }

    @Override
    public String processGeloeschtStatusChange() {
        return super.processStatusChange(Status.A_GELOESCHT);
    }

    public void updateTteamleiter() {
        Tteam tteam = this.anforderung.getTteam();
        this.getBerechtigungService().getTteamleiterOfTteam(tteam)
                .ifPresent(t -> this.anforderung.getTteam().setTeamleiter(t));
    }

    @Override
    public String edit() {
        anforderung.setStatusChangeRequested(false);
        getAnforderungService().saveAnforderung(anforderung);
        return PageUtils.getUrlForPageWithId(Page.ANFORDERUNGEDIT, anforderung.getId());
    }

    @Override
    public String saveFromDialog() {
        return saveValidatedData();
    }

    @Override
    public String save() {
        getAnfoMgmtObject().setDetektoren(super.getDetektorenValues());
        boolean isValid = checkTitelbild();
        if (isValid) {
            boolean showKommentarChangedDialog = checkChangedinPlausi();
            // when dialog is shown this method does nothing.
            // the confirmation button of the dialog triggers the saveValidatedData() method;
            if (showKommentarChangedDialog) {
                return "";
            }
            return saveValidatedData();
        }

        return "";
    }

    public String saveFromView() {
        boolean showKommentarChangedDialog = checkChangedinPlausi();
        // when dialog is shown this method does nothing.
        // the confirmation button of the dialog triggers the saveValidatedData() method;
        if (showKommentarChangedDialog) {
            return "";
        }
        return saveValidatedData();
    }

    public String saveValidatedData() {

        if (this.isJustAbgeleitet()) {
            this.anforderung = anforderungService.saveAbgeleiteteNewAnforderung(anforderung, baseMeldung, statusChangeKommentar, getAnhangForBild(), anforderungEditFahrzeugmerkmalData);
        } else if (this.isNewVersion()) {
            this.anforderung = anforderungService.saveNewAnforderungVersion(anforderung, originalObjectId, versionKommentar, getAnhangForBild(), anforderungEditFahrzeugmerkmalData);
        } else if (this.changedInPlausi) {
            anforderungService.saveChangesToAnforderungAndResetFreigabe(anforderung, getAnhangForBild(), anforderungEditFahrzeugmerkmalData);
        } else {
            this.anforderung = anforderungService.saveChangesToAnforderung(anforderung, getAnhangForBild(), anforderungEditFahrzeugmerkmalData);
        }

        return PageUtils.getUrlForPageWithId(Page.ANFORDERUNGVIEW, anforderung.getId());
    }

    @Override
    public void proceedWithStatusChange() {
        if (checkTitelbild()) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('statusChangeConfirmDialog').show()");
        }
    }

    private boolean checkChangedinPlausi() {
        boolean isKommentarChangedInStatusPlausib = AnforderungBeschreibungChangedValidator.isKommentarChangedInStatusPlausibilisiert(anforderung, originalBeschreibungDe);

        boolean isModulChangedInStatusPlausib = AnforderungModulChangedValidator.isModulChangedInStatusPlausibilisiert(anforderung, originalFreigabeBeiModulSeTeam);

        if (!changedInPlausi) {
            if (isKommentarChangedInStatusPlausib || isModulChangedInStatusPlausib) {
                changedInPlausi = true;
                RequestContext.getCurrentInstance().execute("PF('anforderungPlausiChangedDialog').show()");
                return true;
            }
        }
        changedInPlausi = false;
        return false;
    }

    private boolean checkTitelbild() {
        boolean isTitelbildValid = AnforderungTitelbildValidator.isValid(anforderung, getAnhangForBild());
        // TODO neuer validator
        if (!isTitelbildValid) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Es muss ein Anhang als Bild für die Anforderung ausgewählt werden.", ""));
            context.validationFailed();
        }

        return isTitelbildValid;
    }

    @Override
    public String cancel() {
        if (changedInPlausi) {
            changedInPlausi = false;
        }
        if (anforderung.getId() != null) {
            return PageUtils.getUrlForPageWithId(Page.ANFORDERUNGVIEW, anforderung.getId());
        } else if (originalObjectId != null && anforderungService.getAnforderungById(originalObjectId) != null) {
            return PageUtils.getUrlForPageWithId(Page.ANFORDERUNGVIEW, originalObjectId);
        } else if (originalObjectId != null && anforderung.getStatus().equals(Status.A_INARBEIT)) {
            return PageUtils.getUrlForPageWithId(Page.MELDUNGVIEW, originalObjectId);
        } else {
            return PageUtils.getRedirectUrlForPage(Page.DASHBOARD);
        }
    }

    @Override
    public String changeStatus(Integer newStatus) {
        if (newStatus != null) {
            if ((newStatus == 5 || newStatus == 7) && (statusChangeKommentar == null || "".equals(statusChangeKommentar) || statusChangeKommentar.trim().length() == 0)) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Kommentar zum Statuswechsel fehlt!", "Beim Wechsel zum Status " + Status.getStatusById(newStatus).toString() + " ist ein Kommentar notwendig."));
            } else {
                anforderungStatusChangeService.changeAnforderungStatus(anforderung, newStatus, statusChangeKommentar);
                return PageUtils.getUrlForPageWithId(Page.ANFORDERUNGVIEW, anforderung.getId());
            }
        }
        return "";
    }

    @Override
    public String restore() {
        Mitarbeiter currentUser = session.getUser();
        String message = anforderungService.restoreAnforderung(anforderung, currentUser);
        if (!message.isEmpty()) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Keine Wiederherstellung möglich", message));
            return "";
        }
        return PageUtils.getUrlForPageWithId(Page.ANFORDERUNGVIEW, anforderung.getId());
    }

    @Override
    public String createNewVersion() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('versionPlusDialog').hide();");

        final String comment = this.getVersionKommentar();
        String encodedComment;
        try {
            encodedComment = URLEncoder.encode(comment, "UTF-8");
        } catch (UnsupportedEncodingException exception) {
            LOG.error("Could not encode comment {}", comment);
            encodedComment = "";
        }
        return "anforderungEdit.xhtml?id=" + anforderung.getId() + "&created=new" + "&comment=" + encodedComment + FACES_REDIRECT_TRUE;
    }

    public MenuModel getVersionsMenuItems() {
        boolean hasRightToCreateNewVersion = viewPermission.getNewVersionButton();
        return facade.getVersionsMenuItemsForAnforderung(anforderung, hasRightToCreateNewVersion);
    }

    public MenuModel getZugeordneteProzessbaukastenMenuItems() {
        List<ProzessbaukastenLinkData> zugeordneteProzessbaukasten = viewData.getProzessbaukastenLinks();
        return facade.getZugeordneteProzessbaukastenMenuItems(zugeordneteProzessbaukasten);
    }

    public String getZakId(AnforderungFreigabeBeiModulSeTeam anforderungFreigabeBeiModulSeTeam) {
        return zakService.getZakIdByAnforderungModul(anforderungFreigabeBeiModulSeTeam.getAnforderung(), anforderungFreigabeBeiModulSeTeam.getModulSeTeam().getElternModul());
    }

    public String buildZakUrl(AnforderungFreigabeBeiModulSeTeam anforderungFreigabeBeiModulSeTeam) {
        return zakStatusService.buildZakUrl(getZakId(anforderungFreigabeBeiModulSeTeam));
    }

    public void resetUmsetzerList() {
        umsetzerController.setDialogUmsetzerSet(anforderung.getUmsetzer());
    }

    public void processUmsetzerChanges() {
        if (!umsetzerController.isChanged()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Es wurde kein Umsetzer hinzugefügt!", ""));
        }
        umsetzerController.resetChangedBoolean();

        Set<Umsetzer> dialogSet = this.umsetzerController.getDialogUmsetzerSet();

        dialogSet.stream().filter(umsetzer -> anforderung.getAnfoFreigabeBeiModul().stream()
                .noneMatch(anforderungFreigabeBeiModulSeTeam -> umsetzer.getSeTeam().equals(anforderungFreigabeBeiModulSeTeam.getModulSeTeam())
                )
        )
                .collect(Collectors.toList())
                .forEach(umsetzer -> {
                    AnforderungFreigabeBeiModulSeTeam addedAnfoModul = new AnforderungFreigabeBeiModulSeTeam(getAnfoMgmtObject(), umsetzer.getSeTeam(), false, "", vereinbarung);
                    addFahrzeugmerkmalDataForModulSeTeam(umsetzer, addedAnfoModul);
                    anforderung.getAnfoFreigabeBeiModul().add(addedAnfoModul);
                });

        anforderung.getAnfoFreigabeBeiModul().stream()
                .filter(au -> dialogSet.stream().noneMatch(u -> u.getSeTeam().equals(au.getModulSeTeam())))
                .collect(Collectors.toList())
                .forEach(a -> anforderung.getAnfoFreigabeBeiModul().remove(a));

    }

    private void addFahrzeugmerkmalDataForModulSeTeam(Umsetzer umsetzer, AnforderungFreigabeBeiModulSeTeam addedAnfoModul) {
        AnforderungId anforderungId = getAnforderung().getAnforderungId();
        ModulSeTeamId modulSeTeamId = umsetzer.getSeTeam().getModulSeTeamId();
        final AnforderungEditFahrzeugmerkmalDialogData anforderungEditFahrzeugmerkmalDialogData = facade.getAnforderungEditFahrzeugmerkmalDialogData(anforderungId, modulSeTeamId);
        this.anforderungEditFahrzeugmerkmalData.addDataForModulSeTeam(modulSeTeamId, anforderungEditFahrzeugmerkmalDialogData);
        if (anforderungEditFahrzeugmerkmalDialogData.getFahrzeugmerkmale().isEmpty()) {
            addedAnfoModul.setFahrzeugmerkmalConfigured(Boolean.TRUE);
        }
    }

    public void removeModulFreigabe(AnforderungFreigabeBeiModulSeTeam freigabeBeiModulSeTeam) {
        getAnfoMgmtObject().removeAnfoFreigabeBeiModul(freigabeBeiModulSeTeam);
        getAnfoMgmtObject().getUmsetzer().stream().filter(u -> u.getSeTeam().equals(freigabeBeiModulSeTeam.getModulSeTeam())).collect(Collectors.toList()).forEach(u -> getAnfoMgmtObject().removeUmsetzer(u));

        this.umsetzerController.setFreigabeList(anforderung.getAnfoFreigabeBeiModul());
        this.umsetzerController.setDialogUmsetzerSet(getAnfoMgmtObject().getUmsetzer());

    }

    public void showAnfoModulCommentDialog(AnforderungFreigabeBeiModulSeTeam anfoModul) {
        setAnfoModulZumFreigeben(anfoModul);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('anfoModulCommentDialog').show()");
    }

    public Boolean isAuthorizedForModulSeTeam(ModulSeTeam modulSeTeam) {
        return modulAuthorization.containsKey(modulSeTeam) && modulAuthorization.get(modulSeTeam);
    }

    public Boolean isMyModulSeTeam(ModulSeTeam modulSeTeam) {
        return getBerechtigungService().isMyModulSeTeam(modulSeTeam);
    }

    public void showModulFreigebenDialog(AnforderungFreigabeBeiModulSeTeam anfoModul, boolean freigegeben) {
        setAnfoModulZumFreigeben(anfoModul);
        this.setModulFreigabeKommentar(null);

        if (!freigegeben) {
            processModulAbgelehnt();
        } else {
            processModulFreigabe(anfoModul);
        }
    }

    private void processModulAbgelehnt() {
        setKommentarRequired(true);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute(PF_MODUL_FREIGABE_COMMENT_DIALOG_SHOW);
    }

    private void processModulFreigabe(AnforderungFreigabeBeiModulSeTeam freigabeBeiModulSeTeam) {
        Anforderung freigabeBeiModulSeTeamAnforderung = freigabeBeiModulSeTeam.getAnforderung();
        if (isDemGueltigenProzessbaukastenZugeordnet(freigabeBeiModulSeTeamAnforderung)) {
            LOG.info("No previous versions with gultig prozessbaukasten found");
            showValidationGrowlForZugeordneteAnforderung();
        } else {
            processAnforderungModulFreigabe(freigabeBeiModulSeTeam);
        }
    }

    private void showValidationGrowlForZugeordneteAnforderung() {
        String message = facade.generateGrowlMessageForFreigabeNeuerVersion();
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
    }

    private Boolean isDemGueltigenProzessbaukastenZugeordnet(Anforderung anforderung) {
        return facade.isDemGueltigenProzessbaukastenZugeordnet(anforderung);
    }

    private void processAnforderungModulFreigabe(AnforderungFreigabeBeiModulSeTeam anfoModul) {
        setKommentarRequired(false);
        if (getAnforderungService().checkFreizugeben(anforderung, anfoModul)) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('changeVersionDialog').show()");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(PF_MODUL_FREIGABE_COMMENT_DIALOG_SHOW);
        }
    }

    @Override
    public void proceedWithFullFreigabe() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('changeVersionDialog').hide()");
        context.execute(PF_MODUL_FREIGABE_COMMENT_DIALOG_SHOW);
    }

    @Override
    public String proceedWithModulFreigabe(AnforderungFreigabeBeiModulSeTeam anfoModul) {
        // Modul wird abgelehnt
        if (isKommentarRequired()) {
            return facade.modulAblehnen(anfoModul, modulFreigabeKommentar, anforderung.getId());
        } else { // Modul wird freigegeben
            return facade.anforderungForSelectedModulFreigeben(anfoModul, modulFreigabeKommentar, anforderung.getId());
        }
    }

    public String acceptZurKenntnisGenommen(ZuordnungAnforderungDerivatDTO anforderungDerivat) {
        LOG.info("Accecpt Folgeprozess for Derivat {}", anforderungDerivat.getDerivatName());
        anforderungService.acceptZurKenntnisGenommen(anforderungDerivat);
        return PageUtils.getUrlForPageWithId(Page.ANFORDERUNGVIEW, anforderung.getId());
    }

    public void validateFahrzeugmerkmale(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        final List<AnforderungFreigabeBeiModulSeTeam> freigaben = getAnforderung().getAnfoFreigabeBeiModul();

        String summary = "Fahrzeugmerkmale no message defined";
        if (value instanceof String) {
            summary = (String) value;
        }
        String detail = "----------Fahrzeugmerkmale sind nicht configuriert-----------";
        if (freigaben == null || freigaben.isEmpty() || freigaben.stream().filter(freigabe -> !freigabe.isConfigured()).count() > 0L) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
        }
    }

    public void validateUmsetzer(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String summary = "Umsetzer no message defined";
        if (value instanceof String) {
            summary = (String) value;
        }
        String detail = "----------Umsetzer leer-----------";
        if (getAnfoMgmtObject().getUmsetzer() == null || getAnfoMgmtObject().getUmsetzer().isEmpty()) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
        }
    }

    public void onModulEdit(AnforderungFreigabeBeiModulSeTeam afg) {
        this.addToVtMap(afg);
    }

    public void addToVtMap(AnforderungFreigabeBeiModulSeTeam afg) {
        if (getVereinbarungBeiModulMap() == null) {
            setVereinbarungBeiModulMap(new HashMap<>());
        }

        if (afg != null) {
            getVereinbarungBeiModulMap().put(afg.getModulSeTeam(), afg.getVereinbarungType());
        }
    }

    public List<Umsetzer> getUmsetzerListFilteredByModulSeTeam(ModulSeTeam modulSeTeam) {
        List<Umsetzer> result = new ArrayList<>();
        if (getAnfoMgmtObject().getUmsetzer() != null && !getAnfoMgmtObject().getUmsetzer().isEmpty()) {
            getAnfoMgmtObject().getUmsetzer().stream()
                    .filter(umsetzer -> umsetzer.getSeTeam().equals(modulSeTeam) && umsetzer.getKomponente() != null)
                    .forEachOrdered(result::add);
        }
        return result;
    }

    public boolean isViewDisableEdit() {
        Status status = anforderung.getStatus();
        boolean hasRightToEdit = viewPermission.getEditButton();
        return !AnforderungInlineEditViewPermission.isInlineEdit(status, hasRightToEdit);
    }

    @Override
    public String getModulFreigabeKommentar() {
        return modulFreigabeKommentar;
    }

    @Override
    public void setModulFreigabeKommentar(String modulFreigabeKommentar) {
        this.modulFreigabeKommentar = modulFreigabeKommentar;
    }

    public Map<ModulSeTeam, VereinbarungType> getVereinbarungBeiModulMap() {
        return vereinbarungBeiModulMap;
    }

    public void setVereinbarungBeiModulMap(Map<ModulSeTeam, VereinbarungType> vereinbarungBeiModulMap) {
        this.vereinbarungBeiModulMap = vereinbarungBeiModulMap;
    }

    @Override
    public AnforderungFreigabeBeiModulSeTeam getAnfoModulZumFreigeben() {
        return anfoModulZumFreigeben;
    }

    @Override
    public void setAnfoModulZumFreigeben(AnforderungFreigabeBeiModulSeTeam anfoModulZumFreigeben) {
        this.anfoModulZumFreigeben = anfoModulZumFreigeben;
    }

    public Map<ModulSeTeam, Boolean> getModulAuthorization() {
        return modulAuthorization;
    }

    public void setModulAuthorization(Map<ModulSeTeam, Boolean> modulAuthorization) {
        this.modulAuthorization = modulAuthorization;
    }

    public VereinbarungType getVereinbarung() {
        return vereinbarung;
    }

    public void setVereinbarung(VereinbarungType vereinbarung) {
        this.vereinbarung = vereinbarung;
    }

    public List<ZuordnungAnforderungDerivat> getDerivatZuordnungenWithBerechtigung() {
        return viewData.getDerivatZuordnungenWithBerechtigung();
    }

    /**
     * Do not delete this method. It is actually called from the frontend!
     */
    public void showDerivatenAnzeigenDialog() {
        deriAnforderungen = getAnforderungService().getDeriAnforderungenForAnforderung(anforderung);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('derivatenAnzeigenDialog').show()");
    }

    /**
     * Do not delete this method. It is actually called from the frontend!
     */
    public void showDerivateZuordnenDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('derivateZuordnenDialog').show()");
    }

    /**
     * Do not delete this method. It is actually called from the frontend!
     */
    public void onAnfoDeriEdit(DerivatAnforderungModul derivatAnforderungModul) {
        derivatAnforderungModulService.persistDerivatAnforderungModul(derivatAnforderungModul);
    }

    public String persistDerivatChanges() {
        derivatZuordnungsDialogController.persistChanges();
        return PageUtils.getUrlForPageWithFachIdAndVersionAndId(Page.ANFORDERUNGVIEW, anforderung.getFachId(), anforderung.getVersion(), anforderung.getId());
    }

    public void sendNachZak() {
        FacesContext context = FacesContext.getCurrentInstance();
        List<ZuordnungAnforderungDerivat> zuordnungenForZakUebertragung = getAnfoNachZakList();

        if (!zuordnungenForZakUebertragung.isEmpty()) {
            ProcessResultDto zakUebertragungReport = facade.generateReportNachZakUebertragung(zuordnungenForZakUebertragung);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Uebertragung nach ZAK: ", zakUebertragungReport.getMessage()));
            initViewData();

        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO: ", "Bitte wählen Sie Derivate/Anforderungen für die Übertragung nach ZAK!"));
        }
    }

    public void toggleNachZak(ZuordnungAnforderungDerivat deri) {
        if (!this.getAnfoNachZakList().isEmpty() && this.getAnfoNachZakList().contains(deri)) {
            this.getAnfoNachZakList().remove(deri);
        } else {
            if (this.getAnfoNachZakList() == null) {
                this.setAnfoNachZakList(new ArrayList<>());
            }
            this.getAnfoNachZakList().add(deri);
        }
    }

    public MenuModel getZugeordneteMeldungenMenuItems() {
        boolean hasRightToZuordnen = viewPermission.getMeldungZuordnenButton();
        return facade.getZugeordneteMeldungenMenuItemsForAnforderung(anforderung, hasRightToZuordnen);
    }

    public void reset() {
        this.setAnfoNachZakList(new ArrayList<>());
        this.setDeriAnfoToRemoveAsList(new ArrayList<>());
    }

    // ---------- getter and setter --------------------------------------------
    public Meldung getBaseMeldung() {
        return baseMeldung;
    }

    public void setBaseMeldung(Meldung baseMeldung) {
        this.baseMeldung = baseMeldung;
    }

    public ZakVereinbarungService getZakService() {
        return zakService;
    }

    public void setZakService(ZakVereinbarungService zakService) {
        this.zakService = zakService;
    }

    public DerivatZuordnungsDialogController getDerivatZuordnungsDialogController() {
        return this.derivatZuordnungsDialogController;
    }

    public Boolean canBeRemoved(DerivatAnforderungModul derivatAnforderungModul) {
        return getZakService().getZakUebertragungByDerivatAnforderungModul(derivatAnforderungModul) != null;
    }

    @Override
    public AnforderungViewPermission getViewPermission() {
        return viewPermission;
    }

    public List<ZuordnungAnforderungDerivatDTO> getAnforderungDerivatList() {
        return anforderungDerivatList;
    }

    public void setAnforderungDerivatList(List<ZuordnungAnforderungDerivatDTO> anforderungDerivatList) {
        this.anforderungDerivatList = anforderungDerivatList;
    }

    @Override
    public Anforderung getAnfoMgmtObject() {
        return anforderung;
    }

    public void setAnfoMgmtObject(Anforderung anfoMgmtObject) {
        this.anforderung = anfoMgmtObject;
    }

    public List<ZuordnungAnforderungDerivat> getAnfoNachZakList() {
        return anfoNachZakList;
    }

    public void setAnfoNachZakList(List<ZuordnungAnforderungDerivat> anfoNachZakList) {
        this.anfoNachZakList = anfoNachZakList;
    }

    public List<ZuordnungAnforderungDerivat> getDeriAnforderungen() {
        return deriAnforderungen;
    }

    public void setDeriAnforderungen(List<ZuordnungAnforderungDerivat> deriAnforderungen) {
        this.deriAnforderungen = deriAnforderungen;
    }

    public List<ZuordnungAnforderungDerivat> getDeriAnfoToRemoveAsList() {
        return deriAnfoToRemoveAsList;
    }

    public void setDeriAnfoToRemoveAsList(List<ZuordnungAnforderungDerivat> deriAnfoToRemoveAsList) {
        this.deriAnfoToRemoveAsList = deriAnfoToRemoveAsList;
    }

    public Converter getVereinbarungTypeConverter() {
        return new VereinbarungTypeConverter();
    }

    public boolean isNewVersion() {
        return newVersion;
    }

    public void setNewVersion(boolean newVersion) {
        this.newVersion = newVersion;
    }

    public boolean isJustAbgeleitet() {
        return justAbgeleitet;
    }

    public void setJustAbgeleitet(boolean justAbgeleitet) {
        this.justAbgeleitet = justAbgeleitet;
    }

    public ProzessbaukastenZuordnenDialogViewData getProzessbaukastenZuordnenDialogViewData() {
        return viewData.getProzessbaukastenZuordnenDialogViewData().orElse(null);
    }

    public String getProzessbaukastenZuordnenButtonName() {
        if (viewData.getProzessbaukastenZuordnenDialogViewData().isPresent()) {
            if (isProzessbaukastenPresent()) {
                return localizationService.getValue("anforderung_view_zugeordneten_prozessbaukasten");
            } else {
                return localizationService.getValue("prozessbaukastenzuordnendialog_buttonname");
            }
        }
        return "";
    }

    public String prozessbaukastenZuordnungButton() {
        if (!isProzessbaukastenPresent()) {
            return openZuordnenDialog();
        } else {
            return "";
        }
    }

    private String openZuordnenDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('prozessbaukastenZuordnenDialog').show()");
        return "";
    }

    @Override
    public void resetProzessbaukastenZuordnenDialog() {
        getProzessbaukastenZuordnenDialogViewData().removeProzessbaukasten();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('prozessbaukastenZuordnenDialog').hide()");
    }

    @Override
    public String prozessbaukastenZuordnen() {
        Optional<ProzessbaukastenZuordnenDTO> prozessbaukastenZuordnenDto = getProzessbaukastenZuordnenDialogViewData().getZugeordneterProzessbaukastenForView();
        if (!prozessbaukastenZuordnenDto.isPresent()) {
            return showGrowlMessage();
        }
        return facade.prozessbaukastenZuordnen(prozessbaukastenZuordnenDto.get(), anforderung.getId());
    }

    private String showGrowlMessage() {
        FacesContext context = FacesContext.getCurrentInstance();
        String title = localizationService.getValue("anforderung_view_prozessbaukasten_zuordnen_errorMessage");
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, title, null));
        return "";
    }

    public boolean isProzessbaukastenPresent() {
        return anforderung.isProzessbaukastenZugeordnet();
    }

    public Anforderung getAnforderung() {
        return anforderung;
    }

    @Override
    public boolean isAfoMgmtObjectInstanceOfAnforderung() {
        return true;
    }

    @Override
    public void showFahrzeugmerkmalEditDialogForModulSeTeam(ModulSeTeamId modulSeTeamId) {
        final AnforderungId anforderungId = getAnforderung().getAnforderungId();

        if (anforderungEditFahrzeugmerkmalData.updateActiveData(modulSeTeamId)) {
            LOG.debug("found entry for {} in map", modulSeTeamId);
        } else {
            LOG.debug("load data for {} from database", modulSeTeamId);
            final AnforderungEditFahrzeugmerkmalDialogData anforderungEditFahrzeugmerkmalDialogData = facade.getAnforderungEditFahrzeugmerkmalDialogData(anforderungId, modulSeTeamId);
            this.anforderungEditFahrzeugmerkmalData.addDataForModulSeTeam(modulSeTeamId, anforderungEditFahrzeugmerkmalDialogData);
        }

        LOG.debug("show Fahrzeugmerkmal edit dialog for {} and {}", anforderungId, modulSeTeamId);
        final RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('fahrzeugmerkmalEditDialog').show()");
        requestContext.update("dialog:fahrzeugmerkmalEditDialogContent:editDialogForm");
    }

    @Override
    public void processFahrzeugmerkmalChanges() {
        final List<AnforderungFreigabeBeiModulSeTeam> freigaben = getAnforderung().getAnfoFreigabeBeiModul();
        for (AnforderungFreigabeBeiModulSeTeam anforderungFreigabeBeiModulSeTeam : freigaben) {
            final boolean allSelected = getAnforderungEditFahrzeugmerkmalDialogData().getFahrzeugmerkmale().stream().noneMatch(anforderungEditFahrzeugmerkmal -> anforderungEditFahrzeugmerkmal.getSelectedAuspraegungen().isEmpty());
            if (anforderungFreigabeBeiModulSeTeam.getModulSeTeam().getModulSeTeamId().equals(anforderungEditFahrzeugmerkmalData.getActiveModulSeTeamId()) && allSelected) {
                LOG.debug("update {} to status configured = TRUE", anforderungFreigabeBeiModulSeTeam.getModulSeTeam().getName());
                anforderungFreigabeBeiModulSeTeam.setFahrzeugmerkmalConfigured(Boolean.TRUE);
            } else if (anforderungFreigabeBeiModulSeTeam.getModulSeTeam().getModulSeTeamId().equals(anforderungEditFahrzeugmerkmalData.getActiveModulSeTeamId()) && !allSelected) {
                anforderungFreigabeBeiModulSeTeam.setFahrzeugmerkmalConfigured(Boolean.FALSE);
                LOG.debug("update {} to status configured = FALSE", anforderungFreigabeBeiModulSeTeam.getModulSeTeam().getName());
            }
        }
    }

    public AnforderungEditFahrzeugmerkmalDialogData getAnforderungEditFahrzeugmerkmalDialogData() {
        return anforderungEditFahrzeugmerkmalData.getActiveFahrzeugmerkmalDialogData();
    }

    public AnforderungEditViewPermission getEditViewPermission() {
        return editViewPermission;
    }

}
