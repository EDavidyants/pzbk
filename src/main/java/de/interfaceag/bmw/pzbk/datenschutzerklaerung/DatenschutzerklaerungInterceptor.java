package de.interfaceag.bmw.pzbk.datenschutzerklaerung;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.session.SessionEdit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

/**
 *
 * @author evda
 */
@ManagedBean
@Named
@RequestScoped
public class DatenschutzerklaerungInterceptor implements PhaseListener, Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(DatenschutzerklaerungInterceptor.class);

    @Inject
    private SessionEdit session;

    @Override
    public void beforePhase(PhaseEvent event) {
        if (session == null) {
            LOG.error("Session is null");
        }

        String viewRootId = event.getFacesContext().getViewRoot().getViewId();
        if (!viewRootId.startsWith("/public")) {
            redirectToStartPage(event);
        }

    }

    @Override
    public void afterPhase(PhaseEvent event) {

    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RENDER_RESPONSE;
    }

    private void redirectToStartPage(PhaseEvent event) {
        FacesContext context = event.getFacesContext();
        Mitarbeiter user = session.getUser();

        String contextPath = context.getExternalContext().getRequestContextPath();
        String redirectTo;

        if (user == null) {
            redirectTo = contextPath + "/403.xhtml";
            doRedirect(redirectTo, context);
        } else if (!user.isDatenschutzAngenommen()) {
            redirectTo = contextPath + "/public/datenschutzerklaerung.xhtml";
            doRedirect(redirectTo, context);
        }

    }

    private void doRedirect(String redirectTo, FacesContext context) {
        LOG.debug("redirecting to {}", redirectTo);

        try {
            context.getExternalContext().redirect(redirectTo);
        } catch (IOException ex) {
            LOG.error("DatenschutzerklaerungInterceptor IOException bei redirect");
        }
    }

}
