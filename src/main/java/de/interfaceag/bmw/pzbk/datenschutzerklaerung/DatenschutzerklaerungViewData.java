package de.interfaceag.bmw.pzbk.datenschutzerklaerung;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 *
 * @author evda
 */
public final class DatenschutzerklaerungViewData implements Serializable {

    private final Mitarbeiter user;
    private boolean datenschutzerklaerungReadAndUnderstood;
    private final boolean datenschutzerklaerungAkzeptiert;
    private final String datenschutzerklaerungText;

    private DatenschutzerklaerungViewData(Mitarbeiter user, boolean datenschutzerklaerungReadAndUnderstood,
            boolean datenschutzerklaerungAkzeptiert, String datenschutzerklaerungText) {
        this.user = user;
        this.datenschutzerklaerungReadAndUnderstood = datenschutzerklaerungReadAndUnderstood;
        this.datenschutzerklaerungAkzeptiert = datenschutzerklaerungAkzeptiert;
        this.datenschutzerklaerungText = datenschutzerklaerungText;
    }

    @Override
    public String toString() {
        String akzeptiertStatus = this.datenschutzerklaerungAkzeptiert ? " hat Datenschutzerklaerung akzeptiert"
                : " hat Datenschutzerklaerung noch nicht akzeptiert";
        return user.toString() + akzeptiertStatus;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hashCodeBuilder = new HashCodeBuilder(13, 257);
        return hashCodeBuilder.append(user)
                .append(datenschutzerklaerungReadAndUnderstood)
                .append(datenschutzerklaerungAkzeptiert)
                .append(datenschutzerklaerungText)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof DatenschutzerklaerungViewData)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        DatenschutzerklaerungViewData other = (DatenschutzerklaerungViewData) obj;
        EqualsBuilder equalsBuilder = new EqualsBuilder();
        return equalsBuilder.append(user, other.user)
                .append(datenschutzerklaerungReadAndUnderstood, other.datenschutzerklaerungReadAndUnderstood)
                .append(datenschutzerklaerungAkzeptiert, other.datenschutzerklaerungAkzeptiert)
                .append(datenschutzerklaerungText, other.datenschutzerklaerungText)
                .isEquals();
    }

    public static DatenschutzerklaerungViewData.Builder builder() {
        return new DatenschutzerklaerungViewData.Builder();
    }

    public static class Builder {

        Mitarbeiter user;
        boolean datenschutzerklaerungReadAndUnderstood;
        boolean datenschutzerklaerungAkzeptiert;
        String datenschutzerklaerungText;

        public Builder() {
        }

        public Builder forUser(Mitarbeiter user) {
            this.user = user;
            return this;
        }

        public Builder withDatenschutzerklaerungText(String datenschutzerklaerungText) {
            this.datenschutzerklaerungText = datenschutzerklaerungText;
            return this;
        }

        public Builder withDatenschutzerklaerungReadAndUnderstood(boolean datenschutzerklaerungReadAndUnderstood) {
            this.datenschutzerklaerungReadAndUnderstood = datenschutzerklaerungReadAndUnderstood;
            return this;
        }

        public Builder withDatenschutzerklaerungAkzeptiert(boolean datenschutzerklaerungAkzeptiert) {
            this.datenschutzerklaerungAkzeptiert = datenschutzerklaerungAkzeptiert;
            return this;
        }

        public DatenschutzerklaerungViewData build() {
            return new DatenschutzerklaerungViewData(user, datenschutzerklaerungReadAndUnderstood,
                    datenschutzerklaerungAkzeptiert, datenschutzerklaerungText);
        }

    }

    public Mitarbeiter getUser() {
        return user;
    }

    public boolean isDatenschutzerklaerungReadAndUnderstood() {
        return datenschutzerklaerungReadAndUnderstood;
    }

    public void setDatenschutzerklaerungReadAndUnderstood(boolean datenschutzerklaerungReadAndUnderstood) {
        this.datenschutzerklaerungReadAndUnderstood = datenschutzerklaerungReadAndUnderstood;
    }

    public boolean isDatenschutzerklaerungAkzeptiert() {
        return datenschutzerklaerungAkzeptiert;
    }

    public String getDatenschutzerklaerungText() {
        return datenschutzerklaerungText;
    }

}
