package de.interfaceag.bmw.pzbk.datenschutzerklaerung;

import java.io.Serializable;

/**
 *
 * @author evda
 */
public interface DatenschutzerklaerungMethods extends Serializable {

    boolean notReadyToBeAkzeptiert();

    void updateDatenschutzerklaerungForUser();

}
