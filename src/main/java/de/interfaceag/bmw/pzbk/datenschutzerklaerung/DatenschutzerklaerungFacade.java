package de.interfaceag.bmw.pzbk.datenschutzerklaerung;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.services.UserService;
import de.interfaceag.bmw.pzbk.session.SessionEdit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;

/**
 *
 * @author evda
 */
@Stateless
public class DatenschutzerklaerungFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(DatenschutzerklaerungFacade.class);

    @Inject
    private SessionEdit session;

    @Inject
    private UserService userService;

    @Inject
    private LocalizationService localizationService;

    public DatenschutzerklaerungViewData initViewData() {
        return DatenschutzerklaerungViewData.builder()
                .forUser(session.getUser())
                .withDatenschutzerklaerungReadAndUnderstood(session.userHasDatenschutzerklaerungAkzeptiert())
                .withDatenschutzerklaerungAkzeptiert(session.userHasDatenschutzerklaerungAkzeptiert())
                .withDatenschutzerklaerungText(localizationService.getValue("datenschutzerklaerung.text"))
                .build();
    }

    public boolean updateDatenschutzerklaerungForUser(DatenschutzerklaerungViewData viewData) {
        Mitarbeiter user = viewData.getUser();

        if (user == null) {
            LOG.error("DatenschutzerklaerungFacade.updateDatenschutzerklaerungForUser: User resolved to null");
            return false;
        }

        LOG.debug("Update Datenschutzerklaerung for {}", user);
        user.setDatenschutzAngenommen(true);
        userService.saveMitarbeiter(user);
        LOG.debug("Updated Status of Datenschutzerklaerung: {}", user.isDatenschutzAngenommen());
        session.setUser(user);
        return true;
    }

}
