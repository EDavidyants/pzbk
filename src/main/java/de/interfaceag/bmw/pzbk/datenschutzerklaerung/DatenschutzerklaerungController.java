package de.interfaceag.bmw.pzbk.datenschutzerklaerung;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

/**
 *
 * @author evda
 */
@RequestScoped
@Named
public class DatenschutzerklaerungController implements DatenschutzerklaerungMethods, Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(DatenschutzerklaerungController.class);

    @Inject
    private DatenschutzerklaerungFacade facade;

    private DatenschutzerklaerungViewData viewData;

    @PostConstruct
    public void init() {
        this.viewData = facade.initViewData();
    }

    @Override
    public boolean notReadyToBeAkzeptiert() {
        return !viewData.isDatenschutzerklaerungReadAndUnderstood();
    }

    @Override
    public void updateDatenschutzerklaerungForUser() {
        boolean datenschutzerklaerungUpdatedSuccessfully = facade.updateDatenschutzerklaerungForUser(viewData);
        if (datenschutzerklaerungUpdatedSuccessfully) {
            redirectToDashboard();
        }
    }

    private void redirectToDashboard() {
        try {
            String contextPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
            String redirectTo = contextPath + "/dashboard.xhtml";
            FacesContext.getCurrentInstance().getExternalContext().redirect(redirectTo);
        } catch (IOException ex) {
            LOG.error("DatenschutzerklaerungController IOException bei redirect");
        }
    }

    public DatenschutzerklaerungViewData getViewData() {
        return viewData;
    }

}
