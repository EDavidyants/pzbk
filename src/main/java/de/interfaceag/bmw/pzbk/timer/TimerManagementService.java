package de.interfaceag.bmw.pzbk.timer;

import de.interfaceag.bmw.pzbk.mail.MailService;
import de.interfaceag.bmw.pzbk.reporting.projekt.snapshot.ProjectReportingSnapshotService;
import de.interfaceag.bmw.pzbk.zak.ZakPostService;
import de.interfaceag.bmw.pzbk.zak.ZakUpdateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.Serializable;

/**
 * @author evda
 */
@Stateless
public class TimerManagementService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(TimerManagementService.class.getName());

    @Resource
    protected TimerService timerService;

    @Inject
    private MailService mailService;
    @Inject
    private ZakPostService zakPostService;
    @Inject
    private ZakUpdateService zakUpdateService;
    @Inject
    private ProjectReportingSnapshotService projectReportingSnapshotService;
    @Inject
    private KovaPhaseImDerivatTimerService kovaPhaseImDerivatTimerService;

    @Schedule(hour = "1", dayOfWeek = "1-5", info = "scheduled_job")
    public void calendarTimeoutHandler(Timer timer) {
        LOG.info("Update KovAPhaseImDerivat");
        kovaPhaseImDerivatTimerService.updateKovaPhasenImDerivat();
    }

    @Schedule(hour = "22", dayOfWeek = "1-7", info = "zakPostJob")
    public void zakPostTimeoutHandler(Timer timer) {
        zakPostService.sendQueuedZakUebertragungen();
    }

    @Schedule(hour = "0", dayOfWeek = "1-7", info = "zakGetJob")
    public void zakGetTimeoutHandler(Timer timer) {
        zakUpdateService.getZakUpdate();
    }

    @Schedule(hour = "20", dayOfWeek = "7", info = "statusabgleichSnapshots")
    public void createSnapshots(Timer timer) {
        projectReportingSnapshotService.createSnapshotsForActiveDerivate();
    }

    public MailService getMailService() {
        try {
            Object ms = new InitialContext().lookup("java:global/PZBK/MailService");
            if (ms instanceof MailService) {
                this.mailService = (MailService) ms;
            }
        } catch (NamingException ex) {
            LOG.error("Lookup for JavaMail Session failed: {}", ex.getMessage());
        }
        return mailService;
    }

}
