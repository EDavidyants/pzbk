package de.interfaceag.bmw.pzbk.timer;

import de.interfaceag.bmw.pzbk.dao.KovAPhaseImDerivatDao;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.TimerType;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.mail.KovaPhaseImDerivatMailService;
import de.interfaceag.bmw.pzbk.mail.UmsetzungsverwalterMailTemplate;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungBerechtigungService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import static de.interfaceag.bmw.pzbk.timer.KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer;

@Dependent
public class KovaPhaseImDerivatTimerService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(KovaPhaseImDerivatTimerService.class);

    @Inject
    private KovAPhaseImDerivatDao kovAPhaseImDerivatDao;
    @Inject
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;
    @Inject
    private UmsetzungsbestaetigungBerechtigungService umsetzungsbestaetigungBerechtigungService;
    @Inject
    private KovaPhaseImDerivatMailService kovaPhaseImDerivatMailService;

    public Map<TimerType, Integer> updateKovaPhasenImDerivat() {
        EnumMap<TimerType, Integer> report = new EnumMap<>(TimerType.class);
        updateConfiguredKovaPhasenImDerivat(report);
        updateActiveKovaPhasenImDerivat(report);
        return report;
    }

    private void updateConfiguredKovaPhasenImDerivat(EnumMap<TimerType, Integer> report) {
        Collection<KovAPhaseImDerivat> configuredKovaPhasenImDerivat = getConfiguredKovaPhasenImDerivat();

        sendFirstNotificationToSensorCocLeiterAndVertreter(configuredKovaPhasenImDerivat, report);
        sendBewertungsauftragAndInsertDefaultUmsetzungsverwalter(configuredKovaPhasenImDerivat, report);
        activateKovaPhasenImDerivat(configuredKovaPhasenImDerivat, report);
    }

    private void updateActiveKovaPhasenImDerivat(EnumMap<TimerType, Integer> report) {
        List<KovAPhaseImDerivat> activeKovAPhasenImDerivatWithUnfinishedUmsetzungsverwaltung = getActiveKovAPhasenImDerivatWithUnfinishedUmsetzungsverwaltung();

        sendReminderToUmsetzungsverwalter(activeKovAPhasenImDerivatWithUnfinishedUmsetzungsverwaltung, report);
        sendLastCallToUmsetzungsverwalter(activeKovAPhasenImDerivatWithUnfinishedUmsetzungsverwaltung, report);
        deactivateKovaPhasenImDerivat(activeKovAPhasenImDerivatWithUnfinishedUmsetzungsverwaltung, report);
    }

    private void deactivateKovaPhasenImDerivat(Collection<KovAPhaseImDerivat> relevantKovaPhasenImDerivat, EnumMap<TimerType, Integer> report) {
        for (KovAPhaseImDerivat kovAPhaseImDerivat : relevantKovaPhasenImDerivat) {
            if (isConditionMetForKovaPhaseImDerivatAndTimer(kovAPhaseImDerivat, TimerType.PHASE_ABGESCHLOSSEN)) {
                deactivateKovaPhaseImDerivat(kovAPhaseImDerivat);
                addToReport(TimerType.PHASE_ABGESCHLOSSEN, report);
            }
        }
    }

    private void deactivateKovaPhaseImDerivat(KovAPhaseImDerivat kovAPhaseImDerivat) {
        LOG.info("KovaPhase turns deactivated..");
        kovAPhaseImDerivat.setKovaStatus(KovAStatus.ABGESCHLOSSEN);
        kovAPhaseImDerivatDao.persistKovAPhaseImDerivat(kovAPhaseImDerivat);
    }

    private void sendLastCallToUmsetzungsverwalter(Collection<KovAPhaseImDerivat> relevantKovaPhasenImDerivat, EnumMap<TimerType, Integer> report) {
        for (KovAPhaseImDerivat kovAPhaseImDerivat : relevantKovaPhasenImDerivat) {
            if (isConditionMetForKovaPhaseImDerivatAndTimer(kovAPhaseImDerivat, TimerType.LAST_CALL)) {
                kovaPhaseImDerivatMailService.sendMailToUmsetzungsverwalter(kovAPhaseImDerivat, UmsetzungsverwalterMailTemplate.LASTCALL);
                addToReport(TimerType.LAST_CALL, report);
            }
        }
    }

    private void sendReminderToUmsetzungsverwalter(Collection<KovAPhaseImDerivat> relevantKovaPhasenImDerivat, EnumMap<TimerType, Integer> report) {
        for (KovAPhaseImDerivat kovAPhaseImDerivat : relevantKovaPhasenImDerivat) {
            if (isConditionMetForKovaPhaseImDerivatAndTimer(kovAPhaseImDerivat, TimerType.REMINDER)) {
                sendReminderToUmsetzungsverwalter(kovAPhaseImDerivat);
                addToReport(TimerType.REMINDER, report);
            }
        }
    }

    private List<KovAPhaseImDerivat> getActiveKovAPhasenImDerivatWithUnfinishedUmsetzungsverwaltung() {
        return umsetzungsbestaetigungService.getKovaphasenByKovastatusAndStatus(KovAStatus.AKTIV, UmsetzungsBestaetigungStatus.OFFEN);
    }

    private void activateKovaPhasenImDerivat(Collection<KovAPhaseImDerivat> relevantKovaPhasenImDerivat, EnumMap<TimerType, Integer> report) {
        for (KovAPhaseImDerivat kovAPhaseImDerivat : relevantKovaPhasenImDerivat) {
            if (isConditionMetForKovaPhaseImDerivatAndTimer(kovAPhaseImDerivat, TimerType.PHASE_AKTIV) && kovAPhaseImDerivat.getKovaStatus() != KovAStatus.AKTIV) {
                activateKovaPhaseImDerivat(kovAPhaseImDerivat);
                createDefaultUmsetzungsverwalter(kovAPhaseImDerivat);
                addToReport(TimerType.PHASE_AKTIV, report);
            }
        }
    }

    private void activateKovaPhaseImDerivat(KovAPhaseImDerivat kovAPhaseImDerivat) {
        LOG.info("KovaPhase turns active..");
        kovAPhaseImDerivat.setKovaStatus(KovAStatus.AKTIV);
        kovAPhaseImDerivatDao.persistKovAPhaseImDerivat(kovAPhaseImDerivat);
    }

    private void sendBewertungsauftragAndInsertDefaultUmsetzungsverwalter(Collection<KovAPhaseImDerivat> relevantKovaPhasenImDerivat, EnumMap<TimerType, Integer> report) {
        for (KovAPhaseImDerivat kovAPhaseImDerivat : relevantKovaPhasenImDerivat) {
            if (isConditionMetForKovaPhaseImDerivatAndTimer(kovAPhaseImDerivat, TimerType.BEWERTUNGSAUFTRAG)) {
                createDefaultUmsetzungsverwalter(kovAPhaseImDerivat);
                sendBewertungsauftragToUmsetzungsverwalter(kovAPhaseImDerivat);
                addToReport(TimerType.BEWERTUNGSAUFTRAG, report);
            }
        }
    }

    private void createDefaultUmsetzungsverwalter(KovAPhaseImDerivat kovAPhaseImDerivat) {
        LOG.info("Create default Umsetzungsverwatler");
        umsetzungsbestaetigungBerechtigungService.createAndPersistMissingUmsetzungsbestaetigungBerechtigungForKovaPhase(kovAPhaseImDerivat);
    }


    private void sendReminderToUmsetzungsverwalter(KovAPhaseImDerivat kovAPhaseImDerivat) {
        LOG.info("Sending reminder to UB..");
        kovaPhaseImDerivatMailService.sendMailToUmsetzungsverwalter(kovAPhaseImDerivat, UmsetzungsverwalterMailTemplate.REMINDER);
    }

    private void sendBewertungsauftragToUmsetzungsverwalter(KovAPhaseImDerivat kovAPhaseImDerivat) {
        LOG.info("Sending bewertungsauftrag to UB..");
        kovaPhaseImDerivatMailService.sendMailToUmsetzungsverwalter(kovAPhaseImDerivat, UmsetzungsverwalterMailTemplate.BEWERTUNGSAUFTRAG);
    }


    private void sendFirstNotificationToSensorCocLeiterAndVertreter(Collection<KovAPhaseImDerivat> relevantKovaPhasenImDerivat, EnumMap<TimerType, Integer> report) {
        for (KovAPhaseImDerivat kovAPhaseImDerivat : relevantKovaPhasenImDerivat) {
            if (isConditionMetForKovaPhaseImDerivatAndTimer(kovAPhaseImDerivat, TimerType.FIRST_NOTIFICATION_FOR_FTS)) {
                LOG.info("Sending first notification to FTS..");
                kovaPhaseImDerivatMailService.sendMailToSensorCocLeiterAndVertreter(kovAPhaseImDerivat);
                addToReport(TimerType.FIRST_NOTIFICATION_FOR_FTS, report);
            }
        }
    }

    private List<KovAPhaseImDerivat> getConfiguredKovaPhasenImDerivat() {
        return kovAPhaseImDerivatDao.getKovAPhaseImDerivatWithStartAndEndSet();
    }

    private static void addToReport(TimerType timerType, Map<TimerType, Integer> report) {
        if (report.containsKey(timerType)) {
            report.put(timerType, report.get(timerType) + 1);
        } else {
            report.put(timerType, 1);
        }
    }

}
