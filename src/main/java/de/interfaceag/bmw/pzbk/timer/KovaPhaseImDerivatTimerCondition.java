package de.interfaceag.bmw.pzbk.timer;

import de.interfaceag.bmw.pzbk.config.PropertiesService;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.TimerType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

final class KovaPhaseImDerivatTimerCondition {

    public static final Logger LOG = LoggerFactory.getLogger(KovaPhaseImDerivatTimerCondition.class);

    private KovaPhaseImDerivatTimerCondition() {
    }

    static boolean isConditionMetForKovaPhaseImDerivatAndTimer(KovAPhaseImDerivat kovAPhaseImDerivat, TimerType timerType) {
        Date now = new Date();
        LocalDate nowLocal = Instant.ofEpochMilli(now.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate compareToDate = getCompareDate(kovAPhaseImDerivat, timerType);

        // set phase as active or inactive if time condition is met independent of the current day
        if (isPhaseAktiv(kovAPhaseImDerivat, timerType, nowLocal, compareToDate)) {
            return true;
        } else if (isPhaseAbgeschlossen(kovAPhaseImDerivat, timerType, nowLocal, compareToDate)) {
            return true;
        } else if (isMonday(nowLocal)) { // Current day of week is Monday
            LocalDate yesterday = nowLocal.minusDays(1);
            LocalDate dayBeforeYesterday = nowLocal.minusDays(2);
            if (isTargetDate(timerType, nowLocal, compareToDate)
                    || isTargetDate(timerType, yesterday, compareToDate)
                    || isTargetDate(timerType, dayBeforeYesterday, compareToDate)) {
                return true;
            }
        } else if (isTargetDate(timerType, nowLocal, compareToDate)) {
            // Tue - Fr
            return true;
        }

        return false;
    }

    private static boolean isTargetDate(TimerType timerType, LocalDate nowLocal, LocalDate compareToDate) {
        return getTargetDate(nowLocal, timerType).isEqual(compareToDate);
    }

    private static boolean isPhaseAktiv(KovAPhaseImDerivat kovAPhaseImDerivat, TimerType timerType, LocalDate nowLocal, LocalDate compareToDate) {
        return timerType == TimerType.PHASE_AKTIV && kovAPhaseImDerivat.getKovaStatus() != KovAStatus.AKTIV
                && (isTargetDate(timerType, nowLocal, compareToDate)
                || getTargetDate(nowLocal, timerType).isAfter(kovAPhaseImDerivat.getStartDate()) && getTargetDate(nowLocal, timerType).isBefore(kovAPhaseImDerivat.getEndDate()));
    }

    private static boolean isPhaseAbgeschlossen(KovAPhaseImDerivat kovAPhaseImDerivat, TimerType timerType, LocalDate nowLocal, LocalDate compareToDate) {
        return timerType == TimerType.PHASE_ABGESCHLOSSEN && kovAPhaseImDerivat.getKovaStatus() != KovAStatus.ABGESCHLOSSEN && getTargetDate(nowLocal, timerType).isAfter(compareToDate);
    }

    private static boolean isMonday(LocalDate nowLocal) {
        return nowLocal.getDayOfWeek().equals(DayOfWeek.MONDAY);
    }

    private static LocalDate getCompareDate(KovAPhaseImDerivat kovAPhaseImDerivat, TimerType timerType) {
        switch (timerType) {
            case FIRST_NOTIFICATION_FOR_FTS:
            case BEWERTUNGSAUFTRAG:
            case PHASE_AKTIV:
            case REMINDER:
                return kovAPhaseImDerivat.getStartDate();
            case LAST_CALL:
            case PHASE_ABGESCHLOSSEN:
                return kovAPhaseImDerivat.getEndDate();
            default:
                return null;
        }
    }

    private static LocalDate getTargetDate(LocalDate basis, TimerType timerType) {
        switch (timerType) {
            case FIRST_NOTIFICATION_FOR_FTS:
            case BEWERTUNGSAUFTRAG:
                return basis.plusWeeks(getExpirePeriod(timerType.getPropName()));
            case REMINDER:
            case PHASE_ABGESCHLOSSEN:
                return basis.minusWeeks(getExpirePeriod(timerType.getPropName()));
            case LAST_CALL:
                return basis.plusDays(getExpirePeriod(timerType.getPropName()));
            case PHASE_AKTIV:
            default:
                return basis;
        }
    }

    private static long getExpirePeriod(String timerTypeName) {
        String propValue = PropertiesService.getTimerSettings().getProperty(timerTypeName);
        return Long.parseLong(propValue);
    }

}
