package de.interfaceag.bmw.pzbk.session;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;

import java.io.Serializable;
import java.util.Locale;

/**
 *
 * @author sl
 */
public interface Session extends Serializable {

    UserPermissions<BerechtigungDto> getUserPermissions();

    boolean hasRole(Rolle rolle);

    Mitarbeiter getUser();

    String getUserAcronym();

    void setLocationForView();

    Locale getLocale();

}
