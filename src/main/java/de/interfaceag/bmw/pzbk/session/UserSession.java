package de.interfaceag.bmw.pzbk.session;

import de.interfaceag.bmw.pzbk.berechtigung.UserPermissionsService;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.UserDefinedSearch;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.exceptions.UserNotFoundException;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.security.Principal;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author Christian Schauer
 */
@Named
@SessionScoped
public class UserSession implements Serializable, SessionEdit, UserDefinedSearchApi {

    private static final Logger LOG = LoggerFactory.getLogger(UserSession.class);

    @Inject
    private UserSearchService userSearchService;
    @Inject
    private UserPermissionsService userPermissionsService;
    @Inject
    private UserService userService;

    private Mitarbeiter user;
    private UserPermissions userPermissions;

    @PostConstruct
    public void init() {
        registerIfNecessary();
        updatePermissions();
        final String location = user.getLocation();
        final Locale locale = new Locale(location);
        final FacesContext facesContext = FacesContext.getCurrentInstance();
        final UIViewRoot viewRoot = facesContext.getViewRoot();
        viewRoot.setLocale(locale);
    }

    // ---------- methods ------------------------------------------------------
    private void updatePermissions() {
        if (user != null) {
            userPermissions = userPermissionsService.getUserPermissions(user);
        }
    }

    @Override
    public void setLocationForView() {
        Locale locale = getLocale();
        FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
    }

    @Override
    public Locale getLocale() {
        String location = user.getLocation();
        if (location != null && !location.isEmpty()) {
            return new Locale(user.getLocation());
        } else {
            return Locale.GERMAN;
        }
    }

    private void registerIfNecessary() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Principal principal = fc.getExternalContext().getUserPrincipal();
        String qNumber;
        if (principal == null) {
            qNumber = "";
            LOG.warn("User Principal is null");
        } else {
            LOG.info("Principal {}", principal.toString());
            qNumber = principal.getName().toLowerCase();
        }
        user = userSearchService.getMitarbeiterByQnumber(qNumber);
        if (user != null) {
            try {
                Mitarbeiter update = userSearchService.getMitarbeiterFromLdap(qNumber);
                user = userService.updateUserData(user, update);
            } catch (UserNotFoundException ex) {
                LOG.error(null, ex);
            }
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
        } else {
            try {
                user = userSearchService.getMitarbeiterFromLdap(qNumber);
            } catch (UserNotFoundException ex) {
                LOG.error(null, ex);
            }
        }
    }

    @Override
    public String invalidateSession() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (session != null) {
            session.invalidate();
        }
        return "dashboard?faces-redirect=true";
    }

    @Override
    public boolean userHasDatenschutzerklaerungAkzeptiert() {
        if (user == null) {
            return false;
        }
        return user.isDatenschutzAngenommen();
    }

    @Override
    public UserDefinedSearch getUserDefinedSearchById(Long id) {
        return userService.getUserDefinedSeachById(id);
    }

    @Override
    public List<UserDefinedSearch> getUserDefinedSearchByMitarbeiter() {
        return userService.getUserDefinedSearchByMitarbeiter(user);
    }

    @Override
    public List<UserDefinedSearch> getUserDefinedSearchByNameAndMitarbeiter(String udsName) {
        return userService.getUserDefinedSearchByNameAndMitarbeiter(udsName, user);
    }

    @Override
    public void saveUserDefinedSearch(UserDefinedSearch uds) {
        if (uds.getMitarbeiter() == null) {
            uds.setMitarbeiter(user);
        }
        userService.saveUserDefinedSearch(uds);
    }

    @Override
    public void updateUserDefinedSearch(UserDefinedSearch uds) {
        userService.updateUserDefinedSearch(uds);
    }

    @Override
    public void removeUserDefinedSearch(UserDefinedSearch uds) {
        userService.removeUserDefinedSearch(uds);
    }

    @Override
    @Produces
    public Mitarbeiter getUser() {
        return user;
    }

    @Override
    public void setUser(Mitarbeiter user) {
        this.user = user;
        updatePermissions();
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
    }

    @Override
    public String getUserAcronym() {
        return "" + getUser().getName().charAt(0) + getUser().getNachname().charAt(0);
    }

    @Override
    public boolean hasRole(Rolle role) {
        return userPermissions.hasRole(role);
    }

    @Override
    public UserPermissions getUserPermissions() {
        return userPermissions;
    }

}
