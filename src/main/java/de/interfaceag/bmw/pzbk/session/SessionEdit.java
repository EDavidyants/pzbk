package de.interfaceag.bmw.pzbk.session;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;

/**
 *
 * @author sl
 */
public interface SessionEdit extends Session {

    String invalidateSession();

    void setUser(Mitarbeiter user);

    boolean userHasDatenschutzerklaerungAkzeptiert();

}
