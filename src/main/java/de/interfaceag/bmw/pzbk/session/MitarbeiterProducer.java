package de.interfaceag.bmw.pzbk.session;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.Serializable;

@SessionScoped
public class MitarbeiterProducer implements Serializable {

    @Inject
    private Session session;

    @Current
    @Produces
    public Mitarbeiter getCurrentUser() {
        return session.getUser();
    }

}
