package de.interfaceag.bmw.pzbk.session;

import de.interfaceag.bmw.pzbk.entities.UserDefinedSearch;

import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface UserDefinedSearchApi {

    UserDefinedSearch getUserDefinedSearchById(Long id);

    List<UserDefinedSearch> getUserDefinedSearchByMitarbeiter();

    List<UserDefinedSearch> getUserDefinedSearchByNameAndMitarbeiter(String udsName);

    void saveUserDefinedSearch(UserDefinedSearch uds);

    void updateUserDefinedSearch(UserDefinedSearch uds);

    void removeUserDefinedSearch(UserDefinedSearch uds);

}
