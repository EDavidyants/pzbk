package de.interfaceag.bmw.pzbk.doors.migration;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author evda
 */
public class DoorsMigrationReport implements Serializable, Comparable<DoorsMigrationReport> {

    private final int rowIndex;
    private String fachId;
    private String status;
    private String kommentar;
    private String datum;

    public DoorsMigrationReport(int rowIndex, String fachId, String status, String kommentar, String datum) {
        this.rowIndex = rowIndex;
        this.fachId = fachId;
        this.status = status;
        this.kommentar = kommentar;
        this.datum = datum;
    }

    @Override
    public int compareTo(DoorsMigrationReport other) {
        return this.rowIndex - other.rowIndex;
    }

    @Override
    public String toString() {
        return fachId + ": " + kommentar;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hb = new HashCodeBuilder(31, 383);
        hb.append(fachId);
        hb.append(status);
        return hb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof DoorsMigrationReport)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        DoorsMigrationReport dmr = (DoorsMigrationReport) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(fachId, dmr.fachId);
        eb.append(status, dmr.status);
        return eb.isEquals();
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public String getFachId() {
        return fachId;
    }

    public String getStatus() {
        return status;
    }

    public String getKommentar() {
        return kommentar;
    }

    public String getDatum() {
        return datum;
    }

    public static DoorsMigrationReportBuilder newBuilder() {
        return new DoorsMigrationReportBuilder();
    }

    public static class DoorsMigrationReportBuilder {

        private int rowIndex;
        private String fachId;
        private String status;
        private String kommentar;
        private String datum;

        public DoorsMigrationReportBuilder() {

        }

        public DoorsMigrationReportBuilder withRowIndex(int rowIndex) {
            this.rowIndex = rowIndex;
            return this;
        }

        public DoorsMigrationReportBuilder withFachId(String fachId) {
            this.fachId = fachId;
            return this;
        }

        public DoorsMigrationReportBuilder withStatus(String status) {
            this.status = status;
            return this;
        }

        public DoorsMigrationReportBuilder withKommentar(String kommentar) {
            this.kommentar = kommentar;
            return this;
        }

        public DoorsMigrationReportBuilder withDatum(Date datum) {
            if (datum == null) {
                datum = new Date();
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            this.datum = sdf.format(datum);
            return this;
        }

        public DoorsMigrationReport build() {
            return new DoorsMigrationReport(rowIndex, fachId, status, kommentar, datum);
        }

    }

}
