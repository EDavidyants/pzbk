package de.interfaceag.bmw.pzbk.doors.migration;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.ReferenzSystemLink;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.entities.Zielwert;
import de.interfaceag.bmw.pzbk.enums.Kategorie;
import de.interfaceag.bmw.pzbk.enums.ReferenzSystem;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 *
 * @author evda
 */
@Stateless
@Named
public class DoorsAnforderungMigrationService implements Serializable {

    @Inject
    protected AnforderungService anforderungService;

    @Inject
    protected SensorCocService sensorCocService;

    @Inject
    protected ModulService modulService;

    @Inject
    protected TteamService tteamService;

    @Inject
    protected ExcelDownloadService excelDownloadService;

    @Inject
    protected BerechtigungService berechtigungsService;

    @Inject
    protected AnforderungMeldungHistoryService historyService;

    private static final Logger LOG = LoggerFactory.getLogger(DoorsAnforderungMigrationService.class.getName());

    private static final Map<String, Integer> COLUMNNAMETOCOLUMNINDEXMAP = new HashMap<>();

    static {
        COLUMNNAMETOCOLUMNINDEXMAP.put("fachId", 2);
        COLUMNNAMETOCOLUMNINDEXMAP.put("festgestelltIn", 7);
        COLUMNNAMETOCOLUMNINDEXMAP.put("beschreibungDe", 8);
        COLUMNNAMETOCOLUMNINDEXMAP.put("anforderungKommentar", 12);
        COLUMNNAMETOCOLUMNINDEXMAP.put("loesungsVorschlag", 13);
        COLUMNNAMETOCOLUMNINDEXMAP.put("phasenbezug", 14);
        COLUMNNAMETOCOLUMNINDEXMAP.put("sensorCoc", 16);
        COLUMNNAMETOCOLUMNINDEXMAP.put("status", 18);
        COLUMNNAMETOCOLUMNINDEXMAP.put("umsetzer", 20);
        COLUMNNAMETOCOLUMNINDEXMAP.put("vereinbarungstyp", 21);
        COLUMNNAMETOCOLUMNINDEXMAP.put("zielwert", 22);
        COLUMNNAMETOCOLUMNINDEXMAP.put("beschreibungEn", 24);
        COLUMNNAMETOCOLUMNINDEXMAP.put("ersteller", 27);
        COLUMNNAMETOCOLUMNINDEXMAP.put("referenzSystem", 41);
        COLUMNNAMETOCOLUMNINDEXMAP.put("referenzSystemLink", 42);
    }

    private String errMsg = "";

    public Workbook migrateDoorsAnforderungen(Sheet sheet) {
        LOG.info("Starting DOORS Anforderungen Migration ..");

        List<DoorsMigrationReport> result = new ArrayList<>();
        Row row;
        int rowCounter = 2;
        int lastRowIndex = sheet.getLastRowNum();

        String fachId;

        Anforderung anforderung;

        while (rowCounter <= lastRowIndex) {

            row = sheet.getRow(rowCounter);

            fachId = this.getExcelValueOfAttribute(row, "fachId");
            if (fachId.isEmpty()) {
                errMsg = "Severe: No FachId provided!";
                result.add(buildReportForFailedAnforderung(fachId, (rowCounter + 1)));
                rowCounter++;
                continue;
            }

            // try to find existing Anforderung in DB, assuming version = 1
            anforderung = anforderungService.getAnforderungByFachIdVersion(fachId, 1);
            if (anforderung != null) {
                // check if Anforderung is has a history and write an entry if needed
                String creationDateStr;
                AnforderungHistory anforderungHistory = getFirstAnforderungHistory(anforderung);
                if (anforderungHistory == null) {
                    // write to history
                    creationDateStr = writeAnforderungHistory(anforderung);

                } else {
                    creationDateStr = getAnforderungCreationDate(anforderungHistory);
                }

                result.add(buildReportForExistingAnforderung(fachId, (rowCounter + 1), creationDateStr));

            } else {
                errMsg = "";
                anforderung = buildAnforderungFromRow(row);

                if (anforderung != null && anforderung.getId() != null) {
                    // write to history
                    String creationDateStr = writeAnforderungHistory(anforderung);
                    result.add(buildReportForCreatedDoorsAnforderung(fachId, anforderung.getId(), (rowCounter + 1), creationDateStr));

                } else {
                    result.add(buildReportForFailedAnforderung(fachId, (rowCounter + 1)));
                }
            }

            rowCounter++;
        }

        LOG.info("DOORS Anforderungen Migration successfully finished: {} records processed", result.size());

        return createResultWorkbook(result);
    }

    private DoorsMigrationReport buildReportForExistingAnforderung(String fachId, int rowIndex, String creationDate) {

        String comment = "Anforderung " + fachId + " has already been migrated";
        if (creationDate != null) {
            comment = comment + " on " + creationDate;
        } else {
            comment = comment + " earlier";
        }

        DoorsMigrationReport dmr = DoorsMigrationReport.newBuilder()
                .withRowIndex(rowIndex)
                .withFachId(fachId)
                .withStatus("skipped")
                .withKommentar(comment)
                .withDatum(new Date())
                .build();
        return dmr;
    }

    private DoorsMigrationReport buildReportForCreatedDoorsAnforderung(String fachId, Long anfoId, int rowIndex, String creationDate) {
        String kommentar;
        String kommentarAd = "";
        if (creationDate != null) {
            kommentarAd = " on " + creationDate;
        }

        if (!errMsg.isEmpty()) {
            kommentar = "Anforderung with ID " + anfoId + " has been migrated" + kommentarAd + " with following errors:\n " + errMsg;
        } else {
            kommentar = "Anforderung with ID " + anfoId + " has been successfully migrated" + kommentarAd;
        }

        DoorsMigrationReport dmr = DoorsMigrationReport.newBuilder()
                .withRowIndex(rowIndex)
                .withFachId(fachId)
                .withStatus("successful")
                .withKommentar(kommentar)
                .withDatum(new Date())
                .build();
        return dmr;
    }

    private DoorsMigrationReport buildReportForFailedAnforderung(String fachId, int rowIndex) {
        DoorsMigrationReport dmr = DoorsMigrationReport.newBuilder()
                .withRowIndex(rowIndex)
                .withFachId(fachId)
                .withStatus("failed")
                .withKommentar("Anforderung migration failed: " + errMsg)
                .withDatum(new Date())
                .build();
        return dmr;
    }

    private Anforderung createDoorsAnforderung(String fachId, String festgestelltIn, String beschreibungDe, String anforderungKommentar,
            String loesungsVorschlag, String phasenbezug, String sensorCocTech, String statusBezeichnung,
            String umsetzer, String vereinbarungstyp, String zielwert, String beschreibungEn,
            String ersteller, String referenzSystem, String referenzSystemLink) {

        boolean isValid = validateFields(fachId, festgestelltIn, beschreibungDe, anforderungKommentar, loesungsVorschlag, phasenbezug, sensorCocTech, statusBezeichnung, umsetzer, vereinbarungstyp, zielwert, beschreibungEn, ersteller, referenzSystem, referenzSystemLink);

        if (!isValid) {
            return null;
        }

        Anforderung anforderung = new Anforderung(fachId, 1);

        anforderung.setKategorie(Kategorie.ZWEI);

        List<String> festgestelltInString = Arrays.asList(festgestelltIn.split(";."));
        Set<FestgestelltIn> festgestelltInList = parseFestgestelltIn(festgestelltInString);
        SensorCoc sensorCoc = parseSensorCoc(sensorCocTech);
        Status status = parseStatus(statusBezeichnung);
        String tteamName = "E-Antrieb - Integration";
        Tteam tteam = tteamService.getTteamByName(tteamName);

        if (checkAllValidData(sensorCocTech, statusBezeichnung, festgestelltInList, sensorCoc, status, tteamName, tteam)) {
            return null;
        }

        anforderung.setFestgestelltIn(festgestelltInList);

        anforderung.setBeschreibungAnforderungDe(beschreibungDe);
        anforderung.setKommentarAnforderung(anforderungKommentar);
        anforderung.setLoesungsvorschlag(loesungsVorschlag);
        anforderung.setPhasenbezug(parsePhasenbezug(phasenbezug));
        anforderung.setPotentialStandardisierung(true);
        anforderung.setSensorCoc(sensorCoc);
        anforderung.setStatus(status);
        anforderung.setTteam(tteam);

        // ModulSeTeam List
        List<ModulSeTeam> modulSeTeamList = new ArrayList<>();
        List<String> modulSeTeamStringList = Arrays.asList(umsetzer.split(",."));

        for (String modulSeTeamString : modulSeTeamStringList) {
            ModulSeTeam modulSeTeam = parseModulSeTeam(modulSeTeamString);
            if (modulSeTeam == null) {
                errMsg = errMsg + " Severe: Umsetzer '" + modulSeTeamString + "' does not exist in the system!; ";
                return null;
            }
            modulSeTeamList.add(modulSeTeam);
            Umsetzer newAnforderungUmsetzer = new Umsetzer(modulSeTeam);
            anforderung.addUmsetzer(newAnforderungUmsetzer);
        }

        anforderung.setZielwert(parseZielwert());

        anforderung.setBeschreibungAnforderungEn(beschreibungEn);
        anforderung.setErsteller(ersteller);

        ReferenzSystemLink refsyslink = new ReferenzSystemLink(ReferenzSystem.DOORS, referenzSystemLink);
        anforderung.setReferenzSystemLinks(Arrays.asList(refsyslink));

        Long anfoId = anforderungService.persistAnforderung(anforderung);
        anforderung = anforderungService.getAnforderungById(anfoId);

        for (ModulSeTeam modulSeT : modulSeTeamList) {
            AnforderungFreigabeBeiModulSeTeam newFreigabe = new AnforderungFreigabeBeiModulSeTeam(anforderung, modulSeT);
            newFreigabe.setVereinbarungType(VereinbarungType.STANDARD);
            anforderung.addAnfoFreigabeBeiModul(newFreigabe);
        }

        anforderung.setErstellungsdatum(new Date());
        anforderungService.persistAnforderung(anforderung);
        return anforderung;
    }

    private boolean checkAllValidData(String sensorCocTech, String statusBezeichnung, Set<FestgestelltIn> festgestelltInList, SensorCoc sensorCoc, Status status, String tteamName, Tteam tteam) {
        if (festgestelltInList.isEmpty()) {
            errMsg = errMsg + " Severe: No valid FestgestelltIn definiert; ";
            return true;
        }

        if (sensorCoc == null) {
            errMsg = errMsg + " Severe: SensorCoc Technologie '" + sensorCocTech + "' is not registered in the system!; ";
            return true;
        }
        if (status == null) {
            errMsg = errMsg + " Severe: No Status '" + statusBezeichnung + "' found in the system!; ";
            return true;
        }
        if (tteam == null) {
            errMsg = errMsg + " Severe: Could not find T-Team '" + tteamName + "' in the system!; ";
            return true;
        }
        return false;
    }

    private Anforderung buildAnforderungFromRow(Row row) {

        // excel values
        String fachId = getExcelValueOfAttribute(row, "fachId");
        String festgestelltIn = getExcelValueOfAttribute(row, "festgestelltIn");
        String beschreibungDe = getExcelValueOfAttribute(row, "beschreibungDe");
        String anforderungKommentar = getExcelValueOfAttribute(row, "anforderungKommentar");
        String loesungsVorschlag = getExcelValueOfAttribute(row, "loesungsVorschlag");
        String phasenbezug = getExcelValueOfAttribute(row, "phasenbezug");
        String sensorCoc = getExcelValueOfAttribute(row, "sensorCoc");
        String status = getExcelValueOfAttribute(row, "status");
        String umsetzer = getExcelValueOfAttribute(row, "umsetzer");
        String vereinbarungstyp = getExcelValueOfAttribute(row, "vereinbarungstyp");
        String zielwert = getExcelValueOfAttribute(row, "zielwert");
        String beschreibungEn = getExcelValueOfAttribute(row, "beschreibungEn");
//        String ersteller = getExcelValueOfAttribute(row, "ersteller");
        String ersteller = getErstellerForSensorCoc(sensorCoc); // Column is not known yet
        String referenzSystem = getExcelValueOfAttribute(row, "referenzSystem");
        String referenzSystemLink = getExcelValueOfAttribute(row, "referenzSystemLink");

        Anforderung anforderung = createDoorsAnforderung(fachId, festgestelltIn, beschreibungDe, anforderungKommentar, loesungsVorschlag, phasenbezug, sensorCoc, status, umsetzer, vereinbarungstyp, zielwert, beschreibungEn, ersteller, referenzSystem, referenzSystemLink);

        return anforderung;
    }

    private String getErstellerForSensorCoc(String sensorCocName) {
        SensorCoc sensorCoc = parseSensorCoc(sensorCocName);
        Optional<Mitarbeiter> mitarbeiter = berechtigungsService.getSensorCoCLeiterOfSensorCoc(sensorCoc);
        if (mitarbeiter.isPresent()) {
            return mitarbeiter.get().getName();
        } else {
            return "";
        }
    }

    private boolean validateFields(String fachId, String festgestelltIn, String beschreibungDe, String anforderungKommentar,
            String loesungsVorschlag, String phasenbezug, String sensorCoc, String status,
            String umsetzer, String vereinbarungstyp, String zielwert, String beschreibungEn,
            String ersteller, String referenzSystem, String referenzSystemLink) {

        boolean isValid = true;
        StringBuilder sb = new StringBuilder();

        isValid = checkFachId(fachId, sb, isValid);

        isValid = checkFestgestelltIn(festgestelltIn, sb, isValid);

        isValid = checkBeschreibungDe(beschreibungDe, sb, isValid);

        checkAnforderungKommentar(anforderungKommentar, sb);

        checkLoesungsVorschlag(loesungsVorschlag, sb);

        isValid = checkPhasenbezug(phasenbezug, sb, isValid);

        isValid = checkSensorCoc(sensorCoc, sb, isValid);

        isValid = checkStatus(status, sb, isValid);

        isValid = checkUmsetzer(umsetzer, sb, isValid);

        checkStd(vereinbarungstyp, sb);

        checkZielwert(zielwert, sb);

        checkBeschreibungEn(beschreibungEn, sb);

        checkErsteller(ersteller, sb);

        checkReferenzSystem(referenzSystem, sb);

        checkReferenzSystemLink(referenzSystemLink, sb);

        errMsg = sb.toString();
        return isValid;
    }

    private void checkReferenzSystemLink(String referenzSystemLink, StringBuilder sb) {
        if (referenzSystemLink.isEmpty()) {
            sb.append("!Info: Referenzsystem Link not provided!; ");
        }
    }

    private void checkReferenzSystem(String referenzSystem, StringBuilder sb) {
        if (referenzSystem.isEmpty() || !"DOORS".equals(referenzSystem)) {
            sb.append("!Info: Specified Referenzsystem is not DOORS, setting DOORS per default; ");
        }
    }

    private void checkErsteller(String ersteller, StringBuilder sb) {
        if (ersteller.isEmpty()) {
            sb.append("Info: No Information on Ersteller provided; ");
        }
    }

    private void checkBeschreibungEn(String beschreibungEn, StringBuilder sb) {
        if (beschreibungEn.isEmpty()) {
            sb.append("Info: BeschreibungEn not provided; ");
        }
    }

    private void checkZielwert(String zielwert, StringBuilder sb) {
        if (zielwert.isEmpty() || !"siehe Beschreibung".equals(zielwert)) {
            sb.append("Warning: Zielwert does not match predefined value, setting default one; ");
        }
    }

    private void checkStd(String vereinbarungstyp, StringBuilder sb) {
        if (!"STD".equals(vereinbarungstyp)) {
            sb.append("Warning: Vereinbarungstyp not STD, setting STD per default; ");
        }
    }

    private boolean checkUmsetzer(String umsetzer, StringBuilder sb, boolean isValid) {
        if (umsetzer.isEmpty()) {
            sb.append("Severe: Umsetzer not defined!; ");
            isValid = false;
        }
        return isValid;
    }

    private boolean checkStatus(String status, StringBuilder sb, boolean isValid) {
        if (status.isEmpty()) {
            sb.append("Severe: Status is missing!; ");
            isValid = false;
        }
        return isValid;
    }

    private boolean checkSensorCoc(String sensorCoc, StringBuilder sb, boolean isValid) {
        if (sensorCoc.isEmpty()) {
            sb.append("Severe: SensorCoc is missing!; ");
            isValid = false;
        }
        return isValid;
    }

    private boolean checkPhasenbezug(String phasenbezug, StringBuilder sb, boolean isValid) {
        if (phasenbezug.isEmpty() || !isPhasenbezugValid(phasenbezug)) {
            sb.append("Severe: Phasenbezug is missing or not a valid value!; ");
            isValid = false;
        }
        return isValid;
    }

    private void checkLoesungsVorschlag(String loesungsVorschlag, StringBuilder sb) {
        if (loesungsVorschlag.isEmpty()) {
            sb.append("Info: No Loesungsvorschlag provided; ");
        }
    }

    private void checkAnforderungKommentar(String anforderungKommentar, StringBuilder sb) {
        if (anforderungKommentar.isEmpty()) {
            sb.append("Info: No comment to Anforderung given; ");
        }
    }

    private boolean checkBeschreibungDe(String beschreibungDe, StringBuilder sb, boolean isValid) {
        if (beschreibungDe.isEmpty()) {
            sb.append("Severe: BeschreibungDe is missing!; ");
            isValid = false;
        }
        return isValid;
    }

    private boolean checkFestgestelltIn(String festgestelltIn, StringBuilder sb, boolean isValid) {
        if (festgestelltIn.isEmpty()) {
            sb.append("Severe: No FestgestelltIn provided!; ");
            isValid = false;
        }
        return isValid;
    }

    private boolean checkFachId(String fachId, StringBuilder sb, boolean isValid) {
        if (fachId.isEmpty()) {
            sb.append("Severe: No FachId provided!; ");
            isValid = false;
        }
        return isValid;
    }

    private Set<FestgestelltIn> parseFestgestelltIn(List<String> festgestelltInStringList) {
        Set<FestgestelltIn> festgestelltInList = new HashSet<>();
        for (String festgestelltInString : festgestelltInStringList) {

            FestgestelltIn festgestelltInValue = getFestgestelltInByWert(festgestelltInString);

            if (festgestelltInValue != null) {
                festgestelltInList.add(festgestelltInValue);
            } else {
                errMsg = errMsg + " Info: new FestgestelltIn " + festgestelltInString + " found .. adding to the system .. ";
                festgestelltInValue = new FestgestelltIn(festgestelltInString);
                anforderungService.persistFestgestelltIn(festgestelltInValue);
                festgestelltInValue = getFestgestelltInByWert(festgestelltInString);

                if (festgestelltInValue != null && festgestelltInValue.getId() != null) {
                    festgestelltInList.add(festgestelltInValue);
                    errMsg = errMsg + " successfully; ";
                } else {
                    errMsg = errMsg + " DB save failure !; ";
                }
            }
        }
        return festgestelltInList;
    }

    private FestgestelltIn getFestgestelltInByWert(String festgestelltInString) {
        List<FestgestelltIn> festgestelltInList = anforderungService.getFestgestelltInByWert(festgestelltInString);
        FestgestelltIn festgestelltIn = null;

        if (festgestelltInList != null && !festgestelltInList.isEmpty()) {
            festgestelltIn = festgestelltInList.get(0);
        }

        return festgestelltIn;
    }

    private boolean parsePhasenbezug(String phasenbezugString) {
        return "Konzeptrelevant".equals(phasenbezugString);
    }

    private SensorCoc parseSensorCoc(String sensorCocTechnologie) {
        SensorCoc sensorCoc = null;
        List<SensorCoc> sensorCocList = sensorCocService.getSensorCocsByTechnologie(sensorCocTechnologie);

        if (sensorCocList != null && !sensorCocList.isEmpty()) {
            sensorCoc = sensorCocList.get(0);
        }

        return sensorCoc;
    }

    private Status parseStatus(String statusBezeichnung) {
        return Status.getStatusByBezeichnung(statusBezeichnung);
    }

    private ModulSeTeam parseModulSeTeam(String modulSubmodulName) {
        if (!modulSubmodulName.contains(" / ")) {
            errMsg = errMsg + " !Info: Umsetzer must have format 'Modul / ModulSeTeam'; ";
            return null;
        }

        String[] modulSubmodul = modulSubmodulName.split(" / ");
        String modulName = modulSubmodul[0];
        String submodulName = modulSubmodul[1];

        Modul modul = modulService.getModulByName(modulName);
        List<ModulSeTeam> submoduls = modulService.getSeTeamsThatStartWith(submodulName, modul);

        ModulSeTeam modulSeTeam = (submoduls != null && !submoduls.isEmpty()) ? submoduls.get(0) : null;
        return modulSeTeam;

    }

    private Zielwert parseZielwert() {

        // zielwert for DOORS Anforderungen is predefined as 'siehe Beschreibung'
        String zielwertValue = "siehe Beschreibung";

        Zielwert zielwert = new Zielwert();
        zielwert.setWert(zielwertValue);
        return zielwert;
    }

    private String getExcelValueOfAttribute(Row row, String attrName) {
        Cell cell = row.getCell(COLUMNNAMETOCOLUMNINDEXMAP.get(attrName));
        return cell != null ? cell.getStringCellValue() : "";
    }

    private boolean isPhasenbezugValid(String phasenbezug) {
        return ("Konzeptrelevant".equals(phasenbezug) || "Architekturrelevant".equals(phasenbezug));
    }

    private Workbook createResultWorkbook(List<DoorsMigrationReport> reportData) {
        Workbook workbook = new XSSFWorkbook();

        String sheetName = "Migration_Result";
        Sheet sheet = workbook.createSheet(sheetName);

        if (reportData != null && !reportData.isEmpty()) {

            // sort data by row index
            Collections.sort(reportData);

            int rowIndex = 0;
            Row headerRow = sheet.createRow(rowIndex);

            Cell rowIndexCell = headerRow.createCell(0);
            rowIndexCell.setCellValue("Row");

            Cell fachIdCell = headerRow.createCell(1);
            fachIdCell.setCellValue("FachId");

            Cell statusCell = headerRow.createCell(2);
            statusCell.setCellValue("Status");

            Cell datumCell = headerRow.createCell(3);
            datumCell.setCellValue("Date");

            Cell kommentarCell = headerRow.createCell(4);
            kommentarCell.setCellValue("Comment");

            // style header
            CellStyle headerStyle = this.createHeaderCellStyle(workbook);
            rowIndexCell.setCellStyle(headerStyle);
            fachIdCell.setCellStyle(headerStyle);
            statusCell.setCellStyle(headerStyle);
            datumCell.setCellStyle(headerStyle);
            kommentarCell.setCellStyle(headerStyle);

            // style report data
            CellStyle statusFailureStyle = createStatusFailureStyle(workbook);
            CellStyle statusSkippedStyle = createStatusSkippedStyle(workbook);

            Row row;
            for (DoorsMigrationReport mr : reportData) {
                rowIndex++;
                row = sheet.createRow(rowIndex);

                Cell statusDataCell = row.createCell(2);

                row.createCell(0).setCellValue(mr.getRowIndex());
                row.createCell(1).setCellValue(mr.getFachId());
                statusDataCell.setCellValue(mr.getStatus());
                row.createCell(3).setCellValue(mr.getDatum());
                row.createCell(4).setCellValue(mr.getKommentar());

                // style status
                if (mr.getStatus().equals("failed")) {
                    statusDataCell.setCellStyle(statusFailureStyle);

                } else if (mr.getStatus().equals("skipped")) {
                    statusDataCell.setCellStyle(statusSkippedStyle);
                }
            }
        }

        return workbook;
    }

    public void downloadExcelMigrationResult(List<DoorsMigrationReport> reportData) {
        Workbook workbook = createResultWorkbook(reportData);
        excelDownloadService.downloadExcelExport("MigrationReport", workbook);
    }

    private CellStyle createHeaderCellStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 12);
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        style.setFont(font);
        return style;
    }

    private CellStyle createStatusFailureStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setColor(HSSFColor.RED.index);
        style.setFont(font);
        return style;
    }

    private CellStyle createStatusSkippedStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setColor(HSSFColor.DARK_YELLOW.index);
        style.setFont(font);
        return style;
    }

    private AnforderungHistory getFirstAnforderungHistory(Anforderung anforderung) {
        List<AnforderungHistory> anfoHistoryList = historyService.getAnforderungHistoryByObjectnameAnforderungId(anforderung.getId(), "A", "Anforderung");
        return (anfoHistoryList != null && !anfoHistoryList.isEmpty()) ? anfoHistoryList.get(0) : null;
    }

    private String getAnforderungCreationDate(AnforderungHistory anforderungHistory) {
        if (anforderungHistory == null) {
            return null;
        }

        if (anforderungHistory.getDatum() != null) {
            return parseDateToString(anforderungHistory.getDatum());
        }

        return null;
    }

    private String parseDateToString(Date datum) {
        if (datum == null) {
            datum = new Date();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return sdf.format(datum);
    }

    private String writeAnforderungHistory(Anforderung anforderung) {
        Date creationDate = new Date();

        AnforderungHistory anforderungHistory = new AnforderungHistory(anforderung.getId(), "A", creationDate,
                "Anforderung", anforderung.getErsteller(), "", "neu angelegt");
        AnforderungHistory anforderungHistoryStatus = new AnforderungHistory(anforderung.getId(), "A", creationDate,
                "Status", anforderung.getErsteller(), "", anforderung.getStatus().getStatusBezeichnung());
        historyService.persistAnforderungHistory(anforderungHistory);
        historyService.persistAnforderungHistory(anforderungHistoryStatus);

        return parseDateToString(creationDate);
    }

    public String getErrMsg() {
        return errMsg;
    }

    public AnforderungService getAnforderungService() {
        return anforderungService;
    }

    public void setAnforderungService(AnforderungService anforderungService) {
        this.anforderungService = anforderungService;
    }

    public SensorCocService getSensorCocService() {
        return sensorCocService;
    }

    public void setSensorCocService(SensorCocService sensorCocService) {
        this.sensorCocService = sensorCocService;
    }

    public ModulService getModulService() {
        return modulService;
    }

    public void setModulService(ModulService modulService) {
        this.modulService = modulService;
    }

    public TteamService getTteamService() {
        return tteamService;
    }

    public void setTteamService(TteamService tteamService) {
        this.tteamService = tteamService;
    }

    public ExcelDownloadService getExcelDownloadService() {
        return excelDownloadService;
    }

    public void setExcelDownloadService(ExcelDownloadService excelDownloadService) {
        this.excelDownloadService = excelDownloadService;
    }

    public AnforderungMeldungHistoryService getHistoryService() {
        return historyService;
    }

    public void setHistoryService(AnforderungMeldungHistoryService historyService) {
        this.historyService = historyService;
    }

}
