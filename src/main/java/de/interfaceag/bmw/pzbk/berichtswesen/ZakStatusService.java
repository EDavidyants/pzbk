package de.interfaceag.bmw.pzbk.berichtswesen;

import de.interfaceag.bmw.pzbk.services.ConfigService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 *
 * @author sl
 */
@Stateless
@Named
public class ZakStatusService implements Serializable {

    @Inject
    private ConfigService configService;

    public String buildZakUrl(String zakId) {
        // example url scheme
        // https://zak.bmwgroup.net/zielkat/zakbmwgroup.nsf/SORTttooluid/AB074C8ACB0F6498C12580D70036D977?opendocument&Sicht=

//        https://zaktest.bmwgroup.net/zielkat/ttoolss.nsf    zielkat/zakbmwgroup.nsf/SORTttooluid/61121?opendocument&Sicht=
        String targetPath = configService.getZakUrl();
        if (targetPath != null && !targetPath.isEmpty() && zakId != null) {
            return targetPath + "zakbmwgroup.nsf/SORTttooluid/" + zakId + "?opendocument&Sicht=";
        }
        return "";
    }

}
