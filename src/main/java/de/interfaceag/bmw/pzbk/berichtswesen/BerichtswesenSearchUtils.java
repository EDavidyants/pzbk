package de.interfaceag.bmw.pzbk.berichtswesen;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.filter.DerivatAnforderungModulIdFilter;
import de.interfaceag.bmw.pzbk.filter.FachIdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author fn
 */
final class BerichtswesenSearchUtils {

    private static final Logger LOG = LoggerFactory.getLogger(BerichtswesenSearchUtils.class.getName());

    private BerichtswesenSearchUtils() {
    }

    static QueryPartDTO getZakUebertragungByFilterAndDerivate(Boolean viewAll, List<Derivat> derivate, List<Derivat> selectedDerivate) {

        QueryPartDTO qp;
        qp = new QueryPartDTO("SELECT z FROM ZakUebertragung z "
                + "INNER JOIN z.derivatAnforderungModul AS dam "
                + "INNER JOIN dam.zuordnungAnforderungDerivat AS zda "
                + "INNER JOIN zda.anforderung AS a "
                + "INNER JOIN zda.derivat AS d "
                + "INNER JOIN dam.modul AS m "
                + "WHERE 1 = 1");
        if (!viewAll && derivate != null && !derivate.isEmpty()) {
            qp.append(" AND d IN :derivatList");
            qp.put("derivatList", derivate);
        }
        if (selectedDerivate != null && !selectedDerivate.isEmpty()) {
            qp.append(" AND d.id IN :derivatFilterList");
            qp.put("derivatFilterList", getLongList(selectedDerivate));
        }

        return qp;

    }

    static void getAnforderungQuery(QueryPartDTO qp, FachIdSearchFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND (");
            int i = 0;
            for (SearchFilterObject selectedValue : filter.getSelectedValues()) {
                qp.append(" LOWER(a.fachId) LIKE LOWER(:fachId" + i + ") OR");
                qp.put("fachId" + i, selectedValue.getName());
                i++;

            }
            qp.append(" 1=0)");
        }
    }

    static void getAenderungsDatumQuery(QueryPartDTO qp, DateSearchFilter dateSearchFilter) {

        if (dateSearchFilter.isActive()) {
            qp.append(" AND z.aenderungsdatum >= :dateStart");
            qp.put("dateStart", dateSearchFilter.getSelected());

        }
    }

    static void getBeschreibungQuery(QueryPartDTO qp, TextSearchFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND LOWER(a.beschreibungAnforderungDe) LIKE LOWER(:beschreibung)");
            qp.put("beschreibung", "%" + filter.getAsString() + "%");
        }
    }

    static void getHistoryQuery(QueryPartDTO qp, List<Long> historyIdList) {
        if (historyIdList == null || historyIdList.isEmpty()) {
            qp.append(" AND 1=0");
        } else {
            qp.append(" AND a.id IN :historyIdList");
            qp.put("historyIdList", historyIdList);
        }
    }

    static void getDerivatAnforderungModulModulQuery(QueryPartDTO qp, IdSearchFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND m.id IN :damModulIds");
            qp.put("damModulIds", filter.getSelectedIdsAsSet());
        }
    }

    static void getSensorCocQuery(QueryPartDTO qp, IdSearchFilter filter) {
        if (filter.isActive()) {
            qp.append(" AND a.sensorCoc.sensorCocId IN :sensorCocs");
            qp.put("sensorCocs", filter.getSelectedIdsAsSet());
        }
    }

    static void getDerivatAnforderungModulIdQuery(QueryPartDTO qp, DerivatAnforderungModulIdFilter filter) {
        if (filter.isActive()) {
            Set<Long> derivatAnforderungModulIds = filter.getIds();
            String daids = derivatAnforderungModulIds.stream().map(Object::toString).collect(Collectors.joining(", "));
            LOG.info("derivatAnforderungModul Filter is active. Build query part..with IDs: {}", daids);
            qp.append(" AND dam.id IN :derivatAnforderungModulIds");
            qp.put("derivatAnforderungModulIds", derivatAnforderungModulIds);
        }
    }

    private static List<Long> getLongList(List<Derivat> derivate) {
        List<Long> returnList = new ArrayList<>();
        derivate.forEach(derivat ->
                returnList.add(derivat.getId())
        );
        return returnList;
    }

    static void getBerichtswesenIdQuery(QueryPartDTO qp, List<Long> idList) {
        if (idList == null || idList.isEmpty()) {
            qp.append(" AND 1=0");
        } else {
            qp.append(" AND z.id IN :zIdList");
            qp.put("zIdList", idList);
        }
    }
}
