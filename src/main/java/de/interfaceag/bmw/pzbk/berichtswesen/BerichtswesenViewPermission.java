package de.interfaceag.bmw.pzbk.berichtswesen;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author fn
 */
public class BerichtswesenViewPermission implements Serializable {

    private final ViewPermission page;

    public BerichtswesenViewPermission(Set<Rolle> rolesWithWritePermissions) {

        page = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(rolesWithWritePermissions).get();
    }

    public boolean getPage() {
        return page.isRendered();
    }

}
