package de.interfaceag.bmw.pzbk.berichtswesen;

import de.interfaceag.bmw.pzbk.shared.components.AnforderungLink;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author fn
 */
public class BerichtswesenDTO implements AnforderungLink, Serializable {

    private final Long anforderungId;
    private final String anforderungFachId;
    private final Integer anforderungVersion;
    private final String anforderungBeschreibungDE;
    private final String modulName;
    private final String zakKommentar;
    private final String zakResponse;
    private final String rowKey;
    private final String zakId;
    private final List<Long> derivateIds;
    private final Map<Long, String> mapForStatus;
    private final String ersteller;
    private final String aenderungsDatum;

    public BerichtswesenDTO(Long anforderungId, String anforerungFachId,
                            Integer anforderungVersion, String anforderungBeschreibungDE,
                            String modulName, String zakId,
                            String zakKommentar, String rowKey, List<Long> derivateIds,
                            Map<Long, String> mapForStatus, String zakResponse,
                            String ersteller, String aenderungsDatum) {


        this.anforderungId = anforderungId;
        this.anforderungFachId = anforerungFachId;
        this.anforderungVersion = anforderungVersion;
        this.anforderungBeschreibungDE = anforderungBeschreibungDE;
        this.modulName = modulName;
        this.zakId = zakId;
        this.zakKommentar = zakKommentar;
        this.rowKey = rowKey;
        this.derivateIds = derivateIds;
        this.mapForStatus = mapForStatus;
        this.zakResponse = zakResponse;
        this.ersteller = ersteller;
        this.aenderungsDatum = aenderungsDatum;
    }

    public String getZakResponse() {
        return zakResponse;
    }

    @Override
    public String getId() {
        return Long.toString(anforderungId);
    }

    @Override
    public String getFachId() {
        return anforderungFachId;
    }

    @Override
    public String getVersion() {
        return anforderungVersion.toString();
    }

    public String getErsteller() {
        return ersteller;
    }

    public String getAenderungsDatum() {
        return aenderungsDatum;
    }

    public String getZakId() {
        return zakId;
    }

    public List<Long> getDerivateIds() {
        return derivateIds;
    }

    public Map<Long, String> getMapForStatus() {
        return mapForStatus;
    }

    public Long getAnforderungId() {
        return anforderungId;
    }

    public Integer getAnforderungVersion() {
        return anforderungVersion;
    }

    public String getAnforderungBeschreibungDE() {
        return anforderungBeschreibungDE;
    }

    public String getModulName() {
        return modulName;
    }

    public String getZakKommentar() {
        return zakKommentar;
    }

    public String getRowKey() {
        return rowKey;
    }

}
