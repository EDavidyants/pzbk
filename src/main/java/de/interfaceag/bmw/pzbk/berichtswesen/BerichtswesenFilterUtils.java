package de.interfaceag.bmw.pzbk.berichtswesen;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author fn
 */
final class BerichtswesenFilterUtils {

    private BerichtswesenFilterUtils() {
    }

    static List<BerichtswesenStatusFilter> getFilterForDerivatListAndUrlParamter(
            List<Derivat> derivate, UrlParameter urlParameter) {

        if (derivate != null) {
            return derivate.stream()
                    .map(derivat -> getFilterForDerivatAndUrlParamter(derivat, urlParameter))
                    .collect(Collectors.toList());
        }

        return new ArrayList<>();
    }

    private static BerichtswesenStatusFilter getFilterForDerivatAndUrlParamter(
            Derivat derivat, UrlParameter urlParameter) {
        return new BerichtswesenStatusFilter(derivat.getId(), derivat.getName(), urlParameter);
    }
}
