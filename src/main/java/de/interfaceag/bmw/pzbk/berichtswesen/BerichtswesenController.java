package de.interfaceag.bmw.pzbk.berichtswesen;

import de.interfaceag.bmw.pzbk.controller.dialogs.CommentDialogController;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sl
 */
@ViewScoped
@Named
public class BerichtswesenController implements Serializable {

    private BerichtswesenViewPermission viewPermission;

    @Inject
    private BerichtswesenViewFacade facade;

    private BerichtswesenViewData berichtswesenViewData;

    @Inject
    private Session session;

    private CommentDialogController commentDialogController;

    // TODO refactor this
    // better to use css/js to determine which Beschreibung is rendered
    private HashMap<Long, Boolean> beschreibungRenderMap; // map which is used to determine the current state of the beschreibung (collapsed or full)

    // TODO why do we use object[] here? refactor this
    private Object[] selectedAnforderungModul;

    // ---------- init methods -------------------------------------------------
    @PostConstruct
    public void init() {

        session.setLocationForView();

        viewPermission = new BerichtswesenViewPermission(session.getUserPermissions().getRoles());

        initViewData();

        initBeschreibungRenderMap();
        commentDialogController = new CommentDialogController("ZAK Response");

    }

    private void initViewData() {
        berichtswesenViewData = facade.initViewData(session.getUser(), UrlParameterUtils.getUrlParameter());
    }

    public BerichtswesenViewData getViewData() {
        return berichtswesenViewData;
    }

    public BerichtswesenViewFilter getBerichtswesenViewFilter() {
        return berichtswesenViewData.getFilter();
    }

    public String reset() {
        return getBerichtswesenViewFilter().getResetUrl();
    }

    public String filter() {
        return getBerichtswesenViewFilter().getUrl();
    }

    public void setUrl(String url) {
        // do nothing
    }

    public List<BerichtswesenDTO> getTableData() {
        return getViewData().getBerichtswesenDTO();
    }

    public String getUrl() {
        return facade.getUrl(getViewData());
    }

    private void initBeschreibungRenderMap() {
        beschreibungRenderMap = new HashMap();
        berichtswesenViewData.getZakUebertragungen().forEach(u -> beschreibungRenderMap.put(u.getId(), false));
    }

    // ---------- frontend methods ---------------------------------------------
    // todo refactor this (sh. oben)
    public void toggleBeschreibung(Long id) {
        Boolean current = getBeschreibungFullRenderCondition(id);
        beschreibungRenderMap.remove(id);
        beschreibungRenderMap.put(id, !current);
    }

    public Boolean getBeschreibungFullRenderCondition(Long id) {
        return beschreibungRenderMap.get(id);
    }

    public String buildZakUrl(String zakId) {
        return facade.buildZakUrl(zakId);
    }

    public void showCommentDialog(Object[] sam) {
        selectedAnforderungModul = sam.clone();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('zakCommentDialog').show()");
    }

    public String updateDerivatList() {
        facade.updateUserDerivate(getViewData().getFilteredDerivate());
        return filter();
    }

    // ---------- methods ------------------------------------------------------
    // TODO move to service class
    public void postProcessXLS(Object document) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        wb.removeSheetAt(0);
        Sheet sheet = wb.createSheet("ZAK Status");
        Row headerRow = sheet.createRow(0);
        headerRow.createCell(0).setCellValue("Anforderung");
        headerRow.createCell(1).setCellValue("Modul");
        headerRow.createCell(2).setCellValue("ZAK-ID");
        headerRow.createCell(3).setCellValue("Beschreibung");

        if (berichtswesenViewData.getBerichtswesenDTO() != null && !berichtswesenViewData.getBerichtswesenDTO().isEmpty()) {

            addAllSelectetDerivatToHeaderRow(headerRow);

            addBerichtswesenViewDataToCells(sheet);

        }
    }

    private void addBerichtswesenViewDataToCells(Sheet sheet) {
        Map<Long, String> statusList;
        int i = 0;
        for (BerichtswesenDTO berichtswesenDTO : berichtswesenViewData.getBerichtswesenDTO()) {
            Row row = sheet.createRow(i + 1);

            row.createCell(0).setCellValue(berichtswesenDTO.getFachId() + " | V" + berichtswesenDTO.getAnforderungVersion());
            row.createCell(1).setCellValue(berichtswesenDTO.getModulName());
            row.createCell(2).setCellValue(berichtswesenDTO.getZakId());
            row.createCell(3).setCellValue(berichtswesenDTO.getAnforderungBeschreibungDE());
            statusList = berichtswesenDTO.getMapForStatus();

            addCellDataFromAllSelectedDerivat(statusList, row);
            i++;
        }
    }

    private void addCellDataFromAllSelectedDerivat(Map<Long, String> statusList, Row row) {
        if (berichtswesenViewData.getFilteredDerivate() != null && !berichtswesenViewData.getFilteredDerivate().isEmpty()) {
            for (int j = 0; j < berichtswesenViewData.getFilteredDerivate().size(); j++) {
                Derivat derivat = berichtswesenViewData.getFilteredDerivate().get(j);
                if (statusList.containsKey(derivat.getId())) {
                    String status = statusList.get(derivat.getId());
                    row.createCell(j + 4).setCellValue(status);
                }
            }
        }
    }

    private void addAllSelectetDerivatToHeaderRow(Row headerRow) {
        if (berichtswesenViewData.getFilteredDerivate() != null && !berichtswesenViewData.getFilteredDerivate().isEmpty()) {
            for (int j = 0; j < berichtswesenViewData.getFilteredDerivate().size(); j++) {
                Derivat derivat = berichtswesenViewData.getFilteredDerivate().get(j);
                headerRow.createCell(j + 4).setCellValue(derivat.getName());
            }
        }
    }


    public Object[] getSelectedAnforderungModul() {
        return selectedAnforderungModul.clone();
    }

    public void setSelectedAnforderungModul(Object[] selectedAnforderungModul) {
        this.selectedAnforderungModul = selectedAnforderungModul.clone();
    }

    public CommentDialogController getCommentDialogController() {
        return commentDialogController;
    }

    public void setCommentDialogController(CommentDialogController commentDialogController) {
        this.commentDialogController = commentDialogController;
    }

    public boolean isAllSelected() {
        return getViewData().isAllSelected();
    }

    public void setAllSelected(boolean allSelected) {
        this.getViewData().setAllSelected(allSelected);
    }

    public void setSelectedDerivat(Derivat selectedDerivat) {
        this.getViewData().setSelectedDerivat(selectedDerivat);
    }

    public Derivat getSelectedDerivat() {
        return getViewData().getSelectedDerivat();
    }

    public BerichtswesenViewPermission getViewPermission() {
        return viewPermission;
    }

    public List<BerichtswesenDTO> getSelectedTableData() {
        return getViewData().getSelectedTableData();
    }

    public void setSelectedTableData(List<BerichtswesenDTO> selectedTableData) {
        this.getViewData().setSelectedTableData(selectedTableData);
    }

}
