package de.interfaceag.bmw.pzbk.berichtswesen;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.LogUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author fn
 */
@Stateless
public class BerichtswesenViewFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(BerichtswesenViewFacade.class.getName());

    @Inject
    private Session session;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private ModulService modulService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private BerichtswesenSearchService berichtswesenSearchService;
    @Inject
    private ConfigService configService;
    @Inject
    private ZakStatusService zakStatusService;

    private BerichtswesenViewFilter getViewFilter(UrlParameter urlParameter, Mitarbeiter currentUser) {

        LOG.info("Start getViewFilter");
        Date startDate = new Date();
        List<Derivat> selectedDerivate = derivatService.getUserSelectedDerivate(currentUser);
        List<SensorCoc> allSensorCocs = berechtigungService.getSensorCocForMitarbeiter(currentUser, true);

        List<Modul> allModule = modulService.getAllModule();        

        LOG.info("End getViewFilter");
        Date endDate = new Date();
        LogUtils.logDiff(startDate, endDate);

        return new BerichtswesenViewFilter(urlParameter, allSensorCocs, selectedDerivate, allModule);
    }

    public BerichtswesenViewData initViewData(Mitarbeiter currentUser, UrlParameter urlParameter) {

        BerichtswesenViewFilter viewFilter = getViewFilter(urlParameter, currentUser);

        List<Derivat> allDerivateForUser = berechtigungService.getDerivateForCurrentUser();
        List<Derivat> selectedDerivate = derivatService.getUserSelectedDerivate(currentUser);

        List<ZakUebertragung> zakUebertragungen = berichtswesenSearchService.getZakUebertragungByFilterAndDerivate(true, allDerivateForUser, selectedDerivate, viewFilter);

        BerichtswesenViewData returnViewData = berichtswesenSearchService.getViewDateFromFilter(zakUebertragungen);
        returnViewData.setDerivate(allDerivateForUser);
        returnViewData.setFilteredDerivate(selectedDerivate);
        returnViewData.setBerichtswesenViewFilter(viewFilter);
        returnViewData.setZakUebertragungen(zakUebertragungen);

        return returnViewData;
    }

    public void updateUserDerivate(List<Derivat> selectedDerivate) {
        Mitarbeiter currentUser = session.getUser();
        derivatService.setUserSelectedDerivate(currentUser, selectedDerivate);
    }

    public String getUrl(BerichtswesenViewData viewData) {
        return configService.getUrlPrefix() + "/" + viewData.getFilter().getUrl();
    }

    public String buildZakUrl(String zakId) {
        return zakStatusService.buildZakUrl(zakId);
    }

}
