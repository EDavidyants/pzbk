package de.interfaceag.bmw.pzbk.berichtswesen;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.filter.DerivatAnforderungModulIdFilter;
import de.interfaceag.bmw.pzbk.filter.FachIdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ModulFilter;
import de.interfaceag.bmw.pzbk.filter.SelectedDerivatUrlFilter;
import de.interfaceag.bmw.pzbk.filter.SensorCocFilter;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.filter.VonDateFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author fn
 */
public class BerichtswesenViewFilter implements Serializable {

    private static final Page PAGE = Page.BERICHTSWESEN;

    private final IdSearchFilter sensorCocFilter;

    private final IdSearchFilter modulFilter;

    private final DateSearchFilter freigegebenSeitFilter;

    private final DateSearchFilter geaendertSeitFilter;

    private final List<BerichtswesenStatusFilter> berichtswesenStatusFilter;

    private final FachIdSearchFilter anforderungFilter;

    private final TextSearchFilter beschreibungFilter;

    private final SelectedDerivatUrlFilter selectedDerivatFilter;

    private final DerivatAnforderungModulIdFilter derivatAnforderungModulIdFilter;

    public BerichtswesenViewFilter(
            UrlParameter urlParameter,
            List<SensorCoc> allSensorCocs,
            List<Derivat> allDerivate,
            List<Modul> allModule) {

        anforderungFilter = new FachIdSearchFilter(urlParameter);

        beschreibungFilter = new TextSearchFilter("beschreibung", urlParameter);
        sensorCocFilter = new SensorCocFilter(allSensorCocs, urlParameter);

        modulFilter = new ModulFilter(allModule, urlParameter);

        derivatAnforderungModulIdFilter = new DerivatAnforderungModulIdFilter(urlParameter);

        this.freigegebenSeitFilter = new VonDateFilter("freigabe")
                .parseParameter(urlParameter.getValue("freigabe" + VonDateFilter.PARAMETERNAME));

        this.geaendertSeitFilter = new VonDateFilter("geaendert")
                .parseParameter(urlParameter.getValue("geaendert" + VonDateFilter.PARAMETERNAME));

        berichtswesenStatusFilter = BerichtswesenFilterUtils
                .getFilterForDerivatListAndUrlParamter(allDerivate, urlParameter);
        selectedDerivatFilter = new SelectedDerivatUrlFilter(urlParameter);
    }

    private String getUrlParameter() {

        StringBuilder sb = new StringBuilder("?faces-redirect=true");

        sb.append(sensorCocFilter.getIndependentParameter());
        sb.append(modulFilter.getIndependentParameter());
        sb.append(freigegebenSeitFilter.getIndependentParameter());
        sb.append(geaendertSeitFilter.getIndependentParameter());
        sb.append(anforderungFilter.getIndependentParameter());
        sb.append(beschreibungFilter.getIndependentParameter());
        sb.append(selectedDerivatFilter.getIndependentParameter());
        sb.append(derivatAnforderungModulIdFilter.getIndependentParameter());

        getBerichtswesenStatusFilter().forEach(f -> sb.append(f.getParameter()));
        return sb.toString();
    }

    public String getUrl() {
        return PAGE.getUrl() + getUrlParameter();
    }

    public String getResetUrl() {
        return PAGE.getUrl() + "?faces-redirect=true";
    }

    public List<BerichtswesenStatusFilter> getBerichtswesenStatusFilter() {
        return berichtswesenStatusFilter;
    }

    public IdSearchFilter getSensorCocFilter() {
        return sensorCocFilter;
    }

    public IdSearchFilter getModulFilter() {
        return modulFilter;
    }

    public DateSearchFilter getFreigegebenSeitFilter() {
        return freigegebenSeitFilter;
    }

    public DateSearchFilter getGeaendertSeitFilter() {
        return geaendertSeitFilter;
    }

    public FachIdSearchFilter getAnforderungFilter() {
        return anforderungFilter;
    }

    public TextSearchFilter getBeschreibungFilter() {
        return beschreibungFilter;
    }

    public SelectedDerivatUrlFilter getSelectedDerivatFilter() {
        return selectedDerivatFilter;
    }

    public DerivatAnforderungModulIdFilter getDerivatAnforderungModulIdFilter() {
        return derivatAnforderungModulIdFilter;
    }

}
