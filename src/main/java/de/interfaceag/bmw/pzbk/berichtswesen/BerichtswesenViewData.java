package de.interfaceag.bmw.pzbk.berichtswesen;

import de.interfaceag.bmw.pzbk.converters.DerivatConverter;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fn
 */
public class BerichtswesenViewData implements Serializable {

    private BerichtswesenViewFilter berichtswesenViewFilter;

    private List<BerichtswesenDTO> berichtswesenDTO;
    private List<BerichtswesenDTO> selectedBerichtswesenDTO = new ArrayList<>();

    private List<Derivat> filteredDerivate;
    private List<Derivat> derivate;
    private Derivat selectedDerivat;

    private List<ZakUebertragung> zakUebertragungen;

    private boolean allSelected;

       public BerichtswesenViewData(List<BerichtswesenDTO> berichtswesenDTO) {

        this.berichtswesenDTO = berichtswesenDTO;
    }

    public List<BerichtswesenDTO> getBerichtswesenDTO() {
        return berichtswesenDTO;
    }

    public List<Derivat> getFilteredDerivate() {
        return filteredDerivate;
    }

    public void setFilteredDerivate(List<Derivat> filteredDerivate) {
        this.filteredDerivate = filteredDerivate;
    }

    public List<Derivat> getDerivate() {
        return derivate;
    }

    public void setDerivate(List<Derivat> derivate) {
        this.derivate = derivate;
    }

    public BerichtswesenViewFilter getFilter() {
        return berichtswesenViewFilter;
    }

    public void setBerichtswesenViewFilter(BerichtswesenViewFilter berichtswesenViewFilter) {
        this.berichtswesenViewFilter = berichtswesenViewFilter;
    }

    public List<BerichtswesenDTO> getSelectedTableData() {
        return selectedBerichtswesenDTO;
    }

    public void setSelectedTableData(List<BerichtswesenDTO> selectedBerichtswesenDTO) {
        this.selectedBerichtswesenDTO = selectedBerichtswesenDTO;
    }

    public boolean isAllSelected() {
        return allSelected;
    }

    public void setAllSelected(boolean allSelected) {
        this.allSelected = allSelected;
    }

    public Derivat getSelectedDerivat() {
        return selectedDerivat;
    }

    public void setSelectedDerivat(Derivat selectedDerivat) {
        if (selectedDerivat != null) {
            this.selectedDerivat = selectedDerivat;
            berichtswesenViewFilter.getSelectedDerivatFilter().setAttributeValue(selectedDerivat.getId().toString());
        }
    }

    public DerivatConverter getDerivatConverter() {
        return new DerivatConverter(getDerivate());
    }

    public List<ZakUebertragung> getZakUebertragungen() {
        return zakUebertragungen;
    }

    public void setZakUebertragungen(List<ZakUebertragung> zakUebertragungen) {
        this.zakUebertragungen = zakUebertragungen;
    }

}
