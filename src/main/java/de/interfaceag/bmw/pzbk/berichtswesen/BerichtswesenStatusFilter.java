package de.interfaceag.bmw.pzbk.berichtswesen;

import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ZakStatusFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;

/**
 *
 * @author fn
 */
public class BerichtswesenStatusFilter implements Serializable {

    private final Long derivatId;
    private final String derivatName;

    private final MultiValueEnumSearchFilter zakStatusFilter;

    public BerichtswesenStatusFilter(
            Long derivatId,
            String derivatName,
            UrlParameter urlParameter) {
        this.derivatName = derivatName;
        this.derivatId = derivatId;

        this.zakStatusFilter = new ZakStatusFilter(urlParameter, derivatName);
    }

    public String getParameter() {
        StringBuilder sb = new StringBuilder();
        sb.append(zakStatusFilter.getIndependentParameter());
        return sb.toString();
    }

    public MultiValueEnumSearchFilter getZakStatusFilter() {
        return zakStatusFilter;
    }

    public String getDerivatName() {
        return derivatName;
    }

    public Long getDerivatId() {
        return derivatId;
    }

}
