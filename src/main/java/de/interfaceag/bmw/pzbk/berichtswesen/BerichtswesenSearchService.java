package de.interfaceag.bmw.pzbk.berichtswesen;

import de.interfaceag.bmw.pzbk.dao.SearchDao;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import de.interfaceag.bmw.pzbk.shared.dto.SearchFilterDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author sl
 */
@Stateless
public class BerichtswesenSearchService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(BerichtswesenSearchService.class.getName());

    @Inject
    private SearchDao searchDao;

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager em;

    public List<ZakUebertragung> getZakUebertragungByFilterAndDerivate(Boolean viewAll, List<Derivat> derivate, List<Derivat> selectedDerivate, BerichtswesenViewFilter viewFilter) {

        QueryPartDTO qp = BerichtswesenSearchUtils.getZakUebertragungByFilterAndDerivate(viewAll, derivate, selectedDerivate);

        BerichtswesenSearchUtils.getAnforderungQuery(qp, viewFilter.getAnforderungFilter());

        BerichtswesenSearchUtils.getBeschreibungQuery(qp, viewFilter.getBeschreibungFilter());

        getHistoryQueryPart(qp, viewFilter.getFreigegebenSeitFilter());

        BerichtswesenSearchUtils.getAenderungsDatumQuery(qp, viewFilter.getGeaendertSeitFilter());

        BerichtswesenSearchUtils.getDerivatAnforderungModulModulQuery(qp, viewFilter.getModulFilter());

        BerichtswesenSearchUtils.getSensorCocQuery(qp, viewFilter.getSensorCocFilter());

        BerichtswesenSearchUtils.getDerivatAnforderungModulIdQuery(qp, viewFilter.getDerivatAnforderungModulIdFilter());

        getBerichtswesenIdQueryPart(qp, viewFilter.getBerichtswesenStatusFilter());

        Query q = buildDerivatAnforderungModulQuery(qp);

        return q.getResultList();
    }

    private Query buildDerivatAnforderungModulQuery(QueryPartDTO qp) {
        Query q = em.createQuery(qp.getQuery(), ZakUebertragung.class);
        qp.getParameterMap().entrySet().forEach(p ->
                q.setParameter(p.getKey(), p.getValue())
        );
        return q;
    }

    private void getBerichtswesenIdQueryPart(QueryPartDTO qp, List<BerichtswesenStatusFilter> berichtswesenStatusFilters) {
        if (berichtswesenStatusFilters.stream().anyMatch(f -> f.getZakStatusFilter().isActive())) {
            LOG.info("BerichtswesenIdFilter is active");
            List<Long> idList = getBerichtswesenIdListForDerivatAnforderungFilter(berichtswesenStatusFilters);
            BerichtswesenSearchUtils.getBerichtswesenIdQuery(qp, idList);
        }
    }

    private List<Long> getBerichtswesenIdListForDerivatAnforderungFilter(
            List<BerichtswesenStatusFilter> derivatVereinbarungStatusFilters) {

        Set<Long> columns = new HashSet<>(); // derivatIds

        Map<Long, List<ZakStatus>> zakStatus = new HashMap<>();

        for (BerichtswesenStatusFilter f : derivatVereinbarungStatusFilters) {
            if (f.getZakStatusFilter().isActive()) {
                zakStatus.put(f.getDerivatId(), f.getZakStatusFilter().getAsEnumList());
                columns.add(f.getDerivatId());
            }
        }

        List<List<Long>> columnResults = new ArrayList<>();
        for (Long derivatId : columns) {
            columnResults.add(getDerivatAnforderungModulIdForDerivatColumn(derivatId,
                    zakStatus.get(derivatId)));
        }

        List<Long> result = new ArrayList<>();
        columnResults.forEach(result::addAll);
        columnResults.forEach(result::retainAll);
        return result;
    }

    private List<Long> getDerivatAnforderungModulIdForDerivatColumn(Long derivatId,
                                                                    List<ZakStatus> zakStatus) {
        Map<String, Object> parameterMap = new HashMap<>();
        StringBuilder sb = new StringBuilder();

        sb.append("SELECT DISTINCT da.vereinbarung.id FROM DerivatAnforderungModul da "
                + "WHERE da.zuordnungAnforderungDerivat.derivat.id = :derivatId");

        parameterMap.put("derivatId", derivatId);

        if (zakStatus != null && zakStatus.size() < 10) {
            if (zakStatus.isEmpty()) {
                sb.append(" AND 1 = 0");
            } else {
                String zakStatusParameter = "zakStatus";
                sb.append(" AND da.zakStatus IN :").append(zakStatusParameter);
                parameterMap.put(zakStatusParameter, zakStatus);
            }
        }

        Query q = em.createQuery(sb.toString(), Long.class);
        parameterMap.entrySet().forEach(p -> q.setParameter(p.getKey(), p.getValue()));
        return q.getResultList();
    }

    public BerichtswesenViewData getViewDateFromFilter(List<ZakUebertragung> zakUebertragungen) {

        List<ZakUebertragung> zakUebertragungList = new ArrayList<>();
        Map<Long, List<Long>> anforderungModulMap = new HashMap<>();
        Map<Long, List<Long>> anforderungDerivatMap = new HashMap<>();
        Map<Long, Map<Long, Map<Long, String>>> anforderungModulDerivatStatusMap = new HashMap<>();

        if (zakUebertragungen != null) {
            zakUebertragungen.forEach(zakubertragung -> {
                if (zakubertragung.getDerivatAnforderungModul().getModul() != null
                        && zakubertragung.getDerivatAnforderungModul() != null
                        && zakubertragung.getDerivatAnforderungModul().getAnforderung() != null
                        && zakubertragung.getDerivatAnforderungModul().getDerivat() != null) {
                    Long modulId = zakubertragung.getDerivatAnforderungModul().getModul().getId();
                    Long anforderungId = zakubertragung.getDerivatAnforderungModul().getAnforderung().getId();
                    Long derivatId = zakubertragung.getDerivatAnforderungModul().getDerivat().getId();

                    addModulToModulMap(anforderungModulMap, anforderungId, modulId);

                    zakUebertragungList.add(zakubertragung);

                    addDerivatIdToDerivatMap(anforderungDerivatMap, anforderungId, derivatId);

                    addDataToAnforderungModulDerivatStatusMap(anforderungModulDerivatStatusMap, anforderungId, derivatId, zakubertragung, modulId);
                }
            });
        }

        List<BerichtswesenDTO> result = new ArrayList<>();

        convertAllZakUebertragungenToBerichtswesenDto(zakUebertragungList, anforderungDerivatMap, anforderungModulDerivatStatusMap, result);

        return new BerichtswesenViewData(result);
    }

    private void addDataToAnforderungModulDerivatStatusMap(Map<Long, Map<Long, Map<Long, String>>> anforderungModulDerivatStatusMap, Long anforderungId, Long derivatId, ZakUebertragung zakubertragung, Long modulId) {
        if (!anforderungModulDerivatStatusMap.keySet().contains(anforderungId)) {
            Map<Long, String> ms = new HashMap<>();
            Map<Long, Map<Long, String>> m = new HashMap<>();
            ms.put(derivatId, zakubertragung.getZakStatus().getStatusBezeichnung());
            m.put(modulId, ms);
            anforderungModulDerivatStatusMap.put(anforderungId, m);
        } else {
            Map<Long, Map<Long, String>> m = anforderungModulDerivatStatusMap.get(anforderungId);
            if (!m.keySet().contains(modulId)) {
                Map<Long, String> ms = new HashMap<>();
                ms.put(derivatId, zakubertragung.getZakStatus().getStatusBezeichnung());
                m.put(modulId, ms);
                anforderungModulDerivatStatusMap.put(anforderungId, m);
            } else {
                Map<Long, String> ms = m.get(modulId);
                if (!ms.keySet().contains(derivatId)) {
                    ms.put(derivatId, zakubertragung.getZakStatus().getStatusBezeichnung());
                    m.put(modulId, ms);
                    anforderungModulDerivatStatusMap.put(anforderungId, m);
                }
            }
        }
    }

    private void addDerivatIdToDerivatMap(Map<Long, List<Long>> anforderungDerivatMap, Long anforderungId, Long derivatId) {
        if (!anforderungDerivatMap.keySet().contains(anforderungId)) {
            List<Long> l = new ArrayList<>();
            l.add(derivatId);
            anforderungDerivatMap.put(anforderungId, l);
        } else if (!anforderungDerivatMap.get(anforderungId).contains(derivatId)) {
            List<Long> l = anforderungDerivatMap.get(anforderungId);
            l.add(derivatId);
            anforderungDerivatMap.put(anforderungId, l);
        }
    }

    private void addModulToModulMap(Map<Long, List<Long>> anforderungModulMap, Long anforderungId, Long modulId) {
        if (!anforderungModulMap.keySet().contains(anforderungId)) {
            List<Long> l = new ArrayList<>();
            l.add(modulId);
            anforderungModulMap.put(anforderungId, l);
        } else if (!anforderungModulMap.get(anforderungId).contains(modulId)) {
            List<Long> l = anforderungModulMap.get(anforderungId);
            l.add(modulId);
            anforderungModulMap.put(anforderungId, l);
        }
    }

    private void convertAllZakUebertragungenToBerichtswesenDto(List<ZakUebertragung> zakUebertragungList, Map<Long, List<Long>> anforderungDerivatMap, Map<Long, Map<Long, Map<Long, String>>> anforderungModulDerivatStatusMap, List<BerichtswesenDTO> result) {
        zakUebertragungList.forEach(zakUbertragung -> {
            BerichtswesenDTO berichtswesendto = new BerichtswesenDTO(
                    zakUbertragung.getDerivatAnforderungModul().getAnforderung().getId(),
                    zakUbertragung.getDerivatAnforderungModul().getAnforderung().getFachId(),
                    zakUbertragung.getDerivatAnforderungModul().getAnforderung().getVersion(),
                    zakUbertragung.getDerivatAnforderungModul().getAnforderung().getBeschreibungAnforderungDe(),
                    zakUbertragung.getDerivatAnforderungModul().getModul().getName(),
                    zakUbertragung.getZakId(),
                    zakUbertragung.getZakKommentar(),
                    zakUbertragung.getDerivatAnforderungModul().getAnforderung().getId().toString()
                            + zakUbertragung.getDerivatAnforderungModul().getModul().getId().toString(),
                    anforderungDerivatMap.get(zakUbertragung.getDerivatAnforderungModul().getAnforderung().getId()),
                    anforderungModulDerivatStatusMap.get(zakUbertragung.getDerivatAnforderungModul().getAnforderung().getId()).get(zakUbertragung.getDerivatAnforderungModul().getModul().getId()),
                    zakUbertragung.getZakResponse(),
                    zakUbertragung.getErsteller(),
                    new SimpleDateFormat("dd-MM-yyyy HH:mm").format(zakUbertragung.getAenderungsdatum())
            );
            result.add(berichtswesendto);
        });
    }

    private void getHistoryQueryPart(QueryPartDTO qp, DateSearchFilter freigegebenSeitFilter) {
        if (freigegebenSeitFilter.isActive()) {
            List<Long> historyIdList = getHistoryIdList(freigegebenSeitFilter);
            BerichtswesenSearchUtils.getHistoryQuery(qp, historyIdList);
        }
    }

    private List<Long> getHistoryIdList(DateSearchFilter freigegebenSeitFilter) {
        SearchFilterDTO statusFilter = new SearchFilterDTO(SearchFilterType.STATUS);
        List<Status> statusList = new ArrayList<>();
        statusList.add(Status.A_FREIGEGEBEN);
        statusFilter.setStatusListValue(statusList);

        SearchFilterDTO dateSearchFilter = new SearchFilterDTO(SearchFilterType.HISTORIE);
        dateSearchFilter.setStartDateValue(freigegebenSeitFilter.getSelected());

        return searchDao.getAnforderungHistroyIdList(dateSearchFilter, statusFilter);

    }
}
