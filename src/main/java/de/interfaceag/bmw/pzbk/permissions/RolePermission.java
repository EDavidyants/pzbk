package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl
 */
public final class RolePermission implements EditViewPermission, Serializable {

    private final Set<Rolle> readRoles;
    private final Set<Rolle> writeRoles;
    private final Set<Rolle> userRoles;

    private RolePermission(Set<Rolle> readRoles, Set<Rolle> writeRoles, Set<Rolle> userRoles) {
        this.readRoles = readRoles;
        this.writeRoles = writeRoles;
        this.userRoles = userRoles;
    }

    @Override
    public boolean isRendered() {
        return readRoles.stream().anyMatch(r -> userRoles.contains(r))
                || writeRoles.stream().anyMatch(r -> userRoles.contains(r));
    }

    @Override
    public boolean hasRightToEdit() {
        return writeRoles.stream().anyMatch(r -> userRoles.contains(r));
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private final Set<Rolle> readRoles = new HashSet<>();
        private final Set<Rolle> writeRoles = new HashSet<>();
        private final Set<Rolle> userRoles = new HashSet<>();

        public Builder() {

        }

        public Builder authorizeReadForRole(Rolle rolle) {
            if (rolle != null) {
                this.readRoles.add(rolle);
            }
            return this;
        }

        public Builder authorizeWriteForRole(Rolle rolle) {
            if (rolle != null) {
                this.writeRoles.add(rolle);
            }
            return this;
        }

        public Builder compareTo(Set<Rolle> userRoles) {
            if (userRoles != null) {
                this.userRoles.addAll(userRoles);
            }
            return this;
        }

        public RolePermission get() {
            return new RolePermission(readRoles, writeRoles, userRoles);
        }
    }

}
