package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.permissions.EnumPermission.Builder;

/**
 *
 * @author sl
 */
public final class StatusPermission {

    private StatusPermission() {
    }

    public static Builder<Status> builder() {
        return EnumPermission.<Status>builder();
    }

}
