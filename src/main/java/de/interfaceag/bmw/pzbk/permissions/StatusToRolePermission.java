package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl
 */
public final class StatusToRolePermission implements EditViewPermission, Serializable {

    private final Set<StatusToRole> readStatusToRole;
    private final Set<StatusToRole> writeStatusToRole;

    private final Status currentStatus;
    private final Set<Rolle> userRolesWithReadPermissions;
    private final Set<Rolle> userRolesWithWritePermissions;

    private StatusToRolePermission(Set<StatusToRole> readStatusToRole, Set<StatusToRole> writeStatusToRole, Status currentStatus,
            Set<Rolle> userRolesWithWritePermissions, Set<Rolle> userRolesWithReadPermissions) {
        this.readStatusToRole = readStatusToRole;
        this.writeStatusToRole = writeStatusToRole;
        this.currentStatus = currentStatus;
        this.userRolesWithReadPermissions = userRolesWithReadPermissions;
        this.userRolesWithWritePermissions = userRolesWithWritePermissions;
    }

    @Override
    public boolean isRendered() {
        return readStatusToRole.stream()
                .anyMatch(str -> str.getStatus().equals(currentStatus)
                && str.getRoles().stream().anyMatch(r -> userRolesWithReadPermissions.contains(r)))
                || writeStatusToRole.stream()
                        .anyMatch(str -> str.getStatus().equals(currentStatus)
                        && str.getRoles().stream().anyMatch(r -> userRolesWithWritePermissions.contains(r)));

    }

    @Override
    public boolean hasRightToEdit() {
        return writeStatusToRole.stream()
                .anyMatch(str -> str.getStatus().equals(currentStatus)
                && str.getRoles().stream().anyMatch(r -> userRolesWithWritePermissions.contains(r)));
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private final Set<StatusToRole> readStatusToRole = new HashSet<>();
        private final Set<StatusToRole> writeStatusToRole = new HashSet<>();

        private Status currentStatus;
        private final Set<Rolle> userRolesWithWritePermissions = new HashSet<>();
        private final Set<Rolle> userRolesWithReadPermissions = new HashSet<>();

        public Builder() {

        }

        public Builder authorizeReadForStatusToRole(StatusToRole statusToRole) {
            if (statusToRole != null) {
                this.readStatusToRole.add(statusToRole);
            }
            return this;
        }

        public Builder authorizeReadForStatusToRoles(Set<StatusToRole> statusToRole) {
            if (statusToRole != null) {
                this.readStatusToRole.addAll(statusToRole);
            }
            return this;
        }

        public Builder authorizeWriteForStatusToRole(StatusToRole statusToRole) {
            if (statusToRole != null) {
                this.writeStatusToRole.add(statusToRole);
            }
            return this;
        }

        public Builder authorizeWriteForStatusToRoles(Set<StatusToRole> statusToRole) {
            if (statusToRole != null) {
                this.writeStatusToRole.addAll(statusToRole);
            }
            return this;
        }

        public Builder compareToStatus(Status currentStatus) {
            if (currentStatus != null) {
                this.currentStatus = currentStatus;
            }
            return this;
        }

        public Builder compareToRolesWithWritePermission(Set<Rolle> userRoles) {
            if (userRoles != null) {
                this.userRolesWithWritePermissions.addAll(userRoles);
            }
            return this;
        }

        public Builder compareToRolesWithReadPermission(Set<Rolle> userRoles) {
            if (userRoles != null) {
                this.userRolesWithReadPermissions.addAll(userRoles);
            }
            return this;
        }

        public StatusToRolePermission get() {
            if (currentStatus != null) {
                return new StatusToRolePermission(readStatusToRole, writeStatusToRole, currentStatus,
                        userRolesWithWritePermissions, userRolesWithReadPermissions);
            }
            return null;
        }
    }

}
