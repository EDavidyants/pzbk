package de.interfaceag.bmw.pzbk.permissions;

import java.io.Serializable;

/**
 *
 * @author sl
 */
public interface ViewPermission extends Serializable {

    boolean isRendered();

}
