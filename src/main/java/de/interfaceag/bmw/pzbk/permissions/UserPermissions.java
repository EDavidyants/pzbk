package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamMitgliedBerechtigung;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sl
 * @param <T>
 */
public interface UserPermissions<T extends BerechtigungDto> extends Serializable {

    List<T> getSensorCocAsLeser();

    List<T> getSensorCocAsSensorSchreibend();

    List<T> getSensorCocAsSensorLesend();

    List<T> getSensorCocAsSensorEingSchreibend();

    List<T> getSensorCocAsSensorCocLeiterSchreibend();

    List<T> getSensorCocAsSensorCocLeiterLesend();

    List<T> getSensorCocAsVertreterSCLSchreibend();

    List<T> getSensorCocAsVertreterSCLLesend();

    List<T> getSensorCocAsUmsetzungsbestaetigerSchreibend();

    List<T> getTteamAsTteamleiterSchreibend();

    List<T> getDerivatAsAnfordererSchreibend();

    List<T> getModulSeTeamAsEcocSchreibend();

    List<T> getZakEinordnungAsAnfordererSchreibend();

    List<Long> getTteamIdsForTteamMitgliedSchreibend();

    List<Long> getTteamIdsForTteamMitgliedLesend();

    List<Long> getTteamIdsForTteamVertreter();

    T getAdminBerechtigung();

    List<Rolle> getRollen();

    Set<Rolle> getRoles();

    boolean hasRole(Rolle rolle);

    Set<Rolle> getRolesWithWritePermissionsForNewMeldung();

    Set<Rolle> getRolesWithWritePermissionsForNewAnforderung();

    Set<Rolle> getRolesWithWritePermissionsForMeldung(Long sensorCocId);

    Set<Rolle> getRolesWithReadPermissionsForMeldung(Long sensorCocId);

    Set<Rolle> getRolesWithWritePermissionsForAnforderung(SensorCoc sensorCocId, Long tteamId, Set<Long> modulSeTeamIds);

    Set<Rolle> getRolesWithReadPermissionsForAnforderung(SensorCoc sensorCocId, Long tteamId, Set<Long> modulSeTeamIds);

    TteamMitgliedBerechtigung getTteamMitgliedBerechtigung();

}
