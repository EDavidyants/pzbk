package de.interfaceag.bmw.pzbk.permissions.components;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.TruePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DashboardStepViewPermission implements Serializable {

    private final ViewPermission menuButton;

    private final ViewPermission stepOneToFourPanel;
    private final ViewPermission stepOne;
    private final ViewPermission stepTwo;
    private final ViewPermission stepThree;
    private final ViewPermission stepFour;

    private final ViewPermission stepFiveToSevenPanel;

    private final ViewPermission stepFive;
    private final ViewPermission stepSix;

    private final ViewPermission stepEightToThirteenPanel;

    private final ViewPermission stepEight;
    private final ViewPermission stepNine;
    private final ViewPermission stepTen;
    private final ViewPermission stepEleven;
    private final ViewPermission stepTwelve;
    private final ViewPermission stepThirteen;
    private final ViewPermission stepFourteen;

    private final ViewPermission arbeitsvorratBerechtigungAntrag;

    public DashboardStepViewPermission(List<Rolle> userRoles) {
        Set<Rolle> roles = new HashSet<>(userRoles);

        menuButton = TruePermission.get();

        stepOneToFourPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSOR)
                .authorizeReadForRole(Rolle.SENSOR_EINGESCHRAENKT)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.E_COC)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(roles).get();

        stepOne = RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSOR)
                .authorizeReadForRole(Rolle.SENSOR_EINGESCHRAENKT)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(roles).get();

        stepTwo = RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSOR)
                .authorizeReadForRole(Rolle.SENSOR_EINGESCHRAENKT)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(roles).get();

        stepThree = RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSOR)
                .authorizeReadForRole(Rolle.SENSOR_EINGESCHRAENKT)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.E_COC)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(roles).get();

        stepFour = RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSOR)
                .authorizeReadForRole(Rolle.SENSOR_EINGESCHRAENKT)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.E_COC)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(roles).get();

        stepFiveToSevenPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(roles).get();

        stepFive = RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(roles).get();

        stepSix = RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(roles).get();

        stepEightToThirteenPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.UMSETZUNGSBESTAETIGER)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(roles).get();

        stepEight = RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(roles).get();

        stepNine = RolePermission.builder()
                .authorizeReadForRole(Rolle.UMSETZUNGSBESTAETIGER)
                .compareTo(roles).get();

        stepTen = RolePermission.builder()
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(roles).get();

        stepEleven = RolePermission.builder()
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(roles).get();

        stepTwelve = RolePermission.builder()
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(roles).get();

        stepThirteen = RolePermission.builder()
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(roles).get();

        stepFourteen = RolePermission.builder()
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(roles).get();

        arbeitsvorratBerechtigungAntrag = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(roles).get();

    }

    public ViewPermission getMenuButton() {
        return menuButton;
    }

    public ViewPermission getStepOneToFourPanel() {
        return stepOneToFourPanel;
    }

    public ViewPermission getStepOne() {
        return stepOne;
    }

    public ViewPermission getStepTwo() {
        return stepTwo;
    }

    public ViewPermission getStepThree() {
        return stepThree;
    }

    public ViewPermission getStepFour() {
        return stepFour;
    }

    public ViewPermission getStepFiveToSevenPanel() {
        return stepFiveToSevenPanel;
    }

    public ViewPermission getStepFive() {
        return stepFive;
    }

    public ViewPermission getStepSix() {
        return stepSix;
    }

    public ViewPermission getStepEightToThirteenPanel() {
        return stepEightToThirteenPanel;
    }

    public ViewPermission getStepEight() {
        return stepEight;
    }

    public ViewPermission getStepNine() {
        return stepNine;
    }

    public ViewPermission getStepTen() {
        return stepTen;
    }

    public ViewPermission getStepEleven() {
        return stepEleven;
    }

    public ViewPermission getStepTwelve() {
        return stepTwelve;
    }

    public ViewPermission getStepThirteen() {
        return stepThirteen;
    }

    public ViewPermission getStepFourteen() {
        return stepFourteen;
    }

    public ViewPermission getArbeitsvorratBerechtigungAntrag() {
        return arbeitsvorratBerechtigungAntrag;
    }
}
