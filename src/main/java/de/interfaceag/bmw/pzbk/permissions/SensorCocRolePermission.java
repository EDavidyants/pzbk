package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class SensorCocRolePermission implements EditPermission {

    private final Long currentSensorCocId;
    private final Set<Long> sensorCocIdsAsVerantwortlicherSensorCocLeiter;
    private final Set<Rolle> userRolesWithWritePermissions;

    public SensorCocRolePermission(Long currentSensorCocId,
            Set<Long> sensorCocIdsAsVerantwortlicherSensorCocLeiter,
            Set<Rolle> userRolesWithWritePermissions) {
        this.currentSensorCocId = currentSensorCocId;
        this.sensorCocIdsAsVerantwortlicherSensorCocLeiter = sensorCocIdsAsVerantwortlicherSensorCocLeiter;
        this.userRolesWithWritePermissions = userRolesWithWritePermissions;
    }

    @Override
    public boolean hasRightToEdit() {
        if (userRolesWithWritePermissions.contains(Rolle.ADMIN)) {
            return Boolean.TRUE;
        }
        return sensorCocIdsAsVerantwortlicherSensorCocLeiter.stream()
                .anyMatch(id -> id.equals(currentSensorCocId))
                && userRolesWithWritePermissions.contains(Rolle.SENSORCOCLEITER);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private Long currentSensorCocId;
        private final Set<Long> sensorCocIdsAsVerantwortlicherSensorCocLeiter = new HashSet<>();
        private final Set<Rolle> userRolesWithWritePermissions = new HashSet<>();

        public Builder() {

        }

        public Builder compareWithSensorCocId(Long sensorCocId) {
            this.currentSensorCocId = sensorCocId;
            return this;
        }

        public Builder forUserRolesWithWritePermissions(Set<Rolle> userRolesWithWritePermissions) {
            this.userRolesWithWritePermissions.addAll(userRolesWithWritePermissions);
            return this;
        }

        public Builder forSensorCocidsAsVerantwortlicherSensorCocLeiter(
                Set<Long> sensorCocIdsAsVerantwortlicherSensorCocLeiter) {
            this.sensorCocIdsAsVerantwortlicherSensorCocLeiter.addAll(sensorCocIdsAsVerantwortlicherSensorCocLeiter);
            return this;
        }

        public SensorCocRolePermission get() {
            if (currentSensorCocId != null) {
                return new SensorCocRolePermission(currentSensorCocId,
                        sensorCocIdsAsVerantwortlicherSensorCocLeiter,
                        userRolesWithWritePermissions);
            }
            return null;
        }
    }

}
