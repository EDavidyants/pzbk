package de.interfaceag.bmw.pzbk.permissions;

/**
 *
 * @author fn
 */
public class ProzessbaukastenFalsePermission implements ProzessbaukastenRolePermission {

    @Override
    public boolean hasRightToEditWithTteamMitglied() {
        return false;
    }

    @Override
    public boolean isRendered() {
        return false;
    }

    @Override
    public boolean hasRightToEdit() {
        return false;
    }

}
