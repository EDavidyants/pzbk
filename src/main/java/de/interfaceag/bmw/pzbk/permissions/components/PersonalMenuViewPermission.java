package de.interfaceag.bmw.pzbk.permissions.components;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class PersonalMenuViewPermission implements Serializable {

    private final ViewPermission benutzerverwaltung;
    private final ViewPermission systemLog;
    private final ViewPermission config;
    private final ViewPermission antragsverwaltung;

    public PersonalMenuViewPermission(List<Rolle> userRoles) {
        Set<Rolle> roles = new HashSet<>(userRoles);

        benutzerverwaltung = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .compareTo(roles).get();

        antragsverwaltung = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(roles).get();

        config = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .compareTo(roles).get();

        systemLog = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .compareTo(roles).get();

    }

    public ViewPermission getBenutzerverwaltung() {
        return benutzerverwaltung;
    }

    public ViewPermission getSystemLog() {
        return systemLog;
    }

    public ViewPermission getConfig() {
        return config;
    }

    public ViewPermission getAntragsverwaltung() {
        return antragsverwaltung;
    }
}
