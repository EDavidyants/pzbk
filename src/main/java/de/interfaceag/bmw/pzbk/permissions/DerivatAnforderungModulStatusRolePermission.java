package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author fp
 */
public class DerivatAnforderungModulStatusRolePermission implements EditViewPermission {

    private final Set<DerivatAnforderungModulStatusRole> readStatusRole;
    private final Set<DerivatAnforderungModulStatusRole> writeStatusRole;

    private final DerivatAnforderungModulStatus currentStatus;
    private final Set<Rolle> userRolesWithReadPermissions;
    private final Set<Rolle> userRolesWithWritePermissions;

    public DerivatAnforderungModulStatusRolePermission(Set<DerivatAnforderungModulStatusRole> readStatusRole, Set<DerivatAnforderungModulStatusRole> writeStatusRole, DerivatAnforderungModulStatus currentStatus,
            Set<Rolle> userRolesWithWritePermissions, Set<Rolle> userRolesWithReadPermissions) {
        this.readStatusRole = readStatusRole;
        this.writeStatusRole = writeStatusRole;
        this.currentStatus = currentStatus;
        this.userRolesWithReadPermissions = userRolesWithReadPermissions;
        this.userRolesWithWritePermissions = userRolesWithWritePermissions;
    }

    @Override
    public boolean isRendered() {
        return readStatusRole.stream()
                .anyMatch(str -> str.getStatus().equals(currentStatus)
                && str.getRoles().stream().anyMatch(r -> userRolesWithReadPermissions.contains(r)))
                || writeStatusRole.stream()
                        .anyMatch(str -> str.getStatus().equals(currentStatus)
                        && str.getRoles().stream().anyMatch(r -> userRolesWithWritePermissions.contains(r)));

    }

    @Override
    public boolean hasRightToEdit() {
        return writeStatusRole.stream()
                .anyMatch(str -> str.getStatus().equals(currentStatus)
                && str.getRoles().stream().anyMatch(r -> userRolesWithWritePermissions.contains(r)));
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private final Set<DerivatAnforderungModulStatusRole> readStatusRole = new HashSet<>();
        private final Set<DerivatAnforderungModulStatusRole> writeStatusRole = new HashSet<>();

        private DerivatAnforderungModulStatus currentStatus;
        private final Set<Rolle> userRolesWithWritePermissions = new HashSet<>();
        private final Set<Rolle> userRolesWithReadPermissions = new HashSet<>();

        public Builder() {

        }

        public Builder authorizeReadForStatusRole(DerivatAnforderungModulStatusRole statusRole) {
            if (statusRole != null) {
                this.readStatusRole.add(statusRole);
            }
            return this;
        }

        public Builder authorizeReadForStatusRoles(Set<DerivatAnforderungModulStatusRole> statusRole) {
            if (statusRole != null) {
                this.readStatusRole.addAll(statusRole);
            }
            return this;
        }

        public Builder authorizeWriteForStatusRole(DerivatAnforderungModulStatusRole statusRole) {
            if (statusRole != null) {
                this.writeStatusRole.add(statusRole);
            }
            return this;
        }

        public Builder authorizeWriteForStatusRoles(Set<DerivatAnforderungModulStatusRole> statusRole) {
            if (statusRole != null) {
                this.writeStatusRole.addAll(statusRole);
            }
            return this;
        }

        public Builder compareToStatus(DerivatAnforderungModulStatus currentStatus) {
            if (currentStatus != null) {
                this.currentStatus = currentStatus;
            }
            return this;
        }

        public Builder compareToRolesWithWritePermission(Set<Rolle> userRoles) {
            if (userRoles != null) {
                this.userRolesWithWritePermissions.addAll(userRoles);
            }
            return this;
        }

        public Builder compareToRolesWithReadPermission(Set<Rolle> userRoles) {
            if (userRoles != null) {
                this.userRolesWithReadPermissions.addAll(userRoles);
            }
            return this;
        }

        public DerivatAnforderungModulStatusRolePermission get() {
            if (currentStatus != null) {
                return new DerivatAnforderungModulStatusRolePermission(readStatusRole, writeStatusRole, currentStatus,
                        userRolesWithWritePermissions, userRolesWithReadPermissions);
            }
            return null;
        }
    }

}
