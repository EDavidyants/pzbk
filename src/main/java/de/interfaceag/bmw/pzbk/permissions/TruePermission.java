package de.interfaceag.bmw.pzbk.permissions;

import java.io.Serializable;

/**
 *
 * @author sl
 */
public class TruePermission implements EditViewPermission, Serializable {

    public TruePermission() {
    }

    public static EditViewPermission get() {
        return new TruePermission();
    }

    @Override
    public boolean isRendered() {
        return Boolean.TRUE;
    }

    @Override
    public boolean hasRightToEdit() {
        return Boolean.TRUE;
    }

}
