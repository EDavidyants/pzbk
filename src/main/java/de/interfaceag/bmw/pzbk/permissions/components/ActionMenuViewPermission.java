package de.interfaceag.bmw.pzbk.permissions.components;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ActionMenuViewPermission implements Serializable {

    private final ViewPermission neueMeldungMenuItem;
    private final ViewPermission neueAnforderungMenuItem;

    private final ViewPermission projektverwaltungMenuItem;
    private final ViewPermission anforderungZuordnenMenuItem;
    private final ViewPermission anforderungVereinbarenMenuItem;
    private final ViewPermission berichtswesenMenuItem;
    private final ViewPermission umsetzungsbestaetigungMenuItem;

    private final ViewPermission reportingAnforderungMenuItem;
    private final ViewPermission reportingProjektMenuItem;

    private final ViewPermission neuerProzessbaukastenMenuItem;

    public ActionMenuViewPermission(List<Rolle> userRoles) {
        Set<Rolle> roles = new HashSet<>(userRoles);

        neueMeldungMenuItem = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.SENSOR)
                .authorizeReadForRole(Rolle.SENSOR_EINGESCHRAENKT)
                .compareTo(roles).get();

        neueAnforderungMenuItem = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAMMITGLIED)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(roles).get();

        projektverwaltungMenuItem = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(roles).get();

        anforderungZuordnenMenuItem = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(roles).get();

        anforderungVereinbarenMenuItem = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(roles).get();

        umsetzungsbestaetigungMenuItem = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.UMSETZUNGSBESTAETIGER)
                .compareTo(roles).get();

        berichtswesenMenuItem = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(roles).get();

        reportingAnforderungMenuItem = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.E_COC)
                .compareTo(roles).get();

        reportingProjektMenuItem = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(roles).get();

        neuerProzessbaukastenMenuItem = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(roles).get();
    }

    public ViewPermission getNeueMeldungMenuItem() {
        return neueMeldungMenuItem;
    }

    public ViewPermission getNeueAnforderungMenuItem() {
        return neueAnforderungMenuItem;
    }

    public ViewPermission getProjektverwaltungMenuItem() {
        return projektverwaltungMenuItem;
    }

    public ViewPermission getAnforderungZuordnenMenuItem() {
        return anforderungZuordnenMenuItem;
    }

    public ViewPermission getAnforderungVereinbarenMenuItem() {
        return anforderungVereinbarenMenuItem;
    }

    public ViewPermission getBerichtswesenMenuItem() {
        return berichtswesenMenuItem;
    }

    public ViewPermission getUmsetzungsbestaetigungMenuItem() {
        return umsetzungsbestaetigungMenuItem;
    }

    public ViewPermission getReportingAnforderungMenuItem() {
        return reportingAnforderungMenuItem;
    }

    public ViewPermission getReportingProjektMenuItem() {
        return reportingProjektMenuItem;
    }

    public ViewPermission getNeuerProzessbaukastenMenuItem() {
        return neuerProzessbaukastenMenuItem;
    }

}
