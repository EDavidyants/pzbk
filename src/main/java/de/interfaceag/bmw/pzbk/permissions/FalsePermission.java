package de.interfaceag.bmw.pzbk.permissions;

import java.io.Serializable;

/**
 *
 * @author sl
 */
public class FalsePermission implements EditViewPermission, Serializable {

    public FalsePermission() {
    }

    public static EditViewPermission get() {
        return new FalsePermission();
    }

    @Override
    public boolean isRendered() {
        return Boolean.FALSE;
    }

    @Override
    public boolean hasRightToEdit() {
        return Boolean.FALSE;
    }

}
