package de.interfaceag.bmw.pzbk.permissions.components;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class MitarbeiterBearbeiterDialogViewPermission implements Serializable {

    private final ViewPermission adminTab;
    private final ViewPermission sensorCocLeiterTab;
    private final ViewPermission vertreterSCLTab;
    private final ViewPermission sensorTab;
    private final ViewPermission sensorEingeschraenktTab;
    private final ViewPermission tteamLeiterTab;
    private final ViewPermission ecocTab;
    private final ViewPermission anfordererTab;
    private final ViewPermission umsetzungsbestaetigerTab;
    private final ViewPermission leserTab;
    private final ViewPermission tteamMitgliedTab;
    private final ViewPermission tteamVertreter;

    private final EditPermission removeRoleButton;
    private final EditPermission addPermissionsButton;

    public MitarbeiterBearbeiterDialogViewPermission(Set<Rolle> editUserRoles, Set<Rolle> currentUserRoles) {

        if (currentUserRoles.contains(Rolle.ADMIN)) {
            adminTab = RolePermission.builder()
                    .authorizeReadForRole(Rolle.ADMIN)
                    .compareTo(editUserRoles).get();
            ecocTab = RolePermission.builder()
                    .authorizeReadForRole(Rolle.E_COC)
                    .compareTo(editUserRoles).get();

            anfordererTab = RolePermission.builder()
                    .authorizeReadForRole(Rolle.ANFORDERER)
                    .compareTo(editUserRoles).get();

            leserTab = RolePermission.builder()
                    .authorizeReadForRole(Rolle.LESER)
                    .compareTo(editUserRoles).get();
        } else {
            adminTab = FalsePermission.get();
            ecocTab = FalsePermission.get();
            anfordererTab = FalsePermission.get();
            leserTab = FalsePermission.get();
        }

        if (currentUserRoles.contains(Rolle.ADMIN) || currentUserRoles.contains(Rolle.SENSORCOCLEITER)) {
            sensorCocLeiterTab = RolePermission.builder()
                    .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                    .compareTo(editUserRoles).get();

            vertreterSCLTab = RolePermission.builder()
                    .authorizeReadForRole(Rolle.SCL_VERTRETER)
                    .compareTo(editUserRoles).get();

            sensorTab = RolePermission.builder()
                    .authorizeReadForRole(Rolle.SENSOR)
                    .compareTo(editUserRoles).get();

            sensorEingeschraenktTab = RolePermission.builder()
                    .authorizeReadForRole(Rolle.SENSOR_EINGESCHRAENKT)
                    .compareTo(editUserRoles).get();

            umsetzungsbestaetigerTab = RolePermission.builder()
                    .authorizeReadForRole(Rolle.UMSETZUNGSBESTAETIGER)
                    .compareTo(editUserRoles).get();

        } else {
            sensorCocLeiterTab = FalsePermission.get();
            vertreterSCLTab = FalsePermission.get();
            sensorTab = FalsePermission.get();
            sensorEingeschraenktTab = FalsePermission.get();
            umsetzungsbestaetigerTab = FalsePermission.get();
        }

        if (currentUserRoles.contains(Rolle.ADMIN) || currentUserRoles.contains(Rolle.T_TEAMLEITER)) {
            tteamLeiterTab = RolePermission.builder()
                    .authorizeReadForRole(Rolle.T_TEAMLEITER)
                    .compareTo(editUserRoles).get();

            tteamMitgliedTab = RolePermission.builder()
                    .authorizeReadForRole(Rolle.TTEAMMITGLIED)
                    .compareTo(editUserRoles).get();

            tteamVertreter = RolePermission.builder()
                    .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                    .compareTo(editUserRoles).get();
        } else {
            tteamLeiterTab = FalsePermission.get();
            tteamMitgliedTab = FalsePermission.get();
            tteamVertreter = FalsePermission.get();
        }

        removeRoleButton = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN)
                .compareTo(currentUserRoles).get();

        addPermissionsButton = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN)
                .authorizeWriteForRole(Rolle.SENSORCOCLEITER)
                .authorizeWriteForRole(Rolle.T_TEAMLEITER)
                .compareTo(currentUserRoles).get();

    }

    public boolean getSensorCocLeiterTab() {
        return sensorCocLeiterTab.isRendered();
    }

    public boolean getVertreterSCLTab() {
        return vertreterSCLTab.isRendered();
    }

    public boolean getSensorTab() {
        return sensorTab.isRendered();
    }

    public boolean getSensorEingeschraenktTab() {
        return sensorEingeschraenktTab.isRendered();
    }

    public boolean getTteamLeiterTab() {
        return tteamLeiterTab.isRendered();
    }

    public boolean getTteamMitgliedTab() {
        return tteamMitgliedTab.isRendered();
    }

    public boolean getTteamVertreterTab() {
        return tteamVertreter.isRendered();
    }

    public boolean getEcocTab() {
        return ecocTab.isRendered();
    }

    public boolean getAnfordererTab() {
        return anfordererTab.isRendered();
    }

    public boolean getUmsetzungsbestaetigerTab() {
        return umsetzungsbestaetigerTab.isRendered();
    }

    public boolean getLeserTab() {
        return leserTab.isRendered();
    }

    public boolean getRemoveRoleButton() {
        return removeRoleButton.hasRightToEdit();
    }

    public boolean getAddPermissionButton() {
        return addPermissionsButton.hasRightToEdit();
    }

    public boolean getAdminTab() {
        return adminTab.isRendered();
    }

}
