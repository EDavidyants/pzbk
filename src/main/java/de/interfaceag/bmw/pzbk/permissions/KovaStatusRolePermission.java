package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class KovaStatusRolePermission implements EditPermission {

    private final KovAStatus status;
    private final Set<Rolle> authorizedRoles;
    private final Set<Rolle> userRoles;

    private KovaStatusRolePermission(KovAStatus status, Set<Rolle> authorizedRoles, Set<Rolle> userRoles) {
        this.status = status;
        this.authorizedRoles = authorizedRoles;
        this.userRoles = userRoles;
    }

    @Override
    public boolean hasRightToEdit() {
        if (!status.equals(KovAStatus.AKTIV)) {
            return Boolean.FALSE;
        }
        return userRoles.stream().anyMatch(r -> authorizedRoles.contains(r));
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private KovAStatus status;
        private final Set<Rolle> authorizedRoles = new HashSet<>();
        private final Set<Rolle> userRoles = new HashSet<>();

        public Builder() {

        }

        public Builder forStatus(KovAStatus status) {
            this.status = status;
            return this;
        }

        public Builder compareWith(Set<Rolle> userRoles) {
            this.userRoles.addAll(userRoles);
            return this;
        }

        public Builder authorizeRole(Rolle role) {
            this.authorizedRoles.add(role);
            return this;
        }

        public KovaStatusRolePermission get() {
            if (status != null) {
                return new KovaStatusRolePermission(status, authorizedRoles, userRoles);
            }
            return null;
        }
    }

}
