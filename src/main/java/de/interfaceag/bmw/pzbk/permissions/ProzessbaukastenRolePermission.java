package de.interfaceag.bmw.pzbk.permissions;

/**
 *
 * @author fn
 */
public interface ProzessbaukastenRolePermission extends EditViewPermission {

    boolean hasRightToEditWithTteamMitglied();
}
