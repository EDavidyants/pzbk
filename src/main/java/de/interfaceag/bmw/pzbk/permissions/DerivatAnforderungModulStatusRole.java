package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author fp
 */
public final class DerivatAnforderungModulStatusRole implements Serializable {

    private final DerivatAnforderungModulStatus status;
    private final Set<Rolle> roles;

    private DerivatAnforderungModulStatusRole(DerivatAnforderungModulStatus status, Set<Rolle> roles) {
        this.status = status;
        this.roles = roles;
    }

    public DerivatAnforderungModulStatus getStatus() {
        return status;
    }

    public Set<Rolle> getRoles() {
        return roles;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private DerivatAnforderungModulStatus status;
        private final Set<Rolle> roles = new HashSet<>();

        public Builder() {
        }

        public Builder createMapFor(DerivatAnforderungModulStatus status) {
            this.status = status;
            return this;
        }

        public Builder addRole(Rolle rolle) {
            this.roles.add(rolle);
            return this;
        }

        public DerivatAnforderungModulStatusRole get() {
            if (status != null) {
                return new DerivatAnforderungModulStatusRole(status, roles);
            }
            return null;
        }

    }

}
