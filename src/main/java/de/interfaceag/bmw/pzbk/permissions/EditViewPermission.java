package de.interfaceag.bmw.pzbk.permissions;

/**
 *
 * @author sl
 */
public interface EditViewPermission extends ViewPermission, EditPermission {

}
