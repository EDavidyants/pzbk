package de.interfaceag.bmw.pzbk.permissions;

import java.io.Serializable;

/**
 *
 * @author sl
 */
public interface EditPermission extends Serializable {

    boolean hasRightToEdit();

}
