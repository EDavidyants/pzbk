package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl
 */
public final class StatusToRole implements Serializable {

    private final Status status;
    private final Set<Rolle> roles;

    private StatusToRole(Status status, Set<Rolle> roles) {
        this.status = status;
        this.roles = roles;
    }

    public Status getStatus() {
        return status;
    }

    public Set<Rolle> getRoles() {
        return roles;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private Status status;
        private final Set<Rolle> roles = new HashSet<>();

        public Builder() {
        }

        public Builder createMapFor(Status status) {
            this.status = status;
            return this;
        }

        public Builder addRole(Rolle rolle) {
            this.roles.add(rolle);
            return this;
        }

        public StatusToRole get() {
            if (status != null) {
                return new StatusToRole(status, roles);
            }
            return null;
        }

    }

}
