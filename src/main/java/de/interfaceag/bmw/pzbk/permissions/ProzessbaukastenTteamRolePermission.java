package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class ProzessbaukastenTteamRolePermission implements ProzessbaukastenRolePermission {

    private final RolePermission rolePermission;

    private final Long prozessbaukastenTteamid;
    private final Collection<Long> tteamIdsWithWritePermissions;
    private final Collection<Rolle> userRolesWithPermissions;
    private final Collection<Long> tteamIdsWithReadPermissions;

    private ProzessbaukastenTteamRolePermission(Long prozessbaukastenTteamid,
            Collection<Long> tteamIdsWithWritePermissionsAsTteamleiter,
            Collection<Rolle> userRolesWithWritePermissions,
            Collection<Long> tteamIdsWithReadPermissions, RolePermission rolePermission) {

        this.rolePermission = rolePermission;
        this.prozessbaukastenTteamid = prozessbaukastenTteamid;
        this.tteamIdsWithWritePermissions = tteamIdsWithWritePermissionsAsTteamleiter;
        this.userRolesWithPermissions = userRolesWithWritePermissions;
        this.tteamIdsWithReadPermissions = tteamIdsWithReadPermissions;
    }

    @Override
    public boolean hasRightToEdit() {
        if (rolePermission.hasRightToEdit()) {
            return true;
        }
        if (userRolesWithPermissions.contains(Rolle.T_TEAMLEITER) || userRolesWithPermissions.contains(Rolle.TTEAM_VERTRETER)) {
            return tteamIdsWithWritePermissions.stream().anyMatch(id -> id.equals(prozessbaukastenTteamid));
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public boolean hasRightToEditWithTteamMitglied() {
        if (rolePermission.hasRightToEdit()) {
            return true;
        }
        if (userRolesWithPermissions.contains(Rolle.T_TEAMLEITER) || userRolesWithPermissions.contains(Rolle.TTEAM_VERTRETER) || userRolesWithPermissions.contains(Rolle.TTEAMMITGLIED)) {
            return tteamIdsWithWritePermissions.stream().anyMatch(id -> id.equals(prozessbaukastenTteamid));
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public boolean isRendered() {
        if (rolePermission.isRendered()) {
            return Boolean.TRUE;
        }
        return tteamIdsWithReadPermissions.stream()
                .anyMatch(id -> id.equals(prozessbaukastenTteamid))
                && (userRolesWithPermissions.contains(Rolle.TTEAMMITGLIED));
    }

    public static ProzessbaukastenRolePermission getFalsePermission() {
        return new ProzessbaukastenFalsePermission();
    }

    public static Builder forProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        return new Builder().compareWithTteamId(prozessbaukasten.getTteam().getId());

    }

    public static final class Builder {

        private Long prozessbaukastenTteamid;
        private final Collection<Long> tteamIdsWithWritePermissionsAsTteamleiter = new HashSet<>();
        private final Collection<Rolle> userRolesWithPermissions = new HashSet<>();
        private final Collection<Long> tteamIdsWithReadPermissions = new HashSet<>();
        private RolePermission rolePermission;

        public Builder() {

        }

        private Builder compareWithTteamId(Long prozessbaukastenTteamid) {
            this.prozessbaukastenTteamid = prozessbaukastenTteamid;
            return this;
        }

        public Builder forUserRolesWithPermissions(Collection<Rolle> userRolesWithWritePermissions) {
            this.userRolesWithPermissions.addAll(userRolesWithWritePermissions);
            return this;
        }

        public Builder forTteamIdsWithWritePermissions(
                Collection<Long> tteamIdsWithWritePermissionsAsTteamleiter) {
            this.tteamIdsWithWritePermissionsAsTteamleiter.addAll(tteamIdsWithWritePermissionsAsTteamleiter);
            return this;
        }

        public Builder forTteamIdsWithReadPermissions(Collection<Long> tteamIdsWithReadPermissions) {
            this.tteamIdsWithReadPermissions.addAll(tteamIdsWithReadPermissions);
            return this;
        }

        public Builder withRolePermission(RolePermission rolePermission) {
            this.rolePermission = rolePermission;
            return this;
        }

        public ProzessbaukastenTteamRolePermission build() {
            return new ProzessbaukastenTteamRolePermission(prozessbaukastenTteamid,
                    tteamIdsWithWritePermissionsAsTteamleiter,
                    userRolesWithPermissions, tteamIdsWithReadPermissions, rolePermission);
        }
    }

}
