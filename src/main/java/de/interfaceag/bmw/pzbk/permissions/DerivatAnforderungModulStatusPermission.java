package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class DerivatAnforderungModulStatusPermission {

    private DerivatAnforderungModulStatusPermission() {
    }

    public static EnumPermission.Builder builder() {
        return EnumPermission.<DerivatAnforderungModulStatus>builder();
    }

}
