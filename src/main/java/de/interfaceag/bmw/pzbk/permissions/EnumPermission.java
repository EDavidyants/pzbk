package de.interfaceag.bmw.pzbk.permissions;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl
 * @param <T> extends Enum
 */
public final class EnumPermission<T extends Enum<T>> implements EditViewPermission, Serializable {

    private final Set<T> read;
    private final Set<T> write;
    private final T current;

    private EnumPermission(Set<T> read, Set<T> write, T current) {
        this.read = read;
        this.write = write;
        this.current = current;
    }

    @Override
    public boolean isRendered() {
        return read.contains(current) || write.contains(current);
    }

    @Override
    public boolean hasRightToEdit() {
        return write.contains(current);
    }

    public static Builder builder() {
        return new Builder<>();
    }

    public static final class Builder<T extends Enum<T>> {

        private final Set<T> readSet = new HashSet<>();
        private final Set<T> writeSet = new HashSet<>();
        private T current;

        public Builder() {

        }

        public Builder authorizeRead(T read) {
            if (read != null) {
                this.readSet.add(read);
            }
            return this;
        }

        public Builder authorizeWrite(T write) {
            if (write != null) {
                this.writeSet.add(write);
            }
            return this;
        }

        public Builder compareTo(T current) {
            if (current != null) {
                this.current = current;
            }
            return this;
        }

        public EnumPermission get() {
            if (current != null) {
                return new EnumPermission(readSet, writeSet, current);
            }
            return null;
        }
    }

}
