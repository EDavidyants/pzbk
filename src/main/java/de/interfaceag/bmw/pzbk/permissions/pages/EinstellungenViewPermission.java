package de.interfaceag.bmw.pzbk.permissions.pages;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author fp
 */
public class EinstellungenViewPermission implements Serializable {

    private final ViewPermission adminPanel;
    private final ViewPermission sensorPanel;
    private final ViewPermission sensorEingeschraenktPanel;
    private final ViewPermission sensorCoCLeiterPanel;
    private final ViewPermission vertreterSCLPanel;
    private final ViewPermission tteamleiterPanel;
    private final ViewPermission ecocPanel;
    private final ViewPermission anfordererPanel;
    private final ViewPermission umsetzungsbestaetigerPanel;
    private final ViewPermission leserPanel;
    private final ViewPermission tteamMitgliedPanel;
    private final ViewPermission tteamVertreterPanel;

    public EinstellungenViewPermission(List<Rolle> userRoles) {
        Set<Rolle> roles = new HashSet<>(userRoles);

        adminPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .compareTo(roles).get();

        sensorPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSOR)
                .compareTo(roles).get();

        sensorEingeschraenktPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSOR_EINGESCHRAENKT)
                .compareTo(roles).get();

        sensorCoCLeiterPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .compareTo(roles).get();

        vertreterSCLPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .compareTo(roles).get();

        tteamleiterPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .compareTo(roles).get();

        ecocPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.E_COC)
                .compareTo(roles).get();

        anfordererPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(roles).get();

        umsetzungsbestaetigerPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.UMSETZUNGSBESTAETIGER)
                .compareTo(roles).get();

        leserPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.LESER)
                .compareTo(roles).get();
        tteamMitgliedPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.TTEAMMITGLIED)
                .compareTo(roles).get();
        tteamVertreterPanel = RolePermission.builder()
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .compareTo(roles).get();

    }

    public boolean getAdminPanel() {
        return adminPanel.isRendered();
    }

    public boolean getSensorPanel() {
        return sensorPanel.isRendered();
    }

    public boolean getSensorEingeschraenktPanel() {
        return sensorEingeschraenktPanel.isRendered();
    }

    public boolean getSensorCoCLeiterPanel() {
        return sensorCoCLeiterPanel.isRendered();
    }

    public boolean getVertreterSCLPanel() {
        return vertreterSCLPanel.isRendered();
    }

    public boolean getTteamleiterPanel() {
        return tteamleiterPanel.isRendered();
    }

    public boolean getEcocPanel() {
        return ecocPanel.isRendered();
    }

    public boolean getAnfordererPanel() {
        return anfordererPanel.isRendered();
    }

    public boolean getUmsetzungsbestaetigerPanel() {
        return umsetzungsbestaetigerPanel.isRendered();
    }

    public boolean getLeserPanel() {
        return leserPanel.isRendered();
    }

    public boolean getTteamMitgliedPanel() {
        return tteamMitgliedPanel.isRendered();
    }

    public boolean getTteamVertreterPanel() {
        return tteamVertreterPanel.isRendered();
    }

}
