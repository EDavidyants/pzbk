package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewData;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author fn
 */
public interface UmsetzungsbestaetigungContent {

    void onRowEdit(RowEditEvent event);

    void showEditCommentDialog(UmsetzungsbestaetigungDto umsetzungsbestaetigung);

    void showGroupCommentDialog();

    String processChanges();

    String resetChanges();

    String edit();

    UmsetzungsbestaetigungViewData getViewData();

    void downloadExcelExport();

}
