package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class KovAPhasenViewPermission implements Serializable {

    private final ViewPermission page;

    private final EditPermission dialog;

    public KovAPhasenViewPermission(Set<Rolle> userRoles) {
        this.page = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.UMSETZUNGSBESTAETIGER)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(userRoles).get();

        this.dialog = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN)
                .authorizeWriteForRole(Rolle.SENSORCOCLEITER)
                .authorizeWriteForRole(Rolle.SCL_VERTRETER)
                .compareTo(userRoles).get();
    }

    public boolean getPage() {
        return page.isRendered();
    }

    public boolean getDialog() {
        return dialog.hasRightToEdit();
    }

}
