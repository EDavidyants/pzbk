package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;

import java.io.Serializable;
import java.time.ZoneOffset;
import java.util.Date;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class KovAPhaseImDerivatDto implements Serializable {

    private final Long id;
    private final String derivatName;
    private final KovAPhase kovAPhase;
    private final Date startDate;
    private final Date endDate;
    private final KovAStatus status;

    // umsetzungsbestaetigung progress
    private String progressLabel;
    private Long progressValue;
    private boolean finished;

    // berechtigung progress
    private String berechtigungProgressLabel;
    private Long berechtigungProgressValue;
    private boolean berechtigungFinished;

    public KovAPhaseImDerivatDto(KovAPhaseImDerivat kovAPhaseImDerivat) {
        this.id = kovAPhaseImDerivat.getId();
        this.derivatName = kovAPhaseImDerivat.getDerivat().getName();
        if (kovAPhaseImDerivat.getKovAPhase() != null) {
            this.kovAPhase = kovAPhaseImDerivat.getKovAPhase();
        } else {
            this.kovAPhase = null;
        }
        if (kovAPhaseImDerivat.getStartDate() != null) {
            this.startDate = Date.from(kovAPhaseImDerivat.getStartDate().atTime(00, 01, 00).toInstant(ZoneOffset.UTC));
        } else {
            this.startDate = null;
        }
        if (kovAPhaseImDerivat.getEndDate() != null) {
            this.endDate = Date.from(kovAPhaseImDerivat.getEndDate().atTime(00, 01, 00).toInstant(ZoneOffset.UTC));
        } else {
            this.endDate = null;
        }
        this.status = kovAPhaseImDerivat.getKovaStatus();

    }

    public Long getId() {
        return id;
    }

    public String getDerivatName() {
        return derivatName;
    }

    public KovAPhase getKovAPhase() {
        return kovAPhase;
    }

    public Date getStartDate() {
        if (startDate != null) {
            return new Date(startDate.getTime());
        }
        return null;
    }

    public Date getEndDate() {
        if (endDate != null) {
            return new Date(endDate.getTime());
        }
        return null;
    }

    public KovAStatus getStatus() {
        return status;
    }

    public String getProgressLabel() {
        return progressLabel;
    }

    public Long getProgressValue() {
        return progressValue;
    }

    public boolean isFinished() {
        return finished;
    }

    public String getBerechtigungProgressLabel() {
        return berechtigungProgressLabel;
    }

    public Long getBerechtigungProgressValue() {
        return berechtigungProgressValue;
    }

    public boolean isBerechtigungFinished() {
        return berechtigungFinished;
    }

    // ---------- setter -------------------------------------------------------
    public void setProgressLabel(String progressLabel) {
        this.progressLabel = progressLabel;
    }

    public void setProgressValue(Long progressValue) {
        this.progressValue = progressValue;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public void setBerechtigungProgressLabel(String berechtigungProgressLabel) {
        this.berechtigungProgressLabel = berechtigungProgressLabel;
    }

    public void setBerechtigungProgressValue(Long berechtigungProgressValue) {
        this.berechtigungProgressValue = berechtigungProgressValue;
    }

    public void setBerechtigungFinished(boolean berechtigungFinished) {
        this.berechtigungFinished = berechtigungFinished;
    }

}
