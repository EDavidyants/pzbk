package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOptionalTupleComparator;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOrders;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungenOrderService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author sl
 */
@Stateless
@Named
public class UmsetzungsverwaltungTableDataSortService {

    private static final Logger LOG = LoggerFactory.getLogger(UmsetzungsverwaltungTableDataSortService.class.getName());

    @Inject
    private ProzessbaukastenAnforderungenOrderService prozessbaukastenAnforderungenOrderService;

    public List<UmsetzungsbestaetigungDto> sortTableData(List<UmsetzungsbestaetigungDto> vereinbarungen) {

        LOG.debug("Sort table data");
        LOG.debug("Order before sorting: {}", vereinbarungen);

        Collection<Long> prozessbaukastenIdsForZuordnungen = getProzessbaukastenIdsForZuordnungen(vereinbarungen);

        if (prozessbaukastenIdsForZuordnungen.isEmpty()) {
            return vereinbarungen;
        }

        ProzessbaukastenAnforderungOrders anforderungenOrders = prozessbaukastenAnforderungenOrderService.getAnforderungenOrderForProzessbaukasten(prozessbaukastenIdsForZuordnungen);

        List<UmsetzungsbestaetigungDto> prozessbaukastenAnforderungen = sortProzessbaukastenAnforderungen(vereinbarungen, anforderungenOrders);
        List<UmsetzungsbestaetigungDto> otherAnforderungen = sortNormalAnforderungen(vereinbarungen, anforderungenOrders);

        prozessbaukastenAnforderungen.addAll(otherAnforderungen);

        LOG.debug("Order after sorting: {}", prozessbaukastenAnforderungen);

        return prozessbaukastenAnforderungen;
    }

    private static List<UmsetzungsbestaetigungDto> sortProzessbaukastenAnforderungen(List<UmsetzungsbestaetigungDto> vereinbarungen, ProzessbaukastenAnforderungOrders anforderungenOrders) {
        ProzessbaukastenAnforderungOptionalTupleComparator<UmsetzungsbestaetigungDto> zuordnungProzessbaukastenAnforderungComparator = new ProzessbaukastenAnforderungOptionalTupleComparator<>(anforderungenOrders);

        return vereinbarungen.stream()
                .filter(vereinbarung -> vereinbarung.getProzessbaukastenId().isPresent())
                .sorted(zuordnungProzessbaukastenAnforderungComparator)
                .collect(Collectors.toList());
    }

    private static List<UmsetzungsbestaetigungDto> sortNormalAnforderungen(List<UmsetzungsbestaetigungDto> vereinbarungen, ProzessbaukastenAnforderungOrders anforderungenOrders) {
        ProzessbaukastenAnforderungOptionalTupleComparator<UmsetzungsbestaetigungDto> zuordnungProzessbaukastenAnforderungComparator = new ProzessbaukastenAnforderungOptionalTupleComparator<>(anforderungenOrders);

        return vereinbarungen.stream()
                .filter(vereinbarung -> !vereinbarung.getProzessbaukastenId().isPresent())
                .sorted(zuordnungProzessbaukastenAnforderungComparator)
                .collect(Collectors.toList());
    }

    private static Collection<Long> getProzessbaukastenIdsForZuordnungen(Collection<UmsetzungsbestaetigungDto> vereinbarungen) {
        return vereinbarungen.stream()
                .map(UmsetzungsbestaetigungDto::getProzessbaukastenId)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

}
