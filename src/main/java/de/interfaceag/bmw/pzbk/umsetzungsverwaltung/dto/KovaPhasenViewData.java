package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.filter.pages.KovAPhasenViewFilter;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class KovaPhasenViewData implements Serializable {

    private final KovAPhasenViewPermission viewPermission;
    private final KovAPhasenViewFilter filter;
    private final List<KovAPhaseImDerivatDto> kovAPhasenImDerivat;

    public KovaPhasenViewData(KovAPhasenViewPermission viewPermission,
            KovAPhasenViewFilter filter, List<KovAPhaseImDerivatDto> kovAPhasenImDerivat) {
        this.viewPermission = viewPermission;
        this.filter = filter;
        this.kovAPhasenImDerivat = kovAPhasenImDerivat;
    }

    public KovAPhasenViewPermission getViewPermission() {
        return viewPermission;
    }

    public KovAPhasenViewFilter getFilter() {
        return filter;
    }

    public List<KovAPhaseImDerivatDto> getKovAPhasenImDerivat() {
        return kovAPhasenImDerivat;
    }

}
