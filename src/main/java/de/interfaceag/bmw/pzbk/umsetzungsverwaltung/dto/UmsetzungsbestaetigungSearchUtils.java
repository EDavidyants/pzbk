package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.filter.FachIdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ThemenklammerFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import java.util.List;

/**
 * @author fn
 */
final class UmsetzungsbestaetigungSearchUtils {

    private UmsetzungsbestaetigungSearchUtils() {
    }

    static QueryPart getBaseQueryForDerivatAnforderungModul(KovAPhaseImDerivat kovAPhaseImDerivat, List<Long> userSensorCoc) {
        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT dam FROM DerivatAnforderungModul dam "
                + "INNER JOIN dam.zuordnungAnforderungDerivat zad "
                + "INNER JOIN zad.anforderung a "
                + "INNER JOIN zad.derivat d "
                + "LEFT JOIN a.prozessbaukasten p "
                + "LEFT JOIN a.prozessbaukastenThemenklammern pt "
                + "WHERE a.sensorCoc.sensorCocId IN :sensorCocs AND d.id = :derivatId ");
        queryPart.put("derivatId", kovAPhaseImDerivat.getDerivat().getId());

        queryPart.append(getRelevantDerivatStatusAsQueryPart(kovAPhaseImDerivat.getKovAPhase()));

        queryPart.put("status", DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId());

        queryPart.put("sensorCocs", userSensorCoc);

        return queryPart;
    }

    private static String getRelevantDerivatStatusAsQueryPart(KovAPhase kovAPhase) {
        switch (kovAPhase) {
            case VABG0:
            case VABG1:
            case VABG2:
            case VA_VKBG:
            case VKBG:
                return "AND dam.statusVKBG = :status ";
            case VA_VBBG:
            case VBBG:
            case BBG:
            default:
                return "AND dam.statusZV = :status ";
        }
    }

    static QueryPart getBaseQueryForExistingUmsetzungsbestaetigung(Long kovAPhaseImDerivatId, List<Long> userSensorCoc, boolean prozessbaukastenFilter) {
        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT um FROM Umsetzungsbestaetigung um "
                + "INNER JOIN um.anforderung a "
                + "LEFT JOIN a.prozessbaukastenThemenklammern pt ");

        if (prozessbaukastenFilter) {
            queryPart.append("LEFT JOIN a.prozessbaukasten p ");
        }

        queryPart.append("WHERE um.kovaImDerivat.id = :kovAPhaseImDerivatId AND um.status != :status AND a.sensorCoc.sensorCocId IN :sensorCocs ");
        queryPart.put("kovAPhaseImDerivatId", kovAPhaseImDerivatId);
        queryPart.put("status", UmsetzungsBestaetigungStatus.OFFEN);
        queryPart.put("sensorCocs", userSensorCoc);
        return queryPart;
    }

    static void getAnforderungQuery(QueryPart queryPart, FachIdSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND (");
            int i = 0;
            for (SearchFilterObject selectedValue : filter.getSelectedValues()) {
                queryPart.append(" LOWER(a.fachId) LIKE LOWER(:fachId" + i + ") OR");
                queryPart.put("fachId" + i, selectedValue.getName());
                i++;

            }
            queryPart.append(" 1=0)");
        }
    }

    static void getBeschreibungQuery(QueryPart queryPart, TextSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND LOWER(a.beschreibungAnforderungDe) LIKE LOWER(:beschreibung)");
            queryPart.put("beschreibung", "%" + filter.getAsString() + "%");
        }
    }

    static void getSensorCocQuery(QueryPart queryPart, IdSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND a.sensorCoc.sensorCocId IN :sensorCocs");
            queryPart.put("sensorCocs", filter.getSelectedIdsAsSet());
        }
    }

    static void getProzessbaukastenQuery(QueryPart queryPart, IdSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND p.id IN :prozessbaukastenIds");
            queryPart.put("prozessbaukastenIds", filter.getSelectedIdsAsSet());
        }
    }

    static void getThemenklammerQuery(QueryPart queryPart, ThemenklammerFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND pt.themenklammer.id IN :themenklammerIds");
            queryPart.put("themenklammerIds", filter.getSelectedIdsAsSet());
        }
    }


    static void getDerivatAnforderungModulModulQuery(QueryPart queryPart, IdSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND dam.modul.id IN :damModulIds");
            queryPart.put("damModulIds", filter.getSelectedIdsAsSet());
        }
    }

    static void getUmsetzungsbestaetigungModulQuery(QueryPart queryPart, IdSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND um.modul.id IN :damModulIds");
            queryPart.put("damModulIds", filter.getSelectedIdsAsSet());
        }
    }

    static void getUmsetzungsbestaetigungStatusQueryForOnlyOffenStatus(QueryPart queryPart) {
        queryPart.append(" AND um.status != :umsetzungsBestaetigungStatus ");
        queryPart.put("umsetzungsBestaetigungStatus", UmsetzungsBestaetigungStatus.OFFEN);
    }

    static void getUmsetzungsbestaetigungStatusQuery(QueryPart queryPart, MultiValueEnumSearchFilter<UmsetzungsBestaetigungStatus> filter) {
        List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus = filter.getAsEnumList();
        if (umsetzungsBestaetigungStatus != null && umsetzungsBestaetigungStatus.size() < 10) {
            if (umsetzungsBestaetigungStatus.isEmpty()) {
                queryPart.append(" AND 1 = 0");
            } else {
                addUmsetzungsbestaetigungStatusToQuery(queryPart, umsetzungsBestaetigungStatus);
            }
        }
    }

    private static void addUmsetzungsbestaetigungStatusToQuery(QueryPart queryPart, List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {
        if (umsetzungsBestaetigungStatus.contains(UmsetzungsBestaetigungStatus.OFFEN)) {
            searchForStatusWithoutOffen(queryPart, umsetzungsBestaetigungStatus);
        } else {
            searchForStatus(queryPart, umsetzungsBestaetigungStatus);
        }
    }

    private static void searchForStatus(QueryPart queryPart, List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {
        queryPart.append(" AND um.status IN :umsetzungsBestaetigungStatus ");
        queryPart.put("umsetzungsBestaetigungStatus", umsetzungsBestaetigungStatus);
    }

    private static void searchForStatusWithoutOffen(QueryPart queryPart, List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {
        umsetzungsBestaetigungStatus.remove(UmsetzungsBestaetigungStatus.OFFEN);
        if (!umsetzungsBestaetigungStatus.isEmpty()) {
            searchForStatus(queryPart, umsetzungsBestaetigungStatus);
        }
    }

}
