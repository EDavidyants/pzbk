package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.dao.KovAPhaseImDerivatDao;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.session.Session;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author sl
 */
@Stateless
@Named
public class UmsetzungsbestaetigungBerechtigungProgressService implements Serializable {

    @Inject
    private Session session;
    @Inject
    private KovAPhaseImDerivatDao kovAPhaseImDerivatDao;

    private Optional<Rolle> getRelevantRoleForQuery() {
        Set<Rolle> userRoles = session.getUserPermissions().getRoles();
        if (userRoles.contains(Rolle.ADMIN)) {
            return Optional.of(Rolle.ADMIN);
        }
        if (userRoles.contains(Rolle.SENSORCOCLEITER)) {
            return Optional.of(Rolle.SENSORCOCLEITER);
        }
        if (userRoles.contains(Rolle.SCL_VERTRETER)) {
            return Optional.of(Rolle.SCL_VERTRETER);
        }
        return Optional.empty();
    }

    public long getNumberOfGesetzteBerechtigungenForKovaPhaseImDerivat(long kovaPhaseImDerivatId) {
        Optional<Rolle> relevantRoleForQuery = getRelevantRoleForQuery();
        KovAPhaseImDerivat kovAPhaseImDerivat = kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(kovaPhaseImDerivatId);
        if (relevantRoleForQuery.isPresent()) {
            switch (relevantRoleForQuery.get()) {
                case ADMIN:
                    return getNumberOfSensorCocsForKovaPhaseImDerivatForAdminWithUmsetzer(kovAPhaseImDerivat);
                case SENSORCOCLEITER:
                    return getNumberOfSensorCocsForKovaPhaseImDerivatForSensorCocLeiterWithUmsetzer(kovAPhaseImDerivat);
                case SCL_VERTRETER:
                    return getNumberOfSensorCocsForKovaPhaseImDerivatForSensorCocVertreterWithUmsetzer(kovAPhaseImDerivat);
                default:
                    return 0L;
            }
        }
        return 0L;
    }

    public long getNumberOfAllBerechtigungenForKovaPhaseImDerivat(long kovaPhaseImDerivatID) {
        Optional<Rolle> relevantRoleForQuery = getRelevantRoleForQuery();
        KovAPhaseImDerivat kovAPhaseImDerivat = kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(kovaPhaseImDerivatID);
        if (relevantRoleForQuery.isPresent() && kovAPhaseImDerivat != null) {
            switch (relevantRoleForQuery.get()) {
                case ADMIN:
                    return getNumberOfSensorCocsForKovaPhaseImDerivatForAdmin(kovAPhaseImDerivat);
                case SENSORCOCLEITER:
                    return getNumberOfSensorCocsForKovaPhaseImDerivatForSensorCocLeiter(kovAPhaseImDerivat);
                case SCL_VERTRETER:
                    return getNumberOfSensorCocsForKovaPhaseImDerivatForSensorCocVertreter(kovAPhaseImDerivat);
                default:
                    return 0L;
            }
        }
        return 0L;
    }

    private long getNumberOfSensorCocsForKovaPhaseImDerivatForAdmin(KovAPhaseImDerivat kovAPhaseImDerivat) {
        return kovAPhaseImDerivatDao.getNumberOfSensorCocsForKovaPhaseImDerivat(kovAPhaseImDerivat, new ArrayList<>());
    }

    private long getNumberOfSensorCocsForKovaPhaseImDerivatForSensorCocLeiter(KovAPhaseImDerivat kovAPhaseImDerivat) {
        List<Long> sensorCocIdsForUser = session.getUserPermissions()
                .getSensorCocAsSensorCocLeiterSchreibend().stream().map(BerechtigungDto::getId).collect(Collectors.toList());
        if (!sensorCocIdsForUser.isEmpty()) {
            return kovAPhaseImDerivatDao.getNumberOfSensorCocsForKovaPhaseImDerivat(kovAPhaseImDerivat, sensorCocIdsForUser);
        } else {
            return 0L;
        }
    }

    private long getNumberOfSensorCocsForKovaPhaseImDerivatForSensorCocVertreter(KovAPhaseImDerivat kovAPhaseImDerivat) {
        List<Long> sensorCocIdsForUser = session.getUserPermissions()
                .getSensorCocAsVertreterSCLSchreibend().stream().map(BerechtigungDto::getId).collect(Collectors.toList());
        if (!sensorCocIdsForUser.isEmpty()) {
            return kovAPhaseImDerivatDao.getNumberOfSensorCocsForKovaPhaseImDerivat(kovAPhaseImDerivat, sensorCocIdsForUser);
        } else {
            return 0L;
        }
    }

    private long getNumberOfSensorCocsForKovaPhaseImDerivatForAdminWithUmsetzer(KovAPhaseImDerivat kovAPhaseImDerivat) {
        return kovAPhaseImDerivatDao.getNumberOfSensorCocsForKovaPhaseImDerivatWithUmsetzer(kovAPhaseImDerivat, new ArrayList<>());
    }

    private long getNumberOfSensorCocsForKovaPhaseImDerivatForSensorCocLeiterWithUmsetzer(KovAPhaseImDerivat kovAPhaseImDerivat) {
        List<Long> sensorCocIdsForUser = session.getUserPermissions()
                .getSensorCocAsSensorCocLeiterSchreibend().stream().map(BerechtigungDto::getId).collect(Collectors.toList());
        if (!sensorCocIdsForUser.isEmpty()) {
            return kovAPhaseImDerivatDao.getNumberOfSensorCocsForKovaPhaseImDerivatWithUmsetzer(kovAPhaseImDerivat, sensorCocIdsForUser);
        } else {
            return 0L;
        }
    }

    private long getNumberOfSensorCocsForKovaPhaseImDerivatForSensorCocVertreterWithUmsetzer(KovAPhaseImDerivat kovAPhaseImDerivat) {
        List<Long> sensorCocIdsForUser = session.getUserPermissions()
                .getSensorCocAsVertreterSCLSchreibend().stream().map(BerechtigungDto::getId).collect(Collectors.toList());
        if (!sensorCocIdsForUser.isEmpty()) {
            return kovAPhaseImDerivatDao.getNumberOfSensorCocsForKovaPhaseImDerivatWithUmsetzer(kovAPhaseImDerivat, sensorCocIdsForUser);
        } else {
            return 0L;
        }
    }

}
