package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.dao.KovAPhaseImDerivatDao;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.session.Session;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl
 */
@Stateless
@Named
public class UmsetzungsbestaetigungProgressService implements Serializable {

    @Inject
    private Session session;
    @Inject
    private KovAPhaseImDerivatDao kovAPhaseImDerivatDao;

    @Inject
    private BerechtigungService berechtigungService;

    private Optional<Rolle> getRelevantRoleForQuery() {

        UserPermissions userPermissions = session.getUserPermissions();
        Set<Rolle> userRole = userPermissions.getRoles();

        if (userRole.contains(Rolle.ADMIN)) {
            return Optional.of(Rolle.ADMIN);
        }
        if (userRole.contains(Rolle.SENSORCOCLEITER)) {
            return Optional.of(Rolle.SENSORCOCLEITER);
        }
        if (userRole.contains(Rolle.SCL_VERTRETER)) {
            return Optional.of(Rolle.SCL_VERTRETER);
        }
        if (userRole.contains(Rolle.UMSETZUNGSBESTAETIGER)) {
            return Optional.of(Rolle.UMSETZUNGSBESTAETIGER);
        }
        if (userRole.contains(Rolle.ANFORDERER)) {
            return Optional.of(Rolle.ANFORDERER);
        }
        return Optional.empty();
    }

    public long getNumberofUmgesetzteUmsetzungsbestaetigungenForKovaPhaseImDerivat(long kovaPhaseImDerivatId) {
        Optional<Rolle> relevantRoleForQuery = getRelevantRoleForQuery();
        KovAPhaseImDerivat kovAPhaseImDerivat = kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(kovaPhaseImDerivatId);
        if (relevantRoleForQuery.isPresent() && kovAPhaseImDerivat != null) {
            switch (relevantRoleForQuery.get()) {
                case ADMIN:
                    return getUmgesetzteAnforderungenForKovaPhaseImDerivatForAdmin(kovAPhaseImDerivat);
                case SENSORCOCLEITER:
                case SCL_VERTRETER:
                case ANFORDERER:
                case UMSETZUNGSBESTAETIGER:
                    Optional<List<Long>> sensorCocIdsForUser = getSensorCocIdsForUser(kovaPhaseImDerivatId, relevantRoleForQuery);
                    return getUmgesetzteAnforderungenForKovaPhaseImDerivat(kovAPhaseImDerivat, sensorCocIdsForUser);
                default:
                    return 0L;
            }
        }
        return 0L;
    }

    public long getNumberOfAllUmsetzungsbestaetigungenForKovaPhaseImDerivat(long kovaPhaseImDerivatId) {

        Optional<Rolle> relevantRoleForQuery = getRelevantRoleForQuery();
        KovAPhaseImDerivat kovAPhaseImDerivat = kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(kovaPhaseImDerivatId);
        if (relevantRoleForQuery.isPresent() && kovAPhaseImDerivat != null) {
            switch (relevantRoleForQuery.get()) {
                case ADMIN:
                    return getNumberOfAnforderungenForKovaPhaseImDerivatForAdmin(kovAPhaseImDerivat);
                case SENSORCOCLEITER:
                case SCL_VERTRETER:
                case ANFORDERER:
                case UMSETZUNGSBESTAETIGER:
                    Optional<List<Long>> sensorCocIdsForUser = getSensorCocIdsForUser(kovaPhaseImDerivatId, relevantRoleForQuery);
                    return getNumberOfAnforderungenForKovaPhaseImDerivat(kovAPhaseImDerivat, sensorCocIdsForUser);
                default:
                    return 0L;
            }
        }
        return 0L;
    }

    private Long getNumberOfAnforderungenForKovaPhaseImDerivatForAdmin(KovAPhaseImDerivat kovAPhaseImDerivat) {
        return kovAPhaseImDerivatDao.getNumberOfAllAnforderungenInUmsetzungsbestaetigung(kovAPhaseImDerivat, Collections.emptyList());
    }

    private Long getNumberOfAnforderungenForKovaPhaseImDerivat(KovAPhaseImDerivat kovAPhaseImDerivat, Optional<List<Long>> sensorCocIds) {
        if (sensorCocIds.isPresent()) {
            return kovAPhaseImDerivatDao.getNumberOfAllAnforderungenInUmsetzungsbestaetigung(kovAPhaseImDerivat, sensorCocIds.get());
        } else {
            return 0L;
        }
    }

    protected Long getUmgesetzteAnforderungenForKovaPhaseImDerivatForAdmin(KovAPhaseImDerivat kovAPhaseImDerivat) {
        return kovAPhaseImDerivatDao.getNumberOfAllUmgesetzteAnforderungenInUmsetzungsbestaetigung(kovAPhaseImDerivat, Collections.emptyList());
    }

    protected Long getUmgesetzteAnforderungenForKovaPhaseImDerivat(KovAPhaseImDerivat kovAPhaseImDerivat, Optional<List<Long>> sensorCocIds) {
        if (sensorCocIds.isPresent()) {
            return kovAPhaseImDerivatDao.getNumberOfAllUmgesetzteAnforderungenInUmsetzungsbestaetigung(kovAPhaseImDerivat, sensorCocIds.get());
        } else {
            return 0L;
        }
    }

    private Optional<List<Long>> getSensorCocIdsForUser(long kovaPhaseImDerivatId,
                                                        Optional<Rolle> relevantRoleForQuery) {
        Mitarbeiter user = session.getUser();
        Optional<List<Long>> sensorCocIds = Optional.empty();
        if (relevantRoleForQuery.isPresent()) {
            switch (relevantRoleForQuery.get()) {
                case ADMIN:
                    sensorCocIds = Optional.empty();
                    break;
                case ANFORDERER:
                    sensorCocIds = Optional.of(berechtigungService.
                            getAuthorizedSensorCocListForMitarbeiter(user));
                    break;
                case SENSORCOCLEITER:
                    sensorCocIds = Optional.of(session.getUserPermissions()
                            .getSensorCocAsSensorCocLeiterSchreibend().stream().map(BerechtigungDto::getId).collect(Collectors.toList()));
                    break;
                case SCL_VERTRETER:
                    sensorCocIds = Optional.of(session.getUserPermissions()
                            .getSensorCocAsVertreterSCLSchreibend().stream().map(BerechtigungDto::getId).collect(Collectors.toList()));
                    break;
                case UMSETZUNGSBESTAETIGER:
                    sensorCocIds = Optional.of(kovAPhaseImDerivatDao.getSensorCocIdsForUmsetzungsbestaetiger(
                            kovaPhaseImDerivatId, user.getId()));
                    break;
                default:
                    sensorCocIds = Optional.empty();
                    break;
            }
        }
        return sensorCocIds;
    }

}
