package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Umsetzungsbestaetigung;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collections;
import java.util.List;

/**
 * @author fn
 */
@Stateless
public class UmsetzungsbestaetigungSearchService {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager em;

    public List<Umsetzungsbestaetigung> getExistingUmsetzungsbestaetigung(KovAPhaseImDerivat kovAPhaseImDerivat, UmsetzungsbestaetigungViewFilter filter, List<Long> userSensorCoc) {

        if (isSensorCocListEmpty(userSensorCoc)) {
            return Collections.emptyList();
        }

        QueryPart queryPart = getQueryPartDTOForFilter(kovAPhaseImDerivat, filter, userSensorCoc);
        if (filter.getUmsetzungsbestaetigungStatusFilter().isActive()) {
            UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungStatusQuery(queryPart, filter.getUmsetzungsbestaetigungStatusFilter());
        }

        Query query = em.createQuery(queryPart.getQuery(), Umsetzungsbestaetigung.class);
        queryPart.getParameterMap().forEach(query::setParameter);
        return query.getResultList();

    }

    private QueryPart getQueryPartDTOForFilter(KovAPhaseImDerivat kovAPhaseImDerivat, UmsetzungsbestaetigungViewFilter filter, List<Long> userSensorCoc) {
        final boolean prozessbaukastenFilterIsActive = filter.getProzessbaukastenFilter().isActive();

        QueryPart queryPart = UmsetzungsbestaetigungSearchUtils.getBaseQueryForExistingUmsetzungsbestaetigung(kovAPhaseImDerivat.getId(), userSensorCoc, prozessbaukastenFilterIsActive);

        if (filter.getAnforderungFilter().isActive()) {
            UmsetzungsbestaetigungSearchUtils.getAnforderungQuery(queryPart, filter.getAnforderungFilter());
        }
        if (filter.getBeschreibungFilter().isActive()) {
            UmsetzungsbestaetigungSearchUtils.getBeschreibungQuery(queryPart, filter.getBeschreibungFilter());
        }
        if (filter.getModulFilter().isActive()) {
            UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungModulQuery(queryPart, filter.getModulFilter());
        }
        if (filter.getSensorCocFilter().isActive()) {
            UmsetzungsbestaetigungSearchUtils.getSensorCocQuery(queryPart, filter.getSensorCocFilter());
        }
        if (prozessbaukastenFilterIsActive) {
            UmsetzungsbestaetigungSearchUtils.getProzessbaukastenQuery(queryPart, filter.getProzessbaukastenFilter());
        }

        UmsetzungsbestaetigungSearchUtils.getThemenklammerQuery(queryPart, filter.getThemenklammerFilter());

        return queryPart;
    }

    private boolean isSensorCocListEmpty(List<Long> userSensorCoc) {
        return userSensorCoc == null || userSensorCoc.isEmpty();
    }

    public List<Umsetzungsbestaetigung> getExistingUmsetzungsbestaetigungForOnlyOffenStatus(KovAPhaseImDerivat kovAPhaseImDerivat, UmsetzungsbestaetigungViewFilter filter, List<Long> userSensorCoc) {

        if (isSensorCocListEmpty(userSensorCoc)) {
            return Collections.emptyList();
        }

        QueryPart queryPart = getQueryPartDTOForFilter(kovAPhaseImDerivat, filter, userSensorCoc);
        if (filter.getUmsetzungsbestaetigungStatusFilter().isActive()) {
            UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungStatusQueryForOnlyOffenStatus(queryPart);
        }

        Query query = em.createQuery(queryPart.getQuery(), Umsetzungsbestaetigung.class);
        queryPart.getParameterMap().forEach(query::setParameter);
        return query.getResultList();

    }

    private boolean isNoOffenStatusFilterSelected(UmsetzungsbestaetigungViewFilter filter) {
        return !filter.getUmsetzungsbestaetigungStatusFilter().getAsEnumList().contains(UmsetzungsBestaetigungStatus.OFFEN);
    }

    public List<DerivatAnforderungModul> getVereinbarungWithStatusAngenommen(KovAPhaseImDerivat kovAPhaseImDerivat, UmsetzungsbestaetigungViewFilter filter, List<Long> userSensorCoc) {

        if (isSensorCocListEmpty(userSensorCoc)) {
            return Collections.emptyList();
        }

        QueryPart queryPart = UmsetzungsbestaetigungSearchUtils.getBaseQueryForDerivatAnforderungModul(kovAPhaseImDerivat, userSensorCoc);

        if (filter.getAnforderungFilter().isActive()) {
            UmsetzungsbestaetigungSearchUtils.getAnforderungQuery(queryPart, filter.getAnforderungFilter());
        }
        if (filter.getBeschreibungFilter().isActive()) {
            UmsetzungsbestaetigungSearchUtils.getBeschreibungQuery(queryPart, filter.getBeschreibungFilter());
        }
        if (filter.getModulFilter().isActive()) {
            UmsetzungsbestaetigungSearchUtils.getDerivatAnforderungModulModulQuery(queryPart, filter.getModulFilter());
        }
        if (filter.getSensorCocFilter().isActive()) {
            UmsetzungsbestaetigungSearchUtils.getSensorCocQuery(queryPart, filter.getSensorCocFilter());
        }
        if (filter.getProzessbaukastenFilter().isActive()) {
            UmsetzungsbestaetigungSearchUtils.getProzessbaukastenQuery(queryPart, filter.getProzessbaukastenFilter());
        }
        UmsetzungsbestaetigungSearchUtils.getThemenklammerQuery(queryPart, filter.getThemenklammerFilter());

        if (filter.getUmsetzungsbestaetigungStatusFilter().isActive()) {
            if (isNoOffenStatusFilterSelected(filter)) {
                return Collections.emptyList();
            }
        }

        Query query = em.createQuery(queryPart.getQuery(), DerivatAnforderungModul.class);
        queryPart.getParameterMap().forEach(query::setParameter);
        return query.getResultList();

    }

}
