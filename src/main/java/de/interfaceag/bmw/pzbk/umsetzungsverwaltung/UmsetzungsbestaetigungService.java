package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.dao.UmsetzungsbestaetigungDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.UBzuKovaPhaseImDerivatUndSensorCoC;
import de.interfaceag.bmw.pzbk.entities.Umsetzungsbestaetigung;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.ProcessResultDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungExcelDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungSearchService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewFilter;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl
 */
@Stateless
@Named
public class UmsetzungsbestaetigungService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(UmsetzungsbestaetigungService.class);

    @Inject
    private UmsetzungsbestaetigungDao umsetzungsBestaetigungDao;
    @Inject
    UmsetzungsbestaetigungBerechtigungService umsetzungsbestaetigungBerechtigungService;

    @Inject
    BerechtigungService berechtigungService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private AnforderungService anforderungService;
    @Inject
    DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private LocalizationService localizationService;
    @Inject
    private ExcelDownloadService excelDownloadService;
    @Inject
    private Session session;
    @Inject
    private UmsetzungsbestaetigungSearchService umsetzungsbestaetigungSearchService;

    public void persistUmsetzungsbestaetigungen(List<Umsetzungsbestaetigung> umsetzungsbestaetigungen) {
        umsetzungsBestaetigungDao.persistUmsetzungsbestaetigungen(umsetzungsbestaetigungen);
    }

    public void removeUmsetzungsbestaetigungen(List<Umsetzungsbestaetigung> umsetzungsbestaetigungen) {
        umsetzungsBestaetigungDao.removeUmsetzungsbestaetigungen(umsetzungsbestaetigungen);
    }

    public List<Umsetzungsbestaetigung> createUmsetzungsbestaetigungenForDerivatAnforderungModul(DerivatAnforderungModul derivatAnforderungModul) {
        Anforderung anforderung = derivatAnforderungModul.getAnforderung();
        Derivat derivat = derivatAnforderungModul.getDerivat();
        Modul modul = derivatAnforderungModul.getModul();
        List<Umsetzungsbestaetigung> result = new ArrayList<>();
        KovAPhase.getAllKovAPhasen().forEach(phase -> {
            Optional<Umsetzungsbestaetigung> existingUmsetzungsbestaetigung = getUmsetzungsbestaetigungByAnforderungModulDerivatPhase(anforderung, derivat, modul, phase);
            if (!existingUmsetzungsbestaetigung.isPresent()) {
                KovAPhaseImDerivat kova = derivatService.getKovAPhaseImDerivatByDerivatAndPhase(derivat, phase);
                Umsetzungsbestaetigung umsetzungsbestaetigung = new Umsetzungsbestaetigung(anforderung, kova, modul);
                result.add(umsetzungsbestaetigung);
            }
        });
        return result;
    }

    private List<Long> getSensorCocListForMitarbeiter(KovAPhaseImDerivat kovaPhaseImDerivat) {
        Mitarbeiter user = session.getUser();
        Set<Long> result = new HashSet<>();
        List<Rolle> rollen = session.getUserPermissions().getRollen();
        for (Rolle rolle : rollen) {
            switch (rolle) {
                case ADMIN:
                    return sensorCocService.getAllSensorCocIds();
                case UMSETZUNGSBESTAETIGER:
                    Collection<Long> sensorCocIdsForUmsetzungsbestaetiger = getSensorCocIdsForUmsetzungsbestaetiger(kovaPhaseImDerivat);
                    result.addAll(sensorCocIdsForUmsetzungsbestaetiger);
                    break;
                case SENSORCOCLEITER:
                    Set<Long> sensorCocIdsForSensorCocLeiter = getSensorCocIdsForSensorCocLeiter();
                    result.addAll(sensorCocIdsForSensorCocLeiter);
                    break;
                case ANFORDERER:
                    List<Long> sensorCocIdsForAnforderer = berechtigungService.getAuthorizedSensorCocListForMitarbeiter(user);
                    result.addAll(sensorCocIdsForAnforderer);
                    break;
                default:
                    break;
            }
        }
        return new ArrayList<>(result);
    }

    private Collection<Long> getSensorCocIdsForUmsetzungsbestaetiger(KovAPhaseImDerivat kovaPhaseImDerivat) {
        Mitarbeiter user = session.getUser();
        return umsetzungsbestaetigungBerechtigungService.getSensorCocIdsAsUmsetzungsverwalterForKovaPhaseImDerivat(kovaPhaseImDerivat, user);
    }

    private Set<Long> getSensorCocIdsForSensorCocLeiter() {
        List<Long> sensorCocIdsSchreibend = session.getUserPermissions().getSensorCocAsSensorCocLeiterSchreibend().stream().map(BerechtigungDto::getId).collect(Collectors.toList());
        List<Long> sensorCocIdsLesend = session.getUserPermissions().getSensorCocAsSensorCocLeiterLesend().stream().map(BerechtigungDto::getId).collect(Collectors.toList());
        Set<Long> result = new HashSet<>(sensorCocIdsSchreibend);
        result.addAll(sensorCocIdsLesend);
        return result;
    }

    public List<UmsetzungsbestaetigungDto> getFilterdUmsetzungsbestaetigung(UmsetzungsbestaetigungViewFilter filter, KovAPhaseImDerivat kovAPhaseImDerivat, KovAPhase selectedPreviousKovAPhase) {

        List<Long> userSensorCoc = getSensorCocListForMitarbeiter(kovAPhaseImDerivat);

        List<Umsetzungsbestaetigung> allUmsetzungsbestaetigungForSelectedPhase = umsetzungsBestaetigungDao.getUmsetzungsbestaetigungByDerivatAndPhaseForUserSensorCoc(kovAPhaseImDerivat.getDerivat(), selectedPreviousKovAPhase, userSensorCoc);

        List<UmsetzungsbestaetigungDto> returnList;

        if (isOnlyOffenStatusFilterSelected(filter)) {
            returnList = getUmsetzungsbestaetigungDtosForOnlyOffenFilter(kovAPhaseImDerivat, filter, allUmsetzungsbestaetigungForSelectedPhase, userSensorCoc);
        } else {
            returnList = getUmsetzungsbestaetigungDtos(kovAPhaseImDerivat, filter, allUmsetzungsbestaetigungForSelectedPhase, userSensorCoc);
        }

        setIdInDtoForView(returnList);

        return returnList;
    }

    private boolean isOnlyOffenStatusFilterSelected(UmsetzungsbestaetigungViewFilter filter) {
        return filter.getUmsetzungsbestaetigungStatusFilter().getAsEnumList().contains(UmsetzungsBestaetigungStatus.OFFEN) && filter.getUmsetzungsbestaetigungStatusFilter().getAsEnumList().size() == 1;
    }

    private List<UmsetzungsbestaetigungDto> getUmsetzungsbestaetigungDtos(KovAPhaseImDerivat kovAPhaseImDerivat, UmsetzungsbestaetigungViewFilter filter, List<Umsetzungsbestaetigung> allUmsetzungsbestaetigungForSelectedPhase, List<Long> userSensorCoc) {
        List<UmsetzungsbestaetigungDto> returnList;
        Collection<UmsetzungsbestaetigungDto> vereinbarungWithStatusAngenommen;

        if (filter.getUmsetzungsbestaetigungStatusFilter().getAsEnumList().contains(UmsetzungsBestaetigungStatus.OFFEN)) {
            vereinbarungWithStatusAngenommen = getUmsetzungsbestaetigungDtosForOnlyOffenFilter(kovAPhaseImDerivat, filter, allUmsetzungsbestaetigungForSelectedPhase, userSensorCoc);
        } else {
            vereinbarungWithStatusAngenommen = getVereinbarungWithStatusAngenommen(kovAPhaseImDerivat, filter, allUmsetzungsbestaetigungForSelectedPhase, userSensorCoc);
        }

        Set<UmsetzungsbestaetigungDto> existingForPhase = getExistingForPhase(kovAPhaseImDerivat, filter, allUmsetzungsbestaetigungForSelectedPhase, userSensorCoc);
        existingForPhase.addAll(vereinbarungWithStatusAngenommen);
        returnList = new ArrayList<>(existingForPhase);
        return returnList;
    }

    private List<UmsetzungsbestaetigungDto> getUmsetzungsbestaetigungDtosForOnlyOffenFilter(KovAPhaseImDerivat kovAPhaseImDerivat, UmsetzungsbestaetigungViewFilter filter, List<Umsetzungsbestaetigung> allUmsetzungsbestaetigungForSelectedPhase, List<Long> userSensorCoc) {
        List<UmsetzungsbestaetigungDto> returnList;
        Set<UmsetzungsbestaetigungDto> vereinbarungWithStatusAngenommen = getVereinbarungWithStatusAngenommen(kovAPhaseImDerivat, filter, allUmsetzungsbestaetigungForSelectedPhase, userSensorCoc);
        Set<UmsetzungsbestaetigungDto> existingForPhase = getExistingPhaseForOnlyOffenStatus(kovAPhaseImDerivat, filter, allUmsetzungsbestaetigungForSelectedPhase, userSensorCoc);
        vereinbarungWithStatusAngenommen.removeAll(existingForPhase);
        returnList = new ArrayList<>(vereinbarungWithStatusAngenommen);
        return returnList;
    }

    private List<UmsetzungsbestaetigungDto> setIdInDtoForView(List<UmsetzungsbestaetigungDto> returnList) {
        int i = 0;
        for (UmsetzungsbestaetigungDto umsetzungsbestaetigung : returnList) {
            umsetzungsbestaetigung.setId(i);
            i++;
        }
        return returnList;
    }

    private Set<UmsetzungsbestaetigungDto> getExistingForPhase(KovAPhaseImDerivat kovAPhaseImDerivat, UmsetzungsbestaetigungViewFilter filter, List<Umsetzungsbestaetigung> allUmsetzungsbestaetigungForSelectedPhase, List<Long> userSensorCoc) {

        List<Umsetzungsbestaetigung> existingForPhase = umsetzungsbestaetigungSearchService.getExistingUmsetzungsbestaetigung(kovAPhaseImDerivat, filter, userSensorCoc);

        return generateUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(existingForPhase, allUmsetzungsbestaetigungForSelectedPhase);
    }

    private Set<UmsetzungsbestaetigungDto> getExistingPhaseForOnlyOffenStatus(KovAPhaseImDerivat kovAPhaseImDerivat, UmsetzungsbestaetigungViewFilter filter, List<Umsetzungsbestaetigung> allUmsetzungsbestaetigungForSelectedPhase, List<Long> userSensorCoc) {

        List<Umsetzungsbestaetigung> existingForPhase = umsetzungsbestaetigungSearchService.getExistingUmsetzungsbestaetigungForOnlyOffenStatus(kovAPhaseImDerivat, filter, userSensorCoc);

        return generateUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(existingForPhase, allUmsetzungsbestaetigungForSelectedPhase);
    }


    private Set<UmsetzungsbestaetigungDto> getVereinbarungWithStatusAngenommen(KovAPhaseImDerivat kovAPhaseImDerivat, UmsetzungsbestaetigungViewFilter filter, List<Umsetzungsbestaetigung> allUmsetzungsbestaetigungForSelectedPhase, List<Long> userSensorCoc) {

        List<DerivatAnforderungModul> derivatAnforderungModul = umsetzungsbestaetigungSearchService.getVereinbarungWithStatusAngenommen(kovAPhaseImDerivat, filter, userSensorCoc);

        return generateUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, allUmsetzungsbestaetigungForSelectedPhase, kovAPhaseImDerivat.getKovAPhase());

    }

    private Set<UmsetzungsbestaetigungDto> generateUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(List<Umsetzungsbestaetigung> existingForPhase, List<Umsetzungsbestaetigung> previousPhase) {

        return existingForPhase.stream().map(umsetzungsbestaetigung
                -> getUmsetzungsbestaetigungDtoWithPreviusPhase(umsetzungsbestaetigung, previousPhase)
        ).collect(Collectors.toSet());
    }

    private Set<UmsetzungsbestaetigungDto> generateUmsetzungsbestaetigungDtoForDerivatAnforderungModul(List<DerivatAnforderungModul> derivatAnforderungModuls, List<Umsetzungsbestaetigung> previousPhase, KovAPhase kovAPhase) {
        return derivatAnforderungModuls.stream().map(derivatAnforderungModul
                -> getUmsetzungsbestaetigungDtoWithPreviusPhase(derivatAnforderungModul, previousPhase, kovAPhase)
        ).collect(Collectors.toSet());
    }

    private UmsetzungsbestaetigungDto getUmsetzungsbestaetigungDtoWithPreviusPhase(Umsetzungsbestaetigung existingForPhase, List<Umsetzungsbestaetigung> previousPhase) {

        UmsetzungsBestaetigungStatus previousStatus = null;
        String previousCommentar = null;

        for (Umsetzungsbestaetigung umsetzungsbestaetigung : previousPhase) {
            if (isSameAnforderungAndModul(existingForPhase, umsetzungsbestaetigung)) {
                previousStatus = umsetzungsbestaetigung.getStatus();
                previousCommentar = umsetzungsbestaetigung.getKommentar();
            }
        }

        String umsetzungsbestaetigerAsString = getUmsetzungsbestaetigerAsString(existingForPhase);


        return UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(existingForPhase, previousStatus, previousCommentar, umsetzungsbestaetigerAsString);
    }

    private String getUmsetzungsbestaetigerAsString(Umsetzungsbestaetigung existingForPhase) {
        Optional<UBzuKovaPhaseImDerivatUndSensorCoC> uBzuKovaPhaseImDerivatUndSensorCoC = umsetzungsbestaetigungBerechtigungService.getBerechtigungenListForKovAPhaseImDerivatAndSensorCoc(existingForPhase.getKovaImDerivat(), existingForPhase.getAnforderung().getSensorCoc());
        return getStringNamesForUmsetzungsbestaetiger(uBzuKovaPhaseImDerivatUndSensorCoC);
    }

    private String getStringNamesForUmsetzungsbestaetiger(Optional<UBzuKovaPhaseImDerivatUndSensorCoC> uBzuKovaPhaseImDerivatUndSensorCoC) {
        if (uBzuKovaPhaseImDerivatUndSensorCoC.isPresent()) {
            List<Mitarbeiter> umsetzungsbestaetiger = uBzuKovaPhaseImDerivatUndSensorCoC.get().getUmsetzungsbestaetiger();
            return umsetzungsbestaetiger.stream().map(Mitarbeiter::getFullName).collect(Collectors.joining(", "));
        } else {
            return localizationService.getValue("umsetzungsbestaetigung_sensorcoc_hover_bestaetiger_nichtgesetzt");
        }
    }

    private UmsetzungsbestaetigungDto getUmsetzungsbestaetigungDtoWithPreviusPhase(DerivatAnforderungModul derivatAnforderungModul, List<Umsetzungsbestaetigung> previousPhase, KovAPhase kovAPhase) {

        UmsetzungsBestaetigungStatus previousStatus = null;
        String previousCommentar = null;

        for (Umsetzungsbestaetigung umsetzungsbestaetigung : previousPhase) {
            if (isSameAnforderungAndModul(derivatAnforderungModul, umsetzungsbestaetigung)) {
                previousStatus = umsetzungsbestaetigung.getStatus();
                previousCommentar = umsetzungsbestaetigung.getKommentar();
            }
        }

        String umsetzungsbestaetigerAsString = getUmsetzungsbestaetigerAsStringForDerivatAnforderungModul(derivatAnforderungModul, kovAPhase);

        return UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, previousStatus, previousCommentar, umsetzungsbestaetigerAsString);
    }

    private String getUmsetzungsbestaetigerAsStringForDerivatAnforderungModul(DerivatAnforderungModul derivatAnforderungModul, KovAPhase kovAPhase) {
        Optional<UBzuKovaPhaseImDerivatUndSensorCoC> uBzuKovaPhaseImDerivatUndSensorCoC = umsetzungsbestaetigungBerechtigungService.getUBzuKovaPhaseImDerivatUndSensorCoCByDerivatSensorCocAndPhase(derivatAnforderungModul.getAnforderung().getSensorCoc(), derivatAnforderungModul.getDerivat(), kovAPhase);
        return getStringNamesForUmsetzungsbestaetiger(uBzuKovaPhaseImDerivatUndSensorCoC);
    }

    private boolean isSameAnforderungAndModul(DerivatAnforderungModul derivatAnforderungModul, Umsetzungsbestaetigung umsetzungsbestaetigung) {
        return umsetzungsbestaetigung.getAnforderung().getId().equals(derivatAnforderungModul.getAnforderung().getId()) && umsetzungsbestaetigung.getModul().getId().equals(derivatAnforderungModul.getModul().getId());
    }

    private boolean isSameAnforderungAndModul(Umsetzungsbestaetigung existingForPhase, Umsetzungsbestaetigung umsetzungsbestaetigung) {
        return umsetzungsbestaetigung.getAnforderung().getId().equals(existingForPhase.getAnforderung().getId()) && umsetzungsbestaetigung.getModul().getId().equals(existingForPhase.getModul().getId());
    }

    private List<Umsetzungsbestaetigung> getUmsetzungsbestaetigungenByDerivatAndAnforderung(Derivat derivat, Anforderung anforderung) {
        return umsetzungsBestaetigungDao.getUmsetzungsbestaetigungenByDerivatAndAnforderung(derivat, anforderung);
    }

    public List<Umsetzungsbestaetigung> getUmsetzungsbestaetigungenByDerivatAnforderungModul(
            DerivatAnforderungModul derivatAnforderungModul) {
        return umsetzungsBestaetigungDao.getUmsetzungsbestaetigungByDerivatAnforderungModul(derivatAnforderungModul.getDerivat(),
                derivatAnforderungModul.getAnforderung(), derivatAnforderungModul.getModul());
    }

    public Collection<Mitarbeiter> getSensorCocLeiterAndVertreterForKovaPhaseImDerivat(KovAPhaseImDerivat kovAPhaseImDerivat) {
        List<SensorCoc> sensorCocsInKovaPhaseImDerivat = getSensorCocForDerivatInKovaPhase(kovAPhaseImDerivat);
        return berechtigungService.getSensorCoCLeiterAndVertreterForSensorCocs(sensorCocsInKovaPhaseImDerivat);
    }

    public List<SensorCoc> getSensorCocForDerivatInKovaPhase(KovAPhaseImDerivat kovAPhaseImDerivat) {

        Set<SensorCoc> sensorCocInKovaPhase = new HashSet<>();

        List<Integer> status = new ArrayList<>();
        status.add(DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId());

        List<SensorCoc> sensorCocsFromDerivatAnforderungModul = derivatAnforderungModulService.getSensorCocForDerivatStatusAndKovaPhaseImDerivat(status, kovAPhaseImDerivat);

        sensorCocInKovaPhase.addAll(sensorCocsFromDerivatAnforderungModul);

        List<SensorCoc> sensorCocsFormUmsetzungsbestaetigung = umsetzungsBestaetigungDao.getUmsetzungsbestaetigungSensorCocForDerivatAndPhase(kovAPhaseImDerivat.getDerivat(), kovAPhaseImDerivat.getKovAPhase());

        sensorCocInKovaPhase.addAll(sensorCocsFormUmsetzungsbestaetigung);

        return new ArrayList<>(sensorCocInKovaPhase);
    }

    public List<KovAPhaseImDerivat> getKovaphasenByKovastatusAndStatus(KovAStatus kovAStatus,
                                                                       UmsetzungsBestaetigungStatus umsetzungsBestaetigungStatus) {
        return umsetzungsBestaetigungDao.getKovaphasenByKovastatusAndStatus(kovAStatus, umsetzungsBestaetigungStatus);
    }

    public Optional<Umsetzungsbestaetigung> getUmsetzungsbestaetigungByAnforderungModulDerivatPhase(Anforderung anforderung, Derivat derivat, Modul modul, KovAPhase kovAPhase) {
        return umsetzungsBestaetigungDao.getUmsetzungsbestaetigungByAnforderungModulDerivatPhase(anforderung, derivat, modul, kovAPhase);
    }

    public void persistUmsetzungsbestaetigung(Umsetzungsbestaetigung umsetzungsbestaetigung) {
        umsetzungsBestaetigungDao.persistUmsetzungsbestaetigung(umsetzungsbestaetigung);
    }

    public void removeUmsetzungsbestaetigungenForAnforderungDerivat(ZuordnungAnforderungDerivat anforderungDerivat) {
        List<Umsetzungsbestaetigung> umsetzungsbestaetigungen = getUmsetzungsbestaetigungenByDerivatAndAnforderung(anforderungDerivat.getDerivat(), anforderungDerivat.getAnforderung());
        umsetzungsbestaetigungen.forEach(umsetzungsbestaetigung -> umsetzungsBestaetigungDao.removeUmsetzungsbestaetigung(umsetzungsbestaetigung));
    }

    public void downloadExcelExport(List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungen,
                                    KovAPhaseImDerivat kovAPhaseImDerivat, KovAPhase lastPhase) {
        Workbook workbook = generateExcelExport(umsetzungsbestaetigungen, kovAPhaseImDerivat, lastPhase);
        excelDownloadService.downloadExcelExport("Umsetzungsbestaetigung", workbook);
    }

    public Workbook generateExcelExport(List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungen,
                                        KovAPhaseImDerivat kovAPhaseImDerivat, KovAPhase vorigePhase) {

        Workbook workbook = new XSSFWorkbook();
        CellStyle headerStyle = createHeaderCellStyle(workbook);

        Sheet sheet = workbook.createSheet(localizationService.getValue("umsetzungsbestaetigung_umsetzungsbestaetigung"));

        // create header
        Row headerRow = sheet.createRow(0);
        boolean hasVorigeKovaPhase = UmsetzungsbestaetigungUtils.hasLastPhase(kovAPhaseImDerivat);
        createExcelHeaderRow(headerRow, headerStyle, hasVorigeKovaPhase, kovAPhaseImDerivat, vorigePhase);

        // create content
        for (int i = 0; i < umsetzungsbestaetigungen.size(); i++) {
            Row row = sheet.createRow(i + 1);
            UmsetzungsbestaetigungDto umsetzungsbestaetigung = umsetzungsbestaetigungen.get(i);
            UmsetzungsbestaetigungExcelDto excelData = createExcelDataForUmsetzungsbestaetigung(umsetzungsbestaetigung, hasVorigeKovaPhase);
            createExcelDataRowForUmsetzungsbestaetigung(row, excelData, hasVorigeKovaPhase);
        }

        return workbook;
    }

    private Row createExcelHeaderRow(Row row, CellStyle headerStyle, boolean hasVorigeKovaPhase, KovAPhaseImDerivat kovAPhaseImDerivat, KovAPhase vorigePhase) {
        List<Cell> cells = new ArrayList<>();

        Cell sensorCocCell = row.createCell(0);
        Cell anforderungCell = row.createCell(1);
        Cell beschreibungCell = row.createCell(2);

        sensorCocCell.setCellValue(localizationService.getValue("umsetzungsbestaetigung_sensorCoc"));
        anforderungCell.setCellValue(localizationService.getValue("umsetzungsbestaetigung_anforderung"));
        beschreibungCell.setCellValue(localizationService.getValue("umsetzungsbestaetigung_beschreibung"));

        cells.add(sensorCocCell);
        cells.add(anforderungCell);
        cells.add(beschreibungCell);

        String statusHeader;
        String kommentarHeader;

        Integer columnIndex = 3;
        if (hasVorigeKovaPhase) {
            statusHeader = localizationService.getValue("umsetzungsbestaetigung_status") + " " + UmsetzungsbestaetigungUtils.getLastPhaseString(vorigePhase);
            kommentarHeader = localizationService.getValue("umsetzungsbestaetigung_kommentar") + " " + UmsetzungsbestaetigungUtils.getLastPhaseString(vorigePhase);

            Cell vorigeStatusCell = row.createCell(columnIndex);
            Cell vorigeKommentarCell = row.createCell(columnIndex + 1);
            vorigeStatusCell.setCellValue(statusHeader);
            vorigeKommentarCell.setCellValue(kommentarHeader);

            cells.add(vorigeStatusCell);
            cells.add(vorigeKommentarCell);

            columnIndex = 5;
        }

        statusHeader = localizationService.getValue("umsetzungsbestaetigung_status") + " " + kovAPhaseImDerivat.getKovAPhase().getBezeichnung();
        kommentarHeader = localizationService.getValue("umsetzungsbestaetigung_kommentar") + " " + kovAPhaseImDerivat.getKovAPhase().getBezeichnung();

        Cell statusCell = row.createCell(columnIndex);
        Cell kommentarCell = row.createCell(columnIndex + 1);
        statusCell.setCellValue(statusHeader);
        kommentarCell.setCellValue(kommentarHeader);

        cells.add(statusCell);
        cells.add(kommentarCell);

        cells.forEach(cell ->
                cell.setCellStyle(headerStyle)
        );

        return row;
    }

    private Row createExcelDataRowForUmsetzungsbestaetigung(Row row, UmsetzungsbestaetigungExcelDto excelData, boolean hasVorigeKovaPhase) {
        row.createCell(0).setCellValue(excelData.getSensorCoc());
        row.createCell(1).setCellValue(excelData.getAnforderung());
        row.createCell(2).setCellValue(excelData.getBeschreibung());

        Integer columnIndex = 3;
        if (hasVorigeKovaPhase) {
            row.createCell(columnIndex).setCellValue(excelData.getVorigeKovaPhaseStatus());
            row.createCell(columnIndex + 1).setCellValue(excelData.getVorigeKovaPhaseKommentar());
            columnIndex = 5;
        }

        row.createCell(columnIndex).setCellValue(excelData.getKovaPhaseStatus());
        row.createCell(columnIndex + 1).setCellValue(excelData.getKovaPhaseKommentar());

        return row;
    }

    public UmsetzungsbestaetigungExcelDto createExcelDataForUmsetzungsbestaetigung(UmsetzungsbestaetigungDto umsetzungsbestaetigung, boolean hasVorigeKovaPhase) {
        UmsetzungsbestaetigungExcelDto excelDto = buildExcelDtoWithBasisDaten(umsetzungsbestaetigung);
        if (hasVorigeKovaPhase) {
            buildExcelDtoWithVorigeKovaphase(excelDto, umsetzungsbestaetigung);
        }

        return excelDto;
    }

    private UmsetzungsbestaetigungExcelDto buildExcelDtoWithBasisDaten(UmsetzungsbestaetigungDto umsetzungsbestaetigung) {
        String sensorCoc = umsetzungsbestaetigung.getSensorCoc().getTechnologie();
        String anforderung = umsetzungsbestaetigung.getAnforderung();
        String beschreibung = umsetzungsbestaetigung.getFullText();

        String kovaPhaseStatus = getStatusBezeichnung(umsetzungsbestaetigung.getStatus());
        String kovaPhaseKommentar = umsetzungsbestaetigung.getComment();

        return UmsetzungsbestaetigungExcelDto.getBuilder()
                .withSensorCoc(sensorCoc)
                .withAnforderung(anforderung)
                .withBeschreibung(beschreibung)
                .withKovaPhaseStatus(kovaPhaseStatus)
                .withKovaPhaseKommentar(kovaPhaseKommentar)
                .build();
    }

    private UmsetzungsbestaetigungExcelDto buildExcelDtoWithVorigeKovaphase(UmsetzungsbestaetigungExcelDto excelDto, UmsetzungsbestaetigungDto umsetzungsbestaetigung) {
        String statusOnPreviosPhase;
        String kommentarOnPreviosPhase = umsetzungsbestaetigung.getLastPhaseComment();
        Optional<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus = umsetzungsbestaetigung.getLastPhasestatus();
        if (umsetzungsBestaetigungStatus.isPresent()) {

            statusOnPreviosPhase = getStatusBezeichnung(umsetzungsBestaetigungStatus.get());
        } else {
            statusOnPreviosPhase = localizationService.getValue("umsetzungsbestaetigung_keineDatenVorhanden");
        }

        excelDto.setVorigeKovaPhaseStatus(statusOnPreviosPhase);
        excelDto.setVorigeKovaPhaseKommentar(kommentarOnPreviosPhase);
        return excelDto;
    }

    private CellStyle createHeaderCellStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 12);
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        style.setFont(font);
        return style;
    }

    private String getStatusBezeichnung(UmsetzungsBestaetigungStatus status) {
        return localizationService.getValue(status.getLocalizationName());
    }

    public ProcessResultDto processUmsetzungsbestaetigungChange(UmsetzungsbestaetigungDto umsetzungsbestaetigungDto,
                                                                Mitarbeiter currentUser, KovAPhaseImDerivat kovAPhaseImDerivat) {

        try {
            UmsetzungsbestaetigungUtils.checkForValidInputDataForProcessChanges(umsetzungsbestaetigungDto,
                    currentUser, kovAPhaseImDerivat);
            Locale currentLocale = localizationService.getCurrentLocale();
            UmsetzungsbestaetigungUtils.isKommentarOk(umsetzungsbestaetigungDto, currentLocale);

            Umsetzungsbestaetigung umsetzungsbestaetigung = changeUmsetzungsbestaetigungWithDto(umsetzungsbestaetigungDto, kovAPhaseImDerivat, umsetzungsbestaetigungDto.getDialogComment());

            umsetzungsbestaetigung.setDatum(Calendar.getInstance().getTime());
            umsetzungsbestaetigung.setUmsetzungsbestaetiger(currentUser);
            persistUmsetzungsbestaetigung(umsetzungsbestaetigung);
            String kommentar = umsetzungsbestaetigung.getKovaImDerivat().getKovAPhase().toString() + ": "
                    + umsetzungsbestaetigung.getKommentar();
            derivatAnforderungModulService.updateDerivatAnforderungModulStatusForUmsetzungsbestaetigung(
                    umsetzungsbestaetigung.getStatus(),
                    umsetzungsbestaetigung.getKovaImDerivat().getKovAPhase(), kommentar,
                    umsetzungsbestaetigung.getKovaImDerivat().getDerivat(),
                    umsetzungsbestaetigung.getAnforderung(), umsetzungsbestaetigung.getModul(), currentUser);

            return new ProcessResultDto(true);
        } catch (InvalidDataException ex) {
            LOG.warn(null, ex);
            return new ProcessResultDto(false, ex.getMessage());
        }
    }

    private Optional<Umsetzungsbestaetigung> getUmsetzungsbestaetigungByDtoData(UmsetzungsbestaetigungDto umsetzungsbestaetigungDto, KovAPhaseImDerivat kovAPhaseImDerivat) {

        Anforderung anforderung = anforderungService.getAnforderungById(umsetzungsbestaetigungDto.getAnforderungId());
        Derivat derivat = kovAPhaseImDerivat.getDerivat();
        Modul modul = umsetzungsbestaetigungDto.getModul();
        KovAPhase kovAPhase = kovAPhaseImDerivat.getKovAPhase();

        return getUmsetzungsbestaetigungByAnforderungModulDerivatPhase(anforderung, derivat, modul, kovAPhase);

    }

    private Umsetzungsbestaetigung changeUmsetzungsbestaetigungWithDto(UmsetzungsbestaetigungDto umsetzungsbestaetigungDto, KovAPhaseImDerivat kovAPhaseImDerivat, String newComment) {

        Optional<Umsetzungsbestaetigung> umsetzungsbestaetigung = getUmsetzungsbestaetigungByDtoData(umsetzungsbestaetigungDto, kovAPhaseImDerivat);

        if (umsetzungsbestaetigung.isPresent()) {
            umsetzungsbestaetigung.get().setStatus(umsetzungsbestaetigungDto.getStatus());
            umsetzungsbestaetigung.get().setKommentar(buildKommentarString(umsetzungsbestaetigungDto.getComment(), newComment));
            return umsetzungsbestaetigung.get();
        } else {
            return generateNewUmsetzungsbestaetigungForDto(umsetzungsbestaetigungDto, kovAPhaseImDerivat, newComment);
        }
    }

    public String buildKommentarString(String oldComment, String newComment) {
        if (!newComment.isEmpty()) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

            StringBuilder sb = new StringBuilder();
            sb.append("[").append(dateFormat.format(new Date())).append("] ")
                    .append(session.getUser().getName()).append(": ")
                    .append(newComment).append("; ").append(oldComment).append("\n");

            return sb.toString();
        } else {
            return "";
        }
    }

    private Umsetzungsbestaetigung generateNewUmsetzungsbestaetigungForDto(UmsetzungsbestaetigungDto umsetzungsbestaetigungDto, KovAPhaseImDerivat kovAPhaseImDerivat, String newComment) {
        Anforderung anforderung = anforderungService.getAnforderungById(umsetzungsbestaetigungDto.getAnforderungId());
        Umsetzungsbestaetigung umsetzungsbestaetigungNew = new Umsetzungsbestaetigung(anforderung, kovAPhaseImDerivat, umsetzungsbestaetigungDto.getModul());
        umsetzungsbestaetigungNew.setKommentar(buildKommentarString("", newComment));
        umsetzungsbestaetigungNew.setStatus(umsetzungsbestaetigungDto.getStatus());
        return umsetzungsbestaetigungNew;
    }

    public ProcessResultDto processUmsetzungsbestaetigungBatchChanges(
            List<UmsetzungsbestaetigungDto> selectedUmsetzungsbestaetigungen,
            UmsetzungsBestaetigungStatus groupStatus, String groupKommentar,
            Mitarbeiter currentUser, KovAPhaseImDerivat kovAPhaseImDerivat) {
        try {
            Locale currentLocale = localizationService.getCurrentLocale();
            UmsetzungsbestaetigungUtils.checkForValidInputDataForBatchPersist(
                    selectedUmsetzungsbestaetigungen, groupStatus,
                    groupKommentar, currentUser, currentLocale);

            selectedUmsetzungsbestaetigungen.stream()
                    .filter(umsetzungsbestaetigung -> !umsetzungsbestaetigung.getStatus().equals(UmsetzungsBestaetigungStatus.KEINE_WEITERVERFOLGUNG))
                    .forEach(umsetzungsbestaetigung ->
                            saveUmsetzungsbestaetigung(umsetzungsbestaetigung, kovAPhaseImDerivat, groupStatus, groupKommentar, currentUser)
                    );

            return new ProcessResultDto(true);
        } catch (InvalidDataException ex) {
            LOG.warn(null, ex);
            return new ProcessResultDto(false, ex.getMessage());
        }
    }

    private void saveUmsetzungsbestaetigung(UmsetzungsbestaetigungDto umsetzungsbestaetigungDto, KovAPhaseImDerivat kovAPhaseImDerivat,
                                            UmsetzungsBestaetigungStatus groupStatus, String groupKommentar, Mitarbeiter currentUser) {
        Umsetzungsbestaetigung umsetzungsbestaetigung = changeUmsetzungsbestaetigungWithDto(umsetzungsbestaetigungDto, kovAPhaseImDerivat, groupKommentar);
        umsetzungsbestaetigung.setStatus(groupStatus);

        umsetzungsbestaetigung.setDatum(Calendar.getInstance().getTime());
        umsetzungsbestaetigung.setUmsetzungsbestaetiger(currentUser);
        persistUmsetzungsbestaetigung(umsetzungsbestaetigung);
        String kommentar = umsetzungsbestaetigung.getKovaImDerivat().getKovAPhase().toString() + ": " + groupKommentar;
        derivatAnforderungModulService.updateDerivatAnforderungModulStatusForUmsetzungsbestaetigung(umsetzungsbestaetigungDto.getStatus(),
                umsetzungsbestaetigung.getKovaImDerivat().getKovAPhase(), kommentar, umsetzungsbestaetigung.getKovaImDerivat().getDerivat(),
                umsetzungsbestaetigung.getAnforderung(), umsetzungsbestaetigungDto.getModul(), currentUser);
    }

    public List<Umsetzungsbestaetigung> getUmsetzungsbestaetigungByAnforderungModul(Anforderung anforderung, Modul modul) {
        if (anforderung != null && modul != null) {
            return umsetzungsBestaetigungDao.getUmsetzungsbestaetigungenByAnforderungIdModulId(anforderung.getId(), modul.getId());
        } else {
            LOG.error("Anforderung or Modul is null!");
            return new ArrayList<>();
        }
    }

}
