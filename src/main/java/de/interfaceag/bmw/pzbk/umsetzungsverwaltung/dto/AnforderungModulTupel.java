package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;

/**
 * Diese Klasse ist als zwischen Objekt für die Anzeige der Vorphase erzeugt.
 *
 * @author fn
 */
public class AnforderungModulTupel extends GenericTuple<Anforderung, Modul> {

    public AnforderungModulTupel(Anforderung anforderung, Modul modul) {
        super(anforderung, modul);
    }

    public Modul getModul() {
        return getY();
    }

    public Anforderung getAnforderung() {
        return getX();
    }

}
