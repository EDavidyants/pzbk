package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
final class UmsetzungsbestaetigungUtils implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(UmsetzungsbestaetigungUtils.class.getName());

    private UmsetzungsbestaetigungUtils() {
    }

    static boolean hasLastPhase(KovAPhaseImDerivat kovAPhaseImDerivat) {
        return kovAPhaseImDerivat != null && kovAPhaseImDerivat.getKovAPhase() != null && kovAPhaseImDerivat.getKovAPhase().getId() != 1;
    }

    static String getLastPhaseString(KovAPhase lastPhase) {
        return lastPhase.toString();
    }

    static void isKommentarOk(UmsetzungsbestaetigungDto umsetzungsbestaetigungDto, Locale locale) throws InvalidDataException {
        if (!isKommentarOk(umsetzungsbestaetigungDto.getStatus(), umsetzungsbestaetigungDto.getDialogComment())) {
            String message = LocalizationService.getValue(locale, "kommentarInvalid");
            throw new InvalidDataException(message);
        }
    }

    private static boolean isKommentarForStatusRequired(UmsetzungsBestaetigungStatus status) {
        if (status == null) {
            try {
                throw new InvalidDataException("status is null");
            } catch (InvalidDataException ex) {
                LOG.error(null, ex);
                return false;
            }
        }
        return status.equals(UmsetzungsBestaetigungStatus.NICHT_UMGESETZT)
                || status.equals(UmsetzungsBestaetigungStatus.BEWERTUNG_NICHT_MOEGLICH)
                || status.equals(UmsetzungsBestaetigungStatus.NICHT_RELEVANT);
    }

    private static boolean isKommentarOk(UmsetzungsBestaetigungStatus status, String kommentar) {
        return kommentar != null
                ? !(isKommentarForStatusRequired(status) && kommentar.trim().isEmpty())
                : !isKommentarForStatusRequired(status);
    }

    static void checkForValidInputDataForBatchPersist(List<UmsetzungsbestaetigungDto> selectedUmsetzungsbestaetigungen,
                                                      UmsetzungsBestaetigungStatus groupStatus, String groupKommentar, Mitarbeiter currentUser,
                                                      Locale locale)
            throws InvalidDataException {
        if (selectedUmsetzungsbestaetigungen == null || selectedUmsetzungsbestaetigungen.isEmpty()) {
            throw new InvalidDataException("Nothing Selected");
        }
        if (groupStatus == null) {
            String message = LocalizationService.getValue(locale, "groupStatusIsNull");
            throw new InvalidDataException(message);
        }
        if (groupKommentar == null) {
            String message = LocalizationService.getValue(locale, "groupKommentarIsNull");
            throw new InvalidDataException(message);
        }
        if (!UmsetzungsbestaetigungUtils.isKommentarOk(groupStatus, groupKommentar)) {
            String message = LocalizationService.getValue(locale, "groupKommentarInvalid");
            throw new InvalidDataException(message);
        }
        if (currentUser == null) {
            throw new InvalidDataException("currentUser is null");
        }
    }

    static void checkForValidInputDataForProcessChanges(
            UmsetzungsbestaetigungDto umsetzungsbestaetigung,
            Mitarbeiter currentUser, KovAPhaseImDerivat kovAPhaseImDerivat) throws InvalidDataException {
        if (umsetzungsbestaetigung == null) {
            throw new InvalidDataException("umsetzungsbestaetigung is null");
        }
        if (currentUser == null) {
            throw new InvalidDataException("currentUser is null");
        }
        if (kovAPhaseImDerivat == null || kovAPhaseImDerivat.getKovAPhase() == null) {
            throw new InvalidDataException("kovaImDerivat is null");
        }
    }

}
