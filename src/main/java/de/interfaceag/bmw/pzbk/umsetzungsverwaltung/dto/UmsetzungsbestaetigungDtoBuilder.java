package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.prozessbaukasten.dto.ProzessbaukastenLabelDto;

public class UmsetzungsbestaetigungDtoBuilder {

    private Long anforderungId;
    private String anforderung;
    private String fachId;
    private String version;
    private String shortText;
    private String fullText;
    private SensorCoc sensorCoc;
    private Modul modul;
    private UmsetzungsBestaetigungStatus status;
    private String comment;
    private UmsetzungsBestaetigungStatus lastPhasestatus;
    private String lastPhaseComment;
    private ProzessbaukastenLabelDto prozessbaukastenLabelDto;
    private String eingetrageneUmsetzungsbestaetiger;

    public UmsetzungsbestaetigungDtoBuilder() {
    }

    public UmsetzungsbestaetigungDtoBuilder setAnforderungId(Long anforderungId) {
        this.anforderungId = anforderungId;
        return this;
    }

    public UmsetzungsbestaetigungDtoBuilder setAnforderung(String anforderung) {
        this.anforderung = anforderung;
        return this;
    }

    public UmsetzungsbestaetigungDtoBuilder setFachId(String fachId) {
        this.fachId = fachId;
        return this;
    }

    public UmsetzungsbestaetigungDtoBuilder setVersion(String version) {
        this.version = version;
        return this;
    }

    public UmsetzungsbestaetigungDtoBuilder setShortText(String shortText) {
        this.shortText = shortText;
        return this;
    }

    public UmsetzungsbestaetigungDtoBuilder setFullText(String fullText) {
        this.fullText = fullText;
        return this;
    }

    public UmsetzungsbestaetigungDtoBuilder setSensorCoc(SensorCoc sensorCoc) {
        this.sensorCoc = sensorCoc;
        return this;
    }

    public UmsetzungsbestaetigungDtoBuilder setModul(Modul modul) {
        this.modul = modul;
        return this;
    }

    public UmsetzungsbestaetigungDtoBuilder setStatus(UmsetzungsBestaetigungStatus status) {
        this.status = status;
        return this;
    }

    public UmsetzungsbestaetigungDtoBuilder setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public UmsetzungsbestaetigungDtoBuilder setLastPhasestatus(UmsetzungsBestaetigungStatus lastPhasestatus) {
        this.lastPhasestatus = lastPhasestatus;
        return this;
    }

    public UmsetzungsbestaetigungDtoBuilder setLastPhaseComment(String lastPhaseComment) {
        this.lastPhaseComment = lastPhaseComment;
        return this;
    }

    public UmsetzungsbestaetigungDtoBuilder withProzessbaukastenLabelDto(ProzessbaukastenLabelDto prozessbaukastenLabelDto) {
        this.prozessbaukastenLabelDto = prozessbaukastenLabelDto;
        return this;
    }

    UmsetzungsbestaetigungDtoBuilder withEingetragenenUmsetzungsbestaetiger(String eingetrageneUmsetzungsbestaetiger) {
        this.eingetrageneUmsetzungsbestaetiger = eingetrageneUmsetzungsbestaetiger;
        return this;
    }

    public UmsetzungsbestaetigungDto createUmsetzungsbestaetigungDto() {
        return new UmsetzungsbestaetigungDto(anforderungId, anforderung,
                fachId, version, shortText, fullText,
                sensorCoc, modul, status, comment,
                lastPhasestatus, lastPhaseComment, prozessbaukastenLabelDto, eingetrageneUmsetzungsbestaetiger);
    }

}
