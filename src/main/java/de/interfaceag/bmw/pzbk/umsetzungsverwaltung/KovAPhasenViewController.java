package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilter;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.KovAPhaseImDerivatDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.KovAPhasenViewPermission;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.KovaPhasenViewData;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl
 */
@ViewScoped
@Named
public class KovAPhasenViewController implements Serializable {

    @Inject
    private KovAPhasenViewFacade kovAPhasenViewFacade;

    private KovaPhasenViewData viewData;
    private KovaPhasenViewBerechtigungDialogData berechtigungDialogData;

    // ---------- init ---------------------------------------------------------
    @PostConstruct
    public void init() {
        initViewData();
    }

    public void initViewData() {
        viewData = kovAPhasenViewFacade.getViewData(UrlParameterUtils.getUrlParameter());
    }

    // --------- dialog --------------------------------------------------------
    public void showBerechtigungBearbeitenDialog(Long kovaPhasenid) {
        try {
            berechtigungDialogData = kovAPhasenViewFacade.getBerechtigungDialogData(kovaPhasenid)
                    .orElseThrow(() -> new InvalidDataException("Berechtigung Dialog Daten konnten nicht geladen werden."));

            RequestContext context = RequestContext.getCurrentInstance();
            context.update("dialog:kovaPhasenBerechtigungDialogForm");
            context.execute("PF('kovaPhasenBerechtigungDialog').show();");
        } catch (InvalidDataException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ex.toString()));
            RequestContext.getCurrentInstance().update("kovaPhasenContent:kovaPhasenForm:growl");
        }
    }

    public String processChanges() {
        kovAPhasenViewFacade.processBerechtigungDialogChanges(berechtigungDialogData);
        return filter();
    }

    // --------- filter --------------------------------------------------------
    public String filter() {
        return viewData.getFilter().getUrl();
    }

    public String reset() {
        return viewData.getFilter().getResetUrl();
    }

    // ---------- getter / setter ----------------------------------------------
    public SearchFilter getStatusFilter() {
        return viewData.getFilter().getStatusFilter();
    }

    public SearchFilter getPhaseFilter() {
        return viewData.getFilter().getPhaseFilter();
    }

    public SearchFilter getDerivatFilter() {
        return viewData.getFilter().getDerivatFilter();
    }

    public DateSearchFilter getVonStartDateFilter() {
        return viewData.getFilter().getVonStartDateFilter();
    }

    public DateSearchFilter getBisStartDateFilter() {
        return viewData.getFilter().getBisStartDateFilter();
    }

    public DateSearchFilter getVonEndDateFilter() {
        return viewData.getFilter().getVonEndDateFilter();
    }

    public DateSearchFilter getBisEndDateFilter() {
        return viewData.getFilter().getBisEndDateFilter();
    }

    public KovAPhasenViewPermission getViewPermission() {
        return viewData.getViewPermission();
    }

    public List<KovAPhaseImDerivatDto> getKovAPhasenImDerivat() {
        return viewData.getKovAPhasenImDerivat();
    }

    public KovaPhasenViewBerechtigungDialogData getBerechtigungDialogData() {
        return berechtigungDialogData;
    }

}
