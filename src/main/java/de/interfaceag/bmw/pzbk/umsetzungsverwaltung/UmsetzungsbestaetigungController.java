package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.converters.UmsetzungsBestaetigungStatusConverter;
import de.interfaceag.bmw.pzbk.shared.dto.ProcessResultDto;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewData;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewFilter;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewPermission;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * @author sl
 */
@ViewScoped
@Named
public class UmsetzungsbestaetigungController implements Serializable, UmsetzungsbestaetigungContent {

    private static final Logger LOG = LoggerFactory.getLogger(UmsetzungsbestaetigungController.class.getName());


    private UmsetzungsbestaetigungViewData viewData;

    @Inject
    private UmsetzungsbestaetigungFacade facade;

    @PostConstruct
    public void init() {
        initViewData();

    }

    private void initViewData() {
        viewData = facade.initViewData(UrlParameterUtils.getUrlParameter());
    }

    public String reset() {
        return getFilter().getResetUrl();
    }

    public String filter() {
        return getFilter().getUrl();
    }

    public UmsetzungsbestaetigungViewFilter getFilter() {
        return viewData.getViewFilter();
    }

    @Override
    public UmsetzungsbestaetigungViewData getViewData() {
        return viewData;
    }

    @Override
    public void showEditCommentDialog(UmsetzungsbestaetigungDto umsetzungsbestaetigungDto) {
        viewData.setUmsetzungsbestaetigungDtoForComment(umsetzungsbestaetigungDto);
    }

    @Override
    public void showGroupCommentDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('ubGroupCommentDialog').show()");
    }

    public void showCommentDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('ubCommentDialog').show()");
    }

    @Override
    public String edit() {
        getFilter().getEditModeFilter().setActive();
        return filter();
    }

    @Override
    public void downloadExcelExport() {
        facade.downloadExcelExport(viewData.getUmsetzungsbestaetigungen(),
                viewData.getKovAPhaseImDerivat(),
                viewData.getLastPhase());
    }

    public void setCommentForUmsetzungsbestaetigung() {

        viewData.setCommentForUmsetzungsbestaetigung();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('ubCommentDialog').hide()");
    }

    public void resetCommentForGroupUmsetzungsbestaetigung() {
        viewData.setGroupKommentar(null);
    }

    @Override
    public String processChanges() {
        ProcessResultDto processResult = facade.processUmsetzungsbestaetigungBatchChanges(
                viewData.getSelectedUmsetzungsbestaetigungen(),
                viewData.getSelectedGroupStatus(),
                viewData.getGroupKommentar(),
                viewData.getKovAPhaseImDerivat());

        if (!processResult.isSuccessful()) {
            showErrorMessageInGrowl(processResult.getMessage());
        } else {
            getFilter().getEditModeFilter().setInactive();
            return filter();
        }
        return "";
    }

    @Override
    public void onRowEdit(RowEditEvent event) {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = (UmsetzungsbestaetigungDto) event.getObject();
        ProcessResultDto processResult = facade.processUmsetzungsbestaetigungChange(umsetzungsbestaetigung, viewData.getKovAPhaseImDerivat());
        if (!processResult.isSuccessful()) {
            showErrorMessageInGrowl(processResult.getMessage());
        } else {

            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(getFilter().getUrl());
            } catch (IOException exception) {
                LOG.error("Single Line Row Edit in Umsetzungsbestätigung failed: \n", exception);
            }
        }
    }

    private void showErrorMessageInGrowl(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Fehler!", message));
        context.validationFailed();
        RequestContext.getCurrentInstance().update("umsContent:umsForm:growl");
    }

    @Override
    public String resetChanges() {
        getFilter().getEditModeFilter().setInactive();
        return filter();
    }

    // ---------- getter / setter ----------------------------------------------
    public UmsetzungsBestaetigungStatusConverter getUmsetzungsBestaetigungStatusConverter() {
        return new UmsetzungsBestaetigungStatusConverter();
    }

    //
    public List<UmsetzungsbestaetigungDto> getUmsetzungsbestaetigungen() {
        return viewData.getUmsetzungsbestaetigungen();
    }

    //
    public List<UmsetzungsbestaetigungDto> getSelectedUmsetzungsbestaetigungen() {
        return viewData.getSelectedUmsetzungsbestaetigungen();
    }
//

    public void setSelectedUmsetzungsbestaetigungen(List<UmsetzungsbestaetigungDto> selectedUmsetzungsbestaetigungen) {
        this.viewData.setSelectedUmsetzungsbestaetigungen(selectedUmsetzungsbestaetigungen);
    }

    public UmsetzungsbestaetigungViewPermission getViewPermission() {
        return viewData.getViewPermission();
    }

}
