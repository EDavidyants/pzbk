package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.Collection;

final class KovaPhasenBerechtigungDialogPermission {

    private KovaPhasenBerechtigungDialogPermission() {
    }

    static boolean isAllowedToEdit(Collection<Rolle> userRoles, KovAStatus kovAStatus) {
        switch (kovAStatus) {
            case KONFIGURIERT:
                return userRoles.contains(Rolle.ADMIN) || userRoles.contains(Rolle.SENSORCOCLEITER) || userRoles.contains(Rolle.SCL_VERTRETER);
            case AKTIV:
                return userRoles.contains(Rolle.ADMIN);
            default:
                return Boolean.FALSE;
        }
    }

}
