package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.filter.BooleanUrlFilter;
import de.interfaceag.bmw.pzbk.filter.EditModeUrlFilter;
import de.interfaceag.bmw.pzbk.filter.FachIdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.Filter;
import de.interfaceag.bmw.pzbk.filter.FilterToUrlConverter;
import de.interfaceag.bmw.pzbk.filter.IdFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ModulFilter;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ProzessbaukastenFilter;
import de.interfaceag.bmw.pzbk.filter.SensorCocFilter;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ThemenklammerFilter;
import de.interfaceag.bmw.pzbk.filter.UmsetzungsbestaetigungStatusFilter;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class UmsetzungsbestaetigungViewFilter implements Serializable {

    private static final Page PAGE = Page.UMSETZUNGSBESTAETIGUNG;

    @Filter
    private final FachIdSearchFilter anforderungFilter;
    @Filter
    private final TextSearchFilter beschreibungFilter;
    @Filter
    private IdSearchFilter sensorCocFilter;
    @Filter
    private IdSearchFilter prozessbaukastenFilter;
    @Filter
    private IdSearchFilter modulFilter;
    @Filter
    private final ThemenklammerFilter themenklammerFilter;
    @Filter
    private final MultiValueEnumSearchFilter<UmsetzungsBestaetigungStatus> umsetzungsbestaetigungStatusFilter;
    @Filter
    private final MultiValueEnumSearchFilter<UmsetzungsBestaetigungStatus> umsetzungsbestaetigungPriviousStatusFilter;
    @Filter
    private final BooleanUrlFilter editModeFilter;
    @Filter
    private final IdFilter idFilter;

    public UmsetzungsbestaetigungViewFilter(UrlParameter urlParameter,
                                            List<SensorCoc> allSensorCocs, List<Modul> allModule, KovAPhase kovaPhase, KovAPhase previousPhase,
                                            Collection<ProzessbaukastenFilterDto> prozessbaukaesten, Collection<ThemenklammerDto> themenklammern) {
        anforderungFilter = new FachIdSearchFilter(urlParameter);
        beschreibungFilter = new TextSearchFilter("beschreibung", urlParameter);
        sensorCocFilter = new SensorCocFilter(allSensorCocs, urlParameter);
        prozessbaukastenFilter = new ProzessbaukastenFilter(prozessbaukaesten, urlParameter);
        themenklammerFilter = new ThemenklammerFilter(themenklammern, urlParameter);
        modulFilter = new ModulFilter(allModule, urlParameter);
        this.umsetzungsbestaetigungStatusFilter = new UmsetzungsbestaetigungStatusFilter(urlParameter, kovaPhase.getBezeichnung());

        if (previousPhase == null) {
            this.umsetzungsbestaetigungPriviousStatusFilter = new UmsetzungsbestaetigungStatusFilter(urlParameter, "");
        } else {
            this.umsetzungsbestaetigungPriviousStatusFilter = new UmsetzungsbestaetigungStatusFilter(urlParameter, previousPhase.getBezeichnung());
        }
        editModeFilter = new EditModeUrlFilter(urlParameter);
        idFilter = new IdFilter(urlParameter);
    }

    public String getUrl() {
        return PAGE.getUrl() + getUrlParameter();
    }

    public String getResetUrl() {
        return PAGE.getUrl() + "?faces-redirect=true" + idFilter.getIndependentParameter();
    }

    public void restrictSelectableValuesBasedOnSearchResult(
            List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungDtos, UrlParameter urlParameter,
            Set<SensorCoc> sensorCocs,
            Set<Modul> module, Collection<ProzessbaukastenFilterDto> prozessbaukaesten) {
        if (umsetzungsbestaetigungDtos == null || umsetzungsbestaetigungDtos.isEmpty()) {
            return;
        }
        sensorCocFilter = new SensorCocFilter(sensorCocs, urlParameter);
        modulFilter = new ModulFilter(module, urlParameter);
        prozessbaukastenFilter = new ProzessbaukastenFilter(prozessbaukaesten, urlParameter);
    }

    private String getUrlParameter() {
        FilterToUrlConverter urlConverter = FilterToUrlConverter.forClass(this);
        return urlConverter.getUrl();
    }

    public FachIdSearchFilter getAnforderungFilter() {
        return anforderungFilter;
    }

    public TextSearchFilter getBeschreibungFilter() {
        return beschreibungFilter;
    }

    public IdSearchFilter getProzessbaukastenFilter() {
        return prozessbaukastenFilter;
    }

    public IdSearchFilter getSensorCocFilter() {
        return sensorCocFilter;
    }

    public ThemenklammerFilter getThemenklammerFilter() {
        return themenklammerFilter;
    }

    public IdSearchFilter getModulFilter() {
        return modulFilter;
    }

    public MultiValueEnumSearchFilter<UmsetzungsBestaetigungStatus> getUmsetzungsbestaetigungStatusFilter() {
        return umsetzungsbestaetigungStatusFilter;
    }

    public MultiValueEnumSearchFilter<UmsetzungsBestaetigungStatus> getUmsetzungsbestaetigungPriviousStatusFilter() {
        return umsetzungsbestaetigungPriviousStatusFilter;
    }

    public BooleanUrlFilter getEditModeFilter() {
        return editModeFilter;
    }

    public IdFilter getIdFilter() {
        return idFilter;
    }

}
