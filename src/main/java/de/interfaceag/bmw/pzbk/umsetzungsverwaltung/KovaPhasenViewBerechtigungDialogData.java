package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.BerechtigungKovAPhaseImDerivatSensorCocDTO;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class KovaPhasenViewBerechtigungDialogData implements Serializable {

    private final List<BerechtigungKovAPhaseImDerivatSensorCocDTO> berechtigungList;
    private boolean editMode;
    private final String header;

    private final boolean isAllowedToEdit;

    public KovaPhasenViewBerechtigungDialogData(List<BerechtigungKovAPhaseImDerivatSensorCocDTO> berechtigungList,
                                                Set<Rolle> userRoles, KovAStatus kovAStatus, String header) {
        this.berechtigungList = berechtigungList;
        isAllowedToEdit = KovaPhasenBerechtigungDialogPermission.isAllowedToEdit(userRoles, kovAStatus);
        this.header = header;
    }


    public List<BerechtigungKovAPhaseImDerivatSensorCocDTO> getBerechtigungList() {
        return berechtigungList;
    }

    public void toggleEditMode() {
        this.editMode = !this.editMode;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public boolean getEditPermission() {
        return isAllowedToEdit;
    }

    public String getHeader() {
        return header;
    }

    public void isChanged(BerechtigungKovAPhaseImDerivatSensorCocDTO berechtigung) {
        berechtigung.setChanged(Boolean.TRUE);
    }

    public boolean berechtigungListContains(BerechtigungKovAPhaseImDerivatSensorCocDTO berechtigung) {
        return berechtigungList.stream().anyMatch(b -> b == berechtigung);
    }

}
