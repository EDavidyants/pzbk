package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungDto;

import java.util.List;

/**
 *
 * @author fn
 */
public interface UmsetzungsbestaetigungViewDataContent {

    List<UmsetzungsbestaetigungDto> getUmsetzungsbestaetigungen();

    List<UmsetzungsbestaetigungDto> getSelectedUmsetzungsbestaetigungen();

    void setSelectedUmsetzungsbestaetigungen(List<UmsetzungsbestaetigungDto> selectedUmsetzungsbestaetigungen);

    List<UmsetzungsBestaetigungStatus> getGroupStatus();

    UmsetzungsBestaetigungStatus getSelectedGroupStatus();

    void setSelectedGroupStatus(UmsetzungsBestaetigungStatus selectedGroupStatus);

    String getGroupKommentar();

    void setGroupKommentar(String groupKommentar);

    KovAPhase getLastPhase();

    KovAPhase getCurrentPhase();

    boolean hasLastPhase();

}
