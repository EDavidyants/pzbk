package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author evda
 */
public class UmsetzungsbestaetigungExcelDto implements Serializable {

    private final String sensorCoc;
    private final String anforderung;
    private final String beschreibung;
    private String vorigeKovaPhaseStatus;
    private String vorigeKovaPhaseKommentar;
    private final String kovaPhaseStatus;
    private final String kovaPhaseKommentar;

    public UmsetzungsbestaetigungExcelDto(String sensorCoc, String anforderung, String beschreibung,
            String kovaPhaseStatus, String kovaPhaseKommentar) {

        this.sensorCoc = sensorCoc;
        this.anforderung = anforderung;
        this.beschreibung = beschreibung;
        this.kovaPhaseStatus = kovaPhaseStatus;
        this.kovaPhaseKommentar = kovaPhaseKommentar;
    }

    @Override
    public String toString() {
        return anforderung + " " + kovaPhaseStatus;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.sensorCoc);
        hash = 71 * hash + Objects.hashCode(this.anforderung);
        hash = 71 * hash + Objects.hashCode(this.beschreibung);
        hash = 71 * hash + Objects.hashCode(this.vorigeKovaPhaseStatus);
        hash = 71 * hash + Objects.hashCode(this.vorigeKovaPhaseKommentar);
        hash = 71 * hash + Objects.hashCode(this.kovaPhaseStatus);
        hash = 71 * hash + Objects.hashCode(this.kovaPhaseKommentar);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UmsetzungsbestaetigungExcelDto other = (UmsetzungsbestaetigungExcelDto) obj;
        if (!Objects.equals(this.sensorCoc, other.sensorCoc)) {
            return false;
        }
        if (!Objects.equals(this.anforderung, other.anforderung)) {
            return false;
        }
        if (!Objects.equals(this.beschreibung, other.beschreibung)) {
            return false;
        }
        if (!Objects.equals(this.vorigeKovaPhaseStatus, other.vorigeKovaPhaseStatus)) {
            return false;
        }
        if (!Objects.equals(this.vorigeKovaPhaseKommentar, other.vorigeKovaPhaseKommentar)) {
            return false;
        }
        if (!Objects.equals(this.kovaPhaseStatus, other.kovaPhaseStatus)) {
            return false;
        }
        if (!Objects.equals(this.kovaPhaseKommentar, other.kovaPhaseKommentar)) {
            return false;
        }
        return true;
    }

    public String getSensorCoc() {
        return sensorCoc;
    }

    public String getAnforderung() {
        return anforderung;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public String getVorigeKovaPhaseStatus() {
        return vorigeKovaPhaseStatus;
    }

    public void setVorigeKovaPhaseStatus(String vorigeKovaPhaseStatus) {
        this.vorigeKovaPhaseStatus = vorigeKovaPhaseStatus;
    }

    public String getVorigeKovaPhaseKommentar() {
        return vorigeKovaPhaseKommentar;
    }

    public void setVorigeKovaPhaseKommentar(String vorigeKovaPhaseKommentar) {
        this.vorigeKovaPhaseKommentar = vorigeKovaPhaseKommentar;
    }

    public String getKovaPhaseStatus() {
        return kovaPhaseStatus;
    }

    public String getKovaPhaseKommentar() {
        return kovaPhaseKommentar;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public static class Builder {

        private String sensorCoc;
        private String anforderung;
        private String beschreibung;
        private String kovaPhaseStatus;
        private String kovaPhaseKommentar;

        public Builder() {

        }

        public Builder withSensorCoc(String sensorCoc) {
            this.sensorCoc = sensorCoc;
            return this;
        }

        public Builder withAnforderung(String anforderung) {
            this.anforderung = anforderung;
            return this;
        }

        public Builder withBeschreibung(String beschreibung) {
            this.beschreibung = beschreibung;
            return this;
        }

        public Builder withKovaPhaseStatus(String kovaPhaseStatus) {
            this.kovaPhaseStatus = kovaPhaseStatus;
            return this;
        }

        public Builder withKovaPhaseKommentar(String kovaPhaseKommentar) {
            this.kovaPhaseKommentar = kovaPhaseKommentar;
            return this;
        }

        public UmsetzungsbestaetigungExcelDto build() {
            return new UmsetzungsbestaetigungExcelDto(sensorCoc, anforderung, beschreibung,
                    kovaPhaseStatus, kovaPhaseKommentar);
        }

    }

}
