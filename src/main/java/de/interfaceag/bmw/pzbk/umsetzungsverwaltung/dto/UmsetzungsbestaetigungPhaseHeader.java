package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.enums.Page;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class UmsetzungsbestaetigungPhaseHeader implements UmsetzungsbestaetigungKovAPhasenPhases, Serializable {

    private static final Page PAGE = Page.UMSETZUNGSBESTAETIGUNG;

    private final Map<Integer, UmsetzungsbestaetigungPhase> phases;

    public UmsetzungsbestaetigungPhaseHeader(Map<Integer, UmsetzungsbestaetigungPhase> phases) {
        this.phases = phases;
    }

    private Optional<UmsetzungsbestaetigungPhase> getPhase(int id) {
        return Optional.ofNullable(phases.get(id));
    }

    @Override
    public boolean phaseIsConfig(int id) {
        Optional<UmsetzungsbestaetigungPhase> phase = getPhase(id);
        if (phase.isPresent()) {
            return phase.get().isConfig();
        }
        return Boolean.FALSE;
    }

    @Override
    public boolean isCurrentPhase(int id) {
        Optional<UmsetzungsbestaetigungPhase> phase = getPhase(id);
        if (phase.isPresent()) {
            return phase.get().isCurrentPhase();
        }
        return Boolean.FALSE;
    }

    @Override
    public String goToPhase(int id) {
        Optional<UmsetzungsbestaetigungPhase> phase = getPhase(id);
        if (phase.isPresent()) {
            return PAGE.getUrl() + "?id=" + phase.get().getUrlId() + "&faces-redirect=true";
        }
        return "";
    }

}
