package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.dao.UBzuKovaPhaseImDerivatUndSensorCoCDao;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.UBzuKovaPhaseImDerivatUndSensorCoC;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.BerechtigungKovAPhaseImDerivatSensorCocDTO;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class UmsetzungsbestaetigungBerechtigungService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(UmsetzungsbestaetigungService.class);

    @Inject
    UBzuKovaPhaseImDerivatUndSensorCoCDao ubKovaDerivatDao;

    @Inject
    BerechtigungService berechtigungService;
    @Inject
    DerivatAnforderungModulService derivatAnforderungModulService;

    public void createAndPersistMissingUmsetzungsbestaetigungBerechtigungForKovaPhase(KovAPhaseImDerivat kovAPhaseImDerivat) {
        List<UBzuKovaPhaseImDerivatUndSensorCoC> resultToPersist = createMissingUmsetzungsbestaetigungBerechtigungForKovaPhase(kovAPhaseImDerivat);
        persistUBzuKovaPhaseImDerivatUndSensorCoCList(resultToPersist);
    }

    protected List<SensorCoc> getSensorCocWithoutBerechtigungForKovaPhase(KovAPhaseImDerivat kovAPhaseImDerivat) {
        List<UBzuKovaPhaseImDerivatUndSensorCoC> existingBerechtigungen = getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivat(kovAPhaseImDerivat);
        List<Long> existingBerechtigungSensorCocIdList = existingBerechtigungen.stream().map(UBzuKovaPhaseImDerivatUndSensorCoC::getSensorCoc).map(SensorCoc::getSensorCocId).collect(Collectors.toList());

        return derivatAnforderungModulService.getSensorCocForVereinbarungenWithStatusAngenommenAndKovaPhaseAndSensorCocIdNotInList(kovAPhaseImDerivat, existingBerechtigungSensorCocIdList);
    }

    protected List<UBzuKovaPhaseImDerivatUndSensorCoC> createMissingUmsetzungsbestaetigungBerechtigungForKovaPhase(KovAPhaseImDerivat kovAPhaseImDerivat) {
        List<SensorCoc> notExistingBerechtigungSensorCocList = getSensorCocWithoutBerechtigungForKovaPhase(kovAPhaseImDerivat);

        List<UBzuKovaPhaseImDerivatUndSensorCoC> generatedBerechtigungen = new ArrayList<>();
        UBzuKovaPhaseImDerivatUndSensorCoC newBerechtigung;
        Optional<Mitarbeiter> sensorCocLeiter;

        for (SensorCoc sensorCoc : notExistingBerechtigungSensorCocList) {
            sensorCocLeiter = berechtigungService.getSensorCoCLeiterOfSensorCoc(sensorCoc);
            if (sensorCocLeiter.isPresent()) {
                newBerechtigung = new UBzuKovaPhaseImDerivatUndSensorCoC(sensorCoc, kovAPhaseImDerivat, sensorCocLeiter.get());
            } else {
                newBerechtigung = new UBzuKovaPhaseImDerivatUndSensorCoC(sensorCoc, kovAPhaseImDerivat);
            }
            generatedBerechtigungen.add(newBerechtigung);
        }

        return generatedBerechtigungen;
    }

    public List<BerechtigungKovAPhaseImDerivatSensorCocDTO> getBerechtigungenListForKovAPhaseImDerivatAndSensorCocList(KovAPhaseImDerivat kovAPhaseImDerivat, List<SensorCoc> sensorCocs) {
        List<BerechtigungKovAPhaseImDerivatSensorCocDTO> result = new ArrayList<>();
        for (SensorCoc sensorCoc : sensorCocs) {
            Optional<UBzuKovaPhaseImDerivatUndSensorCoC> uBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList = ubKovaDerivatDao.getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList(kovAPhaseImDerivat, sensorCoc);
            if (uBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList.isPresent()) {
                List<Mitarbeiter> possibleUmsetzungsbestaetiger = berechtigungService.getUmsetzungsbestaetigetListForSensorCoc(sensorCoc);
                result.add(new BerechtigungKovAPhaseImDerivatSensorCocDTO(kovAPhaseImDerivat, sensorCoc, uBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList.get().getUmsetzungsbestaetiger(), Boolean.TRUE, possibleUmsetzungsbestaetiger));
            } else {
                List<Mitarbeiter> possibleUmsetzungsbestaetiger = berechtigungService.getUmsetzungsbestaetigetListForSensorCoc(sensorCoc);
                result.add(new BerechtigungKovAPhaseImDerivatSensorCocDTO(kovAPhaseImDerivat, sensorCoc, new ArrayList<>(), Boolean.FALSE, possibleUmsetzungsbestaetiger));
            }
        }
        return result;
    }

    public Optional<UBzuKovaPhaseImDerivatUndSensorCoC> getBerechtigungenListForKovAPhaseImDerivatAndSensorCoc(KovAPhaseImDerivat kovAPhaseImDerivat, SensorCoc sensorCoc) {
        return ubKovaDerivatDao.getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList(kovAPhaseImDerivat, sensorCoc);
    }

    public Set<Mitarbeiter> getUmsetzungsbestaetigerForKovaPhaseImDerivat(KovAPhaseImDerivat kovAPhaseImDerivat) {
        try {
            checkForValidInput(kovAPhaseImDerivat);
            return new HashSet<>(getUBForDerivatAndPhase(kovAPhaseImDerivat.getDerivat(), kovAPhaseImDerivat.getKovAPhase()));
        } catch (InvalidDataException ex) {
            LOG.error(null, ex);
        }
        return new HashSet<>();
    }

    private void checkForValidInput(KovAPhaseImDerivat kovAPhaseImDerivat) throws InvalidDataException {
        if (kovAPhaseImDerivat == null) {
            throw new InvalidDataException("kovAPhaseImDerivat is null");
        }
        if (kovAPhaseImDerivat.getDerivat() == null) {
            throw new InvalidDataException("kovAPhaseImDerivat.getDerivat() is null");
        }
        if (kovAPhaseImDerivat.getKovAPhase() == null) {
            throw new InvalidDataException("kovAPhaseImDerivat.getKovAPhase() is null");
        }
    }

    public void persistBerechtigungKovAPhaseImDerivatSensorCocDTOList(List<BerechtigungKovAPhaseImDerivatSensorCocDTO> ubDtos) {
        for (BerechtigungKovAPhaseImDerivatSensorCocDTO ubDto : ubDtos) {
            if (ubDto.getUmsetzungsbestaetiger() != null && !ubDto.getUmsetzungsbestaetiger().isEmpty()) {
                if (ubDto.isPersistiert()) {
                    mergeUByuKovaPhaseImDerivatUndSensorCocWithDTO(ubDto);
                } else {
                    persistNewUByuKovaPhaseImDerivatUndSensorCoC(ubDto);
                }
            } else if (ubDto.getUmsetzungsbestaetiger() != null && ubDto.getUmsetzungsbestaetiger().isEmpty() && ubDto.isPersistiert()) {
                Optional<UBzuKovaPhaseImDerivatUndSensorCoC> uBzuKovaPhaseImDerivatUndSensorCoC = ubKovaDerivatDao.getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList(ubDto.getKovAPhaseImDerivat(), ubDto.getSensorCoc());
                uBzuKovaPhaseImDerivatUndSensorCoC.ifPresent(bzuKovaPhaseImDerivatUndSensorCoC -> ubKovaDerivatDao.removeUBzuKovaPhaseImDerivatUndSensorCoC(bzuKovaPhaseImDerivatUndSensorCoC));
            }
        }
    }

    private void persistNewUByuKovaPhaseImDerivatUndSensorCoC(BerechtigungKovAPhaseImDerivatSensorCocDTO ubDto) {
        UBzuKovaPhaseImDerivatUndSensorCoC newUBzuKovaPhaseImDerivatUndSensorCoC = new UBzuKovaPhaseImDerivatUndSensorCoC(ubDto.getSensorCoc(), ubDto.getKovAPhaseImDerivat(), ubDto.getUmsetzungsbestaetiger());
        ubKovaDerivatDao.persistUBzuKovaPhaseImDerivatUndSensorCoC(newUBzuKovaPhaseImDerivatUndSensorCoC);
    }

    private void mergeUByuKovaPhaseImDerivatUndSensorCocWithDTO(BerechtigungKovAPhaseImDerivatSensorCocDTO ubDto) {
        Optional<UBzuKovaPhaseImDerivatUndSensorCoC> uBzuKovaPhaseImDerivatUndSensorCoC = ubKovaDerivatDao.getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList(ubDto.getKovAPhaseImDerivat(), ubDto.getSensorCoc());
        if (uBzuKovaPhaseImDerivatUndSensorCoC.isPresent()) {
            uBzuKovaPhaseImDerivatUndSensorCoC.get().setUmsetzungsbestaetiger(ubDto.getUmsetzungsbestaetiger());
            ubKovaDerivatDao.persistUBzuKovaPhaseImDerivatUndSensorCoC(uBzuKovaPhaseImDerivatUndSensorCoC.get());
        }
    }

    public void persistUBzuKovaPhaseImDerivatUndSensorCoCList(List<UBzuKovaPhaseImDerivatUndSensorCoC> ubzuKovaPhaseImDerivatUndSensorCoCs) {
        ubKovaDerivatDao.persistUBzuKovaPhaseImDerivatUndSensorCoCs(ubzuKovaPhaseImDerivatUndSensorCoCs);
    }

    public List<UBzuKovaPhaseImDerivatUndSensorCoC> getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivat(KovAPhaseImDerivat kovAPhaseImDerivat) {
        return ubKovaDerivatDao.getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList(kovAPhaseImDerivat);
    }

    public List<UBzuKovaPhaseImDerivatUndSensorCoC> createUBzuKovaPhaseImDerivatUndSensorCoC(KovAPhaseImDerivat kovAPhaseImDerivat, List<SensorCoc> removeSensorCoc) {
        List<SensorCoc> sensorCocForDerivat = derivatAnforderungModulService.getSensorCocForDerivat(kovAPhaseImDerivat.getDerivat());
        sensorCocForDerivat.removeAll(removeSensorCoc);
        List<UBzuKovaPhaseImDerivatUndSensorCoC> result = new ArrayList<>();
        sensorCocForDerivat.forEach(s -> result.add(new UBzuKovaPhaseImDerivatUndSensorCoC(s, kovAPhaseImDerivat, new ArrayList<>())));
        return result;
    }

    public List<Mitarbeiter> getUBForDerivatAndPhase(Derivat derivat, KovAPhase phase) {
        return ubKovaDerivatDao.getUmsetzungsbestaetigerByDerivatPhase(derivat, phase);
    }

    public List<SensorCoc> getSensorCocToDoListForUmsetzungsbestaetiger(Mitarbeiter user) {
        return ubKovaDerivatDao.getSensorCocToDoListForUmsetzungsbestaetiger(user);
    }

    public Collection<Long> getSensorCocIdsAsUmsetzungsverwalterForKovaPhaseImDerivat(KovAPhaseImDerivat kovAPhaseImDerivat, Mitarbeiter user) {
        return ubKovaDerivatDao.getSensorCocIdsAsUmsetzungsverwalterForKovaPhaseImDerivat(kovAPhaseImDerivat, user);
    }

    public Optional<UBzuKovaPhaseImDerivatUndSensorCoC> getUBzuKovaPhaseImDerivatUndSensorCoCByDerivatSensorCocAndPhase(SensorCoc sensorCoc, Derivat derivat, KovAPhase kovAPhase) {
        return ubKovaDerivatDao.getUBzuKovaPhaseImDerivatUndSensorCoCByDerivatSensorCocAndPhase(sensorCoc, derivat, kovAPhase);
    }

}
