package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class BerechtigungKovAPhaseImDerivatSensorCocDTO implements Serializable {

    private final KovAPhaseImDerivat kovAPhaseImDerivat;
    private final SensorCoc sensorCoc;
    private List<Mitarbeiter> umsetzungsbestaetiger;
    private final List<Mitarbeiter> possibleUmsetzungsbestaetiger;

    private final boolean persistiert;
    private boolean changed;

    public BerechtigungKovAPhaseImDerivatSensorCocDTO(KovAPhaseImDerivat kovAPhaseImDerivat,
            SensorCoc sensorCoc, List<Mitarbeiter> umsetzungsbestaetiger, Boolean persistiert,
            List<Mitarbeiter> possibleUmsetzungsbestaetiger) {
        this.kovAPhaseImDerivat = kovAPhaseImDerivat;
        this.sensorCoc = sensorCoc;
        this.umsetzungsbestaetiger = umsetzungsbestaetiger;
        this.persistiert = persistiert;
        this.possibleUmsetzungsbestaetiger = possibleUmsetzungsbestaetiger;
    }

    public KovAPhaseImDerivat getKovAPhaseImDerivat() {
        return kovAPhaseImDerivat;
    }

    public SensorCoc getSensorCoc() {
        return sensorCoc;
    }

    public List<Mitarbeiter> getUmsetzungsbestaetiger() {
        return umsetzungsbestaetiger;
    }

    public void setUmsetzungsbestaetiger(List<Mitarbeiter> umsetzungsbestaetiger) {
        this.umsetzungsbestaetiger = umsetzungsbestaetiger;
    }

    public boolean isPersistiert() {
        return persistiert;
    }

    public List<Mitarbeiter> getPossibleUmsetzungsbestaetiger() {
        return possibleUmsetzungsbestaetiger;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

}
