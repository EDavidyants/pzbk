package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.KovaStatusRolePermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.TruePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;
import java.util.Set;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class UmsetzungsbestaetigungViewPermission implements Serializable {

    // page
    private final ViewPermission page;

    // header
    private final EditPermission editButton;
    private final EditPermission processChangesButton;
    private final EditPermission cancelButton;
    private final ViewPermission eingetrageneUmsetzerHover;

    private final ViewPermission editModePermission;

    public UmsetzungsbestaetigungViewPermission(Set<Rolle> userRoles,
                                                KovAStatus kovAStatus, UrlParameter urlParameter) {

        String editMode = urlParameter.getValue("editMode").orElse("");
        this.page = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.SENSORCOCLEITER)
                .authorizeReadForRole(Rolle.SCL_VERTRETER)
                .authorizeReadForRole(Rolle.UMSETZUNGSBESTAETIGER)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(userRoles).get();


        if (kovAStatus == null) {
            this.editButton = new FalsePermission();
            this.processChangesButton = new FalsePermission();
            this.cancelButton = new FalsePermission();
            this.editModePermission = new FalsePermission();
            this.eingetrageneUmsetzerHover = new FalsePermission();
        } else {
            if ("true".equals(editMode)) {
                this.editButton = new FalsePermission();
                this.editModePermission = new TruePermission();
                this.processChangesButton = KovaStatusRolePermission.builder()
                        .forStatus(kovAStatus)
                        .authorizeRole(Rolle.ADMIN)
                        .authorizeRole(Rolle.SENSORCOCLEITER)
                        .authorizeRole(Rolle.SCL_VERTRETER)
                        .authorizeRole(Rolle.UMSETZUNGSBESTAETIGER)
                        .compareWith(userRoles).get();
                this.cancelButton = KovaStatusRolePermission.builder()
                        .forStatus(kovAStatus)
                        .authorizeRole(Rolle.ADMIN)
                        .authorizeRole(Rolle.SENSORCOCLEITER)
                        .authorizeRole(Rolle.SCL_VERTRETER)
                        .authorizeRole(Rolle.UMSETZUNGSBESTAETIGER)
                        .compareWith(userRoles).get();
            } else {
                this.editModePermission = new FalsePermission();
                this.processChangesButton = new FalsePermission();
                this.cancelButton = new FalsePermission();
                this.editButton = KovaStatusRolePermission.builder()
                        .forStatus(kovAStatus)
                        .authorizeRole(Rolle.ADMIN)
                        .authorizeRole(Rolle.SENSORCOCLEITER)
                        .authorizeRole(Rolle.SCL_VERTRETER)
                        .authorizeRole(Rolle.UMSETZUNGSBESTAETIGER)
                        .compareWith(userRoles).get();
            }
            this.eingetrageneUmsetzerHover = RolePermission.builder()
                    .authorizeReadForRole(Rolle.ADMIN)
                    .authorizeReadForRole(Rolle.ANFORDERER)
                    .compareTo(userRoles).get();
        }

    }


    public boolean getPage() {
        return page.isRendered();
    }

    public boolean getEditButton() {
        return editButton.hasRightToEdit();
    }

    public boolean getProcessChangesButton() {
        return processChangesButton.hasRightToEdit();
    }

    public boolean getCancelButton() {
        return cancelButton.hasRightToEdit();
    }

    public boolean isEditMode() {
        return editModePermission.isRendered();
    }

    public boolean getEingetrageneUmsetzerHover() {
        return eingetrageneUmsetzerHover.isRendered();
    }
}
