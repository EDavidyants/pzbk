package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.exceptions.QueryException;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.pages.KovAPhasenViewFilter;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import de.interfaceag.bmw.pzbk.shared.utils.DateUtils;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.KovAPhaseImDerivatDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class KovAPhaseImDerivatSearchService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(KovAPhaseImDerivatSearchService.class.getName());

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    @Inject
    private UmsetzungsbestaetigungProgressService umsetzungsbestaetigungProgressService;
    @Inject
    private UmsetzungsbestaetigungBerechtigungProgressService berechtigungProgressService;
    @Inject
    private Session session;

    public List<KovAPhaseImDerivatDto> getKovAPhasenImDerivat(KovAPhasenViewFilter filter,
                                                              UserPermissions<BerechtigungDto> permissions) {
        List<KovAPhaseImDerivatDto> result = new ArrayList<>();

        try {
            getSearchResultForKovaPhasenView(filter, permissions)
                    .stream()
                    .filter(k -> result.stream().allMatch(d -> !d.getId().equals(k.getId())))
                    .forEach(k -> result.add(new KovAPhaseImDerivatDto(k)));
        } catch (QueryException ex) {
            LOG.error(null, ex);
        }

        sortPhases(result);

        setBerechtigungProgressData(result);
        setUmsetzungsbestaetigungProgressData(result);
        return result;
    }

    private static void sortPhases(List<KovAPhaseImDerivatDto> phases) {
        List<KovAPhaseImDerivatDto> abgeschlossenePhasen = phases.stream()
                .filter(r -> r.getStatus().equals(KovAStatus.ABGESCHLOSSEN))
                .collect(Collectors.toList());

        if (!abgeschlossenePhasen.isEmpty()) {
            KovAPhaseImDerivatDto lastPhaseWithStatusAbgeschlossen = abgeschlossenePhasen.get(abgeschlossenePhasen.size() - 1);

            phases.removeAll(abgeschlossenePhasen);
            abgeschlossenePhasen.remove(abgeschlossenePhasen.size() - 1);
            phases.addAll(abgeschlossenePhasen);
            phases.add(0, lastPhaseWithStatusAbgeschlossen);
        }
    }

    private void setBerechtigungProgressData(List<KovAPhaseImDerivatDto> kovAPhasenImDerivat) {
        kovAPhasenImDerivat.forEach(k -> {
            long total = berechtigungProgressService.
                    getNumberOfAllBerechtigungenForKovaPhaseImDerivat(k.getId());

            long gesetzt = berechtigungProgressService.
                    getNumberOfGesetzteBerechtigungenForKovaPhaseImDerivat(k.getId());

            k.setBerechtigungProgressLabel(getBerechtigungProgressLabel(gesetzt, total));
            k.setBerechtigungProgressValue(getBerechtigungProgress(gesetzt, total));
            k.setBerechtigungFinished(isBerechtigungFinished(gesetzt, total));
        });

    }

    private void setUmsetzungsbestaetigungProgressData(List<KovAPhaseImDerivatDto> kovAPhasenImDerivat) {
        kovAPhasenImDerivat.forEach(k -> {
            long total = umsetzungsbestaetigungProgressService.
                    getNumberOfAllUmsetzungsbestaetigungenForKovaPhaseImDerivat(k.getId());
            long bearbeitet = umsetzungsbestaetigungProgressService.getNumberofUmgesetzteUmsetzungsbestaetigungenForKovaPhaseImDerivat(k.getId());

            k.setProgressLabel(getUmsetzungsbestaetigungProgressLabel(total, bearbeitet));
            k.setProgressValue(getUmsetzungsbestaetigungProgress(total, bearbeitet));
            k.setFinished(isUmsetzungbestaetigungFinished(total, bearbeitet));
        });

    }

    private static long getUmsetzungsbestaetigungProgress(long total, long bearbeitet) {
        if (total != 0L) {
            return bearbeitet * 100 / total;
        }
        return 0L;
    }

    private static String getUmsetzungsbestaetigungProgressLabel(long total, long bearbeitet) {
        return Long.toString(bearbeitet) + " / " + Long.toString(total);
    }

    private static boolean isUmsetzungbestaetigungFinished(long total, long bearbeitet) {
        return total - bearbeitet == 0;
    }

    private static String getBerechtigungProgressLabel(long umsetzerCount, long sensorCocCount) {
        if (umsetzerCount > sensorCocCount) {
            return sensorCocCount + " / " + sensorCocCount;
        }
        return umsetzerCount + " / " + sensorCocCount;
    }

    private static boolean isBerechtigungFinished(long umsetzerCount, long sensorCocCount) {
        return umsetzerCount >= sensorCocCount;
    }

    private long getBerechtigungProgress(long umsetzerCount, long sensorCocCount) {
        if (umsetzerCount > sensorCocCount) {
            return 100L;
        }
        if (sensorCocCount != 0L) {
            return umsetzerCount * 100 / sensorCocCount;
        }
        return 0L;
    }

    private List<KovAPhaseImDerivat> getSearchResultForKovaPhasenView(KovAPhasenViewFilter filter, UserPermissions<BerechtigungDto> permissions)
            throws QueryException {
        checkForValidInput(filter);

        QueryPartDTO qp = getBaseQuery();
        getPermissionQueryPart(qp, permissions);
        getStatusFilterQueryPart(qp, filter);
        getPhaseFilterQueryPart(qp, filter);
        getDerivatFilterQueryPart(qp, filter);
        getVonStartDateFilterQueryPart(qp, filter);
        getBisStartDateFilterQueryPart(qp, filter);
        getVonEndDateFilterQueryPart(qp, filter);
        getBisEndDateFilterQueryPart(qp, filter);
        getSortQueryPart(qp);

        Query q = buildQuery(qp);
        return q.getResultList();
    }

    private Query buildQuery(QueryPartDTO qp) {
        Query q = entityManager.createQuery(qp.getQuery(), KovAPhaseImDerivat.class);
        qp.getParameterMap().entrySet().forEach(p ->
                q.setParameter(p.getKey(), p.getValue())
        );
        return q;
    }

    private static void checkForValidInput(KovAPhasenViewFilter filter) throws QueryException {
        if (filter == null) {
            throw new QueryException("filter ist null");
        }
    }

    private static QueryPartDTO getBaseQuery() {
        QueryPartDTO qp = new QueryPartDTO("SELECT DISTINCT k FROM KovAPhaseImDerivat k "
                + "WHERE k.derivat.status != 4 AND k.kovaStatus != :statusOffen");
        qp.put("statusOffen", KovAStatus.OFFEN);
        return qp;
    }

    private static void getSortQueryPart(QueryPartDTO qp) {
        qp.append(" ORDER BY k.endDate ASC");
    }

    private static void getStatusFilterQueryPart(QueryPartDTO qp, KovAPhasenViewFilter filter) {
        MultiValueEnumSearchFilter<KovAStatus> statusFilter = filter.getStatusFilter();
        if (statusFilter.isActive()) {
            qp.append(" AND k.kovaStatus IN :statusFilter");
            qp.put("statusFilter", statusFilter.getAsEnumList());
        }
    }

    private static void getPhaseFilterQueryPart(QueryPartDTO qp, KovAPhasenViewFilter filter) {
        IdSearchFilter phaseFilter = filter.getPhaseFilter();
        if (phaseFilter.isActive()) {
            qp.append(" AND k.kovAPhase IN :phaseFilter");
            qp.put("phaseFilter", phaseFilter.getSelectedIdsAsSet());
        }
    }

    private static void getDerivatFilterQueryPart(QueryPartDTO qp, KovAPhasenViewFilter filter) {
        IdSearchFilter derivatFilter = filter.getDerivatFilter();
        if (derivatFilter.isActive()) {
            qp.append(" AND k.derivat.id IN :derivatFilter");
            qp.put("derivatFilter", derivatFilter.getSelectedIdsAsSet());
        }
    }

    private static void getVonStartDateFilterQueryPart(QueryPartDTO qp, KovAPhasenViewFilter filter) {
        DateSearchFilter vonStartDateFilter = filter.getVonStartDateFilter();
        if (vonStartDateFilter.isActive()) {
            qp.append(" AND k.startDate > :vonStartDateFilter");
            qp.put("vonStartDateFilter", DateUtils.convertToLocalDate(vonStartDateFilter.getSelected()));
        }
    }

    private static void getVonEndDateFilterQueryPart(QueryPartDTO qp, KovAPhasenViewFilter filter) {
        DateSearchFilter vonEndDateFilter = filter.getVonStartDateFilter();
        if (vonEndDateFilter.isActive()) {
            qp.append(" AND k.endDate > :vonEndDateFilter");
            qp.put("vonEndDateFilter", DateUtils.convertToLocalDate(vonEndDateFilter.getSelected()));
        }
    }

    private static void getBisStartDateFilterQueryPart(QueryPartDTO qp, KovAPhasenViewFilter filter) {
        DateSearchFilter bisStartDateFilter = filter.getBisStartDateFilter();
        if (bisStartDateFilter.isActive()) {
            qp.append(" AND k.startDate < :bisDateFilter");
            qp.put("bisDateFilter", DateUtils.convertToLocalDate(DateUtils.addDays(bisStartDateFilter.getSelected(), 2)));
        }
    }

    private static void getBisEndDateFilterQueryPart(QueryPartDTO qp, KovAPhasenViewFilter filter) {
        DateSearchFilter bisEndDateFilter = filter.getBisEndDateFilter();
        if (bisEndDateFilter.isActive()) {
            qp.append(" AND k.endDate < :bisDateFilter");
            qp.put("bisDateFilter", DateUtils.convertToLocalDate(DateUtils.addDays(bisEndDateFilter.getSelected(), 2)));
        }
    }

    private Collection<Long> getAllKovaPhaseImDerivatForUser(UserPermissions<BerechtigungDto> permissions) {
        Set<Long> kovaPhasenIds = new HashSet<>();
        if (permissions.hasRole(Rolle.ADMIN)) {
            return getKovaPhasenForAdmin();
        }
        if (permissions.hasRole(Rolle.ANFORDERER)) {
            kovaPhasenIds.addAll(getKovaPhasenForAnforderer(permissions));
        }
        if (permissions.hasRole(Rolle.SENSORCOCLEITER)) {
            kovaPhasenIds.addAll(getKovaPhasenForSensorCocLeiter(permissions));
        }
        if (permissions.hasRole(Rolle.SCL_VERTRETER)) {
            kovaPhasenIds.addAll(getKovaPhasenForSensorCocVertreter(permissions));
        }
        if (permissions.hasRole(Rolle.UMSETZUNGSBESTAETIGER)) {
            kovaPhasenIds.addAll(getKovaPhasenForUmsetzungsbestaetiger());
        }
        return kovaPhasenIds;
    }

    private Collection<Long> getKovaPhasenForAnforderer(UserPermissions<BerechtigungDto> permissions) {
        Collection<String> zakEinordnungen = convertToString(permissions.getZakEinordnungAsAnfordererSchreibend());
        Collection<Long> derivatIds = convertToLong(permissions.getDerivatAsAnfordererSchreibend());
        QueryPart qp = new QueryPartDTO();

        qp.append("SELECT DISTINCT k.id FROM kov_a_phase_im_derivat k "
                + "INNER JOIN derivat d ON d.id = k.derivat_id "
                + "INNER JOIN zuordnunganforderungderivat zad ON zad.derivat_id = d.id "
                + "INNER JOIN derivatanforderungmodul dam ON zad.id = dam.zuordnunganforderungderivat_id "
                + "INNER JOIN anforderung a ON a.id = zad.anforderung_id "
                + "INNER JOIN sensorcoc s ON s.sensorcocid = a.sensorcoc_sensorcocid "
                + "WHERE d.id IN ");
        qp.appendNativeParameters(derivatIds);
        qp.append(" AND s.zakeinordnung IN ");
        qp.appendNativeParameters(zakEinordnungen);

        Query query = qp.buildNativeQuery(entityManager);
        return query.getResultList();
    }

    private Collection<Long> getKovaPhasenForSensorCocLeiter(UserPermissions<BerechtigungDto> permissions) {
        Collection<Long> sensorCocIds = convertToLong(permissions.getSensorCocAsSensorCocLeiterSchreibend());
        return getKovaPhasenForSensorCocIds(sensorCocIds);
    }

    private Collection<Long> getKovaPhasenForSensorCocVertreter(UserPermissions<BerechtigungDto> permissions) {
        Collection<Long> sensorCocIds = convertToLong(permissions.getSensorCocAsVertreterSCLSchreibend());
        return getKovaPhasenForSensorCocIds(sensorCocIds);
    }

    private Collection<Long> getKovaPhasenForSensorCocIds(Collection<Long> sensorCocIds) {
        QueryPart qp = new QueryPartDTO();

        qp.append("SELECT DISTINCT k.id FROM kov_a_phase_im_derivat k "
                + "INNER JOIN derivat d ON d.id = k.derivat_id "
                + "INNER JOIN zuordnunganforderungderivat zad ON zad.derivat_id = d.id "
                + "INNER JOIN derivatanforderungmodul dam ON zad.id = dam.zuordnunganforderungderivat_id "
                + "INNER JOIN anforderung a ON a.id = zad.anforderung_id "
                + "INNER JOIN sensorcoc s ON s.sensorcocid = a.sensorcoc_sensorcocid "
                + "WHERE s.sensorcocid IN ");
        qp.appendNativeParameters(sensorCocIds);

        Query query = qp.buildNativeQuery(entityManager);
        return query.getResultList();
    }

    private Collection<Long> getKovaPhasenForUmsetzungsbestaetiger() {
        Mitarbeiter user = session.getUser();
        QueryPart qp = new QueryPartDTO();
        qp.append("SELECT DISTINCT k.id FROM UBzuKovaPhaseImDerivatUndSensorCoC b "
                + "INNER JOIN b.kovaImDerivat k "
                + "INNER JOIN b.umsetzungsbestaetiger u "
                + "WHERE u.id = :userId ");
        qp.put("userId", user.getId());
        Query query = qp.buildGenericQuery(entityManager);
        return query.getResultList();
    }

    private Collection<Long> getKovaPhasenForAdmin() {
        QueryPart queryPart = new QueryPartDTO("SELECT k.id FROM KovAPhaseImDerivat k");
        Query query = queryPart.buildGenericQuery(entityManager);
        return query.getResultList();
    }

    private static Collection<String> convertToString(Collection<BerechtigungDto> berechtigungen) {
        return berechtigungen.stream().map(BerechtigungDto::getName).collect(Collectors.toSet());
    }

    private static Collection<Long> convertToLong(Collection<BerechtigungDto> berechtigungen) {
        return berechtigungen.stream().map(BerechtigungDto::getId).collect(Collectors.toSet());
    }

    private void getPermissionQueryPart(QueryPartDTO qp, UserPermissions<BerechtigungDto> permissions) {
        Collection<Long> kovaPhaseImDerivatIds = getAllKovaPhaseImDerivatForUser(permissions);

        if (kovaPhaseImDerivatIds.isEmpty()) {
            qp.append(" AND 1 = 0 ");
        } else {
            qp.append(" AND k.id IN :kovaPhasen ");
            qp.put("kovaPhasen", kovaPhaseImDerivatIds);
        }
    }

}
