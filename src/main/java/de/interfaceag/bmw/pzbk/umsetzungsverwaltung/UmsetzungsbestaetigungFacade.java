package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.dao.KovAPhaseImDerivatDao;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.ProcessResultDto;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungPhase;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungPhaseHeader;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewData;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewFilter;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewPermission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author fn
 */
@Stateless
@Named
public class UmsetzungsbestaetigungFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(UmsetzungsbestaetigungFacade.class);

    @Inject
    private Session session;
    @Inject
    private DerivatService derivatService;
    @Inject
    private KovAPhaseImDerivatDao kovAPhaseImDerivatDao;
    @Inject
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;
    @Inject
    private UmsetzungsbestaetigungBerechtigungService umsetzungsbestaetigungBerechtigungService;
    @Inject
    private UmsetzungsverwaltungTableDataSortService umsetzungsverwaltungTableDataSortService;
    @Inject
    private UmsetzungsbestaetigungFilterService umsetzungsbestaetigungFilterService;

    public UmsetzungsbestaetigungViewData initViewData(UrlParameter urlParameter) {

        Set<Rolle> userRoles = new HashSet<>(session.getUserPermissions().getRollen());

        KovAPhaseImDerivat currentKovAPhaseImDerivat = getKovAPhaseImDerivatById(urlParameter);

        if (currentKovAPhaseImDerivat == null) {
            return UmsetzungsbestaetigungViewData.forEmptyPhase();
        }

        KovAPhase previousPhase = getPreviousPhase(currentKovAPhaseImDerivat);

        UmsetzungsbestaetigungViewFilter filter = umsetzungsbestaetigungFilterService.getUmsetzungsbestaetigungViewFilter(urlParameter, currentKovAPhaseImDerivat, previousPhase);

        List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungen = getUmsetzungsbestaetigungForView(filter, currentKovAPhaseImDerivat, previousPhase);

        umsetzungsbestaetigungFilterService.setRestrictedFilterForUmsetzungsbestaetigung(filter, umsetzungsbestaetigungen, urlParameter);

        UmsetzungsbestaetigungPhaseHeader phaseHeader = getUmsetzungsbestaetigungPhaseHeaderForView(currentKovAPhaseImDerivat);

        UmsetzungsbestaetigungViewPermission viewPermission = getViewPermission(currentKovAPhaseImDerivat, userRoles, urlParameter);

        return buildUmsetzungsbestaetigungViewData(currentKovAPhaseImDerivat, filter, viewPermission, umsetzungsbestaetigungen, phaseHeader, previousPhase);
    }

    private UmsetzungsbestaetigungViewData buildUmsetzungsbestaetigungViewData(KovAPhaseImDerivat currentKovAPhaseImDerivat, UmsetzungsbestaetigungViewFilter filter,
                                                                               UmsetzungsbestaetigungViewPermission viewPermission, List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungen,
                                                                               UmsetzungsbestaetigungPhaseHeader phaseHeader, KovAPhase previousPhase) {

        return new UmsetzungsbestaetigungViewData(viewPermission, umsetzungsbestaetigungen, filter, phaseHeader,
                UmsetzungsBestaetigungStatus.getSelectableStatusList(), currentKovAPhaseImDerivat, previousPhase);

    }

    private UmsetzungsbestaetigungViewPermission getViewPermission(KovAPhaseImDerivat currentKovAPhaseImDerivat, Set<Rolle> userRoles, UrlParameter urlParameter) {
        boolean isUmsetzungsbestaetigerForPhase = umsetzungsbestaetigungBerechtigungService.getUmsetzungsbestaetigerForKovaPhaseImDerivat(currentKovAPhaseImDerivat)
                .stream().anyMatch(m -> m.getId().equals(session.getUser().getId()));

        if (!isUmsetzungsbestaetigerForPhase) {
            Set<Rolle> authorizedRoles = new HashSet<>(userRoles);
            authorizedRoles.remove(Rolle.UMSETZUNGSBESTAETIGER);
            userRoles = authorizedRoles;
        }

        return new UmsetzungsbestaetigungViewPermission(userRoles, currentKovAPhaseImDerivat.getKovaStatus(), urlParameter);
    }

    private KovAPhaseImDerivat getKovAPhaseImDerivatById(UrlParameter urlParameter) {
        String kovaPhaseImDerivatId = urlParameter.getValue("id").orElse("");
        Long id = null;
        try {
            id = Long.parseLong(kovaPhaseImDerivatId);
        } catch (NumberFormatException nbfe) {
            LOG.info("No Valid KovaPhasenID");
        }
        return kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(id);

    }

    private KovAPhase getPreviousPhase(KovAPhaseImDerivat kovAPhaseImDerivat) {
        if (kovAPhaseImDerivat == null || kovAPhaseImDerivat.getKovAPhase() == null) {
            return null;
        }
        int previousPhaseId = kovAPhaseImDerivat.getKovAPhase().getId() - 1;
        if (previousPhaseId < 1) {
            return null;
        } else {
            return KovAPhase.getById(previousPhaseId).orElse(null);
        }
    }

    private List<UmsetzungsbestaetigungDto> getUmsetzungsbestaetigungForView(UmsetzungsbestaetigungViewFilter filter, KovAPhaseImDerivat kovAPhaseImDerivat, KovAPhase previousPhase) {
        List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungen = umsetzungsbestaetigungService.getFilterdUmsetzungsbestaetigung(filter, kovAPhaseImDerivat, previousPhase);
        return umsetzungsverwaltungTableDataSortService.sortTableData(umsetzungsbestaetigungen);
    }

    private UmsetzungsbestaetigungPhaseHeader getUmsetzungsbestaetigungPhaseHeaderForView(KovAPhaseImDerivat currentKovAPhaseImDerivat) {
        Map<Integer, UmsetzungsbestaetigungPhase> phases = getUmsetzungsbestaetigungPhasesForHeader(currentKovAPhaseImDerivat);

        return new UmsetzungsbestaetigungPhaseHeader(phases);
    }

    private Map<Integer, UmsetzungsbestaetigungPhase> getUmsetzungsbestaetigungPhasesForHeader(KovAPhaseImDerivat currentKovAPhaseImDerivat) {
        Map<Integer, UmsetzungsbestaetigungPhase> phases = new HashMap<>();

        List<KovAPhaseImDerivat> kovaPhasenForDerivat = getKovAPhaseImDerivatForDerivat(currentKovAPhaseImDerivat.getDerivat());

        KovAPhase.getAllKovAPhasen().forEach(kovAPhase -> {
            UmsetzungsbestaetigungPhase phase;
            Optional<KovAPhaseImDerivat> existingKovAPhaseImDerivat = getForKovaPhase(kovaPhasenForDerivat, kovAPhase);

            if (existingKovAPhaseImDerivat.isPresent() && currentKovAPhaseImDerivat.getKovAPhase() != null) {
                phase = getForExistingPhase(kovAPhase, existingKovAPhaseImDerivat.get(), currentKovAPhaseImDerivat.getKovAPhase());
            } else {
                phase = getForNotExistingPhase(kovAPhase);
            }
            phases.put(kovAPhase.getId(), phase);
        });
        return phases;
    }

    private UmsetzungsbestaetigungPhase getForExistingPhase(KovAPhase kovAPhase, KovAPhaseImDerivat kovAPhaseImDerivat, KovAPhase currentKovAPhase) {
        if (isCurrentPhase(kovAPhase, currentKovAPhase)) {
            return getForCurrentPhase(kovAPhase, kovAPhaseImDerivat);
        } else {
            return getForNotCurrentPhase(kovAPhase, kovAPhaseImDerivat);
        }
    }

    private static UmsetzungsbestaetigungPhase getForNotCurrentPhase(KovAPhase kovAPhase, KovAPhaseImDerivat kovAPhaseImDerivat) {
        return new UmsetzungsbestaetigungPhase(kovAPhase.getId(), true, false, kovAPhaseImDerivat.getId().toString());
    }

    private static UmsetzungsbestaetigungPhase getForCurrentPhase(KovAPhase kovAPhase, KovAPhaseImDerivat kovAPhaseImDerivat) {
        return new UmsetzungsbestaetigungPhase(kovAPhase.getId(), true, true, kovAPhaseImDerivat.getId().toString());
    }

    private static boolean isCurrentPhase(KovAPhase kovAPhase, KovAPhase currentKovAPhase) {
        return kovAPhase.getId() == currentKovAPhase.getId();
    }

    private Optional<KovAPhaseImDerivat> getForKovaPhase(List<KovAPhaseImDerivat> kovaPhasenForDerivat, KovAPhase kovAPhase) {
        return kovaPhasenForDerivat.stream().filter(kovAPhaseImDerivat -> (kovAPhaseImDerivat.getKovAPhase().equals(kovAPhase))).findAny();
    }

    private List<KovAPhaseImDerivat> getKovAPhaseImDerivatForDerivat(Derivat derivat) {
        return derivatService.getKovAPhasenImDerivatByDerivat(derivat).stream()
                .filter(k -> !k.getKovaStatus().equals(KovAStatus.OFFEN))
                .collect(Collectors.toList());
    }

    private static UmsetzungsbestaetigungPhase getForNotExistingPhase(KovAPhase kovAPhase) {
        return new UmsetzungsbestaetigungPhase(kovAPhase.getId(), false, false, "");
    }

    public void downloadExcelExport(List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungen, KovAPhaseImDerivat kovAPhaseImDerivat, KovAPhase lastPhase) {
        umsetzungsbestaetigungService.downloadExcelExport(umsetzungsbestaetigungen,
                kovAPhaseImDerivat,
                lastPhase);
    }

    public ProcessResultDto processUmsetzungsbestaetigungChange(UmsetzungsbestaetigungDto umsetzungsbestaetigung, KovAPhaseImDerivat kovAPhaseImDerivat) {
        return umsetzungsbestaetigungService.processUmsetzungsbestaetigungChange(umsetzungsbestaetigung, session.getUser(), kovAPhaseImDerivat);
    }

    public ProcessResultDto processUmsetzungsbestaetigungBatchChanges(List<UmsetzungsbestaetigungDto> selectedUmsetzungsbestaetigungen, UmsetzungsBestaetigungStatus groupStatus, String groupKommentar, KovAPhaseImDerivat kovAPhaseImDerivat) {
        return umsetzungsbestaetigungService.processUmsetzungsbestaetigungBatchChanges(selectedUmsetzungsbestaetigungen, groupStatus, groupKommentar, session.getUser(), kovAPhaseImDerivat);
    }

}
