package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

/**
 *
 * @author fn
 */
public interface UmsetzungsbestaetigungKovAPhasenPhases {

    boolean phaseIsConfig(int id);

    boolean isCurrentPhase(int id);

    String goToPhase(int id);
}
