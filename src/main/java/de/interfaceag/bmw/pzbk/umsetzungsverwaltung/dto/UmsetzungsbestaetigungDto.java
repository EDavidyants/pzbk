package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Umsetzungsbestaetigung;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.prozessbaukasten.dto.ProzessbaukastenLabelDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOptionalTuple;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class UmsetzungsbestaetigungDto implements Serializable, ProzessbaukastenAnforderungOptionalTuple {

    private long id;

    private final Long anforderungId;
    private final String anforderung;
    private final String fachId;
    private final String version;
    private final String shortText;
    private final String fullText;

    private final SensorCoc sensorCoc;


    private final Modul modul;

    private UmsetzungsBestaetigungStatus status;
    private String comment;
    private String dialogComment;

    private final UmsetzungsBestaetigungStatus lastPhasestatus;
    private final String lastPhaseComment;

    private final ProzessbaukastenLabelDto prozessbaukastenLabelDto;

    private final String eingetrageneUmsetzungsbestaetiger;

    public UmsetzungsbestaetigungDto(Long anforderungId,
                                     String anforderung, String fachId, String version, String shortText, String fullText,
                                     SensorCoc sensorCoc, Modul modul, UmsetzungsBestaetigungStatus status,
                                     String comment, UmsetzungsBestaetigungStatus lastPhasestatus,
                                     String lastPhaseComment, ProzessbaukastenLabelDto prozessbaukastenLabelDto, String eingetrageneUmsetzungsbestaetiger) {
        this.anforderungId = anforderungId;
        this.anforderung = anforderung;
        this.shortText = shortText;
        this.fullText = fullText;
        this.sensorCoc = sensorCoc;
        this.modul = modul;
        this.status = status;
        this.comment = comment;
        this.dialogComment = "";
        this.lastPhasestatus = lastPhasestatus;
        this.lastPhaseComment = lastPhaseComment;
        this.fachId = fachId;
        this.version = version;
        this.prozessbaukastenLabelDto = prozessbaukastenLabelDto;
        this.eingetrageneUmsetzungsbestaetiger = eingetrageneUmsetzungsbestaetiger;
    }

    public static UmsetzungsbestaetigungDto buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(DerivatAnforderungModul derivatAnforderungModul,
                                                                                                     UmsetzungsBestaetigungStatus lastPhasestatus,
                                                                                                     String lastPhaseComment, String eingetrageneUmsetzungsbestaetiger) {

        return new UmsetzungsbestaetigungDtoBuilder()
                .setAnforderungId(derivatAnforderungModul.getAnforderung().getId())
                .setAnforderung(derivatAnforderungModul.getAnforderung().toString())
                .setFachId(derivatAnforderungModul.getAnforderung().getFachId())
                .setVersion(derivatAnforderungModul.getAnforderung().getVersion().toString())
                .setShortText(getShortString(derivatAnforderungModul.getAnforderung().getBeschreibungAnforderungDe()))
                .setFullText(derivatAnforderungModul.getAnforderung().getBeschreibungAnforderungDe())
                .setSensorCoc(derivatAnforderungModul.getAnforderung().getSensorCoc())
                .setModul(derivatAnforderungModul.getModul())
                .setStatus(UmsetzungsBestaetigungStatus.OFFEN)
                .setComment("")
                .setLastPhasestatus(lastPhasestatus)
                .setLastPhaseComment(lastPhaseComment)
                .withProzessbaukastenLabelDto(getGueltigesProzessbaukastenlabelDto(derivatAnforderungModul.getAnforderung()))
                .withEingetragenenUmsetzungsbestaetiger(eingetrageneUmsetzungsbestaetiger)
                .createUmsetzungsbestaetigungDto();
    }

    private static ProzessbaukastenLabelDto getGueltigesProzessbaukastenlabelDto(Anforderung anforderung) {
        Optional<ProzessbaukastenLabelDto> prozessbaukastenLabelDto = anforderung.getGueltigerProzessbaukasten().map(prozessbaukasten -> new ProzessbaukastenLabelDto(prozessbaukasten.getId(), prozessbaukasten.getFachId(), prozessbaukasten.getVersion()));
        return prozessbaukastenLabelDto.orElse(null);
    }

    public static UmsetzungsbestaetigungDto buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(Umsetzungsbestaetigung umsetzungsbestaetigung,
                                                                                                    UmsetzungsBestaetigungStatus lastPhasestatus,
                                                                                                    String lastPhaseComment, String eingetrageneUmsetzungsbestaetiger) {
        return new UmsetzungsbestaetigungDtoBuilder()
                .setAnforderungId(umsetzungsbestaetigung.getAnforderung().getId())
                .setAnforderung(umsetzungsbestaetigung.getAnforderung().toString())
                .setFachId(umsetzungsbestaetigung.getAnforderung().getFachId())
                .setVersion(umsetzungsbestaetigung.getAnforderung().getVersion().toString())
                .setShortText(getShortString(umsetzungsbestaetigung.getAnforderung().getBeschreibungAnforderungDe()))
                .setFullText(umsetzungsbestaetigung.getAnforderung().getBeschreibungAnforderungDe())
                .setSensorCoc(umsetzungsbestaetigung.getAnforderung().getSensorCoc())
                .setModul(umsetzungsbestaetigung.getModul()).setStatus(umsetzungsbestaetigung.getStatus())
                .setComment(umsetzungsbestaetigung.getKommentar())
                .setLastPhasestatus(lastPhasestatus)
                .setLastPhaseComment(lastPhaseComment)
                .withProzessbaukastenLabelDto(getGueltigesProzessbaukastenlabelDto(umsetzungsbestaetigung.getAnforderung()))
                .withEingetragenenUmsetzungsbestaetiger(eingetrageneUmsetzungsbestaetiger)
                .createUmsetzungsbestaetigungDto();

    }

    private static String getShortString(String beschreibung) {
        if (beschreibung.length() > 80) {
            return beschreibung.substring(0, 80) + "...";
        } else {
            return beschreibung;
        }
    }

    public boolean isUmgesetzt() {
        return status.equals(UmsetzungsBestaetigungStatus.UMGESETZT);
    }

    @Override
    public Long getAnforderungId() {
        return anforderungId;
    }

    public String getAnforderung() {
        return anforderung;
    }

    public String getShortText() {
        return shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public SensorCoc getSensorCoc() {
        return sensorCoc;
    }

    public Modul getModul() {
        return modul;
    }

    public Optional<UmsetzungsBestaetigungStatus> getLastPhasestatus() {
        return Optional.ofNullable(lastPhasestatus);
    }

    public UmsetzungsBestaetigungStatus getStatus() {
        return status;
    }

    public String getDialogComment() {
        return dialogComment;
    }

    public void setDialogComment(String dialogComment) {
        this.dialogComment = dialogComment;
    }

    public String getComment() {
        return comment;
    }


    public String getLastPhasestatusString() {
        Optional<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus = getLastPhasestatus();
        return umsetzungsBestaetigungStatus.isPresent() ? umsetzungsBestaetigungStatus.get().getStatusBezeichnung() : "";

    }

    public String getLastPhaseComment() {
        return lastPhaseComment != null ? lastPhaseComment : "";
    }

    @Override
    public Optional<Long> getProzessbaukastenId() {
        if (prozessbaukastenLabelDto != null) {
            return Optional.of(prozessbaukastenLabelDto.getId());
        }
        return Optional.empty();
    }

    public ProzessbaukastenLabelDto getProzessbaukastenLabel() {
        return prozessbaukastenLabelDto;
    }

    public boolean isProzessbaukastenZugeordnet() {
        return prozessbaukastenLabelDto != null;
    }

    @Override
    public String getAnforderungFachId() {
        return getFachId();
    }

    // ---------- setter -------------------------------------------------------
    public void setStatus(UmsetzungsBestaetigungStatus status) {
        this.status = status;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public String getFachId() {
        return fachId;
    }

    public String getVersion() {
        return version;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + (int) (this.anforderungId ^ (this.anforderungId >>> 32));
        hash = 31 * hash + Objects.hashCode(this.modul.getId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UmsetzungsbestaetigungDto other = (UmsetzungsbestaetigungDto) obj;
        if (!Objects.equals(this.anforderungId, other.anforderungId)) {
            return false;
        }
        return Objects.equals(this.modul.getId(), other.modul.getId());
    }

    public boolean isUmsetzungsbestaetigungUmgesetzt() {
        return status.equals(UmsetzungsBestaetigungStatus.UMGESETZT);
    }

    public String getEingetrageneUmsetzungsbestaetiger() {
        return eingetrageneUmsetzungsbestaetiger;
    }
}
