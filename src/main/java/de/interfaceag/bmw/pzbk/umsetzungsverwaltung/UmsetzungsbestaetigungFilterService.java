package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewFilter;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Stateless
public class UmsetzungsbestaetigungFilterService {

    @Inject
    private Session session;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private ModulService modulService;
    @Inject
    private ProzessbaukastenReadService prozessbaukastenReadService;
    @Inject
    private ThemenklammerService themenklammerService;

    public UmsetzungsbestaetigungViewFilter getUmsetzungsbestaetigungViewFilter(UrlParameter urlParameter, KovAPhaseImDerivat kovAPhaseImDerivat, KovAPhase previousPhase) {

        List<SensorCoc> sensorCocsForFilter = getSensorCocForFilter();
        List<Modul> modulForFilter = getModulForFilter();
        Collection<ProzessbaukastenFilterDto> prozessbaukaesten = getProzessbaukaestenForFilter();
        Collection<ThemenklammerDto> themenklammern = getThemenklammernForFilter();

        return new UmsetzungsbestaetigungViewFilter(urlParameter, sensorCocsForFilter, modulForFilter, kovAPhaseImDerivat.getKovAPhase(), previousPhase, prozessbaukaesten, themenklammern);
    }

    private Collection<ThemenklammerDto> getThemenklammernForFilter() {
        return themenklammerService.getAll();
    }

    public void setRestrictedFilterForUmsetzungsbestaetigung(UmsetzungsbestaetigungViewFilter filter, List<UmsetzungsbestaetigungDto> umsetzungsbestaetigung, UrlParameter urlParameter) {

        Set<SensorCoc> sensorCocs = getSensorCocsFromUmsetzungsbestaetigung(umsetzungsbestaetigung);
        Set<Modul> module = getModulesFromUmsetzungsbestaetigung(umsetzungsbestaetigung);
        Collection<ProzessbaukastenFilterDto> prozessbaukaesten = getProzessbaukaestenFromUmsetzungsbestaetigung(umsetzungsbestaetigung);

        filter.restrictSelectableValuesBasedOnSearchResult(umsetzungsbestaetigung, urlParameter, sensorCocs, module, prozessbaukaesten);
    }

    private Collection<ProzessbaukastenFilterDto> getProzessbaukaestenFromUmsetzungsbestaetigung(List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungen) {
        for (UmsetzungsbestaetigungDto umsetzungsbestaetigungDto : umsetzungsbestaetigungen) {
            umsetzungsbestaetigungDto.getProzessbaukastenId();
        }

        final Set<Long> prozessbaukastenIds = umsetzungsbestaetigungen.stream()
                .map(UmsetzungsbestaetigungDto::getProzessbaukastenId)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());

        final Collection<ProzessbaukastenFilterDto> prozessbaukaestenForFilter = getProzessbaukaestenForFilter();
        return prozessbaukaestenForFilter.stream().filter(filter -> prozessbaukastenIds.contains(filter.getId())).collect(Collectors.toList());
    }

    private Set<SensorCoc> getSensorCocsFromUmsetzungsbestaetigung(List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungen) {
        return umsetzungsbestaetigungen.stream().map(UmsetzungsbestaetigungDto::getSensorCoc).collect(Collectors.toSet());
    }

    private Set<Modul> getModulesFromUmsetzungsbestaetigung(List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungen) {
        return umsetzungsbestaetigungen.stream().map(UmsetzungsbestaetigungDto::getModul).collect(Collectors.toSet());
    }

    private Collection<ProzessbaukastenFilterDto> getProzessbaukaestenForFilter() {
        return prozessbaukastenReadService.getAllFilterDto();
    }

    private List<SensorCoc> getSensorCocForFilter() {
        return berechtigungService.getSensorCocForMitarbeiter(session.getUser(), true);
    }

    private List<Modul> getModulForFilter() {
        return modulService.getAllModule();
    }


}
