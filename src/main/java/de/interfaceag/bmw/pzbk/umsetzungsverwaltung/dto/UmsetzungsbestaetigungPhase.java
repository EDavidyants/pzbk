package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class UmsetzungsbestaetigungPhase {

    private final int id;
    private final boolean config;
    private final boolean currentPhase;
    private final String urlId;

    public UmsetzungsbestaetigungPhase(int id, boolean config, boolean currentPhase, String urlId) {
        this.id = id;
        this.config = config;
        this.currentPhase = currentPhase;
        this.urlId = urlId;
    }

    public int getId() {
        return id;
    }

    public boolean isConfig() {
        return config;
    }

    public boolean isCurrentPhase() {
        return currentPhase;
    }

    public String getUrlId() {
        return urlId;
    }

}
