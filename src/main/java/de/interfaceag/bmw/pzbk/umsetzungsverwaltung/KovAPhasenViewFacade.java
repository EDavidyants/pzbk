package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import de.interfaceag.bmw.pzbk.filter.pages.KovAPhasenViewFilter;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.BerechtigungKovAPhaseImDerivatSensorCocDTO;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.KovAPhaseImDerivatDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.KovAPhasenViewPermission;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.KovaPhasenViewData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class KovAPhasenViewFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(KovAPhasenViewFacade.class);

    @Inject
    private Session session;

    @Inject
    private BerechtigungService berechtigungService;

    @Inject
    private DerivatService derivatService;
    @Inject
    private KovAPhaseImDerivatSearchService kovAPhaseImDerivatService;
    @Inject
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;
    @Inject
    private UmsetzungsbestaetigungBerechtigungService umsetzungsbestaetigungBerechtigungService;
    @Inject
    private SensorCocService sensorCocService;

    public void processBerechtigungDialogChanges(KovaPhasenViewBerechtigungDialogData berechtigungDialogData) {
        try {
            checkForValidBerechtigungDialogData(berechtigungDialogData);
        } catch (InvalidDataException ex) {
            LOG.error(null, ex);
            return;
        }

        List<BerechtigungKovAPhaseImDerivatSensorCocDTO> changedData
                = berechtigungDialogData.getBerechtigungList()
                .stream()
                .filter(BerechtigungKovAPhaseImDerivatSensorCocDTO::isChanged)
                .collect(Collectors.toList());
        umsetzungsbestaetigungBerechtigungService.persistBerechtigungKovAPhaseImDerivatSensorCocDTOList(changedData);
    }

    private void checkForValidBerechtigungDialogData(KovaPhasenViewBerechtigungDialogData berechtigungDialogData)
            throws InvalidDataException {
        if (berechtigungDialogData == null) {
            throw new InvalidDataException("berechtigungDialogData is null");
        }
    }

    public Optional<KovaPhasenViewBerechtigungDialogData> getBerechtigungDialogData(Long kovaPhaseImDerivatId) {
        try {
            checkForValidKovaPhaseImDerivatId(kovaPhaseImDerivatId);

            Optional<KovAPhaseImDerivat> kovAPhaseImDerivat
                    = derivatService.getKovAPhaseImDerivatById(kovaPhaseImDerivatId);

            if (!kovAPhaseImDerivat.isPresent()) {
                throw new InvalidDataException("kovaPhaseImDerivat is null");
            }

            List<SensorCoc> sensorCocs = getSensorCocForUser();

            List<SensorCoc> derivatSensorCocs = umsetzungsbestaetigungService.getSensorCocForDerivatInKovaPhase(kovAPhaseImDerivat.get());

            sensorCocs.retainAll(derivatSensorCocs);

            List<BerechtigungKovAPhaseImDerivatSensorCocDTO> berechtigungList = umsetzungsbestaetigungBerechtigungService
                    .getBerechtigungenListForKovAPhaseImDerivatAndSensorCocList(
                            kovAPhaseImDerivat.get(), sensorCocs
                    );

            String header = kovAPhaseImDerivat.get().getDerivat().getName() + " | "
                    + kovAPhaseImDerivat.get().getKovAPhase().getBezeichnung();

            Set<Rolle> userRoles = session.getUserPermissions().getRoles();

            return Optional.of(new KovaPhasenViewBerechtigungDialogData(
                    berechtigungList,
                    userRoles,
                    kovAPhaseImDerivat.get().getKovaStatus(),
                    header
            ));

        } catch (InvalidDataException ex) {
            LOG.error(null, ex);
        }
        return Optional.empty();

    }

    private void checkForValidKovaPhaseImDerivatId(Long kovaPhaseImDerivatId)
            throws InvalidDataException {
        if (kovaPhaseImDerivatId == null || kovaPhaseImDerivatId.equals(0L)) {
            throw new InvalidDataException("kovaPhaseImDerivatId is null");
        }
    }

    public KovaPhasenViewData getViewData(UrlParameter urlParameter) {

        UserPermissions<BerechtigungDto> permissions = session.getUserPermissions();

        KovAPhasenViewPermission viewPermission = getViewPermission(permissions.getRoles());

        KovAPhasenViewFilter filter = getFilter(urlParameter);

        List<KovAPhaseImDerivatDto> kovAPhasenImDerivat = getKovAPhasenImDerivat(filter, permissions);

        return new KovaPhasenViewData(viewPermission, filter, kovAPhasenImDerivat);
    }

    private List<SensorCoc> getSensorCocForUser() {
        if (session.hasRole(Rolle.ADMIN)) {
            return sensorCocService.getAllSortByTechnologie();
        }

        List<Long> sensorCocLeiterIds = session.getUserPermissions().getSensorCocAsSensorCocLeiterSchreibend().stream().map(BerechtigungDto::getId).collect(Collectors.toList());
        List<Long> sensorCocVertrederIds = session.getUserPermissions().getSensorCocAsVertreterSCLSchreibend().stream().map(BerechtigungDto::getId).collect(Collectors.toList());
        List<SensorCoc> result = new ArrayList<>();
        List<SensorCoc> sensorCocForLeiter = sensorCocService.getSensorCocsByIdList(sensorCocLeiterIds);
        List<SensorCoc> sensorCocForVertreter = sensorCocService.getSensorCocsByIdList(sensorCocVertrederIds);
        result.addAll(sensorCocForLeiter);
        result.addAll(sensorCocForVertreter);
        return result;
    }

    private List<Derivat> getDerivateForUser() {
        return berechtigungService.getDerivateForCurrentUser();
    }

    private KovAPhasenViewFilter getFilter(UrlParameter urlParameter) {
        List<Derivat> derivate = getDerivateForUser();
        List<KovAPhase> kovaPhasen = KovAPhase.getAllKovAPhasen();
        return new KovAPhasenViewFilter(urlParameter, derivate, kovaPhasen);
    }

    private KovAPhasenViewPermission getViewPermission(Set<Rolle> userRoles) {
        return new KovAPhasenViewPermission(userRoles);
    }

    private List<KovAPhaseImDerivatDto> getKovAPhasenImDerivat(KovAPhasenViewFilter filter,
                                                               UserPermissions<BerechtigungDto> permissions) {
        return kovAPhaseImDerivatService.getKovAPhasenImDerivat(filter, permissions);
    }

}
