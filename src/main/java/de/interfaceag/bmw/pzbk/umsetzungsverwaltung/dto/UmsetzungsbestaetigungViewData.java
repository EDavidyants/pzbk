package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungViewDataContent;

import java.io.Serializable;
import java.util.List;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class UmsetzungsbestaetigungViewData implements Serializable, UmsetzungsbestaetigungViewDataContent {

     private final UmsetzungsbestaetigungViewPermission viewPermission;
     private final UmsetzungsbestaetigungViewFilter viewFilter;
     private final UmsetzungsbestaetigungPhaseHeader phaseHeader;

     private final List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungen;
     private List<UmsetzungsbestaetigungDto> selectedUmsetzungsbestaetigungen;

     private final List<UmsetzungsBestaetigungStatus> groupStatus;
     private UmsetzungsBestaetigungStatus selectedGroupStatus;
     private String groupKommentar;

     private final KovAPhaseImDerivat kovAPhaseImDerivat;
     private final KovAPhase lastPhase;
     private KovAPhase currentPhase;
     private UmsetzungsbestaetigungDto umsetzungsbestaetigungDtoForComment;

     public UmsetzungsbestaetigungViewData(UmsetzungsbestaetigungViewPermission viewPermission,
                                           List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungen,
                                           UmsetzungsbestaetigungViewFilter viewFilter,
                                           UmsetzungsbestaetigungPhaseHeader phaseHeader,
                                           List<UmsetzungsBestaetigungStatus> groupStatus,
                                           KovAPhaseImDerivat kovAPhaseImDerivat, KovAPhase lastPhase
     ) {
          this.viewPermission = viewPermission;
          this.umsetzungsbestaetigungen = umsetzungsbestaetigungen;
          this.viewFilter = viewFilter;
          this.phaseHeader = phaseHeader;
          this.groupStatus = groupStatus;
          this.kovAPhaseImDerivat = kovAPhaseImDerivat;
          this.lastPhase = lastPhase;
          if (kovAPhaseImDerivat != null && kovAPhaseImDerivat.getKovAPhase() != null) {
               this.currentPhase = kovAPhaseImDerivat.getKovAPhase();
          }
     }

     public static UmsetzungsbestaetigungViewData forEmptyPhase() {
          return new UmsetzungsbestaetigungViewData(null,
                  null, null, null,
                  null, null, KovAPhase.VABG0);
     }

     public UmsetzungsbestaetigungViewPermission getViewPermission() {
          return viewPermission;
     }

     public UmsetzungsbestaetigungPhaseHeader getPhaseHeader() {
          return phaseHeader;
     }

     @Override
     public List<UmsetzungsbestaetigungDto> getUmsetzungsbestaetigungen() {
          return umsetzungsbestaetigungen;
     }

     @Override
     public List<UmsetzungsbestaetigungDto> getSelectedUmsetzungsbestaetigungen() {
          return selectedUmsetzungsbestaetigungen;
     }

     @Override
     public void setSelectedUmsetzungsbestaetigungen(List<UmsetzungsbestaetigungDto> selectedUmsetzungsbestaetigungen) {
          this.selectedUmsetzungsbestaetigungen = selectedUmsetzungsbestaetigungen;
     }

     @Override
     public List<UmsetzungsBestaetigungStatus> getGroupStatus() {
          return groupStatus;
     }

     @Override
     public UmsetzungsBestaetigungStatus getSelectedGroupStatus() {
          return selectedGroupStatus;
     }

     @Override
     public void setSelectedGroupStatus(UmsetzungsBestaetigungStatus selectedGroupStatus) {
          this.selectedGroupStatus = selectedGroupStatus;
     }

     @Override
     public String getGroupKommentar() {
          return groupKommentar;
     }

     @Override
     public void setGroupKommentar(String groupKommentar) {
          this.groupKommentar = groupKommentar;
     }

     public KovAPhaseImDerivat getKovAPhaseImDerivat() {
          return kovAPhaseImDerivat;
     }

     @Override
     public KovAPhase getLastPhase() {
          return lastPhase;
     }

     @Override
     public KovAPhase getCurrentPhase() {
          return currentPhase;
     }

     public UmsetzungsbestaetigungViewFilter getViewFilter() {
          return viewFilter;
     }

     @Override
     public boolean hasLastPhase() {
          return lastPhase != null;
     }

     public UmsetzungsbestaetigungDto getUmsetzungsbestaetigungDtoForComment() {
          return umsetzungsbestaetigungDtoForComment;
     }

     public void setUmsetzungsbestaetigungDtoForComment(UmsetzungsbestaetigungDto umsetzungsbestaetigungDtoForComment) {
          this.umsetzungsbestaetigungDtoForComment = umsetzungsbestaetigungDtoForComment;
     }

     public void setCommentForUmsetzungsbestaetigung() {

          if (umsetzungsbestaetigungDtoForComment != null) {
               getUmsetzungsbestaetigungen()
                       .stream()
                       .filter(u -> u.equals(umsetzungsbestaetigungDtoForComment))
                       .findFirst()
                       .ifPresent(u -> u.setComment(u.getComment()));
          }
          umsetzungsbestaetigungDtoForComment = null;
     }

     public int getAnzahlTreffer() {
          if (umsetzungsbestaetigungen == null) {
               return 0;
          }
          return umsetzungsbestaetigungen.size();
     }
}
