package de.interfaceag.bmw.pzbk.dialog.viewdata;

import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author fn
 */
public final class DialogUmsetzerDto implements Serializable {

    private final Long umsetzerId;

    private final Long modulId;
    private final String modulName;

    private final Long seTeamId;
    private final String seTeamName;

    private final Long komponenteId;
    private final String komponenteName;
    private final String komponentePpg;

    private final boolean isModulFreigegeben;

    private DialogUmsetzerDto(Long umsetzerId, Long modulId, String modulName, Long seTeamId, String modulSeTeamName, Long komponenteId, String modulKomponenteName, Boolean isModulFreigegeben, String modulKomponentePpg) {
        this.umsetzerId = umsetzerId;
        this.modulId = modulId;
        this.modulName = modulName;
        this.seTeamId = seTeamId;
        this.seTeamName = modulSeTeamName;
        this.komponenteId = komponenteId;
        this.komponenteName = modulKomponenteName;
        this.isModulFreigegeben = isModulFreigegeben;
        this.komponentePpg = modulKomponentePpg;
    }

    public String getKomponentePpg() {
        return komponentePpg;
    }

    public String getModulName() {
        return modulName;
    }

    public String getSeTeamName() {
        return seTeamName;
    }

    public String getKomponenteName() {
        return komponenteName;
    }

    public Boolean getIsModulFreigegeben() {
        return isModulFreigegeben;
    }

    public Long getUmsetzerId() {
        return umsetzerId;
    }

    public Long getModulId() {
        return modulId;
    }

    public Long getSeTeamId() {
        return seTeamId;
    }

    public Long getKomponenteId() {
        return komponenteId;
    }

    public static DialogUmsetzerDto forUmsetzer(Umsetzer umsetzer, boolean isFreigegeben) {
        return getBuilder().forUmsetzer(umsetzer).isFreigegeben(isFreigegeben).build();
    }

    public static DialogUmsetzerDto forKomponenteDto(ModulKomponenteDto komponenteDto) {
        return getBuilder().forKomponenteDto(komponenteDto).build();
    }

    public static DialogUmsetzerDto forSeTeamDto(ModulSeTeamDto seTeamDto) {
        return getBuilder().forSeTeamDto(seTeamDto).build();
    }

    private static Builder getBuilder() {
        return new Builder();
    }

    public static class Builder {

        private Long umsetzerId;

        private Long modulId;
        private String modulName;

        private Long seTeamId;
        private String seTeamName;

        private Long komponenteId;
        private String komponenteName;
        private String komponentePpg;

        private boolean isModulFreigegeben;

        public Builder() {
        }

        public Builder forUmsetzer(Umsetzer umsetzer) {
            this.umsetzerId = umsetzer.getId();

            if (umsetzer.getKomponente() != null) {
                return forKomponente(umsetzer.getKomponente());
            } else {
                return forSeTeam(umsetzer.getSeTeam());
            }
        }

        public Builder forModul(Modul modul) {
            this.modulId = modul.getId();
            this.modulName = modul.getName();
            return this;
        }

        public Builder forSeTeamDto(ModulSeTeamDto seTeamDto) {
            this.seTeamId = seTeamDto.getId();
            this.seTeamName = seTeamDto.getName();

            this.modulId = seTeamDto.getModulId();
            this.modulName = seTeamDto.getModulName();

            return this;
        }

        public Builder forSeTeam(ModulSeTeam seTeam) {
            this.seTeamId = seTeam.getId();
            this.seTeamName = seTeam.getName();
            return forModul(seTeam.getElternModul());
        }

        public Builder forKomponente(ModulKomponente komponente) {
            this.komponenteId = komponente.getId();
            this.komponenteName = komponente.getName();
            this.komponentePpg = komponente.getPpg();
            return forSeTeam(komponente.getSeTeam());
        }

        public Builder forKomponenteDto(ModulKomponenteDto komponenteDto) {
            this.komponenteId = komponenteDto.getId();
            this.komponenteName = komponenteDto.getName();
            this.komponentePpg = komponenteDto.getPpg();

            this.seTeamId = komponenteDto.getSeTeamId();
            this.seTeamName = komponenteDto.getSeTeamName();

            this.modulId = komponenteDto.getModulId();
            this.modulName = komponenteDto.getModulName();
            return this;
        }

        public Builder isFreigegeben(boolean isFreigegeben) {
            this.isModulFreigegeben = isFreigegeben;
            return this;
        }

        public DialogUmsetzerDto build() {
            return new DialogUmsetzerDto(umsetzerId, modulId, modulName, seTeamId, seTeamName, komponenteId, komponenteName, isModulFreigegeben, komponentePpg);
        }

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.modulId);
        hash = 97 * hash + Objects.hashCode(this.seTeamId);
        hash = 97 * hash + Objects.hashCode(this.komponenteId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DialogUmsetzerDto other = (DialogUmsetzerDto) obj;
        if (!Objects.equals(this.modulId, other.modulId)) {
            return false;
        }
        if (!Objects.equals(this.seTeamId, other.seTeamId)) {
            return false;
        }
        return Objects.equals(this.komponenteId, other.komponenteId);
    }

}
