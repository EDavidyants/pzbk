package de.interfaceag.bmw.pzbk.dialog.viewdata;

import java.io.Serializable;

public class DerivatZuordnenDto implements Serializable {

    private final Long id;
    private final String derivatName;
    private final String produktlinie;

    public DerivatZuordnenDto(Long id, String derivatName, String produktlinie) {
        this.id = id;
        this.derivatName = derivatName;
        this.produktlinie = produktlinie;
    }

    public Long getId() {
        return id;
    }

    public String getDerivatName() {
        return derivatName;
    }

    public String getProduktlinie() {
        return produktlinie;
    }

}
