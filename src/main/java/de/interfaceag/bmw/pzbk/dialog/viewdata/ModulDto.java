package de.interfaceag.bmw.pzbk.dialog.viewdata;

import de.interfaceag.bmw.pzbk.entities.comparator.ModulCompareFields;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author fn
 */
public class ModulDto implements ModulCompareFields, Serializable {

    private final Long id;
    private final String name;
    private final List<ModulSeTeamDto> seTeams;

    public ModulDto(Long modulId, String modulName, List<ModulSeTeamDto> seTeams) {
        this.id = modulId;
        this.name = modulName;
        this.seTeams = seTeams;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<ModulSeTeamDto> getSeTeams() {
        return seTeams;
    }

}
