package de.interfaceag.bmw.pzbk.dialog.viewdata;

import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.comparator.ModulSeTeamCompareFields;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author fn
 */
public final class ModulSeTeamDto implements ModulSeTeamCompareFields, Serializable {

    private final Long id;
    private final String name;
    private final List<ModulKomponenteDto> komponenten;

    private final Long modulId;
    private final String modulName;

    private ModulSeTeamDto(Long id, String name, List<ModulKomponenteDto> komponenten, Long modulId, String modulName) {
        this.id = id;
        this.name = name;
        this.komponenten = komponenten;
        this.modulId = modulId;
        this.modulName = modulName;
    }

    public String getModulName() {
        return modulName;
    }

    public List<ModulKomponenteDto> getKomponenten() {
        return komponenten;
    }

    public Long getId() {
        return id;
    }

    public Long getModulId() {
        return modulId;
    }

    @Override
    public String getName() {
        return name;
    }

    public static ModulSeTeamDto forSeTeam(ModulSeTeam seTeam) {
        List<ModulKomponenteDto> komponenten = seTeam.getKomponenten().stream().map(ModulKomponenteDto::forKomponente).collect(Collectors.toList());
        return new ModulSeTeamDto(seTeam.getId(), seTeam.getName(), komponenten, seTeam.getElternModul().getId(), seTeam.getElternModul().getName());
    }

}
