package de.interfaceag.bmw.pzbk.dialog.viewdata;

import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.comparator.ModulKomponenteComparFields;

import java.io.Serializable;

/**
 *
 * @author fn
 */
public final class ModulKomponenteDto implements ModulKomponenteComparFields, Serializable {

    private final Long id;
    private final String name;
    private final String ppg;

    private final Long seTeamId;
    private final String seTeamName;

    private final Long modulId;
    private final String modulName;

    private ModulKomponenteDto(Long id, String name, String ppg, Long seTeamId, String seTeamName, Long modulId, String modulName) {
        this.id = id;
        this.name = name;
        this.ppg = ppg;
        this.seTeamId = seTeamId;
        this.seTeamName = seTeamName;
        this.modulId = modulId;
        this.modulName = modulName;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPpg() {
        return ppg;
    }

    public Long getSeTeamId() {
        return seTeamId;
    }

    public String getSeTeamName() {
        return seTeamName;
    }

    public Long getModulId() {
        return modulId;
    }

    public String getModulName() {
        return modulName;
    }

    public static ModulKomponenteDto forKomponente(ModulKomponente komponente) {
        return newBuilder().forKomponente(komponente).build();
    }

    private static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {

        private Long id;
        private String name;
        private String ppg;

        private Long seTeamId;
        private String seTeamName;

        private Long modulId;
        private String modulName;

        public Builder() {
        }

        public Builder forKomponente(ModulKomponente komponente) {
            this.id = komponente.getId();
            this.name = komponente.getName();
            this.ppg = komponente.getPpg();
            return forSeTeam(komponente.getSeTeam());
        }

        private Builder forSeTeam(ModulSeTeam seTeam) {
            this.seTeamId = seTeam.getId();
            this.seTeamName = seTeam.getName();
            return forModul(seTeam.getElternModul());
        }

        private Builder forModul(Modul modul) {
            this.modulId = modul.getId();
            this.modulName = modul.getName();
            return this;
        }

        public ModulKomponenteDto build() {
            return new ModulKomponenteDto(id, name, ppg, seTeamId, seTeamName, modulId, modulName);
        }

    }

}
