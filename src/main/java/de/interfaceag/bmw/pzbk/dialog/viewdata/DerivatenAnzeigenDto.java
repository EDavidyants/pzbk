package de.interfaceag.bmw.pzbk.dialog.viewdata;

import java.io.Serializable;

/**
 *
 * @author fn
 */
public class DerivatenAnzeigenDto implements Serializable {

    private final Long id;
    private final String derivatName;
    private final Long derivatId;
    private boolean nachZakUebertragen;

    public DerivatenAnzeigenDto(long id, String derivatName, Long derivatId, boolean nachZakUebertragen) {
        this.id = id;
        this.derivatName = derivatName;
        this.nachZakUebertragen = nachZakUebertragen;
        this.derivatId = derivatId;
    }

    public Long getId() {
        return id;
    }

    public String getDerivatName() {
        return derivatName;
    }

    public boolean isNachZakUebertragen() {
        return nachZakUebertragen;
    }

    public Long getDerivatId() {
        return derivatId;
    }

    public void setNachZakUebertragen(boolean nachZakUebertragen) {
        this.nachZakUebertragen = nachZakUebertragen;
    }

}
