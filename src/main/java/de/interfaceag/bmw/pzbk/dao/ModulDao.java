package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author evda
 */
@Dependent
public class ModulDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public void persistModule(List<Modul> module) {
        module.forEach(m -> {
            if (m.getId() != null) {
                entityManager.merge(m);
            } else {
                entityManager.persist(m);
            }
        });
        entityManager.flush();
    }

    public List<Modul> getModuleByIdList(List<Long> ids) {
        Query q = entityManager.createNamedQuery(Modul.QUERY_BY_ID_LIST, Modul.class);
        q.setParameter("ids", ids);
        return ids != null && !ids.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<String> getAllFachbereiche() {
        TypedQuery<String> q = entityManager.createNamedQuery(Modul.ALL_FACHBEREICHE, String.class);
        return q.getResultList();
    }

    public void persistSeTeam(ModulSeTeam modulSeTeam) {
        if (getSeTeamById(modulSeTeam.getId()) != null) {
            entityManager.merge(modulSeTeam);
        } else {
            entityManager.persist(modulSeTeam);
        }
        entityManager.flush();
    }

    public ModulSeTeam getSeTeamById(Long id) {
        if (id != null) {
            Query q = entityManager.createNamedQuery(ModulSeTeam.BY_ID, ModulSeTeam.class);
            q.setParameter("id", id);
            List<ModulSeTeam> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public void persistModulKomponente(ModulKomponente modulKomponente) {
        if (getModulKomponenteById(modulKomponente.getId()) != null) {
            entityManager.merge(modulKomponente);
        } else {
            entityManager.persist(modulKomponente);
        }
        entityManager.flush();
    }

    public ModulKomponente getModulKomponenteById(Long id) {
        if (id != null) {
            Query q = entityManager.createNamedQuery(ModulKomponente.BY_ID, ModulKomponente.class);
            q.setParameter("id", id);
            List<ModulKomponente> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<Modul> getAllModule() {
        return entityManager.createNamedQuery(Modul.ALL, Modul.class).getResultList();
    }

    public Modul getModulByName(String modulName) {
        if (modulName != null && !modulName.isEmpty()) {
            Query q = entityManager.createNamedQuery(Modul.BY_NAME, Modul.class);
            q.setParameter("name", modulName);
            List<Modul> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<ModulSeTeam> getSeTeamsByNameList(List<String> nameList) {
        Query q = entityManager.createNamedQuery(ModulSeTeam.BY_NAME_LIST, ModulSeTeam.class);
        q.setParameter("nameList", nameList);
        return nameList != null && !nameList.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<ModulSeTeam> getSeTeamsByIdList(List<Long> idList) {
        Query q = entityManager.createNamedQuery(ModulSeTeam.BY_ID_LIST, ModulSeTeam.class);
        q.setParameter("idList", idList);
        return idList != null && !idList.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<ModulSeTeam> getAllSeTeams() {
        return entityManager.createNamedQuery(ModulSeTeam.ALL, ModulSeTeam.class).getResultList();
    }

    public List<ModulSeTeam> getAllSeTeamsFetch() {
        return entityManager.createNamedQuery(ModulSeTeam.ALL_FETCH, ModulSeTeam.class).getResultList();
    }

    public ModulSeTeam getSeTeamByName(String name) {
        if (name != null && !name.isEmpty()) {
            Query q = entityManager.createNamedQuery(ModulSeTeam.BY_NAME, ModulSeTeam.class);
            q.setParameter("name", name);
            List<ModulSeTeam> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<ModulSeTeam> getSeTeamsThatStartWith(String start, Modul modul) {
        Query q;
        if (modul == null) {
            q = entityManager.createNamedQuery(ModulSeTeam.STARTS_WITH, ModulSeTeam.class);
        } else {
            q = entityManager.createNamedQuery(ModulSeTeam.BY_MODUL_AND_STARTS_WITH, ModulSeTeam.class);
            q.setParameter("modul", modul);
        }
        q.setParameter("start", start + "%");
        return start != null ? q.getResultList() : new ArrayList<>();
    }

    public List<ModulKomponente> getAllModulKomponenten() {
        return entityManager.createNamedQuery(ModulKomponente.ALL, ModulKomponente.class).getResultList();
    }

    public ModulKomponente getModulKomponenteByPpg(String ppg) {
        if (ppg != null && !ppg.isEmpty()) {
            Query q = entityManager.createNamedQuery(ModulKomponente.BY_PPG, ModulKomponente.class);
            q.setParameter("ppg", ppg);
            List<ModulKomponente> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<ModulKomponente> getModulKomponenteByName(String name) {
        Query q = entityManager.createNamedQuery(ModulKomponente.BY_NAME, ModulKomponente.class);
        q.setParameter("name", name);
        return name != null && !name.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

}
