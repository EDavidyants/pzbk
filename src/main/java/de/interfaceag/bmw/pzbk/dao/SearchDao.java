package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.SearchFilter;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import de.interfaceag.bmw.pzbk.shared.dto.SearchFilterDTO;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sl
 */
@Dependent
public class SearchDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public List<Long> getAnforderungHistroyIdList(SearchFilterDTO filter, SearchFilterDTO statusFilter) {
        SearchFilter filter1 = new SearchFilter(filter.getFilterType());
        if (filter.getStartDateValue() == null && filter.getEndDateValue() == null) {
            return new ArrayList<>();
        }
        filter1.setStartDateValue(filter.getStartDateValue());
        filter1.setEndDateValue(filter.getEndDateValue());
        SearchFilter filter2 = new SearchFilter(statusFilter.getFilterType());
        filter2.setStatusListValue(statusFilter.getStatusListValue());
        return getAnforderungHistroyIdList(filter1, filter2);
    }

    /**
     * Get list of ids based on the anforderung history which determine whether
     * a status change has occurred in a certain time period.
     *
     * @param filter       which determine the time period.
     * @param statusFilter which determines the considered status.
     * @return list of ids for Anforderung/Meldung.
     */
    @Deprecated
    private List<Long> getAnforderungHistroyIdList(SearchFilter filter, SearchFilter statusFilter) {

        QueryPartDTO queryPartDTO = new QueryPartDTO();

        queryPartDTO.append("SELECT a.anforderungId FROM AnforderungHistory a WHERE 1=1");

        switch (filter.getFilterType()) {
            case HISTORIE:
            case STATUSAENDERUNGSDATUM_VON:
                queryPartDTO.append(" AND a.datum >= :dateStart");
                queryPartDTO.put("dateStart", filter.getStartDateValue());
                break;
            case STATUS_AENDERUNGSDATUM_BIS:
                queryPartDTO.append(" AND a.datum <= :dateEnd");
                queryPartDTO.put("dateEnd", filter.getEndDateValue());
                break;
            case STATUS_AENDERUNGSDATUM:
                queryPartDTO.append(" AND a.datum >= :dateStart AND a.datum <= :dateEnd");
                queryPartDTO.put("dateStart", filter.getStartDateValue());
                queryPartDTO.put("dateEnd", filter.getEndDateValue());
                break;
            default:
                break;
        }

        List<String> statusNameList = statusFilter
                .getStatusListValue()
                .stream()
                .map(Status::getStatusBezeichnung)
                .collect(Collectors.toList());

        queryPartDTO.append(" AND a.auf IN :statusNameList");
        queryPartDTO.put("statusNameList", statusNameList);


        QueryFactory<Long> queryFactory = new QueryFactory<>();
        TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPartDTO, entityManager, Long.class);

        return query.getResultList();
    }

}
