package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.shared.objectIds.DomainObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Optional;

import static java.util.Objects.nonNull;

public class GenericDao<T extends DomainObject> implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(GenericDao.class);

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    private Class<T> type;

    @SuppressWarnings("unchecked")
    public GenericDao() {
        type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public void save(T element) {
        if (nonNull(element.getId())) {
            LOG.debug("update {}", element);
            entityManager.merge(element);
        } else {
            LOG.debug("persist {}", element);
            entityManager.persist(element);
        }
        entityManager.flush();
        entityManager.persist(element);
    }

    public Optional<T> find(DomainObject domainObject) {
        LOG.debug("find type {} by id {}", type, domainObject);
        return Optional.ofNullable(entityManager.find(type, domainObject.getId()));
    }

    public void remove(T element) {
        entityManager.remove(element);
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

}
