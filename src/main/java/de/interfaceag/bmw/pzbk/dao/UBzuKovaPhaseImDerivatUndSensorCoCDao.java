package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.UBzuKovaPhaseImDerivatUndSensorCoC;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author evda
 */
@Dependent
public class UBzuKovaPhaseImDerivatUndSensorCoCDao implements Serializable {

    private static final String KOV_A_PHASE_IM_DERIVAT = "kovAPhaseImDerivat";
    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    private UBzuKovaPhaseImDerivatUndSensorCoC getUBzuKovaPhaseImDerivatUndSensorCoCById(Long id) {
        if (id != null) {
            TypedQuery<UBzuKovaPhaseImDerivatUndSensorCoC> query = entityManager.createNamedQuery(UBzuKovaPhaseImDerivatUndSensorCoC.BY_ID, UBzuKovaPhaseImDerivatUndSensorCoC.class);
            query.setParameter("id", id);
            List<UBzuKovaPhaseImDerivatUndSensorCoC> result = query.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public void persistUBzuKovaPhaseImDerivatUndSensorCoCs(List<UBzuKovaPhaseImDerivatUndSensorCoC> ubzuKovaPhaseImDerivatUndSensorCoCs) {
        ubzuKovaPhaseImDerivatUndSensorCoCs.forEach(uBzuKovaPhaseImDerivatUndSensorCoC -> {
            if (uBzuKovaPhaseImDerivatUndSensorCoC.getId() != null) {
                entityManager.merge(uBzuKovaPhaseImDerivatUndSensorCoC);
            } else {
                entityManager.persist(uBzuKovaPhaseImDerivatUndSensorCoC);
            }
        });
        entityManager.flush();
    }

    public void persistUBzuKovaPhaseImDerivatUndSensorCoC(UBzuKovaPhaseImDerivatUndSensorCoC ub) {
        if (ub != null) {
            if (getUBzuKovaPhaseImDerivatUndSensorCoCById(ub.getId()) != null) {
                entityManager.merge(ub);
            } else {
                entityManager.persist(ub);
            }
            entityManager.flush();
        }
    }

    public void removeUBzuKovaPhaseImDerivatUndSensorCoC(UBzuKovaPhaseImDerivatUndSensorCoC ub) {
        entityManager.remove(entityManager.merge(ub));
        entityManager.flush();
    }

    public List<Mitarbeiter> getUmsetzungsbestaetigerByDerivatPhase(Derivat derivat, KovAPhase kovAPhase) {
        if (derivat != null && kovAPhase != null) {
            TypedQuery<Mitarbeiter> query = entityManager.createNamedQuery(UBzuKovaPhaseImDerivatUndSensorCoC.GET_UB_BY_DERIVAT_PHASE, Mitarbeiter.class);
            query.setParameter("derivat", derivat);

            query.setParameter("kovAPhase", kovAPhase.getId());
            List<Mitarbeiter> result = query.getResultList();

            return result != null && !result.isEmpty() ? result : Collections.emptyList();
        }
        return Collections.emptyList();
    }

    public List<UBzuKovaPhaseImDerivatUndSensorCoC> getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList(KovAPhaseImDerivat kovAPhaseImDerivat) {
        TypedQuery<UBzuKovaPhaseImDerivatUndSensorCoC> q = entityManager.createNamedQuery(UBzuKovaPhaseImDerivatUndSensorCoC.GET_BY_KOVAIMDERIVAT, UBzuKovaPhaseImDerivatUndSensorCoC.class);
        q.setParameter(KOV_A_PHASE_IM_DERIVAT, kovAPhaseImDerivat);
        return kovAPhaseImDerivat != null ? q.getResultList() : new ArrayList<>();
    }

    public Optional<UBzuKovaPhaseImDerivatUndSensorCoC> getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList(KovAPhaseImDerivat kovAPhaseImDerivat, SensorCoc sensorcoc) {

        TypedQuery<UBzuKovaPhaseImDerivatUndSensorCoC> query = entityManager.createNamedQuery(UBzuKovaPhaseImDerivatUndSensorCoC.GET_BY_KOVAIMDERIVAT_SENSORCOC, UBzuKovaPhaseImDerivatUndSensorCoC.class);
        query.setParameter(KOV_A_PHASE_IM_DERIVAT, kovAPhaseImDerivat);
        query.setParameter("sensorCoc", sensorcoc);
        List<UBzuKovaPhaseImDerivatUndSensorCoC> result = query.getResultList();
        return !result.isEmpty() ? Optional.ofNullable(query.getResultList().get(0)) : Optional.empty();

    }

    public List<SensorCoc> getSensorCocToDoListForUmsetzungsbestaetiger(Mitarbeiter user) {
        List<SensorCoc> result = new ArrayList<>();
        if (user != null) {
            TypedQuery<SensorCoc> query = entityManager.createNamedQuery(UBzuKovaPhaseImDerivatUndSensorCoC.GET_SENSORCOC_BY_MITARBEITER, SensorCoc.class);
            query.setParameter("user", user);
            query.setParameter("rolle", Rolle.UMSETZUNGSBESTAETIGER);
            query.setParameter("bereZiel", BerechtigungZiel.SENSOR_COC);
            result = query.getResultList();
        }
        return result != null ? result : new ArrayList<>();
    }

    public Collection<Long> getSensorCocIdsAsUmsetzungsverwalterForKovaPhaseImDerivat(KovAPhaseImDerivat kovAPhaseImDerivat, Mitarbeiter user) {

        if (kovAPhaseImDerivat == null || user == null) {
            return Collections.emptyList();
        }

        TypedQuery<Long> query = entityManager.createNamedQuery(UBzuKovaPhaseImDerivatUndSensorCoC.GET_SENSORCOCIDS_FOR_KOVAPHASEIMDERIVAT_AND_USER, Long.class);
        query.setParameter("user", user);
        query.setParameter(KOV_A_PHASE_IM_DERIVAT, kovAPhaseImDerivat);
        return query.getResultList();
    }

    public Optional<UBzuKovaPhaseImDerivatUndSensorCoC> getUBzuKovaPhaseImDerivatUndSensorCoCByDerivatSensorCocAndPhase(SensorCoc sensorCoc, Derivat derivat, KovAPhase kovAPhase) {
        TypedQuery<UBzuKovaPhaseImDerivatUndSensorCoC> query = entityManager.createNamedQuery(UBzuKovaPhaseImDerivatUndSensorCoC.BY_SENSORCOC_DERIVAT_PHASE, UBzuKovaPhaseImDerivatUndSensorCoC.class);
        query.setParameter("sensorCoc", sensorCoc);
        query.setParameter("derivat", derivat);
        query.setParameter("kovAPhase", kovAPhase.getId());
        List<UBzuKovaPhaseImDerivatUndSensorCoC> result = query.getResultList();
        return !result.isEmpty() ? Optional.ofNullable(query.getResultList().get(0)) : Optional.empty();

    }


}
