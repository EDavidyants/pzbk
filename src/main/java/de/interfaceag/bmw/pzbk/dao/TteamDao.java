package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Tteam;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Schauer
 */
@Dependent
public class TteamDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public void saveTteam(Tteam tteam) {
        if (tteam.getId() != null) {
            entityManager.merge(tteam);
        } else {
            entityManager.persist(tteam);
        }
        entityManager.flush();
    }

    public List<Tteam> getAllTteams() {
        return entityManager.createNamedQuery(Tteam.ALL_TTEAMS, Tteam.class).getResultList();
    }

    public List<Tteam> getAllTteamsSortByName() {
        return entityManager.createNamedQuery(Tteam.ALL_TTEAMS_SORT_BY_NAME, Tteam.class).getResultList();
    }

    public Tteam getTteamById(Long id) {
        if (id != null) {
            Query q = entityManager.createNamedQuery(Tteam.BY_ID, Tteam.class);
            q.setParameter("id", id);
            List<Tteam> teamList = q.getResultList();
            return teamList != null && !teamList.isEmpty() ? teamList.get(0) : null;
        }
        return null;
    }

    public List<Tteam> getTteamsByIdList(List<Long> idList) {
        Query q = entityManager.createNamedQuery(Tteam.BY_ID_LIST, Tteam.class);
        q.setParameter("idList", idList);
        return idList != null && !idList.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public Tteam getTteamByName(String teamname) {
        if (teamname != null) {
            Query q = entityManager.createNamedQuery(Tteam.BY_TEAMNAME, Tteam.class);
            q.setParameter("teamname", teamname);
            List<Tteam> teamList = q.getResultList();
            return teamList != null && !teamList.isEmpty() ? teamList.get(0) : null;
        }
        return null;
    }

    public List<Tteam> getTteamsByNameList(List<String> names) {
        Query q = entityManager.createNamedQuery(Tteam.BY_NAME_LIST, Tteam.class);
        q.setParameter("names", names);
        return names != null && !names.isEmpty() ? q.getResultList() : new ArrayList<>();
    }
}
