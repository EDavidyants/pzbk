package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author evda
 */
@Dependent
public class DerivatDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public void persistDerivat(Derivat derivat) {
        if (getDerivatById(derivat.getId()) != null) {
            entityManager.merge(derivat);
        } else {
            entityManager.persist(derivat);
        }
        entityManager.flush();
    }

    public Derivat getDerivatById(Long id) {
        if (id != null) {
            Query q = entityManager.createNamedQuery(Derivat.BY_ID, Derivat.class);
            q.setParameter("id", id);
            List<Derivat> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public Derivat getDerivatByName(String name) {
        if (name != null && !name.isEmpty()) {
            Query q = entityManager.createNamedQuery(Derivat.BY_NAME, Derivat.class);
            q.setParameter("name", name);
            List<Derivat> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;

    }

    public List<Long> getDerivatIds() {
        Query q = entityManager.createNamedQuery(Derivat.ALL_IDS, Derivat.class);
        return q.getResultList();
    }

    public List<Derivat> getDerivateByNameList(List<String> names) {
        Query q = entityManager.createNamedQuery(Derivat.BY_NAME_LIST, Derivat.class);
        q.setParameter("names", names);
        return names != null && !names.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<Derivat> getDerivateByIdList(List<Long> idList) {
        Query q = entityManager.createNamedQuery(Derivat.BY_ID_LIST, Derivat.class);
        q.setParameter("idList", idList);
        return idList != null && !idList.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<Derivat> getAllDerivate() {
        return entityManager.createNamedQuery(Derivat.ALL, Derivat.class).getResultList();
    }

    public List<Derivat> getAllDerivateWithZielvereinbarungFalse() {
        return entityManager.createNamedQuery(Derivat.ALL_WITH_ZIELVEREINBARUNG_FALSE, Derivat.class).getResultList();
    }

    public List<Derivat> getFilteredDerivate(String derivatName, String derivatProduktlinie) {

        QueryPartDTO queryPartDTO = new QueryPartDTO();
        queryPartDTO.append("SELECT DISTINCT d FROM Derivat d INNER JOIN FETCH d.kovAPhasen WHERE d.status != 4  AND 1 = 1 ");


        if (!"".equals(derivatName)) {
            queryPartDTO.append("AND UPPER(d.name) LIKE :name ");
            queryPartDTO.put("name", "%" + derivatName.toUpperCase() + "%");
        }

        if (!"".equals(derivatProduktlinie)) {
            queryPartDTO.append("AND UPPER(d.produktlinie) LIKE :produktlinie ");
            queryPartDTO.put("produktlinie", derivatProduktlinie.toUpperCase());
        }

        queryPartDTO.append("ORDER BY d.name");


        QueryFactory<Derivat> queryFactory = new QueryFactory<>();
        TypedQuery<Derivat> query = queryFactory.buildTypedQuery(queryPartDTO, entityManager, Derivat.class);

        return query.getResultList();

    }

    public List<Derivat> findAllExistingDerivate() {


        QueryPartDTO queryPartDTO = new QueryPartDTO("SELECT d FROM Derivat d ORDER BY d.name ASC");
        QueryFactory<Derivat> queryFactory = new QueryFactory<>();
        TypedQuery<Derivat> query = queryFactory.buildTypedQuery(queryPartDTO, entityManager, Derivat.class);

        return query.getResultList();
    }

    public List<Derivat> getAllErstanlaeufer() {
        Query q = entityManager.createNamedQuery(Derivat.ALL_ERSTANLAEUFER, Derivat.class);
        List<Derivat> result = q.getResultList();
        return (result != null) ? result : new ArrayList<>();
    }

    public List<Long> getDerivatIdsToDerivatAnforderungModulIds(Collection<Long> derivatAnforderungModulIds) {
        QueryPartDTO qp = new QueryPartDTO("SELECT DISTINCT dam.zuordnungAnforderungDerivat.derivat.id FROM DerivatAnforderungModul dam ");
        qp.append(" WHERE dam.id IN :derivatAnforderungModulIds");
        qp.put("derivatAnforderungModulIds", derivatAnforderungModulIds);

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        final TypedQuery<Long> query = queryFactory.buildTypedQuery(qp, entityManager, Long.class);
        return query.getResultList();
    }

}
