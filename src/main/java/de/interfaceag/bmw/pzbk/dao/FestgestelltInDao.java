package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sl
 */
@Dependent
public class FestgestelltInDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public void persistFestgestelltIn(FestgestelltIn festgestelltIn) {
        if (festgestelltIn.getId() != null) {
            entityManager.merge(festgestelltIn);
        } else {
            entityManager.persist(festgestelltIn);
        }
        entityManager.flush();
    }

    public void removeFestgestelltIn(FestgestelltIn festgestelltIn) {
        entityManager.remove(entityManager.merge(festgestelltIn));
        entityManager.flush();
    }

    public List<FestgestelltIn> getFestgestelltInByWert(String wert) {
        Query q = entityManager.createNamedQuery(FestgestelltIn.QUERY_BY_WERT, FestgestelltIn.class);
        q.setParameter("wert", wert);
        return wert != null && !wert.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<FestgestelltIn> getAllFestgestelltIn() {
        return entityManager.createNamedQuery(FestgestelltIn.QUERY_ALL).getResultList();
    }

    public List<FestgestelltIn> getFestgestelltInByIdList(List<Long> idList) {
        Query q = entityManager.createNamedQuery(FestgestelltIn.QUERY_BY_IDLIST, FestgestelltIn.class);
        q.setParameter("idList", idList);
        return idList != null && !idList.isEmpty() ? q.getResultList() : new ArrayList<>();
    }
}
