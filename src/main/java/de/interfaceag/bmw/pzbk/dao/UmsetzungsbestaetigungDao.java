package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Umsetzungsbestaetigung;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author evda
 */
@Dependent
public class UmsetzungsbestaetigungDao implements Serializable {

    private static final String DERIVAT = "derivat";
    private static final String ANFORDERUNG = "anforderung";
    private static final String MODUL = "modul";
    private static final String ANFORDERUNG_ID = "anforderungId";
    private static final String MODUL_ID = "modulId";
    private static final String DERIVAT_ID = "derivatId";
    private static final String KOV_A_PHASE = "kovAPhase";
    private static final String KOVA_STATUS = "kovaStatus";
    private static final String STATUS = "status";
    private static final String KOV_A_PHASE_ID = "kovAPhaseId";
    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager em;

    public void persistUmsetzungsbestaetigungen(List<Umsetzungsbestaetigung> umsetzungsbestaetigungen) {
        umsetzungsbestaetigungen.forEach(u -> {
            if (u.getId() != null) {
                em.merge(u);
            } else {
                em.persist(u);
            }
        });
        em.flush();
    }

    public void removeUmsetzungsbestaetigungen(List<Umsetzungsbestaetigung> umsetzungsbestaetigungen) {
        umsetzungsbestaetigungen.forEach(u -> em.remove(em.merge(u)));
        em.flush();
    }

    public void persistUmsetzungsbestaetigung(Umsetzungsbestaetigung ums) {
        if (getUmsetzungsbestaetigungById(ums.getId()) != null) {
            em.merge(ums);
        } else {
            em.persist(ums);
        }
        em.flush();
    }

    public void removeUmsetzungsbestaetigung(Umsetzungsbestaetigung ums) {
        em.remove(em.merge(ums));
        em.flush();
    }

    public Umsetzungsbestaetigung getUmsetzungsbestaetigungById(Long id) {
        if (id != null) {
            Query q = em.createNamedQuery(Umsetzungsbestaetigung.BY_ID, Umsetzungsbestaetigung.class);
            q.setParameter("id", id);
            List<Umsetzungsbestaetigung> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<Umsetzungsbestaetigung> getUmsetzungsbestaetigungByDerivatAnforderungModul(Derivat derivat,
                                                                                           Anforderung anforderung, Modul modul) {
        Query q = em.createNamedQuery(Umsetzungsbestaetigung.BY_DERIVAT_ANFORDERUNG_MODUL, Umsetzungsbestaetigung.class);
        q.setParameter(DERIVAT, derivat);
        q.setParameter(ANFORDERUNG, anforderung);
        q.setParameter(MODUL, modul);
        return derivat != null && anforderung != null && modul != null ? q.getResultList() : new ArrayList<>();
    }

    public List<Umsetzungsbestaetigung> getUmsetzungsbestaetigungenByAnforderungIdModulId(Long anforderungId, Long modulId) {
        Query q = em.createNamedQuery(Umsetzungsbestaetigung.BY_ANFORDERUNGID_MODULID, Umsetzungsbestaetigung.class);
        q.setParameter(ANFORDERUNG_ID, anforderungId);
        q.setParameter(MODUL_ID, modulId);
        return anforderungId != null && modulId != null ? q.getResultList() : new ArrayList<>();
    }

    public List<Umsetzungsbestaetigung> getUmsetzungsbestaetigungenByDerivatAndAnforderung(Derivat derivat, Anforderung anforderung) {
        Query q = em.createNamedQuery(Umsetzungsbestaetigung.BY_DERIVAT_ANFORDERUNG, Umsetzungsbestaetigung.class);
        q.setParameter(DERIVAT, derivat);
        q.setParameter(ANFORDERUNG, anforderung);
        return derivat != null && anforderung != null ? q.getResultList() : new ArrayList<>();
    }

    public Optional<Umsetzungsbestaetigung> getUmsetzungsbestaetigungByAnforderungModulDerivatPhase(Anforderung anforderung, Derivat derivat, Modul modul, KovAPhase kovAPhase) {
        if (anforderung != null && derivat != null && kovAPhase != null) {
            Query q = em.createNamedQuery(Umsetzungsbestaetigung.BY_ANFORDERUNG_MODUL_DERIVAT_PHASE, Umsetzungsbestaetigung.class);
            q.setParameter(ANFORDERUNG_ID, anforderung.getId());
            q.setParameter(DERIVAT_ID, derivat.getId());
            q.setParameter(MODUL_ID, modul.getId());
            q.setParameter(KOV_A_PHASE, kovAPhase.getId());
            List<Umsetzungsbestaetigung> result = q.getResultList();
            return result != null && !result.isEmpty() ? Optional.of(result.get(0)) : Optional.empty();
        }
        return Optional.empty();
    }

    public List<KovAPhaseImDerivat> getKovaphasenByKovastatusAndStatus(KovAStatus kovAStatus,
                                                                       UmsetzungsBestaetigungStatus umsetzungsBestaetigungStatus) {
        Query q = em.createNamedQuery(Umsetzungsbestaetigung.GET_KOVAPHASEIMDERIVAT_WITH_STATUS, Umsetzungsbestaetigung.class);
        q.setParameter(KOVA_STATUS, kovAStatus);
        q.setParameter(STATUS, umsetzungsBestaetigungStatus);
        return kovAStatus != null && umsetzungsBestaetigungStatus != null ? q.getResultList() : new ArrayList<>();
    }

    public List<Umsetzungsbestaetigung> getUmsetzungsbestaetigungByDerivatAndPhaseForUserSensorCoc(Derivat derivat, KovAPhase kovAPhase, List<Long> userSensorCoc) {
        if (kovAPhase == null || derivat == null || userSensorCoc == null || userSensorCoc.isEmpty()) {
            return Collections.emptyList();
        }

        Query q = em.createNamedQuery(Umsetzungsbestaetigung.GET_BY_DERIVAT_KOVAPHASE_SENSORCOC, Umsetzungsbestaetigung.class);
        q.setParameter(DERIVAT, derivat);
        q.setParameter(KOV_A_PHASE, kovAPhase.getId());
        q.setParameter("sensorCocList", userSensorCoc);

        return q.getResultList();
    }

    public List<SensorCoc> getUmsetzungsbestaetigungSensorCocForDerivatAndPhase(Derivat derivat, KovAPhase kovAPhase) {

        QueryPart queryPart = new QueryPartDTO();
        queryPart.append("SELECT DISTINCT u.anforderung.sensorCoc FROM Umsetzungsbestaetigung u ");
        queryPart.append("INNER JOIN u.kovaImDerivat k ");
        queryPart.append("WHERE k.derivat.id = :derivatId ");
        queryPart.append("AND k.kovAPhase = :kovAPhaseId ");
        queryPart.put(DERIVAT_ID, derivat.getId());
        queryPart.put(KOV_A_PHASE_ID, kovAPhase.getId());

        Query query = queryPart.buildGenericQuery(em);

        return query.getResultList();
    }

}
