package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Wert;
import de.interfaceag.bmw.pzbk.enums.Attribut;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author evda
 */
@Dependent
public class WertDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager em;

    public void persistWert(Wert wert) {
        if (wert.getId() != null) {
            em.merge(wert);
        } else {
            em.persist(wert);
        }
        em.flush();
    }

    public void removeWert(Wert wert) {
        em.remove(em.merge(wert));
        em.flush();
    }

    public List<Wert> getWertByAttribut(Attribut attribut) {
        Query q = em.createNamedQuery(Wert.QUERY_BY_ATTRIBUT, Wert.class);
        q.setParameter("a", attribut);
        return attribut != null ? q.getResultList() : new ArrayList<>();
    }

    public List<String> getWertStringByAttribut(Attribut attribut) {
        Query q = em.createNamedQuery(Wert.QUERY_ALL_WERTE_BY_ATTRIBUT, String.class);
        q.setParameter("attribut", attribut);
        return attribut != null ? (List<String>) q.getResultList() : new ArrayList<>();
    }

    public List<Wert> getWertByAttributAndWert(Attribut attribut, String wert) {
        Query q = em.createNamedQuery(Wert.QUERY_BY_ATTRIBUT_AND_WERT, Wert.class);
        q.setParameter("attribut", attribut);
        q.setParameter("wert", wert);
        return attribut != null && wert != null ? q.getResultList() : new ArrayList<>();
    }

}
