package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.LogEntry;
import de.interfaceag.bmw.pzbk.enums.SystemType;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Dependent
public class LogEntryDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager em;

    // ---------- CRUD ---------------------------------------------------------
    public void saveLogEntry(LogEntry logEntry) {
        if (logEntryExists(logEntry)) {
            em.merge(logEntry);
        } else {
            em.persist(logEntry);
        }
        em.flush();
    }

    private boolean logEntryExists(LogEntry logEntry) {
        return getLogEntryById(logEntry.getId()) != null;
    }

    // ---------- named queries ------------------------------------------------
    public LogEntry getLogEntryById(Long id) {
        Query q = em.createNamedQuery(LogEntry.BY_ID, LogEntry.class);
        q.setParameter("id", id);
        List<LogEntry> result = q.getResultList();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public List<LogEntry> getAllLogEntries() {
        return em.createNamedQuery(LogEntry.ALL, LogEntry.class).getResultList();
    }

    public List<LogEntry> getAllLogEntriesBySystemType(SystemType systemType) {
        Query q = em.createNamedQuery(LogEntry.BY_SYSTEM_TYPE, LogEntry.class);
        q.setParameter("systemType", systemType);
        return q.getResultList();
    }

    public List<LogEntry> getZAKLogEntriesForTimeRange(Date startDate, Date endDate) {
        TypedQuery q = em.createNamedQuery(LogEntry.BY_ZAKSST_TIMERANGE, LogEntry.class);
        q.setParameter("systemType", SystemType.ZAKSST);
        q.setParameter("startDate", startDate);
        q.setParameter("endDate", endDate);
        return q.getResultList();
    }
}
