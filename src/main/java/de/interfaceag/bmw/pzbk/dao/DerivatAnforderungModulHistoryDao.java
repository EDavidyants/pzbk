package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModulHistory;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author fp
 */
@Dependent
public class DerivatAnforderungModulHistoryDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager emPzbk;

    public void persist(DerivatAnforderungModulHistory dah) {
        if (getDerivatAnforderungModulHistoryById(dah.getId()) != null) {
            emPzbk.merge(dah);
        } else {
            emPzbk.persist(dah);
        }
        emPzbk.flush();
    }

    public void remove(DerivatAnforderungModulHistory dah) {
        emPzbk.remove(emPzbk.merge(dah));
        emPzbk.flush();
    }

    public DerivatAnforderungModulHistory getDerivatAnforderungModulHistoryById(Long id) {
        if (id != null) {
            Query q = emPzbk.createNamedQuery(DerivatAnforderungModulHistory.BY_ID, DerivatAnforderungModulHistory.class);
            q.setParameter("id", id);
            List<DerivatAnforderungModulHistory> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<DerivatAnforderungModulHistory> getDerivatAnforderungModulHistoryByDerivatAnforderungModulId(Long derivatAnforderungModulId) {
        Query q = emPzbk.createNamedQuery(DerivatAnforderungModulHistory.BY_DERIVATANFORDERUNGMODUL_ID, DerivatAnforderungModulHistory.class);
        q.setParameter("id", derivatAnforderungModulId);
        return derivatAnforderungModulId != null ? q.getResultList() : new ArrayList<>();
    }

    public List<DerivatAnforderungModulHistory> getDerivatAnforderungModulHistoryByDerivatAnforderungModul(DerivatAnforderungModul da) {
        Query q = emPzbk.createNamedQuery(DerivatAnforderungModulHistory.BY_DERIVATANFORDERUNGMODUL, DerivatAnforderungModulHistory.class);
        q.setParameter("da", da);
        return da != null ? q.getResultList() : new ArrayList<>();
    }

    public Optional<DerivatAnforderungModulHistory> getLatestDerivatAnforderungModulHistoryByDerivatAnforderungModul(DerivatAnforderungModul da) {
        Query q = emPzbk.createNamedQuery(DerivatAnforderungModulHistory.BY_DERIVATANFORDERUNGMODUL, DerivatAnforderungModulHistory.class);
        q.setParameter("da", da);
        List<DerivatAnforderungModulHistory> result = da != null ? q.getResultList() : new ArrayList<>();
        return !result.isEmpty() ? Optional.ofNullable(result.get(0)) : Optional.empty();
    }

}
