package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author evda
 */
@Dependent
public class AnforderungFreigabeBeiModuSeTeamDao implements Serializable {

    public static final String ANFORDERUNG = "anforderung";
    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager em;

    private AnforderungFreigabeBeiModulSeTeam getAnfoFreigabeById(Long id) {
        if (id != null) {
            Query q = em.createNamedQuery(AnforderungFreigabeBeiModulSeTeam.BY_ID, AnforderungFreigabeBeiModulSeTeam.class);
            q.setParameter("id", id);
            List<AnforderungFreigabeBeiModulSeTeam> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public void persistAnforderungFreigabeBeiModul(AnforderungFreigabeBeiModulSeTeam anforderungFreigabeBeiModulSeTeam) {
        if (getAnfoFreigabeById(anforderungFreigabeBeiModulSeTeam.getId()) != null) {
            em.merge(anforderungFreigabeBeiModulSeTeam);
        } else {
            em.persist(anforderungFreigabeBeiModulSeTeam);
        }
        em.flush();
    }

    public AnforderungFreigabeBeiModulSeTeam getAnfoFreigabeByAnforderungAndModulSeTeam(Anforderung anforderung, ModulSeTeam modulSeTeam) {
        if (anforderung != null && modulSeTeam != null) {
            Query q = em.createNamedQuery(AnforderungFreigabeBeiModulSeTeam.BY_ANFORDERUNG_AND_MODULSETEAM, AnforderungFreigabeBeiModulSeTeam.class);
            q.setParameter(ANFORDERUNG, anforderung);
            q.setParameter("modul", modulSeTeam);
            List<AnforderungFreigabeBeiModulSeTeam> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<AnforderungFreigabeBeiModulSeTeam> getAnfoFreigabeByAnforderung(Anforderung anforderung) {
        Query q = em.createNamedQuery(AnforderungFreigabeBeiModulSeTeam.BY_ANFORDERUNG, AnforderungFreigabeBeiModulSeTeam.class);
        q.setParameter(ANFORDERUNG, anforderung);
        return anforderung != null ? q.getResultList() : new ArrayList<>();
    }

    public int countAllModulSeTeamsForAnforderung(Anforderung anforderung) {
        Query q = em.createNamedQuery(AnforderungFreigabeBeiModulSeTeam.COUNT_ALL_MODULE_FOR_ANFORDERUNG);
        q.setParameter(ANFORDERUNG, anforderung);
        return anforderung != null ? ((Long) q.getSingleResult()).intValue() : 0;
    }

    public Integer countFreigegebeneModulSeTeamsForAnforderung(Anforderung anforderung) {
        Query q = em.createNamedQuery(AnforderungFreigabeBeiModulSeTeam.COUNT_FREIGEGEBENE_MODULE_FOR_ANFORDERUNG);
        q.setParameter(ANFORDERUNG, anforderung);
        return anforderung != null ? ((Long) q.getSingleResult()).intValue() : 0;
    }

    public List<AnforderungFreigabeBeiModulSeTeam> getFreigegebeneModulSeTeamsForAnforderung(Anforderung anforderung) {
        Query q = em.createNamedQuery(AnforderungFreigabeBeiModulSeTeam.GET_FREIGEGEBENE_MODULE_FOR_ANFORDERUNG);
        q.setParameter(ANFORDERUNG, anforderung);
        return anforderung != null ? q.getResultList() : new ArrayList<>();
    }
}
