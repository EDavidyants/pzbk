package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.UrlEncoding;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Dependent
public class UrlEncodingDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager em;

    public void persistUrlEncoding(UrlEncoding urlEncoding) {
        if (urlEncoding.getId() != null) {
            em.merge(urlEncoding);
        } else {
            em.persist(urlEncoding);
        }
        em.flush();
    }

    public UrlEncoding getUrlEncodingByUrlId(String urlId) {
        if (urlId != null && !urlId.isEmpty()) {
            Query q = em.createNamedQuery(UrlEncoding.BY_URLID, UrlEncoding.class);
            q.setParameter("urlId", urlId);
            List<UrlEncoding> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

}
