package de.interfaceag.bmw.pzbk.dao;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Stefan Luchs sl
 */
@Dependent
public class BildDao implements Serializable {

    private static final String SELECT = "SELECT ";
    private static final String B_LARGE_IMAGE = "b.largeImage";
    private static final String B_NORMAL_IMAGE = "b.normalImage";

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager em;

    public byte[] getImageFromAnforderungBildByObjectId(Long objectId, Boolean largeImage, String type) {

        StringBuilder sb = new StringBuilder();

        sb.append(SELECT);
        if (largeImage) {
            sb.append(B_LARGE_IMAGE);
        } else {
            sb.append(B_NORMAL_IMAGE);
        }
        sb.append(" FROM ");
        switch (type) {
            case "A":
                sb.append("Anforderung ");
                break;
            case "M":
                sb.append("Meldung ");
                break;
            case "P":
                sb.append("Pzbk ");
                break;
            default:
                break;
        }
        sb.append("a INNER JOIN a.bild AS b WHERE a.id = :id");
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("id", objectId);

        TypedQuery q = em.createQuery(sb.toString(), Object.class);
        parameterMap.forEach(q::setParameter);
        List<Object> result = q.getResultList();
        if (result != null && !result.isEmpty()) {
            return (byte[]) q.getResultList().get(0);
        }
        return new byte[0];
    }

    public byte[] getProzessbaukastenKonzeptbaumBild(String fachId, int version, Boolean largeImage) {

        StringBuilder sb = new StringBuilder();

        sb.append(SELECT);
        if (largeImage) {
            sb.append(B_LARGE_IMAGE);
        } else {
            sb.append(B_NORMAL_IMAGE);
        }
        sb.append(" FROM Prozessbaukasten p");
        sb.append(" INNER JOIN p.konzeptbaumBild AS b "
                + "WHERE p.fachId = :fachId AND p.version = :version");
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("fachId", fachId);
        parameterMap.put("version", version);

        TypedQuery q = em.createQuery(sb.toString(), Object.class);
        parameterMap.forEach(q::setParameter);
        List<Object> result = q.getResultList();
        if (result != null && !result.isEmpty()) {
            return (byte[]) q.getResultList().get(0);
        }
        return new byte[0];
    }

    public byte[] getProzessbaukastenGrafikUmfangBild(String fachId, int version, Boolean largeImage) {

        StringBuilder sb = new StringBuilder();

        sb.append(SELECT);
        if (largeImage) {
            sb.append(B_LARGE_IMAGE);
        } else {
            sb.append(B_NORMAL_IMAGE);
        }
        sb.append(" FROM Prozessbaukasten p");
        sb.append(" INNER JOIN p.grafikUmfangBild AS b "
                + "WHERE p.fachId = :fachId AND p.version = :version");
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("fachId", fachId);
        parameterMap.put("version", version);

        TypedQuery q = em.createQuery(sb.toString(), Object.class);
        parameterMap.forEach(q::setParameter);
        List<Object> result = q.getResultList();
        if (result != null && !result.isEmpty()) {
            return (byte[]) q.getResultList().get(0);
        }
        return new byte[0];
    }

    public byte[] getDefaultImageForModulByAnforderungId(Long anforderungId, Boolean largeImage) {
        Map<String, Object> parameterMap = new HashMap<>();
        StringBuilder sb = new StringBuilder();

        sb.append("SELECT b");

        if (largeImage) {
            sb.append(".largeImage");
        } else {
            sb.append(".normalImage");
        }

        sb.append(" FROM AnforderungFreigabeBeiModulSeTeam am "
                + "INNER JOIN am.modulSeTeam AS se "
                + "INNER JOIN se.bild AS b "
                + "WHERE am.anforderung.id = :id "
                + "ORDER BY b.id");
        parameterMap.put("id", anforderungId);

        TypedQuery q = em.createQuery(sb.toString(), Object.class);
        parameterMap.forEach(q::setParameter);
        List<Object> result = q.getResultList();
        if (result != null && !result.isEmpty()) {
            return (byte[]) q.getResultList().get(0);
        }
        return new byte[0];
    }

}
