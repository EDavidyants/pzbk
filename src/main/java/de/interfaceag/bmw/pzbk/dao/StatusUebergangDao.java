package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.dashboard.SencorCocIdKovaPhaseImDerivatIdTuple;
import de.interfaceag.bmw.pzbk.dashboard.SencorCocIdKovaPhaseImDerivatIdTuples;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author evda
 */
@Dependent
public class StatusUebergangDao implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(StatusUebergangDao.class.getName());
    public static final String STATUS_LIST = "statusList";
    public static final String USER = "user";
    public static final String SENSOR_COC_IDS = "sensorCocIds";
    public static final String TTEAM_IDS = "tteamIds";
    public static final String MODUL_SE_TEAM_IDS = "modulSeTeamIds";
    public static final String ZAK_EINORDNUNGEN = "zakEinordnungen";
    public static final String SELECT_DISTINCT_A_ID_FROM_ANFORDERUNG_A = "SELECT DISTINCT a.id FROM Anforderung a ";
    public static final String WHERE_A_STATUS_IN_STATUS_LIST = "WHERE a.status IN :statusList ";
    public static final String AND_A_SENSOR_COC_SENSOR_COC_ID_IN_SENSOR_COC_IDS = "AND a.sensorCoc.sensorCocId IN :sensorCocIds ";
    public static final String WHERE_D_STATUS_ZV_IN_DERIVAT_ANFORDERUNG_MODUL_STATUS_LIST = "WHERE d.statusZV IN :derivatAnforderungModulStatusList ";
    public static final String DERIVAT_ANFORDERUNG_MODUL_STATUS_LIST = "derivatAnforderungModulStatusList";

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    // ---------- dashboard queries ----------------------------
    public List<Long> getMeldungenForSensorInStatus(List<Status> statusList, Mitarbeiter user, Collection<Long> sensorCocIds) {
        if (sensorCocIds == null || sensorCocIds.isEmpty()) {
            return new ArrayList<>();
        }

        String query = "SELECT DISTINCT m.id FROM Meldung m "
                + "WHERE m.status IN :statusList "
                + "AND m.sensor = :user "
                + "AND m.sensorCoc.sensorCocId IN :sensorCocIds ";

        QueryPartDTO qp = new QueryPartDTO(query);

        qp.put(STATUS_LIST, statusList);
        qp.put(USER, user);
        qp.put(SENSOR_COC_IDS, sensorCocIds);

        return getQueryResultsAsLongList(qp);
    }

    public List<Long> getMeldungenForSensorCocLeiterInStatus(List<Status> statusList, Collection<Long> sensorCocIds) {
        if (sensorCocIds == null || sensorCocIds.isEmpty()) {
            return new ArrayList<>();
        }

        String query = "SELECT DISTINCT m.id FROM Meldung m "
                + "WHERE m.status IN :statusList "
                + "AND m.sensorCoc.sensorCocId IN :sensorCocIds ";

        QueryPartDTO qp = new QueryPartDTO(query);

        qp.put(STATUS_LIST, statusList);
        qp.put(SENSOR_COC_IDS, sensorCocIds);

        return getQueryResultsAsLongList(qp);
    }

    public List<Long> getAnforderungenForSensorInStatus(List<Status> statusList, Mitarbeiter user, Collection<Long> sensorCocIds) {
        if (sensorCocIds == null || sensorCocIds.isEmpty()) {
            return new ArrayList<>();
        }

        String query = SELECT_DISTINCT_A_ID_FROM_ANFORDERUNG_A
                + WHERE_A_STATUS_IN_STATUS_LIST
                + AND_A_SENSOR_COC_SENSOR_COC_ID_IN_SENSOR_COC_IDS
                + "AND a.sensor = :user ";

        QueryPartDTO qp = new QueryPartDTO(query);

        qp.put(STATUS_LIST, statusList);
        qp.put(USER, user);
        qp.put(SENSOR_COC_IDS, sensorCocIds);

        return getQueryResultsAsLongList(qp);
    }

    public List<Long> getAnforderungenForSensorCocLeiterInStatus(List<Status> statusList, Collection<Long> sensorCocIds) {
        if (sensorCocIds == null || sensorCocIds.isEmpty()) {
            return new ArrayList<>();
        }

        String query = SELECT_DISTINCT_A_ID_FROM_ANFORDERUNG_A
                + WHERE_A_STATUS_IN_STATUS_LIST
                + AND_A_SENSOR_COC_SENSOR_COC_ID_IN_SENSOR_COC_IDS;

        QueryPartDTO qp = new QueryPartDTO(query);

        qp.put(STATUS_LIST, statusList);
        qp.put(SENSOR_COC_IDS, sensorCocIds);

        return getQueryResultsAsLongList(qp);
    }

    public List<Long> getAnforderungenForTteamLeiterInStatus(List<Status> statusList, Collection<Long> tteamIds) {
        if (tteamIds == null || tteamIds.isEmpty()) {
            return new ArrayList<>();
        }

        String query = SELECT_DISTINCT_A_ID_FROM_ANFORDERUNG_A
                + WHERE_A_STATUS_IN_STATUS_LIST
                + "AND a.tteam IN :tteamIds ";

        QueryPartDTO qp = new QueryPartDTO(query);

        qp.put(STATUS_LIST, statusList);
        qp.put(TTEAM_IDS, tteamIds);

        return getQueryResultsAsLongList(qp);
    }

    public List<Long> getAnforderungenForEcocInStatus(List<Status> statusList, Collection<Long> modulSeTeamIds) {
        if (modulSeTeamIds == null || modulSeTeamIds.isEmpty()) {
            return new ArrayList<>();
        }

        String query = SELECT_DISTINCT_A_ID_FROM_ANFORDERUNG_A
                + "INNER JOIN FETCH a.umsetzer u "
                + WHERE_A_STATUS_IN_STATUS_LIST
                + "AND u.seTeam.id IN :modulSeTeamIds ";

        QueryPartDTO qp = new QueryPartDTO(query);

        qp.put(STATUS_LIST, statusList);
        qp.put(MODUL_SE_TEAM_IDS, modulSeTeamIds);

        return getQueryResultsAsLongList(qp);
    }

    public List<Long> getAnforderungenForAnfordererInStatus(List<Status> statusList, Collection<String> zakEinordnungen) {
        if (zakEinordnungen == null || zakEinordnungen.isEmpty()) {
            return new ArrayList<>();
        }

        String query = SELECT_DISTINCT_A_ID_FROM_ANFORDERUNG_A
                + WHERE_A_STATUS_IN_STATUS_LIST
                + "AND a.sensorCoc.zakEinordnung IN :zakEinordnungen ";

        QueryPartDTO qp = new QueryPartDTO(query);

        qp.put(STATUS_LIST, statusList);
        qp.put(ZAK_EINORDNUNGEN, zakEinordnungen);

        return getQueryResultsAsLongList(qp);
    }

    public List<Long> processStepFive(Collection<Long> sensorCocIds) {

        if (sensorCocIds == null || sensorCocIds.isEmpty()) {
            return new ArrayList<>();
        }

        Status anforderungStatus = Status.A_FREIGEGEBEN;
        Integer derivatAnforderungModulStatus = DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId();

        String query = SELECT_DISTINCT_A_ID_FROM_ANFORDERUNG_A
                + "INNER JOIN ZuordnungAnforderungDerivat z ON z.anforderung = a "
                + "INNER JOIN DerivatAnforderungModul dam ON dam.zuordnungAnforderungDerivat = z "
                + "WHERE a.status = :anforderungStatus "
                + "AND dam.statusZV = :derivatAnforderungModulStatus "
                + AND_A_SENSOR_COC_SENSOR_COC_ID_IN_SENSOR_COC_IDS;

        QueryPartDTO qp = new QueryPartDTO(query);

        qp.put("anforderungStatus", anforderungStatus);
        qp.put("derivatAnforderungModulStatus", derivatAnforderungModulStatus);
        qp.put(SENSOR_COC_IDS, sensorCocIds);

        return getQueryResultsAsLongList(qp);
    }

    public List<Long> processStepSix(Collection<Long> sensorCocIds) {

        if (sensorCocIds == null || sensorCocIds.isEmpty()) {
            return new ArrayList<>();
        }

        Status anforderungStatus = Status.A_FREIGEGEBEN;
        UmsetzungsBestaetigungStatus umsetzungsBestaetigungStatus = UmsetzungsBestaetigungStatus.UMGESETZT;

        String query = SELECT_DISTINCT_A_ID_FROM_ANFORDERUNG_A
                + " LEFT OUTER JOIN Umsetzungsbestaetigung u ON u.anforderung = a "
                + " WHERE a.status = :anforderungStatus "
                + " AND u.status = :umsetzungsBestaetigungStatus "
                + " AND a.sensorCoc.sensorCocId IN :sensorCocIds ";

        QueryPartDTO qp = new QueryPartDTO(query);

        qp.put("anforderungStatus", anforderungStatus);
        qp.put("umsetzungsBestaetigungStatus", umsetzungsBestaetigungStatus);
        qp.put(SENSOR_COC_IDS, sensorCocIds);

        return getQueryResultsAsLongList(qp);
    }

    public int processStepEight(Collection<Long> sensorCocIds) {

        if (sensorCocIds == null || sensorCocIds.isEmpty()) {
            return 0;
        }

        KovAStatus kovaStatus = KovAStatus.KONFIGURIERT;
        int derivatAnforderungModulStatusId = DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId();

        String query = "SELECT DISTINCT dam.zuordnungAnforderungDerivat.anforderung.sensorCoc.sensorCocId, k.id "
                + "FROM DerivatAnforderungModul dam INNER JOIN KovAPhaseImDerivat k ON dam.zuordnungAnforderungDerivat.derivat = k.derivat "
                + "WHERE k.kovaStatus = :kovaStatus "
                + "AND dam.statusZV = :derivatAnforderungModulStatusId "
                + "AND dam.zuordnungAnforderungDerivat.anforderung.sensorCoc.sensorCocId IN :sensorCocIds "
                + "AND dam.zuordnungAnforderungDerivat.anforderung.sensorCoc NOT IN "
                + "(SELECT ub.sensorCoc FROM UBzuKovaPhaseImDerivatUndSensorCoC ub WHERE ub.kovaImDerivat = k) ";

        QueryPartDTO qp = new QueryPartDTO(query);

        qp.put("kovaStatus", kovaStatus);
        qp.put(SENSOR_COC_IDS, sensorCocIds);
        qp.put("derivatAnforderungModulStatusId", derivatAnforderungModulStatusId);

        Query q = qp.buildGenericQuery(entityManager);

        List<Object[]> idTuples = q.getResultList();

        LOG.debug("Dashboard Step 8 Ergebnisse: {}", idTuples.size());
        LOG.debug("Dashboard Step 8 Ergebnisse: {}", idTuples);
        return idTuples.size();
    }

    private SencorCocIdKovaPhaseImDerivatIdTuples getSensorCocIdsAndKovaPhaseImDerivatIdsForUmsetzungsbestaetiger(Mitarbeiter user) {

        QueryPart queryPart = new QueryPartDTO();

        queryPart.append("SELECT DISTINCT ub.sensorCoc.sensorCocId, kov.id FROM UBzuKovaPhaseImDerivatUndSensorCoC ub ");
        queryPart.append("INNER JOIN ub.umsetzungsbestaetiger ubm ");
        queryPart.append("INNER JOIN ub.kovaImDerivat kov ");
        queryPart.append("WHERE ubm.id = :userId ");
        queryPart.append("AND kov.kovaStatus = :kovaStatus ");

        queryPart.put("userId", user.getId());
        queryPart.put("kovaStatus", KovAStatus.AKTIV);

        Query query = queryPart.buildGenericQuery(entityManager);
        List<Object[]> result = query.getResultList();

        return convertToSencorCocIdKovaPhaseImDerivatIdTuples(result);
    }

    private SencorCocIdKovaPhaseImDerivatIdTuples convertToSencorCocIdKovaPhaseImDerivatIdTuples(List<Object[]> result) {
        if (result.isEmpty()) {
            throw new IllegalArgumentException("getSensorCocIdsAndKovaPhaseImDerivatIdsForUmsetzungsbestaetiger -> result is empty");
        }
        List<SencorCocIdKovaPhaseImDerivatIdTuple> returnValue = result.stream().map(ob -> new SencorCocIdKovaPhaseImDerivatIdTuple((Long) ob[0], (Long) ob[1])).collect(Collectors.toList());
        return new SencorCocIdKovaPhaseImDerivatIdTuples(returnValue);
    }

    private Long getAnforderungModulTupleFromDerivatAnforderungModulForVKBG(SencorCocIdKovaPhaseImDerivatIdTuples sensorCocKovAPhaseImDerivatTuple) {

        QueryPart queryPart = getAnforderungModulTupleBaseQuery(sensorCocKovAPhaseImDerivatTuple);
        queryPart.append("AND dam.statusVKBG = 3 ");
        queryPart.append("AND k.kovaphase IN (1,2,3,4,5) ");

        return getCountResult(queryPart);

    }

    private Long getAnforderungModulTupleFromDerivatAnforderungModulForZV(SencorCocIdKovaPhaseImDerivatIdTuples sensorCocKovAPhaseImDerivatTuple) {

        QueryPart queryPart = getAnforderungModulTupleBaseQuery(sensorCocKovAPhaseImDerivatTuple);
        queryPart.append("AND dam.statusZV = 3 ");
        queryPart.append("AND k.kovaphase IN (6,7,8) ");
        return getCountResult(queryPart);

    }

    private QueryPart getAnforderungModulTupleBaseQuery(SencorCocIdKovaPhaseImDerivatIdTuples sensorCocKovAPhaseImDerivatTuple) {
        QueryPart queryPart = new QueryPartDTO();
        queryPart.append("SELECT count(*) FROM derivatanforderungmodul dam ");
        queryPart.append("INNER JOIN zuordnunganforderungderivat z ON dam.zuordnunganforderungderivat_id = z.id ");
        queryPart.append("INNER JOIN anforderung a ON z.anforderung_id = a.id ");
        queryPart.append("INNER JOIN sensorcoc sc ON sc.sensorcocid = a.sensorcoc_sensorcocid ");
        queryPart.append("INNER JOIN derivat d ON z.derivat_id = d.id ");
        queryPart.append("INNER JOIN kov_a_phase_im_derivat k ON z.derivat_id = k.derivat_id ");

        queryPart.append("WHERE (sc.sensorcocid, k.id) IN ");
        String tuplesForQuery = sensorCocKovAPhaseImDerivatTuple.getTuplesForQuery();
        queryPart.append(tuplesForQuery).append(" ");

        return queryPart;
    }

    private Long getAnforderungModulTupleFromUmsetzungsbestaetigung(SencorCocIdKovaPhaseImDerivatIdTuples sensorCocKovAPhaseImDerivatTuple) {

        QueryPart queryPart = new QueryPartDTO();

        queryPart.append("SELECT count(*) FROM umsetzungsbestaetigung ub ");
        queryPart.append("INNER JOIN anforderung a ON ub.anforderung_id = a.id ");
        queryPart.append("INNER JOIN sensorcoc sc ON sc.sensorcocid = a.sensorcoc_sensorcocid ");
        queryPart.append("INNER JOIN kov_a_phase_im_derivat k ON ub.kovaimderivat_id = k.id ");
        queryPart.append("WHERE (sc.sensorcocid, k.id) IN ");
        String tuplesForQuery = sensorCocKovAPhaseImDerivatTuple.getTuplesForQuery();
        queryPart.append(tuplesForQuery).append(" ");
        queryPart.append("AND ub.status IN (1,2,3,4,5,6,7,8)");

        return getCountResult(queryPart);
    }

    private Long getCountResult(QueryPart queryPart) {
        Query query = queryPart.buildNativeQuery(entityManager);
        return (Long) query.getSingleResult();
    }

    public int processStepNine(Mitarbeiter user, Collection<Long> sensorCocIds) {

        try {
            validateImputForStepNine(sensorCocIds);

            SencorCocIdKovaPhaseImDerivatIdTuples sensorCocKovAPhaseImDerivatTuple = getSensorCocIdsAndKovaPhaseImDerivatIdsForUmsetzungsbestaetiger(user);

            Long anforderungModulTuplesVKBG = getAnforderungModulTupleFromDerivatAnforderungModulForVKBG(sensorCocKovAPhaseImDerivatTuple);
            Long anforderungModulTuplesZV = getAnforderungModulTupleFromDerivatAnforderungModulForZV(sensorCocKovAPhaseImDerivatTuple);

            Long anforderungModulTuplesUmsetzungsbestaetigung = getAnforderungModulTupleFromUmsetzungsbestaetigung(sensorCocKovAPhaseImDerivatTuple);

            Long anforderungModulTuples = anforderungModulTuplesVKBG + anforderungModulTuplesZV - anforderungModulTuplesUmsetzungsbestaetigung;

            return Math.max(anforderungModulTuples.intValue(), 0);
        } catch (IllegalArgumentException iae) {
            LOG.debug(iae.getMessage());
            return 0;
        }
    }

    private void validateImputForStepNine(Collection<Long> sensorCocIds) {
        if (sensorCocIds == null || sensorCocIds.isEmpty()) {
            throw new IllegalArgumentException("processStepNine -> sensorCocIds null or empty");
        }
    }

    public List<Long> processStepTen(Collection<Long> tteamIds) {

        if (tteamIds == null || tteamIds.isEmpty()) {
            return new ArrayList<>();
        }

        List<Integer> derivatAnforderungModulStatusList = new ArrayList<>();
        derivatAnforderungModulStatusList.add(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET.getStatusId());

        QueryPartDTO qp = new QueryPartDTO("SELECT DISTINCT d.zuordnungAnforderungDerivat.anforderung.id FROM DerivatAnforderungModul d "
                + "WHERE d.zuordnungAnforderungDerivat.zurKenntnisGenommen = FALSE "
                + "AND d.statusZV IN :derivatAnforderungModulStatusList ");

        qp.put(DERIVAT_ANFORDERUNG_MODUL_STATUS_LIST, derivatAnforderungModulStatusList);
        qp.append("AND d.zuordnungAnforderungDerivat.anforderung.tteam.id IN :tteamIds ");
        qp.put(TTEAM_IDS, tteamIds);

        return getQueryResultsAsLongList(qp);
    }

    public List<Long> getUmsetzungInZAKForTteamleiter(Collection<Long> tteamIds, List<ZakStatus> zakStatus) {

        if (tteamIds == null || tteamIds.isEmpty()) {
            return new ArrayList<>();
        }

        List<Integer> derivatAnforderungModulStatusList = new ArrayList<>();
        derivatAnforderungModulStatusList.add(DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId());

        QueryPartDTO qp = new QueryPartDTO("SELECT DISTINCT d.id "
                + "FROM ZakUebertragung zak INNER JOIN zak.derivatAnforderungModul d "
                + WHERE_D_STATUS_ZV_IN_DERIVAT_ANFORDERUNG_MODUL_STATUS_LIST);

        qp.put(DERIVAT_ANFORDERUNG_MODUL_STATUS_LIST, derivatAnforderungModulStatusList);

        if (zakStatus != null) {
            qp.append("AND d.zakStatus IN :zakStatusList ");
            qp.put("zakStatusList", zakStatus);
        }

        qp.append("AND d.zuordnungAnforderungDerivat.anforderung.tteam.id IN :tteamIds ");
        qp.put(TTEAM_IDS, tteamIds);

        return getQueryResultsAsLongList(qp);
    }

    public List<Long> getUmsetzungInZAKForAnfordererByUBStatus(Collection<String> zakEinordnungen, Collection<Long> derivatIds, List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {

        if (zakEinordnungen == null || zakEinordnungen.isEmpty()) {
            return new ArrayList<>();
        }

        List<Integer> derivatAnforderungModulStatusList = new ArrayList<>();
        derivatAnforderungModulStatusList.add(DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId());

        QueryPartDTO qp = new QueryPartDTO("SELECT DISTINCT d.id FROM DerivatAnforderungModul d "
                + WHERE_D_STATUS_ZV_IN_DERIVAT_ANFORDERUNG_MODUL_STATUS_LIST);

        qp.put(DERIVAT_ANFORDERUNG_MODUL_STATUS_LIST, derivatAnforderungModulStatusList);

        if (umsetzungsBestaetigungStatus != null) {
            qp.append(" AND d.umsetzungsBestaetigungStatus IN :umsetzungsBestaetigungStatusList ");
            qp.put("umsetzungsBestaetigungStatusList", umsetzungsBestaetigungStatus);
        }

        qp.append("AND d.zuordnungAnforderungDerivat.anforderung.sensorCoc.zakEinordnung IN :zakEinordnungen ");
        qp.put(ZAK_EINORDNUNGEN, zakEinordnungen);

        qp.append("AND d.zuordnungAnforderungDerivat.derivat.id IN :derivatIds ");
        qp.put("derivatIds", derivatIds);

        return getQueryResultsAsLongList(qp);
    }

    public List<Long> getUmsetzungInZAKForAnfordererByZakStatus(Collection<String> zakEinordnungen, Collection<Long> derivatIds, List<ZakStatus> zakStatus) {

        if (zakEinordnungen == null || zakEinordnungen.isEmpty()) {
            return new ArrayList<>();
        }

        List<Integer> derivatAnforderungModulStatusList = new ArrayList<>();
        derivatAnforderungModulStatusList.add(DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId());

        QueryPartDTO qp = new QueryPartDTO("SELECT DISTINCT d.id "
                + "FROM ZakUebertragung zak INNER JOIN zak.derivatAnforderungModul d "
                + WHERE_D_STATUS_ZV_IN_DERIVAT_ANFORDERUNG_MODUL_STATUS_LIST);

        qp.put(DERIVAT_ANFORDERUNG_MODUL_STATUS_LIST, derivatAnforderungModulStatusList);

        if (zakStatus != null) {
            qp.append(" AND zak.zakStatus IN :zakStatusList ");
            qp.put("zakStatusList", zakStatus);
        }

        qp.append("AND d.zuordnungAnforderungDerivat.anforderung.sensorCoc.zakEinordnung IN :zakEinordnungen ");
        qp.put(ZAK_EINORDNUNGEN, zakEinordnungen);

        qp.append("AND d.zuordnungAnforderungDerivat.derivat.id IN :derivatIds ");
        qp.put("derivatIds", derivatIds);

        return getQueryResultsAsLongList(qp);
    }

    private List<Long> getQueryResultsAsLongList(QueryPartDTO qp) {
        Query q = entityManager.createQuery(qp.getQuery(), Long.class);
        qp.getParameterMap().entrySet().forEach(p -> q.setParameter(p.getKey(), p.getValue()));
        return q.getResultList();
    }

}
