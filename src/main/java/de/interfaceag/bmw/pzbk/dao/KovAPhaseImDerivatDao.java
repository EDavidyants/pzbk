package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author evda
 */
@Dependent
public class KovAPhaseImDerivatDao implements Serializable {

    private static final String STATUS = "status";
    private static final String AND_D_ZUORDNUNG_ANFORDERUNG_DERIVAT_DERIVAT_ID_DERIVATID = "AND d.zuordnungAnforderungDerivat.derivat.id = :derivatid ";
    private static final String AND_D_ZUORDNUNG_ANFORDERUNG_DERIVAT_ANFORDERUNG_SENSOR_COC_SENSOR_COC_ID_IN_SENSOR_COC_ID_LIST = "AND d.zuordnungAnforderungDerivat.anforderung.sensorCoc.sensorCocId IN :sensorCocIdList ";
    private static final String SENSOR_COC_ID_LIST = "sensorCocIdList";
    private static final String DERIVATID = "derivatid";

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public void persistKovAPhaseImDerivat(KovAPhaseImDerivat kova) {
        if (getKovAPhaseImDerivatById(kova.getId()) != null) {
            entityManager.merge(kova);
        } else {
            entityManager.persist(kova);
        }
        entityManager.flush();
    }

    public KovAPhaseImDerivat getKovAPhaseImDerivatById(Long id) {
        if (id != null) {
            Query q = entityManager.createNamedQuery(KovAPhaseImDerivat.BY_ID, KovAPhaseImDerivat.class);
            q.setParameter("id", id);
            List<KovAPhaseImDerivat> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public Optional<KovAPhaseImDerivat> getKovAPhaseImDerivatByIdFetchDerivat(Long id) {
        if (id != null) {
            Query q = entityManager.createNamedQuery(KovAPhaseImDerivat.BY_ID_FETCH, KovAPhaseImDerivat.class);
            q.setParameter("id", id);
            List<KovAPhaseImDerivat> result = q.getResultList();
            return result != null && !result.isEmpty() ? Optional.of(result.get(0)) : Optional.empty();
        } else {
            return Optional.empty();
        }
    }

    public List<KovAPhaseImDerivat> getKovAPhaseImDerivatWithStartAndEndSet() {
        return entityManager.createNamedQuery(KovAPhaseImDerivat.START_AND_END_SET, KovAPhaseImDerivat.class).getResultList();
    }

    public List<KovAPhaseImDerivat> getKovAPhasenImDerivatByDerivatFetch(Derivat derivat) {
        Query q = entityManager.createNamedQuery(KovAPhaseImDerivat.BY_DERIVAT_FETCH, KovAPhaseImDerivat.class);
        q.setParameter("derivat", derivat);
        return derivat != null ? q.getResultList() : new ArrayList<>();
    }

    public KovAPhaseImDerivat getKovAPhaseImDerivatByDerivatAndPhase(Derivat derivat, KovAPhase kovAPhase) {
        if (derivat != null && kovAPhase != null) {
            Query q = entityManager.createNamedQuery(KovAPhaseImDerivat.BY_DERIVAT_PHASE, KovAPhaseImDerivat.class);
            q.setParameter("derivat", derivat);
            q.setParameter("kovAPhase", kovAPhase.getId());
            List<KovAPhaseImDerivat> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<String> getDerivatNamenWithEndDatumOfPhase(KovAPhase phase, LocalDate endDatum) {

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<KovAPhaseImDerivat> cq = cb.createQuery(KovAPhaseImDerivat.class);
        Root<KovAPhaseImDerivat> root = cq.from(KovAPhaseImDerivat.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition;

        if (phase != null) {
            whereCondition = cb.equal(root.get("kovAPhase"), phase.getId());
            whereConditions.add(whereCondition);
        }

        if (endDatum != null) {
            whereCondition = cb.equal(root.get("endDate"), endDatum);
            whereConditions.add(whereCondition);
        }

        //Constructing ORDER BY Clause
        List<Order> orderList = new ArrayList<>();
        orderList.add(cb.asc(root.get("id")));

        cq.select(root)
                .where(whereConditions.toArray(new Predicate[] {}))
                .orderBy(orderList)
                .distinct(true);
        List<String> result = new ArrayList<>();
        List<KovAPhaseImDerivat> prResult = entityManager.createQuery(cq).getResultList();
        prResult.forEach(kovAPhaseImDerivat ->
                result.add(kovAPhaseImDerivat.getDerivat().getName())
        );

        return result;
    }

    public long getNumberOfAllAnforderungenInUmsetzungsbestaetigung(KovAPhaseImDerivat kovAPhaseImDerivat,
                                                                    List<Long> sensorCocIdList) {


        if (kovAPhaseImDerivat == null || kovAPhaseImDerivat.getDerivat() == null) {
            return 0L;
        }

        QueryPartDTO queryPartDTO = new QueryPartDTO();


        queryPartDTO.append("SELECT COUNT(d) FROM DerivatAnforderungModul AS d ");

        queryPartDTO.append(getRelevantDerivatStatusAsQueryPart(kovAPhaseImDerivat.getKovAPhase()));

        queryPartDTO.put(STATUS, DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId());
        if (kovAPhaseImDerivat.getDerivat() != null) {
            queryPartDTO.append(AND_D_ZUORDNUNG_ANFORDERUNG_DERIVAT_DERIVAT_ID_DERIVATID);
            queryPartDTO.put(DERIVATID, kovAPhaseImDerivat.getDerivat().getId());
        } else {
            return 0L;
        }
        if (sensorCocIdList != null && !sensorCocIdList.isEmpty()) {
            queryPartDTO.append(AND_D_ZUORDNUNG_ANFORDERUNG_DERIVAT_ANFORDERUNG_SENSOR_COC_SENSOR_COC_ID_IN_SENSOR_COC_ID_LIST);
            queryPartDTO.put(SENSOR_COC_ID_LIST, sensorCocIdList);
        }

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPartDTO, entityManager, Long.class);

        return query.getSingleResult();
    }

    public long getNumberOfAllUmgesetzteAnforderungenInUmsetzungsbestaetigung(KovAPhaseImDerivat kovAPhaseImDerivat,
                                                                              List<Long> sensorCocIdList) {

        if (kovAPhaseImDerivat == null || kovAPhaseImDerivat.getDerivat() == null || kovAPhaseImDerivat.getId() == null) {
            return 0L;
        }

        StringBuilder sb = new StringBuilder();

        sb.append("SELECT count(d.*) FROM derivatanforderungmodul AS d "
                + "inner join zuordnunganforderungderivat as z ON d.zuordnunganforderungderivat_id = z.id "
                + "inner join derivat as de ON z.derivat_id = de.id "
                + "inner join anforderung as an ON z.anforderung_id = an.id "
                + "inner join sensorcoc as sc ON an.sensorcoc_sensorcocid = sc.sensorcocid "
                + "inner join modul as mo ON mo.id = d.modul_id ");

        switch (kovAPhaseImDerivat.getKovAPhase()) {
            case VABG0:
            case VABG1:
            case VABG2:
            case VA_VKBG:
            case VKBG:
                sb.append("where d.statusVKBG = ").append(DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId());
                break;
            case VA_VBBG:
            case VBBG:
            case BBG:
            default:
                sb.append("where d.statusZV = ").append(DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId());
                break;
        }

        sb.append(" AND de.id = ").append(kovAPhaseImDerivat.getDerivat().getId());
        if (sensorCocIdList != null && !sensorCocIdList.isEmpty()) {
            sb.append(" AND sc.sensorcocid IN ").append(getIdsList(sensorCocIdList));
        }
        sb.append(" AND (an.id, mo.id) IN "
                + "(select um.anforderung_id, um.modul_id From umsetzungsbestaetigung as um "
                + "inner join kov_a_phase_im_derivat  as kode on um.kovaimderivat_id = kode.id ");
        sb.append("where kode.id = ").append(kovAPhaseImDerivat.getId());
        sb.append(" AND um.status != ").append(UmsetzungsBestaetigungStatus.OFFEN.getId()).append(" )");

        Query q = entityManager.createNativeQuery(sb.toString());
        return (Long) q.getSingleResult();
    }

    private static String getIdsList(Collection<Long> ids) {
        StringBuilder sb = new StringBuilder();
        String result = "";

        if (ids != null && !ids.isEmpty()) {
            sb.append("(");
            ids.forEach(id ->
                    sb.append(id.intValue()).append(", ")
            );
            result = sb.substring(0, sb.lastIndexOf(","));
            result = result + ") ";
        }

        return result;
    }

    public List<Long> getSensorCocIdsForUmsetzungsbestaetiger(long kovAPhaseImDerivatId, long userId) {


        QueryPartDTO queryPartDTO = new QueryPartDTO();


        queryPartDTO.append("SELECT DISTINCT(b.sensorCoc.sensorCocId) "
                + "FROM UBzuKovaPhaseImDerivatUndSensorCoC b "
                + "INNER JOIN b.umsetzungsbestaetiger u "
                + "WHERE b.kovaImDerivat.id = :kovaImDerivatId "
                + "AND u.id = :userId");
        queryPartDTO.put("kovaImDerivatId", kovAPhaseImDerivatId);
        queryPartDTO.put("userId", userId);

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPartDTO, entityManager, Long.class);

        return query.getResultList();
    }

    public long getNumberOfSensorCocsForKovaPhaseImDerivatWithUmsetzer(KovAPhaseImDerivat kovAPhaseImDerivat, List<Long> sensorCocIdList) {
        if (kovAPhaseImDerivat == null || kovAPhaseImDerivat.getDerivat() == null || kovAPhaseImDerivat.getId() == null) {
            return 0L;
        }

        QueryPartDTO queryPartDTO = new QueryPartDTO();


        getBaseQueryForDerivatAnforderungModul(kovAPhaseImDerivat, sensorCocIdList, queryPartDTO);
        queryPartDTO.append("AND d.zuordnungAnforderungDerivat.anforderung.sensorCoc.sensorCocId IN ");
        queryPartDTO.append(" (SELECT ubk.sensorCoc.sensorCocId FROM UBzuKovaPhaseImDerivatUndSensorCoC AS ubk ");
        queryPartDTO.append("WHERE ubk.umsetzungsbestaetiger IS NOT EMPTY ");
        queryPartDTO.append("AND ubk.kovaImDerivat.id = :kovaImDerivatId )");
        queryPartDTO.put("kovaImDerivatId", kovAPhaseImDerivat.getId());


        QueryFactory<Long> queryFactory = new QueryFactory<>();
        TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPartDTO, entityManager, Long.class);

        return query.getSingleResult();

    }

    public long getNumberOfSensorCocsForKovaPhaseImDerivat(KovAPhaseImDerivat kovAPhaseImDerivat, List<Long> sensorCocIdList) {

        if (kovAPhaseImDerivat == null || kovAPhaseImDerivat.getDerivat() == null) {
            return 0L;
        }

        QueryPartDTO queryPartDTO = new QueryPartDTO();

        getBaseQueryForDerivatAnforderungModul(kovAPhaseImDerivat, sensorCocIdList, queryPartDTO);

        QueryFactory<Long> queryFactory = new QueryFactory<>();
        TypedQuery<Long> query = queryFactory.buildTypedQuery(queryPartDTO, entityManager, Long.class);

        return query.getSingleResult();
    }

    private void getBaseQueryForDerivatAnforderungModul(KovAPhaseImDerivat kovAPhaseImDerivat, List<Long> sensorCocIdList, QueryPartDTO queryPartDTO) {
        queryPartDTO.append("SELECT COUNT(DISTINCT(d.zuordnungAnforderungDerivat.anforderung.sensorCoc)) FROM DerivatAnforderungModul AS d ");

        queryPartDTO.append(getRelevantDerivatStatusAsQueryPart(kovAPhaseImDerivat.getKovAPhase()));

        queryPartDTO.put(STATUS, DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId());

        queryPartDTO.append(AND_D_ZUORDNUNG_ANFORDERUNG_DERIVAT_DERIVAT_ID_DERIVATID);
        queryPartDTO.put(DERIVATID, kovAPhaseImDerivat.getDerivat().getId());

        if (sensorCocIdList != null && !sensorCocIdList.isEmpty()) {
            queryPartDTO.append(AND_D_ZUORDNUNG_ANFORDERUNG_DERIVAT_ANFORDERUNG_SENSOR_COC_SENSOR_COC_ID_IN_SENSOR_COC_ID_LIST);
            queryPartDTO.put(SENSOR_COC_ID_LIST, sensorCocIdList);
        }
    }

    private static String getRelevantDerivatStatusAsQueryPart(KovAPhase kovAPhase) {
        switch (kovAPhase) {
            case VABG0:
            case VABG1:
            case VABG2:
            case VA_VKBG:
            case VKBG:
                return "WHERE d.statusVKBG = :status ";
            case VA_VBBG:
            case VBBG:
            case BBG:
            default:
                return "WHERE d.statusZV = :status ";
        }
    }
}
