package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Anhang;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author evda
 */
@Dependent
public class DbFileDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager em;

    public Anhang getAnhangById(Long id) {
        if (id != null) {
            Query q = em.createNamedQuery(Anhang.GET_ANHANG_BY_ID, Anhang.class);
            q.setParameter("id", id);
            List<Anhang> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public void saveAnhang(Anhang anhang) {
        if (isExistsAnhang(anhang)) {
            em.merge(anhang);
        } else {
            em.persist(anhang);
        }
        em.flush();
    }

    public boolean isExistsAnhang(Anhang anh) {
        return getAnhangById(anh.getId()) != null;
    }

    public void removeAnhang(Anhang anh) {
        em.remove(em.merge(anh));
        em.flush();
    }

}
