package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungBasisDataDto;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author evda
 */
@Dependent
public class AnforderungHistoryDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager emPzbk;

    /**
     *
     * @param ah AnforderungHistory
     */
    public void persist(AnforderungHistory ah) {
        if (getAnforderungHistoryById(ah.getId()) != null) {
            emPzbk.merge(ah);
        } else {
            emPzbk.persist(ah);
        }
        emPzbk.flush();
    }

    public AnforderungHistory getAnforderungHistoryById(Long id) {
        if (id != null) {
            Query q = emPzbk.createNamedQuery(AnforderungHistory.BY_ID, AnforderungHistory.class);
            q.setParameter("id", id);
            List<AnforderungHistory> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<AnforderungHistory> getAnforderungHistoryByIdAndKennzeichen(Long id, String kennzeichen) {
        if (id != null) {
            Query q = emPzbk.createNamedQuery(AnforderungHistory.GET_BY_ID_AND_KENNZEICHEN, AnforderungHistory.class);
            q.setParameter("id", id);
            q.setParameter("kennzeichen", kennzeichen);
            List<AnforderungHistory> result = q.getResultList();
            return result != null && !result.isEmpty() ? result : new ArrayList<>();
        }
        return new ArrayList<>();
    }

    public List<AnforderungHistory> getAnforderungHistoryByObjectnameAnforderungId(Long anforderungId, String kennzeichen, String objectName) {
        Query q = emPzbk.createNamedQuery(AnforderungHistory.GET_BY_OBJEKTNAME_ANFORDERUNG_ID, AnforderungHistory.class)
                .setParameter("id", anforderungId)
                .setParameter("kennzeichen", kennzeichen)
                .setParameter("objektName", objectName);
        return anforderungId != null && objectName != null && !objectName.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public AnforderungBasisDataDto getMeldungBasisInfo(Long id) {
        if (id == null) {
            return null;
        }

        Query q = emPzbk.createQuery("SELECT meld.id, meld.fachId, meld.status FROM Meldung meld WHERE meld.id = :id");
        q.setParameter("id", id);
        List<Object[]> resultList = q.getResultList();
        List<AnforderungBasisDataDto> result = AnforderungBasisDataDto.buildFromQueryResultListWithoutVersion(resultList);

        return (result != null && !result.isEmpty()) ? result.get(0) : null;
    }

    public AnforderungBasisDataDto getAnforderungBasisInfo(Long id) {
        if (id == null) {
            return null;
        }

        Query q = emPzbk.createQuery("SELECT anfo.id, anfo.fachId, anfo.version, anfo.status FROM Anforderung anfo WHERE anfo.id = :id");
        q.setParameter("id", id);
        List<Object[]> resultList = q.getResultList();
        List<AnforderungBasisDataDto> result = AnforderungBasisDataDto.buildFromQueryResultListWithVersion(resultList);

        return (result != null && !result.isEmpty()) ? result.get(0) : null;
    }

    public AnforderungBasisDataDto getPzbkBasisInfo(Long id) {
        if (id == null) {
            return null;
        }

        Query q = emPzbk.createQuery("SELECT p.id, p.fachId, p.version, p.status FROM Prozessbaukasten p WHERE p.id = :id");
        q.setParameter("id", id);
        List<Object[]> resultList = q.getResultList();
        List<AnforderungBasisDataDto> result = AnforderungBasisDataDto.buildFromProzessbaukasten(resultList);

        return (result != null && !result.isEmpty()) ? result.get(0) : null;
    }

}
