package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.LocalGroup;
import de.interfaceag.bmw.pzbk.entities.LocalUser;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author evda
 */
@Dependent
public class MitarbeiterDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public void saveLocalUser(LocalUser localUser) {
        if (isExistsLocalUser(localUser)) {
            entityManager.merge(localUser);
            entityManager.flush();
        } else {
            entityManager.persist(localUser);
            entityManager.flush();
        }
    }

    private Boolean isExistsLocalUser(LocalUser localUser) {
        return !getLocalUserByQnumber(localUser.getQnumber()).isEmpty();
    }

    private List<LocalUser> getLocalUserByQnumber(String qNumber) {
        Query q = entityManager.createNamedQuery(LocalUser.LOCALUSER_BY_QNUMBER, LocalUser.class);
        q.setParameter("m", qNumber);
        return qNumber != null && !qNumber.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public Boolean isExistsGruppe(LocalGroup localGroup) {
        Query q = entityManager.createNamedQuery(LocalGroup.GRUPPE_BY_NAME, LocalGroup.class);
        q.setParameter("name", localGroup.getGruppenname());
        return !getGruppenByName(localGroup.getGruppenname()).isEmpty();
    }

    public List<LocalGroup> getGruppenByName(String gruppenname) {
        Query q = entityManager.createNamedQuery(LocalGroup.GRUPPE_BY_NAME, LocalGroup.class);
        q.setParameter("name", gruppenname);
        return gruppenname != null && !gruppenname.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<String> getAllAbteilung() {
        Query q = entityManager.createNamedQuery(Mitarbeiter.ALL_ABTEILUNG, Mitarbeiter.class);
        return q.getResultList();
    }

    public void registerNewGroup(LocalGroup localGroup) {
        entityManager.persist(localGroup);
        entityManager.flush();
    }

    public void registerGroup(LocalGroup localGroup) {
        entityManager.merge(localGroup);
        entityManager.flush();
    }

    public void registerNewMitarbeiter(Mitarbeiter mitarbeiter) {
        entityManager.persist(mitarbeiter);
        entityManager.flush();
    }

    @Deprecated
    public void registerMitarbeiter(Mitarbeiter mitarbeiter) {
        entityManager.merge(mitarbeiter);
        entityManager.flush();
    }

    public void persistMitarbeiter(Mitarbeiter mitarbeiter) {
        if (getUniqueMitarbeiterById(mitarbeiter.getId()).isPresent()) {
            entityManager.merge(mitarbeiter);
        } else {
            entityManager.persist(mitarbeiter);
        }
        entityManager.flush();
    }

    public Optional<Mitarbeiter> getMitarbeiterByQnumber(String qNumber) {
        if (qNumber == null || qNumber.isEmpty()) {
            return Optional.empty();
        }
        Query q = entityManager.createNamedQuery(Mitarbeiter.QUERY_BY_QNUMBER, Mitarbeiter.class);
        q.setParameter("qNumber", qNumber);
        final List<Mitarbeiter> result = q.getResultList();
        if (result.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(result.get(0));
        }
    }

    public List<Mitarbeiter> getAllMitarbeiter() {
        return entityManager.createNamedQuery(Mitarbeiter.QUERY_ALL, Mitarbeiter.class).getResultList();
    }

    public Optional<Mitarbeiter> getUniqueMitarbeiterById(Long id) {
        if (id != null) {
            Query q = entityManager.createNamedQuery(Mitarbeiter.QUERY_BY_ID, Mitarbeiter.class);
            q.setParameter("id", id);
            final List<Mitarbeiter> result = q.getResultList();
            if (result.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.ofNullable(result.get(0));
            }
        }
        return Optional.empty();
    }

    public List<Mitarbeiter> getMitarbeiterByMail(String email) {
        Query q = entityManager.createNamedQuery(Mitarbeiter.QUERY_BY_MAIL, Mitarbeiter.class);
        q.setParameter("email", email);
        return email != null && !email.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<Mitarbeiter> getMitarbeiterByCriteria(String vorname, String nachname, String abteilung) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Mitarbeiter> cq = cb.createQuery(Mitarbeiter.class);
        Root<Mitarbeiter> root = cq.from(Mitarbeiter.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition;

        //first name condition
        if (vorname != null && !"".equals(vorname)) {
            whereCondition = cb.like(cb.lower(root.<String>get("vorname")), "%" + vorname.toLowerCase() + "%");
            whereConditions.add(whereCondition);
        }

        //last name condition
        if (nachname != null && !"".equals(nachname)) {
            whereCondition = cb.like(cb.lower(root.<String>get("nachname")), "%" + nachname.toLowerCase() + "%");
            whereConditions.add(whereCondition);
        }

        //department condition
        if (abteilung != null && !"".equals(abteilung)) {
            whereCondition = cb.like(cb.lower(root.<String>get("abteilung")), "%" + abteilung.toLowerCase() + "%");
            whereConditions.add(whereCondition);
        }

        //Constructing ORDER BY Clause
        List<Order> orderList = new ArrayList();
        orderList.add(cb.asc(root.get("qNumber")));

        cq.select(root)
                .where(whereConditions.toArray(new Predicate[] {}))
                .orderBy(orderList)
                .distinct(true);

        return entityManager.createQuery(cq).getResultList();
    }

    public List<Mitarbeiter> getMitarbeiterByVornameOrNachname(String query) {

        QueryPartDTO queryPartDTO = new QueryPartDTO();
        queryPartDTO.append("SELECT DISTINCT m FROM Mitarbeiter m WHERE "
                + "CONCAT(CONCAT(LOWER(m.vorname), ' '), LOWER(m.nachname)) LIKE LOWER(:query)");
        queryPartDTO.put("query", "%" + query + "%");

        QueryFactory<Mitarbeiter> queryFactory = new QueryFactory<>();
        TypedQuery<Mitarbeiter> typedQuery = queryFactory.buildTypedQuery(queryPartDTO, entityManager, Mitarbeiter.class);
        typedQuery.setMaxResults(10);

        return typedQuery.getResultList();
    }

    public List<Derivat> getUpdatedSelectedDerivateForMitarbeiter(Mitarbeiter mitarbeiter) {
        QueryPartDTO queryPartDTO = new QueryPartDTO();
        queryPartDTO.append("SELECT DISTINCT derivate FROM Mitarbeiter m INNER JOIN m.selectedDerivate derivate WHERE m.id = :id");
        queryPartDTO.put("id", mitarbeiter.getId());

        QueryFactory<Derivat> queryFactory = new QueryFactory<>();
        TypedQuery<Derivat> typedQuery = queryFactory.buildTypedQuery(queryPartDTO, entityManager, Derivat.class);

        return typedQuery.getResultList();
    }

}
