package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Dependent
public class SensorCocDao implements Serializable {

    public static final String RESSORT = "ressort";
    public static final String ORTUNG = "ortung";
    public static final String TECHNOLOGIE = "technologie";
    public static final String ZAK_EINORDNUNG = "zakEinordnung";
    public static final String TECHNOLOGIE_LIST = "technologieList";
    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public SensorCoc getSensorCocById(Long id) {
        if (id != null) {
            Query q = entityManager.createNamedQuery(SensorCoc.QUERY_BY_ID, SensorCoc.class);
            q.setParameter("id", id);
            List<SensorCoc> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public void persistSensorCoc(SensorCoc sensorCoc) {
        if (getSensorCocById(sensorCoc.getSensorCocId()) != null) {
            entityManager.merge(sensorCoc);
        } else {
            entityManager.persist(sensorCoc);
        }
        entityManager.flush();
    }

    public List<SensorCoc> getSensorCocsByIdList(List<Long> ids) {
        Query q = entityManager.createNamedQuery(SensorCoc.QUERY_BY_ID_LIST, SensorCoc.class);
        q.setParameter("ids", ids);
        return ids != null && !ids.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<String> getAllZakEinordnung() {
        return entityManager.createNamedQuery(SensorCoc.QUERY_ALL_ZAK_EINORDNUNG, String.class).getResultList();
    }

    public List<SensorCoc> getSensorCocsByRessortTechnologieOrtung(String ressort, String ortung, String technologie) {
        Query q = entityManager.createNamedQuery(SensorCoc.QUERY_BY_RESSORT_ORTUNG_MODUL, SensorCoc.class);
        q.setParameter(RESSORT, ressort);
        q.setParameter(ORTUNG, ortung);
        q.setParameter(TECHNOLOGIE, technologie);
        return q.getResultList();
    }

    public List<SensorCoc> getSensorCocByZakEinordnung(String zak) {
        Query q = entityManager.createNamedQuery(SensorCoc.QUERY_BY_ZAK_EINORDNUNG, SensorCoc.class);
        q.setParameter(ZAK_EINORDNUNG, zak);
        return zak != null && !"".equals(zak) ? q.getResultList() : new ArrayList<>();
    }

    public List<SensorCoc> getSensorCocsByRessortTechnologieOrtungInList(String ressort, String ortung, String technologie, List<Long> sensorCocIdList) {
        Query q = entityManager.createNamedQuery(SensorCoc.QUERY_BY_RESSORT_ORTUNG_MODUL_IN_LIST, SensorCoc.class);
        q.setParameter(RESSORT, ressort);
        q.setParameter(ORTUNG, ortung);
        q.setParameter(TECHNOLOGIE, technologie);
        q.setParameter("lst", sensorCocIdList);
        return q.getResultList();
    }

    public List<SensorCoc> getSensorCocsByTechnologie(String technologie) {
        Query q = entityManager.createNamedQuery(SensorCoc.QUERY_BY_TECHNOLOGIE, SensorCoc.class);
        q.setParameter(TECHNOLOGIE, technologie);
        return technologie != null && !"".equals(technologie) ? q.getResultList() : new ArrayList<>();
    }

    public Collection<SensorCoc> getSensorCocsByOrtung(Collection<String> ortung) {
        Query q = entityManager.createNamedQuery(SensorCoc.QUERY_BY_ORTUNG, SensorCoc.class);
        q.setParameter(ORTUNG, ortung);
        return ortung != null && !ortung.isEmpty() ? q.getResultList() : Collections.emptyList();
    }

    public List<SensorCoc> getSensorCocsByTechnologieList(List<String> technologieList) {
        Query q = entityManager.createNamedQuery(SensorCoc.QUERY_BY_TECHNOLOGIELIST, SensorCoc.class);
        q.setParameter(TECHNOLOGIE_LIST, technologieList);
        return technologieList != null && !technologieList.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<SensorCoc> getAllSensorCoCsSortByTechnologie() {
        return entityManager.createNamedQuery(SensorCoc.QUERY_ALL_SORT_BY_TECHNOLOGIE, SensorCoc.class).getResultList();
    }

    public List<Long> getAllSensorCocIds() {
        return entityManager.createNamedQuery(SensorCoc.QUERY_ALL_IDS, Long.class).getResultList();
    }
}
