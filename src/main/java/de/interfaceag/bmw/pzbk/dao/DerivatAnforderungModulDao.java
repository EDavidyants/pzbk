package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author evda
 */
@Dependent
public class DerivatAnforderungModulDao implements Serializable {

    public static final String STATUS = "status";
    public static final String ANFORDERUNG = "anforderung";
    public static final String DERIVAT = "derivat";
    public static final String ANFORDERUNG_ID = "anforderungId";
    public static final String MODUL = "modul";
    public static final String DERIVAT_ID = "derivatId";
    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public void persistDerivatAnforderungModul(DerivatAnforderungModul df) {
        if (getDerivatAnforderungModulById(df.getId()) != null) {
            entityManager.merge(df);
        } else {
            entityManager.persist(df);
        }
        entityManager.flush();
    }

    public void persistDerivatAnforderungModulen(List<DerivatAnforderungModul> derivatAnforderungModulen) {
        derivatAnforderungModulen.forEach(da -> {
            if (da.getId() != null) {
                entityManager.merge(da);
            } else {
                entityManager.persist(da);
            }
        });
        entityManager.flush();
    }

    public void removeDerivatAnforderungModul(DerivatAnforderungModul df) {
        entityManager.remove(entityManager.merge(df));
        entityManager.flush();
    }

    public DerivatAnforderungModul getDerivatAnforderungModulById(Long id) {
        if (id != null) {
            Query q = entityManager.createNamedQuery(DerivatAnforderungModul.BY_ID, DerivatAnforderungModul.class);
            q.setParameter("id", id);
            List<DerivatAnforderungModul> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<DerivatAnforderungModul> getDerivatAnforderungModulByIdList(List<Long> idList) {
        Query q = entityManager.createNamedQuery(DerivatAnforderungModul.BY_ID_LIST, DerivatAnforderungModul.class);
        q.setParameter("idList", idList);
        return idList != null && !idList.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<DerivatAnforderungModul> getAllDerivatAnforderungModulen() {
        return entityManager.createNamedQuery(DerivatAnforderungModul.ALL, DerivatAnforderungModul.class).getResultList();
    }

    public List<DerivatAnforderungModul> getDerivatAnforderungModulByAnforderungStatus(long anforderungId, DerivatAnforderungModulStatus status) {
        if (status != null) {
            Query query = entityManager.createNamedQuery(DerivatAnforderungModul.BY_ANFORDERUNG_STATUS, DerivatAnforderungModul.class);
            query.setParameter(ANFORDERUNG_ID, anforderungId);
            query.setParameter(STATUS, status.getStatusId());
            return query.getResultList();
        } else {
            return Collections.emptyList();
        }
    }

    public List<DerivatAnforderungModul> getDerivatAnforderungModulByAnforderungAndDerivat(Anforderung anforderung, Derivat derivat) {
        Query q = entityManager.createNamedQuery(DerivatAnforderungModul.BY_ANFORDERUNG_DERIVAT, DerivatAnforderungModul.class);
        q.setParameter(ANFORDERUNG, anforderung);
        q.setParameter(DERIVAT, derivat);
        List<DerivatAnforderungModul> result = q.getResultList();
        return anforderung != null && derivat != null ? result : new ArrayList<>();
    }

    public List<DerivatAnforderungModul> getDerivatAnforderungModulByAnforderungAndModul(Long anforderung, Long modul) {
        if (anforderung != null && modul != null) {
            Query q = entityManager.createNamedQuery(DerivatAnforderungModul.BY_ANFORDERUNG_MODUL, DerivatAnforderungModul.class);
            q.setParameter(ANFORDERUNG, anforderung);
            q.setParameter(MODUL, modul);
            List<DerivatAnforderungModul> result = q.getResultList();
            return result != null && !result.isEmpty() ? result : null;
        }
        return Collections.emptyList();
    }

    public DerivatAnforderungModul getDerivatAnforderungModulByAnforderungAndModulAndDerivat(Anforderung anforderung, Modul modul, Derivat derivat) {
        if (anforderung != null && modul != null && derivat != null) {
            Query q = entityManager.createNamedQuery(DerivatAnforderungModul.BY_ANFORDERUNG_MODUL_DERIVAT, DerivatAnforderungModul.class);
            q.setParameter(ANFORDERUNG, anforderung);
            q.setParameter(MODUL, modul);
            q.setParameter(DERIVAT, derivat);
            List<DerivatAnforderungModul> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<Derivat> getDerivatenForAnforderung(Anforderung anforderung) {
        Query q = entityManager.createNamedQuery(DerivatAnforderungModul.GET_DERIVATE_FOR_ANFORDERUNG, Derivat.class);
        q.setParameter(ANFORDERUNG, anforderung);
        return anforderung != null ? q.getResultList() : new ArrayList<>();
    }

    public List<SensorCoc> getSensorCocForDerivat(Derivat derivat) {

        if (derivat == null) {
            return new ArrayList<>();
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<SensorCoc> cq = cb.createQuery(SensorCoc.class);
        Root<DerivatAnforderungModul> dam = cq.from(DerivatAnforderungModul.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition = cb.equal(dam.get("zuordnungAnforderungDerivat").<Derivat>get(DERIVAT), derivat);
        whereConditions.add(whereCondition);

        cq.select(dam.get("zuordnungAnforderungDerivat").get(ANFORDERUNG).get("sensorCoc"))
                .where(whereConditions.toArray(new Predicate[] {}))
                .distinct(true);
        return entityManager.createQuery(cq).getResultList();
    }

    public List<SensorCoc> getSensorCocForDerivatStatusAndKovaPhase(Derivat derivat, List<Integer> derivatAnforderungModulStatus, KovAPhase kovAPhase) {

        if (somethingIsNull(derivat, derivatAnforderungModulStatus)) {
            return new ArrayList<>();
        }

        QueryPart qp;
        qp = new QueryPartDTO("SELECT DISTINCT zad.anforderung.sensorCoc from DerivatAnforderungModul dam ");
        qp.append(" INNER JOIN ZuordnungAnforderungDerivat zad on dam.zuordnungAnforderungDerivat = zad ");

        qp.append(" WHERE zad.derivat.id = :derivatId");
        qp.put(DERIVAT_ID, derivat.getId());

        qp.append(" AND ");
        getQueryForStatus(qp, kovAPhase);
        qp.put(STATUS, derivatAnforderungModulStatus);
        Query q = qp.buildGenericQuery(entityManager);

        return q.getResultList();

    }

    private static void getQueryForStatus(QueryPart queryPartDTO, KovAPhase kovAPhase) {

        switch (kovAPhase) {
            case VABG0:
            case VABG1:
            case VABG2:
            case VA_VKBG:
            case VKBG:
                queryPartDTO.append(" dam.statusVKBG IN :status");
                break;
            case VA_VBBG:
            case VBBG:
            case BBG:
                queryPartDTO.append(" dam.statusZV IN :status");
                break;
            default:
                break;
        }

    }

    private static boolean somethingIsNull(Derivat derivat, List<Integer> derivatAnforderungModulStatus) {
        return derivat == null || derivatAnforderungModulStatus == null;
    }

    public List<SensorCoc> getSensorCocForVereinbarungenWithStatusAngenommenAndDerivatId(Long derivatId) {
        Query q = entityManager.createNamedQuery(DerivatAnforderungModul.GET_SENSORCOC_FOR_STATUS_AND_DERIVAT, SensorCoc.class);
        q.setParameter(DERIVAT_ID, derivatId);
        q.setParameter(STATUS, DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId());
        return derivatId != null ? q.getResultList() : new ArrayList<>();
    }

    public List<SensorCoc> getSensorCocForVereinbarungenWithStatusAngenommenAndDerivatIdAndSensorCocIdNotInList(Long derivatId, List<Long> existingSensorCocIds) {
        Query q = entityManager.createNamedQuery(DerivatAnforderungModul.GET_SENSORCOC_FOR_STATUS_NOT_IN_SENSORCOCIDLIST_AND_DERIVAT, SensorCoc.class);
        q.setParameter(DERIVAT_ID, derivatId);
        q.setParameter("sensorCocIdList", existingSensorCocIds);
        q.setParameter(STATUS, DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId());
        return derivatId != null && existingSensorCocIds != null && !existingSensorCocIds.isEmpty() ? q.getResultList() : new ArrayList<>();
    }
}
