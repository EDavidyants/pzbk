package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Werk;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fn
 */
@Dependent
public class WerkDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public void persistWerk(Werk werk) {
        if (werk.getId() != null) {
            entityManager.merge(werk);
        } else {
            entityManager.persist(werk);
        }
        entityManager.flush();
    }

    public List<Werk> getWerkByName(String name) {
        Query q = entityManager.createNamedQuery(Werk.QUERY_BY_NAME, Werk.class);
        q.setParameter("name", name);
        return name != null && !name.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<Werk> getAllWerk() {
        return entityManager.createNamedQuery(Werk.QUERY_ALL).getResultList();
    }
}
