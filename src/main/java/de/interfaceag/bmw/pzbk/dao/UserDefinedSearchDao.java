package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.UserDefinedSearch;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author evda
 */
@Dependent
public class UserDefinedSearchDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager em;

    public UserDefinedSearch getUserDefinedSearchById(Long id) {
        if (id != null) {
            Query q = em.createNamedQuery(UserDefinedSearch.BY_ID, UserDefinedSearch.class);
            q.setParameter("id", id);
            List<UserDefinedSearch> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<UserDefinedSearch> getUserDefinedSearchByMitarbeiter(Mitarbeiter mitarbeiter) {
        Query q = em.createNamedQuery(UserDefinedSearch.BY_MITARBEITER, UserDefinedSearch.class);
        q.setParameter("mitarbeiter", mitarbeiter);
        return mitarbeiter != null ? q.getResultList() : new ArrayList<>();
    }

    public List<UserDefinedSearch> getUserDefinedSearchByNameAndMitarbeiter(String name, Mitarbeiter mitarbeiter) {
        Query q = em.createNamedQuery(UserDefinedSearch.BY_NAME_MITARBEITER, UserDefinedSearch.class);
        q.setParameter("name", name);
        q.setParameter("mitarbeiter", mitarbeiter);
        return name != null && mitarbeiter != null ? q.getResultList() : new ArrayList<>();
    }

    public void persistUserDefinedSearch(UserDefinedSearch userDefinedSearch) {
        if (getUserDefinedSearchById(userDefinedSearch.getId()) != null) {
            em.merge(userDefinedSearch);
        } else {
            em.persist(userDefinedSearch);
        }
        em.flush();
    }

    public void removeUserDefinedSearch(UserDefinedSearch userDefinedSearch) {
        em.remove(em.merge(userDefinedSearch));
        em.flush();
    }
}
