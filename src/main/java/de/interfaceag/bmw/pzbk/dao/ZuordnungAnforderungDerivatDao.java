package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.dialog.viewdata.DerivatZuordnenDto;
import de.interfaceag.bmw.pzbk.dialog.viewdata.DerivatenAnzeigenDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Dependent
public class ZuordnungAnforderungDerivatDao implements Serializable {

    private static final String ANFORDERUNG = "anforderung";
    private static final String DERIVAT = "derivat";
    private static final String ANFORDERUNG_LIST = "anforderungList";
    private static final String DERIVAT_LIST = "derivatList";
    private static final String ANFORDERUNG_ID = "anforderungId";
    private static final String DERIVAT_IDS = "derivatIds";
    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public void persistZuordnungAnforderungDerivat(ZuordnungAnforderungDerivat anforderungDerivat) {
        if (anforderungDerivat.getId() != null) {
            entityManager.merge(anforderungDerivat);
        } else {
            entityManager.persist(anforderungDerivat);
        }
        entityManager.flush();
    }

    public void persistZuordnungAnforderungDerivat(List<ZuordnungAnforderungDerivat> anforderungDerivat) {
        anforderungDerivat.forEach(ad -> {
            if (ad.getId() != null) {
                entityManager.merge(ad);
            } else {
                entityManager.persist(ad);
            }
        });
        entityManager.flush();
    }

    public void removeZuordnungAnforderungDerivat(ZuordnungAnforderungDerivat anforderungDerivat) {
        entityManager.remove(entityManager.merge(anforderungDerivat));
        entityManager.flush();
    }

    public ZuordnungAnforderungDerivat getZuordnungAnforderungDerivatById(Long id) {
        return entityManager.find(ZuordnungAnforderungDerivat.class, id);
    }

    public List<ZuordnungAnforderungDerivat> getAllZuordnungAnforderungDerivatForAnforderungListDerivatListFetch(
            Collection<Long> anforderungen, Collection<Long> derivate) {
        TypedQuery<ZuordnungAnforderungDerivat> q = entityManager.createNamedQuery(ZuordnungAnforderungDerivat.GET_BY_ANFORDERUNGLIST_DERIVATLIST,
                ZuordnungAnforderungDerivat.class);
        q.setParameter(ANFORDERUNG_LIST, anforderungen);
        q.setParameter(DERIVAT_LIST, derivate);
        return anforderungen != null && !anforderungen.isEmpty() && derivate != null && !derivate.isEmpty()
                ? q.getResultList() : new ArrayList<>();
    }

    public ZuordnungAnforderungDerivat getZuordnungAnforderungDerivatForAnforderungDerivat(Anforderung anforderung,
                                                                                           Derivat derivat) {
        TypedQuery<ZuordnungAnforderungDerivat> q = entityManager.createNamedQuery(ZuordnungAnforderungDerivat.GET_BY_ANFORDERUNG_DERIVAT,
                ZuordnungAnforderungDerivat.class);
        q.setParameter(ANFORDERUNG, anforderung);
        q.setParameter(DERIVAT, derivat);
        List<ZuordnungAnforderungDerivat> result = derivat != null && anforderung != null
                ? q.getResultList() : new ArrayList<>();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public List<ZuordnungAnforderungDerivat> getZuordnungAnforderungDerivatForAnforderung(Anforderung anforderung) {
        TypedQuery<ZuordnungAnforderungDerivat> q = entityManager.createNamedQuery(ZuordnungAnforderungDerivat.GET_BY_ANFORDERUNG,
                ZuordnungAnforderungDerivat.class);
        q.setParameter(ANFORDERUNG, anforderung);
        return anforderung != null ? q.getResultList() : new ArrayList<>();
    }

    public List<DerivatenAnzeigenDto> getDerivatAnzeigenDialogViewDataForAnforderungId(Long anforderungId) {

        QueryPartDTO queryPartDTO = new QueryPartDTO("SELECT z.id, z.nachZakUebertragen, d.id, d.name FROM ZuordnungAnforderungDerivat z INNER JOIN z.derivat d WHERE z.anforderung.id = :anforderungId");
        queryPartDTO.put(ANFORDERUNG_ID, anforderungId);

        QueryFactory<Object[]> queryFactory = new QueryFactory<>();
        TypedQuery<Object[]> typedQuery = queryFactory.buildTypedQuery(queryPartDTO, entityManager, Object.class);

        Collection<Object[]> result = typedQuery.getResultList();

        return result.stream().map(r -> new DerivatenAnzeigenDto((Long) r[0], (String) r[3], (Long) r[2], (Boolean) r[1])).collect(Collectors.toList());
    }


    public List<DerivatZuordnenDto> getDerivateZuordnenViewDataForAnforderung(Long anforderungId) {
        QueryPartDTO qp = new QueryPartDTO("SELECT d.id, d.name, d.produktlinie ");
        qp.append("FROM Derivat d WHERE d.status != 4 AND d.id IN (SELECT z.derivat_id FROM ZuordnungAnforderungDerivat z  ");
        qp.append(" WHERE z.anforderung_id = " + anforderungId + ")");

        Query q = entityManager.createNativeQuery(qp.getQuery());
        Collection<Object[]> result = q.getResultList();

        return result.stream().map(r -> new DerivatZuordnenDto((Long) r[0], (String) r[1], (String) r[2])).collect(Collectors.toList());
    }

    public List<DerivatZuordnenDto> getDerivateZuordnenViewDataForAnforderung(Long anforderungId, List<Long> derivatIDs) {
        QueryPartDTO qp = new QueryPartDTO("SELECT d.id, d.name, d.produktlinie ");
        qp.append("FROM Derivat d WHERE d.status IN (1, 2) AND d.id NOT IN (SELECT z.derivat_id FROM ZuordnungAnforderungDerivat z  ");
        qp.append(" WHERE z.anforderung_id = " + anforderungId + ")");
        qp.append(" AND d.id IN ").append(getIdsList(derivatIDs));

        Query q = entityManager.createNativeQuery(qp.getQuery());
        Collection<Object[]> result = q.getResultList();

        return result.stream().map(r -> new DerivatZuordnenDto((Long) r[0], (String) r[1], (String) r[2])).collect(Collectors.toList());
    }

    public List<Derivat> getDerivateForAnforderung(Anforderung anforderung) {
        TypedQuery<Derivat> q = entityManager.createNamedQuery(ZuordnungAnforderungDerivat.GET_DERIVAT_BY_ANFORDERUNG, Derivat.class);
        q.setParameter(ANFORDERUNG, anforderung);
        return anforderung != null ? q.getResultList() : new ArrayList<>();
    }

    public List<ZuordnungAnforderungDerivat> getZuordnungAnforderungDerivatByIdList(List<Long> idList) {
        TypedQuery<ZuordnungAnforderungDerivat> q = entityManager.createNamedQuery(ZuordnungAnforderungDerivat.GET_DERIVAT_BY_ID_LIST, ZuordnungAnforderungDerivat.class);
        q.setParameter("idList", idList);
        return idList != null && !idList.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<ZuordnungAnforderungDerivat> getZuordnungAnforderungDerivatForDerivat(Derivat derivat) {
        TypedQuery<ZuordnungAnforderungDerivat> q = entityManager.createNamedQuery(ZuordnungAnforderungDerivat.GET_BY_DERIVAT,
                ZuordnungAnforderungDerivat.class);
        q.setParameter(DERIVAT, derivat);
        return derivat != null ? q.getResultList() : new ArrayList<>();
    }

    private static String getIdsList(Collection<Long> ids) {
        StringBuilder sb = new StringBuilder();
        String result = "";

        if (ids != null && !ids.isEmpty()) {
            sb.append("(");
            ids.forEach(id ->
                    sb.append(id.intValue()).append(", ")
            );
            result = sb.substring(0, sb.lastIndexOf(","));
            result = result + ") ";
        }

        return result;
    }

    public List<ZuordnungAnforderungDerivat> getZuordnungAnforderungDerivatForAnforderungAndDerivatIds(Anforderung anforderung, List<Long> derivatIds) {
        if (anforderung == null || derivatIds.isEmpty()) {
            return new ArrayList<>();
        }

        TypedQuery<ZuordnungAnforderungDerivat> q = entityManager.createNamedQuery(ZuordnungAnforderungDerivat.GET_BY_ANFORDERUNG_DERIVATIDS, ZuordnungAnforderungDerivat.class);
        q.setParameter(ANFORDERUNG, anforderung);
        q.setParameter(DERIVAT_IDS, derivatIds);
        return q.getResultList();
    }

}
