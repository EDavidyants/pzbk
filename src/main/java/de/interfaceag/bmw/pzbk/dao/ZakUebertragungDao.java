package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author evda
 */
@Dependent
public class ZakUebertragungDao implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ZakUebertragungDao.class);
    private static final String DERIVAT_ANFORDERUNG_MODUL = "derivatAnforderungModul";
    private static final String STATUS = "status";
    private static final String UEBERTRAGUNG_ID = "uebertragungId";
    private static final String DERIVAT_ID = "derivatId";
    private static final String MODUL = "modul";
    private static final String ANFORDERUNG = "anforderung";

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public void persistZakUebertragung(ZakUebertragung zak) {
        if (getZakUebertragungById(zak.getId()) != null) {
            entityManager.merge(zak);
        } else {
            entityManager.persist(zak);
        }
        entityManager.flush();
    }

    ZakUebertragung getZakUebertragungById(Long id) {
        if (id != null) {
            TypedQuery<ZakUebertragung> q = entityManager.createNamedQuery(ZakUebertragung.BY_ID, ZakUebertragung.class);
            q.setParameter("id", id);
            List<ZakUebertragung> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<ZakUebertragung> getZakUebertragungByDerivatAnforderungModul(DerivatAnforderungModul derivatAnforderungModul) {
        TypedQuery<ZakUebertragung> q = entityManager.createNamedQuery(ZakUebertragung.BY_DERIVATANFORDERUNGMODUL, ZakUebertragung.class);
        q.setParameter(DERIVAT_ANFORDERUNG_MODUL, derivatAnforderungModul);
        return derivatAnforderungModul != null ? q.getResultList() : new ArrayList<>();
    }

    public List<ZakUebertragung> getZakUebertragungByStatus(ZakStatus status) {
        TypedQuery<ZakUebertragung> q = entityManager.createNamedQuery(ZakUebertragung.GET_BY_STATUS, ZakUebertragung.class);
        q.setParameter(STATUS, status);
        return status != null ? q.getResultList() : new ArrayList<>();
    }

    public Optional<ZakUebertragung> getZakUebertragungByUebertragungIdDerivatId(Long uebertragungId, Long derivatId) {
        if (derivatId != null && uebertragungId != null) {
            TypedQuery<ZakUebertragung> q = entityManager.createNamedQuery(ZakUebertragung.GET_BY_UEBERTRAGUNG_ID_DERIVAT_ID, ZakUebertragung.class);
            q.setParameter(UEBERTRAGUNG_ID, uebertragungId);
            q.setParameter(DERIVAT_ID, derivatId);
            List<ZakUebertragung> result = q.getResultList();
            if (result != null && result.size() == 1) {
                ZakUebertragung zakUebertragung = result.get(0);
                LOG.info("found one result for uebertragungId {} and derivatId {}: {}", uebertragungId, derivatId, zakUebertragung);
                return Optional.of(zakUebertragung);
            } else if (result != null && result.size() > 1) {
                LOG.error("too many results for uebertragungId {} and derivatId {}", uebertragungId, derivatId);
            } else {
                LOG.error("no result for uebertragungId {} and derivatId {}", uebertragungId, derivatId);
            }
        } else {
            LOG.error("derivatId or uebertragungId is null");
        }
        return Optional.empty();
    }

    public List<ZakUebertragung> getZakUebertragungByUebertragungIdsAndDerivatId(List<Long> uebertragungIds, Long derivatId) {
        if (derivatId != null && uebertragungIds != null && !uebertragungIds.isEmpty()) {
            TypedQuery<ZakUebertragung> q = entityManager.createNamedQuery(ZakUebertragung.GET_BY_UEBERTRAGUNG_ID_LIST_DERIVAT_ID, ZakUebertragung.class);
            q.setParameter("uebertragungIds", uebertragungIds);
            q.setParameter(DERIVAT_ID, derivatId);
            return q.getResultList();
        } else {
            LOG.error("derivatId or uebertragungId is null");
            return new ArrayList<>();
        }
    }

    public List<ZakUebertragung> getZakUebertragungByDerivatAnforderungModulList(List<DerivatAnforderungModul> derivatAnforderungModulList) {
        TypedQuery<ZakUebertragung> q = entityManager.createNamedQuery(ZakUebertragung.BY_DERIVATANFORDERUNGMODUL_LIST, ZakUebertragung.class);
        q.setParameter("derivatAnforderungModulList", derivatAnforderungModulList);
        return derivatAnforderungModulList != null && !derivatAnforderungModulList.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public Long getCountByAnforderungAndModulAndStatusList(Anforderung anforderung, Modul modul, List<ZakStatus> statusList) {
        TypedQuery<Long> q = entityManager.createNamedQuery(ZakUebertragung.GET_COUNT_BY_ANFORDERUNG_AND_MODUL_STATUSLIST, Long.class);
        q.setParameter(ANFORDERUNG, anforderung);
        q.setParameter(MODUL, modul);
        q.setParameter("statusList", statusList);
        return anforderung != null && modul != null && statusList != null && !statusList.isEmpty() ? (Long) q.getResultList().get(0) : 0L;
    }

    public List<ZakUebertragung> getZakUebertragungByAnforderungDerivatList(List<Derivat> derivatList, Anforderung anforderung) {
        TypedQuery<ZakUebertragung> q = entityManager.createNamedQuery(ZakUebertragung.GET_BY_ANFORDERUNG_DERIVATLIST, ZakUebertragung.class);
        q.setParameter("derivatList", derivatList);
        q.setParameter(ANFORDERUNG, anforderung);
        return anforderung != null && derivatList != null && !derivatList.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public Long getMaxZakUebertragungIdByAnforderungModul(Anforderung anforderung, Modul modul) {
        TypedQuery<Long> q = entityManager.createNamedQuery(ZakUebertragung.GET_MAXUEBERTRAGUNGID_BY_ANFORDERUNG_MODUL, Long.class);
        q.setParameter(ANFORDERUNG, anforderung);
        q.setParameter(MODUL, modul);
        return anforderung != null && modul != null ? (Long) q.getResultList().get(0) : null;
    }

    public Set<String> getZakIdsByAnforderungModul(Anforderung anforderung, Modul modul) {
        TypedQuery<String> q = entityManager.createNamedQuery(ZakUebertragung.GET_ZAKIDS_BY_ANFORDERUNG_MODUL, String.class);
        q.setParameter(ANFORDERUNG, anforderung);
        q.setParameter(MODUL, modul);
        return anforderung != null && modul != null ? new HashSet<>(q.getResultList()) : new HashSet<>();
    }

    public ZakUebertragung getZakUebertragungForAnforderungModulAfterDate(Anforderung anforderung, Modul modul, Date changeDate) {
        TypedQuery<ZakUebertragung> q = entityManager.createNamedQuery(ZakUebertragung.GET_FIRST_ZAKUEBERTRAGUNG_FOR_ANFORDERUNG_MODUL, ZakUebertragung.class);
        q.setParameter(ANFORDERUNG, anforderung);
        q.setParameter(MODUL, modul);
        q.setParameter("changeDate", changeDate);
        List<ZakUebertragung> result = q.getResultList();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public List<ZakUebertragung> getZakUebertragungForAnforderungModulIds(List<Long> anforderungIds, List<Long> modulIds) {
        TypedQuery<ZakUebertragung> q = entityManager.createNamedQuery(ZakUebertragung.GET_BY_ANFORDERUNG_MODUL_IDS, ZakUebertragung.class);
        q.setParameter("anforderungIds", anforderungIds);
        q.setParameter("modulIds", modulIds);
        return anforderungIds != null && !anforderungIds.isEmpty() && modulIds != null && !modulIds.isEmpty()
                ? q.getResultList() : new ArrayList<>();
    }

    public List<ZakUebertragung> getAllZakUebertragungForTimeRange(Date startDate, Date endDate) {
        TypedQuery<ZakUebertragung> q = entityManager.createNamedQuery(ZakUebertragung.GET_ALL_FOR_TIME_RANGE, ZakUebertragung.class);
        q.setParameter("startDate", startDate);
        q.setParameter("endDate", endDate);

        return q.getResultList();
    }

}
