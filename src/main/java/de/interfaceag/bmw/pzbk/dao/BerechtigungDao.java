package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Berechtigung;
import de.interfaceag.bmw.pzbk.entities.BerechtigungDerivat;
import de.interfaceag.bmw.pzbk.entities.BerechtigungHistory;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.UserToRole;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungBerechtigungService;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author evda
 */
@Dependent
public class BerechtigungDao implements Serializable {

    private static final String ROLLE = "rolle";
    private static final String MITARBEITER = "mitarbeiter";
    private static final String RECHTTYPE = "rechttype";
    private static final String FUER_ID = "fuerId";

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private TteamService tteamService;
    @Inject
    private UmsetzungsbestaetigungBerechtigungService umsetzungsbestaetigungBerechtigungService;

    BerechtigungHistory getBerechtigungHistoryById(Long id) {
        if (id != null) {
            TypedQuery<BerechtigungHistory> q = entityManager.createNamedQuery(BerechtigungHistory.BY_ID, BerechtigungHistory.class);
            q.setParameter("id", id);
            List<BerechtigungHistory> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public void persistBerechtigungHistory(BerechtigungHistory berechtigungHistory) {
        if (getBerechtigungHistoryById(berechtigungHistory.getId()) != null) {
            entityManager.merge(berechtigungHistory);
        } else {
            entityManager.persist(berechtigungHistory);
        }
        entityManager.flush();
    }

    private Berechtigung getBerechtigungById(Long id) {
        if (id != null) {
            TypedQuery<Berechtigung> q = entityManager.createNamedQuery(Berechtigung.BY_ID, Berechtigung.class);
            q.setParameter("id", id);
            List<Berechtigung> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<Berechtigung> getBerechtigungByMitarbeiter(Mitarbeiter mitarbeiter) {
        TypedQuery<Berechtigung> q = entityManager.createNamedQuery(Berechtigung.BY_MITARBEITER, Berechtigung.class);
        q.setParameter(MITARBEITER, mitarbeiter);
        return mitarbeiter != null ? q.getResultList() : new ArrayList<>();
    }

    public List<Berechtigung> getBerechtigungByMitarbeiterAndRolle(Mitarbeiter mitarbeiter, Rolle rolle) {
        TypedQuery<Berechtigung> q = entityManager.createNamedQuery(Berechtigung.BY_MITARBEITER_AND_ROLLE, Berechtigung.class);
        q.setParameter(MITARBEITER, mitarbeiter);
        q.setParameter(ROLLE, rolle);
        return mitarbeiter != null && rolle != null ? q.getResultList() : new ArrayList<>();
    }

    public List<Berechtigung> getBerechtigungByRolle(Rolle rolle) {
        TypedQuery<Berechtigung> q = entityManager.createNamedQuery(Berechtigung.BY_ROLLE, Berechtigung.class);
        q.setParameter(ROLLE, rolle);
        return rolle != null ? q.getResultList() : new ArrayList<>();
    }

    // liefert Berechtigungen zurueck, die Lese- oder Schreib-rechte haben
    public List<Berechtigung> getBerechtigungByMitarbeiterAndRolleAndZiel(Mitarbeiter mitarbeiter, Rolle rolle, BerechtigungZiel fuer) {
        TypedQuery<Berechtigung> q = entityManager.createNamedQuery(Berechtigung.BY_MITARBEITER_ROLLE_ZIEL, Berechtigung.class);
        q.setParameter(MITARBEITER, mitarbeiter);
        q.setParameter(ROLLE, rolle);
        q.setParameter("fuer", fuer);
        return mitarbeiter != null && rolle != null && fuer != null ? q.getResultList() : new ArrayList<>();
    }

    //liefert Berechtigungen zurueck. die das gegebenen Recht haben
    public List<String> getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype(Mitarbeiter mitarbeiter, Rolle rolle, BerechtigungZiel fuer, Rechttype rechttype) {
        TypedQuery<String> q = entityManager.createNamedQuery(Berechtigung.BY_MITARBEITER_ROLLE_ZIEL_WITH_RECHTTYPE, String.class);
        q.setParameter(MITARBEITER, mitarbeiter);
        q.setParameter(ROLLE, rolle);
        q.setParameter("fuer", fuer);
        q.setParameter(RECHTTYPE, rechttype);
        return mitarbeiter != null && rolle != null && fuer != null && rechttype != null ? q.getResultList() : new ArrayList<>();
    }

    //get Lese- und Schreib-berechtigungen
    public List<Berechtigung> getBerechtigungByMitarbeiterAndRolleAndZielById(Mitarbeiter mitarbeiter, Rolle rolle, String fuerId, BerechtigungZiel fuer) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Berechtigung> cq = cb.createQuery(Berechtigung.class);
        Root<Berechtigung> root = cq.from(Berechtigung.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition;

        if (mitarbeiter != null) {
            whereCondition = cb.equal(root.get(MITARBEITER), mitarbeiter);
            whereConditions.add(whereCondition);
        }

        if (rolle != null) {
            whereCondition = cb.equal(root.<Enum>get(ROLLE), rolle);
            whereConditions.add(whereCondition);
        }

        if (fuer != null) {
            whereCondition = cb.equal(root.<Enum>get("fuer"), fuer);
            whereConditions.add(whereCondition);
        }

        if (fuerId != null && fuer != null) {
            whereCondition = cb.equal(root.<String>get(FUER_ID), fuerId);
            whereConditions.add(whereCondition);
        }

        //Constructing ORDER BY Clause
        List<Order> orderList = new ArrayList<>();
        orderList.add(cb.desc(root.get("id")));

        cq.select(root)
                .where(whereConditions.toArray(new Predicate[] {}))
                .orderBy(orderList)
                .distinct(true);

        return entityManager.createQuery(cq).getResultList();
    }

    public List<Mitarbeiter> getMitarbeiterForDefinierteBerechtigungByRolleAndZielId(Rolle rolle, String fuerId, BerechtigungZiel fuer, Rechttype rechttype) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Mitarbeiter> cq = cb.createQuery(Mitarbeiter.class);
        Root<Berechtigung> berechtigung = cq.from(Berechtigung.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition;

        if (rolle != null) {
            whereCondition = cb.equal(berechtigung.<Enum>get(ROLLE), rolle);
            whereConditions.add(whereCondition);
        }

        if (fuer != null) {
            whereCondition = cb.equal(berechtigung.<Enum>get("fuer"), fuer);
            whereConditions.add(whereCondition);
        }

        if (fuerId != null && fuer != null) {
            whereCondition = cb.equal(berechtigung.<String>get(FUER_ID), fuerId);
            whereConditions.add(whereCondition);
        }

        if (rechttype != null) {
            whereCondition = cb.equal(berechtigung.<Enum>get(RECHTTYPE), rechttype);
            whereConditions.add(whereCondition);
        }

        //Constructing ORDER BY Clause
        List<Order> orderList = new ArrayList<>();
        orderList.add(cb.desc(berechtigung.get("id")));

        cq.select(berechtigung.get(MITARBEITER))
                .where(whereConditions.toArray(new Predicate[] {}))
                .orderBy(orderList)
                .distinct(true);

        return entityManager.createQuery(cq).getResultList();
    }

    public List<SensorCoc> getAuthorizedSensorCocListForMitarbeiter(Mitarbeiter mitarbeiter) {
        List<SensorCoc> result = new ArrayList<>();
        List<Rolle> rollen = getRolesByQnumber(mitarbeiter.getQNumber());

        for (Rolle rolle : rollen) {
            switch (rolle) {
                case ADMIN:
                    return sensorCocService.getAllSortByTechnologie();
                case ANFORDERER:
                    getSensorCocsForAnforderer(mitarbeiter, rolle, result);
                    break;
                case UMSETZUNGSBESTAETIGER:
                    getSensorCocsForUmsetzungsbestaetiger(mitarbeiter, result);
                    break;
                default:
                    getSensorCocBerechtigungForMitarbeiterAndRoll(mitarbeiter, rolle, result);
                    break;
            }
        }

        return result;
    }

    private void getSensorCocBerechtigungForMitarbeiterAndRoll(Mitarbeiter mitarbeiter, Rolle rolle, List<SensorCoc> result) {
        BerechtigungZiel fuer;
        fuer = BerechtigungZiel.SENSOR_COC;
        List<Berechtigung> berechtigungen = getBerechtigungByMitarbeiterAndRolleAndZiel(mitarbeiter, rolle, fuer);
        List<String> sensorCocIdAsString = new ArrayList<>();
        Long sensorCocId;
        SensorCoc sensorCoc;
        berechtigungen.forEach(berechtigung -> sensorCocIdAsString.add(berechtigung.getFuerId()));
        for (String bstr : sensorCocIdAsString) {
            sensorCocId = Long.parseLong(bstr);
            sensorCoc = sensorCocService.getSensorCocById(sensorCocId);
            if (!result.contains(sensorCoc) && sensorCoc != null) {
                result.add(sensorCoc);
            }
        }
    }

    private void getSensorCocsForUmsetzungsbestaetiger(Mitarbeiter mitarbeiter, List<SensorCoc> result) {
        List<SensorCoc> scList = umsetzungsbestaetigungBerechtigungService.getSensorCocToDoListForUmsetzungsbestaetiger(mitarbeiter);
        scList.stream().filter(sensorCoc -> (!result.contains(sensorCoc))).forEachOrdered(result::add);
    }

    private void getSensorCocsForAnforderer(Mitarbeiter mitarbeiter, Rolle rolle, List<SensorCoc> result) {
        BerechtigungZiel fuer;
        fuer = BerechtigungZiel.ZAK_EINORDNUNG;
        List<Berechtigung> berechtigungen = getBerechtigungByMitarbeiterAndRolleAndZiel(mitarbeiter, rolle, fuer);
        for (Berechtigung berechtigung : berechtigungen) {
            List<SensorCoc> sensorCocList = sensorCocService.getSensorCocByZakEinordnung(berechtigung.getFuerId());
            for (SensorCoc sensorCoc : sensorCocList) {
                if (!result.contains(sensorCoc) && sensorCoc != null) {
                    result.add(sensorCoc);
                }
            }
        }
    }

    public List<Tteam> getAuthorizedAsSchreiberTteamListForMitarbeiter(Mitarbeiter mitarbeiter) {
        List<Tteam> result = new ArrayList<>();
        List<Rolle> rollen = getRolesByQnumber(mitarbeiter.getQNumber());
        BerechtigungZiel fuer = BerechtigungZiel.TTEAM;

        for (Rolle rolle : rollen) {
            List<String> berechtigungen = getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype(mitarbeiter, rolle, fuer, Rechttype.SCHREIBRECHT);
            Long tteamId;
            Tteam tteam;

            for (String berechtigung : berechtigungen) {
                tteamId = Long.parseLong(berechtigung);
                tteam = tteamService.getTteamById(tteamId);
                if (!result.contains(tteam) && tteam != null) {
                    result.add(tteam);
                }
            }

        } // end of for each role iteration

        return result;
    }

    public List<String> getAuthorizedZakEinordnungListForMitarbeiter(Mitarbeiter mitarbeiter) {
        return getRolesByQnumber(mitarbeiter.getQNumber())
                .stream()
                .map(rolle -> getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype(mitarbeiter, rolle, BerechtigungZiel.ZAK_EINORDNUNG, Rechttype.SCHREIBRECHT))
                .flatMap(List::stream)
                .distinct()
                .collect(Collectors.toList());
    }

    private List<String> getAuthorizedZielIdsForMitarbeiterAndZiel(Mitarbeiter mitarbeiter, BerechtigungZiel fuer) {
        TypedQuery<String> q = entityManager.createNamedQuery(Berechtigung.BY_MITARBEITER_ZIEL, String.class);
        q.setParameter(MITARBEITER, mitarbeiter);
        q.setParameter("fuer", fuer);
        q.setParameter(RECHTTYPE, Rechttype.KEIN);
        return mitarbeiter != null && fuer != null ? q.getResultList() : new ArrayList<>();
    }

    public List<Long> getAuthorizedSensorCocIdListForMitarbeiter(Mitarbeiter mitarbeiter) {
        List<SensorCoc> sensorCocList = getAuthorizedSensorCocListForMitarbeiter(mitarbeiter);
        return sensorCocList.stream().map(SensorCoc::getSensorCocId).distinct().collect(Collectors.toList());
    }

    public List<ModulSeTeam> getAuthorizedModulSeTeamListForMitarbeiter(Mitarbeiter mitarbeiter) {
        if (mitarbeiter != null) {
            List<Long> modulIdList = getAuthorizedZielIdsForMitarbeiterAndZiel(mitarbeiter, BerechtigungZiel.MODULSETEAM)
                    .stream()
                    .map(Long::parseLong)
                    .collect(Collectors.toList());
            TypedQuery<ModulSeTeam> q = entityManager.createNamedQuery(ModulSeTeam.BY_ID_LIST, ModulSeTeam.class);
            q.setParameter("idList", modulIdList);
            return !modulIdList.isEmpty() ? q.getResultList() : new ArrayList<>();
        }
        return new ArrayList<>();
    }

    public List<String> getAuthorizedModulnamesForEcoc(Mitarbeiter mitarbeiter) {
        TypedQuery<String> q = entityManager.createNamedQuery(Berechtigung.BY_MITARBEITER_ROLLE_ZIEL_WITH_RECHTTYPE, String.class);
        q.setParameter(MITARBEITER, mitarbeiter);
        q.setParameter(ROLLE, Rolle.E_COC);
        q.setParameter("fuer", BerechtigungZiel.MODULSETEAM);
        q.setParameter(RECHTTYPE, Rechttype.SCHREIBRECHT);
        List<String> modulList = q.getResultList();

        if (modulList.isEmpty()) {
            modulList.add("0");
        }

        return modulList;
    }

    private List<String> getAuthorizedSensorCocForSensorAsString(Mitarbeiter mitarbeiter, Rolle rolle) {
        TypedQuery<String> q = entityManager.createNamedQuery(Berechtigung.BY_MITARBEITER_ROLLE_ZIEL_WITH_RECHTTYPE, String.class);
        q.setParameter(MITARBEITER, mitarbeiter);
        q.setParameter(ROLLE, rolle);
        q.setParameter("fuer", BerechtigungZiel.SENSOR_COC);
        q.setParameter(RECHTTYPE, Rechttype.SCHREIBRECHT);
        return q.getResultList();
    }

    public List<Long> getAuthorizedSensorCocForSensor(Mitarbeiter mitarbeiter) {
        List<Long> result = new ArrayList<>();
        Long sensorCocId;
        Set<String> idsForSensor = new HashSet<>();
        List<Rolle> userRoles = getRolesByQnumber(mitarbeiter.getQNumber());
        if (userRoles.contains(Rolle.SENSOR)) {
            Rolle rolle = Rolle.SENSOR;
            idsForSensor.addAll(getAuthorizedSensorCocForSensorAsString(mitarbeiter, rolle));
        }

        if (userRoles.contains(Rolle.SENSOR_EINGESCHRAENKT)) {
            Rolle rolle = Rolle.SENSOR_EINGESCHRAENKT;
            idsForSensor.addAll(getAuthorizedSensorCocForSensorAsString(mitarbeiter, rolle));
        }

        if (userRoles.contains(Rolle.SENSORCOCLEITER)) {
            Rolle rolle = Rolle.SENSORCOCLEITER;
            idsForSensor.addAll(getAuthorizedSensorCocForSensorAsString(mitarbeiter, rolle));
        }

        if (userRoles.contains(Rolle.SCL_VERTRETER)) {
            Rolle rolle = Rolle.SCL_VERTRETER;
            idsForSensor.addAll(getAuthorizedSensorCocForSensorAsString(mitarbeiter, rolle));
        }

        for (String sid : idsForSensor) {
            sensorCocId = Long.parseLong(sid);
            if (!result.contains(sensorCocId)) {
                result.add(sensorCocId);
            }
        }

        return result;
    }

    public void addBerechtigung(Mitarbeiter mitarbeiter, Rolle rolle, String fuerId, BerechtigungZiel fuer, Rechttype rechttype) {
        Berechtigung berechtigung = new Berechtigung(mitarbeiter, rolle, rechttype, fuerId, fuer);
        persistBerechtigung(berechtigung);

        if (fuer == BerechtigungZiel.DERIVAT) {
            Long derivatId = Long.parseLong(fuerId);
            BerechtigungDerivat beriDerivat = new BerechtigungDerivat(derivatId, mitarbeiter, rolle, rechttype);
            persistBerechtigungDerivat(beriDerivat);
        }
    }

    public void removeBerechtigung(Mitarbeiter mitarbeiter, Rolle rolle, String fuerId, BerechtigungZiel fuer) {
        List<Berechtigung> berechtigungList = getBerechtigungByMitarbeiterAndRolleAndZielById(mitarbeiter, rolle, fuerId, fuer);
        if (berechtigungList != null && !berechtigungList.isEmpty()) {
            Berechtigung berechtigung = berechtigungList.get(0);
            removeBerechtigung(berechtigung);

            if (fuer == BerechtigungZiel.DERIVAT) {
                Long derivatId = Long.parseLong(fuerId);
                BerechtigungDerivat beriDerivat = getBerechtigungDerivatByMitarbeiterAndRolleForDerivat(derivatId, mitarbeiter, rolle);
                removeBerechtigungDerivat(beriDerivat);
            }
        }
    }

    public void persistNewRolleForUser(UserToRole userToRole) {
        if (!userHasRole(userToRole.getqNumber(), userToRole.getRole())) {
            persistRolle(userToRole);
        }
    }

    public void persistRolle(UserToRole userToRole) {
        if (getRoleById(userToRole.getId()) != null) {
            entityManager.merge(userToRole);
        } else {
            entityManager.persist(userToRole);
        }
        entityManager.flush();
    }

    private UserToRole getRoleById(Long id) {
        TypedQuery<UserToRole> q = entityManager.createNamedQuery(UserToRole.BY_ID, UserToRole.class);
        q.setParameter("id", id);
        List<UserToRole> result = q.getResultList();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public Boolean userHasRole(Mitarbeiter mitarbeiter, Rolle rolle) {
        return userHasRole(mitarbeiter.getQNumber(), rolle.getRolleRawString());
    }

    private Boolean userHasRole(String qNumber, String rolleRawString) {
        return getRoleByQnumberAndRolle(qNumber, rolleRawString) != null;
    }

    public void removeRoleFromUser(Mitarbeiter mitarbeiter, Rolle rolle) {
        UserToRole userToRole = getRoleByQnumberAndRolle(mitarbeiter.getQNumber(), rolle.getRolleRawString());
        if (userToRole != null) {
            removeRolle(userToRole);
        }
    }

    private UserToRole getRoleByQnumberAndRolle(String qNumber, String rolleRawString) {
        TypedQuery<UserToRole> q = entityManager.createNamedQuery(UserToRole.BY_QNUMBER_ROLLE, UserToRole.class);
        q.setParameter("qnumber", qNumber);
        q.setParameter("rolename", rolleRawString);
        List<UserToRole> result = q.getResultList();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public List<Rolle> getRolesByQnumber(String qNumber) {
        List<UserToRole> roleRaw = getUserToRolesByQnumber(qNumber);
        List<Rolle> result = new ArrayList<>();
        if (roleRaw != null && !roleRaw.isEmpty()) {
            roleRaw.forEach(r -> {
                Rolle rolle = Rolle.getRolleByRawString(r.getRole());
                if (rolle != null) {
                    result.add(rolle);
                }
            });
        }
        return result;
    }

    public List<UserToRole> getUserToRolesByQnumber(String qNumber) {
        TypedQuery<UserToRole> q = entityManager.createNamedQuery(UserToRole.ALL_BY_QNUMBER, UserToRole.class);
        q.setParameter("qnumber", qNumber);
        List<UserToRole> result = q.getResultList();
        return result != null && !result.isEmpty() ? result : null;
    }

    private void removeRolle(UserToRole userToRole) {
        entityManager.remove(entityManager.merge(userToRole));
        entityManager.flush();
    }

    public void persistBerechtigung(Berechtigung berechtigung) {
        if (getBerechtigungById(berechtigung.getId()) != null) {
            entityManager.merge(berechtigung);
        } else {
            entityManager.persist(berechtigung);
        }
        entityManager.flush();
    }

    private void removeBerechtigung(Berechtigung berechtigung) {
        entityManager.remove(entityManager.merge(berechtigung));
        entityManager.flush();
    }

    private BerechtigungDerivat getBerechtigungDerivatById(Long id) {
        if (id != null) {
            TypedQuery<BerechtigungDerivat> q = entityManager.createNamedQuery(BerechtigungDerivat.BY_ID, BerechtigungDerivat.class);
            q.setParameter("id", id);
            List<BerechtigungDerivat> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }


    public BerechtigungDerivat getBerechtigungDerivatByMitarbeiterAndRolleForDerivat(Long derivatId, Mitarbeiter mitarbeiter, Rolle rolle) {
        if (derivatId != null && mitarbeiter != null && rolle != null) {
            TypedQuery<BerechtigungDerivat> q = entityManager.createNamedQuery(BerechtigungDerivat.BY_MITARBEITER_AND_ROLLE_FOR_DERIVAT, BerechtigungDerivat.class);
            q.setParameter("derivatId", derivatId);
            q.setParameter(MITARBEITER, mitarbeiter);
            q.setParameter(ROLLE, rolle);
            List<BerechtigungDerivat> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<Long> getDerivateByMitarbeiterAndRolle(Mitarbeiter mitarbeiter, Rolle rolle) {

        TypedQuery<Long> q = entityManager.createNamedQuery(BerechtigungDerivat.GET_DERIVATE_BY_MITARBEITER_AND_ROLLE, Long.class);
        q.setParameter(MITARBEITER, mitarbeiter);
        q.setParameter(ROLLE, rolle);
        List<Long> result = q.getResultList();
        return result != null && !result.isEmpty() ? result : Collections.emptyList();

    }

    private void persistBerechtigungDerivat(BerechtigungDerivat berechtigungDerivat) {
        if (getBerechtigungDerivatById(berechtigungDerivat.getId()) != null) {
            entityManager.merge(berechtigungDerivat);
        } else {
            entityManager.persist(berechtigungDerivat);
        }
        entityManager.flush();
    }

    private void removeBerechtigungDerivat(BerechtigungDerivat berechtigungDerivat) {
        entityManager.remove(entityManager.merge(berechtigungDerivat));
        entityManager.flush();
    }

    public List<Mitarbeiter> getMitarbeiterByRolleBerechtigungZielRechttypeAndFuerIdList(Rolle rolle, List<String> fuerIdList, BerechtigungZiel fuer, Rechttype rechttype) {
        TypedQuery<Mitarbeiter> q = entityManager.createNamedQuery(Berechtigung.GET_MITARBEITER_BY_FUERLIST_ROLLE_ZIEL_RECHTTYPE, Mitarbeiter.class);
        q.setParameter(ROLLE, rolle);
        q.setParameter("fuerIdList", fuerIdList);
        q.setParameter("fuer", fuer);
        q.setParameter(RECHTTYPE, rechttype);
        return fuerIdList != null && !fuerIdList.isEmpty() && fuer != null && rolle != null && rechttype != null ? q.getResultList() : new ArrayList<>();
    }

    public List<Mitarbeiter> getMitarbeiterByDerivatenInListRolleAndRechttype(List<Derivat> derivatList, Rolle rolle, Rechttype rechttype) {
        if (derivatList != null && !derivatList.isEmpty()) {
            List<Long> derivatIdList = derivatList.stream().map(Derivat::getId).collect(Collectors.toList());
            TypedQuery<Mitarbeiter> q = entityManager.createNamedQuery(BerechtigungDerivat.GET_MITARBEITER_BY_DERIVATEN_IN_LIST_ROLLE_AND_RECHTTYPE, Mitarbeiter.class);
            q.setParameter("derivaten", derivatIdList);
            q.setParameter(ROLLE, rolle);
            q.setParameter(RECHTTYPE, rechttype);
            return !derivatIdList.isEmpty() && rolle != null && rechttype != null ? q.getResultList() : new ArrayList<>();
        }
        return new ArrayList<>();
    }

    public Optional<Mitarbeiter> getTteamleiterOfTteam(Tteam tteam) {
        if (tteam != null && tteam.getId() != null) {
            List<Mitarbeiter> result = getMitarbeiterForDefinierteBerechtigungByRolleAndZielId(Rolle.T_TEAMLEITER,
                    String.valueOf(tteam.getId()), BerechtigungZiel.TTEAM, Rechttype.SCHREIBRECHT);
            return result != null && !result.isEmpty() ? Optional.of(result.get(0)) : Optional.empty();
        }
        return Optional.empty();
    }

    private Collection<Mitarbeiter> getSensorCoCVertreterForSensorCocIdAsString(String sensorCocId) {
        if (sensorCocId != null) {
            TypedQuery<Mitarbeiter> q = entityManager.createNamedQuery(Berechtigung.GET_MITARBEITER_BY_FUERID_FUER_ROLLE_RECHTTYPE, Mitarbeiter.class);
            q.setParameter(FUER_ID, sensorCocId);
            q.setParameter(ROLLE, Rolle.SCL_VERTRETER);
            q.setParameter("fuer", BerechtigungZiel.SENSOR_COC);
            q.setParameter(RECHTTYPE, Rechttype.SCHREIBRECHT);
            return q.getResultList();
        } else {
            return Collections.emptyList();
        }
    }

    private Optional<Mitarbeiter> getSensorCoCLeiterForSensorCocIdAsString(String sensorCocId) {
        if (sensorCocId != null) {
            TypedQuery<Mitarbeiter> q = entityManager.createNamedQuery(Berechtigung.GET_MITARBEITER_BY_FUERID_FUER_ROLLE_RECHTTYPE, Mitarbeiter.class);
            q.setParameter(FUER_ID, sensorCocId);
            q.setParameter(ROLLE, Rolle.SENSORCOCLEITER);
            q.setParameter("fuer", BerechtigungZiel.SENSOR_COC);
            q.setParameter(RECHTTYPE, Rechttype.SCHREIBRECHT);
            List<Mitarbeiter> result = q.getResultList();
            return result != null && !result.isEmpty() ? Optional.ofNullable(result.get(0)) : Optional.empty();
        }
        return Optional.empty();
    }

    public List<Mitarbeiter> getAnfordererForSensorCoc(SensorCoc sensorCoc) {
        if (sensorCoc != null && sensorCoc.getSensorCocId() != null) {
            TypedQuery<Mitarbeiter> q = entityManager.createNamedQuery(Berechtigung.GET_MITARBEITER_BY_FUERID_FUER_ROLLE_RECHTTYPE, Mitarbeiter.class);
            q.setParameter(FUER_ID, sensorCoc.getSensorCocId().toString());
            q.setParameter(ROLLE, Rolle.ANFORDERER);
            q.setParameter("fuer", BerechtigungZiel.SENSOR_COC);
            q.setParameter(RECHTTYPE, Rechttype.SCHREIBRECHT);
            return q.getResultList();
        }
        return new ArrayList<>();
    }

    public Collection<Mitarbeiter> getSensorCoCVertreter(SensorCoc sensorCoc) {
        if (sensorCoc != null && sensorCoc.getSensorCocId() != null) {
            return getSensorCoCVertreterForSensorCocIdAsString(sensorCoc.getSensorCocId().toString());
        }
        return Collections.emptyList();
    }

    public Optional<Mitarbeiter> getSensorCoCLeiterForSensorCoc(SensorCoc sensorCoc) {
        if (sensorCoc != null && sensorCoc.getSensorCocId() != null) {
            return getSensorCoCLeiterForSensorCocIdAsString(sensorCoc.getSensorCocId().toString());
        }
        return Optional.empty();
    }

    public Berechtigung getByFuerIdFuerRolleRechttype(String fuerId, Rolle rolle, BerechtigungZiel fuer, Rechttype rechttype) {
        Query q = entityManager.createNamedQuery(Berechtigung.GET_BY_FUERID_FUER_ROLLE_RECHTTYPE, Berechtigung.class);
        q.setParameter(FUER_ID, fuerId);
        q.setParameter(ROLLE, rolle);
        q.setParameter("fuer", fuer);
        q.setParameter(RECHTTYPE, rechttype);
        List<Berechtigung> result = fuerId != null && !fuerId.isEmpty() && rolle != null && rechttype != null && fuer != null ? q.getResultList() : new ArrayList<>();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public List<String> getBerechtigungIdsForUserAndBerechtigungZiel(Mitarbeiter user, BerechtigungZiel fuer) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<String> cq = cb.createQuery(String.class);
        Root<Berechtigung> berechtigung = cq.from(Berechtigung.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition;

        if (user != null) {
            whereCondition = cb.equal(berechtigung.<Mitarbeiter>get(MITARBEITER), user);
            whereConditions.add(whereCondition);
        }

        if (fuer != null) {
            whereCondition = cb.equal(berechtigung.<Enum>get("fuer"), fuer);
            whereConditions.add(whereCondition);
        }

        //Constructing ORDER BY Clause
        List<Order> orderList = new ArrayList<>();
        orderList.add(cb.desc(berechtigung.get("id")));

        cq.select(berechtigung.get(FUER_ID))
                .where(whereConditions.toArray(new Predicate[] {}))
                .orderBy(orderList)
                .distinct(true);

        return entityManager.createQuery(cq).getResultList();
    }

}
