package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungVersionMenuViewData;
import de.interfaceag.bmw.pzbk.entities.AbstractAnforderung;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author evda
 */
@Dependent
public class AnforderungDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    @Inject
    private BerechtigungService berechtigungService;

    public Long persistAnforderung(Anforderung anforderung) {
        if (getAnforderungById(anforderung.getId()) != null) {
            entityManager.merge(anforderung);
        } else {
            entityManager.persist(anforderung);
        }
        return anforderung.getId();
    }

    public Long saveAnforderung(Anforderung anforderung) {
        Long savedAnfoID;
        if (anforderung.getFachId() == null || anforderung.getFachId().equals("")) {
            anforderung.setStatus(Status.A_INARBEIT);
            persistAnforderung(anforderung);
            anforderung.setFachId("A" + anforderung.getId());
            savedAnfoID = persistAnforderung(anforderung);
        } else {
            savedAnfoID = persistAnforderung(anforderung);
        }
        entityManager.flush();
        return savedAnfoID;
    }

    public void clearEntityManager() {
        entityManager.clear();
    }

    public Long persistMeldung(Meldung meldung) {
        if (getMeldungById(meldung.getId()) != null) {
            entityManager.merge(meldung);
        } else {
            entityManager.persist(meldung);
        }
        return meldung.getId();
    }

    public Long saveMeldung(Meldung meldung) {
        Long savedMeldungID;
        if (meldung.getFachId() == null || meldung.getFachId().equals("")) {
            meldung.setStatus(Status.M_ENTWURF);
            persistMeldung(meldung);
            meldung.setFachId("M" + meldung.getId());
            savedMeldungID = persistMeldung(meldung);
        } else {
            savedMeldungID = persistMeldung(meldung);
        }
        entityManager.flush();
        return savedMeldungID;
    }

    public Integer getNextVersionNumber(String fachId) {
        if (fachId != null && !"".equals(fachId)) {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
            Root<Anforderung> anforderung = cq.from(Anforderung.class);

            //Constructing WHERE Clause
            List<Predicate> whereConditions = new ArrayList<>();
            Predicate whereCondition = cb.like(anforderung.<String>get("fachId"), fachId);
            whereConditions.add(whereCondition);

            cq.select(cb.max(anforderung.<Integer>get("version")))
                    .where(whereConditions.toArray(new Predicate[]{}))
                    .distinct(true);

            Integer currentVersionNumberForFachId = entityManager.createQuery(cq).getSingleResult();
            return currentVersionNumberForFachId + 1;
        }
        return 1;
    }

    public Anforderung getAnforderungById(Long id) {
        if (id != null) {
            Query q = entityManager.createNamedQuery(Anforderung.BY_ID, Anforderung.class);
            q.setParameter("id", id);
            List<Anforderung> result = q.getResultList();
            return !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<Anforderung> getAllAnforderungenByStatus(Status status) {
        Query q = entityManager.createNamedQuery(Anforderung.ANY_BY_STATUS, Anforderung.class);
        q.setParameter("status", status);
        return status != null ? q.getResultList() : new ArrayList<>();
    }

    public Anforderung getAnforderungByFachIdVersion(String fachId, Integer version) {

        if (fachId == null || fachId.isEmpty() || version == null) {
            return null;
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Anforderung> cq = cb.createQuery(Anforderung.class);
        Root<Anforderung> anforderung = cq.from(Anforderung.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition = cb.like(anforderung.<String>get("fachId"), fachId);
        whereConditions.add(whereCondition);

        whereCondition = cb.equal(anforderung.<Integer>get("version"), version);
        whereConditions.add(whereCondition);

        cq.select(anforderung)
                .where(whereConditions.toArray(new Predicate[]{}))
                .distinct(true);

        List<Anforderung> result = entityManager.createQuery(cq).getResultList();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public Long countAnforderungenForFestgestelltIn(FestgestelltIn festgestelltIn) {

        if (festgestelltIn == null) {
            return 0L;
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Anforderung> anforderung = cq.from(Anforderung.class);
        anforderung.fetch("festgestelltIn");

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition = cb.equal(anforderung.<FestgestelltIn>get("festgestelltIn"), festgestelltIn);
        whereConditions.add(whereCondition);

        cq.select(anforderung.get("id"))
                .where(whereConditions.toArray(new Predicate[]{}))
                .distinct(true);

        List<Long> foundIds = entityManager.createQuery(cq).getResultList();
        if (foundIds == null || foundIds.isEmpty()) {
            return 0L;
        }

        Integer foundAmount = foundIds.size();
        return Long.parseLong(String.valueOf(foundAmount));
    }

    public Long getMeldungByIdAndSensorCocList(Long id, List<Long> sensorCocList) {

        if (id == null || sensorCocList == null || sensorCocList.isEmpty()) {
            return null;
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Meldung> meldung = cq.from(Meldung.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition = cb.equal(meldung.<Long>get("id"), id);
        whereConditions.add(whereCondition);

        whereCondition = meldung.get("sensorCoc").get("sensorCocId").in(sensorCocList);
        whereConditions.add(whereCondition);

        cq.select(meldung.get("id"))
                .where(whereConditions.toArray(new Predicate[]{}))
                .distinct(true);

        List<Long> result = entityManager.createQuery(cq).getResultList();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public Long getMeldungIdByFachIdAndSensorCocList(String fachId, List<Long> sensorCocList) {

        if (fachId == null || fachId.isEmpty() || sensorCocList == null || sensorCocList.isEmpty()) {
            return null;
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Meldung> meldung = cq.from(Meldung.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition = cb.like(meldung.get("fachId"), fachId);
        whereConditions.add(whereCondition);

        whereCondition = meldung.get("sensorCoc").get("sensorCocId").in(sensorCocList);
        whereConditions.add(whereCondition);

        cq.select(meldung.get("id"))
                .where(whereConditions.toArray(new Predicate[]{}))
                .distinct(true);

        List<Long> result = entityManager.createQuery(cq).getResultList();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public List<Meldung> getMeldungenBySensorCocAndStatus(List<SensorCoc> sensorCocList, List<Status> status) {

        if (sensorCocList == null || sensorCocList.isEmpty() || status == null || status.isEmpty()) {
            return new ArrayList<>();
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Meldung> cq = cb.createQuery(Meldung.class);
        Root<Meldung> meldung = cq.from(Meldung.class);
        meldung.fetch("sensorCoc");

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition = meldung.get("sensorCoc").in(sensorCocList);
        whereConditions.add(whereCondition);

        whereCondition = meldung.get("status").in(status);
        whereConditions.add(whereCondition);

        cq.select(meldung)
                .where(whereConditions.toArray(new Predicate[]{}))
                .distinct(true);

        return entityManager.createQuery(cq).getResultList();
    }

    public List<Meldung> getMeldungenBySensorCocAndStatusNotInIdList(List<SensorCoc> sensorCocList, List<Status> status, List<Long> idList) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Meldung> cq = cb.createQuery(Meldung.class);
        Root<Meldung> meldung = cq.from(Meldung.class);
        meldung.fetch("sensorCoc");

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition;

        if (sensorCocList != null && !sensorCocList.isEmpty()) {
            whereCondition = meldung.get("sensorCoc").in(sensorCocList);
            whereConditions.add(whereCondition);
        }

        if (status != null && !status.isEmpty()) {
            whereCondition = meldung.get("status").in(status);
            whereConditions.add(whereCondition);
        }

        if (idList != null && !idList.isEmpty()) {
            whereCondition = cb.not(meldung.get("id").in(idList));
            whereConditions.add(whereCondition);
        }

        //Constructing ORDER BY Clause
        List<Order> orderList = new ArrayList();
        orderList.add(cb.desc(meldung.<String>get("fachId")));

        cq.select(meldung)
                .where(whereConditions.toArray(new Predicate[]{}))
                .orderBy(orderList)
                .distinct(true);
        return entityManager.createQuery(cq).getResultList();
    }

    public Meldung getMeldungById(Long id) {
        if (id != null) {
            Query q = entityManager.createNamedQuery(Meldung.BY_ID, Meldung.class);
            q.setParameter("id", id);
            List<Meldung> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public List<Anforderung> getAllAnforderung() {
        return entityManager.createNamedQuery(Anforderung.ALL, Anforderung.class).getResultList();
    }

    public List<Anforderung> getAllAnforderungenWithMeldung() {
        return entityManager.createNamedQuery(Anforderung.ALL_WITH_MELDUNG, Anforderung.class).getResultList();
    }

    public List<Meldung> getAllMeldungen() {
        return entityManager.createNamedQuery(Meldung.ALL, Meldung.class).getResultList();
    }

    public List<Meldung> getMeldungByFachIdStartingWith(String fachId) {

        if (fachId == null || fachId.isEmpty()) {
            return new ArrayList<>();
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Meldung> cq = cb.createQuery(Meldung.class);
        Root<Meldung> meldung = cq.from(Meldung.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition = cb.like(meldung.<String>get("fachId"), cb.parameter(String.class, "likeCondition"));
        whereConditions.add(whereCondition);

        cq.select(meldung)
                .where(whereConditions.toArray(new Predicate[]{}))
                .distinct(true);

        TypedQuery<Meldung> tq = entityManager.createQuery(cq);
        String searchFor = fachId + "%";
        tq.setParameter("likeCondition", searchFor);

        return tq.getResultList();
    }

    public List<Anforderung> fetchVersionDataForFachId(String fachId) {

        if (fachId == null || fachId.isEmpty()) {
            return new ArrayList<>();
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Anforderung> cq = cb.createQuery(Anforderung.class);
        Root<Anforderung> anforderung = cq.from(Anforderung.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition = cb.like(anforderung.<String>get("fachId"), fachId);
        whereConditions.add(whereCondition);

        List<Order> orderList = new ArrayList<>();
        orderList.add(cb.asc(anforderung.<Integer>get("version")));

        cq.select(anforderung)
                .where(whereConditions.toArray(new Predicate[]{}))
                .orderBy(orderList)
                .distinct(true);

        return entityManager.createQuery(cq).getResultList();
    }

    public List<AnforderungVersionMenuViewData> fetchVersionViewDataForFachId(String fachId) {

        if (fachId == null || fachId.isEmpty()) {
            return new ArrayList<>();
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Anforderung> cq = cb.createQuery(Anforderung.class);
        Root<Anforderung> anforderung = cq.from(Anforderung.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition = cb.like(anforderung.<String>get("fachId"), fachId);
        whereConditions.add(whereCondition);

        List<Order> orderList = new ArrayList<>();
        orderList.add(cb.asc(anforderung.<Integer>get("version")));

        cq.select(anforderung)
                .where(whereConditions.toArray(new Predicate[]{}))
                .orderBy(orderList)
                .distinct(true);

        List<Anforderung> anforderungen = entityManager.createQuery(cq).getResultList();
        return anforderungen.stream().map(a -> new AnforderungVersionMenuViewData(a.getFachId(), a.getVersion(), a.getErstellungsdatum())).collect(Collectors.toList());
    }

    public List<Anforderung> getAllAnforderungenWithThisMeldung(Meldung meldung) {
        if (meldung != null && meldung.getId() != null) {
            Query q = entityManager.createNamedQuery(Meldung.GET_ALL_ANFORDERUNGEN, Anforderung.class);
            q.setParameter("id", meldung.getId());
            return q.getResultList();
        }
        return new ArrayList<>();
    }

    public Anforderung getUniqueAnforderungByFachId(String fachId) {

        if (fachId == null || fachId.isEmpty()) {
            return null;
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Anforderung> cq = cb.createQuery(Anforderung.class);
        Root<Anforderung> anforderung = cq.from(Anforderung.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition = cb.like(anforderung.<String>get("fachId"), fachId);
        whereConditions.add(whereCondition);

        cq.select(anforderung)
                .where(whereConditions.toArray(new Predicate[]{}))
                .distinct(true);

        List<Anforderung> result = entityManager.createQuery(cq).getResultList();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public Meldung getUniqueMeldungByFachId(String fachId) {

        if (fachId == null || fachId.isEmpty()) {
            return null;
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Meldung> cq = cb.createQuery(Meldung.class);
        Root<Meldung> meldung = cq.from(Meldung.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition = cb.like(meldung.get("fachId"), fachId);
        whereConditions.add(whereCondition);

        cq.select(meldung)
                .where(whereConditions.toArray(new Predicate[]{}))
                .distinct(true);

        List<Meldung> result = entityManager.createQuery(cq).getResultList();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public AbstractAnforderung getUniqueAnforderungOrMeldungByFachIdEndingWith(Long fachIdLong) {
        String fachId = "A" + fachIdLong;
        Anforderung anfo = getUniqueAnforderungByFachId(fachId);
        // Search first among Anforderungen and if no match found search among Meldungen
        if (anfo == null) {
            fachId = "M" + fachIdLong;
            Meldung mel = getUniqueMeldungByFachId(fachId);
            if (mel == null) {
                return null;
            } else {
                return (AbstractAnforderung) mel;
            }
        } else {
            return (AbstractAnforderung) anfo;
        }
    }

    public Meldung getMeldungForIdAndSensor(Long id, Mitarbeiter mitarbeiter) {

        if (id == null || mitarbeiter == null) {
            return null;
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Meldung> cq = cb.createQuery(Meldung.class);
        Root<Meldung> meldung = cq.from(Meldung.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition = cb.equal(meldung.<Long>get("id"), id);
        whereConditions.add(whereCondition);

        whereCondition = cb.equal(meldung.<Mitarbeiter>get("sensor"), mitarbeiter);
        whereConditions.add(whereCondition);

        cq.select(meldung)
                .where(whereConditions.toArray(new Predicate[]{}))
                .distinct(true);

        List<Meldung> result = entityManager.createQuery(cq).getResultList();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public Meldung getMeldungForIdAndSensorCoc(Long id, Mitarbeiter mitarbeiter) {

        if (id == null || mitarbeiter == null) {
            return null;
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Meldung> cq = cb.createQuery(Meldung.class);
        Root<Meldung> meldung = cq.from(Meldung.class);

        //Constructing WHERE Clause
        List<Predicate> whereConditions = new ArrayList<>();
        Predicate whereCondition = cb.equal(meldung.<Long>get("id"), id);
        whereConditions.add(whereCondition);

        List<String> sensorCocIds = berechtigungService.getBerechtigungIdsForUserAndBerechtigungZiel(mitarbeiter, BerechtigungZiel.SENSOR_COC);
        if (sensorCocIds == null || sensorCocIds.isEmpty()) {
            return null;
        }

        List<Long> sensorCocIdList = new ArrayList<>();
        for (String scid : sensorCocIds) {
            Long scIdAsLong = Long.parseLong(scid);
            if (!sensorCocIdList.contains(scIdAsLong)) {
                sensorCocIdList.add(scIdAsLong);
            }
        }

        whereCondition = meldung.get("sensorCoc").get("sensorCocId").in(sensorCocIdList);
        whereConditions.add(whereCondition);

        cq.select(meldung)
                .where(whereConditions.toArray(new Predicate[]{}))
                .distinct(true);

        List<Meldung> result = entityManager.createQuery(cq).getResultList();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    public Anforderung getAnforderungForIdAndBerechtigung(Long id, Mitarbeiter mitarbeiter, List<Rolle> mitarbeiterRollen) {

        Anforderung wantedAnforderung = this.getAnforderungById(id);
        if (wantedAnforderung == null) {
            return null;
        }

        if (mitarbeiterRollen.contains(Rolle.SENSOR) || mitarbeiterRollen.contains(Rolle.SENSORCOCLEITER)
                || mitarbeiterRollen.contains(Rolle.SCL_VERTRETER) || mitarbeiterRollen.contains(Rolle.SENSOR_EINGESCHRAENKT)
                || mitarbeiterRollen.contains(Rolle.UMSETZUNGSBESTAETIGER)) {
            if (checkForSensorCocBerechtigung(mitarbeiter, wantedAnforderung)) {
                return wantedAnforderung;
            }
        }
        if (mitarbeiterRollen.contains(Rolle.E_COC)) {
            if (checkForModulSeTeamBerechtigung(mitarbeiter, wantedAnforderung)) {
                return wantedAnforderung;
            }
        }
        return null;
    }

    private boolean checkForModulSeTeamBerechtigung(Mitarbeiter mitarbeiter, Anforderung wantedAnforderung) {
        List<String> modulseteamIds = berechtigungService.getBerechtigungIdsForUserAndBerechtigungZiel(mitarbeiter, BerechtigungZiel.MODULSETEAM);
        List<AnforderungFreigabeBeiModulSeTeam> anfoSeteams = wantedAnforderung.getAnfoFreigabeBeiModul();
        String seteamId;
        for (AnforderungFreigabeBeiModulSeTeam afm : anfoSeteams) {
            seteamId = afm.getModulSeTeam().getId().toString();
            if (modulseteamIds.contains(seteamId)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkForSensorCocBerechtigung(Mitarbeiter mitarbeiter, Anforderung wantedAnforderung) {
        List<String> sensorCocIds = berechtigungService.getBerechtigungIdsForUserAndBerechtigungZiel(mitarbeiter, BerechtigungZiel.SENSOR_COC);
        SensorCoc anfoSensorCoc = wantedAnforderung.getSensorCoc();
        String sensorCocId = anfoSensorCoc.getSensorCocId().toString();
        return sensorCocIds.contains(sensorCocId);
    }

    public List<Anforderung> getAnforderungenByIdList(List<Long> idList) {
        Query q = entityManager.createNamedQuery(Anforderung.BY_ID_LIST, Anforderung.class);
        q.setParameter("idList", idList);
        return idList != null && !idList.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<Meldung> getMeldungenByIdList(List<Long> idList) {
        Query q = entityManager.createNamedQuery(Meldung.BY_ID_LIST, Meldung.class);
        q.setParameter("idList", idList);
        return idList != null && !idList.isEmpty() ? q.getResultList() : new ArrayList<>();
    }

    public List<Anforderung> getAnforderungenByKomponenteId(Long komponenteId) {
        Query q = entityManager.createNamedQuery(Anforderung.BY_KOMPONENTE, Anforderung.class);
        q.setParameter("id", komponenteId);
        return komponenteId != null ? q.getResultList() : new ArrayList<>();
    }

    public List<Anforderung> getAnforderungenBySeTeamName(String seTeamName) {
        Query q = entityManager.createNamedQuery(Anforderung.BY_SETEAMNAME, Anforderung.class);
        q.setParameter("seTeamName", seTeamName);
        return seTeamName != null ? q.getResultList() : new ArrayList<>();
    }

    public List<Object[]> getProzessbaukastenLinksForAnforderung(Long anforderungId) {
        String queryString = "SELECT p.fachId, p.version, p.bezeichnung FROM Anforderung a INNER JOIN FETCH a.prozessbaukasten p WHERE a.id = :id";
        Query q = entityManager.createQuery(queryString);
        q.setParameter("id", anforderungId);
        List<Object[]> queryResult = q.getResultList();
        return (queryResult != null) ? queryResult : new ArrayList<>();
    }

    public List<Anforderung> getPreviousVersionsWithGueltigenProzessbaukasten(Anforderung anforderung) {
        String fachId = anforderung.getFachId();
        int prozessbaukastenStatus = ProzessbaukastenStatus.GUELTIG.getId();

        Query q = entityManager.createNamedQuery(Anforderung.GET_OTHER_VERSIONS_BY_FACHID_AND_PROZESSBAUKASTEN_IN_STATUS, Anforderung.class);

        q.setParameter("fachId", fachId);
        q.setParameter("prozessbaukastenStatus", prozessbaukastenStatus);
        List<Anforderung> result = q.getResultList();
        return result;
    }

    public List<Anforderung> getVorigeVersionenForAnforderung(Anforderung anforderung) {
        if (anforderung == null) {
            return new ArrayList<>();
        }

        String fachId = anforderung.getFachId();
        Integer version = anforderung.getVersion();

        Query q = entityManager.createNamedQuery(Anforderung.GET_VORIGE_VERSIONEN, Anforderung.class);
        q.setParameter("fachId", fachId);
        q.setParameter("version", version);

        List<Anforderung> result = q.getResultList();
        return result;
    }

}
