package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.InfoText;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author evda
 */
@Dependent
public class InfoTextDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager em;

    public void persistInfoText(InfoText infoText) {
        if (getInfoTextById(infoText.getId()) != null) {
            em.merge(infoText);
        } else {
            em.persist(infoText);
        }
        em.flush();
    }

    public InfoText getInfoTextById(Long id) {
        if (id != null) {
            Query q = em.createNamedQuery(InfoText.BY_ID, InfoText.class);
            q.setParameter("id", id);
            List<InfoText> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public InfoText getInfoTextByFeldName(String feldName) {
        if (feldName != null && !feldName.isEmpty()) {
            Query q = em.createNamedQuery(InfoText.BY_FELDNAME, InfoText.class);
            q.setParameter("feldName", feldName);
            List<InfoText> result = q.getResultList();
            return result != null && !result.isEmpty() ? result.get(0) : null;
        }
        return null;
    }

    public Collection<InfoText> getInfoTextByFeldNames(Collection<String> feldNames) {
        Query q = em.createNamedQuery(InfoText.BY_FELDNAMES, InfoText.class);
        q.setParameter("feldNames", feldNames);
        return feldNames != null && !feldNames.isEmpty() ? q.getResultList() : Collections.emptyList();
    }

    public List<InfoText> getAllInfoTexte() {
        return em.createNamedQuery(InfoText.ALL, InfoText.class).getResultList();
    }
}
