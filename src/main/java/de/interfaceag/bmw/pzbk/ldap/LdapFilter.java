package de.interfaceag.bmw.pzbk.ldap;

import java.io.Serializable;

public final class LdapFilter implements Serializable {

    private String givenname;
    private String surname;
    private String departmentNumer;

    private LdapFilter() {
    }

    public static LdapFilter newInstance() {
        return new LdapFilter();
    }

    public LdapFilter withGivenname(String givenname) {
        this.givenname = givenname;
        return this;
    }

    public LdapFilter withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public LdapFilter withDepartmentNumber(String departmentNumer) {
        this.departmentNumer = departmentNumer;
        return this;
    }

    public String getLdapFilterQuery() {
        StringBuilder stringBuilder = new StringBuilder("(&");

        getFilterQueryForGivenname(stringBuilder);
        getFilterQueryForSurname(stringBuilder);
        getFilterQueryForDepartmentNumber(stringBuilder);

        stringBuilder.append(")");

        return stringBuilder.toString();
    }

    private void getFilterQueryForDepartmentNumber(StringBuilder stringBuilder) {
        getFilterQueryForParameter(stringBuilder, "(departmentnumber=", departmentNumer);
    }

    private void getFilterQueryForSurname(StringBuilder stringBuilder) {
        getFilterQueryForParameter(stringBuilder, "(sn=", surname);
    }

    private void getFilterQueryForGivenname(StringBuilder stringBuilder) {
        getFilterQueryForParameter(stringBuilder, "(givenname=", givenname);
    }

    private void getFilterQueryForParameter(StringBuilder stringBuilder, String ldapParameter, String parameterValue) {
        if (parameterValue != null && !parameterValue.isEmpty()) {
            stringBuilder.append(ldapParameter).append(parameterValue).append(")");
        }
    }


}
