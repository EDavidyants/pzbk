package de.interfaceag.bmw.pzbk.localization;

import de.interfaceag.bmw.pzbk.exceptions.NotSupportedException;
import de.interfaceag.bmw.pzbk.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Named
@Stateless
public class LocalizationService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(LocalizationService.class);

    @Inject
    protected Session session;

    protected static Stream<Locale> getSupportedLocales() {
        return Stream.of(
                Locale.GERMAN,
                Locale.ENGLISH
        );
    }

    public static String getGermanValue(String key) {
        return getValue(Locale.GERMAN, key);
    }

    public static String getValue(Locale currentLocale, String key) {
        try {
            isCurrentLocaleSupported(currentLocale);
            ResourceBundle labels = ResourceBundle.getBundle("de.interfaceag/pzbk", currentLocale);
            String value = labels.getString(key);
            return value;
        } catch (NotSupportedException ex) {
            LOG.warn(ex.toString(), ex);
            if (!currentLocale.equals(Locale.GERMAN)) {
                return getValue(Locale.GERMAN, key);
            } else {
                return "";
            }
        } catch (MissingResourceException ex) {
            LOG.warn("Key ''{}'' f\u00fcr Locale {} ({}) nicht vorhanden!", new Object[]{key, currentLocale.getDisplayName(), currentLocale.toLanguageTag()}, ex);
            if (!currentLocale.equals(Locale.GERMAN)) {
                return getValue(Locale.GERMAN, key);
            } else {
                return "Key '" + key + "' für Locale " + currentLocale.toLanguageTag() + " nicht vorhanden!";
            }
        }
    }

    public String getValue(String key) {
        Locale currentLocale = getCurrentLocale();
        return getValue(currentLocale, key);
    }

    static void isCurrentLocaleSupported(Locale currentLocale) throws NotSupportedException {
        if (getSupportedLocales().noneMatch(l -> l.equals(currentLocale))) {
            throw new NotSupportedException("Locale " + currentLocale.toString() + " is currently not supported!");
        }
    }

    public Locale getCurrentLocale() {

        String location = session.getUser().getLocation();
        switch (location) {
            case "EN":
                return Locale.ENGLISH;
            case "DE":
            default:
                return Locale.GERMAN;

        }
    }

}
