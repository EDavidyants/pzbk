package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author evda
 */
@Named
@Stateless
public class VereinbarungProcessService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(VereinbarungProcessService.class);

    @Inject
    private Session session;

    @Inject
    private VereinbarungService vereinbarungService;

    public void processChanges(VereinbarungViewData viewData) {

        Mitarbeiter currentUser = session.getUser();

        List<VereinbarungDto> selectedTableData;
        if (viewData.isAllSelected()) {
            selectedTableData = viewData.getTableData();
            LOG.info("Vereinbarung: Alle Ausgewaehlt Option for Derivat " + viewData.getSelectedDerivat().getName() + " aktiviert. Total processing: " + selectedTableData.size());
        } else {
            selectedTableData = viewData.getSelectedTableData();
            LOG.info("Vereinbarung: nicht alle Elemente for Derivat " + viewData.getSelectedDerivat().getName() + " wurden ausgewaehlt. Total ausgewaehlt: " + selectedTableData.size());
        }

        vereinbarungService.processVereinbarungBatchChanges(selectedTableData, currentUser, viewData.getGroupStatus(), viewData.getSelectedDerivat());

    }

}
