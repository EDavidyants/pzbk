package de.interfaceag.bmw.pzbk.vereinbarung.history;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DerivatAnforderungModulHistoryDto implements Serializable {

    private final long derivatAnforderungModulId;
    private final Date date;
    private final String status;
    private final String mitarbeiter;
    private final String kommentar;

    public DerivatAnforderungModulHistoryDto(long derivatAnforderungModulId, Date date, String status, String mitarbeiter, String kommentar) {
        this.derivatAnforderungModulId = derivatAnforderungModulId;
        if (date != null) {
            this.date = new Date(date.getTime());
        } else {
            this.date = null;
        }
        this.status = status;
        this.mitarbeiter = mitarbeiter;
        this.kommentar = kommentar;
    }

    public long getDerivatAnforderungModulId() {
        return derivatAnforderungModulId;
    }

    public String getKommentar() {
        return kommentar;
    }

    public Date getDate() {
        if (date != null) {
            return new Date(date.getTime());
        }
        return null;
    }

    public String getStatus() {
        return status;
    }

    public String getMitarbeiter() {
        return mitarbeiter;
    }

}
