package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.filter.UrlFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author fp
 */
public final class VereinbarungUrlParameterUtils {

    private VereinbarungUrlParameterUtils() {
    }

    public static Optional<Derivat> getSelectedDerivat(UrlParameter urlParameter, UrlFilter urlFilter, List<Derivat> allDerivate) {
        Optional<String> parameterValue = urlParameter.getValue(urlFilter.getParameterName());
        if (parameterValue.isPresent()) {
            String value = parameterValue.get();
            if (RegexUtils.matchesId(value)) {
                Long derivatId = Long.parseLong(value);
                return allDerivate.stream().filter(d -> d.getId().equals(derivatId)).findAny();
            }
        }
        return Optional.empty();
    }

    private static boolean parseParameterValue(String value) {
        return "true".equals(value);
    }
}
