package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModulHistory;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Umsetzungsbestaetigung;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.shared.dto.ProcessResultDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungService;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.DerivatVereinbarungDto;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author sl
 */
@Stateless
@Named
public class VereinbarungService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(VereinbarungService.class);

    @Inject
    protected DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;
    @Inject
    protected VereinbarungHistoryService vereinbarungHistoryService;
    @Inject
    protected LocalizationService localizationService;
    @Inject
    private VereinbarungStatusChangeKommentarService vereinbarungStatusChangeKommentarService;

    @Deprecated
    public List<DerivatAnforderungModulHistory> getHistoryForDerivatAnforderungModul(DerivatAnforderungModul da) {
        return vereinbarungHistoryService.getHistoryForDerivatAnforderungModul(da);
    }

    @Deprecated
    public List<DerivatAnforderungModulHistory> getHistoryForDerivatAnforderungModul(Long derivatAnforderungModulId) {
        DerivatAnforderungModul derivatAnforderungModulById = derivatAnforderungModulService.getDerivatAnforderungModulById(derivatAnforderungModulId);
        return vereinbarungHistoryService.getHistoryForDerivatAnforderungModul(derivatAnforderungModulById);
    }

    public ProcessResultDto processVereinbarungRowChanges(VereinbarungDto vereinbarung, Derivat derivat, Mitarbeiter currentUser) {
        // todo add group status and separate methods for batch and single change

        DerivatVereinbarungDto vereinbarungForDerivat = vereinbarung.getVereinbarungForDerivat(derivat.getId());

        DerivatAnforderungModulStatus newStatus = vereinbarungForDerivat.isDirty() && vereinbarungForDerivat.isValid() ? vereinbarungForDerivat.getNewVereinbarungStatus() : null;

        if (newStatus == null) {
            LOG.debug("New status for vereinbarung {} is null!", newStatus);
            return new ProcessResultDto(false, "status is not unique is invalid");
        } else {

            LOG.debug("New status for vereinbarung {}: {}", vereinbarung, newStatus);

            DerivatAnforderungModul derivatAnforderungModul = derivatAnforderungModulService.getDerivatAnforderungModulById(vereinbarungForDerivat.getDerivatAnforderungModulId());
            List<VereinbarungDto> vereinbarungData = Arrays.asList(vereinbarung);
            String kommentar;

            switch (newStatus) {

                case ABZUSTIMMEN:
                case UNZUREICHENDE_ANFORDERUNGSQUALITAET:
                case IN_KLAERUNG:
                case NICHT_BEWERTBAR:
                case ANGENOMMEN:
                    kommentar = vereinbarungStatusChangeKommentarService.getStatusChangeKommentar(derivatAnforderungModul, vereinbarungData, newStatus);
                    derivatAnforderungModulService.
                            updateDerivatAnforderungModulStatus(newStatus, kommentar, derivatAnforderungModul.getDerivat(),
                                    derivatAnforderungModul.getAnforderung(), derivatAnforderungModul.getModul(), currentUser);
                    updateDerivatVereinbarungDtoKommentar(derivatAnforderungModul.getId(), kommentar, vereinbarungData);
                    break;
                case KEINE_WEITERVERFOLGUNG:
                    kommentar = vereinbarungStatusChangeKommentarService.getStatusChangeKommentar(derivatAnforderungModul, vereinbarungData, newStatus);
                    derivatAnforderungModulService.updateDerivatAnforderungModulStatus(newStatus, kommentar, derivatAnforderungModul.getDerivat(),
                            derivatAnforderungModul.getAnforderung(), derivatAnforderungModul.getModul(), currentUser);
                    updateDerivatVereinbarungDtoKommentar(derivatAnforderungModul.getId(), kommentar, vereinbarungData);

                    // TODO write kommentar only once
                    if (derivatAnforderungModul.getUmsetzungsBestaetigungStatus() != null) {
                        kommentar = vereinbarungStatusChangeKommentarService.getStatusChangeKommentar(derivatAnforderungModul, vereinbarungData, newStatus);
                        setUmsetzungsbestaetigungStatusToKeineWeitervervfolgungForDerivatAnforderungModul(kommentar, derivatAnforderungModul, currentUser);
                    }
                    break;
                default:
                    LOG.debug("Statuschange for vereinbarung {} is invalid", derivatAnforderungModul);
                    return new ProcessResultDto(false, newStatus.toString() + " is not supported!");
            }

            vereinbarungForDerivat.setCurrentVereinbarungStatus(newStatus);
            vereinbarungForDerivat.resetStatusChangeKommentar();

        }
        return new ProcessResultDto(true);
    }

    //TODO Refactoring das hier nicht zwei mal das selbe gemacht wird sondern aus beiden Row und Batch die selbe Methode aufgerufen wird
    public ProcessResultDto processVereinbarungBatchChanges(List<VereinbarungDto> vereinbarungData, Mitarbeiter currentUser, DerivatAnforderungModulStatus groupStatus, Derivat derivat) {

        if (groupStatus == null) {
            LOG.debug("Group status is null!");
            return new ProcessResultDto(false, "groupStatus is null");
        } else {

            LOG.debug("New status for vereinbarung {} to {}", vereinbarungData, groupStatus);

            List<DerivatVereinbarungDto> derivatVereinbarungen = vereinbarungData.stream()
                    .flatMap(v -> v.getDerivatVereinbarungen().stream())
                    .filter(dv -> dv.getDerivatId().equals(derivat.getId().toString()))
                    .collect(Collectors.toList());

            derivatVereinbarungen.forEach(dv -> dv.setNewVereinbarungStatus(groupStatus));

            List<Long> derivatAnforderungModulIds = derivatVereinbarungen.stream().filter(dv -> dv.isValid() && dv.getDerivatAnforderungModulId() != null)
                    .map(DerivatVereinbarungDto::getDerivatAnforderungModulId).collect(Collectors.toList());

            List<DerivatAnforderungModul> derivatAnforderungModulList = derivatAnforderungModulService.getDerivatAnforderungModulByIdList(derivatAnforderungModulIds);

            switch (groupStatus) {

                case ABZUSTIMMEN:
                case NICHT_BEWERTBAR:
                case ANGENOMMEN:
                case IN_KLAERUNG:
                case UNZUREICHENDE_ANFORDERUNGSQUALITAET:
                    derivatAnforderungModulList.forEach(dam -> {
                        String kommentar = vereinbarungStatusChangeKommentarService.getStatusChangeKommentar(dam, vereinbarungData, groupStatus);
                        derivatAnforderungModulService.updateDerivatAnforderungModulStatus(groupStatus, kommentar, dam.getDerivat(),
                                dam.getAnforderung(), dam.getModul(), currentUser);
                        updateDerivatVereinbarungDtoKommentar(dam.getId(), kommentar, vereinbarungData);
                    });
                    break;
                case KEINE_WEITERVERFOLGUNG:
                    derivatAnforderungModulList
                            .forEach(dam -> {
                                String kommentar = vereinbarungStatusChangeKommentarService.getStatusChangeKommentar(dam, vereinbarungData, groupStatus);
                                derivatAnforderungModulService.updateDerivatAnforderungModulStatus(groupStatus, kommentar, dam.getDerivat(),
                                        dam.getAnforderung(), dam.getModul(), currentUser);
                                updateDerivatVereinbarungDtoKommentar(dam.getId(), kommentar, vereinbarungData);
                            });

                    // TODO write kommentar only once
                    derivatAnforderungModulList.stream()
                            .filter(dam -> dam.getUmsetzungsBestaetigungStatus() != null)
                            .forEach(dam -> {
                                String kommentar = vereinbarungStatusChangeKommentarService.getStatusChangeKommentar(dam, vereinbarungData, groupStatus);
                                setUmsetzungsbestaetigungStatusToKeineWeitervervfolgungForDerivatAnforderungModul(kommentar, dam, currentUser);
                            });
                    break;
                default:
                    LOG.debug("Statuschange for vereinbarungen to {} is invalid", groupStatus);
                    return new ProcessResultDto(false, groupStatus.toString() + " is not supported!");
            }
        }
        return new ProcessResultDto(true);
    }

    private void updateDerivatVereinbarungDtoKommentar(Long derivatAnforderungModulId, String kommentar, List<VereinbarungDto> vereinbarungData) {
        Optional<DerivatVereinbarungDto> derivatVereinbarung = vereinbarungData.stream()
                .flatMap(VereinbarungDto::getDirty)
                .filter(dv -> dv.getDerivatAnforderungModulId().equals(derivatAnforderungModulId))
                .findAny();
        derivatVereinbarung.ifPresent(dv -> dv.updateKommentar(kommentar));
    }

    private void setUmsetzungsbestaetigungStatusToKeineWeitervervfolgungForDerivatAnforderungModul(String kommentar,
                                                                                                   DerivatAnforderungModul derivatAnforderungModul, Mitarbeiter currentUser) {
        List<Umsetzungsbestaetigung> umsetzungsbestaetigungen = umsetzungsbestaetigungService.getUmsetzungsbestaetigungenByDerivatAnforderungModul(derivatAnforderungModul);

        for (Umsetzungsbestaetigung u : umsetzungsbestaetigungen) {
            if (kommentar == null || kommentar.isEmpty()) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
                String ubkwkommentar = u.getKommentar() + "\n" + dateFormat.format(new Date()) + " > " + currentUser.toString() + " : " + kommentar;
                u.setKommentar(ubkwkommentar);
            }
            if (u.getStatus().equals(UmsetzungsBestaetigungStatus.NICHT_RELEVANT)) {
                u.setStatus(UmsetzungsBestaetigungStatus.NICHT_RELEVANT_KEINE_WEITERVERFOLGUNG);
            } else {
                u.setStatus(UmsetzungsBestaetigungStatus.NICHT_UMGESETZT_KEINE_WEITERVERFOLGUNG);
            }
            derivatAnforderungModulService.updateDerivatAnforderungModulStatus(
                    DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG, kommentar,
                    u.getKovaImDerivat().getDerivat(), u.getAnforderung(), u.getModul(), currentUser);
            umsetzungsbestaetigungService.persistUmsetzungsbestaetigung(u);

        }
    }

    public Workbook createExcelExport(List<VereinbarungDto> vereinbarungData) {
        XSSFWorkbook workbook = new XSSFWorkbook();

        Locale currentLocale = localizationService.getCurrentLocale();
        int offset = 5;

        Sheet sheet = workbook.createSheet(LocalizationService.getValue(currentLocale, "anforderungZuordnung"));
        Row headerRow = sheet.createRow(0);
        headerRow.createCell(0).setCellValue(LocalizationService.getValue(currentLocale, "anforderung"));
        headerRow.createCell(1).setCellValue(LocalizationService.getValue(currentLocale, "beschreibung"));
        headerRow.createCell(2).setCellValue(LocalizationService.getValue(currentLocale, "modul"));
        headerRow.createCell(3).setCellValue(LocalizationService.getValue(currentLocale, "sensorCoc"));
        headerRow.createCell(4).setCellValue(LocalizationService.getValue(currentLocale, "zakid"));
        Row subheaderRow = sheet.createRow(1);

        if (!vereinbarungData.isEmpty()) {
            int numberOfDerivate = vereinbarungData.get(0).getDerivatVereinbarungen().size();

            List<String> derivatNames = vereinbarungData.get(0).getDerivatVereinbarungen()
                    .stream().map(DerivatVereinbarungDto::getDerivatName).collect(Collectors.toList());

            for (int i = 0; i < derivatNames.size(); i++) { // header
                if (derivatNames.size() > i) {
                    headerRow.createCell(i * 4 + 0 + offset).setCellValue(derivatNames.get(i));
                    subheaderRow.createCell(i * 4 + 0 + offset).setCellValue(LocalizationService.getValue(currentLocale, "status"));
                    subheaderRow.createCell(i * 4 + 1 + offset).setCellValue(LocalizationService.getValue(currentLocale, "umsetzungsbeStatus"));
                    subheaderRow.createCell(i * 4 + 2 + offset).setCellValue(LocalizationService.getValue(currentLocale, "zakStatusKurz"));
                    subheaderRow.createCell(i * 4 + 3 + offset).setCellValue(LocalizationService.getValue(currentLocale, "kommentar"));
                }
            }

            for (int i = 0; i < vereinbarungData.size(); i++) { // content
                if (vereinbarungData.size() > i) {
                    VereinbarungDto vereinbarung = vereinbarungData.get(i);

                    Row row = sheet.createRow(i + 2);
                    row.createCell(0).setCellValue(vereinbarung.getAnforderungDisplayName());
                    row.createCell(1).setCellValue(vereinbarung.getAnforderungBeschreibungFull());
                    row.createCell(2).setCellValue(vereinbarung.getModulName());
                    row.createCell(3).setCellValue(vereinbarung.getSensorCoc());
                    row.createCell(4).setCellValue(vereinbarung.getZakId());

                    for (int j = 0; j < numberOfDerivate; j++) {
                        buildContentForDerivat(offset, vereinbarung, row, j);
                    }
                }
            }
        }
        return workbook;
    }

    private void buildContentForDerivat(int offset, VereinbarungDto vereinbarung, Row row, int position) {
        if (vereinbarung.getDerivatVereinbarungen().size() > position) {
            DerivatVereinbarungDto derivatVereinbarung = vereinbarung.getDerivatVereinbarungen().get(position);

            row.createCell(position * 4 + 0 + offset).setCellValue(derivatVereinbarung.getCurrentVereinbarungStatus().toString());

            List<DerivatAnforderungModulHistory> damHistory = this.getHistoryForDerivatAnforderungModul(derivatVereinbarung.getDerivatAnforderungModulId());
            row.createCell(position * 4 + 1 + offset).setCellValue(derivatVereinbarung.getUmsetzungsBestaetigungStatus());
            row.createCell(position * 4 + 2 + offset).setCellValue(derivatVereinbarung.getZakStatus());
            if (damHistory != null && !damHistory.isEmpty()) {
                row.createCell(position * 4 + 3 + offset).setCellValue(damHistory.stream().map(h -> h.getKommentarDetails()).collect(Collectors.joining(", ")));
            }
        }
    }

}
