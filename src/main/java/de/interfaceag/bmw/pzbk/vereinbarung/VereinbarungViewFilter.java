package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.filter.BooleanSearchFilter;
import de.interfaceag.bmw.pzbk.filter.BooleanUrlFilter;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.filter.DerivatAnforderungModulIdFilter;
import de.interfaceag.bmw.pzbk.filter.EditModeUrlFilter;
import de.interfaceag.bmw.pzbk.filter.FachIdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.FestgestelltInFilter;
import de.interfaceag.bmw.pzbk.filter.Filter;
import de.interfaceag.bmw.pzbk.filter.FilterToUrlConverter;
import de.interfaceag.bmw.pzbk.filter.IdListUrlFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.InProzessbaukastenFilter;
import de.interfaceag.bmw.pzbk.filter.ModulFilter;
import de.interfaceag.bmw.pzbk.filter.NotInProzessbaukastenFilter;
import de.interfaceag.bmw.pzbk.filter.PhasenrelevanzFilter;
import de.interfaceag.bmw.pzbk.filter.ProzessbaukastenFilter;
import de.interfaceag.bmw.pzbk.filter.SelectedDerivatUrlFilter;
import de.interfaceag.bmw.pzbk.filter.SensorCocFilter;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ThemenklammerFilter;
import de.interfaceag.bmw.pzbk.filter.VonDateFilter;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.DerivatVereinbarungStatusFilter;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author fp
 */
public class VereinbarungViewFilter implements Serializable {

    private static final Page PAGE = Page.VEREINBARUNG;

    @Filter
    private final FachIdSearchFilter anforderungFilter;
    @Filter
    private final TextSearchFilter beschreibungFilter;

    @Filter
    private IdSearchFilter sensorCocFilter;
    @Filter
    private IdSearchFilter prozessbaukastenFilter;
    @Filter
    private IdSearchFilter modulFilter;
    @Filter
    private IdSearchFilter festgestelltInFilter;
    @Filter
    private final ThemenklammerFilter themenklammerFilter;
    @Filter
    private final BooleanSearchFilter phasenrelevanzFilter;

    @Filter
    private final DateSearchFilter freigegebenSeitFilter;

    private final List<DerivatVereinbarungStatusFilter> derivatVereinbarungStatusFilter;

    @Filter
    private final SelectedDerivatUrlFilter selectedDerivatFilter;
    @Filter
    private final IdListUrlFilter anforderungIdFilter;

    @Filter
    private final DerivatAnforderungModulIdFilter derivatAnforderungModulIdFilter;

    @Filter
    private final BooleanUrlFilter editModeFilter;
    @Filter
    private final BooleanUrlFilter inProzessbaukastenFilter;
    @Filter
    private final BooleanUrlFilter notInProzessbaukastenFilter;

    public VereinbarungViewFilter(
            UrlParameter urlParameter,
            List<SensorCoc> allSensorCocs,
            List<Derivat> allDerivate,
            List<FestgestelltIn> allFestgestelltIn,
            List<ProzessbaukastenFilterDto> prozessbaukaesten,
            List<Modul> allModule,
            Collection<ThemenklammerDto> themenklammern) {
        anforderungFilter = new FachIdSearchFilter(urlParameter);

        beschreibungFilter = new TextSearchFilter("beschreibung", urlParameter);

        sensorCocFilter = new SensorCocFilter(allSensorCocs, urlParameter);

        prozessbaukastenFilter = new ProzessbaukastenFilter(prozessbaukaesten, urlParameter);

        modulFilter = new ModulFilter(allModule, urlParameter);

        festgestelltInFilter = new FestgestelltInFilter(allFestgestelltIn, urlParameter);

        phasenrelevanzFilter = new PhasenrelevanzFilter(urlParameter);

        this.freigegebenSeitFilter = new VonDateFilter("freigabe")
                .parseParameter(urlParameter.getValue("freigabe" + VonDateFilter.PARAMETERNAME));

        derivatVereinbarungStatusFilter = DerivatVereinbarungStatusFilterUtils
                .getFilterForDerivatListAndUrlParamter(allDerivate, urlParameter);

        selectedDerivatFilter = new SelectedDerivatUrlFilter(urlParameter);

        anforderungIdFilter = new IdListUrlFilter(urlParameter);

        derivatAnforderungModulIdFilter = new DerivatAnforderungModulIdFilter(urlParameter);

        editModeFilter = new EditModeUrlFilter(urlParameter);

        inProzessbaukastenFilter = new InProzessbaukastenFilter(urlParameter);

        notInProzessbaukastenFilter = new NotInProzessbaukastenFilter(urlParameter);

        themenklammerFilter = new ThemenklammerFilter(themenklammern, urlParameter);
    }

    public String getUrl() {
        return PAGE.getUrl() + getUrlParameter();
    }

    public String getResetUrl() {
        return PAGE.getUrl() + "?faces-redirect=true";
    }

    private String getUrlParameter() {
        FilterToUrlConverter urlConverter = FilterToUrlConverter.forClass(this);
        final StringBuilder stringBuilder = new StringBuilder(urlConverter.getUrl());

        getDerivatVereinbarungStatusFilter().forEach(filter -> stringBuilder.append(filter.getParameter()));

        return stringBuilder.toString();
    }

    public void restrictSelectableValuesBasedOnSearchResult(
            List<VereinbarungDto> vereinbarungData, UrlParameter urlParameter,
            List<FestgestelltIn> festgestelltIns, List<SensorCoc> sensorCocs,
            List<Modul> module, List<ProzessbaukastenFilterDto> prozessbaukaesten) {
        if (vereinbarungData == null || vereinbarungData.isEmpty()) {
            return;
        }

        prozessbaukastenFilter = new ProzessbaukastenFilter(prozessbaukaesten, urlParameter);
        sensorCocFilter = new SensorCocFilter(sensorCocs, urlParameter);
        festgestelltInFilter = new FestgestelltInFilter(festgestelltIns, urlParameter);
        modulFilter = new ModulFilter(module, urlParameter);

    }

    public FachIdSearchFilter getAnforderungFilter() {
        return anforderungFilter;
    }

    public TextSearchFilter getBeschreibungFilter() {
        return beschreibungFilter;
    }

    public IdSearchFilter getSensorCocFilter() {
        return sensorCocFilter;
    }

    public IdSearchFilter getModulFilter() {
        return modulFilter;
    }

    public IdSearchFilter getFestgestelltInFilter() {
        return festgestelltInFilter;
    }

    public BooleanSearchFilter getPhasenrelevanzFilter() {
        return phasenrelevanzFilter;
    }

    public DateSearchFilter getFreigegebenSeitFilter() {
        return freigegebenSeitFilter;
    }

    public List<DerivatVereinbarungStatusFilter> getDerivatVereinbarungStatusFilter() {
        return derivatVereinbarungStatusFilter;
    }

    public SelectedDerivatUrlFilter getSelectedDerivatFilter() {
        return selectedDerivatFilter;
    }

    public IdListUrlFilter getAnforderungIdFilter() {
        return anforderungIdFilter;
    }

    public DerivatAnforderungModulIdFilter getDerivatAnforderungModulIdFilter() {
        return derivatAnforderungModulIdFilter;
    }

    public ThemenklammerFilter getThemenklammerFilter() {
        return themenklammerFilter;
    }

    public BooleanUrlFilter getEditModeFilter() {
        return editModeFilter;
    }

    public IdSearchFilter getProzessbaukastenFilter() {
        return prozessbaukastenFilter;
    }

    public BooleanUrlFilter getInProzessbaukastenFilter() {
        return inProzessbaukastenFilter;
    }

    public BooleanUrlFilter getNotInProzessbaukastenFilter() {
        return notInProzessbaukastenFilter;
    }

}
