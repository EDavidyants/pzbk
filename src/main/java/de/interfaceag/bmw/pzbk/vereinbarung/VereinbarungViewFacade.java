package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.berichtswesen.ZakStatusService;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusService;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import de.interfaceag.bmw.pzbk.filter.UrlFilter;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.ProcessResultDto;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.LogUtils;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerService;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author fp
 */
@Stateless
@Named
public class VereinbarungViewFacade implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(VereinbarungViewFacade.class);

    @Inject
    private Session session;

    @Inject
    private AnforderungService anforderungService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    private DerivatService derivatService;
    @Inject
    private ModulService modulService;
    @Inject
    private VereinbarungService vereinbarungService;

    @Inject
    private VereinbarungProcessService vereinbarungProcessService;

    @Inject
    private ConfigService configService;
    @Inject
    private SensorCocService sensorCocService;
    @Inject
    private ExcelDownloadService excelDownloadService;
    @Inject
    private ZakStatusService zakStatusService;
    @Inject
    private DerivatStatusService derivatStatusService;

    @Inject
    private ProzessbaukastenReadService prozessbaukastenReadService;
    @Inject
    private ThemenklammerService themenklammerService;

    @Inject
    private VereinbarungViewVereinbarungDataService vereinbarungViewVereinbarungDataService;

    public void updateUserDerivate(List<Derivat> selectedDerivate) {
        Mitarbeiter currentUser = session.getUser();
        derivatService.setUserSelectedDerivate(currentUser, selectedDerivate);
    }

    public String getUrl(VereinbarungViewData viewData) {
        return configService.getUrlPrefix() + "/" + viewData.getFilter().getUrl();
    }

    public String process(VereinbarungViewData viewData) {
        vereinbarungProcessService.processChanges(viewData);
        viewData.getFilter().getEditModeFilter().setInactive();
        return viewData.getFilter().getUrl();
    }

    public VereinbarungViewData getViewData(UrlParameter urlParameter) {

        Mitarbeiter currentUser = session.getUser();

        LOG.debug("currentUser: {}", currentUser);
        LOG.debug("urlParameter: {}", urlParameter);

        LOG.info("Start getLiveKeyFigures");
        Date startDate = new Date();

        List<Derivat> allDerivateForUser = berechtigungService.getDerivateForCurrentUser();
        List<Derivat> selectedDerivate = derivatService.getUserSelectedDerivate(currentUser);

        VereinbarungViewFilter viewFilter = getViewFilter(urlParameter, currentUser, selectedDerivate);

        Set<Long> derivatAnforderungModulIdsFromUrl = viewFilter.getDerivatAnforderungModulIdFilter().getIds();
        boolean requestedFromDashboard = !derivatAnforderungModulIdsFromUrl.isEmpty();

        if (requestedFromDashboard) {
            selectedDerivate = getRedefinedSelectedDerivateBasedOnUrl(allDerivateForUser, derivatAnforderungModulIdsFromUrl);
            viewFilter = getViewFilter(urlParameter, currentUser, selectedDerivate);
        }

        List<VereinbarungDto> vereinbarungData = vereinbarungViewVereinbarungDataService.getVereinbarungData(selectedDerivate, viewFilter);

        LOG.info("End getLiveKeyFigures");
        Date endDate = new Date();
        LogUtils.logDiff(startDate, endDate);

        List<SensorCoc> sensorCocs = getSensorCocsFromVereinbarungData(vereinbarungData);
        List<FestgestelltIn> festgestelltIns = getFestgestelltInFromVereinbarungData(vereinbarungData);
        List<Modul> module = getModulesFromVereinbarungData(vereinbarungData);
        List<ProzessbaukastenFilterDto> prozessbaukaesten = getProzessbaukaestenFromVereinbarungData(vereinbarungData);

        viewFilter.restrictSelectableValuesBasedOnSearchResult(vereinbarungData, urlParameter, festgestelltIns, sensorCocs, module, prozessbaukaesten);

        boolean editMode = viewFilter.getEditModeFilter().isActive();

        Derivat selectedDerivat = getSelectedDerivat(urlParameter, viewFilter, selectedDerivate);

        VereinbarungViewPermission viewPermission = new VereinbarungViewPermission(
                session.getUserPermissions().getRoles(), editMode, selectedDerivat);

        DerivatStatusNames derivatStatusNames = getDerivatStatusNames();

        return new VereinbarungViewData(urlParameter, viewFilter, allDerivateForUser, vereinbarungData,
                selectedDerivate, selectedDerivate, viewPermission, derivatStatusNames, selectedDerivat);
    }

    private List<Derivat> getRedefinedSelectedDerivateBasedOnUrl(List<Derivat> selectedDerivate, Set<Long> derivatAnforderungModulIdsFromUrl) {
        List<Long> relevantDerivatIds = derivatService.getDerivatIdsToDerivatAnforderungModulIds(derivatAnforderungModulIdsFromUrl);
        selectedDerivate = selectedDerivate.stream()
                .filter(derivat -> relevantDerivatIds.contains(derivat.getId()))
                .collect(Collectors.toList());
        return selectedDerivate;
    }

    private Derivat getSelectedDerivat(UrlParameter urlParameter, VereinbarungViewFilter viewFilter, List<Derivat> allDerivate) {
        Optional<Derivat> selectedDerivat = parseSelectedDerivat(urlParameter, viewFilter, allDerivate);
        return selectedDerivat.orElseGet(() -> getFirstDerivat(allDerivate));
    }

    private Optional<Derivat> parseSelectedDerivat(UrlParameter urlParameter, VereinbarungViewFilter viewFilter, List<Derivat> allDerivate) {
        UrlFilter selectedDerivatFilter = viewFilter.getSelectedDerivatFilter();
        return VereinbarungUrlParameterUtils.getSelectedDerivat(urlParameter, selectedDerivatFilter, allDerivate);
    }

    private Derivat getFirstDerivat(List<Derivat> allDerivate) {
        if (!allDerivate.isEmpty()) {
            return allDerivate.get(0);
        } else {
            return null;
        }
    }

    private DerivatStatusNames getDerivatStatusNames() {
        return derivatStatusService.getDerivatStatusNames();
    }

    private List<SensorCoc> getSensorCocsFromVereinbarungData(List<VereinbarungDto> vereinbarungData) {
        List<Long> sensorCocIds = vereinbarungData.stream().map(VereinbarungDto::getSensorCocId).collect(Collectors.toList());
        return sensorCocService.getSensorCocsByIdList(sensorCocIds);
    }

    public ProcessResultDto processVereinbarungRowChanges(VereinbarungDto vereinbarung, Derivat derivat) {
        return vereinbarungService.processVereinbarungRowChanges(vereinbarung, derivat, session.getUser());
    }

    private List<ProzessbaukastenFilterDto> getProzessbaukaestenFromVereinbarungData(List<VereinbarungDto> vereinbarungData) {
        List<Long> anforderungIds = vereinbarungData.stream().map(VereinbarungDto::getAnforderungId).collect(Collectors.toList());
        List<Prozessbaukasten> prozessbaukaestenForAnforderungIds = prozessbaukastenReadService.getProzessbaukaestenForAnforderungIds(anforderungIds);
        return prozessbaukaestenForAnforderungIds.stream().map(ProzessbaukastenFilterDto::forProzessbaukasten).collect(Collectors.toList());
    }

    private List<Modul> getModulesFromVereinbarungData(List<VereinbarungDto> vereinbarungData) {
        List<Long> modulIds = vereinbarungData.stream().map(VereinbarungDto::getModulId).collect(Collectors.toList());
        return modulService.getModuleByIdList(modulIds);
    }

    private List<FestgestelltIn> getFestgestelltInFromVereinbarungData(List<VereinbarungDto> vereinbarungData) {
        List<Long> festgestelltInIds = vereinbarungData.stream().map(VereinbarungDto::getFestgestelltInIds).flatMap(Collection::stream).collect(Collectors.toList());
        return anforderungService.getFestgestelltInByIdList(festgestelltInIds);
    }

    private VereinbarungViewFilter getViewFilter(UrlParameter urlParameter, Mitarbeiter currentUser, List<Derivat> selectedDerivate) {

        LOG.info("Start getViewFilter");
        Date startDate = new Date();

        List<SensorCoc> allSensorCocs = berechtigungService.getSensorCocForMitarbeiter(currentUser, true);
        LOG.debug("{} sensorCocs f\u00fcr {} gefunden", allSensorCocs.size(), currentUser);

        List<FestgestelltIn> allFestgestelltIn = anforderungService.getAllFestgestelltIn();

        List<Modul> allModule = modulService.getAllModule();

        final List<ThemenklammerDto> themenklammern = themenklammerService.getAll();

        List<ProzessbaukastenFilterDto> prozessbaukaesten = prozessbaukastenReadService.getAllFilterDto();
        LOG.info("End getViewFilter");
        Date endDate = new Date();
        LogUtils.logDiff(startDate, endDate);

        return new VereinbarungViewFilter(urlParameter, allSensorCocs, selectedDerivate, allFestgestelltIn, prozessbaukaesten, allModule, themenklammern);
    }

    public void downloadExcelExport(List<VereinbarungDto> vereinbarungData) {

        Workbook resultWorkbook = vereinbarungService.createExcelExport(vereinbarungData);

        if (resultWorkbook != null) {
            excelDownloadService.downloadExcelExport("Vereinbarung", resultWorkbook);
        }
    }

    public String getZakUrl(String zakId) {
        return zakStatusService.buildZakUrl(zakId);
    }

}
