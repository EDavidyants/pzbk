package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.Locale;
import java.util.Optional;

/**
 *
 * @author sl
 */
@Named
@Stateless
public class VereinbarungStatusChangeKommentarService {

    @Inject
    private LocalizationService localizationService;

    private String getDefaultStatusChangeKommentar(DerivatAnforderungModul derivatAnforderungModul, DerivatAnforderungModulStatus status) {
        String kommentar = getCompletedKommentar(derivatAnforderungModul, status, "");
        return kommentar;
    }

    public String getStatusChangeKommentar(DerivatAnforderungModul derivatAnforderungModul, Collection<VereinbarungDto> vereinbarungData, DerivatAnforderungModulStatus status) {
        Optional<String> statusChangeKommentar = vereinbarungData.stream()
                .flatMap(v -> v.getDirty())
                .filter(dv -> dv.isStatusChangeKommentar())
                .filter(dv -> dv.getDerivatAnforderungModulId().equals(derivatAnforderungModul.getId()))
                .map(dv -> dv.getStatusChangeKommentar())
                .findAny();

        String kommentar;
        if (statusChangeKommentar.isPresent()) {
            kommentar = statusChangeKommentar.get();
        } else {
            kommentar = getDefaultStatusChangeKommentar(derivatAnforderungModul, status);
        }
        return kommentar;
    }

    private String getCompletedKommentar(DerivatAnforderungModul dam, DerivatAnforderungModulStatus status, String userKommentar) {
        Locale currentLocale = localizationService.getCurrentLocale();

        String kommentarBody;
        String kommentarHeader;
        StringBuilder sb = new StringBuilder();

        switch (status) {
            case ABZUSTIMMEN:
                sb.append("(").append(LocalizationService.getGermanValue(dam.getDerivat().getStatus().getLocalizationName())).append(")");
                sb.append(LocalizationService.getValue(currentLocale, "anforderung")).append(" ").append(dam.getAnforderung().getFachId()).append(" ");
                sb.append(LocalizationService.getValue(currentLocale, "und")).append(" ").append(LocalizationService.getValue(currentLocale, "derivat")).append(" ").append(dam.getDerivat().getName()).append(" ");
                sb.append(LocalizationService.getValue(currentLocale, "wurdenZugeordnet"));
                break;
            case ANGENOMMEN:
                sb.append("(").append(LocalizationService.getGermanValue(dam.getDerivat().getStatus().getLocalizationName())).append(")");
                sb.append(LocalizationService.getValue(currentLocale, "anforderung")).append(" ").append(dam.getAnforderung().getFachId()).append(" ");
                sb.append(LocalizationService.getValue(currentLocale, "und")).append(" ").append(LocalizationService.getValue(currentLocale, "derivat")).append(" ").append(dam.getDerivat().getName()).append(" ");
                sb.append(LocalizationService.getValue(currentLocale, "wurdenVereinbart"));
                break;
            case NICHT_BEWERTBAR:
                sb.append("(").append(LocalizationService.getGermanValue(dam.getDerivat().getStatus().getLocalizationName())).append(")");
                sb.append(LocalizationService.getValue(currentLocale, "umsetzung")).append(" ").append(LocalizationService.getValue(currentLocale, "von")).append(" ");
                sb.append(LocalizationService.getValue(currentLocale, "anforderung")).append(" ").append(dam.getAnforderung().getFachId()).append(", ");
                sb.append(LocalizationService.getValue(currentLocale, "modul")).append(" ").append(dam.getModul().getName()).append(" ");
                sb.append(LocalizationService.getValue(currentLocale, "in")).append(" ").append(LocalizationService.getValue(currentLocale, "derivat")).append(" ").append(dam.getDerivat().getName()).append(" ");
                sb.append(LocalizationService.getValue(currentLocale, "wurdeAlsNichtBewertbarMarkiert"));
                break;
            case KEINE_WEITERVERFOLGUNG:
                sb.append("(").append(LocalizationService.getGermanValue(dam.getDerivat().getStatus().getLocalizationName())).append(")");
                sb.append(LocalizationService.getValue(currentLocale, "anforderung")).append(" ").append(dam.getAnforderung().getFachId()).append(", ");
                sb.append(LocalizationService.getValue(currentLocale, "modul")).append(" ").append(dam.getModul().getName()).append(" ");
                sb.append(LocalizationService.getValue(currentLocale, "in")).append(" ").append(LocalizationService.getValue(currentLocale, "derivat")).append(" ").append(dam.getDerivat().getName()).append(" ");
                sb.append(LocalizationService.getValue(currentLocale, "wurdeAufKeineWeiterverfolgungGesetzt"));
                break;
            default:
                break;
        }

        kommentarHeader = sb.toString();

        if (userKommentar != null && !userKommentar.isEmpty()) {
            kommentarBody = kommentarHeader + " > " + userKommentar;
        } else {
            kommentarBody = kommentarHeader;
        }

        return kommentarBody;
    }

}
