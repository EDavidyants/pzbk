package de.interfaceag.bmw.pzbk.vereinbarung.dto;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.UmsetzungsbestaetigungStatusFilter;
import de.interfaceag.bmw.pzbk.filter.VereinbarungStatusFilter;
import de.interfaceag.bmw.pzbk.filter.ZakStatusFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DerivatVereinbarungStatusFilter implements Serializable {

    private final Long derivatId;
    private final String derivatName;
    private final DerivatStatus derivatStatus;

    private final MultiValueEnumSearchFilter<DerivatAnforderungModulStatus> vereinbarungStatusFilter;
    private final MultiValueEnumSearchFilter<ZakStatus> zakStatusFilter;
    private final MultiValueEnumSearchFilter<UmsetzungsBestaetigungStatus> umsetzungsbestaetigungStatusFilter;

    public DerivatVereinbarungStatusFilter(
            Long derivatId,
            String derivatName,
            DerivatStatus derivatStatus,
            UrlParameter urlParameter) {
        this.derivatName = derivatName;
        this.derivatId = derivatId;
        this.derivatStatus = derivatStatus;

        this.vereinbarungStatusFilter = new VereinbarungStatusFilter(urlParameter, derivatName);
        this.zakStatusFilter = new ZakStatusFilter(urlParameter, derivatName);
        this.umsetzungsbestaetigungStatusFilter = new UmsetzungsbestaetigungStatusFilter(urlParameter, derivatName);
    }

    public String getParameter() {
        StringBuilder sb = new StringBuilder();
        sb.append(vereinbarungStatusFilter.getIndependentParameter());
        sb.append(zakStatusFilter.getIndependentParameter());
        sb.append(umsetzungsbestaetigungStatusFilter.getIndependentParameter());
        return sb.toString();
    }

    public MultiValueEnumSearchFilter<DerivatAnforderungModulStatus> getVereinbarungStatusFilter() {
        return vereinbarungStatusFilter;
    }

    public MultiValueEnumSearchFilter<ZakStatus>  getZakStatusFilter() {
        return zakStatusFilter;
    }

    public MultiValueEnumSearchFilter<UmsetzungsBestaetigungStatus> getUmsetzungsbestaetigungStatusFilter() {
        return umsetzungsbestaetigungStatusFilter;
    }

    public String getDerivatName() {
        return derivatName;
    }

    public Long getDerivatId() {
        return derivatId;
    }

    public DerivatStatus getDerivatStatus() {
        return derivatStatus;
    }

}
