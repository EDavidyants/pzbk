package de.interfaceag.bmw.pzbk.vereinbarung.dto;

import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author fn
 */
public class ProzessbaukastenLinkData implements Serializable {

    private final String fachId;
    private final Integer version;
    private final String bezeichnung;

    public ProzessbaukastenLinkData(String fachId, Integer version, String bezeichnung) {
        this.fachId = fachId;
        this.version = version;
        this.bezeichnung = bezeichnung;
    }

    public String getFachId() {
        return fachId;
    }

    public Integer getVersion() {
        return version;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public static ProzessbaukastenLinkData forProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        String fachId = prozessbaukasten.getFachId();
        Integer version = prozessbaukasten.getVersion();
        String bezeichnung = prozessbaukasten.getBezeichnung();
        return new ProzessbaukastenLinkData(fachId, version, bezeichnung);
    }

    @Override
    public String toString() {
        return fachId + " | V" + version + " : " + bezeichnung;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.fachId);
        hash = 59 * hash + Objects.hashCode(this.version);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProzessbaukastenLinkData other = (ProzessbaukastenLinkData) obj;
        if (!Objects.equals(this.fachId, other.fachId)) {
            return false;
        }
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        return true;
    }

}
