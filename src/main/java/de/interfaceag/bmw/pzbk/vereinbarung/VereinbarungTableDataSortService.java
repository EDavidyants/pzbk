package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOptionalTupleComparator;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOrders;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungenOrderService;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author sl
 */
@Stateless
@Named
public class VereinbarungTableDataSortService {

    private static final Logger LOG = LoggerFactory.getLogger(VereinbarungTableDataSortService.class.getName());

    @Inject
    private ProzessbaukastenAnforderungenOrderService prozessbaukastenAnforderungenOrderService;

    public List<VereinbarungDto> sortTableData(List<VereinbarungDto> vereinbarungen) {

        LOG.debug("Sort table data");
        LOG.debug("Order before sorting: {}", vereinbarungen);

        Collection<Long> prozessbaukastenIdsForZuordnungen = getProzessbaukastenIdsForZuordnungen(vereinbarungen);

        if (prozessbaukastenIdsForZuordnungen.isEmpty()) {
            return vereinbarungen;
        }

        ProzessbaukastenAnforderungOrders anforderungenOrders = prozessbaukastenAnforderungenOrderService.getAnforderungenOrderForProzessbaukasten(prozessbaukastenIdsForZuordnungen);

        List<VereinbarungDto> prozessbaukastenAnforderungen = sortProzessbaukastenAnforderungen(vereinbarungen, anforderungenOrders);
        List<VereinbarungDto> otherAnforderungen = sortNormalAnforderungen(vereinbarungen, anforderungenOrders);

        prozessbaukastenAnforderungen.addAll(otherAnforderungen);

        LOG.debug("Order after sorting: {}", prozessbaukastenAnforderungen);

        return prozessbaukastenAnforderungen;
    }

    private static List<VereinbarungDto> sortProzessbaukastenAnforderungen(List<VereinbarungDto> vereinbarungen, ProzessbaukastenAnforderungOrders anforderungenOrders) {
        ProzessbaukastenAnforderungOptionalTupleComparator<VereinbarungDto> zuordnungProzessbaukastenAnforderungComparator = new ProzessbaukastenAnforderungOptionalTupleComparator(anforderungenOrders);

        return vereinbarungen.stream()
                .filter(vereinbarung -> vereinbarung.getProzessbaukastenId().isPresent())
                .sorted(zuordnungProzessbaukastenAnforderungComparator)
                .collect(Collectors.toList());
    }

    private static List<VereinbarungDto> sortNormalAnforderungen(List<VereinbarungDto> vereinbarungen, ProzessbaukastenAnforderungOrders anforderungenOrders) {
        ProzessbaukastenAnforderungOptionalTupleComparator<VereinbarungDto> zuordnungProzessbaukastenAnforderungComparator = new ProzessbaukastenAnforderungOptionalTupleComparator(anforderungenOrders);

        return vereinbarungen.stream()
                .filter(vereinbarung -> !vereinbarung.getProzessbaukastenId().isPresent())
                .sorted(zuordnungProzessbaukastenAnforderungComparator)
                .collect(Collectors.toList());
    }

    private static Collection<Long> getProzessbaukastenIdsForZuordnungen(Collection<VereinbarungDto> vereinbarungen) {
        return vereinbarungen.stream()
                .map(vereinbarung -> vereinbarung.getProzessbaukastenId())
                .filter(id -> id.isPresent())
                .map(id -> id.get())
                .collect(Collectors.toSet());
    }

}
