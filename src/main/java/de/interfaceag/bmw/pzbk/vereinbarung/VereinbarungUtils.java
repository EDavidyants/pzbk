package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class VereinbarungUtils {

    private VereinbarungUtils() {
    }

    public static String defineZakColor(ZakStatus zakstatus) {
        if (zakstatus != null) {
            switch (zakstatus) {
                case DONE:
                    return "statusGruen";
                case EMPTY:
                case PENDING:
                    return "statusGelb";
                case NICHT_BEWERTBAR:
                case NICHT_UMSETZBAR:
                case UEBERTRAGUNG_FEHLERHAFT:
                    return "statusRot";
                case KEINE_ZAKUEBERTRAGUNG:
                    return "statusGrau";
                case ZURUECKGEZOGEN_DURCH_ANFORDERER:
                    return "statusBlau";
                default:
                    break;
            }
        }
        return "";
    }

    public static String defineVereinbarungColor(DerivatAnforderungModulStatus status) {
        if (status != null) {
            switch (status) {
                case ABZUSTIMMEN:
                case NICHT_BEWERTBAR:
                case IN_KLAERUNG:
                    return "statusGelb";
                case ANGENOMMEN:
                    return "statusGruen";
                case KEINE_WEITERVERFOLGUNG:
                case UNZUREICHENDE_ANFORDERUNGSQUALITAET:
                    return "statusRot";
                default:
                    return "statusBlau";
            }
        }
        return "statusBlau";
    }

    public static String defineUBColor(UmsetzungsBestaetigungStatus ubstatus) {
        if (ubstatus != null) {
            switch (ubstatus) {
                case UMGESETZT:
                    return "statusGruen";
                case OFFEN:
                case BEWERTUNG_NICHT_MOEGLICH:
                    return "statusGelb";
                case NICHT_UMGESETZT:
                case NICHT_RELEVANT:
                case KEINE_WEITERVERFOLGUNG:
                    return "statusRot";
                default:
                    break;
            }
        }
        return "";
    }

    public static boolean isStatusChangeValid(DerivatAnforderungModulStatus from, DerivatAnforderungModulStatus to) {
        if (from != null && to != null) {
            return DerivatAnforderungModulStatus.getNextStatusListByStatus(from).contains(to);
        }
        return false;
    }

    public static boolean isCellEditDisabled(DerivatAnforderungModulStatus currentStatus, boolean derivatVereinbarungIsActive) {
        if (derivatVereinbarungIsActive && currentStatus != null) {
            switch (currentStatus) {
                case ABZUSTIMMEN:
                case ANGENOMMEN:
                case NICHT_BEWERTBAR:
                case KEINE_WEITERVERFOLGUNG:
                case UNZUREICHENDE_ANFORDERUNGSQUALITAET:
                case IN_KLAERUNG:
                    return Boolean.TRUE;
                default:
                    return Boolean.FALSE;
            }
        } else {
            return Boolean.FALSE;
        }

    }

}
