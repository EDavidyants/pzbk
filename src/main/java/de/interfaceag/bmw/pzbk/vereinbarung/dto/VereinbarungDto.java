package de.interfaceag.bmw.pzbk.vereinbarung.dto;

import de.interfaceag.bmw.pzbk.prozessbaukasten.dto.ProzessbaukastenLabelDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOptionalTuple;
import de.interfaceag.bmw.pzbk.shared.components.AnforderungLink;
import de.interfaceag.bmw.pzbk.shared.utils.SecurityUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class VereinbarungDto implements AnforderungLink, Serializable, ProzessbaukastenAnforderungOptionalTuple {

    private final String key;

    private final long anforderungId;
    private final long version;
    private final String fachId;
    private final String anforderungDisplayName;
    private final String anforderungBeschreibungFull;
    private final List<Long> festgestelltInIds;
    private final ProzessbaukastenLabelDto prozessbaukastenLabel;

    private final String sensorCoc;
    private final Long sensorCocId;

    private final Long modulId;
    private final String modulName;

    private final String zakId;

    private final List<DerivatVereinbarungDto> derivatVereinbarungen;

    // TODO refactor this to builder pattern
    public VereinbarungDto(long anforderungId, String fachId, String anforderungDisplayName,
                           String anforderungBeschreibungFull, Long modulId, String modulName,
                           String zakId, String sensorCoc, Long sensorCocId, List<Long> festgestelltInIds,
                           long version, ProzessbaukastenLabelDto prozessbaukastenLabel) {

        this.key = generateKey(anforderungId, modulName);

        this.anforderungId = anforderungId;
        this.fachId = fachId;
        this.anforderungDisplayName = anforderungDisplayName;
        this.anforderungBeschreibungFull = anforderungBeschreibungFull;
        this.modulId = modulId;
        this.modulName = modulName;
        this.sensorCoc = sensorCoc;
        this.zakId = zakId;
        this.derivatVereinbarungen = new ArrayList<>();
        this.sensorCocId = sensorCocId;
        this.festgestelltInIds = festgestelltInIds;
        this.version = version;
        this.prozessbaukastenLabel = prozessbaukastenLabel;
    }

    public boolean isProzessbaukastenZugeordnet() {
        return prozessbaukastenLabel != null;
    }

    public ProzessbaukastenLabelDto getProzessbaukastenLabel() {
        return prozessbaukastenLabel;
    }

    protected static String generateKey(long anforderungId, String modulName) {
        if (modulName != null) {
            return SecurityUtils.hashStringSha256(Long.toString(anforderungId).concat(modulName));
        }
        return "";
    }

    public void addDerivatVereinbarung(DerivatVereinbarungDto derivatVereinbarung) {
        if (derivatVereinbarung != null) {
            this.derivatVereinbarungen.add(derivatVereinbarung);
        }
    }

    public DerivatVereinbarungDto getVereinbarungForDerivat(Long derivatId) {
        return derivatVereinbarungen.stream()
                .filter(d -> d.getDerivatId().equals(derivatId.toString()))
                .findAny().orElse(null);
    }

    public DerivatVereinbarungDto get(int position) {
        return derivatVereinbarungen.get(position);
    }

    public Stream<DerivatVereinbarungDto> getDirty() {
        return derivatVereinbarungen.stream().filter(DerivatVereinbarungDto::isDirty);
    }

    @Override
    public Optional<Long> getProzessbaukastenId() {
        if (prozessbaukastenLabel != null) {
            return Optional.of(prozessbaukastenLabel.getId());
        }
        return Optional.empty();
    }

    @Override
    public Long getAnforderungId() {
        return anforderungId;
    }

    @Override
    public String getAnforderungFachId() {
        return fachId;
    }

    public String getAnforderungDisplayName() {
        return anforderungDisplayName;
    }

    public String getAnforderungBeschreibungShort() {
        return anforderungBeschreibungFull != null && anforderungBeschreibungFull.length() > 76
                ? anforderungBeschreibungFull.substring(0, 77).concat("...")
                : getAnforderungBeschreibungFull();
    }

    public String getAnforderungBeschreibungFull() {
        return anforderungBeschreibungFull != null ? anforderungBeschreibungFull : "";
    }

    public String getModulName() {
        return modulName;
    }

    public List<DerivatVereinbarungDto> getDerivatVereinbarungen() {
        return derivatVereinbarungen;
    }

    public String getKey() {
        return key;
    }

    public String getSensorCoc() {
        return sensorCoc;
    }

    public Long getModulId() {
        return modulId;
    }

    public String getZakId() {
        return zakId;
    }

    public List<Long> getFestgestelltInIds() {
        return festgestelltInIds;
    }

    public Long getSensorCocId() {
        return sensorCocId;
    }

    @Override
    public String getFachId() {
        return fachId;
    }

    @Override
    public String getId() {
        return Long.toString(anforderungId);
    }

    @Override
    public String getVersion() {
        return Long.toString(version);
    }

    public String getDisplayName() {
        return anforderungDisplayName;
    }

}
