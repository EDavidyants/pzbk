package de.interfaceag.bmw.pzbk.vereinbarung.dto;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.shared.dto.Commentable;
import de.interfaceag.bmw.pzbk.vereinbarung.VereinbarungUtils;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DerivatVereinbarungDto implements Serializable, Commentable {

    private final String derivatId;
    private final String derivatName;
    private final DerivatStatus derivatStatus;

    private final ZakStatus zakStatus;
    private final KovAPhase kovaPhase;
    private final UmsetzungsBestaetigungStatus umsetzungsBestaetigungStatus;

    private final DerivatAnforderungModulStatus statusVKBG;
    private final DerivatAnforderungModulStatus statusZV;

    private DerivatAnforderungModulStatus currentVereinbarungStatus;
    private DerivatAnforderungModulStatus newVereinbarungStatus;

    private String kommentar;

    private String statusChangeKommentar;

    private final Long derivatAnforderungModulId;

    private boolean inEditMode;

    private final boolean derivatVereinbarungActive;

    private final boolean zugeordnet;

    public DerivatVereinbarungDto(String derivatId, String derivatName, DerivatStatus derivatStatus, ZakStatus zakStatus, KovAPhase kovAPhase, UmsetzungsBestaetigungStatus umsetzungsBestaetigungStatus, DerivatAnforderungModulStatus statusVKBG, DerivatAnforderungModulStatus statusZV, DerivatAnforderungModulStatus currentVereinbarungStatus, DerivatAnforderungModulStatus newVereinbarungStatus, String kommentar, Long derivatAnforderungModulId, boolean derivatVereinbarungActive, boolean zugeordnet) {
        this.derivatId = derivatId;
        this.derivatName = derivatName;
        this.derivatStatus = derivatStatus;
        this.zakStatus = zakStatus;
        this.kovaPhase = kovAPhase;
        this.umsetzungsBestaetigungStatus = umsetzungsBestaetigungStatus;
        this.statusVKBG = statusVKBG;
        this.statusZV = statusZV;
        this.currentVereinbarungStatus = currentVereinbarungStatus;
        this.newVereinbarungStatus = newVereinbarungStatus;
        this.kommentar = kommentar;
        this.derivatAnforderungModulId = derivatAnforderungModulId;
        this.derivatVereinbarungActive = derivatVereinbarungActive;
        this.zugeordnet = zugeordnet;
    }

    @Override
    public String toString() {
        return "DerivatVereinbarungDto{" + "derivatId=" + derivatId + ", derivatName=" + derivatName + ", derivatStatus=" + derivatStatus + ", zakStatus=" + zakStatus + ", kovaPhase=" + kovaPhase + ", umsetzungsBestaetigungStatus=" + umsetzungsBestaetigungStatus + ", statusVKBG=" + statusVKBG + ", statusZV=" + statusZV + ", currentVereinbarungStatus=" + currentVereinbarungStatus + ", newVereinbarungStatus=" + newVereinbarungStatus + ", kommentar=" + kommentar + ", statusChangeKommentar=" + statusChangeKommentar + ", derivatAnforderungModulId=" + derivatAnforderungModulId + ", inEditMode=" + inEditMode + ", derivatVereinbarungActive=" + derivatVereinbarungActive + ", zugeordnet=" + zugeordnet + '}';
    }

    public void updateKommentar(String newKommentar) {
        if (newKommentar != null && !newKommentar.isEmpty()) {
            this.kommentar = newKommentar;
        }
    }

    public boolean isZugeordnet() {
        return zugeordnet;
    }

    public boolean isDirty() {
        return !currentVereinbarungStatus.equals(newVereinbarungStatus);
    }

    public boolean isDisabled() {
        return VereinbarungUtils.isCellEditDisabled(currentVereinbarungStatus, derivatVereinbarungActive);
    }

    public boolean isValid() {
        return VereinbarungUtils.isStatusChangeValid(currentVereinbarungStatus, newVereinbarungStatus);
    }

    public List<DerivatAnforderungModulStatus> getNextStatusList() {
        return DerivatAnforderungModulStatus.getNextStatusListByStatus(currentVereinbarungStatus);
    }

    public String getZakColor() {
        if (zakStatus != null) {
            return VereinbarungUtils.defineZakColor(zakStatus);
        }
        return "";
    }

    public String getVereinbarungColor() {
        return VereinbarungUtils.defineVereinbarungColor(statusVKBG);
    }

    public String getUbColor() {
        if (umsetzungsBestaetigungStatus != null) {
            return VereinbarungUtils.defineUBColor(umsetzungsBestaetigungStatus);
        }
        return "";
    }

    public boolean isZak() {
        return zakStatus != null && !zakStatus.equals(ZakStatus.EMPTY);
    }

    public boolean isUb() {
        return umsetzungsBestaetigungStatus != null && kovaPhase != null;
    }

    public String getDerivatId() {
        return derivatId;
    }

    public String getDerivatName() {
        return derivatName;
    }

    public String getZakLabel() {
        return zakStatus != null && !zakStatus.equals(ZakStatus.KEINE_ZAKUEBERTRAGUNG) ? "ZAK" : "STD";
    }

    public String getZakStatus() {
        return zakStatus != null ? zakStatus.toString() : "";
    }

    public String getUbLabel() {
        return kovaPhase != null ? kovaPhase.getBezeichnung() : "";
    }

    public String getUmsetzungsBestaetigungStatus() {
        return umsetzungsBestaetigungStatus != null ? umsetzungsBestaetigungStatus.toString() : "";
    }

    public DerivatAnforderungModulStatus getStatusVKBG() {
        return statusVKBG;
    }

    public DerivatAnforderungModulStatus getStatusZV() {
        return statusZV;
    }

    public DerivatAnforderungModulStatus getNewVereinbarungStatus() {
        return newVereinbarungStatus;
    }

    public DerivatAnforderungModulStatus getCurrentVereinbarungStatus() {
        return currentVereinbarungStatus;
    }

    public String getKommentar() {
        return kommentar != null ? kommentar : "";
    }

    public String getKommentarShort() {
        return getKommentar().length() > 50 ? getKommentar().substring(0, 47).concat("...") : getKommentar();
    }

    public Long getDerivatAnforderungModulId() {
        return derivatAnforderungModulId;
    }

    @Override
    public String getComment() {
        return getStatusChangeKommentar();
    }

    public String getStatusChangeKommentar() {
        return statusChangeKommentar;
    }

    public boolean isStatusChangeKommentar() {
        return statusChangeKommentar != null && !statusChangeKommentar.trim().isEmpty();
    }

    public boolean isInEditMode() {
        return inEditMode;
    }

    public void setNewVereinbarungStatus(DerivatAnforderungModulStatus newVereinbarungStatus) {
        this.newVereinbarungStatus = newVereinbarungStatus;
    }

    @Override
    public void setComment(String comment) {
        setStatusChangeKommentar(comment);
    }

    public void setStatusChangeKommentar(String statusChangeKommentar) {
        this.statusChangeKommentar = statusChangeKommentar;
    }

    public void setInEditMode(boolean inEditMode) {
        this.inEditMode = inEditMode;
    }

    public void setCurrentVereinbarungStatus(DerivatAnforderungModulStatus currentVereinbarungStatus) {
        this.currentVereinbarungStatus = currentVereinbarungStatus;
    }

    public void resetStatusChangeKommentar() {
        this.statusChangeKommentar = "";
    }

    public boolean isDerivatVereinbarungActive() {
        return derivatVereinbarungActive;
    }

    public boolean isShowStatusVKBGLabel() {
        if (statusVKBG == null) {
            return Boolean.FALSE;
        } else {
            switch (derivatStatus) {
                case VEREINBARUNG_ZV:
                case ZV:
                    return Boolean.TRUE;
                default:
                    return Boolean.FALSE;
            }
        }
    }

    public static Builder forDerivat(Derivat derivat) {
        return new Builder().forDerivat(derivat);
    }

    public static Builder forDerivatAnforderungModul(DerivatAnforderungModul derivatAnforderungModul) {
        return new Builder().forDerivatAnforderungModul(derivatAnforderungModul);
    }

    public static final class Builder {

        private String derivatId;
        private String derivatName;
        private DerivatStatus derivatStatus;

        private ZakStatus zakStatus;
        private KovAPhase kovaPhase;
        private UmsetzungsBestaetigungStatus umsetzungsBestaetigungStatus;

        private DerivatAnforderungModulStatus statusVKBG = DerivatAnforderungModulStatus.ABZUSTIMMEN;
        private DerivatAnforderungModulStatus statusZV = DerivatAnforderungModulStatus.ABZUSTIMMEN;

        private String kommentar;

        private Long derivatAnforderungModulId;

        private final boolean inEditMode = Boolean.FALSE;

        private boolean derivatVereinbarungActive;

        private boolean zugeordnet = Boolean.FALSE;

        private Builder() {
        }

        public Builder forDerivat(Derivat derivat) {
            this.derivatId = derivat.getId().toString();
            this.derivatName = derivat.getName();
            this.derivatStatus = derivat.getStatus();
            this.derivatVereinbarungActive = derivat.isVereinbarungActive();
            return this;
        }

        public Builder withZuordnung() {
            this.zugeordnet = Boolean.TRUE;
            return this;
        }

        public Builder forDerivatAnforderungModul(DerivatAnforderungModul derivatAnforderungModul) {
            this.zakStatus = derivatAnforderungModul.getZakStatus();
            if (derivatAnforderungModul.getKovAPhase().isPresent()) {
                this.kovaPhase = derivatAnforderungModul.getKovAPhase().get();
            }
            this.umsetzungsBestaetigungStatus = derivatAnforderungModul.getUmsetzungsBestaetigungStatus();
            this.statusVKBG = derivatAnforderungModul.getStatusVKBG();
            this.statusZV = derivatAnforderungModul.getStatusZV();
            this.zugeordnet = Boolean.TRUE;
            this.derivatAnforderungModulId = derivatAnforderungModul.getId();
            return forDerivat(derivatAnforderungModul.getDerivat());
        }

        public Builder withKommentar(String kommentar) {
            this.kommentar = kommentar;
            return this;
        }

        public DerivatVereinbarungDto build() {
            final DerivatAnforderungModulStatus presentStatus = getCurrentVereinbarungStatus(statusVKBG, statusZV, derivatStatus);
            DerivatAnforderungModulStatus currentVereinbarungStatus = presentStatus;
            DerivatAnforderungModulStatus newVereinbarungStatus = presentStatus;
            return new DerivatVereinbarungDto(derivatId, derivatName, derivatStatus, zakStatus, kovaPhase, umsetzungsBestaetigungStatus, statusVKBG, statusZV, currentVereinbarungStatus, newVereinbarungStatus, kommentar, derivatAnforderungModulId, derivatVereinbarungActive, zugeordnet);
        }

        private DerivatAnforderungModulStatus getCurrentVereinbarungStatus(DerivatAnforderungModulStatus statusVKBG, DerivatAnforderungModulStatus statusZV, DerivatStatus derivatStatus) {
            switch (derivatStatus) {
                case VEREINBARUNG_ZV:
                case ZV:
                    return statusZV;
                case VEREINARBUNG_VKBG:
                default:
                    return statusVKBG;
            }
        }

    }

}
