package de.interfaceag.bmw.pzbk.vereinbarung.history;

import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModulHistory;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.vereinbarung.VereinbarungHistoryService;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.DerivatVereinbarungDto;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
@Named
public class VereinbarungHistoryFacade implements Serializable {

    @Inject
    private Session session;

    @Inject
    private VereinbarungHistoryService vereinbarungHistoryService;

    public List<DerivatAnforderungModulHistory> getHistoryForDerivatAnforderungModul(Long derivatAnforderungModulId) {
        return vereinbarungHistoryService.getHistoryForDerivatAnforderungModulId(derivatAnforderungModulId);
    }

    public void addVereinbarungHistoryEntry(DerivatVereinbarungDto derivatVereinbarungDto, String kommentar) {
        Mitarbeiter user = session.getUser();
        vereinbarungHistoryService.addVereinbarungHistoryEntry(derivatVereinbarungDto, kommentar, user);
    }

}
