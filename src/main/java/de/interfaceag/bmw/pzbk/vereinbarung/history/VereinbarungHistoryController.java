package de.interfaceag.bmw.pzbk.vereinbarung.history;

import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModulHistory;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.DerivatVereinbarungDto;
import org.primefaces.context.RequestContext;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ViewScoped
@Named
public class VereinbarungHistoryController implements Serializable {

    @Inject
    private VereinbarungHistoryFacade vereinbarungHistoryFacade;

    private List<DerivatAnforderungModulHistory> daHistory;
    private DerivatAnforderungModul kommentarDa;
    private String kommentar = "";
    private DerivatVereinbarungDto kommentarDerivatVereinbarungDto;

    public void showDaHistorieDialog(DerivatVereinbarungDto derivatVereinbarung) {
        setDaHistory(vereinbarungHistoryFacade.getHistoryForDerivatAnforderungModul(derivatVereinbarung.getDerivatAnforderungModulId()));
        kommentarDerivatVereinbarungDto = derivatVereinbarung;
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("dialog:derivatAnforderungModulHistorieForm");
        context.execute("PF('derivatAnforderungModulHistorieDialog').show()");
    }

    public void confirmDaHistorieCommentDialog() {
        if (kommentar != null && !"".equals(kommentar)) {
            vereinbarungHistoryFacade.addVereinbarungHistoryEntry(kommentarDerivatVereinbarungDto, kommentar);
            kommentarDerivatVereinbarungDto.updateKommentar(kommentar);
        }
        kommentar = "";
        setDaHistory(vereinbarungHistoryFacade.getHistoryForDerivatAnforderungModul(kommentarDerivatVereinbarungDto.getDerivatAnforderungModulId()));
    }

    public void closeDaHistorieDialog() {
        kommentar = "";
        setDaHistory(new ArrayList<>());
        setKommentarDa(new DerivatAnforderungModul());
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("addAnfoContent:addAnfoForm:addAnfoTable");
        context.execute("PF('derivatAnforderungModulHistorieDialog').hide()");
    }

    public static String convertDate(Date date) {
        SimpleDateFormat toformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return toformat.format(date);
    }

    public List<DerivatAnforderungModulHistory> getDaHistory() {
        return daHistory;
    }

    public void setDaHistory(List<DerivatAnforderungModulHistory> daHistory) {
        this.daHistory = daHistory;
    }

    public DerivatAnforderungModul getKommentarDa() {
        return kommentarDa;
    }

    public void setKommentarDa(DerivatAnforderungModul kommentarDa) {
        this.kommentarDa = kommentarDa;
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }
}
