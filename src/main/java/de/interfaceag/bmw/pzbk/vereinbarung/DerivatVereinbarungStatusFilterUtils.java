package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.DerivatVereinbarungStatusFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class DerivatVereinbarungStatusFilterUtils {

    private DerivatVereinbarungStatusFilterUtils() {
    }

    public static List<DerivatVereinbarungStatusFilter> getFilterForDerivatListAndUrlParamter(
            List<Derivat> derivate, UrlParameter urlParameter) {

        if (derivate != null) {
            return derivate.stream()
                    .map(derivat -> getFilterForDerivatAndUrlParamter(derivat, urlParameter))
                    .collect(Collectors.toList());
        }

        return new ArrayList<>();
    }

    private static DerivatVereinbarungStatusFilter getFilterForDerivatAndUrlParamter(
            Derivat derivat, UrlParameter urlParameter) {
        return new DerivatVereinbarungStatusFilter(derivat.getId(), derivat.getName(), derivat.getStatus(), urlParameter);
    }
}
