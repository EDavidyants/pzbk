package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.DerivatVereinbarungStatusFilter;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

final class VereinbarungQuickFilter {

    private VereinbarungQuickFilter() {
    }

    static void applyVereinbarenModeQuickFilter(Derivat selectedDerivat, VereinbarungViewFilter filter) {
        List<DerivatVereinbarungStatusFilter> derivatVereinbarungStatusFilter = filter.getDerivatVereinbarungStatusFilter();
        Iterator<DerivatVereinbarungStatusFilter> iterator = derivatVereinbarungStatusFilter.iterator();

        while (iterator.hasNext()) {
            DerivatVereinbarungStatusFilter next = iterator.next();
            if (next.getDerivatId().equals(selectedDerivat.getId())) {
                MultiValueEnumSearchFilter<DerivatAnforderungModulStatus> vereinbarungStatusFilter = next.getVereinbarungStatusFilter();
                applyVereinbarungStatusFilter(vereinbarungStatusFilter, DerivatAnforderungModulStatus.ABZUSTIMMEN);
            }
        }
    }

    static void applyFolgeprozessZakQuickFilter(Derivat selectedDerivat, VereinbarungViewFilter filter) {
        Iterator<DerivatVereinbarungStatusFilter> iterator = filter.getDerivatVereinbarungStatusFilter().iterator();

        while (iterator.hasNext()) {
            DerivatVereinbarungStatusFilter next = iterator.next();
            if (next.getDerivatId().equals(selectedDerivat.getId())) {
                MultiValueEnumSearchFilter<DerivatAnforderungModulStatus> vereinbarungStatusFilter = next.getVereinbarungStatusFilter();
                applyVereinbarungStatusFilter(vereinbarungStatusFilter, DerivatAnforderungModulStatus.ANGENOMMEN);

                MultiValueEnumSearchFilter<ZakStatus> zakStatusFilter = next.getZakStatusFilter();
                applyFolgeprozessZakStatusFilter(zakStatusFilter);
            }
        }
    }

    private static void applyFolgeprozessZakStatusFilter(MultiValueEnumSearchFilter<ZakStatus> zakStatusFilter) {
        List<SearchFilterObject> allZakStatus = zakStatusFilter.getAll();
        Set<SearchFilterObject> selectedZakStatus = allZakStatus.stream()
                .filter(sf -> sf.getId().intValue() == ZakStatus.NICHT_BEWERTBAR.getStatusId()
                        || sf.getId().intValue() == ZakStatus.NICHT_UMSETZBAR.getStatusId()
                        || sf.getId().intValue() == ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER.getStatusId()
                ).collect(Collectors.toSet());
        zakStatusFilter.setSelectedValues(selectedZakStatus);
    }

    static void applyFolgeprozessZakKeineWeiterverfolungQuickFilter(Derivat selectedDerivat, VereinbarungViewFilter filter) {
        Iterator<DerivatVereinbarungStatusFilter> iterator = filter.getDerivatVereinbarungStatusFilter().iterator();

        while (iterator.hasNext()) {
            DerivatVereinbarungStatusFilter next = iterator.next();
            if (next.getDerivatId().equals(selectedDerivat.getId())) {
                MultiValueEnumSearchFilter<DerivatAnforderungModulStatus> vereinbarungStatusFilter = next.getVereinbarungStatusFilter();
                applyVereinbarungStatusFilter(vereinbarungStatusFilter, DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG);

                MultiValueEnumSearchFilter<ZakStatus> zakStatusFilter = next.getZakStatusFilter();
                applyFolgeprozessZakKeineWeiterverfolungZakStatusFilter(zakStatusFilter);
            }
        }
    }

    private static void applyFolgeprozessZakKeineWeiterverfolungZakStatusFilter(MultiValueEnumSearchFilter<ZakStatus> zakStatusFilter) {
        List<SearchFilterObject> allZakStatus = zakStatusFilter.getAll();
        Set<SearchFilterObject> selectedZakStatus = allZakStatus.stream()
                .filter(sf -> sf.getId().intValue() == ZakStatus.NICHT_BEWERTBAR.getStatusId()
                        || sf.getId().intValue() == ZakStatus.NICHT_UMSETZBAR.getStatusId()
                        || sf.getId().intValue() == ZakStatus.DONE.getStatusId()
                        || sf.getId().intValue() == ZakStatus.BESTAETIGT_AUS_VORPROZESS.getStatusId()
                        || sf.getId().intValue() == ZakStatus.PENDING.getStatusId()
                        || sf.getId().intValue() == ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER.getStatusId()
                ).collect(Collectors.toSet());
        zakStatusFilter.setSelectedValues(selectedZakStatus);
    }

    private static void applyVereinbarungStatusFilter(MultiValueEnumSearchFilter<DerivatAnforderungModulStatus> vereinbarungStatusFilter, DerivatAnforderungModulStatus vereinbarungStatus) {
        List<SearchFilterObject> allVereinbarungStatus = vereinbarungStatusFilter.getAll();
        Set<SearchFilterObject> selectedVereinbarungStatus = allVereinbarungStatus.stream()
                .filter(sf -> sf.getId().intValue() == vereinbarungStatus.getStatusId())
                .collect(Collectors.toSet());
        vereinbarungStatusFilter.setSelectedValues(selectedVereinbarungStatus);
    }


    static void applyFolgeprozessUmsetzungsverwaltungQuickFilter(Derivat selectedDerivat, VereinbarungViewFilter filter) {
        Iterator<DerivatVereinbarungStatusFilter> iterator = filter.getDerivatVereinbarungStatusFilter().iterator();

        while (iterator.hasNext()) {
            DerivatVereinbarungStatusFilter next = iterator.next();
            if (next.getDerivatId().equals(selectedDerivat.getId())) {
                MultiValueEnumSearchFilter<DerivatAnforderungModulStatus> vereinbarungStatusFilter = next.getVereinbarungStatusFilter();
                applyVereinbarungStatusFilter(vereinbarungStatusFilter, DerivatAnforderungModulStatus.ANGENOMMEN);

                MultiValueEnumSearchFilter<UmsetzungsBestaetigungStatus> umsetzungsbestaetigungStatusFilter = next.getUmsetzungsbestaetigungStatusFilter();
                applyFolgeprozessUmsetzungsverwaltungFilter(umsetzungsbestaetigungStatusFilter);
            }
        }
    }

    private static void applyFolgeprozessUmsetzungsverwaltungFilter(MultiValueEnumSearchFilter<UmsetzungsBestaetigungStatus> umsetzungsbestaetigungStatusFilter) {
        List<SearchFilterObject> allUmsetzungsbestaetigungStatus = umsetzungsbestaetigungStatusFilter.getAll();
        Set<SearchFilterObject> selectedUmsetzungsbestaetigungStatus = allUmsetzungsbestaetigungStatus.stream()
                .filter(sf -> sf.getId().intValue() == UmsetzungsBestaetigungStatus.KEINE_WEITERVERFOLGUNG.getId()
                        || sf.getId().intValue() == UmsetzungsBestaetigungStatus.NICHT_RELEVANT.getId()
                        || sf.getId().intValue() == UmsetzungsBestaetigungStatus.BEWERTUNG_NICHT_MOEGLICH.getId()
                        || sf.getId().intValue() == UmsetzungsBestaetigungStatus.NICHT_UMGESETZT.getId()
                ).collect(Collectors.toSet());
        umsetzungsbestaetigungStatusFilter.setSelectedValues(selectedUmsetzungsbestaetigungStatus);
    }

}
