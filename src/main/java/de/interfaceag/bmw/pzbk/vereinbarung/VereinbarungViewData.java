package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.converters.DerivatConverter;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fp
 */
public class VereinbarungViewData implements Serializable {

    private final VereinbarungViewFilter filter;

    private final VereinbarungViewPermission viewPermission;

    private final List<Derivat> derivate;
    private List<Derivat> filteredDerivate;
    private List<Derivat> kommentarDerivate;
    private Derivat selectedDerivat;

    private final List<VereinbarungDto> tableData;
    private List<VereinbarungDto> selectedTableData = new ArrayList<>();

    private DerivatAnforderungModulStatus groupStatus;

    private boolean zuordnenMode;
    private boolean zakMode;
    private boolean allSelected;

    private final DerivatStatusNames derivatStatusNames;

    protected VereinbarungViewData(
            UrlParameter urlParameter,
            VereinbarungViewFilter filter,
            List<Derivat> derivate,
            List<VereinbarungDto> tableData,
            List<Derivat> filteredDerivate,
            List<Derivat> kommentarDerivate,
            VereinbarungViewPermission viewPermission,
            DerivatStatusNames derivatStatusNames,
            Derivat selectedDerivat
    ) {
        this.filter = filter;
        this.derivate = derivate;
        this.filteredDerivate = filteredDerivate;
        this.kommentarDerivate = kommentarDerivate;
        this.tableData = tableData;
        this.viewPermission = viewPermission;
        this.derivatStatusNames = derivatStatusNames;
        this.selectedDerivat = selectedDerivat;
    }

    public VereinbarungViewPermission getViewPermission() {
        return viewPermission;
    }

    public VereinbarungViewFilter getFilter() {
        return filter;
    }

    public List<Derivat> getDerivate() {
        return derivate;
    }

    public List<Derivat> getFilteredDerivate() {
        return filteredDerivate;
    }

    public void setFilteredDerivate(List<Derivat> filteredDerivate) {
        this.filteredDerivate = filteredDerivate;
    }

    public List<Derivat> getKommentarDerivate() {
        return kommentarDerivate;
    }

    public void setKommentarDerivate(List<Derivat> kommentarDerivate) {
        this.kommentarDerivate = kommentarDerivate;
    }

    public Derivat getSelectedDerivat() {
        return selectedDerivat;
    }

    public void setSelectedDerivat(Derivat selectedDerivat) {
        if (selectedDerivat != null) {
            this.selectedDerivat = selectedDerivat;
            this.getFilter().getSelectedDerivatFilter().setAttributeValue(selectedDerivat.getId().toString());
            this.getViewPermission().updateSelectedDerivat(selectedDerivat);
        }
    }

    public List<VereinbarungDto> getTableData() {
        return tableData;
    }

    public List<VereinbarungDto> getSelectedTableData() {
        return selectedTableData;
    }

    public void setSelectedTableData(List<VereinbarungDto> selectedTableData) {
        this.selectedTableData = selectedTableData;
    }

    public DerivatAnforderungModulStatus getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(DerivatAnforderungModulStatus groupStatus) {
        this.groupStatus = groupStatus;
    }

    public boolean isAllSelected() {
        return allSelected;
    }

    public void setAllSelected(boolean allSelected) {
        this.allSelected = allSelected;
    }

    public boolean isZuordnenMode() {
        return zuordnenMode;
    }

    public void setZuordnenMode(boolean zuordnenMode) {
        this.zuordnenMode = zuordnenMode;
    }

    public boolean isZakMode() {
        return zakMode;
    }

    public void setZakMode(boolean zakMode) {
        this.zakMode = zakMode;
    }

    public DerivatConverter getDerivatConverter() {
        return new DerivatConverter(getDerivate());
    }

    public DerivatStatusNames getDerivatStatusNames() {
        return derivatStatusNames;
    }

}
