package de.interfaceag.bmw.pzbk.vereinbarung.dto;

import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class VereinbarungViewData {

//    private final String filterParam;
//    private final KovAPhasenViewPermission viewPermission;
//    private final KovAPhasenViewFilter filter;
    private final List<VereinbarungDto> vereinbarungen;

    public VereinbarungViewData(List<VereinbarungDto> vereinbarungen) {
        this.vereinbarungen = vereinbarungen;
    }

    public List<VereinbarungDto> getVereinbarungen() {
        return vereinbarungen;
    }

}
