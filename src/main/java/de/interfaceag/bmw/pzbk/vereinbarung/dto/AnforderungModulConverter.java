package de.interfaceag.bmw.pzbk.vereinbarung.dto;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class AnforderungModulConverter {

    private AnforderungModulConverter() {
    }

    public static List<Tuple<Anforderung, Modul>> convertAnforderungList(List<Anforderung> anforderungen, Set<Long> modulIds) {
        List<Tuple<Anforderung, Modul>> result = new ArrayList<>();
        Iterator<Anforderung> anforderungIterator = anforderungen.iterator();
        while (anforderungIterator.hasNext()) {
            Anforderung anforderung = anforderungIterator.next();
            Set<Modul> anforderungModule = getModulesForAnforderung(anforderung);
            Iterator<Modul> modulIterator = anforderungModule.iterator();
            addRelevantModulsToResult(modulIds, result, anforderung, modulIterator);
        }
        return result;
    }

    private static void addRelevantModulsToResult(Set<Long> modulIds, List<Tuple<Anforderung, Modul>> result, Anforderung anforderung, Iterator<Modul> modulIterator) {
        while (modulIterator.hasNext()) {
            Modul modul = modulIterator.next();
            if (modulIds.contains(modul.getId())) {
                Tuple<Anforderung, Modul> newAnforderungModul = new GenericTuple<>(anforderung, modul);
                if (!result.contains(newAnforderungModul)) {
                    result.add(newAnforderungModul);
                }
            }
        }
    }

    private static Set<Modul> getModulesForAnforderung(Anforderung anforderung) {
        return anforderung.getAnfoFreigabeBeiModul().stream()
                .map(AnforderungFreigabeBeiModulSeTeam::getModulSeTeam)
                .map(ModulSeTeam::getElternModul)
                .collect(Collectors.toSet());
    }

    public static List<Tuple<Anforderung, Modul>> convertDerivatAnforderungModulList(List<DerivatAnforderungModul> derivatAnforderungModulList) {
        List<Tuple<Anforderung, Modul>> result = new ArrayList<>();
        Iterator<DerivatAnforderungModul> derivatAnforderungModulIterator = derivatAnforderungModulList.iterator();
        while (derivatAnforderungModulIterator.hasNext()) {
            DerivatAnforderungModul derivatAnforderungModul = derivatAnforderungModulIterator.next();
            Modul modul = derivatAnforderungModul.getModul();
            Anforderung anforderung = derivatAnforderungModul.getAnforderung();
            Tuple<Anforderung, Modul> newAnforderungModul = new GenericTuple<>(anforderung, modul);
            if (!result.contains(newAnforderungModul)) {
                result.add(newAnforderungModul);
            }
        }
        return result;
    }

}
