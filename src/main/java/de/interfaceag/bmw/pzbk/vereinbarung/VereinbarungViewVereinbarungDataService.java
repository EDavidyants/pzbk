package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.AnforderungModulConverter;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.DerivatVereinbarungStatusFilter;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDtoConverter;
import de.interfaceag.bmw.pzbk.zak.ZakVereinbarungService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Stateless
public class VereinbarungViewVereinbarungDataService implements Serializable {

    @Inject
    private VereinbarungSearchService vereinbarungSearchService;
    @Inject
    private ZakVereinbarungService zakVereinbarungService;
    @Inject
    private VereinbarungTableDataSortService vereinbarungTableDataSortService;

    public List<VereinbarungDto> getVereinbarungData(List<Derivat> selectedDerivate, VereinbarungViewFilter viewFilter) {
        if (!selectedDerivate.isEmpty()) {

            List<DerivatAnforderungModul> existingVereinbarungData = vereinbarungSearchService.getVereinbarungDataForFilter(viewFilter, selectedDerivate);
            Map<Long, String> derivatAnforderungModulIdZakIdMap = getDerivatAnforderungModulZakIdMap(existingVereinbarungData);
            List<Tuple<Anforderung, Modul>> zugeordneteAnforderungModulList = getZuordnungen(selectedDerivate, viewFilter);
            List<VereinbarungDto> result = VereinbarungDtoConverter.convertToVereinbarungDto(selectedDerivate, existingVereinbarungData, derivatAnforderungModulIdZakIdMap, zugeordneteAnforderungModulList);

            return sortVereinbarungen(result);
        } else {
            return Collections.emptyList();
        }
    }

    private List<Tuple<Anforderung, Modul>> getZuordnungen(List<Derivat> selectedDerivate, VereinbarungViewFilter viewFilter) {
        List<Anforderung> zuordnungSearchResultForFilter = vereinbarungSearchService.getZuordnungSearchResultForFilter(viewFilter, selectedDerivate);
        List<DerivatAnforderungModul> existingVereinbarungDataWithoutStatusFilter = getExistingVereinbarungDataWithoutStatusFilter(selectedDerivate, viewFilter);
        return removeExistingVereinbarungenFromZuordnungData(viewFilter, zuordnungSearchResultForFilter, existingVereinbarungDataWithoutStatusFilter);
    }

    private List<Tuple<Anforderung, Modul>> removeExistingVereinbarungenFromZuordnungData(VereinbarungViewFilter viewFilter, List<Anforderung> zuordnungSearchResultForFilter, List<DerivatAnforderungModul> existingVereinbarungDataWithoutStatusFilter) {
        final IdSearchFilter modulFilter = viewFilter.getModulFilter();
        final Set<Long> selectedModulIds = modulFilter.getSelectedIdsAsSet();
        List<Tuple<Anforderung, Modul>> zugeordneteAnforderungModulList = AnforderungModulConverter.convertAnforderungList(zuordnungSearchResultForFilter, selectedModulIds);
        List<Tuple<Anforderung, Modul>> existingAnforderungModulList = AnforderungModulConverter.convertDerivatAnforderungModulList(existingVereinbarungDataWithoutStatusFilter);
        zugeordneteAnforderungModulList.removeAll(existingAnforderungModulList);
        return zugeordneteAnforderungModulList;
    }

    private List<DerivatAnforderungModul> getExistingVereinbarungDataWithoutStatusFilter(List<Derivat> selectedDerivate, VereinbarungViewFilter viewFilter) {
        final List<DerivatVereinbarungStatusFilter> derivatVereinbarungStatusFilter = new ArrayList<>(viewFilter.getDerivatVereinbarungStatusFilter());
        viewFilter.getDerivatVereinbarungStatusFilter().clear();

        List<DerivatAnforderungModul> existingVereinbarungDataWithoutStatusFilter = vereinbarungSearchService.getVereinbarungDataForFilter(viewFilter, selectedDerivate);

        viewFilter.getDerivatVereinbarungStatusFilter().addAll(derivatVereinbarungStatusFilter);
        return existingVereinbarungDataWithoutStatusFilter;
    }


    private List<VereinbarungDto> sortVereinbarungen(List<VereinbarungDto> vereinbarungen) {
        return vereinbarungTableDataSortService.sortTableData(vereinbarungen);
    }

    private Map<Long, String> getDerivatAnforderungModulZakIdMap(List<DerivatAnforderungModul> existingVereinbarungData) {
        Map<Long, String> result = new HashMap<>();

        List<ZakUebertragung> zakUebertragungen = zakVereinbarungService.getZakUebertragungByDerivatAnforderungModulList(existingVereinbarungData);

        Iterator<ZakUebertragung> zakUebertragungIterator = zakUebertragungen.iterator();
        while (zakUebertragungIterator.hasNext()) {
            ZakUebertragung zakUebertragung = zakUebertragungIterator.next();
            if (!result.containsKey(zakUebertragung.getDerivatAnforderungModul().getId())) {
                result.put(zakUebertragung.getDerivatAnforderungModul().getId(), zakUebertragung.getZakId());
            }
        }

        return result;
    }

}
