package de.interfaceag.bmw.pzbk.vereinbarung.dto;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModulHistory;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.prozessbaukasten.dto.ProzessbaukastenLabelDto;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;
import de.interfaceag.bmw.pzbk.shared.utils.LogUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class VereinbarungDtoConverter {

    private VereinbarungDtoConverter() {
    }

    private static List<Tuple<Anforderung, Modul>> convertToAnforderungModulTuple(List<DerivatAnforderungModul> derivatAnforderungModule) {
        List<Tuple<Anforderung, Modul>> result = new ArrayList<>();

        Iterator<DerivatAnforderungModul> iterator = derivatAnforderungModule.iterator();
        while (iterator.hasNext()) {
            DerivatAnforderungModul derivatAnforderungModul = iterator.next();
            Tuple<Anforderung, Modul> newTuple = new GenericTuple<>(derivatAnforderungModul.getAnforderung(), derivatAnforderungModul.getModul());
            if (!result.contains(newTuple)) {
                result.add(newTuple);
            }
        }

        return result;
    }

    private static List<DerivatAnforderungModul> getDerivatAnforderungModuleForAnforderungModulTuple(Tuple<Anforderung, Modul> anforderungModul, List<DerivatAnforderungModul> allDerivatAnforderungModule) {
        Anforderung anforderung = anforderungModul.getX();
        Modul modul = anforderungModul.getY();
        return allDerivatAnforderungModule.stream()
                .filter(dam -> dam.getAnforderung().getId().equals(anforderung.getId())
                && dam.getModul().getId().equals(modul.getId()))
                .collect(Collectors.toList());
    }

    private static String getZakIdForAnforderungModulTuple(List<DerivatAnforderungModul> derivatAnforderungModule, Map<Long, String> derivatAnforderungModulIdZakIdMap) {
        Long derivatAnforderungModulId = Collections.max(derivatAnforderungModule.stream().map(DerivatAnforderungModul::getId).collect(Collectors.toSet()));
        return derivatAnforderungModulIdZakIdMap.get(derivatAnforderungModulId);
    }

    public static List<VereinbarungDto> convertToVereinbarungDto(
            List<Derivat> selectedDerivate,
            List<DerivatAnforderungModul> existingDerivatAnforderungModulList,
            Map<Long, String> derivatAnforderungModulIdZakIdMap,
            List<Tuple<Anforderung, Modul>> zugeordneteAnforderungModule) {

        List<VereinbarungDto> result = new ArrayList<>();

        Date loopStartDate = new Date();

        result.addAll(getVereinbarungDtoForExistingData(existingDerivatAnforderungModulList, derivatAnforderungModulIdZakIdMap, selectedDerivate));
        result.addAll(getVereinbarungDtoForZugeordneteAnforderungModule(zugeordneteAnforderungModule, selectedDerivate));

        result.sort(new VereinbarungDtoComparator());

        Date loopEndDate = new Date();
        LogUtils.logDiff(loopStartDate, loopEndDate, "loop");

        return result;

    }

    private static List<VereinbarungDto> getVereinbarungDtoForExistingData(
            List<DerivatAnforderungModul> existingDerivatAnforderungModulList,
            Map<Long, String> derivatAnforderungModulIdZakIdMap,
            List<Derivat> selectedDerivate) {
        List<VereinbarungDto> result = new ArrayList<>();

        List<Tuple<Anforderung, Modul>> existingAnforderungModulTuples = convertToAnforderungModulTuple(existingDerivatAnforderungModulList);

        Iterator<Tuple<Anforderung, Modul>> existingAnforderungModulTupleIterator = existingAnforderungModulTuples.iterator();
        while (existingAnforderungModulTupleIterator.hasNext()) {
            Tuple<Anforderung, Modul> anforderungModul = existingAnforderungModulTupleIterator.next();

            Anforderung anforderung = anforderungModul.getX();
            Modul modul = anforderungModul.getY();

            List<DerivatAnforderungModul> derivatAnforderungModuleForAnforderungModulTuple = getDerivatAnforderungModuleForAnforderungModulTuple(anforderungModul, existingDerivatAnforderungModulList);
            String zakId = getZakIdForAnforderungModulTuple(derivatAnforderungModuleForAnforderungModulTuple, derivatAnforderungModulIdZakIdMap);

            long version = anforderung.getVersion().longValue();
            VereinbarungDto vereinbarungDto;

            vereinbarungDto = getVereinbarungDTO(anforderung, modul, zakId, version);

            List<Derivat> allDerivate = new ArrayList<>(selectedDerivate);

            Iterator<DerivatAnforderungModul> iterator = derivatAnforderungModuleForAnforderungModulTuple.iterator();
            while (iterator.hasNext()) {
                DerivatAnforderungModul derivatAnforderungModul = iterator.next();

                Optional<DerivatAnforderungModulHistory> latestHistoryEntry = derivatAnforderungModul.getLatestHistoryEntry();

                String latestHistoryEntryValue;
                if (latestHistoryEntry.isPresent()) {
                    latestHistoryEntryValue = latestHistoryEntry.get().getKommentar();
                } else {
                    latestHistoryEntryValue = "";
                }

                DerivatVereinbarungDto newDerivatVereinbarungDto = DerivatVereinbarungDto
                        .forDerivatAnforderungModul(derivatAnforderungModul)
                        .withKommentar(latestHistoryEntryValue)
                        .build();

                vereinbarungDto.addDerivatVereinbarung(newDerivatVereinbarungDto);

                allDerivate.remove(derivatAnforderungModul.getDerivat());

            }

            Iterator<Derivat> remainingDerivatIterator = allDerivate.iterator();
            while (remainingDerivatIterator.hasNext()) {
                vereinbarungDto.addDerivatVereinbarung(getDerivatVereinbarungForDerivat(remainingDerivatIterator.next(), false));
            }

            result.add(vereinbarungDto);
        }

        return result;

    }

    private static VereinbarungDto getVereinbarungDTO(Anforderung anforderung, Modul modul, String zakId, Long version) {

        Optional<Prozessbaukasten> gueltigerProzessbaukasten = anforderung.getGueltigerProzessbaukasten();
        ProzessbaukastenLabelDto prozessbaukastenLabelDto;
        if (gueltigerProzessbaukasten.isPresent()) {
            Prozessbaukasten prozessbaukasten = gueltigerProzessbaukasten.get();
            prozessbaukastenLabelDto = new ProzessbaukastenLabelDto(prozessbaukasten.getId(), prozessbaukasten.getFachId(), prozessbaukasten.getVersion());
        } else {
            prozessbaukastenLabelDto = null;
        }

        final List<Long> festgestelltInIds = anforderung.getFestgestelltIn().stream().map(FestgestelltIn::getId).collect(Collectors.toList());
        final SensorCoc anforderungSensorCoc = anforderung.getSensorCoc();

        return new VereinbarungDto(
                anforderung.getId(),
                anforderung.getFachId(),
                anforderung.toString(),
                anforderung.getBeschreibungAnforderungDe(),
                modul.getId(),
                modul.getName(),
                zakId,
                anforderungSensorCoc.toString(),
                anforderungSensorCoc.getSensorCocId(),
                festgestelltInIds,
                version, prozessbaukastenLabelDto
        );
    }

    private static List<VereinbarungDto> getVereinbarungDtoForZugeordneteAnforderungModule(List<Tuple<Anforderung, Modul>> zugeordneteAnforderungModule, List<Derivat> selectedDerivate) {

        List<VereinbarungDto> result = new ArrayList<>();

        Iterator<Tuple<Anforderung, Modul>> zugeordneteAnforderungModuleIterator = zugeordneteAnforderungModule.iterator();

        while (zugeordneteAnforderungModuleIterator.hasNext()) {
            Tuple<Anforderung, Modul> anforderungModul = zugeordneteAnforderungModuleIterator.next();

            Anforderung anforderung = anforderungModul.getX();
            Modul modul = anforderungModul.getY();

            long version = anforderung.getVersion().longValue();
            VereinbarungDto vereinbarungDto;

            vereinbarungDto = getVereinbarungDTO(anforderung, modul, "", version);

            Iterator<Derivat> selectedDerivatIterator = selectedDerivate.iterator();
            while (selectedDerivatIterator.hasNext()) {
                vereinbarungDto.addDerivatVereinbarung(getDerivatVereinbarungForDerivat(selectedDerivatIterator.next(), true));
            }

            result.add(vereinbarungDto);

        }

        return result;
    }

    private static DerivatVereinbarungDto getDerivatVereinbarungForDerivat(Derivat derivat, boolean zugeordnet) {
        DerivatVereinbarungDto.Builder builder = DerivatVereinbarungDto.forDerivat(derivat);

        if (zugeordnet) {
            builder.withZuordnung();
        }

        return builder.build();
    }

}
