package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.controller.dialogs.EditCommentDialogController;
import de.interfaceag.bmw.pzbk.converters.DerivatAnforderungModulStatusConverter;
import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.comparator.DerivatComparator;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.ProcessResultDto;
import de.interfaceag.bmw.pzbk.shared.utils.LogUtils;
import de.interfaceag.bmw.pzbk.shared.utils.UrlParameterUtils;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.DerivatVereinbarungDto;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author sl
 */
@ViewScoped
@Named
public class VereinbarungController implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(VereinbarungController.class);

    @Inject
    private Session session;

    @Inject
    private LocalizationService localizationService;

    @Inject
    private VereinbarungViewFacade facade;

    private EditCommentDialogController editCommentDialogController;

    // ---------- view data ----------------------------------------------------
    private VereinbarungViewData viewData;

    // ---------- init methods -------------------------------------------------
    @PostConstruct
    public void init() {
        LOG.info("Start init");

        initViewData();

        initEditCommentDialogController();

        session.setLocationForView();

        LOG.info("End init");
        Date startDate = new Date();
        Date endDate = new Date();
        LogUtils.logDiff(startDate, endDate);
    }

    private void initViewData() {
        viewData = facade.getViewData(UrlParameterUtils.getUrlParameter());
    }

    private void initEditCommentDialogController() {
        editCommentDialogController = new EditCommentDialogController(localizationService.getValue("kommentarStatuswechsel"));
    }

    // ---------- methods ------------------------------------------------------
    public String updateDerivatList() {
        facade.updateUserDerivate(getViewData().getFilteredDerivate());
        return filter();
    }

    public void updateKommetarDerivate() {
        getViewData().getFilteredDerivate().sort(new DerivatComparator());
    }

    private void setSingleDerivatSelected() {
        if (getViewData().getFilteredDerivate() != null && getViewData().getFilteredDerivate().size() == 1) {
            setSelectedDerivat(getViewData().getFilteredDerivate().get(0));
        }
    }

    public String edit() {
        setSingleDerivatSelected();
        getViewData().getFilteredDerivate().sort(new DerivatComparator());
        if (getSelectedDerivat() == null) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Aktion",
                    "Bitte wählen Sie ein Derivat aus."));
            return "";
        } else {
            getFilter().getEditModeFilter().setActive();
        }
        return filter();
    }

    public String process() {
        return facade.process(getViewData());
    }

    public String toggleVereinbarenMode() {

        setSingleDerivatSelected();

        Derivat selectedDerivat = getSelectedDerivat();

        if (selectedDerivat == null) {
            return filter();
        }

        VereinbarungViewFilter filter = getFilter();
        VereinbarungQuickFilter.applyVereinbarenModeQuickFilter(selectedDerivat, filter);

        return filter();
    }



    public String toggleFpZakMode() {

        setSingleDerivatSelected();

        Derivat selectedDerivat = getSelectedDerivat();

        if (selectedDerivat == null) {
            return filter();
        }

        VereinbarungViewFilter filter = getFilter();
        VereinbarungQuickFilter.applyFolgeprozessZakQuickFilter(selectedDerivat, filter);

        return filter();
    }



    public String toggleFolgeprozessZakKeineWeiterverfolgungMode() {
        setSingleDerivatSelected();

        Derivat selectedDerivat = getSelectedDerivat();
        if (selectedDerivat == null) {
            return filter();
        }

        VereinbarungViewFilter filter = getFilter();
        VereinbarungQuickFilter.applyFolgeprozessZakKeineWeiterverfolungQuickFilter(selectedDerivat, filter);

        return filter();
    }



    public String toggleFpUbMode() {
        setSingleDerivatSelected();

        Derivat selectedDerivat = getSelectedDerivat();
        if (selectedDerivat == null) {
            return filter();
        }

        VereinbarungViewFilter filter = getFilter();
        VereinbarungQuickFilter.applyFolgeprozessUmsetzungsverwaltungQuickFilter(selectedDerivat, filter);

        return filter();
    }


    //=====================content
    public void enableDerivat(Derivat derivat) {
        if (getSelectedDerivat() != null && getSelectedDerivat().equals(derivat)) {
            setSelectedDerivat(null);
        } else {
            setSelectedDerivat(derivat);
        }
    }

    public void selectAllAnforderungen(boolean allselected) {
        setAllSelected(allselected);
        if (allselected) {
            setSelectedTableData(getTableData());
        } else {
            setSelectedTableData(new ArrayList<>());
        }
    }

    public void switchDerivatVereinbarungEditMode(DerivatVereinbarungDto derivatVereinbarungDto) {
        if (derivatVereinbarungDto.isInEditMode()) {
            derivatVereinbarungDto.setInEditMode(false);
        } else {
            derivatVereinbarungDto.setInEditMode(true);
        }
    }

    public void saveDerivatVereinbarungEdit(VereinbarungDto row, Derivat derivat) {
        ProcessResultDto processResult = facade.processVereinbarungRowChanges(row, derivat);
        if (!processResult.isSuccessful()) {
            LOG.info("leider nicht erfolgreich");
//            showErrorMessageInGrowl(processResult.getMessage());
        }
        switchDerivatVereinbarungEditMode(row.getVereinbarungForDerivat(derivat.getId()));
    }

    public void showEditCommentDialog(DerivatVereinbarungDto derivatVereinbarung) {
        editCommentDialogController.show(derivatVereinbarung);
    }

    public EditCommentDialogController getEditCommentDialogController() {
        return editCommentDialogController;
    }

    //Link generieren
    public void updateMessageCopyToClipboard() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null,
                new FacesMessage(
                        FacesMessage.SEVERITY_INFO,
                        "Link wurde in das Clipboard kopiert",
                        "")
        );
    }

    public String getUrl() {
        return facade.getUrl(getViewData());
    }

    public void setUrl(String url) {
        // do nothing
    }

    public void downloadExcelExport() {
        facade.downloadExcelExport(getTableData());
    }

    public String buildZakUrl(String zakId) {
        return facade.getZakUrl(zakId);
    }

    public VereinbarungViewData getViewData() {
        return viewData;
    }

    public VereinbarungViewPermission getViewPermission() {
        return getViewData().getViewPermission();
    }

    public VereinbarungViewFilter getFilter() {
        return getViewData().getFilter();
    }

    public String filter() {
        return getFilter().getUrl();
    }

    public String reset() {
        return getFilter().getResetUrl();
    }

    public List<VereinbarungDto> getTableData() {
        return getViewData().getTableData();
    }

    public List<VereinbarungDto> getSelectedTableData() {
        return getViewData().getSelectedTableData();
    }

    public void setSelectedTableData(List<VereinbarungDto> selectedTableData) {
        this.getViewData().setSelectedTableData(selectedTableData);
    }

    public Derivat getSelectedDerivat() {
        return getViewData().getSelectedDerivat();
    }

    public void setSelectedDerivat(Derivat selectedDerivat) {
        this.getViewData().setSelectedDerivat(selectedDerivat);
    }

    public boolean isAllSelected() {
        return getViewData().isAllSelected();
    }

    public void setAllSelected(boolean allSelected) {
        this.getViewData().setAllSelected(allSelected);
    }

    public DerivatAnforderungModulStatus getGroupStatus() {
        return getViewData().getGroupStatus();
    }

    public void setGroupStatus(DerivatAnforderungModulStatus groupStatus) {
        this.getViewData().setGroupStatus(groupStatus);
    }

    public List<DerivatAnforderungModulStatus> getAllNextStatusList() {
        return DerivatAnforderungModulStatus.getAllStatusListForVereinbarung();
    }

    public List<DerivatAnforderungModulStatus> getNextStatusListByStatus(DerivatAnforderungModulStatus derivatAnforderungModulStatus, DerivatAnforderungModul da) {
        return DerivatAnforderungModulStatus.getNextStatusListByStatus(derivatAnforderungModulStatus);
    }

    public DerivatAnforderungModulStatusConverter getDerivatAnforderungModulStatusConverter() {
        return new DerivatAnforderungModulStatusConverter();
    }

    public boolean isEditMode() {
        return getFilter().getEditModeFilter().isActive();
    }

    public DerivatDto getDerivat() {
        return getViewData().getSelectedDerivat();
    }

}
