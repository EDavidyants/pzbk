package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.dao.DerivatAnforderungModulDao;
import de.interfaceag.bmw.pzbk.dao.DerivatAnforderungModulHistoryDao;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModulHistory;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.Umsetzungsbestaetigung;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.mail.MailService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author sl
 */
@Stateless
@Named
public class DerivatAnforderungModulService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(DerivatAnforderungModulService.class.getName());

    @Inject
    protected DerivatAnforderungModulDao derivatAnforderungModulDao;

    @Inject
    protected DerivatAnforderungModulHistoryDao derivatAnforderungModulHistoryDao;

    @Inject
    private AnforderungMeldungHistoryService historyService;
    @Inject
    private VereinbarungHistoryService vereinbarungHistoryService;
    @Inject
    private BerechtigungService berechtigungService;
    @Inject
    protected MailService mailService;
    @Inject
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;
    @Inject
    private ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;

    public Collection<Mitarbeiter> getAnfordererForDerivatenOfAnforderung(Anforderung anforderung) {
        Set<Mitarbeiter> result = new HashSet<>();
        List<Derivat> derivaten = derivatAnforderungModulDao.getDerivatenForAnforderung(anforderung);

        List<Mitarbeiter> mitarbeiter = berechtigungService.getMitarbeiterByDerivatenInListRolleAndRechttype(derivaten, Rolle.ANFORDERER, Rechttype.SCHREIBRECHT);
        result.addAll(mitarbeiter);
        return result;
    }

    public void updateDerivatAnforderungModulStatus(DerivatAnforderungModulStatus status, String kommentar, Derivat derivat, Anforderung anforderung, Modul modul, Mitarbeiter currentUser) {
        DerivatAnforderungModul derivatAnforderungModul = getDerivatAnforderungModulByAnforderungAndModulAndDerivat(anforderung, modul, derivat);
        if (derivatAnforderungModul != null) {
            if (status.equals(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG)) {
                derivatAnforderungModul.setUmsetzungsBestaetigungStatus(UmsetzungsBestaetigungStatus.KEINE_WEITERVERFOLGUNG);
            }
            updateDerivatAnforderungModulStatus(status, kommentar, derivatAnforderungModul, currentUser);
        }
    }

    public void updateDerivatAnforderungModulStatusForUmsetzungsbestaetigung(UmsetzungsBestaetigungStatus umsetzungsBestaetigungStatus,
            KovAPhase kovAPhase, String kommentar, Derivat derivat, Anforderung anforderung, Modul modul, Mitarbeiter user) {
        DerivatAnforderungModul derivatAnforderungModul = getDerivatAnforderungModulByAnforderungAndModulAndDerivat(anforderung, modul, derivat);

        if (derivatAnforderungModul == null) {
            zuordnungAnforderungDerivatService.assignAnforderungToDerivat(ZuordnungStatus.ZUGEORDNET, anforderung, derivat, kommentar, user);
        } else {

            derivatAnforderungModul.setUmsetzungsBestaetigungStatus(umsetzungsBestaetigungStatus);
            derivatAnforderungModul.setKovAPhase(kovAPhase);
            DerivatAnforderungModulHistory derivatAnforderungModulHistory = new DerivatAnforderungModulHistory(derivatAnforderungModul, umsetzungsBestaetigungStatus.toString(), new Date(), user.toString(), kommentar);
            vereinbarungHistoryService.addVereinbarungHistoryEntry(derivatAnforderungModulHistory);
        }
    }

    private void updateDerivatAnforderungModulBasedOnDerivatStatus(DerivatAnforderungModul derivatAnforderungModul, DerivatAnforderungModulStatus newStatus) {
        DerivatStatus derivatStatus = derivatAnforderungModul.getDerivat().getStatus();
        switch (derivatStatus) {
            case VEREINARBUNG_VKBG:
                derivatAnforderungModul.setStatusVKBG(newStatus);
                derivatAnforderungModul.setStatusZV(newStatus);
                break;
            case VEREINBARUNG_ZV:
                derivatAnforderungModul.setStatusZV(newStatus);
                break;
            default:
        }
    }

    private DerivatAnforderungModulStatus getOldDerivatAnforderungModulStatus(DerivatAnforderungModul derivatAnforderungModul) {
        DerivatStatus derivatStatus = derivatAnforderungModul.getDerivat().getStatus();
        switch (derivatStatus) {
            case VEREINBARUNG_ZV:
                return derivatAnforderungModul.getStatusZV();
            case VEREINARBUNG_VKBG:
            default:
                return derivatAnforderungModul.getStatusVKBG();
        }
    }

    public void updateDerivatAnforderungModulStatus(DerivatAnforderungModulStatus status, String kommentar, DerivatAnforderungModul derivatAnforderungModul, Mitarbeiter user) {
        DerivatAnforderungModulStatus oldStatus = getOldDerivatAnforderungModulStatus(derivatAnforderungModul);
        updateDerivatAnforderungModulBasedOnDerivatStatus(derivatAnforderungModul, status);

        // create and save UBs for DerivatAnforderungModul if Status is ANGENOMMEN or in Klaerung or nicht bewertbar
        switch (status) {
            case ANGENOMMEN:
            case IN_KLAERUNG:
            case NICHT_BEWERTBAR:
                if (!derivatAnforderungModul.getKovAPhase().isPresent()) {
                    break;
                }
                Optional<Umsetzungsbestaetigung> derivatAnforderungModulUb
                        = umsetzungsbestaetigungService.getUmsetzungsbestaetigungByAnforderungModulDerivatPhase(derivatAnforderungModul.getAnforderung(),
                                derivatAnforderungModul.getDerivat(), derivatAnforderungModul.getModul(), derivatAnforderungModul.getKovAPhase().orElse(null));
                if (derivatAnforderungModulUb.isPresent() && !derivatAnforderungModulUb.get().getStatus().equals(UmsetzungsBestaetigungStatus.OFFEN)) {
                    derivatAnforderungModul.setUmsetzungsBestaetigungStatus(derivatAnforderungModulUb.get().getStatus());
                } else {
                    List<Umsetzungsbestaetigung> derivatAnforderungModulUbList = umsetzungsbestaetigungService.createUmsetzungsbestaetigungenForDerivatAnforderungModul(derivatAnforderungModul);
                    if (derivatAnforderungModulUbList != null && !derivatAnforderungModulUbList.isEmpty() && !derivatAnforderungModulUbList.get(0).getStatus().equals(UmsetzungsBestaetigungStatus.OFFEN)) {
                        derivatAnforderungModul.setUmsetzungsBestaetigungStatus(derivatAnforderungModulUbList.get(0).getStatus());
                    }
                    umsetzungsbestaetigungService.persistUmsetzungsbestaetigungen(derivatAnforderungModulUbList);
                }
                persistDerivatAnforderungModul(derivatAnforderungModul);
                break;
            case KEINE_WEITERVERFOLGUNG:
            case UNZUREICHENDE_ANFORDERUNGSQUALITAET:
                removeUmsetzungsbestaetigungenForDerivatAnforderungModul(derivatAnforderungModul);
                break;
            case ABZUSTIMMEN:
                zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.ZUGEORDNET, derivatAnforderungModul.getZuordnungAnforderungDerivat(), user);
                removeUmsetzungsbestaetigungenForDerivatAnforderungModul(derivatAnforderungModul);
                break;
            default:
                break;
        }

        String currentUser = user.toString();
        DerivatAnforderungModulHistory derivatAnforderungModulHistory = new DerivatAnforderungModulHistory(derivatAnforderungModul, status.toString(), new Date(), currentUser, kommentar);
        vereinbarungHistoryService.addVereinbarungHistoryEntry(derivatAnforderungModulHistory);

        // differentiate AnforderungHistory record upon comment
        addAnforderungHistoryEntry(derivatAnforderungModul, kommentar, oldStatus, status, user);

        sendMailToAnfordererBasedOnStatus(derivatAnforderungModul);
    }

    private void addAnforderungHistoryEntry(DerivatAnforderungModul derivatAnforderungModul, String kommentar, DerivatAnforderungModulStatus oldStatus, DerivatAnforderungModulStatus status, Mitarbeiter user) {
        String currentUser = user.toString();
        AnforderungHistory anforderungHistory;

        if (kommentar.contains("nach ZAK")) {
            int pos = kommentar.indexOf("nach ZAK");
            anforderungHistory = new AnforderungHistory(derivatAnforderungModul.getZuordnungAnforderungDerivat().getAnforderung().getId(),
                    "A", new Date(), "Zuordnung", currentUser,
                    "", kommentar.substring(pos) + ", Derivat " + derivatAnforderungModul.getDerivat().getName());

        } else {
            anforderungHistory = new AnforderungHistory(derivatAnforderungModul.getZuordnungAnforderungDerivat().getAnforderung().getId(),
                    "A", new Date(), "Zuordnung", currentUser,
                    derivatAnforderungModul.getModul().getName() + " im Derivat " + derivatAnforderungModul.getDerivat().getName() + ": " + oldStatus.toString(),
                    derivatAnforderungModul.getModul().getName() + " im Derivat " + derivatAnforderungModul.getDerivat().getName() + ": " + status.toString());
        }

        historyService.persistAnforderungHistory(anforderungHistory);
    }

    /**
     * Send mail to Anforderer if there is a conflict between
     * DerivatAnforderungModulStatus and ZakStatus.
     *
     * @param derivatAnforderungModul
     */
    protected void sendMailToAnfordererBasedOnStatus(DerivatAnforderungModul derivatAnforderungModul) {
        if (derivatAnforderungModul.getStatusZV().equals(DerivatAnforderungModulStatus.ANGENOMMEN)) {
            switch (derivatAnforderungModul.getZakStatus()) {
                case NICHT_BEWERTBAR:
                case NICHT_UMSETZBAR:
                case ZURUECKGEZOGEN_DURCH_ANFORDERER:
                    mailService.sendMailToAnforderer(derivatAnforderungModul, true);
                    break;
                case EMPTY:
                case PENDING:
                case DONE:
                case KEINE_ZAKUEBERTRAGUNG:
                case UEBERTRAGUNG_FEHLERHAFT:
                case BESTAETIGT_AUS_VORPROZESS:
                default:
                    break;
            }
        }
    }

    private void removeUmsetzungsbestaetigungenForDerivatAnforderungModul(DerivatAnforderungModul derivatAnforderungModul) {
        //update umsetzungsbestaetigung objecs of modul
        List<Umsetzungsbestaetigung> derivatAnforderungModulUbList = umsetzungsbestaetigungService.getUmsetzungsbestaetigungenByDerivatAnforderungModul(derivatAnforderungModul);
        // if no kovaphase is active or abgeschlossen then remove all umsetzungsbestaetigungen OR if a kovaphase is active then update state of all umsetzungsbestaetigung objects
        if (derivatAnforderungModulUbList.stream().allMatch(u -> u.getKovaImDerivat().getKovaStatus().equals(KovAStatus.OFFEN) || u.getKovaImDerivat().getKovaStatus().equals(KovAStatus.KONFIGURIERT))) {
            umsetzungsbestaetigungService.removeUmsetzungsbestaetigungen(derivatAnforderungModulUbList);
        } else if (derivatAnforderungModulUbList.stream().anyMatch(u -> u.getKovaImDerivat().getKovaStatus().equals(KovAStatus.AKTIV))) {
            derivatAnforderungModulUbList.forEach(u -> u.setStatus(UmsetzungsBestaetigungStatus.KEINE_WEITERVERFOLGUNG));
            umsetzungsbestaetigungService.persistUmsetzungsbestaetigungen(derivatAnforderungModulUbList);
        }
    }

    public void createDerivatAnforderungModulForAnforderungDerivatZuordnung(ZuordnungAnforderungDerivat anforderungDerivat,
            DerivatAnforderungModulStatus das, String kommentar, Mitarbeiter currentUser) {
        List<Modul> module = anforderungDerivat.getAnforderung().getUmsetzer().stream().map(Umsetzer::getModul).distinct().collect(Collectors.toList());
        for (Modul modul : module) {
            if (getDerivatAnforderungModulByAnforderungAndModulAndDerivat(anforderungDerivat.getAnforderung(), modul, anforderungDerivat.getDerivat()) == null) {

                DerivatAnforderungModul newVereinbarung = VereinbarungFactory.createNewVereinbarung(anforderungDerivat, modul, currentUser, das);

                if (newVereinbarung != null) {
                    persistDerivatAnforderungModul(newVereinbarung);

                    if (kommentar == null) {
                        kommentar = "";
                    }
                    DerivatAnforderungModulHistory derivatAnforderungModulHistory = new DerivatAnforderungModulHistory(newVereinbarung,
                            newVereinbarung.getStatusZV().toString(), new Date(), currentUser.toString(), kommentar);
                    vereinbarungHistoryService.addVereinbarungHistoryEntry(derivatAnforderungModulHistory);
                } else {
                    LOG.warn("No DerivatAnforderungModul was created for {} and {}", anforderungDerivat, modul);
                }
            }
        }
    }

    public void updateDerivatAnforderungModulZakStatus(ZakStatus zakStatus, String zakKommentar, DerivatAnforderungModul derivatAnforderungModul) {
        if (zakStatus != null && derivatAnforderungModul != null) {
            derivatAnforderungModul.setZakStatus(zakStatus);
            persistDerivatAnforderungModul(derivatAnforderungModul);
            updateDerivatAnforderungModulZakStatusInHistory(derivatAnforderungModul, zakStatus, zakKommentar);
        }
    }

    private void updateDerivatAnforderungModulZakStatusInHistory(DerivatAnforderungModul derivatAnforderungModul, ZakStatus zakStatus, String zakKommentar) {
        DerivatAnforderungModulHistory derivatAnforderungModulHistory = new DerivatAnforderungModulHistory(derivatAnforderungModul, zakStatus.toString(), new Date(), "ZAK", zakKommentar);
        vereinbarungHistoryService.addVereinbarungHistoryEntry(derivatAnforderungModulHistory);
    }

    public List<DerivatAnforderungModul> getDerivatAnforderungModulByAnforderungStatus(Anforderung anforderung, DerivatAnforderungModulStatus status) {
        if (anforderung != null && anforderung.getId() != null) {
            return derivatAnforderungModulDao.getDerivatAnforderungModulByAnforderungStatus(anforderung.getId(), status);
        }
        return new ArrayList<>();
    }

    public List<SensorCoc> getSensorCocForDerivat(Derivat derivat) {
        return derivatAnforderungModulDao.getSensorCocForDerivat(derivat);
    }

    public List<SensorCoc> getSensorCocForDerivatStatusAndKovaPhaseImDerivat(List<Integer> status, KovAPhaseImDerivat kovAPhaseImDerivat) {
        return derivatAnforderungModulDao.getSensorCocForDerivatStatusAndKovaPhase(kovAPhaseImDerivat.getDerivat(), status, kovAPhaseImDerivat.getKovAPhase());
    }

    public void persistDerivatAnforderungModul(DerivatAnforderungModul df) {
        derivatAnforderungModulDao.persistDerivatAnforderungModul(df);
    }

    public void persistDerivatAnforderungModulen(List<DerivatAnforderungModul> derivatAnforderungModulen) {
        derivatAnforderungModulDao.persistDerivatAnforderungModulen(derivatAnforderungModulen);
    }

    public void removeDerivatAnforderungModulForZuordnungAnforderungDerivat(ZuordnungAnforderungDerivat zad) {
        List<DerivatAnforderungModul> damList = getDerivatAnforderungModulByAnforderungAndDerivat(zad.getAnforderung(), zad.getDerivat());
        damList.forEach(this::removeDerivatAnforderungModulAndHistory);
    }

    public void removeDerivatAnforderungModulAndHistory(DerivatAnforderungModul derivatAnforderungModul) {
        resetLatestHistoryEntry(derivatAnforderungModul);
        vereinbarungHistoryService.getHistoryForDerivatAnforderungModul(derivatAnforderungModul)
                .forEach(dah -> vereinbarungHistoryService.removeDerivatAnforderungModulHistory(dah));

        removeDerivatAnforderungModul(derivatAnforderungModul);
    }

    protected void resetLatestHistoryEntry(DerivatAnforderungModul derivatAnforderungModul) {
        if (derivatAnforderungModul != null) {
            derivatAnforderungModul.setLatestHistoryEntry(null);
            persistDerivatAnforderungModul(derivatAnforderungModul);
        }
    }

    public void removeDerivatAnforderungModul(DerivatAnforderungModul df) {
        List<DerivatAnforderungModulHistory> historyList = derivatAnforderungModulHistoryDao.getDerivatAnforderungModulHistoryByDerivatAnforderungModulId(df.getId());
        resetLatestHistoryEntry(df);

        if (historyList != null && !historyList.isEmpty()) {
            historyList.forEach(ah -> derivatAnforderungModulHistoryDao.remove(ah));
        }
        derivatAnforderungModulDao.removeDerivatAnforderungModul(df);
    }

    public DerivatAnforderungModul getDerivatAnforderungModulById(Long id) {
        return derivatAnforderungModulDao.getDerivatAnforderungModulById(id);
    }

    public List<DerivatAnforderungModul> getDerivatAnforderungModulByIdList(List<Long> idList) {
        return derivatAnforderungModulDao.getDerivatAnforderungModulByIdList(idList);
    }

    public List<DerivatAnforderungModul> getAllDerivatAnforderungModulen() {
        return derivatAnforderungModulDao.getAllDerivatAnforderungModulen();
    }

    public List<DerivatAnforderungModul> getDerivatAnforderungModulByAnforderungAndDerivat(Anforderung anforderung, Derivat derivat) {
        return derivatAnforderungModulDao.getDerivatAnforderungModulByAnforderungAndDerivat(anforderung, derivat);
    }

    public DerivatAnforderungModul getDerivatAnforderungModulByAnforderungAndModulAndDerivat(Anforderung anforderung, Modul modul, Derivat derivat) {
        return derivatAnforderungModulDao.getDerivatAnforderungModulByAnforderungAndModulAndDerivat(anforderung, modul, derivat);
    }

    public List<DerivatAnforderungModul> getDerivatAnforderungModulByAnforderungAndModul(Long anforderung, Long modul) {
        return derivatAnforderungModulDao.getDerivatAnforderungModulByAnforderungAndModul(anforderung, modul);
    }

    public List<SensorCoc> getSensorCocForVereinbarungenWithStatusAngenommenAndKovaPhaseAndSensorCocIdNotInList(KovAPhaseImDerivat kovAPhaseImDerivat, List<Long> existingSensorCocIds) {
        if (kovAPhaseImDerivat.getDerivat() == null || kovAPhaseImDerivat.getDerivat().getId() == null || existingSensorCocIds == null) {
            LOG.error("kovaPhaseImDerivat is corrupt as derivat is null");
            return new ArrayList<>();
        }
        Long derivatId = kovAPhaseImDerivat.getDerivat().getId();
        if (existingSensorCocIds.isEmpty()) {
            return derivatAnforderungModulDao.getSensorCocForVereinbarungenWithStatusAngenommenAndDerivatId(derivatId);
        } else {
            return derivatAnforderungModulDao.getSensorCocForVereinbarungenWithStatusAngenommenAndDerivatIdAndSensorCocIdNotInList(derivatId, existingSensorCocIds);
        }
    }

}
