package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.FalsePermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class VereinbarungViewPermission implements Serializable {

    // page
    private final ViewPermission page;

    // header
    private EditPermission editButton;
    private EditPermission processChangesButton;
    private EditPermission cancelButton;

    private final boolean editMode;
    private final Set<Rolle> userRoles;

    public VereinbarungViewPermission(Set<Rolle> userRoles, boolean editMode, Derivat selectedDerivat) {
        this.editMode = editMode;
        this.userRoles = userRoles;
        this.page = RolePermission.builder()
                .authorizeReadForRole(Rolle.ADMIN)
                .authorizeReadForRole(Rolle.T_TEAMLEITER)
                .authorizeReadForRole(Rolle.TTEAM_VERTRETER)
                .authorizeReadForRole(Rolle.ANFORDERER)
                .compareTo(userRoles).get();

        if (editMode) {
            initEditButtonsForEditMode(selectedDerivat);
        } else {
            initEditButtonsForViewMode(selectedDerivat);
        }
    }

    private void initEditButtonsForViewMode(Derivat selectedDerivat) {
        this.processChangesButton = new FalsePermission();
        this.cancelButton = new FalsePermission();
        if (selectedDerivat != null && selectedDerivat.isVereinbarungActive()) {
            this.editButton = RolePermission.builder()
                    .authorizeWriteForRole(Rolle.ADMIN)
                    .authorizeWriteForRole(Rolle.ANFORDERER)
                    .compareTo(userRoles).get();
        } else {
            this.editButton = new FalsePermission();
        }
    }

    private void initEditButtonsForEditMode(Derivat selectedDerivat) {
        this.editButton = new FalsePermission();
        if (selectedDerivat != null && selectedDerivat.isVereinbarungActive()) {
            this.processChangesButton = RolePermission.builder()
                    .authorizeWriteForRole(Rolle.ADMIN)
                    .authorizeWriteForRole(Rolle.ANFORDERER)
                    .compareTo(userRoles).get();

            this.cancelButton = RolePermission.builder()
                    .authorizeWriteForRole(Rolle.ADMIN)
                    .authorizeWriteForRole(Rolle.ANFORDERER)
                    .compareTo(userRoles).get();
        } else {
            this.processChangesButton = new FalsePermission();
            this.cancelButton = new FalsePermission();
        }
    }

    public void updateSelectedDerivat(Derivat selectedDerivat) {
        if (editMode) {
            initEditButtonsForEditMode(selectedDerivat);
        } else {
            initEditButtonsForViewMode(selectedDerivat);
        }
    }

    public boolean getPage() {
        return page.isRendered();
    }

    public boolean getEditButton() {
        return editButton.hasRightToEdit();
    }

    public boolean getProcessChangesButton() {
        return processChangesButton.hasRightToEdit();
    }

    public boolean getCancelButton() {
        return cancelButton.hasRightToEdit();
    }

}
