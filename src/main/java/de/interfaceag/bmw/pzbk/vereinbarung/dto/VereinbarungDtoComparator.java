package de.interfaceag.bmw.pzbk.vereinbarung.dto;

import de.interfaceag.bmw.pzbk.comparator.FachIdComparator;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author sl
 */
public class VereinbarungDtoComparator implements Comparator<VereinbarungDto>, Serializable {

    @Override
    public int compare(VereinbarungDto v1, VereinbarungDto v2) {
        String fachId1 = v1.getFachId();
        String fachId2 = v2.getFachId();

        String modulName1 = v1.getModulName();
        String modulName2 = v2.getModulName();

        FachIdComparator fachIdComparator = new FachIdComparator();
        int fachIdCompareResult = fachIdComparator.compare(fachId2, fachId1);
        if (fachIdCompareResult == 0) {
            return modulName1.compareTo(modulName2);
        } else {
            return fachIdCompareResult;
        }
    }
}
