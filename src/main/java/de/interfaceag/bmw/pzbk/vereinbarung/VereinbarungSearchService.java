package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.dao.SearchDao;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.shared.QueryFactory;
import de.interfaceag.bmw.pzbk.shared.dto.AnforderungIdModulIdTuple;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import de.interfaceag.bmw.pzbk.shared.dto.SearchFilterDTO;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.DerivatVereinbarungStatusFilter;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static de.interfaceag.bmw.pzbk.shared.utils.InputValidationUtils.allObjectsAreNotNull;

/**
 * @author fp
 */
@Stateless
public class VereinbarungSearchService implements Serializable {

    public static final String AND_1_0 = " AND 1 = 0";
    @Inject
    private SearchDao searchDao;

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private transient EntityManager entityManager;

    public List<DerivatAnforderungModul> getVereinbarungDataForFilter(VereinbarungViewFilter viewFilter, List<Derivat> derivate) {

        List<Long> derivatIds = derivate.stream().map(Derivat::getId).collect(Collectors.toList());

        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT dam FROM DerivatAnforderungModul dam "
                + "INNER JOIN dam.zuordnungAnforderungDerivat zad "
                + "INNER JOIN zad.anforderung a "
                + "LEFT JOIN FETCH a.anfoFreigabeBeiModul "
                + "LEFT JOIN a.prozessbaukastenThemenklammern t "
                + "WHERE zad.derivat.id IN :derivatIds");
        queryPart.put("derivatIds", derivatIds);

        VereinbarungSearchUtils.getAnforderungQuery(queryPart, viewFilter.getAnforderungFilter());
        VereinbarungSearchUtils.getBeschreibungQuery(queryPart, viewFilter.getBeschreibungFilter());
        VereinbarungSearchUtils.getFestgestlltInQuery(queryPart, viewFilter.getFestgestelltInFilter());
        VereinbarungSearchUtils.getPhasenrelevanzQuery(queryPart, viewFilter.getPhasenrelevanzFilter());
        VereinbarungSearchUtils.getSensorCocQuery(queryPart, viewFilter.getSensorCocFilter());
        VereinbarungSearchUtils.getThemenklammerQuery(queryPart, viewFilter.getThemenklammerFilter());
        VereinbarungSearchUtils.getProzessbaukastenQuery(queryPart, viewFilter.getProzessbaukastenFilter());
        VereinbarungSearchUtils.getInProzessbaukastenQuery(queryPart, viewFilter.getInProzessbaukastenFilter());
        VereinbarungSearchUtils.getNotInProzessbaukastenQuery(queryPart, viewFilter.getNotInProzessbaukastenFilter());
        VereinbarungSearchUtils.getDerivatAnforderungModulModulQuery(queryPart, viewFilter.getModulFilter());
        VereinbarungSearchUtils.getDerivatAnforderungModulByIdQuery(queryPart, viewFilter.getDerivatAnforderungModulIdFilter());
        getHistoryQueryPart(queryPart, viewFilter.getFreigegebenSeitFilter());
        getVereinbarungIdQueryPart(queryPart, viewFilter.getDerivatVereinbarungStatusFilter());

        QueryFactory<DerivatAnforderungModul> queryFactory = new QueryFactory<>();
        final TypedQuery<DerivatAnforderungModul> query = queryFactory.buildTypedQuery(queryPart, entityManager, DerivatAnforderungModul.class);
        return query.getResultList();
    }

    public List<Anforderung> getZuordnungSearchResultForFilter(VereinbarungViewFilter viewFilter, List<Derivat> derivate) {

        List<Long> derivatIds = derivate.stream().map(Derivat::getId).collect(Collectors.toList());
        QueryPart queryPart = VereinbarungSearchUtils.getBaseQuery(derivatIds);

        VereinbarungSearchUtils.getAnforderungQuery(queryPart, viewFilter.getAnforderungFilter());
        VereinbarungSearchUtils.getBeschreibungQuery(queryPart, viewFilter.getBeschreibungFilter());
        VereinbarungSearchUtils.getFestgestlltInQuery(queryPart, viewFilter.getFestgestelltInFilter());
        VereinbarungSearchUtils.getPhasenrelevanzQuery(queryPart, viewFilter.getPhasenrelevanzFilter());
        VereinbarungSearchUtils.getSensorCocQuery(queryPart, viewFilter.getSensorCocFilter());
        VereinbarungSearchUtils.getThemenklammerQuery(queryPart, viewFilter.getThemenklammerFilter());
        VereinbarungSearchUtils.getZuordnungAnforderungDerivatModulQuery(queryPart, viewFilter.getModulFilter());

        getHistoryQueryPart(queryPart, viewFilter.getFreigegebenSeitFilter());
        getAnforderungIdQueryPart(queryPart, viewFilter.getDerivatVereinbarungStatusFilter());

        QueryFactory<Anforderung> queryFactory = new QueryFactory<>();
        final TypedQuery<Anforderung> query = queryFactory.buildTypedQuery(queryPart, entityManager, Anforderung.class);
        return query.getResultList();
    }

    private void getVereinbarungIdQueryPart(QueryPart qp, List<DerivatVereinbarungStatusFilter> derivatVereinbarungStatusFilters) {
        if (derivatVereinbarungStatusFilters.stream().anyMatch(f -> f.getVereinbarungStatusFilter().isActive()
                || f.getUmsetzungsbestaetigungStatusFilter().isActive() || f.getZakStatusFilter().isActive())) {
            Set<AnforderungIdModulIdTuple> idList = getVereinbarungIdListForDerivatAnforderungFilter(derivatVereinbarungStatusFilters);
            VereinbarungSearchUtils.getVereinbarungIdQuery(qp, idList);
        }
    }

    private void getAnforderungIdQueryPart(QueryPart qp, List<DerivatVereinbarungStatusFilter> derivatVereinbarungStatusFilters) {
        if (derivatVereinbarungStatusFilters.stream().anyMatch(f -> f.getVereinbarungStatusFilter().isActive()
                || f.getUmsetzungsbestaetigungStatusFilter().isActive() || f.getZakStatusFilter().isActive())) {
            List<Long> idList = getAnforderungIdListForDerivatAnforderungFilter(derivatVereinbarungStatusFilters);
            VereinbarungSearchUtils.getAnforderungIdQuery(qp, idList);
        }
    }

    private Set<AnforderungIdModulIdTuple> getVereinbarungIdListForDerivatAnforderungFilter(
            List<DerivatVereinbarungStatusFilter> derivatVereinbarungStatusFilters) {

        Set<Long> columns = new HashSet<>(); // derivatIds
        Map<Long, List<DerivatAnforderungModulStatus>> vereinbarungStatus = new HashMap<>();
        Map<Long, List<ZakStatus>> zakStatus = new HashMap<>();
        Map<Long, List<UmsetzungsBestaetigungStatus>> umsetzungsbestaetigungStatus = new HashMap<>();
        Map<Long, DerivatStatus> derivatStatusMap = new HashMap<>();

        for (DerivatVereinbarungStatusFilter f : derivatVereinbarungStatusFilters) {
            if (f.getVereinbarungStatusFilter().isActive()) {
                vereinbarungStatus.put(f.getDerivatId(), f.getVereinbarungStatusFilter().getAsEnumList());
                columns.add(f.getDerivatId());
                derivatStatusMap.put(f.getDerivatId(), f.getDerivatStatus());
            }
            if (f.getZakStatusFilter().isActive()) {
                zakStatus.put(f.getDerivatId(), f.getZakStatusFilter().getAsEnumList());
                columns.add(f.getDerivatId());
                derivatStatusMap.put(f.getDerivatId(), f.getDerivatStatus());
            }
            if (f.getUmsetzungsbestaetigungStatusFilter().isActive()) {
                umsetzungsbestaetigungStatus.put(f.getDerivatId(), f.getUmsetzungsbestaetigungStatusFilter().getAsEnumList());
                columns.add(f.getDerivatId());
                derivatStatusMap.put(f.getDerivatId(), f.getDerivatStatus());
            }
        }

        List<Set<AnforderungIdModulIdTuple>> anforderungIdModulIds = new ArrayList<>();

        for (Long derivatId : columns) {
            DerivatStatus derivatStatus = derivatStatusMap.get(derivatId);
            anforderungIdModulIds.add(getDerivatAnforderungModulIdForDerivatColumn(derivatId, derivatStatus, vereinbarungStatus.get(derivatId), zakStatus.get(derivatId), umsetzungsbestaetigungStatus.get(derivatId)));
        }

        Set<AnforderungIdModulIdTuple> result = new HashSet<>();
        anforderungIdModulIds.forEach(result::addAll);
        anforderungIdModulIds.forEach(result::retainAll);

        return result;
    }

    private List<Long> getAnforderungIdListForDerivatAnforderungFilter(
            List<DerivatVereinbarungStatusFilter> derivatVereinbarungStatusFilters) {

        Set<Long> columns = new HashSet<>(); // derivatIds
        Map<Long, List<DerivatAnforderungModulStatus>> vereinbarungStatus = new HashMap<>();
        Map<Long, List<ZakStatus>> zakStatus = new HashMap<>();
        Map<Long, List<UmsetzungsBestaetigungStatus>> umsetzungsbestaetigungStatus = new HashMap<>();
        Map<Long, DerivatStatus> derivatStatusMap = new HashMap<>();

        for (DerivatVereinbarungStatusFilter f : derivatVereinbarungStatusFilters) {
            if (f.getVereinbarungStatusFilter().isActive()) {
                vereinbarungStatus.put(f.getDerivatId(), f.getVereinbarungStatusFilter().getAsEnumList());
                columns.add(f.getDerivatId());
                derivatStatusMap.put(f.getDerivatId(), f.getDerivatStatus());
            }
            if (f.getZakStatusFilter().isActive()) {
                zakStatus.put(f.getDerivatId(), f.getZakStatusFilter().getAsEnumList());
                columns.add(f.getDerivatId());
                derivatStatusMap.put(f.getDerivatId(), f.getDerivatStatus());
            }
            if (f.getUmsetzungsbestaetigungStatusFilter().isActive()) {
                umsetzungsbestaetigungStatus.put(f.getDerivatId(), f.getUmsetzungsbestaetigungStatusFilter().getAsEnumList());
                columns.add(f.getDerivatId());
                derivatStatusMap.put(f.getDerivatId(), f.getDerivatStatus());
            }
        }

        List<List<Long>> columnResults = new ArrayList<>();
        for (Long derivatId : columns) {
            DerivatStatus derivatStatus = derivatStatusMap.get(derivatId);
            columnResults.add(filterDerivatAnforderungenForDerivatColumn(derivatId, derivatStatus,
                    vereinbarungStatus.get(derivatId), zakStatus.get(derivatId), umsetzungsbestaetigungStatus.get(derivatId)));
        }

        List<Long> result = new ArrayList<>();
        columnResults.forEach(result::addAll);
        columnResults.forEach(result::retainAll);
        return result;
    }

    private void getHistoryQueryPart(QueryPart qp, DateSearchFilter freigegebenSeitFilter) {
        if (freigegebenSeitFilter.isActive()) {
            List<Long> historyIdList = getHistoryIdList(freigegebenSeitFilter);
            VereinbarungSearchUtils.getHistoryQuery(qp, historyIdList);
        }
    }

    private List<Long> getHistoryIdList(DateSearchFilter freigegebenSeitFilter) {
        SearchFilterDTO statusFilter = new SearchFilterDTO(SearchFilterType.STATUS);
        List<Status> statusList = new ArrayList<>();
        statusList.add(Status.A_FREIGEGEBEN);
        statusFilter.setStatusListValue(statusList);

        SearchFilterDTO dateSearchFilter = new SearchFilterDTO(SearchFilterType.HISTORIE);
        dateSearchFilter.setStartDateValue(freigegebenSeitFilter.getSelected());

        return searchDao.getAnforderungHistroyIdList(dateSearchFilter, statusFilter);

    }

    private List<Long> filterDerivatAnforderungenForDerivatColumn(Long derivatId,
                                                                  DerivatStatus derivatStatus,
                                                                  List<DerivatAnforderungModulStatus> vereinbAnforderungModulStatus,
                                                                  List<ZakStatus> zakStatus,
                                                                  List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {
        QueryPart queryPart = new QueryPartDTO();

        queryPart.append("SELECT DISTINCT a.id FROM Anforderung a "
                + "WHERE a.status = :statusFreigegeben "
                + "AND ( a.id IN "
                + "(SELECT DISTINCT da.zuordnungAnforderungDerivat.anforderung.id FROM DerivatAnforderungModul da "
                + "WHERE da.zuordnungAnforderungDerivat.derivat.id = :derivatId");

        queryPart.put("statusFreigegeben", Status.A_FREIGEGEBEN);
        queryPart.put("derivatId", derivatId);

        queryPart.merge(getVereinbarungStatusFilter(vereinbAnforderungModulStatus, derivatStatus));

        if (zakStatus != null && zakStatus.size() < 10) {
            if (zakStatus.isEmpty()) {
                queryPart.append(AND_1_0);
            } else {
                String zakStatusParameter = "zakStatus";
                queryPart.append(" AND da.zakStatus IN :").append(zakStatusParameter);
                queryPart.put(zakStatusParameter, zakStatus);
            }
        }

        if (umsetzungsBestaetigungStatus != null && umsetzungsBestaetigungStatus.size() < 10) {
            if (umsetzungsBestaetigungStatus.isEmpty()) {
                queryPart.append(AND_1_0);
            } else {
                String umsetzungsBestaetigungStatusParameter = "umsetzungsBestaetigungStatus";
                queryPart.append(" AND da.umsetzungsBestaetigungStatus IN :").append(umsetzungsBestaetigungStatusParameter);
                queryPart.put(umsetzungsBestaetigungStatusParameter, umsetzungsBestaetigungStatus);
            }
        }

        queryPart.append("))");

        Query q = entityManager.createQuery(queryPart.toString(), Long.class);
        queryPart.getParameterMap().entrySet().forEach(p -> q.setParameter(p.getKey(), p.getValue()));
        return q.getResultList();
    }

    private static QueryPart getVereinbarungStatusFilter(List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus, DerivatStatus derivatStatus) {
        if (allObjectsAreNotNull(vereinbarungAnforderungModulStatus)) {
            return getQueryPartForVereinbarungFilter(vereinbarungAnforderungModulStatus, derivatStatus);
        } else {
            return getEmptyQueryPart();
        }
    }

    private static QueryPart getQueryPartForVereinbarungFilter(List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus, DerivatStatus derivatStatus) {
        if (vereinbarungAnforderungModulStatus.isEmpty()) {
            return getRetrunNothingQueryPart();
        } else {
            return getQueryPartForDerivatStatusAndVereinbarungFilter(vereinbarungAnforderungModulStatus, derivatStatus);
        }
    }

    private static QueryPart getQueryPartForDerivatStatusAndVereinbarungFilter(List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus, DerivatStatus derivatStatus) {
        QueryPart queryPart = new QueryPartDTO();
        String statusParameter = "status";
        List<Integer> vereinbarungStatusIds = vereinbarungAnforderungModulStatus.stream().map(DerivatAnforderungModulStatus::getStatusId).collect(Collectors.toList());
        switch (derivatStatus) {
            case OFFEN:
            case VEREINARBUNG_VKBG:
                queryPart.append(" AND da.statusVKBG IN :").append(statusParameter);
                queryPart.put(statusParameter, vereinbarungStatusIds);
                break;
            case VEREINBARUNG_ZV:
            case ZV:
                queryPart.append(" AND da.statusZV IN :").append(statusParameter);
                queryPart.put(statusParameter, vereinbarungStatusIds);
                break;
            case INAKTIV:
            default:
                return getRetrunNothingQueryPart();
        }
        return queryPart;
    }

    private static QueryPart getEmptyQueryPart() {
        return new QueryPartDTO();
    }

    private static QueryPart getRetrunNothingQueryPart() {
        return new QueryPartDTO(AND_1_0);
    }

    private Set<AnforderungIdModulIdTuple> getDerivatAnforderungModulIdForDerivatColumn(Long derivatId,
                                                                                        DerivatStatus derivatStatus,
                                                                                        List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus,
                                                                                        List<ZakStatus> zakStatus,
                                                                                        List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {

        Set<AnforderungIdModulIdTuple> resultList = new HashSet<>();
        Set<AnforderungIdModulIdTuple> resultvereinbarungList = new HashSet<>();
        Set<AnforderungIdModulIdTuple> resultZakList = new HashSet<>();
        Set<AnforderungIdModulIdTuple> resultUmsetzungbestaetigungList = new HashSet<>();

        if (isVereinbarungAnforderungModulStatusFilterActive(vereinbarungAnforderungModulStatus)) {
            resultvereinbarungList = getAnforderungIdModulIdTupleForVerainbarungAnforderungModulStatus(derivatId, derivatStatus, vereinbarungAnforderungModulStatus);
        }


        if (isZakStatusFilterActive(zakStatus)) {
            resultZakList = getAnforderungIdModulIdTupleForZakStatus(derivatId, zakStatus);
        }

        if (isUmsetzungsbestaetigungStatusActive(umsetzungsBestaetigungStatus)) {
            resultUmsetzungbestaetigungList = getAnforderungIdModulIDTupleForUmsetzungsbestaetigungStatus(derivatId, umsetzungsBestaetigungStatus);

        }

        getAllPossibleFilterPossibilities(vereinbarungAnforderungModulStatus, zakStatus, umsetzungsBestaetigungStatus, resultList, resultvereinbarungList, resultZakList, resultUmsetzungbestaetigungList);


        return resultList;
    }

    private void getAllPossibleFilterPossibilities(List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus, List<ZakStatus> zakStatus, List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus, Set<AnforderungIdModulIdTuple> resultList, Set<AnforderungIdModulIdTuple> resultvereinbarungList, Set<AnforderungIdModulIdTuple> resultZakList, Set<AnforderungIdModulIdTuple> resultUmsetzungbestaetigungList) {
        if (areAllFilterActive(vereinbarungAnforderungModulStatus, zakStatus, umsetzungsBestaetigungStatus)) {
            resultList.addAll(resultvereinbarungList);
            resultList.retainAll(resultZakList);
            resultList.retainAll(resultUmsetzungbestaetigungList);
        } else if (areVereinbarungAndZakActive(vereinbarungAnforderungModulStatus, zakStatus, umsetzungsBestaetigungStatus)) {
            resultList.addAll(resultvereinbarungList);
            resultList.retainAll(resultZakList);
        } else if (areVereinbarungAndUmsetzungsbestaetigungActive(vereinbarungAnforderungModulStatus, zakStatus, umsetzungsBestaetigungStatus)) {
            resultList.addAll(resultvereinbarungList);
            resultList.retainAll(resultUmsetzungbestaetigungList);
        } else if (areZakAndUmsetzungsbestaetiungActive(vereinbarungAnforderungModulStatus, zakStatus, umsetzungsBestaetigungStatus)) {
            resultList.addAll(resultZakList);
            resultList.retainAll(resultUmsetzungbestaetigungList);
        } else if (isVereinbarungActive(vereinbarungAnforderungModulStatus, zakStatus, umsetzungsBestaetigungStatus)) {
            resultList.addAll(resultvereinbarungList);
        } else if (isZakActive(vereinbarungAnforderungModulStatus, zakStatus, umsetzungsBestaetigungStatus)) {
            resultList.addAll(resultZakList);
        } else if (isUmsetzungsbestaetiungActive(vereinbarungAnforderungModulStatus, zakStatus, umsetzungsBestaetigungStatus)) {
            resultList.addAll(resultUmsetzungbestaetigungList);
        }
    }

    private boolean isUmsetzungsbestaetiungActive(List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus, List<ZakStatus> zakStatus, List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {
        return !isVereinbarungAnforderungModulStatusFilterActive(vereinbarungAnforderungModulStatus) && !isZakStatusFilterActive(zakStatus) && isUmsetzungsbestaetigungStatusActive(umsetzungsBestaetigungStatus);
    }

    private boolean isZakActive(List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus, List<ZakStatus> zakStatus, List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {
        return !isVereinbarungAnforderungModulStatusFilterActive(vereinbarungAnforderungModulStatus) && isZakStatusFilterActive(zakStatus) && !isUmsetzungsbestaetigungStatusActive(umsetzungsBestaetigungStatus);
    }

    private boolean isVereinbarungActive(List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus, List<ZakStatus> zakStatus, List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {
        return isVereinbarungAnforderungModulStatusFilterActive(vereinbarungAnforderungModulStatus) && !isZakStatusFilterActive(zakStatus) && !isUmsetzungsbestaetigungStatusActive(umsetzungsBestaetigungStatus);
    }

    private boolean areZakAndUmsetzungsbestaetiungActive(List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus, List<ZakStatus> zakStatus, List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {
        return !isVereinbarungAnforderungModulStatusFilterActive(vereinbarungAnforderungModulStatus) && isZakStatusFilterActive(zakStatus) && isUmsetzungsbestaetigungStatusActive(umsetzungsBestaetigungStatus);
    }

    private boolean areVereinbarungAndUmsetzungsbestaetigungActive(List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus, List<ZakStatus> zakStatus, List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {
        return isVereinbarungAnforderungModulStatusFilterActive(vereinbarungAnforderungModulStatus) && !isZakStatusFilterActive(zakStatus) && isUmsetzungsbestaetigungStatusActive(umsetzungsBestaetigungStatus);
    }

    private boolean areVereinbarungAndZakActive(List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus, List<ZakStatus> zakStatus, List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {
        return isVereinbarungAnforderungModulStatusFilterActive(vereinbarungAnforderungModulStatus) && isZakStatusFilterActive(zakStatus) && !isUmsetzungsbestaetigungStatusActive(umsetzungsBestaetigungStatus);
    }

    private boolean areAllFilterActive(List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus, List<ZakStatus> zakStatus, List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {
        return isVereinbarungAnforderungModulStatusFilterActive(vereinbarungAnforderungModulStatus) && isZakStatusFilterActive(zakStatus) && isUmsetzungsbestaetigungStatusActive(umsetzungsBestaetigungStatus);
    }

    private boolean isUmsetzungsbestaetigungStatusActive(List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {
        return umsetzungsBestaetigungStatus != null && umsetzungsBestaetigungStatus.size() < 10;
    }

    private boolean isZakStatusFilterActive(List<ZakStatus> zakStatus) {
        return zakStatus != null && zakStatus.size() < 10;
    }

    private boolean isVereinbarungAnforderungModulStatusFilterActive(List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus) {
        return vereinbarungAnforderungModulStatus != null && vereinbarungAnforderungModulStatus.size() < 10;
    }

    private Set<AnforderungIdModulIdTuple> getAnforderungIdModulIDTupleForUmsetzungsbestaetigungStatus(Long derivatId, List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus) {
        QueryPart queryPart = new QueryPartDTO();
        Set<AnforderungIdModulIdTuple> returnSet;

        queryPart.append("SELECT DISTINCT a.id, da.modul.id FROM DerivatAnforderungModul da "
                + "JOIN da.zuordnungAnforderungDerivat.anforderung a "
                + "JOIN da.zuordnungAnforderungDerivat.derivat d "
                + "JOIN da.modul m "
                + "JOIN Umsetzungsbestaetigung ub "
                + "JOIN ub.kovaImDerivat kovad "
                + "WHERE da.zuordnungAnforderungDerivat.derivat.id = :derivatId "
                + "AND ub.anforderung.id = a.id "
                + "AND ub.modul.id = m.id "
                + "AND kovad.derivat.id = d.id ");

        queryPart.put("derivatId", derivatId);
        if (umsetzungsBestaetigungStatus.isEmpty()) {
            queryPart.append(AND_1_0);
        } else {
            String umsetzungsBestaetigungStatusParameter = "umsetzungsBestaetigungStatus";
            queryPart.append(" AND ub.status IN :").append(umsetzungsBestaetigungStatusParameter);
            queryPart.put(umsetzungsBestaetigungStatusParameter, umsetzungsBestaetigungStatus);
        }
        returnSet = getAnforderungIdModulIdTuplesForQuery(queryPart);

        return returnSet;

    }

    private Set<AnforderungIdModulIdTuple> getAnforderungIdModulIdTupleForZakStatus(Long derivatId, List<ZakStatus> zakStatus) {
        QueryPart queryPart = new QueryPartDTO();
        queryPart.append("SELECT DISTINCT da.zuordnungAnforderungDerivat.anforderung.id, da.modul.id FROM DerivatAnforderungModul da "
                + "WHERE da.zuordnungAnforderungDerivat.derivat.id = :derivatId ");

        queryPart.put("derivatId", derivatId);
        if (zakStatus.isEmpty()) {
            queryPart.append(AND_1_0);
        } else {
            String zakStatusParameter = "zakStatus";
            queryPart.append(" AND da.zakStatus IN :").append(zakStatusParameter);
            queryPart.put(zakStatusParameter, zakStatus);
        }
        return getAnforderungIdModulIdTuplesForQuery(queryPart);
    }

    private Set<AnforderungIdModulIdTuple> getAnforderungIdModulIdTupleForVerainbarungAnforderungModulStatus(Long derivatId, DerivatStatus derivatStatus, List<DerivatAnforderungModulStatus> vereinbarungAnforderungModulStatus) {
        QueryPart queryPart = new QueryPartDTO();
        queryPart.append("SELECT DISTINCT da.zuordnungAnforderungDerivat.anforderung.id, da.modul.id FROM DerivatAnforderungModul da "
                + "WHERE da.zuordnungAnforderungDerivat.derivat.id = :derivatId ");

        queryPart.put("derivatId", derivatId);
        queryPart.merge(getVereinbarungStatusFilter(vereinbarungAnforderungModulStatus, derivatStatus));
        return getAnforderungIdModulIdTuplesForQuery(queryPart);
    }

    private Set<AnforderungIdModulIdTuple> getAnforderungIdModulIdTuplesForQuery(QueryPart queryPart) {
        Query q = entityManager.createQuery(queryPart.toString(), Long.class);
        queryPart.getParameterMap().forEach(q::setParameter);
        List<Object[]> queryResults = q.getResultList();

        Set<AnforderungIdModulIdTuple> result = new HashSet<>();

        for (Object[] entry : queryResults) {
            AnforderungIdModulIdTuple tuple = new AnforderungIdModulIdTuple(Long.parseLong(String.valueOf(entry[0])), Long.parseLong(String.valueOf(entry[1])));
            result.add(tuple);
        }
        return result;
    }

}
