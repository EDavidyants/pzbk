package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.filter.BooleanSearchFilter;
import de.interfaceag.bmw.pzbk.filter.BooleanUrlFilter;
import de.interfaceag.bmw.pzbk.filter.DerivatAnforderungModulIdFilter;
import de.interfaceag.bmw.pzbk.filter.FachIdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ThemenklammerFilter;
import de.interfaceag.bmw.pzbk.shared.dto.AnforderungIdModulIdTuple;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author fp
 */
final class VereinbarungSearchUtils {

    private VereinbarungSearchUtils() {
    }

    static QueryPart getBaseQuery(List<Long> derivatIds) {
        QueryPart queryPart = new QueryPartDTO("SELECT DISTINCT a FROM ZuordnungAnforderungDerivat zad "
                + "INNER JOIN FETCH zad.anforderung a "
                + "LEFT JOIN FETCH a.umsetzer u "
                + "LEFT JOIN FETCH a.prozessbaukastenThemenklammern t "
                + "LEFT JOIN FETCH a.prozessbaukasten "
                + "INNER JOIN u.seTeam.elternModul m "
                + "WHERE zad.derivat.id IN :derivatIds AND zad.status = :zuordnungStatus ");
        queryPart.put("derivatIds", derivatIds);
        queryPart.put("zuordnungStatus", ZuordnungStatus.ZUGEORDNET.getId());
        return queryPart;
    }

    static void getAnforderungQuery(QueryPart queryPart, FachIdSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND (");
            int i = 0;
            for (SearchFilterObject selectedValue : filter.getSelectedValues()) {
                queryPart.append(" LOWER(a.fachId) LIKE LOWER(:fachId" + i + ") OR");
                queryPart.put("fachId" + i, selectedValue.getName());
                i++;

            }
            queryPart.append(" 1=0)");
        }
    }

    static void getBeschreibungQuery(QueryPart queryPart, TextSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND LOWER(a.beschreibungAnforderungDe) LIKE LOWER(:beschreibung)");
            queryPart.put("beschreibung", "%" + filter.getAsString() + "%");
        }
    }

    static void getSensorCocQuery(QueryPart queryPart, IdSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND a.sensorCoc.sensorCocId IN :sensorCocs");
            queryPart.put("sensorCocs", filter.getSelectedIdsAsSet());
        }
    }

    static void getThemenklammerQuery(QueryPart queryPart, ThemenklammerFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND t.themenklammer.id IN :themenklammerIds");
            queryPart.put("themenklammerIds", filter.getSelectedIdsAsSet());
        }
    }

    static void getProzessbaukastenQuery(QueryPart queryPart, IdSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND a.prozessbaukasten IN :prozessbaukaesten");
            queryPart.put("prozessbaukaesten", filter.getSelectedIdsAsSet());
        }
    }

    static void getInProzessbaukastenQuery(QueryPart queryPart, BooleanUrlFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND a.prozessbaukasten IS NOT EMPTY");
        }
    }

    static void getNotInProzessbaukastenQuery(QueryPart queryPart, BooleanUrlFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND a.prozessbaukasten IS EMPTY");
        }
    }

    static void getDerivatAnforderungModulModulQuery(QueryPart queryPart, IdSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND dam.modul.id IN :damModulIds");
            queryPart.put("damModulIds", filter.getSelectedIdsAsSet());
        }
    }

    static void getZuordnungAnforderungDerivatModulQuery(QueryPart queryPart, IdSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND m.id IN :zadModulIds");
            queryPart.put("zadModulIds", filter.getSelectedIdsAsSet());
        }
    }

    static void getFestgestlltInQuery(QueryPart queryPart, IdSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND a.festgestelltIn IN :festgestelltInList");
            queryPart.put("festgestelltInList", filter.getSelectedIdsAsSet());
        }
    }

    static void getPhasenrelevanzQuery(QueryPart queryPart, BooleanSearchFilter filter) {
        if (filter.isActive()) {
            queryPart.append(" AND a.phasenbezug IN :phasenbezug");
            queryPart.put("phasenbezug", filter.getAsBoolean());
        }
    }

    static void getHistoryQuery(QueryPart queryPart, List<Long> historyIdList) {
        if (historyIdList == null || historyIdList.isEmpty()) {
            queryPart.append(" AND 1=0");
        } else {
            queryPart.append(" AND a.id IN :historyIdList");
            queryPart.put("historyIdList", historyIdList);
        }
    }

    static void getVereinbarungIdQuery(QueryPart queryPart, Set<AnforderungIdModulIdTuple> anforderungIdModulIdTuples) {
        if (anforderungIdModulIdTuples == null || anforderungIdModulIdTuples.isEmpty()) {
            queryPart.append(" AND 1=0");
        } else {

            // builds OR statement for list of anforderungId, modulId Tuple
            queryPart.append(anforderungIdModulIdTuples.stream()
                    .map(tuple -> "(dam.zuordnungAnforderungDerivat.anforderung.id = :anforderungId" + tuple.getAnforderungId().toString() + " AND dam.modul.id = :modulId" + tuple.getModulId().toString() + ")")
                    .collect(Collectors.joining(" OR ", " AND ( ", " ) ")));

            // puts paramter to query
            anforderungIdModulIdTuples.stream().map(AnforderungIdModulIdTuple::getAnforderungId).distinct().forEach(id -> queryPart.put("anforderungId" + id.toString(), id));
            anforderungIdModulIdTuples.stream().map(AnforderungIdModulIdTuple::getModulId).distinct().forEach(id -> queryPart.put("modulId" + id.toString(), id));

        }
    }

    static void getAnforderungIdQuery(QueryPart queryPart, List<Long> idList) {
        if (idList == null || idList.isEmpty()) {
            queryPart.append(" AND 1=0");
        } else {
            queryPart.append(" AND a.id IN :idList");
            queryPart.put("idList", idList);
        }
    }

    static void getDerivatAnforderungModulByIdQuery(QueryPart queryPart, DerivatAnforderungModulIdFilter filter) {
        Set<Long> derivatAnforderungIds = filter.getIds();
        if (!derivatAnforderungIds.isEmpty()) {
            queryPart.append(" AND dam.id IN :derivatAnforderungIds ");
            queryPart.put("derivatAnforderungIds", derivatAnforderungIds);
        }
    }

}
