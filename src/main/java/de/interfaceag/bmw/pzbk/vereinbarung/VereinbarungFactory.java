package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 *
 * @author sl
 */
public final class VereinbarungFactory {

    private static final Logger LOG = LoggerFactory.getLogger(VereinbarungFactory.class);

    private VereinbarungFactory() {
    }

    public static DerivatAnforderungModul createNewVereinbarung(ZuordnungAnforderungDerivat anforderungDerivat, Modul modul, Mitarbeiter currentUser, DerivatAnforderungModulStatus status) {
        DerivatStatus zuordnungDerivatStatus = anforderungDerivat.getZuordnungDerivatStatus();
        if (zuordnungIsActive(zuordnungDerivatStatus)) {
            DerivatAnforderungModul newVereinbarung = createNewVereinbarungWithStatus(anforderungDerivat, modul, currentUser, status);
            setVereinbarungStatusToAngenommenForProzessbaukastenAnforderung(newVereinbarung, anforderungDerivat);
            setStdLabelIfAllFreigabenAreStandard(newVereinbarung);
            return newVereinbarung;
        } else {
            LOG.error("Tried to create Vereinbarung Status for invalid derivatStatus {}", zuordnungDerivatStatus);
            return null;
        }
    }

    private static boolean zuordnungIsActive(DerivatStatus derivatStatus) {
        return derivatStatus.equals(DerivatStatus.VEREINARBUNG_VKBG) || derivatStatus.equals(DerivatStatus.VEREINBARUNG_ZV);
    }

    private static DerivatAnforderungModul createNewVereinbarungWithStatus(ZuordnungAnforderungDerivat anforderungDerivat, Modul modul, Mitarbeiter currentUser, DerivatAnforderungModulStatus status) {
        DerivatAnforderungModul newVereinbarung = new DerivatAnforderungModul(anforderungDerivat, modul, currentUser);
        DerivatStatus zuordnungDerivatStatus = anforderungDerivat.getZuordnungDerivatStatus();
        setDerivatAnforderungModulStatusDependingOnZuordnungAnforderungDerivatStatus(zuordnungDerivatStatus, status, newVereinbarung);
        return newVereinbarung;
    }

    private static void setDerivatAnforderungModulStatusDependingOnZuordnungAnforderungDerivatStatus(DerivatStatus derivatStatus, DerivatAnforderungModulStatus vereinbarungStatus, DerivatAnforderungModul vereinbarung) {
        switch (derivatStatus) {
            case VEREINARBUNG_VKBG:
                vereinbarung.setStatusVKBG(vereinbarungStatus);
                vereinbarung.setStatusZV(vereinbarungStatus);
                break;
            case VEREINBARUNG_ZV:
                vereinbarung.setStatusZV(vereinbarungStatus);
                break;
            default:
                LOG.error("Tried to set Vereinbarung Status for invalid derivatStatus");
        }
    }

    private static void setVereinbarungStatusToAngenommenForProzessbaukastenAnforderung(DerivatAnforderungModul newVereinbarung, ZuordnungAnforderungDerivat anforderungDerivat) {
        Anforderung anforderung = anforderungDerivat.getAnforderung();
        if (anforderung.isProzessbaukastenZugeordnet()) {
            DerivatStatus zuordnungDerivatStatus = anforderungDerivat.getZuordnungDerivatStatus();
            setDerivatAnforderungModulStatusDependingOnZuordnungAnforderungDerivatStatus(zuordnungDerivatStatus, DerivatAnforderungModulStatus.ANGENOMMEN, newVereinbarung);
        }
    }

    private static void setStdLabelIfAllFreigabenAreStandard(DerivatAnforderungModul newVereinbarung) {
        List<AnforderungFreigabeBeiModulSeTeam> freigaben = newVereinbarung.getAnforderung().getAnfoFreigabeBeiModul();
        boolean allStandard = freigaben.stream().allMatch(freigabe -> !freigabe.isZak());

        if (allStandard) {
            newVereinbarung.setZakStatus(ZakStatus.KEINE_ZAKUEBERTRAGUNG);
        }

    }

}
