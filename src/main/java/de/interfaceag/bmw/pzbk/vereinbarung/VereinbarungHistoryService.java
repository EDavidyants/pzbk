package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.dao.DerivatAnforderungModulHistoryDao;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModulHistory;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.DerivatVereinbarungDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@Stateless
public class VereinbarungHistoryService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(VereinbarungHistoryService.class.getName());

    @Inject
    private DerivatAnforderungModulHistoryDao derivatAnforderungModulHistoryDao;

    @Inject
    private DerivatAnforderungModulService derivatAnforderungModulService;
    @Inject
    private AnforderungMeldungHistoryService anforderungHistoryService;

    public List<DerivatAnforderungModulHistory> getHistoryForDerivatAnforderungModul(DerivatAnforderungModul da) {
        return derivatAnforderungModulHistoryDao.getDerivatAnforderungModulHistoryByDerivatAnforderungModul(da);
    }

    public List<DerivatAnforderungModulHistory> getHistoryForDerivatAnforderungModulId(Long derivatAnforderungModulId) {
        return derivatAnforderungModulHistoryDao.getDerivatAnforderungModulHistoryByDerivatAnforderungModulId(derivatAnforderungModulId);
    }

    public Optional<DerivatAnforderungModulHistory> getLatestHistoryForDerivatAnforderungModul(DerivatAnforderungModul da) {
        return derivatAnforderungModulHistoryDao.getLatestDerivatAnforderungModulHistoryByDerivatAnforderungModul(da);
    }

    private boolean isIdenticalWithLastEntry(DerivatAnforderungModulHistory newEntry) {
        Optional<DerivatAnforderungModulHistory> optionalLatestEntry = getLatestHistoryForDerivatAnforderungModul(newEntry.getDerivatAnforderungModul());
        if (optionalLatestEntry.isPresent()) {
            DerivatAnforderungModulHistory latestEntry = optionalLatestEntry.get();
            return latestEntry.getBearbeiter().equals(newEntry.getBearbeiter())
                    && latestEntry.getKommentar().equals(newEntry.getKommentar())
                    && latestEntry.getStatus().equals(newEntry.getStatus());
        }
        return Boolean.FALSE;
    }

    public void addVereinbarungHistoryEntry(DerivatAnforderungModulHistory derivatAnforderungModulHistory) {
        if (isIdenticalWithLastEntry(derivatAnforderungModulHistory)) {
            LOG.debug("New History Entry is identical with last entry and will not be persisted");
            return;
        }
        persistDerivatAnforderungModulHistory(derivatAnforderungModulHistory);
        updateLatestDerivatAnforderungModulHistoryEntry(derivatAnforderungModulHistory);
        addHistoryEntryToAnforderungIfStatusIsKeineWeiterverfolgungOrUnzureichendeAnforderungsqualitaet(derivatAnforderungModulHistory);

    }

    public void addVereinbarungHistoryEntry(DerivatVereinbarungDto derivatVereinbarungDto, String kommentar, Mitarbeiter user) {
        Long derivatAnforderungModulId = derivatVereinbarungDto.getDerivatAnforderungModulId();
        if (derivatAnforderungModulId == null) {
            LOG.error("derivatVereinbarungModulId is null!");
            return;
        }
        DerivatAnforderungModul derivatAnforderungModul = derivatAnforderungModulService.getDerivatAnforderungModulById(derivatAnforderungModulId);
        if (derivatAnforderungModul == null) {
            LOG.error("derivatAnforderungModul is null!");
            return;
        }
        String status = derivatAnforderungModul.getStatusZV().toString();
        if (user == null) {
            LOG.error("user is null");
            return;
        }
        String userAsString = user.toString();
        DerivatAnforderungModulHistory derivatAnforderungModulHistory = new DerivatAnforderungModulHistory(derivatAnforderungModul, status, new Date(), userAsString, kommentar);
        addVereinbarungHistoryEntry(derivatAnforderungModulHistory);
    }

    private void addHistoryEntryToAnforderungIfStatusIsKeineWeiterverfolgungOrUnzureichendeAnforderungsqualitaet(DerivatAnforderungModulHistory derivatAnforderungModulHistory) {
        if (derivatAnforderungModulHistory.getDerivatAnforderungModul().getStatusZV().equals(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG)
                || derivatAnforderungModulHistory.getDerivatAnforderungModul().getStatusZV().equals(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET)) {
            DerivatAnforderungModul derivatAnforderungModul = derivatAnforderungModulHistory.getDerivatAnforderungModul();
            String user = derivatAnforderungModulHistory.getBearbeiter();

            AnforderungHistory anforderungHistory = new AnforderungHistory(derivatAnforderungModul.getZuordnungAnforderungDerivat().getAnforderung().getId(),
                    "A", new Date(), "Zuordnung", user, "",
                    "Kommentar zu " + derivatAnforderungModul.getModul().getName() + " im Derivat "
                    + derivatAnforderungModul.getDerivat().getName() + " im Status "
                    + derivatAnforderungModul.getStatusZV().toString() + ": "
                    + derivatAnforderungModulHistory.getKommentar());

            anforderungHistoryService.persistAnforderungHistory(anforderungHistory);

        }
    }

    private void persistDerivatAnforderungModulHistory(DerivatAnforderungModulHistory derivatAnforderungModulHistory) {
        derivatAnforderungModulHistoryDao.persist(derivatAnforderungModulHistory);
    }

    public void removeDerivatAnforderungModulHistory(DerivatAnforderungModulHistory dah) {
        derivatAnforderungModulHistoryDao.remove(dah);
    }

    private void updateLatestDerivatAnforderungModulHistoryEntry(DerivatAnforderungModulHistory derivatAnforderungModulHistory) {
        DerivatAnforderungModul derivatAnforderungModul = derivatAnforderungModulHistory.getDerivatAnforderungModul();
        derivatAnforderungModul.setLatestHistoryEntry(derivatAnforderungModulHistory);
        derivatAnforderungModulService.persistDerivatAnforderungModul(derivatAnforderungModul);
    }

}
