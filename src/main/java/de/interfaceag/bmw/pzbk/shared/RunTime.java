package de.interfaceag.bmw.pzbk.shared;

import java.io.Serializable;

public final class RunTime implements Serializable {

    private final Long timeStart;
    private Long timeEnd;

    private RunTime(long timeStart) {
        this.timeStart = timeStart;
    }

    public static RunTime start() {
        final long timeStart = System.currentTimeMillis();
        return new RunTime(timeStart);
    }

    public RunTime stop() {
        this.timeEnd = System.currentTimeMillis();
        return this;
    }

    public long getTime() {
        return getDiff();
    }

    private long getDiff() {
        if (timeEnd == null) {
            return 0L;
        } else {
            return timeEnd - timeStart;
        }
    }

}
