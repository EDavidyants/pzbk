package de.interfaceag.bmw.pzbk.shared.command;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungFreigabeDto;

import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface AnforderungFreigabeDtoCommand extends Command<List<AnforderungFreigabeDto>> {

}
