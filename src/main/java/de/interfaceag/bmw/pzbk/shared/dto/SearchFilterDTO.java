package de.interfaceag.bmw.pzbk.shared.dto;

import de.interfaceag.bmw.pzbk.dashboard.DashboardResultDTO;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sl
 */
@Deprecated
public class SearchFilterDTO implements Serializable {

    private SearchFilterType filterType;
    private Boolean booleanValue;
    private List<Long> longListValue;
    private List<Boolean> booleanListValue;
    private List<Status> statusListValue;
    private Date startDateValue;
    private Date endDateValue;
    private Map<Derivat, Boolean> derivatBooleanMapValue;
    private Map<Derivat, List<DerivatAnforderungModulStatus>> derivatStatusMapValue;
    private Map<Derivat, List<UmsetzungsBestaetigungStatus>> umsetzungsbestaetigungStatusMapValue;
    private Map<Derivat, List<ZakStatus>> zakStatusMapValue;
    private Map<Derivat, ZakStatus> derivatZakStatusMapValue;
    private Map<Derivat, List<Boolean>> zakBooleanMapValue;
    private DashboardResultDTO dashboardResult;

    public SearchFilterDTO(SearchFilterType filterType) {
        this.filterType = filterType;
    }

    public SearchFilterDTO(SearchFilterType filterType, Boolean booleanValue) {
        this.filterType = filterType;
        this.booleanValue = booleanValue;
    }

    public SearchFilterDTO(SearchFilterType filterType, List<Long> longListValue) {
        this.filterType = filterType;
        this.longListValue = longListValue;
    }

    public SearchFilterDTO(SearchFilterType filterType, DashboardResultDTO dashboardResult) {
        this.filterType = filterType;
        this.dashboardResult = dashboardResult;
    }

    /**
     * Set value intValue 0 for startDate, 1 for endDate.
     *
     * @param filterType of the filter;
     * @param dateValue instance of Date;
     * @param intValue selector for start and end date;
     */
    public SearchFilterDTO(SearchFilterType filterType, Date dateValue, Integer intValue) {
        this.filterType = filterType;
        if (intValue == 0) {
            this.startDateValue = new Date(dateValue.getTime());
        } else if (intValue == 1) {
            this.endDateValue = new Date(dateValue.getTime());
        }
    }

    public SearchFilterDTO(SearchFilterType filterType, Date startDateValue, Date endDateValue) {
        this.filterType = filterType;
        this.startDateValue = new Date(startDateValue.getTime());
        this.endDateValue = new Date(endDateValue.getTime());
    }

    public Boolean getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(Boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public List<Long> getLongListValue() {
        return longListValue;
    }

    public void setLongListValue(List<Long> longListValue) {
        this.longListValue = longListValue;
    }

    public Date getStartDateValue() {
        return startDateValue == null ? null : new Date(startDateValue.getTime());
    }

    public void setStartDateValue(Date startDateValue) {
        if (startDateValue == null) {
            this.startDateValue = null;
        } else {
            this.startDateValue = new Date(startDateValue.getTime());
        }
    }

    public Date getEndDateValue() {
        return endDateValue == null ? null : new Date(endDateValue.getTime());
    }

    public void setEndDateValue(Date endDateValue) {
        if (endDateValue == null) {
            this.endDateValue = null;
        } else {
            this.endDateValue = new Date(endDateValue.getTime());
        }
    }

    public List<Status> getStatusListValue() {
        return statusListValue;
    }

    public void setStatusListValue(List<Status> statusListValue) {
        this.statusListValue = statusListValue;
    }

    public Map<Derivat, Boolean> getDerivatBooleanMapValue() {
        return derivatBooleanMapValue;
    }

    public void setDerivatBooleanMapValue(Map<Derivat, Boolean> derivatBooleanMapValue) {
        this.derivatBooleanMapValue = derivatBooleanMapValue;
    }

    public Map<Derivat, ZakStatus> getDerivatZakStatusMapValue() {
        return derivatZakStatusMapValue;
    }

    public void setDerivatZakStatusMapValue(Map<Derivat, ZakStatus> derivatZakStatusMapValue) {
        this.derivatZakStatusMapValue = derivatZakStatusMapValue;
    }

    public SearchFilterType getFilterType() {
        return filterType;
    }

    public void setFilterType(SearchFilterType filterType) {
        this.filterType = filterType;
    }

    public Map<Derivat, List<DerivatAnforderungModulStatus>> getDerivatStatusMapValue() {
        return derivatStatusMapValue;
    }

    public void setDerivatStatusMapValue(Map<Derivat, List<DerivatAnforderungModulStatus>> derivatStatusMapValue) {
        this.derivatStatusMapValue = derivatStatusMapValue;
    }

    public Map<Derivat, List<UmsetzungsBestaetigungStatus>> getUmsetzungsbestaetigungStatusMapValue() {
        return umsetzungsbestaetigungStatusMapValue;
    }

    public void setUmsetzungsbestaetigungStatusMapValue(Map<Derivat, List<UmsetzungsBestaetigungStatus>> umsetzungsbestaetigungStatusMapValue) {
        this.umsetzungsbestaetigungStatusMapValue = umsetzungsbestaetigungStatusMapValue;
    }

    public Map<Derivat, List<ZakStatus>> getZakStatusMapValue() {
        return zakStatusMapValue;
    }

    public void setZakStatusMapValue(Map<Derivat, List<ZakStatus>> zakStatusMapValue) {
        this.zakStatusMapValue = zakStatusMapValue;
    }

    public Map<Derivat, List<Boolean>> getZakBooleanMapValue() {
        return zakBooleanMapValue;
    }

    public void setZakBooleanMapValue(Map<Derivat, List<Boolean>> zakBooleanMapValue) {
        this.zakBooleanMapValue = zakBooleanMapValue;
    }

    public DashboardResultDTO getDashboardResult() {
        return dashboardResult;
    }

    public void setDashboardResult(DashboardResultDTO dashboardResult) {
        this.dashboardResult = dashboardResult;
    }

    public List<Boolean> getBooleanListValue() {
        return booleanListValue;
    }

    public void setBooleanListValue(List<Boolean> booleanListValue) {
        this.booleanListValue = booleanListValue;
    }

}
