package de.interfaceag.bmw.pzbk.shared.image;

import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.services.ImageService;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.ByteArrayInputStream;
import java.util.Optional;

/**
 *
 * @author sl
 */
@Stateless
public class ProzessbaukastenImageService {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    EntityManager em;

    @Inject
    private ImageService imageService;

    public StreamedContent getKonzeptImage(String konzeptId) {
        if (konzeptId != null && RegexUtils.matchesId(konzeptId)) {
            Long id = Long.parseLong(konzeptId);
            Konzept konzept = em.find(Konzept.class, id);
            if (konzept.getBild() != null) {
                return getStreamedContentForImage(konzept.getBild().getLargeImage());
            } else {
                return getDefaultImageAsStreamedContent();
            }
        } else {
            return getDefaultImageAsStreamedContent();
        }
    }

    public StreamedContent getProzessbaukastenImage(ProzessbaukastenImageParameters imageParameters) {
        Optional<ProzessbaukastenImage> prozessbaukastenImage = imageParameters.getProzessbaukastenImage();
        if (prozessbaukastenImage.isPresent()) {
            switch (prozessbaukastenImage.get()) {
                case KONZEPTBAUM:
                    return getProzessbaukastenKonzeptbaumImage(imageParameters);
                case GRAFIK_UMFANG:
                    return getProzessbaukastenGrafikUmfangImage(imageParameters);
                default:
                    return getDefaultContent();
            }
        } else {
            return getDefaultContent();
        }
    }

    private StreamedContent getProzessbaukastenKonzeptbaumImage(ProzessbaukastenImageParameters imageParameters) {
        String fachId = imageParameters.getFachId();
        int version = imageParameters.getVersion();
        boolean isLargeImage = imageParameters.isLargeImage();
        byte[] image = getImageForProzessbaukastenKonzeptbaum(fachId, version, isLargeImage);
        return getStreamedContentForImage(image);
    }

    private StreamedContent getProzessbaukastenGrafikUmfangImage(ProzessbaukastenImageParameters imageParameters) {
        String fachId = imageParameters.getFachId();
        int version = imageParameters.getVersion();
        boolean isLargeImage = imageParameters.isLargeImage();
        byte[] image = getImageForProzessbaukastenGrafikUmfang(fachId, version, isLargeImage);
        return getStreamedContentForImage(image);
    }

    private byte[] getImageForProzessbaukastenKonzeptbaum(String fachId, int version, boolean isLargeImage) {
        byte[] image = getImageForProzessbaukastenKonzeptbaumByFachIdAndVersion(fachId, version, isLargeImage);
        if (image == null) {
            return getDefaultImage();
        } else {
            return image;
        }
    }

    private byte[] getImageForProzessbaukastenGrafikUmfang(String fachId, int version, boolean isLargeImage) {
        byte[] image = getImageForProzessbaukastenGrafikUmfangByFachIdAndVersion(fachId, version, isLargeImage);
        if (image == null) {
            return getDefaultImage();
        } else {
            return image;
        }
    }

    private byte[] getImageForProzessbaukastenKonzeptbaumByFachIdAndVersion(String fachId, int version, boolean isLargeImage) {
        if (isProzessbaukastenBildInputValid(fachId, version)) {
            return imageService.getProzessbaukastenKonzeptbaumBild(fachId, version, isLargeImage);
        }
        return new byte[0];
    }

    private byte[] getImageForProzessbaukastenGrafikUmfangByFachIdAndVersion(String fachId, int version, boolean isLargeImage) {
        if (isProzessbaukastenBildInputValid(fachId, version)) {
            return imageService.getProzessbaukastenGrafikUmfangBild(fachId, version, isLargeImage);
        }
        return new byte[0];
    }

    private boolean isProzessbaukastenBildInputValid(String fachId, int version) {
        return fachId != null && version > 0;
    }

    private StreamedContent getStreamedContentForImage(byte[] image) {
        return new DefaultStreamedContent(new ByteArrayInputStream(image));
    }

    private byte[] getDefaultImage() {
        return imageService.loadDefaultImage();
    }

    private StreamedContent getDefaultContent() {
        return new DefaultStreamedContent();
    }

    private StreamedContent getDefaultImageAsStreamedContent() {
        byte[] defaultImage = getDefaultImage();
        return getStreamedContentForImage(defaultImage);
    }

}
