package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;

/**
 *
 * @author evda
 */
public final class ObjectTypeValidationUtils {

    private ObjectTypeValidationUtils() {
    }

    public static boolean isAnforderung(Object obj) {
        return isClassInstanceOfTheObject(obj, Anforderung.class);
    }

    public static boolean isMeldung(Object obj) {
        return isClassInstanceOfTheObject(obj, Meldung.class);
    }

    public static boolean isProzessbaukasten(Object obj) {
        return isClassInstanceOfTheObject(obj, Prozessbaukasten.class);
    }

    public static boolean isAnforderungOhneMeldung(Object obj) {
        boolean isAnforderung = isAnforderung(obj);

        if (!isAnforderung) {
            return false;
        }

        Anforderung anforderung = (Anforderung) obj;
        return anforderung.getMeldungen() == null || anforderung.getMeldungen().isEmpty();
    }

    private static boolean isClassInstanceOfTheObject(Object obj, Class someClass) {
        return someClass.isInstance(obj);
    }

}
