package de.interfaceag.bmw.pzbk.shared.dto;

/**
 *
 * @author lomu
 */
public class AnforderungIdModulIdTuple extends GenericTuple<Long, Long> {

    public AnforderungIdModulIdTuple(Long xValue, Long yValue) {
        super(xValue, yValue);
    }

    public Long getAnforderungId() {
        return getX();
    }

    public Long getModulId() {
        return getY();
    }

}
