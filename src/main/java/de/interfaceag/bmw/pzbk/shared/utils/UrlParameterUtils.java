package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class UrlParameterUtils {

    private UrlParameterUtils() {
    }

    public static UrlParameter getUrlParameter() {
        final FacesContext currentInstance = FacesContext.getCurrentInstance();
        final ExternalContext externalContext = currentInstance.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        return new UrlParameter(request);
    }

}
