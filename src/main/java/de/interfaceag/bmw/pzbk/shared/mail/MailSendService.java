package de.interfaceag.bmw.pzbk.shared.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import java.io.Serializable;

/**
 * @author evda
 */
@Stateless
public class MailSendService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(MailSendService.class);

    @Asynchronous
    public void sendMail(Message message) {
        try {
            Transport.send(message);
        } catch (MessagingException ex) {
            LOG.error("MailSendService MessagingException", ex);
        }
    }
}
