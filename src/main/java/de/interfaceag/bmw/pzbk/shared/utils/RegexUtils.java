package de.interfaceag.bmw.pzbk.shared.utils;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class RegexUtils {

    private RegexUtils() {
    }

    public static boolean matchesFachId(String fachId) {
        return fachId.matches("[AMP]{1}\\d+");
    }

    public static boolean matchesAnforderungFachId(String fachId) {
        return fachId.matches("[A]{1}\\d+");
    }

    public static boolean matchesMeldungFachId(String fachId) {
        return fachId.matches("[M]{1}\\d+");
    }

    public static boolean matchesZakUebertragungId(String fachId) {
        return matchesId(fachId);
    }

    public static boolean matchesId(String fachId) {
        return fachId.matches("\\d+");
    }

    public static boolean matchesVersion(String fachId) {
        return matchesId(fachId);
    }

    public static boolean matchesQnumber(String qnumber) {
        return qnumber.matches("[qQ]{1}[0-9a-zA-Z]{6}");
    }

}
