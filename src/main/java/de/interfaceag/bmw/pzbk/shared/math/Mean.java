package de.interfaceag.bmw.pzbk.shared.math;

import java.util.stream.Stream;

public final class Mean {

    private Mean() {
    }

    public static int computeMean(Stream<Integer> input) {
        final double mean = input.mapToDouble(Integer::doubleValue)
                .average()
                .orElse(0);
        final long roundMean = Math.round(mean);
        return (int) roundMean;
    }

}
