package de.interfaceag.bmw.pzbk.shared;

import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class QueryFactory<T> {

    public QueryFactory() {
    }

    public TypedQuery<T> buildTypedQuery(QueryPart queryPart, EntityManager entityManager, Class type) {
        TypedQuery<T> result = entityManager.createQuery(queryPart.getQuery(), type);
        if (!queryPart.getParameterMap().isEmpty()) {
            queryPart.getParameterMap().forEach(result::setParameter);
        }
        return result;
    }

}
