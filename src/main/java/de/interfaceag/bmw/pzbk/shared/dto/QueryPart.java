package de.interfaceag.bmw.pzbk.shared.dto;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Collection;
import java.util.Map;

/**
 *
 * @author sl
 */
public interface QueryPart {

    QueryPart append(String query);

    Map<String, Object> getParameterMap();

    String getQuery();

    String getSort();

    QueryPart merge(QueryPart queryPart);

    QueryPart put(String key, Object value);

    Query buildGenericQuery(EntityManager entityManager);

    Query buildGenericQuery(EntityManager entityManager, Class type);

    Query buildNativeQuery(EntityManager entityManager);

    Query buildNativeQuery(EntityManager entityManager, Class type);

    Integer putNativeParameter(Object value);

    QueryPart appendNativeParameters(Collection indices);

}
