package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.enums.Phasenbezug;

import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class PhasenbezugUtils {

    private PhasenbezugUtils() {
    }

    public static Stream<Phasenbezug> getAllPhasenbezug() {
        return Stream.of(Phasenbezug.values());
    }

    public static Phasenbezug getPhasenbezugForBoolean(boolean phasenbezug) {
        return getAllPhasenbezug().filter(p -> p.getValue() == phasenbezug).findAny().orElse(null);
    }

    public static Phasenbezug getPhasenbezugForString(String phasenbezug) {
        if (phasenbezug != null) {
            switch (phasenbezug) {
                case "Konzeptrelevant":
                    return getPhasenbezugForBoolean(true);
                case "Architekturrelevant":
                    return getPhasenbezugForBoolean(false);
                default:
                    break;
            }
        }
        return null;
    }

}
