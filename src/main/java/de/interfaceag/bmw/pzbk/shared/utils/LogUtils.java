package de.interfaceag.bmw.pzbk.shared.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class LogUtils {

    private LogUtils() {
    }

    public static void logDiff(Date startDate, Date endDate) {
        StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
        StackTraceElement e = stacktrace[2];
        String methodName = e.getMethodName();
        String className = e.getClassName();
        logDiff(startDate, endDate, methodName, className);
    }

    public static void logDiff(Date startDate, Date endDate, String methodPartName) {
        StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
        StackTraceElement e = stacktrace[2];
        String className = e.getClassName();
        String methodName = e.getMethodName();
        if (methodPartName != null) {
            methodName += " - " + methodPartName;
        }

        logDiff(startDate, endDate, methodName, className);
    }

    private static void logDiff(Date startDate, Date endDate,
            String methodName, String className) {
        Logger logger = LoggerFactory.getLogger(className);
        long diff = endDate.getTime() - startDate.getTime();
        logger.info(methodName + " runtime: {} milliseconds", Long.toString(diff));
    }

}
