package de.interfaceag.bmw.pzbk.shared.dto;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class UrlParameter implements Serializable {

    private final Map<String, String> parameters = new HashMap<>();

    public UrlParameter() {
    }

    public UrlParameter(HttpServletRequest request) {
        parseUrlParamter(request);
    }

    public Map<String, String> getParamters() {
        return parameters;
    }

    public void addOrReplaceParamter(String key, String value) {
        if (parameters.containsKey(key)) {
            parameters.replace(key, value);
        } else {
            parameters.put(key, value);
        }
    }

    public Optional<String> getValue(String key) {
        if (parameters.containsKey(key)) {
            return Optional.ofNullable(parameters.get(key));
        }
        return Optional.empty();
    }

    @Override
    public String toString() {
        if (parameters != null) {
            return parameters.entrySet()
                    .stream().map(e -> e.getKey() + ": " + e.getValue())
                    .collect(Collectors.joining(", "));
        }
        return "";
    }

    private void parseUrlParamter(HttpServletRequest request) {
        Map params = request.getParameterMap();

        Iterator i = params.keySet().iterator();

        while (i.hasNext()) {
            String key = (String) i.next();
            String value = ((String[]) params.get(key))[0];
            this.addOrReplaceParamter(key, value);
        }

    }

}
