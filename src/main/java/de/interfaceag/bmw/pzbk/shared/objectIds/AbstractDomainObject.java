package de.interfaceag.bmw.pzbk.shared.objectIds;

import java.util.Objects;
import java.util.StringJoiner;

public abstract class AbstractDomainObject implements DomainObject {

    private final Long id;

    AbstractDomainObject(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                .add("id=" + id)
                .toString();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        AbstractDomainObject that = (AbstractDomainObject) object;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
