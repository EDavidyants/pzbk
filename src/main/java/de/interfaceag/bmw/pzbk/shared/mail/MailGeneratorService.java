package de.interfaceag.bmw.pzbk.shared.mail;

import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.mail.MailArgs;
import de.interfaceag.bmw.pzbk.mail.MailProcessor;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;
import javax.activation.DataHandler;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author evda
 */
@Stateless
public class MailGeneratorService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(MailGeneratorService.class);
    private static final String SENDER = "t-am@bmw.de";

    @Inject
    @MailSession
    private Session mailSession;

    @Inject
    private ConfigService configService;

    @Inject
    private LocalizationService localizationService;

    @Inject
    private Date date;

    public Session getMailSession() {
        return mailSession;
    }

    public MimeBodyPart buildContentFromTemplate(String template, MailArgs mailValues) {
        MimeBodyPart mimeBodyPart = new MimeBodyPart();

        try {
            String content = new MailProcessor('$').generateContent(template, mailValues);
            mimeBodyPart.setContent(content, "text/html");

        } catch (MessagingException ex) {
            LOG.error("MailGeneratorService MessagingException by trying to build mesage content from template");
        }

        return mimeBodyPart;
    }

    public Message createNoReplyMailMessage(String subject, String toRecipient, MimeBodyPart[] contentParts) throws MessagingException {
        Message message = new MimeMessage(getMailSession());
        buildFromPartForNoReply(message);
        buildToPartForOneRecipient(message, toRecipient);
        buildSubjectPart(message, subject);
        buildContentPart(message, contentParts);
        message.setSentDate(date);
        return message;
    }

    private void buildFromPartForNoReply(Message message) throws MessagingException {
        Optional<Address> fromAddressOptional = getNoReplyAddress();
        if (!fromAddressOptional.isPresent()) {
            throw new MessagingException("No FROM email address provided");
        }
        message.setFrom(fromAddressOptional.get());
    }

    private void buildToPartForOneRecipient(Message message, String toRecipient) throws MessagingException {
        Optional<Address> recipientAddressOptional = convertToInternetAddress(toRecipient);
        Address recipientAddress = recipientAddressOptional.get();
        message.setRecipient(RecipientType.TO, recipientAddress);
    }

    private Optional<Address> getNoReplyAddress() {
        try {
            String noReplyAddress = getFromAddress();
            return convertToInternetAddress(noReplyAddress);
        } catch (AddressException ex) {
            LOG.error("MailGeneratorService AddressException by getting noReply Address");
            return Optional.empty();
        }
    }

    private String getFromAddress() {
        String defaultFromAddress;

        if (isProductionEnvironment()) {
            defaultFromAddress = SENDER;
        } else {
            defaultFromAddress = getMailSession().getProperty("mail.smtp.user");
        }

        LOG.debug("Mail From Address is set to {}", defaultFromAddress);
        return defaultFromAddress;
    }

    public boolean isProductionEnvironment() {
        return configService.isProductionEnvironment();
    }

    private static Optional<Address> convertToInternetAddress(String address) throws AddressException {
        if (address == null || address.isEmpty()) {
            throw new AddressException("Mail address is null or empty");
        }
        InternetAddress internetAddress = new InternetAddress(address);
        return Optional.ofNullable(internetAddress);
    }

    private void buildSubjectPart(Message message, String subject) throws MessagingException {
        String mailSubject;
        if (subject != null && !subject.isEmpty()) {
            mailSubject = subject;
        } else {
            mailSubject = localizationService.getValue("mail_no_subject");
        }
        message.setSubject(mailSubject);
    }

    private void buildContentPart(Message message, MimeBodyPart[] contentParts) throws MessagingException {
        Multipart content = new MimeMultipart();
        content.addBodyPart(getHeaderContentPart(), 0);

        for (int i = 1; i <= contentParts.length; i++) {
            content.addBodyPart(contentParts[i - 1], i);
        }
        message.setContent(content);
    }

    private MimeBodyPart getHeaderContentPart() throws MessagingException {
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        attachEmbededImage("/MailTemplates/icons/bmw_group_logo_black.png", "z158mTnRp", mimeBodyPart);
        return mimeBodyPart;
    }

    private void attachEmbededImage(String imageRelativePath, String contentId, MimeBodyPart mimeBodyPart) throws MessagingException {
        String imageType = "image/png";
        try {
            InputStream inputStream = MailGeneratorService.class.getResourceAsStream(imageRelativePath);
            ByteArrayDataSource imageDataSource = new ByteArrayDataSource(inputStream, imageType);
            DataHandler dataHandler = new DataHandler(imageDataSource);

            /* IMPORTANT
            * to get embedded image without attachment set DataHandler BEFORE
            * setting Message Header and Disposition
             */
            mimeBodyPart.setDataHandler(dataHandler);
            mimeBodyPart.setHeader("Content-ID", "<" + contentId + ">");
            mimeBodyPart.setDisposition(MimeBodyPart.INLINE);

        } catch (IOException ex) {
            LOG.error("MailGeneratorService {} by trying to attach embeded image in getHeaderContentPart", ex.getClass().getName());
        }
    }

    public String generateLogoHtml() {
        String html = "<html><div id='headerDiv' style='margin: 0;'>"
                + "<img src='cid:z158mTnRp' alt='BMW Logo'/>"
                + "</div><br/></html>";
        return html;
    }

}
