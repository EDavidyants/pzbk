package de.interfaceag.bmw.pzbk.shared.image;

import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import java.util.Map;
import java.util.Optional;

/**
 *
 * @author sl
 */
public final class ProzessbaukastenImageParameters {

    private final String identifier;
    private final String fachId;
    private final int version;
    private final String isLargeImage;

    private ProzessbaukastenImageParameters(String identifier, String fachId, int version, String isLargeImage) {
        this.identifier = identifier;
        this.fachId = fachId;
        this.version = version;
        this.isLargeImage = isLargeImage;
    }

    public Optional<ProzessbaukastenImage> getProzessbaukastenImage() {
        return ProzessbaukastenImage.byIdentifier(identifier);
    }

    public String getFachId() {
        return fachId;
    }

    public int getVersion() {
        return version;
    }

    public boolean isLargeImage() {
        return isLargeImage != null && "true".equals(isLargeImage);
    }

    public static ProzessbaukastenImageParameters forRequest(Map<String, String> requestParameterMap) {
        String bild = requestParameterMap.get("bild");
        String fachId = requestParameterMap.get("fachId");
        String versionParameter = requestParameterMap.get("version");
        String isLargeImageParameter = requestParameterMap.get("isLargeImage");

        int version;
        if (RegexUtils.matchesId(versionParameter)) {
            version = Integer.parseInt(versionParameter);
        } else {
            version = -1;
        }

        return ProzessbaukastenImageParameters
                .withIdentifier(bild)
                .withFachId(fachId)
                .withVersion(version)
                .withIsLargeImage(isLargeImageParameter)
                .build();
    }

    private static Builder withIdentifier(String identifier) {
        return new Builder().withIdentifier(identifier);
    }

    public static final class Builder {

        private String identifier;
        private String fachId;
        private int version;
        private String isLargeImage;

        private Builder() {
        }

        public Builder withIdentifier(String identifier) {
            this.identifier = identifier;
            return this;
        }

        public Builder withFachId(String fachId) {
            this.fachId = fachId;
            return this;
        }

        public Builder withVersion(int version) {
            this.version = version;
            return this;
        }

        public Builder withIsLargeImage(String isLargeImage) {
            this.isLargeImage = isLargeImage;
            return this;
        }

        public ProzessbaukastenImageParameters build() {
            return new ProzessbaukastenImageParameters(identifier, fachId, version, isLargeImage);
        }
    }

}
