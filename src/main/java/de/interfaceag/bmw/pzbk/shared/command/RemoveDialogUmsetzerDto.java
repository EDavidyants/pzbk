package de.interfaceag.bmw.pzbk.shared.command;

import de.interfaceag.bmw.pzbk.dialog.viewdata.DialogUmsetzerDto;

import java.util.Collections;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class RemoveDialogUmsetzerDto implements DialogUmsetzerCommand {

    private final DialogUmsetzerDto umsetzerToRemove;

    public RemoveDialogUmsetzerDto(DialogUmsetzerDto umsetzerToRemove) {
        this.umsetzerToRemove = umsetzerToRemove;
    }

    @Override
    public Set<DialogUmsetzerDto> apply(Set<DialogUmsetzerDto> target) {
        if (target == null) {
            return Collections.EMPTY_SET;
        } else {
            target.remove(umsetzerToRemove);
            return target;
        }
    }

}
