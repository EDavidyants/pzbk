package de.interfaceag.bmw.pzbk.shared.components;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface AnforderungLink {

    String getId();

    String getFachId();

    String getVersion();

}
