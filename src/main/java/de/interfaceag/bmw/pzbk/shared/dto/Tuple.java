package de.interfaceag.bmw.pzbk.shared.dto;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @param <X>
 * @param <Y>
 */
public interface Tuple<X, Y> extends Serializable {

    X getX();

    Y getY();

}
