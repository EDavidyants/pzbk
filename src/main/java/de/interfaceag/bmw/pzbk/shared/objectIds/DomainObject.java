package de.interfaceag.bmw.pzbk.shared.objectIds;

import java.io.Serializable;

public interface DomainObject extends Serializable {

    Long getId();

}
