package de.interfaceag.bmw.pzbk.shared.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ProcessResultDto implements Serializable {

    private final boolean successful;

    private final List<ResultMessageDto> messages;

    public ProcessResultDto(boolean successful) {
        this.successful = successful;
        this.messages = new ArrayList<>();
    }

    public ProcessResultDto(boolean successful, String message) {
        this.successful = successful;
        List<ResultMessageDto> messageList = new ArrayList<>();
        messageList.add(new ResultMessageDto(message));
        this.messages = messageList;
    }

    public ProcessResultDto(boolean successful, List<ResultMessageDto> messages) {
        this.successful = successful;
        this.messages = messages;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getMessage() {
        return messages.stream()
                .map(m -> {
                    StringBuilder sb = new StringBuilder();
                    m.getId().ifPresent(id -> sb.append(id.toString()).append(": "));
                    sb.append(m.getMessage());
                    return sb.toString();
                })
                .collect(Collectors.joining(", "));
    }

}
