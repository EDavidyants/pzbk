package de.interfaceag.bmw.pzbk.shared.mail;

import java.io.Serializable;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.mail.Session;

/**
 *
 * @author evda
 */
@Stateless
@Named
public class MailSessionProducer implements Serializable {

    @Resource(lookup = "pzbkMail")
    private Session mailSession;

    @Produces
    @MailSession
    public Session getMailSession() {
        return mailSession;
    }

}
