package de.interfaceag.bmw.pzbk.shared.math;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public final class Sum {

    private Sum() {
    }

    public static Integer computeSum(Stream<Integer> input) {
        AtomicInteger sum = new AtomicInteger();
        input.forEach(integer -> sum.set(integer + sum.get()));
        return sum.get();
    }
}
