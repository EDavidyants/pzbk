package de.interfaceag.bmw.pzbk.shared.dto;

import java.io.Serializable;
import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ResultMessageDto implements Serializable {

    private final Long id;

    private final String message;

    public ResultMessageDto(Long id, String message) {
        this.id = id;
        this.message = message;
    }

    public ResultMessageDto(String message) {
        this.id = null;
        this.message = message;
    }

    public Optional<Long> getId() {
        return Optional.ofNullable(id);
    }

    public String getMessage() {
        return message;
    }

}
