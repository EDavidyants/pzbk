package de.interfaceag.bmw.pzbk.shared.dto;

/**
 *
 * @author lomu
 */
public class Point extends GenericTuple<Double, Double> {

    public Point(Double xValue, Double yValue) {
        super(xValue, yValue);
    }

    public Point(int xValue, int yValue) {
        super(xValue * 1.0, yValue * 1.0);
    }

}
