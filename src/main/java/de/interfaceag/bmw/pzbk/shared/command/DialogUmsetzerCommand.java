package de.interfaceag.bmw.pzbk.shared.command;

import de.interfaceag.bmw.pzbk.dialog.viewdata.DialogUmsetzerDto;

import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface DialogUmsetzerCommand extends Command<Set<DialogUmsetzerDto>> {

}
