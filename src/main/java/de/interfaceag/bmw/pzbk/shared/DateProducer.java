package de.interfaceag.bmw.pzbk.shared;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import java.util.Date;

/**
 *
 * @author fn
 */
@RequestScoped
public class DateProducer {

    @Produces
    @RequestScoped
    public Date getNewDate() {
        return new Date();
    }

}
