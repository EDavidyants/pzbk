package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author baba
 */
public final class SearchUtil {

    private SearchUtil() {
    }

    public static QueryPartDTO buildPermissionQuery(Type type, long mitarbeiterId, String qNumber) {
        QueryPartDTO qp = new QueryPartDTO(" AND (("
                + "('ADMIN' IN ( SELECT r.rolename FROM UserToRole r WHERE r.username = :qNumber )) "
                + "OR "
                + "('ANFORDERER' IN ( SELECT r.rolename FROM UserToRole r WHERE r.username = :qNumber )) "
                + "OR "
                + "('T_TEAMLEITER' IN ( SELECT r.rolename FROM UserToRole r WHERE r.username = :qNumber )) "
                + "OR "
                + "('TTEAM_VERTRETER' IN ( SELECT r.rolename FROM UserToRole r WHERE r.username = :qNumber ))) "
                + "OR ");
        if (!type.isProzessbaukasten()) {
            qp.append("( "
                    + "( 'SENSOR_EINGESCHRAENKT' IN ( SELECT r.rolename FROM UserToRole r WHERE r.username = :qNumber )) "
                    + "AND "
                    + "a.sensor.id = :mitarbeiterId "
                    + "AND "
                    + "( CAST( a.sensorCoc.sensorCocId AS varchar ) IN  "
                    + "( SELECT b.fuerId FROM Berechtigung b "
                    + "WHERE b.mitarbeiter.id = :mitarbeiterId AND b.fuer = :sensorCocFuer )) "
                    + ")"
                    + "OR "
            );
        }
        qp.append("( 'SENSOR_EINGESCHRAENKT' NOT IN ( SELECT r.rolename FROM UserToRole r WHERE r.username = :qNumber )) "
                + "AND ("
                + "( CAST( a.sensorCoc.sensorCocId AS varchar ) IN  "
                + "( SELECT b.fuerId FROM Berechtigung b "
                + "WHERE b.mitarbeiter.id = :mitarbeiterId AND b.fuer = :sensorCocFuer )) "
                + "OR "
                + "( a.sensorCoc.zakEinordnung IN  "
                + "( SELECT b.fuerId FROM Berechtigung b "
                + "WHERE b.mitarbeiter.id = :mitarbeiterId AND b.fuer = :zakEinordnungFuer )) "
        );
        if (type.isAnforderung()) {
            qp.append("OR "
                    + "( CAST( a.tteam.id AS varchar ) IN  "
                    + "( SELECT b.fuerId FROM Berechtigung b "
                    + "WHERE b.mitarbeiter.id = :mitarbeiterId AND b.fuer = :tteamFuer )) "
                    + "OR "
                    + "( CAST( u.seTeam.id AS varchar ) IN  "
                    + "( SELECT b.fuerId FROM Berechtigung b "
                    + "WHERE b.mitarbeiter.id = :mitarbeiterId AND b.fuer = :seTeamFuer )) "
            );
            qp.append("OR "
                    + "( 'TTEAMMITGLIED' IN ( SELECT r.rolename FROM UserToRole r WHERE r.username = :qNumber ))"
                    + " AND "
                    + "( a.tteam.id IN (SELECT tteammitglied.tteam.id FROM TTeamMitglied tteammitglied WHERE tteammitglied.mitarbeiter.id = :mitarbeiterId )) ");
            qp.append("OR "
                    + "( 'TTEAM_VERTRETER' IN ( SELECT r.rolename FROM UserToRole r WHERE r.username = :qNumber ))"
                    + " AND "
                    + "( a.tteam.id IN (SELECT tteamvertreter.tteam.id FROM TteamVertreter tteamvertreter WHERE tteamvertreter.mitarbeiter.id = :mitarbeiterId )) ");

            qp.put("tteamFuer", BerechtigungZiel.TTEAM);
            qp.put("seTeamFuer", BerechtigungZiel.MODULSETEAM);
            qp.put("mitarbeiterId", mitarbeiterId);
            qp.put("qNumber", qNumber);
        }

        qp.append("))");

        qp.put("mitarbeiterId", mitarbeiterId);
        qp.put("qNumber", qNumber);
        qp.put("sensorCocFuer", BerechtigungZiel.SENSOR_COC);
        qp.put("zakEinordnungFuer", BerechtigungZiel.ZAK_EINORDNUNG);

        return qp;
    }

    public static List<String> splitQueryWithTextRecognition(String query) {
        query = query + " ";
        List<String> words = splitQuery(query, " ");
        List<String> result = new ArrayList<>();

        words.forEach(word -> {
            if (patternMatchId(word) || patternMatchNumber(word)) {
                result.add(word);
            }
        });

        words.removeAll(result);
        StringBuilder stringBuilder = new StringBuilder();

        while (query.contains(" ")) {
            String word = query.substring(0, query.indexOf(" "));
            if (!patternMatchId(word) && !patternMatchNumber(word)) {
                stringBuilder.append(word).append(" ");
            } else {
                result.add(stringBuilder.toString());
                stringBuilder = new StringBuilder();
            }
            query = query.substring(query.indexOf(" ") + 1);
        }
        if (stringBuilder.length() > 0) {
            result.add(stringBuilder.toString());
        }

        return result;
    }

    public static List<String> splitQuery(String query, String delimitor) {
        List<String> result = new ArrayList<>();
        StringTokenizer tok = new StringTokenizer(query, delimitor);
        while (tok.hasMoreTokens()) {
            result.add(tok.nextToken());
        }
        return result;
    }

    public static Boolean patternMatchId(String input) {
        String regex = "[aAmM][0-9]+";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(input);
        return m.matches();
    }

    public static Boolean patternMatchNumber(String input) {
        String regex = "\\d+";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(input);
        return m.matches();
    }

    public static QueryPartDTO buildFromStatementForCount(Type type, boolean derivatFilter) {
        QueryPartDTO qp = new QueryPartDTO();

        qp.append(" FROM ").append(type.getBezeichnung()).append(" a");
        if (type.isAnforderung()) {
            qp.append(" LEFT JOIN a.anfoFreigabeBeiModul AS afbm");
            qp.append(" LEFT JOIN a.referenzSystemLinks AS r");
            qp.append(" LEFT JOIN a.umsetzer AS u");
            qp.append(" LEFT JOIN a.prozessbaukasten AS p");
            qp.append(" LEFT JOIN a.prozessbaukastenThemenklammern AS pt ");
            if (derivatFilter) {
                qp.append(" JOIN DerivatAnforderungModul d ON d.zuordnungAnforderungDerivat.anforderung.id = a.id");
            }
        }
        qp.append(" WHERE 1=1");

        return qp;
    }

    public static QueryPartDTO buildFromStatementForSearchResult(Type type, boolean derivatFilter, boolean withImage) {
        QueryPartDTO qp = new QueryPartDTO();

        qp.append(" FROM ").append(type.getBezeichnung()).append(" a");
        if (withImage) {
            qp.append(" LEFT JOIN a.bild AS b");
        }
        if (type.isAnforderung()) {
            qp.append(" LEFT JOIN a.anfoFreigabeBeiModul AS afbm");
            qp.append(" LEFT JOIN a.referenzSystemLinks AS r");
            qp.append(" LEFT JOIN a.umsetzer AS u");
            qp.append(" LEFT JOIN a.meldungen AS m");
            qp.append(" LEFT JOIN a.prozessbaukasten AS p");
            qp.append(" LEFT JOIN a.prozessbaukastenThemenklammern AS pt");
            if (derivatFilter) {
                qp.append(" JOIN DerivatAnforderungModul d ON d.zuordnungAnforderungDerivat.anforderung.id = a.id");
            }
        }
        qp.append(" WHERE 1=1");

        return qp;
    }

    public static QueryPartDTO buildSelectStatementForAnforderungListQuery(Type type, boolean withImage) {
        QueryPartDTO qp = new QueryPartDTO();

        if (type.isProzessbaukasten()) {
            qp.append("SELECT DISTINCT a.id, a.fachId, a.beschreibung, a.status, a.sensorCoc");
        } else {
            qp.append("SELECT DISTINCT a.id, a.fachId, a.beschreibungAnforderungDe, a.status, "
                    + "a.sensorCoc, a.beschreibungStaerkeSchwaeche");
        }

        if (type.isAnforderung() || type.isProzessbaukasten()) {
            qp.append(", a.version");
        }
        if (withImage) {
            qp.append(", b.thumbnailImage");
        }
        if (type.isAnforderung()) {
            qp.append(", COUNT(m)");
        }

        return qp;
    }

    public static QueryPartDTO buildGroupByClause(Type type, boolean withImage) {
        QueryPartDTO qp = new QueryPartDTO();
        if (type.isAnforderung()) {
            qp.append(" GROUP BY a.id, a.sensorCoc ");
            if (withImage) {
                qp.append(", b.thumbnailImage");
            }
        }
        return qp;
    }
}
