package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author evda
 */
public final class BerechtigungDtoUtils implements Serializable {

    private BerechtigungDtoUtils() {

    }

    public static List<Long> selectTteamBerechtigungen(List<BerechtigungDto> berechtigungen) {
        List<Long> tteamIds = berechtigungen.stream()
                .filter(berechtigung -> berechtigung.getType() == BerechtigungZiel.TTEAM)
                .map(berechtigung -> berechtigung.getId())
                .collect(Collectors.toList());
        return tteamIds;
    }

    public static List<String> selectZakEinordnungBerechtigingen(List<BerechtigungDto> berechtigungen) {
        List<String> zakEinordnungen = berechtigungen.stream()
                .filter(berechtigung -> berechtigung.getType() == BerechtigungZiel.ZAK_EINORDNUNG)
                .map(berechtigung -> berechtigung.getName())
                .collect(Collectors.toList());
        return zakEinordnungen;
    }

    public static List<Long> selectDerivatBerechtigungen(List<BerechtigungDto> berechtigungen) {
        List<Long> derivatIds = berechtigungen.stream()
                .filter(berechtigung -> berechtigung.getType() == BerechtigungZiel.DERIVAT)
                .map(berechtigung -> berechtigung.getId())
                .collect(Collectors.toList());
        return derivatIds;
    }

    public static List<Long> selectSensorCocBerechtigingen(List<BerechtigungDto> berechtigungen) {
        List<Long> sensorCocIds = berechtigungen.stream()
                .filter(berechtigung -> berechtigung.getType() == BerechtigungZiel.SENSOR_COC)
                .map(berechtigung -> berechtigung.getId())
                .collect(Collectors.toList());
        return sensorCocIds;
    }

    public static List<Long> selectSensorCocBerechtigungenForUmsetzungbestaetigung(List<BerechtigungDto> berechtigungen) {
        List<Long> sensorCocIds = berechtigungen.stream()
                .filter(berechtigung -> berechtigung.getType() == BerechtigungZiel.SENSOR_COC
                && berechtigung.getRolle() == Rolle.UMSETZUNGSBESTAETIGER)
                .map(berechtigung -> berechtigung.getId())
                .collect(Collectors.toList());
        return sensorCocIds;
    }

    public static List<Long> selectSensorCocBerechtigungenForSCLAndVertreter(List<BerechtigungDto> berechtigungen) {
        List<Long> sensorCocIds = berechtigungen.stream()
                .filter(berechtigung -> berechtigung.getType() == BerechtigungZiel.SENSOR_COC
                && (berechtigung.getRolle() == Rolle.SENSORCOCLEITER || berechtigung.getRolle() == Rolle.SCL_VERTRETER))
                .map(berechtigung -> berechtigung.getId())
                .collect(Collectors.toList());
        return sensorCocIds;
    }

    public static List<Long> selectModulSeTeamBerechtigingen(List<BerechtigungDto> berechtigungen) {
        List<Long> modulSeTeamIds = berechtigungen.stream()
                .filter(berechtigung -> berechtigung.getType() == BerechtigungZiel.MODULSETEAM)
                .map(berechtigung -> berechtigung.getId())
                .collect(Collectors.toList());
        return modulSeTeamIds;
    }

}
