package de.interfaceag.bmw.pzbk.shared.command;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungFreigabeDto;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AddAnforderungFreigabeBeiModulSeTeam implements AnforderungFreigabeDtoCommand {

    private final AnforderungFreigabeDto newFreigabe;

    public AddAnforderungFreigabeBeiModulSeTeam(AnforderungFreigabeDto newFreigabe) {
        this.newFreigabe = newFreigabe;
    }

    @Override
    public List<AnforderungFreigabeDto> apply(List<AnforderungFreigabeDto> target) {
        if (target == null) {
            return Arrays.asList(newFreigabe);
        } else {
            if (target.stream().noneMatch(freigabe -> freigabe.getSeTeamId().equals(newFreigabe.getSeTeamId()))) {
                target.add(newFreigabe);
            }
            return target;
        }
    }

}
