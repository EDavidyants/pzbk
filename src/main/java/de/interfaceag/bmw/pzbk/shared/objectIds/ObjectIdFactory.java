package de.interfaceag.bmw.pzbk.shared.objectIds;

import java.util.Collection;
import java.util.stream.Collectors;

public final class ObjectIdFactory {

    private ObjectIdFactory() {
    }

    public static Collection<AnforderungId> forAnforderungIds(Collection<Long> ids) {
        return ids.stream().map(AnforderungId::new).collect(Collectors.toList());
    }

    public static Collection<MeldungId> forMeldungIds(Collection<Long> ids) {
        return ids.stream().map(MeldungId::new).collect(Collectors.toList());
    }

}
