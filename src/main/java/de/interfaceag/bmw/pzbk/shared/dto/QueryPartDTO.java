package de.interfaceag.bmw.pzbk.shared.dto;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class QueryPartDTO implements QueryPart {

    private String query;
    private Map<String, Object> parameterMap;
    private String sort;
    private int parameterIndex = 1;

    // ---------- constructors ------------------------------
    public QueryPartDTO() {
        this.query = "";
        this.parameterMap = new HashMap<>();
        this.sort = "";
    }

    public QueryPartDTO(String query) {
        this.query = query;
        this.parameterMap = new HashMap<>();
        this.sort = "";
    }

    // ---------- methods ------------------------------------
    @Override
    public QueryPartDTO merge(QueryPart qp) {
        append(qp.getQuery());
        putAll(qp.getParameterMap());
        if (!qp.getSort().isEmpty()) {
            setSort(qp.getSort());
        }
        return this;
    }

    @Override
    public QueryPartDTO append(String queryPart) {
        if (queryPart != null) {
            this.query = this.query.concat(queryPart);
        }
        return this;
    }

    public QueryPartDTO deleteFrom(int begin) {
        if (this.query.length() > begin && begin > 0) {
            this.query = this.query.substring(0, begin);
        }
        return this;
    }

    @Override
    public QueryPartDTO put(String key, Object value) {
        if (key != null && !key.isEmpty() && value != null) {
            this.parameterMap.put(key, value);
        }
        return this;
    }

    public QueryPartDTO putAll(Map<String, Object> parameterMap) {
        if (parameterMap != null) {
            this.parameterMap.putAll(parameterMap);
        }
        return this;
    }

    @Override
    public String toString() {
        return getQuery();
    }

    @Override
    public String getQuery() {
        return query;
    }

    public String getQueryWithSorting() {
        return query + " " + sort;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public Map<String, Object> getParameterMap() {
        return parameterMap;
    }

    public void setParameterMap(Map<String, Object> parameterMap) {
        this.parameterMap = parameterMap;
    }

    @Override
    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    @Override
    public Query buildGenericQuery(EntityManager entityManager, Class type) {
        Query result = entityManager.createQuery(this.getQuery(), type);
        if (!this.getParameterMap().isEmpty()) {
            this.getParameterMap().entrySet().forEach(parameter -> {
                result.setParameter(parameter.getKey(), parameter.getValue());
            });
        }
        return result;
    }

    @Override
    public Query buildNativeQuery(EntityManager entityManager, Class type) {
        Query result = entityManager.createNativeQuery(this.getQuery(), type);
        if (!this.getParameterMap().isEmpty()) {
            this.getParameterMap().entrySet().forEach(parameter -> {
                result.setParameter(parameter.getKey(), parameter.getValue());
            });
        }
        return result;
    }

    @Override
    public Query buildGenericQuery(EntityManager entityManager) {
        Query result = entityManager.createQuery(this.getQuery());
        if (!this.getParameterMap().isEmpty()) {
            this.getParameterMap().entrySet().forEach(parameter -> {
                result.setParameter(parameter.getKey(), parameter.getValue());
            });
        }
        return result;
    }

    @Override
    public Query buildNativeQuery(EntityManager entityManager) {
        Query result = entityManager.createNativeQuery(this.getQuery());
        if (!this.getParameterMap().isEmpty()) {
            this.getParameterMap().entrySet().forEach(parameter -> {
                result.setParameter(parameter.getKey(), parameter.getValue());
            });
        }
        return result;
    }

    @Override
    public QueryPart appendNativeParameters(Collection values) {
        Collection<Integer> indices = putNativeParameters(values);

        String parameters = indices.stream().map(value -> "?").collect(Collectors.joining(",", "(", ")"));
        return this.append(parameters);
    }

    @Override
    public Integer putNativeParameter(Object value) {
        put(Integer.toString(parameterIndex), value);
        parameterIndex++;
        return parameterIndex - 1;
    }

    private Collection<Integer> putNativeParameters(Collection values) {
        Iterator iterator = values.iterator();
        Collection<Integer> indices = new ArrayList<>();
        while (iterator.hasNext()) {
            Object next = iterator.next();
            Integer index = putNativeParameter(next);
            indices.add(index);
        }
        return indices;
    }

}
