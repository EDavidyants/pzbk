package de.interfaceag.bmw.pzbk.shared.dto;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface Commentable extends Serializable {

    String getComment();

    void setComment(String comment);

}
