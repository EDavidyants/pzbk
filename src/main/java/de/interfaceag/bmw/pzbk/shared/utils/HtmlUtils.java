package de.interfaceag.bmw.pzbk.shared.utils;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public final class HtmlUtils {

    private HtmlUtils() {
    }

    /**
     *
     * @param input ...
     * @return ...
     */
    public static String insertHtmlLineBreaks(String input) {
        return input.replace("\n", "<br>");
    }

    /**
     *
     * @param content ...
     * @return ...
     */
    public static String getTitle(String content) {
        return getHtmlTagContent(content, "title");
    }

    /**
     *
     * @param input ...
     * @param tagName ...
     * @return ...
     */
    public static String getHtmlTagContent(String input, String tagName) {
        String startTag = createStartTag(tagName);
        String endTag = createEndTag(tagName);

        int start = input.indexOf(startTag) + startTag.length();
        int end = input.indexOf(endTag);
        if (end - start <= 0) {
            return "";
        } else {
            return input.substring(start, end);
        }
    }

    /**
     *
     * @param name ...
     * @return ...
     */
    public static String createStartTag(String name) {
        return "<" + name + ">";
    }

    /**
     *
     * @param name ...
     * @return ...
     */
    public static String createEndTag(String name) {
        return "</" + name + ">";
    }

    public static String createTableRow(String... input) {
        StringBuilder sb = new StringBuilder();

        sb.append("<tr>");
        for (String str : input) {
            sb.append("<td>").append(str).append("</td>");
        }
        sb.append("</td>");
        return sb.toString();
    }

}
