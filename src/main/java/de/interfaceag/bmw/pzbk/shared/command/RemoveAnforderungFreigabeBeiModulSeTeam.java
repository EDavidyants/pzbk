package de.interfaceag.bmw.pzbk.shared.command;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungFreigabeDto;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class RemoveAnforderungFreigabeBeiModulSeTeam implements AnforderungFreigabeDtoCommand {

    private final AnforderungFreigabeDto freigabeToRemove;

    public RemoveAnforderungFreigabeBeiModulSeTeam(AnforderungFreigabeDto newFreigabe) {
        this.freigabeToRemove = newFreigabe;
    }

    @Override
    public List<AnforderungFreigabeDto> apply(List<AnforderungFreigabeDto> target) {
        if (target == null) {
            return Collections.EMPTY_LIST;
        } else {
            List<AnforderungFreigabeDto> freigabenToRemove = target.stream().filter(freigabe -> freigabe.getSeTeamId().equals(freigabeToRemove.getSeTeamId()))
                    .collect(Collectors.toList());

            freigabenToRemove.forEach(freigabe -> target.remove(freigabe));

            return target;
        }
    }

}
