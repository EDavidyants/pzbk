package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.shared.PersistedObject;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 *
 * @author sl
 * @param <T>
 */
public class CollectionUtils<T extends PersistedObject> {

    public CollectionUtils() {
    }

    public Collection<Long> getIds(Collection<T> collection) {
        return collection.stream().map(PersistedObject::getId).collect(Collectors.toSet());
    }

}
