package de.interfaceag.bmw.pzbk.shared.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class DateUtils {

    private DateUtils() {
    }

    public static LocalDate convertToLocalDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return LocalDateTime.ofInstant(calendar.toInstant(), calendar.getTimeZone().toZoneId()).toLocalDate();
    }

    public static Date addDays(Date date, int number) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, number);
        return calendar.getTime();
    }

    public static Date subtractDays(Date date, int number) {
       return addDays(date, -number);
    }

    public static Date getDateForLocalDate(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }

        ZoneId defaultZoneId = ZoneId.systemDefault();
        return Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());
    }

    public static boolean isMonday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY;
    }

    public static long convertLongToDays(long value) {
        return TimeUnit.DAYS.convert(value, TimeUnit.MILLISECONDS);
    }

    /**
     * removes time values for date.
     * @param date
     */
    public static Date removeTime(Date date) {
        if (date == null) {
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getMaxDate() {
        return new Date();
    }

    public static Date getMinDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2018, 2, 1);
        return calendar.getTime();
    }
}
