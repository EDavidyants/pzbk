package de.interfaceag.bmw.pzbk.shared;

import java.io.Serializable;

/**
 *
 * @author sl
 */
public interface PersistedObject extends Serializable {

    Long getId();

}
