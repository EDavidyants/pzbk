package de.interfaceag.bmw.pzbk.shared.image;

import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author sl
 */
public enum ProzessbaukastenImage {

    KONZEPTBAUM("konzeptbaum"),
    GRAFIK_UMFANG("grafikUmfang");

    private final String identifier;

    ProzessbaukastenImage(String identifier) {
        this.identifier = identifier;
    }

    private String getIdentifier() {
        return identifier;
    }

    public static Optional<ProzessbaukastenImage> byIdentifier(String identifier) {
        return Stream.of(ProzessbaukastenImage.values())
                .filter(pi -> pi.getIdentifier().equals(identifier)).findAny();
    }

}
