package de.interfaceag.bmw.pzbk.shared.command;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @param <T>
 */
public interface Command<T> {

    T apply(T target);

}
