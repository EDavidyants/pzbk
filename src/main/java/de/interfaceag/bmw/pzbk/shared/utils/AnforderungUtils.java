package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;

/**
 *
 * @author sl
 */
public final class AnforderungUtils {

    private AnforderungUtils() {
    }

    public static boolean isStandardvereinbarung(Anforderung anforderung) {
        for (AnforderungFreigabeBeiModulSeTeam freigabe : anforderung.getAnfoFreigabeBeiModul()) {
            if (freigabe.isZak()) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

}
