package de.interfaceag.bmw.pzbk.shared.dto;

import java.util.Objects;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @param <X>
 * @param <Y>
 */
public class GenericTuple<X, Y> implements Tuple<X, Y> {

    private final X x;

    private final Y y;

    public GenericTuple(X xValue, Y yValue) {
        this.x = xValue;
        this.y = yValue;
    }

    @Override
    public String toString() {
        return "Tuple{" + "x=" + x.toString() + ", y=" + y.toString() + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.x);
        hash = 41 * hash + Objects.hashCode(this.y);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GenericTuple<?, ?> other = (GenericTuple<?, ?>) obj;
        if (!Objects.equals(this.x, other.x)) {
            return false;
        }
        if (!Objects.equals(this.y, other.y)) {
            return false;
        }
        return true;
    }

    @Override
    public X getX() {
        return x;
    }

    @Override
    public Y getY() {
        return y;
    }

}
