package de.interfaceag.bmw.pzbk.shared.utils;

import java.io.Serializable;

@Deprecated
public class UserInput implements Serializable {

    // ------------  fields ----------------------------------------------------
    private String value;

    // ------------  constructors ----------------------------------------------
    public UserInput() {
        value = "";
    }

    // ------------  methods ---------------------------------------------------
    public boolean isEmpty() {
        return value == null || value.trim().isEmpty();
    }

    public void reset() {
        value = "";
    }

    // ------------  getter / setter -------------------------------------------
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
