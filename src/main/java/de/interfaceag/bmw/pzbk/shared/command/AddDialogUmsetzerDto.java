package de.interfaceag.bmw.pzbk.shared.command;

import de.interfaceag.bmw.pzbk.dialog.viewdata.DialogUmsetzerDto;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AddDialogUmsetzerDto implements DialogUmsetzerCommand {

    private final DialogUmsetzerDto newUmsetzer;

    public AddDialogUmsetzerDto(DialogUmsetzerDto newUmsetzer) {
        this.newUmsetzer = newUmsetzer;
    }

    @Override
    public Set<DialogUmsetzerDto> apply(Set<DialogUmsetzerDto> target) {
        if (target == null) {
            return new HashSet<>(Arrays.asList(newUmsetzer));
        } else {
            target.add(newUmsetzer);
            return target;
        }
    }

}
