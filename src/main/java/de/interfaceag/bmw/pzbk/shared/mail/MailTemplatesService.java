package de.interfaceag.bmw.pzbk.shared.mail;

import de.interfaceag.bmw.pzbk.entities.Wert;
import de.interfaceag.bmw.pzbk.enums.Attribut;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.shared.utils.FileUtils;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author evda
 */
@Stateless
public class MailTemplatesService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(MailTemplatesService.class);
    public static final String MAILTEMPLATES_BASE_PATH = "/MailTemplates/";

    @Inject
    private ConfigService configService;

    public String getMailTemplate(Attribut templateName) {
        List<Wert> templates = configService.getWertByAttribut(templateName);
        if (!templates.isEmpty()) {
            return templates.get(0).getWert();
        }
        return "";
    }

    public void initDefaultMailTemplates() {
        saveMailTemplate(Attribut.MAILTEMPLATE_STATUS_CHANGED, "MailStatusChanged.html");
        saveMailTemplate(Attribut.MAILTEMPLATE_ATTR_CHANGED, "MailAttrChanged.html");
        saveMailTemplate(Attribut.MAILTEMPLATE_BEWERTUNGSAUFTRAGS_VORLAGE, "MailBewertungsauftragsVorlage.html");
        saveMailTemplate(Attribut.MAILTEMPLATE_REMINDER_VORLAGE, "MailReminderVorlage.html");
        saveMailTemplate(Attribut.MAILTEMPLATE_LASTCALL_VORLAGE, "MailLastCallVorlage.html");
        saveMailTemplate(Attribut.MAILTEMPLATE_MAIL_TO_FTS, "MailToFTSVorlage.html");
        saveMailTemplate(Attribut.MAILTEMPLATE_MAIL_TO_ANFORDERER_ZAK, "MailToAnfordererZAK.html");
        saveMailTemplate(Attribut.MAILTEMPLATE_MAIL_TO_ANFORDERER_UB, "MailToAnfordererUB.html");
        saveMailTemplate(Attribut.BERECHTIGUNGSANTRAG_EINGANGSBESTAETIGUNG, "MailBerechtigungsantragEingangsbestaetigung.html");
        saveMailTemplate(Attribut.BERECHTIGUNGSANTRAG_STATUS_UPDATE, "MailBerechtigungsantragAktualisiert.html");
    }

    private void saveMailTemplate(Attribut templateName, String name) {
        LOG.debug("Saving Mail Template '" + templateName.name() + "'");
        InputStream inputStream = ConfigService.class.getResourceAsStream(MAILTEMPLATES_BASE_PATH + name);
        String inputStreamAsString = FileUtils.inputStreamToUtf8String(inputStream);
        persistMailTemplate(templateName, inputStreamAsString);
    }

    private void persistMailTemplate(Attribut attribut, String content) {
        configService.persistWert(new Wert(attribut, content));
    }

}
