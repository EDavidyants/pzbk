package de.interfaceag.bmw.pzbk.shared.utils;

/**
 *
 * @author sl
 */
public final class InputValidationUtils {

    private InputValidationUtils() {
    }

    public static boolean allObjectsAreNotNull(Object... objects) {
        for (Object object : objects) {
            if (object == null) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    public static boolean anyObjectIsNull(Object... objects) {
        for (Object object : objects) {
            if (object == null) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }
}
