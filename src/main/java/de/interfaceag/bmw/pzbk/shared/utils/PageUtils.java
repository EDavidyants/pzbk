package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.exceptions.NotSupportedException;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ActiveIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class PageUtils {

    private static final Logger LOG = LoggerFactory.getLogger(PageUtils.class.getName());

    private PageUtils() {
    }

    public static String getUrlForPageWithId(Page page, long id) {
        try {
            Map<String, String> paramterMap = new HashMap<>();
            paramterMap.put("id", Long.toString(id));
            switch (page) {
                case ANFORDERUNGVIEW:
                case ANFORDERUNGEDIT:
                case MELDUNGVIEW:
                case MELDUNGEDIT:
                    return getUrlForPageWithParamter(page, paramterMap);
                case SUCHE:
                case BERICHTSWESEN:
                case VEREINBARUNG:
                case UMSETZUNGSBESTAETIGUNG:
                case DASHBOARD:
                default:
                    throw new NotSupportedException("For page " + page.toString() + " this method is not applicable");
            }
        } catch (NotSupportedException ex) {
            LOG.error(ex.toString());
            return "";
        }
    }

    public static String getUrlForProzessbaukastenView(String fachId, String version, ActiveIndex activeIndex) {
        Map<String, String> paramterMap = new HashMap<>();
        paramterMap.put("fachId", fachId);
        paramterMap.put("version", version);
        paramterMap.put("activeIndex", activeIndex.getIndex().toString());
        return getUrlForPageWithParamter(Page.PROZESSBAUKASTEN_VIEW, paramterMap);
    }

    public static String getUrlForProzessbaukastenView(String fachId, Integer version, ActiveIndex activeIndex) {
        return getUrlForProzessbaukastenView(fachId, version.toString(), activeIndex);
    }

    public static String getUrlForProzessbaukastenEdit(String fachId, String version, ActiveIndex activeIndex) {
        Map<String, String> paramterMap = new HashMap<>();
        paramterMap.put("fachId", fachId);
        paramterMap.put("version", version);
        paramterMap.put("activeIndex", activeIndex.getIndex().toString());
        return getUrlForPageWithParamter(Page.PROZESSBAUKASTEN_EDIT, paramterMap);
    }

    public static String getUrlForProzessbaukastenEditWithStatusChange(String fachId, String version, ActiveIndex activeIndex, ProzessbaukastenStatus newStatus) {
        Map<String, String> paramterMap = new HashMap<>();
        paramterMap.put("fachId", fachId);
        paramterMap.put("version", version);
        paramterMap.put("newStatus", Integer.toString(newStatus.getId()));
        paramterMap.put("activeIndex", activeIndex.getIndex().toString());
        return getUrlForPageWithParamter(Page.PROZESSBAUKASTEN_EDIT, paramterMap);
    }

    public static String getUrlForPageWithFachIdAndVersion(Page page, String fachId, Integer version) {
        try {
            Map<String, String> paramterMap = new HashMap<>();
            paramterMap.put("fachId", fachId);
            paramterMap.put("version", version.toString());
            switch (page) {
                case ANFORDERUNGVIEW:
                case ANFORDERUNGEDIT:
                case MELDUNGVIEW:
                case MELDUNGEDIT:
                case PROZESSBAUKASTEN_VIEW:
                case PROZESSBAUKASTEN_EDIT:
                    return getUrlForPageWithParamter(page, paramterMap);
                case SUCHE:
                case BERICHTSWESEN:
                case VEREINBARUNG:
                case UMSETZUNGSBESTAETIGUNG:
                case DASHBOARD:
                default:
                    throw new NotSupportedException("For page " + page.toString() + " this method is not applicable");
            }
        } catch (NotSupportedException ex) {
            LOG.error(ex.toString());
            return "";
        }
    }

    public static String getUrlForPageWithFachIdAndVersionAndId(Page page, String fachId, Integer version, Long id) {
        try {
            Map<String, String> paramterMap = new HashMap<>();
            paramterMap.put("id", Long.toString(id));
            paramterMap.put("fachId", fachId);
            paramterMap.put("version", version.toString());
            switch (page) {
                case ANFORDERUNGVIEW:
                case ANFORDERUNGEDIT:
                case MELDUNGVIEW:
                case MELDUNGEDIT:
                case PROZESSBAUKASTEN_VIEW:
                case PROZESSBAUKASTEN_EDIT:
                    return getUrlForPageWithParamter(page, paramterMap);
                case SUCHE:
                case BERICHTSWESEN:
                case VEREINBARUNG:
                case UMSETZUNGSBESTAETIGUNG:
                case DASHBOARD:
                default:
                    throw new NotSupportedException("For page " + page.toString() + " this method is not applicable");
            }
        } catch (NotSupportedException ex) {
            LOG.error(ex.toString());
            return "";
        }
    }

    public static String getRedirectUrlForPage(Page page) {
        Map<String, String> paramterMap = new HashMap<>();
        return getUrlForPageWithParamter(page, paramterMap);
    }

    public static String getUrlForPageWithParamter(Page page, Map<String, String> paramterMap) {
        return page.getUrl() + getParamterAsString(paramterMap);
    }

    protected static String getParamterAsString(Map<String, String> paramterMap) {
        StringBuilder sb = new StringBuilder("?");
        if (paramterMap != null) {
            paramterMap.entrySet().forEach(e -> sb.append(e.getKey()).append("=").append(e.getValue()).append("&"));
        }
        sb.append("faces-redirect=true");
        return sb.toString();
    }

}
