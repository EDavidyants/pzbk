package de.interfaceag.bmw.pzbk.shared.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Christian Schauer
 */
public final class SecurityUtils {

    private static final Logger LOG = LoggerFactory.getLogger(SecurityUtils.class.getName());

    private SecurityUtils() {
    }

    public static String hashStringSha256(String input) {

        String tmp_pass;
        StringBuilder result = new StringBuilder();

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(input.getBytes("UTF-8"));
            tmp_pass = new BigInteger(1, md.digest()).toString(16);

            // Case leading 0
            int leading_zeros = 64 - tmp_pass.length();

            for (int i = 0; i < leading_zeros; i++) {
                result.append("0");
            }

            result.append(tmp_pass);

        } catch (UnsupportedEncodingException | NoSuchAlgorithmException exception) {
            LOG.error(exception.getMessage());
        }

        return result.toString();
    }

}
