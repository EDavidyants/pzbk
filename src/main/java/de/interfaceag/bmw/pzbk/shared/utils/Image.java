package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.globalsearch.SearchResultDTO;
import de.interfaceag.bmw.pzbk.services.FileService;
import de.interfaceag.bmw.pzbk.services.ImageService;
import de.interfaceag.bmw.pzbk.shared.image.ProzessbaukastenImageParameters;
import de.interfaceag.bmw.pzbk.shared.image.ProzessbaukastenImageService;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author sl
 */
@Named
@RequestScoped
public class Image implements Serializable {

    @Inject
    private ImageService imageService;

    @Inject
    private FileService fileService;

    @Inject
    private ProzessbaukastenImageService prozessbaukastenImageService;

    public StreamedContent getProzessbaukastenImage() {
        if (pageIsRendering()) {
            return getDefaultContent();
        } else {
            Map<String, String> requestParameterMap = getRequestParameterMap();
            ProzessbaukastenImageParameters imageParameters = ProzessbaukastenImageParameters.forRequest(requestParameterMap);
            return prozessbaukastenImageService.getProzessbaukastenImage(imageParameters);
        }
    }

    public StreamedContent getKonzeptImage() {
        if (pageIsRendering()) {
            return getDefaultContent();
        } else {
            Map<String, String> requestParameterMap = getRequestParameterMap();
            String konzeptId = requestParameterMap.get("konzeptId");
            return prozessbaukastenImageService.getKonzeptImage(konzeptId);
        }
    }

    private static Map<String, String> getRequestParameterMap() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        return externalContext.getRequestParameterMap();
    }

    private static boolean pageIsRendering() {
        FacesContext context = FacesContext.getCurrentInstance();
        return context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE;
    }

    private StreamedContent getDefaultContent() {
        return new DefaultStreamedContent();
    }

    public StreamedContent getImageForAnforderung(Integer large) {

        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // case page is rendering. return dummy content.
            return new DefaultStreamedContent();
        } else {
            String objectId = context.getExternalContext().getRequestParameterMap().get("objectId");
            byte[] image = null;

            if (objectId != null) {
                // try to load image for anforderung or meldung
                image = imageService.getImageFromBildByObjectIdAndParameter(Long.parseLong(objectId), large, "A");
                if (image == null) {
                    // if image is null try to load default image for modul for anforderung
                    image = imageService.getDefaultImageForModulByAnforderungIdAndParameter(Long.parseLong(objectId), large);
                }
            }
            if (image == null) {
                // if image is still null load the general default image
                image = imageService.loadDefaultImage();
            }

            return new DefaultStreamedContent(new ByteArrayInputStream(image));
        }

    }

    public StreamedContent getImageForAnforderungMeldungPzbk() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // case page is rendering. return dummy content.
            return new DefaultStreamedContent();
        } else {
            // the browser is requesting the image. return real content.
            String objectId = context.getExternalContext().getRequestParameterMap().get("objectId");
            String parameter = context.getExternalContext().getRequestParameterMap().get("parameter");
            String type = context.getExternalContext().getRequestParameterMap().get("type");
            byte[] image = null;

            if (objectId != null && !objectId.isEmpty() && parameter != null && !parameter.isEmpty()) {
                // try to load image for anforderung or meldung
                image = imageService.getImageFromBildByObjectIdAndParameter(Long.parseLong(objectId), Integer.parseInt(parameter), type);

                if (image == null && "A".equals(type)) {
                    // if image is null try to load default image for modul for anforderung
                    image = imageService.getDefaultImageForModulByAnforderungIdAndParameter(Long.parseLong(objectId), Integer.parseInt(parameter));
                }
            }

            if (image == null) {
                // if image is still null load the general default image
                image = imageService.loadDefaultImage();
            }

            return new DefaultStreamedContent(new ByteArrayInputStream(image));
        }
    }

    public StreamedContent getImageForSearchResult(SearchResultDTO searchResult) throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // case page is rendering. return dummy content.
            return new DefaultStreamedContent();
        } else if (searchResult != null) {
            byte[] image;
            // the browser is requesting the image. return real content.
            if (searchResult.getImage() == null) {
                image = imageService.loadDefaultImage();
            } else {
                image = searchResult.getImage();
            }
            return new DefaultStreamedContent(new ByteArrayInputStream(image));
        }
        return new DefaultStreamedContent();
    }

    public StreamedContent getAnhangContentLoaded() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        } else {
            // So, browser is requesting the image. Return a real StreamedContent with the image bytes.
            String anhangId = context.getExternalContext().getRequestParameterMap().get("anhangId");
            if (anhangId != null && !"".equals(anhangId)) {
                Anhang selectedAnhang = fileService.getAnhangById(Long.parseLong(anhangId));
                if (selectedAnhang != null) {
                    return imageService.downloadAnhang(selectedAnhang);
                }
            }
        }

        return new DefaultStreamedContent();
    }

}
