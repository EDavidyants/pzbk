package de.interfaceag.bmw.pzbk.shared.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author Christian Schauer
 */
public final class FileUtils {

    private static final Logger LOG = LoggerFactory.getLogger(FileUtils.class);

    private FileUtils() {
    }

    public static String inputStreamToUtf8String(InputStream is) {
        StringBuilder output = new StringBuilder();
        String line;

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            while ((line = br.readLine()) != null) {
                output.append(line);
            }
        } catch (IOException ex) {
            LOG.error(null, ex);
        }

        return output.toString();
    }

}
