package de.interfaceag.bmw.pzbk.filter;

import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @param <T>
 */
public interface SingleValueEnumSearchFilter<T> extends SingleValueSearchFilter {

    Optional<T> getAsEnum();

}
