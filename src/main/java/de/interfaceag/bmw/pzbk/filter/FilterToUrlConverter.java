package de.interfaceag.bmw.pzbk.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.stream.Stream;

/**
 *
 * @author sl
 */
public final class FilterToUrlConverter {

    private static final Logger LOG = LoggerFactory.getLogger(FilterToUrlConverter.class);

    private final Object filterImplementation;

    private FilterToUrlConverter(Object filterImplementation) {
        this.filterImplementation = filterImplementation;
    }

    public static FilterToUrlConverter forClass(Object filterImplementation) {
        return new FilterToUrlConverter(filterImplementation);
    }

    public String getUrl() {
        StringBuilder sb = new StringBuilder("?faces-redirect=true");

        Iterator<Field> iterator = getFilterIterator();
        while (iterator.hasNext()) {
            Field field = iterator.next();
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            BasicFilter filter;
            try {
                filter = (BasicFilter) field.get(filterImplementation);
                String parameter = filter.getIndependentParameter();
                sb.append(parameter);
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                LOG.error(null, ex);
            }
        }

        return sb.toString();

    }

    private Iterator<Field> getFilterIterator() {
        Class<?> aClass = filterImplementation.getClass();
        Field[] declaredFields = aClass.getDeclaredFields();
        Stream<Field> fields = Stream.of(declaredFields);
        Stream<Field> filters = fields.filter(field -> field.isAnnotationPresent(Filter.class));
        return filters.iterator();
    }

}
