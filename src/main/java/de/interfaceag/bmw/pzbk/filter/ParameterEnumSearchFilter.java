package de.interfaceag.bmw.pzbk.filter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @param <T>
 */
public interface ParameterEnumSearchFilter<T> extends MultiValueEnumSearchFilter<T> {

    String getParamterName();

}
