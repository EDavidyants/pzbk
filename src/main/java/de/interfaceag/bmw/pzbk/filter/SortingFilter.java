package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.enums.Sorting;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class SortingFilter extends AbstractEnumFilter<Sorting> {

    public static final String PARAMETERNAME = "sorting";

    public SortingFilter(UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForSorting());
        Optional<SearchFilterObject> sortNameDesc = this.getAll().stream().filter(sfo -> sfo.getId().equals(3L)).findAny();
        if (sortNameDesc.isPresent()) {
            super.setSelectedValues(new HashSet<>(Arrays.asList(sortNameDesc.get())));
            super.setSelectedValue(sortNameDesc.get());
        } else {
            super.setSelectedValues(new HashSet<>());
        }
        super.parseParameter(urlParameter);
    }

    @Override
    public List<Sorting> getAsEnumList() {
        return getSelectedValues().stream()
                .map(selected -> Sorting.getById(Math.toIntExact(selected.getId())))
                .filter(optional -> optional.isPresent())
                .map(optional -> optional.get())
                .collect(Collectors.toList());
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.SORTING;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    @Override
    public Optional<Sorting> getAsEnum() {
        if (getSelectedValueAsOptional().isPresent()) {
            return Sorting.getById(getSelectedValueAsOptional().get().getId().intValue());
        }
        return Optional.empty();
    }

}
