package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class SingleSnapshotFilter extends AbstractSingleIdFilter {

    public static final String PARAMETERNAME = "snapshot";

    public SingleSnapshotFilter(Collection<SnapshotFilter> allSnapshots, UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForSnapshotFilter(allSnapshots));
        super.parseParameter(urlParameter);
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.SNAPSHOT;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
