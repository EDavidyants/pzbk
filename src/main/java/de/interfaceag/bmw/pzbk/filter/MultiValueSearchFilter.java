package de.interfaceag.bmw.pzbk.filter;

import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @param <T>
 */
public interface MultiValueSearchFilter<T> extends SearchFilter<T> {

    void setSelectedValues(Set<T> selected);

    Set<T> getSelectedValues();

}
