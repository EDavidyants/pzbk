package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.converters.SearchFilterObjectConverter;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.faces.convert.Converter;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 *
 * @author fp
 */
public class FachIdSearchFilter implements MultiValueSearchFilter<SearchFilterObject> {

    private String value = "";

    public static final String PARAMETERNAME = "fachId";

    private final List<SearchFilterObject> all;
    private Set<SearchFilterObject> selected = new HashSet<>();

    public FachIdSearchFilter(UrlParameter urlParameter) {
        if (selected.size() == 1) {
            SearchFilterObject next = selected.iterator().next();
            this.setValue(next.getName());
        } else {
            this.selected.clear();
        }

        this.all = SearchFilterObjectFactory.getSearchFilterObjectsForText();
        this.parseParameter(urlParameter);
    }

    @Override
    public boolean isActive() {
        return !this.value.isEmpty();
    }

    @Override
    public List<SearchFilterObject> getAll() {
        return all;
    }

    @Override
    public Set<SearchFilterObject> getSelectedValues() {
        return selected;
    }

    @Override
    public void setSelectedValues(Set<SearchFilterObject> selected) {
        this.selected = selected;
    }

    @Override
    public Converter getConverter() {
        return new SearchFilterObjectConverter(all);
    }

    @Override
    public String getIndependentParameter() {
        if (selected.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&" + getParameterName() + "=");
        selected.forEach(s -> sb.append(s.getName()).append(","));
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    @Override
    public String getParameter() {
        if (selected.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder(getParameterName() + "-");
        selected.forEach(s -> sb.append(s.getId().toString()).append(","));
        sb.deleteCharAt(sb.length() - 1);
        sb.append("_");
        return sb.toString();
    }

    protected final void parseParameter(UrlParameter urlParameter) {
        Optional<String> parameter = urlParameter.getValue(getParameterName());
        if (parameter.isPresent()) {
            parseParameter(parameter.get());
        }
    }

    @Override
    public FachIdSearchFilter parseParameter(String parameter) {
        if (parameter != null) {
            String[] paramters = parameter.split(",");
            StringBuilder sb = new StringBuilder();
            for (String p : paramters) {
                sb.append(p).append(" ");
            }
            setValue(sb.toString());
        }
        return this;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.SUCHTEXT;
    }

    public final void setValue(String value) {
        getSelectedValues().clear();
        String[] values = value.split(" ");
        for (String v : values) {
            getSelectedValues().add(new SearchFilterObject(0L, v));
        }

        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getAsString() {
        String result = "";
        result = getSelectedValues().stream().map((selectedValue) -> selectedValue.getName()).reduce(result, String::concat);
        return result;
    }

}
