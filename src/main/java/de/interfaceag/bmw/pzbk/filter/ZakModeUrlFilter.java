package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ZakModeUrlFilter extends AbstractBooleanUrlFilter {

    public ZakModeUrlFilter(UrlParameter urlParameter) {
        super();
        parseParameter(urlParameter);
    }

    @Override
    public String getParameterName() {
        return "zakMode";
    }

    @Override
    public void setActive() {
        super.setActive(true);
        super.setAttributeValue("true");
    }

    @Override
    public void setInactive() {
        super.setActive(false);
        super.setAttributeValue("false");
    }

    @Override
    public final void setAttributeValue(String value) {
        super.setAttributeValue(value);
        super.setActive(true);
    }

}
