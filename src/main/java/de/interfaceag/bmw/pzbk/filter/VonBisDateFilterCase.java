package de.interfaceag.bmw.pzbk.filter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public enum VonBisDateFilterCase {

    VON, BIS, VONBIS, NONE;

}
