package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.converters.SearchFilterObjectConverter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.faces.convert.Converter;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @param <T>
 */
public abstract class AbstractEnumFilter<T> implements MultiValueEnumSearchFilter<T>, SingleValueEnumSearchFilter<T> {

    private List<SearchFilterObject> allStatus;
    private Set<SearchFilterObject> selectedStatusValues;
    private SearchFilterObject selectedStatusValue;

    @Override
    public final boolean isActive() {
        return !this.selectedStatusValues.isEmpty();
    }

    protected void setAll(List<SearchFilterObject> allStatus) {
        this.allStatus = allStatus;
    }

    @Override
    public final List<SearchFilterObject> getAll() {
        return allStatus;
    }

    @Override
    public final Set<SearchFilterObject> getSelectedValues() {
        return selectedStatusValues;
    }

    @Override
    public final void setSelectedValues(Set<SearchFilterObject> selected) {
        this.selectedStatusValues = selected;
    }

    @Override
    public final Optional<SearchFilterObject> getSelectedValueAsOptional() {
        return Optional.ofNullable(selectedStatusValue);
    }

    @Override
    public final SearchFilterObject getSelectedValue() {
        return selectedStatusValue;
    }

    @Override
    public final void setSelectedValue(SearchFilterObject selected) {
        this.selectedStatusValue = selected;
        this.selectedStatusValues.clear();
        this.selectedStatusValues.add(selected);
    }

    @Override
    public final Converter getConverter() {
        return new SearchFilterObjectConverter(allStatus);
    }

    @Override
    public final String getIndependentParameter() {
        if (selectedStatusValues.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&" + getParameterName() + "=");
        selectedStatusValues.forEach(s -> sb.append(s.getId().toString()).append(","));
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    @Override
    public final String getParameter() {
        if (selectedStatusValues.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder(getParameterName() + "-");
        selectedStatusValues.forEach(s -> sb.append(s.getId().toString()).append(","));
        sb.deleteCharAt(sb.length() - 1);
        sb.append("_");
        return sb.toString();
    }

    protected final void parseParameter(UrlParameter urlParameter) {
        Optional<String> parameter = urlParameter.getValue(getParameterName());
        if (parameter.isPresent()) {
            parseParameter(parameter.get());
        }
    }

    @Override
    public final MultiValueEnumSearchFilter<T> parseParameter(String parameter) {
        if (parameter != null) {
            String[] paramters = parameter.split(",");
            setSelectedValues(Stream.of(paramters)
                    .map(p -> (String) p)
                    .filter(p -> p.matches("\\d+"))
                    .map(p -> allStatus.stream().filter(s -> s.getId().toString().equals(p)).findAny())
                    .filter(p -> p.isPresent())
                    .map(p -> p.get())
                    .collect(Collectors.toSet()));

            if (paramters.length == 1) {
                Optional<SearchFilterObject> value = allStatus.stream().filter(s -> s.getId().toString().equals(paramters[0])).findAny();
                if (value.isPresent()) {
                    setSelectedValue(value.get());
                }
            }
        }
        return this;
    }

}
