package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractDateFilter implements DateSearchFilter {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractDateFilter.class);
    private static final String DATE_PATTERN = "dd.MM.yyyy";

    private Date date;
    private boolean active;

    public void setDate(Date date) {
        if (date != null) {
            this.date = new Date(date.getTime());
        } else {
            this.date = null;
        }
    }

    @Override
    public Date getSelected() {
        return this.date != null ? new Date(this.date.getTime()) : null;
    }

    @Override
    public void setSelected(Date selected) {
        if (selected != null) {
            this.date = new Date(selected.getTime());
            this.active = Boolean.TRUE;
        }
    }

    @Override
    public String getIndependentParameter() {
        if (!active) {
            return "";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        String dateAsString = dateFormat.format(this.date);

        return "&" + getParameterName() + "=" + dateAsString;
    }

    @Override
    public String getParameter() {
        if (!active) {
            return "";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        String dateAsString = dateFormat.format(this.date);

        return getParameterName() + "-" + dateAsString + "_";
    }

    protected final void parseParameter(UrlParameter urlParameter) {
        Optional<String> parameter = urlParameter.getValue(getParameterName());
        parameter.ifPresent(this::parseParameter);
    }

    // TODO: remove! Optional should not be used as parameter!!
    public DateSearchFilter parseParameter(Optional<String> parameter) {
        if (parameter.isPresent()) {
            return parseParameter(parameter.get());
        }
        return this;
    }

    @Override
    public DateSearchFilter parseParameter(String parameter) {
        if (parameter != null && parameter.matches("\\d+[.]\\d+[.]\\d+")) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
                this.date = sdf.parse(parameter);
                this.active = Boolean.TRUE;
            } catch (ParseException ex) {
                LOG.error(null, ex);
            }
        }
        return this;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(boolean isActive) {
        this.active = isActive;
    }

}
