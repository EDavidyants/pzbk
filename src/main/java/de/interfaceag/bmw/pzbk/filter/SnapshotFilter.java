package de.interfaceag.bmw.pzbk.filter;

/**
 *
 * @author sl
 */
public interface SnapshotFilter {

    Long getId();

    String getDate();

}
