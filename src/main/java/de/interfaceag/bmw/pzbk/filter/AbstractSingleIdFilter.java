package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.converters.SearchFilterObjectConverter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import javax.faces.convert.Converter;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractSingleIdFilter implements SingleIdSearchFilter {

    private List<SearchFilterObject> all;
    private SearchFilterObject selected;

    @Override
    public List<SearchFilterObject> getAll() {
        return all;
    }

    public void setAll(List<SearchFilterObject> all) {
        this.all = all;
    }

    @Override
    public SearchFilterObject getSelectedValue() {
        return selected;
    }

    @Override
    public void setSelectedValue(SearchFilterObject selected) {
        this.selected = selected;
    }

    @Override
    public boolean isActive() {
        return this.getSelectedValue() != null;
    }

    @Override
    public Converter getConverter() {
        return new SearchFilterObjectConverter(getAll());
    }

    @Override
    public String getIndependentParameter() {
        return getParameter();
    }

    @Override
    public String getParameter() {
        if (!isActive()) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&" + getParameterName() + "=");
        sb.append(selected.getId());
        return sb.toString();
    }

    protected final void parseParameter(UrlParameter urlParameter) {
        Optional<String> parameter = urlParameter.getValue(getParameterName());
        if (parameter.isPresent()) {
            parseParameter(parameter.get());
        }
    }

    @Override
    public SingleIdSearchFilter parseParameter(String parameter) {
        if (parameter != null && !parameter.isEmpty() && RegexUtils.matchesId(parameter)) {
            Long id = Long.parseLong(parameter);
            getAll().stream().filter(s -> s.getId().equals(id)).findAny()
                    .ifPresent(s -> setSelectedValue(s));
        }
        return this;
    }

    @Override
    public OptionalLong getSelectedId() {
        if (this.selected != null) {
            return OptionalLong.of(selected.getId());
        } else {
            return OptionalLong.empty();
        }
    }

    @Override
    public Optional<SearchFilterObject> getSelectedValueAsOptional() {
        return Optional.ofNullable(selected);
    }

}
