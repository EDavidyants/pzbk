package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author fn
 */
public class RolleFilter extends AbstractEnumFilter<Rolle> {

    public static final String PARAMETERNAME = "rolle";

    public RolleFilter(UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForRolle());
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    public RolleFilter(UrlParameter urlParameter, List<Rolle> rollen) {
        super.setAll(getSearchFilterObjectForRolls(rollen));
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    private List<SearchFilterObject> getSearchFilterObjectForRolls(List<Rolle> rollen) {
        return rollen.stream()
                .map(rolle -> new SearchFilterObject(Long.valueOf(rolle.getId()), rolle.getBezeichnung()))
                .collect(Collectors.toList());
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.ROLLE;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    @Override
    public List<Rolle> getAsEnumList() {
        return getSelectedValues().stream()
                .map(r -> Rolle.getRolleById(Math.toIntExact(r.getId())))
                .collect(Collectors.toList());

    }

    @Override
    public Optional<Rolle> getAsEnum() {
        if (getSelectedValueAsOptional().isPresent()) {
            return Optional.of(Rolle.getRolleById(Math.toIntExact(getSelectedValueAsOptional().get().getId().intValue())));
        }
        return Optional.empty();
    }

}
