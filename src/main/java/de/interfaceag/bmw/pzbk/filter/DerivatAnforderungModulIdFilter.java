package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author evda
 */
public class DerivatAnforderungModulIdFilter extends AbstractUrlFilter {

    public static final String PARAMETERNAME = "daid";

    public DerivatAnforderungModulIdFilter(UrlParameter urlParameter) {
        super();
        parseParameter(urlParameter);
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    @Override
    public void setAttributeValue(String value) {
        super.setAttributeValue(value);
        super.setActive(true);
    }

    public void setAttributeValue(Set<Long> ids) {
        super.setAttributeValue(idsToString(ids));
    }

    private static String idsToString(Set<Long> ids) {
        if (ids != null) {
            return ids.stream().map(id -> id.toString()).collect(Collectors.joining(","));
        }
        return "";
    }

    public Set<Long> getIds() {
        return stringToIds(getAttributeValue());
    }

    private static Set<Long> stringToIds(String inputString) {
        Set<Long> result = new HashSet<>();
        if (inputString != null) {
            if (inputString.contains(",")) {
                Stream.of(inputString.split(","))
                        .filter(value -> value != null && RegexUtils.matchesId(value.trim()))
                        .forEach(value -> result.add(Long.parseLong(value.trim())));
            } else if (inputString.contains("+")) {
                Stream.of(inputString.split("\\+"))
                        .filter(value -> value != null && RegexUtils.matchesId(value.trim()))
                        .forEach(value -> result.add(Long.parseLong(value.trim())));
            } else if (inputString.contains(" ")) {
                Stream.of(inputString.split(" "))
                        .filter(value -> value != null && RegexUtils.matchesId(value))
                        .forEach(value -> result.add(Long.parseLong(value)));
            } else if (RegexUtils.matchesId(inputString.trim())) {
                result.add(Long.parseLong(inputString.trim()));
            }
        }
        return result;
    }

}
