package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Werk;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.Kategorie;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.ReferenzSystem;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Sorting;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.shared.utils.PhasenbezugUtils;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungZakStatusUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public final class SearchFilterObjectFactory {

    private SearchFilterObjectFactory() {
    }

    public static List<SearchFilterObject> getSearchFilterObjectsForSensorCoc(Collection<SensorCoc> sensorCocs) {
        if (sensorCocs != null) {
            return sensorCocs.stream()
                    .filter(s -> s.getSensorCocId() != null && s.pathToStringShort() != null)
                    .map(s -> new SearchFilterObject(s.getSensorCocId(), s.pathToStringShort()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterNameObject> getSearchFilterObjectsForFachbereich(List<String> fachbereiche) {
        if (fachbereiche != null) {
            return fachbereiche.stream()
                    .map(SearchFilterNameObject::new)
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterObject> getSearchFilterObjectsForProzessbaukasten(Collection<ProzessbaukastenFilterDto> prozessbaukaesten) {
        if (prozessbaukaesten != null) {
            return prozessbaukaesten.stream()
                    .filter(prozessbaukasten -> prozessbaukasten.getId() != null)
                    .map(prozessbaukasten -> new SearchFilterObject(prozessbaukasten.getId(), prozessbaukasten.toString()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterNameObject> getSearchFilterObjectsForSensorCocTechnologie(Collection<SensorCoc> sensorCocs) {
        if (sensorCocs != null) {
            return sensorCocs.stream()
                    .filter(s -> s.getSensorCocId() != null && s.pathToStringShort() != null)
                    .map(SensorCoc::getOrtung)
                    .distinct()
                    .map(SearchFilterNameObject::new)
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterObject> getSearchFilterObjectsForWerk(Collection<Werk> werke) {
        if (werke != null) {
            return werke.stream()
                    .filter(s -> s.getId() != null && s.getName() != null)
                    .map(s -> new SearchFilterObject(s.getId(), s.getName()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterObject> getSearchFilterObjectsForGenericIdList(Collection<Long> ids) {
        if (ids != null) {
            return ids.stream()
                    .filter(Objects::nonNull)
                    .map(id -> new SearchFilterObject(id, Long.toString(id)))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public static List<SearchFilterObject> getSearchFilterObjectsForTteams(Collection<Tteam> tteams) {
        if (tteams != null) {
            return tteams.stream()
                    .filter(tteam -> tteam.getId() != null && tteam.getTeamName() != null)
                    .map(tteam -> new SearchFilterObject(tteam.getId(), tteam.getTeamName()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterObject> getSearchFilterObjectsForFestgestelltIn(Collection<FestgestelltIn> festgestelltInList) {
        if (festgestelltInList != null) {
            return festgestelltInList.stream()
                    .filter(festgestelltIn -> festgestelltIn.getId() != null && festgestelltIn.getWert() != null)
                    .map(festgestelltIn -> new SearchFilterObject(festgestelltIn.getId(), festgestelltIn.getWert()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterObject> getSearchFilterObjectsForThemenklammern(Collection<ThemenklammerDto> themenklammern) {
        if (themenklammern != null) {
            return themenklammern.stream()
                    .filter(themenklammer -> themenklammer.getId() != null && themenklammer.getBezeichnung() != null)
                    .map(themenklammer -> new SearchFilterObject(themenklammer.getId(), themenklammer.getBezeichnung()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterObject> getSearchFilterObjectsForModul(Collection<Modul> module) {
        if (module != null) {
            return module.stream()
                    .filter(m -> m.getId() != null && m.getName() != null)
                    .map(m -> new SearchFilterObject(m.getId(), m.getName()))
                    .sorted()
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterObject> getSearchFilterObjectsForModulSeTeams(List<ModulSeTeam> modulSeTeams) {
        if (modulSeTeams != null) {
            return modulSeTeams.stream()
                    .filter(m -> m.getId() != null && m.getName() != null)
                    .map(m -> new SearchFilterObject(m.getId(), m.getName()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterObject> getSearchFilterObjectsForDerivatDtos(Collection<DerivatDto> derivate) {
        if (derivate != null) {
            return derivate.stream()
                    .filter(d -> d.getId() != null && d.getName() != null)
                    .map(d -> new SearchFilterObject(d.getId(), d.getName()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterObject> getSearchFilterObjectsForDerivate(Collection<Derivat> derivate) {
        if (derivate != null) {
            return derivate.stream()
                    .filter(d -> d.getId() != null && d.getName() != null)
                    .map(d -> new SearchFilterObject(d.getId(), d.getName()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterObject> getSearchFilterObjectsForSnapshotFilter(Collection<SnapshotFilter> snapshots) {
        if (snapshots != null) {
            return snapshots.stream()
                    .filter(snapshot -> snapshot.getId() != null && snapshot.getDate() != null)
                    .map(d -> new SearchFilterObject(d.getId(), d.getDate()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterObject> getSearchFilterObjectsForPhasenbezug() {
        return PhasenbezugUtils.getAllPhasenbezug()
                .map(phasenbezug -> new SearchFilterObject(phasenbezug.getValue(), phasenbezug.getBeschreibung()))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForBoolean() {
        List<SearchFilterObject> result = new ArrayList<>();
        result.add(new SearchFilterObject(true, ""));
        result.add(new SearchFilterObject(false, ""));
        return result;
    }

    static List<SearchFilterObject> getSearchFilterObjectsForAbteilung(List<String> abteilung) {
        return abteilung.stream()
                .filter(a -> a != null && !a.isEmpty())
                .map(SearchFilterObject::new)
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForZAKEinordnung(List<String> abteilung) {
        return abteilung.stream()
                .filter(a -> a != null && !a.isEmpty())
                .map(SearchFilterObject::new)
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForText() {
        return Collections.emptyList();
    }

    static List<SearchFilterObject> getSearchFilterObjectsForZuordnungZakStatus() {
        return ZuordnungZakStatusUtils.getAllZuordnungZakStatus()
                .map(zuordnungZakStatus -> new SearchFilterObject(zuordnungZakStatus.getValue(), zuordnungZakStatus.getBeschreibung()))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForMeldungStatus() {
        return Status.selectStatusesOfMeldung().stream()
                .map(status -> new SearchFilterObject(Long.valueOf(status.getStatusId()), status.getStatusBezeichnung()))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForAnforderungStatus() {
        return Status.getStatusValuesForGlobalAnforderungFilter().stream()
                .map(status -> new SearchFilterObject(Long.valueOf(status.getStatusId()), status.getStatusBezeichnung()))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForProzessbaukastenStatus() {
        return Stream.of(ProzessbaukastenStatus.values())
                .map(status -> new SearchFilterObject(Long.valueOf(status.getId()),
                LocalizationService.getValue(Locale.GERMAN, status.getLocalizationKey())))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForRolle() {
        return Rolle.getAllRolles().stream()
                .map(rolle -> new SearchFilterObject(Long.valueOf(rolle.getId()), rolle.getBezeichnung()))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForSorting() {
        return Stream.of(Sorting.values())
                .map(sort -> new SearchFilterObject(Long.valueOf(sort.getId()), sort.getPropertyName()))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForType() {
        return Type.getActiveFilters().stream()
                .map(type -> new SearchFilterObject(Long.valueOf(type.getId()), type.getBezeichnung()))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForReferenzSystem() {
        return Stream.of(ReferenzSystem.values())
                .map(referenzSystem -> new SearchFilterObject(Long.valueOf(referenzSystem.getId()), referenzSystem.toString()))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForKategorie() {
        return Stream.of(Kategorie.values())
                .map(kategorie -> new SearchFilterObject(Long.valueOf(kategorie.getId()), kategorie.toString()))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForZuordnungStatus() {
        return ZuordnungStatus.getAll()
                .stream()
                .map(s -> new SearchFilterObject(Long.valueOf(s.getId()), s.getStatusBezeichnung()))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForKovaStatus() {
        return Stream.of(KovAStatus.values())
                .map(s -> new SearchFilterObject(Long.valueOf(s.getStatusId()), s.getStatusBezeichnung()))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForKovaPhase(List<KovAPhase> kovaPhasen) {
        if (kovaPhasen != null) {
            return kovaPhasen.stream()
                    .map(k -> new SearchFilterObject(Long.valueOf(k.getId()), k.getBezeichnung()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<SearchFilterObject> getSearchFilterObjectsForVereinbarungStatus() {
        return DerivatAnforderungModulStatus.getAllStatusListForVereinbarung()
                .stream()
                .map(s -> new SearchFilterObject(Long.valueOf(s.getStatusId()), s.getStatusBezeichnung()))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForZakStatus() {
        return ZakStatus.getAllStatusList()
                .stream()
                .map(s -> new SearchFilterObject(Long.valueOf(s.getStatusId()), s.getStatusBezeichnung()))
                .collect(Collectors.toList());
    }

    static List<SearchFilterObject> getSearchFilterObjectsForUmsetzungsBestaetigungStatus() {
        return UmsetzungsBestaetigungStatus.getFilterStatusList()
                .stream()
                .map(s -> new SearchFilterObject(Long.valueOf(s.getId()), s.getStatusBezeichnung()))
                .collect(Collectors.toList());
    }

}
