package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface UrlFilter extends Serializable, BasicFilter {

    void setAttributeValue(String attributeValue);

    String getAttributeValue();

    String getParameterName();

    void parseParameter(UrlParameter urlParameter);

    boolean isActive();

}
