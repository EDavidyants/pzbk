package de.interfaceag.bmw.pzbk.filter;

import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface BooleanSearchFilter extends MultiValueSearchFilter<SearchFilterObject> {

    List<Boolean> getAsBoolean();

}
