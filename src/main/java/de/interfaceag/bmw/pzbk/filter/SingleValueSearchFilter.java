package de.interfaceag.bmw.pzbk.filter;

import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface SingleValueSearchFilter extends SearchFilter<SearchFilterObject> {

    Optional<SearchFilterObject> getSelectedValueAsOptional();

    SearchFilterObject getSelectedValue();

    void setSelectedValue(SearchFilterObject selected);

}
