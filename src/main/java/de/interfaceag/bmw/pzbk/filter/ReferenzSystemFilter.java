package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.ReferenzSystem;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ReferenzSystemFilter extends AbstractEnumFilter<ReferenzSystem> {

    public static final String PARAMETERNAME = "referenzSystemFilter";

    public ReferenzSystemFilter(UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForReferenzSystem());
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    @Override
    public List<ReferenzSystem> getAsEnumList() {
        return getSelectedValues().stream()
                .map(selected -> ReferenzSystem.getById(Math.toIntExact(selected.getId())))
                .filter(optional -> optional != null)
                .collect(Collectors.toList());
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.TYPE;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    @Override
    public Optional<ReferenzSystem> getAsEnum() {
        if (getSelectedValueAsOptional().isPresent()) {
            return Optional.ofNullable(ReferenzSystem.getById(getSelectedValueAsOptional().get().getId().intValue()));
        }
        return Optional.empty();
    }

}
