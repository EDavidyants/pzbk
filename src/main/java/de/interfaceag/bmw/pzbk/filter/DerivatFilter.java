package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DerivatFilter extends AbstractIdFilter {

    public static final String PARAMETERNAME = "derivatFilter";

    public DerivatFilter(Collection<Derivat> allDerivate, UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForDerivate(allDerivate));
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    private DerivatFilter() {
        super.setSelectedValues(new HashSet<>());
    }

    public static DerivatFilter forDerivatDtos(Collection<DerivatDto> allDerivate, UrlParameter urlParameter) {
        DerivatFilter filter = new DerivatFilter();
        List<SearchFilterObject> all = SearchFilterObjectFactory.getSearchFilterObjectsForDerivatDtos(allDerivate);
        filter.setAll(all);
        filter.parseParameter(urlParameter);
        return filter;
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.DERIVAT;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
