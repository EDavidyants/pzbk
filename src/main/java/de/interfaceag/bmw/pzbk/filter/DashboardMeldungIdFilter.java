package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DashboardMeldungIdFilter extends AbstractIdFilter {

    public static final String PARAMETERNAME = "dashboardMeldungIdFilter";

    public DashboardMeldungIdFilter(Collection<Long> meldungIds) {
        List<SearchFilterObject> searchFilterObjectsForGenericIdList = SearchFilterObjectFactory.getSearchFilterObjectsForGenericIdList(meldungIds);
        super.setAll(searchFilterObjectsForGenericIdList);
        super.setSelectedValues(new HashSet<>(searchFilterObjectsForGenericIdList));
    }

    public DashboardMeldungIdFilter() {
        super.setAll(new ArrayList<>());
        super.setSelectedValues(new HashSet<>());
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.DASHBOARD_MELDUNG;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
