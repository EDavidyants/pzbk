package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.Kategorie;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class KategorieFilter extends AbstractEnumFilter<Kategorie> {

    public static final String PARAMETERNAME = "kategorieFilter";

    public KategorieFilter(UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForKategorie());
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    @Override
    public List<Kategorie> getAsEnumList() {
        return getSelectedValues().stream()
                .map(selected -> Kategorie.getById(Math.toIntExact(selected.getId())))
                .filter(optional -> optional != null)
                .collect(Collectors.toList());
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.TYPE;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    @Override
    public Optional<Kategorie> getAsEnum() {
        if (getSelectedValueAsOptional().isPresent()) {
            return Optional.ofNullable(Kategorie.getById(getSelectedValueAsOptional().get().getId().intValue()));
        }
        return Optional.empty();
    }

}
