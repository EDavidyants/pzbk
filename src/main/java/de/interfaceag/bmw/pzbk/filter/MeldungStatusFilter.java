package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class MeldungStatusFilter extends AbstractEnumFilter<Status> {

    public static final String PARAMETERNAME = "meldungStatusFilter";


    public MeldungStatusFilter(Collection<Status> activeValues) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForMeldungStatus());
        Collection<Long> statusIds = activeValues.stream().map(Status::getStatusId).map(Integer::longValue).collect(Collectors.toSet());
        final Set<SearchFilterObject> selectedvalues = super.getAll().stream().filter(value -> statusIds.contains(value.getId())).collect(Collectors.toSet());
        super.setSelectedValues(selectedvalues);
    }

    public MeldungStatusFilter(UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForMeldungStatus());
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    @Override
    public List<Status> getAsEnumList() {
        return getSelectedValues().stream()
                .map(s -> Status.getStatusById(Math.toIntExact(s.getId())))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Status> getAsEnum() {
        if (getSelectedValueAsOptional().isPresent()) {
            return Optional.of(Status.getStatusById(Math.toIntExact(getSelectedValueAsOptional().get().getId().intValue())));
        }
        return Optional.empty();
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.STATUS;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
