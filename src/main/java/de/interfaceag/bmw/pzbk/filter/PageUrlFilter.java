package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.Min;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class PageUrlFilter extends AbstractUrlFilter {

    public static final Logger LOG = LoggerFactory.getLogger(PageUrlFilter.class);

    @Min(0)
    private int pageId;

    public PageUrlFilter(UrlParameter urlParameter) {
        super();
        parseParameter(urlParameter);
    }

    @Override
    public String getParameterName() {
        return "page";
    }

    public void setAttributeValue(int pageId) {
        this.pageId = pageId;
        super.setAttributeValue(Integer.toString(pageId));
    }

    public int getPageId() {
        if (getAttributeValue() == null) {
            return 0;
        }
        try {
            return Integer.parseInt(getAttributeValue());
        } catch (NumberFormatException exception) {
            LOG.error("Could not parse number", exception);
            return pageId;
        }
    }

    public void incrementPageId() {
        pageId = getPageId();
        pageId++;
        setAttributeValue(pageId);
        setActive(Boolean.TRUE);
    }

    public void decrementPageId() {
        int pageId = getPageId();
        if (pageId > 1) {
            pageId--;
        } else {
            pageId = 0;
        }
        setAttributeValue(pageId);
        setActive(Boolean.TRUE);
    }


}
