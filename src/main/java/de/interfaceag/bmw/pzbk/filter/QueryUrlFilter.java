package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.globalsearch.SearchResultDTO;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.SearchUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class QueryUrlFilter extends AbstractUrlFilter {

    public static final String PARAMETERNAME = "query";

    public QueryUrlFilter(String query) {
        super();
        setAttributeValue(query);
    }

    public QueryUrlFilter(UrlParameter urlParameter) {
        super();
        parseParameter(urlParameter);
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    @Override
    public final void setAttributeValue(String value) {
        super.setAttributeValue(value);
        super.setActive(true);
    }

    public List<SearchResultDTO> getQueries() {
        return getAttributeValues().stream().map(query -> new SearchResultDTO(query)).collect(Collectors.toList());
    }

    public void setQueries(List<SearchResultDTO> queries) {
        if (queries != null) {
            setAttributeValue(queries.stream().map(searchResultDto -> searchResultDto.getName()).collect(Collectors.toList()));
        } else {
            setAttributeValue(new ArrayList<>());
            setActive(Boolean.FALSE);
        }
    }

    public void setAttributeValue(List<String> values) {
        setAttributeValue(valuesToString(values));
    }

    public List<String> getAttributeValues() {
        return stringToValues(getAttributeValue());
    }

    private static String valuesToString(List<String> values) {
        if (values != null) {
            return values.stream().collect(Collectors.joining(","));
        }
        return "";
    }

    private static List<String> stringToValues(String value) {
        List<String> result = new ArrayList<>();
        if (value != null) {
            if (value.contains(",")) {
                Stream.of(value.split(",")).forEach(splitEntry -> result.add(splitEntry));
            } else {
                result.add(value);
            }
        }
        List<String> intelligentQueries = result.stream().map(query -> SearchUtil.splitQueryWithTextRecognition(query))
                .flatMap(list -> list.stream()).map(query -> query.trim()).filter(query -> !query.isEmpty()).collect(Collectors.toList());

        return intelligentQueries;
    }
}
