package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class PhasenrelevanzFilter extends AbstractBooleanFilter {

    public static final String PARAMETERNAME = "vereinbarungStatusFilter";

    public PhasenrelevanzFilter(UrlParameter urlParameter) {
        super(urlParameter, SearchFilterObjectFactory.getSearchFilterObjectsForPhasenbezug());
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.RELEVANZ;
    }

}
