package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ErstellungsdatumVonBisDateFilter extends AbstractVonBisDateFilter {

    public static final String PARAMETERPRAEFIX = "erstellungsdatum";

    public ErstellungsdatumVonBisDateFilter(UrlParameter urlParameter) {
        super(PARAMETERPRAEFIX, urlParameter);
    }

}
