package de.interfaceag.bmw.pzbk.filter.pages;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.filter.BisDateFilter;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.filter.DerivatFilter;
import de.interfaceag.bmw.pzbk.filter.Filter;
import de.interfaceag.bmw.pzbk.filter.FilterToUrlConverter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.KovaPhaseFilter;
import de.interfaceag.bmw.pzbk.filter.KovaStatusFilter;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.VonDateFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class KovAPhasenViewFilter implements Serializable {

    public static final String BASEURL = "kovaPhasen.xhtml";

    @Filter
    private final MultiValueEnumSearchFilter statusFilter;
    @Filter
    private final IdSearchFilter derivatFilter;
    @Filter
    private final IdSearchFilter phaseFilter;
    @Filter
    private final DateSearchFilter vonStartDateFilter;
    @Filter
    private final DateSearchFilter bisStartDateFilter;
    @Filter
    private final DateSearchFilter vonEndDateFilter;
    @Filter
    private final DateSearchFilter bisEndDateFilter;

    public KovAPhasenViewFilter(
            UrlParameter urlParameter,
            List<Derivat> allDerivate,
            List<KovAPhase> allKovaPhasen
    ) {

        this.statusFilter = new KovaStatusFilter(urlParameter);

        this.derivatFilter = new DerivatFilter(allDerivate, urlParameter);

        this.phaseFilter = new KovaPhaseFilter(allKovaPhasen, urlParameter);

        this.vonStartDateFilter = new VonDateFilter("start", urlParameter);

        this.bisStartDateFilter = new BisDateFilter("start", urlParameter);

        this.vonEndDateFilter = new VonDateFilter("end", urlParameter);

        this.bisEndDateFilter = new BisDateFilter("end", urlParameter);
    }

    public MultiValueEnumSearchFilter<KovAStatus> getStatusFilter() {
        return statusFilter;
    }

    public IdSearchFilter getDerivatFilter() {
        return derivatFilter;
    }

    public IdSearchFilter getPhaseFilter() {
        return phaseFilter;
    }

    public DateSearchFilter getVonStartDateFilter() {
        return vonStartDateFilter;
    }

    public DateSearchFilter getBisStartDateFilter() {
        return bisStartDateFilter;
    }

    public DateSearchFilter getVonEndDateFilter() {
        return vonEndDateFilter;
    }

    public DateSearchFilter getBisEndDateFilter() {
        return bisEndDateFilter;
    }

    public String getUrl() {
        return BASEURL + getUrlParameter();
    }

    public String getResetUrl() {
        return BASEURL + "?faces-redirect=true";
    }

    private String getUrlParameter() {
        FilterToUrlConverter urlConverter = FilterToUrlConverter.forClass(this);
        return urlConverter.getUrl();
    }

}
