package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class SingleDerivatFilter extends AbstractSingleIdFilter {

    public static final String PARAMETERNAME = "derivat";

    public SingleDerivatFilter(Collection<Derivat> allDerivate, UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForDerivate(allDerivate));
        super.parseParameter(urlParameter);
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.DERIVAT;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
