package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.converters.SearchFilterObjectConverter;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.faces.convert.Converter;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author fp
 */
public class ZakStatusFilter implements MultiValueEnumSearchFilter<ZakStatus> {

    public static final String PARAMETERNAME = "zakStatusFilter";

    private final List<SearchFilterObject> allStatus;
    private Set<SearchFilterObject> selectedStatus;
    private final String derivatName;

    public ZakStatusFilter(UrlParameter urlParameter, String derivatName) {
        this.allStatus = SearchFilterObjectFactory.getSearchFilterObjectsForZakStatus();
        this.selectedStatus = new HashSet<>();
        this.derivatName = derivatName;
        this.parseParameter(urlParameter);
    }

    @Override
    public List<ZakStatus> getAsEnumList() {
        return selectedStatus.stream().map(s -> ZakStatus.getStatusById(Math.toIntExact(s.getId()))).collect(Collectors.toList());
    }

    @Override
    public boolean isActive() {
        return !this.selectedStatus.isEmpty();
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.ZAK_STATUS;
    }

    @Override
    public List<SearchFilterObject> getAll() {
        return allStatus;
    }

    @Override
    public Set<SearchFilterObject> getSelectedValues() {
        return selectedStatus;
    }

    @Override
    public void setSelectedValues(Set<SearchFilterObject> selected) {
        this.selectedStatus = selected;
    }

    @Override
    public Converter getConverter() {
        return new SearchFilterObjectConverter(allStatus);
    }

    @Override
    public String getIndependentParameter() {
        if (selectedStatus.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&" + getParameterName() + "=");
        selectedStatus.forEach(s -> sb.append(s.getId().toString()).append(","));
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    @Override
    public String getParameter() {
        if (selectedStatus.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder(getParameterName() + "-");
        selectedStatus.forEach(s -> sb.append(s.getId().toString()).append(","));
        sb.deleteCharAt(sb.length() - 1);
        sb.append("_");
        return sb.toString();
    }

    @Override
    public String getParameterName() {
        return derivatName + PARAMETERNAME;
    }

    private void parseParameter(UrlParameter urlParameter) {
        Optional<String> paramter = urlParameter.getValue(getParameterName());
        if (paramter.isPresent()) {
            parseParameter(paramter.get());
        }
    }

    @Override
    public MultiValueEnumSearchFilter<ZakStatus> parseParameter(String parameter) {
        if (parameter != null) {
            String[] paramters = parameter.split(",");
            setSelectedValues(Stream.of(paramters)
                    .map(p -> (String) p)
                    .filter(p -> p.matches("\\d+"))
                    .map(p -> allStatus.stream().filter(s -> s.getId().toString().equals(p)).findAny())
                    .filter(p -> p.isPresent())
                    .map(p -> p.get())
                    .collect(Collectors.toSet()));
        }
        return this;
    }

}
