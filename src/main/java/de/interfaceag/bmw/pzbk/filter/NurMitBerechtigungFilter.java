package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

/**
 *
 * @author fn
 */
public class NurMitBerechtigungFilter extends AbstractBooleanFilter {

    private boolean value = Boolean.TRUE;
    public static final String PARAMETERNAME = "nurBerechtigungFilter";

    public NurMitBerechtigungFilter(UrlParameter urlParameter) {
        super(urlParameter, SearchFilterObjectFactory.getSearchFilterObjectsForBoolean());
        if (super.getSelectedValues().size() == 1) {
            SearchFilterObject next = super.getSelectedValues().iterator().next();
            this.setValue(next.getBooleanValue());
        } else {
            super.getSelectedValues().clear();
        }
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.NURBERECHTIGTE;
    }

    public final void setValue(boolean value) {
        super.getSelectedValues().clear();
        super.getSelectedValues().add(new SearchFilterObject(value, ""));
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }
}
