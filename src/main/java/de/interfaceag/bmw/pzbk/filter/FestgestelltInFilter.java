package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Collection;
import java.util.HashSet;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FestgestelltInFilter extends AbstractIdFilter {

    public static final String PARAMETERNAME = "festgestlltInFilter";

    public FestgestelltInFilter(Collection<FestgestelltIn> allFestgestelltIn, UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForFestgestelltIn(allFestgestelltIn));
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.FESTGESTELLT_IN;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
