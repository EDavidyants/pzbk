package de.interfaceag.bmw.pzbk.filter;

import java.util.OptionalLong;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface SingleIdSearchFilter extends SingleValueSearchFilter {

    OptionalLong getSelectedId();

}
