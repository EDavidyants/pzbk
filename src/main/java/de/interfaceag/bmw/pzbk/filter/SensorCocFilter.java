package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class SensorCocFilter extends AbstractIdFilter {

    public static final String PARAMETERNAME = "sensorCocFilter";

    public SensorCocFilter(Collection<SensorCoc> allSensorCocs, UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForSensorCoc(allSensorCocs));
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    public SensorCocFilter(Collection<SensorCoc> allSensorCocs) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForSensorCoc(allSensorCocs));
        super.setSelectedValues(new HashSet<>());
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.SENSORCOC;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
