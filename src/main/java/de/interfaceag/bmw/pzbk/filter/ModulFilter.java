package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ModulFilter extends AbstractIdFilter {

    public static final String PARAMETERNAME = "modulFilter";

    public ModulFilter(Collection<Modul> allModule, UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForModul(allModule));
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    public ModulFilter(List<Modul> allModule) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForModul(allModule));
        super.setSelectedValues(new HashSet<>());
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.MODUL;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
