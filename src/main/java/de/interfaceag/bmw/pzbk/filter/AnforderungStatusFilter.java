package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AnforderungStatusFilter extends AbstractEnumFilter<Status> {

    public static final String PARAMETERNAME = "anforderungStatusFilter";

    public AnforderungStatusFilter(Collection<Status> activeValues) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForAnforderungStatus());
        Collection<Long> statusIds = activeValues.stream().map(Status::getStatusId).map(Integer::longValue).collect(Collectors.toSet());
        final Set<SearchFilterObject> selectedvalues = super.getAll().stream().filter(value -> statusIds.contains(value.getId())).collect(Collectors.toSet());
        super.setSelectedValues(selectedvalues);
    }

    public AnforderungStatusFilter(UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForAnforderungStatus());
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    @Override
    public List<Status> getAsEnumList() {
        return getSelectedValues().stream()
                .map(s -> Status.getStatusById(Math.toIntExact(s.getId())))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Status> getAsEnum() {
        if (getSelectedValueAsOptional().isPresent()) {
            SearchFilterObject searchFilterObject = getSelectedValueAsOptional().get();
            Integer id = searchFilterObject.getId().intValue();
            return Optional.of(Status.getStatusById(id));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.STATUS;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
