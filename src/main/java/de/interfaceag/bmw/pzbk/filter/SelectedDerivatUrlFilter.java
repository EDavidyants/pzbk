package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class SelectedDerivatUrlFilter extends AbstractUrlFilter {

    private String parameterName = "selectedDerivat";

    public SelectedDerivatUrlFilter(UrlParameter urlParameter) {
        super();
        parseParameter(urlParameter);
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    @Override
    public String getParameterName() {
        return parameterName;
    }

    @Override
    public void setAttributeValue(String value) {
        super.setAttributeValue(value);
        super.setActive(true);
    }

    public Optional<Long> getId() {
        if (getAttributeValue() != null && RegexUtils.matchesId(getAttributeValue())) {
            return Optional.of(Long.parseLong(getAttributeValue()));
        }
        return Optional.empty();
    }

    public static Builder withParamterName(String parameterName) {
        return new Builder().withParameterName(parameterName);
    }

    public static class Builder {

        private String parameterName = "selectedDerivat";
        private UrlParameter urlParameter;

        public Builder() {
        }

        public Builder withUrlParameter(UrlParameter urlParameter) {
            this.urlParameter = urlParameter;
            return this;
        }

        private Builder withParameterName(String parameterName) {
            this.parameterName = parameterName;
            return this;
        }

        public UrlFilter build() {
            SelectedDerivatUrlFilter result = new SelectedDerivatUrlFilter(urlParameter);
            result.setParameterName(parameterName);
            result.parseParameter(urlParameter);
            return result;
        }

    }

}
