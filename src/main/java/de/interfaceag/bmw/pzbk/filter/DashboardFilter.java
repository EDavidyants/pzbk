package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DashboardFilter extends AbstractUrlFilter {

    public static final Logger LOG = LoggerFactory.getLogger(DashboardFilter.class);

    public static final String PARAMETERNAME = "dashboard";

    private int step;

    public DashboardFilter(UrlParameter urlParameter) {
        super();
        parseParameter(urlParameter);
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    public void setAttributeValue(int step) {
        this.step = step;
        super.setAttributeValue(Integer.toString(step));
    }

    @Override
    public void setAttributeValue(String value) {
        super.setAttributeValue(value);
        super.setActive(true);
    }

    public int getStep() {
        try {
            return Integer.parseInt(getAttributeValue());
        } catch (NumberFormatException exeption) {
            LOG.error("Could not parse number", exeption);
            return step;
        }
    }

    public void disable() {
        super.setActive(Boolean.FALSE);
    }

}
