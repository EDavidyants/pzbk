package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Collection;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class TechnologieFilter extends AbstractMultiValueFilter {

    public static final String PARAMETERNAME = "technologieFilter";

    public TechnologieFilter(Collection<SensorCoc> allSensorCocs, UrlParameter urlParameter) {
        super(allSensorCocs, urlParameter);
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.TECHNOLOGIE;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }
}
