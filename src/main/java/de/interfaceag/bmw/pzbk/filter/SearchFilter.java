package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;

import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @param <T>
 */
public interface SearchFilter<T> extends Serializable, BasicFilter {

    SearchFilterType getType();

    List<T> getAll();

    Converter getConverter();

    @Deprecated
    String getParameter();

    String getParameterName();

    SearchFilter parseParameter(String parameter);

    boolean isActive();

}
