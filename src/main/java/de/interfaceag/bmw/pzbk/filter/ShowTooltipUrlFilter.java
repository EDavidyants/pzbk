package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ShowTooltipUrlFilter extends AbstractBooleanUrlFilter {

    public ShowTooltipUrlFilter(UrlParameter urlParameter) {
        super();
        parseParameter(urlParameter);
    }

    @Override
    public void setAttributeValue(String parameterValue) {
        if ("true".equals(parameterValue)) {
            setActive();
        } else {
            setInactive();
        }
    }

    @Override
    public String getParameterName() {
        return "showTooltip";
    }

    @Override
    public void setActive() {
        super.setActive(true);
        super.setAttributeValue("true");
    }

    @Override
    public void setInactive() {
        super.setActive(false);
        super.setAttributeValue("false");
    }

}
