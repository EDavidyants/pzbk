package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;

import java.util.Collection;
import java.util.HashSet;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ThemenklammerFilter extends AbstractIdFilter {

    public static final String PARAMETERNAME = "themenklammerFilter";

    public ThemenklammerFilter(Collection<ThemenklammerDto> themenklammern, UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForThemenklammern(themenklammern));
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.THEMENKLAMMER;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
