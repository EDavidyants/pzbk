package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DatumStatusaenderungVonBisDateFilter extends AbstractVonBisDateFilter {

    public static final String PARAMETERPRAEFIX = "datumStatusaenderung";

    public DatumStatusaenderungVonBisDateFilter(UrlParameter urlParameter) {
        super(PARAMETERPRAEFIX, urlParameter);
    }

}
