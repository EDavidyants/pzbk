package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.HashSet;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class KovaPhaseFilter extends AbstractIdFilter {

    public static final String PARAMETERNAME = "kovaPhaseFilter";

    public KovaPhaseFilter(List<KovAPhase> allKovaPhasen, UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForKovaPhase(allKovaPhasen));
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.KOVA_PHASE;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
