package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class UserDefinedSearchUrlFilter extends AbstractUrlFilter {

    public static final Logger LOG = LoggerFactory.getLogger(UserDefinedSearchUrlFilter.class);

    public static final String PARAMETERNAME = "userDefinedSearch";

    public UserDefinedSearchUrlFilter(UrlParameter urlParameter) {
        super();
        parseParameter(urlParameter);
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    public void setAttributeValue(Long id) {
        if (id != null) {
            this.setAttributeValue(Long.toString(id));
        }
    }

    @Override
    public void setAttributeValue(String value) {
        super.setAttributeValue(value);
        super.setActive(true);
    }

    public Optional<Long> getId() {
        if (getAttributeValue() == null) {
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(Long.parseLong(getAttributeValue()));
        } catch (NumberFormatException exception) {
            LOG.error("Could not parse number", exception);
            return Optional.empty();
        }
    }
}
