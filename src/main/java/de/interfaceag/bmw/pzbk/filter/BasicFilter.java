package de.interfaceag.bmw.pzbk.filter;

/**
 *
 * @author sl
 */
public interface BasicFilter {

    String getIndependentParameter();

}
