package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class BisDateFilter extends AbstractDateFilter {

    public static final String PARAMETERNAME = "bisDateFilter";
    private final String parameterPraefix;

    public BisDateFilter(String parameterPraefix, UrlParameter urlParameter) {
        this.parameterPraefix = parameterPraefix;
        super.parseParameter(urlParameter);
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.BIS_DATE;
    }

    @Override
    public String getParameterName() {
        return parameterPraefix + PARAMETERNAME;
    }

}
