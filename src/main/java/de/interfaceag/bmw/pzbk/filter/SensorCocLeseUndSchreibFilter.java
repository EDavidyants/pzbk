package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.HashSet;
import java.util.List;

/**
 *
 * @author fn
 */
public class SensorCocLeseUndSchreibFilter extends AbstractIdFilter {

    private final String parametername;
    private final boolean isSchreibend;

    public SensorCocLeseUndSchreibFilter(List<SensorCoc> allSensorCocs, UrlParameter urlParameter, String parameterName, boolean isSchreibend) {
        this.parametername = parameterName;
        this.isSchreibend = isSchreibend;
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForSensorCoc(allSensorCocs));
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);

    }

    @Override
    public SearchFilterType getType() {
        if (isSchreibend) {
            return SearchFilterType.SENSORCOC_SCHREIBEND;
        } else {
            return SearchFilterType.SENSORCOC_LESEND;
        }
    }

    @Override
    public String getParameterName() {
        return parametername;
    }

}
