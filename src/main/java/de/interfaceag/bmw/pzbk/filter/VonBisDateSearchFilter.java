package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface VonBisDateSearchFilter extends Serializable {

    SearchFilterType getType();

    VonDateFilter getVonDateFilter();

    BisDateFilter getBisDateFilter();

    Date getVon();

    Date getBis();

    void setVon(Date von);

    void setBis(Date bis);

    String getIndependentParameter();

    boolean isActive();

    VonBisDateFilterCase getCase();

}
