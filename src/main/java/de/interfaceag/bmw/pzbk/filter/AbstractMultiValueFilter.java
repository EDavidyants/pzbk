package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.converters.SearchFilterNameObjectConverter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.faces.convert.Converter;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static de.interfaceag.bmw.pzbk.filter.TechnologieFilter.PARAMETERNAME;

/**
 *
 * @author lomu
 */
public abstract class AbstractMultiValueFilter implements MultiValueNameSearchFilter {

    private List<SearchFilterNameObject> allValues;
    private Collection<SearchFilterNameObject> selectedValues;

    public AbstractMultiValueFilter(List<String> allFachteams, UrlParameter urlParameter) {
        this.allValues = SearchFilterObjectFactory.getSearchFilterObjectsForFachbereich(allFachteams);
        this.selectedValues = new HashSet<>();
        parseParameter(urlParameter);
    }

    public AbstractMultiValueFilter(Collection<SensorCoc> allSensorCocs, UrlParameter urlParameter) {
        allValues = SearchFilterObjectFactory.getSearchFilterObjectsForSensorCocTechnologie(allSensorCocs);
        selectedValues = new HashSet<>();
        parseParameter(urlParameter);
    }

    @Override
    public List<String> getSelectedValuestringsAsList() {
        return getSelectedValues().stream().map(SearchFilterNameObject::getName).collect(Collectors.toList());
    }

    @Override
    public List<SearchFilterNameObject> getAll() {
        return allValues;
    }

    @Override
    public boolean isActive() {
        return !this.selectedValues.isEmpty();
    }

    @Override
    public Set<SearchFilterNameObject> getSelectedValues() {
        return new HashSet<>(selectedValues);
    }

    @Override
    public Converter getConverter() {
        return new SearchFilterNameObjectConverter(allValues);
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    @Override
    public String getIndependentParameter() {
        if (selectedValues.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&" + getParameterName() + "=");
        selectedValues.forEach(s -> sb.append(s.getName()).append(","));
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    @Override
    public String getParameter() {
        if (selectedValues.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder(getParameterName() + "-");
        selectedValues.forEach(s -> sb.append(s).append(","));
        sb.deleteCharAt(sb.length() - 1);
        sb.append("_");
        return sb.toString();
    }

    protected final void parseParameter(UrlParameter urlParameter) {
        Optional<String> parameter = urlParameter.getValue(getParameterName());
        if (parameter.isPresent()) {
            parseParameter(parameter.get());
        }
    }

    @Override
    public MultiValueSearchFilter parseParameter(String parameter) {
        if (parameter != null) {
            Collection<String> paramters = Arrays.asList(parameter.split(","));
            Set<SearchFilterNameObject> all = new HashSet<>(allValues);
            selectedValues = all.stream().filter(sfo -> paramters.contains(sfo.getName())).collect(Collectors.toSet());
        }
        return this;
    }

    @Override
    public void setSelectedValues(Set<SearchFilterNameObject> selected) {
        this.selectedValues = selected;
    }

    public Collection<String> getSelectedStringValues() {
        return selectedValues.stream().map(sfo -> sfo.getName()).collect(Collectors.toSet());
    }

}
