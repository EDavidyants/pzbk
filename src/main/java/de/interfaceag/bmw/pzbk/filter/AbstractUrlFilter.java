package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractUrlFilter implements UrlFilter {

    private boolean active;

    private String attributeValue;

    // --------- methods -------------------------------------------------------
    @Override
    public final String getAttributeValue() {
        return this.attributeValue;
    }

    @Override
    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    @Override
    public final boolean isActive() {
        return active;
    }

    public final void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public final String getIndependentParameter() {
        if (!isActive()) {
            return "";
        }
        return "&" + getParameterName() + "=" + getAttributeValue();
    }

    @Override
    public final void parseParameter(UrlParameter urlParameter) {
        Optional<String> parameter = urlParameter.getValue(getParameterName());
        if (parameter.isPresent()) {
            String parameterValue = parameter.get();
            if (parameterValue != null && !parameterValue.isEmpty()) {
                setAttributeValue(parameterValue);
            }
        }
    }

}
