package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class TypeFilter extends AbstractEnumFilter<Type> {

    public static final String PARAMETERNAME = "typeFilter";

    public TypeFilter(UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForType());
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    @Override
    public List<Type> getAsEnumList() {
        return getSelectedValues().stream()
                .map(selected -> Type.getById(Math.toIntExact(selected.getId())))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.TYPE;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    @Override
    public Optional<Type> getAsEnum() {
        if (getSelectedValueAsOptional().isPresent()) {
            return Type.getById(getSelectedValueAsOptional().get().getId().intValue());
        }
        return Optional.empty();
    }

}
