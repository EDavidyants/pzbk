package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.HashSet;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ModulSeTeamFilter extends AbstractIdFilter {

    public static final String PARAMETERNAME = "modulSeTeamFilter";

    public ModulSeTeamFilter(List<ModulSeTeam> allModulSeTeams, UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForModulSeTeams(allModulSeTeams));
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.MODULSETEAM;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
