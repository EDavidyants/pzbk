package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.entities.Werk;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class WerkFilter extends AbstractIdFilter {

    public static final String PARAMETERNAME = "werkFilter";

    public WerkFilter(Collection<Werk> allWerk, UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForWerk(allWerk));
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.WERK;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
