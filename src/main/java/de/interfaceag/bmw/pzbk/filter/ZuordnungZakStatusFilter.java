package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ZuordnungZakStatusFilter extends AbstractBooleanFilter {

    public static final String PARAMETERNAME = "zuordnungZakStatusFilter";

    private final String derivatName;

    public ZuordnungZakStatusFilter(UrlParameter urlParameter, String derivatName) {
        super(urlParameter, SearchFilterObjectFactory.getSearchFilterObjectsForZuordnungZakStatus());
        this.derivatName = derivatName;
        this.parseParameter(urlParameter);
    }

    @Override
    public String getParameterName() {
        return derivatName + PARAMETERNAME;
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.ZUORDNUNG_ZAK_STATUS;
    }

}
