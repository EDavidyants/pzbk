package de.interfaceag.bmw.pzbk.filter.pages;

import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ModulFilter;
import de.interfaceag.bmw.pzbk.filter.ParameterEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SensorCocFilter;
import de.interfaceag.bmw.pzbk.filter.UmsetzungsbestaetigungStatusFilter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class OldUmsetzungsbestaetigungViewFilter {

    public static final String BASEURL = Page.UMSETZUNGSBESTAETIGUNG.getUrl();

    private final IdSearchFilter sensorCocFilter;
    private final IdSearchFilter modulFilter;
    private final ParameterEnumSearchFilter<UmsetzungsBestaetigungStatus> currentPhaseStatusFilter;
    private final ParameterEnumSearchFilter<UmsetzungsBestaetigungStatus> lastPhaseStatusFilter;
    private final String id;

    public OldUmsetzungsbestaetigungViewFilter(
            String urlParams,
            String id,
            List<SensorCoc> allSensorCocs,
            List<Modul> allModule
    ) {
        Map<String, String> urlParameter = parseUrlParameter(urlParams);

        this.id = id;

        this.sensorCocFilter = new SensorCocFilter(allSensorCocs)
                .parseParameter(urlParameter.get(SensorCocFilter.PARAMETERNAME));

        this.modulFilter = new ModulFilter(allModule)
                .parseParameter(urlParameter.get(ModulFilter.PARAMETERNAME));

        this.currentPhaseStatusFilter = new UmsetzungsbestaetigungStatusFilter("currentPhase");
        this.currentPhaseStatusFilter.parseParameter(urlParameter.get(currentPhaseStatusFilter.getParamterName()));

        this.lastPhaseStatusFilter = new UmsetzungsbestaetigungStatusFilter("lastPhase");
        this.lastPhaseStatusFilter.parseParameter(urlParameter.get(lastPhaseStatusFilter.getParamterName()));
    }

    private static Map<String, String> parseUrlParameter(String urlParams) {
        Map<String, String> result = new HashMap<>();
        if (urlParams != null) {
            String[] urlParameterParts = urlParams.split("_");
            Stream.of(urlParameterParts)
                    .map(s -> (String) s)
                    .filter(s -> s.indexOf("-") > 0)
                    .forEach(s -> {
                        int index = s.indexOf("-");
                        String paramtername = s.substring(0, index);
                        String paramter = s.substring(index + 1);
                        result.put(paramtername, paramter);
                    });
        }

        return result;
    }

    public IdSearchFilter getSensorCocFilter() {
        return sensorCocFilter;
    }

    public IdSearchFilter getModulFilter() {
        return modulFilter;
    }

    public ParameterEnumSearchFilter<UmsetzungsBestaetigungStatus> getCurrentPhaseStatusFilter() {
        return currentPhaseStatusFilter;
    }

    public ParameterEnumSearchFilter<UmsetzungsBestaetigungStatus> getLastPhaseStatusFilter() {
        return lastPhaseStatusFilter;
    }

    public String getUrl() {
        return BASEURL + getUrlParameter();
    }

    public String getResetUrl() {
        return BASEURL + "?faces-redirect=true&id=" + id;
    }

    private String getUrlParameter() {
        StringBuilder sb = new StringBuilder("?faces-redirect=true");
        sb.append("&id=")
                .append(id)
                .append("&filter=")
                .append(sensorCocFilter.getParameter())
                .append(modulFilter.getParameter())
                .append(currentPhaseStatusFilter.getParameter())
                .append(lastPhaseStatusFilter.getParameter());
        return sb.toString();
    }
}
