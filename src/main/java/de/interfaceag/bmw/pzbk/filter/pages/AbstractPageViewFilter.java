package de.interfaceag.bmw.pzbk.filter.pages;

import de.interfaceag.bmw.pzbk.enums.Page;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractPageViewFilter {

    public String getUrl() {
        return getPage().getUrl() + getUrlParameter();
    }

    public String getResetUrl() {
        return getPage().getUrl() + "?faces-redirect=true";
    }

    protected abstract Page getPage();

    protected abstract String getUrlParameter();
}
