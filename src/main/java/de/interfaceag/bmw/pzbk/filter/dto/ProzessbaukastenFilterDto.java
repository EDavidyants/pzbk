package de.interfaceag.bmw.pzbk.filter.dto;

import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;

/**
 *
 * @author fn
 */
public final class ProzessbaukastenFilterDto {

    private final Long id;
    private final String fachId;
    private final Integer version;
    private final String bezeichnung;

    public ProzessbaukastenFilterDto(Long id, String fachId, Integer version, String bezeichnung) {
        this.id = id;
        this.fachId = fachId;
        this.version = version;
        this.bezeichnung = bezeichnung;
    }

    public static ProzessbaukastenFilterDto forProzessbaukasten(Prozessbaukasten prozessbaukasten) {
        return new ProzessbaukastenFilterDto(prozessbaukasten.getId(), prozessbaukasten.getFachId(), prozessbaukasten.getVersion(), prozessbaukasten.getBezeichnung());
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return fachId + " | V" + version + ": " + bezeichnung;
    }

}
