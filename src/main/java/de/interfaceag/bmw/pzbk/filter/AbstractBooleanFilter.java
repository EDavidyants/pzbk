package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.converters.SearchFilterObjectConverter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.faces.convert.Converter;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractBooleanFilter implements BooleanSearchFilter {

    private final List<SearchFilterObject> all;
    private Set<SearchFilterObject> selected;

    public AbstractBooleanFilter(UrlParameter urlParameter, List<SearchFilterObject> all) {

        this.all = all;
        this.selected = new HashSet<>();
        this.parseParameter(urlParameter);
    }

    @Override
    public boolean isActive() {
        return !this.selected.isEmpty();
    }

    @Override
    public List<SearchFilterObject> getAll() {
        return all;
    }

    @Override
    public Set<SearchFilterObject> getSelectedValues() {
        return selected;
    }

    @Override
    public void setSelectedValues(Set<SearchFilterObject> selected) {
        this.selected = selected;
    }

    @Override
    public Converter getConverter() {
        return new SearchFilterObjectConverter(all);
    }

    @Override
    public String getIndependentParameter() {
        if (selected.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&" + getParameterName() + "=");
        selected.forEach(s -> sb.append(s.getId().toString()).append(","));
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    @Override
    public String getParameter() {
        if (selected.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder(getParameterName() + "-");
        selected.forEach(s -> sb.append(s.getId().toString()).append(","));
        sb.deleteCharAt(sb.length() - 1);
        sb.append("_");
        return sb.toString();
    }

    protected final void parseParameter(UrlParameter urlParameter) {
        Optional<String> parameter = urlParameter.getValue(getParameterName());
        if (parameter.isPresent()) {
            parseParameter(parameter.get());
        }
    }

    @Override
    public BooleanSearchFilter parseParameter(String parameter) {
        if (parameter != null) {
            String[] paramters = parameter.split(",");
            setSelectedValues(Stream.of(paramters)
                    .map(p -> (String) p)
                    .filter(p -> p.matches("\\d+"))
                    .map(p -> all.stream().filter(s -> s.getId().toString().equals(p)).findAny())
                    .filter(p -> p.isPresent())
                    .map(p -> p.get())
                    .collect(Collectors.toSet()));
        }
        return this;
    }

    @Override
    public List<Boolean> getAsBoolean() {
        return getSelectedValues().stream().map(s -> s.getId() == 1).collect(Collectors.toList());
    }

}
