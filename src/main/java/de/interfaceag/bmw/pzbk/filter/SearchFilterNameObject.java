package de.interfaceag.bmw.pzbk.filter;

import java.io.Serializable;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class SearchFilterNameObject implements Serializable {

    private final String name;

    public SearchFilterNameObject(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
