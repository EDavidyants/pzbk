package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachbereichFilter extends AbstractMultiValueFilter {

    public static final String PARAMETERNAME = "fachbereichFilter";

    public FachbereichFilter(List<String> allFachteams, UrlParameter urlParameter) {
        super(allFachteams, urlParameter);
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.FACHBEREICH;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }
}
