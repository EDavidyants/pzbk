package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class IdListUrlFilter extends AbstractUrlFilter {

    public IdListUrlFilter(UrlParameter urlParameter) {
        super();
        parseParameter(urlParameter);
    }

    @Override
    public String getParameterName() {
        return "id";
    }

    public void setAttributeValue(Set<Long> ids) {
        super.setAttributeValue(idsToString(ids));
    }

    @Override
    public void setAttributeValue(String value) {
        super.setAttributeValue(value);
        super.setActive(true);
    }

    public Set<Long> getIds() {
        return stringToIds(getAttributeValue());
    }

    private static String idsToString(Set<Long> ids) {
        if (ids != null) {
            return ids.stream().map(id -> id.toString()).collect(Collectors.joining(","));
        }
        return "";
    }

    private static Set<Long> stringToIds(String stringToMap) {
        Set<Long> result = new HashSet<>();
        if (stringToMap != null) {
            if (stringToMap.contains(",")) {
                Stream.of(stringToMap.split(","))
                        .filter(value -> value != null && RegexUtils.matchesId(value))
                        .forEach(value -> result.add(Long.parseLong(value)));
            } else if (stringToMap.contains(" ")) {
                Stream.of(stringToMap.split(" "))
                        .filter(value -> value != null && RegexUtils.matchesId(value))
                        .forEach(value -> result.add(Long.parseLong(value)));
            } else if (RegexUtils.matchesId(stringToMap.trim())) {
                result.add(Long.parseLong(stringToMap.trim()));
            }
        }
        return result;
    }

}
