package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface DateSearchFilter extends Serializable, BasicFilter {

    SearchFilterType getType();

    Date getSelected();

    void setSelected(Date selected);

    String getParameter();

    String getParameterName();

    DateSearchFilter parseParameter(String parameter);

    boolean isActive();

    void setActive(boolean isActive);

}
