package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Date;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractVonBisDateFilter implements VonBisDateSearchFilter {

    private final VonDateFilter vonDateFilter;
    private final BisDateFilter bisDateFilter;

    public AbstractVonBisDateFilter(String parameterPraefix, UrlParameter urlParameter) {
        this.vonDateFilter = new VonDateFilter(parameterPraefix, urlParameter);
        this.bisDateFilter = new BisDateFilter(parameterPraefix, urlParameter);
    }

    @Override
    public final SearchFilterType getType() {
        return SearchFilterType.VON_BIS_DATE;
    }

    @Override
    public final Date getVon() {
        return vonDateFilter.getSelected();
    }

    @Override
    public final Date getBis() {
        return bisDateFilter.getSelected();
    }

    @Override
    public final void setVon(Date von) {
        vonDateFilter.setSelected(von);
    }

    @Override
    public final void setBis(Date bis) {
        bisDateFilter.setSelected(bis);
    }

    @Override
    public final String getIndependentParameter() {
        return vonDateFilter.getIndependentParameter() + bisDateFilter.getIndependentParameter();
    }

    @Override
    public final boolean isActive() {
        return vonDateFilter.isActive() || bisDateFilter.isActive();
    }

    @Override
    public VonDateFilter getVonDateFilter() {
        return vonDateFilter;
    }

    @Override
    public BisDateFilter getBisDateFilter() {
        return bisDateFilter;
    }

    @Override
    public VonBisDateFilterCase getCase() {
        if (getVonDateFilter().isActive() && getBisDateFilter().isActive()) {
            return VonBisDateFilterCase.VONBIS;
        } else if (getVonDateFilter().isActive()) {
            return VonBisDateFilterCase.VON;
        } else if (getBisDateFilter().isActive()) {
            return VonBisDateFilterCase.BIS;
        } else {
            return VonBisDateFilterCase.NONE;
        }
    }

}
