package de.interfaceag.bmw.pzbk.filter;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class SearchFilterObject implements Serializable, Comparable<SearchFilterObject> {

    private final Long id;
    private final String name;

    public SearchFilterObject(boolean idSwitch, String name) {
        this.id = idSwitch ? 1L : 0L;
        this.name = name;
    }

    public SearchFilterObject(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public SearchFilterObject(String name) {
        this.id = 1L;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean getBooleanValue() {
        return id.equals(1L);
    }

    @Override
    public String toString() {
        return "SearchFilterObject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(SearchFilterObject searchFilterObject) {
        if (Objects.isNull(searchFilterObject) || Objects.isNull(searchFilterObject.getName())) {
            return 1;
        } else if (Objects.isNull(this.getName())) {
            return -1;
        } else {
            final String thisName = this.getName();
            final String searchFilterObjectName = searchFilterObject.getName();
            return thisName.compareTo(searchFilterObjectName);
        }
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        SearchFilterObject that = (SearchFilterObject) object;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
