package de.interfaceag.bmw.pzbk.filter;

import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface MultiValueNameSearchFilter extends MultiValueSearchFilter<SearchFilterNameObject> {

    List<String> getSelectedValuestringsAsList();

}
