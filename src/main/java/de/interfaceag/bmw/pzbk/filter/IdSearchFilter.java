package de.interfaceag.bmw.pzbk.filter;

import java.util.List;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface IdSearchFilter extends MultiValueSearchFilter<SearchFilterObject> {

    Set<Long> getSelectedIdsAsSet();

    List<String> getSelectedValuestringsAsList();

}
