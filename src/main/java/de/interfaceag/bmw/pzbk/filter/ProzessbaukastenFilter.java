package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ProzessbaukastenFilter extends AbstractIdFilter {

    public static final String PARAMETERNAME = "prozessbaukastenFilter";

    public ProzessbaukastenFilter(Collection<ProzessbaukastenFilterDto> prozessbaukaesten, UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForProzessbaukasten(prozessbaukaesten));
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.PROZESSBAUKASTEN;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
