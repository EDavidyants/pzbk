package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

/**
 *
 * @author fn
 */
public class IdFilter extends AbstractUrlFilter {

    public IdFilter(UrlParameter urlParameter) {
        super();
        parseParameter(urlParameter);
    }

    @Override
    public String getParameterName() {
        return "id";
    }

    public void setAttributeValue(Long ids) {
        super.setAttributeValue(ids.toString());
    }

    @Override
    public void setAttributeValue(String value) {
        super.setAttributeValue(value);
        super.setActive(true);
    }

    public Long getId() {
        return stringToIds(getAttributeValue());
    }

    private static Long stringToIds(String stringToMap) {
        if (stringToMap != null && RegexUtils.matchesId(stringToMap)) {
            return Long.parseLong(stringToMap);
        } else {
            return 0L;
        }

    }
}
