package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ProzessbaukastenStatusFilter extends AbstractEnumFilter<ProzessbaukastenStatus> {

    public static final String PARAMETERNAME = "prozessbaukastenStatus";

    public ProzessbaukastenStatusFilter(UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForProzessbaukastenStatus());
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    @Override
    public List<ProzessbaukastenStatus> getAsEnumList() {
        return getSelectedValues().stream()
                .map(s -> ProzessbaukastenStatus.getById(Math.toIntExact(s.getId())))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<ProzessbaukastenStatus> getAsEnum() {
        if (getSelectedValueAsOptional().isPresent()) {
            return Optional.of(ProzessbaukastenStatus.getById(Math.toIntExact(getSelectedValueAsOptional().get().getId().intValue())));
        }
        return Optional.empty();
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.PROZESSBAUKASTEN_STATUS;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
