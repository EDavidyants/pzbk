package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ZuordnenModeUrlFilter extends AbstractBooleanUrlFilter {

    public ZuordnenModeUrlFilter(UrlParameter urlParameter) {
        super();
        parseParameter(urlParameter);
    }

    @Override
    public String getParameterName() {
        return "zuordnenMode";
    }

    @Override
    public void setActive() {
        super.setActive(true);
        super.setAttributeValue("true");
    }

    @Override
    public void setInactive() {
        super.setActive(false);
        super.setAttributeValue("false");
    }

}
