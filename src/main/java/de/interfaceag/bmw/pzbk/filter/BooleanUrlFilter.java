package de.interfaceag.bmw.pzbk.filter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public interface BooleanUrlFilter extends UrlFilter {

    void setActive();

    void setInactive();

    void toggle();

}
