package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.RegexUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class IdUrlFilter extends AbstractUrlFilter {

    public static final String PARAMETERNAME = "ids";
    private final Type type;

    public IdUrlFilter(Type type, UrlParameter urlParameter) {
        super();
        this.type = type;
        parseParameter(urlParameter);
    }

    @Override
    public String getParameterName() {
        return type.getBezeichnung() + PARAMETERNAME;
    }

    @Override
    public final void setAttributeValue(String value) {
        super.setAttributeValue(value);
        super.setActive(true);
    }

    public void setAttributeValue(List<Long> values) {
        setAttributeValue(valuesToString(values));
    }

    public List<Long> getAttributeValues() {
        return stringToValues(getAttributeValue());
    }

    private static String valuesToString(List<Long> values) {
        if (values != null) {
            return values.stream().map(l -> l.toString()).collect(Collectors.joining(","));
        }
        return "";
    }

    private static List<Long> stringToValues(String value) {
        List<Long> result = new ArrayList<>();
        if (value != null) {
            if (value.contains(",")) {
                Stream.of(value.split(",")).filter(string -> RegexUtils.matchesId(string) && string.length() < 10)
                        .map(string -> Long.parseLong(string))
                        .forEach(splitEntry -> result.add(splitEntry));
            } else if (RegexUtils.matchesId(value)) {
                result.add(Long.parseLong(value));
            }
        }
        return result;
    }

}
