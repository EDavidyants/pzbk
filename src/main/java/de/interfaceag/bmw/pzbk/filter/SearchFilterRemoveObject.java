package de.interfaceag.bmw.pzbk.filter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class SearchFilterRemoveObject {

    private final String filter;
    private final SearchFilterObject searchFilterObject;

    public SearchFilterRemoveObject(String filter, SearchFilterObject searchFilterObject) {
        this.filter = filter;
        this.searchFilterObject = searchFilterObject;
    }

    public String getFilter() {
        return filter;
    }

    public SearchFilterObject getSearchFilterObject() {
        return searchFilterObject;
    }

    public Long getId() {
        return searchFilterObject.getId();
    }

    public String getName() {
        return searchFilterObject.getName();
    }

}
