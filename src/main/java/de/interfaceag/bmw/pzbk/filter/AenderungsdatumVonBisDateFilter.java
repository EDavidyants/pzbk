package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AenderungsdatumVonBisDateFilter extends AbstractVonBisDateFilter {

    public static final String PARAMETERPRAEFIX = "aenderungsdatum";

    public AenderungsdatumVonBisDateFilter(UrlParameter urlParameter) {
        super(PARAMETERPRAEFIX, urlParameter);
    }

}
