package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.converters.SearchFilterObjectConverter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.faces.convert.Converter;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractIdFilter implements IdSearchFilter {

    private List<SearchFilterObject> all;
    private Set<SearchFilterObject> selected;

    @Override
    public List<SearchFilterObject> getAll() {
        return all;
    }

    public void setAll(List<SearchFilterObject> all) {
        this.all = all;
    }

    @Override
    public Set<SearchFilterObject> getSelectedValues() {
        return selected;
    }

    @Override
    public void setSelectedValues(Set<SearchFilterObject> selected) {
        this.selected = selected;
    }

    @Override
    public boolean isActive() {
        return !this.getSelectedValues().isEmpty();
    }

    @Override
    public Converter getConverter() {
        return new SearchFilterObjectConverter(getAll());
    }

    @Override
    public String getIndependentParameter() {
        if (getSelectedValues().isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&" + getParameterName() + "=");
        getSelectedValues().forEach(s -> sb.append(s.getId().toString()).append(","));
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    @Override
    public String getParameter() {
        if (getSelectedValues().isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder(getParameterName() + "-");
        getSelectedValues().forEach(s -> sb.append(s.getId().toString()).append(","));
        sb.deleteCharAt(sb.length() - 1);
        sb.append("_");
        return sb.toString();
    }

    protected final void parseParameter(UrlParameter urlParameter) {
        Optional<String> parameter = urlParameter.getValue(getParameterName());
        if (parameter.isPresent()) {
            parseParameter(parameter.get());
        }
    }

    @Override
    public IdSearchFilter parseParameter(String parameter) {
        if (parameter != null) {
            String[] paramters = parameter.split(",");
            setSelectedValues(Stream.of(paramters)
                    .map(p -> (String) p)
                    .filter(p -> p.matches("\\d+"))
                    .map(p -> getAll().stream().filter(s -> s.getId().toString().equals(p)).findAny())
                    .filter(p -> p.isPresent())
                    .map(p -> p.get())
                    .collect(Collectors.toSet()));
        }
        return this;
    }

    @Override
    public Set<Long> getSelectedIdsAsSet() {
        return this.getSelectedValues().stream().map(s -> s.getId()).collect(Collectors.toSet());
    }

    @Override
    public List<String> getSelectedValuestringsAsList() {
        return getSelectedValues().stream().map(s -> s.getName()).collect(Collectors.toList());
    }
}
