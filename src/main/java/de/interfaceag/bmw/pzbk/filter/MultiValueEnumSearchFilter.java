package de.interfaceag.bmw.pzbk.filter;

import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 * @param <T>
 */
public interface MultiValueEnumSearchFilter<T> extends MultiValueSearchFilter<SearchFilterObject> {

    List<T> getAsEnumList();

}
