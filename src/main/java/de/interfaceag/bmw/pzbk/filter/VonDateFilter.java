package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class VonDateFilter extends AbstractDateFilter {

    public static final String PARAMETERNAME = "vonDateFilter";
    private final String parameterPraefix;

    public VonDateFilter(String parameterPraefix, UrlParameter urlParameter) {
        this.parameterPraefix = parameterPraefix;
        super.parseParameter(urlParameter);
    }

    public VonDateFilter(String parameterPraefix) {
        this.parameterPraefix = parameterPraefix;
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.VONDATE;
    }

    @Override
    public String getParameterName() {
        return parameterPraefix + PARAMETERNAME;
    }

}
