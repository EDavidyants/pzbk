package de.interfaceag.bmw.pzbk.filter;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractBooleanUrlFilter extends AbstractUrlFilter implements BooleanUrlFilter {

    @Override
    public void setActive() {
        super.setActive(true);
    }

    @Override
    public void setInactive() {
        super.setActive(false);
    }

    @Override
    public void toggle() {
        if (super.isActive()) {
            this.setInactive();
        } else {
            this.setActive();
        }
    }

}
