package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.converters.SearchFilterObjectStringConverter;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import javax.faces.convert.Converter;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author fn
 */
public class AbteilungFilter implements MultiValueSearchFilter<SearchFilterObject> {

    public static final String PARAMETERNAME = "abteilungFilter";
    private final List<String> allAbteilung;
    private List<SearchFilterObject> all;
    private Set<SearchFilterObject> selected;

    public AbteilungFilter(List<String> allAbteilung, UrlParameter urlParameter) {
        all = SearchFilterObjectFactory.getSearchFilterObjectsForAbteilung(allAbteilung);
        selected = new HashSet<>();

        parseParameter(urlParameter);
        this.allAbteilung = allAbteilung;
    }

    public AbteilungFilter(List<String> allAbteilung) {
        all = SearchFilterObjectFactory.getSearchFilterObjectsForAbteilung(allAbteilung);
        selected = new HashSet<>();
        this.allAbteilung = allAbteilung;
    }

    public List<String> getAllAbteilung() {
        return allAbteilung;
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.ABTEILUNG;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

    public void setAll(List<SearchFilterObject> all) {
        this.all = all;
    }

    protected final void parseParameter(UrlParameter urlParameter) {
        Optional<String> parameter = urlParameter.getValue(getParameterName());
        if (parameter.isPresent()) {
            parseParameter(parameter.get());
        }
    }

    @Override
    public void setSelectedValues(Set<SearchFilterObject> selected) {
        this.selected = selected;
    }

    @Override
    public Set<SearchFilterObject> getSelectedValues() {
        return selected;
    }

    @Override
    public List<SearchFilterObject> getAll() {
        return all;
    }

    @Override
    public Converter getConverter() {
        return new SearchFilterObjectStringConverter(getAll());
    }

    @Override
    public String getIndependentParameter() {
        if (getSelectedValues().isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&" + getParameterName() + "=");
        selected.forEach(s -> sb.append(s.getName()).append(","));
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    @Override
    public String getParameter() {

        if (getSelectedValues().isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder(getParameterName() + "-");
        selected.forEach(s -> sb.append(s.getName()).append(","));
        sb.deleteCharAt(sb.length() - 1);
        sb.append("_");
        return sb.toString();
    }

    @Override
    public MultiValueSearchFilter parseParameter(String parameter) {
        if (parameter != null) {
            String[] paramters = parameter.split(",");
            setSelectedValues(Stream.of(paramters)
                    .map(p -> getAll().stream().filter(s -> s.getName().equals(p)).findAny())
                    .filter(p -> p.isPresent())
                    .map(p -> p.get())
                    .collect(Collectors.toSet()));
        }
        return this;
    }

    @Override
    public boolean isActive() {
        return !this.getSelectedValues().isEmpty();
    }

    public List<String> getAllSelectedAsStringList() {
        return this.getSelectedValues().stream().map(s -> s.getName()).collect(Collectors.toList());
    }

}
