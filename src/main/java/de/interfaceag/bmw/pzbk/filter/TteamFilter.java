package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;

import java.util.Collection;
import java.util.HashSet;

/**
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class TteamFilter extends AbstractIdFilter {

    public static final String PARAMETERNAME = "tteamFilter";

    public TteamFilter(Collection<Tteam> allTteams, UrlParameter urlParameter) {
        super.setAll(SearchFilterObjectFactory.getSearchFilterObjectsForTteams(allTteams));
        super.setSelectedValues(new HashSet<>());
        super.parseParameter(urlParameter);
    }

    public TteamFilter(IdSearchFilter tteamFilter) {
        super.setAll(tteamFilter.getAll());
        super.setSelectedValues(tteamFilter.getSelectedValues());
    }

    @Override
    public SearchFilterType getType() {
        return SearchFilterType.TTEAM;
    }

    @Override
    public String getParameterName() {
        return PARAMETERNAME;
    }

}
