package de.interfaceag.bmw.pzbk.derivat;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

/**
 *
 * @author sl
 */
@Stateless
@Named
public class DerivatStatusChangeService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(DerivatStatusChangeService.class.getName());

    @Inject
    private DerivatService derivatService;

    public void changeStatus(Derivat derivat, DerivatStatus newStatus) {
        if (isStatusChangePossible(derivat.getStatus(), newStatus)) {
            updateDerivatStatus(derivat, newStatus);
        } else {
            LOG.warn("Statuschange for Derivvat {} with current Status {} to new status {} is not valid", derivat, derivat.getStatus(), newStatus);
        }
    }

    public void restoreToLastStatus(Derivat derivat) {
        isInputValid(derivat);
        if (derivatIsInaktiv(derivat)) {
            restoreDerivat(derivat);
        } else {
            LOG.warn("Derivat {} is not in Status inaktiv", derivat);
        }
    }

    private void restoreDerivat(Derivat derivat) {
        if (lastStatusIsPresent(derivat)) {
            restoreDerivatToLastStatus(derivat);
        } else {
            restoreDerivatToDefaultStatus(derivat);
        }
    }

    private void restoreDerivatToLastStatus(Derivat derivat) {
        DerivatStatus lastStatus = derivat.getLastStatus();
        changeStatus(derivat, lastStatus);
    }

    private void restoreDerivatToDefaultStatus(Derivat derivat) {
        LOG.info("Not lastStatus is defined for Derivat {}. Restore to default.", derivat);
        changeStatus(derivat, DerivatStatus.OFFEN);
    }

    private void isInputValid(Derivat derivat) {
        if (derivat == null) {
            throw new IllegalArgumentException("Derivat is null");
        }
        DerivatStatus currentStatus = derivat.getStatus();
        if (currentStatus == null) {
            throw new IllegalArgumentException("Current status of derivat is null");
        }
    }

    private static boolean lastStatusIsPresent(Derivat derivat) {
        return derivat.getLastStatus() != null;
    }

    private static boolean derivatIsInaktiv(Derivat derivat) {
        DerivatStatus currentStatus = derivat.getStatus();
        return currentStatus == DerivatStatus.INAKTIV;
    }

    public void updateDerivatStatus(Derivat derivat, DerivatStatus newStatus) {
        LOG.info("Update status of derivat {} to {}", derivat, newStatus);

        DerivatStatus currentStatus = derivat.getStatus();
        derivat.setLastStatus(currentStatus);
        derivat.setStatus(newStatus);
        derivatService.persistDerivat(derivat);

        if (newStatus.isInaktiv()) {
            derivatService.clearFKConstraintForMitarbeiter(derivat);
        }
    }

    private static boolean isStatusChangePossible(DerivatStatus currentStatus, DerivatStatus newStatus) {
        Collection<DerivatStatus> possibleNextStatus;
        switch (currentStatus) {
            case OFFEN:
                possibleNextStatus = Arrays.asList(DerivatStatus.VEREINARBUNG_VKBG, DerivatStatus.INAKTIV);
                break;
            case VEREINARBUNG_VKBG:
                possibleNextStatus = Arrays.asList(DerivatStatus.VEREINBARUNG_ZV, DerivatStatus.ZV, DerivatStatus.INAKTIV);
                break;
            case VEREINBARUNG_ZV:
                possibleNextStatus = Arrays.asList(DerivatStatus.ZV, DerivatStatus.INAKTIV);
                break;
            case ZV:
                possibleNextStatus = Arrays.asList(DerivatStatus.INAKTIV);
                break;
            case INAKTIV:
                possibleNextStatus = Arrays.asList(DerivatStatus.OFFEN, DerivatStatus.VEREINARBUNG_VKBG, DerivatStatus.VEREINBARUNG_ZV, DerivatStatus.ZV);
                break;
            default:
                return false;
        }
        return possibleNextStatus.contains(newStatus);
    }

}
