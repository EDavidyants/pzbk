package de.interfaceag.bmw.pzbk.derivat;

import de.interfaceag.bmw.pzbk.converters.DerivatConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

/**
 *
 * @author sl
 */
public class DerivatDtoConverter implements Converter<DerivatDto> {

    private static final Logger LOG = LoggerFactory.getLogger(DerivatConverter.class.getName());

    private final List<DerivatDto> derivate;

    public DerivatDtoConverter(List<DerivatDto> derivate) {
        this.derivate = derivate;
    }

    @Override
    public DerivatDto getAsObject(FacesContext context, UIComponent component, String value) {
        Long id;
        DerivatDto result = null;
        try {
            id = Long.parseLong(value);
            result = derivate.stream().filter(derivat -> derivat.getId().equals(id)).findFirst().orElse(null);
        } catch (NumberFormatException exception) {
            LOG.error(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, DerivatDto derivat) {
        return derivat.getId().toString();
    }

}
