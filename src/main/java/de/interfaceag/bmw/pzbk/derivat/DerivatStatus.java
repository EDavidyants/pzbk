package de.interfaceag.bmw.pzbk.derivat;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author sl
 */
public enum DerivatStatus {

    OFFEN(0, "derivatstatus_offen", "derivatstatus_offen"),
    VEREINARBUNG_VKBG(1, "derivatstatus_vereinbarung_vkbg", "derivatstatus_vereinbarung_vkbg_starten"),
    VEREINBARUNG_ZV(2, "derivatstatus_vereinbarung_zv", "derivatstatus_vereinbarung_zv_starten"),
    ZV(3, "derivatstatus_zv", "derivatstatus_zv_erreicht"),
    INAKTIV(4, "derivatstatus_inaktiv", "derivatstatus_inaktiv_setzen");

    private final int id;
    private final String localizationName;
    private final String statusChangeLocalizationName;

    DerivatStatus(int id, String localizationName, String statusChangeLocalizationName) {
        this.id = id;
        this.localizationName = localizationName;
        this.statusChangeLocalizationName = statusChangeLocalizationName;
    }

    public Integer getId() {
        return id;
    }

    public String getLocalizationName() {
        return localizationName;
    }

    public String getStatusChangeLocalizationName() {
        return statusChangeLocalizationName;
    }

    public Boolean isInaktiv() {
        return this.equals(DerivatStatus.INAKTIV);
    }

    /**
     * This method should be used to display the possible next status based on
     * the current one. Not for validation! Use DerivatStatusChangeSerivce
     * instead.
     *
     * @return
     */
    public Collection<DerivatStatus> getNextStatus() {
        Collection<DerivatStatus> possibleNextStatus;
        switch (this) {
            case OFFEN:
                possibleNextStatus = Collections.singletonList(DerivatStatus.VEREINARBUNG_VKBG);
                break;
            case VEREINARBUNG_VKBG:
                possibleNextStatus = Arrays.asList(DerivatStatus.VEREINBARUNG_ZV, DerivatStatus.ZV);
                break;
            case VEREINBARUNG_ZV:
                possibleNextStatus = Collections.singletonList(DerivatStatus.ZV);
                break;
            default:
                possibleNextStatus = Collections.emptyList();
        }
        return possibleNextStatus;
    }

    public static DerivatStatus getById(Integer id) {
        if (idIsNull(id)) {
            return getDefaultStatus();
        } else {
            return getDerivatStatusByNotNullId(id);
        }
    }

    private static boolean idIsNull(Integer id) {
        return id == null;
    }

    private static DerivatStatus getDefaultStatus() {
        return DerivatStatus.OFFEN;
    }

    private static DerivatStatus getDerivatStatusByNotNullId(Integer id) {
        Optional<DerivatStatus> statusById = Stream.of(DerivatStatus.values())
                .filter(status -> status.getId().equals(id)).findAny();
        if (statusById.isPresent()) {
            return statusById.get();
        } else {
            throw new IllegalArgumentException("Input id: " + id + " does not match any DerivatStatus id");
        }
    }
}
