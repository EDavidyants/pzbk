package de.interfaceag.bmw.pzbk.derivat;

import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.Collection;

public final class DerivatStatusChangePermission {

    private DerivatStatusChangePermission() {
    }

    public static boolean isStatusChangeEnabled(Collection<Rolle> roles, DerivatStatus status) {
        switch (status) {
            case OFFEN:
            case VEREINARBUNG_VKBG:
            case VEREINBARUNG_ZV:
                return isStatusChangeEnabledForRole(roles);
            default:
                return Boolean.FALSE;
        }
    }

    private static boolean isStatusChangeEnabledForRole(Collection<Rolle> roles) {
        return roles.contains(Rolle.ADMIN);
    }

}
