package de.interfaceag.bmw.pzbk.derivat;

import de.interfaceag.bmw.pzbk.entities.Derivat;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author lomu
 */
public class DerivatStatusChangeDTO implements Serializable {

    private final Derivat derivat;
    private final Collection<DerivatStatus> possibleNextStatus;
    private final DerivatStatusNames derivatStatusNames;

    public DerivatStatusChangeDTO(Derivat derivat, DerivatStatusNames derivatStatusNames) {
        this.derivat = derivat;
        this.possibleNextStatus = derivat.getStatus().getNextStatus();
        this.derivatStatusNames = derivatStatusNames;
    }

    public Collection<DerivatStatus> getPossibleNextStatus() {
        return possibleNextStatus;
    }

    public Derivat getDerivat() {
        return derivat;
    }

    public String getStatusChangeNameForStatus(DerivatStatus derivatStatus) {
        return derivatStatusNames.getStatusChangeNameForStatus(derivatStatus);
    }

}
