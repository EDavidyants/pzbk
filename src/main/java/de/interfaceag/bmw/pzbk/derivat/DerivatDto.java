package de.interfaceag.bmw.pzbk.derivat;

import de.interfaceag.bmw.pzbk.shared.PersistedObject;

/**
 *
 * @author sl
 */
public interface DerivatDto extends PersistedObject {

    String getName();

    DerivatStatus getStatus();

    boolean isVereinbarungActive();

}
