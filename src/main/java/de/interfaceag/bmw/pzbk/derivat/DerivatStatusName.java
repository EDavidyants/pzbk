package de.interfaceag.bmw.pzbk.derivat;

/**
 *
 * @author sl
 */
public class DerivatStatusName {

    private final DerivatStatus status;
    private final String name;
    private final String statusChangeName;

    public DerivatStatusName(DerivatStatus status, String name, String statusChangeName) {
        this.status = status;
        this.name = name;
        this.statusChangeName = statusChangeName;
    }

    public DerivatStatus getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public String getStatusChangeName() {
        return statusChangeName;
    }

}
