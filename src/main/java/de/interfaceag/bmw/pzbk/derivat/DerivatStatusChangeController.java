package de.interfaceag.bmw.pzbk.derivat;

/**
 *
 * @author sl
 */
public interface DerivatStatusChangeController {

    String changeDerivatStatus(DerivatStatus nextStatus);

}
