package de.interfaceag.bmw.pzbk.derivat;

import java.io.Serializable;
import java.util.Collection;
import java.util.Optional;

/**
 *
 * @author sl
 */
public class DerivatStatusNames implements Serializable {

    private final Collection<DerivatStatusName> statusNames;

    public DerivatStatusNames(Collection<DerivatStatusName> statusNames) {
        this.statusNames = statusNames;
    }

    public String getNameForStatus(DerivatStatus status) {
        Optional<DerivatStatusName> statusNameForStatus = getStatusNameForStatus(status);
        if (statusNameForStatus.isPresent()) {
            DerivatStatusName get = statusNameForStatus.get();
            return get.getName();
        } else {
            return "";
        }
    }

    public String getStatusChangeNameForStatus(DerivatStatus status) {
        Optional<DerivatStatusName> statusNameForStatus = getStatusNameForStatus(status);
        if (statusNameForStatus.isPresent()) {
            DerivatStatusName get = statusNameForStatus.get();
            return get.getStatusChangeName();
        } else {
            return "";
        }
    }

    private Optional<DerivatStatusName> getStatusNameForStatus(DerivatStatus status) {
        return statusNames.stream().filter(statusName -> statusName.getStatus().equals(status)).findAny();
    }

}
