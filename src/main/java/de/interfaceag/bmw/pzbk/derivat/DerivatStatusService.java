package de.interfaceag.bmw.pzbk.derivat;

import de.interfaceag.bmw.pzbk.localization.LocalizationService;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

/**
 *
 * @author sl
 */
@Stateless
@Named
public class DerivatStatusService {

    @Inject
    private LocalizationService localizationService;

    @Produces
    public DerivatStatusNames getDerivatStatusNames() {
        Collection<DerivatStatusName> derivatStatusNames = new ArrayList<>();
        Stream.of(DerivatStatus.values()).forEach(status -> derivatStatusNames.add(getStatusNameForStatus(status)));
        return new DerivatStatusNames(derivatStatusNames);
    }

    private DerivatStatusName getStatusNameForStatus(DerivatStatus status) {
        String name = getNameForStatus(status);
        String statusName = getStatusChangeNameForStatus(status);
        return new DerivatStatusName(status, name, statusName);
    }

    private String getNameForStatus(DerivatStatus status) {
        String localizationName = status.getLocalizationName();
        return localizationService.getValue(localizationName);
    }

    public String getStatusChangeNameForStatus(DerivatStatus status) {
        String statusChangeLocalizationName = status.getStatusChangeLocalizationName();
        return localizationService.getValue(statusChangeLocalizationName);
    }

}
