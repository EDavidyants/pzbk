package de.interfaceag.bmw.pzbk.themenklammer;

import de.interfaceag.bmw.pzbk.shared.objectIds.ThemenklammerId;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Objects;

public class ThemenklammerDto {

    private final Long id;
    private final String currentBezeichnung;
    private String bezeichnung;

    public ThemenklammerDto() {
        this.id = null;
        this.currentBezeichnung = null;
    }

    public ThemenklammerDto(String bezeichnung) {
        this.id = null;
        this.bezeichnung = bezeichnung;
        this.currentBezeichnung = bezeichnung;
    }

    public ThemenklammerDto(Long id, String bezeichnung) {
        this.id = id;
        this.bezeichnung = bezeichnung;
        this.currentBezeichnung = bezeichnung;
    }

    @Override
    public String toString() {
        return "ThemenklammerDto{" +
                "id=" + id +
                ", currentBezeichnung='" + currentBezeichnung + '\'' +
                ", bezeichnung='" + bezeichnung + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        ThemenklammerDto that = (ThemenklammerDto) object;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(currentBezeichnung, that.currentBezeichnung)
                .append(bezeichnung, that.bezeichnung)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(currentBezeichnung)
                .append(bezeichnung)
                .toHashCode();
    }

    public boolean isNew() {
        return Objects.isNull(id) && Objects.nonNull(bezeichnung) && !bezeichnung.trim().isEmpty();
    }

    public boolean isChanged() {
        return Objects.nonNull(id) && !Objects.equals(currentBezeichnung, bezeichnung);
    }

    public ThemenklammerId getThemenklammerId() {
        return new ThemenklammerId(id);
    }

    public Long getId() {
        return id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
}
