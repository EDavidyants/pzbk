package de.interfaceag.bmw.pzbk.themenklammer;

import de.interfaceag.bmw.pzbk.entities.Themenklammer;
import de.interfaceag.bmw.pzbk.shared.objectIds.ThemenklammerId;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Stateless
public class ThemenklammerService implements Serializable {

    @Inject
    private ThemenklammerDao themenklammerDao;

    public Optional<Themenklammer> find(ThemenklammerId themenklammerId) {
        return themenklammerDao.find(themenklammerId);
    }

    public void saveDtos(Collection<ThemenklammerDto> themenklammerDtos) {
        for (ThemenklammerDto themenklammerDto : themenklammerDtos) {
            saveDto(themenklammerDto);
        }
    }

    private void saveDto(ThemenklammerDto themenklammerDto) {
        if (themenklammerDto.isNew()) {
            saveAsNewThemenklammer(themenklammerDto);
        } else if (themenklammerDto.isChanged()) {
            updateThemenklammer(themenklammerDto);
        }
    }

    private void updateThemenklammer(ThemenklammerDto themenklammerDto) {
        final Optional<Themenklammer> themenklammerOptional = themenklammerDao.find(themenklammerDto.getThemenklammerId());
        if (themenklammerOptional.isPresent()) {
            themenklammerOptional.ifPresent(themenklammer -> updateThemenklammer(themenklammerDto, themenklammer));
        }
    }

    private void updateThemenklammer(ThemenklammerDto themenklammerDto, Themenklammer themenklammer1) {
        final String bezeichnung = themenklammerDto.getBezeichnung();
        themenklammer1.setBezeichnung(bezeichnung);
        save(themenklammer1);
    }

    private void saveAsNewThemenklammer(ThemenklammerDto themenklammerDto) {
        final String bezeichnung = themenklammerDto.getBezeichnung();
        Themenklammer newThemenklammer = new Themenklammer(bezeichnung);
        save(newThemenklammer);
    }

    private void save(Themenklammer themenklammer) {
        themenklammerDao.save(themenklammer);
    }

    @Produces
    public List<ThemenklammerDto> getAll() {
        final List<Themenklammer> all = themenklammerDao.getAll();
        return all.stream()
                .map(themenklammer -> new ThemenklammerDto(themenklammer.getId(), themenklammer.getBezeichnung()))
                .collect(Collectors.toList());
    }


}
