package de.interfaceag.bmw.pzbk.themenklammer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Stateless
public class ThemenklammerTestdataService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ThemenklammerTestdataService.class);

    @Inject
    private ThemenklammerService themenklammerService;

    public void generateTestdata() {
        LOG.debug("Start Themenklammer test data generation");

        final List<ThemenklammerDto> all = getThemenklammerDtos();
        if (all.isEmpty()) {
            generateThemenklammerTestdata();
        } else {
            LOG.debug("No Themenklammern generated. Already {} entries in database.", all.size());
        }
    }

    private List<ThemenklammerDto> getThemenklammerDtos() {
        return themenklammerService.getAll();
    }

    private void generateThemenklammerTestdata() {
        LOG.debug("Generate Themenklammer test data");

        Collection<ThemenklammerDto> themenklammern = new ArrayList<>();

        themenklammern.add(new ThemenklammerDto("Aeroblade"));
        themenklammern.add(new ThemenklammerDto("Allgemein"));
        themenklammern.add(new ThemenklammerDto("Anbauteile"));
        themenklammern.add(new ThemenklammerDto("Befestigung"));
        themenklammern.add(new ThemenklammerDto("Einstellung"));
        themenklammern.add(new ThemenklammerDto("Kennzeichnung"));
        themenklammern.add(new ThemenklammerDto("Heckspoiler"));
        themenklammern.add(new ThemenklammerDto("Heckspoiler - Lieferumfang"));
        themenklammern.add(new ThemenklammerDto("Fertigungs und Montageirgendwas"));

        themenklammerService.saveDtos(themenklammern);
    }

}
