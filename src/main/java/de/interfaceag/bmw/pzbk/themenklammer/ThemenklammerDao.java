package de.interfaceag.bmw.pzbk.themenklammer;

import de.interfaceag.bmw.pzbk.entities.Themenklammer;
import de.interfaceag.bmw.pzbk.shared.objectIds.ThemenklammerId;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * @author evda
 */
@Dependent
public class ThemenklammerDao implements Serializable {

    @PersistenceContext(unitName = "de.interfaceag.bmw.pzbk")
    private EntityManager entityManager;

    public Optional<Themenklammer> find(ThemenklammerId themenklammerId) {
        return Optional.ofNullable(entityManager.find(Themenklammer.class, themenklammerId.getId()));
    }

    public void save(Themenklammer themenklammer) {
        if (themenklammer.getId() != null) {
            entityManager.merge(themenklammer);
        } else {
            entityManager.persist(themenklammer);
        }
        entityManager.flush();
    }

    public List<Themenklammer> getAll() {
        TypedQuery<Themenklammer> typedQuery = entityManager.createNamedQuery(Themenklammer.GET_ALL, Themenklammer.class);
        return typedQuery.getResultList();
    }

}
