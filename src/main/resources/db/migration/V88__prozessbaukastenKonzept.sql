CREATE SEQUENCE konzept_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE konzept (
    id bigint NOT NULL,
    bezeichnung character varying(1000),
    beschreibung character varying(1000),
    anhang_id bigint NOT NULL   
);

ALTER TABLE ONLY konzept
    ADD CONSTRAINT konzept_pkey PRIMARY KEY (id);
	
ALTER TABLE ONLY konzept
    ADD CONSTRAINT fk_konzept_anhang_id FOREIGN KEY (anhang_id) REFERENCES anhang(id);

		
CREATE TABLE prozessbaukasten_konzept (
    prozessbaukasten_id bigint NOT NULL,
    konzepte_id bigint NOT NULL
);

ALTER TABLE ONLY prozessbaukasten_konzept
    ADD CONSTRAINT fk_prozessbaukasten_konzept_id FOREIGN KEY (konzepte_id) REFERENCES konzept(id);
	
ALTER TABLE ONLY prozessbaukasten_konzept
    ADD CONSTRAINT fk_konzept_prozessbaukasten_id FOREIGN KEY (prozessbaukasten_id) REFERENCES prozessbaukasten(id);