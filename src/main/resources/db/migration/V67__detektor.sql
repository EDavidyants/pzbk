CREATE TABLE anforderung_detektor
(
  anforderung_id bigint,
  detektor character varying(255),
  CONSTRAINT fk_anforderung_detektor_sanforderung_id FOREIGN KEY (anforderung_id)
      REFERENCES anforderung (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE meldung_detektor
(
  meldung_id bigint,
  detektor character varying(255),
  CONSTRAINT fk_meldung_detektor_sanforderung_id FOREIGN KEY (meldung_id)
      REFERENCES meldung (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);