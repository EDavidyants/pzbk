CREATE SEQUENCE prozessbaukasten_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE prozessbaukasten (
    id bigint NOT NULL,
    fachid character varying(10) NOT NULL,
    version integer NOT NULL,
    bezeichnung character varying(255) NOT NULL,
    beschreibung character varying(10000),
    tteam_id bigint,
    leadTechnologie_id bigint,
    standardisierterFertigungsprozess character varying(10000),
    erstellungsdatum timestamp without time zone,
    aenderungsdatum timestamp without time zone

);

ALTER TABLE ONLY prozessbaukasten
    ADD CONSTRAINT prozessbaukasten_pkey PRIMARY KEY (id);

ALTER TABLE ONLY prozessbaukasten
    ADD CONSTRAINT unq_prozessbaukasten_0 UNIQUE (fachid, version);

ALTER TABLE ONLY prozessbaukasten
    ADD CONSTRAINT fk_prozessbaukasten_tteam_id FOREIGN KEY (tteam_id) REFERENCES t_team(id);

ALTER TABLE ONLY prozessbaukasten
    ADD CONSTRAINT fk_prozessbaukasten_mitarbeiter_id FOREIGN KEY (leadTechnologie_id) REFERENCES mitarbeiter(id);


