ALTER TABLE derivat ADD COLUMN zielvereinbarung boolean;
-- update existing derivate
UPDATE derivat SET zielvereinbarung = false WHERE zielvereinbarung IS NULL;