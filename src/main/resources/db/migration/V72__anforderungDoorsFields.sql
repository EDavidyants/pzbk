ALTER TABLE anforderung 
    ADD COLUMN ersteller character varying(256);

CREATE TABLE anforderung_referenzsystemlink (
    anforderung_id bigint NOT NULL,
    referenzsystemlink_id bigint NOT NULL
);

ALTER TABLE ONLY anforderung_referenzsystemlink
    ADD CONSTRAINT anforderung_referenzsystemlink_pkey PRIMARY KEY (anforderung_id, referenzsystemlink_id);

ALTER TABLE ONLY anforderung_referenzsystemlink
    ADD CONSTRAINT fk_anforderung_referenzsystemlink_anforderung_id 
    FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);

ALTER TABLE ONLY anforderung_referenzsystemlink
    ADD CONSTRAINT fk_anforderung_referenzsystemlink_referenzsystemlink_id 
    FOREIGN KEY (referenzsystemlink_id) REFERENCES referenz_system_link(id);




