
CREATE TABLE zakdoubleupdate (
    id bigint NOT NULL,
    derivat_id bigint,
    updatedate date,
    ttooluid bigint,
    zakid character varying(255)
);

ALTER TABLE ONLY zakdoubleupdate
    ADD CONSTRAINT zakdoubleupdate_pkey PRIMARY KEY (id);

CREATE SEQUENCE zakdoubleupdate_id_seq INCREMENT BY 1 START WITH 1;

ALTER TABLE ONLY zakdoubleupdate
    ADD CONSTRAINT fk_zakdoubleupdate_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);


