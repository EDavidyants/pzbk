CREATE TABLE fahrzeugmerkmal_modulseteam
(
    fahrzeugmerkmal_id bigint NOT NULL,
    modulseteam_id     bigint NOT NULL
);

ALTER TABLE ONLY fahrzeugmerkmal_modulseteam
    ADD CONSTRAINT fk_fahrzeugmerkmal_modulseteam_fahrzeugmerkmal_id FOREIGN KEY (fahrzeugmerkmal_id) REFERENCES fahrzeugmerkmal(id);

ALTER TABLE ONLY fahrzeugmerkmal_modulseteam
    ADD CONSTRAINT fk_fahrzeugmerkmal_modulseteam_modulseteam_id FOREIGN KEY (modulseteam_id) REFERENCES modulseteam(id);
