
/* Join Table for Anforderung und ReferenzSystemLink */
CREATE TABLE anforderung_referenz_system_link (
    anforderung_id bigint NOT NULL,
    referenzsystemlinks_id bigint NOT NULL
);

ALTER TABLE ONLY anforderung_referenz_system_link
    ADD CONSTRAINT anforderung_referenzsystemlink_pkey PRIMARY KEY (anforderung_id, referenzsystemlinks_id);

ALTER TABLE ONLY anforderung_referenz_system_link
    ADD CONSTRAINT fk_anforderung_referenzsystemlink_anforderung_id 
    FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);

ALTER TABLE ONLY anforderung_referenz_system_link
    ADD CONSTRAINT fk_anforderung_referenzsystemlink_referenzsystemlinks_id 
    FOREIGN KEY (referenzsystemlinks_id) REFERENCES referenz_system_link(id);


/* Delete Column anforderung_id from referenz_system_link Table */
ALTER TABLE ONLY referenz_system_link 
    DROP CONSTRAINT fk_referenzsystemlink_anforderung_id;

ALTER TABLE ONLY referenz_system_link 
    DROP COLUMN anforderung_id;
