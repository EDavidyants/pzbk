TRUNCATE TABLE derivatanforderungmodul CASCADE;
TRUNCATE TABLE umsetzungsbestaetigung CASCADE;

ALTER TABLE derivatanforderungmodul ADD COLUMN zuordnunganforderungderivat_id bigint NOT NULL;
ALTER TABLE derivatanforderungmodul DROP COLUMN anforderung_id;
ALTER TABLE derivatanforderungmodul DROP COLUMN derivat_id;

ALTER TABLE ONLY derivatanforderungmodul ADD CONSTRAINT unq_derivatanforderungmodul UNIQUE (zuordnunganforderungderivat_id, modul_id);

ALTER TABLE ONLY derivatanforderungmodul
    ADD CONSTRAINT fk_derivatanforderungmodul_zuordnunganforderungderivat_id FOREIGN KEY (zuordnunganforderungderivat_id) REFERENCES zuordnunganforderungderivat(id);