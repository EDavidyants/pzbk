CREATE SEQUENCE tteam_vertreter_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE tteamvertreter (
    id bigint NOT NULL,
    tteam_id bigint,
    mitarbeiter_id bigint
);

ALTER TABLE ONLY tteamvertreter
    ADD CONSTRAINT tteamvertreter_pkey PRIMARY KEY (id);
	
ALTER TABLE ONLY tteamvertreter
    ADD CONSTRAINT fk_tteamvertreter_tteam_id FOREIGN KEY (tteam_id) REFERENCES t_team(id);

ALTER TABLE ONLY tteamvertreter
    ADD CONSTRAINT fk_tteamvertreter_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);