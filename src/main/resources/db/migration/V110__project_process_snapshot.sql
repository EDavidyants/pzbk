CREATE SEQUENCE projectprocesssnapshot_id_seq INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE projectprocesssnapshotentry_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE projectprocesssnapshotentry (
    id bigint NOT NULL,
    keyfigure integer NOT NULL,
    value integer NOT NULL
);

ALTER TABLE ONLY projectprocesssnapshotentry
    ADD CONSTRAINT projectprocesssnapshotentry_pkey PRIMARY KEY (id);




CREATE TABLE projectprocesssnapshot (
    id bigint NOT NULL,
    datum timestamp without time zone NOT NULL,
    derivat_id bigint NOT NULL
);

ALTER TABLE ONLY projectprocesssnapshot
    ADD CONSTRAINT projectprocesssnapshot_pkey PRIMARY KEY (id);

ALTER TABLE ONLY projectprocesssnapshot
    ADD CONSTRAINT fk_projectprocesssnapshot_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);




CREATE TABLE projectprocesssnapshot_projectprocesssnapshotentry (
    projectprocesssnapshot_id bigint NOT NULL,
    entries_id bigint NOT NULL
);

ALTER TABLE ONLY projectprocesssnapshot_projectprocesssnapshotentry
    ADD CONSTRAINT fk_statusabgleichsnapshot_id FOREIGN KEY (projectprocesssnapshot_id) REFERENCES projectprocesssnapshot(id);

ALTER TABLE ONLY projectprocesssnapshot_projectprocesssnapshotentry
    ADD CONSTRAINT fk_entries_id FOREIGN KEY (entries_id) REFERENCES projectprocesssnapshotentry(id);