CREATE TABLE zakupdate (
    id bigint NOT NULL,
    derivat_id bigint,
    zakurl character varying(1000),
    updatedfrom timestamp without time zone,
    status character varying(1000),
    zakResponse bytea
);

ALTER TABLE ONLY zakupdate
    ADD CONSTRAINT zakupdate_pkey PRIMARY KEY (id);


ALTER TABLE ONLY zakupdate
    ADD CONSTRAINT fk_zakupdate_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);

CREATE SEQUENCE zakupdate_id_seq INCREMENT BY 1 START WITH 1;
