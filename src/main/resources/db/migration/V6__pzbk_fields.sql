CREATE TABLE pzbk_umsetzer (
    pzbk_id bigint NOT NULL,
    umsetzer_umsetzer_id bigint NOT NULL
);

CREATE TABLE pzbk_anhang (
    pzbk_id bigint NOT NULL,
    anhaenge_id bigint NOT NULL
);

ALTER TABLE pzbk 
    ADD COLUMN titel character varying(255);

ALTER TABLE pzbk 
    ADD COLUMN beschreibung character varying(10000);

ALTER TABLE pzbk 
    ADD COLUMN status integer;

ALTER TABLE pzbk 
    ADD COLUMN version integer;

ALTER TABLE pzbk 
    ADD COLUMN tteam_id bigint;

ALTER TABLE pzbk 
    ADD COLUMN sensorcoc_sensorcocid bigint;

ALTER TABLE pzbk 
    ADD COLUMN zieleinsatzDerivat_id bigint;

ALTER TABLE pzbk 
    ADD COLUMN bild_id bigint;

ALTER TABLE ONLY pzbk
    ADD CONSTRAINT fk_pzbk_tteam_id FOREIGN KEY (tteam_id) REFERENCES t_team(id);

ALTER TABLE ONLY pzbk
    ADD CONSTRAINT fk_pzbk_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);

ALTER TABLE ONLY pzbk
    ADD CONSTRAINT fk_pzbk_derivat_id FOREIGN KEY (zieleinsatzDerivat_id) REFERENCES derivat(id);

ALTER TABLE ONLY pzbk
    ADD CONSTRAINT fk_pzbk_bild_id FOREIGN KEY (bild_id) REFERENCES bild(id);


ALTER TABLE ONLY pzbk_umsetzer
    ADD CONSTRAINT pzbk_umsetzer_pkey PRIMARY KEY (pzbk_id, umsetzer_umsetzer_id);

ALTER TABLE ONLY pzbk_umsetzer
    ADD CONSTRAINT fk_pzbk_umsetzer_pzbk_id FOREIGN KEY (pzbk_id) REFERENCES pzbk(id);

ALTER TABLE ONLY pzbk_umsetzer
    ADD CONSTRAINT fk_pzbk_umsetzer_umsetzer_umsetzer_id FOREIGN KEY (umsetzer_umsetzer_id) REFERENCES umsetzer(umsetzer_id);


ALTER TABLE ONLY pzbk_anhang
    ADD CONSTRAINT pzbk_anhang_pkey PRIMARY KEY (pzbk_id, anhaenge_id);

ALTER TABLE ONLY pzbk_anhang
    ADD CONSTRAINT fk_pzbk_anhang_pzbk_id FOREIGN KEY (pzbk_id) REFERENCES pzbk(id);

ALTER TABLE ONLY pzbk_anhang
    ADD CONSTRAINT fk_pzbk_anhang_anhaenge_id FOREIGN KEY (anhaenge_id) REFERENCES anhang(id);
