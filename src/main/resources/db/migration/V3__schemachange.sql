/* 
 * Attribute 'designrelevant' is deleted from tables 'Meldung' and 'Anforderung'
 */
/**
 * Author:  evda
 * Created: 24.07.2017
 */
ALTER TABLE meldung DROP COLUMN designrelevant;

ALTER TABLE anforderung DROP COLUMN designrelevant;