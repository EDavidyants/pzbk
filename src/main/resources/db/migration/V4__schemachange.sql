/* 
 * Records with attribut = 'EINHEIT' are deleted from Table wert
 *  
 */
/**
 * Author:  evda
 * Created: 26.07.2017
 */

DELETE FROM wert w WHERE w.attribut LIKE 'EINHEIT';

AlTER TABLE derivat_kov_a_phase_im_derivat 
DROP CONSTRAINT IF EXISTS fk_derivat_kov_a_phase_im_derivat_kovaphasen_id;

DROP TABLE IF EXISTS derivat_kov_a_phase_im_derivat;

ALTER TABLE usersettings_selectedderivate 
DROP CONSTRAINT IF EXISTS fk_usersettings_selectedderivate_usersettings_id;

DROP TABLE IF EXISTS usersettings_selectedderivate;

ALTER TABLE usersettings_selectedzakderivate 
DROP CONSTRAINT IF EXISTS fk_usersettings_selectedzakderivate_usersettings_id;

DROP TABLE IF EXISTS usersettings_selectedzakderivate;

ALTER TABLE anforderung DROP COLUMN fachlicheransprechpartner_id;

ALTER TABLE anforderung DROP COLUMN teamleiter_id;

ALTER TABLE meldung DROP COLUMN fachlicheransprechpartner_id;

ALTER TABLE meldung DROP COLUMN teamleiter_id;

ALTER TABLE t_team DROP COLUMN teamleiter_id;

ALTER TABLE sensorcoc DROP COLUMN fachlicheransprechpartner_id;

DROP TABLE IF EXISTS sensorcoc_mitarbeiter;

