CREATE TABLE derivat_fahrzeugmerkmal (
    derivat_id bigint NOT NULL, 
    fahrzeugmerkmale_id bigint NOT NULL
);

ALTER TABLE ONLY fahrzeugmerkmal
    ADD CONSTRAINT fahrzeugmerkmal_pkey PRIMARY KEY (id);

ALTER TABLE ONLY derivat_fahrzeugmerkmal
    ADD CONSTRAINT derivat_fahrzeugmerkmal_pkey PRIMARY KEY (derivat_id, fahrzeugmerkmale_id);

ALTER TABLE ONLY derivat_fahrzeugmerkmal
    ADD CONSTRAINT fk_derivat_fahrzeugmerkmal_fahrzeugmerkmale_id FOREIGN KEY (fahrzeugmerkmale_id) REFERENCES fahrzeugmerkmal(id);

ALTER TABLE ONLY derivat_fahrzeugmerkmal
    ADD CONSTRAINT fk_derivat_fahrzeugmerkmal_derivat_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);