ALTER TABLE derivatanforderungmodul ADD COLUMN latesthistoryentry_id bigint;

ALTER TABLE derivatanforderungmodulhistory ADD CONSTRAINT derivatanforderungmodulhistory_pkey PRIMARY KEY (id);

ALTER TABLE ONLY derivatanforderungmodul
ADD CONSTRAINT fk_derivatanforderungmodul_derivatanforderungmodulhistory_id 
FOREIGN KEY (latesthistoryentry_id) REFERENCES derivatanforderungmodulhistory(id);
