CREATE SEQUENCE urlencoding_id_seq INCREMENT BY 1 START WITH 10000;

CREATE TABLE urlencoding (
    id bigint PRIMARY KEY,
    page integer NOT NULL,
    urlid character varying(255) NOT NULL UNIQUE,
    urlparameter character varying(10000) NOT NULL
);