CREATE SEQUENCE tteam_mitglied_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE tteammitglied (
    id bigint NOT NULL,
    tteam_id bigint,
    mitarbeiter_id bigint,
    rechttype integer   
);

ALTER TABLE ONLY tteammitglied
    ADD CONSTRAINT tteammitglied_pkey PRIMARY KEY (id);
	
ALTER TABLE ONLY tteammitglied
    ADD CONSTRAINT fk_tteammitglied_tteam_id FOREIGN KEY (tteam_id) REFERENCES t_team(id);

ALTER TABLE ONLY tteammitglied
    ADD CONSTRAINT fk_tteammitglied_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);