CREATE TABLE logentry (
    id bigint NOT NULL,
    datum timestamp without time zone NOT NULL,
    loglevel integer NOT NULL,
    systemtype integer NOT NULL,
    logvalue character varying(10000) NOT NULL
);

CREATE SEQUENCE logentry_id_seq INCREMENT BY 1 START WITH 1;