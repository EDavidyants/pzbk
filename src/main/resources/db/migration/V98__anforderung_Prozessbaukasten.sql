ALTER TABLE anforderung ADD COLUMN prozessbaukasten_id bigint;

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_prozessbaukasten_id FOREIGN KEY (prozessbaukasten_id) REFERENCES prozessbaukasten(id);
