CREATE SEQUENCE zuordnunganforderungderivat_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE zuordnunganforderungderivat (
    id bigint NOT NULL,
    anforderung_id bigint NOT NULL,
    derivat_id bigint NOT NULL,
    status integer NOT NULL,
    nachzakuebertragen boolean NOT NULL
);

ALTER TABLE ONLY zuordnunganforderungderivat
    ADD CONSTRAINT zuordnunganforderungderivat_pkey PRIMARY KEY (id);

ALTER TABLE ONLY zuordnunganforderungderivat
    ADD CONSTRAINT fk_zuordnunganforderungderivat_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);

ALTER TABLE ONLY zuordnunganforderungderivat
    ADD CONSTRAINT fk_zuordnunganforderungderivat_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);