ALTER TABLE prozessbaukasten ADD COLUMN erstanlaeufer_id bigint; 

ALTER TABLE ONLY prozessbaukasten
    DROP CONSTRAINT IF EXISTS fk_prozessbaukasten_derivat_id;

ALTER TABLE ONLY prozessbaukasten
    ADD CONSTRAINT fk_prozessbaukasten_derivat_id FOREIGN KEY (erstanlaeufer_id) REFERENCES derivat(id);

