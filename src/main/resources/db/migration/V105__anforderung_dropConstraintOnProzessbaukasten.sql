
ALTER TABLE ONLY anforderung DROP CONSTRAINT IF EXISTS fk_anforderung_prozessbaukasten_id;

ALTER TABLE ONLY anforderung DROP COLUMN IF EXISTS prozessbaukasten_id;
