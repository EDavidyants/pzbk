ALTER TABLE anforderung_freigabe_bei_modul DROP CONSTRAINT fk_anforderung_freigabe_bei_modul_modul_id;

ALTER TABLE anforderung_freigabe_bei_modul RENAME modul_id TO modulseteam_id;

ALTER TABLE ONLY anforderung_freigabe_bei_modul
    ADD CONSTRAINT fk_anforderung_freigabe_bei_modul_modulseteam_id FOREIGN KEY (modulseteam_id) REFERENCES modulseteam(id);