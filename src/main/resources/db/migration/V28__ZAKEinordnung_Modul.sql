-- rename table and column to follow general naming convention
ALTER TABLE zak_uebertragung RENAME TO zakuebertragung;
ALTER TABLE zakuebertragung RENAME COLUMN zak_uebertragung_id TO zakuebertragung_id;
-- create new column for modul id
ALTER TABLE zakuebertragung ADD COLUMN modul_id bigint;

-- migrate existing data to new column
UPDATE zakuebertragung SET modul_id = (SELECT m.id FROM modul m WHERE zakuebertragung.modul_name = m.name); 

-- create new fk contraints
ALTER TABLE ONLY zakuebertragung
    ADD CONSTRAINT fk_zakuebertragung_modul_id FOREIGN KEY (modul_id) REFERENCES modul(id);
ALTER TABLE ONLY zakuebertragung
    ADD CONSTRAINT fk_zakuebertragung_derivatanforderung_id FOREIGN KEY (derivatanforderung_id) REFERENCES derivatanforderung(id);

-- drop old column
ALTER TABLE zakuebertragung DROP COLUMN modul_name;