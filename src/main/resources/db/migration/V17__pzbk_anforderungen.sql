CREATE TABLE pzbk_anforderung (
    pzbk_id bigint NOT NULL,
    anforderung_id bigint NOT NULL
);

ALTER TABLE ONLY pzbk_anforderung
    ADD CONSTRAINT pzbk_anforderung_pkey PRIMARY KEY (anforderung_id, pzbk_id);

ALTER TABLE ONLY pzbk_anforderung
    ADD CONSTRAINT fk_pzbk_anforderung_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);

ALTER TABLE ONLY pzbk_anforderung
    ADD CONSTRAINT fk_pzbk_anforderung_pzbk_id FOREIGN KEY (pzbk_id) REFERENCES pzbk(id);