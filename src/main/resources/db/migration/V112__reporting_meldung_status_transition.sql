CREATE SEQUENCE reportingmeldungstatustransition_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE reportingmeldungstatustransition (
    id bigint NOT NULL,
    meldung_id integer NOT NULL,
    entrydate timestamp without time zone NOT NULL,
    gemeldet_entry timestamp without time zone,
    gemeldet_exit timestamp without time zone,
    unstimmig_entry timestamp without time zone,
    unstimmig_exit timestamp without time zone,
    geloescht_entry timestamp without time zone,
    geloescht_exit timestamp without time zone,
    newanforderungzugeordnet_entry timestamp without time zone,
    newanforderungzugeordnet_exit timestamp without time zone,
    existinganforderungzugeordnet_entry timestamp without time zone,
    existinganforderungzugeordnet_exit timestamp without time zone
);

ALTER TABLE ONLY reportingmeldungstatustransition
    ADD CONSTRAINT reportingmeldungstatustransition_pkey PRIMARY KEY (id);

ALTER TABLE ONLY reportingmeldungstatustransition
    ADD CONSTRAINT fk_meldung_id FOREIGN KEY (meldung_id) REFERENCES meldung(id);
