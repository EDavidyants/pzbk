CREATE SEQUENCE reportingconfig_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE reportingconfig
(
    id      bigint                  NOT NULL,
    keyword character varying(1000) NOT NULL,
    value   character varying(1000) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO reportingconfig(id, keyword, value)
VALUES (1, 'start_date', '01.02.2018');

INSERT INTO reportingconfig(id, keyword, value)
VALUES (2, 'langlaufer_fachteam', '50');

INSERT INTO reportingconfig(id, keyword, value)
VALUES (3, 'langlaufer_tteam', '50');