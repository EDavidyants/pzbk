
CREATE SEQUENCE IF NOT EXISTS themenklammer_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE IF NOT EXISTS themenklammer (
    id bigint NOT NULL,
    bezeichnung character varying(255)
);

ALTER TABLE ONLY themenklammer
    ADD CONSTRAINT themenklammer_pkey PRIMARY KEY (id);

CREATE SEQUENCE IF NOT EXISTS prozessbaukasten_themenklammer_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE IF NOT EXISTS prozessbaukastenthemenklammer (
    id bigint NOT NULL,
    prozessbaukasten_id bigint NOT NULL,
    themenklammer_id bigint NOT NULL
);

ALTER TABLE ONLY prozessbaukastenthemenklammer
    ADD CONSTRAINT prozessbaukastenthemenklammer_pkey PRIMARY KEY (id);

ALTER TABLE ONLY prozessbaukastenthemenklammer
    ADD CONSTRAINT fk_prozessbaukastenthemenklammer_prozessbaukasten_id 
    FOREIGN KEY (prozessbaukasten_id) 
    REFERENCES prozessbaukasten(id);


ALTER TABLE ONLY prozessbaukastenthemenklammer
    ADD CONSTRAINT fk_prozessbaukastenthemenklammer_themenklammer_id 
    FOREIGN KEY (themenklammer_id) 
    REFERENCES themenklammer(id);


CREATE TABLE IF NOT EXISTS anforderung_prozessbaukastenthemenklammer (
    anforderung_id bigint NOT NULL,
    prozessbaukastenthemenklammer_id bigint NOT NULL
);

ALTER TABLE ONLY anforderung_prozessbaukastenthemenklammer
    ADD CONSTRAINT anforderung_prozessbaukastenthemenklammer_pkey 
    PRIMARY KEY (anforderung_id, prozessbaukastenthemenklammer_id);

ALTER TABLE ONLY anforderung_prozessbaukastenthemenklammer
    ADD CONSTRAINT fk_anforderung_prozessbaukastenthemenklammer_anforderung_id 
    FOREIGN KEY (anforderung_id) 
    REFERENCES anforderung(id);

ALTER TABLE ONLY anforderung_prozessbaukastenthemenklammer
    ADD CONSTRAINT fk_anforderung_prozessbaukastenthemenklammer_prozessbaukastenthemenklammer_id 
    FOREIGN KEY (prozessbaukastenthemenklammer_id) 
    REFERENCES prozessbaukastenthemenklammer(id);










