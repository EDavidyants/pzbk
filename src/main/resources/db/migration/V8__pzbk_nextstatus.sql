ALTER TABLE pzbk 
    ADD COLUMN nextstatus integer;

ALTER TABLE pzbk 
    ADD COLUMN statuschangerequested boolean;

ALTER TABLE pzbk 
    ADD COLUMN statuswechselkommentar character varying(255);