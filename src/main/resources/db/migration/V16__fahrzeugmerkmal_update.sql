DROP TABLE derivat_fahrzeugmerkmal CASCADE;
DROP TABLE fahrzeugmerkmal CASCADE;

CREATE TABLE fahrzeugmerkmal (
    id bigint NOT NULL,
    merkmal character varying(1000)
);
ALTER TABLE ONLY fahrzeugmerkmal
    ADD CONSTRAINT fahrzeugmerkmal_pkey PRIMARY KEY (id);

CREATE TABLE fahrzeugmerkmalwert (
    id bigint NOT NULL,
    fahrzeugmerkmal_id bigint,
    wert character varying(1000)
);
ALTER TABLE ONLY fahrzeugmerkmalwert
    ADD CONSTRAINT fahrzeugmerkmalwert_pkey PRIMARY KEY (id);
ALTER TABLE ONLY fahrzeugmerkmalwert
    ADD CONSTRAINT fk_fahrzeugmerkmalwert_fahrzeugmerkmal_id FOREIGN KEY (fahrzeugmerkmal_id) REFERENCES fahrzeugmerkmal(id);

CREATE TABLE fahrzeugmerkmal_fahrzeugmerkmalwert (
    fahrzeugmerkmal_id bigint NOT NULL, 
    werte_id bigint NOT NULL
);
ALTER TABLE ONLY fahrzeugmerkmal_fahrzeugmerkmalwert
    ADD CONSTRAINT fahrzeugmerkmal_fahrzeugmerkmalwert_pkey PRIMARY KEY (fahrzeugmerkmal_id, werte_id);
ALTER TABLE ONLY fahrzeugmerkmal_fahrzeugmerkmalwert
    ADD CONSTRAINT fk_fahrzeugmerkmal_fahrzeugmerkmalwert_fahrzeugmerkmal_id FOREIGN KEY (fahrzeugmerkmal_id) REFERENCES fahrzeugmerkmal(id);
ALTER TABLE ONLY fahrzeugmerkmal_fahrzeugmerkmalwert
    ADD CONSTRAINT fk_fahrzeugmerkmal_fahrzeugmerkmalwert_werte_id FOREIGN KEY (werte_id) REFERENCES fahrzeugmerkmalwert(id);


CREATE TABLE derivat_fahrzeugmerkmalwert (
    derivat_id bigint NOT NULL, 
    fahrzeugmerkmale_id bigint NOT NULL
);
ALTER TABLE ONLY derivat_fahrzeugmerkmalwert
    ADD CONSTRAINT derivat_fahrzeugmerkmal_pkey PRIMARY KEY (derivat_id, fahrzeugmerkmale_id);
ALTER TABLE ONLY derivat_fahrzeugmerkmalwert
    ADD CONSTRAINT fk_derivat_fahrzeugmerkmal_fahrzeugmerkmale_id FOREIGN KEY (fahrzeugmerkmale_id) REFERENCES fahrzeugmerkmalwert(id);
ALTER TABLE ONLY derivat_fahrzeugmerkmalwert
    ADD CONSTRAINT fk_derivat_fahrzeugmerkmal_derivat_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);
