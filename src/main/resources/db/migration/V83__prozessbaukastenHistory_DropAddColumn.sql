ALTER TABLE ONLY prozessbaukasten_history 
    DROP COLUMN IF EXISTS fachid;

ALTER TABLE ONLY prozessbaukasten_history 
    DROP COLUMN IF EXISTS version;


ALTER TABLE ONLY prozessbaukasten_history 
    ADD COLUMN prozessbaukasten_id bigint;

ALTER TABLE ONLY prozessbaukasten_history
    ADD CONSTRAINT fk_prozessbaukastenhistory_prozessbaukasten_id 
    FOREIGN KEY (prozessbaukasten_id) REFERENCES prozessbaukasten(id);

