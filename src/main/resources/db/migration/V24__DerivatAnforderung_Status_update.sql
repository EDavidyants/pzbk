ALTER TABLE derivatanforderung 
    RENAME status TO statusstring;

ALTER TABLE derivatanforderung 
    ADD COLUMN status integer;

UPDATE derivatanforderung SET status = 1
                    WHERE statusstring LIKE 'zugeordnet';

UPDATE derivatanforderung SET status = 5
                    WHERE statusstring LIKE 'ZAK | vereinbart';

UPDATE derivatanforderung SET status = 5
                    WHERE statusstring LIKE '%offen';

UPDATE derivatanforderung SET status = 7
                    WHERE statusstring LIKE '%Bewertung nicht möglich';

UPDATE derivatanforderung SET status = 8
                    WHERE statusstring LIKE '%nicht relevant';

UPDATE derivatanforderung SET status = 9
                    WHERE statusstring LIKE '%nicht umgesetzt';

UPDATE derivatanforderung SET status = 10
                    WHERE statusstring LIKE '%umgesetzt';

UPDATE derivatanforderung SET status = 11
                    WHERE statusstring LIKE '%keine Weiterverfolgung';

UPDATE derivatanforderung SET status = 12
                    WHERE statusstring LIKE '%und Vereinbarung der Umsetzung';


ALTER TABLE derivatanforderung DROP COLUMN statusstring;