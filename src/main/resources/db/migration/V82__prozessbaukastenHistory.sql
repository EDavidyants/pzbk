CREATE SEQUENCE prozessbaukastenhistory_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE prozessbaukasten_history (
    id bigint NOT NULL PRIMARY KEY,
    fachid character varying(10),
    version integer,
    datum timestamp without time zone,
    changedfield character varying(255),
    benutzer character varying(255),
    von character varying(10000),
    auf character varying(10000)
);




