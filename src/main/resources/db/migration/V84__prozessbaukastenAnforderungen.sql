ALTER TABLE prozessbaukasten_anforderungen 
    RENAME TO prozessbaukasten_anforderung;

ALTER TABLE prozessbaukasten_anforderung 
    RENAME pzbk_id TO prozessbaukasten_id;

ALTER TABLE prozessbaukasten_anforderung 
    RENAME anforderungs_id TO anforderung_id;

ALTER TABLE ONLY prozessbaukasten_anforderung
    ADD CONSTRAINT prozessbaukasten_anforderung_pkey PRIMARY KEY (prozessbaukasten_id, anforderung_id);

ALTER TABLE ONLY prozessbaukasten_anforderung
    ADD CONSTRAINT fk_prozessbaukasten_anforderung_prozessbaukasten_id 
    FOREIGN KEY (prozessbaukasten_id) REFERENCES prozessbaukasten(id);

ALTER TABLE ONLY prozessbaukasten_anforderung
    ADD CONSTRAINT fk_prozessbaukasten_anforderung_anforderung_id 
    FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);

