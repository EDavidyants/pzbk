CREATE SEQUENCE IF NOT EXISTS derivatfahrzeugmerkmal_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE IF NOT EXISTS derivatfahrzeugmerkmal
(
    id                 bigint NOT NULL,
    derivat_id         bigint NOT NULL,
    fahrzeugmerkmal_id bigint NOT NULL,
    isnotrelevant      boolean
);

ALTER TABLE ONLY derivatfahrzeugmerkmal
    ADD CONSTRAINT derivatfahrzeugmerkmal_pkey PRIMARY KEY (id);

ALTER TABLE ONLY derivatfahrzeugmerkmal
    ADD CONSTRAINT fk_derivatfahrzeugmerkmal_derivat_id
        FOREIGN KEY (derivat_id)
            REFERENCES derivat(id);

ALTER TABLE ONLY derivatfahrzeugmerkmal
    ADD CONSTRAINT fk_derivatfahrzeugmerkmal_fahrzeugmerkmal_id
        FOREIGN KEY (fahrzeugmerkmal_id)
            REFERENCES fahrzeugmerkmal(id);


CREATE TABLE if not exists derivatfahrzeugmerkmal_fahrzeugmerkmalauspraegung
(
    derivatfahrzeugmerkmal_id     bigint,
    fahrzeugmerkmalauspraegung_id bigint
);


ALTER TABLE ONLY derivatfahrzeugmerkmal_fahrzeugmerkmalauspraegung
    ADD CONSTRAINT fk_derivatfahrzeugmerkmal_fahrzeugmerkmalauspraegung_fahrzeugmerkmalauspraegung_id
        FOREIGN KEY (fahrzeugmerkmalauspraegung_id)
            REFERENCES fahrzeugmerkmalauspraegung(id);

ALTER TABLE ONLY derivatfahrzeugmerkmal_fahrzeugmerkmalauspraegung
    ADD CONSTRAINT fk_derivatfahrzeugmerkmal_fahrzeugmerkmalauspraegung_derivatfahrzeugmerkmal_id
        FOREIGN KEY (derivatfahrzeugmerkmal_id)
            REFERENCES derivatfahrzeugmerkmal(id);










