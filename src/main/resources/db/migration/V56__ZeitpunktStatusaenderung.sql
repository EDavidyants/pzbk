ALTER TABLE anforderung ADD COLUMN zeitpunktstatusaenderung timestamp without time zone;
ALTER TABLE meldung ADD COLUMN zeitpunktstatusaenderung timestamp without time zone;