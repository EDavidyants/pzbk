CREATE SEQUENCE reportingstatustransition_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE reportingstatustransition (
    id bigint NOT NULL,
    anforderung_id integer NOT NULL,
    entrydate timestamp without time zone NOT NULL,
    inarbeit_entry timestamp without time zone,
    inarbeit_exit timestamp without time zone,
    abgestimmt_entry timestamp without time zone,
    abgestimmt_exit timestamp without time zone,
    plausibilisiert_entry timestamp without time zone,
    plausibilisiert_exit timestamp without time zone,
    freigegeben_entry timestamp without time zone,
    freigegeben_exit timestamp without time zone,
    keineweiterverfolgung_entry timestamp without time zone,
    keineweiterverfolgung_exit timestamp without time zone,
    fachteam_unstimmig_entry timestamp without time zone,
    fachteam_unstimmig_exit timestamp without time zone,
    tteam_unstimmig_entry timestamp without time zone,
    tteam_unstimmig_exit timestamp without time zone,
    vereinbarung_unstimmig_entry timestamp without time zone,
    vereinbarung_unstimmig_exit timestamp without time zone,
    fachteam_geloescht_entry timestamp without time zone,
    fachteam_geloescht_exit timestamp without time zone,
    tteam_geloescht_entry timestamp without time zone,
    tteam_geloescht_exit timestamp without time zone,
    vereinbarung_geloescht_entry timestamp without time zone,
    vereinbarung_geloescht_exit timestamp without time zone,
    freigegeben_geloescht_entry timestamp without time zone,
    freigegeben_geloescht_exit timestamp without time zone,
    fachteam_prozessbaukasten_entry timestamp without time zone,
    fachteam_prozessbaukasten_exit timestamp without time zone,
    tteam_prozessbaukasten_entry timestamp without time zone,
    tteam_prozessbaukasten_exit timestamp without time zone,
    vereinbarung_prozessbaukasten_entry timestamp without time zone,
    vereinbarung_prozessbaukasten_exit timestamp without time zone,
    freigegeben_prozessbaukasten_entry timestamp without time zone,
    freigegeben_prozessbaukasten_exit timestamp without time zone
);

ALTER TABLE ONLY reportingstatustransition
    ADD CONSTRAINT reportingstatustransition_pkey PRIMARY KEY (id);

ALTER TABLE ONLY reportingstatustransition
    ADD CONSTRAINT fk_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);
