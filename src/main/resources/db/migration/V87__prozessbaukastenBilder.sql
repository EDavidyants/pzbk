ALTER TABLE ONLY prozessbaukasten ADD COLUMN grafikumfangbild_id bigint;
ALTER TABLE ONLY prozessbaukasten ADD COLUMN konzeptbaumbild_id bigint;

ALTER TABLE ONLY prozessbaukasten ADD CONSTRAINT fk_prozessbaukasten_konzeptbaumbild_id 
FOREIGN KEY (konzeptbaumbild_id) REFERENCES bild(id);

ALTER TABLE ONLY prozessbaukasten ADD CONSTRAINT fk_prozessbaukasten_grafikumfangbild_id 
FOREIGN KEY (grafikumfangbild_id) REFERENCES bild(id);