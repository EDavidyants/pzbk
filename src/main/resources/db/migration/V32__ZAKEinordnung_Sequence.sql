-- generate new sequence for zakuebertragung
CREATE SEQUENCE zakuebertragung_id_seq
      INCREMENT BY 1
      START WITH 1;

-- grant access for pzbk user
-- GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO pzbk;