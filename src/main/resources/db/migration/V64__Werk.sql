


CREATE TABLE werk (
    id bigint NOT NULL,
    name character varying(255)
);

ALTER TABLE ONLY werk
    ADD CONSTRAINT werk_pkey PRIMARY KEY (id);


CREATE SEQUENCE werk_id_seq INCREMENT BY 1 START WITH 1;

ALTER TABLE meldung ADD COLUMN werk_id bigint;
ALTER TABLE anforderung ADD COLUMN werk_id bigint;

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_werk_id FOREIGN KEY (werk_id) REFERENCES werk(id);

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_werk_id FOREIGN KEY (werk_id) REFERENCES werk(id);