CREATE TABLE derivatanforderunghistory (
    id bigint NOT NULL,
    derivatanforderung_id bigint,
    Status character varying(255),
    datum timestamp without time zone,
    bearbeiter character varying(255),
    kommentar character varying(10000)
);

ALTER TABLE ONLY derivatanforderunghistory
    ADD CONSTRAINT fk_derivatanforderunghistory_derivatanforderung_id FOREIGN KEY (derivatanforderung_id) REFERENCES derivatanforderung(id);