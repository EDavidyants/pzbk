CREATE TABLE prozessbaukasten_anhang (
    prozessbaukasten_id bigint NOT NULL,
    weitereanhaenge_id bigint NOT NULL
);

ALTER TABLE ONLY prozessbaukasten_anhang
    ADD CONSTRAINT prozessbaukasten_anhang_pkey PRIMARY KEY (prozessbaukasten_id, weitereanhaenge_id);

ALTER TABLE ONLY prozessbaukasten_anhang
    ADD CONSTRAINT fk_prozessbaukasten_anhang_prozessbaukasten_id FOREIGN KEY (prozessbaukasten_id) REFERENCES prozessbaukasten(id);

ALTER TABLE ONLY prozessbaukasten_anhang
    ADD CONSTRAINT fk_prozessbaukasten_anhang_anhang_id FOREIGN KEY (weitereanhaenge_id) REFERENCES anhang(id);

