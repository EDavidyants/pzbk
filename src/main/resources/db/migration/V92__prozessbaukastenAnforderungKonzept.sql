 CREATE SEQUENCE prozessbaukasten_anforderung_konzept_id_seq INCREMENT BY 1 START WITH 1;

 CREATE TABLE prozessbaukastenanforderungkonzept (
     id bigint NOT NULL,
     anforderung_id bigint NOT NULL,
     prozessbaukasten_id bigint NOT NULL,
     konzept_id bigint
 );

 ALTER TABLE ONLY prozessbaukastenanforderungkonzept ADD CONSTRAINT prozessbaukastenanforderungkonzept_pkey PRIMARY KEY (id);

 ALTER TABLE ONLY prozessbaukastenanforderungkonzept ADD CONSTRAINT fk_prozessbaukastenanforderungkonzept_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);
 ALTER TABLE ONLY prozessbaukastenanforderungkonzept ADD CONSTRAINT fk_prozessbaukastenanforderungkonzept_prozessbaukasten_id FOREIGN KEY (prozessbaukasten_id) REFERENCES prozessbaukasten(id);
 ALTER TABLE ONLY prozessbaukastenanforderungkonzept ADD CONSTRAINT fk_prozessbaukastenanforderungkonzept_konzept_id FOREIGN KEY (konzept_id) REFERENCES konzept(id);
