DROP TABLE IF EXISTS anforderung_referenzsystemlink;

ALTER TABLE ONLY referenz_system_link 
    DROP COLUMN IF EXISTS derivat_id;

ALTER TABLE ONLY referenz_system_link 
    DROP COLUMN IF EXISTS doorsurl;

ALTER TABLE ONLY referenz_system_link 
    DROP COLUMN IF EXISTS updatedfrom;

ALTER TABLE ONLY referenz_system_link 
    DROP COLUMN IF EXISTS status;


ALTER TABLE ONLY referenz_system_link 
    ADD COLUMN referenzsystem character varying(32);

ALTER TABLE ONLY referenz_system_link 
    ADD COLUMN referenzsystem_id character varying(255);

ALTER TABLE ONLY referenz_system_link 
    ADD COLUMN anforderung_id bigint NOT NULL;

ALTER TABLE ONLY referenz_system_link
    ADD CONSTRAINT fk_referenzsystemlink_anforderung_id 
    FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


