ALTER TABLE derivatanforderung 
    ADD COLUMN kovaphase_id bigint;

ALTER TABLE ONLY derivatanforderung
    ADD CONSTRAINT fk_pzbk_kovaphase_id FOREIGN KEY (kovaphase_id) REFERENCES kovaphase(id);