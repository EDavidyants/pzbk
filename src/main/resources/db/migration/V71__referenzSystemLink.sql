CREATE TABLE referenz_system_link (
    id bigint NOT NULL,
    derivat_id bigint,
    doorsurl character varying(1000),
    updatedfrom timestamp without time zone,
    status character varying(1000)    
);

ALTER TABLE ONLY referenz_system_link
    ADD CONSTRAINT referenz_system_link_pkey PRIMARY KEY (id);


ALTER TABLE ONLY referenz_system_link
    ADD CONSTRAINT fk_ref_system_link_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);


