CREATE SEQUENCE statusabgleichsnapshot_id_seq INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE statusabgleichsnapshotentry_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE statusabgleichsnapshotentry (
    id bigint NOT NULL,
    x integer NOT NULL,
    y integer NOT NULL,
    entry integer NOT NULL
);

ALTER TABLE ONLY statusabgleichsnapshotentry
    ADD CONSTRAINT statusabgleichsnapshotentry_pkey PRIMARY KEY (id);




CREATE TABLE statusabgleichsnapshot (
    id bigint NOT NULL,
    datum timestamp without time zone NOT NULL,
    derivat_id bigint NOT NULL
);

ALTER TABLE ONLY statusabgleichsnapshot
    ADD CONSTRAINT statusabgleichsnapshot_pkey PRIMARY KEY (id);

ALTER TABLE ONLY statusabgleichsnapshot
    ADD CONSTRAINT fk_statusabgleichsnapshot_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);




CREATE TABLE statusabgleichsnapshot_statusabgleichsnapshotentry (
    statusabgleichsnapshot_id bigint NOT NULL,
    entries_id bigint NOT NULL
);

ALTER TABLE ONLY statusabgleichsnapshot_statusabgleichsnapshotentry
    ADD CONSTRAINT fk_statusabgleichsnapshot_id FOREIGN KEY (statusabgleichsnapshot_id) REFERENCES statusabgleichsnapshot(id);

ALTER TABLE ONLY statusabgleichsnapshot_statusabgleichsnapshotentry
    ADD CONSTRAINT fk_entries_id FOREIGN KEY (entries_id) REFERENCES statusabgleichsnapshotentry(id);

