ALTER TABLE kov_a_phase_im_derivat  
ALTER COLUMN kov_a_phase 
SET DATA TYPE bigint;

UPDATE kovaphase
    SET id = code;

ALTER TABLE ONLY kov_a_phase_im_derivat
    ADD CONSTRAINT fk_kov_a_phase_im_derivat_kovaphase_id FOREIGN KEY (kov_a_phase) 
    REFERENCES kovaphase(id);


