-- drop old tables for fahrzeugmerkmal and relevant join tables
DROP TABLE derivat_fahrzeugmerkmalwert CASCADE;
DROP TABLE fahrzeugmerkmal CASCADE;
DROP TABLE fahrzeugmerkmalwert CASCADE;
DROP TABLE fahrzeugmerkmal_fahrzeugmerkmalwert CASCADE;

-- create new fahrzeugmerkmal table
CREATE TABLE fahrzeugmerkmal (
                                 id bigint NOT NULL,
                                 merkmal character varying(1000) NOT NULL
);

ALTER TABLE ONLY fahrzeugmerkmal
    ADD CONSTRAINT fahrzeugmerkmal_pkey PRIMARY KEY (id);

-- create new fahrzeugmerkmalauspraegung table
CREATE TABLE fahrzeugmerkmalauspraegung (
                                            id bigint NOT NULL,
                                            fahrzeugmerkmal_id bigint NOT NULL,
                                            auspraegung character varying(1000) NOT NULL
);

ALTER TABLE ONLY fahrzeugmerkmalauspraegung
    ADD CONSTRAINT fahrzeugmerkmalauspraegung_pkey PRIMARY KEY (id);

ALTER TABLE ONLY fahrzeugmerkmalauspraegung
    ADD CONSTRAINT fk_fahrzeugmerkmalauspraegung_fahrzeugmerkmal_id FOREIGN KEY (fahrzeugmerkmal_id) REFERENCES fahrzeugmerkmal(id);

