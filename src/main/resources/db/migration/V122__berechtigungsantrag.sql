CREATE SEQUENCE IF NOT EXISTS berechtigungsantrag_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE IF NOT EXISTS berechtigungsantrag
(
    id               bigint  NOT NULL,
    antragsteller_id bigint  NOT NULL,
    status           integer NOT NULL,
    bearbeiter_id    bigint,
    kommentar        character varying(10000),
    rolle            integer NOT NULL,
    rechttype        integer NOT NULL,
    sensorcoc_id     bigint,
    tteam_id         bigint,
    modulseteam_id   bigint,
    erstellungsdatum timestamp without time zone,
    aenderungsdatum  timestamp without time zone
);

ALTER TABLE ONLY berechtigungsantrag
    ADD CONSTRAINT berechtigungsantrag_pkey PRIMARY KEY (id);

ALTER TABLE ONLY berechtigungsantrag
    ADD CONSTRAINT fk_berechtigungsantrag_antragsteller_id FOREIGN KEY (antragsteller_id) REFERENCES mitarbeiter (id);

ALTER TABLE ONLY berechtigungsantrag
    ADD CONSTRAINT fk_berechtigungsantrag_bearbeiter_id FOREIGN KEY (bearbeiter_id) REFERENCES mitarbeiter (id);

ALTER TABLE ONLY berechtigungsantrag
    ADD CONSTRAINT fk_berechtigungsantrag_sensorcoc_id FOREIGN KEY (sensorcoc_id) REFERENCES sensorcoc (sensorcocid);

ALTER TABLE ONLY berechtigungsantrag
    ADD CONSTRAINT fk_berechtigungsantrag_tteam_id FOREIGN KEY (tteam_id) REFERENCES t_team (id);

ALTER TABLE ONLY berechtigungsantrag
    ADD CONSTRAINT fk_berechtigungsantrag_modulseteam_id FOREIGN KEY (modulseteam_id) REFERENCES modulseteam (id);









