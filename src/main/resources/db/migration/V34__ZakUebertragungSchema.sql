ALTER TABLE zakuebertragung DROP COLUMN umsetzer;
ALTER TABLE zakuebertragung RENAME COLUMN zak_kommentar TO zakkommentar;
ALTER TABLE zakuebertragung RENAME COLUMN zak_uebertrager TO ersteller;
ALTER TABLE zakuebertragung RENAME COLUMN zakuebertragung TO erstellungsdatum;
ALTER TABLE zakuebertragung RENAME COLUMN zakuebertragung_id TO uebertragungid;