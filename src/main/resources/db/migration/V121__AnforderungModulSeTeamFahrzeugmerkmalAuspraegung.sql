CREATE SEQUENCE IF NOT EXISTS anforderungmodulseteamfahrzeugmerkmalauspraegung_id_seq INCREMENT BY 1 START WITH 1;

CREATE TABLE anforderungmodulseteamfahrzeugmerkmalauspraegung (
    id bigint NOT NULL,
    anforderung_id bigint NOT NULL,
    fahrzeugmerkmalauspraegung_id bigint NOT NULL,
    modulseteam_id bigint NOT NULL
);

ALTER TABLE ONLY anforderungmodulseteamfahrzeugmerkmalauspraegung
    ADD CONSTRAINT anforderungmodulseteamfahrzeugmerkmalauspraegung_pkey PRIMARY KEY (id);

ALTER TABLE ONLY anforderungmodulseteamfahrzeugmerkmalauspraegung
    ADD CONSTRAINT fk_anforderungmodulseteamfahrzeugmerkmalauspraegung_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);

ALTER TABLE ONLY anforderungmodulseteamfahrzeugmerkmalauspraegung
    ADD CONSTRAINT fk_anforderungmodulseteamfahrzeugmerkmalauspraegung_fahrzeugmerkmalauspraegung_id FOREIGN KEY (fahrzeugmerkmalauspraegung_id) REFERENCES fahrzeugmerkmalauspraegung(id);

ALTER TABLE ONLY anforderungmodulseteamfahrzeugmerkmalauspraegung
    ADD CONSTRAINT fk_anforderungmodulseteamfahrzeugmerkmalauspraegung_modulseteam_id FOREIGN KEY (modulseteam_id) REFERENCES modulseteam(id);
