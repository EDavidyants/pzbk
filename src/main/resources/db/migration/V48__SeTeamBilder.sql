ALTER TABLE modulseteam ADD COLUMN bild_id bigint;

ALTER TABLE ONLY modulseteam
    ADD CONSTRAINT fk_modulseteam_bild_id FOREIGN KEY (bild_id) REFERENCES bild(id);