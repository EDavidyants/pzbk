ALTER TABLE ONLY prozessbaukasten 
    ADD COLUMN startbrief_id bigint;

ALTER TABLE ONLY prozessbaukasten 
    ADD COLUMN grafikumfang_id bigint;

ALTER TABLE ONLY prozessbaukasten 
    ADD COLUMN konzeptbaum_id bigint;

ALTER TABLE ONLY prozessbaukasten 
    ADD COLUMN beauftragungtpk_id bigint;

ALTER TABLE ONLY prozessbaukasten 
    ADD COLUMN genehmigungtpk_id bigint;

ALTER TABLE ONLY prozessbaukasten 
    ADD COLUMN weitereanhaenge_id bigint;

ALTER TABLE ONLY prozessbaukasten
    ADD CONSTRAINT fk_prozessbaukasten_startbrief_id FOREIGN KEY (startbrief_id) REFERENCES anhang(id);

ALTER TABLE ONLY prozessbaukasten
    ADD CONSTRAINT fk_prozessbaukasten_grafikumfang_id FOREIGN KEY (grafikumfang_id) REFERENCES anhang(id);

ALTER TABLE ONLY prozessbaukasten
    ADD CONSTRAINT fk_prozessbaukasten_konzeptbaum_id FOREIGN KEY (konzeptbaum_id) REFERENCES anhang(id);

ALTER TABLE ONLY prozessbaukasten
    ADD CONSTRAINT fk_prozessbaukasten_beauftragungtpk_id FOREIGN KEY (beauftragungtpk_id) REFERENCES anhang(id);

ALTER TABLE ONLY prozessbaukasten
    ADD CONSTRAINT fk_prozessbaukasten_genehmigungtpk_id FOREIGN KEY (genehmigungtpk_id) REFERENCES anhang(id);

ALTER TABLE ONLY prozessbaukasten
    ADD CONSTRAINT fk_prozessbaukasten_weitereanhaenge_id FOREIGN KEY (weitereanhaenge_id) REFERENCES anhang(id);

