ALTER TABLE konzept ADD COLUMN bild_id bigint;

ALTER TABLE ONLY konzept ADD CONSTRAINT fk_konzept_bild_id FOREIGN KEY (bild_id) REFERENCES bild(id);