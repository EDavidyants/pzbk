-- remove existing umsetzungsbestaetigungen
TRUNCATE TABLE umsetzungsbestaetigung CASCADE;
-- update derivatanforderung status. at status 3 the umsetzungsbestatigung objects are generated
UPDATE derivatanforderung SET status = 1 WHERE status = 3;
-- add new column to table
ALTER TABLE umsetzungsbestaetigung ADD COLUMN modul_id bigint;