CREATE SEQUENCE meldung_id_seq INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE anforderung_id_seq INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE pzbk_id_seq INCREMENT BY 1 START WITH 1;

SELECT setval('meldung_id_seq', nextval('abstractanfomgmtobject_id_seq'));
SELECT setval('anforderung_id_seq', nextval('meldung_id_seq'));
SELECT setval('pzbk_id_seq', nextval('anforderung_id_seq'));
