

CREATE TABLE anforderung (
    id bigint NOT NULL,
    aenderungsdatum timestamp without time zone,
    beschreibunganforderungde character varying(10000),
    beschreibunganforderungen character varying(10000),
    beschreibungstaerkeschwaeche character varying(10000),
    designrelevant boolean,
    erstellungsdatum timestamp without time zone,
    fachid character varying(255),
    kommentaranforderung character varying(10000),
    loesungsvorschlag character varying(10000),
    nextstatus integer,
    phasenbezug boolean,
    potentialstandardisierung boolean,
    prioinbl character varying(255),
    referenzen character varying(255),
    staerkeschwaeche boolean,
    status integer,
    statuschangerequested boolean,
    statuswechselkommentar character varying(255),
    umsetzenderbereich character varying(255),
    ursache character varying(10000),
    version integer,
    zeitpktumsetzungsbest boolean,
    einheit character varying(255),
    kommentar character varying(10000),
    wert character varying(255),
    bild_id bigint,
    fachlicheransprechpartner_id bigint,
    sensor_id bigint,
    sensorcoc_sensorcocid bigint,
    teamleiter_id bigint,
    tteam_id bigint
);




--
-- TOC entry 226 (class 1259 OID 2423758)
-- Name: anforderung_anforderung_freigabe_bei_modul; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE anforderung_anforderung_freigabe_bei_modul (
    anforderung_id bigint NOT NULL,
    anfofreigabebeimodul_id bigint NOT NULL
);




--
-- TOC entry 227 (class 1259 OID 2423763)
-- Name: anforderung_anhang; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE anforderung_anhang (
    anforderung_id bigint NOT NULL,
    anhaenge_id bigint NOT NULL
);




--
-- TOC entry 228 (class 1259 OID 2423768)
-- Name: anforderung_auswirkung; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE anforderung_auswirkung (
    anforderung_id bigint NOT NULL,
    auswirkungen_id bigint NOT NULL
);




--
-- TOC entry 229 (class 1259 OID 2423773)
-- Name: anforderung_festgestelltin; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE anforderung_festgestelltin (
    anforderung_id bigint NOT NULL,
    festgestelltin_id bigint NOT NULL
);




--
-- TOC entry 194 (class 1259 OID 2423541)
-- Name: anforderung_freigabe_bei_modul; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE anforderung_freigabe_bei_modul (
    id bigint NOT NULL,
    abgelehnt boolean,
    datum timestamp without time zone,
    freigabe boolean,
    hauptmodul boolean,
    kommentar character varying(10000),
    vereinbarung integer,
    zak boolean,
    anforderung_id bigint,
    bearbeiter_id bigint,
    modul_id bigint
);




--
-- TOC entry 208 (class 1259 OID 2423632)
-- Name: anforderung_history; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE anforderung_history (
    id bigint NOT NULL,
    anforderung_id bigint,
    auf character varying(10000),
    benutzer character varying(255),
    datum timestamp without time zone,
    kennzeichen character varying(255),
    objekt_name character varying(255),
    objekt_type character varying(255),
    version character varying(255),
    von character varying(10000)
);




--
-- TOC entry 231 (class 1259 OID 2423783)
-- Name: anforderung_umsetzer; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE anforderung_umsetzer (
    anforderung_id bigint NOT NULL,
    umsetzer_umsetzer_id bigint NOT NULL
);




--
-- TOC entry 217 (class 1259 OID 2423692)
-- Name: anhang; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE anhang (
    id bigint NOT NULL,
    dateiname character varying(255),
    dateityp character varying(255),
    filesize integer,
    isstandardbild boolean,
    zeitstempel timestamp without time zone,
    content_id bigint,
    creator_id bigint
);




--
-- TOC entry 187 (class 1259 OID 2401446)
-- Name: anwender; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE anwender (
    qnumber integer NOT NULL,
    email character varying(255),
    kurzzeichen character varying(255),
    name character varying(255),
    tel character varying(255)
);




--
-- TOC entry 218 (class 1259 OID 2423700)
-- Name: auswirkung; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE auswirkung (
    id bigint NOT NULL,
    auswirkung character varying(10000),
    wert character varying(10000)
);




--
-- TOC entry 215 (class 1259 OID 2423682)
-- Name: berechtigung; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE berechtigung (
    id bigint NOT NULL,
    fuer integer,
    fuerid character varying(255),
    rechttype integer,
    rolle integer,
    mitarbeiter_id bigint
);




--
-- TOC entry 198 (class 1259 OID 2423570)
-- Name: berechtigung_derivat; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE berechtigung_derivat (
    id bigint NOT NULL,
    derivat_id bigint,
    rechttype integer,
    rolle integer,
    mitarbeiter_id bigint
);




--
-- TOC entry 220 (class 1259 OID 2423713)
-- Name: berechtigunghistory; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE berechtigunghistory (
    id bigint NOT NULL,
    berechtigter character varying(255),
    berechtigter_qnumber character varying(255),
    datum timestamp without time zone,
    erteilt boolean,
    fuer character varying(255),
    ziel_id character varying(255),
    rechttype integer,
    rolle character varying(255),
    berechtigt_von character varying(255),
    berechtigt_von_qnumber character varying(255)
);




--
-- TOC entry 221 (class 1259 OID 2423721)
-- Name: bild; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE bild (
    id bigint NOT NULL,
    largeimage bytea,
    normalimage bytea,
    thumbnailimage bytea
);




--
-- TOC entry 197 (class 1259 OID 2423562)
-- Name: dbfile; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE dbfile (
    id bigint NOT NULL,
    category character varying(255),
    filecontent bytea,
    creationdate timestamp without time zone,
    filename character varying(255),
    filetype character varying(255)
);




--
-- TOC entry 210 (class 1259 OID 2423648)
-- Name: derivat; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE derivat (
    id bigint NOT NULL,
    bezeichnung character varying(255),
    name character varying(255),
    produktlinie character varying(255),
    startofproduction date
);




--
-- TOC entry 238 (class 1259 OID 2423821)
-- Name: derivat_kov_a_phase_im_derivat; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE derivat_kov_a_phase_im_derivat (
    derivat_id bigint NOT NULL,
    kovaphasen_id bigint NOT NULL
);




--
-- TOC entry 211 (class 1259 OID 2423656)
-- Name: festgestelltin; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE festgestelltin (
    id bigint NOT NULL,
    wert character varying(10000)
);




--
-- TOC entry 196 (class 1259 OID 2423554)
-- Name: infotext; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE infotext (
    id bigint NOT NULL,
    beschreibungstext character varying(10000),
    feldbeschreibunglang character varying(10000),
    feldname character varying(255)
);




--
-- TOC entry 202 (class 1259 OID 2423596)
-- Name: kov_a_phase_im_derivat; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE kov_a_phase_im_derivat (
    id bigint NOT NULL,
    end_date date,
    kov_a_phase integer,
    status integer,
    start_date date,
    derivat_id bigint,
    umsetzungsbestaetiger_id bigint
);










--
-- TOC entry 205 (class 1259 OID 2423614)
-- Name: meldung; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE meldung (
    id bigint NOT NULL,
    aenderungsdatum timestamp without time zone,
    assignedtoanforderung boolean,
    beschreibunganforderungde character varying(10000),
    beschreibunganforderungen character varying(10000),
    beschreibungstaerkeschwaeche character varying(10000),
    designrelevant boolean,
    erstellungsdatum timestamp without time zone,
    fachid character varying(255),
    kommentaranforderung character varying(10000),
    loesungsvorschlag character varying(10000),
    nextstatus integer,
    prioinbl character varying(255),
    referenzen character varying(255),
    staerkeschwaeche boolean,
    status integer,
    statuschangerequested boolean,
    statuswechselkommentar character varying(255),
    umsetzenderbereich character varying(255),
    ursache character varying(10000),
    einheit character varying(255),
    kommentar character varying(10000),
    wert character varying(255),
    bild_id bigint,
    fachlicheransprechpartner_id bigint,
    sensor_id bigint,
    sensorcoc_sensorcocid bigint,
    teamleiter_id bigint,
    tteam_id bigint
);



--
-- TOC entry 230 (class 1259 OID 2423778)
-- Name: meldung_anforderung; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE meldung_anforderung (
    anforderung_id bigint NOT NULL,
    meldung_id bigint NOT NULL
);




--
-- TOC entry 234 (class 1259 OID 2423801)
-- Name: meldung_anhang; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE meldung_anhang (
    meldung_id bigint NOT NULL,
    anhaenge_id bigint NOT NULL
);




--
-- TOC entry 235 (class 1259 OID 2423806)
-- Name: meldung_auswirkung; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE meldung_auswirkung (
    meldung_id bigint NOT NULL,
    auswirkungen_id bigint NOT NULL
);




--
-- TOC entry 236 (class 1259 OID 2423811)
-- Name: meldung_festgestelltin; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE meldung_festgestelltin (
    meldung_id bigint NOT NULL,
    festgestelltin_id bigint NOT NULL
);




--
-- TOC entry 214 (class 1259 OID 2423674)
-- Name: mitarbeiter; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE mitarbeiter (
    id bigint NOT NULL,
    abteilung character varying(255),
    email character varying(255),
    kurzzeichen character varying(255),
    mail_receipt_enabled boolean,
    nachname character varying(255),
    qnumber character varying(255),
    tel character varying(255),
    vorname character varying(255)
);




--
-- TOC entry 243 (class 1259 OID 2423838)
-- Name: mitarbeiter_derivat; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE mitarbeiter_derivat (
    mitarbeiter_id bigint NOT NULL,
    derivaten_id bigint NOT NULL
);




--
-- TOC entry 244 (class 1259 OID 2423843)
-- Name: mitarbeiter_modul; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE mitarbeiter_modul (
    mitarbeiter_id bigint NOT NULL,
    moduls_id bigint NOT NULL
);




--
-- TOC entry 245 (class 1259 OID 2423848)
-- Name: mitarbeiter_selectedderivate; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE mitarbeiter_selectedderivate (
    mitarbeiter_id bigint NOT NULL,
    selectedderivate_id bigint NOT NULL
);




--
-- TOC entry 246 (class 1259 OID 2423853)
-- Name: mitarbeiter_selectedzakderivate; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE mitarbeiter_selectedzakderivate (
    mitarbeiter_id bigint NOT NULL,
    selectedzakderivate_id bigint NOT NULL
);




--
-- TOC entry 247 (class 1259 OID 2423858)
-- Name: mitarbeiter_sensorcoc; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE mitarbeiter_sensorcoc (
    mitarbeiter_id bigint NOT NULL,
    sensorcoc_sensorcocid bigint NOT NULL
);




--
-- TOC entry 248 (class 1259 OID 2423863)
-- Name: mitarbeiter_sensorcoc_lese; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE mitarbeiter_sensorcoc_lese (
    mitarbeiter_id bigint NOT NULL,
    sensorcocslese_sensorcocid bigint NOT NULL
);




--
-- TOC entry 249 (class 1259 OID 2423868)
-- Name: mitarbeiter_tteam; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE mitarbeiter_tteam (
    mitarbeiter_id bigint NOT NULL,
    tteams_id bigint NOT NULL
);




--
-- TOC entry 242 (class 1259 OID 2423835)
-- Name: mitarbeiter_zakeinordnungen; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE mitarbeiter_zakeinordnungen (
    mitarbeiter_id bigint,
    zakeinordnungen character varying(255)
);




--
-- TOC entry 222 (class 1259 OID 2423729)
-- Name: modul; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE modul (
    id bigint NOT NULL,
    beschreibung character varying(10000),
    fachbereich character varying(255),
    name character varying(255),
    modulbild_id bigint
);




--
-- TOC entry 251 (class 1259 OID 2423878)
-- Name: modul_modulseteam; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE modul_modulseteam (
    modul_id bigint NOT NULL,
    seteams_id bigint NOT NULL
);




--
-- TOC entry 207 (class 1259 OID 2423627)
-- Name: modulbild; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE modulbild (
    id bigint NOT NULL,
    bild_id bigint,
    modul_id bigint
);




--
-- TOC entry 200 (class 1259 OID 2423583)
-- Name: modulkomponente; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE modulkomponente (
    id bigint NOT NULL,
    name character varying(255),
    ppg character varying(255),
    seteam_id bigint
);




--
-- TOC entry 201 (class 1259 OID 2423591)
-- Name: modulseteam; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE modulseteam (
    id bigint NOT NULL,
    name character varying(255),
    elternmodul_id bigint
);




--
-- TOC entry 232 (class 1259 OID 2423788)
-- Name: modulseteam_modulkomponente; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE modulseteam_modulkomponente (
    modulseteam_id bigint NOT NULL,
    komponenten_id bigint NOT NULL
);




--
-- TOC entry 225 (class 1259 OID 2423753)
-- Name: pzbk; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE pzbk (
    id bigint NOT NULL,
    pzbkid character varying(255)
);




--
-- TOC entry 213 (class 1259 OID 2423669)
-- Name: search_filter; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE search_filter (
    id bigint NOT NULL,
    booleanvalue boolean,
    enddatevalue timestamp without time zone,
    filtertype integer,
    startdatevalue timestamp without time zone
);




--
-- TOC entry 239 (class 1259 OID 2423826)
-- Name: searchfilter_longlistvalue; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE searchfilter_longlistvalue (
    searchfilter_id bigint,
    longlistvalue bigint
);




--
-- TOC entry 240 (class 1259 OID 2423829)
-- Name: searchfilter_statuslistvalue; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE searchfilter_statuslistvalue (
    searchfilter_id bigint,
    statuslistvalue character varying(255)
);




--
-- TOC entry 241 (class 1259 OID 2423832)
-- Name: searchfilter_stringlistvalue; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE searchfilter_stringlistvalue (
    searchfilter_id bigint,
    stringlistvalue character varying(255)
);




--
-- TOC entry 209 (class 1259 OID 2423640)
-- Name: sensorcoc; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE sensorcoc (
    sensorcocid bigint NOT NULL,
    ortung character varying(255),
    ressort character varying(255),
    technologie character varying(255),
    zakeinordnung character varying(255),
    fachlicheransprechpartner_id bigint
);




--
-- TOC entry 237 (class 1259 OID 2423816)
-- Name: sensorcoc_mitarbeiter; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE sensorcoc_mitarbeiter (
    sensorcoc_sensorcocid bigint NOT NULL,
    umsetzungsbestaetiger_id bigint NOT NULL
);




--
-- TOC entry 186 (class 1259 OID 2399559)
-- Name: sequence; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE sequence (
    seq_name character varying(50) NOT NULL,
    seq_count numeric(38,0)
);




--
-- TOC entry 195 (class 1259 OID 2423549)
-- Name: status_vereinbarung_anforderung_im_derivat; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE status_vereinbarung_anforderung_im_derivat (
    id bigint NOT NULL,
    nach_zak boolean,
    anforderer_id bigint,
    anforderung_id bigint,
    derivat_id bigint
);




--
-- TOC entry 212 (class 1259 OID 2423664)
-- Name: statusnext; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE statusnext (
    id integer NOT NULL,
    statusid integer,
    nextstatusid integer
);




--
-- TOC entry 223 (class 1259 OID 2423737)
-- Name: t_role_group; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE t_role_group (
    id bigint NOT NULL,
    groupname character varying(255),
    rolename character varying(255)
);




--
-- TOC entry 192 (class 1259 OID 2423528)
-- Name: t_role_user; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE t_role_user (
    id bigint NOT NULL,
    rolename character varying(255),
    username character varying(255)
);




--
-- TOC entry 219 (class 1259 OID 2423708)
-- Name: t_team; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE t_team (
    id bigint NOT NULL,
    team_name character varying(255),
    teamleiter_id bigint
);







--
-- TOC entry 193 (class 1259 OID 2423536)
-- Name: ub_zu_kova_phase_im_derivat_und_sensorcoc; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE ub_zu_kova_phase_im_derivat_und_sensorcoc (
    id bigint NOT NULL,
    kovaimderivat_id bigint,
    sensorcoc_sensorcocid bigint,
    umsetzungsbestaetiger_id bigint
);




--
-- TOC entry 203 (class 1259 OID 2423601)
-- Name: umsetzer; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE umsetzer (
    umsetzer_id bigint NOT NULL,
    komponente_id bigint,
    seteam_id bigint
);




--
-- TOC entry 199 (class 1259 OID 2423575)
-- Name: umsetzungsbestaetigung; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE umsetzungsbestaetigung (
    id bigint NOT NULL,
    datum timestamp without time zone,
    kommentar character varying(10000),
    status integer,
    anforderung_id bigint,
    kovaimderivat_id bigint,
    umsetzungsbestaetiger_id bigint
);




--
-- TOC entry 216 (class 1259 OID 2423687)
-- Name: user_defined_search; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE user_defined_search (
    id bigint NOT NULL,
    name character varying(255),
    mitarbeiter_id bigint
);




--
-- TOC entry 250 (class 1259 OID 2423873)
-- Name: user_defined_search_search_filter; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE user_defined_search_search_filter (
    userdefinedsearch_id bigint NOT NULL,
    searchfilterlist_id bigint NOT NULL
);



--
-- TOC entry 188 (class 1259 OID 2403625)
-- Name: usersettings_selectedderivate; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE usersettings_selectedderivate (
    usersettings_id bigint NOT NULL,
    selectedderivat_id bigint NOT NULL
);




--
-- TOC entry 189 (class 1259 OID 2403630)
-- Name: usersettings_selectedzakderivate; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE usersettings_selectedzakderivate (
    usersettings_id bigint NOT NULL,
    selectedzakderivat_id bigint NOT NULL
);




--
-- TOC entry 191 (class 1259 OID 2423520)
-- Name: wert; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE wert (
    id bigint NOT NULL,
    attribut character varying(10000),
    wert character varying(10000)
);




--
-- TOC entry 224 (class 1259 OID 2423745)
-- Name: zak_uebertragung; Type: TABLE; Schema: public; OWNER:
--

CREATE TABLE zak_uebertragung (
    id bigint NOT NULL,
    aenderungsdatum timestamp without time zone,
    umsetzer character varying(255),
    zak_kommentar character varying(10000),
    zakstatus integer,
    zak_uebertrager character varying(255),
    zakuebertragung timestamp without time zone,
    zak_uebertragung_id bigint,
    derivatanforderung_id bigint,
    modul_name character varying(255)
);




--
-- TOC entry 2289 (class 2606 OID 1587012)
-- Name: EJB__TIMER__TBL PK_EJB__TIMER__TBL; Type: CONSTRAINT; Schema: public; OWNER:
--




--
-- TOC entry 2373 (class 2606 OID 2423762)
-- Name: anforderung_anforderung_freigabe_bei_modul anforderung_anforderung_freigabe_bei_modul_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_anforderung_freigabe_bei_modul
    ADD CONSTRAINT anforderung_anforderung_freigabe_bei_modul_pkey PRIMARY KEY (anforderung_id, anfofreigabebeimodul_id);


--
-- TOC entry 2375 (class 2606 OID 2423767)
-- Name: anforderung_anhang anforderung_anhang_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_anhang
    ADD CONSTRAINT anforderung_anhang_pkey PRIMARY KEY (anforderung_id, anhaenge_id);


--
-- TOC entry 2377 (class 2606 OID 2423772)
-- Name: anforderung_auswirkung anforderung_auswirkung_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_auswirkung
    ADD CONSTRAINT anforderung_auswirkung_pkey PRIMARY KEY (anforderung_id, auswirkungen_id);


--
-- TOC entry 2379 (class 2606 OID 2423777)
-- Name: anforderung_festgestelltin anforderung_festgestelltin_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_festgestelltin
    ADD CONSTRAINT anforderung_festgestelltin_pkey PRIMARY KEY (anforderung_id, festgestelltin_id);


--
-- TOC entry 2307 (class 2606 OID 2423548)
-- Name: anforderung_freigabe_bei_modul anforderung_freigabe_bei_modul_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_freigabe_bei_modul
    ADD CONSTRAINT anforderung_freigabe_bei_modul_pkey PRIMARY KEY (id);


--
-- TOC entry 2337 (class 2606 OID 2423639)
-- Name: anforderung_history anforderung_history_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_history
    ADD CONSTRAINT anforderung_history_pkey PRIMARY KEY (id);


--
-- TOC entry 2299 (class 2606 OID 2423519)
-- Name: anforderung anforderung_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT anforderung_pkey PRIMARY KEY (id);


--
-- TOC entry 2383 (class 2606 OID 2423787)
-- Name: anforderung_umsetzer anforderung_umsetzer_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_umsetzer
    ADD CONSTRAINT anforderung_umsetzer_pkey PRIMARY KEY (anforderung_id, umsetzer_umsetzer_id);


--
-- TOC entry 2355 (class 2606 OID 2423699)
-- Name: anhang anhang_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anhang
    ADD CONSTRAINT anhang_pkey PRIMARY KEY (id);


--
-- TOC entry 2293 (class 2606 OID 2401453)
-- Name: anwender anwender_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anwender
    ADD CONSTRAINT anwender_pkey PRIMARY KEY (qnumber);


--
-- TOC entry 2357 (class 2606 OID 2423707)
-- Name: auswirkung auswirkung_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY auswirkung
    ADD CONSTRAINT auswirkung_pkey PRIMARY KEY (id);


--
-- TOC entry 2317 (class 2606 OID 2423574)
-- Name: berechtigung_derivat berechtigung_derivat_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY berechtigung_derivat
    ADD CONSTRAINT berechtigung_derivat_pkey PRIMARY KEY (id);


--
-- TOC entry 2351 (class 2606 OID 2423686)
-- Name: berechtigung berechtigung_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY berechtigung
    ADD CONSTRAINT berechtigung_pkey PRIMARY KEY (id);


--
-- TOC entry 2361 (class 2606 OID 2423720)
-- Name: berechtigunghistory berechtigunghistory_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY berechtigunghistory
    ADD CONSTRAINT berechtigunghistory_pkey PRIMARY KEY (id);


--
-- TOC entry 2363 (class 2606 OID 2423728)
-- Name: bild bild_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY bild
    ADD CONSTRAINT bild_pkey PRIMARY KEY (id);


--
-- TOC entry 2315 (class 2606 OID 2423569)
-- Name: dbfile dbfile_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY dbfile
    ADD CONSTRAINT dbfile_pkey PRIMARY KEY (id);


--
-- TOC entry 2397 (class 2606 OID 2423825)
-- Name: derivat_kov_a_phase_im_derivat derivat_kov_a_phase_im_derivat_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY derivat_kov_a_phase_im_derivat
    ADD CONSTRAINT derivat_kov_a_phase_im_derivat_pkey PRIMARY KEY (derivat_id, kovaphasen_id);


--
-- TOC entry 2341 (class 2606 OID 2423655)
-- Name: derivat derivat_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY derivat
    ADD CONSTRAINT derivat_pkey PRIMARY KEY (id);


--
-- TOC entry 2343 (class 2606 OID 2423663)
-- Name: festgestelltin festgestelltin_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY festgestelltin
    ADD CONSTRAINT festgestelltin_pkey PRIMARY KEY (id);


--
-- TOC entry 2313 (class 2606 OID 2423561)
-- Name: infotext infotext_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY infotext
    ADD CONSTRAINT infotext_pkey PRIMARY KEY (id);


--
-- TOC entry 2325 (class 2606 OID 2423600)
-- Name: kov_a_phase_im_derivat kov_a_phase_im_derivat_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY kov_a_phase_im_derivat
    ADD CONSTRAINT kov_a_phase_im_derivat_pkey PRIMARY KEY (id);

--
-- TOC entry 2381 (class 2606 OID 2423782)
-- Name: meldung_anforderung meldung_anforderung_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung_anforderung
    ADD CONSTRAINT meldung_anforderung_pkey PRIMARY KEY (anforderung_id, meldung_id);


--
-- TOC entry 2389 (class 2606 OID 2423805)
-- Name: meldung_anhang meldung_anhang_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung_anhang
    ADD CONSTRAINT meldung_anhang_pkey PRIMARY KEY (meldung_id, anhaenge_id);


--
-- TOC entry 2391 (class 2606 OID 2423810)
-- Name: meldung_auswirkung meldung_auswirkung_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung_auswirkung
    ADD CONSTRAINT meldung_auswirkung_pkey PRIMARY KEY (meldung_id, auswirkungen_id);


--
-- TOC entry 2393 (class 2606 OID 2423815)
-- Name: meldung_festgestelltin meldung_festgestelltin_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung_festgestelltin
    ADD CONSTRAINT meldung_festgestelltin_pkey PRIMARY KEY (meldung_id, festgestelltin_id);


--
-- TOC entry 2331 (class 2606 OID 2423621)
-- Name: meldung meldung_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT meldung_pkey PRIMARY KEY (id);


--
-- TOC entry 2399 (class 2606 OID 2423842)
-- Name: mitarbeiter_derivat mitarbeiter_derivat_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_derivat
    ADD CONSTRAINT mitarbeiter_derivat_pkey PRIMARY KEY (mitarbeiter_id, derivaten_id);


--
-- TOC entry 2401 (class 2606 OID 2423847)
-- Name: mitarbeiter_modul mitarbeiter_modul_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_modul
    ADD CONSTRAINT mitarbeiter_modul_pkey PRIMARY KEY (mitarbeiter_id, moduls_id);


--
-- TOC entry 2349 (class 2606 OID 2423681)
-- Name: mitarbeiter mitarbeiter_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter
    ADD CONSTRAINT mitarbeiter_pkey PRIMARY KEY (id);


--
-- TOC entry 2403 (class 2606 OID 2423852)
-- Name: mitarbeiter_selectedderivate mitarbeiter_selectedderivate_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_selectedderivate
    ADD CONSTRAINT mitarbeiter_selectedderivate_pkey PRIMARY KEY (mitarbeiter_id, selectedderivate_id);


--
-- TOC entry 2405 (class 2606 OID 2423857)
-- Name: mitarbeiter_selectedzakderivate mitarbeiter_selectedzakderivate_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_selectedzakderivate
    ADD CONSTRAINT mitarbeiter_selectedzakderivate_pkey PRIMARY KEY (mitarbeiter_id, selectedzakderivate_id);


--
-- TOC entry 2409 (class 2606 OID 2423867)
-- Name: mitarbeiter_sensorcoc_lese mitarbeiter_sensorcoc_lese_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_sensorcoc_lese
    ADD CONSTRAINT mitarbeiter_sensorcoc_lese_pkey PRIMARY KEY (mitarbeiter_id, sensorcocslese_sensorcocid);


--
-- TOC entry 2407 (class 2606 OID 2423862)
-- Name: mitarbeiter_sensorcoc mitarbeiter_sensorcoc_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_sensorcoc
    ADD CONSTRAINT mitarbeiter_sensorcoc_pkey PRIMARY KEY (mitarbeiter_id, sensorcoc_sensorcocid);


--
-- TOC entry 2411 (class 2606 OID 2423872)
-- Name: mitarbeiter_tteam mitarbeiter_tteam_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_tteam
    ADD CONSTRAINT mitarbeiter_tteam_pkey PRIMARY KEY (mitarbeiter_id, tteams_id);


--
-- TOC entry 2415 (class 2606 OID 2423882)
-- Name: modul_modulseteam modul_modulseteam_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modul_modulseteam
    ADD CONSTRAINT modul_modulseteam_pkey PRIMARY KEY (modul_id, seteams_id);


--
-- TOC entry 2365 (class 2606 OID 2423736)
-- Name: modul modul_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modul
    ADD CONSTRAINT modul_pkey PRIMARY KEY (id);


--
-- TOC entry 2335 (class 2606 OID 2423631)
-- Name: modulbild modulbild_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modulbild
    ADD CONSTRAINT modulbild_pkey PRIMARY KEY (id);


--
-- TOC entry 2321 (class 2606 OID 2423590)
-- Name: modulkomponente modulkomponente_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modulkomponente
    ADD CONSTRAINT modulkomponente_pkey PRIMARY KEY (id);


--
-- TOC entry 2385 (class 2606 OID 2423792)
-- Name: modulseteam_modulkomponente modulseteam_modulkomponente_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modulseteam_modulkomponente
    ADD CONSTRAINT modulseteam_modulkomponente_pkey PRIMARY KEY (modulseteam_id, komponenten_id);


--
-- TOC entry 2323 (class 2606 OID 2423595)
-- Name: modulseteam modulseteam_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modulseteam
    ADD CONSTRAINT modulseteam_pkey PRIMARY KEY (id);


--
-- TOC entry 2371 (class 2606 OID 2423757)
-- Name: pzbk pzbk_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY pzbk
    ADD CONSTRAINT pzbk_pkey PRIMARY KEY (id);


--
-- TOC entry 2347 (class 2606 OID 2423673)
-- Name: search_filter search_filter_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY search_filter
    ADD CONSTRAINT search_filter_pkey PRIMARY KEY (id);


--
-- TOC entry 2395 (class 2606 OID 2423820)
-- Name: sensorcoc_mitarbeiter sensorcoc_mitarbeiter_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY sensorcoc_mitarbeiter
    ADD CONSTRAINT sensorcoc_mitarbeiter_pkey PRIMARY KEY (sensorcoc_sensorcocid, umsetzungsbestaetiger_id);


--
-- TOC entry 2339 (class 2606 OID 2423647)
-- Name: sensorcoc sensorcoc_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY sensorcoc
    ADD CONSTRAINT sensorcoc_pkey PRIMARY KEY (sensorcocid);


--
-- TOC entry 2291 (class 2606 OID 2399563)
-- Name: sequence sequence_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY sequence
    ADD CONSTRAINT sequence_pkey PRIMARY KEY (seq_name);


--
-- TOC entry 2309 (class 2606 OID 2423553)
-- Name: status_vereinbarung_anforderung_im_derivat status_vereinbarung_anforderung_im_derivat_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT status_vereinbarung_anforderung_im_derivat_pkey PRIMARY KEY (id);


--
-- TOC entry 2345 (class 2606 OID 2423668)
-- Name: statusnext statusnext_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY statusnext
    ADD CONSTRAINT statusnext_pkey PRIMARY KEY (id);


--
-- TOC entry 2367 (class 2606 OID 2423744)
-- Name: t_role_group t_role_group_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY t_role_group
    ADD CONSTRAINT t_role_group_pkey PRIMARY KEY (id);


--
-- TOC entry 2303 (class 2606 OID 2423535)
-- Name: t_role_user t_role_user_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY t_role_user
    ADD CONSTRAINT t_role_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2359 (class 2606 OID 2423712)
-- Name: t_team t_team_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY t_team
    ADD CONSTRAINT t_team_pkey PRIMARY KEY (id);



--
-- TOC entry 2305 (class 2606 OID 2423540)
-- Name: ub_zu_kova_phase_im_derivat_und_sensorcoc ub_zu_kova_phase_im_derivat_und_sensorcoc_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc
    ADD CONSTRAINT ub_zu_kova_phase_im_derivat_und_sensorcoc_pkey PRIMARY KEY (id);


--
-- TOC entry 2327 (class 2606 OID 2423605)
-- Name: umsetzer umsetzer_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY umsetzer
    ADD CONSTRAINT umsetzer_pkey PRIMARY KEY (umsetzer_id);


--
-- TOC entry 2319 (class 2606 OID 2423582)
-- Name: umsetzungsbestaetigung umsetzungsbestaetigung_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY umsetzungsbestaetigung
    ADD CONSTRAINT umsetzungsbestaetigung_pkey PRIMARY KEY (id);


--
-- TOC entry 2311 (class 2606 OID 2423884)
-- Name: status_vereinbarung_anforderung_im_derivat unq_status_vereinbarung_anforderung_im_derivat_0; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT unq_status_vereinbarung_anforderung_im_derivat_0 UNIQUE (anforderung_id, derivat_id);


--
-- TOC entry 2353 (class 2606 OID 2423691)
-- Name: user_defined_search user_defined_search_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY user_defined_search
    ADD CONSTRAINT user_defined_search_pkey PRIMARY KEY (id);


--
-- TOC entry 2413 (class 2606 OID 2423877)
-- Name: user_defined_search_search_filter user_defined_search_search_filter_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY user_defined_search_search_filter
    ADD CONSTRAINT user_defined_search_search_filter_pkey PRIMARY KEY (userdefinedsearch_id, searchfilterlist_id);



--
-- TOC entry 2295 (class 2606 OID 2403629)
-- Name: usersettings_selectedderivate usersettings_selectedderivate_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY usersettings_selectedderivate
    ADD CONSTRAINT usersettings_selectedderivate_pkey PRIMARY KEY (usersettings_id, selectedderivat_id);


--
-- TOC entry 2297 (class 2606 OID 2403634)
-- Name: usersettings_selectedzakderivate usersettings_selectedzakderivate_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY usersettings_selectedzakderivate
    ADD CONSTRAINT usersettings_selectedzakderivate_pkey PRIMARY KEY (usersettings_id, selectedzakderivat_id);


--
-- TOC entry 2301 (class 2606 OID 2423527)
-- Name: wert wert_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY wert
    ADD CONSTRAINT wert_pkey PRIMARY KEY (id);


--
-- TOC entry 2369 (class 2606 OID 2423752)
-- Name: zak_uebertragung zak_uebertragung_pkey; Type: CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY zak_uebertragung
    ADD CONSTRAINT zak_uebertragung_pkey PRIMARY KEY (id);


--
-- TOC entry 2457 (class 2606 OID 2424090)
-- Name: anforderung_anforderung_freigabe_bei_modul anforderunganforderungfreigabe_bei_modulanfofreigabebeimodul_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_anforderung_freigabe_bei_modul
    ADD CONSTRAINT anforderunganforderungfreigabe_bei_modulanfofreigabebeimodul_id FOREIGN KEY (anfofreigabebeimodul_id) REFERENCES anforderung_freigabe_bei_modul(id);


--
-- TOC entry 2456 (class 2606 OID 2424085)
-- Name: anforderung_anforderung_freigabe_bei_modul fk_anforderung_anforderung_freigabe_bei_modul_anforderung_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_anforderung_freigabe_bei_modul
    ADD CONSTRAINT fk_anforderung_anforderung_freigabe_bei_modul_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2459 (class 2606 OID 2424100)
-- Name: anforderung_anhang fk_anforderung_anhang_anforderung_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_anhang
    ADD CONSTRAINT fk_anforderung_anhang_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2458 (class 2606 OID 2424095)
-- Name: anforderung_anhang fk_anforderung_anhang_anhaenge_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_anhang
    ADD CONSTRAINT fk_anforderung_anhang_anhaenge_id FOREIGN KEY (anhaenge_id) REFERENCES anhang(id);


--
-- TOC entry 2460 (class 2606 OID 2424105)
-- Name: anforderung_auswirkung fk_anforderung_auswirkung_anforderung_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_auswirkung
    ADD CONSTRAINT fk_anforderung_auswirkung_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2461 (class 2606 OID 2424110)
-- Name: anforderung_auswirkung fk_anforderung_auswirkung_auswirkungen_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_auswirkung
    ADD CONSTRAINT fk_anforderung_auswirkung_auswirkungen_id FOREIGN KEY (auswirkungen_id) REFERENCES auswirkung(id);


--
-- TOC entry 2417 (class 2606 OID 2423890)
-- Name: anforderung fk_anforderung_bild_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_bild_id FOREIGN KEY (bild_id) REFERENCES bild(id);


--
-- TOC entry 2418 (class 2606 OID 2423895)
-- Name: anforderung fk_anforderung_fachlicheransprechpartner_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_fachlicheransprechpartner_id FOREIGN KEY (fachlicheransprechpartner_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2463 (class 2606 OID 2424120)
-- Name: anforderung_festgestelltin fk_anforderung_festgestelltin_anforderung_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_festgestelltin
    ADD CONSTRAINT fk_anforderung_festgestelltin_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2462 (class 2606 OID 2424115)
-- Name: anforderung_festgestelltin fk_anforderung_festgestelltin_festgestelltin_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_festgestelltin
    ADD CONSTRAINT fk_anforderung_festgestelltin_festgestelltin_id FOREIGN KEY (festgestelltin_id) REFERENCES festgestelltin(id);


--
-- TOC entry 2426 (class 2606 OID 2423935)
-- Name: anforderung_freigabe_bei_modul fk_anforderung_freigabe_bei_modul_anforderung_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_freigabe_bei_modul
    ADD CONSTRAINT fk_anforderung_freigabe_bei_modul_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2425 (class 2606 OID 2423930)
-- Name: anforderung_freigabe_bei_modul fk_anforderung_freigabe_bei_modul_bearbeiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_freigabe_bei_modul
    ADD CONSTRAINT fk_anforderung_freigabe_bei_modul_bearbeiter_id FOREIGN KEY (bearbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2427 (class 2606 OID 2423940)
-- Name: anforderung_freigabe_bei_modul fk_anforderung_freigabe_bei_modul_modul_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_freigabe_bei_modul
    ADD CONSTRAINT fk_anforderung_freigabe_bei_modul_modul_id FOREIGN KEY (modul_id) REFERENCES modul(id);


--
-- TOC entry 2416 (class 2606 OID 2423885)
-- Name: anforderung fk_anforderung_sensor_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_sensor_id FOREIGN KEY (sensor_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2421 (class 2606 OID 2423910)
-- Name: anforderung fk_anforderung_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- TOC entry 2419 (class 2606 OID 2423900)
-- Name: anforderung fk_anforderung_teamleiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_teamleiter_id FOREIGN KEY (teamleiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2420 (class 2606 OID 2423905)
-- Name: anforderung fk_anforderung_tteam_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung
    ADD CONSTRAINT fk_anforderung_tteam_id FOREIGN KEY (tteam_id) REFERENCES t_team(id);


--
-- TOC entry 2466 (class 2606 OID 2424135)
-- Name: anforderung_umsetzer fk_anforderung_umsetzer_anforderung_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_umsetzer
    ADD CONSTRAINT fk_anforderung_umsetzer_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2467 (class 2606 OID 2424140)
-- Name: anforderung_umsetzer fk_anforderung_umsetzer_umsetzer_umsetzer_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anforderung_umsetzer
    ADD CONSTRAINT fk_anforderung_umsetzer_umsetzer_umsetzer_id FOREIGN KEY (umsetzer_umsetzer_id) REFERENCES umsetzer(umsetzer_id);


--
-- TOC entry 2452 (class 2606 OID 2424065)
-- Name: anhang fk_anhang_content_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anhang
    ADD CONSTRAINT fk_anhang_content_id FOREIGN KEY (content_id) REFERENCES dbfile(id);


--
-- TOC entry 2453 (class 2606 OID 2424070)
-- Name: anhang fk_anhang_creator_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY anhang
    ADD CONSTRAINT fk_anhang_creator_id FOREIGN KEY (creator_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2431 (class 2606 OID 2423960)
-- Name: berechtigung_derivat fk_berechtigung_derivat_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY berechtigung_derivat
    ADD CONSTRAINT fk_berechtigung_derivat_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2450 (class 2606 OID 2424055)
-- Name: berechtigung fk_berechtigung_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY berechtigung
    ADD CONSTRAINT fk_berechtigung_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2481 (class 2606 OID 2424210)
-- Name: derivat_kov_a_phase_im_derivat fk_derivat_kov_a_phase_im_derivat_derivat_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY derivat_kov_a_phase_im_derivat
    ADD CONSTRAINT fk_derivat_kov_a_phase_im_derivat_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);


--
-- TOC entry 2480 (class 2606 OID 2424205)
-- Name: derivat_kov_a_phase_im_derivat fk_derivat_kov_a_phase_im_derivat_kovaphasen_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY derivat_kov_a_phase_im_derivat
    ADD CONSTRAINT fk_derivat_kov_a_phase_im_derivat_kovaphasen_id FOREIGN KEY (kovaphasen_id) REFERENCES kov_a_phase_im_derivat(id);


--
-- TOC entry 2437 (class 2606 OID 2423990)
-- Name: kov_a_phase_im_derivat fk_kov_a_phase_im_derivat_derivat_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY kov_a_phase_im_derivat
    ADD CONSTRAINT fk_kov_a_phase_im_derivat_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);


--
-- TOC entry 2438 (class 2606 OID 2423995)
-- Name: kov_a_phase_im_derivat fk_kov_a_phase_im_derivat_umsetzungsbestaetiger_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY kov_a_phase_im_derivat
    ADD CONSTRAINT fk_kov_a_phase_im_derivat_umsetzungsbestaetiger_id FOREIGN KEY (umsetzungsbestaetiger_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2464 (class 2606 OID 2424125)
-- Name: meldung_anforderung fk_meldung_anforderung_anforderung_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung_anforderung
    ADD CONSTRAINT fk_meldung_anforderung_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2465 (class 2606 OID 2424130)
-- Name: meldung_anforderung fk_meldung_anforderung_meldung_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung_anforderung
    ADD CONSTRAINT fk_meldung_anforderung_meldung_id FOREIGN KEY (meldung_id) REFERENCES meldung(id);


--
-- TOC entry 2472 (class 2606 OID 2424165)
-- Name: meldung_anhang fk_meldung_anhang_anhaenge_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung_anhang
    ADD CONSTRAINT fk_meldung_anhang_anhaenge_id FOREIGN KEY (anhaenge_id) REFERENCES anhang(id);


--
-- TOC entry 2473 (class 2606 OID 2424170)
-- Name: meldung_anhang fk_meldung_anhang_meldung_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung_anhang
    ADD CONSTRAINT fk_meldung_anhang_meldung_id FOREIGN KEY (meldung_id) REFERENCES meldung(id);


--
-- TOC entry 2474 (class 2606 OID 2424175)
-- Name: meldung_auswirkung fk_meldung_auswirkung_auswirkungen_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung_auswirkung
    ADD CONSTRAINT fk_meldung_auswirkung_auswirkungen_id FOREIGN KEY (auswirkungen_id) REFERENCES auswirkung(id);


--
-- TOC entry 2475 (class 2606 OID 2424180)
-- Name: meldung_auswirkung fk_meldung_auswirkung_meldung_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung_auswirkung
    ADD CONSTRAINT fk_meldung_auswirkung_meldung_id FOREIGN KEY (meldung_id) REFERENCES meldung(id);


--
-- TOC entry 2443 (class 2606 OID 2424020)
-- Name: meldung fk_meldung_bild_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_bild_id FOREIGN KEY (bild_id) REFERENCES bild(id);


--
-- TOC entry 2445 (class 2606 OID 2424030)
-- Name: meldung fk_meldung_fachlicheransprechpartner_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_fachlicheransprechpartner_id FOREIGN KEY (fachlicheransprechpartner_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2477 (class 2606 OID 2424190)
-- Name: meldung_festgestelltin fk_meldung_festgestelltin_festgestelltin_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung_festgestelltin
    ADD CONSTRAINT fk_meldung_festgestelltin_festgestelltin_id FOREIGN KEY (festgestelltin_id) REFERENCES festgestelltin(id);


--
-- TOC entry 2476 (class 2606 OID 2424185)
-- Name: meldung_festgestelltin fk_meldung_festgestelltin_meldung_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung_festgestelltin
    ADD CONSTRAINT fk_meldung_festgestelltin_meldung_id FOREIGN KEY (meldung_id) REFERENCES meldung(id);


--
-- TOC entry 2442 (class 2606 OID 2424015)
-- Name: meldung fk_meldung_sensor_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_sensor_id FOREIGN KEY (sensor_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2444 (class 2606 OID 2424025)
-- Name: meldung fk_meldung_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- TOC entry 2446 (class 2606 OID 2424035)
-- Name: meldung fk_meldung_teamleiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_teamleiter_id FOREIGN KEY (teamleiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2441 (class 2606 OID 2424010)
-- Name: meldung fk_meldung_tteam_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY meldung
    ADD CONSTRAINT fk_meldung_tteam_id FOREIGN KEY (tteam_id) REFERENCES t_team(id);


--
-- TOC entry 2487 (class 2606 OID 2424240)
-- Name: mitarbeiter_derivat fk_mitarbeiter_derivat_derivaten_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_derivat
    ADD CONSTRAINT fk_mitarbeiter_derivat_derivaten_id FOREIGN KEY (derivaten_id) REFERENCES derivat(id);


--
-- TOC entry 2486 (class 2606 OID 2424235)
-- Name: mitarbeiter_derivat fk_mitarbeiter_derivat_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_derivat
    ADD CONSTRAINT fk_mitarbeiter_derivat_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2488 (class 2606 OID 2424245)
-- Name: mitarbeiter_modul fk_mitarbeiter_modul_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_modul
    ADD CONSTRAINT fk_mitarbeiter_modul_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2489 (class 2606 OID 2424250)
-- Name: mitarbeiter_modul fk_mitarbeiter_modul_moduls_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_modul
    ADD CONSTRAINT fk_mitarbeiter_modul_moduls_id FOREIGN KEY (moduls_id) REFERENCES modul(id);


--
-- TOC entry 2491 (class 2606 OID 2424260)
-- Name: mitarbeiter_selectedderivate fk_mitarbeiter_selectedderivate_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_selectedderivate
    ADD CONSTRAINT fk_mitarbeiter_selectedderivate_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2490 (class 2606 OID 2424255)
-- Name: mitarbeiter_selectedderivate fk_mitarbeiter_selectedderivate_selectedderivate_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_selectedderivate
    ADD CONSTRAINT fk_mitarbeiter_selectedderivate_selectedderivate_id FOREIGN KEY (selectedderivate_id) REFERENCES derivat(id);


--
-- TOC entry 2492 (class 2606 OID 2424265)
-- Name: mitarbeiter_selectedzakderivate fk_mitarbeiter_selectedzakderivate_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_selectedzakderivate
    ADD CONSTRAINT fk_mitarbeiter_selectedzakderivate_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2493 (class 2606 OID 2424270)
-- Name: mitarbeiter_selectedzakderivate fk_mitarbeiter_selectedzakderivate_selectedzakderivate_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_selectedzakderivate
    ADD CONSTRAINT fk_mitarbeiter_selectedzakderivate_selectedzakderivate_id FOREIGN KEY (selectedzakderivate_id) REFERENCES derivat(id);


--
-- TOC entry 2496 (class 2606 OID 2424285)
-- Name: mitarbeiter_sensorcoc_lese fk_mitarbeiter_sensorcoc_lese_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_sensorcoc_lese
    ADD CONSTRAINT fk_mitarbeiter_sensorcoc_lese_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2497 (class 2606 OID 2424290)
-- Name: mitarbeiter_sensorcoc_lese fk_mitarbeiter_sensorcoc_lese_sensorcocslese_sensorcocid; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_sensorcoc_lese
    ADD CONSTRAINT fk_mitarbeiter_sensorcoc_lese_sensorcocslese_sensorcocid FOREIGN KEY (sensorcocslese_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- TOC entry 2494 (class 2606 OID 2424275)
-- Name: mitarbeiter_sensorcoc fk_mitarbeiter_sensorcoc_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_sensorcoc
    ADD CONSTRAINT fk_mitarbeiter_sensorcoc_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2495 (class 2606 OID 2424280)
-- Name: mitarbeiter_sensorcoc fk_mitarbeiter_sensorcoc_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_sensorcoc
    ADD CONSTRAINT fk_mitarbeiter_sensorcoc_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- TOC entry 2499 (class 2606 OID 2424300)
-- Name: mitarbeiter_tteam fk_mitarbeiter_tteam_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_tteam
    ADD CONSTRAINT fk_mitarbeiter_tteam_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2498 (class 2606 OID 2424295)
-- Name: mitarbeiter_tteam fk_mitarbeiter_tteam_tteams_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_tteam
    ADD CONSTRAINT fk_mitarbeiter_tteam_tteams_id FOREIGN KEY (tteams_id) REFERENCES t_team(id);


--
-- TOC entry 2485 (class 2606 OID 2424230)
-- Name: mitarbeiter_zakeinordnungen fk_mitarbeiter_zakeinordnungen_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY mitarbeiter_zakeinordnungen
    ADD CONSTRAINT fk_mitarbeiter_zakeinordnungen_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2455 (class 2606 OID 2424080)
-- Name: modul fk_modul_modulbild_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modul
    ADD CONSTRAINT fk_modul_modulbild_id FOREIGN KEY (modulbild_id) REFERENCES bild(id);


--
-- TOC entry 2502 (class 2606 OID 2424315)
-- Name: modul_modulseteam fk_modul_modulseteam_modul_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modul_modulseteam
    ADD CONSTRAINT fk_modul_modulseteam_modul_id FOREIGN KEY (modul_id) REFERENCES modul(id);


--
-- TOC entry 2503 (class 2606 OID 2424320)
-- Name: modul_modulseteam fk_modul_modulseteam_seteams_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modul_modulseteam
    ADD CONSTRAINT fk_modul_modulseteam_seteams_id FOREIGN KEY (seteams_id) REFERENCES modulseteam(id);


--
-- TOC entry 2448 (class 2606 OID 2424045)
-- Name: modulbild fk_modulbild_bild_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modulbild
    ADD CONSTRAINT fk_modulbild_bild_id FOREIGN KEY (bild_id) REFERENCES anhang(id);


--
-- TOC entry 2447 (class 2606 OID 2424040)
-- Name: modulbild fk_modulbild_modul_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modulbild
    ADD CONSTRAINT fk_modulbild_modul_id FOREIGN KEY (modul_id) REFERENCES modul(id);


--
-- TOC entry 2435 (class 2606 OID 2423980)
-- Name: modulkomponente fk_modulkomponente_seteam_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modulkomponente
    ADD CONSTRAINT fk_modulkomponente_seteam_id FOREIGN KEY (seteam_id) REFERENCES modulseteam(id);


--
-- TOC entry 2436 (class 2606 OID 2423985)
-- Name: modulseteam fk_modulseteam_elternmodul_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modulseteam
    ADD CONSTRAINT fk_modulseteam_elternmodul_id FOREIGN KEY (elternmodul_id) REFERENCES modul(id);


--
-- TOC entry 2469 (class 2606 OID 2424150)
-- Name: modulseteam_modulkomponente fk_modulseteam_modulkomponente_komponenten_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modulseteam_modulkomponente
    ADD CONSTRAINT fk_modulseteam_modulkomponente_komponenten_id FOREIGN KEY (komponenten_id) REFERENCES modulkomponente(id);


--
-- TOC entry 2468 (class 2606 OID 2424145)
-- Name: modulseteam_modulkomponente fk_modulseteam_modulkomponente_modulseteam_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY modulseteam_modulkomponente
    ADD CONSTRAINT fk_modulseteam_modulkomponente_modulseteam_id FOREIGN KEY (modulseteam_id) REFERENCES modulseteam(id);


--
-- TOC entry 2482 (class 2606 OID 2424215)
-- Name: searchfilter_longlistvalue fk_searchfilter_longlistvalue_searchfilter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY searchfilter_longlistvalue
    ADD CONSTRAINT fk_searchfilter_longlistvalue_searchfilter_id FOREIGN KEY (searchfilter_id) REFERENCES search_filter(id);


--
-- TOC entry 2483 (class 2606 OID 2424220)
-- Name: searchfilter_statuslistvalue fk_searchfilter_statuslistvalue_searchfilter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY searchfilter_statuslistvalue
    ADD CONSTRAINT fk_searchfilter_statuslistvalue_searchfilter_id FOREIGN KEY (searchfilter_id) REFERENCES search_filter(id);


--
-- TOC entry 2484 (class 2606 OID 2424225)
-- Name: searchfilter_stringlistvalue fk_searchfilter_stringlistvalue_searchfilter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY searchfilter_stringlistvalue
    ADD CONSTRAINT fk_searchfilter_stringlistvalue_searchfilter_id FOREIGN KEY (searchfilter_id) REFERENCES search_filter(id);


--
-- TOC entry 2449 (class 2606 OID 2424050)
-- Name: sensorcoc fk_sensorcoc_fachlicheransprechpartner_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY sensorcoc
    ADD CONSTRAINT fk_sensorcoc_fachlicheransprechpartner_id FOREIGN KEY (fachlicheransprechpartner_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2479 (class 2606 OID 2424200)
-- Name: sensorcoc_mitarbeiter fk_sensorcoc_mitarbeiter_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY sensorcoc_mitarbeiter
    ADD CONSTRAINT fk_sensorcoc_mitarbeiter_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- TOC entry 2478 (class 2606 OID 2424195)
-- Name: sensorcoc_mitarbeiter fk_sensorcoc_mitarbeiter_umsetzungsbestaetiger_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY sensorcoc_mitarbeiter
    ADD CONSTRAINT fk_sensorcoc_mitarbeiter_umsetzungsbestaetiger_id FOREIGN KEY (umsetzungsbestaetiger_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2429 (class 2606 OID 2423950)
-- Name: status_vereinbarung_anforderung_im_derivat fk_status_vereinbarung_anforderung_im_derivat_anforderer_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT fk_status_vereinbarung_anforderung_im_derivat_anforderer_id FOREIGN KEY (anforderer_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2428 (class 2606 OID 2423945)
-- Name: status_vereinbarung_anforderung_im_derivat fk_status_vereinbarung_anforderung_im_derivat_anforderung_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT fk_status_vereinbarung_anforderung_im_derivat_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2430 (class 2606 OID 2423955)
-- Name: status_vereinbarung_anforderung_im_derivat fk_status_vereinbarung_anforderung_im_derivat_derivat_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY status_vereinbarung_anforderung_im_derivat
    ADD CONSTRAINT fk_status_vereinbarung_anforderung_im_derivat_derivat_id FOREIGN KEY (derivat_id) REFERENCES derivat(id);


--
-- TOC entry 2454 (class 2606 OID 2424075)
-- Name: t_team fk_t_team_teamleiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY t_team
    ADD CONSTRAINT fk_t_team_teamleiter_id FOREIGN KEY (teamleiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2422 (class 2606 OID 2423915)
-- Name: ub_zu_kova_phase_im_derivat_und_sensorcoc fk_ub_zu_kova_phase_im_derivat_und_sensorcoc_kovaimderivat_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc
    ADD CONSTRAINT fk_ub_zu_kova_phase_im_derivat_und_sensorcoc_kovaimderivat_id FOREIGN KEY (kovaimderivat_id) REFERENCES kov_a_phase_im_derivat(id);


--
-- TOC entry 2440 (class 2606 OID 2424005)
-- Name: umsetzer fk_umsetzer_komponente_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY umsetzer
    ADD CONSTRAINT fk_umsetzer_komponente_id FOREIGN KEY (komponente_id) REFERENCES modulkomponente(id);


--
-- TOC entry 2439 (class 2606 OID 2424000)
-- Name: umsetzer fk_umsetzer_seteam_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY umsetzer
    ADD CONSTRAINT fk_umsetzer_seteam_id FOREIGN KEY (seteam_id) REFERENCES modulseteam(id);


--
-- TOC entry 2433 (class 2606 OID 2423970)
-- Name: umsetzungsbestaetigung fk_umsetzungsbestaetigung_anforderung_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY umsetzungsbestaetigung
    ADD CONSTRAINT fk_umsetzungsbestaetigung_anforderung_id FOREIGN KEY (anforderung_id) REFERENCES anforderung(id);


--
-- TOC entry 2434 (class 2606 OID 2423975)
-- Name: umsetzungsbestaetigung fk_umsetzungsbestaetigung_kovaimderivat_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY umsetzungsbestaetigung
    ADD CONSTRAINT fk_umsetzungsbestaetigung_kovaimderivat_id FOREIGN KEY (kovaimderivat_id) REFERENCES kov_a_phase_im_derivat(id);


--
-- TOC entry 2432 (class 2606 OID 2423965)
-- Name: umsetzungsbestaetigung fk_umsetzungsbestaetigung_umsetzungsbestaetiger_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY umsetzungsbestaetigung
    ADD CONSTRAINT fk_umsetzungsbestaetigung_umsetzungsbestaetiger_id FOREIGN KEY (umsetzungsbestaetiger_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2451 (class 2606 OID 2424060)
-- Name: user_defined_search fk_user_defined_search_mitarbeiter_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY user_defined_search
    ADD CONSTRAINT fk_user_defined_search_mitarbeiter_id FOREIGN KEY (mitarbeiter_id) REFERENCES mitarbeiter(id);


--
-- TOC entry 2500 (class 2606 OID 2424305)
-- Name: user_defined_search_search_filter fk_user_defined_search_search_filter_searchfilterlist_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY user_defined_search_search_filter
    ADD CONSTRAINT fk_user_defined_search_search_filter_searchfilterlist_id FOREIGN KEY (searchfilterlist_id) REFERENCES search_filter(id);


--
-- TOC entry 2501 (class 2606 OID 2424310)
-- Name: user_defined_search_search_filter fk_user_defined_search_search_filter_userdefinedsearch_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY user_defined_search_search_filter
    ADD CONSTRAINT fk_user_defined_search_search_filter_userdefinedsearch_id FOREIGN KEY (userdefinedsearch_id) REFERENCES user_defined_search(id);





--
-- TOC entry 2424 (class 2606 OID 2423925)
-- Name: ub_zu_kova_phase_im_derivat_und_sensorcoc ub_zu_kova_phase_im_derivat_und_sensorcoc_sensorcoc_sensorcocid; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc
    ADD CONSTRAINT ub_zu_kova_phase_im_derivat_und_sensorcoc_sensorcoc_sensorcocid FOREIGN KEY (sensorcoc_sensorcocid) REFERENCES sensorcoc(sensorcocid);


--
-- TOC entry 2423 (class 2606 OID 2423920)
-- Name: ub_zu_kova_phase_im_derivat_und_sensorcoc ubzukova_phase_im_derivat_und_sensorcocumsetzungsbestaetiger_id; Type: FK CONSTRAINT; Schema: public; OWNER:
--

ALTER TABLE ONLY ub_zu_kova_phase_im_derivat_und_sensorcoc
    ADD CONSTRAINT ubzukova_phase_im_derivat_und_sensorcocumsetzungsbestaetiger_id FOREIGN KEY (umsetzungsbestaetiger_id) REFERENCES mitarbeiter(id);


-- Completed on 2017-07-21 15:34:58

--
-- PostgreSQL database dump complete
--

-- additional insert statement for creating the internally managed sequence table for eclipse persistence manager
INSERT INTO sequence (seq_name, seq_count)
VALUES ('SEQ_GEN', '6950');