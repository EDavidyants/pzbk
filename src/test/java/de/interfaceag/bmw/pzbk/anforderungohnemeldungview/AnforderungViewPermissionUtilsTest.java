package de.interfaceag.bmw.pzbk.anforderungohnemeldungview;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.permission.AnforderungViewPermissionUtils;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.EditViewPermission;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl
 */
public class AnforderungViewPermissionUtilsTest {

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testBuildPermissionForProzessbaukastenButtonRead(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);
        EditViewPermission permission = AnforderungViewPermissionUtils.buildPermissionForProzessbaukastenButton(userRoles);
        boolean result = permission.isRendered();
        switch (role) {
            case LESER:
                Assertions.assertFalse(result);
                break;
            default:
                Assertions.assertTrue(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testBuildPermissionForProzessbaukastenButtonWrite(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);
        EditViewPermission permission = AnforderungViewPermissionUtils.buildPermissionForProzessbaukastenButton(userRoles);
        boolean result = permission.hasRightToEdit();
        switch (role) {
            case ADMIN:
            case TTEAMMITGLIED:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
                Assertions.assertTrue(result);
                break;
            default:
                Assertions.assertFalse(result);
                break;
        }
    }
}
