package de.interfaceag.bmw.pzbk.anforderungohnemeldungview;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.ImageService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.UserSession;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.zak.ZakVereinbarungService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author fn
 */
public class AnforderungOhneMeldungViewFacadeTest {

    private AnforderungOhneMeldungViewFacade viewFacade;

    private HttpServletRequest request;

    @Before
    public void setUp() {

        viewFacade = mock(AnforderungOhneMeldungViewFacade.class);
        viewFacade.anforderungService = mock(AnforderungService.class);
        viewFacade.imageService = mock(ImageService.class);
        viewFacade.zuordnungAnforderungDerivatService = mock(ZuordnungAnforderungDerivatService.class);
        viewFacade.derivatService = mock(DerivatService.class);
        viewFacade.zakVereinbarungService = mock(ZakVereinbarungService.class);
        viewFacade.session = mock(UserSession.class);
        viewFacade.berechtigungService = mock(BerechtigungService.class);
        viewFacade.prozessbaukastenZuordnenService = mock(ProzessbaukastenZuordnenService.class);
        request = mock(HttpServletRequest.class);
        when(request.getParameterMap()).thenReturn(new HashMap<>());
    }

    @Test
    public void getViewDataForCorrectFachIdAndVersion() {

        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "A42");
        urlParameter.addOrReplaceParamter("version", "1");

        when(viewFacade.anforderungService.getAnforderungByFachIdVersion("A42", 1)).thenReturn(TestDataFactory.generateAnforderung());
        when(viewFacade.session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("test"));
        when(viewFacade.anforderungService.isUserBerechtigtForAnforderung(any())).thenReturn(Boolean.TRUE);
        when(viewFacade.berechtigungService.getTteamleiterOfTteam(any())).thenReturn(Optional.ofNullable(TestDataFactory.createMitarbeiter("MaxTTeam")));
        when(viewFacade.berechtigungService.getSensorCoCLeiterOfSensorCoc(any())).thenReturn(Optional.ofNullable(TestDataFactory.createMitarbeiter("MaxSensorcCocLeiter")));
        when(viewFacade.prozessbaukastenZuordnenService.getAddProzessbaukastenDialogViewData(any())).thenReturn(TestDataFactory.generateProzessbaukastenZuordnenDialogViewData());
        when(viewFacade.prozessbaukastenZuordnenService.prozessbaukastenZuordnen(any(), any())).thenReturn("");
        Set<Rolle> rolesWithWritePermissions = new HashSet<>();
        rolesWithWritePermissions.add(Rolle.ADMIN);
        when(viewFacade.getUserRolesWithWritePermissions(any())).thenReturn(rolesWithWritePermissions);
        when(viewFacade.getViewData(any())).thenCallRealMethod();

        AnforderungOhneMeldungViewData viewObjekt = viewFacade.getViewData(urlParameter);
        AnforderungOhneMeldungViewAnforderungData viewData = viewObjekt.getAnforderungOhneMeldungViewAnforderungData().get();
        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewObjekt),
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertTrue(viewData.getFachId().equals("A42")),
                () -> Assertions.assertTrue(viewData.getVersion().equals(1)));

    }

    @Test
    public void getViewDataForIncorrectFachId() {

        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "12");
        urlParameter.addOrReplaceParamter("version", "1");

        when(viewFacade.getViewData(any())).thenCallRealMethod();

        AnforderungOhneMeldungViewData viewObjekt = viewFacade.getViewData(urlParameter);
        Assertions.assertFalse(viewObjekt.getAnforderungOhneMeldungViewAnforderungData().isPresent());

    }

    @Test
    public void getViewDataForIncorrectVersion() {

        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "42");
        urlParameter.addOrReplaceParamter("version", "0");
        when(viewFacade.getViewData(any())).thenCallRealMethod();
        AnforderungOhneMeldungViewData viewObjekt = viewFacade.getViewData(urlParameter);
        Assertions.assertFalse(viewObjekt.getAnforderungOhneMeldungViewAnforderungData().isPresent());

    }

    @Test
    public void getViewDataForIDWithIncorrectFachID() {

        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "42");
        urlParameter.addOrReplaceParamter("version", "1");
        urlParameter.addOrReplaceParamter("id", "42");

        when(viewFacade.anforderungService.getAnforderungById(any())).thenReturn(TestDataFactory.generateAnforderung());
        when(viewFacade.session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("test"));
        when(viewFacade.anforderungService.isUserBerechtigtForAnforderung(any())).thenReturn(Boolean.TRUE);
        when(viewFacade.berechtigungService.getTteamleiterOfTteam(any())).thenReturn(Optional.ofNullable(TestDataFactory.createMitarbeiter("MaxTTeam")));

        when(viewFacade.berechtigungService.getSensorCoCLeiterOfSensorCoc(any())).thenReturn(Optional.ofNullable(TestDataFactory.createMitarbeiter("MaxSensorcCocLeiter")));

        when(viewFacade.prozessbaukastenZuordnenService.getAddProzessbaukastenDialogViewData(any())).thenReturn(TestDataFactory.generateProzessbaukastenZuordnenDialogViewData());
        when(viewFacade.prozessbaukastenZuordnenService.prozessbaukastenZuordnen(any(), any())).thenReturn("");
        Set<Rolle> rolesWithWritePermissions = new HashSet<>();
        rolesWithWritePermissions.add(Rolle.ADMIN);
        when(viewFacade.getUserRolesWithWritePermissions(any())).thenReturn(rolesWithWritePermissions);
        when(viewFacade.getViewData(any())).thenCallRealMethod();

        AnforderungOhneMeldungViewData viewObjekt = viewFacade.getViewData(urlParameter);
        AnforderungOhneMeldungViewAnforderungData viewData = viewObjekt.getAnforderungOhneMeldungViewAnforderungData().get();
        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewObjekt),
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertTrue(viewData.getFachId().equals("A42")),
                () -> Assertions.assertTrue(viewData.getVersion().equals(1)));

    }

    @Test
    public void getViewDataForIDWithIncorrectVersion() {

        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "A42");
        urlParameter.addOrReplaceParamter("version", "0");
        urlParameter.addOrReplaceParamter("id", "42");

        when(viewFacade.anforderungService.getAnforderungById(any())).thenReturn(TestDataFactory.generateAnforderung());
        when(viewFacade.session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("test"));
        when(viewFacade.anforderungService.isUserBerechtigtForAnforderung(any())).thenReturn(Boolean.TRUE);
        when(viewFacade.berechtigungService.getTteamleiterOfTteam(any())).thenReturn(Optional.ofNullable(TestDataFactory.createMitarbeiter("MaxTTeam")));

        when(viewFacade.berechtigungService.getSensorCoCLeiterOfSensorCoc(any())).thenReturn(Optional.ofNullable(TestDataFactory.createMitarbeiter("MaxSensorcCocLeiter")));
        when(viewFacade.prozessbaukastenZuordnenService.getAddProzessbaukastenDialogViewData(any())).thenReturn(TestDataFactory.generateProzessbaukastenZuordnenDialogViewData());
        when(viewFacade.prozessbaukastenZuordnenService.prozessbaukastenZuordnen(any(), any())).thenReturn("");
        Set<Rolle> rolesWithWritePermissions = new HashSet<>();
        rolesWithWritePermissions.add(Rolle.ADMIN);
        when(viewFacade.getUserRolesWithWritePermissions(any())).thenReturn(rolesWithWritePermissions);
        when(viewFacade.getViewData(any())).thenCallRealMethod();

        AnforderungOhneMeldungViewData viewObjekt = viewFacade.getViewData(urlParameter);
        AnforderungOhneMeldungViewAnforderungData viewData = viewObjekt.getAnforderungOhneMeldungViewAnforderungData().get();
        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewObjekt),
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertTrue(viewData.getFachId().equals("A42")),
                () -> Assertions.assertTrue(viewData.getVersion().equals(1)));
    }

    @Test
    public void getModulAuthorizationMapTest() {

        Anforderung anforderung = TestDataFactory.generateAnforderung();

        when(viewFacade.getModulAuthorizationMapForAnforderung(any())).thenCallRealMethod();
        when(viewFacade.anforderungService.isAuthorizedForModulSeTeam(any(), any(), any())).thenReturn(Boolean.TRUE);

        Map<ModulSeTeam, Boolean> modulAuthorizationMap = viewFacade.getModulAuthorizationMapForAnforderung(anforderung);

        Assertions.assertTrue(!modulAuthorizationMap.isEmpty());

    }

}
