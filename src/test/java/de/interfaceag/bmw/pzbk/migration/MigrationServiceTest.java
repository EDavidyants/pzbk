package de.interfaceag.bmw.pzbk.migration;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
public class MigrationServiceTest {

    private MigrationService migrationService;

    @Before
    public void setup() {
        migrationService = new MigrationService();
        migrationService.modulService = mock(ModulService.class);

        when(migrationService.modulService.getModulKomponenteByPpg(any())).thenReturn(null);
        when(migrationService.modulService.getModulSeTeamByName(any())).thenReturn(null);
        when(migrationService.modulService.getModulByName(any())).thenReturn(null);
    }

    @Test
    public void parseModulListFromExcelSheetCase1() {
        List<Modul> result = migrationService.parseModulListFromExcelSheet(generateModulSheetCase1());
        Assertions.assertAll("Groesse",
                () -> {
                    assertEquals(1, result.size());
                    Modul m0 = result.get(0);

                    Assertions.assertAll("Modul",
                            () -> assertEquals("ML: CA", m0.getName()),
                            () -> assertEquals("Beschreibung", m0.getBeschreibung()),
                            () -> assertEquals("EE", m0.getFachBereich()),
                            () -> assertEquals(1, m0.getSeTeams().size())
                    );

                    ModulSeTeam s0 = new ArrayList<>(m0.getSeTeams()).get(0);

                    Assertions.assertAll("SE-Team",
                            () -> assertEquals("CA01", s0.getName()),
                            () -> assertEquals("ML: CA", s0.getElternModul().getName()),
                            () -> assertEquals(2, s0.getKomponenten().size())
                    );
                }
        );
    }

    @Test
    public void parseModulListFromExcelSheetCase2() {
        List<Modul> result = migrationService.parseModulListFromExcelSheet(generateModulSheetCase2());
        Assertions.assertAll("Groesse",
                () -> {
                    assertEquals(1, result.size());
                    Modul m0 = result.get(0);

                    Assertions.assertAll("Modul",
                            () -> assertEquals("ML: CA", m0.getName()),
                            () -> assertEquals("Beschreibung", m0.getBeschreibung()),
                            () -> assertEquals("EE", m0.getFachBereich()),
                            () -> assertEquals(3, m0.getSeTeams().size())
                    );
                }
        );
    }

//    @Test
    public void parseUmsetzer() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
//        String umsetzerString = "ML: Motorprojekt V-Motoren / MA01 (V-Motor) / KURBELGEHAEUSE (V-Motor)(P1111_001)\n"
//                + "ML: Motorprojekt Diesel Baukastenmotoren / MA01 (Diesel) / KURBELGEHAEUSE (Diesel)(P1111_001)\n";
        String umsetzerString = "ML: Kühlung / Ansaugluftführung Otto / ME01 (Otto)\n";
        Set<Umsetzer> result = migrationService.parseUmsetzer(anforderung, umsetzerString);
    }

    // ---------- utils --------------------------------------------------------
    private Sheet generateModulSheetCase1() {
        HSSFWorkbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("Umsetzerliste");
        Row headerRow = sheet.createRow(0);

        // create header
        headerRow.createCell(0).setCellValue("FB");
        headerRow.createCell(1).setCellValue("EGV/EV,ML");
        headerRow.createCell(2).setCellValue("Beschreibung");
        headerRow.createCell(3).setCellValue("SE-Team");
        headerRow.createCell(4).setCellValue("Komponente");
        headerRow.createCell(5).setCellValue("PPG");

        Row row = sheet.createRow(1);
        row.createCell(0).setCellValue("EE");
        row.createCell(1).setCellValue("ML: CA");
        row.createCell(2).setCellValue("Beschreibung");
        row.createCell(3).setCellValue("CA01");
        row.createCell(4).setCellValue("RADAR");
        row.createCell(5).setCellValue("P0042");

        row = sheet.createRow(2);
        row.createCell(0).setCellValue("EE");
        row.createCell(1).setCellValue("ML: CA");
        row.createCell(2).setCellValue("Beschreibung");
        row.createCell(3).setCellValue("CA01");
        row.createCell(4).setCellValue("LIDAR");
        row.createCell(5).setCellValue("P0043");

        return wb.getSheet("Umsetzerliste");
    }

    private Sheet generateModulSheetCase2() {
        HSSFWorkbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("Umsetzerliste");
        Row headerRow = sheet.createRow(0);

        // create header
        headerRow.createCell(0).setCellValue("FB");
        headerRow.createCell(1).setCellValue("EGV/EV,ML");
        headerRow.createCell(2).setCellValue("Beschreibung");
        headerRow.createCell(3).setCellValue("SE-Team");
        headerRow.createCell(4).setCellValue("Komponente");
        headerRow.createCell(5).setCellValue("PPG");

        Row row = sheet.createRow(1);
        row.createCell(0).setCellValue("EE");
        row.createCell(1).setCellValue("ML: CA");
        row.createCell(2).setCellValue("Beschreibung");
        row.createCell(3).setCellValue("CA01");
        row.createCell(4).setCellValue("RADAR");
        row.createCell(5).setCellValue("P0042");

        row = sheet.createRow(2);
        row.createCell(0).setCellValue("EE");
        row.createCell(1).setCellValue("ML: CA");
        row.createCell(2).setCellValue("Beschreibung");
        row.createCell(3).setCellValue("CA03");
        row.createCell(4).setCellValue("LIDAR");
        row.createCell(5).setCellValue("P0043");

        row = sheet.createRow(3);
        row.createCell(0).setCellValue("EE");
        row.createCell(1).setCellValue("ML: CA");
        row.createCell(2).setCellValue("Beschreibung");
        row.createCell(3).setCellValue("CA02");
        row.createCell(4).setCellValue("KAMERA");
        row.createCell(5).setCellValue("P0044");

        return wb.getSheet("Umsetzerliste");
    }

}
