package de.interfaceag.bmw.pzbk.migration;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.doors.migration.DoorsAnforderungMigrationService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.services.TteamService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@RunWith(MockitoJUnitRunner.class)
public class DoorsAnforderungMigrationServiceTest {

    private static final Logger LOG = LoggerFactory.getLogger(DoorsAnforderungMigrationServiceTest.class);

    @Mock
    private AnforderungService anforderungService;

    @Mock
    private SensorCocService sensorCocService;

    @Mock
    private ModulService modulService;

    @Mock
    private TteamService tteamService;

    @Mock
    private AnforderungMeldungHistoryService historyService;

    @Mock
    private BerechtigungService berechtigungService;

    @InjectMocks
    private final DoorsAnforderungMigrationService doorsMigrationService = new DoorsAnforderungMigrationService();

    private Class<?>[] params;
    private Method validateFieldsMethod;
    private Method createDoorsAnforderungMethod;

    @Before
    public void SetUp() {
        params = new Class<?>[]{String.class, String.class, String.class, String.class,
            String.class, String.class, String.class, String.class, String.class, String.class,
            String.class, String.class, String.class, String.class, String.class};

        try {
            validateFieldsMethod = doorsMigrationService.getClass().getDeclaredMethod("validateFields", params);
            validateFieldsMethod.setAccessible(true);

            createDoorsAnforderungMethod = doorsMigrationService.getClass().getDeclaredMethod("createDoorsAnforderung", params);
            createDoorsAnforderungMethod.setAccessible(true);

        } catch (NoSuchMethodException | SecurityException ex) {
            LOG.error(null, ex);
        }

        List<FestgestelltIn> fgiF01 = new ArrayList<>();
        fgiF01.add(new FestgestelltIn("F01"));

        List<FestgestelltIn> fgiF06 = new ArrayList<>();
        fgiF06.add(new FestgestelltIn("F06"));

        List<FestgestelltIn> fgiF07 = new ArrayList<>();
        fgiF07.add(new FestgestelltIn("F07"));

        when(anforderungService.getFestgestelltInByWert("F01")).thenReturn(fgiF01);
        when(anforderungService.getFestgestelltInByWert("F06")).thenReturn(fgiF06);
        when(anforderungService.getFestgestelltInByWert("F07")).thenReturn(fgiF07);

        Tteam tteam = new Tteam("E-Antrieb - Integration");
        tteam.setId(18L);

        when(tteamService.getTteamByName("E-Antrieb - Integration")).thenReturn(tteam);

        SensorCoc sensorCoc = new SensorCoc();
        sensorCoc.setSensorCocId(9L);
        sensorCoc.setOrtung("Technologie Gesamtfahrzeug");
        sensorCoc.setRessort("T-Ressort");
        sensorCoc.setTechnologie("TGF_Hybrid");
        sensorCoc.setZakEinordnung("AK Direkt / AG: Herstellbarkeit SSA");

        when(sensorCocService.getSensorCocsByTechnologie("TGF_Hybrid")).thenReturn(Arrays.asList(sensorCoc));
        when(sensorCocService.getSensorCocsByTechnologie("nonExistingSensorCoc")).thenReturn(new ArrayList<>());

        Modul modul1 = new Modul("1 Modul");
        modul1.setId(3L);
        modul1.setBeschreibung("1 Modul");
        modul1.setFachBereich("EI");

        ModulSeTeam modulSeTeam7 = new ModulSeTeam();
        modulSeTeam7.setId(10L);
        modulSeTeam7.setName("SeTeam 7");
        modulSeTeam7.setElternModul(modul1);

        ModulSeTeam modulSeTeam8 = new ModulSeTeam();
        modulSeTeam8.setId(11L);
        modulSeTeam8.setName("SeTeam 8");
        modulSeTeam8.setElternModul(modul1);

        ModulSeTeam modulSeTeam9 = new ModulSeTeam();
        modulSeTeam9.setId(12L);
        modulSeTeam9.setName("SeTeam 9");
        modulSeTeam9.setElternModul(modul1);

        ModulSeTeam modulSeTeam10 = new ModulSeTeam();
        modulSeTeam10.setId(13L);
        modulSeTeam10.setName("SeTeam 10");
        modulSeTeam10.setElternModul(modul1);

        when(modulService.getModulByName("1 Modul")).thenReturn(modul1);
        when(modulService.getSeTeamsThatStartWith("SeTeam 7", modul1)).thenReturn(Arrays.asList(modulSeTeam7));
        when(modulService.getSeTeamsThatStartWith("unknownSeTeam", modul1)).thenReturn(new ArrayList<>());

        Anforderung a30000 = new Anforderung();
        a30000.setId(30000L);
        a30000.setFachId("A30000");
        a30000.setVersion(1);

        Anforderung a30001 = new Anforderung();
        a30000.setId(30001L);
        a30000.setFachId("A30001");
        a30000.setVersion(1);

        Anforderung a221 = new Anforderung();
        a221.setId(221L);
        a221.setFachId("A221");
        a221.setVersion(1);

        Anforderung a222 = new Anforderung();
        a222.setId(222L);
        a222.setFachId("A222");
        a222.setVersion(1);

        when(anforderungService.getAnforderungByFachIdVersion("A30000", 1)).thenReturn(null);
//        when(anforderungService.getAnforderungByFachIdVersion("A30001", 1)).thenReturn(null);
        when(anforderungService.getAnforderungByFachIdVersion("A221", 1)).thenReturn(a221);
//        when(anforderungService.getAnforderungByFachIdVersion("A222", 1)).thenReturn(a222);

//        when(anforderungService.persistAnforderung(a30000)).thenReturn(30000L);
        when(anforderungService.persistAnforderung(a30001)).thenReturn(30001L);
//        when(anforderungService.persistAnforderung(a221)).thenReturn(221L);
//        when(anforderungService.persistAnforderung(a222)).thenReturn(222L);

        when(anforderungService.getAnforderungById(30000L)).thenReturn(a30000);
        when(anforderungService.getAnforderungById(30001L)).thenReturn(a30001);
//        when(anforderungService.getAnforderungById(221L)).thenReturn(a221);
//        when(anforderungService.getAnforderungById(222L)).thenReturn(a222);

        List<AnforderungHistory> a221History = new ArrayList<>();

        when(historyService.getAnforderungHistoryByObjectnameAnforderungId(221L, "A", "Anforderung")).thenReturn(a221History);
//        when(historyService.getAnforderungHistoryByObjectnameAnforderungId(222L, "A", "Anforderung")).thenReturn(new ArrayList<>());
        doNothing().when(historyService).persistAnforderungHistory(any(AnforderungHistory.class));

        when(berechtigungService.getSensorCoCLeiterOfSensorCoc(any())).thenReturn(Optional.of(TestDataFactory.createMitarbeiter("")));

    }

    @Test
    public void validateFieldsPositivTest() {
        try {
            boolean isValid;
            String errMsg;

            Object[] allFilledIn = new Object[]{"A30000", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "Konzeptrelevant",
                "TGF_Hybrid", "in Arbeit", "1 Modul / SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            try {
                isValid = (boolean) validateFieldsMethod.invoke(doorsMigrationService, allFilledIn);
                Assertions.assertTrue(isValid, "All fields are filled in");
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.isEmpty());

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void validateFieldsNegativTestFachId() {
        try {
            boolean isValid;
            String errMsg;

            Object[] fachIdMissing = new Object[]{"", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "Konzeptrelevant",
                "TGF_Hybrid", "in Arbeit", "1 Modul / SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            try {
                isValid = (boolean) validateFieldsMethod.invoke(doorsMigrationService, fachIdMissing);
                Assertions.assertFalse(isValid, "FachId is missing");
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("Severe: No FachId provided!;"));

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void validateFieldsNegativTestFestgestelltIn() {
        try {
            boolean isValid;
            String errMsg;

            Object[] festgestelltInMissing = new Object[]{"A30000", "", "beschreibungDe", "Kommentar", "Loesung", "Konzeptrelevant",
                "TGF_Hybrid", "in Arbeit", "1 Modul / SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            try {
                isValid = (boolean) validateFieldsMethod.invoke(doorsMigrationService, festgestelltInMissing);
                Assertions.assertFalse(isValid, "FestgestelltIn is missing");
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("Severe: No FestgestelltIn provided!;"));

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void validateFieldsNegativTestBeschreibungDe() {
        try {
            boolean isValid;
            String errMsg;

            Object[] beschreibungDeMissing = new Object[]{"A30000", "F01; F06; F07", "", "Kommentar", "Loesung", "Konzeptrelevant",
                "TGF_Hybrid", "in Arbeit", "1 Modul / SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            try {
                isValid = (boolean) validateFieldsMethod.invoke(doorsMigrationService, beschreibungDeMissing);
                Assertions.assertFalse(isValid, "BeschreibungDe is missing");
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("Severe: BeschreibungDe is missing!;"));

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void validateFieldsNegativTestPhasenbezug() {
        try {
            boolean isValid;
            String errMsg;

            Object[] phasenbezugMissing = new Object[]{"A30000", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "",
                "TGF_Hybrid", "in Arbeit", "1 Modul / SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            Object[] phasenbezugIllegal = new Object[]{"A30000", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "blabla",
                "TGF_Hybrid", "in Arbeit", "1 Modul / SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            try {
                isValid = (boolean) validateFieldsMethod.invoke(doorsMigrationService, phasenbezugMissing);
                Assertions.assertFalse(isValid, "Phasenbezug is missing");
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("Severe: Phasenbezug is missing or not a valid value!;"));

                isValid = (boolean) validateFieldsMethod.invoke(doorsMigrationService, phasenbezugIllegal);
                Assertions.assertFalse(isValid, "Illegal Phasenbezug");
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("Severe: Phasenbezug is missing or not a valid value!;"));

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void validateFieldsNegativTestSensorCoc() {
        try {
            boolean isValid;
            String errMsg;

            Object[] sensorCocMissing = new Object[]{"A30000", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "Konzeptrelevant",
                "", "in Arbeit", "1 Modul / SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            try {
                isValid = (boolean) validateFieldsMethod.invoke(doorsMigrationService, sensorCocMissing);
                Assertions.assertFalse(isValid, "SensorCoc is missing");
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("Severe: SensorCoc is missing!;"));

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void validateFieldsNegativTestStatus() {
        try {
            boolean isValid;
            String errMsg;

            Object[] statusMissing = new Object[]{"A30000", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "Konzeptrelevant",
                "TGF_Hybrid", "", "1 Modul / SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            try {
                isValid = (boolean) validateFieldsMethod.invoke(doorsMigrationService, statusMissing);
                Assertions.assertFalse(isValid, "Status is missing");
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("Severe: Status is missing!;"));

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void validateFieldsNegativTestUmsetzer() {
        try {
            boolean isValid;
            String errMsg;

            Object[] umsetzerMissing = new Object[]{"A30000", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "Konzeptrelevant",
                "TGF_Hybrid", "in Arbeit", "", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            try {
                isValid = (boolean) validateFieldsMethod.invoke(doorsMigrationService, umsetzerMissing);
                Assertions.assertFalse(isValid, "Umsetzer is missing");
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("Severe: Umsetzer not defined!;"));

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void validateFieldsTestRefSystemEmpty() {
        try {
            boolean isValid;
            String errMsg;

            Object[] refSystemMissing = new Object[]{"A30000", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "Konzeptrelevant",
                "TGF_Hybrid", "in Arbeit", "1 Modul / SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "", "TI_HE_SYS_324"};

            try {
                isValid = (boolean) validateFieldsMethod.invoke(doorsMigrationService, refSystemMissing);
                Assertions.assertTrue(isValid, "ReferenzSystem is missing");
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("!Info: Specified Referenzsystem is not DOORS, setting DOORS per default;"));

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void validateFieldsTestRefSystemLinkEmpty() {
        try {
            boolean isValid;
            String errMsg;

            Object[] refSystemLinkMissing = new Object[]{"A30000", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "Konzeptrelevant",
                "TGF_Hybrid", "in Arbeit", "1 Modul / SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", ""};

            try {
                isValid = (boolean) validateFieldsMethod.invoke(doorsMigrationService, refSystemLinkMissing);
                Assertions.assertTrue(isValid, "ReferenzSystemLink is missing");
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("!Info: Referenzsystem Link not provided!;"));

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void parseFestgestelltInTest() {
        Class<?>[] festgestelltInParameterListe = new Class<?>[]{List.class};
        try {
            Method parseFestgestelltInMethod = doorsMigrationService.getClass().getDeclaredMethod("parseFestgestelltIn", festgestelltInParameterListe);
            parseFestgestelltInMethod.setAccessible(true);

            Set<String> festgestelltInStringList = new HashSet<>();
            festgestelltInStringList.add("F01");
            festgestelltInStringList.add("F06");
            festgestelltInStringList.add("F07");

            try {
                Set<FestgestelltIn> festgestelltInList = (Set<FestgestelltIn>) parseFestgestelltInMethod.invoke(doorsMigrationService, festgestelltInStringList);
                Assertions.assertEquals(3, festgestelltInList.size());

                FestgestelltIn f01 = new FestgestelltIn("F01");
                Assertions.assertTrue(festgestelltInList.contains(f01));

                FestgestelltIn f06 = new FestgestelltIn("F06");
                Assertions.assertTrue(festgestelltInList.contains(f06));

                FestgestelltIn f07 = new FestgestelltIn("F07");
                Assertions.assertTrue(festgestelltInList.contains(f07));

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (NoSuchMethodException | SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void parsePhasenbezugTest() {
        Class<?>[] phasenbezugParam = new Class<?>[]{String.class};

        try {
            Method parsePhasenbezugMethod = doorsMigrationService.getClass().getDeclaredMethod("parsePhasenbezug", phasenbezugParam);
            parsePhasenbezugMethod.setAccessible(true);

            boolean isKonzeptrelevant;
            try {
                isKonzeptrelevant = (boolean) parsePhasenbezugMethod.invoke(doorsMigrationService, "Konzeptrelevant");
                Assertions.assertTrue(isKonzeptrelevant);

                isKonzeptrelevant = (boolean) parsePhasenbezugMethod.invoke(doorsMigrationService, "Architekturrelevant");
                Assertions.assertFalse(isKonzeptrelevant);

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (NoSuchMethodException | SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void parseSensorCocTest() {
        Class<?>[] sensorCocParam = new Class<?>[]{String.class};

        try {
            Method parseSensorCocMethod = doorsMigrationService.getClass().getDeclaredMethod("parseSensorCoc", sensorCocParam);
            parseSensorCocMethod.setAccessible(true);

            SensorCoc sensorCoc;

            try {
                sensorCoc = (SensorCoc) parseSensorCocMethod.invoke(doorsMigrationService, "TGF_Hybrid");

                SensorCoc sensorCocExpected = new SensorCoc();
                sensorCocExpected.setSensorCocId(9L);
                sensorCocExpected.setOrtung("Technologie Gesamtfahrzeug");
                sensorCocExpected.setRessort("T-Ressort");
                sensorCocExpected.setTechnologie("TGF_Hybrid");
                sensorCocExpected.setZakEinordnung("AK Direkt / AG: Herstellbarkeit SSA");

                Assertions.assertEquals(sensorCocExpected, sensorCoc);

                sensorCoc = (SensorCoc) parseSensorCocMethod.invoke(doorsMigrationService, "notExistingSensorCoc");
                Assertions.assertNull(sensorCoc);

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (NoSuchMethodException | SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void parseStatusTest() {
        Class<?>[] statusParam = new Class<?>[]{String.class};

        try {
            Method parseStatusMethod = doorsMigrationService.getClass().getDeclaredMethod("parseStatus", statusParam);
            parseStatusMethod.setAccessible(true);

            Status status;
            try {
                status = (Status) parseStatusMethod.invoke(doorsMigrationService, "in Arbeit");
                Assertions.assertEquals(Status.A_INARBEIT, status);

                status = (Status) parseStatusMethod.invoke(doorsMigrationService, "abgestimmt");
                Assertions.assertEquals(Status.A_FTABGESTIMMT, status);

                status = (Status) parseStatusMethod.invoke(doorsMigrationService, "undefined");
                Assertions.assertNull(status);

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (NoSuchMethodException | SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void parseModulSeTeamTest() {
        Class<?>[] modulSeTeamParam = new Class<?>[]{String.class};

        try {
            Method parseModulSeTeamMethod = doorsMigrationService.getClass().getDeclaredMethod("parseModulSeTeam", modulSeTeamParam);
            parseModulSeTeamMethod.setAccessible(true);

            Modul modul1 = new Modul("1 Modul");
            modul1.setId(3L);
            modul1.setBeschreibung("1 Modul");
            modul1.setFachBereich("EI");

            ModulSeTeam modulSeTeam7 = new ModulSeTeam();
            modulSeTeam7.setId(10L);
            modulSeTeam7.setName("SeTeam 7");
            modulSeTeam7.setElternModul(modul1);

            ModulSeTeam modulSeTeam8 = new ModulSeTeam();
            modulSeTeam8.setId(11L);
            modulSeTeam8.setName("SeTeam 8");
            modulSeTeam8.setElternModul(modul1);

            ModulSeTeam modulSeTeam9 = new ModulSeTeam();
            modulSeTeam9.setId(12L);
            modulSeTeam9.setName("SeTeam 9");
            modulSeTeam9.setElternModul(modul1);

            ModulSeTeam modulSeTeam10 = new ModulSeTeam();
            modulSeTeam10.setId(13L);
            modulSeTeam10.setName("SeTeam 10");
            modulSeTeam10.setElternModul(modul1);

            try {
                ModulSeTeam modulSeTeam = (ModulSeTeam) parseModulSeTeamMethod.invoke(doorsMigrationService, "1 Modul / SeTeam 7");
                Assertions.assertEquals(modulSeTeam7, modulSeTeam);
                Modul modul = modulSeTeam.getElternModul();
                Assertions.assertEquals(modul1, modul);

                modulSeTeam = (ModulSeTeam) parseModulSeTeamMethod.invoke(doorsMigrationService, "1 Modul / unknownSeTeam");
                Assertions.assertNull(modulSeTeam);

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (NoSuchMethodException | SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void isPhasenbezugValidTest() {
        Class<?>[] phasenbezugParam = new Class<?>[]{String.class};

        try {
            Method isPhasenbezugValidMethod = doorsMigrationService.getClass().getDeclaredMethod("isPhasenbezugValid", phasenbezugParam);
            isPhasenbezugValidMethod.setAccessible(true);

            boolean isPhasenbezugValid;

            try {
                isPhasenbezugValid = (boolean) isPhasenbezugValidMethod.invoke(doorsMigrationService, "Konzeptrelevant");
                Assertions.assertTrue(isPhasenbezugValid);

                isPhasenbezugValid = (boolean) isPhasenbezugValidMethod.invoke(doorsMigrationService, "Architekturrelevant");
                Assertions.assertTrue(isPhasenbezugValid);

                isPhasenbezugValid = (boolean) isPhasenbezugValidMethod.invoke(doorsMigrationService, "other");
                Assertions.assertFalse(isPhasenbezugValid);

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (NoSuchMethodException | SecurityException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void createDoorsAnforderungPositivTest() {

        try {
            Object[] attributeValues = new Object[]{"A30000", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "Konzeptrelevant",
                "TGF_Hybrid", "in Arbeit", "1 Modul / SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            Anforderung createdAnforderung;
            String errMsg;

            try {
                createdAnforderung = (Anforderung) createDoorsAnforderungMethod.invoke(doorsMigrationService, attributeValues);
                Assertions.assertNotNull(createdAnforderung);
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.isEmpty());

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }
    }

    @Test
    public void createDoorsAnforderungNegativTestSensorCoc() {

        try {
            Object[] attributeValues = new Object[]{"A30000", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "Konzeptrelevant",
                "nonExistingSensorCoc", "in Arbeit", "1 Modul / SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            Anforderung createdAnforderung;
            String errMsg;

            try {
                createdAnforderung = (Anforderung) createDoorsAnforderungMethod.invoke(doorsMigrationService, attributeValues);
                Assertions.assertNull(createdAnforderung);
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("Severe: SensorCoc Technologie 'nonExistingSensorCoc' is not registered in the system!;"));

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }
    }

    @Test
    public void createDoorsAnforderungNegativTestStatus() {

        try {
            Object[] attributeValues = new Object[]{"A30000", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "Konzeptrelevant",
                "TGF_Hybrid", "nonExistingStatus", "1 Modul / SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            Anforderung createdAnforderung;
            String errMsg;

            try {
                createdAnforderung = (Anforderung) createDoorsAnforderungMethod.invoke(doorsMigrationService, attributeValues);
                Assertions.assertNull(createdAnforderung);
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("Severe: No Status 'nonExistingStatus' found in the system!;"));

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }
    }

    @Test
    public void createDoorsAnforderungNegativTestUmsetzer() {

        try {
            Object[] attributeValues = new Object[]{"A30000", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "Konzeptrelevant",
                "TGF_Hybrid", "in Arbeit", "1 Modul SeTeam 7", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            Object[] attributeValues2 = new Object[]{"A30000", "F01; F06; F07", "beschreibungDe", "Kommentar", "Loesung", "Konzeptrelevant",
                "TGF_Hybrid", "in Arbeit", "1 Modul / unknownSeTeam", "STD", "siehe Beschreibung", "beschreibungEn",
                "Markus Wolf", "DOORS", "TI_HE_SYS_324"};

            Anforderung createdAnforderung;
            String errMsg;

            try {
                createdAnforderung = (Anforderung) createDoorsAnforderungMethod.invoke(doorsMigrationService, attributeValues);
                Assertions.assertNull(createdAnforderung);
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("!Info: Umsetzer must have format 'Modul / ModulSeTeam';"));
                Assertions.assertTrue(errMsg.contains("Severe: Umsetzer '1 Modul SeTeam 7' does not exist in the system!;"));

                createdAnforderung = (Anforderung) createDoorsAnforderungMethod.invoke(doorsMigrationService, attributeValues2);
                Assertions.assertNull(createdAnforderung);
                errMsg = doorsMigrationService.getErrMsg();
                Assertions.assertTrue(errMsg.contains("Severe: Umsetzer '1 Modul / unknownSeTeam' does not exist in the system!;"));

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error(null, ex);
            }

        } catch (SecurityException ex) {
            LOG.error(null, ex);
        }
    }

    @Test
    public void migrateDoorsAnforderungenPositivTestCreated() {
        Anforderung a30000 = new Anforderung();
        a30000.setId(30000L);
        a30000.setFachId("A30000");
        a30000.setVersion(1);
        a30000.setStatus(Status.A_INARBEIT);
        when(anforderungService.getAnforderungByFachIdVersion("A30000", 1)).thenReturn(null);
        when(anforderungService.persistAnforderung(any(Anforderung.class))).thenReturn(30000L);
        when(anforderungService.getAnforderungById(30000L)).thenReturn(a30000);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("DOORS Migration");
        Assertions.assertNotNull(sheet);
        Workbook book;

        sheet.createRow(0);
        sheet.createRow(1);
        Row anforderungRow = sheet.createRow(2);

        Cell anforderungfachIdCell = anforderungRow.createCell(2);
        Cell festgestelltInCell = anforderungRow.createCell(7);
        Cell beschreibungDeCell = anforderungRow.createCell(8);
        Cell anforderungKommentarCell = anforderungRow.createCell(12);
        Cell loesungsVorschlagCell = anforderungRow.createCell(13);
        Cell phasenbezugCell = anforderungRow.createCell(14);
        Cell sensorCocCell = anforderungRow.createCell(16);
        Cell statusCell = anforderungRow.createCell(18);
        Cell umsetzerCell = anforderungRow.createCell(20);
        Cell vereinbarungstypCell = anforderungRow.createCell(21);
        Cell zielwertCell = anforderungRow.createCell(22);
        Cell beschreibungEnCell = anforderungRow.createCell(24);
        Cell erstellerCell = anforderungRow.createCell(27);
        Cell referenzSystemCell = anforderungRow.createCell(41);
        Cell referenzSystemLinkCell = anforderungRow.createCell(42);

        anforderungfachIdCell.setCellValue("A30000");
        festgestelltInCell.setCellValue("F01; F06; F07");
        beschreibungDeCell.setCellValue("beschreibungDe");
        anforderungKommentarCell.setCellValue("Kommentar");
        loesungsVorschlagCell.setCellValue("Loesung");
        phasenbezugCell.setCellValue("Konzeptrelevant");
        sensorCocCell.setCellValue("TGF_Hybrid");
        statusCell.setCellValue("in Arbeit");
        umsetzerCell.setCellValue("1 Modul / SeTeam 7");
        vereinbarungstypCell.setCellValue("STD");
        zielwertCell.setCellValue("siehe Beschreibung");
        beschreibungEnCell.setCellValue("beschreibungEn");
        erstellerCell.setCellValue("Markus Wolf");
        referenzSystemCell.setCellValue("DOORS");
        referenzSystemLinkCell.setCellValue("TI_HE_SYS_324");

        book = doorsMigrationService.migrateDoorsAnforderungen(sheet);
        Assertions.assertNotNull(book);
        Sheet resultSheet = book.getSheet("Migration_Result");
        Assertions.assertNotNull(resultSheet);
        Row reportRow = resultSheet.getRow(1);
        Assertions.assertNotNull(reportRow);

        Cell rowIndexCell = reportRow.getCell(0);
        Cell fachIdCell = reportRow.getCell(1);
        Cell migrationStatusCell = reportRow.getCell(2);
        Cell kommentarCell = reportRow.getCell(4);

        Assertions.assertEquals(3, rowIndexCell.getNumericCellValue());
        Assertions.assertEquals("A30000", fachIdCell.getStringCellValue());
        Assertions.assertEquals("successful", migrationStatusCell.getStringCellValue());
        Assertions.assertTrue(kommentarCell.getStringCellValue().contains("Anforderung with ID 30000 has been successfully migrated"));

    }

    @Test
    public void migrateDoorsAnforderungenNegativTestFachId() {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("DOORS Migration");
        Assertions.assertNotNull(sheet);
        String errMsg;
        Workbook book;

        sheet.createRow(0);
        sheet.createRow(1);
        Row anforderungRow = sheet.createRow(2);

        Cell anforderungfachIdCell = anforderungRow.createCell(2);

        anforderungfachIdCell.setCellValue("");
        book = doorsMigrationService.migrateDoorsAnforderungen(sheet);
        errMsg = doorsMigrationService.getErrMsg();
        Assertions.assertNotNull(book);
        Assertions.assertTrue(errMsg.contains("Severe: No FachId provided!"));
        Sheet resultSheet = book.getSheet("Migration_Result");
        Assertions.assertNotNull(resultSheet);
        Row reportRow = resultSheet.getRow(1);
        Assertions.assertNotNull(reportRow);

        Cell rowIndexCell = reportRow.getCell(0);
        Cell fachIdCell = reportRow.getCell(1);
        Cell migrationStatusCell = reportRow.getCell(2);
        Cell kommentarCell = reportRow.getCell(4);

        Assertions.assertEquals(3, rowIndexCell.getNumericCellValue());
        Assertions.assertEquals("", fachIdCell.getStringCellValue());
        Assertions.assertEquals("failed", migrationStatusCell.getStringCellValue());
        Assertions.assertTrue(kommentarCell.getStringCellValue().contains("Severe: No FachId provided!"));

    }

    @Test
    public void migrateDoorsAnforderungenSkippedTest() {
        Anforderung a221 = new Anforderung();
        a221.setId(221L);
        a221.setFachId("A221");
        a221.setVersion(1);
        a221.setStatus(Status.A_INARBEIT);
        when(anforderungService.getAnforderungByFachIdVersion("A221", 1)).thenReturn(a221);
//        when(anforderungService.persistAnforderung(any(Anforderung.class))).thenReturn(221L);
//        when(anforderungService.getAnforderungById(221L)).thenReturn(a221);
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("DOORS Migration");
        Workbook book;

        Row anforderungRow = sheet.createRow(2);

        Cell anforderungfachIdCell = anforderungRow.createCell(2);

        anforderungfachIdCell.setCellValue("A221");
        book = doorsMigrationService.migrateDoorsAnforderungen(sheet);
        Assertions.assertNotNull(book);
        Sheet resultSheet = book.getSheet("Migration_Result");
        Assertions.assertNotNull(resultSheet);
        Row reportRow = resultSheet.getRow(1);
        Assertions.assertNotNull(reportRow);

        Cell rowIndexCell = reportRow.getCell(0);
        Cell fachIdCell = reportRow.getCell(1);
        Cell migrationStatusCell = reportRow.getCell(2);
        Cell kommentarCell = reportRow.getCell(4);

        Assertions.assertEquals(3, rowIndexCell.getNumericCellValue());
        Assertions.assertEquals("A221", fachIdCell.getStringCellValue());
        Assertions.assertEquals("skipped", migrationStatusCell.getStringCellValue());
        Assertions.assertTrue(kommentarCell.getStringCellValue().contains("Anforderung A221 has already been migrated"));

    }

}
