package de.interfaceag.bmw.pzbk.berichtswesen;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class BerichtswesenSearchServiceTest {

    @InjectMocks
    private BerichtswesenSearchService berichtswesenSearchService;

    @Test
    public void initAnforderungModulList() {
        Anforderung anforderung = new Anforderung();
        Derivat derivat = new Derivat();
        derivat.setId(1L);
        Modul modul = new Modul();
        Date date = new Date();

        BerichtswesenViewData viewData = berichtswesenSearchService.getViewDateFromFilter(TestDataFactory.generateZakUebertragungen(anforderung, derivat, modul, date));

        assertAll("Groesse",
                () -> {
                    assertEquals(viewData.getBerichtswesenDTO().size(), 1);
                    BerichtswesenDTO result = viewData.getBerichtswesenDTO().get(0);

                    assertAll("Position der Felder",
                            () -> assertEquals(anforderung.getFachId(), result.getFachId()),
                            () -> assertEquals(modul.getName(), result.getModulName()),
                            () -> assertEquals("42", result.getZakId()),
                            () -> assertEquals("ZAK Uebertrager", result.getErsteller()),
                            () -> assertEquals(new SimpleDateFormat("dd-MM-yyyy HH:mm").format(date), result.getAenderungsDatum()),
                            () -> assertEquals("ZAK Kommentar", result.getZakKommentar())
                    );

                    List<Long> result03 = (List<Long>) result.getDerivateIds();
                    assertAll("List",
                            () -> assertEquals(result03.size(), 1),
                            () -> assertTrue(result03.get(0).equals(1L))
                    );

                    HashMap<Long, String> result04 = (HashMap<Long, String>) result.getMapForStatus();
                    assertAll("Map",
                            () -> assertEquals(result04.size(), 1),
                            () -> assertTrue(result04.get(1L).equals(ZakStatus.DONE.getStatusBezeichnung()))
                    );
                });
    }

}
