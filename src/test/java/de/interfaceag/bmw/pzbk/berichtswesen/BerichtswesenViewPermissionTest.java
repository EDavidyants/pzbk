package de.interfaceag.bmw.pzbk.berichtswesen;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.HashSet;
import java.util.Set;

class BerichtswesenViewPermissionTest {

    private BerichtswesenViewPermission viewPermission;

    private Set<Rolle> roles;

    @BeforeEach
    void setup() {
        roles = new HashSet<>();
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getPage(Rolle rolle) {
        setupViewPermission(rolle);
        boolean result = viewPermission.getPage();

        switch (rolle) {
            case ADMIN:
            case T_TEAMLEITER:
            case ANFORDERER:
            case TTEAM_VERTRETER:
                MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.FALSE));
                break;
        }
    }

    private void setupViewPermission(Rolle rolle) {
        roles.add(rolle);
        viewPermission = new BerichtswesenViewPermission(roles);
    }
}