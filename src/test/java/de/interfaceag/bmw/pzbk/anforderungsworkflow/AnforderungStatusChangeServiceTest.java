package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.mail.MailService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.AnforderungReportingStatusTransitionAdapter;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungStatusChangeServiceTest {

    private static final String STATUSCHANGE = "STATUSCHANGE";
    private static final Status FREIGEGEBEN = Status.A_FREIGEGEBEN;
    private static final Status GELOESCHT = Status.A_GELOESCHT;
    private static final Status STATUSMELDUNGDEFAULT = Status.M_ZUGEORDNET;
    private static final String DEFAULTFREIGABECOMMENT = "Wurde freigegeben";

    @Mock
    private Session session;
    @Mock
    private MailService mailService;
    @Mock
    private AnforderungService anforderungService;
    @Mock
    private AnforderungMeldungHistoryService anforderungMeldungHistoryService;
    @Mock
    private AnforderungReportingStatusTransitionAdapter reportingStatusTransitionAdapter;
    @InjectMocks
    private AnforderungStatusChangeService anforderungStatusChangeService;

    private Anforderung anforderung;
    private Meldung meldung;
    @Mock
    private Anforderung anforderungCopy;
    @Mock
    private Meldung meldungCopy;
    @Mock
    private Mitarbeiter mitarbeiter;

    @BeforeEach
    void setup() {
        anforderung = TestDataFactory.generateAnforderung();
        meldung = TestDataFactory.generateMeldung();
        meldung.setStatus(STATUSMELDUNGDEFAULT);
        Set<Meldung> meldungen = new HashSet<>();
        meldungen.add(meldung);
        Set<Anforderung> anforderungen = new HashSet<>();
        anforderungen.add(anforderung);
        anforderung.setMeldungen(meldungen);
        meldung.setAnforderung(anforderungen);
        when(session.getUser()).thenReturn(mitarbeiter);
        when(anforderungService.getAnforderungWorkCopyByIdOrNewAnforderung(any())).thenReturn(anforderungCopy);
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    public void testChangeAnforderungStatus(Status newStatus) {
        Anforderung result = anforderungStatusChangeService.changeAnforderungStatus(anforderung, newStatus.getStatusId(), STATUSCHANGE);
        MatcherAssert.assertThat(result.getStatus(), is(newStatus));
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    public void testChangeAnforderungStatusStatusChangeCommentUpdated(Status newStatus) {
        Anforderung result = anforderungStatusChangeService.changeAnforderungStatus(anforderung, newStatus.getStatusId(), STATUSCHANGE);
        MatcherAssert.assertThat(result.getStatusWechselKommentar(), is(STATUSCHANGE));
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    public void testChangeAnforderungStatusWriteHistoryEntry(Status newStatus) {
        anforderungStatusChangeService.changeAnforderungStatus(anforderung, newStatus.getStatusId(), STATUSCHANGE);
        verify(anforderungMeldungHistoryService, times(1)).writeHistoryForChangedValuesOfAnfoMgmtObject(any(), eq(anforderung), eq(anforderungCopy));
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    public void testChangeAnforderungStatusMailSent(Status newStatus) {
        anforderungStatusChangeService.changeAnforderungStatus(anforderung, newStatus.getStatusId(), STATUSCHANGE);
        verify(mailService, times(1)).sendMailAnforderungChanged(any(), any());
    }

    @Test
    public void testChangeAnforderungStatusToGeloescht() {
        Anforderung result = anforderungStatusChangeService.changeAnforderungStatus(anforderung, GELOESCHT.getStatusId(), STATUSCHANGE);
        MatcherAssert.assertThat(result.getStatus(), is(GELOESCHT));
    }

    @Test
    public void testChangeAnforderungStatusToFreigegeben() {
        Anforderung result = anforderungStatusChangeService.changeAnforderungStatus(anforderung, FREIGEGEBEN.getStatusId(), STATUSCHANGE);
        MatcherAssert.assertThat(result.getStatus(), is(FREIGEGEBEN));
    }

    @Test
    public void testChangeAnforderungStatusToFreigegebenUpdateAnforderungFreigabeMeiModules() {
        anforderungStatusChangeService.changeAnforderungStatus(anforderung, FREIGEGEBEN.getStatusId(), STATUSCHANGE);
        verify(anforderungService, times(1)).anforderungForAllMyModulSeTeamsFreigeben(eq(anforderung), any(), eq(STATUSCHANGE));
    }

    @Test
    public void testChangeAnforderungStatusToFreigegebenUpdateAnforderungFreigabeMeiModulesWithEmptyComment() {
        anforderungStatusChangeService.changeAnforderungStatus(anforderung, FREIGEGEBEN.getStatusId(), "");
        verify(anforderungService, times(1)).anforderungForAllMyModulSeTeamsFreigeben(eq(anforderung), any(), eq(DEFAULTFREIGABECOMMENT));
    }

    @Test
    public void testChangeAnforderungStatusToFreigegebenUpdateAnforderungFreigabeMeiModulesWithNullComment() {
        anforderungStatusChangeService.changeAnforderungStatus(anforderung, FREIGEGEBEN.getStatusId(), null);
        verify(anforderungService, times(1)).anforderungForAllMyModulSeTeamsFreigeben(eq(anforderung), any(), eq(DEFAULTFREIGABECOMMENT));
    }

    @Test
    public void testChangeAnforderungStatusToGeloeschtMeldungStatusChanged() {
        when(anforderungService.getMeldungWorkCopyByIdOrNewMeldung(any(Meldung.class))).thenReturn(meldungCopy);
        anforderungStatusChangeService.changeAnforderungStatus(anforderung, GELOESCHT.getStatusId(), STATUSCHANGE);
        MatcherAssert.assertThat(meldung.getStatus(), is(Status.M_GELOESCHT));
    }

    @Test
    public void testChangeAnforderungStatusToGeloeschtWriteMeldungHistory() {
        when(anforderungService.getMeldungWorkCopyByIdOrNewMeldung(any(Meldung.class))).thenReturn(meldungCopy);
        anforderungStatusChangeService.changeAnforderungStatus(anforderung, GELOESCHT.getStatusId(), STATUSCHANGE);
        verify(anforderungMeldungHistoryService, times(1)).writeHistoryForChangedValuesOfAnfoMgmtObject(any(), any(Meldung.class), any(Meldung.class));
    }

    @Test
    public void testChangeAnforderungStatusToGeloeschtMeldungStatusNotChangedIfNullAnforderungen() {
        meldung.setAnforderung(null);
        anforderungStatusChangeService.changeAnforderungStatus(anforderung, GELOESCHT.getStatusId(), STATUSCHANGE);
        MatcherAssert.assertThat(meldung.getStatus(), is(STATUSMELDUNGDEFAULT));
    }

    @Test
    public void testChangeAnforderungStatusToGeloeschtMeldungStatusNotChangedIfMultipleAnforderungen() {
        Set<Anforderung> anforderungen = new HashSet<>();
        anforderungen.add(anforderung);
        anforderungen.add(anforderungCopy);
        meldung.setAnforderung(anforderungen);
        anforderungStatusChangeService.changeAnforderungStatus(anforderung, GELOESCHT.getStatusId(), STATUSCHANGE);
        MatcherAssert.assertThat(meldung.getStatus(), is(STATUSMELDUNGDEFAULT));
    }

    @Test
    public void testChangeAnforderungStatusToGeloeschtWriteNoMeldungHistory() {
        Set<Anforderung> anforderungen = new HashSet<>();
        anforderungen.add(anforderung);
        anforderungen.add(anforderungCopy);
        meldung.setAnforderung(anforderungen);
        anforderungStatusChangeService.changeAnforderungStatus(anforderung, GELOESCHT.getStatusId(), STATUSCHANGE);
        verify(anforderungMeldungHistoryService, never()).writeHistoryForChangedValuesOfAnfoMgmtObject(any(), any(Meldung.class), any(Meldung.class));
    }

}
