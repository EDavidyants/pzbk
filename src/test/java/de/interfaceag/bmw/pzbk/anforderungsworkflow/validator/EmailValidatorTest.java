package de.interfaceag.bmw.pzbk.anforderungsworkflow.validator;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

/**
 *
 * @author fn
 */
public class EmailValidatorTest {

    @Test
    public void validateCorrectEmail() {
        boolean valid = EmailValidator.validate("test@test.de");
        Assertions.assertTrue(valid);
    }

    @Test
    public void validateWrongEmail() {
        boolean valid = EmailValidator.validate("@test.de");
        Assertions.assertFalse(valid);
    }

    @Test
    public void validateWrongEmail2() {
        boolean valid = EmailValidator.validate("test@.de");
        Assertions.assertFalse(valid);
    }

    @Test
    public void validateWrongEmail3() {
        boolean valid = EmailValidator.validate("test@test");
        Assertions.assertFalse(valid);
    }

    @Test
    public void validateWrongEmail4() {
        boolean valid = EmailValidator.validate("test@@test.de");
        Assertions.assertFalse(valid);
    }

    @Test
    public void validateWrongEmail5() {
        boolean valid = EmailValidator.validate("test@test@test.de");
        Assertions.assertFalse(valid);
    }

}
