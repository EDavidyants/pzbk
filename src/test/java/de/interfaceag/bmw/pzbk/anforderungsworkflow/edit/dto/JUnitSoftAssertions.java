package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto;

/**
 * Like {@link SoftAssertions} but as a junit rule that takes care of calling
 * {@link SoftAssertions#assertAll() assertAll()} at the end of each test.
 * <p>
 * Example:
 * <pre><code class='java'> public class SoftlyTest {
 *
 *     &#064;Rule
 *     public final JUnitBDDSoftAssertions softly = new JUnitBDDSoftAssertions();
 *
 *     &#064;Test
 *     public void soft_bdd_assertions() throws Exception {
 *       softly.assertThat(1).isEqualTo(2);
 *       softly.assertThat(Lists.newArrayList(1, 2)).containsOnly(1, 2);
 *       // no need to call assertAll(), this is done automatically.
 *     }
 *  }</code></pre>
 */
@javax.annotation.Generated(value="assertj-assertions-generator")
public class JUnitSoftAssertions extends org.assertj.core.api.JUnitSoftAssertions {

  /**
   * Creates a new "soft" instance of <code>{@link de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalAuspraegungDtoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalAuspraegungDtoAssert assertThat(de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalAuspraegungDto actual) {
    return proxy(de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalAuspraegungDtoAssert.class, de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalAuspraegungDto.class, actual);
  }

  /**
   * Creates a new "soft" instance of <code>{@link de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDataAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDataAssert assertThat(de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalData actual) {
    return proxy(de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDataAssert.class, de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalData.class, actual);
  }

  /**
   * Creates a new "soft" instance of <code>{@link de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDialogDtoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDialogDtoAssert assertThat(de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDialogDto actual) {
    return proxy(de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDialogDtoAssert.class, de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDialogDto.class, actual);
  }

  /**
   * Creates a new "soft" instance of <code>{@link de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDtoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDtoAssert assertThat(de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDto actual) {
    return proxy(de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDtoAssert.class, de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDto.class, actual);
  }

}
