package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AnforderungViewFacadeTest {

    @Test
    public void idParamterIsPresentEmptyTest() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Optional<String> idParameter = Optional.empty();
        boolean result = idParamterIsPresent(idParameter);
        assertFalse(result);
    }

    @Test
    public void idParamterIsPresentIdStringTest() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Optional<String> idParameter = Optional.of("12");
        boolean result = idParamterIsPresent(idParameter);
        assertTrue(result);
    }

    @Test
    public void idParamterIsPresentWrongIdStringTest() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Optional<String> idParameter = Optional.of("A12");
        boolean result = idParamterIsPresent(idParameter);
        assertFalse(result);
    }

    private boolean idParamterIsPresent(Optional<String> idParameter) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Method method = AnforderungViewFacade.class.getDeclaredMethod("idParameterIsPresent", Optional.class);
        method.setAccessible(true);
        return (Boolean) method.invoke("idParameterIsPresent", idParameter);
    }

    @Test
    public void fachIdAndVersionParamterArePresentEmptyTest() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Optional<String> fachIdParamter = Optional.empty();
        Optional<String> versionParamter = Optional.empty();
        boolean result = fachIdAndVersionParamterArePresent(fachIdParamter, versionParamter);
        assertFalse(result);
    }

    @Test
    public void fachIdAndVersionParamterArePresentTest() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Optional<String> fachIdParamter = Optional.of("A12");
        Optional<String> versionParamter = Optional.of("1");
        boolean result = fachIdAndVersionParamterArePresent(fachIdParamter, versionParamter);
        assertTrue(result);
    }

    @Test
    public void fachIdAndVersionParamterArePresentWrongVersion1Test() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Optional<String> fachIdParamter = Optional.of("A12");
        Optional<String> versionParamter = Optional.of("A1");
        boolean result = fachIdAndVersionParamterArePresent(fachIdParamter, versionParamter);
        assertFalse(result);
    }

    @Test
    public void fachIdAndVersionParamterArePresentWrongVersion2Test() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Optional<String> fachIdParamter = Optional.of("A12");
        Optional<String> versionParamter = Optional.of("");
        boolean result = fachIdAndVersionParamterArePresent(fachIdParamter, versionParamter);
        assertFalse(result);
    }

    @Test
    public void fachIdAndVersionParamterArePresentWrongFachId1Test() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Optional<String> fachIdParamter = Optional.of("A");
        Optional<String> versionParamter = Optional.of("1");
        boolean result = fachIdAndVersionParamterArePresent(fachIdParamter, versionParamter);
        assertFalse(result);
    }

    @Test
    public void fachIdAndVersionParamterArePresentWrongFachId2Test() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Optional<String> fachIdParamter = Optional.of("M12");
        Optional<String> versionParamter = Optional.of("1");
        boolean result = fachIdAndVersionParamterArePresent(fachIdParamter, versionParamter);
        assertFalse(result);
    }

    @Test
    public void fachIdAndVersionParamterArePresentWrongFachId3Test() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Optional<String> fachIdParamter = Optional.of("12");
        Optional<String> versionParamter = Optional.of("1");
        boolean result = fachIdAndVersionParamterArePresent(fachIdParamter, versionParamter);
        assertFalse(result);
    }

    @Test
    public void fachIdAndVersionParamterArePresentWrongFachId4Test() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Optional<String> fachIdParamter = Optional.of("");
        Optional<String> versionParamter = Optional.of("1");
        boolean result = fachIdAndVersionParamterArePresent(fachIdParamter, versionParamter);
        assertFalse(result);
    }

    private boolean fachIdAndVersionParamterArePresent(Optional<String> fachIdAsString, Optional<String> versionAsString) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Method method = AnforderungViewFacade.class.getDeclaredMethod("fachIdAndVersionParameterArePresent", Optional.class, Optional.class);
        method.setAccessible(true);
        return (Boolean) method.invoke("fachIdAndVersionParameterArePresent", fachIdAsString, versionAsString);
    }

    @Test
    public void anforderungHasMeldungEmptyTest() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Optional<Anforderung> anforderung = Optional.empty();
        boolean result = anforderungHasMeldung(anforderung);
        assertFalse(result);
    }

    @Test
    public void anforderungHasMeldungNoMeldungTest() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        anforderung.getMeldungen().clear();
        Optional<Anforderung> optionalAnforderung = Optional.of(anforderung);
        boolean result = anforderungHasMeldung(optionalAnforderung);
        assertFalse(result);
    }

    @Test
    public void anforderungHasMeldungTest() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Optional<Anforderung> anforderung = Optional.of(TestDataFactory.generateAnforderung());
        boolean result = anforderungHasMeldung(anforderung);
        assertTrue(result);
    }

    private boolean anforderungHasMeldung(Optional<Anforderung> anforderung) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Method method = AnforderungViewFacade.class.getDeclaredMethod("anforderungHasMeldung", Optional.class);
        method.setAccessible(true);
        return (Boolean) method.invoke("anforderungHasMeldung", anforderung);
    }

}
