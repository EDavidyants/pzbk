package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class AnforderungEditFahrzeugmerkmalDialogDtoTest {

    private AnforderungEditFahrzeugmerkmalDialogDto anforderungEditFahrzeugmerkmalDialogDto;

    @Mock
    private AnforderungEditFahrzeugmerkmal anforderungEditFahrzeugmerkmal;
    private List<AnforderungEditFahrzeugmerkmal> fahrzeugmerkmale;
    @Mock
    private ModulSeTeamId modulSeTeamId;

    @BeforeEach
    void setUp() {
        fahrzeugmerkmale = Collections.singletonList(anforderungEditFahrzeugmerkmal);
        anforderungEditFahrzeugmerkmalDialogDto = new AnforderungEditFahrzeugmerkmalDialogDto(modulSeTeamId, fahrzeugmerkmale);
    }

    @Test
    void testToString() {
        final String result = anforderungEditFahrzeugmerkmalDialogDto.toString();
        MatcherAssert.assertThat(result, CoreMatchers.is("AnforderungEditFahrzeugmerkmalDialogDto[modulSeTeamId=modulSeTeamId, fahrzeugmerkmale=[anforderungEditFahrzeugmerkmal]]"));
    }

    @Test
    void testGetters() {
        AnforderungEditFahrzeugmerkmalDialogDtoAssert.assertThat(anforderungEditFahrzeugmerkmalDialogDto)
                .hasFahrzeugmerkmale(fahrzeugmerkmale)
                .hasModulSeTeamId(modulSeTeamId);
    }

    @Test
    void setFahrzeugmerkmale() {
        anforderungEditFahrzeugmerkmalDialogDto.setFahrzeugmerkmale(null);
        MatcherAssert.assertThat(anforderungEditFahrzeugmerkmalDialogDto.getFahrzeugmerkmale(), IsNull.nullValue());
    }
}