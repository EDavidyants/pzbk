package de.interfaceag.bmw.pzbk.anforderungsworkflow.permission;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author evda
 */
public class AnforderungEditViewPermissionForUmsetzerTest {

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testUmsetzerPanelPermissionForInArbeitStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForUmsetzerPanel(userRollen, Status.A_INARBEIT);
        switch (role) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testUmsetzerPanelPermissionForUnstimmigStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForUmsetzerPanel(userRollen, Status.A_UNSTIMMIG);
        switch (role) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testUmsetzerPanelPermissionForAbgestimmtStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForUmsetzerPanel(userRollen, Status.A_FTABGESTIMMT);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testUmsetzerPanelPermissionForPlausibilisiertStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForUmsetzerPanel(userRollen, Status.A_PLAUSIB);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testUmsetzerPanelPermissionForFreigegebenStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForUmsetzerPanel(userRollen, Status.A_FREIGEGEBEN);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testUmsetzerPanelPermissionForKeineWeiterverfolgungStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForUmsetzerPanel(userRollen, Status.A_KEINE_WEITERVERFOLG);
        switch (role) {
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testUmsetzerPanelPermissionForGeloeschtStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForUmsetzerPanel(userRollen, Status.A_GELOESCHT);
        switch (role) {
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testUmsetzerPanelPermissionForAngelegtInProzessbaukastenStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForUmsetzerPanel(userRollen, Status.A_ANGELEGT_IN_PROZESSBAUKASTEN);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testUmsetzerPanelPermissionForGenemigtInProzessbaukastenStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForUmsetzerPanel(userRollen, Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
        switch (role) {
            case ADMIN:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

}
