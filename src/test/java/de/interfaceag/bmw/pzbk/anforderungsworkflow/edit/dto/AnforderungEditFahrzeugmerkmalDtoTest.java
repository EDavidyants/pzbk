package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.faces.convert.Converter;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AnforderungEditFahrzeugmerkmalDtoTest {

    private AnforderungEditFahrzeugmerkmalDto anforderungEditFahrzeugmerkmalDto;

    @Mock
    private FahrzeugmerkmalId fahrzeugmerkmalId;
    private String merkmal = "Merkmal";
    @Mock
    private AnforderungEditFahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung;
    private List<AnforderungEditFahrzeugmerkmalAuspraegung> allFahrzeugmerkmalAuspraegungen;
    private List<AnforderungEditFahrzeugmerkmalAuspraegung> selectedFahrzeugmerkmalAuspraegungen;

    @BeforeEach
    void setUp() {
        allFahrzeugmerkmalAuspraegungen = Collections.singletonList(fahrzeugmerkmalAuspraegung);
        selectedFahrzeugmerkmalAuspraegungen = Collections.emptyList();
        anforderungEditFahrzeugmerkmalDto = new AnforderungEditFahrzeugmerkmalDto(fahrzeugmerkmalId, merkmal, allFahrzeugmerkmalAuspraegungen, selectedFahrzeugmerkmalAuspraegungen);
    }

    @Test
    void toString1() {
        final String result = anforderungEditFahrzeugmerkmalDto.toString();
        MatcherAssert.assertThat(result, CoreMatchers.is("AnforderungEditFahrzeugmerkmalDto[fahrzeugmerkmalId=fahrzeugmerkmalId, merkmal='Merkmal', allFahrzeugmerkmalAuspraegungen=[fahrzeugmerkmalAuspraegung], selectedFahrzeugmerkmalAuspraegungen=[]]"));
    }

    @Test
    void testGettersAfterConstructor() {
        AnforderungEditFahrzeugmerkmalDtoAssert.assertThat(anforderungEditFahrzeugmerkmalDto)
                .hasAllAuspraegungen(allFahrzeugmerkmalAuspraegungen)
                .hasSelectedAuspraegungen(selectedFahrzeugmerkmalAuspraegungen)
                .hasMerkmal(merkmal)
                .hasFahrzeugmerkmalId(fahrzeugmerkmalId);
    }

    @Test
    void setSelectedAuspraegungen() {
        anforderungEditFahrzeugmerkmalDto.setSelectedAuspraegungen(allFahrzeugmerkmalAuspraegungen);
        AnforderungEditFahrzeugmerkmalDtoAssert.assertThat(anforderungEditFahrzeugmerkmalDto)
                .hasSelectedAuspraegungen(allFahrzeugmerkmalAuspraegungen);
    }

    @Test
    void getAuspraegungConverterToString() {
        when(fahrzeugmerkmalAuspraegung.getId()).thenReturn(42L);
        final Converter auspraegungConverter = anforderungEditFahrzeugmerkmalDto.getAuspraegungConverter();
        final String result = auspraegungConverter.getAsString(null, null, fahrzeugmerkmalAuspraegung);
        MatcherAssert.assertThat(result, CoreMatchers.is("42"));
    }

    @Test
    void getAuspraegungConverterToSObject() {
        when(fahrzeugmerkmalAuspraegung.getId()).thenReturn(42L);
        final Converter auspraegungConverter = anforderungEditFahrzeugmerkmalDto.getAuspraegungConverter();
        final Object result = auspraegungConverter.getAsObject(null, null, "42");
        MatcherAssert.assertThat(result, CoreMatchers.is(fahrzeugmerkmalAuspraegung));
    }
}