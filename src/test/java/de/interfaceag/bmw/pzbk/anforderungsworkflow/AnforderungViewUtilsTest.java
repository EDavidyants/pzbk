package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AnforderungViewUtilsTest {

    @Test
    void hasRightToSetTteamNullStatus() {
        List<Rolle> userRoles = new ArrayList<>();
        userRoles.add(Rolle.SENSORCOCLEITER);
        Assertions.assertFalse(AnforderungViewUtils.hasRightToSetTteam(null, userRoles));
    }

    @Test
    void hasRightToSetTteamNullRole() {
        Assertions.assertFalse(AnforderungViewUtils.hasRightToSetTteam(Status.A_FTABGESTIMMT, null));
    }

    @Test
    void hasRightToSetTteamEmptyRole() {
        List<Rolle> userRoles = new ArrayList<>();
        Assertions.assertFalse(AnforderungViewUtils.hasRightToSetTteam(Status.A_FTABGESTIMMT, userRoles));
    }

    @ParameterizedTest(name = "Status {index}: {0}")
    @EnumSource(Status.class)
    void statusToSetTteam(Status status) {
        List<Rolle> userRoles = new ArrayList<>();
        userRoles.add(Rolle.SENSORCOCLEITER);

        if (AnforderungViewUtils.getEditTteamStatus().anyMatch(s -> s.equals(status))) {
            Assertions.assertTrue(AnforderungViewUtils.hasRightToSetTteam(status, userRoles));
        } else {
            Assertions.assertFalse(AnforderungViewUtils.hasRightToSetTteam(status, userRoles));
        }
    }

    @ParameterizedTest(name = "Rolle {index}: {0}")
    @EnumSource(Rolle.class)
    void statusToSetTteam(Rolle role) {
        List<Rolle> userRoles = new ArrayList<>();
        userRoles.add(role);
        if (role.equals(Rolle.SENSORCOCLEITER) || role.equals(Rolle.SCL_VERTRETER) || userRoles.contains(Rolle.ADMIN)
                || userRoles.contains(Rolle.T_TEAMLEITER) || userRoles.contains(Rolle.TTEAM_VERTRETER)) {
            Assertions.assertTrue(AnforderungViewUtils.hasRightToSetTteam(Status.A_FTABGESTIMMT, userRoles));
        } else {
            Assertions.assertFalse(AnforderungViewUtils.hasRightToSetTteam(Status.A_FTABGESTIMMT, userRoles));
        }
    }

    @Test
    void getEditTteamStatusSize() {
        Stream<Status> editTteamStatus = AnforderungViewUtils.getEditTteamStatus();
        Assertions.assertEquals(4, editTteamStatus.count());
    }

    @Test
    void getEditTteamRolesSize() {
        Stream<Status> editTteamStatus = AnforderungViewUtils.getEditTteamStatus();
        Assertions.assertEquals(4, editTteamStatus.count());
    }

}
