package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.dao.AnforderungHistoryDao;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.shared.utils.DateUtils;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AnforderungMeldungHistoryServiceTest {


    @Mock
    private AnforderungHistoryDao anforderungHistoryDao;


    @InjectMocks
    private AnforderungMeldungHistoryService anforderungMeldungHistoryService;

    @Test
    void getLastStatusByObjectnameAnforderungId() {


        AnforderungHistory anforderungHistory = TestDataFactory.generateAnforderungHistory(1L, 42L,
                "abgestimmt", "in Arbeit",
                "Apfelkuchen", new Date(), "A", "Status");

        when(anforderungHistoryDao.getAnforderungHistoryByObjectnameAnforderungId(anyLong(), any(), any())).thenReturn(Arrays.asList(anforderungHistory));

        Status status = anforderungMeldungHistoryService.getLastStatusByObjectnameAnforderungId(42L, "A");

        Assert.assertEquals(Status.A_INARBEIT, status);

    }

    @Test
    void getLastStatusByObjectnameAnforderungIdForAdditionalAnforderungHistoryEntries() {


        AnforderungHistory anforderungHistory = TestDataFactory.generateAnforderungHistory(1L, 42L,
                "abgestimmt", "in Arbeit",
                "Apfelkuchen", DateUtils.addDays(new Date(), -2), "A", "Status");
        AnforderungHistory anforderungHistory2 = TestDataFactory.generateAnforderungHistory(1L, 42L,
                "unstimmig", "abgestimmt",
                "Apfelkuchen", DateUtils.addDays(new Date(), -1), "A", "Status");
        AnforderungHistory anforderungHistory3 = TestDataFactory.generateAnforderungHistory(1L, 42L,
                "geloescht", "unstimmig",
                "Apfelkuchen", DateUtils.addDays(new Date(), 0), "A", "Status");

        when(anforderungHistoryDao.getAnforderungHistoryByObjectnameAnforderungId(anyLong(), any(), any())).thenReturn(Arrays.asList(anforderungHistory, anforderungHistory2, anforderungHistory3));

        Status status = anforderungMeldungHistoryService.getLastStatusByObjectnameAnforderungId(42L, "A");

        Assert.assertEquals(Status.A_UNSTIMMIG, status);

    }
}