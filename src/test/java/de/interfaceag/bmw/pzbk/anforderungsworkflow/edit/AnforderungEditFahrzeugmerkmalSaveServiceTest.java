package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalDialogData;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalData;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungModulSeTeamFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AnforderungEditFahrzeugmerkmalSaveServiceTest {

    @Mock
    private ModulService modulService;
    @Mock
    private FahrzeugmerkmalService fahrzeugmerkmalService;
    @Mock
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService anforderungModulSeTeamFahrzeugmerkmalAuspraegungService;
    @InjectMocks
    private AnforderungEditFahrzeugmerkmalSaveService anforderungEditFahrzeugmerkmalSaveService;

    @Mock
    private Anforderung anforderung;
    @Mock
    private AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData;

    private AnforderungId anforderungId = new AnforderungId(7L);
    private ModulSeTeamId modulSeTeamId = new ModulSeTeamId(42L);
    private FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId = new FahrzeugmerkmalAuspraegungId(75L);

    @Mock
    private AnforderungEditFahrzeugmerkmalDialogData anforderungEditFahrzeugmerkmalDialogData;
    private Map<ModulSeTeamId, AnforderungEditFahrzeugmerkmalDialogData> dialogDataMap;

    @Mock
    private AnforderungEditFahrzeugmerkmal anforderungEditFahrzeugmerkmal;
    private List<AnforderungEditFahrzeugmerkmal> fahrzeugmerkmale;

    @Mock
    private FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung;
    private List<FahrzeugmerkmalAuspraegung> existingAuspraegungen;

    @Mock
    private AnforderungEditFahrzeugmerkmalAuspraegung anforderungEditFahrzeugmerkmalAuspraegung;
    private List<AnforderungEditFahrzeugmerkmalAuspraegung> selectedAuspraegungen;

    @Mock
    private ModulSeTeam modulSeTeam;
    @Mock
    private FahrzeugmerkmalAuspraegung auspraegungById;

    @Mock
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegung entryForAnforderungModulSeTeamFahrzeugmerkmalAuspraegung;

    @BeforeEach
    void setUp() {
        dialogDataMap = Collections.singletonMap(modulSeTeamId, anforderungEditFahrzeugmerkmalDialogData);
        fahrzeugmerkmale = Collections.singletonList(anforderungEditFahrzeugmerkmal);
        selectedAuspraegungen = Collections.singletonList(anforderungEditFahrzeugmerkmalAuspraegung);
        when(anforderung.getAnforderungId()).thenReturn(anforderungId);
        when(anforderungEditFahrzeugmerkmalData.getModulSeTeamFahrzeugmerkmalDialogDataMap()).thenReturn(dialogDataMap);
        when(anforderungEditFahrzeugmerkmalDialogData.getFahrzeugmerkmale()).thenReturn(fahrzeugmerkmale);
        when(anforderungEditFahrzeugmerkmalDialogData.getModulSeTeamId()).thenReturn(modulSeTeamId);
        when(anforderungEditFahrzeugmerkmal.getSelectedAuspraegungen()).thenReturn(selectedAuspraegungen);
    }

    @Test
    void saveAnforderungFahrzeugmerkmalChangesAddNew() {
        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(anforderungId, modulSeTeamId))
                .thenReturn(Collections.emptyList());
        when(anforderungEditFahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegungId()).thenReturn(fahrzeugmerkmalAuspraegungId);
        when(modulService.getSeTeamById(any())).thenReturn(modulSeTeam);
        when(fahrzeugmerkmalService.findFahrzeugmerkmalAuspraegung(any())).thenReturn(Optional.of(auspraegungById));

        anforderungEditFahrzeugmerkmalSaveService.saveAnforderungFahrzeugmerkmalChanges(anforderung, anforderungEditFahrzeugmerkmalData);
        AnforderungModulSeTeamFahrzeugmerkmalAuspraegung newEntry = new AnforderungModulSeTeamFahrzeugmerkmalAuspraegung(anforderung, modulSeTeam, auspraegungById);
        verify(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService, times(1)).save(newEntry, false);
    }

    @Test
    void saveAnforderungFahrzeugmerkmalChangesAddNewModulSeTeamNotFound() {
        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(anforderungId, modulSeTeamId))
                .thenReturn(Collections.emptyList());
        when(anforderungEditFahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegungId()).thenReturn(fahrzeugmerkmalAuspraegungId);
        when(modulService.getSeTeamById(any())).thenReturn(null);
        when(fahrzeugmerkmalService.findFahrzeugmerkmalAuspraegung(any())).thenReturn(Optional.of(auspraegungById));

        anforderungEditFahrzeugmerkmalSaveService.saveAnforderungFahrzeugmerkmalChanges(anforderung, anforderungEditFahrzeugmerkmalData);
        verify(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService, never()).save(any(), eq(false));
    }

    @Test
    void saveAnforderungFahrzeugmerkmalChangesAddNewFahrzeugmerkmalAuspraegungByIdNotFound() {
        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(anforderungId, modulSeTeamId))
                .thenReturn(Collections.emptyList());
        when(anforderungEditFahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegungId()).thenReturn(fahrzeugmerkmalAuspraegungId);
        when(modulService.getSeTeamById(any())).thenReturn(modulSeTeam);
        when(fahrzeugmerkmalService.findFahrzeugmerkmalAuspraegung(any())).thenReturn(Optional.empty());

        anforderungEditFahrzeugmerkmalSaveService.saveAnforderungFahrzeugmerkmalChanges(anforderung, anforderungEditFahrzeugmerkmalData);
        verify(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService, never()).save(any(), eq(false));
    }

    @Test
    void saveAnforderungFahrzeugmerkmalChangesNoChangesInAddCase() {
        when(fahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegungId()).thenReturn(fahrzeugmerkmalAuspraegungId);
        existingAuspraegungen = Collections.singletonList(fahrzeugmerkmalAuspraegung);
        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(anforderungId, modulSeTeamId))
                .thenReturn(existingAuspraegungen);
        when(anforderungEditFahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegungId()).thenReturn(fahrzeugmerkmalAuspraegungId);

        anforderungEditFahrzeugmerkmalSaveService.saveAnforderungFahrzeugmerkmalChanges(anforderung, anforderungEditFahrzeugmerkmalData);
        verify(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService, never()).save(any(), eq(false));
    }

    @Test
    void saveAnforderungFahrzeugmerkmalChangesRemoveExisting() {
        when(fahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegungId()).thenReturn(fahrzeugmerkmalAuspraegungId);
        existingAuspraegungen = Collections.singletonList(fahrzeugmerkmalAuspraegung);
        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(anforderungId, modulSeTeamId))
                .thenReturn(existingAuspraegungen);
        selectedAuspraegungen = Collections.emptyList();

        when(anforderungEditFahrzeugmerkmal.getSelectedAuspraegungen()).thenReturn(Collections.emptyList());


        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungModulSeTeamAndFahrzeugmerkmalAuspraegung(anforderungId, modulSeTeamId, fahrzeugmerkmalAuspraegungId))
        .thenReturn(Optional.of(entryForAnforderungModulSeTeamFahrzeugmerkmalAuspraegung));


        anforderungEditFahrzeugmerkmalSaveService.saveAnforderungFahrzeugmerkmalChanges(anforderung, anforderungEditFahrzeugmerkmalData);
        verify(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService, times(1)).remove(entryForAnforderungModulSeTeamFahrzeugmerkmalAuspraegung);
    }

    @Test
    void saveAnforderungFahrzeugmerkmalChangesRemoveExistingEntryNotFound() {
        when(fahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegungId()).thenReturn(fahrzeugmerkmalAuspraegungId);
        existingAuspraegungen = Collections.singletonList(fahrzeugmerkmalAuspraegung);
        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(anforderungId, modulSeTeamId))
                .thenReturn(existingAuspraegungen);
        selectedAuspraegungen = Collections.emptyList();

        when(anforderungEditFahrzeugmerkmal.getSelectedAuspraegungen()).thenReturn(Collections.emptyList());


        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungModulSeTeamAndFahrzeugmerkmalAuspraegung(anforderungId, modulSeTeamId, fahrzeugmerkmalAuspraegungId))
                .thenReturn(Optional.empty());

        anforderungEditFahrzeugmerkmalSaveService.saveAnforderungFahrzeugmerkmalChanges(anforderung, anforderungEditFahrzeugmerkmalData);
        verify(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService, never()).remove(any());
    }
}