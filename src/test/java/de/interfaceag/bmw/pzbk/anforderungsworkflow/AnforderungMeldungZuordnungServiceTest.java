package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.meldung.MeldungReportingStatusTransitionAdapter;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungMeldungZuordnungServiceTest {

    @Mock
    private AnforderungService anforderungService;
    @Mock
    private AnforderungMeldungHistoryService historyService;
    @Mock
    private MeldungReportingStatusTransitionAdapter meldungReportingStatusTransitionAdapter;

    @Mock
    private Meldung meldung;
    @Mock
    private Anforderung anforderung;
    @Mock
    private Anforderung anforderungOther;
    @Mock
    private Mitarbeiter user;

    @InjectMocks
    private AnforderungMeldungZuordnungService anforderungMeldungZuordnungService;

    @Test
    void testAddMeldungToExistingAnforderungForNichtZugeordneteMeldung() {
        when(meldung.getStatus()).thenReturn(Status.M_GEMELDET);
        when(meldung.getStatusWechselKommentar()).thenReturn("gemeldet");
        doNothing().when(historyService).writeHistoryForChangedKeyValue(any(), any(), any(), any(), any());
        when(anforderungService.saveMeldung(meldung)).thenReturn(1L);

        Set<Anforderung> anforderungen = new HashSet<>();
        when(meldung.getAnforderung()).thenReturn(anforderungen);
        List<Meldung> meldungen = Arrays.asList(meldung);
        anforderungMeldungZuordnungService.addMeldungenToAnforderung(anforderung, meldungen, user);
        verify(meldungReportingStatusTransitionAdapter).addMeldungToExistingAnforderung(meldung);
    }

    @Test
    void testAddMeldungToExistingAnforderungForZugeordneteMeldung() {
        when(meldung.getStatus()).thenReturn(Status.M_GEMELDET);
        when(meldung.getStatusWechselKommentar()).thenReturn("gemeldet");
        doNothing().when(historyService).writeHistoryForChangedKeyValue(any(), any(), any(), any(), any());
        when(anforderungService.saveMeldung(meldung)).thenReturn(1L);

        Set<Anforderung> anforderungen = new HashSet<>();
        anforderungen.add(anforderung);
        when(meldung.getAnforderung()).thenReturn(anforderungen);
        List<Meldung> meldungen = Arrays.asList(meldung);
        anforderungMeldungZuordnungService.addMeldungenToAnforderung(anforderung, meldungen, user);
        verify(meldungReportingStatusTransitionAdapter, never()).addMeldungToExistingAnforderung(meldung);
    }

    @Test
    void testRemoveMeldungFromAnforderungForSingleZuordnung() {
        Set<Anforderung> anforderungen = new HashSet<>();
        Set<Meldung> meldungen = new HashSet<>();
        meldungen.add(meldung);
        anforderungen.add(anforderung);

        when(meldung.getStatus()).thenReturn(Status.M_ZUGEORDNET);
        when(meldung.getStatusWechselKommentar()).thenReturn("zugeordnet");
        when(meldung.getAnforderung()).thenReturn(anforderungen);
        when(anforderung.getMeldungen()).thenReturn(meldungen);

        anforderungMeldungZuordnungService.removeMeldungenFromAnforderung(anforderung, meldungen, user);
        verify(meldungReportingStatusTransitionAdapter).removeMeldungFromAnforderung(meldung, anforderung);
    }

    @Test
    void testRemoveMeldungFromAnforderungForMultipleZuordnung() {
        Set<Anforderung> anforderungen = new HashSet<>();
        Set<Meldung> meldungen = new HashSet<>();
        meldungen.add(meldung);
        anforderungen.add(anforderung);
        anforderungen.add(anforderungOther);

        when(meldung.getAnforderung()).thenReturn(anforderungen);
        when(anforderung.getMeldungen()).thenReturn(meldungen);

        anforderungMeldungZuordnungService.removeMeldungenFromAnforderung(anforderung, meldungen, user);
        verify(meldungReportingStatusTransitionAdapter, never()).removeMeldungFromAnforderung(meldung, anforderung);
    }
}
