package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalDialogData;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalAuspraegungDto;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDto;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AnforderungEditFahrzeugmerkmalDialogServiceTest {

    private static final String AUSPRAEGUNG = "Auspraegung";
    private static final String MERKMAL = "Merkmal";

    @Mock
    private FahrzeugmerkmalService fahrzeugmerkmalService;
    @Mock
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService anforderungModulSeTeamFahrzeugmerkmalAuspraegungService;
    @InjectMocks
    private AnforderungEditFahrzeugmerkmalDialogService anforderungEditFahrzeugmerkmalDialogService;

    private AnforderungId anforderungId = new AnforderungId(2L);
    private ModulSeTeamId modulSeTeamId = new ModulSeTeamId(4L);
    private FahrzeugmerkmalId fahrzeugmerkmalId = new FahrzeugmerkmalId(8L);
    private FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId = new FahrzeugmerkmalAuspraegungId(16L);

    private AnforderungEditFahrzeugmerkmal anforderungEditFahrzeugmerkmal;
    private List<AnforderungEditFahrzeugmerkmal> fahrzeugmerkmale;

    @Mock
    private Fahrzeugmerkmal fahrzeugmerkmal;
    private List<Fahrzeugmerkmal> allFahrzeugmerkmaleForModulSeTeam;
    @Mock
    private FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung;
    private List<FahrzeugmerkmalAuspraegung> selectedAuspraegungenForModulSeTeamAndAnforderung;
    private List<FahrzeugmerkmalAuspraegung> allAuspraegungenForFahrzeugmerkmal;

    @BeforeEach
    void setUp() {
        allFahrzeugmerkmaleForModulSeTeam = Collections.singletonList(fahrzeugmerkmal);
        selectedAuspraegungenForModulSeTeamAndAnforderung = Collections.singletonList(fahrzeugmerkmalAuspraegung);
        allAuspraegungenForFahrzeugmerkmal = Collections.singletonList(fahrzeugmerkmalAuspraegung);
        fahrzeugmerkmale = Collections.singletonList(anforderungEditFahrzeugmerkmal);
        when(fahrzeugmerkmal.getFahrzeugmerkmalId()).thenReturn(fahrzeugmerkmalId);
        when(fahrzeugmerkmal.getMerkmal()).thenReturn(MERKMAL);
        when(fahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegungId()).thenReturn(fahrzeugmerkmalAuspraegungId);
        when(fahrzeugmerkmalAuspraegung.getAuspraegung()).thenReturn(AUSPRAEGUNG);
        when(fahrzeugmerkmalAuspraegung.getFahrzeugmerkmal()).thenReturn(fahrzeugmerkmal);
        when(fahrzeugmerkmalService.getFahrzeugmerkmaleForModulSeTeam(modulSeTeamId)).thenReturn(allFahrzeugmerkmaleForModulSeTeam);
        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(anforderungId, modulSeTeamId))
                .thenReturn(selectedAuspraegungenForModulSeTeamAndAnforderung);
        when(fahrzeugmerkmalService.getAllAuspraegungenForFahrzeugmerkmal(fahrzeugmerkmalId))
                .thenReturn(allAuspraegungenForFahrzeugmerkmal);

    }

    @Test
    void getDialogDataModulSeTeamValue() {
        final AnforderungEditFahrzeugmerkmalDialogData result = anforderungEditFahrzeugmerkmalDialogService.getDialogData(anforderungId, modulSeTeamId);
        final ModulSeTeamId resultModulSeTeamId = result.getModulSeTeamId();
        MatcherAssert.assertThat(resultModulSeTeamId, CoreMatchers.is(modulSeTeamId));
    }

    @Test
    void getDialogDataFahrzeugmerkmalSize() {
        final AnforderungEditFahrzeugmerkmalDialogData result = anforderungEditFahrzeugmerkmalDialogService.getDialogData(anforderungId, modulSeTeamId);
        final List<AnforderungEditFahrzeugmerkmal> resultFahrzeugmerkmale = result.getFahrzeugmerkmale();
        MatcherAssert.assertThat(resultFahrzeugmerkmale, IsCollectionWithSize.hasSize(1));
    }

    @Test
    void getDialogDataFahrzeugmerkmalValue() {
        final AnforderungEditFahrzeugmerkmalDialogData result = anforderungEditFahrzeugmerkmalDialogService.getDialogData(anforderungId, modulSeTeamId);
        final List<AnforderungEditFahrzeugmerkmal> resultFahrzeugmerkmale = result.getFahrzeugmerkmale();

        AnforderungEditFahrzeugmerkmalAuspraegung anforderungEditFahrzeugmerkmalAuspraegung = new AnforderungEditFahrzeugmerkmalAuspraegungDto(fahrzeugmerkmalAuspraegungId, AUSPRAEGUNG);

        List<AnforderungEditFahrzeugmerkmalAuspraegung> allFahrzeugmerkmalAuspraegungen = Collections.singletonList(anforderungEditFahrzeugmerkmalAuspraegung);
        List<AnforderungEditFahrzeugmerkmalAuspraegung> selectedFahrzeugmerkmalAuspraegungen = Collections.singletonList(anforderungEditFahrzeugmerkmalAuspraegung);
        anforderungEditFahrzeugmerkmal = new AnforderungEditFahrzeugmerkmalDto(fahrzeugmerkmalId, MERKMAL, allFahrzeugmerkmalAuspraegungen, selectedFahrzeugmerkmalAuspraegungen);
        MatcherAssert.assertThat(resultFahrzeugmerkmale, IsIterableContaining.hasItem(anforderungEditFahrzeugmerkmal));
    }
}