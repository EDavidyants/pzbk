package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalData;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

class AnforderungEditFahrzeugmerkmalDataFactoryTest {

    private AnforderungEditFahrzeugmerkmalDataFactory factory = new AnforderungEditFahrzeugmerkmalDataFactory();

    @Test
    void getNewAnforderungEditFahrzeugmerkmalData() {
        final AnforderungEditFahrzeugmerkmalData result = factory.getNewAnforderungEditFahrzeugmerkmalData();
        MatcherAssert.assertThat(result, CoreMatchers.is(new AnforderungEditFahrzeugmerkmalData()));
    }
}