package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto;

/**
 * Entry point for BDD assertions of different data types.
 */
@javax.annotation.Generated(value="assertj-assertions-generator")
public class BddAssertions {

  /**
   * Creates a new instance of <code>{@link de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalAuspraegungDtoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalAuspraegungDtoAssert then(de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalAuspraegungDto actual) {
    return new de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalAuspraegungDtoAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDataAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDataAssert then(de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalData actual) {
    return new de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDataAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDialogDtoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDialogDtoAssert then(de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDialogDto actual) {
    return new de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDialogDtoAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDtoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDtoAssert then(de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDto actual) {
    return new de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto.AnforderungEditFahrzeugmerkmalDtoAssert(actual);
  }

  /**
   * Creates a new <code>{@link BddAssertions}</code>.
   */
  protected BddAssertions() {
    // empty
  }
}
