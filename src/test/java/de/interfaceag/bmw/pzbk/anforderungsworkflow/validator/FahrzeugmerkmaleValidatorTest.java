package de.interfaceag.bmw.pzbk.anforderungsworkflow.validator;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungFreigabeDto;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.util.Arrays;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmaleValidatorTest {

    @Mock
    private FacesContext facesContext;

    @Mock
    private UIComponent uiComponent;

    @Test
    void validateFahrzeugmerkmale_invalid() {

        List<AnforderungFreigabeDto> anforderungFreigabeDto = Arrays.asList(
                new AnforderungFreigabeDto(1L, "", "", VereinbarungType.STANDARD),
                new AnforderungFreigabeDto(2L, "", "", VereinbarungType.STANDARD));

        anforderungFreigabeDto.get(0).setFahrzeugmerkmalConfigured(false);
        anforderungFreigabeDto.get(1).setFahrzeugmerkmalConfigured(true);

        Exception exception = null;
        try {
            FahrzeugmerkmaleValidator.validateFahrzeugmerkmale(facesContext, uiComponent, null, anforderungFreigabeDto);
        } catch (ValidatorException ex) {
            exception = ex;
        }
        Assert.assertNotNull(exception);
    }

    @Test
    void validateFahrzeugmerkmale_valid() {

        List<AnforderungFreigabeDto> anforderungFreigabeDto = Arrays.asList(
                new AnforderungFreigabeDto(1L, "", "", VereinbarungType.STANDARD),
                new AnforderungFreigabeDto(2L, "", "", VereinbarungType.STANDARD));

        anforderungFreigabeDto.get(0).setFahrzeugmerkmalConfigured(true);
        anforderungFreigabeDto.get(1).setFahrzeugmerkmalConfigured(true);

        Exception exception = null;
        try {
            FahrzeugmerkmaleValidator.validateFahrzeugmerkmale(facesContext, uiComponent, null, anforderungFreigabeDto);
        } catch (ValidatorException ex) {
            exception = ex;
        }
        Assert.assertNull(exception);
    }
}
