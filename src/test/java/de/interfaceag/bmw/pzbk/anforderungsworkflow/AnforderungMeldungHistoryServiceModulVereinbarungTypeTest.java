package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.dao.AnforderungHistoryDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungMeldungHistoryServiceModulVereinbarungTypeTest {

    @Mock
    private AnforderungHistoryDao anforderungHistoryDao;

    @Mock
    private Anforderung anforderungOld;

    @Mock
    private Anforderung anforderungNew;

    @Mock
    private ModulSeTeam modulSeTeam;

    @Mock
    private Mitarbeiter user;

    @Mock
    private AnforderungFreigabeBeiModulSeTeam anforderungModulOld;
    @Mock
    private AnforderungFreigabeBeiModulSeTeam anforderungModulNew;

    @Mock
    private Date currentDate;

    @InjectMocks
    private AnforderungMeldungHistoryService historyService;

    @BeforeEach
    public void setUp() {

    }

    @Test
    public void testNoChangeToVereinbarungType() {
        when(anforderungModulOld.getVereinbarungType()).thenReturn(VereinbarungType.ZAK);
        when(anforderungModulNew.getVereinbarungType()).thenReturn(VereinbarungType.ZAK);
        when(anforderungOld.getAnfoFreigabeBeiModul()).thenReturn(Arrays.asList(anforderungModulOld));
        when(anforderungNew.getAnfoFreigabeBeiModul()).thenReturn(Arrays.asList(anforderungModulNew));

        historyService.writeChangesOfModulenVereinbarungType(anforderungOld, anforderungNew, user);
        verify(anforderungHistoryDao, never()).persist(any(AnforderungHistory.class));
    }

    @Test
    public void testChangeVereinbarungFromZAKToSTD() {
        when(anforderungModulOld.getVereinbarungType()).thenReturn(VereinbarungType.ZAK);
        when(anforderungModulNew.getVereinbarungType()).thenReturn(VereinbarungType.STANDARD);
        when(user.getName()).thenReturn("Max Admin");
        when(modulSeTeam.toString()).thenReturn("Modul 1 > SeTeam 11");
        when(anforderungNew.getId()).thenReturn(123L);
        when(anforderungModulOld.getModulSeTeam()).thenReturn(modulSeTeam);
        when(anforderungOld.getAnfoFreigabeBeiModul()).thenReturn(Arrays.asList(anforderungModulOld));
        when(anforderungNew.getAnfoFreigabeBeiModul()).thenReturn(Arrays.asList(anforderungModulNew));

        historyService.writeChangesOfModulenVereinbarungType(anforderungOld, anforderungNew, user);
        AnforderungHistory history = new AnforderungHistory(123L, "A", currentDate, "Modul 1 > SeTeam 11", "Max Admin", "Vereinbarung in ZAK", "Vereinbarung STD");
        verify(anforderungHistoryDao).persist(history);
    }

    @Test
    public void testChangeVereinbarungFromSTDToZAK() {
        when(anforderungModulOld.getVereinbarungType()).thenReturn(VereinbarungType.STANDARD);
        when(anforderungModulNew.getVereinbarungType()).thenReturn(VereinbarungType.ZAK);
        when(user.getName()).thenReturn("Max Admin");
        when(modulSeTeam.toString()).thenReturn("Modul 1 > SeTeam 11");
        when(anforderungNew.getId()).thenReturn(123L);
        when(anforderungModulOld.getModulSeTeam()).thenReturn(modulSeTeam);
        when(anforderungOld.getAnfoFreigabeBeiModul()).thenReturn(Arrays.asList(anforderungModulOld));
        when(anforderungNew.getAnfoFreigabeBeiModul()).thenReturn(Arrays.asList(anforderungModulNew));

        historyService.writeChangesOfModulenVereinbarungType(anforderungOld, anforderungNew, user);
        AnforderungHistory history = new AnforderungHistory(123L, "A", currentDate, "Modul 1 > SeTeam 11", "Max Admin", "Vereinbarung STD", "Vereinbarung in ZAK");
        verify(anforderungHistoryDao).persist(history);
    }

}
