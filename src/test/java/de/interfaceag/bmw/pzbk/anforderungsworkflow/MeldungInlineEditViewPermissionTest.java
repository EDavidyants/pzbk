package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.enums.Status;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class MeldungInlineEditViewPermissionTest {

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 11})
    public void testInlineEditPermittedForBerechtigtenUser(int statusId) {
        Status status = Status.getStatusById(statusId);
        boolean hasRightToInlineEdit = MeldungInlineEditViewPermission.isInlineEdit(status, true);

        switch (status) {
            case M_ENTWURF:
            case M_GEMELDET:
            case M_UNSTIMMIG:
                Assertions.assertTrue(hasRightToInlineEdit);
                break;
            default:
                Assertions.assertFalse(hasRightToInlineEdit);
                break;
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 11})
    public void testInlineEditPermittedForNotBerechtigtenUser(int statusId) {
        Status status = Status.getStatusById(statusId);
        boolean hasRightToInlineEdit = MeldungInlineEditViewPermission.isInlineEdit(status, false);
        Assertions.assertFalse(hasRightToInlineEdit);
    }

}
