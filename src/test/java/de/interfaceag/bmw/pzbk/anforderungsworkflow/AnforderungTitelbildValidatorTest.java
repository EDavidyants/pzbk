package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AnforderungTitelbildValidatorTest {

    @Test
    void isAnhangRequiredForStatusSize() {
        Assertions.assertEquals(5, AnforderungTitelbildValidator.getAnhangRequiredStatus().count());
    }

    @ParameterizedTest(name = "Status {index}: {0}")
    @EnumSource(Status.class)
    void isAnhangRequiredForStatus(Status status) {
        if (AnforderungTitelbildValidator.getAnhangRequiredStatus().anyMatch(s -> s.equals(status))) {
            Assertions.assertTrue(AnforderungTitelbildValidator.isTitelbildRequiredForStatus(status));
        } else {
            Assertions.assertFalse(AnforderungTitelbildValidator.isTitelbildRequiredForStatus(status));
        }
    }

    @Test
    @DisplayName(value = "isAnhangRequiredForNullStatus")
    void isAnhangRequiredForNullStatus() {
        Assertions.assertFalse(AnforderungTitelbildValidator.isTitelbildRequiredForStatus(null));
    }

    @Test
    void validateNull() {
        final boolean valid = AnforderungTitelbildValidator.isValid(null, null);
        Assertions.assertFalse(valid);
    }

    @Test
    void validateValid() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Anhang anhang = TestDataFactory.generateAnhang();
        anforderung.addAnhang(anhang);
        anhang.setStandardBild(true);
        boolean isValid = AnforderungTitelbildValidator.isValid(anforderung, anhang);
        Assertions.assertTrue(isValid);
    }

    @Test
    void validateInValidAnhangNotInList() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Anhang anhang = TestDataFactory.generateAnhang();
        boolean isValid = AnforderungTitelbildValidator.isValid(anforderung, anhang);
        Assertions.assertFalse(isValid);
    }

    @Test
    void validateInValidAnhangNotStandardbild() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Anhang anhang = TestDataFactory.generateAnhang();
        anforderung.addAnhang(anhang);
        boolean isValid = AnforderungTitelbildValidator.isValid(anforderung, anhang);
        Assertions.assertTrue(isValid);
    }

    @Test
    void validateInValidAnhangNull() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        boolean isValid = AnforderungTitelbildValidator.isValid(anforderung, null);
        Assertions.assertFalse(isValid);
    }
}
