package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.controller.dialogs.DerivatZuordnungsDialogController;
import de.interfaceag.bmw.pzbk.dao.DerivatDao;
import de.interfaceag.bmw.pzbk.dao.ZuordnungAnforderungDerivatDao;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ZuordenbareDerivateInAnforderungViewTest {

    @Mock
    private DerivatDao derivatDao;

    @Mock
    private BerechtigungService berechtigungService;

    @InjectMocks
    private final DerivatService derivatService = new DerivatService();

    @Mock
    private ZuordnungAnforderungDerivatDao zuordnungAnforderungDerivatDao;

    @Mock
    private DerivatAnforderungModulService derivatAnforderungModulService;

    @Mock
    private AnforderungMeldungHistoryService anforderungMeldungHistoryService;

    @InjectMocks
    private final ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService = new ZuordnungAnforderungDerivatService();

    private DerivatZuordnungsDialogController controller;

    private List<Derivat> alleDerivate;
    private List<Derivat> zugeordneteDerivate;
    private Derivat e20;
    private Derivat e24;
    private Derivat m2;
    private Derivat f13;
    private Derivat i2;
    private Anforderung anforderung;
    private Mitarbeiter user;

    @BeforeEach
    public void setUp() {
        alleDerivate = new ArrayList<>();
        e20 = TestDataFactory.generateDerivat();
        e20.setName("E20");
        e20.setId(20L);
        e20.setStatus(DerivatStatus.VEREINARBUNG_VKBG);

        e24 = TestDataFactory.generateDerivat();
        e24.setName("E24");
        e24.setId(24L);
        e24.setStatus(DerivatStatus.VEREINBARUNG_ZV);

        m2 = TestDataFactory.generateDerivat();
        m2.setName("M2");
        m2.setId(2L);
        m2.setStatus(DerivatStatus.OFFEN);

        f13 = TestDataFactory.generateDerivat();
        f13.setName("F13");
        f13.setId(13L);
        f13.setStatus(DerivatStatus.ZV);

        i2 = TestDataFactory.generateDerivat();
        i2.setName("I2");
        i2.setId(21L);
        i2.setStatus(DerivatStatus.INAKTIV);

        alleDerivate.add(e20);
        alleDerivate.add(e24);
        alleDerivate.add(m2);
        alleDerivate.add(f13);
        alleDerivate.add(i2);

        List<String> derivatIds = Arrays.asList(String.valueOf(20L), String.valueOf(24L), String.valueOf(2L), String.valueOf(13L), String.valueOf(21L));

        anforderung = TestDataFactory.generateAnforderungWithFachIdAndVersion(123L, "A123", 1);
        anforderung.setStatus(Status.A_FREIGEGEBEN);
        user = TestDataFactory.generateMitarbeiter("Otto", "Mueller", "Abt.-1", "q3333333");

        zugeordneteDerivate = new ArrayList();

        when(derivatDao.getAllDerivate()).thenReturn(alleDerivate);
        when(zuordnungAnforderungDerivatDao.getDerivateForAnforderung(anforderung)).thenReturn(zugeordneteDerivate);

        when(berechtigungService.getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype(user, Rolle.ANFORDERER, BerechtigungZiel.DERIVAT, Rechttype.SCHREIBRECHT)).thenReturn(derivatIds);
        doReturn(e20).when(derivatDao).getDerivatById(20L);
        doReturn(e24).when(derivatDao).getDerivatById(24L);
        doReturn(m2).when(derivatDao).getDerivatById(2L);
        doReturn(f13).when(derivatDao).getDerivatById(13L);
        doReturn(i2).when(derivatDao).getDerivatById(21L);

        controller = new DerivatZuordnungsDialogController(anforderung, user, false, derivatService, zuordnungAnforderungDerivatService);
    }

    @Test
    public void testGenerateListeChoosableDerivateSizeByKeinenZugeordneten() {
        controller = new DerivatZuordnungsDialogController(anforderung, user, false, derivatService, zuordnungAnforderungDerivatService);
        int derivateSize = controller.getDerivatChoosable().size();
        Assertions.assertEquals(2, derivateSize);
    }

    @Test
    public void testGenerateListeChoosableDerivateSizeByEinemZugeordneten() {
        zugeordneteDerivate.add(e20);
        controller = new DerivatZuordnungsDialogController(anforderung, user, false, derivatService, zuordnungAnforderungDerivatService);
        int derivateSize = controller.getDerivatChoosable().size();
        Assertions.assertEquals(1, derivateSize);
    }

    @Test
    public void testGenerateListeChoosableDerivateStatus() {
        controller = new DerivatZuordnungsDialogController(anforderung, user, false, derivatService, zuordnungAnforderungDerivatService);
        List<Derivat> derivate = controller.getDerivatChoosable();
        Assertions.assertTrue(derivate.stream().allMatch(Derivat::isVereinbarungActive));
    }

    @Test
    public void testAddDerivat() {
        controller = new DerivatZuordnungsDialogController(anforderung, user, false, derivatService, zuordnungAnforderungDerivatService);
        // initial state
        int derivateChoosableSize = controller.getDerivatChoosable().size();
        Assertions.assertEquals(2, derivateChoosableSize);
        int derivateZugeordnetSize = controller.getZugeordneteDerivate().size();
        Assertions.assertEquals(0, derivateZugeordnetSize);
        controller.addDerivat(e20);
        // final state after calling addDerivat
        derivateChoosableSize = controller.getDerivatChoosable().size();
        Assertions.assertEquals(1, derivateChoosableSize);
        derivateZugeordnetSize = controller.getZugeordneteDerivate().size();
        Assertions.assertEquals(1, derivateZugeordnetSize);
    }

    @Test
    public void testAddToZugeordneteDerivateToRemove() {
        zugeordneteDerivate.add(e20);
        controller = new DerivatZuordnungsDialogController(anforderung, user, false, derivatService, zuordnungAnforderungDerivatService);
        // initial state
        int derivateChoosableSize = controller.getDerivatChoosable().size();
        Assertions.assertEquals(1, derivateChoosableSize);
        int derivateZugeordnetSize = controller.getZugeordneteDerivate().size();
        Assertions.assertEquals(1, derivateZugeordnetSize);
        controller.addToZugeordneteDerivateToRemove(e20);
        // final state after calling addDerivat
        derivateChoosableSize = controller.getDerivatChoosable().size();
        Assertions.assertEquals(2, derivateChoosableSize);
        derivateZugeordnetSize = controller.getZugeordneteDerivate().size();
        Assertions.assertEquals(0, derivateZugeordnetSize);
    }

    @Test
    public void testAddDerivateToAnforderungEmptyList() {
        controller = new DerivatZuordnungsDialogController(anforderung, user, false, derivatService, zuordnungAnforderungDerivatService);
        controller.addDerivateToAnforderung();
        verify(derivatAnforderungModulService, never()).createDerivatAnforderungModulForAnforderungDerivatZuordnung(any(), any(), any(), any());
    }

    @Test
    public void testAddDerivateToAnforderung() {
        zugeordneteDerivate.add(e20);
        controller = new DerivatZuordnungsDialogController(anforderung, user, false, derivatService, zuordnungAnforderungDerivatService);
        when(berechtigungService.isAnfordererForDerivat(e20, user)).thenReturn(Boolean.TRUE);
        when(zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatForAnforderungDerivat(anforderung, e20)).thenReturn(null);

        ZuordnungAnforderungDerivat anforderungDerivat = new ZuordnungAnforderungDerivat(anforderung, e20, ZuordnungStatus.ZUGEORDNET);
        doNothing().when(zuordnungAnforderungDerivatDao).persistZuordnungAnforderungDerivat(anforderungDerivat);
        doNothing().when(anforderungMeldungHistoryService).persistAnforderungHistory(any());
        controller.addDerivateToAnforderung();
        verify(derivatAnforderungModulService, times(1)).createDerivatAnforderungModulForAnforderungDerivatZuordnung(anforderungDerivat, DerivatAnforderungModulStatus.ABZUSTIMMEN, "Die Anforderung A123 | V1 wurde dem Derivat E20 zugeordnet.", user);
    }

}
