package de.interfaceag.bmw.pzbk.anforderungsworkflow;

import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

/**
 *
 * @author sl
 */
public class AnforderungInlineEditViewPermissionTest {

    @ParameterizedTest
    @EnumSource(Status.class)
    public void testIsInlineEditForWritePermissionTrue(Status status) {
        boolean result = AnforderungInlineEditViewPermission.isInlineEdit(status, Boolean.TRUE);

        switch (status) {
            case A_INARBEIT:
            case A_UNSTIMMIG:
            case A_FTABGESTIMMT:
            case A_PLAUSIB:
            case A_ANGELEGT_IN_PROZESSBAUKASTEN:
                Assertions.assertTrue(result);
                break;
            default:
                Assertions.assertFalse(result);
        }
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    public void testIsInlineEditForWritePermissionFalse(Status status) {
        boolean result = AnforderungInlineEditViewPermission.isInlineEdit(status, Boolean.FALSE);
        Assertions.assertFalse(result);
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    public void testIsInlineEditForWriteViewPermissionTrue(Status status) {
        ViewPermission result = AnforderungInlineEditViewPermission.isInlineEditViewPermission(status, Boolean.TRUE);

        switch (status) {
            case A_INARBEIT:
            case A_UNSTIMMIG:
            case A_FTABGESTIMMT:
            case A_PLAUSIB:
            case A_ANGELEGT_IN_PROZESSBAUKASTEN:
                Assertions.assertTrue(result.isRendered());
                break;
            default:
                Assertions.assertFalse(result.isRendered());
        }
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    public void testIsInlineEditForWriteViewPermissionFalse(Status status) {
        ViewPermission result = AnforderungInlineEditViewPermission.isInlineEditViewPermission(status, Boolean.FALSE);
        Assertions.assertFalse(result.isRendered());
    }

}
