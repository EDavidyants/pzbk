package de.interfaceag.bmw.pzbk.anforderungsworkflow.permission;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author evda
 */
public class AnforderungViewPermissionUtilsTest {

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testFahrzeugmerkmalePermissionForInArbeitStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForFahrzeugmerkmaleButton(userRollen, Status.A_INARBEIT);
        switch (role) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testFahrzeugmerkmalePermissionForUnstimmigStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForFahrzeugmerkmaleButton(userRollen, Status.A_UNSTIMMIG);
        switch (role) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testFahrzeugmerkmalePermissionForAbgestimmtStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForFahrzeugmerkmaleButton(userRollen, Status.A_FTABGESTIMMT);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testFahrzeugmerkmalePermissionForPlausibilisiertStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForFahrzeugmerkmaleButton(userRollen, Status.A_PLAUSIB);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testFahrzeugmerkmalePermissionForFreigegebenStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForFahrzeugmerkmaleButton(userRollen, Status.A_FREIGEGEBEN);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testFahrzeugmerkmalePermissionForKeineWeiterverfolgungStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForFahrzeugmerkmaleButton(userRollen, Status.A_KEINE_WEITERVERFOLG);
        switch (role) {
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testFahrzeugmerkmalePermissionForGeloeschtStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForFahrzeugmerkmaleButton(userRollen, Status.A_GELOESCHT);
        switch (role) {
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testFahrzeugmerkmalePermissionForAngelegtInProzessbaukastenStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForFahrzeugmerkmaleButton(userRollen, Status.A_ANGELEGT_IN_PROZESSBAUKASTEN);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testFahrzeugmerkmalePermissionForGenemigtInProzessbaukastenStatus(Rolle role) {
        Set<Rolle> userRollen = new HashSet();
        userRollen.add(role);
        EditPermission permission = AnforderungViewPermissionUtils
                .buildPermissionForFahrzeugmerkmaleButton(userRollen, Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
        switch (role) {
            case ADMIN:
                Assertions.assertTrue(permission.hasRightToEdit());
                break;
            default:
                Assertions.assertFalse(permission.hasRightToEdit());
                break;
        }
    }

}
