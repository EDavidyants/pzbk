package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.api.AnforderungEditFahrzeugmerkmalDialogData;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsMapContaining;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

@ExtendWith(MockitoExtension.class)
class AnforderungEditFahrzeugmerkmalDataTest {

    private static final ModulSeTeamId MODUL_SE_TEAM_ID = new ModulSeTeamId(32L);
    private AnforderungEditFahrzeugmerkmalData anforderungEditFahrzeugmerkmalData;

    @Mock
    private AnforderungEditFahrzeugmerkmalDialogData fahrzeugmerkmalDialogData;

    @BeforeEach
    void setUp() {
        anforderungEditFahrzeugmerkmalData = new AnforderungEditFahrzeugmerkmalData();
    }

    @Test
    void testDefaultConstructor() {
        AnforderungEditFahrzeugmerkmalDataAssert.assertThat(anforderungEditFahrzeugmerkmalData)
                .hasActiveFahrzeugmerkmalDialogData(null)
                .hasActiveModulSeTeamId(null)
                .hasModulSeTeamFahrzeugmerkmalDialogDataMap(new HashMap());
    }

    @Test
    void addDataForModulSeTeam() {
        anforderungEditFahrzeugmerkmalData.addDataForModulSeTeam(MODUL_SE_TEAM_ID, fahrzeugmerkmalDialogData);
        final Map<ModulSeTeamId, AnforderungEditFahrzeugmerkmalDialogData> map = anforderungEditFahrzeugmerkmalData.getModulSeTeamFahrzeugmerkmalDialogDataMap();
        MatcherAssert.assertThat(map, IsMapContaining.hasEntry(MODUL_SE_TEAM_ID, fahrzeugmerkmalDialogData));
    }


    @Test
    void containsDataForModulSeTeamNotPresent() {
        anforderungEditFahrzeugmerkmalData.addDataForModulSeTeam(MODUL_SE_TEAM_ID, fahrzeugmerkmalDialogData);
        final boolean result = anforderungEditFahrzeugmerkmalData.updateActiveData(new ModulSeTeamId(2L));
        Assertions.assertFalse(result);
        AnforderungEditFahrzeugmerkmalDataAssert.assertThat(anforderungEditFahrzeugmerkmalData)
                .hasActiveFahrzeugmerkmalDialogData(null)
                .hasActiveModulSeTeamId(null);
    }

    @Test
    void containsDataForModulSeTeamPresent() {
        anforderungEditFahrzeugmerkmalData.addDataForModulSeTeam(MODUL_SE_TEAM_ID, fahrzeugmerkmalDialogData);
        final boolean result = anforderungEditFahrzeugmerkmalData.updateActiveData(MODUL_SE_TEAM_ID);
        Assertions.assertTrue(result);
        AnforderungEditFahrzeugmerkmalDataAssert.assertThat(anforderungEditFahrzeugmerkmalData)
                .hasActiveFahrzeugmerkmalDialogData(fahrzeugmerkmalDialogData)
                .hasActiveModulSeTeamId(MODUL_SE_TEAM_ID);
    }

    @Test
    void getAnforderungEditFahrzeugmerkmalDialogDataForModulSeTeam() {
        anforderungEditFahrzeugmerkmalData.addDataForModulSeTeam(MODUL_SE_TEAM_ID, fahrzeugmerkmalDialogData);
        final AnforderungEditFahrzeugmerkmalDialogData result = anforderungEditFahrzeugmerkmalData.getAnforderungEditFahrzeugmerkmalDialogDataForModulSeTeam(MODUL_SE_TEAM_ID);
        MatcherAssert.assertThat(result, CoreMatchers.is(fahrzeugmerkmalDialogData));

    }
}