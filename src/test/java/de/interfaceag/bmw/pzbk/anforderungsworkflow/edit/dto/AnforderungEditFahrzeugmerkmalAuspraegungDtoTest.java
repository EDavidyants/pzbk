package de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.dto;

import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AnforderungEditFahrzeugmerkmalAuspraegungDtoTest {

    private static final Long ID = 42L;
    private static final String AUSPRAEGUNG = "auspraegung";
    private static final FahrzeugmerkmalAuspraegungId FAHRZEUGMERKMAL_AUSPRAEGUNG_ID = new FahrzeugmerkmalAuspraegungId(ID);

    private AnforderungEditFahrzeugmerkmalAuspraegungDto anforderungEditFahrzeugmerkmalAuspraegungDto;

    @BeforeEach
    void setUp() {
        anforderungEditFahrzeugmerkmalAuspraegungDto = new AnforderungEditFahrzeugmerkmalAuspraegungDto(FAHRZEUGMERKMAL_AUSPRAEGUNG_ID, AUSPRAEGUNG);
    }

    @Test
    void testGetters() {
        AnforderungEditFahrzeugmerkmalAuspraegungDtoAssert
                .assertThat(anforderungEditFahrzeugmerkmalAuspraegungDto)
                .hasAuspraegung(AUSPRAEGUNG)
                .hasId(ID)
                .hasFahrzeugmerkmalAuspraegungId(FAHRZEUGMERKMAL_AUSPRAEGUNG_ID);
    }

}