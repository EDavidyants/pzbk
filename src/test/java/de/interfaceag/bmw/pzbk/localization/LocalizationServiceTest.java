package de.interfaceag.bmw.pzbk.localization;

import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.UserSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Locale;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@RunWith(MockitoJUnitRunner.class)
public class LocalizationServiceTest {

    @InjectMocks
    private final LocalizationService localizationService = new LocalizationService();

    @Before
    public void setup() {
        localizationService.session = mock(UserSession.class);
        when(localizationService.session.getUser()).thenReturn(TestDataFactory.createMitarbeiter(""));
    }

    @Test
    public void getCurrentLocaleDummyTest() {
        Locale currentLocale = localizationService.getCurrentLocale();
        Assertions.assertEquals(Locale.GERMAN, currentLocale);
    }

}
