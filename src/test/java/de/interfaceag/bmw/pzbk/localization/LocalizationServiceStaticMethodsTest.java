package de.interfaceag.bmw.pzbk.localization;

import de.interfaceag.bmw.pzbk.exceptions.NotSupportedException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.Locale;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class LocalizationServiceStaticMethodsTest {

    @Test
    public void accessKeyValueTest() {
        String value = LocalizationService.getValue(Locale.GERMAN, "fuer");
        Assertions.assertEquals("für", value);
    }

    @Test
    public void accessKeyNotFoundTest() {
        String value = LocalizationService.getValue(Locale.GERMAN, "fuerrrr");
        Assertions.assertEquals("Key 'fuerrrr' für Locale de nicht vorhanden!", value);
    }

    @Test
    public void notSupportedLocaleTest() {
        String value = LocalizationService.getValue(Locale.FRENCH, "fuer");
        Assertions.assertEquals("für", value);
    }

    @Test
    public void supportedLocalesSizeTest() {
        Stream<Locale> locales = LocalizationService.getSupportedLocales();
        Assertions.assertEquals(2, locales.count());
    }

    @Test
    public void supportedLocalesContainsTestGerman() {
        Stream<Locale> locales = LocalizationService.getSupportedLocales();
        Assertions.assertTrue(locales.anyMatch(l -> l.equals(Locale.GERMAN)));
    }

    @Test
    public void supportedLocalesContainsTestEnglisch() {
        Stream<Locale> locales = LocalizationService.getSupportedLocales();
        Assertions.assertTrue(locales.anyMatch(l -> l.equals(Locale.ENGLISH)));
    }

    @Test(expected = NotSupportedException.class)
    public void notSupportedLocaleExceptionTest() throws NotSupportedException {
        LocalizationService.isCurrentLocaleSupported(Locale.FRENCH);
    }

}
