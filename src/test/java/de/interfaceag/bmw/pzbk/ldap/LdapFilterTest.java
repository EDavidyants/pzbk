package de.interfaceag.bmw.pzbk.ldap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class LdapFilterTest {

    private static final String GIVENNAME = "gn";
    private static final String SURNAME = "sn";
    private static final String DEPARTMENTNUMBER = "dn";


    private LdapFilter ldapFilter;

    @BeforeEach
    void setUp() {
        ldapFilter = LdapFilter.newInstance();
    }

    @Test
    void newInstanceQuery() {
        final String ldapFilterQuery = ldapFilter.getLdapFilterQuery();
        assertThat(ldapFilterQuery, is("(&)"));
    }

    @Test
    void withGivennameValidValueQuery() {
        final String ldapFilterQuery = ldapFilter.withGivenname(GIVENNAME).getLdapFilterQuery();
        assertThat(ldapFilterQuery, is("(&(givenname=" + GIVENNAME + "))"));
    }

    @Test
    void withGivennameNullQuery() {
        final String ldapFilterQuery = ldapFilter.withGivenname(null).getLdapFilterQuery();
        assertThat(ldapFilterQuery, is("(&)"));
    }

    @Test
    void withGivennameEmptyStringQuery() {
        final String ldapFilterQuery = ldapFilter.withGivenname("").getLdapFilterQuery();
        assertThat(ldapFilterQuery, is("(&)"));
    }

    @Test
    void withSurnameValidValueQuery() {
        final String ldapFilterQuery = ldapFilter.withSurname(SURNAME).getLdapFilterQuery();
        assertThat(ldapFilterQuery, is("(&(sn=" + SURNAME + "))"));
    }

    @Test
    void withSurnameNullQuery() {
        final String ldapFilterQuery = ldapFilter.withSurname(null).getLdapFilterQuery();
        assertThat(ldapFilterQuery, is("(&)"));
    }

    @Test
    void withSurnameEmptyStringQuery() {
        final String ldapFilterQuery = ldapFilter.withSurname("").getLdapFilterQuery();
        assertThat(ldapFilterQuery, is("(&)"));
    }

    @Test
    void withDepartmentNumberValidValueQuery() {
        final String ldapFilterQuery = ldapFilter.withDepartmentNumber(DEPARTMENTNUMBER).getLdapFilterQuery();
        assertThat(ldapFilterQuery, is("(&(departmentnumber=" + DEPARTMENTNUMBER + "))"));
    }

    @Test
    void withDepartmentNumberNullQuery() {
        final String ldapFilterQuery = ldapFilter.withDepartmentNumber(null).getLdapFilterQuery();
        assertThat(ldapFilterQuery, is("(&)"));
    }

    @Test
    void withDepartmentNumberEmptyStringQuery() {
        final String ldapFilterQuery = ldapFilter.withDepartmentNumber("").getLdapFilterQuery();
        assertThat(ldapFilterQuery, is("(&)"));
    }

}