package de.interfaceag.bmw.pzbk.modulorga;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ModulOrgaUtilsTest {

    @Test
    public void testAnforderungAlreadyHasNewModul() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Modul newModul = TestDataFactory.generateModul();
        assertTrue(ModulOrgaUtils.anforderungAlreadyHasNewModul(newModul, anforderung));
    }

    @Test
    public void testAnforderungAlreadyHasNewModulDifferentId() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Modul newModul = TestDataFactory.generateModul();
        newModul.setId(2L);
        assertFalse(ModulOrgaUtils.anforderungAlreadyHasNewModul(newModul, anforderung));
    }

}
