package de.interfaceag.bmw.pzbk.enums;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Optional;

/**
 * @author evda
 */
public class DashboardStepTest {

    @Test
    public void getAllSteps() {
        DashboardStep[] steps = DashboardStep.values();
        Assertions.assertEquals(14, steps.length);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14})
    public void testGetByStepNumberExistent(int stepNumber) {
        Optional<DashboardStep> step = DashboardStep.getByStepNumber(stepNumber);
        Assertions.assertTrue(step.isPresent());
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 7})
    public void testGetByStepNumberNotExistent(int stepNumber) {
        Optional<DashboardStep> step = DashboardStep.getByStepNumber(stepNumber);
        Assertions.assertFalse(step.isPresent());
    }

    @ParameterizedTest
    @EnumSource(DashboardStep.class)
    public void testStepNumber(DashboardStep step) {
        switch (step) {
            case MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG:
                Assertions.assertEquals(1, step.getStepNumber());
                break;
            case GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG:
                Assertions.assertEquals(2, step.getStepNumber());
                break;
            case ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT:
                Assertions.assertEquals(3, step.getStepNumber());
                break;
            case FREIGEGEBENEN_ANFORDERUNGEN:
                Assertions.assertEquals(4, step.getStepNumber());
                break;
            case ANFORDERUNGEN_VEREINBART:
                Assertions.assertEquals(5, step.getStepNumber());
                break;
            case ANFORDERUNGEN_UMGESETZT:
                Assertions.assertEquals(6, step.getStepNumber());
                break;
            case BESTAETIGER_ZUORDNEN:
                Assertions.assertEquals(8, step.getStepNumber());
                break;
            case UMSETZUNG_BESTAETIGEN:
                Assertions.assertEquals(9, step.getStepNumber());
                break;
            case ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET:
                Assertions.assertEquals(10, step.getStepNumber());
                break;
            case NICHT_UMGESETZTEN_ANFORDERUNGEN:
                Assertions.assertEquals(11, step.getStepNumber());
                break;
            case ANFORDERUNGEN_OFFEN_IN_ZAK:
                Assertions.assertEquals(12, step.getStepNumber());
                break;
            case ANFORDERUNGEN_ABGELEHNT_IN_ZAK:
                Assertions.assertEquals(13, step.getStepNumber());
                break;
            case LANGLAEUFER:
                Assertions.assertEquals(14, step.getStepNumber());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(DashboardStep.class)
    public void testStepLabel(DashboardStep step) {
        switch (step) {
            case MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG:
                Assertions.assertEquals("Entwurf | unstimmig", step.getStepLabel());
                break;
            case GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG:
                Assertions.assertEquals("Gemeldet | in Arbeit | unstimmig", step.getStepLabel());
                break;
            case ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT:
                Assertions.assertEquals("Abgestimmt | plausibilisiert", step.getStepLabel());
                break;
            case FREIGEGEBENEN_ANFORDERUNGEN:
                Assertions.assertEquals("Freigegeben", step.getStepLabel());
                break;
            case ANFORDERUNGEN_VEREINBART:
                Assertions.assertEquals("Vereinbart", step.getStepLabel());
                break;
            case ANFORDERUNGEN_UMGESETZT:
                Assertions.assertEquals("Umgesetzt", step.getStepLabel());
                break;
            case BESTAETIGER_ZUORDNEN:
                Assertions.assertEquals("Bestaetiger zuordnen", step.getStepLabel());
                break;
            case UMSETZUNG_BESTAETIGEN:
                Assertions.assertEquals("Umsetzung bestaetigen", step.getStepLabel());
                break;
            case ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET:
                Assertions.assertEquals("Unzureichende Anforderungsqualitaet", step.getStepLabel());
                break;
            case NICHT_UMGESETZTEN_ANFORDERUNGEN:
                Assertions.assertEquals("Nicht umgesetzt", step.getStepLabel());
                break;
            case ANFORDERUNGEN_OFFEN_IN_ZAK:
                Assertions.assertEquals("ZAK | offen", step.getStepLabel());
                break;
            case ANFORDERUNGEN_ABGELEHNT_IN_ZAK:
                Assertions.assertEquals("ZAK | abgelehnt", step.getStepLabel());
                break;
            case LANGLAEUFER:
                Assertions.assertEquals("Langlaeufer", step.getStepLabel());
                break;
        }
    }
}
