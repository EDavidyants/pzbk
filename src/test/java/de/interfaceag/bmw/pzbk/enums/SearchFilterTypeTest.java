package de.interfaceag.bmw.pzbk.enums;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class SearchFilterTypeTest {

    @Test
    void test_case_0_SearchFilterType_STATUS() {
        Assert.assertEquals("Status", SearchFilterType.STATUS.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_status", SearchFilterType.STATUS.getPropertyName());
        Assert.assertEquals("Status", SearchFilterType.STATUS.getClassName());
    }

    @Test
    void test_case_1_SearchFilterType_SENSORCOC() {
        Assert.assertEquals("Sensor Coc", SearchFilterType.SENSORCOC.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_sensorcoc", SearchFilterType.SENSORCOC.getPropertyName());
        Assert.assertEquals("SensorCoc", SearchFilterType.SENSORCOC.getClassName());
    }

    @Test
    void test_case_2_SearchFilterType_TTEAM() {
        Assert.assertEquals("T-Team", SearchFilterType.TTEAM.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_tteam", SearchFilterType.TTEAM.getPropertyName());
        Assert.assertEquals("Tteam", SearchFilterType.TTEAM.getClassName());
    }

    @Test
    void test_case_3_SearchFilterType_DERIVAT() {
        Assert.assertEquals("Derivat", SearchFilterType.DERIVAT.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_derivat", SearchFilterType.DERIVAT.getPropertyName());
        Assert.assertEquals("Derivat", SearchFilterType.DERIVAT.getClassName());
    }

    @Test
    void test_case_4_SearchFilterType_DASHBOARD() {
        Assert.assertEquals("Dashboard", SearchFilterType.DASHBOARD.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_dashboard", SearchFilterType.DASHBOARD.getPropertyName());
        Assert.assertEquals("Long", SearchFilterType.DASHBOARD.getClassName());
    }

    @Test
    void test_case_5_SearchFilterType_MELDUNG() {
        Assert.assertEquals("Meldung", SearchFilterType.MELDUNG.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_meldung", SearchFilterType.MELDUNG.getPropertyName());
        Assert.assertEquals("Meldung", SearchFilterType.MELDUNG.getClassName());
    }

    @Test
    void test_case_6_SearchFilterType_ANFORDERUNG() {
        Assert.assertEquals("Anforderung", SearchFilterType.ANFORDERUNG.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_anforderung", SearchFilterType.ANFORDERUNG.getPropertyName());
        Assert.assertEquals("Anforderung", SearchFilterType.ANFORDERUNG.getClassName());
    }

    @Test
    void test_case_7_SearchFilterType_ID() {
        Assert.assertEquals("Id", SearchFilterType.ID.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_id", SearchFilterType.ID.getPropertyName());
        Assert.assertEquals("Long", SearchFilterType.ID.getClassName());
    }

    @Test
    void test_case_8_SearchFilterType_ERSTELLUNGSDATUM() {
        Assert.assertEquals("Erstellungdatum: von - bis", SearchFilterType.ERSTELLUNGSDATUM.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_erstellungDatum", SearchFilterType.ERSTELLUNGSDATUM.getPropertyName());
        Assert.assertEquals("Erstellungdatum", SearchFilterType.ERSTELLUNGSDATUM.getClassName());
    }

    @Test
    void test_case_9_SearchFilterType_AENDERUNGSDATUM() {
        Assert.assertEquals("Änderungsdatum: von - bis", SearchFilterType.AENDERUNGSDATUM.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_aenderungsDatum", SearchFilterType.AENDERUNGSDATUM.getPropertyName());
        Assert.assertEquals("Änderungsdatum", SearchFilterType.AENDERUNGSDATUM.getClassName());
    }

    @Test
    void test_case_10_SearchFilterType_STATUS_ANFORDERUNG() {
        Assert.assertEquals("Status Anforderung", SearchFilterType.STATUS_ANFORDERUNG.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_satusAnforderung", SearchFilterType.STATUS_ANFORDERUNG.getPropertyName());
        Assert.assertEquals("Status", SearchFilterType.STATUS_ANFORDERUNG.getClassName());
    }

    @Test
    void test_case_11_SearchFilterType_STATUS_MELDUNG() {
        Assert.assertEquals("Status Meldung", SearchFilterType.STATUS_MELDUNG.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_statusMeldung", SearchFilterType.STATUS_MELDUNG.getPropertyName());
        Assert.assertEquals("Status", SearchFilterType.STATUS_MELDUNG.getClassName());
    }

    @Test
    void test_case_12_SearchFilterType_AENDERUNGSDATUMVON() {
        Assert.assertEquals("Änderungsdatum: von", SearchFilterType.AENDERUNGSDATUM_VON.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_aenderungsdatumVon", SearchFilterType.AENDERUNGSDATUM_VON.getPropertyName());
        Assert.assertEquals("Änderungsdatum", SearchFilterType.AENDERUNGSDATUM_VON.getClassName());
    }

    @Test
    void test_case_13_SearchFilterType_AENDERUNGSDATUM_BIS() {
        Assert.assertEquals("Änderungsdatum: bis", SearchFilterType.AENDERUNGSDATUM_BIS.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_aenderungsdatumBis", SearchFilterType.AENDERUNGSDATUM_BIS.getPropertyName());
        Assert.assertEquals("Änderungsdatum", SearchFilterType.AENDERUNGSDATUM_BIS.getClassName());
    }

    @Test
    void test_case_14_SearchFilterType_ERSTELLUNGSDATUM_VON() {
        Assert.assertEquals("Erstellungdatum: von", SearchFilterType.ERSTELLUNGSDATUM_VON.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_erstellungdatumVon", SearchFilterType.ERSTELLUNGSDATUM_VON.getPropertyName());
        Assert.assertEquals("Erstellungdatum", SearchFilterType.ERSTELLUNGSDATUM_VON.getClassName());
    }

    @Test
    void test_case_15_SearchFilterType_ERSTELLUNGSDATUM_BIS() {
        Assert.assertEquals("Erstellungdatum: bis", SearchFilterType.ERSTELLUNGSDATUM_BIS.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_erstellungdatumBis", SearchFilterType.ERSTELLUNGSDATUM_BIS.getPropertyName());
        Assert.assertEquals("Erstellungdatum", SearchFilterType.ERSTELLUNGSDATUM_BIS.getClassName());
    }

    @Test
    void test_case_16_SearchFilterType_STATUS_AENDERUNGSDATUM() {
        Assert.assertEquals("Status Änderungsdatum: von - bis", SearchFilterType.STATUS_AENDERUNGSDATUM.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_statusaenderungdatum", SearchFilterType.STATUS_AENDERUNGSDATUM.getPropertyName());
        Assert.assertEquals("Status Änderungsdatum", SearchFilterType.STATUS_AENDERUNGSDATUM.getClassName());
    }

    @Test
    void test_case_17_SearchFilterType_STATUSAENDERUNGSDATUM_VON() {
        Assert.assertEquals("Status Änderungsdatum: von", SearchFilterType.STATUSAENDERUNGSDATUM_VON.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_statusaenderungsdatumVon", SearchFilterType.STATUSAENDERUNGSDATUM_VON.getPropertyName());
        Assert.assertEquals("Status Änderungsdatum", SearchFilterType.STATUSAENDERUNGSDATUM_VON.getClassName());
    }

    @Test
    void test_case_18_SearchFilterType_STATUS_AENDERUNGSDATUM_BIS() {
        Assert.assertEquals("Status Änderungsdatum: bis", SearchFilterType.STATUS_AENDERUNGSDATUM_BIS.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_statusaenderungsdatumBis", SearchFilterType.STATUS_AENDERUNGSDATUM_BIS.getPropertyName());
        Assert.assertEquals("Status Änderungsdatum", SearchFilterType.STATUS_AENDERUNGSDATUM_BIS.getClassName());
    }

    @Test
    void test_case_19_SearchFilterType_SORT_NAME_ASC() {
        Assert.assertEquals("Sortierung: FachId (aufsteigend)", SearchFilterType.SORT_NAME_ASC.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_sortierungFachIdAufsteigend", SearchFilterType.SORT_NAME_ASC.getPropertyName());
        Assert.assertEquals("Sortierung", SearchFilterType.SORT_NAME_ASC.getClassName());
    }

    @Test
    void test_case_20_SearchFilterType_SORT_NAME_DESC() {
        Assert.assertEquals("Sortierung: FachId (absteigend)", SearchFilterType.SORT_NAME_DESC.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_sortierungFachIdAbsteigend", SearchFilterType.SORT_NAME_DESC.getPropertyName());
        Assert.assertEquals("Sortierung", SearchFilterType.SORT_NAME_DESC.getClassName());
    }

    @Test
    void test_case_21_SearchFilterType_SORT_AENDERUNGSDATUM_ASC() {
        Assert.assertEquals("Sortierung: Änderungsdatum (aufsteigend)", SearchFilterType.SORT_AENDERUNGSDATUM_ASC.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_sortierungAenderungsdatumAufsteigend", SearchFilterType.SORT_AENDERUNGSDATUM_ASC.getPropertyName());
        Assert.assertEquals("Sortierung", SearchFilterType.SORT_AENDERUNGSDATUM_ASC.getClassName());
    }

    @Test
    void test_case_22_SearchFilterType_SORT_AENDERUNGSDATUM_DESC() {
        Assert.assertEquals("Sortierung: Änderungsdatum (absteigend)", SearchFilterType.SORT_AENDERUNGSDATUM_DESC.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_sortierungAenderungsdatumAbsteigend", SearchFilterType.SORT_AENDERUNGSDATUM_DESC.getPropertyName());
        Assert.assertEquals("Sortierung", SearchFilterType.SORT_AENDERUNGSDATUM_DESC.getClassName());
    }

    @Test
    void test_case_23_SearchFilterType_HISTORIE() {
        Assert.assertEquals("Historie", SearchFilterType.HISTORIE.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_historie", SearchFilterType.HISTORIE.getPropertyName());
        Assert.assertEquals("Historie", SearchFilterType.HISTORIE.getClassName());
    }

    @Test
    void test_case_24_SearchFilterType_STATUS_DERIVAT() {
        Assert.assertEquals("Status Derivat", SearchFilterType.STATUS_DERIVAT.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_statusDerivat", SearchFilterType.STATUS_DERIVAT.getPropertyName());
        Assert.assertEquals("Derivat", SearchFilterType.STATUS_DERIVAT.getClassName());
    }

    @Test
    void test_case_25_SearchFilterType_STATUS_GELOESCHT() {
        Assert.assertEquals("Gelöschte Elemente anzeigen", SearchFilterType.STATUS_GELOESCHT.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_geloeschteElementeAnzeigen", SearchFilterType.STATUS_GELOESCHT.getPropertyName());
        Assert.assertEquals("Status", SearchFilterType.STATUS_GELOESCHT.getClassName());
    }

    @Test
    void test_case_26_SearchFilterType_URL() {
        Assert.assertEquals("URL", SearchFilterType.URL.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_url", SearchFilterType.URL.getPropertyName());
        Assert.assertEquals("Long", SearchFilterType.URL.getClassName());
    }

    @Test
    void test_case_27_SearchFilterType_MODUL() {
        Assert.assertEquals("Modul", SearchFilterType.MODUL.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_modul", SearchFilterType.MODUL.getPropertyName());
        Assert.assertEquals("Modul", SearchFilterType.MODUL.getClassName());
    }

    @Test
    void test_case_28_SearchFilterType_MODULSETEAM() {
        Assert.assertEquals("ModulSeTeam", SearchFilterType.MODULSETEAM.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_modulSeTeam", SearchFilterType.MODULSETEAM.getPropertyName());
        Assert.assertEquals("ModulSeTeam", SearchFilterType.MODULSETEAM.getClassName());
    }

    @Test
    void test_case_29_SearchFilterType_SUCHTEXT() {
        Assert.assertEquals("Suchtext", SearchFilterType.SUCHTEXT.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_suchtext", SearchFilterType.SUCHTEXT.getPropertyName());
        Assert.assertEquals("String", SearchFilterType.SUCHTEXT.getClassName());
    }

    @Test
    void test_case_30_SearchFilterType_PZBK() {
        Assert.assertEquals("Pzbk", SearchFilterType.PZBK.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_pzbk", SearchFilterType.PZBK.getPropertyName());
        Assert.assertEquals("Pzbk", SearchFilterType.PZBK.getClassName());
    }

    @Test
    void test_case_31_SearchFilterType_FESTGESTELLT_IN() {
        Assert.assertEquals("Festgestellt In", SearchFilterType.FESTGESTELLT_IN.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_festgestelltIn", SearchFilterType.FESTGESTELLT_IN.getPropertyName());
        Assert.assertEquals("Long", SearchFilterType.FESTGESTELLT_IN.getClassName());
    }

    @Test
    void test_case_32_SearchFilterType_UMSETZUNGSBESTAETIGUNG_STATUS() {
        Assert.assertNull(SearchFilterType.UMSETZUNGSBESTAETIGUNG_STATUS.getBezeichnung());
        Assert.assertEquals("", SearchFilterType.UMSETZUNGSBESTAETIGUNG_STATUS.getPropertyName());
        Assert.assertNull(SearchFilterType.UMSETZUNGSBESTAETIGUNG_STATUS.getClassName());
    }

    @Test
    void test_case_33_SearchFilterType_ZAK_STATUS() {
        Assert.assertNull(SearchFilterType.ZAK_STATUS.getBezeichnung());
        Assert.assertEquals("", SearchFilterType.ZAK_STATUS.getPropertyName());
        Assert.assertNull(SearchFilterType.ZAK_STATUS.getClassName());
    }

    @Test
    void test_case_34_SearchFilterType_ZAK_BOOLEAN() {
        Assert.assertNull(SearchFilterType.ZAK_BOOLEAN.getBezeichnung());
        Assert.assertEquals("", SearchFilterType.ZAK_BOOLEAN.getPropertyName());
        Assert.assertNull(SearchFilterType.ZAK_BOOLEAN.getClassName());
    }

    @Test
    void test_case_35_SearchFilterType_STATUS_OFFEN() {
        Assert.assertNull(SearchFilterType.STATUS_OFFEN.getBezeichnung());
        Assert.assertEquals("", SearchFilterType.STATUS_OFFEN.getPropertyName());
        Assert.assertNull(SearchFilterType.STATUS_OFFEN.getClassName());
    }

    @Test
    void test_case_36_SearchFilterType_DASHBOARD_MELDUNG() {
        Assert.assertEquals("Dashboard Meldung", SearchFilterType.DASHBOARD_MELDUNG.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_dashboardMeldung", SearchFilterType.DASHBOARD_MELDUNG.getPropertyName());
        Assert.assertEquals("Long", SearchFilterType.DASHBOARD_MELDUNG.getClassName());
    }

    @Test
    void test_case_37_SearchFilterType_DASHBOARD_ANFORDERUNG() {
        Assert.assertEquals("Dashboard Anforderung", SearchFilterType.DASHBOARD_ANFORDERUNG.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_dashboardAnforderung", SearchFilterType.DASHBOARD_ANFORDERUNG.getPropertyName());
        Assert.assertEquals("Long", SearchFilterType.DASHBOARD_ANFORDERUNG.getClassName());
    }

    @Test
    void test_case_38_SearchFilterType_URL_MELDUNG() {
        Assert.assertEquals("URL Meldung", SearchFilterType.URL_MELDUNG.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_urlMeldung", SearchFilterType.URL_MELDUNG.getPropertyName());
        Assert.assertEquals("Long", SearchFilterType.URL_MELDUNG.getClassName());
    }

    @Test
    void test_case_39_SearchFilterType_URL_ANFORDERUNG() {
        Assert.assertEquals("URL Anforderung", SearchFilterType.URL_ANFORDERUNG.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_urlAnforderung", SearchFilterType.URL_ANFORDERUNG.getPropertyName());
        Assert.assertEquals("Long", SearchFilterType.URL_ANFORDERUNG.getClassName());
    }

    @Test
    void test_case_40_SearchFilterType_KOVA_STATUS() {
        Assert.assertEquals("KovAStatus", SearchFilterType.KOVA_STATUS.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_kovastatus", SearchFilterType.KOVA_STATUS.getPropertyName());
        Assert.assertNull(SearchFilterType.KOVA_STATUS.getClassName());
    }

    @Test
    void test_case_41_SearchFilterType_KOVA_PHASE() {
        Assert.assertEquals("KovAPhase", SearchFilterType.KOVA_PHASE.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_kovaphase", SearchFilterType.KOVA_PHASE.getPropertyName());
        Assert.assertNull(SearchFilterType.KOVA_PHASE.getClassName());
    }

    @Test
    void test_case_42_SearchFilterType_VONDATE() {
        Assert.assertEquals("von", SearchFilterType.VONDATE.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_von", SearchFilterType.VONDATE.getPropertyName());
        Assert.assertNull(SearchFilterType.VONDATE.getClassName());
    }

    @Test
    void test_case_43_SearchFilterType_BISDATE() {
        Assert.assertEquals("bis", SearchFilterType.BIS_DATE.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_bis", SearchFilterType.BIS_DATE.getPropertyName());
        Assert.assertNull(SearchFilterType.BIS_DATE.getClassName());
    }

    @Test
    void test_case_44_SearchFilterType_RELEVANZ() {
        Assert.assertEquals("Phasenrelevanz", SearchFilterType.RELEVANZ.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_phasenrelevanz", SearchFilterType.RELEVANZ.getPropertyName());
        Assert.assertNull(SearchFilterType.RELEVANZ.getClassName());
    }

    @Test
    void test_case_45_SearchFilterType_ZUORDNUNG_ZAK_STATUS() {
        Assert.assertEquals("ZAK Status", SearchFilterType.ZUORDNUNG_ZAK_STATUS.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_zakStatus", SearchFilterType.ZUORDNUNG_ZAK_STATUS.getPropertyName());
        Assert.assertNull(SearchFilterType.ZUORDNUNG_ZAK_STATUS.getClassName());
    }

    @Test
    void test_case_46_SearchFilterType_VON_BIS_DATE() {
        Assert.assertNull(SearchFilterType.VON_BIS_DATE.getBezeichnung());
        Assert.assertEquals("", SearchFilterType.VON_BIS_DATE.getPropertyName());
        Assert.assertNull(SearchFilterType.VON_BIS_DATE.getClassName());
    }

    @Test
    void test_case_47_SearchFilterType_TYPE() {
        Assert.assertNull(SearchFilterType.TYPE.getBezeichnung());
        Assert.assertEquals("", SearchFilterType.TYPE.getPropertyName());
        Assert.assertNull(SearchFilterType.TYPE.getClassName());
    }

    @Test
    void test_case_48_SearchFilterType_SORTING() {
        Assert.assertNull(SearchFilterType.SORTING.getBezeichnung());
        Assert.assertEquals("", SearchFilterType.SORTING.getPropertyName());
        Assert.assertNull(SearchFilterType.SORTING.getClassName());
    }

    @Test
    void test_case_49_SearchFilterType_ROLLE() {
        Assert.assertEquals("Rolle", SearchFilterType.ROLLE.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_rolle", SearchFilterType.ROLLE.getPropertyName());
        Assert.assertNull(SearchFilterType.ROLLE.getClassName());
    }

    @Test
    void test_case_50_SearchFilterType_ABTEILUNG() {
        Assert.assertEquals("Abteilung", SearchFilterType.ABTEILUNG.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_abteilung", SearchFilterType.ABTEILUNG.getPropertyName());
        Assert.assertNull(SearchFilterType.ABTEILUNG.getClassName());
    }

    @Test
    void test_case_51_SearchFilterType_ZAK_EINORDNUNG() {
        Assert.assertEquals("Zak Einordnung", SearchFilterType.ZAK_EINORDNUNG.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_zakEinordung", SearchFilterType.ZAK_EINORDNUNG.getPropertyName());
        Assert.assertNull(SearchFilterType.ZAK_EINORDNUNG.getClassName());
    }

    @Test
    void test_case_52_SearchFilterType_SENSORCOC_SCHREIBEND() {
        Assert.assertEquals("SensorCoc schreibend", SearchFilterType.SENSORCOC_SCHREIBEND.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_sensorcocschreibend", SearchFilterType.SENSORCOC_SCHREIBEND.getPropertyName());
        Assert.assertNull(SearchFilterType.SENSORCOC_SCHREIBEND.getClassName());
    }

    @Test
    void test_case_53_SearchFilterType_SENSORCOC_LESEND() {
        Assert.assertEquals("SensorCoc lesend", SearchFilterType.SENSORCOC_LESEND.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_sensorcoclesend", SearchFilterType.SENSORCOC_LESEND.getPropertyName());
        Assert.assertNull(SearchFilterType.SENSORCOC_LESEND.getClassName());
    }

    @Test
    void test_case_54_SearchFilterType_NURBERECHTIGTE() {
        Assert.assertEquals("Nur Berechtigte Benutzer anzeigen", SearchFilterType.NURBERECHTIGTE.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_berechtigteBenutzer", SearchFilterType.NURBERECHTIGTE.getPropertyName());
        Assert.assertNull(SearchFilterType.NURBERECHTIGTE.getClassName());
    }

    @Test
    void test_case_55_SearchFilterType_WERK() {
        Assert.assertEquals("Werk", SearchFilterType.WERK.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_werk", SearchFilterType.WERK.getPropertyName());
        Assert.assertNull(SearchFilterType.WERK.getClassName());
    }

    @Test
    void test_case_56_SearchFilterType_TECHNOLOGIE() {
        Assert.assertEquals("Technologie", SearchFilterType.TECHNOLOGIE.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_technologie", SearchFilterType.TECHNOLOGIE.getPropertyName());
        Assert.assertNull(SearchFilterType.TECHNOLOGIE.getClassName());
    }

    @Test
    void test_case_57_SearchFilterType_PROZESSBAUKASTEN_STATUS() {
        Assert.assertEquals("Prozessbaukasten Status", SearchFilterType.PROZESSBAUKASTEN_STATUS.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_technologie", SearchFilterType.PROZESSBAUKASTEN_STATUS.getPropertyName());
        Assert.assertNull(SearchFilterType.PROZESSBAUKASTEN_STATUS.getClassName());
    }

    @Test
    void test_case_58_SearchFilterType_SNAPSHOT() {
        Assert.assertEquals("Snapshot", SearchFilterType.SNAPSHOT.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_snapshot", SearchFilterType.SNAPSHOT.getPropertyName());
        Assert.assertNull(SearchFilterType.SNAPSHOT.getClassName());
    }

    @Test
    void test_case_59_SearchFilterType_PROZESSBAUKASTEN() {
        Assert.assertEquals("Prozessbaukasten", SearchFilterType.PROZESSBAUKASTEN.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_prozessbaukasten", SearchFilterType.PROZESSBAUKASTEN.getPropertyName());
        Assert.assertNull(SearchFilterType.PROZESSBAUKASTEN.getClassName());
    }

    @Test
    void test_case_60_SearchFilterType_IN_PROZESSBAUKASTEN() {
        Assert.assertEquals("In Prozessbaukasten", SearchFilterType.IN_PROZESSBAUKASTEN.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_in_prozessbaukasten", SearchFilterType.IN_PROZESSBAUKASTEN.getPropertyName());
        Assert.assertNull(SearchFilterType.IN_PROZESSBAUKASTEN.getClassName());
    }

    @Test
    void test_case_61_SearchFilterType_FACHBEREICH() {
        Assert.assertEquals("Fachbereich", SearchFilterType.FACHBEREICH.getBezeichnung());
        Assert.assertEquals("searchFilterType_propertyName_fachbereich", SearchFilterType.FACHBEREICH.getPropertyName());
        Assert.assertNull(SearchFilterType.FACHBEREICH.getClassName());
    }
}