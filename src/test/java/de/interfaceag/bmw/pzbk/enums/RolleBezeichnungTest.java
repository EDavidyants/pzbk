package de.interfaceag.bmw.pzbk.enums;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class RolleBezeichnungTest {

    @Test
    void roleAdminGetBezeichnungTest() {
        final Rolle rolle = Rolle.ADMIN;
        assertThat(rolle.getBezeichnung(), is("Admin"));
    }

    @Test
    void roleSensorGetBezeichnungTest() {
        final Rolle rolle = Rolle.SENSOR;
        assertThat(rolle.getBezeichnung(), is("Sensor"));
    }

    @Test
    void roleSensorEingeschraenktGetBezeichnungTest() {
        final Rolle rolle = Rolle.SENSOR_EINGESCHRAENKT;
        assertThat(rolle.getBezeichnung(), is("Sensor eingeschränkt"));
    }

    @Test
    void roleSensorCocLeiterGetBezeichnungTest() {
        final Rolle rolle = Rolle.SENSORCOCLEITER;
        assertThat(rolle.getBezeichnung(), is("SensorCoC-Leiter"));
    }

    @Test
    void roleTteamLeiterGetBezeichnungTest() {
        final Rolle rolle = Rolle.T_TEAMLEITER;
        assertThat(rolle.getBezeichnung(), is("T-Teamleiter"));
    }

    @Test
    void roleEcocGetBezeichnungTest() {
        final Rolle rolle = Rolle.E_COC;
        assertThat(rolle.getBezeichnung(), is("E-CoC"));
    }

    @Test
    void roleAnfordererGetBezeichnungTest() {
        final Rolle rolle = Rolle.ANFORDERER;
        assertThat(rolle.getBezeichnung(), is("Anforderer"));
    }

    @Test
    void roleUmsetzungsbestaetigerGetBezeichnungTest() {
        final Rolle rolle = Rolle.UMSETZUNGSBESTAETIGER;
        assertThat(rolle.getBezeichnung(), is("Umsetzungsbestätiger"));
    }

    @Test
    void roleLeserGetBezeichnungTest() {
        final Rolle rolle = Rolle.LESER;
        assertThat(rolle.getBezeichnung(), is("Leser"));
    }

    @Test
    void roleSensorCocVertreterGetBezeichnungTest() {
        final Rolle rolle = Rolle.SCL_VERTRETER;
        assertThat(rolle.getBezeichnung(), is("SensorCoc Vertreter"));
    }

    @Test
    void roleTteamMitgliedGetBezeichnungTest() {
        final Rolle rolle = Rolle.TTEAMMITGLIED;
        assertThat(rolle.getBezeichnung(), is("T-Team Mitglied"));
    }

    @Test
    void roleTteamVertreterGetBezeichnungTest() {
        final Rolle rolle = Rolle.TTEAM_VERTRETER;
        assertThat(rolle.getBezeichnung(), is("T-Team Vertreter"));
    }


}