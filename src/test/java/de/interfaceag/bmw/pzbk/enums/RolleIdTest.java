package de.interfaceag.bmw.pzbk.enums;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class RolleIdTest {

    @Test
    void roleAdminGetIdTest() {
        final Rolle rolle = Rolle.ADMIN;
        assertThat(rolle.getId(), is(0));
    }

    @Test
    void roleSensorGetIdTest() {
        final Rolle rolle = Rolle.SENSOR;
        assertThat(rolle.getId(), is(1));
    }

    @Test
    void roleSensorEingeschraenktGetIdTest() {
        final Rolle rolle = Rolle.SENSOR_EINGESCHRAENKT;
        assertThat(rolle.getId(), is(2));
    }

    @Test
    void roleSensorCocLeiterGetIdTest() {
        final Rolle rolle = Rolle.SENSORCOCLEITER;
        assertThat(rolle.getId(), is(3));
    }

    @Test
    void roleTteamLeiterGetIdTest() {
        final Rolle rolle = Rolle.T_TEAMLEITER;
        assertThat(rolle.getId(), is(4));
    }

    @Test
    void roleEcocGetIdTest() {
        final Rolle rolle = Rolle.E_COC;
        assertThat(rolle.getId(), is(5));
    }

    @Test
    void roleAnfordererGetIdTest() {
        final Rolle rolle = Rolle.ANFORDERER;
        assertThat(rolle.getId(), is(6));
    }

    @Test
    void roleUmsetzungsbestaetigerGetIdTest() {
        final Rolle rolle = Rolle.UMSETZUNGSBESTAETIGER;
        assertThat(rolle.getId(), is(7));
    }

    @Test
    void roleLeserGetIdTest() {
        final Rolle rolle = Rolle.LESER;
        assertThat(rolle.getId(), is(8));
    }

    @Test
    void roleSensorCocVertreterGetIdTest() {
        final Rolle rolle = Rolle.SCL_VERTRETER;
        assertThat(rolle.getId(), is(9));
    }

    @Test
    void roleTteamMitgliedGetIdTest() {
        final Rolle rolle = Rolle.TTEAMMITGLIED;
        assertThat(rolle.getId(), is(10));
    }

    @Test
    void roleTteamVertreterGetIdTest() {
        final Rolle rolle = Rolle.TTEAM_VERTRETER;
        assertThat(rolle.getId(), is(11));
    }


}