package de.interfaceag.bmw.pzbk.enums;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class ApplicationEnvironmentTest {

    @ParameterizedTest
    @ValueSource(strings = {"production", "PRODUCTION", "PROductioN"})
    void testStringToAppModeEnumProduction(String input) {
        final ApplicationEnvironment result = ApplicationEnvironment.getByName(input);
        MatcherAssert.assertThat(result, CoreMatchers.is(ApplicationEnvironment.PRODUCTION));
    }

    @ParameterizedTest
    @ValueSource(strings = {"development", "DEVELOPMENT", "DEVElopMenT"})
    void testStringToAppModeEnumDevelopment(String input) {
        final ApplicationEnvironment result = ApplicationEnvironment.getByName(input);
        MatcherAssert.assertThat(result, CoreMatchers.is(ApplicationEnvironment.DEVELOPMENT));
    }

    @ParameterizedTest
    @ValueSource(strings = {"dev", "", "Lorenz!"})
    void testStringToAppModeEnumDefault(String input) {
        final ApplicationEnvironment result = ApplicationEnvironment.getByName(input);
        MatcherAssert.assertThat(result, CoreMatchers.is(ApplicationEnvironment.DEVELOPMENT));
    }

    @Test
    void testStringToAppModeEnumNullInput() {
        final ApplicationEnvironment result = ApplicationEnvironment.getByName(null);
        MatcherAssert.assertThat(result, CoreMatchers.is(ApplicationEnvironment.DEVELOPMENT));
    }


    @Test
    void productionIsProduction() {
        final boolean result = ApplicationEnvironment.PRODUCTION.isProduction();
        Assertions.assertTrue(result);
    }

    @Test
    void productionIsDevelopment() {
        final boolean result = ApplicationEnvironment.PRODUCTION.isDevelopment();
        Assertions.assertFalse(result);
    }

    @Test
    void developmentIsProduction() {
        final boolean result = ApplicationEnvironment.PRODUCTION.isDevelopment();
        Assertions.assertFalse(result);
    }

    @Test
    void developmentIsDevelopment() {
        final boolean result = ApplicationEnvironment.DEVELOPMENT.isDevelopment();
        Assertions.assertTrue(result);
    }
}
