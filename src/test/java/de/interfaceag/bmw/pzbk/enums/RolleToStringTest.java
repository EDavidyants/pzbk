package de.interfaceag.bmw.pzbk.enums;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class RolleToStringTest {

    @Test
    void roleAdminToStringTest() {
        final Rolle rolle = Rolle.ADMIN;
        assertThat(rolle.toString(), is("Admin"));
    }

    @Test
    void roleSensorToStringTest() {
        final Rolle rolle = Rolle.SENSOR;
        assertThat(rolle.toString(), is("Sensor"));
    }

    @Test
    void roleSensorEingeschraenktToStringTest() {
        final Rolle rolle = Rolle.SENSOR_EINGESCHRAENKT;
        assertThat(rolle.toString(), is("Sensor eingeschränkt"));
    }

    @Test
    void roleSensorCocLeiterToStringTest() {
        final Rolle rolle = Rolle.SENSORCOCLEITER;
        assertThat(rolle.toString(), is("SensorCoC-Leiter"));
    }

    @Test
    void roleTteamLeiterToStringTest() {
        final Rolle rolle = Rolle.T_TEAMLEITER;
        assertThat(rolle.toString(), is("T-Teamleiter"));
    }

    @Test
    void roleEcocToStringTest() {
        final Rolle rolle = Rolle.E_COC;
        assertThat(rolle.toString(), is("E-CoC"));
    }

    @Test
    void roleAnfordererToStringTest() {
        final Rolle rolle = Rolle.ANFORDERER;
        assertThat(rolle.toString(), is("Anforderer"));
    }

    @Test
    void roleUmsetzungsbestaetigerToStringTest() {
        final Rolle rolle = Rolle.UMSETZUNGSBESTAETIGER;
        assertThat(rolle.toString(), is("Umsetzungsbestätiger"));
    }

    @Test
    void roleLeserToStringTest() {
        final Rolle rolle = Rolle.LESER;
        assertThat(rolle.toString(), is("Leser"));
    }

    @Test
    void roleSensorCocVertreterToStringTest() {
        final Rolle rolle = Rolle.SCL_VERTRETER;
        assertThat(rolle.toString(), is("SensorCoc Vertreter"));
    }

    @Test
    void roleTteamMitgliedToStringTest() {
        final Rolle rolle = Rolle.TTEAMMITGLIED;
        assertThat(rolle.toString(), is("T-Team Mitglied"));
    }

    @Test
    void roleTteamVertreterToStringTest() {
        final Rolle rolle = Rolle.TTEAM_VERTRETER;
        assertThat(rolle.toString(), is("T-Team Vertreter"));
    }


}