package de.interfaceag.bmw.pzbk.enums;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class KategorieTest {

    @Test
    public void testKategorieEinsId() {
        int id = Kategorie.EINS.getId();
        Assertions.assertEquals(1, id, "Id should always be 1!");
    }

    @Test
    public void testKategorieEinsGetById() {
        Kategorie kategorie = Kategorie.getById(1);
        Assertions.assertEquals(Kategorie.EINS, kategorie);
    }

    @Test
    public void testKategorieZweiId() {
        int id = Kategorie.ZWEI.getId();
        Assertions.assertEquals(2, id, "Id should always be 2!");
    }

    @Test
    public void testKategorieZweiGetById() {
        Kategorie kategorie = Kategorie.getById(2);
        Assertions.assertEquals(Kategorie.ZWEI, kategorie);
    }

    @Test
    public void testKategorieToString() {
        String name = Kategorie.EINS.toString();
        Assertions.assertEquals("EINS", name);
    }

}
