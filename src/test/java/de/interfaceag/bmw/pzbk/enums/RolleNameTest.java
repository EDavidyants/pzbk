package de.interfaceag.bmw.pzbk.enums;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class RolleNameTest {

    @Test
    void roleAdminNameTest() {
        final Rolle rolle = Rolle.ADMIN;
        assertThat(rolle.name(), is("ADMIN"));
    }

    @Test
    void roleSensorNameTest() {
        final Rolle rolle = Rolle.SENSOR;
        assertThat(rolle.name(), is("SENSOR"));
    }

    @Test
    void roleSensorEingeschraenktNameTest() {
        final Rolle rolle = Rolle.SENSOR_EINGESCHRAENKT;
        assertThat(rolle.name(), is("SENSOR_EINGESCHRAENKT"));
    }

    @Test
    void roleSensorCocLeiterNameTest() {
        final Rolle rolle = Rolle.SENSORCOCLEITER;
        assertThat(rolle.name(), is("SENSORCOCLEITER"));
    }

    @Test
    void roleTteamLeiterNameTest() {
        final Rolle rolle = Rolle.T_TEAMLEITER;
        assertThat(rolle.name(), is("T_TEAMLEITER"));
    }

    @Test
    void roleEcocNameTest() {
        final Rolle rolle = Rolle.E_COC;
        assertThat(rolle.name(), is("E_COC"));
    }

    @Test
    void roleAnfordererNameTest() {
        final Rolle rolle = Rolle.ANFORDERER;
        assertThat(rolle.name(), is("ANFORDERER"));
    }

    @Test
    void roleUmsetzungsbestaetigerNameTest() {
        final Rolle rolle = Rolle.UMSETZUNGSBESTAETIGER;
        assertThat(rolle.name(), is("UMSETZUNGSBESTAETIGER"));
    }

    @Test
    void roleLeserNameTest() {
        final Rolle rolle = Rolle.LESER;
        assertThat(rolle.name(), is("LESER"));
    }

    @Test
    void roleSensorCocVertreterNameTest() {
        final Rolle rolle = Rolle.SCL_VERTRETER;
        assertThat(rolle.name(), is("SCL_VERTRETER"));
    }

    @Test
    void roleTteamMitgliedNameTest() {
        final Rolle rolle = Rolle.TTEAMMITGLIED;
        assertThat(rolle.name(), is("TTEAMMITGLIED"));
    }

    @Test
    void roleTteamVertreterNameTest() {
        final Rolle rolle = Rolle.TTEAM_VERTRETER;
        assertThat(rolle.name(), is("TTEAM_VERTRETER"));
    }


}