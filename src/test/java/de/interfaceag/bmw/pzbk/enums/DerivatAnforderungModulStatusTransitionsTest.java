package de.interfaceag.bmw.pzbk.enums;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DerivatAnforderungModulStatusTransitionsTest {

    @Test
    public void AbzustimmenStatusTransitionsTest() {
        DerivatAnforderungModulStatus status = DerivatAnforderungModulStatus.ABZUSTIMMEN;
        List<DerivatAnforderungModulStatus> statusTransitions = DerivatAnforderungModulStatus.getNextStatusListByStatus(status);
        Assert.assertEquals(5, statusTransitions.size());
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.ANGENOMMEN));
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.IN_KLAERUNG));
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.NICHT_BEWERTBAR));
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET));
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG));
    }

    @Test
    public void AngenommenStatusTransitionsTest() {
        DerivatAnforderungModulStatus status = DerivatAnforderungModulStatus.ANGENOMMEN;
        List<DerivatAnforderungModulStatus> statusTransitions = DerivatAnforderungModulStatus.getNextStatusListByStatus(status);
        Assert.assertEquals(5, statusTransitions.size());
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.ABZUSTIMMEN));
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.NICHT_BEWERTBAR));
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.IN_KLAERUNG));
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG));
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET));
    }

    @Test
    public void NichtBewertbarStatusTransitionsTest() {
        DerivatAnforderungModulStatus status = DerivatAnforderungModulStatus.NICHT_BEWERTBAR;
        List<DerivatAnforderungModulStatus> statusTransitions = DerivatAnforderungModulStatus.getNextStatusListByStatus(status);
        Assert.assertEquals(3, statusTransitions.size());
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.IN_KLAERUNG));
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET));
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG));
    }

    @Test
    public void KeineWeiterverfolgungStatusTransitionsTest() {
        DerivatAnforderungModulStatus status = DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG;
        List<DerivatAnforderungModulStatus> statusTransitions = DerivatAnforderungModulStatus.getNextStatusListByStatus(status);
        Assert.assertEquals(2, statusTransitions.size());
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.ABZUSTIMMEN));
        Assert.assertTrue(statusTransitions.contains(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET));
    }

}
