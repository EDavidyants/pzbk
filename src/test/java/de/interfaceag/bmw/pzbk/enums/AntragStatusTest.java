package de.interfaceag.bmw.pzbk.enums;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AntragStatusTest {

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testAntragStatusId(AntragStatus status) {
        int id = status.getId();
        switch (status) {
            case OFFEN:
                Assertions.assertEquals(0, id);
                break;
            case ANGENOMMEN:
                Assertions.assertEquals(1, id);
                break;
            case ABGELEHNT:
                Assertions.assertEquals(2, id);
                break;
            default:
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testAntragStatusBezeichnung(AntragStatus status) {
        String bezeichnung = status.getBezeichnung();
        switch (status) {
            case OFFEN:
                Assertions.assertEquals("Offen", bezeichnung);
                break;
            case ANGENOMMEN:
                Assertions.assertEquals("Angenommen", bezeichnung);
                break;
            case ABGELEHNT:
                Assertions.assertEquals("Abgelehnt", bezeichnung);
                break;
            default:
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testAntragStatusToString(AntragStatus status) {
        String toString = status.toString();
        switch (status) {
            case OFFEN:
                Assertions.assertEquals("Offen", toString);
                break;
            case ANGENOMMEN:
                Assertions.assertEquals("Angenommen", toString);
                break;
            case ABGELEHNT:
                Assertions.assertEquals("Abgelehnt", toString);
                break;
            default:
                break;
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3})
    public void testGetById(int id) {
        AntragStatus status = AntragStatus.getById(id);
        switch (id) {
            case 0:
                Assertions.assertEquals(AntragStatus.OFFEN, status);
                break;
            case 1:
                Assertions.assertEquals(AntragStatus.ANGENOMMEN, status);
                break;
            case 2:
                Assertions.assertEquals(AntragStatus.ABGELEHNT, status);
                break;
            case 3:
                Assertions.assertNull(status);
                break;
            default:
                Assertions.assertNull(status);
                break;
        }
    }
}
