package de.interfaceag.bmw.pzbk.enums;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class RolleRawStringTest {

    @Test
    void roleAdminGetRolleRawStringTest() {
        final Rolle rolle = Rolle.ADMIN;
        assertThat(rolle.getRolleRawString(), is("ADMIN"));
    }

    @Test
    void roleSensorGetRolleRawStringTest() {
        final Rolle rolle = Rolle.SENSOR;
        assertThat(rolle.getRolleRawString(), is("SENSOR"));
    }

    @Test
    void roleSensorEingeschraenktGetRolleRawStringTest() {
        final Rolle rolle = Rolle.SENSOR_EINGESCHRAENKT;
        assertThat(rolle.getRolleRawString(), is("SENSOR_EINGESCHRAENKT"));
    }

    @Test
    void roleSensorCocLeiterGetRolleRawStringTest() {
        final Rolle rolle = Rolle.SENSORCOCLEITER;
        assertThat(rolle.getRolleRawString(), is("SENSORCOCLEITER"));
    }

    @Test
    void roleTteamLeiterGetRolleRawStringTest() {
        final Rolle rolle = Rolle.T_TEAMLEITER;
        assertThat(rolle.getRolleRawString(), is("T_TEAMLEITER"));
    }

    @Test
    void roleEcocGetRolleRawStringTest() {
        final Rolle rolle = Rolle.E_COC;
        assertThat(rolle.getRolleRawString(), is("E_COC"));
    }

    @Test
    void roleAnfordererGetRolleRawStringTest() {
        final Rolle rolle = Rolle.ANFORDERER;
        assertThat(rolle.getRolleRawString(), is("ANFORDERER"));
    }

    @Test
    void roleUmsetzungsbestaetigerGetRolleRawStringTest() {
        final Rolle rolle = Rolle.UMSETZUNGSBESTAETIGER;
        assertThat(rolle.getRolleRawString(), is("UMSETZUNGSBESTAETIGER"));
    }

    @Test
    void roleLeserGetRolleRawStringTest() {
        final Rolle rolle = Rolle.LESER;
        assertThat(rolle.getRolleRawString(), is("LESER"));
    }

    @Test
    void roleSensorCocVertreterGetRolleRawStringTest() {
        final Rolle rolle = Rolle.SCL_VERTRETER;
        assertThat(rolle.getRolleRawString(), is("SCL_VERTRETER"));
    }

    @Test
    void roleTteamMitgliedGetRolleRawStringTest() {
        final Rolle rolle = Rolle.TTEAMMITGLIED;
        assertThat(rolle.getRolleRawString(), is("TTEAMMITGLIED"));
    }

    @Test
    void roleTteamVertreterGetRolleRawStringTest() {
        final Rolle rolle = Rolle.TTEAM_VERTRETER;
        assertThat(rolle.getRolleRawString(), is("TTEAM_VERTRETER"));
    }


}