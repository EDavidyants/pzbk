package de.interfaceag.bmw.pzbk.enums;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RolleTest {

    @Test
    void getRolleByBezeichnungValidInput() {
        final Rolle rolle = Rolle.getRolleByName("ADMIN");
        assertThat(rolle, is(Rolle.ADMIN));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "Admin", "ADMIN ", " ADMIN"})
    void getRolleByBezeichnungInvalidInput(String input) {
        final Rolle rolle = Rolle.getRolleByName(input);
        assertThat(rolle, is(nullValue()));
    }

    @Test
    void getRolleByBezeichnungNullInput() {
        final Rolle rolle = Rolle.getRolleByName(null);
        assertThat(rolle, is(nullValue()));
    }

    @Test
    void getRolleByRawStringValidInput() {
        final Rolle rolle = Rolle.getRolleByRawString("ADMIN");
        assertThat(rolle, is(Rolle.ADMIN));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "Admin", "ADMIN ", " ADMIN"})
    void getRolleByRawStringInvalidInput(String input) {
        final Rolle rolle = Rolle.getRolleByRawString(input);
        assertThat(rolle, is(nullValue()));
    }

    @Test
    void getRolleByRawStringNullInput() {
        final Rolle rolle = Rolle.getRolleByRawString(null);
        assertThat(rolle, is(nullValue()));
    }

    @Test
    void getRolleByIdValidInput() {
        final Rolle rolle = Rolle.getRolleById(0);
        assertThat(rolle, is(Rolle.ADMIN));
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 12, 15})
    void getRolleByIdInvalidInput(int input) {
        assertThrows(IllegalArgumentException.class, () -> Rolle.getRolleById(input));
    }


    @Test
    void getAllRolles() {
        final List<Rolle> allRolles = Rolle.getAllRolles();
        assertThat(allRolles, hasSize(12));
    }
}