package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class DerivatAnforderungModulIdFilterTest {

    @Mock
    HttpServletRequest request;

    @InjectMocks
    private UrlParameter urlParameter;

    private Map<String, String[]> parameterMap;

    private DerivatAnforderungModulIdFilter filter;

    @BeforeEach
    public void setUp() {
        parameterMap = new HashMap<>();
    }

    @Test
    public void testParameterName() {
        filter = new DerivatAnforderungModulIdFilter(urlParameter);
        String parameterName = filter.getParameterName();
        Assertions.assertEquals("daid", parameterName);
    }

    @Test
    public void testIsActivePositive() {
        parameterMap.put("daid", new String[]{"756"});
        when(request.getParameterMap()).thenReturn(parameterMap);
        urlParameter = new UrlParameter(request);
        filter = new DerivatAnforderungModulIdFilter(urlParameter);
        boolean isActive = filter.isActive();
        Assertions.assertTrue(isActive);
    }

    @Test
    public void testIsActiveNegative() {
        when(request.getParameterMap()).thenReturn(parameterMap);
        urlParameter = new UrlParameter(request);
        filter = new DerivatAnforderungModulIdFilter(urlParameter);
        boolean isActive = filter.isActive();
        Assertions.assertFalse(isActive);
    }

    @Test
    public void testGetIdsForEmptyList() {
        when(request.getParameterMap()).thenReturn(parameterMap);
        urlParameter = new UrlParameter(request);
        filter = new DerivatAnforderungModulIdFilter(urlParameter);
        Assertions.assertTrue(filter.getIds().isEmpty());
    }

    @Test
    public void testGetIdsWithOneElement() {
        parameterMap.put("daid", new String[]{"756"});
        when(request.getParameterMap()).thenReturn(parameterMap);
        urlParameter = new UrlParameter(request);
        filter = new DerivatAnforderungModulIdFilter(urlParameter);
        Set<Long> ids = filter.getIds();
        Assertions.assertEquals(1, ids.size());
        Assertions.assertTrue(ids.contains(756L));
    }

    @Test
    public void testGetIdsWithTwoElementsForParameterOne() {
        parameterMap.put("daid", new String[]{"756,757"});
        when(request.getParameterMap()).thenReturn(parameterMap);
        urlParameter = new UrlParameter(request);
        filter = new DerivatAnforderungModulIdFilter(urlParameter);
        Set<Long> ids = filter.getIds();
        Assertions.assertEquals(2, ids.size());
        Assertions.assertTrue(ids.contains(756L));
        Assertions.assertTrue(ids.contains(757L));
    }

    @Test
    public void testGetIdsWithTwoElementsForParameterTwo() {
        parameterMap.put("daid", new String[]{"756, 757"});
        when(request.getParameterMap()).thenReturn(parameterMap);
        urlParameter = new UrlParameter(request);
        filter = new DerivatAnforderungModulIdFilter(urlParameter);
        Set<Long> ids = filter.getIds();
        Assertions.assertEquals(2, ids.size());
        Assertions.assertTrue(ids.contains(756L));
        Assertions.assertTrue(ids.contains(757L));
    }

    @Test
    public void testGetIdsWithTwoElementsForParameterThree() {
        parameterMap.put("daid", new String[]{"756, 757 "});
        when(request.getParameterMap()).thenReturn(parameterMap);
        urlParameter = new UrlParameter(request);
        filter = new DerivatAnforderungModulIdFilter(urlParameter);
        Set<Long> ids = filter.getIds();
        Assertions.assertEquals(2, ids.size());
        Assertions.assertTrue(ids.contains(756L));
        Assertions.assertTrue(ids.contains(757L));
    }

    @Test
    public void testGetIdsWithTwoElementsForParameterFour() {
        parameterMap.put("daid", new String[]{"756 + 757 "});
        when(request.getParameterMap()).thenReturn(parameterMap);
        urlParameter = new UrlParameter(request);
        filter = new DerivatAnforderungModulIdFilter(urlParameter);
        Set<Long> ids = filter.getIds();
        Assertions.assertEquals(2, ids.size());
        Assertions.assertTrue(ids.contains(756L));
        Assertions.assertTrue(ids.contains(757L));
    }

    @Test
    public void testGetIdsWithTwoElementsForParameterFive() {
        parameterMap.put("daid", new String[]{" 756 + 757 "});
        when(request.getParameterMap()).thenReturn(parameterMap);
        urlParameter = new UrlParameter(request);
        filter = new DerivatAnforderungModulIdFilter(urlParameter);
        Set<Long> ids = filter.getIds();
        Assertions.assertEquals(2, ids.size());
        Assertions.assertTrue(ids.contains(756L));
        Assertions.assertTrue(ids.contains(757L));
    }

}
