package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author sl
 */
@RunWith(MockitoJUnitRunner.class)
public class FilterToUrlConverterTest {

    private ReportingFilter filter;

    @Mock
    private UrlParameter urlParameter;

    @Before
    public void setup() {
        filter = getReportingFilter();
    }

    @Test
    public void testGetUrl() throws Exception {
        FilterToUrlConverter filterToUrlConverter = FilterToUrlConverter.forClass(filter);
        String result = filterToUrlConverter.getUrl();
        String expected = getExpected();
        assertEquals(expected, result);
    }

    private static String getExpected() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String date = dateFormat.format(new Date());
        return "?faces-redirect=true&vonDateFilter=" + date + "&bisDateFilter=" + date;
    }

    private ReportingFilter getReportingFilter() {
        return new ReportingFilter(Page.SUCHE, urlParameter, Collections.emptyList(),
                Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), new Date(), Collections.emptyList());
    }
}
