package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

class SearchFilterObjectFactoryProzessbaukastenTest {

    private ProzessbaukastenFilterDto prozessbaukastenFilterDto;
    private Collection<ProzessbaukastenFilterDto> prozessbaukasten;

    @BeforeEach
    void setup() {
        prozessbaukastenFilterDto = new ProzessbaukastenFilterDto(1L, "P123", 12, "Bezeichnung");
        prozessbaukasten = Collections.singletonList(prozessbaukastenFilterDto);
    }

    @Test
    void getSearchFilterObjectsForProzessbaukastenResultSize() {
        final List<SearchFilterObject> result = SearchFilterObjectFactory.getSearchFilterObjectsForProzessbaukasten(prozessbaukasten);
        assertThat(result, hasSize(1));
    }

    @Test
    void getSearchFilterObjectsForProzessbaukastenResultContains() {
        final List<SearchFilterObject> result = SearchFilterObjectFactory.getSearchFilterObjectsForProzessbaukasten(prozessbaukasten);
        assertThat(result, IsIterableContaining.hasItem(new SearchFilterObject(prozessbaukastenFilterDto.getId(), prozessbaukastenFilterDto.toString())));
    }
}