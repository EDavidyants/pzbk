package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class SelectedDerivatUrlFilterTest {

    UrlFilter urlFilter;

    @Mock
    UrlParameter urlParameter;

    @BeforeEach
    public void setUp() {
    }

    @Test
    public void testDefaultParamterName() {
        urlFilter = new SelectedDerivatUrlFilter(urlParameter);
        String parameterName = urlFilter.getParameterName();
        MatcherAssert.assertThat("selectedDerivat", is(parameterName));
    }

    @Test
    public void testCustomParamterName() {
        String expected = "derivat";
        urlFilter = SelectedDerivatUrlFilter.withParamterName(expected).withUrlParameter(urlParameter).build();
        String parameterName = urlFilter.getParameterName();
        MatcherAssert.assertThat(expected, is(parameterName));
    }

}
