package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.SearchFilterType;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
class ThemenklammerFilterTest {

    @Mock
    private UrlParameter urlParameter;

    private ThemenklammerFilter themenklammerFilter;
    private Collection<ThemenklammerDto> themenklammern;

    @BeforeEach
    void setUp() {
        themenklammerFilter = new ThemenklammerFilter(themenklammern, urlParameter);
    }

    @Test
    void getType() {
        final SearchFilterType themenklammerFilterType = themenklammerFilter.getType();
        assertThat(themenklammerFilterType, is(SearchFilterType.THEMENKLAMMER));
    }

    @Test
    void getParameterName() {
        final String parameterName = themenklammerFilter.getParameterName();
        assertThat(parameterName, is("themenklammerFilter"));
    }
}