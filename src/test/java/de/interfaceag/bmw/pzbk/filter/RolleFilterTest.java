package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author fn
 */
public class RolleFilterTest {

    private HttpServletRequest request;
    private UrlParameter urlParameter;

    @Before
    public void setUp() {

    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testRolleFilterDefaultConstructor(Rolle role) {
        request = mock(HttpServletRequest.class);
        when(request.getParameterMap()).thenReturn(new HashMap<>());
        urlParameter = new UrlParameter(request);

        RolleFilter rolleFilter = new RolleFilter(urlParameter);
        boolean result = false;
        for (SearchFilterObject searchFilterObject : rolleFilter.getAll()) {
            if (searchFilterObject.getName().equals(role.getBezeichnung())) {
                result = true;
            }
        }

        switch (role) {

            default:
                Assertions.assertTrue(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testRolleFilterForRoleMap(Rolle role) {
        request = mock(HttpServletRequest.class);
        when(request.getParameterMap()).thenReturn(new HashMap<>());
        urlParameter = new UrlParameter(request);

        List<Rolle> rolle = new ArrayList<>();
        rolle.add(Rolle.ADMIN);
        rolle.add(Rolle.TTEAMMITGLIED);
        rolle.add(Rolle.SCL_VERTRETER);

        RolleFilter rolleFilter = new RolleFilter(urlParameter, rolle);
        boolean result = false;
        for (SearchFilterObject searchFilterObject : rolleFilter.getAll()) {
            if (searchFilterObject.getName().equals(role.getBezeichnung())) {
                result = true;
            }
        }

        switch (role) {
            case ADMIN:
            case TTEAMMITGLIED:
            case SCL_VERTRETER:
                Assertions.assertTrue(result);
                break;
            default:
                Assertions.assertFalse(result);
                break;
        }
    }

}
