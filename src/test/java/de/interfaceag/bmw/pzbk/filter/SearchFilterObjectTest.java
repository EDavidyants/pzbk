package de.interfaceag.bmw.pzbk.filter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class SearchFilterObjectTest {

    private static final String NAME_1 = "Name1";
    private static final String NAME_2 = "Name2";
    private SearchFilterObject searchFilterObject1;
    private SearchFilterObject searchFilterObject2;

    @BeforeEach
    void setUp() {
        searchFilterObject1 = new SearchFilterObject(1L, NAME_1);
        searchFilterObject2 = new SearchFilterObject(1L, NAME_2);
    }

    @Test
    void compareToNull() {
        final int result = searchFilterObject1.compareTo(null);
        assertThat(result, is(1));
    }

    @Test
    void compareToNullName1() {
        searchFilterObject1 = new SearchFilterObject(1L, null);
        final int result = searchFilterObject1.compareTo(searchFilterObject2);
        assertThat(result, is(-1));
    }

    @Test
    void compareToNullName2() {
        searchFilterObject2 = new SearchFilterObject(1L, null);
        final int result = searchFilterObject1.compareTo(searchFilterObject2);
        assertThat(result, is(1));
    }

    @Test
    void compareTo() {
        final int result = searchFilterObject1.compareTo(searchFilterObject2);
        final int expectedResult = NAME_1.compareTo(NAME_2);
        assertThat(result, is(expectedResult));
    }
}