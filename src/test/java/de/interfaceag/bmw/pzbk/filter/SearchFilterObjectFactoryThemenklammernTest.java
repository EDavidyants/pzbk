package de.interfaceag.bmw.pzbk.filter;

import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

class SearchFilterObjectFactoryThemenklammernTest {

    private Collection<ThemenklammerDto> themenklammern;

    @Test
    void getSearchFilterObjectsForThemenklammernResultSize() {
        themenklammern = Collections.singletonList(new ThemenklammerDto(42L, "Bezeichnung"));
        final List<SearchFilterObject> result = SearchFilterObjectFactory.getSearchFilterObjectsForThemenklammern(themenklammern);
        assertThat(result, hasSize(1));
    }

    @Test
    void getSearchFilterObjectsForThemenklammernResultValue() {
        themenklammern = Collections.singletonList(new ThemenklammerDto(42L, "Bezeichnung"));
        final List<SearchFilterObject> result = SearchFilterObjectFactory.getSearchFilterObjectsForThemenklammern(themenklammern);
        assertThat(result, IsIterableContaining.hasItem(new SearchFilterObject(42L, "Bezeichnung")));
    }
}