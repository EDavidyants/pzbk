package de.interfaceag.bmw.pzbk.filter.dto;

import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class ProzessbaukastenFilterDtoTest {

    private Prozessbaukasten prozessbaukasten;
    private ProzessbaukastenFilterDto prozessbaukastenFilterDto;

    @BeforeEach
    void setUp() {
        prozessbaukasten = TestDataFactory.generateProzessbaukasten();
        prozessbaukastenFilterDto = ProzessbaukastenFilterDto.forProzessbaukasten(prozessbaukasten);
    }

    @Test
    void getId() {
        final Long id = prozessbaukastenFilterDto.getId();
        assertThat(id, is(prozessbaukasten.getId()));
    }

    @Test
    void toString1() {
        final String result = prozessbaukastenFilterDto.toString();
        assertThat(result, is("P1234 | V1: Test Bezeichnung"));
    }
}