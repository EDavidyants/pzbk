package de.interfaceag.bmw.pzbk.staticvalidators;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

/**
 *
 * @author fn
 */
public class AnforderungValidatorTest {

    @Test
    public void validatCorrectAnforderung() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();

        Assertions.assertTrue(AnforderungValidator.validatAnforderung(anforderung));

    }

    @Test
    public void validatNullAnforderung() {
        Anforderung anforderung = null;

        Assertions.assertFalse(AnforderungValidator.validatAnforderung(anforderung));
    }

    @Test
    public void validatAnforderungWithNullId() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        anforderung.setId(null);
        Assertions.assertFalse(AnforderungValidator.validatAnforderung(anforderung));
    }

    @Test
    public void validatAnforderungwithNullFachId() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        anforderung.setFachId(null);
        Assertions.assertFalse(AnforderungValidator.validatAnforderung(anforderung));
    }

    @Test
    public void validatAnforderungWithNullVersion() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        anforderung.setVersion(null);
        Assertions.assertFalse(AnforderungValidator.validatAnforderung(anforderung));
    }
}
