package de.interfaceag.bmw.pzbk.staticvalidators;

import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

/**
 *
 * @author fn
 */
public class ProzessbaukastenValidatorTest {

    @Test
    public void validatCorrectAnforderung() {
        Prozessbaukasten prozessbaukasten = TestDataFactory.generateProzessbaukasten();

        Assertions.assertTrue(ProzessbaukastenValidator.validatProzessbaukasten(prozessbaukasten));

    }

    @Test
    public void validatNullAnforderung() {
        Prozessbaukasten prozessbaukasten = null;

        Assertions.assertFalse(ProzessbaukastenValidator.validatProzessbaukasten(prozessbaukasten));
    }

    @Test
    public void validatAnforderungWithNullId() {
        Prozessbaukasten prozessbaukasten = TestDataFactory.generateProzessbaukasten();
        prozessbaukasten.setId(null);
        Assertions.assertFalse(ProzessbaukastenValidator.validatProzessbaukasten(prozessbaukasten));
    }

    @Test
    public void validatAnforderungwithNullFachId() {
        Prozessbaukasten prozessbaukasten = TestDataFactory.generateProzessbaukasten();
        prozessbaukasten.setFachId(null);
        Assertions.assertFalse(ProzessbaukastenValidator.validatProzessbaukasten(prozessbaukasten));
    }

    @Test
    public void validatAnforderungWithNullVersion() {
        Prozessbaukasten prozessbaukasten = TestDataFactory.generateProzessbaukasten();
        prozessbaukasten.setVersion(0);
        Assertions.assertFalse(ProzessbaukastenValidator.validatProzessbaukasten(prozessbaukasten));
    }
}
