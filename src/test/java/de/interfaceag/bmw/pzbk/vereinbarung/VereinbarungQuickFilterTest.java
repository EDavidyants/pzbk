package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.DerivatVereinbarungStatusFilter;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VereinbarungQuickFilterTest {

    private static final long DERIVAT_ID = 1L;
    private static final String DERIVAT_NAME = "DERIVAT";
    private DerivatVereinbarungStatusFilter derivatVereinbarungStatusFilter;

    @Mock
    private VereinbarungViewFilter filter;
    @Mock
    private Derivat derivat;
    @Mock
    private UrlParameter urlParameter;

    @BeforeEach
    void setup() {
        derivatVereinbarungStatusFilter = new DerivatVereinbarungStatusFilter(DERIVAT_ID, DERIVAT_NAME, DerivatStatus.VEREINARBUNG_VKBG, urlParameter);
        List<DerivatVereinbarungStatusFilter> derivatVereinbarungStatusFilters = new ArrayList<>();
        derivatVereinbarungStatusFilters.add(derivatVereinbarungStatusFilter);

        when(derivat.getId()).thenReturn(DERIVAT_ID);
        when(filter.getDerivatVereinbarungStatusFilter()).thenReturn(derivatVereinbarungStatusFilters);
    }

    @Test
    void applyVereinbarenModeQuickFilter() {
        VereinbarungQuickFilter.applyVereinbarenModeQuickFilter(derivat, filter);
        MultiValueEnumSearchFilter<DerivatAnforderungModulStatus> vereinbarungStatusFilter = derivatVereinbarungStatusFilter.getVereinbarungStatusFilter();
        List<DerivatAnforderungModulStatus> vereinbarungen = vereinbarungStatusFilter.getAsEnumList();
        MatcherAssert.assertThat(vereinbarungen, IsIterableContaining.hasItems(DerivatAnforderungModulStatus.ABZUSTIMMEN));
    }


    @Test
    void applyFolgeprozessZakQuickFilterVereinbarungStatusFilter() {
        VereinbarungQuickFilter.applyFolgeprozessZakQuickFilter(derivat, filter);
        MultiValueEnumSearchFilter<DerivatAnforderungModulStatus> vereinbarungStatusFilter = derivatVereinbarungStatusFilter.getVereinbarungStatusFilter();
        List<DerivatAnforderungModulStatus> vereinbarungen = vereinbarungStatusFilter.getAsEnumList();
        MatcherAssert.assertThat(vereinbarungen, IsIterableContaining.hasItems(DerivatAnforderungModulStatus.ANGENOMMEN));
    }

    @Test
    void applyFolgeprozessZakQuickFilterZakStatusFilter() {
        VereinbarungQuickFilter.applyFolgeprozessZakQuickFilter(derivat, filter);
        MultiValueEnumSearchFilter<ZakStatus> vereinbarungStatusFilter = derivatVereinbarungStatusFilter.getZakStatusFilter();
        List<ZakStatus> zakStatuses = vereinbarungStatusFilter.getAsEnumList();
        MatcherAssert.assertThat(zakStatuses, IsIterableContaining.hasItems(ZakStatus.NICHT_UMSETZBAR, ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER, ZakStatus.NICHT_UMSETZBAR));
    }

    @Test
    void applyFolgeprozessZakKeineWeiterverfolungQuickFilterVereinbarungStatusFilter() {
        VereinbarungQuickFilter.applyFolgeprozessZakKeineWeiterverfolungQuickFilter(derivat, filter);
        MultiValueEnumSearchFilter<DerivatAnforderungModulStatus> vereinbarungStatusFilter = derivatVereinbarungStatusFilter.getVereinbarungStatusFilter();
        List<DerivatAnforderungModulStatus> vereinbarungen = vereinbarungStatusFilter.getAsEnumList();
        MatcherAssert.assertThat(vereinbarungen, IsIterableContaining.hasItems(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG));
    }

    @Test
    void applyFolgeprozessZakKeineWeiterverfolungQuickFilterZakStatusFilter() {
        VereinbarungQuickFilter.applyFolgeprozessZakKeineWeiterverfolungQuickFilter(derivat, filter);
        MultiValueEnumSearchFilter<ZakStatus> vereinbarungStatusFilter = derivatVereinbarungStatusFilter.getZakStatusFilter();
        List<ZakStatus> zakStatuses = vereinbarungStatusFilter.getAsEnumList();
        MatcherAssert.assertThat(zakStatuses, IsIterableContaining.hasItems(ZakStatus.NICHT_UMSETZBAR, ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER, ZakStatus.NICHT_UMSETZBAR));
    }

    @Test
    void applyFolgeprozessUmsetzungsverwaltungQuickFilterVereinbarungStatusFilter() {
        VereinbarungQuickFilter.applyFolgeprozessUmsetzungsverwaltungQuickFilter(derivat, filter);
        MultiValueEnumSearchFilter<DerivatAnforderungModulStatus> vereinbarungStatusFilter = derivatVereinbarungStatusFilter.getVereinbarungStatusFilter();
        List<DerivatAnforderungModulStatus> vereinbarungen = vereinbarungStatusFilter.getAsEnumList();
        MatcherAssert.assertThat(vereinbarungen, IsIterableContaining.hasItems(DerivatAnforderungModulStatus.ANGENOMMEN));
    }


    @Test
    void applyFolgeprozessUmsetzungsverwaltungQuickFilterUmsetzungsverwaltungStatusFilter() {
        VereinbarungQuickFilter.applyFolgeprozessUmsetzungsverwaltungQuickFilter(derivat, filter);
        MultiValueEnumSearchFilter<UmsetzungsBestaetigungStatus> vereinbarungStatusFilter = derivatVereinbarungStatusFilter.getUmsetzungsbestaetigungStatusFilter();
        List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatuses = vereinbarungStatusFilter.getAsEnumList();
        MatcherAssert.assertThat(umsetzungsBestaetigungStatuses, IsIterableContaining.hasItems(UmsetzungsBestaetigungStatus.KEINE_WEITERVERFOLGUNG, UmsetzungsBestaetigungStatus.NICHT_UMGESETZT, UmsetzungsBestaetigungStatus.BEWERTUNG_NICHT_MOEGLICH, UmsetzungsBestaetigungStatus.NICHT_RELEVANT));
    }
}