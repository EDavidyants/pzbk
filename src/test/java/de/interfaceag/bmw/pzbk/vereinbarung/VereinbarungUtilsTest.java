package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class VereinbarungUtilsTest {

    @ParameterizedTest(name = "DerivatAnforderungModulStatus {index}: {0}")
    @EnumSource(DerivatAnforderungModulStatus.class)
    void isCellEditDisabledForActiveDerivat(DerivatAnforderungModulStatus status) {
        boolean result = VereinbarungUtils.isCellEditDisabled(status, true);

        switch (status) {
            case ABZUSTIMMEN:
            case ANGENOMMEN:
            case NICHT_BEWERTBAR:
            case UNZUREICHENDE_ANFORDERUNGSQUALITAET:
            case IN_KLAERUNG:
            case KEINE_WEITERVERFOLGUNG:
                Assertions.assertTrue(result);
                break;
            default:
                Assertions.assertFalse(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(DerivatAnforderungModulStatus.class)
    public void testDefineVereinbarungColor(DerivatAnforderungModulStatus status) {
        String result = VereinbarungUtils.defineVereinbarungColor(status);
        String expected;
        switch (status) {
            case ABZUSTIMMEN:
            case NICHT_BEWERTBAR:
            case IN_KLAERUNG:
                expected = "statusGelb";
                break;
            case ANGENOMMEN:
                expected = "statusGruen";
                break;
            case KEINE_WEITERVERFOLGUNG:
            case UNZUREICHENDE_ANFORDERUNGSQUALITAET:
                expected = "statusRot";
                break;
            default:
                expected = "statusBlau";
                break;
        }
        Assertions.assertEquals(expected, result);
    }

    @ParameterizedTest(name = "DerivatAnforderungModulStatus {index}: {0}")
    @EnumSource(DerivatAnforderungModulStatus.class)
    void isCellEditDisabledForInactiveDerivat(DerivatAnforderungModulStatus status
    ) {
        boolean result = VereinbarungUtils.isCellEditDisabled(status, false);
        Assertions.assertFalse(result);
    }

    @Test
    void defineZakColorNull() {
        String result = VereinbarungUtils.defineZakColor(null);
        Assertions.assertEquals("", result);
    }

    @Test
    void defineUBColorNull() {
        String result = VereinbarungUtils.defineUBColor(null);
        Assertions.assertEquals("", result);
    }

    @Test
    void isStatusChangeValidNull() {
        boolean result = VereinbarungUtils.isStatusChangeValid(null, null);
        Assertions.assertFalse(result);
    }

}
