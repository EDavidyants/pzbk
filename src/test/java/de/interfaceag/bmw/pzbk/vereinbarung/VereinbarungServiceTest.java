package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.Umsetzungsbestaetigung;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.shared.dto.ProcessResultDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungService;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.DerivatVereinbarungDto;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class VereinbarungServiceTest {

    @Mock
    private DerivatAnforderungModulService derivatAnforderungModulService;

    @Mock
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;

    @Mock
    private VereinbarungStatusChangeKommentarService vereinbarungStatusChangeKommentarService;

    @Mock
    private VereinbarungDto vereinbarungDto;

    @Mock
    private DerivatVereinbarungDto derivatVereinbarungDto;

    @Mock
    private DerivatAnforderungModul derivatAnforderungModul;

    @Mock
    private Umsetzungsbestaetigung umsetzungsbestaetigung;

    @InjectMocks
    private final VereinbarungService vereinbarungService = new VereinbarungService();

    private final Mitarbeiter user = TestDataFactory.createMitarbeiter("Max");
    private final Derivat derivat = TestDataFactory.generateDerivat();
    private final Anforderung anforderung = TestDataFactory.generateAnforderung();
    private final Modul modul = TestDataFactory.generateFullModul();
    private final KovAPhaseImDerivat kovaPhaseImDerivat = TestDataFactory.generateKovAPhaseImDerivat();

    @BeforeEach
    public void setUp() {

    }

    @Test
    public void testProcessVereinbarungBatchChangesWithNullGroupStatus() {
        List<VereinbarungDto> vereinbarungData = Arrays.asList(vereinbarungDto);
        ProcessResultDto result = vereinbarungService.processVereinbarungBatchChanges(vereinbarungData, user, null, derivat);
        Assertions.assertFalse(result.isSuccessful());
    }

    @Test
    public void testProcessVereinbarungBatchChangesWithNullGroupStatusMessage() {
        List<VereinbarungDto> vereinbarungData = Arrays.asList(vereinbarungDto);
        ProcessResultDto result = vereinbarungService.processVereinbarungBatchChanges(vereinbarungData, user, null, derivat);
        Assertions.assertEquals("groupStatus is null", result.getMessage());
    }

    @Test
    public void testProcessVereinbarungBatchChangesForGroupStatusAbzustimmen() {
        DerivatAnforderungModulStatus newVereinbarungStatus = DerivatAnforderungModulStatus.ABZUSTIMMEN;
        performTestForVereinbarungStatus(newVereinbarungStatus);
    }

    @Test
    public void testProcessVereinbarungBatchChangesForGroupStatusAngenommen() {
        DerivatAnforderungModulStatus newVereinbarungStatus = DerivatAnforderungModulStatus.ANGENOMMEN;
        performTestForVereinbarungStatus(newVereinbarungStatus);
    }

    @Test
    public void testProcessVereinbarungBatchChangesForGroupStatusNichtBewertbar() {
        DerivatAnforderungModulStatus newVereinbarungStatus = DerivatAnforderungModulStatus.NICHT_BEWERTBAR;
        performTestForVereinbarungStatus(newVereinbarungStatus);
    }

    @Test
    public void testProcessVereinbarungBatchChangesForGroupStatusInKlaerung() {
        DerivatAnforderungModulStatus newVereinbarungStatus = DerivatAnforderungModulStatus.IN_KLAERUNG;
        performTestForVereinbarungStatus(newVereinbarungStatus);
    }

    @Test
    public void testProcessVereinbarungBatchChangesForGroupStatusUnzureichendeAnforderungsqualitaet() {
        DerivatAnforderungModulStatus newVereinbarungStatus = DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET;
        performTestForVereinbarungStatus(newVereinbarungStatus);
    }

    @Test
    public void testProcessVereinbarungBatchChangesForGroupStatusKeineWeiterverfolgung() {
        DerivatAnforderungModulStatus newVereinbarungStatus = DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG;
        performTestForVereinbarungStatus(newVereinbarungStatus);
    }

    @Test
    public void testProcessVereinbarungBatchChangesForGroupStatusKeineWeiterverfolgungUmsetzungsbestaetigung() {
        DerivatAnforderungModulStatus newVereinbarungStatus = DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG;
        performTestForVereinbarungStatusUmsetzungsbestaetigung(newVereinbarungStatus);
    }

    private void performTestForVereinbarungStatus(DerivatAnforderungModulStatus newVereinbarungStatus) {
        doStubbingForVereinbarungStatus(newVereinbarungStatus);

        List<VereinbarungDto> vereinbarungData = Arrays.asList(vereinbarungDto);
        vereinbarungService.processVereinbarungBatchChanges(vereinbarungData, user, newVereinbarungStatus, derivat);
        verify(derivatAnforderungModulService, times(1)).updateDerivatAnforderungModulStatus(newVereinbarungStatus, "Kommentar", derivat, anforderung, modul, user);

    }

    private void doStubbingForVereinbarungStatus(DerivatAnforderungModulStatus newVereinbarungStatus) {
        List<DerivatVereinbarungDto> vereinbarungenToProcess = Arrays.asList(derivatVereinbarungDto);
        when(vereinbarungDto.getDerivatVereinbarungen()).thenReturn(vereinbarungenToProcess);
        doCallRealMethod().when(derivatVereinbarungDto).setNewVereinbarungStatus(newVereinbarungStatus);
        when(derivatVereinbarungDto.getDerivatId()).thenReturn("1");
        List<DerivatAnforderungModul> derivatAnforderungModulList = Arrays.asList(derivatAnforderungModul);
        when(derivatAnforderungModulService.getDerivatAnforderungModulByIdList(any())).thenReturn(derivatAnforderungModulList);
        when(vereinbarungStatusChangeKommentarService.getStatusChangeKommentar(any(), any(), any(DerivatAnforderungModulStatus.class))).thenReturn("Kommentar");
        when(derivatAnforderungModul.getId()).thenReturn(123L);
        when(derivatAnforderungModul.getDerivat()).thenReturn(derivat);
        when(derivatAnforderungModul.getAnforderung()).thenReturn(anforderung);
        when(derivatAnforderungModul.getModul()).thenReturn(modul);
    }

    private void performTestForVereinbarungStatusUmsetzungsbestaetigung(DerivatAnforderungModulStatus newVereinbarungStatus) {
        doStubbingForVereinbarungStatusUmsetzungsbestaetigung(newVereinbarungStatus);

        List<VereinbarungDto> vereinbarungData = Arrays.asList(vereinbarungDto);
        vereinbarungService.processVereinbarungBatchChanges(vereinbarungData, user, newVereinbarungStatus, derivat);
        verify(derivatAnforderungModulService, times(2)).updateDerivatAnforderungModulStatus(newVereinbarungStatus, "Kommentar", derivat, anforderung, modul, user);
    }

    private void doStubbingForVereinbarungStatusUmsetzungsbestaetigung(DerivatAnforderungModulStatus newVereinbarungStatus) {
        doStubbingForVereinbarungStatus(newVereinbarungStatus);
        when(derivatAnforderungModul.getUmsetzungsBestaetigungStatus()).thenReturn(UmsetzungsBestaetigungStatus.OFFEN);
        when(umsetzungsbestaetigungService.getUmsetzungsbestaetigungenByDerivatAnforderungModul(derivatAnforderungModul)).thenReturn(Arrays.asList(umsetzungsbestaetigung));
        when(umsetzungsbestaetigung.getStatus()).thenReturn(UmsetzungsBestaetigungStatus.OFFEN);
        when(umsetzungsbestaetigung.getKovaImDerivat()).thenReturn(kovaPhaseImDerivat);
        when(umsetzungsbestaetigung.getAnforderung()).thenReturn(anforderung);
        when(umsetzungsbestaetigung.getModul()).thenReturn(modul);
    }

}
