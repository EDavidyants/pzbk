package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.dao.DerivatAnforderungModulDao;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import static org.mockito.Mockito.mock;

/**
 *
 * @author sl
 */
@TestInstance(Lifecycle.PER_CLASS)
public class DerivatAnforderungModulServiceTest {

    private DerivatAnforderungModulService derivatAnforderungModulService;

    @BeforeAll
    public void setup() {
        derivatAnforderungModulService = new DerivatAnforderungModulService();
        derivatAnforderungModulService.derivatAnforderungModulDao = mock(DerivatAnforderungModulDao.class);
    }

    @Test
    public void resetLatestHistoryEntry() {
        DerivatAnforderungModul derivatAnforderungModul = TestDataFactory.generateDerivatAnforderungModul();

        Assertions.assertTrue(derivatAnforderungModul.getLatestHistoryEntry().isPresent(), "history entry should be present");

        derivatAnforderungModulService.resetLatestHistoryEntry(derivatAnforderungModul);

        Assertions.assertFalse(derivatAnforderungModul.getLatestHistoryEntry().isPresent(), "history entry should not be present");
    }

    @Test
    public void resetLatestHistoryEntryNullEntry() {
        DerivatAnforderungModul derivatAnforderungModul = TestDataFactory.generateDerivatAnforderungModul();
        derivatAnforderungModul.setLatestHistoryEntry(null);

        Assertions.assertFalse(derivatAnforderungModul.getLatestHistoryEntry().isPresent(), "history entry should be present");

        derivatAnforderungModulService.resetLatestHistoryEntry(derivatAnforderungModul);

        Assertions.assertFalse(derivatAnforderungModul.getLatestHistoryEntry().isPresent(), "history entry should not be present");
    }

    @Test
    public void resetLatestHistoryEntryNullInput() {
        derivatAnforderungModulService.resetLatestHistoryEntry(null);
    }

}
