package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOrders;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungenOrderService;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class VereinbarungTableDataSortServiceTest {

    @Mock
    ProzessbaukastenAnforderungenOrderService prozessbaukastenAnforderungenOrderService;

    @InjectMocks
    VereinbarungTableDataSortService vereinbarungTableDataSortService;

    @Mock
    VereinbarungDto vereinbarung1;
    @Mock
    VereinbarungDto vereinbarung2;
    @Mock
    VereinbarungDto vereinbarung3;

    List<VereinbarungDto> vereinbarungen;

    @Mock
    ProzessbaukastenAnforderungOrders prozessbaukastenAnforderungOrders;

    @BeforeEach
    public void setUp() {
        vereinbarungen = new ArrayList<>();
        vereinbarungen.add(vereinbarung1);
        vereinbarungen.add(vereinbarung2);
        vereinbarungen.add(vereinbarung3);
    }

    private void setupList() {
        when(vereinbarung2.getAnforderungId()).thenReturn(2L);
        when(vereinbarung3.getAnforderungId()).thenReturn(3L);

        when(vereinbarung1.getProzessbaukastenId()).thenReturn(Optional.empty());
        when(vereinbarung2.getProzessbaukastenId()).thenReturn(Optional.of(1L));
        when(vereinbarung3.getProzessbaukastenId()).thenReturn(Optional.of(1L));

        when(prozessbaukastenAnforderungenOrderService.getAnforderungenOrderForProzessbaukasten(any())).thenReturn(prozessbaukastenAnforderungOrders);

        when(prozessbaukastenAnforderungOrders.getOrderForProzessbaukastenAnforderung(anyLong(), anyLong()))
                .thenAnswer(invocation -> {
                    Long argument = (Long) invocation.getArgument(1);
                    if (2L == argument) {
                        return 0;
                    } else if (3L == argument) {
                        return 1;
                    } else {
                        return null;
                    }
                });

    }

    @Test
    public void testSortTableDataBeforeSize() {
        MatcherAssert.assertThat(vereinbarungen, Matchers.hasSize(3));
    }

    @Test
    public void testSortTableDataBeforeOrder() {
        MatcherAssert.assertThat(vereinbarungen, Matchers.contains(vereinbarung1, vereinbarung2, vereinbarung3));
    }

    @Test
    public void testSortTableData() {
        setupList();
        List<VereinbarungDto> result = vereinbarungTableDataSortService.sortTableData(vereinbarungen);
        MatcherAssert.assertThat(result, Matchers.contains(vereinbarung2, vereinbarung3, vereinbarung1));
    }

}
