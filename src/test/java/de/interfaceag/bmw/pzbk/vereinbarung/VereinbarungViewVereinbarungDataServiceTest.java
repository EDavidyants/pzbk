package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;
import de.interfaceag.bmw.pzbk.zak.ZakVereinbarungService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VereinbarungViewVereinbarungDataServiceTest {

    @Mock
    private VereinbarungSearchService vereinbarungSearchService;
    @Mock
    private ZakVereinbarungService zakVereinbarungService;
    @Mock
    private VereinbarungTableDataSortService vereinbarungTableDataSortService;
    @InjectMocks
    private VereinbarungViewVereinbarungDataService vereinbarungViewVereinbarungDataService;

    @Mock
    private VereinbarungViewFilter viewFilter;

    @Mock
    private Derivat derivat;
    private List<Derivat> selectedDerivate;

    @Mock
    private DerivatAnforderungModul derivatAnforderungModul;
    @Mock
    private Anforderung anforderung;
    @Mock
    private IdSearchFilter modulFilter;
    @Mock
    private AnforderungFreigabeBeiModulSeTeam freigabe;
    @Mock
    private ModulSeTeam modulSeTeam;
    @Mock
    private Modul modul;
    @Mock
    private SensorCoc sensorCoc;

    private List<DerivatAnforderungModul> existingVereinbarungData;
    private List<DerivatAnforderungModul> existingVereinbarungDataWithoutFilter;
    private List<ZakUebertragung> zakUebertragungen;
    private List<Anforderung> zuordnungSearchResultForFilter;
    private Set<Long> ids;
    private List<AnforderungFreigabeBeiModulSeTeam> freigaben;

    @BeforeEach
    void setUp() {
        selectedDerivate = new ArrayList<>();
        existingVereinbarungData = new ArrayList<>();
        existingVereinbarungDataWithoutFilter = new ArrayList<>();
        zakUebertragungen = new ArrayList<>();
        zuordnungSearchResultForFilter = Collections.singletonList(anforderung);
        ids = Collections.singleton(1L);
        freigaben = Collections.singletonList(freigabe);
    }

    @Test
    void getVereinbarungDataEmptyDerivatList() {
        final List<VereinbarungDto> result = vereinbarungViewVereinbarungDataService.getVereinbarungData(selectedDerivate, viewFilter);
        assertThat(result, empty());
    }

    @Test
    void getVereinbarungDataZuordnungExistsButNoVereinbarung() {
        selectedDerivate.add(derivat);

        when(vereinbarungSearchService.getVereinbarungDataForFilter(any(), any())).thenReturn(existingVereinbarungData).thenReturn(existingVereinbarungDataWithoutFilter);
        when(zakVereinbarungService.getZakUebertragungByDerivatAnforderungModulList(existingVereinbarungData)).thenReturn(zakUebertragungen);
        when(vereinbarungTableDataSortService.sortTableData(any())).thenAnswer((Answer<List<VereinbarungDto>>) invocation -> {
            Object[] args = invocation.getArguments();
            return (List<VereinbarungDto>) args[0];
        });
        when(vereinbarungSearchService.getZuordnungSearchResultForFilter(viewFilter, selectedDerivate)).thenReturn(zuordnungSearchResultForFilter);
        when(viewFilter.getModulFilter()).thenReturn(modulFilter);
        when(modulFilter.getSelectedIdsAsSet()).thenReturn(ids);
        when(anforderung.getAnfoFreigabeBeiModul()).thenReturn(freigaben);
        when(freigabe.getModulSeTeam()).thenReturn(modulSeTeam);
        when(modulSeTeam.getElternModul()).thenReturn(modul);
        when(modul.getId()).thenReturn(1L);
        when(anforderung.getVersion()).thenReturn(1);
        when(anforderung.getSensorCoc()).thenReturn(sensorCoc);
        when(anforderung.getFestgestelltIn()).thenReturn(Collections.emptySet());
        when(derivat.getStatus()).thenReturn(DerivatStatus.VEREINARBUNG_VKBG);

        final List<VereinbarungDto> result = vereinbarungViewVereinbarungDataService.getVereinbarungData(selectedDerivate, viewFilter);
        assertThat(result, hasSize(1));
    }

    @Test
    void getVereinbarungDataZuordnungExistisButVereinbarungWithOtherStatus() {
        selectedDerivate.add(derivat);
        existingVereinbarungDataWithoutFilter.add(derivatAnforderungModul);

        when(vereinbarungSearchService.getVereinbarungDataForFilter(any(), any())).thenReturn(existingVereinbarungData).thenReturn(existingVereinbarungDataWithoutFilter);
        when(zakVereinbarungService.getZakUebertragungByDerivatAnforderungModulList(existingVereinbarungData)).thenReturn(zakUebertragungen);
        when(vereinbarungTableDataSortService.sortTableData(any())).thenAnswer((Answer<List<VereinbarungDto>>) invocation -> {
            Object[] args = invocation.getArguments();
            return (List<VereinbarungDto>) args[0];
        });
        when(vereinbarungSearchService.getZuordnungSearchResultForFilter(viewFilter, selectedDerivate)).thenReturn(zuordnungSearchResultForFilter);
        when(viewFilter.getModulFilter()).thenReturn(modulFilter);
        when(modulFilter.getSelectedIdsAsSet()).thenReturn(ids);
        when(anforderung.getAnfoFreigabeBeiModul()).thenReturn(freigaben);
        when(freigabe.getModulSeTeam()).thenReturn(modulSeTeam);
        when(modulSeTeam.getElternModul()).thenReturn(modul);
        when(modul.getId()).thenReturn(1L);
        when(derivatAnforderungModul.getModul()).thenReturn(modul);
        when(derivatAnforderungModul.getAnforderung()).thenReturn(anforderung);

        final List<VereinbarungDto> result = vereinbarungViewVereinbarungDataService.getVereinbarungData(selectedDerivate, viewFilter);
        assertThat(result, empty());
    }

    @Test
    void getVereinbarungDataZuordnungExistisButVereinbarungWithSameStatus() {
        selectedDerivate.add(derivat);
        existingVereinbarungData.add(derivatAnforderungModul);
        existingVereinbarungDataWithoutFilter.add(derivatAnforderungModul);

        when(vereinbarungSearchService.getVereinbarungDataForFilter(any(), any())).thenReturn(existingVereinbarungData).thenReturn(existingVereinbarungDataWithoutFilter);
        when(zakVereinbarungService.getZakUebertragungByDerivatAnforderungModulList(existingVereinbarungData)).thenReturn(zakUebertragungen);
        when(vereinbarungTableDataSortService.sortTableData(any())).thenAnswer((Answer<List<VereinbarungDto>>) invocation -> {
            Object[] args = invocation.getArguments();
            return (List<VereinbarungDto>) args[0];
        });
        when(vereinbarungSearchService.getZuordnungSearchResultForFilter(viewFilter, selectedDerivate)).thenReturn(zuordnungSearchResultForFilter);
        when(viewFilter.getModulFilter()).thenReturn(modulFilter);
        when(modulFilter.getSelectedIdsAsSet()).thenReturn(ids);
        when(anforderung.getAnfoFreigabeBeiModul()).thenReturn(freigaben);
        when(freigabe.getModulSeTeam()).thenReturn(modulSeTeam);
        when(modulSeTeam.getElternModul()).thenReturn(modul);
        when(modul.getId()).thenReturn(1L);
        when(derivatAnforderungModul.getModul()).thenReturn(modul);
        when(derivatAnforderungModul.getAnforderung()).thenReturn(anforderung);
        when(derivatAnforderungModul.getDerivat()).thenReturn(derivat);
        when(anforderung.getSensorCoc()).thenReturn(sensorCoc);
        when(derivat.getId()).thenReturn(1L);
        when(derivat.getStatus()).thenReturn(DerivatStatus.VEREINARBUNG_VKBG);

        final List<VereinbarungDto> result = vereinbarungViewVereinbarungDataService.getVereinbarungData(selectedDerivate, viewFilter);
        assertThat(result, hasSize(1));
    }
}