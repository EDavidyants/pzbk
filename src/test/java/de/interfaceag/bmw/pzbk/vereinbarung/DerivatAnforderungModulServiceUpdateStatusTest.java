package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.dao.DerivatAnforderungModulDao;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModulHistory;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.Umsetzungsbestaetigung;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.mail.MailService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class DerivatAnforderungModulServiceUpdateStatusTest {

    @Mock
    private DerivatAnforderungModulDao derivatAnforderungModulDao;

    @Mock
    private ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;

    @Mock
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;

    @Mock
    private VereinbarungHistoryService vereinbarungHistoryService;

    @Mock
    private AnforderungMeldungHistoryService historyService;

    @Mock
    private MailService mailService;

    @Mock
    private Anforderung anforderung;

    @Mock
    private Modul modul;

    @Mock
    private Derivat derivat;

    @Mock
    private DerivatAnforderungModul derivatAnforderungModul;

    @Mock
    private ZuordnungAnforderungDerivat zuordnung;

    @Mock
    private Umsetzungsbestaetigung umsetzungsbestaetigung;

    @Mock
    private KovAPhaseImDerivat kovaPhaseImDerivat;

    @Mock
    private Mitarbeiter user;

    @InjectMocks
    private DerivatAnforderungModulService derivatAnforderungModulService;

    private String kommentar;

    @BeforeEach
    public void setUp() {
        kommentar = "kommentar";
        when(derivatAnforderungModulService.getDerivatAnforderungModulByAnforderungAndModulAndDerivat(anforderung, modul, derivat))
                .thenReturn(derivatAnforderungModul);

        when(derivatAnforderungModul.getZuordnungAnforderungDerivat()).thenReturn(zuordnung);
        when(zuordnung.getAnforderung()).thenReturn(anforderung);
        when(anforderung.getId()).thenReturn(123L);

        when(derivatAnforderungModul.getDerivat()).thenReturn(derivat);
        when(derivatAnforderungModul.getModul()).thenReturn(modul);
        when(derivat.getName()).thenReturn("E20");
        when(modul.getName()).thenReturn("Modul1");

        when(derivat.getStatus()).thenReturn(DerivatStatus.VEREINARBUNG_VKBG);
        when(derivatAnforderungModul.getStatusVKBG()).thenReturn(DerivatAnforderungModulStatus.ABZUSTIMMEN);
        doNothing().when(vereinbarungHistoryService).addVereinbarungHistoryEntry(any(DerivatAnforderungModulHistory.class));
        doNothing().when(historyService).persistAnforderungHistory(any(AnforderungHistory.class));
        when(derivatAnforderungModul.getStatusZV()).thenReturn(DerivatAnforderungModulStatus.ABZUSTIMMEN);
    }

    @Test
    public void testVereinbarungStatusChangeToAbzustimmen() {
        DerivatAnforderungModulStatus newVereinbarungStatus = DerivatAnforderungModulStatus.ABZUSTIMMEN;
        List<Umsetzungsbestaetigung> umsetzungsbestaetigungen = Collections.singletonList(umsetzungsbestaetigung);

        when(umsetzungsbestaetigungService.getUmsetzungsbestaetigungenByDerivatAnforderungModul(derivatAnforderungModul)).thenReturn(umsetzungsbestaetigungen);
        when(umsetzungsbestaetigung.getKovaImDerivat()).thenReturn(kovaPhaseImDerivat);
        when(kovaPhaseImDerivat.getKovaStatus()).thenReturn(KovAStatus.AKTIV);
        doNothing().when(umsetzungsbestaetigungService).persistUmsetzungsbestaetigungen(any());

        derivatAnforderungModulService.updateDerivatAnforderungModulStatus(newVereinbarungStatus, kommentar, derivat, anforderung, modul, user);
        verify(zuordnungAnforderungDerivatService).updateZuordnungAnforderungDerivatStatus(any(ZuordnungStatus.class), any(ZuordnungAnforderungDerivat.class), any(Mitarbeiter.class));
    }

    @Test
    public void testVereinbarungStatusChangeToKeineWeiterverfolgung() {
        DerivatAnforderungModulStatus newVereinbarungStatus = DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG;
        List<Umsetzungsbestaetigung> umsetzungsbestaetigungen = Collections.singletonList(umsetzungsbestaetigung);
        when(umsetzungsbestaetigungService.getUmsetzungsbestaetigungenByDerivatAnforderungModul(derivatAnforderungModul)).thenReturn(umsetzungsbestaetigungen);
        when(umsetzungsbestaetigung.getKovaImDerivat()).thenReturn(kovaPhaseImDerivat);
        when(kovaPhaseImDerivat.getKovaStatus()).thenReturn(KovAStatus.AKTIV);
        doNothing().when(umsetzungsbestaetigungService).persistUmsetzungsbestaetigungen(any());

        derivatAnforderungModulService.updateDerivatAnforderungModulStatus(newVereinbarungStatus, kommentar, derivat, anforderung, modul, user);
        verify(zuordnungAnforderungDerivatService, never()).updateZuordnungAnforderungDerivatStatus(any(ZuordnungStatus.class), any(ZuordnungAnforderungDerivat.class), any(Mitarbeiter.class));
    }

    @Test
    public void testVereinbarungStatusChangeToUnzureichendeAnforderungsqualitaet() {
        DerivatAnforderungModulStatus newVereinbarungStatus = DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET;
        List<Umsetzungsbestaetigung> umsetzungsbestaetigungen = Collections.singletonList(umsetzungsbestaetigung);
        when(umsetzungsbestaetigungService.getUmsetzungsbestaetigungenByDerivatAnforderungModul(derivatAnforderungModul)).thenReturn(umsetzungsbestaetigungen);
        when(umsetzungsbestaetigung.getKovaImDerivat()).thenReturn(kovaPhaseImDerivat);
        when(kovaPhaseImDerivat.getKovaStatus()).thenReturn(KovAStatus.AKTIV);
        doNothing().when(umsetzungsbestaetigungService).persistUmsetzungsbestaetigungen(any());

        derivatAnforderungModulService.updateDerivatAnforderungModulStatus(newVereinbarungStatus, kommentar, derivat, anforderung, modul, user);
        verify(zuordnungAnforderungDerivatService, never()).updateZuordnungAnforderungDerivatStatus(any(ZuordnungStatus.class), any(ZuordnungAnforderungDerivat.class), any(Mitarbeiter.class));
    }

    @Test
    public void testVereinbarungStatusChangeToAngenommen() {
        DerivatAnforderungModulStatus newVereinbarungStatus = DerivatAnforderungModulStatus.ANGENOMMEN;
        List<Umsetzungsbestaetigung> umsetzungsbestaetigungen = Collections.singletonList(umsetzungsbestaetigung);

        when(derivatAnforderungModul.getKovAPhase()).thenReturn(Optional.of(KovAPhase.VA_VKBG));
        when(derivatAnforderungModul.getAnforderung()).thenReturn(anforderung);
        when(umsetzungsbestaetigungService.getUmsetzungsbestaetigungByAnforderungModulDerivatPhase(anforderung, derivat, modul, KovAPhase.VA_VKBG))
                .thenReturn(Optional.ofNullable(umsetzungsbestaetigung));
        when(umsetzungsbestaetigung.getStatus()).thenReturn(UmsetzungsBestaetigungStatus.OFFEN);
        when(umsetzungsbestaetigungService.createUmsetzungsbestaetigungenForDerivatAnforderungModul(derivatAnforderungModul))
                .thenReturn(umsetzungsbestaetigungen);

        doNothing().when(umsetzungsbestaetigungService).persistUmsetzungsbestaetigungen(any());

        derivatAnforderungModulService.updateDerivatAnforderungModulStatus(newVereinbarungStatus, kommentar, derivat, anforderung, modul, user);
        verify(zuordnungAnforderungDerivatService, never()).updateZuordnungAnforderungDerivatStatus(any(ZuordnungStatus.class), any(ZuordnungAnforderungDerivat.class), any(Mitarbeiter.class));
    }

    @Test
    public void testVereinbarungStatusChangeToInKlaerung() {
        DerivatAnforderungModulStatus newVereinbarungStatus = DerivatAnforderungModulStatus.IN_KLAERUNG;
        List<Umsetzungsbestaetigung> umsetzungsbestaetigungen = Collections.singletonList(umsetzungsbestaetigung);

        when(derivatAnforderungModul.getKovAPhase()).thenReturn(Optional.of(KovAPhase.VA_VKBG));
        when(derivatAnforderungModul.getAnforderung()).thenReturn(anforderung);
        when(umsetzungsbestaetigungService.getUmsetzungsbestaetigungByAnforderungModulDerivatPhase(anforderung, derivat, modul, KovAPhase.VA_VKBG))
                .thenReturn(Optional.ofNullable(umsetzungsbestaetigung));
        when(umsetzungsbestaetigung.getStatus()).thenReturn(UmsetzungsBestaetigungStatus.OFFEN);
        when(umsetzungsbestaetigungService.createUmsetzungsbestaetigungenForDerivatAnforderungModul(derivatAnforderungModul))
                .thenReturn(umsetzungsbestaetigungen);

        doNothing().when(umsetzungsbestaetigungService).persistUmsetzungsbestaetigungen(any());

        derivatAnforderungModulService.updateDerivatAnforderungModulStatus(newVereinbarungStatus, kommentar, derivat, anforderung, modul, user);
        verify(zuordnungAnforderungDerivatService, never()).updateZuordnungAnforderungDerivatStatus(any(ZuordnungStatus.class), any(ZuordnungAnforderungDerivat.class), any(Mitarbeiter.class));
    }

    @Test
    public void testVereinbarungStatusChangeToNichtBewertbar() {
        DerivatAnforderungModulStatus newVereinbarungStatus = DerivatAnforderungModulStatus.NICHT_BEWERTBAR;
        List<Umsetzungsbestaetigung> umsetzungsbestaetigungen = Arrays.asList(umsetzungsbestaetigung);

        when(derivatAnforderungModul.getKovAPhase()).thenReturn(Optional.of(KovAPhase.VA_VKBG));
        when(derivatAnforderungModul.getAnforderung()).thenReturn(anforderung);
        when(umsetzungsbestaetigungService.getUmsetzungsbestaetigungByAnforderungModulDerivatPhase(anforderung, derivat, modul, KovAPhase.VA_VKBG))
                .thenReturn(Optional.ofNullable(umsetzungsbestaetigung));
        when(umsetzungsbestaetigung.getStatus()).thenReturn(UmsetzungsBestaetigungStatus.OFFEN);
        when(umsetzungsbestaetigungService.createUmsetzungsbestaetigungenForDerivatAnforderungModul(derivatAnforderungModul))
                .thenReturn(umsetzungsbestaetigungen);

        doNothing().when(umsetzungsbestaetigungService).persistUmsetzungsbestaetigungen(any());

        derivatAnforderungModulService.updateDerivatAnforderungModulStatus(newVereinbarungStatus, kommentar, derivat, anforderung, modul, user);
        verify(zuordnungAnforderungDerivatService, never()).updateZuordnungAnforderungDerivatStatus(any(ZuordnungStatus.class), any(ZuordnungAnforderungDerivat.class), any(Mitarbeiter.class));
    }

}
