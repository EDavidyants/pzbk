package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class VereinbarungFactoryInvalidStatus {

    @Mock
    ZuordnungAnforderungDerivat anforderungDerivat;
    @Mock
    Modul modul;
    @Mock
    Mitarbeiter mitarbeiter;

    Anforderung anforderung;
    Set<Prozessbaukasten> prozessbaukasten;

    @BeforeEach
    public void setUp() {
        anforderung = TestDataFactory.generateAnforderung();
        prozessbaukasten = new HashSet<>();
    }

    @Test
    public void testCreateNewVereinbarungForStatusInaktiv() {
        prozessbaukasten.add(TestDataFactory.generateProzessbaukasten());
        anforderung.setProzessbaukasten(prozessbaukasten);
        when(anforderungDerivat.getZuordnungDerivatStatus()).thenReturn(DerivatStatus.INAKTIV);

        DerivatAnforderungModul result = VereinbarungFactory.createNewVereinbarung(anforderungDerivat, modul, mitarbeiter, DerivatAnforderungModulStatus.ABZUSTIMMEN);
        assertThat(result, is(nullValue()));
    }

    @Test
    public void testCreateNewVereinbarungForStatusOffen() {
        prozessbaukasten.add(TestDataFactory.generateProzessbaukasten());
        anforderung.setProzessbaukasten(prozessbaukasten);
        when(anforderungDerivat.getZuordnungDerivatStatus()).thenReturn(DerivatStatus.OFFEN);

        DerivatAnforderungModul result = VereinbarungFactory.createNewVereinbarung(anforderungDerivat, modul, mitarbeiter, DerivatAnforderungModulStatus.ABZUSTIMMEN);
        assertThat(result, is(nullValue()));
    }

    @Test
    public void testCreateNewVereinbarungForStatusZV() {
        prozessbaukasten.add(TestDataFactory.generateProzessbaukasten());
        anforderung.setProzessbaukasten(prozessbaukasten);
        when(anforderungDerivat.getZuordnungDerivatStatus()).thenReturn(DerivatStatus.ZV);

        DerivatAnforderungModul result = VereinbarungFactory.createNewVereinbarung(anforderungDerivat, modul, mitarbeiter, DerivatAnforderungModulStatus.ABZUSTIMMEN);
        assertThat(result, is(nullValue()));
    }

}
