package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class VereinbarungProcessServiceTest {

    @Mock
    private Session session;

    @Mock
    private VereinbarungService vereinbarungService;

    @Mock
    private VereinbarungViewData viewData;

    @Mock
    private List<VereinbarungDto> tableData;
    @Mock
    private List<VereinbarungDto> selectedTableData;

    @InjectMocks
    private final VereinbarungProcessService vereinbarungProcessService = new VereinbarungProcessService();

    private final Derivat derivat = TestDataFactory.generateDerivat();
    private final Mitarbeiter user = TestDataFactory.createMitarbeiter("Max");

    @BeforeEach
    public void setUp() {
        when(session.getUser()).thenReturn(user);
        when(viewData.getGroupStatus()).thenReturn(DerivatAnforderungModulStatus.ANGENOMMEN);
        when(viewData.getSelectedDerivat()).thenReturn(derivat);

    }

    @Test
    public void testProcessChangesIfNotAllSelected() {
        when(viewData.isAllSelected()).thenReturn(Boolean.FALSE);
        when(viewData.getSelectedTableData()).thenReturn(selectedTableData);

        vereinbarungProcessService.processChanges(viewData);

        verify(vereinbarungService, times(1)).processVereinbarungBatchChanges(selectedTableData, user, DerivatAnforderungModulStatus.ANGENOMMEN, derivat);
    }

    @Test
    public void testProcessChangesIfAllSelected() {
        when(viewData.isAllSelected()).thenReturn(Boolean.TRUE);
        when(viewData.getTableData()).thenReturn(tableData);

        vereinbarungProcessService.processChanges(viewData);

        verify(vereinbarungService, times(1)).processVereinbarungBatchChanges(tableData, user, DerivatAnforderungModulStatus.ANGENOMMEN, derivat);

    }

}
