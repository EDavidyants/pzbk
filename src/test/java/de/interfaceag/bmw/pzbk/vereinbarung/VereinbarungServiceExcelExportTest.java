package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.dto.ProzessbaukastenLabelDto;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.DerivatVereinbarungDto;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.VereinbarungDto;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class VereinbarungServiceExcelExportTest {

    @Mock
    Derivat derivat;

    private VereinbarungService vereinbarungService;

    private ProzessbaukastenLabelDto prozessbaukastenLabelDto;

    @BeforeEach
    public void setup() {
        vereinbarungService = new VereinbarungService();
        prozessbaukastenLabelDto = new ProzessbaukastenLabelDto(1L, "P123", 1);
        vereinbarungService.localizationService = mock(LocalizationService.class);
        vereinbarungService.derivatAnforderungModulService = mock(DerivatAnforderungModulService.class);
        vereinbarungService.vereinbarungHistoryService = mock(VereinbarungHistoryService.class);
        when(vereinbarungService.localizationService.getCurrentLocale()).thenReturn(Locale.GERMAN);
//        when(vereinbarungService.derivatAnforderungModulService.getDerivatAnforderungModulById(any())).thenReturn(null);
//        when(vereinbarungService.vereinbarungHistoryService.getHistoryForDerivatAnforderungModul(any())).thenReturn(new ArrayList<>());
    }

    @Test
    public void excelExportHeaderStructureTest() {
        List<VereinbarungDto> vereinbarungData = new ArrayList<>();

        Workbook excelExportWorkbook = vereinbarungService.createExcelExport(vereinbarungData);

        Assertions.assertEquals(1, excelExportWorkbook.getNumberOfSheets());
        Assertions.assertEquals("Anforderung Zuordnung", excelExportWorkbook.getSheetName(0));

        Sheet sheet = excelExportWorkbook.getSheetAt(0);

        Assertions.assertEquals(1, sheet.getLastRowNum());

        Row headerRow = sheet.getRow(0);

        Assertions.assertEquals(5, headerRow.getLastCellNum());

        Assertions.assertEquals("Anforderung", headerRow.getCell(0).toString());
        Assertions.assertEquals("Beschreibung", headerRow.getCell(1).toString());
        Assertions.assertEquals("Modul", headerRow.getCell(2).toString());
        Assertions.assertEquals("Sensor CoC", headerRow.getCell(3).toString());
        Assertions.assertEquals("ZAK ID", headerRow.getCell(4).toString());

    }

    @Test
    public void excelExportHeaderStructureWithOneDerivatTest() {
        when(derivat.getId()).thenReturn(84L);
        when(derivat.getName()).thenReturn("E42");
        when(derivat.getStatus()).thenReturn(DerivatStatus.OFFEN);
        when(derivat.isVereinbarungActive()).thenReturn(Boolean.FALSE);

        List<VereinbarungDto> vereinbarungData = new ArrayList<>();

        VereinbarungDto vereinbarungDto = new VereinbarungDto(12L, "A12", "A12", "Beschreibung", 24L, "ML", "42", "SensorCoc", 84L, new ArrayList<>(), 1l, prozessbaukastenLabelDto);
        DerivatVereinbarungDto derivatVereinbarungDto = DerivatVereinbarungDto.forDerivat(derivat).build();

        vereinbarungDto.addDerivatVereinbarung(derivatVereinbarungDto);

        vereinbarungData.add(vereinbarungDto);

        Workbook excelExportWorkbook = vereinbarungService.createExcelExport(vereinbarungData);

        Assertions.assertEquals(1, excelExportWorkbook.getNumberOfSheets());
        Assertions.assertEquals("Anforderung Zuordnung", excelExportWorkbook.getSheetName(0));

        Sheet sheet = excelExportWorkbook.getSheetAt(0);

        Assertions.assertEquals(2, sheet.getLastRowNum());

        Row headerRow = sheet.getRow(0);

        Assertions.assertEquals(6, headerRow.getLastCellNum());

        Assertions.assertEquals("Anforderung", headerRow.getCell(0).toString());
        Assertions.assertEquals("Beschreibung", headerRow.getCell(1).toString());
        Assertions.assertEquals("Modul", headerRow.getCell(2).toString());
        Assertions.assertEquals("Sensor CoC", headerRow.getCell(3).toString());
        Assertions.assertEquals("ZAK ID", headerRow.getCell(4).toString());
        Assertions.assertEquals("E42", headerRow.getCell(5).toString());

    }

    @Test
    public void excelExportEmptyListTest() {
        when(derivat.getId()).thenReturn(84L);
        when(derivat.getName()).thenReturn("E42");
        when(derivat.getStatus()).thenReturn(DerivatStatus.OFFEN);
        when(derivat.isVereinbarungActive()).thenReturn(Boolean.FALSE);

        List<VereinbarungDto> vereinbarungData = new ArrayList<>();

        VereinbarungDto vereinbarungDto = new VereinbarungDto(12L, "A12", "A12", "Beschreibung", 24L, "ML", "42", "SensorCoc", 84L, new ArrayList<>(), 1l, prozessbaukastenLabelDto);
        DerivatVereinbarungDto derivatVereinbarungDto = DerivatVereinbarungDto.forDerivat(derivat).build();
        vereinbarungDto.addDerivatVereinbarung(derivatVereinbarungDto);

        vereinbarungData.add(vereinbarungDto);

        Workbook excelExportWorkbook = vereinbarungService.createExcelExport(vereinbarungData);

        Sheet sheet = excelExportWorkbook.getSheetAt(0);

        Assertions.assertEquals(2, sheet.getLastRowNum());

        Row resultRow = sheet.getRow(2);

        Assertions.assertEquals(8, resultRow.getLastCellNum());

        Assertions.assertEquals("A12", resultRow.getCell(0).toString());
        Assertions.assertEquals("Beschreibung", resultRow.getCell(1).toString());
        Assertions.assertEquals("ML", resultRow.getCell(2).toString());
        Assertions.assertEquals("SensorCoc", resultRow.getCell(3).toString());
        Assertions.assertEquals("42", resultRow.getCell(4).toString());
        Assertions.assertEquals("abzustimmen", resultRow.getCell(5).toString());
        Assertions.assertEquals("", resultRow.getCell(6).toString());
        Assertions.assertEquals("", resultRow.getCell(7).toString());

    }

}
