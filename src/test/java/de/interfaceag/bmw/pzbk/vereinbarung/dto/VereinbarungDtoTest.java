package de.interfaceag.bmw.pzbk.vereinbarung.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.text.IsEmptyString.emptyString;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class VereinbarungDtoTest {

    private static final Logger LOG = LoggerFactory.getLogger(VereinbarungDtoTest.class.getName());

    @Test
    void generateKeyItNotNull() {
        String key = VereinbarungDto.generateKey(1234L, "ML:IO");
        LOG.info(key);
        assertThat(key, notNullValue());
    }

    @Test
    void generateKeyItNotEmpty() {
        String key = VereinbarungDto.generateKey(1234L, "ML:IO");
        LOG.info(key);
        assertThat(key, not(emptyString()));
    }

    @Test
    void generateKeyNull() {
        String key = VereinbarungDto.generateKey(1L, null);
        Assertions.assertEquals("", key);
    }

}
