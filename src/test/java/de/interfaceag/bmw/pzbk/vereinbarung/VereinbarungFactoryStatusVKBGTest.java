package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class VereinbarungFactoryStatusVKBGTest {

    @Mock
    ZuordnungAnforderungDerivat anforderungDerivat;
    @Mock
    Modul modul;
    @Mock
    Mitarbeiter mitarbeiter;

    Anforderung anforderung;
    Set<Prozessbaukasten> prozessbaukasten;

    @BeforeEach
    public void setUp() {
        anforderung = TestDataFactory.generateAnforderung();
        when(anforderungDerivat.getZuordnungDerivatStatus()).thenReturn(DerivatStatus.VEREINARBUNG_VKBG);
        prozessbaukasten = new HashSet<>();
    }

    @Test
    public void testCreateNewVereinbarungForAnforderungWithProzessbaukasten() {
        prozessbaukasten.add(TestDataFactory.generateProzessbaukasten());
        anforderung.setProzessbaukasten(prozessbaukasten);
        when(anforderungDerivat.getAnforderung()).thenReturn(anforderung);

        DerivatAnforderungModul result = VereinbarungFactory.createNewVereinbarung(anforderungDerivat, modul, mitarbeiter, DerivatAnforderungModulStatus.ABZUSTIMMEN);
        DerivatAnforderungModulStatus statusVKBG = result.getStatusVKBG();
        DerivatAnforderungModulStatus statusZV = result.getStatusZV();
        assertThat(statusVKBG, is(DerivatAnforderungModulStatus.ANGENOMMEN));
        assertThat(statusZV, is(DerivatAnforderungModulStatus.ANGENOMMEN));
    }

    @Test
    public void testCreateNewVereinbarungForAnforderungWithoutProzessbaukasten() {
        when(anforderungDerivat.getAnforderung()).thenReturn(anforderung);

        DerivatAnforderungModul result = VereinbarungFactory.createNewVereinbarung(anforderungDerivat, modul, mitarbeiter, DerivatAnforderungModulStatus.ABZUSTIMMEN);
        DerivatAnforderungModulStatus statusVKBG = result.getStatusVKBG();
        DerivatAnforderungModulStatus statusZV = result.getStatusZV();
        assertThat(statusVKBG, is(DerivatAnforderungModulStatus.ABZUSTIMMEN));
        assertThat(statusZV, is(DerivatAnforderungModulStatus.ABZUSTIMMEN));
    }

    @Test
    public void testCreateVereinbarungForZakAnforderung() {
        when(anforderungDerivat.getAnforderung()).thenReturn(anforderung);
        anforderung.getAnfoFreigabeBeiModul().forEach(af -> af.setZak(Boolean.TRUE));

        DerivatAnforderungModul result = VereinbarungFactory.createNewVereinbarung(anforderungDerivat, modul, mitarbeiter, DerivatAnforderungModulStatus.ABZUSTIMMEN);
        ZakStatus resultZakStatus = result.getZakStatus();
        assertThat(resultZakStatus, is(ZakStatus.EMPTY));
    }

    @Test
    public void testCreateVereinbarungForStdAnforderung() {
        when(anforderungDerivat.getAnforderung()).thenReturn(anforderung);
        anforderung.getAnfoFreigabeBeiModul().forEach(af -> af.setZak(Boolean.FALSE));

        DerivatAnforderungModul result = VereinbarungFactory.createNewVereinbarung(anforderungDerivat, modul, mitarbeiter, DerivatAnforderungModulStatus.ABZUSTIMMEN);
        ZakStatus resultZakStatus = result.getZakStatus();
        assertThat(resultZakStatus, is(ZakStatus.KEINE_ZAKUEBERTRAGUNG));
    }

}
