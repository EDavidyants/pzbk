package de.interfaceag.bmw.pzbk.vereinbarung;

import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.mail.MailService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author sl
 */
public class DerivatAnforderungModulServiceMailTest {

    private DerivatAnforderungModulService derivatAnforderungModulService;

    @Before
    public void setup() {
        derivatAnforderungModulService = new DerivatAnforderungModulService();
        derivatAnforderungModulService.mailService = mock(MailService.class);
    }

    @Test
    public void sendMailToAnfordererStatusNotAngenommen() {
        DerivatAnforderungModul derivatAnforderungModul = TestDataFactory.generateDerivatAnforderungModul();
        derivatAnforderungModul.setStatusVKBG(DerivatAnforderungModulStatus.ABZUSTIMMEN);
        derivatAnforderungModul.setStatusZV(DerivatAnforderungModulStatus.ABZUSTIMMEN);
        derivatAnforderungModulService.sendMailToAnfordererBasedOnStatus(derivatAnforderungModul);
        verify(derivatAnforderungModulService.mailService, never()).sendMailToAnforderer(any(), any());
    }

    @Test
    public void sendMailToAnfordererZakStatusEmpty() {
        DerivatAnforderungModul derivatAnforderungModul = TestDataFactory.generateDerivatAnforderungModul();
        derivatAnforderungModul.setStatusVKBG(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setStatusZV(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setZakStatus(ZakStatus.EMPTY);
        derivatAnforderungModulService.sendMailToAnfordererBasedOnStatus(derivatAnforderungModul);
        verify(derivatAnforderungModulService.mailService, never()).sendMailToAnforderer(any(), any());
    }

    @Test
    public void sendMailToAnfordererZakStatusPending() {
        DerivatAnforderungModul derivatAnforderungModul = TestDataFactory.generateDerivatAnforderungModul();
        derivatAnforderungModul.setStatusVKBG(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setStatusZV(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setZakStatus(ZakStatus.PENDING);
        derivatAnforderungModulService.sendMailToAnfordererBasedOnStatus(derivatAnforderungModul);
        verify(derivatAnforderungModulService.mailService, never()).sendMailToAnforderer(any(), any());
    }

    @Test
    public void sendMailToAnfordererZakStatusDone() {
        DerivatAnforderungModul derivatAnforderungModul = TestDataFactory.generateDerivatAnforderungModul();
        derivatAnforderungModul.setStatusVKBG(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setStatusZV(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setZakStatus(ZakStatus.DONE);
        derivatAnforderungModulService.sendMailToAnfordererBasedOnStatus(derivatAnforderungModul);
        verify(derivatAnforderungModulService.mailService, never()).sendMailToAnforderer(any(), any());
    }

    @Test
    public void sendMailToAnfordererZakStatusNichtBewertbar() {
        DerivatAnforderungModul derivatAnforderungModul = TestDataFactory.generateDerivatAnforderungModul();
        derivatAnforderungModul.setStatusVKBG(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setStatusZV(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setZakStatus(ZakStatus.NICHT_BEWERTBAR);
        derivatAnforderungModulService.sendMailToAnfordererBasedOnStatus(derivatAnforderungModul);
        verify(derivatAnforderungModulService.mailService, times(1)).sendMailToAnforderer(any(), any());
    }

    @Test
    public void sendMailToAnfordererZakStatusNichtUmsetzbar() {
        DerivatAnforderungModul derivatAnforderungModul = TestDataFactory.generateDerivatAnforderungModul();
        derivatAnforderungModul.setStatusVKBG(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setStatusZV(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setZakStatus(ZakStatus.NICHT_UMSETZBAR);
        derivatAnforderungModulService.sendMailToAnfordererBasedOnStatus(derivatAnforderungModul);
        verify(derivatAnforderungModulService.mailService, times(1)).sendMailToAnforderer(any(), any());
    }

    @Test
    public void sendMailToAnfordererZakStatusKeineZakUebertragung() {
        DerivatAnforderungModul derivatAnforderungModul = TestDataFactory.generateDerivatAnforderungModul();
        derivatAnforderungModul.setStatusVKBG(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setStatusZV(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setZakStatus(ZakStatus.KEINE_ZAKUEBERTRAGUNG);
        derivatAnforderungModulService.sendMailToAnfordererBasedOnStatus(derivatAnforderungModul);
        verify(derivatAnforderungModulService.mailService, never()).sendMailToAnforderer(any(), any());
    }

    @Test
    public void sendMailToAnfordererZakStatusUebertragungFehlerhaft() {
        DerivatAnforderungModul derivatAnforderungModul = TestDataFactory.generateDerivatAnforderungModul();
        derivatAnforderungModul.setStatusVKBG(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setStatusZV(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setZakStatus(ZakStatus.UEBERTRAGUNG_FEHLERHAFT);
        derivatAnforderungModulService.sendMailToAnfordererBasedOnStatus(derivatAnforderungModul);
        verify(derivatAnforderungModulService.mailService, never()).sendMailToAnforderer(any(), any());
    }

    @Test
    public void sendMailToAnfordererZakStatusZurueckgezogen() {
        DerivatAnforderungModul derivatAnforderungModul = TestDataFactory.generateDerivatAnforderungModul();
        derivatAnforderungModul.setStatusVKBG(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setStatusZV(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setZakStatus(ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER);
        derivatAnforderungModulService.sendMailToAnfordererBasedOnStatus(derivatAnforderungModul);
        verify(derivatAnforderungModulService.mailService, times(1)).sendMailToAnforderer(any(), any());
    }

    @Test
    public void sendMailToAnfordererZakStatusBestaetigtAusVorprozess() {
        DerivatAnforderungModul derivatAnforderungModul = TestDataFactory.generateDerivatAnforderungModul();
        derivatAnforderungModul.setStatusVKBG(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setStatusZV(DerivatAnforderungModulStatus.ANGENOMMEN);
        derivatAnforderungModul.setZakStatus(ZakStatus.BESTAETIGT_AUS_VORPROZESS);
        derivatAnforderungModulService.sendMailToAnfordererBasedOnStatus(derivatAnforderungModul);
        verify(derivatAnforderungModulService.mailService, never()).sendMailToAnforderer(any(), any());
    }

}
