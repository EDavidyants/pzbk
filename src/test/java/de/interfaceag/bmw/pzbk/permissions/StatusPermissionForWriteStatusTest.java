package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.Status;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class StatusPermissionForWriteStatusTest extends AbstractStatusPermissionTest {

    @Before
    public void initRolePermission() {
        Status readStatus = Status.M_GEMELDET;
        Status writeStatus = Status.M_ENTWURF;
        Status currentStatus = writeStatus;
        initPermission(readStatus, writeStatus, currentStatus);
    }

    @Test
    public void testViewPermission() {
        Assert.assertTrue(getPermission().isRendered());
    }

    @Test
    public void testEditPermission() {
        Assert.assertTrue(getPermission().hasRightToEdit());
    }

}
