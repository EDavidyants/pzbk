package de.interfaceag.bmw.pzbk.permissions.components;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DashboardRoleSensorCocLeiterViewPermissionTest extends AbstractDashboardStepViewPermissionTest {

    @Before
    public void initPermission() {
        initPermission(Rolle.SENSORCOCLEITER);
    }

    @Test
    public void testStepOneToFourPanel() {
        Assert.assertTrue(getStepOneToFourPanel());
    }

    @Test
    public void testStepOne() {
        Assert.assertTrue(getStepOne());
    }

    @Test
    public void testStepTwo() {
        Assert.assertTrue(getStepTwo());
    }

    @Test
    public void testStepThree() {
        Assert.assertTrue(getStepThree());
    }

    @Test
    public void testStepFour() {
        Assert.assertTrue(getStepFour());
    }

    @Test
    public void testStepFiveToSevenPanel() {
        Assert.assertTrue(getStepFiveToSevenPanel());
    }

    @Test
    public void testStepFive() {
        Assert.assertTrue(getStepFive());
    }

    @Test
    public void testStepSix() {
        Assert.assertTrue(getStepSix());
    }

    @Test
    public void testStepEightToThirteenPanel() {
        Assert.assertTrue(getStepEightToThirteenPanel());
    }

    @Test
    public void testStepEight() {
        Assert.assertTrue(getStepEight());
    }

    @Test
    public void testStepNine() {
        Assert.assertFalse(getStepNine());
    }

    @Test
    public void testStepTen() {
        Assert.assertFalse(getStepTen());
    }

    @Test
    public void testStepEleven() {
        Assert.assertFalse(getStepEleven());
    }

    @Test
    public void testStepTwelve() {
        Assert.assertFalse(getStepTwelve());
    }

    @Test
    public void testStepThirteen() {
        Assert.assertFalse(getStepThirteen());
    }

}
