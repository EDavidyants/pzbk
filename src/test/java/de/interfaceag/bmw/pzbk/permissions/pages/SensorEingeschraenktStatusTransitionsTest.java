package de.interfaceag.bmw.pzbk.permissions.pages;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungViewPermission;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class SensorEingeschraenktStatusTransitionsTest {

    private AnforderungViewPermission viewPermission;

    public AnforderungViewPermission initViewPermission(Status status) {
        Set<Rolle> rolesWithWritePermissions = new HashSet<>();
        rolesWithWritePermissions.add(Rolle.SENSOR_EINGESCHRAENKT);
        return new AnforderungViewPermission(rolesWithWritePermissions, status, true, true);
    }

    @Test
    public void testStatusTransitionsForSensorForMeldungInStatusEntwurf() {
        viewPermission = initViewPermission(Status.M_ENTWURF);
        Assert.assertTrue(viewPermission.getStatusTransition());
    }

    @Test
    public void testStatusTransitionsForSensorForMeldungInStatusGemeldet() {
        viewPermission = initViewPermission(Status.M_GEMELDET);
        Assert.assertFalse(viewPermission.getStatusTransition());
    }

    @Test
    public void testStatusTransitionsForSensorForMeldungInStatusUnstimmig() {
        viewPermission = initViewPermission(Status.M_UNSTIMMIG);
        Assert.assertTrue(viewPermission.getStatusTransition());
    }

    @Test
    public void testStatusTransitionsForSensorForMeldungInStatusZugeordnet() {
        viewPermission = initViewPermission(Status.M_ZUGEORDNET);
        Assert.assertFalse(viewPermission.getStatusTransition());
    }

    @Test
    public void testStatusTransitionsForSensorForMeldungInStatusGeloescht() {
        viewPermission = initViewPermission(Status.M_GELOESCHT);
        Assert.assertFalse(viewPermission.getStatusTransition());
    }

    @Test
    public void testStatusTransitionsForSensorForMeldungInStatusAngelegt() {
        viewPermission = initViewPermission(Status.M_ANGELEGT);
        Assert.assertFalse(viewPermission.getStatusTransition());
    }

    @Test
    public void testStatusTransitionsForSensorForAnforderungInStatusInArbeit() {
        viewPermission = initViewPermission(Status.A_INARBEIT);
        Assert.assertFalse(viewPermission.getStatusTransition());
    }

    @Test
    public void testStatusTransitionsForSensorForAnforderungInStatusAbgestimmt() {
        viewPermission = initViewPermission(Status.A_FTABGESTIMMT);
        Assert.assertFalse(viewPermission.getStatusTransition());
    }

    @Test
    public void testStatusTransitionsForSensorForAnforderungInStatusPlausibilisiert() {
        viewPermission = initViewPermission(Status.A_PLAUSIB);
        Assert.assertFalse(viewPermission.getStatusTransition());
    }

    @Test
    public void testStatusTransitionsForSensorForAnforderungInStatusUnstimmig() {
        viewPermission = initViewPermission(Status.A_UNSTIMMIG);
        Assert.assertFalse(viewPermission.getStatusTransition());
    }

    @Test
    public void testStatusTransitionsForSensorForAnforderungInStatusKeineWeiterverfolgung() {
        viewPermission = initViewPermission(Status.A_KEINE_WEITERVERFOLG);
        Assert.assertFalse(viewPermission.getStatusTransition());
    }

    @Test
    public void testStatusTransitionsForSensorForAnforderungInStatusGeloescht() {
        viewPermission = initViewPermission(Status.A_GELOESCHT);
        Assert.assertFalse(viewPermission.getStatusTransition());
    }

    @Test
    public void testStatusTransitionsForSensorForAnforderungInStatusFreigegeben() {
        viewPermission = initViewPermission(Status.A_FREIGEGEBEN);
        Assert.assertFalse(viewPermission.getStatusTransition());
    }

}
