package de.interfaceag.bmw.pzbk.permissions.components;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.ViewPermission;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

/**
 *
 * @author sl
 */
public class ActionMenuViewPermissionTest {

    private ActionMenuViewPermission viewPermission;

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetNeueMeldungMenuItem(Rolle rolle) {
        setupViewPermission(rolle);

        ViewPermission result = viewPermission.getNeueMeldungMenuItem();
        boolean rendered = result.isRendered();

        switch (rolle) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
                MatcherAssert.assertThat(rendered, is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(rendered, is(Boolean.FALSE));
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetNeueAnforderungMenuItem(Rolle rolle) {
        setupViewPermission(rolle);

        ViewPermission result = viewPermission.getNeueAnforderungMenuItem();
        boolean rendered = result.isRendered();

        switch (rolle) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case TTEAMMITGLIED:
            case TTEAM_VERTRETER:
            case T_TEAMLEITER:
                MatcherAssert.assertThat(rendered, is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(rendered, is(Boolean.FALSE));
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetProjektverwaltungMenuItem(Rolle rolle) {
        setupViewPermission(rolle);

        ViewPermission result = viewPermission.getProjektverwaltungMenuItem();
        boolean rendered = result.isRendered();

        switch (rolle) {
            case ADMIN:
            case ANFORDERER:
                MatcherAssert.assertThat(rendered, is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(rendered, is(Boolean.FALSE));
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetAnforderungZuordnenMenuItem(Rolle rolle) {
        setupViewPermission(rolle);

        ViewPermission result = viewPermission.getAnforderungZuordnenMenuItem();
        boolean rendered = result.isRendered();

        switch (rolle) {
            case ADMIN:
            case ANFORDERER:
                MatcherAssert.assertThat(rendered, is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(rendered, is(Boolean.FALSE));
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetAnforderungVereinbarenMenuItem(Rolle rolle) {
        setupViewPermission(rolle);

        ViewPermission result = viewPermission.getAnforderungVereinbarenMenuItem();
        boolean rendered = result.isRendered();

        switch (rolle) {
            case ADMIN:
            case ANFORDERER:
                MatcherAssert.assertThat(rendered, is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(rendered, is(Boolean.FALSE));
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetBerichtswesenMenuItem(Rolle rolle) {
        setupViewPermission(rolle);

        ViewPermission result = viewPermission.getBerichtswesenMenuItem();
        boolean rendered = result.isRendered();

        switch (rolle) {
            case ADMIN:
            case ANFORDERER:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
                MatcherAssert.assertThat(rendered, is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(rendered, is(Boolean.FALSE));
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetUmsetzungsbestaetigungMenuItem(Rolle rolle) {
        setupViewPermission(rolle);

        ViewPermission result = viewPermission.getUmsetzungsbestaetigungMenuItem();
        boolean rendered = result.isRendered();

        switch (rolle) {
            case ADMIN:
            case ANFORDERER:
            case UMSETZUNGSBESTAETIGER:
                MatcherAssert.assertThat(rendered, is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(rendered, is(Boolean.FALSE));
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetReportingAnforderungMenuItem(Rolle rolle) {
        setupViewPermission(rolle);

        ViewPermission result = viewPermission.getReportingAnforderungMenuItem();
        boolean rendered = result.isRendered();

        switch (rolle) {
            case ADMIN:
            case ANFORDERER:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case SENSORCOCLEITER:
            case E_COC:
            case SCL_VERTRETER:
                MatcherAssert.assertThat(rendered, is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(rendered, is(Boolean.FALSE));
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetReportingProjektMenuItem(Rolle rolle) {
        setupViewPermission(rolle);

        ViewPermission result = viewPermission.getReportingProjektMenuItem();
        boolean rendered = result.isRendered();

        switch (rolle) {
            case ADMIN:
            case ANFORDERER:
                MatcherAssert.assertThat(rendered, is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(rendered, is(Boolean.FALSE));
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetNeuerProzessbaukastenMenuItem(Rolle rolle) {
        setupViewPermission(rolle);

        ViewPermission result = viewPermission.getNeuerProzessbaukastenMenuItem();
        boolean rendered = result.isRendered();

        switch (rolle) {
            case ADMIN:
            case TTEAM_VERTRETER:
            case T_TEAMLEITER:
                MatcherAssert.assertThat(rendered, is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(rendered, is(Boolean.FALSE));
        }
    }

    private void setupViewPermission(Rolle rolle) {
        List<Rolle> userRoles = Arrays.asList(rolle);
        viewPermission = new ActionMenuViewPermission(userRoles);
    }
}
