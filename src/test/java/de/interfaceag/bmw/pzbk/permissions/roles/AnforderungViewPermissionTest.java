package de.interfaceag.bmw.pzbk.permissions.roles;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungViewPermission;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author fn
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungViewPermissionTest {

    private AnforderungViewPermission viewPermission;

    private AnforderungViewPermission initViewPermission(Status status, Rolle role) {
        Set<Rolle> rolesWithWritePermissions = new HashSet<>();
        rolesWithWritePermissions.add(role);
        return new AnforderungViewPermission(rolesWithWritePermissions, status, true, false);
    }

    private AnforderungViewPermission initViewPermissionForProzessbaukastenZuordnung(Status status, Rolle role) {
        Set<Rolle> rolesWithWritePermissions = new HashSet<>();
        rolesWithWritePermissions.add(role);
        boolean anforderungHasProzessbaukatenZuordnung = true;
        return new AnforderungViewPermission(rolesWithWritePermissions, status, true, anforderungHasProzessbaukatenZuordnung);
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editButtonMEntwurftTest(Rolle role) {
        viewPermission = initViewPermission(Status.M_ENTWURF, role);

        switch (role) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
                assertTrue(viewPermission.getEditButton());
                break;
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editButtonMUnstimmigTest(Rolle role) {
        viewPermission = initViewPermission(Status.M_UNSTIMMIG, role);

        switch (role) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
                assertTrue(viewPermission.getEditButton());
                break;
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editButtonMGemeldetTest(Rolle role) {
        viewPermission = initViewPermission(Status.M_GEMELDET, role);

        switch (role) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                assertTrue(viewPermission.getEditButton());
                break;
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editButtonMZugeordnetTest(Rolle role) {
        viewPermission = initViewPermission(Status.M_ZUGEORDNET, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editButtonMGeloeschtTest(Rolle role) {
        viewPermission = initViewPermission(Status.M_GELOESCHT, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editButtonInArbeitTest(Rolle role) {
        viewPermission = initViewPermission(Status.A_INARBEIT, role);

        switch (role) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                assertTrue(viewPermission.getEditButton());
                break;
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editButtonUnstimmigTest(Rolle role) {
        viewPermission = initViewPermission(Status.A_UNSTIMMIG, role);

        switch (role) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                assertTrue(viewPermission.getEditButton());
                break;
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editButtonAbgestimmtTest(Rolle role) {
        viewPermission = initViewPermission(Status.A_FTABGESTIMMT, role);

        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAMMITGLIED:
            case TTEAM_VERTRETER:
                assertTrue(viewPermission.getEditButton());
                break;
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editButtonPlausibilisiertTest(Rolle role) {
        viewPermission = initViewPermission(Status.A_PLAUSIB, role);

        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAMMITGLIED:
            case TTEAM_VERTRETER:
                assertTrue(viewPermission.getEditButton());
                break;
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editButtonFreigegebenTest(Rolle role) {
        viewPermission = initViewPermission(Status.A_FREIGEGEBEN, role);

        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAMMITGLIED:
            case TTEAM_VERTRETER:
                assertTrue(viewPermission.getEditButton());
                break;
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editButtonKeineWeiterverfolgungTest(Rolle role) {
        viewPermission = initViewPermission(Status.A_KEINE_WEITERVERFOLG, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editButtonGeloeschtTest(Rolle role) {
        viewPermission = initViewPermission(Status.A_GELOESCHT, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZustandButtonButtonMEntwurftTest(Rolle role) {
        viewPermission = initViewPermissionForProzessbaukastenZuordnung(Status.M_ENTWURF, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getDerivatZustandButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZustandButtonMUnstimmigTest(Rolle role) {
        viewPermission = initViewPermissionForProzessbaukastenZuordnung(Status.M_UNSTIMMIG, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getDerivatZustandButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZustandButtonMGemeldetTest(Rolle role) {
        viewPermission = initViewPermissionForProzessbaukastenZuordnung(Status.M_GEMELDET, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getDerivatZustandButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZustandButtonMZugeordnetTest(Rolle role) {
        viewPermission = initViewPermissionForProzessbaukastenZuordnung(Status.M_ZUGEORDNET, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZustandButtonMGeloeschtTest(Rolle role) {
        viewPermission = initViewPermissionForProzessbaukastenZuordnung(Status.M_GELOESCHT, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZustandButtonInArbeitTest(Rolle role) {
        viewPermission = initViewPermissionForProzessbaukastenZuordnung(Status.A_INARBEIT, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getDerivatZustandButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZustandButtonUnstimmigTest(Rolle role) {
        viewPermission = initViewPermissionForProzessbaukastenZuordnung(Status.A_UNSTIMMIG, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getDerivatZustandButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZustandButtonAbgestimmtTest(Rolle role) {
        viewPermission = initViewPermissionForProzessbaukastenZuordnung(Status.A_FTABGESTIMMT, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getDerivatZustandButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZustandButtonPlausibilisiertTest(Rolle role) {
        viewPermission = initViewPermissionForProzessbaukastenZuordnung(Status.A_PLAUSIB, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getDerivatZustandButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZustandButtonFreigegebenTest(Rolle role) {
        viewPermission = initViewPermissionForProzessbaukastenZuordnung(Status.A_FREIGEGEBEN, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getDerivatZustandButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZustandButtonKeineWeiterverfolgungTest(Rolle role) {
        viewPermission = initViewPermissionForProzessbaukastenZuordnung(Status.A_KEINE_WEITERVERFOLG, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getDerivatZustandButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZustandButtonGeloeschtTest(Rolle role) {
        viewPermission = initViewPermissionForProzessbaukastenZuordnung(Status.A_GELOESCHT, role);

        switch (role) {
            default:
                assertFalse(viewPermission.getDerivatZustandButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZustandButtonFreigegebenWithoutZuordnungPZBKTest(Rolle role) {
        viewPermission = initViewPermission(Status.A_FREIGEGEBEN, role);

        switch (role) {
            case ADMIN:
            case ANFORDERER:
                assertTrue(viewPermission.getDerivatZuordnenButton());
                break;
            default:
                assertFalse(viewPermission.getDerivatZustandButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    void newVersionButtonAdminTest(Status status) {
        Rolle role = Rolle.ADMIN;
        viewPermission = initViewPermission(status, role);

        switch (status) {
            case A_INARBEIT:
            case A_GELOESCHT:
            case A_KEINE_WEITERVERFOLG:
            case A_FREIGEGEBEN:
            case A_PLAUSIB:
            case A_FTABGESTIMMT:
            case A_ANGELEGT_IN_PROZESSBAUKASTEN:
            case A_GENEHMIGT_IN_GUELTIGEM_PROZESSBAUKASTEN:
            case A_GENEHMIGT_IN_PROZESSBAUKASTEN:
                assertTrue(viewPermission.getNewVersionButton());
                break;
            default:
                assertFalse(viewPermission.getNewVersionButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    void newVersionButtonTteamLeiterTest(Status status) {
        Rolle role = Rolle.T_TEAMLEITER;
        viewPermission = initViewPermission(status, role);

        switch (status) {
            case A_INARBEIT:
            case A_GELOESCHT:
            case A_KEINE_WEITERVERFOLG:
            case A_FREIGEGEBEN:
            case A_PLAUSIB:
            case A_FTABGESTIMMT:
            case A_ANGELEGT_IN_PROZESSBAUKASTEN:
            case A_GENEHMIGT_IN_GUELTIGEM_PROZESSBAUKASTEN:
            case A_GENEHMIGT_IN_PROZESSBAUKASTEN:
                assertTrue(viewPermission.getNewVersionButton());
                break;
            default:
                assertFalse(viewPermission.getNewVersionButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    void newVersionButtonTteamVertreterTest(Status status) {
        Rolle role = Rolle.TTEAM_VERTRETER;
        viewPermission = initViewPermission(status, role);

        switch (status) {
            case A_INARBEIT:
            case A_GELOESCHT:
            case A_KEINE_WEITERVERFOLG:
            case A_FREIGEGEBEN:
            case A_PLAUSIB:
            case A_FTABGESTIMMT:
            case A_ANGELEGT_IN_PROZESSBAUKASTEN:
            case A_GENEHMIGT_IN_GUELTIGEM_PROZESSBAUKASTEN:
            case A_GENEHMIGT_IN_PROZESSBAUKASTEN:
                assertTrue(viewPermission.getNewVersionButton());
                break;
            default:
                assertFalse(viewPermission.getNewVersionButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    void newVersionButtonTteamMitgliedTest(Status status) {
        Rolle role = Rolle.TTEAMMITGLIED;
        viewPermission = initViewPermission(status, role);
        assertFalse(viewPermission.getNewVersionButton());
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    void newVersionButtonSensorCocLeiterTest(Status status) {
        Rolle role = Rolle.SENSORCOCLEITER;
        viewPermission = initViewPermission(status, role);

        switch (status) {
            case A_INARBEIT:
            case A_GELOESCHT:
            case A_KEINE_WEITERVERFOLG:
            case A_FREIGEGEBEN:
            case A_PLAUSIB:
            case A_FTABGESTIMMT:
                assertTrue(viewPermission.getNewVersionButton());
                break;
            default:
                assertFalse(viewPermission.getNewVersionButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    void newVersionButtonSensorCocVertreterTest(Status status) {
        Rolle role = Rolle.SCL_VERTRETER;
        viewPermission = initViewPermission(status, role);

        switch (status) {
            case A_INARBEIT:
            case A_GELOESCHT:
            case A_KEINE_WEITERVERFOLG:
            case A_FREIGEGEBEN:
            case A_PLAUSIB:
            case A_FTABGESTIMMT:
                assertTrue(viewPermission.getNewVersionButton());
                break;
            default:
                assertFalse(viewPermission.getNewVersionButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    void newVersionButtonSensorTest(Status status) {
        Rolle role = Rolle.SENSOR;
        viewPermission = initViewPermission(status, role);
        assertFalse(viewPermission.getNewVersionButton());
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    void newVersionButtonAnfordererTest(Status status) {
        Rolle role = Rolle.ANFORDERER;
        viewPermission = initViewPermission(status, role);
        assertFalse(viewPermission.getNewVersionButton());
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    void newVersionButtonUmsetzungsbestaetigerTest(Status status) {
        Rolle role = Rolle.UMSETZUNGSBESTAETIGER;
        viewPermission = initViewPermission(status, role);
        assertFalse(viewPermission.getNewVersionButton());
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    void newVersionButtonEcocTest(Status status) {
        Rolle role = Rolle.E_COC;
        viewPermission = initViewPermission(status, role);
        assertFalse(viewPermission.getNewVersionButton());
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    void newVersionButtonSensorEingeschraenktTest(Status status) {
        Rolle role = Rolle.SENSOR_EINGESCHRAENKT;
        viewPermission = initViewPermission(status, role);
        assertFalse(viewPermission.getNewVersionButton());
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    void newVersionButtonLeserTest(Status status) {
        Rolle role = Rolle.LESER;
        viewPermission = initViewPermission(status, role);
        assertFalse(viewPermission.getNewVersionButton());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void wiederHerstellenButtonTest(Rolle role) {
        Status[] status = Status.values();
        for (Status s : status) {
            viewPermission = initViewPermission(s, role);
            switch (s) {

                case M_GELOESCHT:
                case A_GELOESCHT:
                    switch (role) {
                        case ADMIN:
                        case SENSORCOCLEITER:
                        case SCL_VERTRETER:
                        case TTEAM_VERTRETER:
                        case T_TEAMLEITER:
                            assertTrue(viewPermission.getWiederherstellenButton());
                            break;
                        default:
                            assertFalse(viewPermission.getWiederherstellenButton());
                            break;
                    }
                    break;

                default:
                    switch (role) {
                        default:
                            assertFalse(viewPermission.getWiederherstellenButton());
                            break;
                    }
                    break;
            }

        }

    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void meldungZuordnenButtonTest(Rolle role) {
        Status[] status = Status.values();
        for (Status s : status) {
            viewPermission = initViewPermission(s, role);
            switch (s) {
                case A_INARBEIT:
                case A_FTABGESTIMMT:
                case A_PLAUSIB:
                case A_FREIGEGEBEN:
                case A_UNSTIMMIG:
                    switch (role) {
                        case ADMIN:
                        case SENSORCOCLEITER:
                        case SCL_VERTRETER:
                            assertTrue(viewPermission.getMeldungZuordnenButton());
                            break;
                        default:
                            assertFalse(viewPermission.getMeldungZuordnenButton());
                            break;
                    }
                    break;
                default:
                    assertFalse(viewPermission.getMeldungZuordnenButton());
                    break;
            }

        }

    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void derivatZuordnenButtonTest(Rolle role) {
        Status[] status = Status.values();
        for (Status s : status) {
            viewPermission = initViewPermission(s, role);
            switch (s) {
                case A_FREIGEGEBEN:
                    switch (role) {
                        case ADMIN:
                        case ANFORDERER:
                            assertTrue(viewPermission.getDerivatZuordnenButton());
                            break;
                        default:
                            assertFalse(viewPermission.getDerivatZuordnenButton());
                            break;
                    }
                    break;
                default:
                    assertFalse(viewPermission.getDerivatZuordnenButton());
                    break;
            }

        }

    }

}
