package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class RolePermissionForNoRoleTest extends AbstractRolePermissionTest {

    @Before
    public void initRolePermission() {
        Rolle readRole = Rolle.SENSOR;
        Rolle writeRole = Rolle.SENSORCOCLEITER;
        initPermission(readRole, writeRole);
    }

    @Test
    public void testViewPermission() {
        Assert.assertFalse(getPermission().isRendered());
    }

    @Test
    public void testEditPermission() {
        Assert.assertFalse(getPermission().hasRightToEdit());
    }

}
