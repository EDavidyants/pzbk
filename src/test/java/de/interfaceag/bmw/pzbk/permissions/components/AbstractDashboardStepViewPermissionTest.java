package de.interfaceag.bmw.pzbk.permissions.components;

import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractDashboardStepViewPermissionTest {

    private DashboardStepViewPermission viewPermission;
    private final List<Rolle> userRoles = new ArrayList<>();

    protected void initPermission(Rolle role) {
        userRoles.add(role);
        viewPermission = new DashboardStepViewPermission(userRoles);
    }

    public boolean getMenuButton() {
        return viewPermission.getMenuButton().isRendered();
    }

    public boolean getStepOneToFourPanel() {
        return viewPermission.getStepOneToFourPanel().isRendered();
    }

    public boolean getStepOne() {
        return viewPermission.getStepOne().isRendered();
    }

    public boolean getStepTwo() {
        return viewPermission.getStepTwo().isRendered();
    }

    public boolean getStepThree() {
        return viewPermission.getStepThree().isRendered();
    }

    public boolean getStepFour() {
        return viewPermission.getStepFour().isRendered();
    }

    public boolean getStepFiveToSevenPanel() {
        return viewPermission.getStepFiveToSevenPanel().isRendered();
    }

    public boolean getStepFive() {
        return viewPermission.getStepFive().isRendered();
    }

    public boolean getStepSix() {
        return viewPermission.getStepSix().isRendered();
    }

    public boolean getStepEightToThirteenPanel() {
        return viewPermission.getStepEightToThirteenPanel().isRendered();
    }

    public boolean getStepEight() {
        return viewPermission.getStepEight().isRendered();
    }

    public boolean getStepNine() {
        return viewPermission.getStepNine().isRendered();
    }

    public boolean getStepTen() {
        return viewPermission.getStepTen().isRendered();
    }

    public boolean getStepEleven() {
        return viewPermission.getStepEleven().isRendered();
    }

    public boolean getStepTwelve() {
        return viewPermission.getStepTwelve().isRendered();
    }

    public boolean getStepThirteen() {
        return viewPermission.getStepThirteen().isRendered();
    }

}
