package de.interfaceag.bmw.pzbk.permissions.components;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class PersonalMenuRoleAdminViewPermissionTest extends AbstractPersonalMenuViewPermissionTest {

    @Before
    public void initPermission() {
        initPermission(Rolle.ADMIN);
    }

    @Test
    public void testConfigViewPermission() {
        Assert.assertTrue(getConfig());
    }

    @Test
    public void testSystemLogViewPermission() {
        Assert.assertTrue(getSystemLog());
    }

    @Test
    public void testBenutzerverwaltungViewPermission() {
        Assert.assertTrue(getBenutzerverwaltung());
    }

    @Test
    public void testAntragsverwaltungViewPermission() {
        Assert.assertTrue(getAntrasverwaltung());
    }

}
