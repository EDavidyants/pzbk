package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class RolePermissionForOneReadRoleTest extends AbstractRolePermissionTest {

    @Before
    public void initRolePermission() {
        Rolle readRole = Rolle.SENSOR;
        Rolle writeRole = Rolle.SENSORCOCLEITER;
        Set<Rolle> userRoles = Stream.of(Rolle.SENSOR).collect(Collectors.toSet());
        initPermission(readRole, writeRole, userRoles);
    }

    @Test
    public void testViewPermission() {
        Assert.assertTrue(getPermission().isRendered());
    }

    @Test
    public void testEditPermission() {
        Assert.assertFalse(getPermission().hasRightToEdit());
    }

}
