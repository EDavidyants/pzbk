package de.interfaceag.bmw.pzbk.permissions;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FalsePermissionTest {

    private EditViewPermission permission;

    @Before
    public void initRolePermission() {
        permission = new FalsePermission();
    }

    @Test
    public void testViewPermission() {
        Assert.assertFalse(permission.isRendered());
    }

    @Test
    public void testEditPermission() {
        Assert.assertFalse(permission.hasRightToEdit());
    }

}
