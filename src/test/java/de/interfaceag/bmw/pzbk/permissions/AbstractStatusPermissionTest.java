package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.Status;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractStatusPermissionTest {

    private EditViewPermission permission;

    protected void initPermission(Status readStatus, Status writeStatus, Status currentStatus) {
        permission = StatusPermission.builder()
                .authorizeRead(readStatus)
                .authorizeWrite(writeStatus)
                .compareTo(currentStatus).get();
    }

    protected EditViewPermission getPermission() {
        return permission;
    }

}
