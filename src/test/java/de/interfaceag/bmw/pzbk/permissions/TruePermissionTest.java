package de.interfaceag.bmw.pzbk.permissions;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class TruePermissionTest {

    private EditViewPermission permission;

    @Before
    public void initRolePermission() {
        permission = new TruePermission();
    }

    @Test
    public void testViewPermission() {
        Assert.assertTrue(permission.isRendered());
    }

    @Test
    public void testEditPermission() {
        Assert.assertTrue(permission.hasRightToEdit());
    }

}
