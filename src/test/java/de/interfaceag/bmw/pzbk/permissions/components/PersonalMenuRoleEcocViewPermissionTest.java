package de.interfaceag.bmw.pzbk.permissions.components;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class PersonalMenuRoleEcocViewPermissionTest extends AbstractPersonalMenuViewPermissionTest {

    @Before
    public void initPermission() {
        initPermission(Rolle.E_COC);
    }

    @Test
    public void testConfigViewPermission() {
        Assert.assertFalse(getConfig());
    }

    @Test
    public void testSystemLogViewPermission() {
        Assert.assertFalse(getSystemLog());
    }

    @Test
    public void testBenutzerverwaltungViewPermission() {
        Assert.assertFalse(getBenutzerverwaltung());
    }

    @Test
    public void testAntragsverwaltungViewPermission() {
        Assert.assertFalse(getAntrasverwaltung());
    }

}
