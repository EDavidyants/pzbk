package de.interfaceag.bmw.pzbk.permissions.components;

import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractPersonalMenuViewPermissionTest {

    private PersonalMenuViewPermission viewPermission;
    private final List<Rolle> userRoles = new ArrayList<>();

    protected void initPermission(Rolle role) {
        userRoles.add(role);
        viewPermission = new PersonalMenuViewPermission(userRoles);
    }

    public boolean getBenutzerverwaltung() {
        return viewPermission.getBenutzerverwaltung().isRendered();
    }

    public boolean getAntrasverwaltung() {
        return viewPermission.getAntragsverwaltung().isRendered();
    }

    public boolean getSystemLog() {
        return viewPermission.getSystemLog().isRendered();
    }

    public boolean getConfig() {
        return viewPermission.getConfig().isRendered();
    }



}
