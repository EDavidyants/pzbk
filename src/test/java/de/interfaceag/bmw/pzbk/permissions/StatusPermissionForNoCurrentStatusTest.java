package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.Status;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class StatusPermissionForNoCurrentStatusTest extends AbstractStatusPermissionTest {

    @Before
    public void initRolePermission() {
        Status readStatus = Status.M_GEMELDET;
        Status writeStatus = Status.M_ENTWURF;
        Status currentStatus = null;
        initPermission(readStatus, writeStatus, currentStatus);
    }

    @Test
    public void testPermissionIsNull() {
        Assert.assertNull(getPermission());
    }

}
