package de.interfaceag.bmw.pzbk.permissions.components;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class PersonalMenuRoleSensorcocLeiterViewPermissionTest extends AbstractPersonalMenuViewPermissionTest {

    @Before
    public void initPermission() {
        initPermission(Rolle.SENSORCOCLEITER);
    }

    @Test
    public void testConfigViewPermission() {
        Assert.assertFalse(getConfig());
    }

    @Test
    public void testSystemLogViewPermission() {
        Assert.assertFalse(getSystemLog());
    }

    @Test
    public void testBenutzerverwaltungViewPermission() {
        Assert.assertTrue(getBenutzerverwaltung());
    }

    @Test
    public void testAntragsverwaltungViewPermission() {
        Assert.assertTrue(getAntrasverwaltung());
    }

}
