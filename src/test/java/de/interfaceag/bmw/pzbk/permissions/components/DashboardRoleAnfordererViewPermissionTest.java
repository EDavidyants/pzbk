package de.interfaceag.bmw.pzbk.permissions.components;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class DashboardRoleAnfordererViewPermissionTest extends AbstractDashboardStepViewPermissionTest {

    @Before
    public void initPermission() {
        initPermission(Rolle.ANFORDERER);
    }

    @Test
    public void testStepOneToFourPanel() {
        Assert.assertTrue(getStepOneToFourPanel());
    }

    @Test
    public void testStepOne() {
        Assert.assertFalse(getStepOne());
    }

    @Test
    public void testStepTwo() {
        Assert.assertFalse(getStepTwo());
    }

    @Test
    public void testStepThree() {
        Assert.assertFalse(getStepThree());
    }

    @Test
    public void testStepFour() {
        Assert.assertTrue(getStepFour());
    }

    @Test
    public void testStepFiveToSevenPanel() {
        Assert.assertFalse(getStepFiveToSevenPanel());
    }

    @Test
    public void testStepFive() {
        Assert.assertFalse(getStepFive());
    }

    @Test
    public void testStepSix() {
        Assert.assertFalse(getStepSix());
    }

    @Test
    public void testStepEightToThirteenPanel() {
        Assert.assertTrue(getStepEightToThirteenPanel());
    }

    @Test
    public void testStepEight() {
        Assert.assertFalse(getStepEight());
    }

    @Test
    public void testStepNine() {
        Assert.assertFalse(getStepNine());
    }

    @Test
    public void testStepTen() {
        Assert.assertFalse(getStepTen());
    }

    @Test
    public void testStepEleven() {
        Assert.assertTrue(getStepEleven());
    }

    @Test
    public void testStepTwelve() {
        Assert.assertTrue(getStepTwelve());
    }

    @Test
    public void testStepThirteen() {
        Assert.assertFalse(getStepThirteen());
    }

}
