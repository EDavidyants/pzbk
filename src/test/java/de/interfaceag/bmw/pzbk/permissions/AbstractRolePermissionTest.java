package de.interfaceag.bmw.pzbk.permissions;

import de.interfaceag.bmw.pzbk.enums.Rolle;

import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public abstract class AbstractRolePermissionTest {

    private EditViewPermission permission;

    protected void initPermission(Rolle readRole, Rolle writeRole) {
        permission = RolePermission.builder()
                .authorizeReadForRole(readRole)
                .authorizeWriteForRole(writeRole).get();
    }

    protected void initPermission(Rolle readRole, Rolle writeRole, Set<Rolle> userRoles) {
        permission = RolePermission.builder()
                .authorizeReadForRole(readRole)
                .authorizeWriteForRole(writeRole)
                .compareTo(userRoles).get();
    }

    protected EditViewPermission getPermission() {
        return permission;
    }

}
