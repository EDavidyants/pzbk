package de.interfaceag.bmw.pzbk.permissions.pages;

import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewPermission;
import org.junit.Assert;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class UmsetzungsbestaetitgungAnfordererViewPermissionTest {

    private static final Rolle ROLLE = Rolle.ANFORDERER;

    private UmsetzungsbestaetigungViewPermission viewPermission;

    private HttpServletRequest request;

    public UmsetzungsbestaetigungViewPermission initViewPermission(KovAStatus status, Boolean editMode) {
        Set<Rolle> rolesWithWritePermissions = new HashSet<>();
        rolesWithWritePermissions.add(ROLLE);
        request = mock(HttpServletRequest.class);
        when(request.getParameterMap()).thenReturn(new HashMap<>());
        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("editMode", editMode.toString());
        return new UmsetzungsbestaetigungViewPermission(rolesWithWritePermissions, status, urlParameter);
    }

    @Test
    public void viewPageReadModeTest() {
        viewPermission = initViewPermission(KovAStatus.AKTIV, false);
        Assert.assertTrue(viewPermission.getPage());
    }

    @Test
    public void viewPageEditModeTest() {
        viewPermission = initViewPermission(KovAStatus.AKTIV, true);
        Assert.assertTrue(viewPermission.getPage());
    }

    @Test
    public void viewEditButtonViewModeTest() {
        viewPermission = initViewPermission(KovAStatus.AKTIV, false);
        Assert.assertFalse(viewPermission.getEditButton());
    }

    @Test
    public void viewEditButtonEditModeTest() {
        viewPermission = initViewPermission(KovAStatus.AKTIV, true);
        Assert.assertFalse(viewPermission.getEditButton());
    }

    @Test
    public void viewProcessChangesButtonViewModeTest() {
        viewPermission = initViewPermission(KovAStatus.AKTIV, false);
        Assert.assertFalse(viewPermission.getProcessChangesButton());
    }

    @Test
    public void viewProcessChangesButtonEditModeTest() {
        viewPermission = initViewPermission(KovAStatus.AKTIV, true);
        Assert.assertFalse(viewPermission.getProcessChangesButton());
    }

    @Test
    public void viewCancelButtonViewModeTest() {
        viewPermission = initViewPermission(KovAStatus.AKTIV, false);
        Assert.assertFalse(viewPermission.getProcessChangesButton());
    }

    @Test
    public void viewCancelButtonEditModeTest() {
        viewPermission = initViewPermission(KovAStatus.AKTIV, true);
        Assert.assertFalse(viewPermission.getProcessChangesButton());
    }

}
