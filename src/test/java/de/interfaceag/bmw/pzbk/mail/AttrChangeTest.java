package de.interfaceag.bmw.pzbk.mail;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungHistoryDto;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Christian Schauer <christian.schauer at interface-ag.de>
 */
public class AttrChangeTest {

    private static final Logger LOG = LoggerFactory.getLogger(AttrChangeTest.class.getName());

    public AttrChangeTest() {
    }

    private AttrChange createChangeByDateAndAttribut(String date, String attribut) {
        String id = "1";
        String oldValue = "old";
        String newValue = "new";
        //String d = "25/01/2017 15:34:27";
        Date datum = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:MM:ss");
        try {
            datum = sdf.parse(date);
        } catch (ParseException ex) {
            LOG.error(null, ex);
        }
        String benutzer = "Änderer 1";

        AnforderungHistoryDto change = AnforderungHistoryDto.newBuilder()
                .withAnforderungMeldungId(Long.parseLong(id))
                .withKennzeichen("A")
                .withZeitpunkt(datum)
                .withAttribut(attribut)
                .withBenutzer(benutzer)
                .withVon(oldValue)
                .withAuf(newValue)
                .build();

        AttrChange a = new AttrChange(change);
        return a;
    }

    /**
     * Test of compareTo method, of class AttrChange.
     */
    @Test
    public void testCompareTo() {
        AttrChange a1a = createChangeByDateAndAttribut("25/01/2017 15:34:27", "a1a");
        AttrChange a1b = createChangeByDateAndAttribut("25/01/2017 15:34:27", "a1b");
        AttrChange a2 = createChangeByDateAndAttribut("25/01/2017 16:34:27", "a2");

        int expResult = 0;
        int result = a1a.compareTo(a1b);
        assertEquals(expResult, result);
        assertTrue(a1a.compareTo(a2) < 0);
    }

    @Test
    public void testGetLastChanges() {
        AttrChange a1a = createChangeByDateAndAttribut("25/01/2017 15:34:27", "a1a");
        AttrChange a1b = createChangeByDateAndAttribut("25/01/2017 15:34:27", "a1b");
        AttrChange a2 = createChangeByDateAndAttribut("25/01/2017 14:34:27", "a2");
        SortedSet<AttrChange> changes = new TreeSet<>(new AttrChangeComparator());
        changes.add(a1a);
        changes.add(a2);
        changes.add(a1b);

        Collection<AttrChange> lastChanges = AttrChange.getLastChanges(changes);

        assertEquals(2, lastChanges.size());

        assertTrue(lastChanges.contains(a1a));
        assertTrue(lastChanges.contains(a1b));
        assertFalse(lastChanges.contains(a2));
    }
}
