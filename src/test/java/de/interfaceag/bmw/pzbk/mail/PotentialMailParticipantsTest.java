package de.interfaceag.bmw.pzbk.mail;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.Test;

import java.util.Collection;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class PotentialMailParticipantsTest {

    public PotentialMailParticipantsTest() {
    }

    /**
     * Test of getEmailByRolle method, of class PotentialMailParticipants.
     */
    @Test
    public void testAddAndGetMitarbeiterByRolle() {
        IPotentialMailParticipants pmp = new PotentialMailParticipants();

        assertEquals(pmp.getEmailByRolle(Rolle.SENSOR), null);
        String email = "test@mail.de";
        Mitarbeiter m1 = new Mitarbeiter();
        Mitarbeiter m2 = new Mitarbeiter();
        m1.setEmail(email);
        m2.setEmail(email);
        pmp.addMitarbeiter(m1, Rolle.SENSOR);

        assertEquals(pmp.getEmailByRolle(Rolle.SENSOR), m2.getEmail());
    }

    /**
     * Test of getAllEmailsByRolle method, of class PotentialMailParticipants.
     */
    @Test
    public void testAddAndGetAllMitarbeiterByRolle() {
        IPotentialMailParticipants pmp = new PotentialMailParticipants();
        Mitarbeiter m1 = new Mitarbeiter();
        Mitarbeiter m2 = new Mitarbeiter();
        m1.setQNumber("1");
        m2.setQNumber("2");
        m1.setEmail("test1@mail.de");
        m2.setEmail("test2@mail.de");
        pmp.addMitarbeiter(m1, Rolle.SENSOR);
        pmp.addMitarbeiter(m2, Rolle.SENSOR);

        assertEquals(pmp.getAllEmailsByRolle(Rolle.SENSOR).size(), 2);

        assertTrue(pmp.getAllEmailsByRolle(Rolle.SENSOR).contains(m1.getEmail()));

        assertTrue(pmp.getAllEmailsByRolle(Rolle.SENSOR).contains(m2.getEmail()));

    }

    /**
     * Test of addMitarbeiter method, of class PotentialMailParticipants.
     */
    @Test
    public void testAddMitarbeiter_Collection_Rolle() {
        IPotentialMailParticipants pmp = new PotentialMailParticipants();
        Collection<Mitarbeiter> mColl = new HashSet<>();
        Mitarbeiter m1 = new Mitarbeiter();
        Mitarbeiter m2 = new Mitarbeiter();
        m1.setQNumber("1");
        m2.setQNumber("2");
        m1.setEmail("test1@mail.de");
        m2.setEmail("test2@mail.de");
        mColl.add(m1);
        mColl.add(m2);
        mColl.add(m2);

        pmp.addMitarbeiter(mColl, Rolle.SENSOR);

        assertEquals(pmp.getAllEmailsByRolle(Rolle.SENSOR).size(), 2);

        assertTrue(pmp.getAllEmailsByRolle(Rolle.SENSOR).contains(m1.getEmail()));

        assertTrue(pmp.getAllEmailsByRolle(Rolle.SENSOR).contains(m2.getEmail()));
    }
}
