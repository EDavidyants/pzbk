package de.interfaceag.bmw.pzbk.mail;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class MailParticipantsTest {

    private static final Logger LOG = LoggerFactory.getLogger(MailParticipantsTest.class.getName());

    public MailParticipantsTest() {
    }

    /**
     * Test of addRecipientMail method, of class MailParticipants.
     */
    @Test
    public void testAddRecipient_Mitarbeiter() {
        LOG.info("addRecipientMail");
        Mitarbeiter m = TestDataFactory.createMitarbeiter("m");
        m.setEmail("mail@test.de");
        m.setMailReceiptEnabled(true);
        MailParticipants mp = new MailParticipants();
        mp.addRecipient(m);

        assertEquals(1, mp.getRecipients().size());
    }

    /**
     * Test of addRecipientMail method, of class MailParticipants.
     */
    //@Test
//    public void testAddRecipient_Collection() {
//        LOG.info("addRecipientMail");
//        final int anzMitarbeiter = 5;
//        Collection<Mitarbeiter> mitarbeiters = TestDataFactory.createMitarbeiter(anzMitarbeiter, "m");
//        MailParticipants mp = new MailParticipants();
//
//        mp.addRecipientMail(mitarbeiters);
//
//        Collection<Mitarbeiter> mitarbeitersFromMp = mp.getRecipients();
//
//        assertEquals(anzMitarbeiter, mitarbeitersFromMp.size());
//
//        for (Mitarbeiter m : mitarbeiters) {
//            assertTrue(mitarbeitersFromMp.contains(m));
//        }
//
//        Mitarbeiter m = TestDataFactory.createMitarbeiter("m");
//        m.setMailReceiptEnabled(false);
//        mp.addRecipientMail(m);
//        assertEquals(anzMitarbeiter, mitarbeitersFromMp.size());
//
//    }
    /**
     * Test of addCC method, of class MailParticipants.
     */
    @Test
    public void testAddCC_Mitarbeiter() {
        LOG.info("addCC");
        String m = "mail@test.de";
        MailParticipants mp = new MailParticipants();
        mp.addCC(m);

        assertEquals(1, mp.getCcRecipients().size());
        assertEquals(m, mp.getCcRecipients().iterator().next());
    }

    /**
     * Test of addCC method, of class MailParticipants.
     */
//    //@Test
//    public void testAddCC_Collection() {
//        LOG.info("addCC");
//        final int anzMitarbeiter = 5;
//        Collection<Mitarbeiter> mitarbeiters = TestDataFactory.createMitarbeiter(anzMitarbeiter, "m");
//        MailParticipants mp = new MailParticipants();
//
//        mp.addCC(mitarbeiters);
//
//        Collection<Mitarbeiter> mitarbeitersFromMp = mp.getCcRecipients();
//
//        assertEquals(anzMitarbeiter, mitarbeitersFromMp.size());
//
//        for (Mitarbeiter m : mitarbeiters) {
//            assertTrue(mitarbeitersFromMp.contains(m));
//        }
//
//        Mitarbeiter m = TestDataFactory.createMitarbeiter("m");
//        m.setMailReceiptEnabled(false);
//        mp.addCC(m);
//        assertEquals(anzMitarbeiter, mitarbeitersFromMp.size());
//    }
    /**
     * Test of addBCC method, of class MailParticipants.
     */
    @Test
    public void testAddBCC() {
        LOG.info("addBCC");
        String m = "mail@test.de";
        MailParticipants mp = new MailParticipants();
        mp.addBCC(m);

        assertEquals(1, mp.getBccRecipients().size());
        assertEquals(m, mp.getBccRecipients().iterator().next());
    }

    /**
     * Test of addBCC method, of class MailParticipants.
     */
    //@Test
//    public void testAddBCC_Collection() {
//        LOG.info("addBCC");
//        final int anzMitarbeiter = 5;
//        Collection<Mitarbeiter> mitarbeiters = TestDataFactory.createMitarbeiter(anzMitarbeiter, "m");
//        MailParticipants mp = new MailParticipants();
//
//        mp.addBCC(mitarbeiters);
//
//        Collection<String> mitarbeitersFromMp = mp.getBccRecipients();
//
//        assertEquals(anzMitarbeiter, mitarbeitersFromMp.size());
//
//        for (Mitarbeiter m : mitarbeiters) {
//            assertTrue(mitarbeitersFromMp.contains(m));
//        }
//
//        Mitarbeiter m = TestDataFactory.createMitarbeiter("m");
//        m.setMailReceiptEnabled(false);
//        mp.addBCC(m);
//        assertEquals(anzMitarbeiter, mitarbeitersFromMp.size());
//    }
    /**
     * Test of removeRecipient method, of class MailParticipants.
     */
    @Test
    public void testRemoveRecipient() {
        LOG.info("removeRecipient");
        String m = "mail@test.de";
        MailParticipants mp = new MailParticipants();
        mp.addRecipient(m);
        assertEquals(1, mp.getRecipients().size());
        mp.removeRecipient(m);
        assertTrue(mp.getRecipients().isEmpty());
    }

    /**
     * Test of removeCC method, of class MailParticipants.
     */
    @Test
    public void testRemoveCC() {
        LOG.info("removeCC");
        String m = "mail@test.de";
        MailParticipants mp = new MailParticipants();
        mp.addCC(m);
        assertEquals(1, mp.getCcRecipients().size());
        mp.removeCC(m);
        assertTrue(mp.getCcRecipients().isEmpty());
    }

    /**
     * Test of removeBCC method, of class MailParticipants.
     */
    @Test
    public void testRemoveBCC() {
        LOG.info("removeBCC");
        String m = "mail@test.de";
        MailParticipants mp = new MailParticipants();
        mp.addBCC(m);
        assertEquals(1, mp.getBccRecipients().size());
        mp.removeBCC(m);
        assertTrue(mp.getBccRecipients().isEmpty());
    }
}
