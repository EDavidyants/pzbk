package de.interfaceag.bmw.pzbk.mail;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class MailArgsTest {

    public MailArgsTest() {
    }

    /**
     * Test of toStringArr method, of class MailArgs.
     */
    @Test
    public void testToStringArr() {
        MailArgs args = new MailArgs();
        args.putValue("3", "pos 3");
        String[] arr = args.toStringArr();
        assertEquals(arr.length, 1);
    }

    @Test
    public void testGetMaxKey() {
        MailArgs args = new MailArgs();
        args.putValue("0", "pos 0");
        args.putValue("3", "pos 3");
        assertEquals(2, args.size());
    }

}
