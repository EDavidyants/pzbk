package de.interfaceag.bmw.pzbk.mail;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class MailProcessorTest {

    public MailProcessorTest() {
    }

    /**
     * Test of generateContent method, of class MailProcessor.
     */
    @Test
    public void testGenerateContent() {
        MailArgs args = new MailArgs();
        String word1 = "Hello";
        args.putValue("0", "World");
        String expResult = "Hello $" + args.getValueFor("0") + "!";
        String text = word1 + " $${0}!";

        MailProcessor mu = new MailProcessor('$');

        InputStream is = new ByteArrayInputStream(text.getBytes(StandardCharsets.UTF_8));
        String result = mu.generateContent(is, args);
        assertEquals(expResult, result);
    }

    /**
     * Test of replaceByOrder method, of class MailProcessor.
     */
    @Test
    public void testReplaceByOrder() {
        MailArgs args = new MailArgs();
        args.putValue("15", "TEST");
        String escape = "${15}";
        MailProcessor mp = new MailProcessor('$');
        String result = mp.replaceByOrder(escape, args);
        assertEquals("TEST", result);
    }

}
