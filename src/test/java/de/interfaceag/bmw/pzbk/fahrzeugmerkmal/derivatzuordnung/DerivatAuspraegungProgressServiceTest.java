package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalDao;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmaleProgressDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class DerivatAuspraegungProgressServiceTest {

    @Mock
    private FahrzeugmerkmalDao fahrzeugmerkmalDao;

    @Mock
    private DerivatFahrzeugmerkmalDao derivatFahrzeugmerkmalDao;

    @InjectMocks
    private FahrzeugmerkmaleProgressService progressService;

    @Mock
    private Fahrzeugmerkmal fahrzeugmerkmal1;
    @Mock
    private Fahrzeugmerkmal fahrzeugmerkmal2;
    @Mock
    private Fahrzeugmerkmal fahrzeugmerkmal3;

    @Mock
    private Derivat derivat1;
    @Mock
    private Derivat derivat2;

    List<Fahrzeugmerkmal> fahrzeugmerkmale;
    List<Derivat> derivate;

    @Test
    public void testGetTotalNumberOfFahrzeugmerkmale() {
        fahrzeugmerkmale = Arrays.asList(fahrzeugmerkmal1, fahrzeugmerkmal2, fahrzeugmerkmal3);
        when(fahrzeugmerkmalDao.getAll()).thenReturn(fahrzeugmerkmale);
        int totalFahrzeugmerkmale = progressService.getTotalNumberOfFahrzeugmerkmale();
        assertThat(totalFahrzeugmerkmale, is(3));
    }

    @Test
    public void testBuildDerivatAuspraegungenMapForDerivateForNullCollection() {
        derivate = null;
        Collection<FahrzeugmerkmaleProgressDto> result = progressService.generateFahrzeugmerkmaleProgressDtosForDerivate(derivate);
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void testBuildDerivatAuspraegungenMapForDerivateForEmptyCollection() {
        derivate = Collections.emptyList();
        Collection<FahrzeugmerkmaleProgressDto> result = progressService.generateFahrzeugmerkmaleProgressDtosForDerivate(derivate);
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void testBuildDerivatAuspraegungenMapForDerivateForDerivatCollection() {
        doReturn(1L).when(derivat1).getId();
        doReturn(2L).when(derivat2).getId();
        derivate = Arrays.asList(derivat1, derivat2);
        List<Long> derivatIds = Arrays.asList(1L, 2L);
        Collection<FahrzeugmerkmaleProgressDto> expectedResult = new HashSet<>();
        expectedResult.add(new FahrzeugmerkmaleProgressDto(1L, 2L));
        expectedResult.add(new FahrzeugmerkmaleProgressDto(2L, 1L));
        when(derivatFahrzeugmerkmalDao.getNumberOfPersistedFahrzeugmerkmaleForDerivatIds(derivatIds)).thenReturn(expectedResult);
        Collection<FahrzeugmerkmaleProgressDto> result = progressService.generateFahrzeugmerkmaleProgressDtosForDerivate(derivate);
        Assertions.assertEquals(2, result.size());
        assertTrue(isElementInCollection(expectedResult, new FahrzeugmerkmaleProgressDto(1L, 2L)));
        assertTrue(isElementInCollection(expectedResult, new FahrzeugmerkmaleProgressDto(2L, 1L)));
    }

    private boolean isElementInCollection(Collection<FahrzeugmerkmaleProgressDto> elements, FahrzeugmerkmaleProgressDto searchedForElement) {
        Optional<FahrzeugmerkmaleProgressDto> element = elements.stream()
                .filter(dto -> dto.equals(searchedForElement)).findAny();
        return element.isPresent();
    }

}
