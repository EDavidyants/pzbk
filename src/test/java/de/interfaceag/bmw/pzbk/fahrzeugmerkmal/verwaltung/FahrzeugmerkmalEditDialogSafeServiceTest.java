package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung;

import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalEditDialogData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalAuspraegungDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalModulSeTeamDto;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalEditDialogSafeServiceTest {

    private static final String MERKMAL = "Merkmal";

    @Mock
    private FahrzeugmerkmalService fahrzeugmerkmalService;
    @Mock
    private ModulService modulService;
    @InjectMocks
    private FahrzeugmerkmalEditDialogSafeService safeService;

    @Mock
    private FahrzeugmerkmalEditDialogData editDialogData;

    private Fahrzeugmerkmal fahrzeugmerkmal;

    @Mock
    private FahrzeugmerkmalModulSeTeamDto fahrzeugmerkmalModulSeTeamDto;
    private List<FahrzeugmerkmalModulSeTeamDto> dialogDataModulSeTeams;

    @Mock
    private ModulSeTeam modulSeTeam;

    @Mock
    private FahrzeugmerkmalAuspraegungDto fahrzeugmerkmalAuspraegungDto;
    private List<FahrzeugmerkmalAuspraegungDto> dialogAuspraegungen;

    @Mock
    private FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung;

    @BeforeEach
    void setUp() {
        dialogDataModulSeTeams = Collections.singletonList(fahrzeugmerkmalModulSeTeamDto);
        dialogAuspraegungen = Collections.singletonList(fahrzeugmerkmalAuspraegungDto);
        fahrzeugmerkmal = new Fahrzeugmerkmal();
    }

    @Test
    void saveNewAuspraegungenIsNull() {
        when(editDialogData.isNewObject()).thenReturn(true);
        Fahrzeugmerkmal result = safeService.save(editDialogData);
        assertThat(result, is(nullValue()));
    }

    @Test
    void saveNewAuspraegungenIsEmpy() {
        when(editDialogData.isNewObject()).thenReturn(true);
        when(editDialogData.getAuspraegungen()).thenReturn(Collections.emptyList());
        Fahrzeugmerkmal result = safeService.save(editDialogData);
        assertThat(result, is(nullValue()));
    }

    @Test
    void saveModulSeTeamsIsNull() {
        when(editDialogData.isNewObject()).thenReturn(true);
        when(editDialogData.getAuspraegungen()).thenReturn(dialogAuspraegungen);
        when(editDialogData.getFahrzeugmerkmalLabel()).thenReturn(MERKMAL);
        Fahrzeugmerkmal result = safeService.save(editDialogData);
        assertThat(result.getMerkmal(), is(MERKMAL));
    }

    @Test
    void saveNewObjectMerkmalValue() {
        when(editDialogData.isNewObject()).thenReturn(true);
        when(editDialogData.getAuspraegungen()).thenReturn(dialogAuspraegungen);
        when(editDialogData.getFahrzeugmerkmalLabel()).thenReturn(MERKMAL);
        Fahrzeugmerkmal result = safeService.save(editDialogData);
        assertThat(result.getMerkmal(), is(MERKMAL));
    }

    @Test
    void saveNewObjectModuleAdded() {
        when(editDialogData.isNewObject()).thenReturn(true);
        when(editDialogData.isModuleChanged()).thenReturn(true);
        when(editDialogData.getAuspraegungen()).thenReturn(dialogAuspraegungen);
        when(editDialogData.getModulSeTeams()).thenReturn(dialogDataModulSeTeams);
        when(editDialogData.getFahrzeugmerkmalLabel()).thenReturn(MERKMAL);
        when(modulService.getSeTeamById(anyLong())).thenReturn(modulSeTeam);
        when(fahrzeugmerkmalModulSeTeamDto.getModulSeTeamId()).thenReturn(new ModulSeTeamId(1L));
        Fahrzeugmerkmal result = safeService.save(editDialogData);
        assertThat(result.getModulSeTeams(), hasSize(1));
    }

    @Test
    void saveNewObjectModuleAddedButNotFound() {
        when(editDialogData.isNewObject()).thenReturn(true);
        when(editDialogData.isModuleChanged()).thenReturn(true);
        when(editDialogData.getModulSeTeams()).thenReturn(dialogDataModulSeTeams);
        when(editDialogData.getAuspraegungen()).thenReturn(dialogAuspraegungen);
        when(editDialogData.getFahrzeugmerkmalLabel()).thenReturn(MERKMAL);
        when(modulService.getSeTeamById(anyLong())).thenReturn(null);
        when(fahrzeugmerkmalModulSeTeamDto.getModulSeTeamId()).thenReturn(new ModulSeTeamId(1L));
        Fahrzeugmerkmal result = safeService.save(editDialogData);
        assertThat(result.getModulSeTeams(), hasSize(0));
    }

    @Test
    void saveNewObjectAuspraegungAdded() {
        when(editDialogData.isNewObject()).thenReturn(true);
        when(editDialogData.isAuspraegungenChanged()).thenReturn(true);
        when(fahrzeugmerkmalAuspraegungDto.getId()).thenReturn(null);
        when(fahrzeugmerkmalAuspraegungDto.getAuspraegung()).thenReturn("Auspraegung");
        when(editDialogData.getAuspraegungen()).thenReturn(dialogAuspraegungen);
        when(editDialogData.getFahrzeugmerkmalLabel()).thenReturn(MERKMAL);
        safeService.save(editDialogData);
        verify(fahrzeugmerkmalService, times(1)).saveFahrzeugmerkmalAuspraegung(any());
    }

    @Test
    void saveExistingObjectModuleRemoved() {
        fahrzeugmerkmal.addModulSeTeam(modulSeTeam);
        when(fahrzeugmerkmalService.findFahrzeugmerkmal(any())).thenReturn(Optional.of(fahrzeugmerkmal));
        when(editDialogData.isNewObject()).thenReturn(false);
        when(editDialogData.isModuleChanged()).thenReturn(true);
        when(editDialogData.getModulSeTeams()).thenReturn(Collections.emptyList());
        when(modulService.getSeTeamById(anyLong())).thenReturn(modulSeTeam);
        when(modulSeTeam.getModulSeTeamId()).thenReturn(new ModulSeTeamId(1L));

        Fahrzeugmerkmal result = safeService.save(editDialogData);
        assertThat(result.getModulSeTeams(), hasSize(0));
    }

    @Test
    void saveExistingObjectModuleRemovedButNotFound() {
        fahrzeugmerkmal.setModulSeTeams(Collections.singletonList(modulSeTeam));
        when(fahrzeugmerkmalService.findFahrzeugmerkmal(any())).thenReturn(Optional.of(fahrzeugmerkmal));
        when(editDialogData.isNewObject()).thenReturn(false);
        when(editDialogData.isModuleChanged()).thenReturn(true);
        when(editDialogData.getModulSeTeams()).thenReturn(Collections.emptyList());
        when(modulService.getSeTeamById(anyLong())).thenReturn(null);
        when(modulSeTeam.getModulSeTeamId()).thenReturn(new ModulSeTeamId(1L));

        Fahrzeugmerkmal result = safeService.save(editDialogData);
        assertThat(result.getModulSeTeams(), hasSize(1));
    }

    @Test
    void saveExistingObjectMerkmalLabelChanged() {
        fahrzeugmerkmal.addModulSeTeam(modulSeTeam);
        when(fahrzeugmerkmalService.findFahrzeugmerkmal(any())).thenReturn(Optional.of(fahrzeugmerkmal));
        when(editDialogData.isNewObject()).thenReturn(false);
        when(editDialogData.isFahrzeugmerkmalLabelChanged()).thenReturn(true);
        when(editDialogData.getFahrzeugmerkmalLabel()).thenReturn("NEU");

        Fahrzeugmerkmal result = safeService.save(editDialogData);
        assertThat(result.getMerkmal(), is("NEU"));
    }

    @Test
    void saveExistingObjectNotFound() {
        when(fahrzeugmerkmalService.findFahrzeugmerkmal(any())).thenReturn(Optional.empty());
        when(editDialogData.isNewObject()).thenReturn(false);
        Fahrzeugmerkmal result = safeService.save(editDialogData);
        assertThat(result, nullValue());
    }

    @Test
    void saveExistingObjectAuspraegungChanged() {
        when(editDialogData.isNewObject()).thenReturn(false);
        when(fahrzeugmerkmalService.findFahrzeugmerkmal(any())).thenReturn(Optional.of(fahrzeugmerkmal));
        when(editDialogData.isAuspraegungenChanged()).thenReturn(true);
        when(fahrzeugmerkmalAuspraegungDto.getId()).thenReturn(null);
        when(fahrzeugmerkmalAuspraegungDto.getAuspraegung()).thenReturn("Auspraegung");
        when(fahrzeugmerkmalAuspraegungDto.isChanged()).thenReturn(true);
        when(fahrzeugmerkmalService.findFahrzeugmerkmalAuspraegung(any())).thenReturn(Optional.of(fahrzeugmerkmalAuspraegung));
        when(editDialogData.getAuspraegungen()).thenReturn(dialogAuspraegungen);
        safeService.save(editDialogData);
        verify(fahrzeugmerkmalService, times(1)).saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegung);
    }

    @Test
    void saveExistingObjectAuspraegungChangedButNotFound() {
        when(editDialogData.isNewObject()).thenReturn(false);
        when(fahrzeugmerkmalService.findFahrzeugmerkmal(any())).thenReturn(Optional.of(fahrzeugmerkmal));
        when(editDialogData.isAuspraegungenChanged()).thenReturn(true);
        when(fahrzeugmerkmalAuspraegungDto.getId()).thenReturn(null);
        when(fahrzeugmerkmalAuspraegungDto.getAuspraegung()).thenReturn("Auspraegung");
        when(fahrzeugmerkmalAuspraegungDto.isChanged()).thenReturn(true);
        when(fahrzeugmerkmalService.findFahrzeugmerkmalAuspraegung(any())).thenReturn(Optional.empty());
        when(editDialogData.getAuspraegungen()).thenReturn(dialogAuspraegungen);
        safeService.save(editDialogData);
        verify(fahrzeugmerkmalService, never()).saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegung);
    }

}