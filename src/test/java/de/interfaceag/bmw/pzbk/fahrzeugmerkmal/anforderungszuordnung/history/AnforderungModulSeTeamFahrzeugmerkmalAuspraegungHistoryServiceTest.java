package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.history;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.AnforderungModulSeTeamFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import org.assertj.core.api.Java6Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AnforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryServiceTest {

    @InjectMocks
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService anforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService;
    @Mock
    private Session session;
    @Mock
    private AnforderungMeldungHistoryService anforderungMeldungHistoryService;

    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegung;

    private Mitarbeiter mitarbeiter;

    @BeforeEach
    void setUp() {
        anforderungModulSeTeamFahrzeugmerkmalAuspraegung = new AnforderungModulSeTeamFahrzeugmerkmalAuspraegung(
                TestDataFactory.generateAnforderung(),
                TestDataFactory.generateModulSeTeam(TestDataFactory.generateModul("Modulname", "Fachbereich 1A", "Modulbeschreibung"), TestDataFactory.generateModulKomponenten()),
                TestDataFactory.generateAuspraegung("AuspraegungName")
        );
        mitarbeiter = TestDataFactory.createMitarbeiter("Hubertus");


    }

    @Test
    void buildAnforderungHistoryWithBooleanTrue() {
        AnforderungHistory anforderungHistory = anforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService.buildAnforderungHistory(anforderungModulSeTeamFahrzeugmerkmalAuspraegung, true, "Hubertus");

        Java6Assertions.assertThat(anforderungHistory.toString())
                .contains("Anforderung ID 2")
                .contains("Kennzeichen A")
                .contains("SE-TEAM - Fahrzeugmerkmal:Merkmal1")
                .contains("von Ausprägung: AuspraegungName")
                .contains("auf hinzugefügt");
    }

    @Test
    void buildAnforderungHistoryWithBooleanFalse() {
        AnforderungHistory anforderungHistory = anforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService.buildAnforderungHistory(anforderungModulSeTeamFahrzeugmerkmalAuspraegung, false, "Hubertus");

        Java6Assertions.assertThat(anforderungHistory.toString())
                .contains("Anforderung ID 2")
                .contains("Kennzeichen A")
                .contains("SE-TEAM - Fahrzeugmerkmal:Merkmal1")
                .contains("von Ausprägung: AuspraegungName")
                .contains("auf entfernt");
    }

    @Test
    void writeAnforderungFahrzeugmerkmalAuspraegungHinzugefuegtToHistory() {
        when(session.getUser()).thenReturn(mitarbeiter);
        anforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService.writeAnforderungFahrzeugmerkmalAuspraegungHinzugefuegtToHistory(anforderungModulSeTeamFahrzeugmerkmalAuspraegung);
        verify(anforderungMeldungHistoryService, times(1)).persistAnforderungHistory(any());
    }

    @Test
    void writeAnforderungFahrzeugmerkmalAuspraegungGeloeschtToHistory() {
        when(session.getUser()).thenReturn(mitarbeiter);
        anforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService.writeAnforderungFahrzeugmerkmalAuspraegungGeloeschtToHistory(anforderungModulSeTeamFahrzeugmerkmalAuspraegung);
        verify(anforderungMeldungHistoryService, times(1)).persistAnforderungHistory(any());
    }
}