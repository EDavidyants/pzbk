package de.interfaceag.bmw.pzbk.fahrzeugmerkmal;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalTableEntry;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalViewData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalViewDataTest {

    @Mock
    private List<FahrzeugmerkmalTableEntry> fahrzeugmerkmalTableEntries;

    private FahrzeugmerkmalViewData fahrzeugmerkmalViewData;

    @BeforeEach
    void setUp() {
        fahrzeugmerkmalViewData = new FahrzeugmerkmalViewData(fahrzeugmerkmalTableEntries);
    }

    @Test
    void getFahrzeugmerkmalTableEntries() {
        final List<FahrzeugmerkmalTableEntry> viewDataFahrzeugmerkmalTableEntries = fahrzeugmerkmalViewData.getFahrzeugmerkmalTableEntries();
        assertThat(viewDataFahrzeugmerkmalTableEntries, is(fahrzeugmerkmalTableEntries));
    }
}