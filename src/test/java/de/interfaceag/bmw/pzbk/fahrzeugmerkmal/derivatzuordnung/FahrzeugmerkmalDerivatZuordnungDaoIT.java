package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.dao.DerivatDao;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalAuspraegungDao;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalDao;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalAuspraegungDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class FahrzeugmerkmalDerivatZuordnungDaoIT extends IntegrationTest {

    private FahrzeugmerkmalDerivatZuordnungDao fahrzeugmerkmalDerivatZuordnungDao = new FahrzeugmerkmalDerivatZuordnungDao();
    private DerivatDao derivatDao = new DerivatDao();
    private FahrzeugmerkmalDao fahrzeugmerkmalDao = new FahrzeugmerkmalDao();
    private FahrzeugmerkmalAuspraegungDao fahrzeugmerkmalAuspraegungDao = new FahrzeugmerkmalAuspraegungDao();

    private final Derivat derivat = new Derivat("E24", "");
    private final Fahrzeugmerkmal fahrzeugmerkmal = new Fahrzeugmerkmal("Merkmal");
    private final FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "Auspraegung");
    private final FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung2 = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "Auspraegung 2");

    @BeforeEach
    void setUp() throws IllegalAccessException {
        super.begin();

        FieldUtils.writeField(derivatDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(fahrzeugmerkmalDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(fahrzeugmerkmalAuspraegungDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(fahrzeugmerkmalDerivatZuordnungDao, "entityManager", super.getEntityManager(), true);

        super.commit();
        super.begin();

        derivatDao.persistDerivat(derivat);
        fahrzeugmerkmalDao.save(fahrzeugmerkmal);
        fahrzeugmerkmalAuspraegungDao.save(fahrzeugmerkmalAuspraegung);
        fahrzeugmerkmalAuspraegungDao.save(fahrzeugmerkmalAuspraegung2);
        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal = new DerivatFahrzeugmerkmal(derivat, fahrzeugmerkmal, Arrays.asList(fahrzeugmerkmalAuspraegung, fahrzeugmerkmalAuspraegung2));
        fahrzeugmerkmalDerivatZuordnungDao.save(derivatFahrzeugmerkmal);
    }

    @Test
    void getAllFahrzeugmerkmaleForDerivat() {
        final List<FahrzeugmerkmalDerivatDto> result = fahrzeugmerkmalDerivatZuordnungDao.getAllFahrzeugmerkmaleForDerivat(derivat.getDerivatId());
        MatcherAssert.assertThat(result, IsCollectionWithSize.hasSize(1));
    }

    @Test
    void getAlreadySelectedAuspraegungen() {
        FahrzeugmerkmalDerivatDto selectedFahrzeugmerkmal = new FahrzeugmerkmalDerivatDto(fahrzeugmerkmal.getFahrzeugmerkmalId(), fahrzeugmerkmal.getMerkmal(), derivat.getDerivatId(), null, null);
        final List<FahrzeugmerkmalAuspraegungDerivatDto> result = fahrzeugmerkmalDerivatZuordnungDao.getAlreadySelectedAuspraegungenForFahrzeugmerkmalAndDerivat(selectedFahrzeugmerkmal.getFahrzeugmerkmalId(), selectedFahrzeugmerkmal.getDerivatId());
        MatcherAssert.assertThat(result, IsCollectionWithSize.hasSize(2));
    }
}