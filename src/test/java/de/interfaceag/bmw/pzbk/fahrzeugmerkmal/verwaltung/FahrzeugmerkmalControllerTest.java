package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalEditDialogData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalViewData;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ContextMocker;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalControllerTest {

    @Mock
    private FahrzeugmerkmalViewFacade facade;
    @Mock
    private FahrzeugmerkmalViewData viewData;
    @InjectMocks
    private FahrzeugmerkmalController controller;

    @Mock
    private FahrzeugmerkmalDto fahrzeugmerkmalDto;
    @Mock
    private FahrzeugmerkmalEditDialogData dialogData;

    @Test
    void getViewData() {
        final FahrzeugmerkmalViewData controllerViewData = controller.getViewData();
        assertThat(controllerViewData, is(viewData));
    }

    @Test
    void updateEditDialog() {
        ContextMocker.mockRequestContext();
        controller.updateEditDialog(fahrzeugmerkmalDto);
        verify(facade, times(1)).getEditDialogData(fahrzeugmerkmalDto);
    }

    @Test
    void showNewMerkmalDialog() {
        ContextMocker.mockRequestContext();
        controller.showNewMerkmalDialog();
        verify(facade, times(1)).getEditDialogDataForNewFahrzeugmerkmal();
    }

    @Test
    void getDialogData() {
        ContextMocker.mockRequestContext();
        when(facade.getEditDialogData(any())).thenReturn(dialogData);
        controller.updateEditDialog(fahrzeugmerkmalDto);
        final FahrzeugmerkmalEditDialogData controllerDialogData = controller.getDialogData();
        assertThat(controllerDialogData, is(dialogData));
    }

    @Test
    void save() {
        controller.save();
        verify(facade, times(1)).saveEditDialog(any());
    }
}