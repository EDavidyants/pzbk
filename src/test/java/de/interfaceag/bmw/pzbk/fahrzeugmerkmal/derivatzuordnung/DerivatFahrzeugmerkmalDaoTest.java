package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DerivatFahrzeugmerkmalDaoTest {

    @Mock
    private TypedQuery query;

    @Mock
    private EntityManager entityManager;

    @InjectMocks
    private DerivatFahrzeugmerkmalDao derivatFahrzeugmerkmalDao;

    @Test
    void getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId_no_result() {
        DerivatId derivatId = new DerivatId(42L);
        FahrzeugmerkmalId fahrzeugmerkmalId = new FahrzeugmerkmalId(44L);

        when(entityManager.createQuery(any(), any())).thenReturn(query);
        when(query.getResultList()).thenReturn(new ArrayList<>());

        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal = derivatFahrzeugmerkmalDao.getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId(derivatId, fahrzeugmerkmalId);
        Assert.assertNull(derivatFahrzeugmerkmal);
    }

    @Test
    void getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId() {
        DerivatId derivatId = new DerivatId(42L);
        FahrzeugmerkmalId fahrzeugmerkmalId = new FahrzeugmerkmalId(44L);

        when(entityManager.createQuery(any(), any())).thenReturn(query);
        when(query.getResultList()).thenReturn(Arrays.asList(mock(DerivatFahrzeugmerkmal.class)));

        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal = derivatFahrzeugmerkmalDao.getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId(derivatId, fahrzeugmerkmalId);
        Assert.assertNotNull(derivatFahrzeugmerkmal);
    }
}