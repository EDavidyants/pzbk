package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.Collections;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class FahrzeugmerkmalViewPermissionTest {

    private FahrzeugmerkmalViewPermission viewPermission;

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getPage(Rolle role) {
        Set<Rolle> roles = Collections.singleton(role);
        viewPermission = new FahrzeugmerkmalViewPermission(roles);
        final boolean page = viewPermission.getPage();
        boolean expectedPermission;
        switch (role) {
            case ADMIN:
                expectedPermission = Boolean.TRUE;
                break;
            default:
                expectedPermission = Boolean.FALSE;
                break;
        }
        assertThat(page, is(expectedPermission));
    }
}