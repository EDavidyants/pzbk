package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.dao.ModulDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungModulSeTeamFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalAuspraegungDao;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalDao;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsIterableContaining.hasItem;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AnforderungModulSeTeamFahrzeugmerkmalAuspraegungDaoIT extends IntegrationTest {

    private AnforderungDao anforderungDao = new AnforderungDao();
    private ModulDao modulDao = new ModulDao();
    private FahrzeugmerkmalDao fahrzeugmerkmalDao = new FahrzeugmerkmalDao();
    private FahrzeugmerkmalAuspraegungDao fahrzeugmerkmalAuspraegungDao = new FahrzeugmerkmalAuspraegungDao();
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungDao anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao = new AnforderungModulSeTeamFahrzeugmerkmalAuspraegungDao();

    private Modul modul = new Modul();

    private ModulSeTeam modulSeTeam1 = new ModulSeTeam("SeTeam 1", modul, Collections.emptySet());
    private Anforderung anforderung1 = new Anforderung();
    private Fahrzeugmerkmal fahrzeugmerkmal1 = new Fahrzeugmerkmal("Merkmal");
    private FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung1;
    AnforderungModulSeTeamFahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegung1;

    private ModulSeTeam modulSeTeam2 = new ModulSeTeam("SeTeam 2", modul, Collections.emptySet());
    private Anforderung anforderung2 = new Anforderung();
    private Fahrzeugmerkmal fahrzeugmerkmal2 = new Fahrzeugmerkmal("Merkmal");
    private FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung2;
    AnforderungModulSeTeamFahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegung2;

    @BeforeEach
    void setUp() throws IllegalAccessException {
        FieldUtils.writeField(anforderungDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(modulDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(fahrzeugmerkmalDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(fahrzeugmerkmalAuspraegungDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao, "entityManager", super.getEntityManager(), true);

        super.begin();

        modulDao.persistModule(Collections.singletonList(modul));

        modulDao.persistSeTeam(modulSeTeam1);
        anforderungDao.persistAnforderung(anforderung1);
        fahrzeugmerkmalDao.save(fahrzeugmerkmal1);

        modulDao.persistSeTeam(modulSeTeam2);
        anforderungDao.persistAnforderung(anforderung2);
        fahrzeugmerkmalDao.save(fahrzeugmerkmal2);

        fahrzeugmerkmalAuspraegung1 = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal1, "Auspraegung");
        fahrzeugmerkmalAuspraegungDao.save(fahrzeugmerkmalAuspraegung1);

        fahrzeugmerkmalAuspraegung2 = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal2, "Auspraegung");
        fahrzeugmerkmalAuspraegungDao.save(fahrzeugmerkmalAuspraegung2);

        anforderungModulSeTeamFahrzeugmerkmalAuspraegung1 = new AnforderungModulSeTeamFahrzeugmerkmalAuspraegung(anforderung1, modulSeTeam1, fahrzeugmerkmalAuspraegung1);
        anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.save(anforderungModulSeTeamFahrzeugmerkmalAuspraegung1);

        anforderungModulSeTeamFahrzeugmerkmalAuspraegung2 = new AnforderungModulSeTeamFahrzeugmerkmalAuspraegung(anforderung2, modulSeTeam2, fahrzeugmerkmalAuspraegung2);
        anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.save(anforderungModulSeTeamFahrzeugmerkmalAuspraegung2);

        super.commit();
        super.begin();
    }

    @Test
    void getConfiguredModulSeTeamsForAnforderung() {
        final Collection<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> byAnforderung = anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.getByAnforderung(anforderung1.getAnforderungId());
        assertThat(byAnforderung, hasItem(anforderungModulSeTeamFahrzeugmerkmalAuspraegung1));
    }

    @Test
    void getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam() {
        final List<FahrzeugmerkmalAuspraegung> auspraegungen = anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(anforderung1.getAnforderungId(), modulSeTeam1.getModulSeTeamId());
        assertThat(auspraegungen, hasItem(fahrzeugmerkmalAuspraegung1));
    }

    @Test
    void getSelectedFahrzeugmerkmalAuspraegungenForAnforderungModulSeTeamAndFahrzeugmerkmalAuspraegung() {
        final Optional<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> result = anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungModulSeTeamAndFahrzeugmerkmalAuspraegung(anforderung1.getAnforderungId(), modulSeTeam1.getModulSeTeamId(), fahrzeugmerkmalAuspraegung1.getFahrzeugmerkmalAuspraegungId());
        assertTrue(result.isPresent());
    }

    @Test
    void getSelectedFahrzeugmerkmalAuspraegungenForAnforderungModulSeTeamAndFahrzeugmerkmalAuspraegungNotFound() {
        final Optional<AnforderungModulSeTeamFahrzeugmerkmalAuspraegung> result = anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungModulSeTeamAndFahrzeugmerkmalAuspraegung(anforderung1.getAnforderungId(), modulSeTeam1.getModulSeTeamId(), new FahrzeugmerkmalAuspraegungId(0L));
        assertFalse(result.isPresent());
    }
}