package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto;

import de.interfaceag.bmw.pzbk.converters.DomainObjectConverter;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.faces.convert.Converter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalEditDialogDataDtoTest {

    private FahrzeugmerkmalEditDialogDataDto dto;

    @Mock
    private FahrzeugmerkmalDto fahrzeugmerkmalDto;

    @Mock
    private FahrzeugmerkmalModulSeTeamDto fahrzeugmerkmalModulSeTeamDto1;
    @Mock
    private FahrzeugmerkmalModulSeTeamDto fahrzeugmerkmalModulSeTeamDto2;

    @Mock
    private FahrzeugmerkmalAuspraegungDto fahrzeugmerkmalAuspraegungDto;

    private List<FahrzeugmerkmalModulSeTeamDto> allModulSeTeams;
    private List<FahrzeugmerkmalModulSeTeamDto> fahrzeugmerkmalModulSeTeams;
    private List<FahrzeugmerkmalAuspraegungDto> auspraegungen;

    @BeforeEach
    void setUp() {
        allModulSeTeams = Arrays.asList(fahrzeugmerkmalModulSeTeamDto1, fahrzeugmerkmalModulSeTeamDto2);
        fahrzeugmerkmalModulSeTeams = Collections.singletonList(fahrzeugmerkmalModulSeTeamDto1);
        auspraegungen = Collections.singletonList(fahrzeugmerkmalAuspraegungDto);

        when(fahrzeugmerkmalDto.getId()).thenReturn(1L);
        when(fahrzeugmerkmalDto.getMerkmal()).thenReturn("Merkmal");

        dto = new FahrzeugmerkmalEditDialogDataDto(fahrzeugmerkmalDto, allModulSeTeams, fahrzeugmerkmalModulSeTeams, auspraegungen);
    }

    @Test
    void toString1() {
        final String result = dto.toString();
        assertThat(result, is("FahrzeugmerkmalEditDialogDataDto{newObject=false, fahrzeugmerkmalId=1, originalFahrzeugmerkmalLabel='Merkmal', fahrzeugmerkmalLabel='Merkmal', allModulSeTeams=[fahrzeugmerkmalModulSeTeamDto1, fahrzeugmerkmalModulSeTeamDto2], originalFahrzeugmerkmalModulSeTeams=[fahrzeugmerkmalModulSeTeamDto1], fahrzeugmerkmalModulSeTeams=[fahrzeugmerkmalModulSeTeamDto1], newAuspraegung='null', originalAuspraegungen=[FahrzeugmerkmalModulSeTeamDto{id=0, auspraegung='null'}], auspraegungen=[fahrzeugmerkmalAuspraegungDto]}"));
    }

    @Test
    void getFahrzeugmerkmalId() {
        final FahrzeugmerkmalId fahrzeugmerkmalId = dto.getFahrzeugmerkmalId();
        assertThat(fahrzeugmerkmalId, is(new FahrzeugmerkmalId(1L)));
    }

    @Test
    void getFahrzeugmerkmalLabel() {
        final String fahrzeugmerkmalLabel = dto.getFahrzeugmerkmalLabel();
        assertThat(fahrzeugmerkmalLabel, is("Merkmal"));
    }

    @Test
    void setFahrzeugmerkmalLabel() {
        dto.setFahrzeugmerkmalLabel("NEU");
        final String fahrzeugmerkmalLabel = dto.getFahrzeugmerkmalLabel();
        assertThat(fahrzeugmerkmalLabel, is("NEU"));
    }

    @Test
    void getModulSeTeams() {
        final List<FahrzeugmerkmalModulSeTeamDto> modulSeTeams = dto.getModulSeTeams();
        assertThat(modulSeTeams, is(fahrzeugmerkmalModulSeTeams));
    }

    @Test
    void setModulSeTeams() {
        dto.setModulSeTeams(Collections.emptyList());
        final List<FahrzeugmerkmalModulSeTeamDto> modulSeTeams = dto.getModulSeTeams();
        assertThat(modulSeTeams, is(Collections.emptyList()));
    }

    @Test
    void getModulConverter() {
        final Converter modulConverter = dto.getModulConverter();
        assertThat(modulConverter, instanceOf(DomainObjectConverter.class));
    }

    @Test
    void completeModuleEmptyString() {
        final List<FahrzeugmerkmalModulSeTeamDto> result = dto.completeModule("");
        assertThat(result, IsCollectionWithSize.hasSize(0));
    }

    @Test
    void completeModuleNullInput() {
        final List<FahrzeugmerkmalModulSeTeamDto> result = dto.completeModule(null);
        assertThat(result, IsCollectionWithSize.hasSize(0));
    }

    @Test
    void completeModule() {
        when(fahrzeugmerkmalModulSeTeamDto1.getBezeichnung()).thenReturn("Modul 1");
        when(fahrzeugmerkmalModulSeTeamDto2.getBezeichnung()).thenReturn("Kein Modul");
        final List<FahrzeugmerkmalModulSeTeamDto> result = dto.completeModule("Kein");
        assertThat(result, IsCollectionWithSize.hasSize(1));
    }

    @Test
    void completeModuleWrongCase() {
        when(fahrzeugmerkmalModulSeTeamDto1.getBezeichnung()).thenReturn("Modul 1");
        when(fahrzeugmerkmalModulSeTeamDto2.getBezeichnung()).thenReturn("Kein Modul");
        final List<FahrzeugmerkmalModulSeTeamDto> result = dto.completeModule("kein");
        assertThat(result, IsCollectionWithSize.hasSize(1));
    }

    @Test
    void isModuleChangedDefault() {
        final boolean result = dto.isModuleChanged();
        assertFalse(result);
    }

    @Test
    void isModuleChanged() {
        dto.setModulSeTeams(Collections.emptyList());
        final boolean result = dto.isModuleChanged();
        assertTrue(result);
    }

    @Test
    void getNewAuspraegung() {
        final String newAuspraegung = dto.getNewAuspraegung();
        assertThat(newAuspraegung, nullValue());
    }

    @Test
    void setNewAuspraegung() {
        dto.setNewAuspraegung("NEU");
        final String newAuspraegung = dto.getNewAuspraegung();
        assertThat(newAuspraegung, is("NEU"));
    }

    @Test
    void addAuspraegungEmptyString() {
        dto.setAuspraegungen(new ArrayList<>());
        final List<FahrzeugmerkmalAuspraegungDto> auspraegungen = dto.getAuspraegungen();
        assertThat(auspraegungen, IsCollectionWithSize.hasSize(0));
        dto.addAuspraegung();
        assertThat(auspraegungen, IsCollectionWithSize.hasSize(0));
    }

    @Test
    void addAuspraegung() {
        dto.setNewAuspraegung("NEU");
        dto.setAuspraegungen(new ArrayList<>());
        final List<FahrzeugmerkmalAuspraegungDto> auspraegungen = dto.getAuspraegungen();
        assertThat(auspraegungen, IsCollectionWithSize.hasSize(0));
        dto.addAuspraegung();
        assertThat(auspraegungen, IsCollectionWithSize.hasSize(1));
    }

    @Test
    void getAuspraegungen() {
        final List<FahrzeugmerkmalAuspraegungDto> dtoAuspraegungen = dto.getAuspraegungen();
        assertThat(dtoAuspraegungen, is(auspraegungen));
    }

    @Test
    void setAuspraegungen() {
        dto.setAuspraegungen(Collections.emptyList());
        final List<FahrzeugmerkmalAuspraegungDto> dtoAuspraegungen = dto.getAuspraegungen();
        assertThat(dtoAuspraegungen, is(Collections.emptyList()));
    }

    @Test
    void isFahrzeugmerkmalLabelChangedDefault() {
        final boolean result = dto.isFahrzeugmerkmalLabelChanged();
        assertFalse(result);
    }

    @Test
    void isFahrzeugmerkmalLabelChanged() {
        dto.setFahrzeugmerkmalLabel("");
        final boolean result = dto.isFahrzeugmerkmalLabelChanged();
        assertTrue(result);
    }

    @Test
    void isAuspraegungenChanged() {
        dto.setAuspraegungen(Collections.emptyList());
        final boolean result = dto.isAuspraegungenChanged();
        assertTrue(result);
    }

    @Test
    void isNewObject() {
        final boolean result = dto.isNewObject();
        assertFalse(result);
    }

    @Test
    void isNewObjectNewConstructor() {
        dto = new FahrzeugmerkmalEditDialogDataDto(allModulSeTeams);
        final boolean result = dto.isNewObject();
        assertTrue(result);
    }
}