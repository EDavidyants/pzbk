package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.converters;

import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

class FahrzeugmerkmalDerivatDtoConverterTest {

    @Test
    public void testForDerivatFahrzeugmerkmale() {
        List<DerivatFahrzeugmerkmal> derivatFahrzeugmerkmale = new ArrayList<>();
        derivatFahrzeugmerkmale.add(new DerivatFahrzeugmerkmal(
                TestDataFactory.generateDerivat(),
                TestDataFactory.generateFahrzeugmerkmal("Merkmal"),
                TestDataFactory.generateAuspraegungen(5)));

        List<FahrzeugmerkmalDerivatDto> result = FahrzeugmerkmalDerivatDtoConverter.forDerivatFahrzeugmerkmale(derivatFahrzeugmerkmale);

        MatcherAssert.assertThat(result, hasSize(1));
        MatcherAssert.assertThat(result.toString(), is("[FahrzeugmerkmalDerivatDto{fahrzeugmerkmalId=FahrzeugmerkmalId[id=null], fahrzeugmerkmalMerkmal='Merkmal', derivatId=DerivatId[id=1], status=RELEVANT, isNotRelevant=false}]"));
    }
}