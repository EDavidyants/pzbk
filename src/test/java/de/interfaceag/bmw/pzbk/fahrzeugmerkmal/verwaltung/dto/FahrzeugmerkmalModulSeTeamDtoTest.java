package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto;

import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FahrzeugmerkmalModulSeTeamDtoTest {

    private FahrzeugmerkmalModulSeTeamDto dto;

    @BeforeEach
    void setUp() {
        dto = new FahrzeugmerkmalModulSeTeamDto(1L, "Lorenz");
    }

    @Test
    void toString1() {
        final String result = dto.toString();
        assertThat(result, is("FahrzeugmerkmalModulSeTeamDto{id=1, bezeichnung='Lorenz'}"));
    }

    @Test
    void equalsNull() {
        final boolean result = dto.equals(null);
        assertFalse(result);
    }

    @Test
    void equalsOtherClass() {
        final boolean result = dto.equals(new ModulSeTeamId(1L));
        assertFalse(result);
    }

    @Test
    void equalsOtherId() {
        FahrzeugmerkmalModulSeTeamDto otherDto = new FahrzeugmerkmalModulSeTeamDto(2L, "Lorenz");
        final boolean result = dto.equals(otherDto);
        assertFalse(result);
    }

    @Test
    void equalsOtherMerkmal() {
        FahrzeugmerkmalModulSeTeamDto otherDto = new FahrzeugmerkmalModulSeTeamDto(1L, "Lorenz 2");
        final boolean result = dto.equals(otherDto);
        assertFalse(result);
    }

    @Test
    void equalsEqual() {
        FahrzeugmerkmalModulSeTeamDto otherDto = new FahrzeugmerkmalModulSeTeamDto(1L, "Lorenz");
        final boolean result = dto.equals(otherDto);
        assertTrue(result);
    }

    @Test
    void equalsSame() {
        final boolean result = dto.equals(dto);
        assertTrue(result);
    }

    @Test
    void hashCode1() {
        final int result = dto.hashCode();
        assertThat(result, is(-2013120912));
    }

    @Test
    void getModulSeTeamId() {
        final ModulSeTeamId result = dto.getModulSeTeamId();
        assertThat(result, is(new ModulSeTeamId(1L)));
    }

    @Test
    void getId() {
        final Long result = dto.getId();
        assertThat(result, is(1L));
    }

    @Test
    void getBezeichnung() {
        final String result = dto.getBezeichnung();
        assertThat(result, is("Lorenz"));
    }
}