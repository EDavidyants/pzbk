package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung;

import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalEditDialogData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalAuspraegungDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalModulSeTeamDto;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalEditDialogServiceTest {

    @Mock
    private ModulService modulService;
    @InjectMocks
    private FahrzeugmerkmalEditDialogService fahrzeugmerkmalEditDialogService;

    private List<ModulSeTeam> modulSeTeams;
    @Mock
    private ModulSeTeam modulSeTeam;
    @Mock
    private FahrzeugmerkmalDto fahrzeugmerkmalDto;
    @Mock
    private List<FahrzeugmerkmalAuspraegungDto> auspraegungen;
    @Mock
    private FahrzeugmerkmalModulSeTeamDto modulForFahrzeugmerkmal;
    private List<FahrzeugmerkmalModulSeTeamDto> moduleForFahrzeugmerkmal;

    @BeforeEach
    void setUp() {
        modulSeTeams = Collections.singletonList(modulSeTeam);
        moduleForFahrzeugmerkmal = Collections.singletonList(modulForFahrzeugmerkmal);
        when(modulService.getAllSeTeams()).thenReturn(modulSeTeams);
        when(modulSeTeam.getId()).thenReturn(1L);
        when(modulSeTeam.getName()).thenReturn("SeTeam");
    }

    @Test
    void getEditDialogDataForFahrzeugmerkmalFahrzeugmerkmalLabelValue() {
        when(fahrzeugmerkmalDto.getMerkmal()).thenReturn("Merkmal");
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForFahrzeugmerkmal(fahrzeugmerkmalDto);
        assertThat(result.getFahrzeugmerkmalLabel(), is("Merkmal"));

    }

    @Test
    void getEditDialogDataForFahrzeugmerkmalFahrzeugmerkmalIdValue() {
        when(fahrzeugmerkmalDto.getId()).thenReturn(1L);
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForFahrzeugmerkmal(fahrzeugmerkmalDto);
        assertThat(result.getFahrzeugmerkmalId(), is(new FahrzeugmerkmalId(1L)));
    }

    @Test
    void getEditDialogDataForFahrzeugmerkmalAuspraegungenValue() {
        when(fahrzeugmerkmalDto.getAuspraegungenForFahrzeugmerkmal()).thenReturn(auspraegungen);
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForFahrzeugmerkmal(fahrzeugmerkmalDto);
        assertThat(result.getAuspraegungen(), is(auspraegungen));
    }

    @Test
    void getEditDialogDataForFahrzeugmerkmalModulSeTeamValues() {
        when(fahrzeugmerkmalDto.getModuleForFahrzeugmerkmal()).thenReturn(moduleForFahrzeugmerkmal);
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForFahrzeugmerkmal(fahrzeugmerkmalDto);
        assertThat(result.getModulSeTeams(), is(moduleForFahrzeugmerkmal));
    }

    @Test
    void getEditDialogDataForFahrzeugmerkmalIsAuspraegungenChangedDefault() {
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForFahrzeugmerkmal(fahrzeugmerkmalDto);
        assertFalse(result.isAuspraegungenChanged());
    }

    @Test
    void getEditDialogDataForFahrzeugmerkmalIsFahrzeugmerkmalLabelChangedDefault() {
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForFahrzeugmerkmal(fahrzeugmerkmalDto);
        assertFalse(result.isFahrzeugmerkmalLabelChanged());
    }

    @Test
    void getEditDialogDataForFahrzeugmerkmalIsModuleChangedDefault() {
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForFahrzeugmerkmal(fahrzeugmerkmalDto);
        assertFalse(result.isModuleChanged());
    }

    @Test
    void getEditDialogDataForFahrzeugmerkmalIsNewObject() {
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForFahrzeugmerkmal(fahrzeugmerkmalDto);
        assertFalse(result.isNewObject());
    }
    
    @Test
    void getEditDialogDataForNewFahrzeugmerkmalFahrzeugmerkmalLabelValue() {
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForNewFahrzeugmerkmal();
        assertThat(result.getFahrzeugmerkmalLabel(), is(""));
    }

    @Test
    void getEditDialogDataForNewFahrzeugmerkmalFahrzeugmerkmalIdValue() {
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForNewFahrzeugmerkmal();
        assertThat(result.getFahrzeugmerkmalId(), is(new FahrzeugmerkmalId(null)));
    }

    @Test
    void getEditDialogDataForNewFahrzeugmerkmalAuspraegungenValue() {
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForNewFahrzeugmerkmal();
        assertThat(result.getAuspraegungen(), empty());
    }

    @Test
    void getEditDialogDataForNewFahrzeugmerkmalModulSeTeamValues() {
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForNewFahrzeugmerkmal();
        assertThat(result.getModulSeTeams(), empty());
    }

    @Test
    void getEditDialogDataForNewFahrzeugmerkmalIsAuspraegungenChangedDefault() {
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForNewFahrzeugmerkmal();
        assertFalse(result.isAuspraegungenChanged());
    }

    @Test
    void getEditDialogDataForNewFahrzeugmerkmalIsFahrzeugmerkmalLabelChangedDefault() {
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForNewFahrzeugmerkmal();
        assertFalse(result.isFahrzeugmerkmalLabelChanged());
    }

    @Test
    void getEditDialogDataForNewFahrzeugmerkmalIsModuleChangedDefault() {
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForNewFahrzeugmerkmal();
        assertFalse(result.isModuleChanged());
    }


    @Test
    void getEditDialogDataForNewFahrzeugmerkmalIsNewObject() {
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalEditDialogService.getEditDialogDataForNewFahrzeugmerkmal();
        assertTrue(result.isNewObject());
    }
}