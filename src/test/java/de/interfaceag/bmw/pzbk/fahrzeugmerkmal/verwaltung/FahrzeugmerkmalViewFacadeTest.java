package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung;

import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalEditDialogData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalViewFacadeTest {

    @Mock
    private FahrzeugmerkmalEditDialogService fahrzeugmerkmalEditDialogService;
    @Mock
    private FahrzeugmerkmalEditDialogSafeService fahrzeugmerkmalEditDialogSafeService;
    @InjectMocks
    private FahrzeugmerkmalViewFacade fahrzeugmerkmalViewFacade;


    @Mock
    private FahrzeugmerkmalEditDialogData fahrzeugmerkmalEditDialogData;
    @Mock
    private FahrzeugmerkmalDto fahrzeugmerkmalDto;
    @Mock
    private Fahrzeugmerkmal fahrzeugmerkmal;

    @Test
    void getEditDialogData() {
        when(fahrzeugmerkmalEditDialogService.getEditDialogDataForFahrzeugmerkmal(any())).thenReturn(fahrzeugmerkmalEditDialogData);
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalViewFacade.getEditDialogData(fahrzeugmerkmalDto);
        assertThat(result, is(fahrzeugmerkmalEditDialogData));
    }

    @Test
    void saveEditDialogMethodCall() {
        fahrzeugmerkmalViewFacade.saveEditDialog(fahrzeugmerkmalEditDialogData);
        verify(fahrzeugmerkmalEditDialogSafeService, times(1)).save(fahrzeugmerkmalEditDialogData);
    }

    @Test
    void saveEditDialogRetrunValueInlvalidSave() {
        when(fahrzeugmerkmalEditDialogSafeService.save(fahrzeugmerkmalEditDialogData)).thenReturn(null);
        String result = fahrzeugmerkmalViewFacade.saveEditDialog(fahrzeugmerkmalEditDialogData);
        assertThat(result, is(""));
    }

    @Test
    void saveEditDialogRetrunValue() {
        when(fahrzeugmerkmalEditDialogSafeService.save(fahrzeugmerkmalEditDialogData)).thenReturn(fahrzeugmerkmal);
        String result = fahrzeugmerkmalViewFacade.saveEditDialog(fahrzeugmerkmalEditDialogData);
        assertThat(result, is("fahrzeugmerkmalVerwaltung.xhtml?faces-redirect=true"));
    }

    @Test
    void getEditDialogDataForNewFahrzeugmerkmal() {
        when(fahrzeugmerkmalEditDialogService.getEditDialogDataForNewFahrzeugmerkmal()).thenReturn(fahrzeugmerkmalEditDialogData);
        final FahrzeugmerkmalEditDialogData result = fahrzeugmerkmalViewFacade.getEditDialogDataForNewFahrzeugmerkmal();
        assertThat(result, is(fahrzeugmerkmalEditDialogData));
    }
}