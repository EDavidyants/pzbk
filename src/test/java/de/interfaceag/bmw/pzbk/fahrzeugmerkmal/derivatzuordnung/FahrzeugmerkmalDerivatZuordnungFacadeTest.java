package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalAuspraegungDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.SelectedFahrzeugmerkmalData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalDerivatZuordnungFacadeTest {

    @Mock
    private FahrzeugmerkmalDerivatZuordnungService fahrzeugmerkmalDerivatZuordnungService;

    @InjectMocks
    private FahrzeugmerkmalDerivatZuordnungFacade fahrzeugmerkmalDerivatZuordnungFacade;

    @Mock
    private List<FahrzeugmerkmalAuspraegungDerivatDto> allAuspraegungen;

    @Mock
    private List<FahrzeugmerkmalAuspraegungDerivatDto> selectedAuspraegungen;

    @Mock
    private FahrzeugmerkmalDerivatDto selectedFahrzeugmerkmal;

    @Mock
    private SelectedFahrzeugmerkmalData selectedFahrzeugmerkmalData;

    @Test
    void getSelectedFahrzeugmerkmalDataForFahrzeugmerkmalAllAuspraegungenValue() {
        when(fahrzeugmerkmalDerivatZuordnungService.getAllAuspraegungenForFahrzeugmerkmal(any())).thenReturn(allAuspraegungen);
        when(fahrzeugmerkmalDerivatZuordnungService.getAlreadySelectedAuspraegungenForFahrzeugmerkmalAndDerivat(any())).thenReturn(selectedAuspraegungen);

        final SelectedFahrzeugmerkmalData result = fahrzeugmerkmalDerivatZuordnungFacade.getSelectedFahrzeugmerkmalDataForFahrzeugmerkmal(selectedFahrzeugmerkmal);
        final List<FahrzeugmerkmalAuspraegungDerivatDto> resultAllAuspraegungen = result.getAllAuspraegungen();
        assertThat(resultAllAuspraegungen, is(allAuspraegungen));
    }

    @Test
    void getSelectedFahrzeugmerkmalDataForFahrzeugmerkmalSelectedAuspraegungenValue() {
        when(fahrzeugmerkmalDerivatZuordnungService.getAllAuspraegungenForFahrzeugmerkmal(any())).thenReturn(allAuspraegungen);
        when(fahrzeugmerkmalDerivatZuordnungService.getAlreadySelectedAuspraegungenForFahrzeugmerkmalAndDerivat(any())).thenReturn(selectedAuspraegungen);

        final SelectedFahrzeugmerkmalData result = fahrzeugmerkmalDerivatZuordnungFacade.getSelectedFahrzeugmerkmalDataForFahrzeugmerkmal(selectedFahrzeugmerkmal);
        final List<FahrzeugmerkmalAuspraegungDerivatDto> resultSelectedAuspraegungen = result.getSelectedAuspraegungen();
        assertThat(resultSelectedAuspraegungen, is(selectedAuspraegungen));
    }

    @Test
    void persistSelectedAuspraegung() {
        fahrzeugmerkmalDerivatZuordnungFacade.persistSelectedAuspraegung(selectedFahrzeugmerkmalData);
        verify(fahrzeugmerkmalDerivatZuordnungService, times(1)).persistAuspraegung(any());
    }

}