package de.interfaceag.bmw.pzbk.fahrzeugmerkmal;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.FahrzeugmerkmalController;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalViewData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalControllerTest {

    @Mock
    private FahrzeugmerkmalViewData viewData;
    @InjectMocks
    private FahrzeugmerkmalController fahrzeugmerkmalController;

    @Test
    void getViewData() {
        final FahrzeugmerkmalViewData fahrzeugmerkmalControllerViewData = fahrzeugmerkmalController.getViewData();
        assertThat(fahrzeugmerkmalControllerViewData, is(viewData));
    }
}