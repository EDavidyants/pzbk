package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalDerivatZuordnungStatusTest {

    @Mock
    private DerivatFahrzeugmerkmal derivatFahrzeugmerkmal;
    @Mock
    private List<FahrzeugmerkmalAuspraegung> auspraegungen;

    @ParameterizedTest
    @EnumSource(FahrzeugmerkmalDerivatZuordnungStatus.class)
    void getIcon(FahrzeugmerkmalDerivatZuordnungStatus status) {
        final String icon = status.getIcon();
        String expected = "";
        switch (status) {
            case NICHT_BEWERTET:
                expected = "icon-kreis-blau.png";
                break;
            case RELEVANT:
                expected = "icon-check-green.png";
                break;
            case NICHT_RELEVANT:
                expected = "icon-cancel-red.png";
                break;
            default:
                fail("No test case present!");
        }
        assertThat(icon, is(expected));
    }

    @Test
    void getStatusFromDerivatFahrzeugmerkmalisNotRelevantTrue() {
        when(derivatFahrzeugmerkmal.isNotRelevant()).thenReturn(Boolean.TRUE);
        final FahrzeugmerkmalDerivatZuordnungStatus status = FahrzeugmerkmalDerivatZuordnungStatus.getStatusFromDerivatFahrzeugmerkmal(derivatFahrzeugmerkmal);
        assertThat(status, is(FahrzeugmerkmalDerivatZuordnungStatus.NICHT_RELEVANT));
    }

    @Test
    void getStatusFromDerivatFahrzeugmerkmalisNotRelevantFalseAuspraegungenIsNotEmpty() {
        when(derivatFahrzeugmerkmal.isNotRelevant()).thenReturn(Boolean.FALSE);
        when(derivatFahrzeugmerkmal.getAuspraegungen()).thenReturn(auspraegungen);
        when(auspraegungen.isEmpty()).thenReturn(Boolean.FALSE);
        final FahrzeugmerkmalDerivatZuordnungStatus status = FahrzeugmerkmalDerivatZuordnungStatus.getStatusFromDerivatFahrzeugmerkmal(derivatFahrzeugmerkmal);
        assertThat(status, is(FahrzeugmerkmalDerivatZuordnungStatus.RELEVANT));
    }

    @Test
    void getStatusFromDerivatFahrzeugmerkmalisNotRelevantFalseAuspraegungenIsEmpty() {
        when(derivatFahrzeugmerkmal.isNotRelevant()).thenReturn(Boolean.FALSE);
        when(derivatFahrzeugmerkmal.getAuspraegungen()).thenReturn(auspraegungen);
        when(auspraegungen.isEmpty()).thenReturn(Boolean.TRUE);
        final FahrzeugmerkmalDerivatZuordnungStatus status = FahrzeugmerkmalDerivatZuordnungStatus.getStatusFromDerivatFahrzeugmerkmal(derivatFahrzeugmerkmal);
        assertThat(status, is(FahrzeugmerkmalDerivatZuordnungStatus.NICHT_BEWERTET));
    }

}