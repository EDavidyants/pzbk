package de.interfaceag.bmw.pzbk.fahrzeugmerkmal;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

class FahrzeugmerkmalAuspraegungDaoIT extends IntegrationTest {

    private FahrzeugmerkmalDao fahrzeugmerkmalDao = new FahrzeugmerkmalDao();
    private FahrzeugmerkmalAuspraegungDao fahrzeugmerkmalAuspraegungDao = new FahrzeugmerkmalAuspraegungDao();

    @BeforeEach
    void setUp() throws IllegalAccessException {
        // start transaction before test case
        super.begin();

        // override field entityManager in fahrzeugmerkmalAuspraegungDao with private access
        FieldUtils.writeField(fahrzeugmerkmalDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(fahrzeugmerkmalAuspraegungDao, "entityManager", super.getEntityManager(), true);

        // write entity to h2 database
        final Fahrzeugmerkmal fahrzeugmerkmal = new Fahrzeugmerkmal("merkmal");
        fahrzeugmerkmalDao.save(fahrzeugmerkmal);

        fahrzeugmerkmalAuspraegungDao.save(new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "1"));
        super.commit();
        super.begin();
    }

    @Test
    void getAll() {
        final List<FahrzeugmerkmalAuspraegung> all = fahrzeugmerkmalAuspraegungDao.getAll();
        assertThat(all, hasSize(1));
    }
}