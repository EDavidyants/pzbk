package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalAuspraegungDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.SelectedFahrzeugmerkmalData;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalDerivatZuordnungServiceTest {

    @Mock
    private FahrzeugmerkmalDerivatZuordnungDao fahrzeugmerkmalDerivatZuordnungDao;

    @Mock
    private FahrzeugmerkmalService fahrzeugmerkmalService;

    @InjectMocks
    private FahrzeugmerkmalDerivatZuordnungService fahrzeugmerkmalDerivatZuordnungService;

    @Mock
    private DerivatFahrzeugmerkmal derivatFahrzeugmerkmal;

    @Mock
    private FahrzeugmerkmalDerivatDto selectedFahrzeugmerkmal;

    @Mock
    private List<FahrzeugmerkmalAuspraegungDerivatDto> selectedAuspraegungenForMerkmalAndDerivat;

    @Mock
    private FahrzeugmerkmalId fahrzeugmerkmalId;

    @Mock
    private FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung;
    private List<FahrzeugmerkmalAuspraegung> allAuspraegungenForFahrzeugmerkmal;

    @Mock
    private Derivat derivat;

    @Mock
    private FahrzeugmerkmalDerivatDto alreadySetFahrzeugmerkmal;
    private List<FahrzeugmerkmalDerivatDto> alreadySetFahrzeugmerkmale;

    @Mock
    private SelectedFahrzeugmerkmalData selectedFahrzeugmerkmalData;

    @Mock
    private FahrzeugmerkmalDerivatDto fahrzeugmerkmalDerivatDto;

    @Mock
    private Fahrzeugmerkmal fahrzeugmerkmal;
    private List<Fahrzeugmerkmal> allFahrzeugmerkmale;

    @Mock
    private DerivatFahrzeugmerkmalService derivatFahrzeugmerkmalService;

    @Mock
    private DerivatService derivatService;

    @BeforeEach
    void setUp() {
        alreadySetFahrzeugmerkmale = Collections.singletonList(alreadySetFahrzeugmerkmal);
        allFahrzeugmerkmale = Collections.singletonList(fahrzeugmerkmal);
        allAuspraegungenForFahrzeugmerkmal = Collections.singletonList(fahrzeugmerkmalAuspraegung);
    }

    @Test
    void save() {
        fahrzeugmerkmalDerivatZuordnungService.save(derivatFahrzeugmerkmal);
        verify(fahrzeugmerkmalDerivatZuordnungDao, times(1)).save(derivatFahrzeugmerkmal);
    }

    @Test
    void getAllFahrzeugmerkmaleForDerivatDerivatNotFound() {
        final List<FahrzeugmerkmalDerivatDto> result = fahrzeugmerkmalDerivatZuordnungService.getAllFahrzeugmerkmaleForDerivat(derivat);
        assertThat(result, empty());
    }

    @Test
    void getAllFahrzeugmerkmaleForDerivatDerivatFoundResultSize() {
        when(fahrzeugmerkmalDerivatZuordnungDao.getAllFahrzeugmerkmaleForDerivat(any())).thenReturn(alreadySetFahrzeugmerkmale);
        when(fahrzeugmerkmalService.getAllFahrzeugmerkmaleNotBewertetForDerivat(derivat)).thenReturn(allFahrzeugmerkmale);
        when(fahrzeugmerkmal.getFahrzeugmerkmalId()).thenReturn(fahrzeugmerkmalId);
        when(fahrzeugmerkmal.getMerkmal()).thenReturn("Merkmal");
        final List<FahrzeugmerkmalDerivatDto> result = fahrzeugmerkmalDerivatZuordnungService.getAllFahrzeugmerkmaleForDerivat(derivat);
        assertThat(result, hasSize(2));
    }


    @Test
    void getAllFahrzeugmerkmaleForDerivatDerivatFoundResultValues() {
        when(derivat.getDerivatId()).thenReturn(new DerivatId(42L));
        when(fahrzeugmerkmalDerivatZuordnungDao.getAllFahrzeugmerkmaleForDerivat(any())).thenReturn(alreadySetFahrzeugmerkmale);
        when(fahrzeugmerkmalService.getAllFahrzeugmerkmaleNotBewertetForDerivat(derivat)).thenReturn(allFahrzeugmerkmale);
        when(fahrzeugmerkmal.getFahrzeugmerkmalId()).thenReturn(fahrzeugmerkmalId);
        when(fahrzeugmerkmal.getMerkmal()).thenReturn("Merkmal");
        final List<FahrzeugmerkmalDerivatDto> result = fahrzeugmerkmalDerivatZuordnungService.getAllFahrzeugmerkmaleForDerivat(derivat);
        FahrzeugmerkmalDerivatDto newFahrzeugmerkmalDto = new FahrzeugmerkmalDerivatDto(fahrzeugmerkmalId, "Merkmal", new DerivatId(42L), FahrzeugmerkmalDerivatZuordnungStatus.NICHT_BEWERTET, null);
        assertThat(result, IsIterableContaining.hasItems(alreadySetFahrzeugmerkmal, newFahrzeugmerkmalDto));
    }

    @Test
    void getSelectedAuspraegungenForMerkmalAndDerivat() {
        when(fahrzeugmerkmalDerivatZuordnungDao.getAlreadySelectedAuspraegungenForFahrzeugmerkmalAndDerivat(any(), any())).thenReturn(selectedAuspraegungenForMerkmalAndDerivat);
        final List<FahrzeugmerkmalAuspraegungDerivatDto> result = fahrzeugmerkmalDerivatZuordnungService.getAlreadySelectedAuspraegungenForFahrzeugmerkmalAndDerivat(selectedFahrzeugmerkmal);
        assertThat(result, is(selectedAuspraegungenForMerkmalAndDerivat));
    }

    @Test
    void getAllAuspraegungenForFahrzeugmerkmalResultSize() {
        when(fahrzeugmerkmalService.getAllAuspraegungenForFahrzeugmerkmal(fahrzeugmerkmalId)).thenReturn(allAuspraegungenForFahrzeugmerkmal);
        when(fahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegungId()).thenReturn(new FahrzeugmerkmalAuspraegungId(1L));
        when(fahrzeugmerkmalAuspraegung.getAuspraegung()).thenReturn("Auspraegungen");
        final List<FahrzeugmerkmalAuspraegungDerivatDto> result = fahrzeugmerkmalDerivatZuordnungService.getAllAuspraegungenForFahrzeugmerkmal(fahrzeugmerkmalId);

        assertThat(result, hasSize(1));
    }

    @Test
    void getAllAuspraegungenForFahrzeugmerkmalResultValue() {
        when(fahrzeugmerkmalService.getAllAuspraegungenForFahrzeugmerkmal(fahrzeugmerkmalId)).thenReturn(allAuspraegungenForFahrzeugmerkmal);
        when(fahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegungId()).thenReturn(new FahrzeugmerkmalAuspraegungId(1L));
        when(fahrzeugmerkmalAuspraegung.getAuspraegung()).thenReturn("Auspraegungen");
        final List<FahrzeugmerkmalAuspraegungDerivatDto> result = fahrzeugmerkmalDerivatZuordnungService.getAllAuspraegungenForFahrzeugmerkmal(fahrzeugmerkmalId);

        FahrzeugmerkmalAuspraegungDerivatDto fahrzeugmerkmalAuspraegungDerivatDto = new FahrzeugmerkmalAuspraegungDerivatDto(new FahrzeugmerkmalAuspraegungId(1L), "Auspraegungen");
        assertThat(result, IsIterableContaining.hasItem(fahrzeugmerkmalAuspraegungDerivatDto));
    }

    @Test
    void persistAuspraegung_notRelevant() {
        when(selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto()).thenReturn(fahrzeugmerkmalDerivatDto);
        when(selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto().getNotRelevant()).thenReturn(false);
        when(fahrzeugmerkmalService.findFahrzeugmerkmal(any())).thenReturn(Optional.of(fahrzeugmerkmal));
        when(fahrzeugmerkmalService.findFahrzeugmerkmalAuspraegungFromSelectedFahrzeugmerkmal(selectedFahrzeugmerkmalData)).thenReturn(allAuspraegungenForFahrzeugmerkmal);
        when(derivatFahrzeugmerkmalService.getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId(any(), any())).thenReturn(derivatFahrzeugmerkmal);

        fahrzeugmerkmalDerivatZuordnungService.persistAuspraegung(selectedFahrzeugmerkmalData);

        verify(fahrzeugmerkmalService, times(1)).findFahrzeugmerkmal(any());
        verify(fahrzeugmerkmalService, times(1)).findFahrzeugmerkmalAuspraegungFromSelectedFahrzeugmerkmal(any());
        verify(derivatFahrzeugmerkmalService, times(1)).getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId(any(), any());

        verify(fahrzeugmerkmalDerivatZuordnungDao, times(1)).save(any());
    }

    @Test
    void persistAuspraegung_relevant() {
        when(selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto()).thenReturn(fahrzeugmerkmalDerivatDto);
        when(selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto().getNotRelevant()).thenReturn(true);
        when(selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto().getDerivatId()).thenReturn(new DerivatId(1L));

        when(fahrzeugmerkmalService.findFahrzeugmerkmal(any())).thenReturn(Optional.of(fahrzeugmerkmal));
        when(fahrzeugmerkmalService.findFahrzeugmerkmalAuspraegungFromSelectedFahrzeugmerkmal(selectedFahrzeugmerkmalData)).thenReturn(allAuspraegungenForFahrzeugmerkmal);
        when(derivatService.getDerivatById(any())).thenReturn(derivat);

        fahrzeugmerkmalDerivatZuordnungService.persistAuspraegung(selectedFahrzeugmerkmalData);

        verify(fahrzeugmerkmalDerivatZuordnungDao, times(1)).save(any());
    }
}