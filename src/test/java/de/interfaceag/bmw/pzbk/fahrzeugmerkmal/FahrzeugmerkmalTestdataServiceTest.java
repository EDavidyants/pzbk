package de.interfaceag.bmw.pzbk.fahrzeugmerkmal;

import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.testdata.FahrzeugmerkmalTestdataService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalTestdataServiceTest {

    @Mock
    private FahrzeugmerkmalService fahrzeugmerkmalService;
    @Mock
    private ModulService modulService;
    @InjectMocks
    private FahrzeugmerkmalTestdataService fahrzeugmerkmalTestdataService;

    @Mock
    private List<FahrzeugmerkmalAuspraegung> auspraegungen;

    @BeforeEach
    void setUp() {
        when(fahrzeugmerkmalService.getAllFahrzeugmerkmalAuspraegungen()).thenReturn(auspraegungen);
    }

    @Test
    void generateTestdataDataPresent() {
        when(auspraegungen.isEmpty()).thenReturn(false);
        fahrzeugmerkmalTestdataService.generateTestdata();
        verify(fahrzeugmerkmalService, never()).saveFahrzeugmerkmal(any());
        verify(fahrzeugmerkmalService, never()).saveFahrzeugmerkmalAuspraegung(any());
    }

    @Test
    void generateTestdataNoDataPresent() {
        when(auspraegungen.isEmpty()).thenReturn(true);
        fahrzeugmerkmalTestdataService.generateTestdata();
        verify(fahrzeugmerkmalService, times(4)).saveFahrzeugmerkmal(any());
        verify(fahrzeugmerkmalService, times(11)).saveFahrzeugmerkmalAuspraegung(any());
    }
}