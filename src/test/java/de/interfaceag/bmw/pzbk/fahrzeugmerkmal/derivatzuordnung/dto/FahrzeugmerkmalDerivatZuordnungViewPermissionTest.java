package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;

class FahrzeugmerkmalDerivatZuordnungViewPermissionTest {

    private FahrzeugmerkmalDerivatZuordnungViewPermission viewPermission;
    private Collection<DerivatId> derivatIds = new ArrayList<>();
    private DerivatId derivatId = new DerivatId(1L);

    @BeforeEach
    void setUp() {
    }

    @Test
    void getPageDefaultConstructor() {
        viewPermission = new FahrzeugmerkmalDerivatZuordnungViewPermission();
        final boolean page = viewPermission.getPage();
        assertFalse(page);
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getPageDerivatIdsContainsNoDerivatId(Rolle role) {
        Set<Rolle> roles = Collections.singleton(role);
        viewPermission = new FahrzeugmerkmalDerivatZuordnungViewPermission(roles, derivatIds, derivatId);
        boolean expectedPermission;
        switch (role) {
            case ADMIN:
                expectedPermission = Boolean.TRUE;
                break;
            default:
                expectedPermission = Boolean.FALSE;
                break;
        }

        final boolean page = viewPermission.getPage();
        MatcherAssert.assertThat(page, CoreMatchers.is(expectedPermission));
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getPageDerivatIdsContainsDerivat(Rolle role) {
        Set<Rolle> roles = Collections.singleton(role);
        derivatIds.add(derivatId);
        viewPermission = new FahrzeugmerkmalDerivatZuordnungViewPermission(roles, derivatIds, derivatId);
        boolean expectedPermission;
        switch (role) {
            case ADMIN:
            case ANFORDERER:
                expectedPermission = Boolean.TRUE;
                break;
            default:
                expectedPermission = Boolean.FALSE;
                break;
        }

        final boolean page = viewPermission.getPage();
        MatcherAssert.assertThat(page, CoreMatchers.is(expectedPermission));
    }
}