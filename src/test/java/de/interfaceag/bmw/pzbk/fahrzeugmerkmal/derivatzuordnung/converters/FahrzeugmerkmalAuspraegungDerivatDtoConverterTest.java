package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.converters;

import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalAuspraegungDerivatDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

class FahrzeugmerkmalAuspraegungDerivatDtoConverterTest {

    @Test
    void testForFahrzeugmerkmalAuspraeungen() {
        List<FahrzeugmerkmalAuspraegung> fahrzeugmerkmalAuspraegungen = new ArrayList<>();
        fahrzeugmerkmalAuspraegungen.add(new FahrzeugmerkmalAuspraegung(new Fahrzeugmerkmal("Merkmal"), "Auspraegung"));

        List<FahrzeugmerkmalAuspraegungDerivatDto> result = FahrzeugmerkmalAuspraegungDerivatDtoConverter.forFahrzeugmerkmalAuspraeungen(fahrzeugmerkmalAuspraegungen);

        assertThat(result, hasSize(1));
        assertThat(result.get(0).toString(), is("FahrzeugmerkmalAuspraegungDerivatDto{fahrzeugmerkmalAuspraegungId=FahrzeugmerkmalAuspraegungId[id=null], auspraegung='Auspraegung'}"));

    }
}