package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalDerivatZuordnungFilterTest {

    FahrzeugmerkmalDerivatZuordnungFilter filter;

    @Mock
    UrlParameter urlParameter;

    @BeforeEach
    public void setUp() {
        when(urlParameter.getValue(any())).thenReturn(Optional.of("E20"));
        filter = new FahrzeugmerkmalDerivatZuordnungFilter(urlParameter);
    }

    @Test
    void getDerivatFilter() {
        String reset = filter.filter();
        assertThat(reset, containsString("derivat=E20"));
    }

    @Test
    void filter() {
        String reset = filter.filter();
        assertThat(reset, startsWith("fahrzeugmerkmalDerivatzuordnung.xhtml"));
    }

    @Test
    void reset() {
        String reset = filter.reset();
        assertThat(reset, startsWith("fahrzeugmerkmalDerivatzuordnung.xhtml?faces-redirect=true"));
    }
}