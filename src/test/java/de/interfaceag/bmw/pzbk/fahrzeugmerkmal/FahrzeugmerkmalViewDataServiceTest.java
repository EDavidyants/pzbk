package de.interfaceag.bmw.pzbk.fahrzeugmerkmal;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.FahrzeugmerkmalViewDataService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.api.FahrzeugmerkmalTableEntry;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalViewData;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalViewDataServiceTest {

    @Mock
    private FahrzeugmerkmalService fahrzeugmerkmalService;
    @InjectMocks
    private FahrzeugmerkmalViewDataService fahrzeugmerkmalViewDataService;

    @Mock
    private FahrzeugmerkmalDto fahrzeugmerkmalDto;
    private Collection<FahrzeugmerkmalDto> allFahrzeugmerkmale;

    @BeforeEach
    void setUp() {
        allFahrzeugmerkmale = Collections.singletonList(fahrzeugmerkmalDto);
        when(fahrzeugmerkmalService.getAllFahrzeugmerkmalTableEntries()).thenReturn(allFahrzeugmerkmale);

    }

    @Test
    void getViewDataType() {
        final FahrzeugmerkmalViewData viewData = fahrzeugmerkmalViewDataService.getViewData();
        MatcherAssert.assertThat(viewData, instanceOf(FahrzeugmerkmalViewData.class));
    }

    @Test
    void getViewDataFahrzeugmerkmalValues() {
        final FahrzeugmerkmalViewData viewData = fahrzeugmerkmalViewDataService.getViewData();
        final List<FahrzeugmerkmalTableEntry> fahrzeugmerkmalTableEntries = viewData.getFahrzeugmerkmalTableEntries();
        MatcherAssert.assertThat(fahrzeugmerkmalTableEntries, hasSize(1));
    }
}