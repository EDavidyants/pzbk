package de.interfaceag.bmw.pzbk.fahrzeugmerkmal;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class FahrzeugmerkmalDaoIT extends IntegrationTest {

    private FahrzeugmerkmalDao fahrzeugmerkmalDao = new FahrzeugmerkmalDao();

    private Fahrzeugmerkmal fahrzeugmerkmal = new Fahrzeugmerkmal("1");

    @BeforeEach
    void setUp() throws IllegalAccessException {

        // start transaction before test case
        super.begin();

        // override field entityManager in fahrzeugmerkmalDao with private access
        FieldUtils.writeField(fahrzeugmerkmalDao, "entityManager", super.getEntityManager(), true);

        // write entity to h2 database
        fahrzeugmerkmalDao.save(fahrzeugmerkmal);
        super.commit();
        super.begin();
    }

    @Test
    void saveExistingFahrzeugmerkmal() {
        final Optional<Fahrzeugmerkmal> optionalFahrzeugmerkmal = fahrzeugmerkmalDao.find(fahrzeugmerkmal.getFahrzeugmerkmalId());
        if (optionalFahrzeugmerkmal.isPresent()) {
            final Fahrzeugmerkmal fahrzeugmerkmal = optionalFahrzeugmerkmal.get();
            fahrzeugmerkmal.setMerkmal("Neu");
            fahrzeugmerkmalDao.save(fahrzeugmerkmal);
        } else {
            fail("fahrzeugmerkmal not found");
        }
    }

    @Test
    void saveFahrzeugmerkmalAndFind() {
        final Optional<Fahrzeugmerkmal> optionalFahrzeugmerkmal = fahrzeugmerkmalDao.find(fahrzeugmerkmal.getFahrzeugmerkmalId());
        assertTrue(optionalFahrzeugmerkmal.isPresent());
    }

}