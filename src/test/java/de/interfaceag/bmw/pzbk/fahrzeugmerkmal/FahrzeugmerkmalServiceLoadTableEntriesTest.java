package de.interfaceag.bmw.pzbk.fahrzeugmerkmal;

import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalAuspraegungDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalDto;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalServiceLoadTableEntriesTest {

    private static final String AUSPRAEGUNG_VALUE_1 = "1";
    private static final String AUSPRAEGUNG_VALUE_2 = "2";
    private static final String AUSPRAEGUNG_VALUE_3 = "3";
    private static final String MERKMAL_1 = "M1";
    private static final String MERKMAL_2 = "M2";

    @Mock
    private FahrzeugmerkmalDao fahrzeugmerkmalDao;
    @Mock
    private FahrzeugmerkmalAuspraegungDao fahrzeugmerkmalAuspraegungDao;
    @InjectMocks
    private FahrzeugmerkmalService fahrzeugmerkmalService;

    @Mock
    private Fahrzeugmerkmal fahrzeugmerkmal1;
    @Mock
    private Fahrzeugmerkmal fahrzeugmerkmal2;
    @Mock
    private FahrzeugmerkmalAuspraegung auspraegung1;
    @Mock
    private FahrzeugmerkmalAuspraegung auspraegung2;
    @Mock
    private FahrzeugmerkmalAuspraegung auspraegung3;

    private List<FahrzeugmerkmalAuspraegung> allFahrzeugmerkmalAuspraegungen;

    @BeforeEach
    void setUp() {
        when(fahrzeugmerkmal1.getMerkmal()).thenReturn(MERKMAL_1);
        when(fahrzeugmerkmal2.getMerkmal()).thenReturn(MERKMAL_2);

        when(auspraegung1.getFahrzeugmerkmal()).thenReturn(fahrzeugmerkmal1);
        when(auspraegung2.getFahrzeugmerkmal()).thenReturn(fahrzeugmerkmal1);
        when(auspraegung3.getFahrzeugmerkmal()).thenReturn(fahrzeugmerkmal2);

        when(auspraegung1.getDto()).thenReturn(new FahrzeugmerkmalAuspraegungDto(AUSPRAEGUNG_VALUE_1));
        when(auspraegung2.getDto()).thenReturn(new FahrzeugmerkmalAuspraegungDto(AUSPRAEGUNG_VALUE_2));
        when(auspraegung3.getDto()).thenReturn(new FahrzeugmerkmalAuspraegungDto(AUSPRAEGUNG_VALUE_3));

        allFahrzeugmerkmalAuspraegungen = Arrays.asList(auspraegung1, auspraegung2, auspraegung3);
        when(fahrzeugmerkmalAuspraegungDao.getAll()).thenReturn(allFahrzeugmerkmalAuspraegungen);
    }

    @Test
    void getAllFahrzeugmerkmalTableEntriesResultSize() {
        final Collection<FahrzeugmerkmalDto> result = fahrzeugmerkmalService.getAllFahrzeugmerkmalTableEntries();
        assertThat(result, hasSize(2));
    }

    @Test
    void getAllFahrzeugmerkmalTableEntriesResultValues() {
        final Collection<FahrzeugmerkmalDto> result = fahrzeugmerkmalService.getAllFahrzeugmerkmalTableEntries();

        final FahrzeugmerkmalDto fahrzeugmerkmalDto1 = new FahrzeugmerkmalDto(0L, MERKMAL_1, Collections.emptyList());
        fahrzeugmerkmalDto1.addAuspraegung(new FahrzeugmerkmalAuspraegungDto(AUSPRAEGUNG_VALUE_1));
        fahrzeugmerkmalDto1.addAuspraegung(new FahrzeugmerkmalAuspraegungDto(AUSPRAEGUNG_VALUE_2));

        final FahrzeugmerkmalDto fahrzeugmerkmalDto2 = new FahrzeugmerkmalDto(0L, MERKMAL_2, Collections.emptyList());
        fahrzeugmerkmalDto2.addAuspraegung(new FahrzeugmerkmalAuspraegungDto(AUSPRAEGUNG_VALUE_3));

        assertThat(result, IsIterableContaining.hasItems(fahrzeugmerkmalDto1, fahrzeugmerkmalDto2));
    }
}