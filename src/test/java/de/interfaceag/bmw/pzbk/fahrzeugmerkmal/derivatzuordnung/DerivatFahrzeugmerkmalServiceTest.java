package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DerivatFahrzeugmerkmalServiceTest {

    @Mock
    private DerivatFahrzeugmerkmalDao derivatFahrzeugmerkmalDao;

    @InjectMocks
    private DerivatFahrzeugmerkmalService derivatFahrzeugmerkmalService;

    @Test
    void getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId() {
        DerivatId derivatId = new DerivatId(42L);
        FahrzeugmerkmalId fahrzeugmerkmalId = new FahrzeugmerkmalId(43L);

        DerivatFahrzeugmerkmal response = mock(DerivatFahrzeugmerkmal.class);
        when(derivatFahrzeugmerkmalDao.getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId(derivatId, fahrzeugmerkmalId)).thenReturn(response);

        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal = derivatFahrzeugmerkmalService.getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId(derivatId, fahrzeugmerkmalId);

        verify(derivatFahrzeugmerkmalDao, times(1)).getDerivatFahrzeugmerkmalByDerivatAndFahrzeugmerkmalId(derivatId, fahrzeugmerkmalId);
        Assert.assertNotNull(derivatFahrzeugmerkmal);
    }
}