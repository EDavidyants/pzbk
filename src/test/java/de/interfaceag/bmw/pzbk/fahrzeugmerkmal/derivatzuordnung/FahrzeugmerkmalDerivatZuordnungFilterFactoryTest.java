package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ContextMocker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalDerivatZuordnungFilterFactoryTest {

    @InjectMocks
    private FahrzeugmerkmalDerivatZuordnungFilterFactory fahrzeugmerkmalDerivatZuordnungFilterFactory;

    @BeforeEach
    void setUp() {
        ContextMocker.mockFacesContextForUrlParamterUtils();
    }

    @Test
    void buildFahrzeugmerkmalDerivatZuordnungFilter() {
        final FahrzeugmerkmalDerivatZuordnungFilter fahrzeugmerkmalDerivatZuordnungFilter = fahrzeugmerkmalDerivatZuordnungFilterFactory.buildFahrzeugmerkmalDerivatZuordnungFilter();
        final String filter = fahrzeugmerkmalDerivatZuordnungFilter.filter();
        assertThat(filter, containsString(Page.FAHRZEUGMERMAL_DERIVAT_ZUORDNUNG.getUrl()));
    }
}