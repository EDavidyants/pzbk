package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto;

import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsIterableContaining.hasItem;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalDtoTest {

    private FahrzeugmerkmalDto dto;

    @Mock
    private FahrzeugmerkmalModulSeTeamDto fahrzeugmerkmalModulSeTeamDto1;
    @Mock
    private FahrzeugmerkmalModulSeTeamDto fahrzeugmerkmalModulSeTeamDto2;
    private List<FahrzeugmerkmalModulSeTeamDto> modulSeTeams;

    @Mock
    private FahrzeugmerkmalAuspraegungDto fahrzeugmerkmalAuspraegungDto1;
    @Mock
    private FahrzeugmerkmalAuspraegungDto fahrzeugmerkmalAuspraegungDto2;

    @BeforeEach
    void setUp() {
        modulSeTeams = Arrays.asList(fahrzeugmerkmalModulSeTeamDto1, fahrzeugmerkmalModulSeTeamDto2);
        dto = new FahrzeugmerkmalDto(1L, "Merk dir das mal", modulSeTeams);
    }

    @Test
    void equalsNull() {
        final boolean result = dto.equals(null);
        assertFalse(result);
    }

    @Test
    void equalsOtherObject() {
        final boolean result = dto.equals(new FahrzeugmerkmalId(1L));
        assertFalse(result);
    }

    @Test
    void equalsOtherId() {
        FahrzeugmerkmalDto otherDto = new FahrzeugmerkmalDto(2L, "Merk dir das mal", modulSeTeams);
        final boolean result = dto.equals(otherDto);
        assertFalse(result);
    }

    @Test
    void equalsOtherMerkmal() {
        FahrzeugmerkmalDto otherDto = new FahrzeugmerkmalDto(1L, "Merk dir das auch mal", modulSeTeams);
        final boolean result = dto.equals(otherDto);
        assertFalse(result);
    }

    @Test
    void equalsOtherModule() {
        FahrzeugmerkmalDto otherDto = new FahrzeugmerkmalDto(1L, "Merk dir das mal", Collections.emptyList());
        final boolean result = dto.equals(otherDto);
        assertFalse(result);
    }

    @Test
    void equalsOtherEqual() {
        FahrzeugmerkmalDto otherDto = new FahrzeugmerkmalDto(1L, "Merk dir das mal", modulSeTeams);
        final boolean result = dto.equals(otherDto);
        assertTrue(result);
    }

    @Test
    void equalsSame() {
        final boolean result = dto.equals(dto);
        assertTrue(result);
    }

    @Test
    void compareTo() {
        FahrzeugmerkmalDto otherDto = new FahrzeugmerkmalDto(1L, "Merk dir das auch mal", modulSeTeams);
        final int expectedResult = "Merk dir das mal".compareTo("Merk dir das auch mal");
        final int result = dto.compareTo(otherDto);
        assertThat(result, is(expectedResult));
    }

    @Test
    void getMerkmal() {
        final String merkmal = dto.getMerkmal();
        assertThat(merkmal, is("Merk dir das mal"));
    }

    @Test
    void getAuspraegungen() {
        when(fahrzeugmerkmalAuspraegungDto1.getAuspraegung()).thenReturn("Auspraegung 1");
        when(fahrzeugmerkmalAuspraegungDto2.getAuspraegung()).thenReturn("Auspraegung 2");
        dto.addAuspraegung(fahrzeugmerkmalAuspraegungDto1);
        dto.addAuspraegung(fahrzeugmerkmalAuspraegungDto2);
        final String auspraegungen = dto.getAuspraegungen();
        assertThat(auspraegungen, is("Auspraegung 1, Auspraegung 2"));
    }

    @Test
    void getAuspraegungenForFahrzeugmerkmal() {
        dto.addAuspraegung(fahrzeugmerkmalAuspraegungDto1);
        final List<FahrzeugmerkmalAuspraegungDto> result = dto.getAuspraegungenForFahrzeugmerkmal();
        assertThat(result, hasSize(1));
        assertThat(result, hasItem(fahrzeugmerkmalAuspraegungDto1));
    }

    @Test
    void getModule() {
        when(fahrzeugmerkmalModulSeTeamDto1.getBezeichnung()).thenReturn("Modul 1");
        when(fahrzeugmerkmalModulSeTeamDto2.getBezeichnung()).thenReturn("Modul 2");
        final String module = dto.getModule();
        assertThat(module, is("Modul 1, Modul 2"));
    }

    @Test
    void getId() {
        final Long id = dto.getId();
        assertThat(id, is(1L));
    }

    @Test
    void getModuleForFahrzeugmerkmal() {
        final List<FahrzeugmerkmalModulSeTeamDto> dtoModuleForFahrzeugmerkmal = dto.getModuleForFahrzeugmerkmal();
        assertThat(dtoModuleForFahrzeugmerkmal, is(modulSeTeams));
    }

    @Test
    void hashCode1() {
        dto = new FahrzeugmerkmalDto(1L, "Merk dir das mal", Collections.emptyList());
        final int result = dto.hashCode();
        assertThat(result, is(1856488368));
    }
}