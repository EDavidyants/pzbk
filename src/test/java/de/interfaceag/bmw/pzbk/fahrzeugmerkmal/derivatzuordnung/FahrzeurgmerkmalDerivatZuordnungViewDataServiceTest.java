package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatZuordnungViewData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatZuordnungViewPermission;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeurgmerkmalDerivatZuordnungViewDataServiceTest {

    @Mock
    private DerivatService derivatService;
    @Mock
    private FahrzeugmerkmalDerivatZuordnungService fahrzeugmerkmalDerivatZuordnungService;
    @Mock
    private FahrzeugmerkmalDerivatZuordnungViewPermissionFactory fahrzeugmerkmalDerivatZuordnungViewPermissionFactory;
    @InjectMocks
    private FahrzeurgmerkmalDerivatZuordnungViewDataService viewDataService;

    @Mock
    private UrlParameter urlParameter;
    @Mock
    private List<FahrzeugmerkmalDerivatDto> allFahrzeugmerkmaleForDerivat;
    @Mock
    private Derivat derivat;
    @Mock
    private FahrzeugmerkmalDerivatZuordnungViewPermission viewPermissionWithoutDerivat;
    @Mock
    private FahrzeugmerkmalDerivatZuordnungViewPermission viewPermissionForDerivat;

    @Test
    void getFahrzeugmerkmalDerivatZuordnungViewDataNoDerivatSelected() {
        when(urlParameter.getValue("derivat")).thenReturn(Optional.empty());
        final FahrzeugmerkmalDerivatZuordnungViewData viewData = viewDataService.buildFahrzeugmerkmalDerivatZuordnungViewDataForUrlParameter(urlParameter);
        final List<FahrzeugmerkmalDerivatDto> allFahrzeugmerkmaleForDerivat = viewData.getAllFahrzeugmerkmaleForDerivat();
        assertThat(allFahrzeugmerkmaleForDerivat, empty());
    }

    @Test
    void getFahrzeugmerkmalDerivatZuordnungViewDataDerivatSelectedButNotFoundFahrzeugmerkmalValues() {
        when(urlParameter.getValue("derivat")).thenReturn(Optional.of("E24"));
        when(derivatService.getDerivatByName(any())).thenReturn(null);
        when(fahrzeugmerkmalDerivatZuordnungViewPermissionFactory.buildFahrzeugmerkmalDerivatZuordnungViewPermissionForDerivatNotPresent()).thenReturn(viewPermissionWithoutDerivat);
        final FahrzeugmerkmalDerivatZuordnungViewData viewData = viewDataService.buildFahrzeugmerkmalDerivatZuordnungViewDataForUrlParameter(urlParameter);
        final List<FahrzeugmerkmalDerivatDto> allFahrzeugmerkmaleForDerivat = viewData.getAllFahrzeugmerkmaleForDerivat();
        assertThat(allFahrzeugmerkmaleForDerivat, IsEmptyCollection.empty());
    }

    @Test
    void getFahrzeugmerkmalDerivatZuordnungViewDataDerivatSelectedButNotFoundViewPermissionValue() {
        when(urlParameter.getValue("derivat")).thenReturn(Optional.of("E24"));
        when(derivatService.getDerivatByName(any())).thenReturn(null);
        when(fahrzeugmerkmalDerivatZuordnungViewPermissionFactory.buildFahrzeugmerkmalDerivatZuordnungViewPermissionForDerivatNotPresent()).thenReturn(viewPermissionWithoutDerivat);
        final FahrzeugmerkmalDerivatZuordnungViewData viewData = viewDataService.buildFahrzeugmerkmalDerivatZuordnungViewDataForUrlParameter(urlParameter);
        final FahrzeugmerkmalDerivatZuordnungViewPermission viewPermission = viewData.getViewPermission();
        assertThat(viewPermission, is(viewPermissionWithoutDerivat));
    }

    @Test
    void getFahrzeugmerkmalDerivatZuordnungViewDataDerivatSelectedFahrzeugmerkmalValues() {
        when(urlParameter.getValue("derivat")).thenReturn(Optional.of("E24"));
        when(derivatService.getDerivatByName(any())).thenReturn(derivat);
        when(fahrzeugmerkmalDerivatZuordnungViewPermissionFactory.buildFahrzeugmerkmalDerivatZuordnungViewPermissionForDerivat(any())).thenReturn(viewPermissionForDerivat);
        when(fahrzeugmerkmalDerivatZuordnungService.getAllFahrzeugmerkmaleForDerivat(any())).thenReturn(allFahrzeugmerkmaleForDerivat);
        final FahrzeugmerkmalDerivatZuordnungViewData viewData = viewDataService.buildFahrzeugmerkmalDerivatZuordnungViewDataForUrlParameter(urlParameter);
        final List<FahrzeugmerkmalDerivatDto> allFahrzeugmerkmaleForDerivat = viewData.getAllFahrzeugmerkmaleForDerivat();
        assertThat(allFahrzeugmerkmaleForDerivat, is(allFahrzeugmerkmaleForDerivat));
    }

    @Test
    void getFahrzeugmerkmalDerivatZuordnungViewDataDerivatSelectedViewPermissionValue() {
        when(urlParameter.getValue("derivat")).thenReturn(Optional.of("E24"));
        when(derivatService.getDerivatByName(any())).thenReturn(derivat);
        when(fahrzeugmerkmalDerivatZuordnungViewPermissionFactory.buildFahrzeugmerkmalDerivatZuordnungViewPermissionForDerivat(any())).thenReturn(viewPermissionForDerivat);
        when(fahrzeugmerkmalDerivatZuordnungService.getAllFahrzeugmerkmaleForDerivat(any())).thenReturn(allFahrzeugmerkmaleForDerivat);
        final FahrzeugmerkmalDerivatZuordnungViewData viewData = viewDataService.buildFahrzeugmerkmalDerivatZuordnungViewDataForUrlParameter(urlParameter);
        final FahrzeugmerkmalDerivatZuordnungViewPermission viewPermission = viewData.getViewPermission();
        assertThat(viewPermission, is(viewPermissionForDerivat));
    }
}