package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung;

import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatDto;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatZuordnungViewData;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmalDerivatZuordnungViewPermission;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.SelectedFahrzeugmerkmalData;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalDerivatZuordnungControllerTest {

    @Mock
    private FahrzeugmerkmalDerivatZuordnungFacade facade;
    @Mock
    private FahrzeugmerkmalDerivatZuordnungViewData viewData;
    @InjectMocks
    private FahrzeugmerkmalDerivatZuordnungController controller;

    @Mock
    private FahrzeugmerkmalDerivatDto fahrzeugmerkmalDerivatDto;
    @Mock
    private SelectedFahrzeugmerkmalData selectedFahrzeugmerkmalData;
    @Mock
    private FahrzeugmerkmalDerivatZuordnungFilter filter;
    @Mock
    private FahrzeugmerkmalDerivatZuordnungViewPermission viewPermission;

    @Test
    void showDetailForFahrzeugmerkmal() {
        when(facade.getSelectedFahrzeugmerkmalDataForFahrzeugmerkmal(fahrzeugmerkmalDerivatDto)).thenReturn(selectedFahrzeugmerkmalData);
        controller.showDetailForFahrzeugmerkmal(fahrzeugmerkmalDerivatDto);
        final SelectedFahrzeugmerkmalData controllerSelectedFahrzeugmerkmalData = controller.getSelectedFahrzeugmerkmalData();
        assertThat(controllerSelectedFahrzeugmerkmalData, is(selectedFahrzeugmerkmalData));
    }

    @Test
    void getAllFahrzeugmerkmaleForDerivat() {
        final List<FahrzeugmerkmalDerivatDto> result = controller.getAllFahrzeugmerkmaleForDerivat();
        assertThat(result, is(viewData.getAllFahrzeugmerkmaleForDerivat()));
    }

    @Test
    void testPersistSelectedAuspraegung() {
        controller.persistSelectedAuspraegung();
        verify(facade, times(1)).persistSelectedAuspraegung(selectedFahrzeugmerkmalData);
    }

    @Test
    void testGetViewPermission() {
        controller.getViewPermission();
        verify(viewData, times(1)).getViewPermission();

    }

    @Test
    void checkboxClickedAndRelevant() {
        when(selectedFahrzeugmerkmalData.getFahrzeugmerkmalDerivatDto()).thenReturn(fahrzeugmerkmalDerivatDto);
        when(fahrzeugmerkmalDerivatDto.getNotRelevant()).thenReturn(true);

        controller.checkboxClicked();
        Assert.assertTrue(selectedFahrzeugmerkmalData.getSelectedAuspraegungen().isEmpty());
    }
}