package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungModulSeTeamFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalService;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.history.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import de.interfaceag.bmw.pzbk.shared.objectIds.ModulSeTeamId;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.emptyCollectionOf;
import static org.hamcrest.core.IsIterableContaining.hasItem;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AnforderungModulSeTeamFahrzeugmerkmalAuspraegungServiceTest {

    @Mock
    private FahrzeugmerkmalService fahrzeugmerkmalService;
    @Mock
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungDao anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao;
    @InjectMocks
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService anforderungModulSeTeamFahrzeugmerkmalAuspraegungService;

    @Mock
    private ModulSeTeam modulSeTeam;
    private Collection<ModulSeTeam> configuredModulSeTeamsForAnforderung;

    @Mock
    private List<FahrzeugmerkmalAuspraegung> selectedFahrzeugmerkmalAuspraegungen;
    @Mock
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegung;

    @Mock
    private Anforderung anforderung;
    @Mock
    private AnforderungFreigabeBeiModulSeTeam freigabe;
    private List<AnforderungFreigabeBeiModulSeTeam> freigaben;
    @Mock
    private Session session;
    @Mock
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService anforderungModulSeTeamFahrzeugmerkmalAuspraegungHistoryService;
    @Mock
    private Fahrzeugmerkmal fahrzeugmerkmal;
    @Mock
    private FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung;
    private List<FahrzeugmerkmalAuspraegung> selectedAuspraegungenForModulSeTeam;
    private List<Fahrzeugmerkmal> fahrzeugmerkmaleForModulSeTeam;

    @BeforeEach
    void setUp() {
        configuredModulSeTeamsForAnforderung = Collections.singletonList(modulSeTeam);
        freigaben = Collections.singletonList(freigabe);
        selectedAuspraegungenForModulSeTeam = Collections.singletonList(fahrzeugmerkmalAuspraegung);
        fahrzeugmerkmaleForModulSeTeam = new ArrayList<>();
    }

    @Test
    void getConfiguredModulSeTeamsForAnforderungNullInput() {
        final Collection<ModulSeTeamId> result = anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getConfiguredModulSeTeamsForAnforderung(null);
        assertThat(result, emptyCollectionOf(ModulSeTeamId.class));
    }

    @Test
    void getConfiguredModulSeTeamsForAnforderungValidInputNoMatchInDatabase() {
        when(modulSeTeam.getModulSeTeamId()).thenReturn(new ModulSeTeamId(42L));
        when(anforderung.getAnfoFreigabeBeiModul()).thenReturn(freigaben);
        when(anforderung.getAnforderungId()).thenReturn(new AnforderungId(1L));
        when(freigabe.getModulSeTeam()).thenReturn(modulSeTeam);
        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(any(), any())).thenReturn(selectedAuspraegungenForModulSeTeam);
        when(fahrzeugmerkmalAuspraegung.getFahrzeugmerkmal()).thenReturn(fahrzeugmerkmal);
        when(fahrzeugmerkmal.getFahrzeugmerkmalId()).thenReturn(new FahrzeugmerkmalId(12L));
        when(fahrzeugmerkmalService.getFahrzeugmerkmaleForModulSeTeam(any())).thenReturn(fahrzeugmerkmaleForModulSeTeam);


        final Collection<ModulSeTeamId> result = anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getConfiguredModulSeTeamsForAnforderung(anforderung);
        assertThat(result, IsEmptyCollection.empty());
    }

    @Test
    void getConfiguredModulSeTeamsForAnforderungValidInputWithMatchInDatabase() {
        fahrzeugmerkmaleForModulSeTeam.add(fahrzeugmerkmal);

        when(modulSeTeam.getModulSeTeamId()).thenReturn(new ModulSeTeamId(42L));
        when(anforderung.getAnfoFreigabeBeiModul()).thenReturn(freigaben);
        when(anforderung.getAnforderungId()).thenReturn(new AnforderungId(1L));
        when(freigabe.getModulSeTeam()).thenReturn(modulSeTeam);
        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(any(), any())).thenReturn(selectedAuspraegungenForModulSeTeam);
        when(fahrzeugmerkmalAuspraegung.getFahrzeugmerkmal()).thenReturn(fahrzeugmerkmal);
        when(fahrzeugmerkmal.getFahrzeugmerkmalId()).thenReturn(new FahrzeugmerkmalId(12L));
        when(fahrzeugmerkmalService.getFahrzeugmerkmaleForModulSeTeam(any())).thenReturn(fahrzeugmerkmaleForModulSeTeam);

        final Collection<ModulSeTeamId> result = anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getConfiguredModulSeTeamsForAnforderung(anforderung);
        assertThat(result, hasItem(new ModulSeTeamId(42L)));
    }

    @Test
    void getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeamAnforderungNullInput() {
        final List<FahrzeugmerkmalAuspraegung> result = anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(null, new ModulSeTeamId(32L));
        assertThat(result, emptyCollectionOf(FahrzeugmerkmalAuspraegung.class));
    }

    @Test
    void getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeamModulSeTeamNullInput() {
        final List<FahrzeugmerkmalAuspraegung> result = anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(new AnforderungId(2L), null);
        assertThat(result, emptyCollectionOf(FahrzeugmerkmalAuspraegung.class));
    }

    @Test
    void getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeamAllNullInput() {
        final List<FahrzeugmerkmalAuspraegung> result = anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(null, null);
        assertThat(result, emptyCollectionOf(FahrzeugmerkmalAuspraegung.class));
    }

    @Test
    void getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeamValidInput() {
        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(any(), any())).thenReturn(selectedFahrzeugmerkmalAuspraegungen);
        final List<FahrzeugmerkmalAuspraegung> result = anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungAndModulSeTeam(new AnforderungId(2L), new ModulSeTeamId(12L));
        assertThat(result, is(selectedFahrzeugmerkmalAuspraegungen));
    }

    @Test
    void save() {
        anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.save(anforderungModulSeTeamFahrzeugmerkmalAuspraegung, false);
        verify(anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao, times(1)).save(anforderungModulSeTeamFahrzeugmerkmalAuspraegung);
    }

    @Test
    void find() {
        final AnforderungModulSeTeamFahrzeugmerkmalAuspraegungId id = new AnforderungModulSeTeamFahrzeugmerkmalAuspraegungId(1L);
        anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.find(id);
        verify(anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao, times(1)).find(id);
    }

    @Test
    void remove() {
        anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.remove(anforderungModulSeTeamFahrzeugmerkmalAuspraegung);
        verify(anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao, times(1)).remove(anforderungModulSeTeamFahrzeugmerkmalAuspraegung);
    }

    @Test
    void getSelectedFahrzeugmerkmalAuspraegungenForAnforderungModulSeTeamAndFahrzeugmerkmalAuspraegung() {
        final AnforderungId anforderungId = new AnforderungId(1L);
        final ModulSeTeamId modulSeTeamId = new ModulSeTeamId(2L);
        final FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId = new FahrzeugmerkmalAuspraegungId(3L);
        anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getSelectedFahrzeugmerkmalAuspraegungenForAnforderungModulSeTeamAndFahrzeugmerkmalAuspraegung(anforderungId, modulSeTeamId, fahrzeugmerkmalAuspraegungId);
        verify(anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao, times(1)).getSelectedFahrzeugmerkmalAuspraegungenForAnforderungModulSeTeamAndFahrzeugmerkmalAuspraegung(anforderungId, modulSeTeamId, fahrzeugmerkmalAuspraegungId);

    }
}