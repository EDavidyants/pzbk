package de.interfaceag.bmw.pzbk.fahrzeugmerkmal;

import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalServiceTest {

    @Mock
    private FahrzeugmerkmalDao fahrzeugmerkmalDao;
    @Mock
    private FahrzeugmerkmalAuspraegungDao fahrzeugmerkmalAuspraegungDao;
    @InjectMocks
    private FahrzeugmerkmalService fahrzeugmerkmalService;

    @Mock
    private Fahrzeugmerkmal fahrzeugmerkmal;
    @Mock
    private FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung;
    @Mock
    private List<FahrzeugmerkmalAuspraegung> allFahrzeugmerkmalAuspraegungen;

    @Test
    void saveFahrzeugmerkmal() {
        fahrzeugmerkmalService.saveFahrzeugmerkmal(fahrzeugmerkmal);
        verify(fahrzeugmerkmalDao, times(1)).save(fahrzeugmerkmal);
    }

    @Test
    void saveFahrzeugmerkmalAuspraegung() {
        fahrzeugmerkmalService.saveFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegung);
        verify(fahrzeugmerkmalAuspraegungDao, times(1)).save(fahrzeugmerkmalAuspraegung);
    }

    @Test
    void getAllFahrzeugmerkmalAuspraegungen() {
        when(fahrzeugmerkmalAuspraegungDao.getAll()).thenReturn(allFahrzeugmerkmalAuspraegungen);
        final List<FahrzeugmerkmalAuspraegung> auspraegungen = fahrzeugmerkmalService.getAllFahrzeugmerkmalAuspraegungen();
        assertThat(auspraegungen, is(allFahrzeugmerkmalAuspraegungen));
    }

    @Test
    void findFahrzeugmerkmal() {
        final FahrzeugmerkmalId fahrzeugmerkmalId = new FahrzeugmerkmalId(1L);
        when(fahrzeugmerkmalDao.find(fahrzeugmerkmalId)).thenReturn(Optional.of(fahrzeugmerkmal));
        final Optional<Fahrzeugmerkmal> result = fahrzeugmerkmalService.findFahrzeugmerkmal(fahrzeugmerkmalId);
        assertThat(result, is(Optional.of(fahrzeugmerkmal)));
    }

    @Test
    void findFahrzeugmerkmalAuspraegung() {
        final FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId = new FahrzeugmerkmalAuspraegungId(1L);
        when(fahrzeugmerkmalAuspraegungDao.find(fahrzeugmerkmalAuspraegungId)).thenReturn(Optional.of(fahrzeugmerkmalAuspraegung));
        final Optional<FahrzeugmerkmalAuspraegung> result = fahrzeugmerkmalService.findFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegungId);
        assertThat(result, is(Optional.of(fahrzeugmerkmalAuspraegung)));
    }
}