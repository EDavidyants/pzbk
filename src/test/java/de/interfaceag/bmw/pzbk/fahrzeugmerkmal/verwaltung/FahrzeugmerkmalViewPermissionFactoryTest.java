package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto.FahrzeugmerkmalViewPermission;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalViewPermissionFactoryTest {

    @Mock
    private Session session;
    @InjectMocks
    private FahrzeugmerkmalViewPermissionFactory fahrzeugmerkmalViewPermissionFactory;

    @Mock
    private UserPermissions<BerechtigungDto> userPermissions;
    private Set<Rolle> roles;

    @BeforeEach
    void setUp() {
        roles = Collections.singleton(Rolle.ADMIN);
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRoles()).thenReturn(roles);
    }

    @Test
    void getFahrzeugmerkmalViewPermission() {
        final FahrzeugmerkmalViewPermission result = fahrzeugmerkmalViewPermissionFactory.getFahrzeugmerkmalViewPermission();
        assertThat(result, instanceOf(FahrzeugmerkmalViewPermission.class));
    }
}