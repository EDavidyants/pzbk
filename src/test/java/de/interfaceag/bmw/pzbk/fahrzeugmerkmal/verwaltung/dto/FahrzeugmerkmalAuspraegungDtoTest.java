package de.interfaceag.bmw.pzbk.fahrzeugmerkmal.verwaltung.dto;

import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FahrzeugmerkmalAuspraegungDtoTest {

    private FahrzeugmerkmalAuspraegungDto dto;

    @BeforeEach
    void setUp() {
        dto = new FahrzeugmerkmalAuspraegungDto(1L, "Auspraegung");
    }

    @Test
    void toString1() {
        final String result = dto.toString();
        assertThat(result, is("FahrzeugmerkmalModulSeTeamDto{id=1, auspraegung='Auspraegung'}"));
    }

    @Test
    void equalsNull() {
        final boolean result = dto.equals(null);
        assertFalse(result);
    }

    @Test
    void equalsOtherClass() {
        final boolean result = dto.equals(new FahrzeugmerkmalId(1L));
        assertFalse(result);
    }

    @Test
    void equalsOtherId() {
        FahrzeugmerkmalAuspraegungDto otherDto = new FahrzeugmerkmalAuspraegungDto(2L, "Auspraegung");
        final boolean result = dto.equals(otherDto);
        assertFalse(result);
    }

    @Test
    void equalsOtherAuspraegung() {
        FahrzeugmerkmalAuspraegungDto otherDto = new FahrzeugmerkmalAuspraegungDto(1L, "Auspraegung 2");
        final boolean result = dto.equals(otherDto);
        assertFalse(result);
    }

    @Test
    void equalsOtherEqual() {
        FahrzeugmerkmalAuspraegungDto otherDto = new FahrzeugmerkmalAuspraegungDto(1L, "Auspraegung");
        final boolean result = dto.equals(otherDto);
        assertTrue(result);
    }

    @Test
    void equalsOtherSame() {
        final boolean result = dto.equals(dto);
        assertTrue(result);
    }

    @Test
    void hashCode1() {
        final int result = dto.hashCode();
        assertThat(result, is(-1162853254));
    }

    @Test
    void getId() {
        final Long id = dto.getId();
        assertThat(id, is(1L));
    }

    @Test
    void getFahrzeugmerkmalAuspraegungId() {
        final FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId = dto.getFahrzeugmerkmalAuspraegungId();
        assertThat(fahrzeugmerkmalAuspraegungId, is(new FahrzeugmerkmalAuspraegungId(1L)));
    }

    @Test
    void getAuspraegung() {
        final String auspraegung = dto.getAuspraegung();
        assertThat(auspraegung, is("Auspraegung"));
    }

    @Test
    void setAuspraegung() {
        dto.setAuspraegung("Auspraegung NEU");
        final String auspraegung = dto.getAuspraegung();
        assertThat(auspraegung, is("Auspraegung NEU"));
    }

    @Test
    void isChangedDefault() {
        final boolean changed = dto.isChanged();
        assertFalse(changed);
    }

    @Test
    void isChanged() {
        dto.setAuspraegung("Auspraegung NEU");
        final boolean changed = dto.isChanged();
        assertTrue(changed);
    }

    @Test
    void copyConstructor() {
        final FahrzeugmerkmalAuspraegungDto copy = new FahrzeugmerkmalAuspraegungDto(dto);
        assertThat(copy.getId(), is(dto.getId()));
        assertThat(copy.getAuspraegung(), is(dto.getAuspraegung()));
        assertThat(copy.getFahrzeugmerkmalAuspraegungId(), is(dto.getFahrzeugmerkmalAuspraegungId()));
    }
}