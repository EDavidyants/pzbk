package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Konzept;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class KonzeptDtoTest {

    private static final String BEZEICHNUNG = "Bezeichnung";
    private static final String BESCHREIBUNG = "Beschreibung";
    private static final String BESCHREIBUNG_LONG = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut l";

    @Mock
    private Konzept konzept;

    private List<Konzept> konzepte;

    @BeforeEach
    public void init() {
        this.konzepte = Collections.singletonList(konzept);
        when(konzept.getId()).thenReturn(1L);
        when(konzept.getBezeichnung()).thenReturn(BEZEICHNUNG);
        when(konzept.getBeschreibung()).thenReturn(BESCHREIBUNG);
    }

    @Test
    public void testForKonzepteSize() {
        List<KonzeptDto> result = KonzeptDto.forKonzepte(konzepte);
        assertEquals(1, result.size());
    }

    @Test
    public void testForKonzepteIdMapping() {
        List<KonzeptDto> result = KonzeptDto.forKonzepte(konzepte);
        KonzeptDto result0 = result.get(0);
        assertEquals(1L, (long) result0.getId());
    }

    @Test
    public void testForKonzepteBezeichnungMapping() {
        List<KonzeptDto> result = KonzeptDto.forKonzepte(konzepte);
        KonzeptDto result0 = result.get(0);
        assertEquals(BEZEICHNUNG, result0.getBezeichnung());
    }

    @Test
    public void testForKonzepteBeschreibungFullMapping() {
        List<KonzeptDto> result = KonzeptDto.forKonzepte(konzepte);
        KonzeptDto result0 = result.get(0);
        assertEquals(BESCHREIBUNG, result0.getBeschreibungFull());
    }

    @Test
    public void testForKonzepteBeschreibungShortMapping() {
        List<KonzeptDto> result = KonzeptDto.forKonzepte(konzepte);
        KonzeptDto result0 = result.get(0);
        assertEquals(BESCHREIBUNG, result0.getBeschreibungPreview());
    }

    @Test
    public void testForKonzepteBeschreibungShortLongerValueMapping() {
        when(konzept.getBeschreibung()).thenReturn(BESCHREIBUNG_LONG);
        List<KonzeptDto> result = KonzeptDto.forKonzepte(konzepte);
        KonzeptDto result0 = result.get(0);
        assertEquals(62, result0.getBeschreibungPreview().length());
    }

    @Test
    public void testForKonzepteBeschreibungFullLongerValueMapping() {
        when(konzept.getBeschreibung()).thenReturn(BESCHREIBUNG_LONG);
        List<KonzeptDto> result = KonzeptDto.forKonzepte(konzepte);
        KonzeptDto result0 = result.get(0);
        assertEquals(100, result0.getBeschreibungFull().length());
    }

}
