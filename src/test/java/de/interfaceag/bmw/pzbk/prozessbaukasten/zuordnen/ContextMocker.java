package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.primefaces.context.RequestContext;

import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.mock;

/**
 * @author fn
 */
public abstract class ContextMocker extends FacesContext {

    private ContextMocker() {
    }

    private static final Release RELEASE = new Release();

    private static class Release implements Answer<Void> {

        @Override
        public Void answer(InvocationOnMock invocation) throws Throwable {
            setCurrentInstance(null);
            return null;
        }
    }

    public static FacesContext mockFacesContextForComponentLookup() {
        FacesContext context = mock(FacesContext.class);
        setCurrentInstance(context);

        final UIViewRoot viewRoot = mock(UIViewRoot.class);

        Mockito.doReturn(viewRoot)
                .when(context)
                .getViewRoot();

        return context;
    }

    public static FacesContext mockFacesContextForUrlParamterUtils() {
        FacesContext context = mock(FacesContext.class);
        setCurrentInstance(context);
        final ExternalContext externalContext = mock(ExternalContext.class);
        final HttpServletRequest request = mock(HttpServletRequest.class);

        Mockito.doReturn(externalContext)
                .when(context)
                .getExternalContext();

        Mockito.doReturn(request)
                .when(externalContext)
                .getRequest();

        return context;
    }

    public static FacesContext mockFacesContext() {
        FacesContext context = mock(FacesContext.class);
        setCurrentInstance(context);
        return context;
    }

    public static RequestContext mockRequestContext() {
        RequestContext requestContext = mock(RequestContext.class);
        FacesContext facesContext = mock(FacesContext.class);
        RequestContext.setCurrentInstance(requestContext, facesContext);
        return requestContext;
    }


}
