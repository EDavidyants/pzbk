package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author sl
 */
public class ProzessbaukastenStatusGultigDisabledAttributesTest implements ProzessbaukastenDisabledAttributesTest {

    private final ProzessbaukastenDisabledAttributes disabledAttributes = new ProzessbaukastenStatusGultigDisabledAttributes(false);
    private final ProzessbaukastenDisabledAttributes disabledAttributesForAdmin = new ProzessbaukastenStatusGultigDisabledAttributes(true);

    @Test
    @Override
    public void testIsBezeichnungDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isBezeichnungDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsBeschreibungDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isBeschreibungDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsTteamDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isTteamDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsTteamDisabledForAdmin() {
        boolean expected = false;
        boolean result = disabledAttributesForAdmin.isTteamDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsLeadTechnologieDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isLeadTechnologieDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsKonzeptDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isKonzeptDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsStandardisierterFertigungsprozessDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isStandardisierterFertigungsprozessDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsErstanlaeuferDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isErstanlaeuferDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnhangStartbriefDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnhangStartbriefDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnhangGrafikUmfangDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnhangGrafikUmfangDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnhangGrafikKonzeptbaumDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnhangGrafikKonzeptbaumDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnhangBeauftragungTPKDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnhangBeauftragungTPKDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnhangGenehmigungTPKDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnhangGenehmigungTPKDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsWeitereAnhangeDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isWeitereAnhangeDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsStammdatenDetailsPanelDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isStammdatenDetailsPanelDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnforderungenTabDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnforderungenTabDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnhangeTabDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnhangeTabDisabled();
        assertEquals(expected, result);
    }

}
