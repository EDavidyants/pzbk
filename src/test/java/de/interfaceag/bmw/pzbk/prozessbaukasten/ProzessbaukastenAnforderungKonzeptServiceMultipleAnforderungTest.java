package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenAnforderungKonzept;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.ProzessbaukastenAnforderungEdit;
import de.interfaceag.bmw.pzbk.prozessbaukasten.history.ProzessbaukastenHistoryService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProzessbaukastenAnforderungKonzeptServiceMultipleAnforderungTest {

    @Mock
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;

    @Mock
    private Session session;

    @Mock
    private Prozessbaukasten prozessbaukasten;
    @Mock
    private Konzept konzept;

    @Mock
    private Anforderung anforderung1;
    @Mock
    private ProzessbaukastenAnforderungKonzept anforderung1Konzept;
    @Mock
    private ProzessbaukastenAnforderungEdit anforderung1Edit;

    @Mock
    private Anforderung anforderung2;
    @Mock
    private ProzessbaukastenAnforderungEdit anforderung2Edit;

    private final List<Anforderung> anforderungen = new ArrayList<>();

    private final List<Konzept> konzepte = new ArrayList<>();
    private final List<ProzessbaukastenAnforderungKonzept> prozessbaukastenAnforderungKonzepte = new ArrayList<>();
    private final List<ProzessbaukastenAnforderungEdit> anforderungDtos = new ArrayList<>();

    @InjectMocks
    private ProzessbaukastenAnforderungKonzeptService service;
    @Mock
    private ProzessbaukastenAnforderungKonzeptDao dao;

    @BeforeEach
    public void setUp() {
        konzepte.add(konzept);
        anforderungDtos.add(anforderung1Edit);
        anforderungDtos.add(anforderung2Edit);
        anforderungen.add(anforderung1);
        anforderungen.add(anforderung2);

        when(anforderung1.getId()).thenReturn(1L);
        when(anforderung1Edit.getId()).thenReturn("1");
        when(anforderung1Konzept.getAnforderung()).thenReturn(anforderung1);
        when(anforderung1Konzept.getKonzept()).thenReturn(konzept);
        prozessbaukastenAnforderungKonzepte.add(anforderung1Konzept);

        when(anforderung2.getId()).thenReturn(2L);
        when(anforderung2Edit.getId()).thenReturn("2");

        when(prozessbaukasten.getAnforderungen()).thenReturn(anforderungen);
        when(dao.getByProzessbaukastenId(anyLong())).thenReturn(prozessbaukastenAnforderungKonzepte);

        when(session.getUser()).thenReturn(TestDataFactory.generateMitarbeiter("Max", "Admin", "Abt.-2", "q2222222"));

    }

    @Test
    public void testAddNewKonzeptToAnforderung2() throws Exception {
        when(anforderung1Edit.getAnforderungKonzept()).thenReturn(konzepte);
        when(anforderung2Edit.getAnforderungKonzept()).thenReturn(konzepte);

        service.updateAnforderungKonzept(prozessbaukasten, anforderungDtos);
        verify(dao, times(1)).save(any());
        verify(dao, never()).remove(any());
    }

    @Test
    public void testRemoveKonzeptFromAnforderung1AndAddNewKonzeptToAnforderung2() throws Exception {
        when(anforderung1Edit.getAnforderungKonzept()).thenReturn(Collections.EMPTY_LIST);
        when(anforderung2Edit.getAnforderungKonzept()).thenReturn(konzepte);

        service.updateAnforderungKonzept(prozessbaukasten, anforderungDtos);
        verify(dao, times(1)).save(any());
        verify(dao, times(1)).remove(any());
    }

    @Test
    public void testRemoveKonzeptFromAnforderung1() throws Exception {
        when(anforderung1Edit.getAnforderungKonzept()).thenReturn(Collections.EMPTY_LIST);
        when(anforderung2Edit.getAnforderungKonzept()).thenReturn(Collections.EMPTY_LIST);

        service.updateAnforderungKonzept(prozessbaukasten, anforderungDtos);
        verify(dao, never()).save(any());
        verify(dao, times(1)).remove(any());
    }

}
