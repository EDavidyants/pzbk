package de.interfaceag.bmw.pzbk.prozessbaukasten;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 * @author evda
 */
public class KonzeptDtoCompareTest {

    @Test
    public void testCompareToByBezeichnungOne() {
        KonzeptDto theKonzept = new KonzeptDto(123L, "ORESUND", "Konzept one");
        KonzeptDto otherKonzept = new KonzeptDto(256L, "KLIPPAN", "Konzept two");
        int compareToFact = theKonzept.compareTo(otherKonzept);
        Assertions.assertTrue(compareToFact > 0);
    }

    @Test
    public void testCompareToByBezeichnungTwo() {
        KonzeptDto theKonzept = new KonzeptDto(123L, "ORESUND", "Konzept one");
        KonzeptDto otherKonzept = new KonzeptDto(256L, "KLIPPAN", "Konzept two");
        int compareToFact = otherKonzept.compareTo(theKonzept);
        Assertions.assertTrue(compareToFact < 0);
    }

    @Test
    public void testCompareToCase() {
        KonzeptDto theKonzept = new KonzeptDto(null, "klippan", "Konzept one");
        KonzeptDto otherKonzept = new KonzeptDto(256L, "KLIPPAN", "Konzept two");
        int compareToFact = theKonzept.compareTo(otherKonzept);
        assertThat(compareToFact, is(0));
    }


    @Test
    public void testCompareToByIdOne() {
        KonzeptDto theKonzept = new KonzeptDto(null, "KLIPPAN", "Konzept one");
        KonzeptDto otherKonzept = new KonzeptDto(256L, "KLIPPAN", "Konzept two");
        int compareToFact = theKonzept.compareTo(otherKonzept);
        assertThat(compareToFact, is(0));
    }

    @Test
    public void testCompareToByIdTwo() {
        KonzeptDto theKonzept = new KonzeptDto(123L, "KLIPPAN", "Konzept one");
        KonzeptDto otherKonzept = new KonzeptDto(null, "KLIPPAN", "Konzept two");
        int compareToFact = otherKonzept.compareTo(theKonzept);
        assertThat(compareToFact, is(0));
    }

    @Test
    public void testCompareToByIdThree() {
        KonzeptDto theKonzept = new KonzeptDto(null, "KLIPPAN", "Konzept one");
        KonzeptDto otherKonzept = new KonzeptDto(null, "KLIPPAN", "Konzept two");
        int compareToFact = otherKonzept.compareTo(theKonzept);
        assertThat(compareToFact, is(0));
    }

    @Test
    public void testCompareToByIdFour() {
        KonzeptDto theKonzept = new KonzeptDto(123L, "KLIPPAN", "Konzept one");
        KonzeptDto otherKonzept = new KonzeptDto(256L, "KLIPPAN", "Konzept two");
        int compareToFact = otherKonzept.compareTo(theKonzept);
        assertThat(compareToFact, is(0));
    }

    @Test
    public void testCompareToByIdFive() {
        KonzeptDto theKonzept = new KonzeptDto(123L, "KLIPPAN", "Konzept one");
        KonzeptDto otherKonzept = new KonzeptDto(256L, "KLIPPAN", "Konzept two");
        int compareToFact = theKonzept.compareTo(otherKonzept);
        assertThat(compareToFact, is(0));
    }

}
