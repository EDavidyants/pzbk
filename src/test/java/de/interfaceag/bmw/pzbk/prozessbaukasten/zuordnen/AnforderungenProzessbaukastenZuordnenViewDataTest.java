package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import de.interfaceag.bmw.pzbk.globalsearch.AutocompleteSearchResult;
import de.interfaceag.bmw.pzbk.globalsearch.SearchResultDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.primefaces.event.SelectEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 *
 * @author fn
 */
@RunWith(MockitoJUnitRunner.class)
public class AnforderungenProzessbaukastenZuordnenViewDataTest {

    private final AnforderungenProzessbaukastenZuordnenViewData viewData = new AnforderungenProzessbaukastenZuordnenViewData();

    private AnforderungenProzessbaukastenZuordnenDTO prozessbaukastenZuordnenDTO = new AnforderungenProzessbaukastenZuordnenDTO("A42", 1, "");

    @Mock
    private SelectEvent event;

    private final AutocompleteSearchResult autocompleteSearchResult = new SearchResultDTO(1L, "", "", "", "", "", "", "1");

    @Before
    public void setUp() {
        when(event.getObject()).thenReturn(autocompleteSearchResult);

        List<AnforderungenProzessbaukastenZuordnenDTO> list = new ArrayList<>(Arrays.asList(prozessbaukastenZuordnenDTO));
        viewData.setZugeordneteAnforderungen(list);

    }

    @Test
    public void testResetZugeordneteAnforderungen() {
        Assertions.assertEquals(1, viewData.getZugeordneteAnforderungen().size());
        viewData.resetZugeordneteAnforderungen();
        Assertions.assertEquals(0, viewData.getZugeordneteAnforderungen().size());
    }

    @Test
    public void testAddAnforderung() {
        Assertions.assertEquals(1, viewData.getZugeordneteAnforderungen().size());
        viewData.addAnforderung(event);
        Assertions.assertEquals(2, viewData.getZugeordneteAnforderungen().size());
    }

    @Test
    public void testRemoveAnforderung() {
        Assertions.assertEquals(1, viewData.getZugeordneteAnforderungen().size());
        viewData.removeAnforderung(prozessbaukastenZuordnenDTO);
        Assertions.assertEquals(0, viewData.getZugeordneteAnforderungen().size());

    }

    @Test
    public void testViewDataForProzessbaukasten() {
        AnforderungenProzessbaukastenZuordnenViewData testViewData = AnforderungenProzessbaukastenZuordnenViewData.forProzessbaukasten();
        Assertions.assertEquals(0, testViewData.getZugeordneteAnforderungen().size());
    }

}
