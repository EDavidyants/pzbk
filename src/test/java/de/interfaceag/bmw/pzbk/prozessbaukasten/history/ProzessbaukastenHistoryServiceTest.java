package de.interfaceag.bmw.pzbk.prozessbaukasten.history;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenHistory;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.konzepte.ProzessbaukastenKonzeptHistoryService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProzessbaukastenHistoryServiceTest {

    private static final String USER = "USER";
    private static final String KOMMENTAR = "kommentar";
    private static final String BEZEICHNUNG = "bezeichnung";
    @Mock
    private ProzessbaukastenHistoryDao prozessbaukastenHistoryDao;
    @Mock
    private ProzessbaukastenReadService prozessbaukastenReadService;
    @Mock
    private AnforderungMeldungHistoryService anforderungHistoryService;
    @Mock
    private ProzessbaukastenKonzeptHistoryService prozessbaukastenKonzeptHistoryService;
    @InjectMocks
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;

    @Mock
    private Mitarbeiter user;
    private Prozessbaukasten prozessbaukasten;

    @BeforeEach
    void setUp() {
        when(user.toString()).thenReturn(USER);
        prozessbaukasten = TestDataFactory.generateProzessbaukasten();
    }

    @Test
    void writeHistoryForNewProzessbaukastenBentzer() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeHistoryForNewProzessbaukasten(user, prozessbaukasten);
        assertThat(prozessbaukastenHistory.getBenutzer(), is(USER));
    }

    @Test
    void writeHistoryForNewProzessbaukastenProzessbaukasten() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeHistoryForNewProzessbaukasten(user, prozessbaukasten);
        assertThat(prozessbaukastenHistory.getProzessbaukasten(), is(prozessbaukasten));
    }

    @Test
    void writeHistoryForNewProzessbaukastenDatum() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeHistoryForNewProzessbaukasten(user, prozessbaukasten);
        assertThat(prozessbaukastenHistory.getDatum(), notNullValue());
    }

    @Test
    void writeHistoryForNewProzessbaukastenVon() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeHistoryForNewProzessbaukasten(user, prozessbaukasten);
        assertThat(prozessbaukastenHistory.getVon(), is(""));
    }

    @Test
    void writeHistoryForNewProzessbaukastenAuf() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeHistoryForNewProzessbaukasten(user, prozessbaukasten);
        assertThat(prozessbaukastenHistory.getAuf(), is("neu angelegt"));
    }

    @Test
    void writeHistoryForNewProzessbaukastenChangedField() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeHistoryForNewProzessbaukasten(user, prozessbaukasten);
        assertThat(prozessbaukastenHistory.getChangedField(), is("Prozessbaukasten"));
    }

    @Test
    void writeHistoryForProzessbaukastenNewVersionBenutzer() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeHistoryForProzessbaukastenNewVersion(user, prozessbaukasten, 1, KOMMENTAR);
        assertThat(prozessbaukastenHistory.getBenutzer(), is(USER));
    }

    @Test
    void writeHistoryForProzessbaukastenNewVersionProzessbaukasten() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeHistoryForProzessbaukastenNewVersion(user, prozessbaukasten, 1, KOMMENTAR);
        assertThat(prozessbaukastenHistory.getProzessbaukasten(), is(prozessbaukasten));
    }

    @Test
    void writeHistoryForProzessbaukastenNewVersionDatum() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeHistoryForProzessbaukastenNewVersion(user, prozessbaukasten, 1, KOMMENTAR);
        assertThat(prozessbaukastenHistory.getDatum(), notNullValue());
    }

    @Test
    void writeHistoryForProzessbaukastenNewVersionVon() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeHistoryForProzessbaukastenNewVersion(user, prozessbaukasten, 1, KOMMENTAR);
        assertThat(prozessbaukastenHistory.getVon(), is(""));
    }

    @Test
    void writeHistoryForProzessbaukastenNewVersionAuf() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeHistoryForProzessbaukastenNewVersion(user, prozessbaukasten, 1, KOMMENTAR);
        assertThat(prozessbaukastenHistory.getAuf(), is(KOMMENTAR));
    }

    @Test
    void writeHistoryForProzessbaukastenNewVersionChangedField() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeHistoryForProzessbaukastenNewVersion(user, prozessbaukasten, 1, KOMMENTAR);
        assertThat(prozessbaukastenHistory.getChangedField(), is("Kommentar zur neuen Version"));
    }

    @Test
    void writeKommentarForChangedFieldBenutzer() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeKommentarForChangedField(prozessbaukasten, user, BEZEICHNUNG, KOMMENTAR);
        assertThat(prozessbaukastenHistory.getBenutzer(), is(USER));
    }

    @Test
    void writeKommentarForChangedFieldProzessbaukasten() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeKommentarForChangedField(prozessbaukasten, user, BEZEICHNUNG, KOMMENTAR);
        assertThat(prozessbaukastenHistory.getProzessbaukasten(), is(prozessbaukasten));
    }

    @Test
    void writeKommentarForChangedFieldDatum() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeKommentarForChangedField(prozessbaukasten, user, BEZEICHNUNG, KOMMENTAR);
        assertThat(prozessbaukastenHistory.getDatum(), notNullValue());
    }

    @Test
    void writeKommentarForChangedFieldVon() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeKommentarForChangedField(prozessbaukasten, user, BEZEICHNUNG, KOMMENTAR);
        assertThat(prozessbaukastenHistory.getVon(), is(""));
    }

    @Test
    void writeKommentarForChangedFieldAuf() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeKommentarForChangedField(prozessbaukasten, user, BEZEICHNUNG, KOMMENTAR);
        assertThat(prozessbaukastenHistory.getBenutzer(), is(USER));
        assertThat(prozessbaukastenHistory.getAuf(), is(KOMMENTAR));
    }

    @Test
    void writeKommentarForChangedFieldChangedField() {
        final ProzessbaukastenHistory prozessbaukastenHistory = prozessbaukastenHistoryService.writeKommentarForChangedField(prozessbaukasten, user, BEZEICHNUNG, KOMMENTAR);
        assertThat(prozessbaukastenHistory.getChangedField(), is(BEZEICHNUNG));
    }
}