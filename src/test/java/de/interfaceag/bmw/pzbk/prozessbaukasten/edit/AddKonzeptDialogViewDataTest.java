package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required.ProzessbaukastenRequiredAttributes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class AddKonzeptDialogViewDataTest {

    @Mock
    private Konzept existingKonzept;
    @Mock
    private Konzept newKonzept;
    @Mock
    private ProzessbaukastenRequiredAttributes requiredAttributes;

    private List<Konzept> konzepte;

    private AddKonzeptDialog addKonzeptDialog;

    @BeforeEach
    public void setup() {
        this.konzepte = new ArrayList<>();
        this.konzepte.add(existingKonzept);
        this.addKonzeptDialog = new AddKonzeptDialogViewData(requiredAttributes, konzepte);
    }

    @Test
    public void testGetKonzept() {
        Konzept konzept = addKonzeptDialog.getKonzept();
        assertEquals(new Konzept(), konzept);
    }

    @Test
    public void testSetKonzept() {
        addKonzeptDialog.setKonzept(newKonzept);
        Konzept konzept = addKonzeptDialog.getKonzept();
        assertEquals(newKonzept, konzept);
    }

    @Test
    public void testAddKonzeptSizeBeforeAdd() {
        assertEquals(1, konzepte.size());
    }

    @Test
    public void testAddKonzeptSizeAfterAdd() {
        addKonzeptDialog.setKonzept(newKonzept);
        addKonzeptDialog.addKonzept();
        assertEquals(2, konzepte.size());
    }

    @Test
    public void testAddKonzeptPositionBeforeAdd() {
        assertEquals(existingKonzept, konzepte.get(0));
    }

    @Test
    public void testAddKonzeptPositionAfterAdd() {
        addKonzeptDialog.setKonzept(newKonzept);
        addKonzeptDialog.addKonzept();
        assertEquals(newKonzept, konzepte.get(0));
    }

    @Test
    public void testGetRequiredAttributes() {
        ProzessbaukastenRequiredAttributes result = addKonzeptDialog.getRequiredAttributes();
        assertEquals(requiredAttributes, result);
    }

}
