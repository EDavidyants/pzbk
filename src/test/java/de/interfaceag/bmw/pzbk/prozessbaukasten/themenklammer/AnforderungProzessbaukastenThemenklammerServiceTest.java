package de.interfaceag.bmw.pzbk.prozessbaukasten.themenklammer;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenThemenklammer;
import de.interfaceag.bmw.pzbk.entities.Themenklammer;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.ProzessbaukastenAnforderungEdit;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.shared.objectIds.ThemenklammerId;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AnforderungProzessbaukastenThemenklammerServiceTest {

    private static final ThemenklammerId themenklammerId = new ThemenklammerId(42L);

    @Mock
    private ThemenklammerService themenklammerService;
    @Mock
    private AnforderungProzessbaukastenThemenklammerHistoryService anforderungProzessbaukastenThemenklammerHistoryService;
    @InjectMocks
    private AnforderungProzessbaukastenThemenklammerService anforderungProzessbaukastenThemenklammerService;

    @Mock
    private Themenklammer themenklammer;
    @Mock
    private ThemenklammerDto themenklammerDto;
    @Mock
    private ProzessbaukastenAnforderungEdit prozessbaukastenAnforderungEdit;
    private List<ProzessbaukastenAnforderungEdit> prozessbaukastenAnforderungDtos;

    private Anforderung anforderung;
    private Prozessbaukasten prozessbaukasten;


    @BeforeEach
    void setUp() {
        anforderung = TestDataFactory.generateAnforderung();

        prozessbaukasten = new Prozessbaukasten();
        prozessbaukasten.addAnforderung(anforderung);

        prozessbaukastenAnforderungDtos = Collections.singletonList(prozessbaukastenAnforderungEdit);

        when(prozessbaukastenAnforderungEdit.getId()).thenReturn("2");
    }

    @Test
    void updateAnforderungThemenklammernAnforderungNotFoundInProzessbaukasten() {
        when(prozessbaukastenAnforderungEdit.getId()).thenReturn("3");

        anforderung.addProzessbaukastenThemenklammer(new ProzessbaukastenThemenklammer(prozessbaukasten, themenklammer));

        anforderungProzessbaukastenThemenklammerService.updateAnforderungThemenklammern(prozessbaukasten, prozessbaukastenAnforderungDtos);
        final List<ProzessbaukastenThemenklammer> prozessbaukastenThemenklammern = anforderung.getProzessbaukastenThemenklammern();
        assertThat(prozessbaukastenThemenklammern, hasSize(1));
    }

    @Test
    void updateAnforderungThemenklammernRemoveThemenklammer() {
        anforderung.addProzessbaukastenThemenklammer(new ProzessbaukastenThemenklammer(prozessbaukasten, themenklammer));
        when(prozessbaukastenAnforderungEdit.getThemenklammern()).thenReturn(Collections.emptyList());

        anforderungProzessbaukastenThemenklammerService.updateAnforderungThemenklammern(prozessbaukasten, prozessbaukastenAnforderungDtos);
        final List<ProzessbaukastenThemenklammer> prozessbaukastenThemenklammern = anforderung.getProzessbaukastenThemenklammern();
        assertThat(prozessbaukastenThemenklammern, empty());
    }

    @Test
    void updateAnforderungThemenklammernRemoveThemenklammerWriteHistoryForRemoveThemenklammer() {
        anforderung.addProzessbaukastenThemenklammer(new ProzessbaukastenThemenklammer(prozessbaukasten, themenklammer));
        when(prozessbaukastenAnforderungEdit.getThemenklammern()).thenReturn(Collections.emptyList());

        anforderungProzessbaukastenThemenklammerService.updateAnforderungThemenklammern(prozessbaukasten, prozessbaukastenAnforderungDtos);
        anforderung.getProzessbaukastenThemenklammern();
        verify(anforderungProzessbaukastenThemenklammerHistoryService, times(1)).writeHistoryEntryForRemovedThemenklammer(any(), any());
    }

    @Test
    void updateAnforderungThemenklammernAddThemenklammer() {
        when(themenklammerService.find(themenklammerId)).thenReturn(Optional.of(themenklammer));
        when(themenklammerDto.getThemenklammerId()).thenReturn(themenklammerId);
        when(prozessbaukastenAnforderungEdit.getThemenklammern()).thenReturn(Collections.singletonList(themenklammerDto));

        anforderungProzessbaukastenThemenklammerService.updateAnforderungThemenklammern(prozessbaukasten, prozessbaukastenAnforderungDtos);
        final List<ProzessbaukastenThemenklammer> prozessbaukastenThemenklammern = anforderung.getProzessbaukastenThemenklammern();
        assertThat(prozessbaukastenThemenklammern, hasSize(1));
    }

    @Test
    void updateAnforderungThemenklammernAddThemenklammerWriteHistoryForAddThemenklammer() {
        when(themenklammerService.find(themenklammerId)).thenReturn(Optional.of(themenklammer));
        when(themenklammerDto.getThemenklammerId()).thenReturn(themenklammerId);
        when(prozessbaukastenAnforderungEdit.getThemenklammern()).thenReturn(Collections.singletonList(themenklammerDto));

        anforderungProzessbaukastenThemenklammerService.updateAnforderungThemenklammern(prozessbaukasten, prozessbaukastenAnforderungDtos);
        anforderung.getProzessbaukastenThemenklammern();
        verify(anforderungProzessbaukastenThemenklammerHistoryService, times(1)).writeHistoryEntryForAddedThemenklammer(any(), any());
    }

    @Test
    void updateAnforderungThemenklammernAddThemenklammerNotFound() {
        when(themenklammerService.find(themenklammerId)).thenReturn(Optional.empty());
        when(themenklammerDto.getThemenklammerId()).thenReturn(themenklammerId);
        when(prozessbaukastenAnforderungEdit.getThemenklammern()).thenReturn(Collections.singletonList(themenklammerDto));

        anforderungProzessbaukastenThemenklammerService.updateAnforderungThemenklammern(prozessbaukasten, prozessbaukastenAnforderungDtos);
        final List<ProzessbaukastenThemenklammer> prozessbaukastenThemenklammern = anforderung.getProzessbaukastenThemenklammern();
        assertThat(prozessbaukastenThemenklammern, empty());
    }
}