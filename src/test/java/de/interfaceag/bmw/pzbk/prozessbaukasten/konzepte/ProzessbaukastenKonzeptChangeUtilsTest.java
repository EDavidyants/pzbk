package de.interfaceag.bmw.pzbk.prozessbaukasten.konzepte;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.prozessbaukasten.dto.ProzessbaukastenKonzeptChangeDto;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.collection.IsIterableWithSize.iterableWithSize;
import static org.hamcrest.core.IsIterableContaining.hasItems;

class ProzessbaukastenKonzeptChangeUtilsTest {

    private Konzept konzeptNew;
    private Konzept konzeptOld;

    @BeforeEach
    void setUp() {
        konzeptNew = TestDataFactory.generateKonzept("Old Konzept Bezeichnung", "Old Konzept Beschreibung");
        konzeptOld = TestDataFactory.generateKonzept("Old Konzept Bezeichnung", "Old Konzept Beschreibung");
    }

    @Test
    void getChangedKonzeptAttributeBezeichnungSizeTest() {

        konzeptNew.setBezeichnung("New Konzept Bezeichnung");
        List<ProzessbaukastenKonzeptChangeDto> prozessbaukastenKonzeptChangeDto = ProzessbaukastenKonzeptChangeUtils.getChangedKonzeptAttribute(konzeptNew, konzeptOld);
        assertThat(prozessbaukastenKonzeptChangeDto, iterableWithSize(1));
    }

    @Test
    void getChangedKonzeptAttributeBezeichnungTest() {
        ProzessbaukastenKonzeptChangeDto expectedResult = new ProzessbaukastenKonzeptChangeDto("Old Konzept Bezeichnung", "Bezeichnung", "Old Konzept Bezeichnung", "New Konzept Bezeichnung");
        konzeptNew.setBezeichnung("New Konzept Bezeichnung");
        List<ProzessbaukastenKonzeptChangeDto> prozessbaukastenKonzeptChangeDto = ProzessbaukastenKonzeptChangeUtils.getChangedKonzeptAttribute(konzeptNew, konzeptOld);
        assertThat(prozessbaukastenKonzeptChangeDto, hasItems(expectedResult));
    }

    @Test
    void getChangedKonzeptAttributeBeschreibungTest() {
        ProzessbaukastenKonzeptChangeDto expectedResult = new ProzessbaukastenKonzeptChangeDto("Old Konzept Bezeichnung", "Beschreibung", "Old Konzept Beschreibung", "New Konzept Beschreibung");
        konzeptNew.setBeschreibung("New Konzept Beschreibung");
        List<ProzessbaukastenKonzeptChangeDto> prozessbaukastenKonzeptChangeDto = ProzessbaukastenKonzeptChangeUtils.getChangedKonzeptAttribute(konzeptNew, konzeptOld);
        assertThat(prozessbaukastenKonzeptChangeDto, hasItems(expectedResult));
    }

    @Test
    void getChangedKonzeptAttributeBezeichnungAndBeschreibungTest() {
        ProzessbaukastenKonzeptChangeDto expectedResult = new ProzessbaukastenKonzeptChangeDto("Old Konzept Bezeichnung", "Beschreibung", "Old Konzept Beschreibung", "New Konzept Beschreibung");
        ProzessbaukastenKonzeptChangeDto expectedResult2 = new ProzessbaukastenKonzeptChangeDto("Old Konzept Bezeichnung", "Bezeichnung", "Old Konzept Bezeichnung", "New Konzept Bezeichnung");
        konzeptNew.setBezeichnung("New Konzept Bezeichnung");
        konzeptNew.setBeschreibung("New Konzept Beschreibung");

        List<ProzessbaukastenKonzeptChangeDto> prozessbaukastenKonzeptChangeDto = ProzessbaukastenKonzeptChangeUtils.getChangedKonzeptAttribute(konzeptNew, konzeptOld);
        assertThat(prozessbaukastenKonzeptChangeDto, hasItems(expectedResult, expectedResult2));
    }

    @Test
    void getChangedKonzeptAttributeAnhangHinzufuegenTest() {
        Anhang anhang = new Anhang();
        anhang.setDateiname("Anhang");
        ProzessbaukastenKonzeptChangeDto expectedResult = new ProzessbaukastenKonzeptChangeDto("Old Konzept Bezeichnung", "Anhang", "kein", "Anhang");
        konzeptNew.setAnhang(anhang);
        List<ProzessbaukastenKonzeptChangeDto> prozessbaukastenKonzeptChangeDto = ProzessbaukastenKonzeptChangeUtils.getChangedKonzeptAttribute(konzeptNew, konzeptOld);
        assertThat(prozessbaukastenKonzeptChangeDto, hasItems(expectedResult));
    }

    @Test
    void getChangedKonzeptAttributeAnhangEntfernenTest() {
        Anhang anhang = new Anhang();
        anhang.setDateiname("Anhang");
        ProzessbaukastenKonzeptChangeDto expectedResult = new ProzessbaukastenKonzeptChangeDto("Old Konzept Bezeichnung", "Anhang", "Anhang", "kein");
        konzeptOld.setAnhang(anhang);
        List<ProzessbaukastenKonzeptChangeDto> prozessbaukastenKonzeptChangeDto = ProzessbaukastenKonzeptChangeUtils.getChangedKonzeptAttribute(konzeptNew, konzeptOld);
        assertThat(prozessbaukastenKonzeptChangeDto, hasItems(expectedResult));
    }

    @Test
    void getChangedKonzeptAttributeAnhangChangeTest() {
        Anhang anhang = new Anhang();
        anhang.setDateiname("Anhang");

        Anhang anhang2 = new Anhang();
        anhang2.setDateiname("Anhang2");

        ProzessbaukastenKonzeptChangeDto expectedResult = new ProzessbaukastenKonzeptChangeDto("Old Konzept Bezeichnung", "Anhang", "Anhang", "Anhang2");
        konzeptOld.setAnhang(anhang);
        konzeptNew.setAnhang(anhang2);
        List<ProzessbaukastenKonzeptChangeDto> prozessbaukastenKonzeptChangeDto = ProzessbaukastenKonzeptChangeUtils.getChangedKonzeptAttribute(konzeptNew, konzeptOld);
        assertThat(prozessbaukastenKonzeptChangeDto, hasItems(expectedResult));
    }

    @Test
    void getChangedKonzeptAttributeBezeichnungAndBeschreibungAndAnhangTest() {
        ProzessbaukastenKonzeptChangeDto expectedResult = new ProzessbaukastenKonzeptChangeDto("Old Konzept Bezeichnung", "Beschreibung", "Old Konzept Beschreibung", "New Konzept Beschreibung");
        ProzessbaukastenKonzeptChangeDto expectedResult2 = new ProzessbaukastenKonzeptChangeDto("Old Konzept Bezeichnung", "Bezeichnung", "Old Konzept Bezeichnung", "New Konzept Bezeichnung");
        ProzessbaukastenKonzeptChangeDto expectedResult3 = new ProzessbaukastenKonzeptChangeDto("Old Konzept Bezeichnung", "Anhang", "kein", "Anhang");

        Anhang anhang = new Anhang();
        anhang.setDateiname("Anhang");

        konzeptNew.setAnhang(anhang);

        konzeptNew.setBezeichnung("New Konzept Bezeichnung");
        konzeptNew.setBeschreibung("New Konzept Beschreibung");

        List<ProzessbaukastenKonzeptChangeDto> prozessbaukastenKonzeptChangeDto = ProzessbaukastenKonzeptChangeUtils.getChangedKonzeptAttribute(konzeptNew, konzeptOld);
        assertThat(prozessbaukastenKonzeptChangeDto, hasItems(expectedResult, expectedResult2, expectedResult3));
    }

    @Test
    void getChangedKonzeptAttributeNoChangeTest() {

        List<ProzessbaukastenKonzeptChangeDto> prozessbaukastenKonzeptChangeDto = ProzessbaukastenKonzeptChangeUtils.getChangedKonzeptAttribute(konzeptNew, konzeptOld);
        assertThat(prozessbaukastenKonzeptChangeDto, is(empty()));
    }
}
