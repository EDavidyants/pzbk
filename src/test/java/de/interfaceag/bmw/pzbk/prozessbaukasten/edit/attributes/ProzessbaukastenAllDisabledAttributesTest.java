package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author sl
 */
public class ProzessbaukastenAllDisabledAttributesTest {

    private final ProzessbaukastenDisabledAttributes disabledAttributes = new ProzessbaukastenAllDisabledAttributes();

    @Test
    public void testIsBezeichnungDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isBezeichnungDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsBeschreibungDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isBeschreibungDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsTteamDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isTteamDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsLeadTechnologieDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isLeadTechnologieDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsKonzeptDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isKonzeptDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsStandardisierterFertigungsprozessDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isStandardisierterFertigungsprozessDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsErstanlaeuferDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isErstanlaeuferDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsAnhangStartbriefDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnhangStartbriefDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsAnhangGrafikUmfangDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnhangGrafikUmfangDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsAnhangGrafikKonzeptbaumDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnhangGrafikKonzeptbaumDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsAnhangBeauftragungTPKDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnhangBeauftragungTPKDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsAnhangGenehmigungTPKDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnhangGenehmigungTPKDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsWeitereAnhangeDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isWeitereAnhangeDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsStammdatenDetailsPanelDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isStammdatenDetailsPanelDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsAnforderungenTabDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnforderungenTabDisabled();
        assertEquals(expected, result);
    }

    @Test
    public void testIsAnhangeTabDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnhangeTabDisabled();
        assertEquals(expected, result);
    }

}
