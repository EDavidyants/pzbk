package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.List;

/**
 *
 * @author evda
 */
public class ProzessbaukastenStatusUebergangTest {

    @Test
    public void testStatusUebergaengeFromAngelegt() {
        List<ProzessbaukastenStatus> statusUebergaenge = ProzessbaukastenStatusUebergang.getNextStatus(ProzessbaukastenStatus.ANGELEGT);
        Assertions.assertTrue(statusUebergaenge.size() == 2);
        Assertions.assertTrue(statusUebergaenge.contains(ProzessbaukastenStatus.BEAUFTRAGT));
        Assertions.assertTrue(statusUebergaenge.contains(ProzessbaukastenStatus.ABGEBROCHEN));
    }

    @Test
    public void testStatusUebergaengeFromBeaftragt() {
        List<ProzessbaukastenStatus> statusUebergaenge = ProzessbaukastenStatusUebergang.getNextStatus(ProzessbaukastenStatus.BEAUFTRAGT);
        Assertions.assertTrue(statusUebergaenge.size() == 2);
        Assertions.assertTrue(statusUebergaenge.contains(ProzessbaukastenStatus.ABGEBROCHEN));
        Assertions.assertTrue(statusUebergaenge.contains(ProzessbaukastenStatus.GUELTIG));
    }

    @Test
    public void testStatusUebergaengeFromAbgebrochen() {
        List<ProzessbaukastenStatus> statusUebergaenge = ProzessbaukastenStatusUebergang.getNextStatus(ProzessbaukastenStatus.ABGEBROCHEN);
        Assertions.assertTrue(statusUebergaenge.size() == 1);
        Assertions.assertTrue(statusUebergaenge.contains(ProzessbaukastenStatus.GELOESCHT));
    }

    @Test
    public void testStatusUebergaengeFromGueltig() {
        List<ProzessbaukastenStatus> statusUebergaenge = ProzessbaukastenStatusUebergang.getNextStatus(ProzessbaukastenStatus.GUELTIG);
        Assertions.assertTrue(statusUebergaenge.size() == 1);
        Assertions.assertTrue(statusUebergaenge.contains(ProzessbaukastenStatus.STILLGELEGT));
    }

}
