package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenAnforderungKonzept;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.ProzessbaukastenAnforderungEdit;
import de.interfaceag.bmw.pzbk.prozessbaukasten.history.ProzessbaukastenHistoryService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ProzessbaukastenAnforderungKonzeptWriteHistoryTest {

    @Mock
    private ProzessbaukastenAnforderungKonzeptDao dao;

    @Mock
    private ProzessbaukastenHistoryService historyService;

    @Mock
    private Session session;

    @Mock
    private Anforderung anforderung;

    @Mock
    private Konzept konzept;

    @Mock
    private Prozessbaukasten prozessbaukasten;

    @Mock
    private ProzessbaukastenAnforderungEdit prozessbaukastenAnforderungEdit;

    @InjectMocks
    private ProzessbaukastenAnforderungKonzeptService service;

    private ProzessbaukastenAnforderungKonzept prozessbaukastenAnforderungKonzept;
    private Mitarbeiter user;

    @BeforeEach
    public void setUp() {
        prozessbaukastenAnforderungKonzept = new ProzessbaukastenAnforderungKonzept(anforderung, konzept, prozessbaukasten);
        user = TestDataFactory.generateMitarbeiter("Max", "Admin", "Abt.-2", "q2222222");
        when(session.getUser()).thenReturn(user);
    }

    @Test
    public void testSaveProzessbaukastenAnforderungKonzept() {
        service.createAnforderungKonzept(anforderung, prozessbaukasten, konzept);
        verify(historyService).writeHistoryForAddedAnforderungKonzept(prozessbaukastenAnforderungKonzept, user);
    }

    @Test
    public void testUpdateAnforderungKonzeptWriteHistoryForAddedAnforderungKonzept() {
        prozessbaukastenAnforderungKonzept.setId(null);
        when(prozessbaukastenAnforderungEdit.getId()).thenReturn("123");
        service.updateAnforderungKonzept(prozessbaukasten, Collections.singletonList(prozessbaukastenAnforderungEdit), Collections.singletonList(prozessbaukastenAnforderungKonzept));
        verify(historyService).writeHistoryForAddedAnforderungKonzept(prozessbaukastenAnforderungKonzept, user);
    }

    @Test
    public void testUpdateAnforderungKonzeptWriteHistoryForRemovedAnforderungKonzept() {
        prozessbaukastenAnforderungKonzept.setId(234L);
        when(prozessbaukasten.getAnforderungen()).thenReturn(Collections.singletonList(anforderung));
        when(prozessbaukastenAnforderungEdit.getAnforderungKonzept()).thenReturn(Collections.emptyList());
        when(prozessbaukastenAnforderungEdit.getId()).thenReturn("123");
        when(anforderung.getId()).thenReturn(123L);
        service.updateAnforderungKonzept(prozessbaukasten, Collections.singletonList(prozessbaukastenAnforderungEdit), Collections.singletonList(prozessbaukastenAnforderungKonzept));
        verify(historyService).writeHistoryForRemovedAnforderungKonzept(prozessbaukastenAnforderungKonzept, user);
    }

}
