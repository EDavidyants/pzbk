package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

/**
 *
 * @author sl
 */
public class ProzessbaukastenNoRequiredAttributesTest {

    private final ProzessbaukastenRequiredAttributes requiredAttributes = new ProzessbaukastenNoRequiredAttributes();

    @Test
    public void testIsBezeichnungRequired() {
        boolean result = requiredAttributes.isBezeichnungRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsBeschreibungRequired() {
        boolean result = requiredAttributes.isBeschreibungRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsTteamRequired() {
        boolean result = requiredAttributes.isTteamRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsLeadTechnologieRequired() {
        boolean result = requiredAttributes.isLeadTechnologieRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsKonzeptRequired() {
        boolean result = requiredAttributes.isKonzeptRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsStandardisierterFertigungsprozessRequired() {
        boolean result = requiredAttributes.isStandardisierterFertigungsprozessRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsErstanlaeuferRequired() {
        boolean result = requiredAttributes.isErstanlaeuferRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsAnhangStartbriefRequired() {
        boolean result = requiredAttributes.isAnhangStartbriefRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsAnhangGrafikUmfangRequired() {
        boolean result = requiredAttributes.isAnhangGrafikUmfangRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsAnhangGrafikKonzeptbaumRequired() {
        boolean result = requiredAttributes.isAnhangGrafikKonzeptbaumRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsAnhangBeauftragungTPKRequired() {
        boolean result = requiredAttributes.isAnhangBeauftragungTPKRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsAnhangGenehmigungTPKRequired() {
        boolean result = requiredAttributes.isAnhangGenehmigungTPKRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsWeitereAnhangeRequired() {
        boolean result = requiredAttributes.isWeitereAnhangeRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsKonzeptBezeichnungRequired() {
        boolean result = requiredAttributes.isKonzeptBezeichnungRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsKonzeptBeschreibungRequired() {
        boolean result = requiredAttributes.isKonzeptBeschreibungRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIsKonzeptAnhangRequired() {
        boolean result = requiredAttributes.isKonzeptAnhangRequired();
        boolean expected = false;
        Assertions.assertEquals(expected, result);
    }

}
