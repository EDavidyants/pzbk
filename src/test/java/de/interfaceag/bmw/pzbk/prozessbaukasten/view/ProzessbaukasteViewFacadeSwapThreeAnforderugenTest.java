package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenService;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.primefaces.event.ReorderEvent;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProzessbaukasteViewFacadeSwapThreeAnforderugenTest {

    @Mock
    ProzessbaukastenService prozessbaukastenService;
    @InjectMocks
    ProzessbaukastenViewFacade facade;

    @Mock
    Prozessbaukasten prozessbaukasten;
    @Mock
    ReorderEvent event;
    @Mock
    Anforderung anforderung1;
    @Mock
    Anforderung anforderung2;
    @Mock
    Anforderung anforderung3;

    List<Anforderung> anforderungen;

    @BeforeEach
    public void setUp() {
        anforderungen = new ArrayList<>();
        anforderungen.add(anforderung1);
        anforderungen.add(anforderung2);
        anforderungen.add(anforderung3);
    }

    private void initReorderEvent() {
        when(prozessbaukasten.getAnforderungen()).thenReturn(anforderungen);
        when(event.getFromIndex()).thenReturn(2);
        when(event.getToIndex()).thenReturn(0);
    }

    @Test
    public void testReorderAnforderungenBeforeOrder() {
        MatcherAssert.assertThat(anforderungen, contains(anforderung1, anforderung2, anforderung3));
    }

    @Test
    public void testReorderAnforderungenAfterOrder() {
        initReorderEvent();
        facade.reorderAnforderungen(prozessbaukasten, event);
        MatcherAssert.assertThat(anforderungen, contains(anforderung3, anforderung1, anforderung2));
    }

    @Test
    public void testReorderAnforderungenCallSave() {
        initReorderEvent();
        facade.reorderAnforderungen(prozessbaukasten, event);
        verify(prozessbaukastenService, times(1)).save(prozessbaukasten);
    }

}
