package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenAnforderungKonzeptService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenDao;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 *
 * @author fn
 */
@ExtendWith(MockitoExtension.class)
public class ProzessbaukastenEditViewFacadeTest {

    @Mock
    Session session;
    @Mock
    TteamService tteamService;
    @Mock
    ProzessbaukastenDao prozessbaukastenDao;
    @Mock
    ProzessbaukastenService prozessbaukastenService;
    @Mock
    ProzessbaukastenReadService prozessbaukastenReadService;
    @Mock
    ProzessbaukastenAnforderungKonzeptService anforderungKonzeptService;
    @Mock
    BerechtigungService berechtigungService;
    @Mock
    private HttpServletRequest request;
    @Mock
    private ThemenklammerService themenklammerService;

    @InjectMocks
    private ProzessbaukastenEditViewFacade facade;

    @BeforeEach
    public void setUp() {
        when(request.getParameterMap()).thenReturn(new HashMap<>());
    }

    @Test
    public void getProzessbaukasatenEditViewDataExistingViewData() {
        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "P1234");
        urlParameter.addOrReplaceParamter("version", "1");

        when(facade.session.getUserPermissions()).thenReturn(TestDataFactory.generateUserPermissions());
        when(facade.anforderungKonzeptService.getByProzessbaukastenId(any())).thenReturn(new ArrayList<>());
        when(facade.prozessbaukastenService.createRealCopy(any())).thenReturn(Optional.ofNullable(TestDataFactory.generateProzessbaukasten()));
        when(tteamService.getTteamByIdList(any())).thenReturn(TestDataFactory.generateTteams());
        when(prozessbaukastenReadService.getByFachIdAndVersion(any(), anyInt())).thenReturn(TestDataFactory.generateOptionalProzessbaukasten());
        when(berechtigungService.getTteamleiterOfTteam(any())).thenReturn(TestDataFactory.generateOptionalTteamLeiter());

        ProzessbaukastenEditViewData viewData = facade.getViewData(urlParameter);

        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertTrue(viewData.getFachId().equals("P1234")),
                () -> Assertions.assertTrue(viewData.getVersion().equals("1")),
                () -> Assertions.assertTrue(viewData.getStatus().equals(ProzessbaukastenStatus.ANGELEGT)),
                () -> Assertions.assertEquals(viewData.getErstanlaeufer().getName(), "E46"));

    }

    @Test
    public void getProzessbaukasatenEditViewDataNewStatus() {
        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "P1234");
        urlParameter.addOrReplaceParamter("version", "1");
        urlParameter.addOrReplaceParamter("newStatus", "1");

        when(facade.session.getUserPermissions()).thenReturn(TestDataFactory.generateUserPermissions());
        when(facade.anforderungKonzeptService.getByProzessbaukastenId(any())).thenReturn(new ArrayList<>());
        when(facade.prozessbaukastenService.createRealCopy(any())).thenReturn(Optional.ofNullable(TestDataFactory.generateProzessbaukasten()));
        when(tteamService.getTteamByIdList(any())).thenReturn(TestDataFactory.generateTteams());
        when(prozessbaukastenReadService.getByFachIdAndVersion(any(), anyInt())).thenReturn(TestDataFactory.generateOptionalProzessbaukasten());
        when(berechtigungService.getTteamleiterOfTteam(any())).thenReturn(TestDataFactory.generateOptionalTteamLeiter());

        ProzessbaukastenEditViewData viewData = facade.getViewData(urlParameter);

        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertTrue(viewData.getFachId().equals("P1234")),
                () -> Assertions.assertTrue(viewData.getVersion().equals("1")),
                () -> Assertions.assertTrue(viewData.getNewStatus().equals(ProzessbaukastenStatus.BEAUFTRAGT)),
                () -> Assertions.assertEquals(viewData.getErstanlaeufer().getName(), "E46"));

    }

    @Test
    public void getProzessbaukasatenEditViewDataNewVersion() {
        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "P1234");
        urlParameter.addOrReplaceParamter("version", "1");
        urlParameter.addOrReplaceParamter("created", "1");
        urlParameter.addOrReplaceParamter("comment", "Version Kommentar");

        when(facade.session.getUserPermissions()).thenReturn(TestDataFactory.generateUserPermissions());
        when(facade.prozessbaukastenService.createCopyForNewVersion(any())).thenReturn(TestDataFactory.generateProzessbaukasten());
        when(tteamService.getTteamByIdList(any())).thenReturn(TestDataFactory.generateTteams());
        when(prozessbaukastenReadService.getByFachIdAndVersion(any(), anyInt())).thenReturn(TestDataFactory.generateOptionalProzessbaukasten());
        when(berechtigungService.getTteamleiterOfTteam(any())).thenReturn(TestDataFactory.generateOptionalTteamLeiter());

        ProzessbaukastenEditViewData viewData = facade.getViewData(urlParameter);

        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertTrue(viewData.getFachId().equals("P1234")),
                () -> Assertions.assertTrue(viewData.getVersion().equals("1")),
                () -> Assertions.assertTrue(viewData.getNewVersionKommentar().equals("Version Kommentar")),
                () -> Assertions.assertTrue(viewData.isNewVersion()),
                () -> Assertions.assertEquals(viewData.getErstanlaeufer().getName(), "E46"));

    }

    @Test
    public void getProzessbaukasatenEditViewDataNewProzessbaukasten() {
        UrlParameter urlParameter = new UrlParameter(request);

        when(facade.session.getUserPermissions()).thenReturn(TestDataFactory.generateUserPermissions());
        when(tteamService.getTteamByIdList(any())).thenReturn(TestDataFactory.generateTteams());

        ProzessbaukastenEditViewData viewData = facade.getViewData(urlParameter);

        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertTrue(viewData.getFachId().equals("")),
                () -> Assertions.assertTrue(viewData.getVersion().equals("1")));

    }

}
