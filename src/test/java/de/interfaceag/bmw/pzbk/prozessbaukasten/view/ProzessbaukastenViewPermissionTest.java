package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamMitgliedBerechtigung;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author sl
 */
@RunWith(MockitoJUnitRunner.class)
public class ProzessbaukastenViewPermissionTest {

    private final Prozessbaukasten prozessbaukasten = TestDataFactory.generateProzessbaukasten();
    private final Tteam tteam = TestDataFactory.generateTteam();
    private final Collection<Long> tteamIds = Arrays.asList(1L);

    @BeforeEach
    public void setup() {
        tteam.setId(1L);
        prozessbaukasten.setTteam(tteam);
    }

    @Test
    public void testDenied() {
        ProzessbaukastenViewPermission deniedPermission = ProzessbaukastenViewPermission.denied();
        assertFalse(deniedPermission.getPage());
        assertFalse(deniedPermission.getAddAnforderungButton());
        assertFalse(deniedPermission.getNewAnforderungButton());
        assertFalse(deniedPermission.getNewVersionButton());
        assertFalse(deniedPermission.getStatusChangeButton());
        assertFalse(deniedPermission.getEditButton());
    }

    @Test
    public void testConditionTteamMitgliedAndLeser() {
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        Set<Rolle> roles = new HashSet<>(Arrays.asList(Rolle.LESER));
        ProzessbaukastenViewPermission viewPermission = ProzessbaukastenViewPermission
                .forProzessbaukasten(prozessbaukasten)
                .forTteamIdsAsTteamleiter(tteamIds)
                .forUserRoles(roles)
                .forTteamMitgliedBerechtigung(tteamMitgliedBerechtigung)
                .forTteamIdsAsTteamVertreter(tteamIds).build();
        assertFalse(viewPermission.getPage());

        roles.add(Rolle.TTEAMMITGLIED);
        viewPermission = ProzessbaukastenViewPermission
                .forProzessbaukasten(prozessbaukasten)
                .forTteamIdsAsTteamleiter(tteamIds)
                .forUserRoles(roles)
                .forTteamMitgliedBerechtigung(tteamMitgliedBerechtigung)
                .forTteamIdsAsTteamVertreter(tteamIds).build();
        assertTrue(viewPermission.getPage());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testPagePermissionForRole(Rolle role) {
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
            case ANFORDERER:
                assertTrue(viewPermission.getPage());
                break;
            default:
                assertFalse(viewPermission.getPage());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testEditButtonPermissionForRole(Rolle role) {
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                assertTrue(viewPermission.getEditButton());
                break;
            case ANFORDERER:
                assertFalse(viewPermission.getEditButton());
                break;
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testEditButtonPermissionForRoleWrongTteam(Rolle role) {
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Collections.EMPTY_LIST, Collections.EMPTY_LIST);
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, Collections.EMPTY_SET, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
                assertTrue(viewPermission.getEditButton());
                break;
            case T_TEAMLEITER:
            case TTEAMMITGLIED:
            case ANFORDERER:
                assertFalse(viewPermission.getEditButton());
                break;
            default:
                assertFalse(viewPermission.getEditButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testAddAnforderungButtonPermissionForRoleAndProzessbaukastenAbgebrochen(Rolle role) {
        prozessbaukasten.setStatus(ProzessbaukastenStatus.ABGEBROCHEN);
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        assertFalse(viewPermission.getAddAnforderungButton());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testAddAnforderungButtonPermissionForRoleAndProzessbaukastenGeloescht(Rolle role) {
        prozessbaukasten.setStatus(ProzessbaukastenStatus.GELOESCHT);
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        assertFalse(viewPermission.getAddAnforderungButton());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testAddAnforderungButtonPermissionForRoleAndProzessbaukastenGueltig(Rolle role) {
        prozessbaukasten.setStatus(ProzessbaukastenStatus.GUELTIG);
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        assertFalse(viewPermission.getAddAnforderungButton());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testAddAnforderungButtonPermissionForRoleAndProzessbaukastenStillgelegt(Rolle role) {
        prozessbaukasten.setStatus(ProzessbaukastenStatus.STILLGELEGT);
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        assertFalse(viewPermission.getAddAnforderungButton());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testAddAnforderungButtonPermissionForRoleAndProzessbaukastenAngelegt(Rolle role) {
        prozessbaukasten.setStatus(ProzessbaukastenStatus.ANGELEGT);
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                assertTrue(viewPermission.getAddAnforderungButton());
                break;
            case ANFORDERER:
                assertFalse(viewPermission.getAddAnforderungButton());
                break;
            default:
                assertFalse(viewPermission.getAddAnforderungButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testAddAnforderungButtonPermissionForRoleAndProzessbaukastenBeauftragt(Rolle role) {
        prozessbaukasten.setStatus(ProzessbaukastenStatus.BEAUFTRAGT);
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                assertTrue(viewPermission.getAddAnforderungButton());
                break;
            default:
                assertFalse(viewPermission.getAddAnforderungButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testAddAnforderungButtonPermissionForRoleWrongTteam(Rolle role) {
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Collections.EMPTY_LIST, Collections.EMPTY_LIST);
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, Collections.EMPTY_SET, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
                assertTrue(viewPermission.getAddAnforderungButton());
                break;
            case T_TEAMLEITER:
            case TTEAMMITGLIED:
            case TTEAM_VERTRETER:
            case ANFORDERER:
                assertFalse(viewPermission.getAddAnforderungButton());
                break;
            default:
                assertFalse(viewPermission.getAddAnforderungButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testNewAnforderungButtonPermissionForRoleAndProzessbaukastenAngelegt(Rolle role) {
        prozessbaukasten.setStatus(ProzessbaukastenStatus.ANGELEGT);
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                assertTrue(viewPermission.getNewAnforderungButton());
                break;
            default:
                assertFalse(viewPermission.getNewAnforderungButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testNewAnforderungButtonPermissionForRoleAndProzessbaukastenBeauftragt(Rolle role) {
        prozessbaukasten.setStatus(ProzessbaukastenStatus.BEAUFTRAGT);
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                assertTrue(viewPermission.getNewAnforderungButton());
                break;
            default:
                assertFalse(viewPermission.getNewAnforderungButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testNewAnforderungButtonPermissionForRoleAndProzessbaukastenAbgebrochen(Rolle role) {
        prozessbaukasten.setStatus(ProzessbaukastenStatus.ABGEBROCHEN);
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        assertFalse(viewPermission.getNewAnforderungButton());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testNewAnforderungButtonPermissionForRoleAndProzessbaukastenGeloescht(Rolle role) {
        prozessbaukasten.setStatus(ProzessbaukastenStatus.GELOESCHT);
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        assertFalse(viewPermission.getNewAnforderungButton());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testNewAnforderungButtonPermissionForRoleAndProzessbaukastenGueltig(Rolle role) {
        prozessbaukasten.setStatus(ProzessbaukastenStatus.GUELTIG);
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        assertFalse(viewPermission.getNewAnforderungButton());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testNewAnforderungButtonPermissionForRoleAndProzessbaukastenStillgelegt(Rolle role) {
        prozessbaukasten.setStatus(ProzessbaukastenStatus.STILLGELEGT);
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        assertFalse(viewPermission.getNewAnforderungButton());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testNewAnforderungButtonPermissionForRoleWrongTteam(Rolle role) {
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Collections.EMPTY_LIST, Collections.EMPTY_LIST);
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, Collections.EMPTY_SET, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
                assertTrue(viewPermission.getNewAnforderungButton());
                break;
            case T_TEAMLEITER:
            case TTEAMMITGLIED:
            case TTEAM_VERTRETER:
            case ANFORDERER:
                assertFalse(viewPermission.getNewAnforderungButton());
                break;
            default:
                assertFalse(viewPermission.getNewAnforderungButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testNewVersionButtonPermissionForRole(Rolle role) {
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
                assertTrue(viewPermission.getNewVersionButton());
                break;
            default:
                assertFalse(viewPermission.getNewVersionButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testNewVersionButtonPermissionForRoleWrongTteam(Rolle role) {
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, Collections.EMPTY_SET, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
                assertTrue(viewPermission.getNewVersionButton());
                break;
            case T_TEAMLEITER:
                assertFalse(viewPermission.getNewVersionButton());
                break;
            default:
                assertFalse(viewPermission.getNewVersionButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testStatusChangeButtonPermissionForRole(Rolle role) {
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
                assertTrue(viewPermission.getStatusChangeButton());
                break;
            default:
                assertFalse(viewPermission.getStatusChangeButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testStatusChangeButtonPermissionForRoleWrongTteam(Rolle role) {
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, Collections.EMPTY_SET, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
                assertTrue(viewPermission.getStatusChangeButton());
                break;
            case T_TEAMLEITER:
            case ANFORDERER:
                assertFalse(viewPermission.getStatusChangeButton());
                break;
            default:
                assertFalse(viewPermission.getStatusChangeButton());
                break;
        }
    }

    private ProzessbaukastenViewPermission getViewPermissionForRole(Rolle role,
            Prozessbaukasten prozessbaukasten, Collection<Long> tteamIds, TteamMitgliedBerechtigung tteamMitgliedBerechtigung) {
        Set<Rolle> userRoles = new HashSet<>();

        userRoles.add(role);
        return ProzessbaukastenViewPermission
                .forProzessbaukasten(prozessbaukasten)
                .forTteamIdsAsTteamleiter(tteamIds)
                .forUserRoles(userRoles)
                .forTteamMitgliedBerechtigung(tteamMitgliedBerechtigung)
                .forTteamIdsAsTteamVertreter(tteamIds).build();
    }

}
