package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@RunWith(MockitoJUnitRunner.class)
public class ProzessbaukastenStatusChangeServiceTest {

    @InjectMocks
    private ProzessbaukastenStatusChangeService statusChangeService;

    @Mock
    private Prozessbaukasten prozessbaukasten;

    @Mock
    private Mitarbeiter mitarbeiter;

    @Mock
    private ProzessbaukastenService prozessbaukastenService;

    @Before
    public void setUp() {
        when(prozessbaukasten.getStatus()).thenReturn(ProzessbaukastenStatus.GUELTIG);
    }

    @Test
    public void testProcessStatusChangeDatumStatuswechselCalled() {
        statusChangeService.processStatusChange(ProzessbaukastenStatus.ANGELEGT, prozessbaukasten, "", mitarbeiter, new Date());
        verify(prozessbaukasten, times(1)).setDatumStatuswechsel(any());
    }

    @Test
    public void testProcessStatusChangeNewStatusCorrect() {
        ProzessbaukastenStatus newStatus = ProzessbaukastenStatus.ANGELEGT;
        statusChangeService.processStatusChange(newStatus, prozessbaukasten, "", mitarbeiter, new Date());
        verify(prozessbaukasten, times(1)).setStatus(newStatus);
    }

    @Test
    public void testProcessStatusChangeInvalidStatusInput() {
        statusChangeService.processStatusChange(null, prozessbaukasten, "", mitarbeiter, new Date());
        verify(prozessbaukasten, never()).setStatus(any());
    }

    @Test
    public void testProcessStatusChangeInvalidProzessbaukastenInput() {
        statusChangeService.processStatusChange(ProzessbaukastenStatus.ABGEBROCHEN, null, "", mitarbeiter, new Date());
        verify(prozessbaukasten, never()).setStatus(any());
    }

}
