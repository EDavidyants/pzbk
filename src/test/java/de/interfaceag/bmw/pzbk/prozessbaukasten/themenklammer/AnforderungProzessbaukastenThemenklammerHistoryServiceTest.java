package de.interfaceag.bmw.pzbk.prozessbaukasten.themenklammer;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenHistory;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenThemenklammer;
import de.interfaceag.bmw.pzbk.entities.Themenklammer;
import de.interfaceag.bmw.pzbk.prozessbaukasten.history.ProzessbaukastenHistoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AnforderungProzessbaukastenThemenklammerHistoryServiceTest {

    private static final String USER = "USER";
    private static final String THEMENKLAMMER = "THEMENKLAMMER";
    private static final String ANFORDERUNG = "ANFORDERUNG";


    @Mock
    private Mitarbeiter currentUser;
    @Mock
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;
    @InjectMocks
    private AnforderungProzessbaukastenThemenklammerHistoryService anforderungProzessbaukastenThemenklammerHistoryService;

    @Mock
    private Anforderung anforderung;
    @Mock
    private ProzessbaukastenThemenklammer prozessbaukastenThemenklammer;
    @Mock
    private Themenklammer themenklammer;
    @Mock
    private Prozessbaukasten prozessbaukasten;

    @BeforeEach
    void setUp() {
        when(anforderung.toString()).thenReturn(ANFORDERUNG);
        when(currentUser.toString()).thenReturn(USER);
        when(prozessbaukastenThemenklammer.getProzessbaukasten()).thenReturn(prozessbaukasten);
        when(prozessbaukastenThemenklammer.getThemenklammer()).thenReturn(themenklammer);
        when(themenklammer.toString()).thenReturn(THEMENKLAMMER);
    }

    @Test
    void writeHistoryEntryForAddedThemenklammerBenutzerValue() {
        final ProzessbaukastenHistory result = anforderungProzessbaukastenThemenklammerHistoryService.writeHistoryEntryForAddedThemenklammer(anforderung, prozessbaukastenThemenklammer);
        final String value = result.getBenutzer();
        assertThat(value, is(USER));
    }

    @Test
    void writeHistoryEntryForAddedThemenklammerChangedFieldValue() {
        final ProzessbaukastenHistory result = anforderungProzessbaukastenThemenklammerHistoryService.writeHistoryEntryForAddedThemenklammer(anforderung, prozessbaukastenThemenklammer);
        final String value = result.getChangedField();
        assertThat(value, is(THEMENKLAMMER));
    }

    @Test
    void writeHistoryEntryForAddedThemenklammerVonValue() {
        final ProzessbaukastenHistory result = anforderungProzessbaukastenThemenklammerHistoryService.writeHistoryEntryForAddedThemenklammer(anforderung, prozessbaukastenThemenklammer);
        final String value = result.getVon();
        assertThat(value, is(ANFORDERUNG));
    }
    
    @Test
    void writeHistoryEntryForAddedThemenklammerAufValue() {
        final ProzessbaukastenHistory result = anforderungProzessbaukastenThemenklammerHistoryService.writeHistoryEntryForAddedThemenklammer(anforderung, prozessbaukastenThemenklammer);
        final String value = result.getAuf();
        assertThat(value, is("zugeordnet"));
    }

    @Test
    void writeHistoryEntryForAddedThemenklammerProzessbaukastenValue() {
        final ProzessbaukastenHistory result = anforderungProzessbaukastenThemenklammerHistoryService.writeHistoryEntryForAddedThemenklammer(anforderung, prozessbaukastenThemenklammer);
        final Prozessbaukasten resultProzessbaukasten = result.getProzessbaukasten();
        assertThat(resultProzessbaukasten, is(prozessbaukasten));
    }

    @Test
    void writeHistoryEntryForRemovedThemenklammerBenutzerValue() {
        final ProzessbaukastenHistory result = anforderungProzessbaukastenThemenklammerHistoryService.writeHistoryEntryForRemovedThemenklammer(anforderung, prozessbaukastenThemenklammer);
        final String value = result.getBenutzer();
        assertThat(value, is(USER));
    }

    @Test
    void writeHistoryEntryForRemovedThemenklammerChangedFieldValue() {
        final ProzessbaukastenHistory result = anforderungProzessbaukastenThemenklammerHistoryService.writeHistoryEntryForRemovedThemenklammer(anforderung, prozessbaukastenThemenklammer);
        final String value = result.getChangedField();
        assertThat(value, is(THEMENKLAMMER));
    }

    @Test
    void writeHistoryEntryForRemovedThemenklammerVonValue() {
        final ProzessbaukastenHistory result = anforderungProzessbaukastenThemenklammerHistoryService.writeHistoryEntryForRemovedThemenklammer(anforderung, prozessbaukastenThemenklammer);
        final String value = result.getVon();
        assertThat(value, is(ANFORDERUNG));
    }

    @Test
    void writeHistoryEntryForRemovedThemenklammerAufValue() {
        final ProzessbaukastenHistory result = anforderungProzessbaukastenThemenklammerHistoryService.writeHistoryEntryForRemovedThemenklammer(anforderung, prozessbaukastenThemenklammer);
        final String value = result.getAuf();
        assertThat(value, is("entfernt"));
    }

    @Test
    void writeHistoryEntryForRemovedThemenklammerProzessbaukastenValue() {
        final ProzessbaukastenHistory result = anforderungProzessbaukastenThemenklammerHistoryService.writeHistoryEntryForRemovedThemenklammer(anforderung, prozessbaukastenThemenklammer);
        final Prozessbaukasten resultProzessbaukasten = result.getProzessbaukasten();
        assertThat(resultProzessbaukasten, is(prozessbaukasten));
    }
}