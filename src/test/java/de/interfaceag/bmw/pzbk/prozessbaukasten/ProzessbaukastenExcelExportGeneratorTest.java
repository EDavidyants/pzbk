package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.enums.Status;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

class ProzessbaukastenExcelExportGeneratorTest {

    private static final String ID = "Id";
    private static final String BESCHREIBUNG = "Beschreibung";
    private static final String SENSOR_COC = "SensorCoc";
    private static final String UMSETZER = "Umsetzer";
    private static final String KONZEPT = "Konzept";
    private static final String THEMENKLAMMER = "Themenklammer";

    private ProzessbaukastenAnforderung prozessbaukastenAnforderung;
    private List<ProzessbaukastenAnforderung> anforderungen;
    private Workbook workbook;

    @BeforeEach
    void setUp() {
        prozessbaukastenAnforderung = getProzessbaukastenAnforderung();
        anforderungen = Collections.singletonList(prozessbaukastenAnforderung);
        workbook = new XSSFWorkbook();
    }

    private ProzessbaukastenAnforderung getProzessbaukastenAnforderung() {
        return AnforderungenTabDTO.getBuilder()
                .withId(ID)
                .withFachId("A12")
                .withVersion("3")
                .withBeschreibung(BESCHREIBUNG)
                .withSensorCoc(SENSOR_COC)
                .withUmsetzer(UMSETZER)
                .withKonzept(KONZEPT)
                .withThemenklammer(THEMENKLAMMER)
                .withStatus(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN)
                .build();
    }

    @Test
    void generateExcelExportStructureTest() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        assertThat(workbook.getNumberOfSheets(), is(1));
    }

    @Test
    void generateExcelExportSheetNameTest() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        assertThat(workbook.getSheet("Anforderungen"), notNullValue());
    }

    @Test
    void generateExcelExportSheetNumberOfColumnsHeaderRowTest() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row headerRow = sheet.getRow(0);
        assertThat(headerRow.getLastCellNum(), is(new Short("7")));
    }

    @Test
    void generateExcelExportSheetHeaderRowColl0Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row headerRow = sheet.getRow(0);
        final Cell cell = headerRow.getCell(0);
        assertThat(cell.getStringCellValue(), is("Id"));
    }

    @Test
    void generateExcelExportSheetHeaderRowColl1Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row headerRow = sheet.getRow(0);
        final Cell cell = headerRow.getCell(1);
        assertThat(cell.getStringCellValue(), is("Beschreibung"));
    }

    @Test
    void generateExcelExportSheetHeaderRowColl2Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row headerRow = sheet.getRow(0);
        final Cell cell = headerRow.getCell(2);
        assertThat(cell.getStringCellValue(), is("Anf.-Geber"));
    }

    @Test
    void generateExcelExportSheetHeaderRowColl3Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row headerRow = sheet.getRow(0);
        final Cell cell = headerRow.getCell(3);
        assertThat(cell.getStringCellValue(), is("Umsetzer"));
    }

    @Test
    void generateExcelExportSheetHeaderRowColl4Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row headerRow = sheet.getRow(0);
        final Cell cell = headerRow.getCell(4);
        assertThat(cell.getStringCellValue(), is("Konzept"));
    }

    @Test
    void generateExcelExportSheetHeaderRowColl5Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row headerRow = sheet.getRow(0);
        final Cell cell = headerRow.getCell(5);
        assertThat(cell.getStringCellValue(), is("Themenklammer"));
    }

    @Test
    void generateExcelExportSheetHeaderRowColl6Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row headerRow = sheet.getRow(0);
        final Cell cell = headerRow.getCell(6);
        assertThat(cell.getStringCellValue(), is("Status"));
    }

    @Test
    void generateExcelExportSheetNumberOfColumnsContentRowTest() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row headerRow = sheet.getRow(1);
        assertThat(headerRow.getLastCellNum(), is(new Short("7")));
    }

    @Test
    void generateExcelExportSheetContentRowColl0Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row contentRow = sheet.getRow(1);
        final Cell cell = contentRow.getCell(0);
        assertThat(cell.getStringCellValue(), is("A12 | V3"));
    }

    @Test
    void generateExcelExportSheetContentRowColl1Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row contentRow = sheet.getRow(1);
        final Cell cell = contentRow.getCell(1);
        assertThat(cell.getStringCellValue(), is(BESCHREIBUNG));
    }

    @Test
    void generateExcelExportSheetContentRowColl2Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row contentRow = sheet.getRow(1);
        final Cell cell = contentRow.getCell(2);
        assertThat(cell.getStringCellValue(), is(SENSOR_COC));
    }

    @Test
    void generateExcelExportSheetContentRowColl3Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row contentRow = sheet.getRow(1);
        final Cell cell = contentRow.getCell(3);
        assertThat(cell.getStringCellValue(), is(UMSETZER));
    }

    @Test
    void generateExcelExportSheetContentRowColl4Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row contentRow = sheet.getRow(1);
        final Cell cell = contentRow.getCell(4);
        assertThat(cell.getStringCellValue(), is(KONZEPT));
    }

    @Test
    void generateExcelExportSheetContentRowColl5Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row contentRow = sheet.getRow(1);
        final Cell cell = contentRow.getCell(5);
        assertThat(cell.getStringCellValue(), is(THEMENKLAMMER));
    }

    @Test
    void generateExcelExportSheetContentRowColl6Test() {
        ProzessbaukastenExcelExportGenerator.generateExcelExport(workbook, anforderungen);
        final Sheet sheet = workbook.getSheet("Anforderungen");
        final Row contentRow = sheet.getRow(1);
        final Cell cell = contentRow.getCell(6);
        assertThat(cell.getStringCellValue(), is("PZBK genehmigt"));
    }
    
}