package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes.required;

import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

/**
 *
 * @author sl
 */
public class ProzessbaukastenRequiredAttributesFactoryTest {

    @ParameterizedTest
    @EnumSource(ProzessbaukastenStatus.class)
    public void testGetRequiredAttributesForProzessbaukasten(ProzessbaukastenStatus status) {
        ProzessbaukastenRequiredAttributes result = ProzessbaukastenRequiredAttributesFactory.getRequiredAttributesForProzessbaukasten(status);
        Class expected;
        switch (status) {
            case ANGELEGT:
                expected = ProzessbaukastenStatusAngelegtRequiredAttributes.class;
                break;
            case BEAUFTRAGT:
                expected = ProzessbaukastenStatusBeauftragtRequiredAttributes.class;
                break;
            case GUELTIG:
                expected = ProzessbaukastenStatusGueltigRequiredAttributes.class;
                break;
            case GELOESCHT:
            case STILLGELEGT:
            case ABGEBROCHEN:
            default:
                expected = ProzessbaukastenNoRequiredAttributes.class;
                break;
        }
        Assertions.assertEquals(expected, result.getClass());
    }

}
