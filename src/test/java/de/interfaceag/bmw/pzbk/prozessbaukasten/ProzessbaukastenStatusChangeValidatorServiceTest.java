package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenViewData;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProzessbaukastenStatusChangeValidatorServiceTest {


    private Prozessbaukasten prozessbaukasten;

    private ProzessbaukastenViewData prozessbaukastenViewData;

    @Mock
    private ProzessbaukastenReadService prozessbaukastenReadService;

    @Mock
    private LocalizationService localizationService;

    @Mock
    private ProzessbaukastenDialogAndGrowlService prozessbaukastenDialogAndGrowlService;

    @InjectMocks
    private ProzessbaukastenStatusChangeValidatorService prozessbaukastenStatusChangeValidatorService;

    private ActiveIndex activeIndex;

    @BeforeEach
    public void setUp() {
        activeIndex = ActiveIndex.forTab(ProzessbaukastenTab.STAMMDATEN);
        prozessbaukastenViewData = TestDataFactory.generateProzessbaukastenViewData();
        prozessbaukasten = TestDataFactory.generateProzessbaukasten();

    }

    @ParameterizedTest
    @EnumSource(ProzessbaukastenStatus.class)
    void validateStatusChangeTest(ProzessbaukastenStatus status) {
        String result;
        switch (status) {
            case ANGELEGT:
                result = prozessbaukastenStatusChangeValidatorService.validateStatusChange(status, prozessbaukastenViewData, activeIndex, prozessbaukasten);
                Assertions.assertEquals("prozessbaukastenEdit.xhtml?activeIndex=0&newStatus=0&fachId=P1234&version=1&faces-redirect=true", result);
                break;
            case BEAUFTRAGT:
                result = prozessbaukastenStatusChangeValidatorService.validateStatusChange(status, prozessbaukastenViewData, activeIndex, prozessbaukasten);
                Assertions.assertEquals("prozessbaukastenEdit.xhtml?activeIndex=0&newStatus=1&fachId=P1234&version=1&faces-redirect=true", result);
                break;
            case ABGEBROCHEN:
            case GELOESCHT:
            case STILLGELEGT:
                when(prozessbaukastenDialogAndGrowlService.openStatusChangeDialog()).thenReturn("");
                result = prozessbaukastenStatusChangeValidatorService.validateStatusChange(status, prozessbaukastenViewData, activeIndex, prozessbaukasten);
                Assertions.assertEquals("", result);
                break;
            case GUELTIG:
                Prozessbaukasten prozessbaukastenForTest = TestDataFactory.generateProzessbaukasten();
                prozessbaukastenForTest.setStatus(ProzessbaukastenStatus.STILLGELEGT);

                when(prozessbaukastenReadService.getByFachIdAndVersion(any(), anyInt())).thenReturn(Optional.of(prozessbaukastenForTest));

                Anforderung anforderungForTest = TestDataFactory.generateAnforderung();
                anforderungForTest.setStatus(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
                prozessbaukasten.setAnforderungen(Arrays.asList(anforderungForTest));


                result = prozessbaukastenStatusChangeValidatorService.validateStatusChange(status, prozessbaukastenViewData, activeIndex, prozessbaukasten);
                Assertions.assertEquals("prozessbaukastenEdit.xhtml?activeIndex=0&newStatus=4&fachId=P1234&version=1&faces-redirect=true", result);
                break;
        }

    }

    @Test
    void validateGueltigenProzessbaukasatenWithoutAnforderungen() {

        Prozessbaukasten prozessbaukastenPrevious = TestDataFactory.generateProzessbaukasten();
        prozessbaukastenPrevious.setStatus(ProzessbaukastenStatus.STILLGELEGT);

        when(prozessbaukastenReadService.getByFachIdAndVersion(any(), anyInt())).thenReturn(Optional.of(prozessbaukastenPrevious));

        when(localizationService.getValue("prozessbaukasten_view_nichtAlleAnforderungenFreigegeben")).thenReturn("Nicht Alle Anforderungen wurden Freigegeben");

        Anforderung anforderungForTest = TestDataFactory.generateAnforderung();
        anforderungForTest.setStatus(Status.A_ANGELEGT_IN_PROZESSBAUKASTEN);
        prozessbaukasten.setAnforderungen(Arrays.asList(anforderungForTest));

        when(prozessbaukastenDialogAndGrowlService.openStatusChangeNotAllowedGrowl(any())).thenReturn("");

        String result = prozessbaukastenStatusChangeValidatorService.validateStatusChange(ProzessbaukastenStatus.GUELTIG, prozessbaukastenViewData, activeIndex, prozessbaukasten);
        Assertions.assertEquals("", result);

    }

    @Test
    void validateGueltigerProzessbaukastenWithoutPreviousProzessbaukasten() {

        when(prozessbaukastenReadService.getByFachIdAndVersion(any(), anyInt())).thenReturn(Optional.ofNullable(null));


        prozessbaukasten.setAnforderungen(Collections.emptyList());


        String result = prozessbaukastenStatusChangeValidatorService.validateStatusChange(ProzessbaukastenStatus.GUELTIG, prozessbaukastenViewData, activeIndex, prozessbaukasten);
        Assertions.assertEquals("prozessbaukastenEdit.xhtml?activeIndex=0&newStatus=4&fachId=P1234&version=1&faces-redirect=true", result);
    }

    @Test
    void validateGueltigerProzessbaukastenWithPreviousProzessbaukastenNotStillgelegt() {

        Prozessbaukasten prozessbaukastenPrevious = TestDataFactory.generateProzessbaukasten();
        prozessbaukastenPrevious.setStatus(ProzessbaukastenStatus.GUELTIG);

        when(prozessbaukastenReadService.getByFachIdAndVersion(any(), anyInt())).thenReturn(Optional.of(prozessbaukastenPrevious));

        when(localizationService.getValue("prozessbaukasten_view_statusWechselToGenehmigtNichtMoeglichMessage")).thenReturn("Nicht Alle Anforderungen wurden Freigegeben");

        Anforderung anforderungForTest = TestDataFactory.generateAnforderung();
        anforderungForTest.setStatus(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
        prozessbaukasten.setAnforderungen(Arrays.asList(anforderungForTest));

        when(prozessbaukastenDialogAndGrowlService.openStatusChangeNotAllowedGrowl(any())).thenReturn("");

        String result = prozessbaukastenStatusChangeValidatorService.validateStatusChange(ProzessbaukastenStatus.GUELTIG, prozessbaukastenViewData, activeIndex, prozessbaukasten);
        Assertions.assertEquals("", result);
    }

    @Test
    void validateGueltigerProzessbaukastenWithPreviousProzessbaukastenGeloescht() {

        Prozessbaukasten prozessbaukastenPrevious = TestDataFactory.generateProzessbaukasten();
        prozessbaukastenPrevious.setStatus(ProzessbaukastenStatus.GELOESCHT);

        when(prozessbaukastenReadService.getByFachIdAndVersion(any(), anyInt())).thenReturn(Optional.of(prozessbaukastenPrevious));

        Anforderung anforderungForTest = TestDataFactory.generateAnforderung();
        anforderungForTest.setStatus(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
        prozessbaukasten.setAnforderungen(Arrays.asList(anforderungForTest));

        String result = prozessbaukastenStatusChangeValidatorService.validateStatusChange(ProzessbaukastenStatus.GUELTIG, prozessbaukastenViewData, activeIndex, prozessbaukasten);
        Assertions.assertEquals("prozessbaukastenEdit.xhtml?activeIndex=0&newStatus=4&fachId=P1234&version=1&faces-redirect=true", result);
    }

    @Test
    void validateGueltigerProzessbaukastenWithPreviousProzessbaukastenStillgelegt() {

        Prozessbaukasten prozessbaukastenPrevious = TestDataFactory.generateProzessbaukasten();
        prozessbaukastenPrevious.setStatus(ProzessbaukastenStatus.STILLGELEGT);

        when(prozessbaukastenReadService.getByFachIdAndVersion(any(), anyInt())).thenReturn(Optional.of(prozessbaukastenPrevious));

        Anforderung anforderungForTest = TestDataFactory.generateAnforderung();
        anforderungForTest.setStatus(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
        prozessbaukasten.setAnforderungen(Arrays.asList(anforderungForTest));

        String result = prozessbaukastenStatusChangeValidatorService.validateStatusChange(ProzessbaukastenStatus.GUELTIG, prozessbaukastenViewData, activeIndex, prozessbaukasten);
        Assertions.assertEquals("prozessbaukastenEdit.xhtml?activeIndex=0&newStatus=4&fachId=P1234&version=1&faces-redirect=true", result);
    }
}