package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungStatusChangeService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.Kategorie;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.AnforderungReportingStatusTransitionAdapter;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ProzessbaukastenCreateAnforderungZuordnungServiceTest {

    @Mock
    private AnforderungMeldungHistoryService historyService;
    @Mock
    private AnforderungReportingStatusTransitionAdapter anforderungReportingStatusTransitionAdapter;
    @Mock
    private AnforderungStatusChangeService anforderungStatusChangeSerivce;
    @Mock
    private ProzessbaukastenService prozessbaukastenService;
    @Mock
    private Mitarbeiter mitarbeiter;

    @InjectMocks
    private ProzessbaukastenCreateAnforderungZuordnungService createAnforderungZuordnungService;

    private Prozessbaukasten prozessbaukasten;
    private Anforderung anforderung;

    @BeforeEach
    void setUp() {
        anforderung = TestDataFactory.generateAnforderung();
        prozessbaukasten = TestDataFactory.generateProzessbaukasten();
    }

    @Test
    void assignAnforderungToProzessbaukastenKategorieBefore() {
        final Kategorie kategorie = anforderung.getKategorie();
        assertThat(kategorie, is(Kategorie.ZWEI));
    }

    @Test
    void assignAnforderungToProzessbaukastenNewKategorie() {
        createAnforderungZuordnungService.assignAnforderungToProzessbaukasten(anforderung, prozessbaukasten, mitarbeiter);
        final Kategorie kategorie = anforderung.getKategorie();
        assertThat(kategorie, is(Kategorie.EINS));
    }

    @Test
    void assignAnforderungToProzessbaukastenAnforderungStatusChangeToAngelegt() {
        createAnforderungZuordnungService.assignAnforderungToProzessbaukasten(anforderung, prozessbaukasten, mitarbeiter);
        verify(anforderungStatusChangeSerivce, times(1)).changeAnforderungStatus(anforderung, Status.A_ANGELEGT_IN_PROZESSBAUKASTEN, "Angelegt im Prozessbaukasten", mitarbeiter);
    }

    @Test
    void assignAnforderungToProzessbaukastenReportingEntry() {
        createAnforderungZuordnungService.assignAnforderungToProzessbaukasten(anforderung, prozessbaukasten, mitarbeiter);
        verify(anforderungReportingStatusTransitionAdapter, times(1)).addAnforderungToProzessbaukasten(anforderung, Status.A_INARBEIT);
    }

    @Test
    void assignAnforderungToProzessbaukastenAnforderungListOfProzessbaukastenBefore() {
        final Set<Prozessbaukasten> prozessbaukasten = anforderung.getProzessbaukasten();
        assertThat(prozessbaukasten, empty());
    }

    @Test
    void assignAnforderungToProzessbaukastenAnforderungListOfProzessbaukastenAfter() {
        createAnforderungZuordnungService.assignAnforderungToProzessbaukasten(anforderung, prozessbaukasten, mitarbeiter);
        final Set<Prozessbaukasten> prozessbaukasten = anforderung.getProzessbaukasten();
        assertThat(prozessbaukasten, hasSize(1));
    }

    @Test
    void assignAnforderungToProzessbaukastenProzessbaukastenListOfAnforderungenBefore() {
        final List<Anforderung> anforderungen = prozessbaukasten.getAnforderungen();
        assertThat(anforderungen, hasSize(1));
    }

    @Test
    void assignAnforderungToProzessbaukastenProzessbaukastenListOfAnforderungenAfter() {
        createAnforderungZuordnungService.assignAnforderungToProzessbaukasten(anforderung, prozessbaukasten, mitarbeiter);
        final List<Anforderung> anforderungen = prozessbaukasten.getAnforderungen();
        assertThat(anforderungen, hasSize(2));
    }
}
