package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Collections;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProzessbaukastenFreigabeUtilsTest {


    @Mock
    Anforderung anforderung1;
    @Mock
    Anforderung anforderung2;

    @Mock
    ProzessbaukastenAnforderung prozessbaukastenAnforderung1;
    @Mock
    ProzessbaukastenAnforderung prozessbaukastenAnforderung2;

    private Collection<Anforderung> anforderungen;
    private Collection<ProzessbaukastenAnforderung> prozessbaukastenAnforderungen;

    @BeforeEach
    void setUp() {
        anforderungen = asList(anforderung1, anforderung2);
        prozessbaukastenAnforderungen = asList(prozessbaukastenAnforderung1, prozessbaukastenAnforderung2);
    }

    @Test
    void isAllAnforderungenGenehmigtImProzessbaukastenNull() {
        final boolean result = ProzessbaukastenFreigabeUtils.isAllAnforderungenGenehmigtImProzessbaukasten(null);
        assertTrue(result);
    }

    @Test
    void isAllAnforderungenGenehmigtImProzessbaukastenEmptyList() {
        final boolean result = ProzessbaukastenFreigabeUtils.isAllAnforderungenGenehmigtImProzessbaukasten(Collections.emptyList());
        assertTrue(result);
    }

    @Test
    void isAllAnforderungenGenehmigtImProzessbaukastenNoneGueltig() {
        when(anforderung1.getStatus()).thenReturn(Status.A_INARBEIT);
        final boolean result = ProzessbaukastenFreigabeUtils.isAllAnforderungenGenehmigtImProzessbaukasten(anforderungen);
        assertFalse(result);
    }

    @Test
    void isAllAnforderungenGenehmigtImProzessbaukastenSomeGueltig() {
        when(anforderung1.getStatus()).thenReturn(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
        when(anforderung2.getStatus()).thenReturn(Status.A_INARBEIT);
        final boolean result = ProzessbaukastenFreigabeUtils.isAllAnforderungenGenehmigtImProzessbaukasten(anforderungen);
        assertFalse(result);
    }

    @Test
    void isAllAnforderungenGenehmigtImProzessbaukastenAllGueltig() {
        when(anforderung1.getStatus()).thenReturn(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
        when(anforderung2.getStatus()).thenReturn(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
        final boolean result = ProzessbaukastenFreigabeUtils.isAllAnforderungenGenehmigtImProzessbaukasten(anforderungen);
        assertTrue(result);
    }

    @Test
    void isAllProzessbaukastenAnforderungenGenehmigtImProzessbaukastenNull() {
        final boolean result = ProzessbaukastenFreigabeUtils.isAllProzessbaukastenAnforderungenGenehmigtImProzessbaukasten(null);
        assertTrue(result);
    }

    @Test
    void isAllProzessbaukastenAnforderungenGenehmigtImProzessbaukastenEmptyList() {
        final boolean result = ProzessbaukastenFreigabeUtils.isAllProzessbaukastenAnforderungenGenehmigtImProzessbaukasten(Collections.emptyList());
        assertTrue(result);
    }

    @Test
    void isAllProzessbaukastenAnforderungenGenehmigtImProzessbaukastenNoneGueltig() {
        when(prozessbaukastenAnforderung1.getStatusValue()).thenReturn(Status.A_INARBEIT);
        final boolean result = ProzessbaukastenFreigabeUtils.isAllProzessbaukastenAnforderungenGenehmigtImProzessbaukasten(prozessbaukastenAnforderungen);
        assertFalse(result);
    }

    @Test
    void isAllProzessbaukastenAnforderungenGenehmigtImProzessbaukastenSomeGueltig() {
        when(prozessbaukastenAnforderung1.getStatusValue()).thenReturn(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
        when(prozessbaukastenAnforderung2.getStatusValue()).thenReturn(Status.A_INARBEIT);
        final boolean result = ProzessbaukastenFreigabeUtils.isAllProzessbaukastenAnforderungenGenehmigtImProzessbaukasten(prozessbaukastenAnforderungen);
        assertFalse(result);
    }

    @Test
    void isAllProzessbaukastenAnforderungenGenehmigtImProzessbaukastenAllGueltig() {
        when(prozessbaukastenAnforderung1.getStatusValue()).thenReturn(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
        when(prozessbaukastenAnforderung2.getStatusValue()).thenReturn(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN);
        final boolean result = ProzessbaukastenFreigabeUtils.isAllProzessbaukastenAnforderungenGenehmigtImProzessbaukasten(prozessbaukastenAnforderungen);
        assertTrue(result);
    }
}