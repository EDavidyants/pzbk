package de.interfaceag.bmw.pzbk.prozessbaukasten.sort;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProzessbaukastenAnforderungOptionalTupleComparatorTest {

    @Mock
    ProzessbaukastenAnforderungOrders prozessbaukastenAnforderungOrders;

    @Mock
    ProzessbaukastenAnforderungOptionalTuple prozessbaukastenAnforderungTuple1;
    @Mock
    ProzessbaukastenAnforderungOptionalTuple prozessbaukastenAnforderungTuple2;

    ProzessbaukastenAnforderungOptionalTupleComparator comparator;

    @BeforeEach
    public void setUp() {
        when(prozessbaukastenAnforderungTuple1.getProzessbaukastenId()).thenReturn(Optional.of(1L));
        comparator = new ProzessbaukastenAnforderungOptionalTupleComparator(prozessbaukastenAnforderungOrders);
    }

    @Test
    public void testCompareForTwoProzessbaukastenAnforderungenFromSameProzessbaukasten() {
        when(prozessbaukastenAnforderungOrders.getOrderForProzessbaukastenAnforderung(anyLong(), anyLong()))
                .thenAnswer(invocation -> {
                    Long argument = (Long) invocation.getArgument(1);
                    if (1L == argument) {
                        return 0;
                    } else if (2L == argument) {
                        return 1;
                    } else {
                        return null;
                    }
                });
        when(prozessbaukastenAnforderungTuple1.getAnforderungId()).thenReturn(1L);
        when(prozessbaukastenAnforderungTuple2.getAnforderungId()).thenReturn(2L);
        when(prozessbaukastenAnforderungTuple2.getProzessbaukastenId()).thenReturn(Optional.of(1L));

        int result = comparator.compare(prozessbaukastenAnforderungTuple1, prozessbaukastenAnforderungTuple2);
        int expected = -1;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testCompareForTwoProzessbaukastenAnforderungenFromDifferentProzessbaukaesten() {
        when(prozessbaukastenAnforderungTuple1.getAnforderungId()).thenReturn(1L);
        when(prozessbaukastenAnforderungTuple2.getAnforderungId()).thenReturn(2L);
        when(prozessbaukastenAnforderungTuple2.getProzessbaukastenId()).thenReturn(Optional.of(2L));
        int result = comparator.compare(prozessbaukastenAnforderungTuple1, prozessbaukastenAnforderungTuple2);
        int expected = -1;
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testCompareForOneProzessbaukastenAnforderungWithNormalAnforderung() {
        when(prozessbaukastenAnforderungTuple2.getProzessbaukastenId()).thenReturn(Optional.empty());
        int result = comparator.compare(prozessbaukastenAnforderungTuple1, prozessbaukastenAnforderungTuple2);
        int expected = 1;
        Assertions.assertEquals(expected, result);
    }

}
