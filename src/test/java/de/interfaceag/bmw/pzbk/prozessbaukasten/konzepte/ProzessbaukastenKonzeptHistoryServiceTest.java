package de.interfaceag.bmw.pzbk.prozessbaukasten.konzepte;

import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.prozessbaukasten.history.ProzessbaukastenHistoryService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ProzessbaukastenKonzeptHistoryServiceTest {

    @Mock
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;

    @InjectMocks
    private ProzessbaukastenKonzeptHistoryService prozessbaukastenKonzeptHistoryService;

    private Prozessbaukasten prozessbaukastenNew;
    private Prozessbaukasten prozessbaukastenOld;
    private Mitarbeiter mitarbeiter;

    @BeforeEach
    void setUp() {
        prozessbaukastenNew = TestDataFactory.generateProzessbaukasten();
        prozessbaukastenOld = TestDataFactory.generateProzessbaukasten();
        mitarbeiter = TestDataFactory.createMitarbeiter("Max");
    }

    @Test
    void writeHistoryForKonzepteNoChange() {

        prozessbaukastenKonzeptHistoryService.writeHistoryForKonzepte(prozessbaukastenOld, prozessbaukastenNew, mitarbeiter);

        verify(prozessbaukastenHistoryService, never()).writeHistoryForChangedKonzeptFields(any(), any(), any(), any(), any(), any());
    }

    @Test
    void writeHistoryForKonzepteAddKonzept() {
        Konzept konzept = TestDataFactory.generateKonzept("Konzept Bezeichnung", "Konzept Beschreibung");
        prozessbaukastenNew.addKonzept(konzept);

        prozessbaukastenKonzeptHistoryService.writeHistoryForKonzepte(prozessbaukastenOld, prozessbaukastenNew, mitarbeiter);

        verify(prozessbaukastenHistoryService, times(1)).writeHistoryForChangedKonzeptFields(any(), any(), any(), any(), eq("neu"), eq("zugeordnet"));
    }

    @Test
    void writeHistoryForKonzepteRemoveKonzept() {
        Konzept konzept = TestDataFactory.generateKonzept("Konzept Bezeichnung", "Konzept Beschreibung");
        prozessbaukastenOld.addKonzept(konzept);

        prozessbaukastenKonzeptHistoryService.writeHistoryForKonzepte(prozessbaukastenOld, prozessbaukastenNew, mitarbeiter);

        verify(prozessbaukastenHistoryService, times(1)).writeHistoryForChangedKonzeptFields(any(), any(), any(), eq("Konzept Bezeichnung"), eq("zugeordnet"), eq("entfernt"));
    }

    @Test
    void writeHistoryForKonzepteKonzeptChanged() {
        Konzept konzept = TestDataFactory.generateKonzept("Konzept Bezeichnung", "Konzept Beschreibung");
        Konzept konzept2 = TestDataFactory.generateKonzept("New Konzept Bezeichnung", "Konzept Beschreibung");

        prozessbaukastenOld.addKonzept(konzept);
        prozessbaukastenNew.addKonzept(konzept2);

        prozessbaukastenKonzeptHistoryService.writeHistoryForKonzepte(prozessbaukastenOld, prozessbaukastenNew, mitarbeiter);

        verify(prozessbaukastenHistoryService, times(1)).writeHistoryForChangedKonzeptFields(any(), any(), any(), eq("Konzept Bezeichnung Bezeichnung"), eq("Konzept Bezeichnung"), eq("New Konzept Bezeichnung"));
    }
}
