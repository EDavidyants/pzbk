package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ProzessbaukastenNeueVersionTest {

    @Mock
    private ProzessbaukastenDao prozessbaukastenDao;

    @InjectMocks
    private final ProzessbaukastenService prozessbaukastenService = new ProzessbaukastenService();

    private Prozessbaukasten prozessbaukasten;
    private Prozessbaukasten prozessbaukastenVersionCopy;
    private Prozessbaukasten prozessbaukastenInvalid;
    private Derivat derivatE46;
    private Anforderung anforderungOne;
    private Anforderung anforderungTwo;

    @BeforeEach
    public void setUp() {
        prozessbaukasten = new Prozessbaukasten("P456", 1);
        prozessbaukasten.setId(456L);
        prozessbaukasten.setBezeichnung("Prozessbaukasten 456");
        prozessbaukasten.setBeschreibung("Beschreibung");
        Mitarbeiter leadTechnologie = TestDataFactory.generateMitarbeiter("Max", "Admin", "Abt.-2");
        prozessbaukasten.setLeadTechnologie(leadTechnologie);
        prozessbaukasten.setStandardisierterFertigungsprozess("standardisierterFertigungsprozess");
        Tteam tteam = TestDataFactory.generateTteam();
        prozessbaukasten.setTteam(tteam);

        TestDataFactory testDataFactory = new TestDataFactory();
        List<Konzept> konzepte = new ArrayList<>();
        Konzept konzeptOne = testDataFactory.generateKonzeptWithAnhang("K123", "Konzept One", "tukan.jpg");
        Konzept konzeptTwo = testDataFactory.generateKonzeptWithAnhang("K456", "Konzept Two", "Kingfisher.jpg");
        konzepte.add(konzeptOne);
        konzepte.add(konzeptTwo);
        prozessbaukasten.setKonzepte(konzepte);

        derivatE46 = TestDataFactory.generateDerivat();
        prozessbaukasten.setErstanlaeufer(derivatE46);

        anforderungOne = TestDataFactory.generateAnforderungWithFachIdAndVersion(123L, "A123", 1);
        anforderungTwo = TestDataFactory.generateAnforderungWithFachIdAndVersion(456L, "A456", 1);
        prozessbaukasten.addAnforderung(anforderungOne);
        prozessbaukasten.addAnforderung(anforderungTwo);

        prozessbaukastenInvalid = new Prozessbaukasten("P456", 1);

        when(prozessbaukastenDao.getLastVersionNumberByFachId("P456")).thenReturn(Optional.of(1));

        prozessbaukastenVersionCopy = prozessbaukastenService.createCopyForNewVersion(prozessbaukasten);

    }

    @Test
    public void createCopyForNewVersionFachIdTest() {
        Assertions.assertEquals(prozessbaukasten.getFachId(), prozessbaukastenVersionCopy.getFachId());
    }

    @Test
    public void createCopyForNewVersionVersionTest() {
        Assertions.assertEquals(prozessbaukasten.getVersion() + 1, prozessbaukastenVersionCopy.getVersion());
    }

    @Test
    public void createCopyForNewVersionBezeichnungTest() {
        Assertions.assertEquals(prozessbaukasten.getBezeichnung(), prozessbaukastenVersionCopy.getBezeichnung());
    }

    @Test
    public void createCopyForNewVersionBeschreibungTest() {
        Assertions.assertEquals(prozessbaukasten.getBeschreibung(), prozessbaukastenVersionCopy.getBeschreibung());
    }

    @Test
    public void createCopyForNewVersionLeadTechnologieTest() {
        Assertions.assertEquals(prozessbaukasten.getLeadTechnologie(), prozessbaukastenVersionCopy.getLeadTechnologie());
    }

    @Test
    public void createCopyForNewVersionTteamTest() {
        Assertions.assertEquals(prozessbaukasten.getTteam(), prozessbaukastenVersionCopy.getTteam());
    }

    @Test
    public void createCopyForNewVersionErstanlaeuferTest() {
        Assertions.assertEquals(derivatE46, prozessbaukastenVersionCopy.getErstanlaeufer());
    }

    @Test
    public void createCopyForNewVersionAnforderungenTest() {
        Assertions.assertEquals(prozessbaukasten.getAnforderungen(), prozessbaukastenVersionCopy.getAnforderungen());
    }

    @Test
    public void createCopyForNewVersionKonzepteTest() {
        Assertions.assertEquals(prozessbaukasten.getKonzepte(), prozessbaukastenVersionCopy.getKonzepte());
    }

    @Test
    public void createCopyForNewVersionTestForNullId() {
        prozessbaukastenVersionCopy = prozessbaukastenService.createCopyForNewVersion(prozessbaukastenInvalid);
        Assertions.assertNotEquals(prozessbaukastenInvalid.getVersion() + 1, prozessbaukastenVersionCopy.getVersion());
    }

    @Test
    public void createCopyForNewVersionTestForNullProzessbaukasten() {
        prozessbaukastenVersionCopy = prozessbaukastenService.createCopyForNewVersion(null);
        Assertions.assertEquals("", prozessbaukastenVersionCopy.getFachId());
    }

}
