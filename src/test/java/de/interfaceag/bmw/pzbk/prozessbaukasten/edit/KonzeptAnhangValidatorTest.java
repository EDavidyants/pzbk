package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.faces.validator.ValidatorException;

import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@RunWith(MockitoJUnitRunner.class)
public class KonzeptAnhangValidatorTest {

    @Mock
    private Konzept konzept;

    @Mock
    private Anhang anhang;

    private KonzeptAnhangValidator konzeptAnhangValidator;

    @Before
    public void setup() {
        this.konzeptAnhangValidator = new KonzeptAnhangValidator(konzept);
    }

    @Test(expected = ValidatorException.class)
    public void testValidateFail() {
        when(konzept.getAnhang()).thenReturn(null);
        konzeptAnhangValidator.validate(null, null, null);
    }

    @Test
    public void testValidateSuccessful() {
        when(konzept.getAnhang()).thenReturn(anhang);
        konzeptAnhangValidator.validate(null, null, null);
    }

}
