package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.entities.Konzept;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author sl
 */
public class KonzeptRemoverTest {

    @Mock
    private Konzept konzept;

    @Mock
    private Konzept anotherKonzept;

    private List<Konzept> konzepte;

    private KonzeptRemover konzeptRemover;

    @Before
    public void setUp() {
        konzepte = new ArrayList<>();
        konzepte.add(konzept);
        konzepte.add(anotherKonzept);
        konzeptRemover = new KonzeptRemover(konzepte, konzept);
    }

    @Test
    public void testInitial() {
        assertEquals(2, konzepte.size());
    }

    @Test
    public void testRemove() {
        konzeptRemover.remove();
        assertEquals(1, konzepte.size());
    }

}
