package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author sl
 */
public class ProzessbaukastenStatusAngelegtDisabledAttributesTest implements ProzessbaukastenDisabledAttributesTest {

    private final ProzessbaukastenDisabledAttributes disabledAttributes = new ProzessbaukastenStatusAngelegtDisabledAttributes();

    @Test
    @Override
    public void testIsBezeichnungDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isBezeichnungDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsBeschreibungDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isBeschreibungDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsTteamDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isTteamDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsLeadTechnologieDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isLeadTechnologieDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsKonzeptDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isKonzeptDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsStandardisierterFertigungsprozessDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isStandardisierterFertigungsprozessDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsErstanlaeuferDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isErstanlaeuferDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnhangStartbriefDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isAnhangStartbriefDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnhangGrafikUmfangDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isAnhangGrafikUmfangDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnhangGrafikKonzeptbaumDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isAnhangGrafikKonzeptbaumDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnhangBeauftragungTPKDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isAnhangBeauftragungTPKDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnhangGenehmigungTPKDisabled() {
        boolean expected = true;
        boolean result = disabledAttributes.isAnhangGenehmigungTPKDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsWeitereAnhangeDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isWeitereAnhangeDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsStammdatenDetailsPanelDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isStammdatenDetailsPanelDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnforderungenTabDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isAnforderungenTabDisabled();
        assertEquals(expected, result);
    }

    @Test
    @Override
    public void testIsAnhangeTabDisabled() {
        boolean expected = false;
        boolean result = disabledAttributes.isAnhangeTabDisabled();
        assertEquals(expected, result);
    }

}
