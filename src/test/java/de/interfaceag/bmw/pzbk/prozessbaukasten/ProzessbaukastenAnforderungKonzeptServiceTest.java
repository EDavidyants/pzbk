package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenAnforderungKonzept;
import de.interfaceag.bmw.pzbk.prozessbaukasten.edit.ProzessbaukastenAnforderungEdit;
import de.interfaceag.bmw.pzbk.prozessbaukasten.history.ProzessbaukastenHistoryService;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProzessbaukastenAnforderungKonzeptServiceTest {

    @InjectMocks
    private ProzessbaukastenAnforderungKonzeptService service;
    @Mock
    private ProzessbaukastenAnforderungKonzeptDao dao;
    @Spy
    private final List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte = new ArrayList<>();
    @Mock
    private ProzessbaukastenAnforderungKonzept anforderungKonzept;
    @Mock
    private Prozessbaukasten prozessbaukasten;
    @Spy
    private final List<ProzessbaukastenAnforderungEdit> anforderungDtos = new ArrayList<>();
    @Mock
    private ProzessbaukastenAnforderungEdit anforderungEdit;
    @Spy
    private final List<Anforderung> anforderungen = new ArrayList<>();
    @Mock
    private Anforderung anforderung;
    @Mock
    private Konzept konzept;
    private final List<Konzept> konzepte = new ArrayList<>();

    @Mock
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;
    @Mock
    private Session session;

    @BeforeEach
    public void setUp() {
        anforderungDtos.add(anforderungEdit);
        anforderungen.add(anforderung);
        anforderungKonzepte.add(anforderungKonzept);
        konzepte.add(konzept);
    }

    @Test
    public void testUpdateAnforderungKonzeptCreate() throws Exception {
        when(prozessbaukasten.getAnforderungen()).thenReturn(anforderungen);
        when(anforderungEdit.getId()).thenReturn("1");
        when(anforderungEdit.getAnforderungKonzept()).thenReturn(konzepte);
        when(anforderung.getId()).thenReturn(1L);
        anforderungKonzepte.clear();

        service.updateAnforderungKonzept(prozessbaukasten, anforderungDtos);

        verify(anforderungKonzept, never()).setKonzept(konzept);
        verify(dao, times(1)).save(any());
        verify(dao, never()).remove(any());
    }

    @Test
    public void testUpdateAnforderungKonzeptRemove() throws Exception {
        when(prozessbaukasten.getAnforderungen()).thenReturn(anforderungen);
        when(anforderungEdit.getId()).thenReturn("1");
        when(anforderung.getId()).thenReturn(1L);
        when(anforderungEdit.getAnforderungKonzept()).thenReturn(Collections.EMPTY_LIST);
        when(dao.getByProzessbaukastenId(anyLong())).thenReturn(anforderungKonzepte);
        when(anforderungKonzept.getAnforderung()).thenReturn(anforderung);

        service.updateAnforderungKonzept(prozessbaukasten, anforderungDtos);

        verify(dao, never()).save(any());
        verify(dao, times(1)).remove(any());
    }

    @Test
    public void testGetByProzessbaukastenId() {
        when(dao.getByProzessbaukastenId(anyLong())).thenReturn(anforderungKonzepte);
        List<ProzessbaukastenAnforderungKonzept> result = service.getByProzessbaukastenId(0L);
        Assertions.assertEquals(anforderungKonzepte, result);
    }

    @Test
    public void testGetByProzessbaukastenIdNull() {
        List<ProzessbaukastenAnforderungKonzept> result = service.getByProzessbaukastenId(null);
        Assertions.assertEquals(Collections.EMPTY_LIST, result);
    }

    @Test
    public void testGetByAnforderungId() {
        when(dao.getByAnforderungId(anyLong())).thenReturn(anforderungKonzepte);
        List<ProzessbaukastenAnforderungKonzept> result = service.getByAnforderungId(0);
        Assertions.assertEquals(anforderungKonzepte, result);
    }

}
