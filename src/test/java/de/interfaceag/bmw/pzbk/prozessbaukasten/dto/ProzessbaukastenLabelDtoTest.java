package de.interfaceag.bmw.pzbk.prozessbaukasten.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProzessbaukastenLabelDtoTest {

    private ProzessbaukastenLabelDto prozessbaukastenLabelDto;

    @BeforeEach
    void setUp() {
        prozessbaukastenLabelDto = new ProzessbaukastenLabelDto(1L, "P123", 42);
    }

    @Test
    void getFachId() {
        assertEquals("P123", prozessbaukastenLabelDto.getFachId());
    }

    @Test
    void getVersion() {
        assertEquals(42, prozessbaukastenLabelDto.getVersion());
    }

    @Test
    void getId() {
        assertEquals(1L, prozessbaukastenLabelDto.getId());
    }

    @Test
    void getLabelString() {
        assertEquals("P123 | V42", prozessbaukastenLabelDto.getLabelString());
    }
}