package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenAnforderungKonzept;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ProzessbaukastenAnforderungKonzepteForNewVersionTest {

    @Mock
    private ProzessbaukastenAnforderungKonzeptDao anforderungKonzeptDao;

    @InjectMocks
    private final ProzessbaukastenAnforderungKonzeptService service = new ProzessbaukastenAnforderungKonzeptService();

    private Prozessbaukasten prozessbaukastenOrigin;
    private Prozessbaukasten prozessbaukastenNewVersion;
    private Anforderung anforderungOne;
    private Anforderung anforderungTwo;
    private Konzept konzeptOne;
    private Konzept konzeptTwo;
    private Konzept konzeptThree;

    private ProzessbaukastenAnforderungKonzept anforderungKonzeptOne;
    private ProzessbaukastenAnforderungKonzept anforderungKonzeptTwo;
    private ProzessbaukastenAnforderungKonzept anforderungKonzeptThree;

    @BeforeEach
    public void setUp() {
        prozessbaukastenOrigin = TestDataFactory.generateProzessbaukaten("P123", 1);
        prozessbaukastenOrigin.setId(123L);

        prozessbaukastenNewVersion = TestDataFactory.generateProzessbaukaten("P123", 2);
        prozessbaukastenNewVersion.setId(456L);

        anforderungOne = TestDataFactory.generateAnforderung();
        anforderungOne.setId(1L);
        anforderungOne.setFachId("A1");
        anforderungOne.setVersion(1);

        anforderungTwo = TestDataFactory.generateAnforderung();
        anforderungTwo.setId(2L);
        anforderungTwo.setFachId("A2");
        anforderungTwo.setVersion(1);

        List<Anforderung> prozessbaukastenAnforderungen = new ArrayList<>();
        prozessbaukastenOrigin.setAnforderungen(prozessbaukastenAnforderungen);

        konzeptOne = new Konzept("K1", "Konzept1", null);
        konzeptOne.setId(3L);
        konzeptTwo = new Konzept("K2", "Konzept2", null);
        konzeptTwo.setId(4L);
        konzeptThree = new Konzept("K3", "Konzept3", null);
        konzeptThree.setId(5L);
        List<Konzept> konzepteOrigin = Arrays.asList(konzeptOne, konzeptTwo, konzeptThree);

        List<Konzept> konzepteNewVersion = new ArrayList<>();
        konzepteOrigin.forEach(konzept -> {
            konzepteNewVersion.add(konzept.getCopy());
        });

        prozessbaukastenNewVersion.setKonzepte(konzepteNewVersion);

        anforderungKonzeptOne = new ProzessbaukastenAnforderungKonzept(anforderungOne, konzeptOne, prozessbaukastenOrigin);
        anforderungKonzeptTwo = new ProzessbaukastenAnforderungKonzept(anforderungOne, konzeptTwo, prozessbaukastenOrigin);
        anforderungKonzeptThree = new ProzessbaukastenAnforderungKonzept(anforderungTwo, konzeptThree, prozessbaukastenOrigin);
        List<ProzessbaukastenAnforderungKonzept> anforderungKonzepte = Arrays.asList(anforderungKonzeptOne, anforderungKonzeptTwo, anforderungKonzeptThree);

        when(anforderungKonzeptDao.getByProzessbaukastenId(123L)).thenReturn(anforderungKonzepte);
    }

    @Test
    public void testAnforderungKonzepteSize() {
        List<ProzessbaukastenAnforderungKonzept> newVersionAnforderungKonzepte = service.generateAnforderungKonzepteForNewVersion(prozessbaukastenNewVersion, prozessbaukastenOrigin);
        Assertions.assertEquals(3, newVersionAnforderungKonzepte.size());
    }

    @Test
    public void testTwoAnforderungKonzepteSize() {
        List<ProzessbaukastenAnforderungKonzept> newVersionAnforderungKonzepte = service.generateAnforderungKonzepteForNewVersion(prozessbaukastenNewVersion, prozessbaukastenOrigin);
        Assertions.assertEquals(anforderungKonzeptDao.getByProzessbaukastenId(prozessbaukastenOrigin.getId()).size(), newVersionAnforderungKonzepte.size());
    }

    @Test
    public void testAnforderungenNumberInAnforderungKonzepte() {
        List<ProzessbaukastenAnforderungKonzept> newVersionAnforderungKonzepte = service.generateAnforderungKonzepteForNewVersion(prozessbaukastenNewVersion, prozessbaukastenOrigin);

        Map<Anforderung, List<ProzessbaukastenAnforderungKonzept>> anforderungMapping = newVersionAnforderungKonzepte.stream().collect(Collectors.groupingBy(ProzessbaukastenAnforderungKonzept::getAnforderung));
        Set<Anforderung> anforderungen = anforderungMapping.keySet();
        Assertions.assertEquals(2, anforderungen.size());
    }

    @Test
    public void testAnforderungOneInAnforderungKonzepte() {
        List<ProzessbaukastenAnforderungKonzept> newVersionAnforderungKonzepte = service.generateAnforderungKonzepteForNewVersion(prozessbaukastenNewVersion, prozessbaukastenOrigin);

        Map<Anforderung, List<ProzessbaukastenAnforderungKonzept>> anforderungMapping = newVersionAnforderungKonzepte.stream().collect(Collectors.groupingBy(ProzessbaukastenAnforderungKonzept::getAnforderung));
        Set<Anforderung> anforderungen = anforderungMapping.keySet();
        Assertions.assertTrue(anforderungen.contains(anforderungOne));
    }

    @Test
    public void testAnforderungTwoInAnforderungKonzepte() {
        List<ProzessbaukastenAnforderungKonzept> newVersionAnforderungKonzepte = service.generateAnforderungKonzepteForNewVersion(prozessbaukastenNewVersion, prozessbaukastenOrigin);

        Map<Anforderung, List<ProzessbaukastenAnforderungKonzept>> anforderungMapping = newVersionAnforderungKonzepte.stream().collect(Collectors.groupingBy(ProzessbaukastenAnforderungKonzept::getAnforderung));
        Set<Anforderung> anforderungen = anforderungMapping.keySet();
        Assertions.assertTrue(anforderungen.contains(anforderungTwo));
    }

    @Test
    public void testKonzepteNumberByAnforderungOneInAnforderungKonzepte() {
        List<ProzessbaukastenAnforderungKonzept> newVersionAnforderungKonzepte = service.generateAnforderungKonzepteForNewVersion(prozessbaukastenNewVersion, prozessbaukastenOrigin);

        Map<Anforderung, List<ProzessbaukastenAnforderungKonzept>> anforderungMapping = newVersionAnforderungKonzepte.stream().collect(Collectors.groupingBy(ProzessbaukastenAnforderungKonzept::getAnforderung));
        List<ProzessbaukastenAnforderungKonzept> konzepte = anforderungMapping.get(anforderungOne);
        Assertions.assertEquals(2, konzepte.size());
    }

    @Test
    public void testKonzepteNumberByAnforderungTwoInAnforderungKonzepte() {
        List<ProzessbaukastenAnforderungKonzept> newVersionAnforderungKonzepte = service.generateAnforderungKonzepteForNewVersion(prozessbaukastenNewVersion, prozessbaukastenOrigin);

        Map<Anforderung, List<ProzessbaukastenAnforderungKonzept>> anforderungMapping = newVersionAnforderungKonzepte.stream().collect(Collectors.groupingBy(ProzessbaukastenAnforderungKonzept::getAnforderung));
        List<ProzessbaukastenAnforderungKonzept> konzepte = anforderungMapping.get(anforderungTwo);
        Assertions.assertEquals(1, konzepte.size());
    }

    @Test
    public void testKonzeptOneByAnforderungOneInAnforderungKonzepte() {
        List<ProzessbaukastenAnforderungKonzept> newVersionAnforderungKonzepte = service.generateAnforderungKonzepteForNewVersion(prozessbaukastenNewVersion, prozessbaukastenOrigin);

        Map<Anforderung, List<ProzessbaukastenAnforderungKonzept>> anforderungMapping = newVersionAnforderungKonzepte.stream().collect(Collectors.groupingBy(ProzessbaukastenAnforderungKonzept::getAnforderung));
        List<ProzessbaukastenAnforderungKonzept> konzepte = anforderungMapping.get(anforderungOne);

        boolean isPresent = konzepte.stream()
                .filter(konzept -> konzept.getKonzept().getBezeichnung().equals("K1")
                && konzept.getKonzept().getBeschreibung().equals("Konzept1"))
                .findAny().isPresent();

        Assertions.assertTrue(isPresent);
    }

    @Test
    public void testKonzeptTwoByAnforderungOneInAnforderungKonzepte() {
        List<ProzessbaukastenAnforderungKonzept> newVersionAnforderungKonzepte = service.generateAnforderungKonzepteForNewVersion(prozessbaukastenNewVersion, prozessbaukastenOrigin);

        Map<Anforderung, List<ProzessbaukastenAnforderungKonzept>> anforderungMapping = newVersionAnforderungKonzepte.stream().collect(Collectors.groupingBy(ProzessbaukastenAnforderungKonzept::getAnforderung));
        List<ProzessbaukastenAnforderungKonzept> konzepte = anforderungMapping.get(anforderungOne);

        boolean isPresent = konzepte.stream()
                .filter(konzept -> konzept.getKonzept().getBezeichnung().equals("K2")
                && konzept.getKonzept().getBeschreibung().equals("Konzept2"))
                .findAny().isPresent();

        Assertions.assertTrue(isPresent);
    }

    @Test
    public void testKonzeptThreeByAnforderungTwoInAnforderungKonzepte() {
        List<ProzessbaukastenAnforderungKonzept> newVersionAnforderungKonzepte = service.generateAnforderungKonzepteForNewVersion(prozessbaukastenNewVersion, prozessbaukastenOrigin);

        Map<Anforderung, List<ProzessbaukastenAnforderungKonzept>> anforderungMapping = newVersionAnforderungKonzepte.stream().collect(Collectors.groupingBy(ProzessbaukastenAnforderungKonzept::getAnforderung));
        List<ProzessbaukastenAnforderungKonzept> konzepte = anforderungMapping.get(anforderungTwo);

        boolean isPresent = konzepte.stream()
                .filter(konzept -> konzept.getKonzept().getBezeichnung().equals("K3")
                && konzept.getKonzept().getBeschreibung().equals("Konzept3"))
                .findAny().isPresent();

        Assertions.assertTrue(isPresent);
    }

}
