package de.interfaceag.bmw.pzbk.prozessbaukasten.edit;

import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamMitgliedBerechtigung;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author sl
 */
public class ProzessbaukastenEditViewPermissionTest {

    private final Prozessbaukasten prozessbaukasten = TestDataFactory.generateProzessbaukasten();
    private final Tteam tteam = TestDataFactory.generateTteam();
    private final Collection<Long> tteamIds = Arrays.asList(1L);

    @BeforeEach
    public void setup() {
        tteam.setId(1L);
        prozessbaukasten.setTteam(tteam);
    }

    @Test
    public void testDenied() {
        ProzessbaukastenEditViewPermission deniedPermission = ProzessbaukastenEditViewPermission.denied();
        assertFalse(deniedPermission.getPage());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testPagePermissionForRole(Rolle role) {
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        ProzessbaukastenEditViewPermission viewPermission = getViewPermissionForRole(role, prozessbaukasten, tteamIds, tteamMitgliedBerechtigung);
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case TTEAMMITGLIED:
                assertTrue(viewPermission.getPage());
                break;
            default:
                assertFalse(viewPermission.getPage());
                break;
        }
    }

    private ProzessbaukastenEditViewPermission getViewPermissionForRole(Rolle role,
            Prozessbaukasten prozessbaukasten, Collection<Long> tteamIds, TteamMitgliedBerechtigung tteamMitgliedBerechtigung) {
        Set<Rolle> userRoles = new HashSet<>();

        userRoles.add(role);
        return ProzessbaukastenEditViewPermission.builder()
                .forProzessbaukaten(prozessbaukasten)
                .forTteamIdsWithWritePermissionAsTteamLeiter(tteamIds)
                .forUserRoles(userRoles)
                .forTteamMitgliedBerechtigung(tteamMitgliedBerechtigung).build();
    }

}
