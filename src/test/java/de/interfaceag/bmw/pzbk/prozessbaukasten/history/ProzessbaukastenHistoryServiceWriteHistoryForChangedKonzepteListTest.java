package de.interfaceag.bmw.pzbk.prozessbaukasten.history;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenAnforderungKonzept;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenHistory;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenHistoryBuilder;
import de.interfaceag.bmw.pzbk.prozessbaukasten.konzepte.ProzessbaukastenKonzeptHistoryService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ProzessbaukastenHistoryServiceWriteHistoryForChangedKonzepteListTest {

    @Mock
    private ProzessbaukastenHistoryDao prozessbaukastenHistoryDao;

    @Mock
    private ProzessbaukastenKonzeptHistoryService prozessbaukastenKonzeptHistoryService;

    @Mock
    private Date currentDate;

    @InjectMocks
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;

    private Prozessbaukasten prozessbaukastenOld;
    private Prozessbaukasten prozessbaukastenNew;
    private Mitarbeiter user;
    private Konzept konzept;
    private List<Konzept> konzepte;
    private Anforderung anforderung;
    private ProzessbaukastenAnforderungKonzept prozessbaukastenAnforderungKonzept;
    private TestDataFactory testDataFactory;

    @BeforeEach
    public void setUp() {
        testDataFactory = new TestDataFactory();
        user = TestDataFactory.generateMitarbeiter("Max", "Admin", "Abt.-2");
        anforderung = TestDataFactory.generateAnforderung();
    }

    @Test
    public void testNoChangesToKonzeptList() {
        prozessbaukastenOld = generateProzessbaukasten();
        prozessbaukastenNew = generateProzessbaukasten();

        prozessbaukastenHistoryService.writeHistoryForChangedProzessbaukasten(user, prozessbaukastenNew, prozessbaukastenOld);
        verify(prozessbaukastenHistoryDao, never()).save(any(ProzessbaukastenHistory.class));
    }

    private Prozessbaukasten generateProzessbaukasten() {
        Prozessbaukasten prozessbaukasten = new Prozessbaukasten("P456", 1);
        prozessbaukasten.setId(456L);
        prozessbaukasten.setBezeichnung("Prozessbaukasten 456");
        prozessbaukasten.setBeschreibung("Beschreibung");
        prozessbaukasten.setKonzepte(generateKonzepte());
        return prozessbaukasten;
    }

    private List<Konzept> generateKonzepte() {
        konzepte = new ArrayList<>();
        konzept = testDataFactory.generateKonzeptWithAnhang("K123", "Konzept One", "tukan.jpg");
        konzept.setId(123L);
        Konzept konzeptTwo = testDataFactory.generateKonzeptWithAnhang("K456", "Konzept Two", "Kingfisher.jpg");
        konzeptTwo.setId(456L);
        konzepte.add(konzept);
        konzepte.add(konzeptTwo);
        return konzepte;
    }

    @Test
    public void testWriteHistoryForAddedAnforderungKonzept() {
        Prozessbaukasten prozessbaukasten = new Prozessbaukasten("P456", 1);
        konzept = testDataFactory.generateKonzeptWithAnhang("K123", "Konzept One", "tukan.jpg");
        prozessbaukastenAnforderungKonzept = new ProzessbaukastenAnforderungKonzept(anforderung, konzept, prozessbaukasten);
        prozessbaukastenHistoryService.writeHistoryForAddedAnforderungKonzept(prozessbaukastenAnforderungKonzept, user);
        String fieldName = "A42 | V1 Konzept Zuordnung";
        String oldFieldValue = "Konzept One";
        String newFieldValue = "zugeordnet";
        ProzessbaukastenHistory history = new ProzessbaukastenHistoryBuilder().setProzessbaukasten(prozessbaukasten).setDatum(currentDate).setBenutzer("Max Admin | Abt.-2").setChangedField(fieldName).setVon(oldFieldValue).setAuf(newFieldValue).build();
        verify(prozessbaukastenHistoryDao).save(history);
    }

    @Test
    public void testWriteHistoryForRemovedAnforderungKonzept() {
        Prozessbaukasten prozessbaukasten = new Prozessbaukasten("P456", 1);
        konzept = testDataFactory.generateKonzeptWithAnhang("K123", "Konzept One", "tukan.jpg");
        prozessbaukastenAnforderungKonzept = new ProzessbaukastenAnforderungKonzept(anforderung, konzept, prozessbaukasten);
        prozessbaukastenHistoryService.writeHistoryForRemovedAnforderungKonzept(prozessbaukastenAnforderungKonzept, user);
        String fieldName = "A42 | V1 Konzept Zuordnung";
        String oldFieldValue = "Konzept One";
        String newFieldValue = "entfernt";
        ProzessbaukastenHistory history = new ProzessbaukastenHistoryBuilder().setProzessbaukasten(prozessbaukasten).setDatum(currentDate).setBenutzer("Max Admin | Abt.-2").setChangedField(fieldName).setVon(oldFieldValue).setAuf(newFieldValue).build();
        verify(prozessbaukastenHistoryDao).save(history);
    }

}
