package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes;

/**
 *
 * @author sl
 */
public interface ProzessbaukastenDisabledAttributesTest {

    void testIsAnforderungenTabDisabled();

    void testIsAnhangBeauftragungTPKDisabled();

    void testIsAnhangGenehmigungTPKDisabled();

    void testIsAnhangGrafikKonzeptbaumDisabled();

    void testIsAnhangGrafikUmfangDisabled();

    void testIsAnhangStartbriefDisabled();

    void testIsAnhangeTabDisabled();

    void testIsBeschreibungDisabled();

    void testIsBezeichnungDisabled();

    void testIsErstanlaeuferDisabled();

    void testIsKonzeptDisabled();

    void testIsLeadTechnologieDisabled();

    void testIsStammdatenDetailsPanelDisabled();

    void testIsStandardisierterFertigungsprozessDisabled();

    void testIsTteamDisabled();

    void testIsWeitereAnhangeDisabled();

}
