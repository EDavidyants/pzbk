package de.interfaceag.bmw.pzbk.prozessbaukasten.edit.attributes;

import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

/**
 *
 * @author sl
 */
public class ProzessbaukastenDisabledAttributesFactoryTest {

    @ParameterizedTest
    @EnumSource(ProzessbaukastenStatus.class)
    public void testGetDisabledAttributesForProzessbaukastenStatus(ProzessbaukastenStatus status) {
        ProzessbaukastenDisabledAttributes result = ProzessbaukastenDisabledAttributesFactory.getDisabledAttributesForProzessbaukasten(status, true);
        Class expected;
        switch (status) {
            case BEAUFTRAGT:
                expected = ProzessbaukastenStatusBeauftragtDisabledAttributes.class;
                break;
            case GUELTIG:
                expected = ProzessbaukastenStatusGultigDisabledAttributes.class;
                break;
            case ABGEBROCHEN:
            case GELOESCHT:
            case STILLGELEGT:
                expected = ProzessbaukastenAllDisabledAttributes.class;
                break;
            case ANGELEGT:
                expected = ProzessbaukastenStatusAngelegtDisabledAttributes.class;
                break;
            default:
                expected = ProzessbaukastenNeuDisabledAttributes.class;
                break;
        }
        Assertions.assertEquals(expected, result.getClass());
    }

}
