package de.interfaceag.bmw.pzbk.prozessbaukasten.view;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungEditDtoImpl;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamVertreterBerechtigung;
import de.interfaceag.bmw.pzbk.berechtigung.dto.UserPermissionsDTO;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@RunWith(MockitoJUnitRunner.class)
public class NewVersionAndStatusChangeButtonsForTteamVertreterTest {

    @Mock
    private Session session;

    @InjectMocks
    private final ProzessbaukastenViewFacade facade = new ProzessbaukastenViewFacade();

    private Tteam tteamAF;
    private Tteam tteamVerdeck;

    private Prozessbaukasten prozessbaukastenAF;
    private Prozessbaukasten prozessbaukastenVerdeck;

    private Class<?>[] params;
    private Method getViewPermissionMethod;

    @Before
    public void setUp() {
        tteamAF = new Tteam("AF");
        tteamAF.setId(1L);

        tteamVerdeck = new Tteam("Verdeck");
        tteamVerdeck.setId(2L);

        prozessbaukastenAF = TestDataFactory.generateProzessbaukasten();
        prozessbaukastenAF.setTteam(tteamAF);

        prozessbaukastenVerdeck = TestDataFactory.generateProzessbaukasten();
        prozessbaukastenVerdeck.setTteam(tteamVerdeck);

        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(Rolle.TTEAM_VERTRETER);

        params = new Class<?>[]{Prozessbaukasten.class};

        try {
            getViewPermissionMethod = facade.getClass().getDeclaredMethod("getViewPermission", params);
            getViewPermissionMethod.setAccessible(true);

        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(NewVersionAndStatusChangeButtonsForTteamVertreterTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<BerechtigungEditDtoImpl> berechtigungen = new ArrayList<>();
        List<BerechtigungEditDtoImpl> tteamBerechtigungen = Arrays.asList(new BerechtigungEditDtoImpl(1L, "AF", BerechtigungZiel.TTEAM, Rechttype.SCHREIBRECHT, Rolle.TTEAM_VERTRETER));
        berechtigungen.addAll(tteamBerechtigungen);

        List<Rolle> userRoleList = new ArrayList<>();
        userRoleList.addAll(userRoles);
        TteamVertreterBerechtigung tteamVertreterberechtigung = new TteamVertreterBerechtigung(Arrays.asList(1L));

        UserPermissionsDTO userPermission = new UserPermissionsDTO<>(userRoleList, berechtigungen, null, tteamVertreterberechtigung);
        when(session.getUserPermissions()).thenReturn(userPermission);

    }

    @Test
    public void testStatusChangeButtonViewPermissionPositive() {

        Object[] prozessbaukasten = new Object[]{prozessbaukastenAF};

        try {
            ProzessbaukastenViewPermission viewPermission = (ProzessbaukastenViewPermission) getViewPermissionMethod.invoke(facade, prozessbaukasten);
            boolean isStatusChangeButtonVisible = viewPermission.getStatusChangeButton();
            Assertions.assertTrue(isStatusChangeButtonVisible, "Fehler: Status Change Button ist nicht sichtbar fuer Tteam-Vertreter");

        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(NewVersionAndStatusChangeButtonsForTteamVertreterTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void testStatusChangeButtonViewPermissionNegative() {

        Object[] prozessbaukasten = new Object[]{prozessbaukastenVerdeck};

        try {
            ProzessbaukastenViewPermission viewPermission = (ProzessbaukastenViewPermission) getViewPermissionMethod.invoke(facade, prozessbaukasten);
            boolean isStatusChangeButtonVisible = viewPermission.getStatusChangeButton();
            Assertions.assertFalse(isStatusChangeButtonVisible, "Fehler: Status Change Button ist sichtbar fuer unberechtigte user");

        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(NewVersionAndStatusChangeButtonsForTteamVertreterTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void testNewVersionButtonViewPermissionPositive() {

        Object[] prozessbaukasten = new Object[]{prozessbaukastenAF};

        try {
            ProzessbaukastenViewPermission viewPermission = (ProzessbaukastenViewPermission) getViewPermissionMethod.invoke(facade, prozessbaukasten);
            boolean isStatusChangeButtonVisible = viewPermission.getNewVersionButton();
            Assertions.assertTrue(isStatusChangeButtonVisible, "Fehler: New Version Button ist nicht sichtbar fuer Tteam-Vertreter");

        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(NewVersionAndStatusChangeButtonsForTteamVertreterTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void testNewVersionButtonViewPermissionNegative() {

        Object[] prozessbaukasten = new Object[]{prozessbaukastenVerdeck};

        try {
            ProzessbaukastenViewPermission viewPermission = (ProzessbaukastenViewPermission) getViewPermissionMethod.invoke(facade, prozessbaukasten);
            boolean isStatusChangeButtonVisible = viewPermission.getNewVersionButton();
            Assertions.assertFalse(isStatusChangeButtonVisible, "Fehler: New Version Button ist sichtbar fuer unberechtigte user");

        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(NewVersionAndStatusChangeButtonsForTteamVertreterTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
