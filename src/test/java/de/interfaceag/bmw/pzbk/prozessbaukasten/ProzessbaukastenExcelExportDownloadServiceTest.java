package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ProzessbaukastenExcelExportDownloadServiceTest {

    private static final String ID = "Id";
    private static final String BESCHREIBUNG = "Beschreibung";
    private static final String SENSOR_COC = "SensorCoc";
    private static final String UMSETZER = "Umsetzer";
    private static final String KONZEPT = "Konzept";
    private static final String THEMENKLAMMER = "Themenklammer";

    @Mock
    private ExcelDownloadService excelDownloadService;
    @InjectMocks
    private ProzessbaukastenExcelExportDownloadService prozessbaukastenExcelExportDownloadService;

    private ProzessbaukastenAnforderung prozessbaukastenAnforderung;
    private List<ProzessbaukastenAnforderung> anforderungen;

    @BeforeEach
    void setUp() {
        prozessbaukastenAnforderung = getProzessbaukastenAnforderung();
        anforderungen = Collections.singletonList(prozessbaukastenAnforderung);
    }

    private ProzessbaukastenAnforderung getProzessbaukastenAnforderung() {
        return AnforderungenTabDTO.getBuilder()
                .withId(ID)
                .withFachId("A12")
                .withVersion("3")
                .withBeschreibung(BESCHREIBUNG)
                .withSensorCoc(SENSOR_COC)
                .withUmsetzer(UMSETZER)
                .withKonzept(KONZEPT)
                .withThemenklammer(THEMENKLAMMER)
                .withStatus(Status.A_GENEHMIGT_IN_PROZESSBAUKASTEN)
                .build();
    }

    @Test
    void downloadExcelExport() {
        prozessbaukastenExcelExportDownloadService.downloadExcelExport("fileName", anforderungen);
        verify(excelDownloadService, times(1)).downloadExcelExport(any(), any());
    }
}