package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@RunWith(MockitoJUnitRunner.class)
public class ActiveIndexTest {

    @Mock
    private UrlParameter urlParameter;

    @Before
    public void setUp() {
        when(urlParameter.getValue(any())).thenReturn(Optional.of("1"));
    }

    @ParameterizedTest
    @EnumSource(ProzessbaukastenTab.class)
    public void testForTab(ProzessbaukastenTab tab) {
        ActiveIndex activeIndex = ActiveIndex.forTab(tab);
        assertEquals(tab.getIndex(), activeIndex.getIndex());
    }

    @Test
    public void testForUrlParameter() {
        ActiveIndex activeIndex = ActiveIndex.forUrlParameter(urlParameter);
        assertEquals(1, (int) activeIndex.getIndex());
    }

}
