package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungStatusChangeService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.Kategorie;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.history.ProzessbaukastenHistoryService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.AnforderungReportingStatusTransitionAdapter;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ProzessbaukastenRemoveAnforderungZuordnungServiceTest {

    @Mock
    private AnforderungReportingStatusTransitionAdapter anforderungReportingStatusTransitionAdapter;
    @Mock
    private AnforderungStatusChangeService anforderungStatusChangeSerivce;
    @Mock
    private ProzessbaukastenService prozessbaukastenService;
    @Mock
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;
    @Mock
    private Mitarbeiter currentUser;

    @InjectMocks
    private ProzessbaukastenRemoveAnforderungZuordnungService removeAnforderungZuordnungService;


    private Prozessbaukasten prozessbaukasten;
    private Prozessbaukasten prozessbaukasten2;
    private Anforderung anforderung;

    @BeforeEach
    void setUp() {
        anforderung = TestDataFactory.generateAnforderung();
        anforderung.setKategorie(Kategorie.EINS);
        prozessbaukasten = TestDataFactory.generateProzessbaukasten();
        prozessbaukasten2 = TestDataFactory.generateProzessbaukasten();
        prozessbaukasten2.setFachId("Was anderes");
        anforderung.getProzessbaukasten().add(prozessbaukasten);
        anforderung.getProzessbaukasten().add(prozessbaukasten2);
        prozessbaukasten.getAnforderungen().add(anforderung);
    }

    @Test
    void unassignAnforderungFromProzessbaukastenKategorieBefore() {
        final Kategorie kategorie = anforderung.getKategorie();
        assertThat(kategorie, is(Kategorie.EINS));
    }

    @Test
    void unassignAnforderungFromProzessbaukastenNewKategorie() {
        removeAnforderungZuordnungService.unassignAnforderungFromProzessbaukasten(anforderung, prozessbaukasten, "");
        final Kategorie kategorie = anforderung.getKategorie();
        assertThat(kategorie, is(Kategorie.EINS));
    }

    @Test
    void unassignAnforderungToProzessbaukastenAnforderungStatusChangeToInArbeitAfterBothRemoved() {
        removeAnforderungZuordnungService.unassignAnforderungFromProzessbaukasten(anforderung, prozessbaukasten, "");
        removeAnforderungZuordnungService.unassignAnforderungFromProzessbaukasten(anforderung, prozessbaukasten2, "");
        verify(anforderungStatusChangeSerivce, times(1)).changeAnforderungStatus(anforderung, Status.A_INARBEIT, "");
    }

    @Test
    void unassignAnforderungToProzessbaukastenAnforderungStatusChangeToInArbeit() {
        removeAnforderungZuordnungService.unassignAnforderungFromProzessbaukasten(anforderung, prozessbaukasten, "");
        verify(anforderungStatusChangeSerivce, never()).changeAnforderungStatus(anforderung, Status.A_INARBEIT, "");
    }

    @Test
    void unassignAnforderungToProzessbaukastenReportingEntry() {
        removeAnforderungZuordnungService.unassignAnforderungFromProzessbaukasten(anforderung, prozessbaukasten, "");
        verify(anforderungReportingStatusTransitionAdapter, times(1)).removeAnforderungFromProzessbaukasten(anforderung, Status.A_INARBEIT);
    }

    @Test
    void unassignAnforderungToProzessbaukastenAnforderungListOfProzessbaukastenBefore() {
        final Set<Prozessbaukasten> prozessbaukasten = anforderung.getProzessbaukasten();
        assertThat(prozessbaukasten, hasSize(2));
    }

    @Test
    void unassignAnforderungToProzessbaukastenAnforderungListOfProzessbaukastenAfter() {
        removeAnforderungZuordnungService.unassignAnforderungFromProzessbaukasten(anforderung, prozessbaukasten, "");
        final Set<Prozessbaukasten> prozessbaukasten = anforderung.getProzessbaukasten();
        assertThat(prozessbaukasten, hasSize(1));
    }

    @Test
    void unassignAnforderungToProzessbaukastenProzessbaukastenListOfAnforderungenBefore() {
        final List<Anforderung> anforderungen = prozessbaukasten.getAnforderungen();
        assertThat(anforderungen, hasSize(2));
    }

    @Test
    void unassignAnforderungToProzessbaukastenProzessbaukastenListOfAnforderungenAfter() {
        removeAnforderungZuordnungService.unassignAnforderungFromProzessbaukasten(anforderung, prozessbaukasten, "");
        final List<Anforderung> anforderungen = prozessbaukasten.getAnforderungen();
        assertThat(anforderungen, hasSize(1));
    }

}