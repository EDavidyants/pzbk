package de.interfaceag.bmw.pzbk.prozessbaukasten;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author sl
 */
public class ProzessbaukastenTabTest {

    @ParameterizedTest
    @ValueSource(ints = {-1, 0, 1, 2, 3, 4, 5})
    public void testGetByIndex(Integer index) {
        Optional<ProzessbaukastenTab> result = ProzessbaukastenTab.getByIndex(index);
        Optional<ProzessbaukastenTab> excepted;
        switch (index) {
            case 0:
                excepted = Optional.of(ProzessbaukastenTab.STAMMDATEN);
                break;
            case 1:
                excepted = Optional.of(ProzessbaukastenTab.ANFORDERUNGEN);
                break;
            case 2:
                excepted = Optional.of(ProzessbaukastenTab.FESTGESCHRIEBENE_GROESSEN);
                break;
            case 3:
                excepted = Optional.of(ProzessbaukastenTab.ANHANGE);
                break;
            case 4:
                excepted = Optional.of(ProzessbaukastenTab.DERIVATE);
                break;
            default:
                excepted = Optional.empty();
                break;
        }
        assertEquals(excepted, result);
    }

}
