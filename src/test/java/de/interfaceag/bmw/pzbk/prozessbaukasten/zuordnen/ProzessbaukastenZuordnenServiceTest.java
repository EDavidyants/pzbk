package de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungStatusChangeService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.AnforderungReportingStatusTransitionAdapter;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author fn
 */
@ExtendWith(MockitoExtension.class)
public class ProzessbaukastenZuordnenServiceTest {

    @Mock
    AnforderungService anforderungService;
    @Mock
    AnforderungMeldungHistoryService anforderungMeldungHistoryService;
    @Mock
    Session session;
    @Mock
    ProzessbaukastenService prozessbaukastenService;
    @Mock
    ProzessbaukastenReadService prozessbaukastenReadService;
    @Mock
    LocalizationService localizationService;
    @Mock
    AnforderungStatusChangeService anforderungStatusChangeService;
    @Mock
    private AnforderungReportingStatusTransitionAdapter anforderungReportingStatusTransitionAdapter;

    @InjectMocks
    ProzessbaukastenZuordnenService prozessbaukastenZuordnenService;

    @Mock
    UserPermissions userPermissions;
    private Mitarbeiter user;

    @BeforeEach
    public void setUp() {
        user = TestDataFactory.generateMitarbeiter("Max", "Admin", "Abr.-2", "q2222222");
    }

    @Test
    public void getProzessbaukastenZuordnenViewDataWithCorrectAnforderungWithoutZuordnung() {

        Anforderung anforderung = TestDataFactory.generateAnforderung();

        when(prozessbaukastenReadService.getAllZuweisbareProzessbaukaesten()).thenReturn(TestDataFactory.generateListOfProzessbaukaesten());

        List<Rolle> roles = new ArrayList<>();
        roles.add(Rolle.ADMIN);
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRollen()).thenReturn(roles);

        ProzessbaukastenZuordnenDialogViewData viewData = prozessbaukastenZuordnenService.getAddProzessbaukastenDialogViewData(anforderung);
        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertNotNull(viewData.getProzessbaukastenChoosable()));

    }

    @Test
    public void getProzessbaukastenZuordnenViewDataWithNullAnforderung() {

        Anforderung anforderung = null;

        ProzessbaukastenZuordnenDialogViewData viewData = prozessbaukastenZuordnenService.getAddProzessbaukastenDialogViewData(anforderung);
        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertNotNull(viewData.getProzessbaukastenChoosable()));

    }

    @Test
    public void getProzessbaukastenZuordnenViewDataWithNullAnforderungID() {

        Anforderung anforderung = TestDataFactory.generateAnforderung();
        anforderung.setId(null);
        ProzessbaukastenZuordnenDialogViewData viewData = prozessbaukastenZuordnenService.getAddProzessbaukastenDialogViewData(anforderung);
        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertNotNull(viewData.getProzessbaukastenChoosable()));

    }

    @Test
    public void getProzessbaukastenZuordnenViewDataWithCorrectAnforderungWithZuordnung() {

        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Set<Prozessbaukasten> prozessbaukaesten = new HashSet<>();
        prozessbaukaesten.add(TestDataFactory.generateProzessbaukasten());

        anforderung.setProzessbaukasten(prozessbaukaesten);
        when(prozessbaukastenZuordnenService.prozessbaukastenReadService.getAllZuweisbareProzessbaukaesten()).thenReturn(TestDataFactory.generateListOfProzessbaukaesten());

        when(session.getUserPermissions()).thenReturn(userPermissions);

        ProzessbaukastenZuordnenDialogViewData viewData = prozessbaukastenZuordnenService.getAddProzessbaukastenDialogViewData(anforderung);
        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertNotNull(viewData.getProzessbaukastenChoosable()));

    }

    @Test
    public void getProzessbaukastenZuordnenViewDataNoChoosableProzessbaukasten() {

        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Set<Prozessbaukasten> prozessbaukaesten = new HashSet<>();
        prozessbaukaesten.add(TestDataFactory.generateProzessbaukasten());
        anforderung.setProzessbaukasten(prozessbaukaesten);

        user = TestDataFactory.generateMitarbeiter("Max", "Admin", "Abr.-2", "q2222222");

        when(prozessbaukastenReadService.getAllZuweisbareProzessbaukaesten()).thenReturn(new ArrayList<>());

        List<Rolle> roles = new ArrayList<>();
        roles.add(Rolle.ADMIN);
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRollen()).thenReturn(roles);

        ProzessbaukastenZuordnenDialogViewData viewData = prozessbaukastenZuordnenService.getAddProzessbaukastenDialogViewData(anforderung);
        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertNotNull(viewData.getProzessbaukastenChoosable()));

    }

    @Test
    public void saveProzessbaukastenZuordnenWithNullProzessbaukastenDTO() {

        when(anforderungService.getAnforderungById(any())).thenReturn(TestDataFactory.generateAnforderung());

        String returnValue = prozessbaukastenZuordnenService.prozessbaukastenZuordnen(null, 2L);

        Assertions.assertEquals(returnValue, "anforderungView.xhtml?fachId=A42&id=2&version=1&faces-redirect=true");

    }

    @Test
    public void prozessbaukastenZuordnenByProzessbaukatenAndAnforderungWithNoValidProzessbaukasten() {

        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Prozessbaukasten prozessbaukasten = null;
        String returnString = prozessbaukastenZuordnenService.prozessbaukastenZuordnenByProzessbaukatenAndAnforderung(prozessbaukasten, anforderung);

        Assertions.assertEquals(returnString, "");

    }

    @Test
    public void prozessbaukastenZuordnenByProzessbaukatenAndAnforderungWithNoValidAnforderung() {

        Anforderung anforderung = null;
        Prozessbaukasten prozessbaukasten = TestDataFactory.generateProzessbaukasten();
        String returnString = prozessbaukastenZuordnenService.prozessbaukastenZuordnenByProzessbaukatenAndAnforderung(prozessbaukasten, anforderung);

        Assertions.assertEquals(returnString, "");

    }

    @MockitoSettings(strictness = Strictness.LENIENT)
    @Test
    public void anforderungenProzessbaukastenZuordnenWithZugeordneterAnforderung() {

        AnforderungenProzessbaukastenZuordnenDTO anforderungDto = new AnforderungenProzessbaukastenZuordnenDTO("A123", 1, "Beschreibung");

        List<AnforderungenProzessbaukastenZuordnenDTO> anforderungProzessbaukastenZuordnenDTOList = new ArrayList<>();
        anforderungProzessbaukastenZuordnenDTOList.add(anforderungDto);

        Anforderung anforderung = new Anforderung("A123", 1);
        anforderung.setStatus(Status.A_KEINE_WEITERVERFOLG);

        Prozessbaukasten prozessbaukasten = new Prozessbaukasten("P776", 1);
        prozessbaukasten.setId(776L);
        ContextMocker.mockFacesContext();
        ContextMocker.mockFacesContext();
        Set<Prozessbaukasten> prozessbaukaesten = new HashSet<>();
        prozessbaukaesten.add(prozessbaukasten);
        anforderung.setProzessbaukasten(prozessbaukaesten);

        when(localizationService.getValue("prozessbaukasten_anforderungZuordnen_nichtMoeglich")).thenReturn("Zuordnung nicht möglich");
        when(localizationService.getValue("prozessbaukasten_anforderungZuordnen_bereitsZugeordnetTitle")).thenReturn("Bereits zugeordnete Anforderungen");
        when(localizationService.getValue("prozessbaukasten_anforderungZuordnenNichtMoeglich_status")).thenReturn("Zuordnung wegen Status der Anforderung nicht möglich");

        when(prozessbaukastenService.createRealCopy(prozessbaukasten)).thenReturn(Optional.of(prozessbaukasten));
        when(anforderungService.getAnforderungByFachIdVersion("A123", 1)).thenReturn(anforderung);
        when(prozessbaukastenService.getProzessbaukastenForAnforderung(anforderung)).thenReturn(prozessbaukasten);

        String returnString = prozessbaukastenZuordnenService.anforderungenProzessbaukastenZuordnen(anforderungProzessbaukastenZuordnenDTOList, prozessbaukasten);

        Assertions.assertEquals("", returnString);

    }

    @Test
    public void anforderungenProzessbaukastenZuordnenWithNullData() {

        List<AnforderungenProzessbaukastenZuordnenDTO> anforderungProzessbaukastenZuordnenDTOList = null;
        Prozessbaukasten prozessbaukasten = null;

        String returnString = prozessbaukastenZuordnenService.anforderungenProzessbaukastenZuordnen(anforderungProzessbaukastenZuordnenDTOList, prozessbaukasten);

        Assertions.assertEquals("", returnString);

    }

    @Test
    public void anforderungenProzessbaukastenZuordnenWithEmptyList() {
        List<AnforderungenProzessbaukastenZuordnenDTO> anforderungProzessbaukastenZuordnenDTOList = new ArrayList<>();
        Prozessbaukasten prozessbaukasten = TestDataFactory.generateProzessbaukasten();
        String returnString = prozessbaukastenZuordnenService.anforderungenProzessbaukastenZuordnen(anforderungProzessbaukastenZuordnenDTOList, prozessbaukasten);
        Assertions.assertEquals("prozessbaukastenView.xhtml?activeIndex=1&fachId=P1234&version=1&faces-redirect=true", returnString);
    }

    @Test
    public void getProzessbaukastenZuordnenViewDataWithCorrectAnforderungWithMultipleZuordnungen() {

        Anforderung anforderung = TestDataFactory.generateAnforderung();

        List<Prozessbaukasten> prozessbaukasten = TestDataFactory.generateListOfProzessbaukaesten();
        Prozessbaukasten prozessbaukastenOne = prozessbaukasten.get(0);
        Prozessbaukasten prozessbaukastenTwo = prozessbaukasten.get(1);
        Set<Prozessbaukasten> zugeordnetenProzessbaukasten = new HashSet<>(Arrays.asList(prozessbaukastenOne, prozessbaukastenTwo));
        anforderung.setProzessbaukasten(zugeordnetenProzessbaukasten);
        prozessbaukasten.remove(prozessbaukastenOne);
        prozessbaukasten.remove(prozessbaukastenTwo);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(prozessbaukastenReadService.getAllZuweisbareProzessbaukaesten()).thenReturn(prozessbaukasten);

        ProzessbaukastenZuordnenDialogViewData viewData = prozessbaukastenZuordnenService.getAddProzessbaukastenDialogViewData(anforderung);
        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertNotNull(viewData.getProzessbaukastenChoosable()));

        Assertions.assertEquals(18, viewData.getProzessbaukastenChoosable().size());
    }

}
