package de.interfaceag.bmw.pzbk.prozessbaukasten.dto;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenDerivat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProzessbaukastenDerivatDtoTest {

    private static final String NAME = "name";
    private LocalDate localDate;
    private Date date;

    @Mock
    Derivat derivat;

    @Mock
    DerivatStatusNames derivatStatusNames;

    List<Derivat> derivate;

    @BeforeEach
    public void setUp() {
        derivate = new ArrayList<>();
        derivate.add(derivat);
    }

    private void setUpMock() {
        when(derivat.getName()).thenReturn(NAME);
        when(derivatStatusNames.getNameForStatus(any())).thenReturn("");
    }

    @Test
    public void testForDerivatName() {
        setUpMock();
        ProzessbaukastenDerivat result = ProzessbaukastenDerivatDto.forDerivat(derivat, derivatStatusNames);
        assertThat(NAME, is(result.getBezeichnung()));
    }

    @Test
    public void testForDerivateSize() {
        setUpMock();
        List<ProzessbaukastenDerivat> result = ProzessbaukastenDerivatDto.forDerivate(derivate, derivatStatusNames);
        assertThat(result, hasSize(1));
    }

    @Test
    public void testForDerivateContentBezeichnung() {
        setUpMock();
        List<ProzessbaukastenDerivat> result = ProzessbaukastenDerivatDto.forDerivate(derivate, derivatStatusNames);
        ProzessbaukastenDerivat derivatResult = result.get(0);
        assertThat(NAME, is(derivatResult.getBezeichnung()));
    }

    @Test
    public void testEmpty() {
        ProzessbaukastenDerivat empty = ProzessbaukastenDerivatDto.empty();
        assertThat("", is(empty.getBezeichnung()));
    }

}
