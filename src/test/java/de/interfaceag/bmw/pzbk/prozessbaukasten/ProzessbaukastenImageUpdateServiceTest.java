package de.interfaceag.bmw.pzbk.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Bild;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.services.ImageService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.exceptions.base.MockitoAssertionError;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@RunWith(MockitoJUnitRunner.class)
public class ProzessbaukastenImageUpdateServiceTest {

    @Mock
    private Anhang anhang;

    @Mock
    private Prozessbaukasten prozessbaukasten;

    @Mock
    private ImageService imageService;

    @Mock
    private Bild bild;

    @InjectMocks
    private final ProzessbaukastenImageUpdateService prozessbaukastenImageUpdateService = new ProzessbaukastenImageUpdateService();

    @Before
    public void setup() {
        when(imageService.generateBildFromAnhang(any())).thenReturn(bild);
    }

    @Test
    public void testGrafikUmfangUpdateGenerateBildFromAnhangCall() {
        when(prozessbaukasten.getGrafikUmfang()).thenReturn(anhang);

        prozessbaukastenImageUpdateService.updateProzessbaukastenBilder(prozessbaukasten);

        verify(imageService, times(1)).generateBildFromAnhang(any());
    }

    @Test
    public void testGrafikUmfangUpdateSetGrafikUmfangBild() {
        when(prozessbaukasten.getGrafikUmfang()).thenReturn(anhang);

        prozessbaukastenImageUpdateService.updateProzessbaukastenBilder(prozessbaukasten);

        try {
            verify(prozessbaukasten, times(1)).setGrafikUmfangBild(bild);
        } catch (AssertionError e) {
            throw new MockitoAssertionError("Expected bild as arugment for method but got " + e.getMessage());
        }
    }

    @Test
    public void testGrafikUmfangUpdateSetKonzeptbaumBild() {
        when(prozessbaukasten.getGrafikUmfang()).thenReturn(anhang);

        prozessbaukastenImageUpdateService.updateProzessbaukastenBilder(prozessbaukasten);

        try {
            verify(prozessbaukasten, times(1)).setKonzeptbaumBild(null);
        } catch (AssertionError e) {
            throw new MockitoAssertionError("Expected null as arugment for method but got " + e.getMessage());
        }
    }

    @Test
    public void testKonzeptbaumUpdateGenerateAnhangFromBild() {
        when(prozessbaukasten.getKonzeptbaum()).thenReturn(anhang);

        prozessbaukastenImageUpdateService.updateProzessbaukastenBilder(prozessbaukasten);

        verify(imageService, times(1)).generateBildFromAnhang(any());
    }

    @Test
    public void testKonzeptbaumUpdateSetGrafikUmfang() {
        when(prozessbaukasten.getKonzeptbaum()).thenReturn(anhang);

        prozessbaukastenImageUpdateService.updateProzessbaukastenBilder(prozessbaukasten);

        try {
            verify(prozessbaukasten, times(1)).setGrafikUmfangBild(null);
        } catch (AssertionError e) {
            throw new MockitoAssertionError("Expected null as arugment for method but got " + e.getMessage());
        }
    }

    @Test
    public void testKonzeptbaumUpdateSetKonzeptbaumBild() {
        when(prozessbaukasten.getKonzeptbaum()).thenReturn(anhang);

        prozessbaukastenImageUpdateService.updateProzessbaukastenBilder(prozessbaukasten);

        try {
            verify(prozessbaukasten, times(1)).setKonzeptbaumBild(bild);
        } catch (AssertionError e) {
            throw new MockitoAssertionError("Expected bild as arugment for method but got " + e.getMessage());
        }
    }

    @Test
    public void testNullUpdateGenerateBildFromAnhang() {
        prozessbaukastenImageUpdateService.updateProzessbaukastenBilder(prozessbaukasten);

        verify(imageService, never()).generateBildFromAnhang(any());
    }

    @Test
    public void testNullUpdateSetBildMethods() {
        prozessbaukastenImageUpdateService.updateProzessbaukastenBilder(prozessbaukasten);

        try {
            verify(prozessbaukasten, times(1)).setGrafikUmfangBild(null);
            verify(prozessbaukasten, times(1)).setKonzeptbaumBild(null);
        } catch (AssertionError e) {
            throw new MockitoAssertionError("Expected null as arugment for method but got " + e.getMessage());
        }
    }

    @Test
    public void testDoubleUpdateGenerateBildFromAnhang() {
        when(prozessbaukasten.getKonzeptbaum()).thenReturn(anhang);
        when(prozessbaukasten.getGrafikUmfang()).thenReturn(anhang);

        prozessbaukastenImageUpdateService.updateProzessbaukastenBilder(prozessbaukasten);

        verify(imageService, times(2)).generateBildFromAnhang(any());
    }

    @Test
    public void testDoubleUpdateSetBildMethods() {
        when(prozessbaukasten.getKonzeptbaum()).thenReturn(anhang);
        when(prozessbaukasten.getGrafikUmfang()).thenReturn(anhang);

        prozessbaukastenImageUpdateService.updateProzessbaukastenBilder(prozessbaukasten);

        try {
            verify(prozessbaukasten, times(1)).setGrafikUmfangBild(bild);
            verify(prozessbaukasten, times(1)).setKonzeptbaumBild(bild);
        } catch (AssertionError e) {
            throw new MockitoAssertionError("Expected bild as arugment for method but got " + e.getMessage());
        }
    }

}
