package de.interfaceag.bmw.pzbk.controller;

import de.interfaceag.bmw.pzbk.dashboard.DashboardViewData;
import de.interfaceag.bmw.pzbk.dashboard.DashboardViewPermission;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DashboardControllerTest {

    private static final String CSS_STYLE = "pointer-events: none; background: rgba(181, 181, 181, 0.3); opacity: 0.4;";

    @InjectMocks
    private DashboardController dashboardController;

    @Mock
    private DashboardViewData viewData;

    @Mock
    private DashboardViewPermission dashboardViewPermission;

    @Test
    void isProzessbaukastenEnabled() {
        when(viewData.getViewPermission()).thenReturn(dashboardViewPermission);
        when(dashboardViewPermission.getNeuerProzessbaukastenMenuItem()).thenReturn(true);

        String prozessbaukastenDisabled = dashboardController.isProzessbaukastenMenuDisabled();

        Assert.assertTrue(prozessbaukastenDisabled.isEmpty());
    }

    @Test
    void isProzessbaukastenDisabled() {
        when(viewData.getViewPermission()).thenReturn(dashboardViewPermission);
        when(dashboardViewPermission.getNeuerProzessbaukastenMenuItem()).thenReturn(false);

        String prozessbaukastenDisabled = dashboardController.isProzessbaukastenMenuDisabled();

        Assert.assertEquals(CSS_STYLE, prozessbaukastenDisabled);
    }

    @Test
    void isProjekteEnabled() {
        when(viewData.getViewPermission()).thenReturn(dashboardViewPermission);
        when(dashboardViewPermission.getProjektverwaltungMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getAnforderungZuordnenMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getAnforderungVereinbarenMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getBerichtswesenMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getUmsetzungsbestaetigungMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getReportingProjektMenuItem()).thenReturn(true);

        String projekteDisabled = dashboardController.isProjekteMenuDisabled();

        Assert.assertTrue(projekteDisabled.isEmpty());
    }

    @Test
    void isProjekteDisabled() {
        when(viewData.getViewPermission()).thenReturn(dashboardViewPermission);
        when(dashboardViewPermission.getProjektverwaltungMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getAnforderungZuordnenMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getAnforderungVereinbarenMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getBerichtswesenMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getUmsetzungsbestaetigungMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getReportingProjektMenuItem()).thenReturn(false);

        String projekteDisabled = dashboardController.isProjekteMenuDisabled();

        Assert.assertEquals(CSS_STYLE, projekteDisabled);
    }

    @Test
    void isAnforderungDisabled() {
        when(viewData.getViewPermission()).thenReturn(dashboardViewPermission);
        when(dashboardViewPermission.getNeueMeldungMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getNeueAnforderungMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getReportingAnforderungMenuItem()).thenReturn(false);

        String anforderungDisabled = dashboardController.isAnforderungMenuDisabled();
        Assert.assertEquals(CSS_STYLE, anforderungDisabled);
    }

    @Test
    void isAnforderungEnabeld() {
        when(viewData.getViewPermission()).thenReturn(dashboardViewPermission);
        when(dashboardViewPermission.getNeueMeldungMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getNeueAnforderungMenuItem()).thenReturn(false);
        when(dashboardViewPermission.getReportingAnforderungMenuItem()).thenReturn(true);

        String anforderungDisabled = dashboardController.isAnforderungMenuDisabled();
        Assert.assertTrue(anforderungDisabled.isEmpty());
    }

}