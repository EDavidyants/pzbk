package de.interfaceag.bmw.pzbk.controller.dialogs;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class SensorCocDialogControllerTest {

    @Mock
    private SensorCocService sensorCocService;
    @InjectMocks
    private SensorCocDialogController sensorCocDialogController;

    @Mock
    SensorCoc sensorCoc;
    List<SensorCoc> sensorCocs;

    private void setupSensorCocList() {
        when(sensorCoc.pathToString()).thenReturn("Test");
        sensorCocs = Arrays.asList(sensorCoc);
        when(sensorCocService.getSensorCocsByRessortTechnologieOrtungByRole(any(), any(), any())).thenReturn(sensorCocs);
    }

    @Test
    public void testCompleteSensorCoc() {
        setupSensorCocList();
        List<SensorCoc> result = sensorCocDialogController.completeSensorCoc("Test");
        MatcherAssert.assertThat(result, IsCollectionWithSize.hasSize(1));
    }

    @Test
    public void testCompleteSensorCocLowercase() {
        setupSensorCocList();
        List<SensorCoc> result = sensorCocDialogController.completeSensorCoc("test");
        MatcherAssert.assertThat(result, IsCollectionWithSize.hasSize(1));
    }

    @Test
    public void testCompleteSensorCocUppercase() {
        setupSensorCocList();
        List<SensorCoc> result = sensorCocDialogController.completeSensorCoc("TEST");
        MatcherAssert.assertThat(result, IsCollectionWithSize.hasSize(1));
    }

    @Test
    public void testCompleteSensorCocPartialTermPraefix() {
        setupSensorCocList();
        List<SensorCoc> result = sensorCocDialogController.completeSensorCoc("Tes");
        MatcherAssert.assertThat(result, IsCollectionWithSize.hasSize(1));
    }

    @Test
    public void testCompleteSensorCocPartialTermSuffix() {
        setupSensorCocList();
        List<SensorCoc> result = sensorCocDialogController.completeSensorCoc("est");
        MatcherAssert.assertThat(result, IsCollectionWithSize.hasSize(1));
    }

    @Test
    public void testCompleteSensorCocInvalidTerm() {
        setupSensorCocList();
        List<SensorCoc> result = sensorCocDialogController.completeSensorCoc("A");
        MatcherAssert.assertThat(result, IsEmptyCollection.empty());
    }

    @Test
    public void testCompleteSensorCocNullTerm() {
        List<SensorCoc> result = sensorCocDialogController.completeSensorCoc(null);
        MatcherAssert.assertThat(result, IsEmptyCollection.empty());
    }

    @Test
    public void testCompleteSensorCocEmptyTerm() {
        List<SensorCoc> result = sensorCocDialogController.completeSensorCoc("");
        MatcherAssert.assertThat(result, IsEmptyCollection.empty());
    }

}
