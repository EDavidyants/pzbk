package de.interfaceag.bmw.pzbk.zak;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.services.LogService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.zak.xml.Anforderung;
import de.interfaceag.bmw.pzbk.zak.xml.XmlGet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@TestInstance(Lifecycle.PER_CLASS)
public class XmlServiceParseZakStatusTest {

    private XmlService xmlService = new XmlService();

    @BeforeAll
    public void setup() {
        xmlService.logService = mock(LogService.class);
        xmlService.zakVereinbarungService = mock(ZakVereinbarungService.class);
        xmlService.zakUebertragungService = mock(ZakUebertragungService.class);
        doNothing().when(xmlService.logService).addLogEntry(any(), any(), any());
        de.interfaceag.bmw.pzbk.entities.Anforderung anforderung = TestDataFactory.generateAnforderung();
        Derivat derivat = TestDataFactory.generateDerivat();
        Modul modul = TestDataFactory.generateModul();
        when(xmlService.zakVereinbarungService.getZakUebertragungByUebertragungIdDerivatId(any(), any()))
                .thenReturn(Optional.of(TestDataFactory.generateZakUebertragung(anforderung, derivat, modul, new Date(), null)));
        ZakUebertragung zakUebertragung = TestDataFactory.generateZakUebertragung(anforderung, derivat, modul, new Date(), null);
        zakUebertragung.setUebertragungId(42L);
        when(xmlService.zakUebertragungService.getZakUebertragungByUebertragungIdsAndDerivat(any(), any()))
                .thenReturn(Arrays.asList(zakUebertragung));
    }

    @Test
    public void parseZakStatusEntscheidungTest() {
        ZakStatus result = XmlService.parseZakStatusFromXml("", "Zurückgezogen durch Anforderer");
        Assertions.assertEquals(ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"bestätigt", "bestätigt aus Vorprozess", "nicht umsetzbar", "nicht bewertbar", "offen", " "})
    public void parseZakStatusAbstimmungTest(String abstimmung) {
        ZakStatus result = XmlService.parseZakStatusFromXml(abstimmung, "");

        switch (abstimmung) {
            case "bestätigt":
                Assertions.assertEquals(ZakStatus.DONE, result);
                break;
            case "bestätigt aus Vorprozess":
                Assertions.assertEquals(ZakStatus.BESTAETIGT_AUS_VORPROZESS, result);
                break;
            case "nicht umsetzbar":
                Assertions.assertEquals(ZakStatus.NICHT_UMSETZBAR, result);
                break;
            case "nicht bewertbar":
                Assertions.assertEquals(ZakStatus.NICHT_BEWERTBAR, result);
                break;
            case "offen":
                Assertions.assertEquals(ZakStatus.PENDING, result);
                break;
            case " ":
                Assertions.assertNull(result);
                break;
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"bestätigt", "bestätigt aus Vorprozess", "nicht umsetzbar", "nicht bewertbar", "offen"})
    public void parseZakStatusFromXmlGet(String abstimmung) {
        Anforderung anforderung = new Anforderung("42", abstimmung, "entscheidung", "kommentar");
        anforderung.setZakid("42");
        XmlGet xmlGet = new XmlGet(Arrays.asList(anforderung));
        Derivat derivat = TestDataFactory.generateDerivat();
        List<ZakUebertragung> parseResult = xmlService.parseXmlGet(xmlGet, derivat);
        Assertions.assertEquals(1, parseResult.size());

        ZakUebertragung zakUebertragung = parseResult.get(0);

        ZakStatus result = zakUebertragung.getZakStatus();

        switch (abstimmung) {
            case "bestätigt":
                Assertions.assertEquals(ZakStatus.DONE, result);
                break;
            case "bestätigt aus Vorprozess":
                Assertions.assertEquals(ZakStatus.BESTAETIGT_AUS_VORPROZESS, result);
                break;
            case "nicht umsetzbar":
                Assertions.assertEquals(ZakStatus.NICHT_UMSETZBAR, result);
                break;
            case "nicht bewertbar":
                Assertions.assertEquals(ZakStatus.NICHT_BEWERTBAR, result);
                break;
            case "offen":
                Assertions.assertEquals(ZakStatus.PENDING, result);
                break;
        }
    }

}
