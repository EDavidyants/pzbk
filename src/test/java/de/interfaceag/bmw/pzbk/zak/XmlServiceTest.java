package de.interfaceag.bmw.pzbk.zak;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.exceptions.ZakUebertragungException;
import de.interfaceag.bmw.pzbk.services.AnforderungFreigabeService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.LogService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.zak.xml.Dokument;
import de.interfaceag.bmw.pzbk.zak.xml.Konfigwert;
import de.interfaceag.bmw.pzbk.zak.xml.SaveXml;
import de.interfaceag.bmw.pzbk.zak.xml.XmlGet;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author sl
 */
public class XmlServiceTest {

    private static final Logger LOG = LoggerFactory.getLogger(XmlServiceTest.class);

    private XmlService xmlService;

    @Before
    public void setup() {
        xmlService = new XmlService();
        xmlService.zakVereinbarungService = mock(ZakVereinbarungService.class);
        xmlService.anforderungFreigabeService = mock(AnforderungFreigabeService.class);
        xmlService.berechtigungService = mock(BerechtigungService.class);
        xmlService.historyService = mock(AnforderungMeldungHistoryService.class);
        xmlService.logService = mock(LogService.class);
    }

    @Test
    public void generateXmlForZakUebertragung() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Derivat derivat = TestDataFactory.generateDerivat();
        Modul modul = TestDataFactory.generateModul();
        Date date = new Date();
        ZakUebertragung zakUebertragung = TestDataFactory.generateZakUebertragung(anforderung, derivat, modul, date);
        List<ModulSeTeam> modulSeTeams = TestDataFactory.generateModulSeTeams(modul, TestDataFactory.generateModulKomponenten());
        List<AnforderungFreigabeBeiModulSeTeam> anforderungFreigabe = TestDataFactory.generateAnforderungFreigabeBeiModulSeTeams(anforderung, modulSeTeams);

        when(xmlService.zakVereinbarungService.existsAnforderungModulInZak(any(), any())).thenReturn(false);
        when(xmlService.anforderungFreigabeService.getAnfoFreigabeByAnforderung(anforderung)).thenReturn(anforderungFreigabe);
        when(xmlService.berechtigungService.getSensorCoCLeiterOfSensorCoc(any()))
                .thenReturn(Optional.of(TestDataFactory.generateMitarbeiter("Fach", "Teamsprecher", "IF2")));
        try {
            SaveXml xml = xmlService.generateXmlForZakUebertragung(zakUebertragung);

            if (xml == null) {
                fail("xml is null");
            } else {
                xmlService.marshalXml(xml);

                assertAll("mapping",
                        () -> {
                            assertAll("header",
                                    () -> assertEquals(zakUebertragung.getDerivatAnforderungModul().getAnforderer().getQNumber(), xml.getHeader().getUser()),
                                    () -> assertEquals("ja", xml.getHeader().getNeu()),
                                    () -> assertEquals("ja", xml.getHeader().getTtool()),
                                    () -> assertEquals("ZA", xml.getHeader().getGeltung())
                            );
                            assertEquals(1, xml.getDokumentList().size());
                            Dokument dokument = xml.getDokumentList().get(0);
                            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                            assertAll("dokumente",
                                    () -> assertEquals("Name", dokument.getUmsetzer()),
                                    () -> assertEquals("1", dokument.getTtooluid()),
                                    () -> assertEquals(anforderung.getSensorCoc().getOrdnungsstruktur(), dokument.getOrdnungsstruktur()),
                                    () -> assertEquals(anforderung.getKommentarAnforderung(), dokument.getKommentar()),
                                    () -> assertEquals("Anforderungen", dokument.getForm()),
                                    () -> assertEquals("neu", dokument.getId()),
                                    () -> assertEquals(sdf.format(date) + " | Bear Beiter | IF-2: Kommentar \n", dokument.getFreigabekommentar())
                            );
                            assertEquals(1, dokument.getKonfigwertList().size());
                            Konfigwert konfigwert = dokument.getKonfigwertList().get(0);
                            assertAll("konfigwert",
                                    () -> assertEquals("neu", konfigwert.getId()),
                                    () -> assertEquals("E46", konfigwert.getZuordnung()),
                                    () -> assertEquals("", konfigwert.getWert())
                            );
                        });
            }
        } catch (ZakUebertragungException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void generateXmlForZakUebertragungUpdate() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Derivat derivat = TestDataFactory.generateDerivat();
        Modul modul = TestDataFactory.generateModul();
        Date date = new Date();
        ZakUebertragung zakUebertragung = TestDataFactory.generateZakUebertragung(anforderung, derivat, modul, date);

        when(xmlService.historyService.anforderungIsChangedAfterLastPlVorschlag(any())).thenReturn(false);
        when(xmlService.zakVereinbarungService.existsAnforderungModulInZak(any(), any())).thenReturn(true);
        try {
            SaveXml xml = xmlService.generateXmlForZakUebertragung(zakUebertragung);

            if (xml == null) {
                fail("xml is null");
            }

            xmlService.marshalXml(xml);

            assertAll("mapping",
                    () -> {
                        assertEquals(1, xml.getDokumentList().size());
                        Dokument dokument = xml.getDokumentList().get(0);
                        assertAll("dokument",
                                () -> assertEquals("Anforderungen", dokument.getForm()),
                                () -> assertEquals("1", dokument.getId())
                        );
                        assertEquals(1, dokument.getKonfigwertList().size());
                        Konfigwert konfigwert = dokument.getKonfigwertList().get(0);
                        assertAll("konfigwert",
                                () -> assertEquals("neu", konfigwert.getId()),
                                () -> assertEquals("E46", konfigwert.getZuordnung()),
                                () -> assertEquals("", konfigwert.getWert())
                        );
                    });
        } catch (ZakUebertragungException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void generateXmlForChangedZakUebertragung() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Derivat derivat = TestDataFactory.generateDerivat();
        Modul modul = TestDataFactory.generateModul();
        derivat.setId(42L);
        Date date = new Date();
        ZakUebertragung zakUebertragung = TestDataFactory.generateZakUebertragung(anforderung, derivat, modul, date);

        when(xmlService.historyService.anforderungIsChangedAfterLastPlVorschlag(any())).thenReturn(true);
        when(xmlService.zakVereinbarungService.existsAnforderungModulInZak(any(), any())).thenReturn(true);
        when(xmlService.zakVereinbarungService.getZakUebertragungForAnforderungModulAfterDate(any(), any(), any())).thenReturn(zakUebertragung);

        try {
            SaveXml xml = xmlService.generateXmlForZakUebertragung(zakUebertragung);

            if (xml == null) {
                fail("xml is null");
            }

            xmlService.marshalXml(xml);

            assertAll("mapping",
                    () -> {
                        assertAll("header",
                                () -> assertEquals(zakUebertragung.getDerivatAnforderungModul().getAnforderer().getQNumber(), xml.getHeader().getUser()),
                                () -> assertEquals("ja", xml.getHeader().getNeu()),
                                () -> assertEquals("ja", xml.getHeader().getTtool()),
                                () -> assertEquals("ZA", xml.getHeader().getGeltung())
                        );
                        assertEquals(1, xml.getDokumentList().size());
                        Dokument dokument = xml.getDokumentList().get(0);
                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                        assertAll("dokumente",
                                () -> assertEquals("Name", dokument.getUmsetzer()),
                                () -> assertEquals("1042", dokument.getTtooluid()),
                                () -> assertEquals(anforderung.getSensorCoc().getOrdnungsstruktur(), dokument.getOrdnungsstruktur()),
                                () -> assertEquals(anforderung.getKommentarAnforderung(), dokument.getKommentar()),
                                () -> assertEquals("Anforderungen", dokument.getForm()),
                                () -> assertEquals("neu", dokument.getId()),
                                () -> assertEquals("", dokument.getFreigabekommentar())
                        );
                        assertEquals(1, dokument.getKonfigwertList().size());
                        Konfigwert konfigwert = dokument.getKonfigwertList().get(0);
                        assertAll("konfigwert",
                                () -> assertEquals("neu", konfigwert.getId()),
                                () -> assertEquals("E46", konfigwert.getZuordnung()),
                                () -> assertEquals("", konfigwert.getWert())
                        );
                    });
        } catch (ZakUebertragungException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void generateXmlForChangedZakUebertragungUebertragungIdAlreadyUpdated() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Derivat derivat = TestDataFactory.generateDerivat();
        Modul modul = TestDataFactory.generateModul();
        derivat.setId(42L);
        Date date = new Date();
        ZakUebertragung zakUebertragung = TestDataFactory.generateZakUebertragung(anforderung, derivat, modul, date);
        zakUebertragung.setUebertragungId(1042L);

        when(xmlService.historyService.anforderungIsChangedAfterLastPlVorschlag(any())).thenReturn(true);
        when(xmlService.zakVereinbarungService.existsAnforderungModulInZak(any(), any())).thenReturn(true);
        when(xmlService.zakVereinbarungService.getZakUebertragungForAnforderungModulAfterDate(any(), any(), any())).thenReturn(zakUebertragung);

        try {
            SaveXml xml = xmlService.generateXmlForZakUebertragung(zakUebertragung);

            if (xml == null) {
                fail("xml is null");
            }

            xmlService.marshalXml(xml);

            assertAll("mapping",
                    () -> {
                        assertAll("header",
                                () -> assertEquals(zakUebertragung.getDerivatAnforderungModul().getAnforderer().getQNumber(), xml.getHeader().getUser()),
                                () -> assertEquals("ja", xml.getHeader().getNeu(), "neu is wrong"),
                                () -> assertEquals("ja", xml.getHeader().getTtool(), "ttool is wrong"),
                                () -> assertEquals("ZA", xml.getHeader().getGeltung())
                        );
                        assertEquals(1, xml.getDokumentList().size());
                        Dokument dokument = xml.getDokumentList().get(0);
                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                        assertAll("dokumente",
                                () -> assertEquals("Name", dokument.getUmsetzer()),
                                () -> assertEquals("1042", dokument.getTtooluid()),
                                () -> assertEquals(anforderung.getSensorCoc().getOrdnungsstruktur(), dokument.getOrdnungsstruktur()),
                                () -> assertEquals(anforderung.getKommentarAnforderung(), dokument.getKommentar()),
                                () -> assertEquals("Anforderungen", dokument.getForm()),
                                () -> assertEquals("neu", dokument.getId()),
                                () -> assertEquals("", dokument.getFreigabekommentar())
                        );
                        assertEquals(1, dokument.getKonfigwertList().size());
                        Konfigwert konfigwert = dokument.getKonfigwertList().get(0);
                        assertAll("konfigwert",
                                () -> assertEquals("neu", konfigwert.getId()),
                                () -> assertEquals("E46", konfigwert.getZuordnung()),
                                () -> assertEquals("", konfigwert.getWert())
                        );
                    });
        } catch (ZakUebertragungException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void generateXmlForChangedZakUebertragungUebertragungIdVeryLargeUebertragungId() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Derivat derivat = TestDataFactory.generateDerivat();
        Modul modul = TestDataFactory.generateModul();
        derivat.setId(42L);
        Date date = new Date();
        ZakUebertragung zakUebertragung = TestDataFactory.generateZakUebertragung(anforderung, derivat, modul, date);
        zakUebertragung.setUebertragungId(186250042042042042L);

        when(xmlService.historyService.anforderungIsChangedAfterLastPlVorschlag(any())).thenReturn(true);
        when(xmlService.zakVereinbarungService.existsAnforderungModulInZak(any(), any())).thenReturn(true);
        when(xmlService.zakVereinbarungService.getZakUebertragungForAnforderungModulAfterDate(any(), any(), any())).thenReturn(zakUebertragung);

        try {
            SaveXml xml = xmlService.generateXmlForZakUebertragung(zakUebertragung);

            if (xml == null) {
                fail("xml is null");
            }

            xmlService.marshalXml(xml);

            assertAll("mapping",
                    () -> {
                        assertAll("header",
                                () -> assertEquals(zakUebertragung.getDerivatAnforderungModul().getAnforderer().getQNumber(), xml.getHeader().getUser()),
                                () -> assertEquals("ja", xml.getHeader().getNeu(), "neu is wrong"),
                                () -> assertEquals("ja", xml.getHeader().getTtool(), "ttool is wrong"),
                                () -> assertEquals("ZA", xml.getHeader().getGeltung())
                        );
                        assertEquals(1, xml.getDokumentList().size());
                        Dokument dokument = xml.getDokumentList().get(0);
                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                        assertAll("dokumente",
                                () -> assertEquals("Name", dokument.getUmsetzer()),
                                () -> assertEquals("186250042042042042", dokument.getTtooluid(), "ttooluid is wrong"),
                                () -> assertEquals(anforderung.getSensorCoc().getOrdnungsstruktur(), dokument.getOrdnungsstruktur()),
                                () -> assertEquals(anforderung.getKommentarAnforderung(), dokument.getKommentar()),
                                () -> assertEquals("Anforderungen", dokument.getForm()),
                                () -> assertEquals("neu", dokument.getId()),
                                () -> assertEquals("", dokument.getFreigabekommentar())
                        );
                        assertEquals(1, dokument.getKonfigwertList().size());
                        Konfigwert konfigwert = dokument.getKonfigwertList().get(0);
                        assertAll("konfigwert",
                                () -> assertEquals("neu", konfigwert.getId()),
                                () -> assertEquals("E46", konfigwert.getZuordnung()),
                                () -> assertEquals("", konfigwert.getWert())
                        );
                    });
        } catch (ZakUebertragungException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void generateDokumentOptionalSensorCocLeiter() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Modul modul = TestDataFactory.generateModul();
        Mitarbeiter sensorCocLeiter = TestDataFactory.createMitarbeiter("42");
        when(xmlService.berechtigungService.getSensorCoCLeiterOfSensorCoc(any())).thenReturn(Optional.of(sensorCocLeiter));

        Dokument dokument;
        try {
            dokument = xmlService.generateDokument("1", modul, anforderung);

            if (dokument == null) {
                Assertions.fail("dokument is null");
            }

            Assertions.assertAll("mapping SensorCocLeiter --> dokument.erstellt",
                    () -> Assertions.assertNotNull(dokument.getErstellt()),
                    () -> Assertions.assertEquals(sensorCocLeiter.toString(), dokument.getErstellt())
            );

        } catch (ZakUebertragungException ex) {
            LOG.error(ex.toString());
            Assertions.fail(ex.toString());
        }
    }

    @Test
    public void generateResponseXml() {
        de.interfaceag.bmw.pzbk.zak.xml.Anforderung anforderung = new de.interfaceag.bmw.pzbk.zak.xml.Anforderung("42", "bestätigt", "offen", "Sepp Maier, IF-007: Das ist nocht offen!");
        List<de.interfaceag.bmw.pzbk.zak.xml.Anforderung> anforderungList = new ArrayList<>();
        anforderungList.add(anforderung);
        XmlGet anforderungen = new XmlGet(anforderungList);

        when(xmlService.zakVereinbarungService.existsAnforderungModulInZak(any(), any())).thenReturn(true);
        final String responseXml = xmlService.marshalResponseXml(anforderungen);

        assertThat(responseXml, is("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><xmlget><anforderungen><anforderung><abstimmung>bestätigt</abstimmung><abstimmungkommentar>Sepp Maier, IF-007: Das ist nocht offen!</abstimmungkommentar><entscheidung>offen</entscheidung><ttooluid>42</ttooluid></anforderung></anforderungen></xmlget>"));

    }

}
