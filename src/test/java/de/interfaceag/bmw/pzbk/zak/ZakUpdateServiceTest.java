package de.interfaceag.bmw.pzbk.zak;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.LogService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author fn
 */
@ExtendWith(MockitoExtension.class)
public class ZakUpdateServiceTest {

    @Mock
    private DerivatService derivatService;
    @Mock
    private LogService logService;
    @Mock
    private XmlService xmlService;
    @Mock
    private ZakUpdateDao zakUpdateDao;
    @Mock
    private ZakVereinbarungService zakVereinbarungService;
    @Mock
    private DerivatAnforderungModulService derivatAnforderungModulService;
    @Mock
    private ZakDoubleUpdateDao zakDoubleUpdateDao;

    @InjectMocks
    private ZakUpdateService zakUpdateService;

    Derivat derivat;
    Anforderung anforderung;
    Modul modul;

    @BeforeEach
    public void setUp() {
        derivat = TestDataFactory.generateDerivat();
        anforderung = TestDataFactory.generateAnforderung();
        modul = TestDataFactory.generateFullModul();

        when(derivatService.getAllDerivateWithZielvereinbarungFalse()).thenReturn(TestDataFactory.genereteDerivate());
    }

    @Test
    public void testGetZakUpdateLoadAllDerivatWithZakUpdates() {

        when(xmlService.getZakUbertragungen(any())).thenReturn(TestDataFactory.generateZakUpdateDto());
        when(xmlService.parseXmlGet(any(), any())).thenReturn(TestDataFactory.generateZakUebertragungen(anforderung, derivat, modul, new Date()));

        zakUpdateService.getZakUpdate();

        verify(derivatService, times(1)).getAllDerivateWithZielvereinbarungFalse();
    }

    @Test
    public void testGetZakUpdateSaveNewZakStatusOnDerivatAnforderungModul() {
        when(xmlService.getZakUbertragungen(any())).thenReturn(TestDataFactory.generateZakUpdateDto());
        when(xmlService.parseXmlGet(any(), any())).thenReturn(TestDataFactory.generateZakUebertragungen(anforderung, derivat, modul, new Date()));

        zakUpdateService.getZakUpdate();

        verify(derivatAnforderungModulService, times(1)).updateDerivatAnforderungModulZakStatus(any(), any(), any());

    }

    @Test
    public void testGetZakUpdateSaveNewZakUebertragung() {
        when(xmlService.getZakUbertragungen(any())).thenReturn(TestDataFactory.generateZakUpdateDto());
        when(xmlService.parseXmlGet(any(), any())).thenReturn(TestDataFactory.generateZakUebertragungen(anforderung, derivat, modul, new Date()));

        zakUpdateService.getZakUpdate();

        verify(zakVereinbarungService, times(1)).persistZakUebertragung(any());
    }

    @Test
    public void testGetZakUpdateSaveZakUpdateDateOnDerivat() {
        when(xmlService.getZakUbertragungen(any())).thenReturn(TestDataFactory.generateZakUpdateDto());
        when(xmlService.parseXmlGet(any(), any())).thenReturn(TestDataFactory.generateZakUebertragungen(anforderung, derivat, modul, new Date()));

        zakUpdateService.getZakUpdate();

        verify(derivatService, times(1)).persistDerivat(any());
    }

    @Test
    public void testGetZakUpdateParsXMLGetFromZakToZakUebertragung() {
        when(xmlService.getZakUbertragungen(any())).thenReturn(TestDataFactory.generateZakUpdateDto());
        when(xmlService.parseXmlGet(any(), any())).thenReturn(TestDataFactory.generateZakUebertragungen(anforderung, derivat, modul, new Date()));

        zakUpdateService.getZakUpdate();

        verify(xmlService, times(1)).parseXmlGet(any(), any());
    }

    @Test
    public void testGetZakUpdateSaveZakUpdateFromZak() {
        when(xmlService.getZakUbertragungen(any())).thenReturn(TestDataFactory.generateZakUpdateDto());
        when(xmlService.parseXmlGet(any(), any())).thenReturn(TestDataFactory.generateZakUebertragungen(anforderung, derivat, modul, new Date()));

        zakUpdateService.getZakUpdate();

        verify(zakUpdateDao, times(1)).persistZakUpdate(any());
    }

    @Test
    public void testGetZakUpdateGetZakUpdateFromZakXml() {
        when(xmlService.getZakUbertragungen(any())).thenReturn(TestDataFactory.generateZakUpdateDto());
        when(xmlService.parseXmlGet(any(), any())).thenReturn(TestDataFactory.generateZakUebertragungen(anforderung, derivat, modul, new Date()));

        zakUpdateService.getZakUpdate();

        verify(xmlService, times(1)).getZakUbertragungen(any());
    }

    @Test
    public void testGetZakUpdateLogZakUpdate() {
        when(xmlService.getZakUbertragungen(any())).thenReturn(TestDataFactory.generateZakUpdateDto());
        when(xmlService.parseXmlGet(any(), any())).thenReturn(TestDataFactory.generateZakUebertragungen(anforderung, derivat, modul, new Date()));

        zakUpdateService.getZakUpdate();

        verify(logService, times(3)).addLogEntry(any(), any(), any());
    }

    @Test
    public void testGetZakUpdateAllDerivatAtStatusZielvereinbart() {

        when(derivatService.getAllDerivateWithZielvereinbarungFalse()).thenReturn(Collections.emptyList());

        zakUpdateService.getZakUpdate();

        verify(derivatService, times(0)).persistDerivat(any());
    }

    @Test
    public void testGetZakUpdateNoZakUebertragungFromXmlGet() {

        when(xmlService.getZakUbertragungen(any())).thenReturn(TestDataFactory.generateZakUpdateDto());
        when(xmlService.parseXmlGet(any(), any())).thenReturn(Collections.emptyList());

        zakUpdateService.getZakUpdate();

        verify(derivatService, times(0)).persistDerivat(any());
        verify(logService, times(2)).addLogEntry(any(), any(), any());
    }

    @Test
    public void testGetZakUpdateDoubleZakUpdate() {

        when(xmlService.getZakUbertragungen(any())).thenReturn(TestDataFactory.generateZakUpdateDto());

        List<ZakUebertragung> zakUpdateList = new ArrayList<>();
        zakUpdateList.add(TestDataFactory.generateZakUebertragung(anforderung, derivat, modul, new Date()));
        zakUpdateList.add(TestDataFactory.generateZakUebertragung(anforderung, derivat, modul, new Date()));
        when(xmlService.parseXmlGet(any(), any())).thenReturn(zakUpdateList);
        zakUpdateService.getZakUpdate();

        verify(zakDoubleUpdateDao, times(1)).persistZakUpdate(any());

    }

}
