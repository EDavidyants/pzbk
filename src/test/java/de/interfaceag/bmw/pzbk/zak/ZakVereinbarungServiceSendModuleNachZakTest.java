package de.interfaceag.bmw.pzbk.zak;

import de.interfaceag.bmw.pzbk.dao.ZakUebertragungDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.services.LogService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ZakVereinbarungServiceSendModuleNachZakTest {

    @Mock
    private Session session;

    @Mock
    private LogService logService;

    @Mock
    private DerivatAnforderungModulService derivatAnforderungModulService;

    @Mock
    private ZakUebertragungDao zakUebertragungDao;

    @Mock
    private ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;

    @Mock
    private ZakUebertragung zakUebertragung;

    @InjectMocks
    private ZakVereinbarungService zakVereinbarungService;

    private Mitarbeiter user;
    private List<DerivatAnforderungModul> vereinbarungen;
    private DerivatAnforderungModul vereinbarung;
    private List<ZakUebertragung> zakUebertragungen;
    private List<Derivat> derivaten;
    private Anforderung anforderung;

    @BeforeEach
    public void setUp() {
        user = new Mitarbeiter("Max", "Admin");
        vereinbarung = TestDataFactory.generateDerivatAnforderungModul();
        vereinbarungen = new ArrayList<>();

        doReturn(false).when(session).hasRole(Rolle.ANFORDERER);
        doReturn(true).when(session).hasRole(Rolle.ADMIN);
        vereinbarungen.add(vereinbarung);
        derivaten = Arrays.asList(vereinbarung.getDerivat());
        anforderung = vereinbarung.getAnforderung();
        when(derivatAnforderungModulService.getDerivatAnforderungModulByIdList(any())).thenReturn(vereinbarungen);

        when(session.getUser()).thenReturn(user);
    }

    @ParameterizedTest
    @EnumSource(ZakStatus.class)
    public void testModuleNachZakUebertragenByZakStatus(ZakStatus zakStatus) {

        when(zakUebertragung.getZakStatus()).thenReturn(zakStatus);
        when(zakUebertragungDao.getZakUebertragungByAnforderungDerivatList(derivaten, anforderung)).thenReturn(Arrays.asList(zakUebertragung));

        Map<Derivat, List<ZakUebertragung>> report = zakVereinbarungService.sendModuleNachZak(vereinbarungen);

        switch (zakStatus) {
            case EMPTY:
            case PENDING:
            case DONE:
            case NICHT_BEWERTBAR:
            case NICHT_UMSETZBAR:
            case KEINE_ZAKUEBERTRAGUNG:
            case ZURUECKGEZOGEN_DURCH_ANFORDERER:
            case BESTAETIGT_AUS_VORPROZESS: {
                testZuordnungOnZakUebertragungErfolgreich(report);
                break;
            }

            case UEBERTRAGUNG_FEHLERHAFT: {
                testZuordnungOnZakUebertragungFehlerhaft(report);
                break;
            }

            default:
                break;
        }

    }

    private void testZuordnungOnZakUebertragungFehlerhaft(Map<Derivat, List<ZakUebertragung>> report) {
        List<ZakUebertragung> zaks = report.get(vereinbarung.getDerivat());
        zaks.stream().map((zak) -> zak.getDerivatAnforderungModul().getZuordnungAnforderungDerivat()).forEachOrdered((zuordnung) -> {
            verify(zuordnungAnforderungDerivatService).setDerivatAnforderungAsNachZakUebertragen(zuordnung, true);
        });
    }

    private void testZuordnungOnZakUebertragungErfolgreich(Map<Derivat, List<ZakUebertragung>> report) {
        List<ZakUebertragung> zaks = report.get(vereinbarung.getDerivat());
        zaks.stream().map((zak) -> zak.getDerivatAnforderungModul().getZuordnungAnforderungDerivat()).forEachOrdered((zuordnung) -> {
            verify(zuordnungAnforderungDerivatService).setDerivatAnforderungAsNachZakUebertragen(zuordnung, false);
        });
    }

}
