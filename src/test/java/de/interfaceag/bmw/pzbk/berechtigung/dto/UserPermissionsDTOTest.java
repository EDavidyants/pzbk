package de.interfaceag.bmw.pzbk.berechtigung.dto;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class UserPermissionsDTOTest {

    private UserPermissionsDTO userPermissions;

    @Mock
    private TteamMitgliedBerechtigung tteamMitgliedBerechtigung;
    @Mock
    private TteamVertreterBerechtigung tteamVertreterBerechtigung;
    @Mock
    private List<BerechtigungDto> berechtigungen;
    @Mock
    private List<Rolle> rollen;
    private List<Long> writeTteamIds;
    private List<Long> readTteamIds;

    @BeforeEach
    public void setUp() {
        writeTteamIds = Arrays.asList(1L);
        readTteamIds = Arrays.asList(2L);

        userPermissions = new UserPermissionsDTO(rollen,
                berechtigungen,
                tteamMitgliedBerechtigung,
                tteamVertreterBerechtigung
        );
    }

    @Test
    public void testGetTteamIdsForTteamMitgliedSchreibend() {
        when(tteamMitgliedBerechtigung.getTteamIdsMitSchreibrechten()).thenReturn(writeTteamIds);
        List<Long> result = userPermissions.getTteamIdsForTteamMitgliedSchreibend();
        Assertions.assertEquals(writeTteamIds, result);
    }

    @Test
    public void testGetTteamIdsForTteamMitgliedLesend() {
        when(tteamMitgliedBerechtigung.getTteamIdsMitLeserechten()).thenReturn(readTteamIds);
        List<Long> result = userPermissions.getTteamIdsForTteamMitgliedLesend();
        Assertions.assertEquals(readTteamIds, result);
    }

}
