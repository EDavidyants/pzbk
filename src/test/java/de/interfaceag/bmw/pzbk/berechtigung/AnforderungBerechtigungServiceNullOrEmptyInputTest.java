package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableWithSize.iterableWithSize;

@ExtendWith(MockitoExtension.class)
class AnforderungBerechtigungServiceNullOrEmptyInputTest {

    @Mock
    private Session session;
    @Mock
    private AnforderungBerechtigungDao anforderungBerechtigungDao;
    @InjectMocks
    private AnforderungBerechtigungService anforderungBerechtigungService;

    @Test
    void restrictToAuthorizedMeldungenNullInput() {
        final Collection<Long> result = anforderungBerechtigungService.restrictToAuthorizedAnforderungen(null);
        assertThat(result, iterableWithSize(0));
    }

    @Test
    void restrictToAuthorizedMeldungenEmptyInput() {
        final Collection<Long> result = anforderungBerechtigungService.restrictToAuthorizedAnforderungen(Collections.emptyList());
        assertThat(result, iterableWithSize(0));
    }

}