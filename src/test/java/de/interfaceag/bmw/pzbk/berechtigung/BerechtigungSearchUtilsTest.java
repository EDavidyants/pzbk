package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class BerechtigungSearchUtilsTest {

    Set<Mitarbeiter> mitarbeiterSet;

    @BeforeEach
    void setUp() {
        mitarbeiterSet = new HashSet<>();
        Mitarbeiter mitarbeiter = new Mitarbeiter("Peter", "Lustig");
        mitarbeiterSet.add(mitarbeiter);
    }


    @Test
    public void testGetTteamvertreterForMitarbeiterList() {

        assertThat(BerechtigungSearchUtils.getTteamVertreterForMitarbeiter(mitarbeiterSet).getQuery(), is(" SELECT DISTINCT ttv.mitarbeiter FROM TteamVertreter ttv WHERE ttv.mitarbeiter IN :existingMitarbeiter"));
    }

    @Test
    public void testGetTteamvMitgliedForMitarbeiterList() {
        assertThat(BerechtigungSearchUtils.getTteamMitgliederforMitarbeiter(mitarbeiterSet).getQuery(), is(" SELECT DISTINCT ttm.mitarbeiter FROM TTeamMitglied ttm WHERE ttm.mitarbeiter IN :existingMitarbeiter"));
    }

    @Test
    public void getAllTteamMitgliederTest() {
        assertThat(BerechtigungSearchUtils.getAllTteamMitglieder().getQuery(), is(" SELECT DISTINCT ttm.mitarbeiter FROM TTeamMitglied ttm"));
    }

    @Test
    public void getAllTteamVertreterTest() {
        assertThat(BerechtigungSearchUtils.getAllTteamVertreter().getQuery(), is(" SELECT DISTINCT ttv.mitarbeiter FROM TteamVertreter ttv"));
    }
}