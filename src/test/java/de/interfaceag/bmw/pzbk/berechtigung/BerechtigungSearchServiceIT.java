package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.berechtigung.dao.TteamMitgliedDAO;
import de.interfaceag.bmw.pzbk.berechtigung.dao.TteamVertreterDAO;
import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigung.dto.MitarbeiterDTO;
import de.interfaceag.bmw.pzbk.dao.BerechtigungDao;
import de.interfaceag.bmw.pzbk.dao.DerivatDao;
import de.interfaceag.bmw.pzbk.dao.MitarbeiterDao;
import de.interfaceag.bmw.pzbk.dao.ModulDao;
import de.interfaceag.bmw.pzbk.dao.SensorCocDao;
import de.interfaceag.bmw.pzbk.dao.TteamDao;
import de.interfaceag.bmw.pzbk.entities.Berechtigung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.rollen.TTeamMitglied;
import de.interfaceag.bmw.pzbk.entities.rollen.TteamVertreter;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

class BerechtigungSearchServiceIT extends IntegrationTest {

    private BerechtigungSearchService berechtigungSearchService = new BerechtigungSearchService();
    private BerechtigungDao berechtigungDao = new BerechtigungDao();
    private MitarbeiterDao mitarbeiterDao = new MitarbeiterDao();
    private TteamVertreterDAO tteamVertreterDAO = new TteamVertreterDAO();
    private TteamMitgliedDAO tteamMitgliedDAO = new TteamMitgliedDAO();
    private TteamDao tteamDao = new TteamDao();
    private SensorCocDao sensorCocDao = new SensorCocDao();
    private DerivatDao derivatDao = new DerivatDao();
    private ModulDao modulDao = new ModulDao();
    private SensorCoc sensorCoc;
    private Derivat derivat;
    private ModulSeTeam modulSeTeam;
    private Tteam tteam;

    private BerechtigungViewFilter berechtigungViewFilter;

    @BeforeEach
    void setUp() throws IllegalAccessException {
        // start transaction before test case
        super.begin();

        // override field entityManager in themenklammerDao with private access
        FieldUtils.writeField(berechtigungSearchService, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(berechtigungDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(mitarbeiterDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(berechtigungSearchService, "session", getSession(), true);
        FieldUtils.writeField(berechtigungSearchService, "berechtigungService", getBerechtigungService(), true);
        FieldUtils.writeField(tteamVertreterDAO, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(tteamMitgliedDAO, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(tteamDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(sensorCocDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(derivatDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(modulDao, "entityManager", super.getEntityManager(), true);

        buildBerechtigungen();

        super.commit();
        super.begin();
    }

    private void buildBerechtigungen() {

        Mitarbeiter testMitarbeiter = TestDataFactory.generateMitarbeiter("Klaus", "Haus", "Abteilung 1");
        Mitarbeiter testMitarbeiter2 = TestDataFactory.generateMitarbeiter("Peter", "Lustig", "Abteilung 1");
        Mitarbeiter testMitarbeiter3 = TestDataFactory.generateMitarbeiter("Felix", "Firefox", "Abteilung 1");

        mitarbeiterDao.persistMitarbeiter(testMitarbeiter);
        mitarbeiterDao.persistMitarbeiter(testMitarbeiter2);
        mitarbeiterDao.persistMitarbeiter(testMitarbeiter3);

        tteam = TestDataFactory.generateTteam("tteam1");

        TTeamMitglied tteamMitglied = generateTteamMitgliedForMitarbeiter(testMitarbeiter2, tteam);
        TteamVertreter tteamVertreter = generateTteamVertreterForMitarbeiter(testMitarbeiter3, tteam);

        tteamDao.saveTteam(tteam);

        tteamMitgliedDAO.persistTteamMitglied(tteamMitglied);
        tteamVertreterDAO.persistTteamVertreter(tteamVertreter);

        Berechtigung berechtigung = generateAdminBerechtigungForMitarbeiter(testMitarbeiter);
        berechtigungDao.persistBerechtigung(berechtigung);

        sensorCoc = TestDataFactory.generateSensorCoc();
        sensorCocDao.persistSensorCoc(sensorCoc);

        Berechtigung berechtigungSensor = generateSensorBerechtigung(testMitarbeiter, sensorCoc.getSensorCocId());
        berechtigungDao.persistBerechtigung(berechtigungSensor);

        Berechtigung berechtigungSensorCocLeiter = generateSernsorCocLeiterBerechtigung(testMitarbeiter, sensorCoc.getSensorCocId());
        berechtigungDao.persistBerechtigung(berechtigungSensorCocLeiter);

        Berechtigung berechtigungSensorCocVertreter = generateSensorCocVertreterBerechtigung(testMitarbeiter, sensorCoc.getSensorCocId());
        berechtigungDao.persistBerechtigung(berechtigungSensorCocVertreter);

        Berechtigung berechtigungUmsetzungsbestaetiger = generateUmsetzungsbestaetigerBerechtigung(testMitarbeiter, sensorCoc.getSensorCocId());
        berechtigungDao.persistBerechtigung(berechtigungUmsetzungsbestaetiger);

        Berechtigung berechtigungTteamLeiter = generateTteamLeiterBerechtigung(testMitarbeiter, tteam.getId());
        berechtigungDao.persistBerechtigung(berechtigungTteamLeiter);

        derivat = TestDataFactory.generateDerivat("DerivatName", "DerivatBezeichung", "Produktlinie");
        derivatDao.persistDerivat(derivat);

        Berechtigung berechtigungAnforderer = generateAnfordererBerechtigung(testMitarbeiter, derivat.getId());
        berechtigungDao.persistBerechtigung(berechtigungAnforderer);

        Modul modul = TestDataFactory.generateModul("Modul1", "Fachbereich1", "Beschreibung");
        modulDao.persistModule(Arrays.asList(modul));

        ModulKomponente modulkomponente = TestDataFactory.generateModulKomponente();
        modulDao.persistModulKomponente(modulkomponente);


        modulSeTeam = TestDataFactory.generateModulSeTeam(modul, Arrays.asList(modulkomponente));
        modulDao.persistSeTeam(modulSeTeam);

        Berechtigung berechtigungECoC = generateECoCBerechtigung(testMitarbeiter, modulSeTeam.getId());
        berechtigungDao.persistBerechtigung(berechtigungECoC);

    }


    private Session getSession() {
        return new Session() {
            @Override
            public UserPermissions<BerechtigungDto> getUserPermissions() {
                return null;
            }

            @Override
            public boolean hasRole(Rolle rolle) {
                return false;
            }

            @Override
            public Mitarbeiter getUser() {
                return null;
            }

            @Override
            public String getUserAcronym() {
                return null;
            }

            @Override
            public void setLocationForView() {

            }

            @Override
            public Locale getLocale() {
                return null;
            }
        };
    }

    private BerechtigungService getBerechtigungService() {
        return new BerechtigungService() {
            @Override
            public List<Rolle> getRolesForUser(Mitarbeiter mitarbeiter) {
                return Rolle.getAllRolles();
            }
        };
    }

    private Berechtigung generateAdminBerechtigungForMitarbeiter(Mitarbeiter testMitarbeiter) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(testMitarbeiter);
        berechtigung.setRolle(Rolle.ADMIN);

        return berechtigung;
    }

    private TTeamMitglied generateTteamMitgliedForMitarbeiter(Mitarbeiter testMitarbeiter, Tteam tteam) {
        TTeamMitglied tteamMitglied = new TTeamMitglied();
        tteamMitglied.setMitarbeiter(testMitarbeiter);
        tteamMitglied.setRechttype(Rechttype.SCHREIBRECHT);
        tteamMitglied.setTteam(tteam);
        return tteamMitglied;
    }

    private TteamVertreter generateTteamVertreterForMitarbeiter(Mitarbeiter testMitarbeiter, Tteam tteam) {
        TteamVertreter tteamVertreter = new TteamVertreter();
        tteamVertreter.setMitarbeiter(testMitarbeiter);
        tteamVertreter.setTteam(tteam);
        return tteamVertreter;
    }

    private Berechtigung generateSensorBerechtigung(Mitarbeiter mitarbeiter, Long sensorCocId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.SENSOR);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.SENSOR_COC);
        berechtigung.setFuerId(sensorCocId.toString());
        return berechtigung;
    }

    private Berechtigung generateSernsorCocLeiterBerechtigung(Mitarbeiter mitarbeiter, Long sensorCocId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.SENSORCOCLEITER);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.SENSOR_COC);
        berechtigung.setFuerId(sensorCocId.toString());
        return berechtigung;
    }

    private Berechtigung generateSensorCocVertreterBerechtigung(Mitarbeiter mitarbeiter, Long sensorCocId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.SCL_VERTRETER);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.SENSOR_COC);
        berechtigung.setFuerId(sensorCocId.toString());
        return berechtigung;
    }

    private Berechtigung generateAnfordererBerechtigung(Mitarbeiter mitarbeiter, Long derivatId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.ANFORDERER);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.DERIVAT);
        berechtigung.setFuerId(derivatId.toString());
        return berechtigung;
    }

    private Berechtigung generateECoCBerechtigung(Mitarbeiter mitarbeiter, Long modulSeTeamNameId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.E_COC);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.MODULSETEAM);
        berechtigung.setFuerId(modulSeTeamNameId.toString());
        return berechtigung;
    }

    private Berechtigung generateTteamLeiterBerechtigung(Mitarbeiter mitarbeiter, Long tteamId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.T_TEAMLEITER);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.TTEAM);
        berechtigung.setFuerId(tteamId.toString());
        return berechtigung;
    }

    private Berechtigung generateUmsetzungsbestaetigerBerechtigung(Mitarbeiter mitarbeiter, Long sensorCocId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.UMSETZUNGSBESTAETIGER);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.SENSOR_COC);
        berechtigung.setFuerId(sensorCocId.toString());
        return berechtigung;
    }


    private void buildBerechtigungViewFilter(UrlParameter urlParameter) {

        List<String> allAbteilung = TestDataFactory.generateAbteilungen();
        List<SensorCoc> allSensorCoc = Arrays.asList(sensorCoc);
        List<ModulSeTeam> allModulSeTeam = Arrays.asList(modulSeTeam);
        List<Derivat> allDerivat = Arrays.asList(derivat);
        List<Tteam> allTteams = Arrays.asList(tteam);
        List<String> allZakEinordung = TestDataFactory.createZakEinordnungen(3);
        List<Rolle> allRolesForUser = Rolle.getAllRolles();

        berechtigungViewFilter = new BerechtigungViewFilter(urlParameter, allAbteilung, allSensorCoc, allModulSeTeam, allDerivat, allTteams, allZakEinordung, allRolesForUser);
    }

    @Test
    void getSearchResultWithoutFilter() {

        buildBerechtigungViewFilter(new UrlParameter());


        List<MitarbeiterDTO> foundMitarbeiter = berechtigungSearchService.getSeachResult(berechtigungViewFilter.getVornameFilter(),
                berechtigungViewFilter.getNachnameFilter(), berechtigungViewFilter.getRolleFilter(),
                berechtigungViewFilter.getAbteilungFilter(), berechtigungViewFilter.getSensorCocFilterLesend(),
                berechtigungViewFilter.getSensorCocFilterSchreibend(), berechtigungViewFilter.getModulSeTeamFilter(),
                berechtigungViewFilter.getDerivatFilter(), berechtigungViewFilter.gettTeamFilter(), berechtigungViewFilter.getZakEinordnungFilter(),
                berechtigungViewFilter.getNurMitBerechtigungFilter());

        MatcherAssert.assertThat(foundMitarbeiter, IsCollectionWithSize.hasSize(3));

    }

    @Test
    void getSearchResultForFilterNachname() {

        UrlParameter urlParameter = new UrlParameter();
        urlParameter.addOrReplaceParamter("nachname", "Haus");
        buildBerechtigungViewFilter(urlParameter);

        List<MitarbeiterDTO> foundMitarbeiter = berechtigungSearchService.getSeachResult(berechtigungViewFilter.getVornameFilter(),
                berechtigungViewFilter.getNachnameFilter(), berechtigungViewFilter.getRolleFilter(),
                berechtigungViewFilter.getAbteilungFilter(), berechtigungViewFilter.getSensorCocFilterLesend(),
                berechtigungViewFilter.getSensorCocFilterSchreibend(), berechtigungViewFilter.getModulSeTeamFilter(),
                berechtigungViewFilter.getDerivatFilter(), berechtigungViewFilter.gettTeamFilter(), berechtigungViewFilter.getZakEinordnungFilter(),
                berechtigungViewFilter.getNurMitBerechtigungFilter());

        MatcherAssert.assertThat(foundMitarbeiter, IsCollectionWithSize.hasSize(1));

    }

    @Test
    void getSearchResultForAllMitarbeiterFilter() {

        UrlParameter urlParameter = new UrlParameter();
        urlParameter.addOrReplaceParamter("nachname", "Haus");
        urlParameter.addOrReplaceParamter("vorname", "Klaus");
        urlParameter.addOrReplaceParamter("rolle", "0");
        urlParameter.addOrReplaceParamter("abteilungFilter", "Abteilung 1");
        urlParameter.addOrReplaceParamter("nurBerechtigungFilter", "1");
        buildBerechtigungViewFilter(urlParameter);

        List<MitarbeiterDTO> foundMitarbeiter = berechtigungSearchService.getSeachResult(berechtigungViewFilter.getVornameFilter(),
                berechtigungViewFilter.getNachnameFilter(), berechtigungViewFilter.getRolleFilter(),
                berechtigungViewFilter.getAbteilungFilter(), berechtigungViewFilter.getSensorCocFilterLesend(),
                berechtigungViewFilter.getSensorCocFilterSchreibend(), berechtigungViewFilter.getModulSeTeamFilter(),
                berechtigungViewFilter.getDerivatFilter(), berechtigungViewFilter.gettTeamFilter(), berechtigungViewFilter.getZakEinordnungFilter(),
                berechtigungViewFilter.getNurMitBerechtigungFilter());

        MatcherAssert.assertThat(foundMitarbeiter, IsCollectionWithSize.hasSize(1));

    }

    @Test
    void getSearchResultForSensorSchreibend() {

        UrlParameter urlParameter = new UrlParameter();
        urlParameter.addOrReplaceParamter("sensorCocSchreibend", sensorCoc.getSensorCocId().toString());
        urlParameter.addOrReplaceParamter("nurBerechtigungFilter", "1");
        buildBerechtigungViewFilter(urlParameter);

        List<MitarbeiterDTO> foundMitarbeiter = berechtigungSearchService.getSeachResult(berechtigungViewFilter.getVornameFilter(),
                berechtigungViewFilter.getNachnameFilter(), berechtigungViewFilter.getRolleFilter(),
                berechtigungViewFilter.getAbteilungFilter(), berechtigungViewFilter.getSensorCocFilterLesend(),
                berechtigungViewFilter.getSensorCocFilterSchreibend(), berechtigungViewFilter.getModulSeTeamFilter(),
                berechtigungViewFilter.getDerivatFilter(), berechtigungViewFilter.gettTeamFilter(), berechtigungViewFilter.getZakEinordnungFilter(),
                berechtigungViewFilter.getNurMitBerechtigungFilter());

        MatcherAssert.assertThat(foundMitarbeiter, IsCollectionWithSize.hasSize(1));

    }

    @Test
    void getSearchResultForTteamFilter() {

        UrlParameter urlParameter = new UrlParameter();
        urlParameter.addOrReplaceParamter("tteamFilter", String.valueOf(tteam.getId()));
        urlParameter.addOrReplaceParamter("nurBerechtigungFilter", "1");
        buildBerechtigungViewFilter(urlParameter);

        List<MitarbeiterDTO> foundMitarbeiter = berechtigungSearchService.getSeachResult(berechtigungViewFilter.getVornameFilter(),
                berechtigungViewFilter.getNachnameFilter(), berechtigungViewFilter.getRolleFilter(),
                berechtigungViewFilter.getAbteilungFilter(), berechtigungViewFilter.getSensorCocFilterLesend(),
                berechtigungViewFilter.getSensorCocFilterSchreibend(), berechtigungViewFilter.getModulSeTeamFilter(),
                berechtigungViewFilter.getDerivatFilter(), berechtigungViewFilter.gettTeamFilter(), berechtigungViewFilter.getZakEinordnungFilter(),
                berechtigungViewFilter.getNurMitBerechtigungFilter());

        MatcherAssert.assertThat(foundMitarbeiter, IsCollectionWithSize.hasSize(1));

    }

    @Test
    void getSearchResultForDerivatFilter() {

        UrlParameter urlParameter = new UrlParameter();
        urlParameter.addOrReplaceParamter("derivatFilter", String.valueOf(derivat.getId()));
        urlParameter.addOrReplaceParamter("nurBerechtigungFilter", "1");
        buildBerechtigungViewFilter(urlParameter);

        List<MitarbeiterDTO> foundMitarbeiter = berechtigungSearchService.getSeachResult(berechtigungViewFilter.getVornameFilter(),
                berechtigungViewFilter.getNachnameFilter(), berechtigungViewFilter.getRolleFilter(),
                berechtigungViewFilter.getAbteilungFilter(), berechtigungViewFilter.getSensorCocFilterLesend(),
                berechtigungViewFilter.getSensorCocFilterSchreibend(), berechtigungViewFilter.getModulSeTeamFilter(),
                berechtigungViewFilter.getDerivatFilter(), berechtigungViewFilter.gettTeamFilter(), berechtigungViewFilter.getZakEinordnungFilter(),
                berechtigungViewFilter.getNurMitBerechtigungFilter());

        MatcherAssert.assertThat(foundMitarbeiter, IsCollectionWithSize.hasSize(1));

    }

    @Test
    void getSearchResultForModulSeTeamFilter() {

        UrlParameter urlParameter = new UrlParameter();
        urlParameter.addOrReplaceParamter("modulSeTeamFilter", String.valueOf(modulSeTeam.getId()));
        urlParameter.addOrReplaceParamter("nurBerechtigungFilter", "1");
        buildBerechtigungViewFilter(urlParameter);

        List<MitarbeiterDTO> foundMitarbeiter = berechtigungSearchService.getSeachResult(berechtigungViewFilter.getVornameFilter(),
                berechtigungViewFilter.getNachnameFilter(), berechtigungViewFilter.getRolleFilter(),
                berechtigungViewFilter.getAbteilungFilter(), berechtigungViewFilter.getSensorCocFilterLesend(),
                berechtigungViewFilter.getSensorCocFilterSchreibend(), berechtigungViewFilter.getModulSeTeamFilter(),
                berechtigungViewFilter.getDerivatFilter(), berechtigungViewFilter.gettTeamFilter(), berechtigungViewFilter.getZakEinordnungFilter(),
                berechtigungViewFilter.getNurMitBerechtigungFilter());

        MatcherAssert.assertThat(foundMitarbeiter, IsCollectionWithSize.hasSize(1));

    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getSearchResultForRoles(Rolle rolle) {
        UrlParameter urlParameter = new UrlParameter();
        urlParameter.addOrReplaceParamter("rolle", String.valueOf(rolle.getId()));
        buildBerechtigungViewFilter(urlParameter);

        List<MitarbeiterDTO> foundMitarbeiter = berechtigungSearchService.getSeachResult(berechtigungViewFilter.getVornameFilter(),
                berechtigungViewFilter.getNachnameFilter(), berechtigungViewFilter.getRolleFilter(),
                berechtigungViewFilter.getAbteilungFilter(), berechtigungViewFilter.getSensorCocFilterLesend(),
                berechtigungViewFilter.getSensorCocFilterSchreibend(), berechtigungViewFilter.getModulSeTeamFilter(),
                berechtigungViewFilter.getDerivatFilter(), berechtigungViewFilter.gettTeamFilter(), berechtigungViewFilter.getZakEinordnungFilter(),
                berechtigungViewFilter.getNurMitBerechtigungFilter());

        switch (rolle) {
            case SENSOR_EINGESCHRAENKT:
            case LESER:
                MatcherAssert.assertThat(foundMitarbeiter, IsCollectionWithSize.hasSize(0));
                break;
            default:
                MatcherAssert.assertThat(foundMitarbeiter, IsCollectionWithSize.hasSize(1));
                break;
        }

    }


}
