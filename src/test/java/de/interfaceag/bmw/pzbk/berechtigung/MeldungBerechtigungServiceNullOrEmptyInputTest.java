package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableWithSize.iterableWithSize;

@ExtendWith(MockitoExtension.class)
class MeldungBerechtigungServiceNullOrEmptyInputTest {

    @Mock
    private Session session;
    @Mock
    private MeldungBerechtigungDao meldungBerechtigungDao;
    @InjectMocks
    private MeldungBerechtigungService meldungBerechtigungService;

    @Test
    void restrictToAuthorizedMeldungenNullInput() {
        final Collection<Long> result = meldungBerechtigungService.restrictToAuthorizedMeldungen(null);
        assertThat(result, iterableWithSize(0));
    }

    @Test
    void restrictToAuthorizedMeldungenEmptyInput() {
        final Collection<Long> result = meldungBerechtigungService.restrictToAuthorizedMeldungen(Collections.emptyList());
        assertThat(result, iterableWithSize(0));
    }

}