package de.interfaceag.bmw.pzbk.berechtigung.dto;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class UserPermissionsDTONewAnforderungTest {

    private UserPermissionsDTO userPermissions;

    @Mock
    private TteamMitgliedBerechtigung tteamMitgliedBerechtigung;
    @Mock
    private TteamVertreterBerechtigung tteamVertreterBerechtigung;
    @Mock
    private List<BerechtigungDto> berechtigungen;

    private List<Rolle> rollen;

    @BeforeEach
    public void setUp() {
        rollen = new ArrayList<>();
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetRolesWithWritePermissionsForNewMeldung(Rolle rolle) {

        setupUserPermissionsForRole(rolle);

        Set<Rolle> result = userPermissions.getRolesWithWritePermissionsForNewMeldung();

        switch (rolle) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
                MatcherAssert.assertThat(result, Matchers.contains(rolle));
                break;
            default:
                MatcherAssert.assertThat(result, Matchers.empty());
        }

    }

    private void setupUserPermissionsForRole(Rolle rolle) {
        rollen.add(rolle);

        userPermissions = new UserPermissionsDTO(rollen,
                berechtigungen,
                tteamMitgliedBerechtigung,
                tteamVertreterBerechtigung
        );
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetRolesWithWritePermissionsForNewAnforderung(Rolle rolle) {

        setupUserPermissionsForRole(rolle);

        Set<Rolle> result = userPermissions.getRolesWithWritePermissionsForNewAnforderung();

        switch (rolle) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case T_TEAMLEITER:
            case TTEAMMITGLIED:
            case TTEAM_VERTRETER:
                MatcherAssert.assertThat(result, Matchers.contains(rolle));
                break;
            default:
                MatcherAssert.assertThat(result, Matchers.empty());
        }

    }

}
