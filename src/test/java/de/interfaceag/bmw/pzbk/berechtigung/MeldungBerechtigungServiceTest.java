package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableWithSize.iterableWithSize;
import static org.hamcrest.core.IsIterableContaining.hasItems;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MeldungBerechtigungServiceTest {

    @Mock
    private Session session;
    @Mock
    private MeldungBerechtigungDao meldungBerechtigungDao;
    @InjectMocks
    private MeldungBerechtigungService meldungBerechtigungService;

    @Mock
    private UserPermissions<BerechtigungDto> userPermissions;
    @Mock
    private Mitarbeiter sensor;

    private Collection<Long> inputIds;
    private Collection<Long> inputIdsSchreibend;
    private Collection<Long> allIds;
    private Set<Rolle> roles;

    @BeforeEach
    void setUp() {
        inputIds = asList(1L, 2L, 42L);
        inputIdsSchreibend = new ArrayList<>(asList(1L, 2L));
        allIds = asList(1L, 2L, 3L, 5L, 42L);
        roles = new HashSet<>();

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRoles()).thenReturn(roles);
    }

    @Test
    void restrictToAuthorizedMeldungenWithRoleAdmin() {
        roles.add(Rolle.ADMIN);
        when(meldungBerechtigungDao.getAllMeldungIds()).thenReturn(allIds);
        final Collection<Long> result = meldungBerechtigungService.restrictToAuthorizedMeldungen(inputIds);
        assertThat(result, hasItems(1L, 2L, 42L));
    }

    @Test
    void restrictToAuthorizedMeldungenWithRoleAnforderer() {
        roles.add(Rolle.ANFORDERER);
        when(meldungBerechtigungDao.getAllMeldungIds()).thenReturn(allIds);
        final Collection<Long> result = meldungBerechtigungService.restrictToAuthorizedMeldungen(inputIds);
        assertThat(result, hasItems(1L, 2L, 42L));
    }

    @Test
    void restrictToAuthorizedMeldungenWithRoleSensor() {
        roles.add(Rolle.SENSOR);
        BerechtigungDto berechtigung1 = getBerechtigungDtoWithId(1L);
        BerechtigungDto berechtigung2 = getBerechtigungDtoWithId(2L);
        BerechtigungDto berechtigung42 = getBerechtigungDtoWithId(42L);

        List<BerechtigungDto> sensorCocSchreibendPermissions = asList(berechtigung1, berechtigung2);
        List<BerechtigungDto> sensorCocLesendPermissions = Collections.singletonList(berechtigung42);
        when(userPermissions.getSensorCocAsSensorSchreibend()).thenReturn(sensorCocSchreibendPermissions);
        when(userPermissions.getSensorCocAsSensorLesend()).thenReturn(sensorCocLesendPermissions);

        Set<Long> allMeldungIds = new HashSet<>(asList(1L, 2L, 42L));

        meldungBerechtigungService.restrictToAuthorizedMeldungen(inputIds);

        verify(meldungBerechtigungDao, times(1)).getAllMeldungIdsForSensorCocIds(allMeldungIds);
    }

    @Test
    void restrictToAuthorizedMeldungenWithRoleSensorEingeschraenkt() {
        roles.add(Rolle.SENSOR_EINGESCHRAENKT);
        BerechtigungDto berechtigung1 = getBerechtigungDtoWithId(1L);
        BerechtigungDto berechtigung2 = getBerechtigungDtoWithId(2L);
        BerechtigungDto berechtigung42 = getBerechtigungDtoWithId(42L);

        List<BerechtigungDto> sensorCocSchreibendPermissions = asList(berechtigung1, berechtigung2, berechtigung42);
        when(userPermissions.getSensorCocAsSensorEingSchreibend()).thenReturn(sensorCocSchreibendPermissions);
        when(session.getUser()).thenReturn(sensor);

        List<Long> allMeldungIds = asList(1L, 2L, 42L);

        meldungBerechtigungService.restrictToAuthorizedMeldungen(inputIds);

        verify(meldungBerechtigungDao, times(1)).getAllMeldungIdsForSensorCocIdsWithUserAsSensor(allMeldungIds, sensor);
    }

    @Test
    void restrictToAuthorizedMeldungenWithRoleSensorCocLeiter() {
        roles.add(Rolle.SENSORCOCLEITER);
        BerechtigungDto berechtigung1 = getBerechtigungDtoWithId(1L);
        BerechtigungDto berechtigung2 = getBerechtigungDtoWithId(2L);

        List<BerechtigungDto> sensorCocSchreibendPermissions = asList(berechtigung1, berechtigung2);
        when(userPermissions.getSensorCocAsSensorCocLeiterSchreibend()).thenReturn(sensorCocSchreibendPermissions);

        List<Long> sensorCocIdsSchreibend = new ArrayList<>(asList(1L, 2L));

        meldungBerechtigungService.restrictToAuthorizedMeldungen(inputIds);

        verify(meldungBerechtigungDao, times(1)).getAllMeldungIdsForSensorCocIds(sensorCocIdsSchreibend);
    }

    @Test
    void restrictToAuthorizedMeldungenWithRoleSensorCocVertreter() {
        roles.add(Rolle.SCL_VERTRETER);
        BerechtigungDto berechtigung1 = getBerechtigungDtoWithId(1L);
        BerechtigungDto berechtigung2 = getBerechtigungDtoWithId(2L);

        List<BerechtigungDto> sensorCocSchreibendPermissions = asList(berechtigung1, berechtigung2);
        when(userPermissions.getSensorCocAsVertreterSCLSchreibend()).thenReturn(sensorCocSchreibendPermissions);

        List<Long> sensorCocIdsSchreibend = new ArrayList<>(asList(1L, 2L));

        meldungBerechtigungService.restrictToAuthorizedMeldungen(inputIds);

        verify(meldungBerechtigungDao, times(1)).getAllMeldungIdsForSensorCocIds(sensorCocIdsSchreibend);
    }

    @Test
    void restrictToAuthorizedMeldungenWithRoleTteamLeiter() {
        roles.add(Rolle.T_TEAMLEITER);
        final Collection<Long> result = meldungBerechtigungService.restrictToAuthorizedMeldungen(inputIds);
        assertThat(result, iterableWithSize(0));
    }

    @Test
    void restrictToAuthorizedMeldungenWithRoleTteamVertreter() {
        roles.add(Rolle.TTEAM_VERTRETER);
        final Collection<Long> result = meldungBerechtigungService.restrictToAuthorizedMeldungen(inputIds);
        assertThat(result, iterableWithSize(0));
    }

    @Test
    void restrictToAuthorizedMeldungenWithRoleTteamMitglied() {
        roles.add(Rolle.TTEAMMITGLIED);
        final Collection<Long> result = meldungBerechtigungService.restrictToAuthorizedMeldungen(inputIds);
        assertThat(result, iterableWithSize(0));
    }

    @Test
    void restrictToAuthorizedMeldungenWithRoleLeser() {
        roles.add(Rolle.LESER);
        final Collection<Long> result = meldungBerechtigungService.restrictToAuthorizedMeldungen(inputIds);
        assertThat(result, iterableWithSize(0));
    }

    @Test
    void restrictToAuthorizedMeldungenWithRoleEcoc() {
        roles.add(Rolle.E_COC);
        final Collection<Long> result = meldungBerechtigungService.restrictToAuthorizedMeldungen(inputIds);
        assertThat(result, iterableWithSize(0));
    }

    @Test
    void restrictToAuthorizedMeldungenWithRoleUmsetzungsbestaetiger() {
        roles.add(Rolle.UMSETZUNGSBESTAETIGER);
        final Collection<Long> result = meldungBerechtigungService.restrictToAuthorizedMeldungen(inputIds);
        assertThat(result, iterableWithSize(0));
    }

    private static BerechtigungDto getBerechtigungDtoWithId(Long id) {
        return new BerechtigungDto() {
            @Override
            public Long getId() {
                return id;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public Rechttype getRechttype() {
                return null;
            }

            @Override
            public Rolle getRolle() {
                return null;
            }

            @Override
            public BerechtigungZiel getType() {
                return null;
            }
        };
    }
}
