package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableWithSize.iterableWithSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AnforderungBerechtigungServiceTest {

    @Mock
    private Session session;
    @Mock
    private AnforderungBerechtigungDao anforderungBerechtigungDao;
    @InjectMocks
    private AnforderungBerechtigungService anforderungBerechtigungService;

    @Mock
    private UserPermissions<BerechtigungDto> userPermissions;
    @Mock
    private Mitarbeiter sensor;

    private Collection<Long> inputIds;
    private Collection<Long> inputIdsSchreibend;
    private Collection<Long> allIds;
    private Set<Rolle> roles;

    @BeforeEach
    void setUp() {
        inputIds = new ArrayList<>(asList(1L, 2L, 42L));
        inputIdsSchreibend = new ArrayList<>(asList(1L, 2L));
        allIds = asList(1L, 2L, 3L, 5L, 42L);
        roles = new HashSet<>();

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRoles()).thenReturn(roles);
    }

    @Test
    void restrictToAuthorizedAnforderungenWithRoleAdmin() {
        roles.add(Rolle.ADMIN);
        when(anforderungBerechtigungDao.getAllAnforderungIds()).thenReturn(allIds);
        final Collection<Long> result = anforderungBerechtigungService.restrictToAuthorizedAnforderungen(inputIds);
        assertThat(result, hasItems(1L, 2L, 42L));
    }

    @Test
    void restrictToAuthorizedAnforderungenWithRoleAnforderer() {
        roles.add(Rolle.ANFORDERER);
        when(anforderungBerechtigungDao.getAllAnforderungIds()).thenReturn(allIds);
        final Collection<Long> result = anforderungBerechtigungService.restrictToAuthorizedAnforderungen(inputIds);
        assertThat(result, hasItems(1L, 2L, 42L));
    }

    @Test
    void restrictToAuthorizedAnforderungenWithRoleSensor() {
        roles.add(Rolle.SENSOR);
        BerechtigungDto berechtigung1 = getBerechtigungDtoWithId(1L);
        BerechtigungDto berechtigung2 = getBerechtigungDtoWithId(2L);
        BerechtigungDto berechtigung42 = getBerechtigungDtoWithId(42L);

        List<BerechtigungDto> sensorCocSchreibendPermissions = asList(berechtigung1, berechtigung2);
        List<BerechtigungDto> sensorCocLesendPermissions = Collections.singletonList(berechtigung42);
        when(userPermissions.getSensorCocAsSensorSchreibend()).thenReturn(sensorCocSchreibendPermissions);
        when(userPermissions.getSensorCocAsSensorLesend()).thenReturn(sensorCocLesendPermissions);

        Set<Long> allSensorCocIds = new HashSet<>(asList(1L, 2L, 42L));

        when(anforderungBerechtigungDao.getAllAnforderungIdsForSensorCocIds(any())).thenReturn(allSensorCocIds);

        anforderungBerechtigungService.restrictToAuthorizedAnforderungen(inputIds);

        verify(anforderungBerechtigungDao, times(1)).getAllAnforderungIdsForSensorCocIds(allSensorCocIds);
    }

    @Test
    void restrictToAuthorizedAnforderungenWithRoleSensorEingeschraenkt() {
        roles.add(Rolle.SENSOR_EINGESCHRAENKT);
        BerechtigungDto berechtigung1 = getBerechtigungDtoWithId(1L);
        BerechtigungDto berechtigung2 = getBerechtigungDtoWithId(2L);
        BerechtigungDto berechtigung42 = getBerechtigungDtoWithId(42L);

        List<BerechtigungDto> sensorCocSchreibendPermissions = asList(berechtigung1, berechtigung2, berechtigung42);
        when(userPermissions.getSensorCocAsSensorEingSchreibend()).thenReturn(sensorCocSchreibendPermissions);
        when(session.getUser()).thenReturn(sensor);

        List<Long> allSensorCocIds = asList(1L, 2L, 42L);

        when(anforderungBerechtigungDao.getAllAnforderungIdsForSensorCocIdsWithUserAsSensor(any(), any())).thenReturn(allSensorCocIds);

        anforderungBerechtigungService.restrictToAuthorizedAnforderungen(inputIds);

        verify(anforderungBerechtigungDao, times(1)).getAllAnforderungIdsForSensorCocIdsWithUserAsSensor(allSensorCocIds, sensor);
    }

    @Test
    void restrictToAuthorizedAnforderungenWithRoleSensorCocLeiter() {
        roles.add(Rolle.SENSORCOCLEITER);
        BerechtigungDto berechtigung1 = getBerechtigungDtoWithId(1L);
        BerechtigungDto berechtigung2 = getBerechtigungDtoWithId(2L);

        List<BerechtigungDto> sensorCocSchreibendPermissions = asList(berechtigung1, berechtigung2);
        when(userPermissions.getSensorCocAsSensorCocLeiterSchreibend()).thenReturn(sensorCocSchreibendPermissions);

        List<Long> sensorCocIdsSchreibend = new ArrayList<>(asList(1L, 2L));

        when(anforderungBerechtigungDao.getAllAnforderungIdsForSensorCocIds(any())).thenReturn(sensorCocIdsSchreibend);

        anforderungBerechtigungService.restrictToAuthorizedAnforderungen(inputIdsSchreibend);

        verify(anforderungBerechtigungDao, times(1)).getAllAnforderungIdsForSensorCocIds(sensorCocIdsSchreibend);
    }

    @Test
    void restrictToAuthorizedAnforderungenWithRoleSensorCocVertreter() {
        roles.add(Rolle.SCL_VERTRETER);
        BerechtigungDto berechtigung1 = getBerechtigungDtoWithId(1L);
        BerechtigungDto berechtigung2 = getBerechtigungDtoWithId(2L);

        List<BerechtigungDto> sensorCocSchreibendPermissions = asList(berechtigung1, berechtigung2);
        when(userPermissions.getSensorCocAsVertreterSCLSchreibend()).thenReturn(sensorCocSchreibendPermissions);

        List<Long> sensorCocIdsSchreibend = new ArrayList<>(asList(1L, 2L));

        when(anforderungBerechtigungDao.getAllAnforderungIdsForSensorCocIds(any())).thenReturn(sensorCocIdsSchreibend);

        anforderungBerechtigungService.restrictToAuthorizedAnforderungen(inputIdsSchreibend);

        verify(anforderungBerechtigungDao, times(1)).getAllAnforderungIdsForSensorCocIds(sensorCocIdsSchreibend);
    }

    @Test
    void restrictToAuthorizedAnforderungenWithRoleTteamLeiter() {
        roles.add(Rolle.T_TEAMLEITER);
        BerechtigungDto berechtigung1 = getBerechtigungDtoWithId(1L);
        BerechtigungDto berechtigung2 = getBerechtigungDtoWithId(2L);

        List<BerechtigungDto> tteamPermissions = asList(berechtigung1, berechtigung2);
        when(userPermissions.getTteamAsTteamleiterSchreibend()).thenReturn(tteamPermissions);

        List<Long> allTteamIds = new ArrayList<>(asList(1L, 2L));

        when(anforderungBerechtigungDao.getAllAnforderungIdsForTteamIds(any())).thenReturn(allTteamIds);

        anforderungBerechtigungService.restrictToAuthorizedAnforderungen(inputIds);

        verify(anforderungBerechtigungDao, times(1)).getAllAnforderungIdsForTteamIds(allTteamIds);
    }

    @Test
    void restrictToAuthorizedAnforderungenWithRoleTteamVertreter() {
        roles.add(Rolle.TTEAM_VERTRETER);

        List<Long> tteamPermissions = asList(1L, 2L);
        when(userPermissions.getTteamIdsForTteamVertreter()).thenReturn(tteamPermissions);

        List<Long> allTteamIds = new ArrayList<>(asList(1L, 2L));

        when(anforderungBerechtigungDao.getAllAnforderungIdsForTteamIds(any())).thenReturn(allTteamIds);

        anforderungBerechtigungService.restrictToAuthorizedAnforderungen(inputIds);

        verify(anforderungBerechtigungDao, times(1)).getAllAnforderungIdsForTteamIds(allTteamIds);
    }

    @Test
    void restrictToAuthorizedAnforderungenWithRoleTteamMitglied() {
        roles.add(Rolle.TTEAMMITGLIED);

        List<Long> tteamPermissionsSchreiebend = asList(1L, 2L);
        List<Long> tteamPermissionsLesend = Collections.singletonList(42L);
        when(userPermissions.getTteamIdsForTteamMitgliedSchreibend()).thenReturn(tteamPermissionsSchreiebend);
        when(userPermissions.getTteamIdsForTteamMitgliedLesend()).thenReturn(tteamPermissionsLesend);

        Set<Long> allTteamIds = new HashSet<>(asList(1L, 2L, 42L));

        when(anforderungBerechtigungDao.getAllAnforderungIdsForTteamIds(any())).thenReturn(allTteamIds);

        anforderungBerechtigungService.restrictToAuthorizedAnforderungen(inputIds);

        verify(anforderungBerechtigungDao, times(1)).getAllAnforderungIdsForTteamIds(allTteamIds);
    }

    @Test
    void restrictToAuthorizedAnforderungenWithRoleLeser() {
        roles.add(Rolle.LESER);
        final Collection<Long> result = anforderungBerechtigungService.restrictToAuthorizedAnforderungen(inputIds);
        assertThat(result, iterableWithSize(0));
    }

    @Test
    void restrictToAuthorizedAnforderungenWithRoleEcoc() {
        roles.add(Rolle.E_COC);

        BerechtigungDto berechtigung1 = getBerechtigungDtoWithId(1L);
        BerechtigungDto berechtigung2 = getBerechtigungDtoWithId(2L);
        List<BerechtigungDto> modulSeTeams = asList(berechtigung1, berechtigung2);
        when(userPermissions.getModulSeTeamAsEcocSchreibend()).thenReturn(modulSeTeams);

        List<Long> allModulSeTeams = new ArrayList<>(asList(1L, 2L));

        when(anforderungBerechtigungDao.getAllAnforderungIdsForModulSeTeamIds(any())).thenReturn(allModulSeTeams);

        anforderungBerechtigungService.restrictToAuthorizedAnforderungen(inputIds);

        verify(anforderungBerechtigungDao, times(1)).getAllAnforderungIdsForModulSeTeamIds(allModulSeTeams);
    }

    @Test
    void restrictToAuthorizedAnforderungenWithRoleUmsetzungsbestaetiger() {
        roles.add(Rolle.UMSETZUNGSBESTAETIGER);
        BerechtigungDto berechtigung1 = getBerechtigungDtoWithId(1L);
        BerechtigungDto berechtigung2 = getBerechtigungDtoWithId(2L);

        List<BerechtigungDto> sensorCocSchreibendPermissions = asList(berechtigung1, berechtigung2);
        when(userPermissions.getSensorCocAsUmsetzungsbestaetigerSchreibend()).thenReturn(sensorCocSchreibendPermissions);

        List<Long> allSensorCocIds = new ArrayList<>(asList(1L, 2L));

        when(anforderungBerechtigungDao.getAllAnforderungIdsForSensorCocIds(any())).thenReturn(allSensorCocIds);

        anforderungBerechtigungService.restrictToAuthorizedAnforderungen(inputIds);

        verify(anforderungBerechtigungDao, times(1)).getAllAnforderungIdsForSensorCocIds(allSensorCocIds);
    }

    private static BerechtigungDto getBerechtigungDtoWithId(Long id) {
        return new BerechtigungDto() {
            @Override
            public Long getId() {
                return id;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public Rechttype getRechttype() {
                return null;
            }

            @Override
            public Rolle getRolle() {
                return null;
            }

            @Override
            public BerechtigungZiel getType() {
                return null;
            }
        };
    }
}
