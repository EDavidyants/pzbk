package de.interfaceag.bmw.pzbk.berechtigung;

import de.interfaceag.bmw.pzbk.dao.BerechtigungDao;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.UserToRole;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.services.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author fn
 */
@ExtendWith(MockitoExtension.class)
public class QnumberChangeServiceTest {

    @Mock
    BerechtigungDao berechtigungDao;
    @Mock
    BerechtigungSearchService berechtigungSearchService;
    @Mock
    UserService userService;

    @InjectMocks
    QnumberChangeService qnumberChangeService;

    Mitarbeiter mitarbeiter;

    @BeforeEach
    public void setUp() {
        mitarbeiter = TestDataFactory.createMitarbeiter("test");
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "a", "asdfghj", "q1234567", "aq123456", "aq1234567"})
    public void testChangeQnumberWrongQnumber(String qnumber) throws Exception {
        Boolean result = qnumberChangeService.changeQnumber(mitarbeiter, qnumber);
        Assertions.assertFalse(result);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 4, 5})
    public void testChangeQnumberPersistUpdatedRoles(int i) throws Exception {

        when(berechtigungDao.getUserToRolesByQnumber(any())).thenReturn(getUserToRoleList(i));
        when(berechtigungSearchService.getAllRolesForUserFromBerechtigung(any())).thenReturn(getRoleSetForSearchService());

        qnumberChangeService.changeQnumber(mitarbeiter, "QXXXXXX");

        verify(berechtigungDao, times(i)).persistRolle(any());

    }

    @Test
    public void testChangeQnumberPersistUpdatedRolesInUserToRole() throws Exception {

        when(berechtigungDao.getUserToRolesByQnumber(any())).thenReturn(getUserToRoleList(1));
        Set<Rolle> userRolesFromBerechtigung = getRoleSetForSearchService();
        userRolesFromBerechtigung.add(Rolle.ADMIN);
        when(berechtigungSearchService.getAllRolesForUserFromBerechtigung(any())).thenReturn(getRoleSetForSearchService());

        qnumberChangeService.changeQnumber(mitarbeiter, "QXXXXXX");

        verify(berechtigungDao, times(1)).persistRolle(any());

    }

    private Set<Rolle> getRoleSetForSearchService() {
        Set<Rolle> returnSet = new HashSet<>();
        returnSet.add(Rolle.SENSOR);
        return returnSet;
    }

    private List<UserToRole> getUserToRoleList(int numberOfUserToRoles) {
        List<UserToRole> returnList = new ArrayList<>();
        for (int i = numberOfUserToRoles; i > 0; i--) {
            returnList.add(new UserToRole("testName", "SENSOR"));
        }
        return returnList;
    }

}
