package de.interfaceag.bmw.pzbk.timer;

import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.TimerType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static de.interfaceag.bmw.pzbk.shared.utils.DateUtils.isMonday;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class KovaPhaseImDerivatTimerConditionTest {

    private static final Date NOW = new Date();

    private LocalDate startDate;
    private LocalDate endDate;
    private KovAStatus status;

    @Mock
    private KovAPhaseImDerivat kovaPhaseImDerivat;

    @BeforeEach
    void setUp() {
        startDate = Instant.ofEpochMilli(NOW.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        endDate = Instant.ofEpochMilli(NOW.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    @Test
    void isConditionMetForKovaPhaseImDerivatAndTimerTypeAktivSameDay() {
        setupStartDate();
        setupStatus();

        final boolean result = KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer(kovaPhaseImDerivat, TimerType.PHASE_AKTIV);
        assertTrue(result);
    }

    @Test
    void isConditionMetForKovaPhaseImDerivatAndTimerTypeAktivYesterdayAndBeforeEndDate() {
        startDate = startDate.minusDays(1L);
        endDate = endDate.plusDays(1L);

        setupStartDate();
        setupEndDate();
        setupStatus();

        final boolean result = KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer(kovaPhaseImDerivat, TimerType.PHASE_AKTIV);
        assertTrue(result);
    }

    @Test
    void isConditionMetForKovaPhaseImDerivatAndTimerTypeAktivYesterdayAndAfterEndDate() {
        startDate = startDate.minusDays(5L);
        endDate = endDate.minusDays(4L);

        setupStartDate();
        setupEndDate();
        setupStatus();

        final boolean result = KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer(kovaPhaseImDerivat, TimerType.PHASE_AKTIV);
        assertFalse(result);
    }

    @Test
    void isConditionMetForKovaPhaseImDerivatAndTimerTypeAktivTomorrow() {
        startDate = startDate.plusDays(1L);

        setupStartDate();
        setupStatus();

        final boolean result = KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer(kovaPhaseImDerivat, TimerType.PHASE_AKTIV);
        assertFalse(result);
    }

    @Test
    void isConditionMetForKovaPhaseImDerivatAndTimerTypeAbgeschlossenSameDay() {
        setupEndDate();
        setupStatus();

        final boolean result = KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer(kovaPhaseImDerivat, TimerType.PHASE_ABGESCHLOSSEN);
        assertFalse(result);
    }

    @Test
    void isConditionMetForKovaPhaseImDerivatAndTimerTypeAbgeschlossenOneWeekLater() {
        endDate = endDate.minusDays(8L);

        setupEndDate();
        setupStatus();

        final boolean result = KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer(kovaPhaseImDerivat, TimerType.PHASE_ABGESCHLOSSEN);
        assertTrue(result);
    }

    @Test
    void isConditionMetForKovaPhaseImDerivatAndTimerTypeFirstNotifivationForSensorCocLeiterSameDay() {
        setupStartDate();

        final boolean result = KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer(kovaPhaseImDerivat, TimerType.FIRST_NOTIFICATION_FOR_FTS);
        assertFalse(result);
    }

    @ParameterizedTest
    @ValueSource(longs = {1L, 2L, 3L, 4L, 5L, 6L})
    void isConditionMetForKovaPhaseImDerivatAndTimerTypeFirstNotifivationForSensorCocLeiterWeeksBefore(Long weeks) {
        startDate = startDate.plusWeeks(weeks);
        setupStartDate();

        final boolean result = KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer(kovaPhaseImDerivat, TimerType.FIRST_NOTIFICATION_FOR_FTS);

        if (weeks == 4L) {
            assertTrue(result);
        } else {
            assertFalse(result);
        }
    }

    @Test
    void isConditionMetForKovaPhaseImDerivatAndTimerTypeBewertungsauftragSameDay() {
        setupStartDate();

        final boolean result = KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer(kovaPhaseImDerivat, TimerType.BEWERTUNGSAUFTRAG);
        assertFalse(result);
    }

    @ParameterizedTest
    @ValueSource(longs = {1L, 2L, 3L, 4L, 5L, 6L})
    void isConditionMetForKovaPhaseImDerivatAndTimerTypeBewertungsauftragrWeeksBefore(Long weeks) {
        startDate = startDate.plusWeeks(weeks);
        setupStartDate();

        final boolean result = KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer(kovaPhaseImDerivat, TimerType.BEWERTUNGSAUFTRAG);

        if (weeks == 1L) {
            assertTrue(result);
        } else {
            assertFalse(result);
        }
    }

    @Test
    void isConditionMetForKovaPhaseImDerivatAndTimerTypeReminderSameDay() {
        setupStartDate();

        final boolean result = KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer(kovaPhaseImDerivat, TimerType.REMINDER);
        assertFalse(result);
    }

    @ParameterizedTest
    @ValueSource(longs = {1L, 2L, 3L, 4L, 5L, 6L})
    void isConditionMetForKovaPhaseImDerivatAndTimerTypeReminderWeeksAfter(Long weeks) {
        startDate = startDate.minusWeeks(weeks);
        setupStartDate();

        final boolean result = KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer(kovaPhaseImDerivat, TimerType.REMINDER);

        if (weeks == 1L) {
            assertTrue(result);
        } else {
            assertFalse(result);
        }
    }

    @ParameterizedTest
    @ValueSource(longs = {1L, 2L, 3L, 4L, 5L, 6L})
    void isConditionMetForKovaPhaseImDerivatAndTimerTypeLastCallDaysBefore(Long days) {
        endDate = endDate.minusDays(days);
        setupEndDate();

        final boolean result = KovaPhaseImDerivatTimerCondition.isConditionMetForKovaPhaseImDerivatAndTimer(kovaPhaseImDerivat, TimerType.LAST_CALL);

        if (isMonday(NOW)) {
            if (days == 1L) {
                assertTrue(result);
            } else {
                assertFalse(result);
            }
        } else {
            assertFalse(result);
        }
    }

    private void setupEndDate() {
        when(kovaPhaseImDerivat.getEndDate()).thenReturn(endDate);
    }

    private void setupStartDate() {
        when(kovaPhaseImDerivat.getStartDate()).thenReturn(startDate);
    }

    private void setupStatus() {
        when(kovaPhaseImDerivat.getKovaStatus()).thenReturn(status);
    }
}