package de.interfaceag.bmw.pzbk.timer;

import de.interfaceag.bmw.pzbk.dao.KovAPhaseImDerivatDao;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.TimerType;
import de.interfaceag.bmw.pzbk.mail.KovaPhaseImDerivatMailService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungBerechtigungService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungService;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsMapContaining;
import org.hamcrest.collection.IsMapWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static de.interfaceag.bmw.pzbk.shared.utils.DateUtils.isMonday;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class KovaPhaseImDerivatTimerServiceTest {

    private static final Date NOW = new Date();

    @Mock
    private KovAPhaseImDerivatDao kovAPhaseImDerivatDao;
    @Mock
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;
    @Mock
    private UmsetzungsbestaetigungBerechtigungService umsetzungsbestaetigungBerechtigungService;
    @Mock
    private KovaPhaseImDerivatMailService kovaPhaseImDerivatMailService;
    @InjectMocks
    private KovaPhaseImDerivatTimerService kovaPhaseImDerivatTimerService;

    private LocalDate startDate;
    private LocalDate endDate;
    private List<KovAPhaseImDerivat> kovAPhasenImDerivat;

    @Mock
    private KovAPhaseImDerivat kovAPhaseImDerivat;

    @BeforeEach
    void setUp() {
        startDate = Instant.ofEpochMilli(NOW.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        endDate = Instant.ofEpochMilli(NOW.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        kovAPhasenImDerivat = Collections.singletonList(kovAPhaseImDerivat);
    }

    @Test
    void updateKovaPhasenImDerivatSendFirstNotificationToSensorCocLeiterAndVertreter() {
        setupConfiguredPhases();

        startDate = startDate.plusWeeks(4);
        setupStartDate();

        final Map<TimerType, Integer> report = kovaPhaseImDerivatTimerService.updateKovaPhasenImDerivat();

        MatcherAssert.assertThat(report, IsMapContaining.hasKey(TimerType.FIRST_NOTIFICATION_FOR_FTS));
    }

    @Test
    void updateKovaPhasenImDerivatSendBewertungsauftrag() {
        setupConfiguredPhases();

        startDate = startDate.plusWeeks(1);
        setupStartDate();

        final Map<TimerType, Integer> report = kovaPhaseImDerivatTimerService.updateKovaPhasenImDerivat();

        MatcherAssert.assertThat(report, IsMapContaining.hasKey(TimerType.BEWERTUNGSAUFTRAG));
    }


    @Test
    void updateKovaPhasenImDerivatSendBewertungsauftragAndInsertDefaultUmsetzungsverwalter() {
        setupConfiguredPhases();

        startDate = startDate.plusWeeks(1);
        setupStartDate();

        kovaPhaseImDerivatTimerService.updateKovaPhasenImDerivat();

        verify(umsetzungsbestaetigungBerechtigungService).createAndPersistMissingUmsetzungsbestaetigungBerechtigungForKovaPhase(any());
    }

    @Test
    void updateKovaPhasenImDerivatActivateKovaPhasenImDerivat() {
        setupConfiguredPhases();

        setupStartDate();

        final Map<TimerType, Integer> report = kovaPhaseImDerivatTimerService.updateKovaPhasenImDerivat();

        MatcherAssert.assertThat(report, IsMapContaining.hasKey(TimerType.PHASE_AKTIV));
    }

    @Test
    void updateKovaPhasenImDerivatActivateKovaPhasenImDerivatAndInsertDefaultUmsetzungsverwalter() {
        setupConfiguredPhases();

        setupStartDate();

        kovaPhaseImDerivatTimerService.updateKovaPhasenImDerivat();

        verify(umsetzungsbestaetigungBerechtigungService).createAndPersistMissingUmsetzungsbestaetigungBerechtigungForKovaPhase(any());
    }

    @Test
    void updateKovaPhasenImDerivatActivateKovaPhasenImDerivatSendReminderToUmsetzungsverwalter() {
        setupActivePhases();

        startDate = startDate.minusWeeks(1);
        setupStartDate();
        setupEndDate();

        final Map<TimerType, Integer> report = kovaPhaseImDerivatTimerService.updateKovaPhasenImDerivat();

        MatcherAssert.assertThat(report, IsMapContaining.hasKey(TimerType.REMINDER));
    }

    @Test
    void updateKovaPhasenImDerivatActivateKovaPhasenImDerivatSendLastCallToUmsetzungsverwalter() {
        setupActivePhases();

        setupStartDate();

        endDate = endDate.minusDays(1L);
        setupEndDate();

        final Map<TimerType, Integer> report = kovaPhaseImDerivatTimerService.updateKovaPhasenImDerivat();

        if (isMonday(NOW)) {
            MatcherAssert.assertThat(report, IsMapContaining.hasKey(TimerType.LAST_CALL));
        } else {
            MatcherAssert.assertThat(report, IsMapWithSize.anEmptyMap());
        }
    }

    @Test
    void updateKovaPhasenImDerivatActivateKovaPhasenDeactivatePhasen() {
        setupActivePhases();

        setupStartDate();

        endDate = endDate.minusDays(8L);
        setupEndDate();

        final Map<TimerType, Integer> report = kovaPhaseImDerivatTimerService.updateKovaPhasenImDerivat();

        MatcherAssert.assertThat(report, IsMapContaining.hasKey(TimerType.PHASE_ABGESCHLOSSEN));
    }

    private void setupStartDate() {
        when(kovAPhaseImDerivat.getStartDate()).thenReturn(startDate);
    }

    private void setupEndDate() {
        when(kovAPhaseImDerivat.getEndDate()).thenReturn(endDate);
    }

    private void setupConfiguredPhases() {
        when(kovAPhaseImDerivatDao.getKovAPhaseImDerivatWithStartAndEndSet()).thenReturn(kovAPhasenImDerivat);
    }

    private void setupActivePhases() {
        when(umsetzungsbestaetigungService.getKovaphasenByKovastatusAndStatus(any(), any())).thenReturn(kovAPhasenImDerivat);
    }
}