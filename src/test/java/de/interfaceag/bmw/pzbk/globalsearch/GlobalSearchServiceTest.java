package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten.ProzessbaukastenSearchService;
import de.interfaceag.bmw.pzbk.services.ImageService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class GlobalSearchServiceTest {

    @Mock
    private UrlParameter urlParameter;

    @Mock
    private ImageService imageService;

    @Mock
    private ProzessbaukastenSearchService prozessbaukastenSearchService;

    @Mock
    private QueryPartService queryPartService;

    @InjectMocks
    private GlobalSearchService globalSearchService = new GlobalSearchService();

    private Map<String, String> urlParameters = new HashMap<>();
    private GlobalSearchFilter filter;

    private Mitarbeiter mitarbeiter;
    private List<SensorCoc> sensorCocs;
    private List<Tteam> tteams;
    private List<Modul> module;
    private List<ModulSeTeam> modulSeTeams;
    private List<Derivat> derivate;
    private List<ProzessbaukastenFilterDto> prozessbaukaesten;
    private List<FestgestelltIn> festgestelltIn;

    @BeforeEach
    public void setUp() {
        mitarbeiter = TestDataFactory.createMitarbeiter("Max");

        sensorCocs = TestDataFactory.generateSensorCocs();
        tteams = TestDataFactory.generateTteams();
        module = TestDataFactory.generateModule();
        modulSeTeams = TestDataFactory.generateModulSeTeams(module.get(0), Arrays.asList(new ModulKomponente("ppg1", "komponente1")));
        derivate = TestDataFactory.genereteDerivate();
        prozessbaukaesten = Collections.singletonList(new ProzessbaukastenFilterDto(456L, "P456", 1, "Bezeichnung"));
        festgestelltIn = TestDataFactory.generateFestgestelltInList();
        filter = new GlobalSearchFilter(urlParameter, sensorCocs, tteams, module, modulSeTeams, derivate, prozessbaukaesten, festgestelltIn, Collections.emptyList());

        when(prozessbaukastenSearchService.getSearchResultCount(filter)).thenReturn(1L);
        when(queryPartService.getSingleResultAsLong(any())).thenReturn(1L);
        when(queryPartService.getResultListWithMaxResults(any(), any(), anyInt(), anyInt())).thenReturn(new ArrayList<>());
    }

    @Test
    public void testGetGlobalSearchResultWithImageForNoSelection() {
        GlobalSearchResult searchResult = globalSearchService.getGlobalSearchResultWithImage(filter, mitarbeiter);
        Assertions.assertTrue(searchResult.getSelectedSearchResults().isEmpty());
    }

    @Test
    public void testGetGlobalSearchResultWithImageForUrlParameterWithFacesRedirectOnly() {
        GlobalSearchResult searchResult = globalSearchService.getGlobalSearchResultWithImage(filter, mitarbeiter);
        Assertions.assertEquals(searchResult.getSearchResults(), searchResult.getSelectedSearchResults());
    }

}
