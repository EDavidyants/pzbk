package de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProzessbaukastenPermissionQueryBuilderTest {

    @Mock
    private UserPermissions userPermissions;
    private Set<Rolle> roles;
    private List<Long> tteamIds;

    @BeforeEach
    public void setUp() {
        roles = new HashSet<>();
        tteamIds = new ArrayList<>();
        when(userPermissions.getRoles()).thenReturn(roles);
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetPermissionQueryPartForRole(Rolle role) {
        roles.add(role);
        QueryPartDTO result = ProzessbaukastenPermissionQueryBuilder.getPermissionQueryPart(userPermissions);

        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case ANFORDERER:
                assertEquals("", result.getQuery());
                break;
            case TTEAMMITGLIED: // no tteamid in list results in no result
            default:
                assertEquals(" AND 1 = 0 ", result.getQuery());
        }
    }

    @Test
    public void testGetPermissionQueryPartForTteamMitgliedWithoutPermission() {
        when(userPermissions.getTteamIdsForTteamMitgliedLesend()).thenReturn(tteamIds);
        roles.add(Rolle.TTEAMMITGLIED);
        QueryPartDTO result = ProzessbaukastenPermissionQueryBuilder.getPermissionQueryPart(userPermissions);
        assertEquals(" AND 1 = 0 ", result.getQuery());
    }

    @Test
    public void testGetPermissionQueryPartForTteamMitgliedWithPermissionQuery() {
        when(userPermissions.getTteamIdsForTteamMitgliedLesend()).thenReturn(tteamIds);
        roles.add(Rolle.TTEAMMITGLIED);
        tteamIds.add(1L);
        QueryPartDTO result = ProzessbaukastenPermissionQueryBuilder.getPermissionQueryPart(userPermissions);
        assertEquals(" AND p.tteam.id IN :tteamMitgliedTteamIds ", result.getQuery());
    }

    @Test
    public void testGetPermissionQueryPartForTteamMitgliedWithPermissionParameterMapSize() {
        when(userPermissions.getTteamIdsForTteamMitgliedLesend()).thenReturn(tteamIds);
        roles.add(Rolle.TTEAMMITGLIED);
        tteamIds.add(1L);
        QueryPartDTO result = ProzessbaukastenPermissionQueryBuilder.getPermissionQueryPart(userPermissions);
        assertEquals(1, result.getParameterMap().size());
    }

    @Test
    public void testGetPermissionQueryPartForTteamMitgliedWithPermissionParameterMapValue() {
        when(userPermissions.getTteamIdsForTteamMitgliedLesend()).thenReturn(tteamIds);
        roles.add(Rolle.TTEAMMITGLIED);
        tteamIds.add(1L);
        QueryPartDTO result = ProzessbaukastenPermissionQueryBuilder.getPermissionQueryPart(userPermissions);
        assertEquals(tteamIds, result.getParameterMap().get("tteamMitgliedTteamIds"));
    }

}
