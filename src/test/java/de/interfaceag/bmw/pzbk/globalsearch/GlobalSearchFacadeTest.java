package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class GlobalSearchFacadeTest {

    @Mock
    private Session session;

    @Mock
    private GlobalSearchService globalSearchService;

    @Mock
    private GlobalSearchViewData viewData;

    @Mock
    private GlobalSearchFilter filter;

    @InjectMocks
    private GlobalSearchFacade facade;

    private Mitarbeiter user;
    private SearchResultDTO element1;
    private SearchResultDTO element2;
    private SearchResultDTO element3;
    private List<SearchResultDTO> searchResults;

    @BeforeEach
    public void setUp() {
        element1 = new SearchResultDTO(1L, "M11", "Meldung", "beschreibung1", "beschreibungStaerkeSchwaeche1", "zugeordnet", "LaKa_TOF", "");
        element1.setSelected(true);
        element2 = new SearchResultDTO(21L, "A21", "Anforderung", "beschreibung21", "beschreibungStaerkeSchwaeche21", "in Arbeit", "T-E/E Antrieb", "V1");
        element2.setSelected(false);
        element3 = new SearchResultDTO(21L, "A22", "Anforderung", "beschreibung22", "beschreibungStaerkeSchwaeche22", "abgestimmt", "T-E/E Analyse", "V1");
        element3.setSelected(true);
        searchResults = Arrays.asList(element1, element2, element3);
        user = TestDataFactory.generateMitarbeiter("Max", "Admin", "Abt.-2", "q2222222");
        doNothing().when(viewData).setSelectedSearchResults(any());
    }

    @Test
    public void testGetSelectedSearchResultsForAllSelected() {
        when(session.getUser()).thenReturn(user);
        when(viewData.isAllSelected()).thenReturn(Boolean.TRUE);
        when(viewData.getSearchFilter()).thenReturn(filter);
        when(globalSearchService.getAllSearchResultsWithoutImage(filter, user)).thenReturn(searchResults);

        List<SearchResultDTO> fact = facade.getSelectedSearchResults(viewData);
        Assertions.assertSame(searchResults, fact);
    }

    @Test
    public void testGetSelectedSearchResultsForNotAllSelected() {
        when(viewData.isAllSelected()).thenReturn(Boolean.FALSE);
        when(viewData.getSearchResults()).thenReturn(searchResults);

        List<SearchResultDTO> expected = Arrays.asList(element1, element3);
        List<SearchResultDTO> fact = facade.getSelectedSearchResults(viewData);
        Assertions.assertEquals(expected, fact);
    }
}
