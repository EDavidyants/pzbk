package de.interfaceag.bmw.pzbk.globalsearch;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Kategorie;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.ReferenzSystem;
import de.interfaceag.bmw.pzbk.enums.Sorting;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

@ExtendWith(MockitoExtension.class)
class GlobalSearchFilterTest {


    @Mock
    HttpServletRequest request;

    UrlParameter urlParameter;

    private GlobalSearchFilter globalSearchFilter;

    @BeforeEach
    void setUp() {
        urlParameter = new UrlParameter(request);

    }


    private void setGlobalSearchFilter(UrlParameter urlParameter) {
        List<SensorCoc> allSensorCocs = Collections.singletonList(TestDataFactory.generateSensorCoc(1L));
        List<Tteam> allTteams = Collections.singletonList(TestDataFactory.generateTteam());
        List<Modul> allModule = TestDataFactory.generateModule();
        List<ModulSeTeam> allModulSeTeams = Collections.singletonList(TestDataFactory.generateModulSeTeam(1L, TestDataFactory.generateModul(), TestDataFactory.generateModulKomponenten()));
        List<Derivat> allDerivate = TestDataFactory.genereteDerivate();
        List<ProzessbaukastenFilterDto> prozessbaukaesten = Collections.singletonList(new ProzessbaukastenFilterDto(1L, "P123", 1, "PYBK Beyeichnung"));
        List<FestgestelltIn> allFestgestelltIn = TestDataFactory.generateFestgestelltInList();
        Collection<ThemenklammerDto> themenklammern = Collections.singletonList(new ThemenklammerDto(1L, "Bezeichnung"));
        globalSearchFilter = new GlobalSearchFilter(urlParameter, allSensorCocs, allTteams, allModule, allModulSeTeams, allDerivate, prozessbaukaesten, allFestgestelltIn, themenklammern);
    }

    @Test
    void queryFilterTest() {
        urlParameter.addOrReplaceParamter("query", "A123");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        List<String> filterValues = globalSearchFilter.getQueryFilter().getAttributeValues();


        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains("A123"));
    }

    @Test
    void referenzSzstemFilterTest() {
        urlParameter.addOrReplaceParamter("referenzSystemFilter", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        List<ReferenzSystem> filterValues = globalSearchFilter.getReferenzSystemFilter().getAsEnumList();


        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(ReferenzSystem.getById(1)));
    }

    @Test
    void kategorieFilterTest() {
        urlParameter.addOrReplaceParamter("kategorieFilter", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        List<Kategorie> filterValues = globalSearchFilter.getKategorieFilter().getAsEnumList();


        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(Kategorie.getById(1)));
    }

    @Test
    void typeFilterTest() {
        urlParameter.addOrReplaceParamter("typeFilter", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        List<Type> filterValues = globalSearchFilter.getTypeFilter().getAsEnumList();


        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(Type.getById(1).get()));
    }

    @Test
    void sortingFilterTest() {
        urlParameter.addOrReplaceParamter("sorting", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Optional<Sorting> filterValues = globalSearchFilter.getSortingFilter().getAsEnum();

        Assertions.assertTrue(filterValues.isPresent());
        assertThat(filterValues.get(), equalTo(Sorting.getById(1).get()));
    }

    @Test
    void showStatusGeloeschtFilterTest() {
        urlParameter.addOrReplaceParamter("geloeschteElemente", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Boolean filterValues = globalSearchFilter.getShowStatusGeloeschtFilter().getValue();

        Assertions.assertTrue(filterValues);

    }

    @Test
    void meldungStatusFilterTest() {
        urlParameter.addOrReplaceParamter("meldungStatusFilter", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        List<Status> filterValues = globalSearchFilter.getMeldungStatusFilter().getAsEnumList();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(Status.getStatusById(1)));

    }

    @Test
    void anforderungStatusFilterTest() {
        urlParameter.addOrReplaceParamter("anforderungStatusFilter", "4");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        List<Status> filterValues = globalSearchFilter.getAnforderungStatusFilter().getAsEnumList();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(Status.getStatusById(4)));

    }

    @Test
    void prozessbaukastenStatusFilterTest() {
        urlParameter.addOrReplaceParamter("prozessbaukastenStatus", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        List<ProzessbaukastenStatus> filterValues = globalSearchFilter.getProzessbaukastenStatusFilter().getAsEnumList();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(ProzessbaukastenStatus.getById(1)));

    }

    @Test
    void anforderungIdFilterTest() {
        urlParameter.addOrReplaceParamter("Anforderungids", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        List<Long> filterValues = globalSearchFilter.getAnforderungIdFilter().getAttributeValues();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(1L));

    }

    @Test
    void meldungIdFilterTest() {
        urlParameter.addOrReplaceParamter("Meldungids", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        List<Long> filterValues = globalSearchFilter.getMeldungIdFilter().getAttributeValues();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(1L));

    }

    @Test
    void sensorCocFilterTest() {
        urlParameter.addOrReplaceParamter("sensorCocFilter", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Set<Long> filterValues = globalSearchFilter.getSensorCocFilter().getSelectedIdsAsSet();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(1L));

    }

    @Test
    void tteamFilterTest() {
        urlParameter.addOrReplaceParamter("tteamFilter", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Set<Long> filterValues = globalSearchFilter.getTteamFilter().getSelectedIdsAsSet();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(1L));

    }

    @Test
    void derivatFilterTest() {
        urlParameter.addOrReplaceParamter("derivatFilter", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Set<Long> filterValues = globalSearchFilter.getDerivatFilter().getSelectedIdsAsSet();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(1L));

    }

    @Test
    void themenklammerFilterTest() {
        urlParameter.addOrReplaceParamter("themenklammerFilter", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Set<Long> filterValues = globalSearchFilter.getThemenklammerFilter().getSelectedIdsAsSet();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(1L));
    }

    @Test
    void phasenrelevanzFilterTest() {
        urlParameter.addOrReplaceParamter("vereinbarungStatusFilter", "0");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        List<Boolean> filterValues = globalSearchFilter.getPhasenrelevanzFilter().getAsBoolean();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(Boolean.FALSE));

    }

    @Test
    void festgestelltInFilterTest() {
        urlParameter.addOrReplaceParamter("festgestlltInFilter", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Set<Long> filterValues = globalSearchFilter.getFestgestelltInFilter().getSelectedIdsAsSet();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(1L));

    }

    @Test
    void modulSeTeamFilterTest() {
        urlParameter.addOrReplaceParamter("modulSeTeamFilter", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Set<Long> filterValues = globalSearchFilter.getModulSeTeamFilter().getSelectedIdsAsSet();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(1L));

    }

    @Test
    void modulFilterTest() {
        urlParameter.addOrReplaceParamter("modulFilter", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Set<Long> filterValues = globalSearchFilter.getModulFilter().getSelectedIdsAsSet();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(1L));

    }

    @Test
    void prozessbaukastenFilterTest() {
        urlParameter.addOrReplaceParamter("prozessbaukastenFilter", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Set<Long> filterValues = globalSearchFilter.getProzessbaukastenFilter().getSelectedIdsAsSet();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(1L));

    }

    @Test
    void aenderungsdatumFilterTest() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String date = dateFormat.format(new Date());
        urlParameter.addOrReplaceParamter("aenderungsdatumvonDateFilter", date);
        urlParameter.addOrReplaceParamter("aenderungsdatumbisDateFilter", date);
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Date filterValues = globalSearchFilter.getAenderungsdatumFilter().getBis();
        Date filterValues2 = globalSearchFilter.getAenderungsdatumFilter().getVon();

        String date1 = dateFormat.format(filterValues);
        String date2 = dateFormat.format(filterValues2);

        assertThat(date1, equalTo(date));
        assertThat(date2, equalTo(date));
    }

    @Test
    void erstellungsdatumFilterTest() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String date = dateFormat.format(new Date());

        urlParameter.addOrReplaceParamter("erstellungsdatumvonDateFilter", date);
        urlParameter.addOrReplaceParamter("erstellungsdatumbisDateFilter", date);
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Date filterValues = globalSearchFilter.getErstellungsdatumFilter().getBis();
        Date filterValues2 = globalSearchFilter.getErstellungsdatumFilter().getVon();

        String date1 = dateFormat.format(filterValues);
        String date2 = dateFormat.format(filterValues2);

        assertThat(date1, equalTo(date));
        assertThat(date2, equalTo(date));
    }


    @Test
    void datumStatusaenderungFilterTest() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String date = dateFormat.format(new Date());
        urlParameter.addOrReplaceParamter("datumStatusaenderungvonDateFilter", date);
        urlParameter.addOrReplaceParamter("datumStatusaenderungbisDateFilter", date);
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Date filterValues = globalSearchFilter.getDatumStatusaenderungFilter().getBis();
        Date filterValues2 = globalSearchFilter.getDatumStatusaenderungFilter().getVon();

        String date1 = dateFormat.format(filterValues);
        String date2 = dateFormat.format(filterValues2);

        assertThat(date1, equalTo(date));
        assertThat(date2, equalTo(date));
    }

    @Test
    void pageUrlFilterTest() {
        urlParameter.addOrReplaceParamter("page", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        int filterValues = globalSearchFilter.getPageUrlFilter().getPageId();

        assertThat(filterValues, equalTo(1));

    }

    @Test
    void dashboardFilterTest() {
        urlParameter.addOrReplaceParamter("dashboard", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        int filterValues = globalSearchFilter.getDashboardFilter().getStep();

        assertThat(filterValues, equalTo(1));

    }

    @Test
    void dashboardMeldungIdFilterTest() {
        urlParameter.addOrReplaceParamter("mId", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Set<Long> filterValues = globalSearchFilter.getDashboardMeldungIdFilter().getSelectedIdsAsSet();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(1L));

    }

    @Test
    void dashboardAnforderungIdFilterTest() {
        urlParameter.addOrReplaceParamter("aId", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Set<Long> filterValues = globalSearchFilter.getDashboardAnforderungIdFilter().getSelectedIdsAsSet();

        assertThat(filterValues, hasSize(1));
        assertThat(filterValues, contains(1L));

    }

    @Test
    void userDefinedSearchUrlFilterTest() {
        urlParameter.addOrReplaceParamter("userDefinedSearch", "1");
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Optional<Long> filterValues = globalSearchFilter.getUserDefinedSearchUrlFilter().getId();

        assertThat(filterValues.get(), equalTo(1L));

    }

    @Test
    void userDefinedSearchUrlFilterOfNullTest() {
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        Optional<Long> filterValues = globalSearchFilter.getUserDefinedSearchUrlFilter().getId();

        Assertions.assertFalse(filterValues.isPresent());

    }


    @Test
    void pageUrlFilterOfNullTest() {
        urlParameter.addOrReplaceParamter("test", "A345");
        setGlobalSearchFilter(urlParameter);

        int filterValues = globalSearchFilter.getPageUrlFilter().getPageId();

        assertThat(filterValues, equalTo(0));

    }


}