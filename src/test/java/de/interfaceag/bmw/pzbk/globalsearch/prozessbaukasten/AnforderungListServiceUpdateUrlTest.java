package de.interfaceag.bmw.pzbk.globalsearch.prozessbaukasten;

import de.interfaceag.bmw.pzbk.entities.UrlEncoding;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.globalsearch.AnforderungListService;
import de.interfaceag.bmw.pzbk.globalsearch.SearchResultDTO;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.UrlEncodingService;
import de.interfaceag.bmw.pzbk.shared.utils.SecurityUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungListServiceUpdateUrlTest {

    @Mock
    private ConfigService configService;

    @Mock
    private UrlEncodingService urlEncodingService;

    @InjectMocks
    private final AnforderungListService anforderungListService = new AnforderungListService();

    private final List<SearchResultDTO> searchResults = new ArrayList<>();
    private final SearchResultDTO meldung = new SearchResultDTO("M123");
    private final SearchResultDTO anforderung = new SearchResultDTO("A456");
    private final SearchResultDTO prozessbaukasten = new SearchResultDTO("P789");

    private String urlId;
    private String urlParameter;
    private UrlEncoding urlEncoding;
    private final String baseUrl = "http://localhost:8080/pzbk";

    @BeforeEach
    public void setUp() {
        when(configService.getUrlPrefix()).thenReturn(baseUrl);
    }

    @Test
    public void testUpdateUrlForEmptySearchResult() {
        String urlActual = anforderungListService.updateUrl(searchResults);
        String urlExpected = baseUrl + "/anforderungList.xhtml";
        Assertions.assertEquals(urlExpected, urlActual);
    }

    @Test
    public void testUpdateUrlForMeldung() {
        searchResults.add(meldung);
        urlParameter = "?query=" + meldung.getName();
        urlId = SecurityUtils.hashStringSha256(Integer.toString(Page.SUCHE.getId()) + urlParameter);

        urlEncoding = new UrlEncoding(Page.SUCHE, urlId, urlParameter);
        when(urlEncodingService.createNewUrlEncodingForPageAndParameter(Page.SUCHE, urlParameter)).thenReturn(urlEncoding);

        String urlActual = anforderungListService.updateUrl(searchResults);
        String urlExpected = baseUrl + "/anforderungList.xhtml" + "?p=" + urlId;
        Assertions.assertEquals(urlExpected, urlActual);
    }

    @Test
    public void testUpdateUrlForAnforderung() {
        searchResults.add(anforderung);
        urlParameter = "?query=" + anforderung.getName();
        urlId = SecurityUtils.hashStringSha256(Integer.toString(Page.SUCHE.getId()) + urlParameter);

        urlEncoding = new UrlEncoding(Page.SUCHE, urlId, urlParameter);
        when(urlEncodingService.createNewUrlEncodingForPageAndParameter(Page.SUCHE, urlParameter)).thenReturn(urlEncoding);

        String urlActual = anforderungListService.updateUrl(searchResults);
        String urlExpected = baseUrl + "/anforderungList.xhtml" + "?p=" + urlId;
        Assertions.assertEquals(urlExpected, urlActual);
    }

    @Test
    public void testUpdateUrlForProzessbaukasten() {
        searchResults.add(prozessbaukasten);
        urlParameter = "?query=" + prozessbaukasten.getName();
        urlId = SecurityUtils.hashStringSha256(Integer.toString(Page.SUCHE.getId()) + urlParameter);

        urlEncoding = new UrlEncoding(Page.SUCHE, urlId, urlParameter);
        when(urlEncodingService.createNewUrlEncodingForPageAndParameter(Page.SUCHE, urlParameter)).thenReturn(urlEncoding);

        String urlActual = anforderungListService.updateUrl(searchResults);
        String urlExpected = baseUrl + "/anforderungList.xhtml" + "?p=" + urlId;
        Assertions.assertEquals(urlExpected, urlActual);
    }

    @Test
    public void testUpdateUrlForMeldungAnforderung() {
        searchResults.add(meldung);
        searchResults.add(anforderung);
        urlParameter = "?query=" + meldung.getName() + "," + anforderung.getName();
        urlId = SecurityUtils.hashStringSha256(Integer.toString(Page.SUCHE.getId()) + urlParameter);

        urlEncoding = new UrlEncoding(Page.SUCHE, urlId, urlParameter);
        when(urlEncodingService.createNewUrlEncodingForPageAndParameter(Page.SUCHE, urlParameter)).thenReturn(urlEncoding);

        String urlActual = anforderungListService.updateUrl(searchResults);
        String urlExpected = baseUrl + "/anforderungList.xhtml" + "?p=" + urlId;
        Assertions.assertEquals(urlExpected, urlActual);
    }

    @Test
    public void testUpdateUrlForMeldungProzessbaukasten() {
        searchResults.add(meldung);
        searchResults.add(prozessbaukasten);
        urlParameter = "?query=" + meldung.getName() + "," + prozessbaukasten.getName();
        urlId = SecurityUtils.hashStringSha256(Integer.toString(Page.SUCHE.getId()) + urlParameter);

        urlEncoding = new UrlEncoding(Page.SUCHE, urlId, urlParameter);
        when(urlEncodingService.createNewUrlEncodingForPageAndParameter(Page.SUCHE, urlParameter)).thenReturn(urlEncoding);

        String urlActual = anforderungListService.updateUrl(searchResults);
        String urlExpected = baseUrl + "/anforderungList.xhtml" + "?p=" + urlId;
        Assertions.assertEquals(urlExpected, urlActual);
    }

    @Test
    public void testUpdateUrlForAnforderungProzessbaukasten() {
        searchResults.add(anforderung);
        searchResults.add(prozessbaukasten);
        urlParameter = "?query=" + anforderung.getName() + "," + prozessbaukasten.getName();
        urlId = SecurityUtils.hashStringSha256(Integer.toString(Page.SUCHE.getId()) + urlParameter);

        urlEncoding = new UrlEncoding(Page.SUCHE, urlId, urlParameter);
        when(urlEncodingService.createNewUrlEncodingForPageAndParameter(Page.SUCHE, urlParameter)).thenReturn(urlEncoding);

        String urlActual = anforderungListService.updateUrl(searchResults);
        String urlExpected = baseUrl + "/anforderungList.xhtml" + "?p=" + urlId;
        Assertions.assertEquals(urlExpected, urlActual);
    }

    @Test
    public void testUpdateUrlForMeldungAnforderungProzessbaukasten() {
        searchResults.add(meldung);
        searchResults.add(anforderung);
        searchResults.add(prozessbaukasten);
        urlParameter = "?query=" + meldung.getName() + "," + anforderung.getName() + "," + prozessbaukasten.getName();
        urlId = SecurityUtils.hashStringSha256(Integer.toString(Page.SUCHE.getId()) + urlParameter);

        urlEncoding = new UrlEncoding(Page.SUCHE, urlId, urlParameter);
        when(urlEncodingService.createNewUrlEncodingForPageAndParameter(Page.SUCHE, urlParameter)).thenReturn(urlEncoding);

        String urlActual = anforderungListService.updateUrl(searchResults);
        String urlExpected = baseUrl + "/anforderungList.xhtml" + "?p=" + urlId;
        Assertions.assertEquals(urlExpected, urlActual);
    }

}
