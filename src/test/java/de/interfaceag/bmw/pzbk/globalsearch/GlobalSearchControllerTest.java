package de.interfaceag.bmw.pzbk.globalsearch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class GlobalSearchControllerTest {

    @Mock
    private GlobalSearchViewData viewData;

    @InjectMocks
    private GlobalSearchController controller;

    private SearchResultDTO element1;
    private SearchResultDTO element2;
    private SearchResultDTO element3;
    private List<SearchResultDTO> searchResults;

    @BeforeEach
    public void setUp() {
        element1 = new SearchResultDTO(1L, "M11", "Meldung", "beschreibung1", "beschreibungStaerkeSchwaeche1", "zugeordnet", "LaKa_TOF", "");
        element1.setSelected(true);
        element2 = new SearchResultDTO(21L, "A21", "Anforderung", "beschreibung21", "beschreibungStaerkeSchwaeche21", "in Arbeit", "T-E/E Antrieb", "V1");
        element2.setSelected(false);
        element3 = new SearchResultDTO(21L, "A22", "Anforderung", "beschreibung22", "beschreibungStaerkeSchwaeche22", "abgestimmt", "T-E/E Analyse", "V1");
        element3.setSelected(true);
        searchResults = Arrays.asList(element1, element2, element3);
    }

    @Test
    public void testGetNumberOfResultsForAllSelected() {
        when(viewData.isAllSelected()).thenReturn(Boolean.TRUE);
        when(viewData.getNumberOfResults()).thenReturn(3L);

        long numberOfResultsFact = controller.getNumberOfResults();
        Assertions.assertEquals(3L, numberOfResultsFact);
    }

    @Test
    public void testGetNumberOfResultsForNotAllSelected() {
        when(viewData.isAllSelected()).thenReturn(Boolean.FALSE);
        when(viewData.getSearchResults()).thenReturn(searchResults);

        long numberOfResultsFact = controller.getNumberOfResults();
        Assertions.assertEquals(2L, numberOfResultsFact);
    }

}
