package de.interfaceag.bmw.pzbk.derivat;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.ArrayList;
import java.util.Collection;

class DerivatStatusChangePermissionTest {

    Collection<Rolle> roles;

    @BeforeEach
    void setup() {
        roles = new ArrayList<>();
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isStatusChangeEnabledForStatusOffen(Rolle rolle) {
        roles.add(rolle);
        boolean result = DerivatStatusChangePermission.isStatusChangeEnabled(roles, DerivatStatus.OFFEN);
        switch (rolle) {
            case ADMIN:
                MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.FALSE));
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isStatusChangeEnabledForStatusVereinbarungVKBG(Rolle rolle) {
        roles.add(rolle);
        boolean result = DerivatStatusChangePermission.isStatusChangeEnabled(roles, DerivatStatus.VEREINARBUNG_VKBG);
        switch (rolle) {
            case ADMIN:
                MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.FALSE));
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isStatusChangeEnabledForStatusVereinbarungZV(Rolle rolle) {
        roles.add(rolle);
        boolean result = DerivatStatusChangePermission.isStatusChangeEnabled(roles, DerivatStatus.VEREINBARUNG_ZV);
        switch (rolle) {
            case ADMIN:
                MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.FALSE));
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isStatusChangeEnabledForStatusZV(Rolle rolle) {
        roles.add(rolle);
        boolean result = DerivatStatusChangePermission.isStatusChangeEnabled(roles, DerivatStatus.ZV);
        MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.FALSE));
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isStatusChangeEnabledForStatusInaktiv(Rolle rolle) {
        roles.add(rolle);
        boolean result = DerivatStatusChangePermission.isStatusChangeEnabled(roles, DerivatStatus.INAKTIV);
        MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.FALSE));
    }

}
