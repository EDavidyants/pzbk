package de.interfaceag.bmw.pzbk.derivat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author sl
 */
public class DerivatStatusTest {

    @ParameterizedTest
    @EnumSource(DerivatStatus.class)
    public void testDerivatStatusId(DerivatStatus status) {
        Integer expectedId;
        Integer result = status.getId();

        switch (status) {
            case OFFEN:
                expectedId = 0;
                break;
            case VEREINARBUNG_VKBG:
                expectedId = 1;
                break;
            case VEREINBARUNG_ZV:
                expectedId = 2;
                break;
            case ZV:
                expectedId = 3;
                break;
            case INAKTIV:
                expectedId = 4;
                break;
            default:
                throw new AssertionError(status.name());
        }
        assertEquals(expectedId, result);
    }

    @ParameterizedTest
    @EnumSource(DerivatStatus.class)
    public void testDerivatStatusName(DerivatStatus status) {
        String expected;
        String result = status.getLocalizationName();

        switch (status) {
            case OFFEN:
                expected = "derivatstatus_offen";
                break;
            case VEREINARBUNG_VKBG:
                expected = "derivatstatus_vereinbarung_vkbg";
                break;
            case VEREINBARUNG_ZV:
                expected = "derivatstatus_vereinbarung_zv";
                break;
            case ZV:
                expected = "derivatstatus_zv";
                break;
            case INAKTIV:
                expected = "derivatstatus_inaktiv";
                break;
            default:
                throw new AssertionError(status.name());
        }
        assertEquals(expected, result);
    }

    @ParameterizedTest
    @EnumSource(DerivatStatus.class)
    public void testDerivatStatusChangeName(DerivatStatus status) {
        String expected;
        String result = status.getStatusChangeLocalizationName();

        switch (status) {
            case OFFEN:
                expected = "derivatstatus_offen";
                break;
            case VEREINARBUNG_VKBG:
                expected = "derivatstatus_vereinbarung_vkbg_starten";
                break;
            case VEREINBARUNG_ZV:
                expected = "derivatstatus_vereinbarung_zv_starten";
                break;
            case ZV:
                expected = "derivatstatus_zv_erreicht";
                break;
            case INAKTIV:
                expected = "derivatstatus_inaktiv_setzen";
                break;
            default:
                throw new AssertionError(status.name());
        }
        assertEquals(expected, result);
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 0, 1, 2, 3, 4, 5, 6})
    public void testGetById(Integer id) {
        DerivatStatus result;
        switch (id) {
            case 0:
                result = DerivatStatus.getById(id);
                assertEquals(DerivatStatus.OFFEN, result);
                break;
            case 1:
                result = DerivatStatus.getById(id);
                assertEquals(DerivatStatus.VEREINARBUNG_VKBG, result);
                break;
            case 2:
                result = DerivatStatus.getById(id);
                assertEquals(DerivatStatus.VEREINBARUNG_ZV, result);
                break;
            case 3:
                result = DerivatStatus.getById(id);
                assertEquals(DerivatStatus.ZV, result);
                break;
            case 4:
                result = DerivatStatus.getById(id);
                assertEquals(DerivatStatus.INAKTIV, result);
                break;
            default:
                assertThrows(IllegalArgumentException.class, () -> DerivatStatus.getById(id));
        }
    }

    @Test
    public void testGetByNullId() {
        DerivatStatus result = DerivatStatus.getById(null);
        assertThat(DerivatStatus.OFFEN, is(result));
    }

    @ParameterizedTest
    @EnumSource(DerivatStatus.class)
    public void testGetNextStatus(DerivatStatus status) {
        Collection<DerivatStatus> result = status.getNextStatus();
        Collection<DerivatStatus> expected;
        switch (status) {
            case OFFEN:
                expected = Arrays.asList(DerivatStatus.VEREINARBUNG_VKBG);
                break;
            case VEREINARBUNG_VKBG:
                expected = Arrays.asList(DerivatStatus.VEREINBARUNG_ZV, DerivatStatus.ZV);
                break;
            case VEREINBARUNG_ZV:
                expected = Arrays.asList(DerivatStatus.ZV);
                break;
            case ZV:
            case INAKTIV:
            default:
                expected = Collections.EMPTY_LIST;
                break;
        }
        assertEquals(expected, result);
    }

}
