package de.interfaceag.bmw.pzbk.derivat;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class DerivatStatusChangeSerivceTest {

    @Mock
    private DerivatService derivatService;
    @InjectMocks
    private DerivatStatusChangeService derivatStatusChangeSerivce;

    @ParameterizedTest
    @EnumSource(DerivatStatus.class)
    public void testChangeStatusFromStatusOffen(DerivatStatus newStatus) {
        Derivat derivat = new Derivat();
        DerivatStatus currentStatus = DerivatStatus.OFFEN;
        derivat.setStatus(currentStatus);

        derivatStatusChangeSerivce.changeStatus(derivat, newStatus);

        DerivatStatus result = derivat.getStatus();
        if (newStatus.equals(DerivatStatus.OFFEN) || newStatus.equals(DerivatStatus.VEREINARBUNG_VKBG) || newStatus.equals(DerivatStatus.INAKTIV)) {
            assertEquals(newStatus, result);
        } else {
            assertEquals(currentStatus, result);
        }
    }

    @ParameterizedTest
    @EnumSource(DerivatStatus.class)
    public void testChangeStatusFromStatusVereinbarungVKBG(DerivatStatus newStatus) {
        Derivat derivat = new Derivat();
        DerivatStatus currentStatus = DerivatStatus.VEREINARBUNG_VKBG;
        derivat.setStatus(currentStatus);

        derivatStatusChangeSerivce.changeStatus(derivat, newStatus);

        DerivatStatus result = derivat.getStatus();
        if (newStatus.equals(DerivatStatus.VEREINARBUNG_VKBG) || newStatus.equals(DerivatStatus.VEREINBARUNG_ZV) || newStatus.equals(DerivatStatus.ZV) || newStatus.equals(DerivatStatus.INAKTIV)) {
            assertEquals(newStatus, result);
        } else {
            assertEquals(currentStatus, result);
        }
    }

    @ParameterizedTest
    @EnumSource(DerivatStatus.class)
    public void testChangeStatusFromStatusVereinbarungZV(DerivatStatus newStatus) {
        Derivat derivat = new Derivat();
        DerivatStatus currentStatus = DerivatStatus.VEREINBARUNG_ZV;
        derivat.setStatus(currentStatus);

        derivatStatusChangeSerivce.changeStatus(derivat, newStatus);

        DerivatStatus result = derivat.getStatus();
        if (newStatus.equals(DerivatStatus.VEREINBARUNG_ZV) || newStatus.equals(DerivatStatus.ZV) || newStatus.equals(DerivatStatus.INAKTIV)) {
            assertEquals(newStatus, result);
        } else {
            assertEquals(currentStatus, result);
        }
    }

    @ParameterizedTest
    @EnumSource(DerivatStatus.class)
    public void testChangeStatusFromStatusZV(DerivatStatus newStatus) {
        Derivat derivat = new Derivat();
        DerivatStatus currentStatus = DerivatStatus.ZV;
        derivat.setStatus(currentStatus);

        derivatStatusChangeSerivce.changeStatus(derivat, newStatus);

        DerivatStatus result = derivat.getStatus();
        if (newStatus.equals(DerivatStatus.ZV) || newStatus.equals(DerivatStatus.INAKTIV)) {
            assertEquals(newStatus, result);
        } else {
            assertEquals(currentStatus, result);
        }
    }

    @ParameterizedTest
    @EnumSource(DerivatStatus.class)
    public void testChangeStatusFromStatusInaktiv(DerivatStatus newStatus) {
        Derivat derivat = new Derivat();
        DerivatStatus currentStatus = DerivatStatus.INAKTIV;
        derivat.setStatus(currentStatus);

        derivatStatusChangeSerivce.changeStatus(derivat, newStatus);

        DerivatStatus result = derivat.getStatus();
        assertEquals(newStatus, result);
    }

    @Test
    public void testRestoreToLastStatusFallback() {
        Derivat derivat = new Derivat();
        derivat.setStatus(DerivatStatus.INAKTIV);
        derivatStatusChangeSerivce.restoreToLastStatus(derivat);
        DerivatStatus result = derivat.getStatus();
        assertEquals(DerivatStatus.OFFEN, result);
    }

    @Test
    public void testRestoreToLastStatus() {
        Derivat derivat = new Derivat();
        derivat.setStatus(DerivatStatus.INAKTIV);
        derivat.setLastStatus(DerivatStatus.VEREINARBUNG_VKBG);
        derivatStatusChangeSerivce.restoreToLastStatus(derivat);
        DerivatStatus result = derivat.getStatus();
        assertEquals(DerivatStatus.VEREINARBUNG_VKBG, result);
    }

    @Test
    public void testRestoreToLastStatusNotInaktiv() {
        Derivat derivat = new Derivat();
        derivat.setStatus(DerivatStatus.VEREINARBUNG_VKBG);
        derivatStatusChangeSerivce.restoreToLastStatus(derivat);
        DerivatStatus result = derivat.getStatus();
        assertEquals(DerivatStatus.VEREINARBUNG_VKBG, result);
    }

    @Test
    public void testRestoreToLastStatusNullDerivat() {
        assertThrows(IllegalArgumentException.class, () -> derivatStatusChangeSerivce.restoreToLastStatus(null));
    }

    @Test
    public void testRestoreToLastStatusNullCurrentStatus() {
        Derivat derivat = new Derivat();
        derivat.setStatus(null);
        derivatStatusChangeSerivce.restoreToLastStatus(derivat);
        DerivatStatus result = derivat.getStatus();
        assertEquals(DerivatStatus.OFFEN, result);
    }

}
