package de.interfaceag.bmw.pzbk.derivat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class DerivatStatusNamesTest {

    private static final String NAME = "name";
    private static final String STATUSCHANGENAME = "statusChangeName";
    private static final DerivatStatus STATUS = DerivatStatus.OFFEN;
    private static final DerivatStatus ANOTHERSTATUS = DerivatStatus.ZV;

    @Mock
    private DerivatStatusName derivatStatusName;

    private DerivatStatusNames derivatStatusNames;

    @BeforeEach
    public void setUp() {
        Collection<DerivatStatusName> statusNames = Arrays.asList(derivatStatusName);
        derivatStatusNames = new DerivatStatusNames(statusNames);
        when(derivatStatusName.getStatus()).thenReturn(STATUS);
    }

    @Test
    public void testGetNameForStatus() {
        when(derivatStatusName.getName()).thenReturn(NAME);
        String result = derivatStatusNames.getNameForStatus(STATUS);
        Assertions.assertEquals(NAME, result);
    }

    @Test
    public void testGetNameForStatusNotInList() {
        String result = derivatStatusNames.getNameForStatus(ANOTHERSTATUS);
        Assertions.assertEquals("", result);
    }

    @Test
    public void testGetStatusChangeNameForStatus() {
        when(derivatStatusName.getStatusChangeName()).thenReturn(STATUSCHANGENAME);
        String result = derivatStatusNames.getStatusChangeNameForStatus(STATUS);
        Assertions.assertEquals(STATUSCHANGENAME, result);
    }

    @Test
    public void testGetStatusChangeNameForStatusNotInList() {
        String result = derivatStatusNames.getStatusChangeNameForStatus(ANOTHERSTATUS);
        Assertions.assertEquals("", result);
    }

}
