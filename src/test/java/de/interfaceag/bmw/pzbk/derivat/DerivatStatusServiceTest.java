package de.interfaceag.bmw.pzbk.derivat;

import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class DerivatStatusServiceTest {

    private static final String NAME = "name";

    @Mock
    private LocalizationService localizationService;
    @InjectMocks
    private DerivatStatusService derivatStatusService;

    @BeforeEach
    public void setUp() {
        when(localizationService.getValue(any())).thenReturn(NAME);
    }

    @ParameterizedTest
    @EnumSource(DerivatStatus.class)
    public void testGetDerivatStatusNames(DerivatStatus status) {
        DerivatStatusNames result = derivatStatusService.getDerivatStatusNames();
        String nameForStatus = result.getNameForStatus(status);
        Assertions.assertEquals(NAME, nameForStatus);
    }

}
