package de.interfaceag.bmw.pzbk.derivat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author sl
 */
public class DerivatStatusNameTest {

    private static final DerivatStatus STATUS = DerivatStatus.INAKTIV;
    private static final String NAME = "name";
    private static final String STATUSCHANGENAME = "name";

    private DerivatStatusName derivatStatusName;

    @BeforeEach
    public void setUp() {
        derivatStatusName = new DerivatStatusName(STATUS, NAME, STATUSCHANGENAME);
    }

    @Test
    public void testGetStatus() {
        DerivatStatus result = derivatStatusName.getStatus();
        assertEquals(STATUS, result);
    }

    @Test
    public void testGetName() {
        String result = derivatStatusName.getName();
        assertEquals(NAME, result);
    }

    @Test
    public void testGetStatusChangeName() {
        String result = derivatStatusName.getStatusChangeName();
        assertEquals(STATUSCHANGENAME, result);
    }

}
