package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingMeldungStatusTransitionServiceTest {

    @Mock
    private ReportingMeldungStatusTransitionZuordnungToAnforderungService meldungStatusTransitionZuordnungToAnforderungService;
    @Mock
    private ReportingMeldungStatusTransitionZuordnungToAnforderungAufhebenService meldungStatusTransitionZuordnungToAnforderungAufhebenService;

    @Mock
    private ReportingStatusTransitionChangeMeldungStatusService changeMeldungStatusService;

    @InjectMocks
    private ReportingMeldungStatusTransitionService reportingStatusTransitionService;
    @Mock
    private Date date;
    @Mock
    private Meldung meldung;
    @Mock
    private Anforderung anforderung;
    @Mock
    private Anforderung anforderungOther;

    private Status currentStatus;
    private Status newStatus;

    @Mock
    private ReportingMeldungStatusTransition reportingMeldungStatusTransition;

    @BeforeEach
    void setUp() {
        currentStatus = Status.A_INARBEIT;
        newStatus = Status.A_FTABGESTIMMT;
    }

    @Test
    void changeMeldungStatus() {
        when(changeMeldungStatusService.saveStatusChange(any(), any(), any(), any())).thenReturn(reportingMeldungStatusTransition);
        final ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_ENTWURF, Status.M_GEMELDET, date);
        assertThat(result, is(reportingMeldungStatusTransition));
    }

    @Test
    void addMeldungToNewAnforderung() {
        when(meldungStatusTransitionZuordnungToAnforderungService.addMeldungToNewAnforderung(any(), any())).thenReturn(reportingMeldungStatusTransition);
        final ReportingMeldungStatusTransition result = reportingStatusTransitionService.addMeldungToNewAnforderung(meldung, date);
        assertThat(result, is(reportingMeldungStatusTransition));
    }

    @Test
    void addMeldungToExistingAnforderung() {
        when(meldungStatusTransitionZuordnungToAnforderungService.addMeldungToExistingAnforderung(any(), any())).thenReturn(reportingMeldungStatusTransition);
        final ReportingMeldungStatusTransition result = reportingStatusTransitionService.addMeldungToExistingAnforderung(meldung);
        assertThat(result, is(reportingMeldungStatusTransition));
    }

    @Test
    void removeMeldungFromAnforderung() {
        when(meldungStatusTransitionZuordnungToAnforderungAufhebenService.removeMeldungFromAnforderung(any(), any(), any())).thenReturn(reportingMeldungStatusTransition);
        final ReportingMeldungStatusTransition result = reportingStatusTransitionService.removeMeldungFromAnforderung(meldung, anforderung);
        assertThat(result, is(reportingMeldungStatusTransition));
    }

}
