package de.interfaceag.bmw.pzbk.reporting;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ReportingFilterViewPermissionTest {

    ReportingFilterViewPermission viewPermission;
    Set<Rolle> roles;

    @BeforeEach
    void setUp() {
        roles = new HashSet<>();
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isSensorCocFilter(Rolle role) {
        roles.add(role);
        viewPermission = new ReportingFilterViewPermission(roles);
        final boolean result = viewPermission.isSensorCocFilter();

        switch (role) {
            case ADMIN:
            case ANFORDERER:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                assertTrue(result);
                break;
            default:
                assertFalse(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isTechnologieFilter(Rolle role) {
        roles.add(role);
        viewPermission = new ReportingFilterViewPermission(roles);
        final boolean result = viewPermission.isTechnologieFilter();

        switch (role) {
            case ADMIN:
            case ANFORDERER:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                assertTrue(result);
                break;
            default:
                assertFalse(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isWerkFilter(Rolle role) {
        roles.add(role);
        viewPermission = new ReportingFilterViewPermission(roles);
        final boolean result = viewPermission.isWerkFilter();

        switch (role) {
            case ADMIN:
            case ANFORDERER:
                assertTrue(result);
                break;
            default:
                assertFalse(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isDerivatFilter(Rolle role) {
        roles.add(role);
        viewPermission = new ReportingFilterViewPermission(roles);
        final boolean result = viewPermission.isDerivatFilter();

        switch (role) {
            case ADMIN:
            case ANFORDERER:
                assertTrue(result);
                break;
            default:
                assertFalse(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isTteamFilter(Rolle role) {
        roles.add(role);
        viewPermission = new ReportingFilterViewPermission(roles);
        final boolean result = viewPermission.isTteamFilter();

        switch (role) {
            case ADMIN:
            case ANFORDERER:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
                assertTrue(result);
                break;
            default:
                assertFalse(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isDateFilter(Rolle role) {
        roles.add(role);
        viewPermission = new ReportingFilterViewPermission(roles);
        final boolean result = viewPermission.isDateFilter();

        switch (role) {
            case ADMIN:
            case ANFORDERER:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case SENSORCOCLEITER:
            case E_COC:
            case SCL_VERTRETER:
                assertTrue(result);
                break;
            default:
                assertFalse(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isModulSeTeamFilter(Rolle role) {
        roles.add(role);
        viewPermission = new ReportingFilterViewPermission(roles);
        final boolean result = viewPermission.isModulSeTeamFilter();

        switch (role) {
            case ADMIN:
            case E_COC:
                assertTrue(result);
                break;
            default:
                assertFalse(result);
                break;
        }
    }
}