package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.filter.AbstractMultiValueFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessAmpelThresholdService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsMapContaining;
import org.hamcrest.collection.IsMapWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl, evda
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingUmsetzungsverwaltungKeyFigureServiceVKBGTest {

    private static final Long VEREINBARUNG_VKBG_ANGENOMMEN = 50L;

    @Mock
    ProjectReportingUmsetzungsverwaltungKeyFigureDao umsetzungsverwaltungKeyFigureDao;

    @Mock
    ProjectProcessAmpelThresholdService ampelThresholdService;

    @InjectMocks
    ProjectReportingUmsetzungsverwaltungKeyFigureService umsetzungsverwaltungKeyFigureService;

    @Mock
    Derivat derivat;

    @Mock
    ProjectProcessFilter filter;

    @Mock
    AbstractMultiValueFilter technologieFilter;

    List<Object[]> queryResults;
    List<Object[]> queryResultsOffen;

    @BeforeEach
    public void setUp() {
        Object[] queryResult1 = new Object[2];
        queryResult1[0] = UmsetzungsBestaetigungStatus.OFFEN.getId();
        queryResult1[1] = 1L;

        Object[] queryResult2 = new Object[2];
        queryResult2[0] = UmsetzungsBestaetigungStatus.UMGESETZT.getId();
        queryResult2[1] = 2L;

        Object[] queryResult3 = new Object[2];
        queryResult3[0] = UmsetzungsBestaetigungStatus.BEWERTUNG_NICHT_MOEGLICH.getId();
        queryResult3[1] = 3L;

        Object[] queryResult4 = new Object[2];
        queryResult4[0] = UmsetzungsBestaetigungStatus.NICHT_UMGESETZT.getId();
        queryResult4[1] = 4L;

        Object[] queryResult5 = new Object[2];
        queryResult5[0] = UmsetzungsBestaetigungStatus.NICHT_RELEVANT.getId();
        queryResult5[1] = 5L;

        queryResults = new ArrayList<>();
        queryResults.add(queryResult1);
        queryResults.add(queryResult2);
        queryResults.add(queryResult3);
        queryResults.add(queryResult4);
        queryResults.add(queryResult5);

        when(umsetzungsverwaltungKeyFigureDao.getQueryDataForUmsetzungsverwaltung(any(), any(), any(ProjectProcessFilter.class))).thenReturn(queryResults);
        when(ampelThresholdService.getThresholdForUmsetzungsverwaltungVKBG()).thenReturn(0.60);
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterResultSize() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapWithSize.aMapWithSize(7));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterContainsL11() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L11.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterL11Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L11.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(4L));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterContainsL10() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L10.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterL10Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L10.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(45L));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterContainsOffen() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L13.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterOffenValue() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L13.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(36L));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterContainsUmsegesetzt() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L12.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterUmgesetztValue() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L12.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(2L));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterContainsBewertungNichtMoeglich() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L14.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterBewertungNichtMoeglichValue() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L14.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(3L));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterContainsNichtUmgesetzt() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L15.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterNichtUmgesetztValue() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L15.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(4L));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterContainsNichtRelevant() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L16.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungVKBGWithFilterNichtRelevantValue() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(derivat, VEREINBARUNG_VKBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L16.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(5L));
    }

}
