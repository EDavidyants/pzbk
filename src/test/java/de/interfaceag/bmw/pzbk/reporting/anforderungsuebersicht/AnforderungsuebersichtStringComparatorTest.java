package de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author evda
 */
public class AnforderungsuebersichtStringComparatorTest {

    private List<String> anforderungFachIds;

    @Before
    public void setUp() {
        // List of seven objects inkl 3 Meldungen and 4 Anforderungen (inkl one with multiple versions)
        anforderungFachIds = new ArrayList<>();

        anforderungFachIds.add("M1");
        anforderungFachIds.add("M21");
        anforderungFachIds.add("M3");
        anforderungFachIds.add("A1 | V1");
        anforderungFachIds.add("A1 | V2");
        anforderungFachIds.add("A123 | V1");
        anforderungFachIds.add("A13 | V1");
    }

    @Test
    public void testCollectionSort() {
        // Sort descending by fachId and version
        anforderungFachIds.sort(new AnforderungsuebersichtStringComparator());
        // Expected order after sort
        // First 'A123 | V1'
        // Second 'A13 | V1'
        // Third 'A1 | V2'
        // Fourth 'A1 | V1'
        // Fifth 'M21'
        // Sixth 'M3'
        // Seventh 'M1'
        Assertions.assertEquals("A123 | V1", anforderungFachIds.get(0));
        Assertions.assertEquals("A13 | V1", anforderungFachIds.get(1));
        Assertions.assertEquals("A1 | V2", anforderungFachIds.get(2));
        Assertions.assertEquals("A1 | V1", anforderungFachIds.get(3));
        Assertions.assertEquals("M21", anforderungFachIds.get(4));
        Assertions.assertEquals("M3", anforderungFachIds.get(5));
        Assertions.assertEquals("M1", anforderungFachIds.get(6));
    }

    @Test
    public void testCollectionSortReverseOrder() {
        // Sort ascending by fachId and version
        anforderungFachIds.sort(Collections.reverseOrder(new AnforderungsuebersichtStringComparator()));
        // Expected order after sort
        // First 'M1'
        // Second 'M3'
        // Third 'M21'
        // Fourth 'A1 | V1'
        // Fifth 'A1 | V2'
        // Sixth 'A13 | V1'
        // Seventh 'A123 | V1'
        Assertions.assertEquals("M1", anforderungFachIds.get(0));
        Assertions.assertEquals("M3", anforderungFachIds.get(1));
        Assertions.assertEquals("M21", anforderungFachIds.get(2));
        Assertions.assertEquals("A1 | V1", anforderungFachIds.get(3));
        Assertions.assertEquals("A1 | V2", anforderungFachIds.get(4));
        Assertions.assertEquals("A13 | V1", anforderungFachIds.get(5));
        Assertions.assertEquals("A123 | V1", anforderungFachIds.get(6));
    }

}
