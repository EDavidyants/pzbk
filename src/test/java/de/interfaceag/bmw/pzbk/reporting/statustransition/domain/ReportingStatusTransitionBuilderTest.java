package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTransitionBuilderTest {

    @Mock
    private Date date;
    @Mock
    private Anforderung anforderung;
    @Mock
    private EntryExitTimeStamp timeStamp;

    ReportingStatusTransitionBuilder builder;

    @BeforeEach
    void setUp() {
        builder = new ReportingStatusTransitionBuilder();
    }

    @Test
    void setEntryDate() {
        builder.setEntryDate(date);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final Date entryDate = reportingStatusTransition.getEntryDate();
        assertThat(entryDate, is(date));
    }

    @Test
    void setAnforderung() {
        builder.setAnforderung(anforderung);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final Anforderung statusTransitionAnforderung = reportingStatusTransition.getAnforderung();
        assertThat(statusTransitionAnforderung, is(anforderung));
    }

    @Test
    void setInArbeit() {
        builder.setInArbeit(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp inArbeit = reportingStatusTransition.getInArbeit();
        assertThat(inArbeit, is(timeStamp));
    }

    @Test
    void setAbgestimmt() {
        builder.setAbgestimmt(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp abgestimmt = reportingStatusTransition.getAbgestimmt();
        assertThat(abgestimmt, is(timeStamp));
    }

    @Test
    void setPlausibilisiert() {
        builder.setPlausibilisiert(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp plausibilisiert = reportingStatusTransition.getPlausibilisiert();
        assertThat(plausibilisiert, is(timeStamp));
    }

    @Test
    void setFreigegeben() {
        builder.setFreigegeben(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp freigegeben = reportingStatusTransition.getFreigegeben();
        assertThat(freigegeben, is(timeStamp));
    }

    @Test
    void setTteamUnstimmig() {
        builder.setTteamUnstimmig(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp tteamUnstimmig = reportingStatusTransition.getTteamUnstimmig();
        assertThat(tteamUnstimmig, is(timeStamp));
    }

    @Test
    void setVereinbarungUnstimmig() {
        builder.setVereinbarungUnstimmig(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp vereinbarungUnstimmig = reportingStatusTransition.getVereinbarungUnstimmig();
        assertThat(vereinbarungUnstimmig, is(timeStamp));
    }

    @Test
    void setFachteamGeloescht() {
        builder.setFachteamGeloescht(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp fachteamGeloescht = reportingStatusTransition.getFachteamGeloescht();
        assertThat(fachteamGeloescht, is(timeStamp));
    }

    @Test
    void setTteamGeloescht() {
        builder.setTteamGeloescht(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp tteamGeloescht = reportingStatusTransition.getTteamGeloescht();
        assertThat(tteamGeloescht, is(timeStamp));
    }

    @Test
    void setVereinbarungGeloescht() {
        builder.setVereinbarungGeloescht(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp vereinbarungGeloescht = reportingStatusTransition.getVereinbarungGeloescht();
        assertThat(vereinbarungGeloescht, is(timeStamp));
    }

    @Test
    void setFreigegebenGeloescht() {
        builder.setFreigegebenGeloescht(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp freigegebenGeloescht = reportingStatusTransition.getFreigegebenGeloescht();
        assertThat(freigegebenGeloescht, is(timeStamp));
    }

    @Test
    void setFachteamProzessbaukasten() {
        builder.setFachteamProzessbaukasten(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp fachteamProzessbaukasten = reportingStatusTransition.getFachteamProzessbaukasten();
        assertThat(fachteamProzessbaukasten, is(timeStamp));
    }

    @Test
    void setTteamProzessbaukasten() {
        builder.setTteamProzessbaukasten(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp tteamProzessbaukasten = reportingStatusTransition.getTteamProzessbaukasten();
        assertThat(tteamProzessbaukasten, is(timeStamp));
    }

    @Test
    void setVereinbarungProzessbaukasten() {
        builder.setVereinbarungProzessbaukasten(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp vereinbarungProzessbaukasten = reportingStatusTransition.getVereinbarungProzessbaukasten();
        assertThat(vereinbarungProzessbaukasten, is(timeStamp));
    }

    @Test
    void setFreigegebenProzessbaukasten() {
        builder.setFreigegebenProzessbaukasten(timeStamp);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final EntryExitTimeStamp freigegebenProzessbaukasten = reportingStatusTransition.getFreigegebenProzessbaukasten();
        assertThat(freigegebenProzessbaukasten, is(timeStamp));
    }

    @Test
    void setNewVersion() {
        boolean newVersion = false;
        builder.setNewVersion(newVersion);
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        final boolean reportingStatusTransitionNewVersion = reportingStatusTransition.isNewVersion();
        assertThat(reportingStatusTransitionNewVersion, is(newVersion));
    }

    @Test
    void buildDefaultValuesNotNull() {
        final ReportingStatusTransition reportingStatusTransition = builder.build();
        assertThat(reportingStatusTransition, notNullValue());
    }
}