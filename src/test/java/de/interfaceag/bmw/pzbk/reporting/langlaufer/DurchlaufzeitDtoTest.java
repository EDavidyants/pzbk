package de.interfaceag.bmw.pzbk.reporting.langlaufer;

import de.interfaceag.bmw.pzbk.enums.Type;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DurchlaufzeitDtoTest {

    private static final int THRESHOLD = 7;
    private static final long DURCHLAUFZEIT = 12L;
    private static final long ID = 42L;
    private static final Type TYPE = Type.ANFORDERUNG;

    private DurchlaufzeitDto durchlaufzeitDto;

    @BeforeEach
    void setUp() {
        durchlaufzeitDto = new DurchlaufzeitDto(TYPE, ID, DURCHLAUFZEIT, THRESHOLD);
    }

    @Test
    void getType() {
        final Type type = durchlaufzeitDto.getType();
        assertThat(type, is(TYPE));
    }

    @Test
    void getId() {
        final Long id = durchlaufzeitDto.getId();
        assertThat(id, is(ID));
    }

    @Test
    void getDurchlaufzeit() {
        final Long durchlaufzeit = durchlaufzeitDto.getDurchlaufzeit();
        assertThat(durchlaufzeit, is(DURCHLAUFZEIT));
    }

    @Test
    void isLanglauferTrue() {
        final boolean langlaufer = durchlaufzeitDto.isLanglaufer();
        assertTrue(langlaufer);
    }

    @Test
    void isLanglauferFalse() {
        durchlaufzeitDto = new DurchlaufzeitDto(TYPE, ID, DURCHLAUFZEIT, 13);
        final boolean langlaufer = durchlaufzeitDto.isLanglaufer();
        assertFalse(langlaufer);
    }
}