package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import de.interfaceag.bmw.pzbk.entities.Meldung;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;

class ReportingMeldungStatusTransitionTest {

    private ReportingMeldungStatusTransition reportingMeldungStatusTransition;

    @BeforeEach
    void setUp() {
        reportingMeldungStatusTransition = new ReportingMeldungStatusTransition();
    }

    @Test
    void getBuilder() {
        final ReportingMeldungStatusTransitionBuilder builder = ReportingMeldungStatusTransition.getBuilder();
        assertThat(builder, instanceOf(ReportingMeldungStatusTransitionBuilder.class));
    }

    @Test
    void getId() {
        final Long id = reportingMeldungStatusTransition.getId();
        assertThat(id, nullValue());
    }

    @Test
    void getEntryDateDefaultValue() {
        final Date entryDate = reportingMeldungStatusTransition.getEntryDate();
        assertThat(entryDate, nullValue());
    }

    @Test
    void getMeldungDefault() {
        final Meldung meldung = reportingMeldungStatusTransition.getMeldung();
        assertThat(meldung, nullValue());
    }

    @Test
    void getGemeldetDefault() {
        final EntryExitTimeStamp gemeldet = reportingMeldungStatusTransition.getGemeldet();
        assertThat(gemeldet, nullValue());
    }

    @Test
    void setGemeldet() {
        EntryExitTimeStamp timeStamp = new EntryExitTimeStamp();
        reportingMeldungStatusTransition.setGemeldet(timeStamp);
        final EntryExitTimeStamp gemeldet = reportingMeldungStatusTransition.getGemeldet();
        assertThat(gemeldet, is(timeStamp));
    }

    @Test
    void getUnstimmig() {
        final EntryExitTimeStamp unstimmig = reportingMeldungStatusTransition.getUnstimmig();
        assertThat(unstimmig, nullValue());
    }

    @Test
    void setUnstimmig() {
        EntryExitTimeStamp timeStamp = new EntryExitTimeStamp();
        reportingMeldungStatusTransition.setUnstimmig(timeStamp);
        final EntryExitTimeStamp unstimmig = reportingMeldungStatusTransition.getUnstimmig();
        assertThat(unstimmig, is(timeStamp));
    }

    @Test
    void getGeloescht() {
        final EntryExitTimeStamp geloescht = reportingMeldungStatusTransition.getGeloescht();
        assertThat(geloescht, nullValue());
    }

    @Test
    void setGeloescht() {
        EntryExitTimeStamp timeStamp = new EntryExitTimeStamp();
        reportingMeldungStatusTransition.setGeloescht(timeStamp);
        final EntryExitTimeStamp geloescht = reportingMeldungStatusTransition.getGeloescht();
        assertThat(geloescht, is(timeStamp));
    }

    @Test
    void getNewAnforderungZugeordnet() {
        final EntryExitTimeStamp newAnforderungZugeordnet = reportingMeldungStatusTransition.getNewAnforderungZugeordnet();
        assertThat(newAnforderungZugeordnet, nullValue());
    }

    @Test
    void setNewAnforderungZugeordnet() {
        EntryExitTimeStamp timeStamp = new EntryExitTimeStamp();
        reportingMeldungStatusTransition.setNewAnforderungZugeordnet(timeStamp);
        final EntryExitTimeStamp newAnforderungZugeordnet = reportingMeldungStatusTransition.getNewAnforderungZugeordnet();
        assertThat(newAnforderungZugeordnet, is(timeStamp));
    }

    @Test
    void getExistingAnforderungZugeordnet() {
        final EntryExitTimeStamp existingAnforderungZugeordnet = reportingMeldungStatusTransition.getExistingAnforderungZugeordnet();
        assertThat(existingAnforderungZugeordnet, nullValue());
    }

    @Test
    void setExistingAnforderungZugeordnet() {
        EntryExitTimeStamp timeStamp = new EntryExitTimeStamp();
        reportingMeldungStatusTransition.setExistingAnforderungZugeordnet(timeStamp);
        final EntryExitTimeStamp existingAnforderungZugeordnet = reportingMeldungStatusTransition.getExistingAnforderungZugeordnet();
        assertThat(existingAnforderungZugeordnet, is(timeStamp));
    }
}