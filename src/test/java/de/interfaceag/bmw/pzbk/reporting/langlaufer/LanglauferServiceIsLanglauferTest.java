package de.interfaceag.bmw.pzbk.reporting.langlaufer;

import de.interfaceag.bmw.pzbk.berechtigung.AnforderungBerechtigungService;
import de.interfaceag.bmw.pzbk.berechtigung.MeldungBerechtigungService;
import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ContextMocker;
import de.interfaceag.bmw.pzbk.reporting.config.ReportingConfigService;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTuple;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTupleCollection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LanglauferServiceIsLanglauferTest {

    @Mock
    private ReportingConfigService reportingConfigService;
    @Mock
    private AnforderungBerechtigungService anforderungBerechtigungService;
    @Mock
    private MeldungBerechtigungService meldungBerechtigungService;
    @Mock
    private DurchlaufzeitService durchlaufzeitService;
    @InjectMocks
    private LanglauferService langlauferService;

    private List<Long> langlauferIds;
    private IdTypeTuple idTypeTuple;
    private IdTypeTupleCollection idTypeTuples;

    @Mock
    private DurchlaufzeitDto durchlaufzeitDto;
    private Collection<DurchlaufzeitDto> durchlaufzeiten;

    @BeforeEach
    void setUp() {
        idTypeTuple = new IdTypeTuple(Type.ANFORDERUNG, 1L);
        langlauferIds = Collections.singletonList(1L);
        durchlaufzeiten = Collections.singletonList(durchlaufzeitDto);
        idTypeTuples = new IdTypeTupleCollection();
        idTypeTuples.add(idTypeTuple);
    }

    @Test
    void isFachteamLanglauferNoIdTypeTules() {
        when(durchlaufzeitService.getFachteamDurchlaufzeiten(any())).thenReturn(durchlaufzeiten);

        ContextMocker.mockFacesContextForUrlParamterUtils();

        final boolean result = langlauferService.isFachteamLanglaufer(new IdTypeTupleCollection());
        assertFalse(result);
    }

    @Test
    void isFachteamLanglaufer() {
        when(durchlaufzeitDto.isLanglaufer()).thenReturn(true);
        when(durchlaufzeitDto.getId()).thenReturn(1L);
        when(durchlaufzeitService.getFachteamDurchlaufzeiten(any())).thenReturn(durchlaufzeiten);
        Collection<Long> durchlaufzeitIds = Collections.singletonList(1L);
        when(anforderungBerechtigungService.restrictToAuthorizedAnforderungen(durchlaufzeitIds)).thenReturn(durchlaufzeitIds);

        ContextMocker.mockFacesContextForUrlParamterUtils();

        final boolean result = langlauferService.isFachteamLanglaufer(idTypeTuples);
        assertTrue(result);
    }

    @Test
    void isTteamLanglauferNoIdTypeTuples() {
        final boolean result = langlauferService.isTteamLanglaufer(new IdTypeTupleCollection());
        assertFalse(result);
    }

    @Test
    void isTteamLanglaufer() {
        when(durchlaufzeitService.getTteamDurchlaufzeiten(any())).thenReturn(durchlaufzeiten);
        when(durchlaufzeitDto.isLanglaufer()).thenReturn(true);
        when(durchlaufzeitDto.getId()).thenReturn(1L);
        ContextMocker.mockFacesContextForUrlParamterUtils();

        final boolean result = langlauferService.isTteamLanglaufer(idTypeTuples);
        assertTrue(result);
    }

    @Test
    void isVereinbarungLanglauferNoIdTypeTuples() {
        final boolean result = langlauferService.isVereinbarungLanglaufer(new IdTypeTupleCollection());
        assertFalse(result);
    }

    @Test
    void isVereinbarungLanglaufer() {
        when(durchlaufzeitService.getVereinbarungDurchlaufzeiten(any())).thenReturn(durchlaufzeiten);
        when(durchlaufzeitDto.isLanglaufer()).thenReturn(true);
        when(durchlaufzeitDto.getId()).thenReturn(1L);
        ContextMocker.mockFacesContextForUrlParamterUtils();

        final boolean result = langlauferService.isVereinbarungLanglaufer(idTypeTuples);
        assertTrue(result);
    }
}