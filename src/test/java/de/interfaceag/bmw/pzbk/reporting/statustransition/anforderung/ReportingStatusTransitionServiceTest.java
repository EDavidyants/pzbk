package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTransitionServiceTest {

    @Mock
    private ReportingStatusTransitionAnforderungStatusChangeService anforderungStatusChangeService;
    @Mock
    private ReportingStatusTransitionAnforderungProzessbaukastenEntryService anforderungProzessbaukastenEntryService;
    @Mock
    private ReportingStatusTransitionAnforderungProzessbaukastenExitService anforderungProzessbaukastenExitService;
    @Mock
    private ReportingStatusTransitionCreateAnforderungAsNewVersionService anforderungAsNewVersionService;
    @Mock
    private ReportingStatusTransitionCreateAnforderungFromMeldungService createAnforderungFromMeldungService;
    @InjectMocks
    private ReportingStatusTransitionService reportingStatusTransitionService;

    private Anforderung anforderung;
    private Status currentStatus;
    private Status newStatus;

    @Mock
    private ReportingStatusTransition reportingStatusTransition;

    @BeforeEach
    void setUp() {
        anforderung = TestDataFactory.generateAnforderung();
        currentStatus = Status.A_INARBEIT;
        newStatus = Status.A_FTABGESTIMMT;
    }

    @Test
    void addAnforderungToProzessbaukasten() {
        when(anforderungProzessbaukastenEntryService.addAnforderungToProzessbaukasten(any(), any(), any())).thenReturn(reportingStatusTransition);
        final ReportingStatusTransition result = reportingStatusTransitionService.addAnforderungToProzessbaukasten(anforderung, currentStatus);
        assertThat(result, is(reportingStatusTransition));
    }

    @Test
    void removeAnforderungFromProzessbaukasten() {
        when(anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(any(), any(), any())).thenReturn(reportingStatusTransition);
        final ReportingStatusTransition result = reportingStatusTransitionService.removeAnforderungFromProzessbaukasten(anforderung, currentStatus);
        assertThat(result, is(reportingStatusTransition));
    }

    @Test
    void changeAnforderungStatus() {
        when(anforderungStatusChangeService.changeAnforderungStatus(any(), any(), any(), any())).thenReturn(reportingStatusTransition);
        final ReportingStatusTransition result = reportingStatusTransitionService.changeAnforderungStatus(anforderung, currentStatus, newStatus);
        assertThat(result, is(reportingStatusTransition));
    }

    @Test
    void createAnforderungAsNewVersion() {
        when(anforderungAsNewVersionService.createAnforderungAsNewVersion(any(), any())).thenReturn(reportingStatusTransition);
        final ReportingStatusTransition result = reportingStatusTransitionService.createAnforderungAsNewVersion(anforderung);
        assertThat(result, is(reportingStatusTransition));
    }

    @Test
    void createAnforderungFromMeldung() {
        when(createAnforderungFromMeldungService.createAnforderungFromMeldung(any(), any())).thenReturn(reportingStatusTransition);
        final ReportingStatusTransition result = reportingStatusTransitionService.createAnforderungFromMeldung(anforderung);
        assertThat(result, is(reportingStatusTransition));
    }

    @Test
    void addAnforderungOhneMeldungToProzessbaukasten() {
        anforderung.setMeldungen(Collections.emptySet());
        final ReportingStatusTransition result = reportingStatusTransitionService.addAnforderungToProzessbaukasten(anforderung, currentStatus);
        Assertions.assertNull(result);
    }

    @Test
    void removeAnforderungOhneMeldungFromProzessbaukasten() {
        anforderung.setMeldungen(Collections.emptySet());
        final ReportingStatusTransition result = reportingStatusTransitionService.removeAnforderungFromProzessbaukasten(anforderung, currentStatus);
        Assertions.assertNull(result);
    }

    @Test
    void changeAnforderungOhneMeldungStatus() {
        anforderung.setMeldungen(Collections.emptySet());
        final ReportingStatusTransition result = reportingStatusTransitionService.changeAnforderungStatus(anforderung, currentStatus, newStatus);
        Assertions.assertNull(result);
    }

    @Test
    void createAnforderungOhneMeldungAsNewVersion() {
        anforderung.setMeldungen(Collections.emptySet());
        final ReportingStatusTransition result = reportingStatusTransitionService.createAnforderungAsNewVersion(anforderung);
        Assertions.assertNull(result);
    }

    @Test
    void createAnforderungOhneMeldungFromMeldung() {
        anforderung.setMeldungen(Collections.emptySet());
        final ReportingStatusTransition result = reportingStatusTransitionService.createAnforderungFromMeldung(anforderung);
        Assertions.assertNull(result);
    }

}
