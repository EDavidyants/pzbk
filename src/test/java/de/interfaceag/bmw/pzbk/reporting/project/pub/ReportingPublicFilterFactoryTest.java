package de.interfaceag.bmw.pzbk.reporting.project.pub;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import de.interfaceag.bmw.pzbk.filter.UrlFilter;
import de.interfaceag.bmw.pzbk.reporting.SelectedDerivatFilterService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot.ProjectProcessSnapshotReadService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableWithSize.iterableWithSize;
import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingPublicFilterFactoryTest {

    @Mock
    private UrlParameter urlParameter;
    @Mock
    private SelectedDerivatFilterService selectedDerivatFilterService;
    @Mock
    private ProjectProcessSnapshotReadService projectProcessSnapshotReadService;
    @InjectMocks
    private ReportingPublicFilterFactory reportingPublicFilterFactory;

    @Mock
    private Derivat derivat;
    @Mock
    private SnapshotFilter snapshotFilter;

    private List<SnapshotFilter> snapshotDates;

    @Test
    void getPublicProcessViewFilterSnapshotFilterHasCorrectSize() {
        when(snapshotFilter.getId()).thenReturn(1L);
        when(snapshotFilter.getDate()).thenReturn("Date");

        snapshotDates = Collections.singletonList(snapshotFilter);
        when(projectProcessSnapshotReadService.getSnapshotDatesForDerivat(any())).thenReturn(snapshotDates);
        when(selectedDerivatFilterService.getDerivatForUrlParameter(any())).thenReturn(Optional.of(derivat));

        final ReportingPublicFilter viewFilter = reportingPublicFilterFactory.getPublicProcessViewFilter(urlParameter);
        final List<SearchFilterObject> all = viewFilter.getSnapshotFilter().getAll();
        assertThat(all, iterableWithSize(1));
    }

    @Test
    void getPublicProcessViewFilterHasProjectProcessUrl() {
        final ReportingPublicFilter viewFilter = reportingPublicFilterFactory.getPublicProcessViewFilter(urlParameter);
        final String filter = viewFilter.filter();
        assertThat(filter, containsString(Page.REPORTING_PROJECT_PUBLIC_PROCESS.getUrl()));
    }

    @Test
    void getPublicProcessViewFilterDerivatFilter() {
        when(urlParameter.getValue(any())).thenReturn(Optional.of("E20"));

        final ReportingPublicFilter viewFilter = reportingPublicFilterFactory.getPublicProcessViewFilter(urlParameter);
        final UrlFilter derivatFilter = viewFilter.getDerivatFilter();
        final String attributeValue = derivatFilter.getAttributeValue();
        assertThat(attributeValue, is("E20"));
    }

    @Test
    void getPublicDashboardViewFilterDerivatFilter() {
        when(urlParameter.getValue(any())).thenReturn(Optional.of("E20"));

        final ReportingPublicFilter viewFilter = reportingPublicFilterFactory.getPublicDashboardViewFilter(urlParameter);
        final UrlFilter derivatFilter = viewFilter.getDerivatFilter();
        final String attributeValue = derivatFilter.getAttributeValue();
        assertThat(attributeValue, is("E20"));
    }

    @Test
    void getPublicDashboardViewFilterHasDashboardUrl() {
        final ReportingPublicFilter viewFilter = reportingPublicFilterFactory.getPublicDashboardViewFilter(urlParameter);
        final String filter = viewFilter.filter();
        assertThat(filter, containsString(Page.REPORTING_PROJECT_PUBLIC_DASHBOARD.getUrl()));
    }

    @Test
    void getPublicDashboardViewFilterSnapshotFilterIsEmpty() {
        final ReportingPublicFilter viewFilter = reportingPublicFilterFactory.getPublicDashboardViewFilter(urlParameter);
        final List<SearchFilterObject> all = viewFilter.getSnapshotFilter().getAll();
        assertThat(all, iterableWithSize(0));
    }
}