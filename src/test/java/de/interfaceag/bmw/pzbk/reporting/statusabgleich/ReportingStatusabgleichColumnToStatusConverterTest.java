package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ReportingStatusabgleichColumnToStatusConverterTest {

    @Test
    public void testConvertColumnToZakStatusCol0() {
        Collection<ZakStatus> result = ReportingStatusabgleichColumnToStatusConverter.convertColumnToZakStatus(0);
        Assertions.assertEquals(Collections.EMPTY_SET, result);
    }

    @Test
    public void testConvertColumnToZakStatusCol1() {
        Collection<ZakStatus> result = ReportingStatusabgleichColumnToStatusConverter.convertColumnToZakStatus(1);
        Assertions.assertEquals(Arrays.asList(ZakStatus.DONE), result);
    }

    @Test
    public void testConvertColumnToZakStatusCol2() {
        Collection<ZakStatus> result = ReportingStatusabgleichColumnToStatusConverter.convertColumnToZakStatus(2);
        Assertions.assertEquals(Arrays.asList(ZakStatus.BESTAETIGT_AUS_VORPROZESS, ZakStatus.KEINE_ZAKUEBERTRAGUNG), result);
    }

    @Test
    public void testConvertColumnToZakStatusCol3() {
        Collection<ZakStatus> result = ReportingStatusabgleichColumnToStatusConverter.convertColumnToZakStatus(3);
        Assertions.assertEquals(Arrays.asList(ZakStatus.PENDING), result);
    }

    @Test
    public void testConvertColumnToZakStatusCol4() {
        Collection<ZakStatus> result = ReportingStatusabgleichColumnToStatusConverter.convertColumnToZakStatus(4);
        Assertions.assertEquals(Arrays.asList(ZakStatus.EMPTY, ZakStatus.UEBERTRAGUNG_FEHLERHAFT), result);
    }

    @Test
    public void testConvertColumnToZakStatusCol5() {
        Collection<ZakStatus> result = ReportingStatusabgleichColumnToStatusConverter.convertColumnToZakStatus(5);
        Assertions.assertEquals(Arrays.asList(ZakStatus.NICHT_BEWERTBAR), result);
    }

    @Test
    public void testConvertColumnToZakStatusCol6() {
        Collection<ZakStatus> result = ReportingStatusabgleichColumnToStatusConverter.convertColumnToZakStatus(6);
        Assertions.assertEquals(Arrays.asList(ZakStatus.NICHT_UMSETZBAR), result);
    }

    @Test
    public void testConvertColumnToZakStatusCol7() {
        Collection<ZakStatus> result = ReportingStatusabgleichColumnToStatusConverter.convertColumnToZakStatus(7);
        Assertions.assertEquals(Arrays.asList(ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER), result);
    }

    @Test
    public void testConvertColumnToZakStatusCol8() {
        Collection<ZakStatus> result = ReportingStatusabgleichColumnToStatusConverter.convertColumnToZakStatus(8);
        Assertions.assertEquals(Collections.EMPTY_SET, result);
    }

}
