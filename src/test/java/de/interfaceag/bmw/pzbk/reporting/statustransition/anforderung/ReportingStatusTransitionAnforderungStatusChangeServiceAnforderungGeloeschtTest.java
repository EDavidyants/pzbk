package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author fn
 */
@ExtendWith(MockitoExtension.class)
public class ReportingStatusTransitionAnforderungStatusChangeServiceAnforderungGeloeschtTest {

    @Mock
    private ReportingStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Mock
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Mock
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Mock
    private ReportingStatusTransitionAnforderungEntyDateService entyDateService;

    @InjectMocks
    private ReportingStatusTransitionAnforderungStatusChangeService reportingStatusTransitionAnforderungStatusChangeService;

    @Mock
    private Anforderung anforderung;

    private ReportingStatusTransition reportingStatusTransition;

    @Mock
    private EntryExitTimeStamp exitTimeStamp;
    @Mock
    private EntryExitTimeStamp entryTimeStamp;
    @Mock
    private Date date;

    private Date newEntryDate = new Date();

    private Date newExitDate = new Date();

    private Date entryDate;

    @BeforeEach
    void setUp() {

        reportingStatusTransition = new ReportingStatusTransition();
        entryDate = new Date();
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(statusTransitionDatabaseAdapter.getLatestForAnforderung(anforderung)).thenReturn(Optional.empty());
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);
        when(reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(any(), any())).thenReturn(entryTimeStamp);
    }

    @Test
    void changeAnforderungStatusAbgestimmtToGeloeschtExit() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_FTABGESTIMMT, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp exit = statusTransition.getAbgestimmt();
        assertThat(exit, is(exitTimeStamp));
    }

    @Test
    void changeAnforderungStatusInArbeitToGeloeschtExit() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_INARBEIT, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp exit = statusTransition.getInArbeit();
        assertThat(exit, is(exitTimeStamp));
    }

    @Test
    void changeAnforderungStatusTteamUnstimmigToGeloeschtExit() {
        EntryExitTimeStamp entryTimeStamp = new EntryExitTimeStamp();
        entryTimeStamp.setEntry(newEntryDate);
        EntryExitTimeStamp entryExitTimeStamp = new EntryExitTimeStamp();
        entryExitTimeStamp.setEntry(newEntryDate);
        entryExitTimeStamp.setExit(newExitDate);

        when(reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(any(), any())).thenReturn(entryTimeStamp);
        ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_FTABGESTIMMT, Status.A_UNSTIMMIG, date);
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(entryExitTimeStamp);
        statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_UNSTIMMIG, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp exit = statusTransition.getTteamUnstimmig();
        Assertions.assertEquals(exit.getExit(), newExitDate);
    }

    @Test
    void changeAnforderungStatusVereinbarungUnstimmigToGeloeschtExit() {
        EntryExitTimeStamp entryTimeStamp = new EntryExitTimeStamp();
        entryTimeStamp.setEntry(newEntryDate);
        EntryExitTimeStamp entryExitTimeStamp = new EntryExitTimeStamp();
        entryExitTimeStamp.setEntry(newEntryDate);
        entryExitTimeStamp.setExit(newExitDate);

        when(reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(any(), any())).thenReturn(entryTimeStamp);
        ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_PLAUSIB, Status.A_UNSTIMMIG, date);
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(entryExitTimeStamp);
        statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_UNSTIMMIG, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp exit = statusTransition.getVereinbarungUnstimmig();
        Assertions.assertEquals(exit.getExit(), newExitDate);
    }

    @Test
    void changeAnforderungStatusPlausbilisiertToGeloeschtExit() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_PLAUSIB, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp exit = statusTransition.getPlausibilisiert();
        assertThat(exit, is(exitTimeStamp));
    }

    @Test
    void changeAnforderungStatusAbgestimmtToGeloeschtEntry() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_FTABGESTIMMT, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp entry = statusTransition.getTteamGeloescht();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void changeAnforderungStatusInArbeitToGeloeschtEntry() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_INARBEIT, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp entry = statusTransition.getFachteamGeloescht();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void changeAnforderungStatusUnstimmigToGeloeschtEntry() {
        EntryExitTimeStamp entryTimeStamp = new EntryExitTimeStamp();
        entryTimeStamp.setEntry(newEntryDate);
        EntryExitTimeStamp entryExitTimeStamp = new EntryExitTimeStamp();
        entryExitTimeStamp.setEntry(newEntryDate);
        entryExitTimeStamp.setExit(newExitDate);

        when(reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(any(), any())).thenReturn(entryTimeStamp);
        ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_PLAUSIB, Status.A_UNSTIMMIG, date);
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(entryExitTimeStamp);
        statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_UNSTIMMIG, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp entry = statusTransition.getFachteamGeloescht();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void changeAnforderungStatusPlausbilisiertToGeloeschtEntry() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_PLAUSIB, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp entry = statusTransition.getVereinbarungGeloescht();
        assertThat(entry, is(entryTimeStamp));
    }

}
