package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamAnforderungAbgestimmtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamAnforderungNewVersionKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamCurrentObjectsFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamGeloeschteObjekteKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamGemeldeteMeldungenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamMeldungExistingAnforderungKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FreigegebenCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FreigegebenGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FreigegebenProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamPlausibilisiertKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamUnstimmigKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungFreigegebenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungUnstimmigKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.langlaufer.DurchlaufzeitService;
import de.interfaceag.bmw.pzbk.reporting.processoverview.ProcessOverviewKeyFigures;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureFilterService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.StatusTransitionKeyFigureFactory;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureValidationService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Date;

import static org.hamcrest.collection.IsIterableWithSize.iterableWithSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StatusTransitionKeyFigureServiceTest {

    @Mock
    private KeyFigureValidationService keyFigureValidationService;
    @Mock
    private StatusTransitionKeyFigureFactory keyFigureFactory;
    @Mock
    private KeyFigureFilterService keyFigureFilterService;
    @InjectMocks
    private StatusTransitionKeyFigureService statusTransitionKeyFigureService;

    @Mock
    private DurchlaufzeitService durchlaufzeitService;
    @Mock
    private ReportingFilter filter;
    @Mock
    private FachteamAnforderungNewVersionKeyFigure fachteamAnforderungNewVersionKeyFigure;
    @Mock
    private VereinbarungUnstimmigKeyFigure vereinbarungUnstimmigKeyFigure;
    @Mock
    private TteamUnstimmigKeyFigure tteamUnstimmigKeyFigure;
    @Mock
    private FachteamGeloeschteObjekteKeyFigure fachteamGeleoschteObjekteKeyFigure;
    @Mock
    private TteamGeloeschtKeyFigure tteamGeloeschtKeyFigure;
    @Mock
    private VereinbarungGeloeschtKeyFigure vereinbarungGeloeschtKeyFigure;
    @Mock
    private FreigegebenGeloeschtKeyFigure freigegebenGeloeschtKeyFigure;
    @Mock
    private FachteamProzessbaukastenKeyFigure fachteamProzessbaukastenKeyFigure;
    @Mock
    private TteamProzessbaukastenKeyFigure tteamProzessbaukastenKeyFigure;
    @Mock
    private VereinbarungProzessbaukastenKeyFigure vereinbarungProzessbaukastenKeyFigure;
    @Mock
    private FreigegebenProzessbaukastenKeyFigure freigegebenProzessbaukastenKeyFigure;
    @Mock
    private FachteamMeldungExistingAnforderungKeyFigure fachteamMeldungExistingAnforderungKeyFigure;
    @Mock
    private FachteamAnforderungAbgestimmtKeyFigure fachteamAnforderungAbgestimmtKeyFigure;
    @Mock
    private TteamPlausibilisiertKeyFigure tteamPlausibilisiertKeyFigure;
    @Mock
    private VereinbarungFreigegebenKeyFigure vereinbarungFreigegebenKeyFigure;
    @Mock
    private FachteamGemeldeteMeldungenKeyFigure fachteamGemeldeteMeldungenKeyFigure;
    @Mock
    private FachteamCurrentObjectsFigure fachteamCurrentObjectsFigure;
    @Mock
    private TteamCurrentKeyFigure tteamCurrentKeyFigure;
    @Mock
    private VereinbarungCurrentKeyFigure vereinbarungCurrentKeyFigure;
    @Mock
    private FreigegebenCurrentKeyFigure freigegebenCurrentKeyFigure;
    @Mock
    private DateSearchFilter bisDateFilter;
    @Mock
    private Date selectedDate;

    @BeforeEach
    void setUp() throws InvalidDataException {

        when(keyFigureFactory.computeFachteamAnforderungNewVersionKeyFigure(any(), any())).thenReturn(fachteamAnforderungNewVersionKeyFigure);
        when(keyFigureFactory.computeTteamUnstimmigKeyFigure(any(), any())).thenReturn(tteamUnstimmigKeyFigure);
        when(keyFigureFactory.computeVereinbarungUnstimmigKeyFigure(any(), any())).thenReturn(vereinbarungUnstimmigKeyFigure);
        when(keyFigureFactory.computeFachteamGeloeschteObjekteKeyFigure(any(), any())).thenReturn(fachteamGeleoschteObjekteKeyFigure);
        when(keyFigureFactory.computeFachteamGemeldeteMeldungenKeyFigure(any(), any())).thenReturn(fachteamGemeldeteMeldungenKeyFigure);
        when(keyFigureFactory.computeFachteamUnstimmigKeyFigure(any())).thenReturn(TestDataFactory.generateFachteamUnstimmigKeyFigure());
        when(keyFigureFactory.computeFachteamInputKeyFigure(any())).thenReturn(TestDataFactory.generateFachteamInputKeyFigure());
        when(keyFigureFactory.computeFachteamOutputKeyFigure(any())).thenReturn(TestDataFactory.generateFachteamOutputKeyFigure());
        when(keyFigureFactory.computeFachteamCurrentObjectsFigure(any(), any())).thenReturn(fachteamCurrentObjectsFigure);
        when(keyFigureFactory.computeFreigegebenCurrentKeyFigure(any(), any())).thenReturn(freigegebenCurrentKeyFigure);
        when(filter.getBisDateFilter()).thenReturn(bisDateFilter);
        when(bisDateFilter.getSelected()).thenReturn(selectedDate);
    }

    private void mockFullCall() throws InvalidDataException {
        when(keyFigureFactory.computeTteamGeloeschtKeyFigure(any(), any())).thenReturn(tteamGeloeschtKeyFigure);
        when(keyFigureFactory.computeVereinbarungGeloeschtKeyFigure(any(), any())).thenReturn(vereinbarungGeloeschtKeyFigure);
        when(keyFigureFactory.computeFreigegebenGeloeschtKeyFigure(any(), any())).thenReturn(freigegebenGeloeschtKeyFigure);
        when(keyFigureFactory.computeFachteamProzessbaukastenKeyFigure(any(), any())).thenReturn(fachteamProzessbaukastenKeyFigure);
        when(keyFigureFactory.computeTteamProzessbaukastenKeyFigure(any(), any())).thenReturn(tteamProzessbaukastenKeyFigure);
        when(keyFigureFactory.computeVereinbarungProzessbaukastenKeyFigure(any(), any())).thenReturn(vereinbarungProzessbaukastenKeyFigure);
        when(keyFigureFactory.computeFreigegebenProzessbaukastenKeyFigure(any(), any())).thenReturn(freigegebenProzessbaukastenKeyFigure);
        when(keyFigureFactory.computeFachteamMeldungExistingAnforderungKeyFigure(any(), any())).thenReturn(fachteamMeldungExistingAnforderungKeyFigure);
        when(keyFigureFactory.computeTteamEntryKeyFigure(any(), any())).thenReturn(fachteamAnforderungAbgestimmtKeyFigure);
        when(keyFigureFactory.computeVereinbarungEntryKeyFigure(any(), any())).thenReturn(tteamPlausibilisiertKeyFigure);
        when(keyFigureFactory.computeFreigegebenEntryKeyFigure(any(), any())).thenReturn(vereinbarungFreigegebenKeyFigure);
        when(keyFigureFactory.computeTteamOutputKeyFigure(any())).thenReturn(TestDataFactory.generateTteamOutputKeyFigure());
        when(keyFigureFactory.computeVereinbarungOutputKeyFigure(any())).thenReturn(TestDataFactory.generateVereinbarungOutputKeyFigure());
        when(keyFigureFactory.computeTteamCurrentKeyFigure(any(), any())).thenReturn(tteamCurrentKeyFigure);
        when(keyFigureFactory.computeVereinbarungCurrentKeyFigure(any(), any())).thenReturn(vereinbarungCurrentKeyFigure);
    }

    @Test
    void getKeyFiguresForFachteamReporting() {
        final KeyFigureCollection keyFiguresForFachteamReporting = statusTransitionKeyFigureService.getKeyFiguresForFachteamReporting(filter);
        MatcherAssert.assertThat(keyFiguresForFachteamReporting, iterableWithSize(11));
    }

    @Test
    void getKeyFigures() throws InvalidDataException {
        mockFullCall();
        final KeyFigureCollection keyFigures = statusTransitionKeyFigureService.getKeyFigures(filter);
        MatcherAssert.assertThat(keyFigures, iterableWithSize(25));
    }

    @Test
    void getDetailProcessOverviewKeyFigures() throws InvalidDataException {
        mockFullCall();
        when(keyFigureValidationService.validateKeyFigures(any())).thenReturn(Collections.emptyList());
        final ProcessOverviewKeyFigures result = statusTransitionKeyFigureService.getDetailProcessOverviewKeyFigures(filter);
        final KeyFigureCollection keyFigures = result.getKeyFigures();
        MatcherAssert.assertThat(keyFigures, iterableWithSize(25));
    }
}
