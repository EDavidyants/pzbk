package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsIterableWithSize;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

class ProjectProcessSnapshotConverterTest {

    private static final Long ONE = 1L;
    private static final ProjectReportingProcessKeyFigure L_4 = ProjectReportingProcessKeyFigure.L4;
    private Date date = null;

    @BeforeEach
    void setUp() {
    }

    @Test
    void toKeyFigureDataCollection() {
        ProjectProcessSnapshotEntry entry = new ProjectProcessSnapshotEntry(L_4, ONE.intValue());
        Collection<ProjectProcessSnapshotEntry> entries = new ArrayList<>();
        entries.add(entry);

        final ProjectReportingProcessKeyFigureDataCollection result = ProjectProcessSnapshotConverter.toKeyFigureDataCollection(entries, date);

        final Optional<ProjectReportingProcessKeyFigureData> keyFigureData = result.getDataForKeyFigure(L_4);

        Assertions.assertTrue(keyFigureData.isPresent());
    }

    @Test
    void toKeyFigureDataCollectionValue() {
        ProjectProcessSnapshotEntry entry = new ProjectProcessSnapshotEntry(L_4, ONE.intValue());
        Collection<ProjectProcessSnapshotEntry> entries = new ArrayList<>();
        entries.add(entry);

        final ProjectReportingProcessKeyFigureDataCollection result = ProjectProcessSnapshotConverter.toKeyFigureDataCollection(entries, date);

        ProjectReportingProcessKeyFigureData keyFigureData = result.getDataForKeyFigure(L_4).get();

        final Long value = keyFigureData.getValue();

        MatcherAssert.assertThat(value, CoreMatchers.is(ONE));
    }

    @Test
    void toKeyFigureDataCollectionKeyFigure() {
        ProjectProcessSnapshotEntry entry = new ProjectProcessSnapshotEntry(L_4, ONE.intValue());
        Collection<ProjectProcessSnapshotEntry> entries = new ArrayList<>();
        entries.add(entry);

        final ProjectReportingProcessKeyFigureDataCollection result = ProjectProcessSnapshotConverter.toKeyFigureDataCollection(entries, date);

        ProjectReportingProcessKeyFigureData keyFigureData = result.getDataForKeyFigure(L_4).get();

        final ProjectReportingProcessKeyFigure keyFigure = keyFigureData.getKeyFigure();

        MatcherAssert.assertThat(keyFigure, CoreMatchers.is(L_4));
    }

    @Test
    void toProjectProcessSnapshotEntriesResultSize() {
        ProjectReportingProcessKeyFigureDataCollection keyFigureDataCollection = getProjectReportingProcessKeyFigureDataCollection();
        final Collection<ProjectProcessSnapshotEntry> result = ProjectProcessSnapshotConverter.toProjectProcessSnapshotEntries(keyFigureDataCollection);
        MatcherAssert.assertThat(result, IsIterableWithSize.iterableWithSize(1));
    }

    @Test
    void toProjectProcessSnapshotEntriesResultValue() {
        ProjectReportingProcessKeyFigureDataCollection keyFigureDataCollection = getProjectReportingProcessKeyFigureDataCollection();
        final Collection<ProjectProcessSnapshotEntry> result = ProjectProcessSnapshotConverter.toProjectProcessSnapshotEntries(keyFigureDataCollection);
        for (ProjectProcessSnapshotEntry next : result) {
            final int value = next.getValue();
            MatcherAssert.assertThat(value, CoreMatchers.is(ONE.intValue()));
        }
    }

    @Test
    void toProjectProcessSnapshotEntriesResultKeyFigure() {
        ProjectReportingProcessKeyFigureDataCollection keyFigureDataCollection = getProjectReportingProcessKeyFigureDataCollection();
        final Collection<ProjectProcessSnapshotEntry> result = ProjectProcessSnapshotConverter.toProjectProcessSnapshotEntries(keyFigureDataCollection);
        for (ProjectProcessSnapshotEntry next : result) {
            final ProjectReportingProcessKeyFigure keyFigure = next.getKeyFigure();
            MatcherAssert.assertThat(keyFigure, CoreMatchers.is(L_4));
        }
    }

    private static ProjectReportingProcessKeyFigureDataCollection getProjectReportingProcessKeyFigureDataCollection() {
        return new ProjectReportingProcessKeyFigureDataCollection() {
            @Override
            public Long getValueForKeyFigure(ProjectReportingProcessKeyFigure keyFigure) {
                if (keyFigure.equals(L_4)) {
                    return ONE;
                } else {
                    return 0L;
                }
            }

            @Override
            public Long getValueForKeyFigureId(Integer keyFigureId) {
                if (keyFigureId.equals(L_4.getId())) {
                    return ONE;
                } else {
                    return 0L;
                }
            }

            @Override
            public Optional<ProjectReportingProcessKeyFigureData> getDataForKeyFigure(ProjectReportingProcessKeyFigure keyFigure) {
                if (keyFigure.equals(L_4)) {
                    ProjectReportingProcessKeyFigureData keyFigureData = new ProjectReportingProcessKeyFigureData() {
                        @Override
                        public ProjectReportingProcessKeyFigure getKeyFigure() {
                            return L_4;
                        }

                        @Override
                        public Long getValue() {
                            return ONE;
                        }

                        @Override
                        public boolean isAmpelGreen() {
                            return false;
                        }
                    };

                    return Optional.of(keyFigureData);
                } else {
                    return Optional.empty();
                }
            }

            @Override
            public Date getDate() {
                return null;
            }

            @Override
            public boolean isAmpelGreenForKeyFigureId(Integer keyFigure) {
                return false;
            }
        };
    }
}
