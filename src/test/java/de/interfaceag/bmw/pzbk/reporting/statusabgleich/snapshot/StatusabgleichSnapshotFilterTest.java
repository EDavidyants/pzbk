package de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 * @author sl
 */
public class StatusabgleichSnapshotFilterTest {

    StatusabgleichSnapshotFilter statusabgleichSnapshotFilter;

    Date date;

    @BeforeEach
    public void setUp() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1992, 4, 7, 6, 52);
        date = calendar.getTime();
        statusabgleichSnapshotFilter = new StatusabgleichSnapshotFilter(1L, date);
    }

    @Test
    public void testGetId() {
        Long id = statusabgleichSnapshotFilter.getId();
        assertThat(id, is(1L));
    }

    @Test
    public void testGetDate() {
        String result = statusabgleichSnapshotFilter.getDate();
        String expected = "1992.05.07";
        assertThat(result, is(expected));
    }

}
