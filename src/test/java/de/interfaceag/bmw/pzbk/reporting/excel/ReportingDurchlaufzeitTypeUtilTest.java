package de.interfaceag.bmw.pzbk.reporting.excel;

import de.interfaceag.bmw.pzbk.enums.ReportingDurchlaufzeitType;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ReportingDurchlaufzeitTypeUtilTest {

    @ParameterizedTest
    @EnumSource(KeyType.class)
    public void testReportingDurchlaufzeitType(KeyType keyType) {

        ReportingDurchlaufzeitType durchlaufzeitType = ReportingDurchlaufzeitTypeUtil.getDurchlaufzeitTypeForKeyType(keyType);

        switch (keyType) {
            case FACHTEAM_CURRENT:
                assertThat(durchlaufzeitType, is(ReportingDurchlaufzeitType.FACHTEAM));
                break;

            case TTEAM_CURRENT:
                assertThat(durchlaufzeitType, is(ReportingDurchlaufzeitType.TTEAM));
                break;

            case VEREINBARUNG_CURRENT:
                assertThat(durchlaufzeitType, is(ReportingDurchlaufzeitType.VEREINBARUNG));
                break;

            default:
                assertThat(durchlaufzeitType, is(ReportingDurchlaufzeitType.UNDEFINED));
                break;
        }
    }

}
