package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessAmpelThresholdService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsMapContaining;
import org.hamcrest.collection.IsMapWithSize;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingUmsetzungsverwaltungKeyFigureUtilsVBBGTest {

    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR14;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR15;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR16;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR17;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR18;

    @InjectMocks
    private final ProjectProcessAmpelThresholdService ampelThresholdService = new ProjectProcessAmpelThresholdService();

    Map<Integer, ProjectReportingProcessKeyFigureData> keyFigureData;

    @BeforeEach
    public void setUp() {
        when(ProjectReportingProcessKeyFigureDataR14.getValue()).thenReturn(4L);
        when(ProjectReportingProcessKeyFigureDataR15.getValue()).thenReturn(5L);
        when(ProjectReportingProcessKeyFigureDataR16.getValue()).thenReturn(6L);
        when(ProjectReportingProcessKeyFigureDataR17.getValue()).thenReturn(7L);

        keyFigureData = new HashMap<>();
        keyFigureData.put(ProjectReportingProcessKeyFigure.R14.getId(), ProjectReportingProcessKeyFigureDataR14);
        keyFigureData.put(ProjectReportingProcessKeyFigure.R15.getId(), ProjectReportingProcessKeyFigureDataR15);
        keyFigureData.put(ProjectReportingProcessKeyFigure.R16.getId(), ProjectReportingProcessKeyFigureDataR16);
        keyFigureData.put(ProjectReportingProcessKeyFigure.R17.getId(), ProjectReportingProcessKeyFigureDataR17);
        keyFigureData.put(ProjectReportingProcessKeyFigure.R18.getId(), ProjectReportingProcessKeyFigureDataR18);
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBGResultSize() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungVBBG());
        MatcherAssert.assertThat(result, IsMapWithSize.aMapWithSize(7));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBGContainsR12() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungVBBG());
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R12.getId()));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBGR12Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungVBBG());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R12.getId());
        Long dataValue = data.getValue();
        MatcherAssert.assertThat(dataValue, is(22L));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBGContainsR13() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungVBBG());
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R13.getId()));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBGR13Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungVBBG());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R13.getId());
        Long dataValue = data.getValue();
        MatcherAssert.assertThat(dataValue, is(18L));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBGR13Ampel() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVBBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungVBBG());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R13.getId());
        boolean isAmpelGreen = data.isAmpelGreen();
        Assertions.assertFalse(isAmpelGreen);
    }

}
