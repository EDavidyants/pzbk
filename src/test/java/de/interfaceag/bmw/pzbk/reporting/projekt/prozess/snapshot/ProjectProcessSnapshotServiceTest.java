package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures.ProjectReportingKeyFigureService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollectionBuilder;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsIterableWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProjectProcessSnapshotServiceTest {

    private static final ProjectReportingProcessKeyFigure L_4 = ProjectReportingProcessKeyFigure.L4;
    private static final Long ONE = 1L;

    @Mock
    private ProjectProcessSnapshotDao statusabgleichSnapshotDao;
    @Mock
    private ProjectReportingKeyFigureService kennzahlenService;
    @InjectMocks
    private ProjectProcessSnapshotService processSnapshotService;

    @Mock
    private Derivat derivat;
    @Mock
    private ProjectProcessFilter filter;

    @Mock
    private List<SnapshotFilter> snapshotFilters;
    @Mock
    private ProjectProcessSnapshot snapshot;
    @Mock
    private ProjectProcessSnapshot snapshot1;

    private List<ProjectProcessSnapshot> snapshots;

    @BeforeEach
    void setup() {
        snapshots = new ArrayList<>();
        snapshots.add(snapshot);
        snapshots.add(snapshot1);
    }

    @Test
    void createNewSnapshotForDerivatDerivatValue() {
        setupCreateNewSnapshotForDerivat();
        final ProjectProcessSnapshot snapshot = processSnapshotService.createNewSnapshotForDerivat(derivat);
        final Derivat snapshotDerivat = snapshot.getDerivat();
        MatcherAssert.assertThat(snapshotDerivat, CoreMatchers.is(derivat));
    }

    @Test
    void createNewSnapshotForDerivatEntriesSize() {
        setupCreateNewSnapshotForDerivat();
        final ProjectProcessSnapshot snapshot = processSnapshotService.createNewSnapshotForDerivat(derivat);
        final Collection<ProjectProcessSnapshotEntry> entries = snapshot.getEntries();
        MatcherAssert.assertThat(entries, IsIterableWithSize.iterableWithSize(1));
    }

    @Test
    void createNewSnapshotForDerivatEntryValue() {
        setupCreateNewSnapshotForDerivat();
        final ProjectProcessSnapshot snapshot = processSnapshotService.createNewSnapshotForDerivat(derivat);
        final Collection<ProjectProcessSnapshotEntry> entries = snapshot.getEntries();
        for (ProjectProcessSnapshotEntry entry : entries) {
            final int value = entry.getValue();
            MatcherAssert.assertThat(value, CoreMatchers.is(ONE.intValue()));
        }
    }

    @Test
    void createNewSnapshotForDerivatEntryKeyFigure() {
        setupCreateNewSnapshotForDerivat();
        final ProjectProcessSnapshot snapshot = processSnapshotService.createNewSnapshotForDerivat(derivat);
        final Collection<ProjectProcessSnapshotEntry> entries = snapshot.getEntries();
        for (ProjectProcessSnapshotEntry entry : entries) {
            final ProjectReportingProcessKeyFigure keyFigure = entry.getKeyFigure();
            MatcherAssert.assertThat(keyFigure, CoreMatchers.is(L_4));
        }
    }

    private void setupCreateNewSnapshotForDerivat() {
        ProjectReportingProcessKeyFigureData keyFigureData = new ProjectReportingProcessKeyFigureData() {
            @Override
            public ProjectReportingProcessKeyFigure getKeyFigure() {
                return L_4;
            }

            @Override
            public Long getValue() {
                return ONE;
            }

            @Override
            public boolean isAmpelGreen() {
                return false;
            }
        };

        ProjectReportingProcessKeyFigureDataCollection keyFigureDataCollection = new ProjectReportingProcessKeyFigureDataCollectionBuilder().withKeyFigure(keyFigureData).build();

        when(kennzahlenService.buildProjectProcessFilterForDerivat()).thenReturn(filter);
        when(kennzahlenService.computeKeyFiguresForDerivat(any(), any(ProjectProcessFilter.class))).thenReturn(keyFigureDataCollection);
    }

    @Test
    void getSnapshotDatesForDerivat() {
        when(statusabgleichSnapshotDao.getAllDatesForDerivat(any())).thenReturn(snapshotFilters);
        final List<SnapshotFilter> result = processSnapshotService.getSnapshotDatesForDerivat(derivat);
        MatcherAssert.assertThat(result, CoreMatchers.is(snapshotFilters));
    }

    @Test
    void getForDerivatAndDate() {
        when(statusabgleichSnapshotDao.getForDerivatAndDate(any(), any())).thenReturn(Optional.of(snapshot));
        Optional<ProjectProcessSnapshot> result = processSnapshotService.getForDerivatAndDate(derivat, new Date());
        MatcherAssert.assertThat(result, CoreMatchers.is(Optional.of(snapshot)));
    }

    @Test
    void getByNullId() {
        when(statusabgleichSnapshotDao.getById(any())).thenReturn(Optional.of(snapshot));
        Optional<ProjectProcessSnapshot> result = processSnapshotService.getById(null);
        MatcherAssert.assertThat(result, CoreMatchers.is(Optional.of(snapshot)));
    }

    @Test
    void getById() {
        when(statusabgleichSnapshotDao.getById(any())).thenReturn(Optional.of(snapshot));
        Optional<ProjectProcessSnapshot> result = processSnapshotService.getById(ONE);
        MatcherAssert.assertThat(result, CoreMatchers.is(Optional.of(snapshot)));
    }

    @Test
    void getLatestSnapshotForDerivat() {
        when(statusabgleichSnapshotDao.getAllForDerivat(any())).thenReturn(snapshots);
        final Optional<ProjectProcessSnapshot> result = processSnapshotService.getLatestSnapshotForDerivat(derivat);
        MatcherAssert.assertThat(result, CoreMatchers.is(Optional.of(snapshot)));
    }

    @Test
    void getLatestSnapshotForDerivatNoSnapshotPresent() {
        when(statusabgleichSnapshotDao.getAllForDerivat(any())).thenReturn(Collections.emptyList());
        final Optional<ProjectProcessSnapshot> result = processSnapshotService.getLatestSnapshotForDerivat(derivat);
        MatcherAssert.assertThat(result, CoreMatchers.is(Optional.empty()));
    }
}
