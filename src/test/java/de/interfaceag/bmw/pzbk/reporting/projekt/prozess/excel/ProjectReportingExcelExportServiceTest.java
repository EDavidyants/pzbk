package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.excel;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures.ProjectReportingKeyFigureService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingExcelExportServiceTest {

    @Mock
    ProjectReportingKeyFigureService vereinbarungKeyFigureService;
    @Mock
    ExcelDownloadService excelDownloadService;
    @Mock
    ProjectReportingKeyFigureService projectReportingKeyFigureService;

    @InjectMocks
    ProjectReportingExcelExportService projectReportingExcelExportService;

    @Mock
    Derivat derivat;

    @Mock
    ProjectProcessFilter filter;

    @Mock
    UrlParameter urlParameter;

    @Mock
    ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat;

    @BeforeEach
    public void setUp() {
        when(derivat.getName()).thenReturn("E20");
        when(vereinbarungKeyFigureService.computeKeyFiguresForDerivat(any(), any(ProjectProcessFilter.class))).thenReturn(keyFiguresForDerivat);
    }

    @Test
    public void testDownloadKeyFiguresAsExcelFile() {
        projectReportingExcelExportService.downloadKeyFiguresAsExcelFile(derivat, filter);
        verify(vereinbarungKeyFigureService, times(1)).computeKeyFiguresForDerivat(any(), any(ProjectProcessFilter.class));
        verify(excelDownloadService, times(1)).downloadExcelExport(any(), any());
    }

}
