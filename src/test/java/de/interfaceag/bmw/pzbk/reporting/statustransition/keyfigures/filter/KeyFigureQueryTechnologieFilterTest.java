package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.filter.TechnologieFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapWithSize.aMapWithSize;
import static org.hamcrest.collection.IsMapWithSize.anEmptyMap;
import static org.hamcrest.text.IsEmptyString.emptyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class KeyFigureQueryTechnologieFilterTest {


    @Mock
    private ReportingFilter filter;
    @Mock
    private TechnologieFilter technologieFilter;

    private QueryPart queryPart;

    @BeforeEach
    void setUp() {
        queryPart = new QueryPartDTO();
        when(filter.getTechnologieFilter()).thenReturn(technologieFilter);
    }

    private static void setupActive(TechnologieFilter filter) {
        when(filter.isActive()).thenReturn(Boolean.TRUE);
        when(filter.getSelectedStringValues()).thenReturn(Collections.emptySet());
    }

    private void setupInactive() {
        when(technologieFilter.isActive()).thenReturn(Boolean.FALSE);
    }

    @Test
    void appendNotActive() {
        setupInactive();
        KeyFigureQueryTechnologieFilter.append(filter, queryPart);
        final String query = queryPart.getQuery();
        assertThat(query, is(emptyString()));
    }

    @Test
    void appendNotActiveParameterMap() {
        setupInactive();
        KeyFigureQueryTechnologieFilter.append(filter, queryPart);
        final Map<String, Object> parameterMap = queryPart.getParameterMap();
        assertThat(parameterMap, is(anEmptyMap()));
    }

    @Test
    void appendActive() {
        setupActive(technologieFilter);
        KeyFigureQueryTechnologieFilter.append(filter, queryPart);
        final String query = queryPart.getQuery();
        assertThat(query, is(" AND a.sensorCoc.ortung IN :technologien "));
    }


    @Test
    void appendActiveParameterMap() {
        setupActive(technologieFilter);
        KeyFigureQueryTechnologieFilter.append(filter, queryPart);
        final Map<String, Object> parameterMap = queryPart.getParameterMap();
        assertThat(parameterMap, is(aMapWithSize(1)));
    }

}