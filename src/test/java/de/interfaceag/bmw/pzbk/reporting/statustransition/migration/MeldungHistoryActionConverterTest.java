package de.interfaceag.bmw.pzbk.reporting.statustransition.migration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 *
 * @author fn
 */
public class MeldungHistoryActionConverterTest {

    public MeldungHistoryActionConverterTest() {
    }

    @Test
    public void convertActionRemoveFromAnforderung() {
        MeldungHistoryAction result = MeldungHistoryActionConverter.convert("Status", "zugeordnet", "gemeldet");
        Assertions.assertEquals(MeldungHistoryAction.REMOVE_FROM_ANFORDERUNG, result);
    }

    @Test
    public void convertActionAddToNewAnforderung() {
        MeldungHistoryAction result = MeldungHistoryActionConverter.convert("Status", "gemeldet", "zugeordnet");
        Assertions.assertEquals(MeldungHistoryAction.ADD_TO_NEW_ANFORDERUNG, result);
    }

    @Test
    public void convertActionAddToExistingAnforderung() {
        MeldungHistoryAction result = MeldungHistoryActionConverter.convert("Kommentar zum Statuswechsel", null, "wurde der bestehenden Anforderung A123 | V2 zugeordnet");
        Assertions.assertEquals(MeldungHistoryAction.ADD_TO_EXISTING_ANFORDERUNG, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"Entwurf", "gemeldet", "unstimmig"})
    public void convertActionStatusChangeWithVariableVonValueToGemeldet(String von) {
        MeldungHistoryAction result = MeldungHistoryActionConverter.convert("Status", von, "gemeldet");
        Assertions.assertEquals(MeldungHistoryAction.STATUS_CHANGE, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"Entwurf", "gemeldet", "unstimmig", "zugeordnet"})
    public void convertActionStatusChangeWithVariableVonValueToUnstimmig(String von) {
        MeldungHistoryAction result = MeldungHistoryActionConverter.convert("Status", von, "unstimmig");
        Assertions.assertEquals(MeldungHistoryAction.STATUS_CHANGE, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"Entwurf", "unstimmig", "zugeordnet"})
    public void convertActionStatusChangeWithVariableVonValueToZugeordnet(String von) {
        MeldungHistoryAction result = MeldungHistoryActionConverter.convert("Status", von, "zugeordnet");
        Assertions.assertEquals(MeldungHistoryAction.OTHER, result);
    }

    @Test
    public void convertActionIsNullSafe() {
        MeldungHistoryAction result = MeldungHistoryActionConverter.convert(null, null, null);
        Assertions.assertEquals(MeldungHistoryAction.OTHER, result);
    }
}
