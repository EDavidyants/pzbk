package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class TteamLanglauferKeyFigureSumCalculatorTest {

    private Collection<TteamKeyFigure> keyFigures;

    @Test
    void getVereinbarungLanglauferSum() {
    }

    @BeforeEach
    void setUp() {
        keyFigures = new ArrayList<>();
    }


    @Test
    void getTteamLanglauferSumWithNoEntry() {
        final int langlauferSum = TteamLanglauferKeyFigureSumCalculator.getTteamLanglauferSum(keyFigures);
        assertThat(langlauferSum, is(0));
    }

    @Test
    void getTteamLanglauferSumWithOneEntry() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        final int langlauferSum = TteamLanglauferKeyFigureSumCalculator.getTteamLanglauferSum(keyFigures);
        assertThat(langlauferSum, is(1));
    }

    @Test
    void getTteamLanglauferSumWithTwoEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3, 4));
        final int langlauferSum = TteamLanglauferKeyFigureSumCalculator.getTteamLanglauferSum(keyFigures);
        assertThat(langlauferSum, is(4));
    }

    @Test
    void getTteamLanglauferSumWithThreeEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3, 4));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(42, 42));
        final int langlauferSum = TteamLanglauferKeyFigureSumCalculator.getTteamLanglauferSum(keyFigures);
        assertThat(langlauferSum, is(46));
    }

    @Test
    void getVereinbarungLanglauferSumWithNoEntry() {
        final int langlauferSum = TteamLanglauferKeyFigureSumCalculator.getVereinbarungLanglauferSum(keyFigures);
        assertThat(langlauferSum, is(0));
    }

    @Test
    void getVereinbarungLanglauferSumWithOneEntry() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        final int langlauferSum = TteamLanglauferKeyFigureSumCalculator.getVereinbarungLanglauferSum(keyFigures);
        assertThat(langlauferSum, is(2));
    }

    @Test
    void getVereinbarungLanglauferSumWithTwoEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3, 4));
        final int langlauferSum = TteamLanglauferKeyFigureSumCalculator.getVereinbarungLanglauferSum(keyFigures);
        assertThat(langlauferSum, is(6));
    }

    @Test
    void getVereinbarungLanglauferSumWithThreeEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3, 4));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(42, 42));
        final int langlauferSum = TteamLanglauferKeyFigureSumCalculator.getVereinbarungLanglauferSum(keyFigures);
        assertThat(langlauferSum, is(48));
    }

    private TteamKeyFigure getFachteamKeyFigureWithDurchlaufzeit(int tteamLanglauferCount, int vereinbarungLanglauferCount) {
        final KeyFigureCollection keyFigureCollection = new KeyFigureCollection();
        DurchlaufzeitKeyFigure tteamDurchlaufzeitKeyFigure = getDurchlaufzeitKeyFigureWithValue(1);
        DurchlaufzeitKeyFigure vereinbarungDurchlaufzeitKeyFigure = getDurchlaufzeitKeyFigureWithValue(12);
        return new TteamKeyFigure(1L, "SensorCoc1", keyFigureCollection, tteamDurchlaufzeitKeyFigure, vereinbarungDurchlaufzeitKeyFigure, tteamLanglauferCount, vereinbarungLanglauferCount);
    }

    private DurchlaufzeitKeyFigure getDurchlaufzeitKeyFigureWithValue(int value) {
        return new DurchlaufzeitKeyFigure() {
            @Override
            public KeyType getKeyType() {
                return null;
            }

            @Override
            public int getKeyTypeId() {
                return 0;
            }

            @Override
            public int getValue() {
                return value;
            }
        };
    }


}