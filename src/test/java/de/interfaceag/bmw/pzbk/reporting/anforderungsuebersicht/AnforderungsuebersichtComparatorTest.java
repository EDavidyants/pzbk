package de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author evda
 */
public class AnforderungsuebersichtComparatorTest {

    private List<Anforderungsuebersicht> anforderungen;

    @Before
    public void setUp() {
        // List of seven objects inkl 3 Meldungen and 4 Anforderungen (inkl one with multiple versions)
        anforderungen = new ArrayList<>();
        AnforderungsuebersichtDto meldungOne = AnforderungsuebersichtDto.newBuilder()
                .withFachIdAndVersion("M1").build();

        AnforderungsuebersichtDto meldungTwo = AnforderungsuebersichtDto.newBuilder()
                .withFachIdAndVersion("M21").build();

        AnforderungsuebersichtDto meldungThree = AnforderungsuebersichtDto.newBuilder()
                .withFachIdAndVersion("M3").build();

        AnforderungsuebersichtDto anforderungOne = AnforderungsuebersichtDto.newBuilder()
                .withFachIdAndVersion("A1 | V1").build();

        AnforderungsuebersichtDto anforderungTwo = AnforderungsuebersichtDto.newBuilder()
                .withFachIdAndVersion("A1 | V2").build();

        AnforderungsuebersichtDto anforderungThree = AnforderungsuebersichtDto.newBuilder()
                .withFachIdAndVersion("A123 | V1").build();

        AnforderungsuebersichtDto anforderungFour = AnforderungsuebersichtDto.newBuilder()
                .withFachIdAndVersion("A13 | V1").build();

        anforderungen.add(meldungOne);
        anforderungen.add(meldungTwo);
        anforderungen.add(meldungThree);
        anforderungen.add(anforderungOne);
        anforderungen.add(anforderungTwo);
        anforderungen.add(anforderungThree);
        anforderungen.add(anforderungFour);
    }

    @Test
    public void testCollectionSort() {
        // Sort descending by fachId and version
        anforderungen.sort(new AnforderungsuebersichtComparator());
        // Expected order after sort
        // First 'A123 | V1'
        // Second 'A13 | V1'
        // Third 'A1 | V2'
        // Fourth 'A1 | V1'
        // Fifth 'M21'
        // Sixth 'M3'
        // Seventh 'M1'
        Assertions.assertEquals("A123 | V1", anforderungen.get(0).getFachIdAndVersion());
        Assertions.assertEquals("A13 | V1", anforderungen.get(1).getFachIdAndVersion());
        Assertions.assertEquals("A1 | V2", anforderungen.get(2).getFachIdAndVersion());
        Assertions.assertEquals("A1 | V1", anforderungen.get(3).getFachIdAndVersion());
        Assertions.assertEquals("M21", anforderungen.get(4).getFachIdAndVersion());
        Assertions.assertEquals("M3", anforderungen.get(5).getFachIdAndVersion());
        Assertions.assertEquals("M1", anforderungen.get(6).getFachIdAndVersion());
    }

    @Test
    public void testCollectionSortReverseOrder() {
        // Sort ascending by fachId and version
        anforderungen.sort(Collections.reverseOrder(new AnforderungsuebersichtComparator()));
        // Expected order after sort
        // First 'M1'
        // Second 'M3'
        // Third 'M21'
        // Fourth 'A1 | V1'
        // Fifth 'A1 | V2'
        // Sixth 'A13 | V1'
        // Seventh 'A123 | V1'

        Assertions.assertEquals("M1", anforderungen.get(0).getFachIdAndVersion());
        Assertions.assertEquals("M3", anforderungen.get(1).getFachIdAndVersion());
        Assertions.assertEquals("M21", anforderungen.get(2).getFachIdAndVersion());
        Assertions.assertEquals("A1 | V1", anforderungen.get(3).getFachIdAndVersion());
        Assertions.assertEquals("A1 | V2", anforderungen.get(4).getFachIdAndVersion());
        Assertions.assertEquals("A13 | V1", anforderungen.get(5).getFachIdAndVersion());
        Assertions.assertEquals("A123 | V1", anforderungen.get(6).getFachIdAndVersion());
    }

}
