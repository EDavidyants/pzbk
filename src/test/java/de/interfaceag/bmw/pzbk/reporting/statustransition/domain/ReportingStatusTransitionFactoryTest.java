package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTransitionFactoryTest {

    @InjectMocks
    private ReportingStatusTransitionFactory reportingStatusTransitionFactory;

    @Mock
    private Date entryDate;
    @Mock
    private Anforderung anforderung;
    @Mock
    private Meldung meldung;

    @Test
    void getNewStatusTransitionForAnforderungWithEntryDateAnforderungValue() {
        final ReportingStatusTransition result = reportingStatusTransitionFactory.getNewStatusTransitionForAnforderungWithEntryDate(anforderung, entryDate);
        final Anforderung resultAnforderung = result.getAnforderung();
        assertThat(resultAnforderung, is(anforderung));
    }

    @Test
    void getNewStatusTransitionForAnforderungWithEntryDateEntryDateValue() {
        final ReportingStatusTransition result = reportingStatusTransitionFactory.getNewStatusTransitionForAnforderungWithEntryDate(anforderung, entryDate);
        final Date resultEntryDate = result.getEntryDate();
        assertThat(resultEntryDate, is(entryDate));
    }

    @Test
    void getNewStatusTransitionForMeldungWithEntryDateMeldungValue() {
        final ReportingMeldungStatusTransition result = reportingStatusTransitionFactory.getNewStatusTransitionForMeldungWithEntryDate(meldung, entryDate);
        final Meldung resultMeldung = result.getMeldung();
        assertThat(resultMeldung, is(meldung));
    }

    @Test
    void getNewStatusTransitionForMeldungWithEntryDateEntryDateValue() {
        final ReportingMeldungStatusTransition result = reportingStatusTransitionFactory.getNewStatusTransitionForMeldungWithEntryDate(meldung, entryDate);
        final Date resultEntryDate = result.getEntryDate();
        assertThat(resultEntryDate, is(entryDate));
    }

}