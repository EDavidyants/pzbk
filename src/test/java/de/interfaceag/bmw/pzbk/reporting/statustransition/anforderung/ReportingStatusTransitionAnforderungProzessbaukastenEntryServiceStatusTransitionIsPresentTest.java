package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTransitionAnforderungProzessbaukastenEntryServiceStatusTransitionIsPresentTest {

    @Mock
    private ReportingStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Mock
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Mock
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Mock
    private ReportingStatusTransitionAnforderungEntyDateService entyDateService;
    @Mock
    private ReportingStatusTransitionAnforderungStatusChangeService statusTransitionAnforderungStatusChangeService;
    @InjectMocks
    private ReportingStatusTransitionAnforderungProzessbaukastenEntryService reportingStatusTransitionAnforderungProzessbaukastenEntryService;

    @Mock
    private Anforderung anforderung;

    private ReportingStatusTransition reportingStatusTransition;

    @Mock
    private EntryExitTimeStamp exitTimeStamp;
    @Mock
    private EntryExitTimeStamp entryTimeStamp;
    @Mock
    private Date date;

    private Date entryDate;

    @BeforeEach
    void setUp() {
        reportingStatusTransition = new ReportingStatusTransition();
        entryDate = new Date();
        when(statusTransitionDatabaseAdapter.getLatestForAnforderung(anforderung)).thenReturn(Optional.of(reportingStatusTransition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(any(), any())).thenReturn(entryTimeStamp);
    }

    @Test
    void addAnforderungToProzessbaukastenInArbeitEntry() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungProzessbaukastenEntryService.addAnforderungToProzessbaukasten(anforderung, Status.A_INARBEIT, date);
        final EntryExitTimeStamp entry = statusTransition.getFachteamProzessbaukasten();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void addAnforderungToProzessbaukastenUnstimmigEntry() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungProzessbaukastenEntryService.addAnforderungToProzessbaukasten(anforderung, Status.A_UNSTIMMIG, date);
        final EntryExitTimeStamp entry = statusTransition.getFachteamProzessbaukasten();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void addAnforderungToProzessbaukastenAbgestimmtEntry() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungProzessbaukastenEntryService.addAnforderungToProzessbaukasten(anforderung, Status.A_FTABGESTIMMT, date);
        final EntryExitTimeStamp entry = statusTransition.getTteamProzessbaukasten();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void addAnforderungToProzessbaukastenPlausibilisiertEntry() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungProzessbaukastenEntryService.addAnforderungToProzessbaukasten(anforderung, Status.A_PLAUSIB, date);
        final EntryExitTimeStamp entry = statusTransition.getVereinbarungProzessbaukasten();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void addAnforderungToProzessbaukastenFreigegebenEntry() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungProzessbaukastenEntryService.addAnforderungToProzessbaukasten(anforderung, Status.A_FREIGEGEBEN, date);
        final EntryExitTimeStamp entry = statusTransition.getFreigegebenProzessbaukasten();
        assertThat(entry, is(entryTimeStamp));
    }

}
