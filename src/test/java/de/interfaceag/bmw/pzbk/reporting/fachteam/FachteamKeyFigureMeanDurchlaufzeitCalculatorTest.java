package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class FachteamKeyFigureMeanDurchlaufzeitCalculatorTest {

    Collection<FachteamKeyFigure> keyFigures;

    @BeforeEach
    void setUp() {
        keyFigures = new ArrayList<>();
    }

    @Test
    void computeMeanDurchlaufzeitNoEntry() {
        final int durchlaufzeit = FachteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(0));
    }

    @Test
    void computeMeanDurchlaufzeitOneEntry() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1));
        final int durchlaufzeit = FachteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(1));
    }

    @Test
    void computeMeanDurchlaufzeitTwoEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3));
        final int durchlaufzeit = FachteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(2));
    }

    @Test
    void computeMeanDurchlaufzeitThreeEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(12));
        final int durchlaufzeit = FachteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(5));
    }

    @Test
    void computeMeanDurchlaufzeitFourEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(12));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(42));
        final int durchlaufzeit = FachteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(15));
    }

    @Test
    void computeMeanDurchlaufzeitFiveEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(12));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(42));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(222));
        final int durchlaufzeit = FachteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(56));
    }

    private FachteamKeyFigure getFachteamKeyFigureWithDurchlaufzeit(int durchlaufzeit) {
        final KeyFigureCollection keyFigureCollection = new KeyFigureCollection();
        DurchlaufzeitKeyFigure durchlaufzeitKeyFigure = getDurchlaufzeitKeyFigureWithValue(durchlaufzeit);
        return new FachteamKeyFigure(1L, "SensorCoc1", keyFigureCollection, durchlaufzeitKeyFigure);
    }

    private DurchlaufzeitKeyFigure getDurchlaufzeitKeyFigureWithValue(int value) {
        return new DurchlaufzeitKeyFigure() {
            @Override
            public KeyType getKeyType() {
                return null;
            }

            @Override
            public int getKeyTypeId() {
                return 0;
            }

            @Override
            public int getValue() {
                return value;
            }
        };
    }
}