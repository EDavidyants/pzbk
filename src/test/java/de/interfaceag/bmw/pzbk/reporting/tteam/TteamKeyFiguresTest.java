package de.interfaceag.bmw.pzbk.reporting.tteam;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
class TteamKeyFiguresTest {

    @Mock
    private List<TteamKeyFigure> keyFigures;

    private TteamKeyFigures tteamKeyFigures;

    @BeforeEach
    void setUp() {
        tteamKeyFigures = new TteamKeyFigures(keyFigures);
    }

    @Test
    void getKeyFigures() {
        final Collection<TteamKeyFigure> tteamKeyFiguresKeyFigures = tteamKeyFigures.getKeyFigures();
        assertThat(tteamKeyFiguresKeyFigures, is(keyFigures));
    }
}