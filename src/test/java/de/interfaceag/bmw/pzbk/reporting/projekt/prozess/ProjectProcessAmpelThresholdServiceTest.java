package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ProjectProcessAmpelThresholdServiceTest {

    @InjectMocks
    private final ProjectProcessAmpelThresholdService ampelThresholdService = new ProjectProcessAmpelThresholdService();

    @Test
    public void testGetThresholdForVereinbarungVKBGKeyFigureL3() {
        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.L3);
        MatcherAssert.assertThat(thresholdForGreenAmpel, is(ampelThresholdService.getThresholdForVereinbarungVKBG()));
    }

    @Test
    public void testGetThresholdForVereinbarungVKBGValue() {
        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.L3);
        MatcherAssert.assertThat(thresholdForGreenAmpel, is(0.85));
    }

    @Test
    public void testGetThresholdForVereinbarungZVKeyFigureR5() {
        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.R5);
        MatcherAssert.assertThat(thresholdForGreenAmpel, is(ampelThresholdService.getThresholdForVereinbarungZV()));
    }

    @Test
    public void testGetThresholdForVereinbarungZVValue() {
        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.R5);
        MatcherAssert.assertThat(thresholdForGreenAmpel, is(0.95));
    }

    @Test
    public void testGetThresholdForUmsetzungsverwaltungVKBGKeyFigureL11() {
        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.L11);
        MatcherAssert.assertThat(thresholdForGreenAmpel, is(ampelThresholdService.getThresholdForUmsetzungsverwaltungVKBG()));
    }

    @Test
    public void testGetThresholdForUmsetzungsverwaltungVKBGValue() {
        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.L11);
        MatcherAssert.assertThat(thresholdForGreenAmpel, is(0.60));
    }

    @Test
    public void testGetThresholdForUmsetzungsverwaltungVBBGKeyFigureR13() {
        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.R13);
        MatcherAssert.assertThat(thresholdForGreenAmpel, is(ampelThresholdService.getThresholdForUmsetzungsverwaltungVBBG()));
    }

    @Test
    public void testGetThresholdForUmsetzungsverwaltungVBBGValue() {
        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.R13);
        MatcherAssert.assertThat(thresholdForGreenAmpel, is(0.85));
    }

    @Test
    public void testGetThresholdForUmsetzungsverwaltungBBGKeyFigureR20() {
        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.R20);
        MatcherAssert.assertThat(thresholdForGreenAmpel, is(ampelThresholdService.getThresholdForUmsetzungsverwaltungBBG()));
    }

    @Test
    public void testGetThresholdForUmsetzungsverwaltungBBGValue() {
        Double thresholdForGreenAmpel = ampelThresholdService.getThresholdValueForKeyFigure(ProjectReportingProcessKeyFigure.R20);
        MatcherAssert.assertThat(thresholdForGreenAmpel, is(0.95));
    }

}
