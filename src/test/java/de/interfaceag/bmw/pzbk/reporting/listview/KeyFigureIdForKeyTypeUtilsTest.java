package de.interfaceag.bmw.pzbk.reporting.listview;

import de.interfaceag.bmw.pzbk.reporting.fachteam.FachteamKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamCurrentObjectsFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamGeloeschteObjekteKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamInputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

class KeyFigureIdForKeyTypeUtilsTest {

    private KeyFigureIdForKeyTypeUtils<FachteamKeyFigure> keyFigureIdForKeyTypeUtils = new KeyFigureIdForKeyTypeUtils<>();

    private String LABEL = "Label";
    private Collection<FachteamKeyFigure> keyFigures;
    private Collection<Long> ids1;
    private Collection<Long> ids2;

    @BeforeEach
    void setUp() {
        ids1 = Arrays.asList(1L, 1L, 42L);
        ids2 = Arrays.asList(2L, 42L);
        keyFigures = new ArrayList<>();
    }

    @Test
    void getAnforderungIdsForKeyTypeUniqueValues() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids2, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final Collection<Long> anforderungIdsForKeyType = keyFigureIdForKeyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.FACHTEAM_GELOESCHT);
        MatcherAssert.assertThat(anforderungIdsForKeyType, IsIterableContaining.hasItems(2L, 42L));
    }

    @Test
    void getAnforderungIdsForKeyTypeUniqueValuesSize() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids2, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final Collection<Long> anforderungIdsForKeyType = keyFigureIdForKeyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.FACHTEAM_GELOESCHT);
        MatcherAssert.assertThat(anforderungIdsForKeyType, IsCollectionWithSize.hasSize(2));
    }

    @Test
    void getAnforderungIdsForKeyTypeDuplicatedValues() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final Collection<Long> anforderungIdsForKeyType = keyFigureIdForKeyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.FACHTEAM_GELOESCHT);
        MatcherAssert.assertThat(anforderungIdsForKeyType, IsIterableContaining.hasItems(1L, 42L));
    }

    @Test
    void getAnforderungIdsForKeyTypeDuplicatedValuesSize() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final Collection<Long> anforderungIdsForKeyType = keyFigureIdForKeyTypeUtils.getAnforderungIdsForKeyType(keyFigures, KeyType.FACHTEAM_GELOESCHT);
        MatcherAssert.assertThat(anforderungIdsForKeyType, IsCollectionWithSize.hasSize(3));
    }

    @Test
    void getMeldungIdsForKeyTypeUniqueValues() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids2, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final Collection<Long> meldungIdsForKeyType = keyFigureIdForKeyTypeUtils.getMeldungIdsForKeyType(keyFigures, KeyType.FACHTEAM_GELOESCHT);
        MatcherAssert.assertThat(meldungIdsForKeyType, IsIterableContaining.hasItems(2L, 42L));
    }

    @Test
    void getMeldungIdsForKeyTypeUniqueValuesSize() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids2, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final Collection<Long> meldungIdsForKeyType = keyFigureIdForKeyTypeUtils.getMeldungIdsForKeyType(keyFigures, KeyType.FACHTEAM_GELOESCHT);
        MatcherAssert.assertThat(meldungIdsForKeyType, IsCollectionWithSize.hasSize(2));
    }

    @Test
    void getMeldungIdsForKeyTypeDulicatedValues() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids1);
        keyFigures.add(fachteamKeyFigure1);
        final Collection<Long> meldungIdsForKeyType = keyFigureIdForKeyTypeUtils.getMeldungIdsForKeyType(keyFigures, KeyType.FACHTEAM_GELOESCHT);
        MatcherAssert.assertThat(meldungIdsForKeyType, IsIterableContaining.hasItems(1L, 42L));
    }

    @Test
    void getMeldungIdsForKeyTypeDulicatedValuesSize() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids1);
        keyFigures.add(fachteamKeyFigure1);
        final Collection<Long> meldungIdsForKeyType = keyFigureIdForKeyTypeUtils.getMeldungIdsForKeyType(keyFigures, KeyType.FACHTEAM_GELOESCHT);
        MatcherAssert.assertThat(meldungIdsForKeyType, IsCollectionWithSize.hasSize(3));
    }


    private FachteamKeyFigure buildFachteamKeyFigureWithFachteamKeyFigures(Collection<Long> anforderungIds, Collection<Long> meldungIds) {
        DurchlaufzeitKeyFigure durchlaufZeitKeyFigure = getDurchlaufZeitKeyFigure(5);
        KeyFigureCollection keyFigureCollection = new KeyFigureCollection();
        keyFigureCollection.add(new FachteamGeloeschteObjekteKeyFigure(anforderungIds, meldungIds));
        keyFigureCollection.add(new FachteamOutputKeyFigure(anforderungIds, meldungIds, 0L));
        keyFigureCollection.add(new FachteamCurrentObjectsFigure(meldungIds, anforderungIds, 0L));
        keyFigureCollection.add(new FachteamInputKeyFigure(meldungIds, anforderungIds, 0L));
        return new FachteamKeyFigure(1L, LABEL, keyFigureCollection, durchlaufZeitKeyFigure);
    }

    private static DurchlaufzeitKeyFigure getDurchlaufZeitKeyFigure(int value) {
        return new DurchlaufzeitKeyFigure() {
            @Override
            public KeyType getKeyType() {
                return KeyType.DURCHLAUFZEIT_FACHTEAM;
            }

            @Override
            public int getKeyTypeId() {
                return KeyType.DURCHLAUFZEIT_FACHTEAM.getId();
            }

            @Override
            public int getValue() {
                return value;
            }
        };
    }

}