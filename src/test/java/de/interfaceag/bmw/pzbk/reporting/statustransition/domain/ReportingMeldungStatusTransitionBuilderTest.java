package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import de.interfaceag.bmw.pzbk.entities.Meldung;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
class ReportingMeldungStatusTransitionBuilderTest {

    @Mock
    private Date date;
    @Mock
    private Meldung meldung;
    @Mock
    private EntryExitTimeStamp timeStamp;

    private ReportingMeldungStatusTransitionBuilder builder;

    @BeforeEach
    void setUp() {
        builder = new ReportingMeldungStatusTransitionBuilder();
    }

    @Test
    void setEntryDate() {
        builder.setEntryDate(date);
        final ReportingMeldungStatusTransition reportingMeldungStatusTransition = builder.build();
        final Date reportingMeldungStatusTransitionEntryDate = reportingMeldungStatusTransition.getEntryDate();
        assertThat(reportingMeldungStatusTransitionEntryDate, is(date));
    }

    @Test
    void setMeldung() {
        builder.setMeldung(meldung);
        final ReportingMeldungStatusTransition reportingMeldungStatusTransition = builder.build();
        final Meldung reportingMeldungStatusTransitionMeldung = reportingMeldungStatusTransition.getMeldung();
        assertThat(reportingMeldungStatusTransitionMeldung, is(meldung));
    }

    @Test
    void setGemeldet() {
        builder.setGemeldet(timeStamp);
        final ReportingMeldungStatusTransition reportingMeldungStatusTransition = builder.build();
        final EntryExitTimeStamp reportingMeldungStatusTransitionGemeldet = reportingMeldungStatusTransition.getGemeldet();
        assertThat(reportingMeldungStatusTransitionGemeldet, is(timeStamp));
    }

    @Test
    void setUnstimmig() {
        builder.setUnstimmig(timeStamp);
        final ReportingMeldungStatusTransition reportingMeldungStatusTransition = builder.build();
        final EntryExitTimeStamp reportingMeldungStatusTransitionUnstimmig = reportingMeldungStatusTransition.getUnstimmig();
        assertThat(reportingMeldungStatusTransitionUnstimmig, is(timeStamp));
    }

    @Test
    void setGeloescht() {
        builder.setGeloescht(timeStamp);
        final ReportingMeldungStatusTransition reportingMeldungStatusTransition = builder.build();
        final EntryExitTimeStamp reportingMeldungStatusTransitionGeloescht = reportingMeldungStatusTransition.getGeloescht();
        assertThat(reportingMeldungStatusTransitionGeloescht, is(timeStamp));
    }

    @Test
    void setNewAnforderungZugeordnet() {
        builder.setNewAnforderungZugeordnet(timeStamp);
        final ReportingMeldungStatusTransition reportingMeldungStatusTransition = builder.build();
        final EntryExitTimeStamp reportingMeldungStatusTransitionNewAnforderungZugeordnet = reportingMeldungStatusTransition.getNewAnforderungZugeordnet();
        assertThat(reportingMeldungStatusTransitionNewAnforderungZugeordnet, is(timeStamp));
    }

    @Test
    void setExistingAnforderungZugeordnet() {
        builder.setExistingAnforderungZugeordnet(timeStamp);
        final ReportingMeldungStatusTransition reportingMeldungStatusTransition = builder.build();
        final EntryExitTimeStamp reportingMeldungStatusTransitionExistingAnforderungZugeordnet = reportingMeldungStatusTransition.getExistingAnforderungZugeordnet();
        assertThat(reportingMeldungStatusTransitionExistingAnforderungZugeordnet, is(timeStamp));
    }

    @Test
    void buildDefaultNotNull() {
        final ReportingMeldungStatusTransition reportingMeldungStatusTransition = builder.build();
        assertThat(reportingMeldungStatusTransition, is(CoreMatchers.notNullValue()));
    }
}