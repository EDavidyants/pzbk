package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungDurchlaufzeitKeyFigure;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class TteamKeyFigureTest {

    private static final long TTEAM_ID = 42L;
    private static final String TTEAM_NAME = "NAME";
    private static final int TTEAMDURCHLAUFZEIT = 14;
    private static final int VEREINBARUNGDURCHLAUFZEIT = 12;
    private int vereinbarungLanglauferCount = 1;
    private int tteamLanglauferCount = 2;
    private DurchlaufzeitKeyFigure vereinbarungDurchlaufzeitKeyFigure;
    private DurchlaufzeitKeyFigure tteamDurchlaufzeitKeyFigure;
    private KeyFigureCollection keyFigures;
    private KeyFigure keyFigure;

    private TteamKeyFigure tteamKeyFigure;

    @BeforeEach
    void setUp() {
        keyFigure = new TteamCurrentKeyFigure(Collections.emptyList());
        keyFigures = new KeyFigureCollection();
        keyFigures.add(keyFigure);

        tteamDurchlaufzeitKeyFigure = new TteamDurchlaufzeitKeyFigure(TTEAMDURCHLAUFZEIT);
        vereinbarungDurchlaufzeitKeyFigure = new VereinbarungDurchlaufzeitKeyFigure(VEREINBARUNGDURCHLAUFZEIT);
        tteamKeyFigure = new TteamKeyFigure(TTEAM_ID, TTEAM_NAME, keyFigures, tteamDurchlaufzeitKeyFigure, vereinbarungDurchlaufzeitKeyFigure, tteamLanglauferCount, vereinbarungLanglauferCount);
    }

    @Test
    void getTteamIdDefault() {
        tteamKeyFigure = new TteamKeyFigure(TTEAM_NAME, keyFigures, tteamDurchlaufzeitKeyFigure, vereinbarungDurchlaufzeitKeyFigure, tteamLanglauferCount, vereinbarungLanglauferCount);
        final long tteamId = tteamKeyFigure.getTteamId();
        assertThat(tteamId, is(-1L));
    }

    @Test
    void getTteamId() {
        final long tteamId = tteamKeyFigure.getTteamId();
        assertThat(tteamId, is(TTEAM_ID));
    }

    @Test
    void getTteamName() {
        final String tteamName = tteamKeyFigure.getTteamName();
        assertThat(tteamName, is(TTEAM_NAME));
    }

    @Test
    void getKeyFigures() {
        final Collection<KeyFigure> tteamKeyFigureKeyFigures = tteamKeyFigure.getKeyFigures();
        assertThat(tteamKeyFigureKeyFigures, is(keyFigures));
    }

    @Test
    void getTteamDurchlaufzeit() {
        final int tteamDurchlaufzeit = tteamKeyFigure.getTteamDurchlaufzeit();
        assertThat(tteamDurchlaufzeit, is(TTEAMDURCHLAUFZEIT));
    }

    @Test
    void getVereinbarungDurchlaufzeit() {
        final int vereinbarungDurchlaufzeit = tteamKeyFigure.getVereinbarungDurchlaufzeit();
        assertThat(vereinbarungDurchlaufzeit, is(VEREINBARUNGDURCHLAUFZEIT));
    }

    @Test
    void getTteamLanglauferCount() {
        final int tteamKeyFigureTteamLanglauferCount = tteamKeyFigure.getTteamLanglauferCount();
        assertThat(tteamKeyFigureTteamLanglauferCount, is(tteamLanglauferCount));
    }

    @Test
    void getVereinbarungLanglauferCount() {
        final int tteamKeyFigureVereinbarungLanglauferCount = tteamKeyFigure.getVereinbarungLanglauferCount();
        assertThat(tteamKeyFigureVereinbarungLanglauferCount, is(vereinbarungLanglauferCount));
    }

    @Test
    void getKeyFigureByTypeId() {
        final KeyFigure keyFigureByTypeId = tteamKeyFigure.getKeyFigureByTypeId(10);
        assertThat(keyFigureByTypeId, is(keyFigure));
    }

    @Test
    void getKeyFigureByType() {
        final KeyFigure keyFigureByTypeId = tteamKeyFigure.getKeyFigureByType(KeyType.TTEAM_CURRENT);
        assertThat(keyFigureByTypeId, is(keyFigure));
    }
}