package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionCreatePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author fn
 */
@ExtendWith(MockitoExtension.class)
public class ReportingStatusTransitionAnforderungStatusChangeServiceAnforderungGeloeschtExitTest {

    @Mock
    private ReportingStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Mock
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Mock
    private ReportingStatusTransitionAnforderungEntyDateService entyDateService;

    @InjectMocks
    private ReportingStatusTransitionAnforderungStatusChangeService reportingStatusTransitionAnforderungStatusChangeService;

    @Mock
    private Anforderung anforderung;

    private ReportingStatusTransition reportingStatusTransition;

    @Mock
    private Date date;

    private Date newEntryDate = new Date();

    private Date newExitDate = new Date();

    private Date entryDate;

    @BeforeEach
    void setUp() {

        reportingStatusTransition = new ReportingStatusTransition();
        entryDate = new Date();
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(statusTransitionDatabaseAdapter.getLatestForAnforderung(anforderung)).thenReturn(Optional.empty());
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
    }


    @Test
    void changeAnforderungStatusUnstimmigGeloeschtToInArbeitExit() {
        EntryExitTimeStamp entryTimeStamp = new EntryExitTimeStamp();
        entryTimeStamp.setEntry(newEntryDate);
        EntryExitTimeStamp entryExitTimeStamp = new EntryExitTimeStamp();
        entryExitTimeStamp.setEntry(newEntryDate);
        entryExitTimeStamp.setExit(newExitDate);

        ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_GELOESCHT, Status.A_INARBEIT, date);
        final EntryExitTimeStamp exit = statusTransition.getFachteamGeloescht();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void changeAnforderungStatusUnstimmigGeloeschtToInArbeitEntry() {
        EntryExitTimeStamp entryTimeStamp = new EntryExitTimeStamp();
        entryTimeStamp.setEntry(newEntryDate);
        EntryExitTimeStamp entryExitTimeStamp = new EntryExitTimeStamp();
        entryExitTimeStamp.setEntry(newEntryDate);
        entryExitTimeStamp.setExit(newExitDate);

        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_GELOESCHT, Status.A_INARBEIT, date);
        final EntryExitTimeStamp entry = statusTransition.getInArbeit();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void changeAnforderungStatusInArbeitGeloeschtToInArbeitExit() {
        EntryExitTimeStamp entryTimeStamp = new EntryExitTimeStamp();
        entryTimeStamp.setEntry(newEntryDate);
        EntryExitTimeStamp entryExitTimeStamp = new EntryExitTimeStamp();
        entryExitTimeStamp.setEntry(newEntryDate);
        entryExitTimeStamp.setExit(newExitDate);

        ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_GELOESCHT, Status.A_INARBEIT, date);
        final EntryExitTimeStamp exit = statusTransition.getFachteamGeloescht();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void changeAnforderungStatusInArbeitGeloeschtToInArbeitEntry() {
        EntryExitTimeStamp entryTimeStamp = new EntryExitTimeStamp();
        entryTimeStamp.setEntry(newEntryDate);
        EntryExitTimeStamp entryExitTimeStamp = new EntryExitTimeStamp();
        entryExitTimeStamp.setEntry(newEntryDate);
        entryExitTimeStamp.setExit(newExitDate);

        ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_GELOESCHT, Status.A_INARBEIT, date);
        final EntryExitTimeStamp entry = statusTransition.getInArbeit();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void changeAnforderungStatusAbgestimmtGeloeschtToInArbeitExit() {
        EntryExitTimeStamp entryTimeStamp = new EntryExitTimeStamp();
        entryTimeStamp.setEntry(newEntryDate);
        EntryExitTimeStamp entryExitTimeStamp = new EntryExitTimeStamp();
        entryExitTimeStamp.setEntry(newEntryDate);
        entryExitTimeStamp.setExit(newExitDate);

        ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_GELOESCHT, Status.A_INARBEIT, date);
        final EntryExitTimeStamp exit = statusTransition.getTteamGeloescht();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void changeAnforderungStatusAbgestimmtGeloeschtToInArbeitEntry() {
        EntryExitTimeStamp entryTimeStamp = new EntryExitTimeStamp();
        entryTimeStamp.setEntry(newEntryDate);
        EntryExitTimeStamp entryExitTimeStamp = new EntryExitTimeStamp();
        entryExitTimeStamp.setEntry(newEntryDate);
        entryExitTimeStamp.setExit(newExitDate);

        ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_GELOESCHT, Status.A_INARBEIT, date);
        final EntryExitTimeStamp entry = statusTransition.getInArbeit();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void changeAnforderungStatusPlausibilisiertGeloeschtToInArbeitExit() {
        EntryExitTimeStamp entryTimeStamp = new EntryExitTimeStamp();
        entryTimeStamp.setEntry(newEntryDate);

        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_GELOESCHT, Status.A_INARBEIT, date);

        final EntryExitTimeStamp exit = statusTransition.getVereinbarungGeloescht();

        assertThat(exit, is(nullValue()));
    }

    @Test
    void changeAnforderungStatusPlausibilisiertGeloeschtToInArbeitEntry() {
        EntryExitTimeStamp entryTimeStamp = new EntryExitTimeStamp();
        entryTimeStamp.setEntry(newEntryDate);
        EntryExitTimeStamp entryExitTimeStamp = new EntryExitTimeStamp();
        entryExitTimeStamp.setEntry(newEntryDate);
        entryExitTimeStamp.setExit(newExitDate);

        ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_GELOESCHT, Status.A_INARBEIT, date);
        final EntryExitTimeStamp entry = statusTransition.getInArbeit();
        assertThat(entry, is(nullValue()));
    }

}
