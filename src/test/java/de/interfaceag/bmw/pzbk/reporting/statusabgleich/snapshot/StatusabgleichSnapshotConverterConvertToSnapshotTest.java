package de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.StatusabgleichKennzahlen;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class StatusabgleichSnapshotConverterConvertToSnapshotTest {

    @Mock
    private Derivat derivat;

    private StatusabgleichKennzahlen kennzahlen;

    @BeforeEach
    public void setUp() {
        Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> matrixValues = new HashMap<>();
        Tuple<DerivatAnforderungModulStatus, ZakStatus> tuple1 = new GenericTuple<>(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.DONE);
        matrixValues.put(tuple1, 1L);
        kennzahlen = new StatusabgleichKennzahlen(matrixValues);
    }

    @Test
    public void testConvertToSnapshotDerivat() {
        StatusabgleichSnapshot snapshot = StatusabgleichSnapshotConverter.convertToSnapshot(kennzahlen, derivat);
        Derivat result = snapshot.getDerivat();
        assertThat(result, is(derivat));
    }

    @Test
    public void testConvertToSnapshotListSize() {
        StatusabgleichSnapshot snapshot = StatusabgleichSnapshotConverter.convertToSnapshot(kennzahlen, derivat);
        List<StatusabgleichSnapshotEntry> result = snapshot.getEntries();
        assertThat(result, hasSize(1));
    }

    @Test
    public void testConvertToSnapshotEntryValues() {
        StatusabgleichSnapshot snapshot = StatusabgleichSnapshotConverter.convertToSnapshot(kennzahlen, derivat);
        List<StatusabgleichSnapshotEntry> entries = snapshot.getEntries();
        StatusabgleichSnapshotEntry entry = entries.get(0);
        DerivatAnforderungModulStatus vereinbarungStatus = entry.getVereinbarungStatus();
        ZakStatus zakStatus = entry.getZakStatus();
        int value = entry.getEntry();
        assertThat(value, is(1));
        assertThat(vereinbarungStatus, is(DerivatAnforderungModulStatus.ABZUSTIMMEN));
        assertThat(zakStatus, is(ZakStatus.DONE));
    }

}
