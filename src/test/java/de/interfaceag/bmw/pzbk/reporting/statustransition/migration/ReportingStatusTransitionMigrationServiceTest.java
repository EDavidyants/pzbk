package de.interfaceag.bmw.pzbk.reporting.statustransition.migration;

import com.google.common.collect.Lists;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.AnforderungReportingStatusTransitionAdapterForMigration;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.ReportingStatusTransitionDatabaseAdapter;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.meldung.MeldungReportingStatusTransitionAdapterForMigration;
import de.interfaceag.bmw.pzbk.reporting.statustransition.meldung.ReportingMeldungStatusTransitionDatabaseAdapter;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author fn
 */
@ExtendWith(MockitoExtension.class)
public class ReportingStatusTransitionMigrationServiceTest {

    @Mock
    private AnforderungMeldungHistoryService historyService;
    @Mock
    private AnforderungReportingStatusTransitionAdapterForMigration anforderungReportingStatusTransitionAdapterForMigration;
    @Mock
    private ReportingMeldungStatusTransitionDatabaseAdapter reportingMeldungStatusTransitionDatabaseAdapter;
    @Mock
    private MeldungReportingStatusTransitionAdapterForMigration meldungReportingStatusTransitionAdapterForMigration;
    @Mock
    private ReportingStatusTransitionDatabaseAdapter reportingStatusTransitionDatabaseAdapter;
    @Mock
    private ReportingStatusTransition reportingStatusTransition;

    @Mock
    private ReportingMeldungStatusTransition reportingMeldungStatusTransition;
    @Mock
    private AnforderungService anforderungService;

    @InjectMocks
    private ReportingStatusTransitionMigrationService reportingStatusTransitionMigrationService;

    @Test
    public void migrateAnforderungTestAnforderungAllStatus() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();

        when(historyService.getAnforderungHistoryMigrationDtosForAnforderung(any())).thenReturn(TestDataFactory.generateAnforderungHistoryMigrationDtosForAnforderungAllStatus(anforderung.getId()));
        when(anforderungReportingStatusTransitionAdapterForMigration.createAnforderungFromMeldung(any(), any())).thenReturn(reportingStatusTransition);
        when(anforderungReportingStatusTransitionAdapterForMigration.changeAnforderungStatus(any(), any(), any(), any())).thenReturn(reportingStatusTransition);
        when(reportingStatusTransitionDatabaseAdapter.isHistoryAlreadyExisting(any())).thenReturn(false);
        ReportingStatusTransitionMigrationResult result = reportingStatusTransitionMigrationService.migrateAnforderung(anforderung);

        Assertions.assertEquals(17, result.getMigrationResult().size());

    }

    @Test
    public void migrateAnforderungTestAnforderungWithProzessbaukasten() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();

        when(historyService.getAnforderungHistoryMigrationDtosForAnforderung(any())).thenReturn(TestDataFactory.generateAnforderungHistoryMigrationDtosForAnforderungWithProzessbaukasten(anforderung.getId()));
        when(reportingStatusTransitionDatabaseAdapter.isHistoryAlreadyExisting(any())).thenReturn(false);
        when(anforderungReportingStatusTransitionAdapterForMigration.createAnforderungFromMeldung(any(), any())).thenReturn(reportingStatusTransition);
        when(anforderungReportingStatusTransitionAdapterForMigration.addAnforderungToProzessbaukasten(any(), any(), any())).thenReturn(reportingStatusTransition);
        when(anforderungReportingStatusTransitionAdapterForMigration.removeAnforderungFromProzessbaukasten(any(), any(), any())).thenReturn(reportingStatusTransition);

        ReportingStatusTransitionMigrationResult result = reportingStatusTransitionMigrationService.migrateAnforderung(anforderung);

        Assertions.assertEquals(3, result.getMigrationResult().size());

    }

    @Test
    public void migrateAnforderungTestAnforderungNewVersion() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();

        when(historyService.getAnforderungHistoryMigrationDtosForAnforderung(any()))
                .thenReturn(Arrays.asList(TestDataFactory.generateAnforderungHistoryMigrationDtoForNewVersion(anforderung.getId())));

        when(anforderungReportingStatusTransitionAdapterForMigration.createAnforderungAsNewVersion(any(), any())).thenReturn(reportingStatusTransition);
        when(reportingStatusTransitionDatabaseAdapter.isHistoryAlreadyExisting(any())).thenReturn(false);
        ReportingStatusTransitionMigrationResult result = reportingStatusTransitionMigrationService.migrateAnforderung(anforderung);

        Assertions.assertEquals(1, result.getMigrationResult().size());

    }

    @Test
    public void migrateAnforderungTestAllAnforderungen() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();

        when(anforderungService.getAllAnforderungenWithMeldung()).thenReturn(Lists.newArrayList(anforderung, anforderung, anforderung));
        when(historyService.getAnforderungHistoryMigrationDtosForAnforderung(any())).thenReturn(TestDataFactory.generateAnforderungHistoryMigrationDtosForAnforderungAllStatus(anforderung.getId()));
        when(reportingStatusTransitionDatabaseAdapter.isHistoryAlreadyExisting(any())).thenReturn(false);
        when(anforderungReportingStatusTransitionAdapterForMigration.createAnforderungFromMeldung(any(), any())).thenReturn(reportingStatusTransition);
        when(anforderungReportingStatusTransitionAdapterForMigration.changeAnforderungStatus(any(), any(), any(), any())).thenReturn(reportingStatusTransition);

        List<ReportingStatusTransitionMigrationResult> result = reportingStatusTransitionMigrationService.migrateAnforderungen();

        Assertions.assertEquals(3, result.size());

    }

    @Test
    public void migrateAnforderungenAlreadyInDatabase() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        when(reportingStatusTransitionDatabaseAdapter.isHistoryAlreadyExisting(any())).thenReturn(true);
        ReportingStatusTransitionMigrationResult result = reportingStatusTransitionMigrationService.migrateAnforderung(anforderung);

        Assertions.assertEquals(0, result.getMigrationResult().size());

    }

    @Test
    public void migrateMeldungTestMeldungAllStatus() {
        Meldung meldung = TestDataFactory.generateMeldung();

        when(historyService.getMeldungHistoryMigrationDtosForAnforderung(any())).thenReturn(TestDataFactory.generateMeldungHistoryMigrationDtosForMeldungAllStatus(meldung.getId()));
        when(reportingMeldungStatusTransitionDatabaseAdapter.isHistoryAlreadyExisting(any())).thenReturn(false);

        when(meldungReportingStatusTransitionAdapterForMigration.changeMeldungStatus(any(), any(), any(), any())).thenReturn(reportingMeldungStatusTransition);

        ReportingMeldungStatusTransitionMigrationResult result = reportingStatusTransitionMigrationService.migrateMeldung(meldung);

        Assertions.assertEquals(7, result.getMigrationResult().size());

    }

    @Test
    public void migrateMeldungTestAddToNewAnforderung() {
        Meldung meldung = TestDataFactory.generateMeldung();

        when(historyService.getMeldungHistoryMigrationDtosForAnforderung(any())).thenReturn(Arrays.asList(TestDataFactory.generateMeldungHistoryMigrationDtoForAddToNewAnforderung(meldung.getId())));
        when(reportingMeldungStatusTransitionDatabaseAdapter.isHistoryAlreadyExisting(any())).thenReturn(false);

        when(meldungReportingStatusTransitionAdapterForMigration.addMeldungToNewAnforderung(any(), any())).thenReturn(reportingMeldungStatusTransition);

        ReportingMeldungStatusTransitionMigrationResult result = reportingStatusTransitionMigrationService.migrateMeldung(meldung);

        Assertions.assertEquals(1, result.getMigrationResult().size());
    }

    @Test
    public void migrateMeldungTestAddToExistingAnforderung() {
        Meldung meldung = TestDataFactory.generateMeldung();

        when(historyService.getMeldungHistoryMigrationDtosForAnforderung(any())).thenReturn(Arrays.asList(TestDataFactory.generateMeldungHistoryMigrationDtoForAddToExistingAnforderung(meldung.getId())));
        when(reportingMeldungStatusTransitionDatabaseAdapter.isHistoryAlreadyExisting(any())).thenReturn(false);

        when(meldungReportingStatusTransitionAdapterForMigration.addMeldungToExistingAnforderung(any(), any())).thenReturn(reportingMeldungStatusTransition);

        ReportingMeldungStatusTransitionMigrationResult result = reportingStatusTransitionMigrationService.migrateMeldung(meldung);

        Assertions.assertEquals(1, result.getMigrationResult().size());
    }

    @Test
    public void migrateMeldungTestRemoveFromAnforderung() {
        Meldung meldung = TestDataFactory.generateMeldung();

        when(historyService.getMeldungHistoryMigrationDtosForAnforderung(any())).thenReturn(Arrays.asList(TestDataFactory.generateMeldungHistoryMigrationDtoForRemoveFromAnforderung(meldung.getId())));
        when(reportingMeldungStatusTransitionDatabaseAdapter.isHistoryAlreadyExisting(any())).thenReturn(false);

        when(meldungReportingStatusTransitionAdapterForMigration.removeMeldungFromAnforderung(any(), any())).thenReturn(reportingMeldungStatusTransition);

        ReportingMeldungStatusTransitionMigrationResult result = reportingStatusTransitionMigrationService.migrateMeldung(meldung);

        Assertions.assertEquals(1, result.getMigrationResult().size());
    }

    public void migrateMeldungAllMeldungen() {
        Meldung meldung = TestDataFactory.generateMeldung();

        when(anforderungService.getAllMeldungen()).thenReturn(Arrays.asList(meldung, meldung, meldung));
        when(historyService.getMeldungHistoryMigrationDtosForAnforderung(any())).thenReturn(TestDataFactory.generateMeldungHistoryMigrationDtosForMeldungAllStatus(meldung.getId()));
        when(reportingMeldungStatusTransitionDatabaseAdapter.isHistoryAlreadyExisting(any())).thenReturn(false);

        when(meldungReportingStatusTransitionAdapterForMigration.changeMeldungStatus(any(), any(), any(), any())).thenReturn(reportingMeldungStatusTransition);

        List<ReportingMeldungStatusTransitionMigrationResult> result = reportingStatusTransitionMigrationService.migrateMeldungen();

        Assertions.assertEquals(3, result.size());
    }

    @Test
    public void migrateMeldungenIdAllreadyInDatabase() {
        Meldung meldung = TestDataFactory.generateMeldung();

        when(reportingMeldungStatusTransitionDatabaseAdapter.isHistoryAlreadyExisting(any())).thenReturn(true);

        ReportingMeldungStatusTransitionMigrationResult result = reportingStatusTransitionMigrationService.migrateMeldung(meldung);

        Assertions.assertEquals(0, result.getMigrationResult().size());
    }
}
