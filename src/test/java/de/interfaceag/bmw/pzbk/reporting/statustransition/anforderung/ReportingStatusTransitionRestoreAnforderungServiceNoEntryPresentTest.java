package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTransitionRestoreAnforderungServiceNoEntryPresentTest {

    @Mock
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;

    @InjectMocks
    private ReportingStatusTransitionRestoreAnforderungService restoreAnforderungService;

    @Mock
    private Date oldDate;
    @Mock
    private Date newDate;

    @Mock
    private Anforderung anforderung;

    private ReportingStatusTransition statusTransitionBefore;

    @BeforeEach
    void setUp() {
        EntryExitTimeStamp timeStamp = new EntryExitTimeStamp();
        timeStamp.setEntry(oldDate);
        timeStamp.setExit(oldDate);

        statusTransitionBefore = new ReportingStatusTransition();
        statusTransitionBefore.setInArbeit(timeStamp);

        when(statusTransitionDatabaseAdapter.getLatestForAnforderung(any())).thenReturn(Optional.of(statusTransitionBefore));
    }


    @Test
    void restoreAnforderungNoEntryPresent() {
        when(statusTransitionDatabaseAdapter.getLatestForAnforderung(any())).thenReturn(Optional.empty());
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        assertThat(statusTransition, is(nullValue()));
    }

}