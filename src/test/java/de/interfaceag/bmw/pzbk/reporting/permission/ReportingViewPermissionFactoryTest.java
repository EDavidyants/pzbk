package de.interfaceag.bmw.pzbk.reporting.permission;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.reporting.dashboard.ReportingDashboardViewPermission;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingViewPermissionFactoryTest {

    @Mock
    Session session;

    @InjectMocks
    ReportingViewPermissionFactory factory;

    @Mock
    private UserPermissions<BerechtigungDto> userPermission;

    private Set<Rolle> roles;

    @BeforeEach
    void setUp() {
        roles = new HashSet<>();
        when(session.getUserPermissions()).thenReturn(userPermission);
        when(userPermission.getRoles()).thenReturn(roles);
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getViewPermissionForPage(Rolle role) {
        roles.add(role);
        final ReportingDashboardViewPermission viewPermission = factory.getReportingDashboardViewPermission();
        final boolean page = viewPermission.isPage();

        switch (role) {
            case ADMIN:
            case ANFORDERER:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case E_COC:
                assertTrue(page);
                break;
            default:
                assertFalse(page);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getViewPermissionIsProcessOverviewSimple(Rolle role) {
        roles.add(role);
        final ReportingDashboardViewPermission viewPermission = factory.getReportingDashboardViewPermission();
        final boolean page = viewPermission.isProcessOverviewSimple();

        switch (role) {
            case ADMIN:
            case ANFORDERER:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case E_COC:
                assertTrue(page);
                break;
            default:
                assertFalse(page);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getViewPermissionIsProcessOverviewDetail(Rolle role) {
        roles.add(role);
        final ReportingDashboardViewPermission viewPermission = factory.getReportingDashboardViewPermission();
        final boolean page = viewPermission.isProcessOverviewDetail();

        switch (role) {
            case ADMIN:
            case ANFORDERER:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case E_COC:
                assertTrue(page);
                break;
            default:
                assertFalse(page);
                break;
        }
    }
}