package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTransitionRestoreAnforderungServiceTest {

    @Mock
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Mock
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;

    @InjectMocks
    private ReportingStatusTransitionRestoreAnforderungService restoreAnforderungService;

    @Mock
    private Date oldDate;
    @Mock
    private Date newDate;

    @Mock
    private Anforderung anforderung;

    private ReportingStatusTransition statusTransitionBefore;

    @BeforeEach
    void setUp() {
        statusTransitionBefore = new ReportingStatusTransition();
        statusTransitionBefore.setInArbeit(getNewTimeStamp());
        statusTransitionBefore.setAbgestimmt(getNewTimeStamp());
        statusTransitionBefore.setPlausibilisiert(getNewTimeStamp());

        statusTransitionBefore.setFreigegeben(getNewTimeStamp());
        statusTransitionBefore.setKeineWeiterverfolgung(getNewTimeStamp());
        statusTransitionBefore.setFachteamUnstimmig(getNewTimeStamp());
        statusTransitionBefore.setTteamUnstimmig(getNewTimeStamp());
        statusTransitionBefore.setVereinbarungUnstimmig(getNewTimeStamp());

        statusTransitionBefore.setFachteamGeloescht(getNewTimeStamp());
        statusTransitionBefore.setTteamGeloescht(getNewTimeStamp());
        statusTransitionBefore.setVereinbarungGeloescht(getNewTimeStamp());
        statusTransitionBefore.setFreigegebenGeloescht(getNewTimeStamp());

        statusTransitionBefore.setFachteamProzessbaukasten(getNewTimeStamp());
        statusTransitionBefore.setTteamProzessbaukasten(getNewTimeStamp());
        statusTransitionBefore.setVereinbarungProzessbaukasten(getNewTimeStamp());
        statusTransitionBefore.setFreigegebenProzessbaukasten(getNewTimeStamp());

        when(statusTransitionDatabaseAdapter.getLatestForAnforderung(any())).thenReturn(Optional.of(statusTransitionBefore));

        Answer<EntryExitTimeStamp> clearTimeStamp = invocation -> {
            EntryExitTimeStamp argument = invocation.getArgument(0);
            if (argument != null) {
                argument.clear();
                return argument;
            } else {
                return new EntryExitTimeStamp();
            }
        };
        when(reportingStatusTransitionTimeStampUpdatePort.clearTimeStamp(any())).thenAnswer(clearTimeStamp);

        Answer<EntryExitTimeStamp> updateEntryForTimeStamp = invocation -> {
            EntryExitTimeStamp entryExitTimeStamp = invocation.getArgument(0);
            Date date = invocation.getArgument(1);

            if (entryExitTimeStamp == null) {
                entryExitTimeStamp = new EntryExitTimeStamp();
            }

            entryExitTimeStamp.setEntry(date);
            return entryExitTimeStamp;
        };
        when(reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(any(), any())).thenAnswer(updateEntryForTimeStamp);

    }

    private EntryExitTimeStamp getNewTimeStamp() {
        EntryExitTimeStamp timeStamp = new EntryExitTimeStamp();
        timeStamp.setEntry(oldDate);
        timeStamp.setExit(oldDate);
        return timeStamp;
    }

    @Test
    void restoreAnforderungInArbeitEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp inArbeit = statusTransition.getInArbeit();
        final Date entry = inArbeit.getEntry();
        assertThat(entry, is(newDate));
    }

    @Test
    void restoreAnforderungInArbeitExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp inArbeit = statusTransition.getInArbeit();
        final Date entry = inArbeit.getExit();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungAbgestimmtEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp abgestimmt = statusTransition.getAbgestimmt();
        final Date entry = abgestimmt.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungAbgestimmtExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp abgestimmt = statusTransition.getAbgestimmt();
        final Date exit = abgestimmt.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungPlausibilisiertEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp plausibilisiert = statusTransition.getPlausibilisiert();
        final Date entry = plausibilisiert.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungPlausibilisiertExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp plausibilisiert = statusTransition.getPlausibilisiert();
        final Date exit = plausibilisiert.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungFreigegebenEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp freigegeben = statusTransition.getFreigegeben();
        final Date entry = freigegeben.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungFreigegebenExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp freigegeben = statusTransition.getFreigegeben();
        final Date exit = freigegeben.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungKeineWeiterverfolgungEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getKeineWeiterverfolgung();
        final Date entry = timestamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungKeineWeiterverfolgungExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getKeineWeiterverfolgung();
        final Date exit = timestamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungFachteamUnstimmigEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getFachteamUnstimmig();
        final Date entry = timestamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungFachteamUnstimmigExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getFachteamUnstimmig();
        final Date exit = timestamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungTteamUnstimmigEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getTteamUnstimmig();
        final Date entry = timestamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungTteamUnstimmigExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getTteamUnstimmig();
        final Date exit = timestamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungVereinbarungUnstimmigEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getVereinbarungUnstimmig();
        final Date entry = timestamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungVereinbarungUnstimmigExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getVereinbarungUnstimmig();
        final Date exit = timestamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungFachteamGeloeschtEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getFachteamGeloescht();
        final Date entry = timestamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungFachteamGeloeschtExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getFachteamGeloescht();
        final Date exit = timestamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungTteamGeloeschtEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getTteamGeloescht();
        final Date entry = timestamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungTteamGeloeschtExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getTteamGeloescht();
        final Date exit = timestamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungVereinbarungGeloeschtEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getVereinbarungGeloescht();
        final Date entry = timestamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungVereinbarungGeloeschtExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getVereinbarungGeloescht();
        final Date exit = timestamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungFreigegebenGeloeschtEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getFreigegebenGeloescht();
        final Date entry = timestamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungFreigegebenGeloeschtExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getFreigegebenGeloescht();
        final Date exit = timestamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungFachteamProzessbaukastenEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getFachteamProzessbaukasten();
        final Date entry = timestamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungFachteamProzessbaukastenExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getFachteamProzessbaukasten();
        final Date exit = timestamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungTteamProzessbaukastenEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getTteamProzessbaukasten();
        final Date entry = timestamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungTteamProzessbaukastenExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getTteamProzessbaukasten();
        final Date exit = timestamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungVereinbarungProzessbaukastenEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getVereinbarungProzessbaukasten();
        final Date entry = timestamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungVereinbarungProzessbaukastenExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getVereinbarungProzessbaukasten();
        final Date exit = timestamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void restoreAnforderungFreigegebenProzessbaukastenEntry() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getFreigegebenProzessbaukasten();
        final Date entry = timestamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void restoreAnforderungFreigegegebenProzessbaukastenExit() {
        final ReportingStatusTransition statusTransition = restoreAnforderungService.restoreAnforderung(anforderung, newDate);
        final EntryExitTimeStamp timestamp = statusTransition.getFreigegebenProzessbaukasten();
        final Date exit = timestamp.getExit();
        assertThat(exit, is(nullValue()));
    }
}
