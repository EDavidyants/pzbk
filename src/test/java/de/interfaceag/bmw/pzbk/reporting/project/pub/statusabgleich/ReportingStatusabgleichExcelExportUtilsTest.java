package de.interfaceag.bmw.pzbk.reporting.project.pub.statusabgleich;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusName;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.ReportingStatusabgleichExcelExportUtils;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.ReportingStatusabgleichFilter;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.ReportingStatusabgleichViewData;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.StatusabgleichKennzahlen;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author evda
 */
@RunWith(MockitoJUnitRunner.class)
public class ReportingStatusabgleichExcelExportUtilsTest {

    @Mock
    private UrlParameter urlParameter;

    private Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> matrixValues = new HashMap<>();
    private ReportingStatusabgleichFilter filter;
    private StatusabgleichKennzahlen kennzahlen;
    private DerivatStatusNames derivatStatusNames;
    private ReportingStatusabgleichViewData data;
    private Map<String, String> amToolStatusNames = new HashMap<>();
    private Map<String, String> zakStatusNames = new HashMap<>();
    private Date datum;
    private Workbook workbook;
    private String sheetName;
    private String[] totalsByZakStatus = new String[8];
    private String[] totalsByAMToolStatus = new String[7];
    private String[][] statusAbgleichValues = new String[6][7];

    @Before
    public void setUp() {
        Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put("derivat", "1");
//        when(urlParameter.getParamters()).thenReturn(parameterMap);

        Derivat e21 = TestDataFactory.generateDerivat();
        e21.setName("E21");
        Collection<Derivat> derivate = Arrays.asList(e21);
        filter = new ReportingStatusabgleichFilter(urlParameter, derivate, Collections.EMPTY_LIST, Collections.EMPTY_LIST, new ArrayList<>(), new ArrayList<>());
        buildMatrixValuesMap();
        kennzahlen = new StatusabgleichKennzahlen(matrixValues);

        Collection<DerivatStatusName> statusNames = Arrays.asList(new DerivatStatusName(DerivatStatus.VEREINARBUNG_VKBG, "E21", "Vereinbarung ZV"));
        derivatStatusNames = new DerivatStatusNames(statusNames);
        data = ReportingStatusabgleichViewData.withKennzahlen(kennzahlen).withFilter(filter).withDerivat(e21).withDerivatStatusNames(derivatStatusNames).get();

        amToolStatusNames.put("reporting_statusmatrix_amtoolstatus", "AM-Tool-Status");
        amToolStatusNames.put("reporting_statusmatrix_angenommenUmgesetzt", "Angenommen/ umgesetzt");
        amToolStatusNames.put("reporting_statusmatrix_inklarung", "In Klärung");
        amToolStatusNames.put("reporting_statusmatrix_abzustimmen", "Abzustimmen");
        amToolStatusNames.put("reporting_statusmatrix_nichtBewertbar", "Nicht bewertbar");
        amToolStatusNames.put("reporting_statusmatrix_keineWeiterverfolgung", "Keine Weiterverfolgung");
        amToolStatusNames.put("reporting_statusmatrix_unzureichendeAnforderungsqualitaet", "Unzureichende Anforderungsqualität");

        zakStatusNames.put("reporting_statusmatrix_zakstatus", "ZAK-Status");
        zakStatusNames.put("reporting_statusmatrix_bestaetigt", "Bestätigt");
        zakStatusNames.put("reporting_statusmatrix_bestaetigtAusVorprozess", "Bestätigt aus Vorprozess / Standardvereinbarung");
        zakStatusNames.put("reporting_statusmatrix_offen", "Offen");
        zakStatusNames.put("reporting_statusmatrix_zuUebertragung", "Zu übertragen / Übertragung fehlerhaft");
        zakStatusNames.put("reporting_statusmatrix_nichtBewertbar", "Nicht bewertbar");
        zakStatusNames.put("reporting_statusmatrix_nichtUmsetzbar", "Nicht umsetzbar");
        zakStatusNames.put("reporting_statusmatrix_zuruckgezogen", "Zurückgezogen durch Anforderer");

        generateTotalsByZakStatus();
        generateTotalsByAMToolStatus();
        generateStatusAbgleichValues();

        Calendar calendar = Calendar.getInstance();
        calendar.set(2019, 0, 11);
        datum = calendar.getTime();

        sheetName = "Reporting Statusabgleich E21";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String standName = "Stand: " + dateFormat.format(datum);
        workbook = ReportingStatusabgleichExcelExportUtils.generateExcelFileForStatusabgleich(data, sheetName, standName, amToolStatusNames, zakStatusNames);

    }

    @Test
    public void testGenerateFileName() {
        String derivatName = "E21";
        String expectedFileName = "Reporting Statusabgleich E21";
        String actualFileName = ReportingStatusabgleichExcelExportUtils.generateFileName(derivatName);

        Assertions.assertEquals(expectedFileName, actualFileName);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichWorkbookNotNull() {
        Assertions.assertNotNull(workbook);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichSheetNotNull() {
        Sheet sheet = workbook.getSheet(sheetName);
        Assertions.assertNotNull(sheet);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichStandLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Cell a1 = sheet.getRow(0).getCell(0);
        String cellValue = a1.getStringCellValue();
        Assertions.assertEquals("Stand: 11.01.2019", cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZAKStatusLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Cell b1 = sheet.getRow(0).getCell(2);
        String cellValue = b1.getStringCellValue();
        Assertions.assertEquals("ZAK-Status", cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusBestaetigtLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(2);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(zakStatusNames.get("reporting_statusmatrix_bestaetigt"), cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusBestaetigtAusVorprozessLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(3);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(zakStatusNames.get("reporting_statusmatrix_bestaetigtAusVorprozess"), cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusOffenLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(4);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(zakStatusNames.get("reporting_statusmatrix_offen"), cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusZuUebertragenLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(5);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(zakStatusNames.get("reporting_statusmatrix_zuUebertragung"), cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusNichtBewertbarLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(6);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(zakStatusNames.get("reporting_statusmatrix_nichtBewertbar"), cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusNichtUmsetzbarLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(7);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(zakStatusNames.get("reporting_statusmatrix_nichtUmsetzbar"), cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusZurueckgezogenLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(8);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(zakStatusNames.get("reporting_statusmatrix_zuruckgezogen"), cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusBestaetigtTotal() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(2);
        Cell cell = row.getCell(2);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(totalsByZakStatus[1], cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusBestaetigtAusVorprozessTotal() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(2);
        Cell cell = row.getCell(3);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(totalsByZakStatus[2], cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusOffenTotal() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(2);
        Cell cell = row.getCell(4);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(totalsByZakStatus[3], cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusZuUebertragenTotal() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(2);
        Cell cell = row.getCell(5);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(totalsByZakStatus[4], cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusNichtBewertbarTotal() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(2);
        Cell cell = row.getCell(6);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(totalsByZakStatus[5], cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusNichtUmsetzbarTotal() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(2);
        Cell cell = row.getCell(7);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(totalsByZakStatus[6], cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichZakStatusZurueckgezogenTotal() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(2);
        Cell cell = row.getCell(8);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(totalsByZakStatus[7], cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichAmToolStatusAngenommenLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(3);
        Cell cell = row.getCell(0);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(amToolStatusNames.get("reporting_statusmatrix_angenommenUmgesetzt"), cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichAmToolStatusInKlaerungLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(4);
        Cell cell = row.getCell(0);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(amToolStatusNames.get("reporting_statusmatrix_inklarung"), cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichAmToolStatusAbzustimmenLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(5);
        Cell cell = row.getCell(0);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(amToolStatusNames.get("reporting_statusmatrix_abzustimmen"), cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichAmToolStatusNichtBewertbarLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(6);
        Cell cell = row.getCell(0);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(amToolStatusNames.get("reporting_statusmatrix_nichtBewertbar"), cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichAmToolStatusKeineWeiterverfolgungLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(7);
        Cell cell = row.getCell(0);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(amToolStatusNames.get("reporting_statusmatrix_keineWeiterverfolgung"), cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichAmToolStatusUnzureichendeAQualitaetLabel() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(8);
        Cell cell = row.getCell(0);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(amToolStatusNames.get("reporting_statusmatrix_unzureichendeAnforderungsqualitaet"), cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichAmToolStatusAngenommenTotal() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(3);
        Cell cell = row.getCell(1);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(totalsByAMToolStatus[1], cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichAmToolStatusInKlaerungTotal() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(4);
        Cell cell = row.getCell(1);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(totalsByAMToolStatus[2], cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichAmToolStatusAbzustimmenTotal() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(5);
        Cell cell = row.getCell(1);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(totalsByAMToolStatus[3], cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichAmToolStatusNichtBewertbarTotal() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(6);
        Cell cell = row.getCell(1);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(totalsByAMToolStatus[4], cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichAmToolStatusKeineWeiterverfolgungTotal() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(7);
        Cell cell = row.getCell(1);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(totalsByAMToolStatus[5], cellValue);
    }

    @Test
    public void testGenerateExcelFileForStatusabgleichAmToolStatusUnzureichendeAQualitaetTotal() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(8);
        Cell cell = row.getCell(1);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(totalsByAMToolStatus[6], cellValue);
    }

    @Test
    public void testValueAngenommenBestaetigt() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(3);
        Cell cell = row.getCell(2);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[0][0], cellValue);
    }

    @Test
    public void testValueAngenommenBestaetigtAusVorprozess() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(3);
        Cell cell = row.getCell(3);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[0][1], cellValue);
    }

    @Test
    public void testValueAngenommenOffen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(3);
        Cell cell = row.getCell(4);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[0][2], cellValue);
    }

    @Test
    public void testValueAngenommenZuUebertragen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(3);
        Cell cell = row.getCell(5);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[0][3], cellValue);
    }

    @Test
    public void testValueAngenommenNichtBewertbar() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(3);
        Cell cell = row.getCell(6);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[0][4], cellValue);
    }

    @Test
    public void testValueAngenommenNichtUmsetzbar() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(3);
        Cell cell = row.getCell(7);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[0][5], cellValue);
    }

    @Test
    public void testValueAngenommenZurueckgezogen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(3);
        Cell cell = row.getCell(8);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[0][6], cellValue);
    }

    @Test
    public void testValueInKlaerungBestaetigt() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(4);
        Cell cell = row.getCell(2);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[1][0], cellValue);
    }

    @Test
    public void testValueInKlaerungBestaetigtAusVorprozess() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(4);
        Cell cell = row.getCell(3);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[1][1], cellValue);
    }

    @Test
    public void testValueInKlaerungOffen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(4);
        Cell cell = row.getCell(4);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[1][2], cellValue);
    }

    @Test
    public void testValueInKlaerungZuUebertragen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(4);
        Cell cell = row.getCell(5);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[1][3], cellValue);
    }

    @Test
    public void testValueInKlaerungNichtBewertbar() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(4);
        Cell cell = row.getCell(6);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[1][4], cellValue);
    }

    @Test
    public void testValueInKlaerungNichtUmsetzbar() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(4);
        Cell cell = row.getCell(7);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[1][5], cellValue);
    }

    @Test
    public void testValueInKlaerungZurueckgezogen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(4);
        Cell cell = row.getCell(8);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[1][6], cellValue);
    }

    @Test
    public void testValueAbzustimmenBestaetigt() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(5);
        Cell cell = row.getCell(2);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[2][0], cellValue);
    }

    @Test
    public void testValueAbzustimmenBestaetigtAusVorprozess() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(5);
        Cell cell = row.getCell(3);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[2][1], cellValue);
    }

    @Test
    public void testValueAbzustimmenOffen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(5);
        Cell cell = row.getCell(4);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[2][2], cellValue);
    }

    @Test
    public void testValueAbzustimmenZuUebertragen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(5);
        Cell cell = row.getCell(5);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[2][3], cellValue);
    }

    @Test
    public void testValueAbzustimmenNichtBewertbar() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(5);
        Cell cell = row.getCell(6);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[2][4], cellValue);
    }

    @Test
    public void testValueAbzustimmenNichtUmsetzbar() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(5);
        Cell cell = row.getCell(7);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[2][5], cellValue);
    }

    @Test
    public void testValueAbzustimmenZurueckgezogen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(5);
        Cell cell = row.getCell(8);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[2][6], cellValue);
    }

    @Test
    public void testValueNichtBewertbarBestaetigt() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(6);
        Cell cell = row.getCell(2);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[3][0], cellValue);
    }

    @Test
    public void testValueNichtBewertbarBestaetigtAusVorprozess() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(6);
        Cell cell = row.getCell(3);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[3][1], cellValue);
    }

    @Test
    public void testValueNichtBewertbarOffen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(6);
        Cell cell = row.getCell(4);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[3][2], cellValue);
    }

    @Test
    public void testValueNichtBewertbarZuUebertragen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(6);
        Cell cell = row.getCell(5);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[3][3], cellValue);
    }

    @Test
    public void testValueNichtBewertbarNichtBewertbar() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(6);
        Cell cell = row.getCell(6);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[3][4], cellValue);
    }

    @Test
    public void testValueNichtBewertbarNichtUmsetzbar() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(6);
        Cell cell = row.getCell(7);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[3][5], cellValue);
    }

    @Test
    public void testValueNichtBewertbarZurueckgezogen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(6);
        Cell cell = row.getCell(8);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[3][6], cellValue);
    }

    @Test
    public void testValueKeineWeiterverfolgungBestaetigt() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(7);
        Cell cell = row.getCell(2);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[4][0], cellValue);
    }

    @Test
    public void testValueKeineWeiterverfolgungBestaetigtAusVorprozess() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(7);
        Cell cell = row.getCell(3);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[4][1], cellValue);
    }

    @Test
    public void testValueKeineWeiterverfolgungOffen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(7);
        Cell cell = row.getCell(4);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[4][2], cellValue);
    }

    @Test
    public void testValueKeineWeiterverfolgungZuUebertragen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(7);
        Cell cell = row.getCell(5);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[4][3], cellValue);
    }

    @Test
    public void testValueKeineWeiterverfolgungNichtBewertbar() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(7);
        Cell cell = row.getCell(6);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[4][4], cellValue);
    }

    @Test
    public void testValueKeineWeiterverfolgungNichtUmsetzbar() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(7);
        Cell cell = row.getCell(7);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[4][5], cellValue);
    }

    @Test
    public void testValueKeineWeiterverfolgungZurueckgezogen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(7);
        Cell cell = row.getCell(8);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[4][6], cellValue);
    }

    @Test
    public void testValueUnzureichendeAQualitaetBestaetigt() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(8);
        Cell cell = row.getCell(2);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[5][0], cellValue);
    }

    @Test
    public void testValueUnzureichendeAQualitaetBestaetigtAusVorprozess() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(8);
        Cell cell = row.getCell(3);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[5][1], cellValue);
    }

    @Test
    public void testValueUnzureichendeAQualitaetOffen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(8);
        Cell cell = row.getCell(4);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[5][2], cellValue);
    }

    @Test
    public void testValueUnzureichendeAQualitaetZuUebertragen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(8);
        Cell cell = row.getCell(5);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[5][3], cellValue);
    }

    @Test
    public void testValueUnzureichendeAQualitaetNichtBewertbar() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(8);
        Cell cell = row.getCell(6);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[5][4], cellValue);
    }

    @Test
    public void testValueUnzureichendeAQualitaetNichtUmsetzbar() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(8);
        Cell cell = row.getCell(7);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[5][5], cellValue);
    }

    @Test
    public void testValueUnzureichendeAQualitaetZurueckgezogen() {
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(8);
        Cell cell = row.getCell(8);
        String cellValue = cell.getStringCellValue();
        Assertions.assertEquals(statusAbgleichValues[5][6], cellValue);
    }

    private void buildMatrixValuesMap() {
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> angenommenBestaetigt = new GenericTuple<>(DerivatAnforderungModulStatus.ANGENOMMEN, ZakStatus.DONE);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> angenommenBestaetigtAusVorprozess = new GenericTuple<>(DerivatAnforderungModulStatus.ANGENOMMEN, ZakStatus.BESTAETIGT_AUS_VORPROZESS);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> angenommenOffen = new GenericTuple<>(DerivatAnforderungModulStatus.ANGENOMMEN, ZakStatus.PENDING);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> angenommenZuUebertragen = new GenericTuple<>(DerivatAnforderungModulStatus.ANGENOMMEN, ZakStatus.UEBERTRAGUNG_FEHLERHAFT);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> angenommenNichtBewertbar = new GenericTuple<>(DerivatAnforderungModulStatus.ANGENOMMEN, ZakStatus.NICHT_BEWERTBAR);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> angenommenNichtUmsetzbar = new GenericTuple<>(DerivatAnforderungModulStatus.ANGENOMMEN, ZakStatus.NICHT_UMSETZBAR);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> angenommenZurueckgezogen = new GenericTuple<>(DerivatAnforderungModulStatus.ANGENOMMEN, ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER);

        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> inKlaerungBestaetigt = new GenericTuple<>(DerivatAnforderungModulStatus.IN_KLAERUNG, ZakStatus.DONE);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> inKlaerungBestaetigtAusVorprozess = new GenericTuple<>(DerivatAnforderungModulStatus.IN_KLAERUNG, ZakStatus.BESTAETIGT_AUS_VORPROZESS);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> inKlaerungOffen = new GenericTuple<>(DerivatAnforderungModulStatus.IN_KLAERUNG, ZakStatus.PENDING);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> inKlaerungZuUebertragen = new GenericTuple<>(DerivatAnforderungModulStatus.IN_KLAERUNG, ZakStatus.UEBERTRAGUNG_FEHLERHAFT);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> inKlaerungNichtBewertbar = new GenericTuple<>(DerivatAnforderungModulStatus.IN_KLAERUNG, ZakStatus.NICHT_BEWERTBAR);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> inKlaerungNichtUmsetzbar = new GenericTuple<>(DerivatAnforderungModulStatus.IN_KLAERUNG, ZakStatus.NICHT_UMSETZBAR);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> inKlaerungZurueckgezogen = new GenericTuple<>(DerivatAnforderungModulStatus.IN_KLAERUNG, ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER);

        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> abzustimmenBestaetigt = new GenericTuple<>(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.DONE);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> abzustimmenBestaetigtAusVorprozess = new GenericTuple<>(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.BESTAETIGT_AUS_VORPROZESS);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> abzustimmenOffen = new GenericTuple<>(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.PENDING);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> abzustimmenZuUebertragen = new GenericTuple<>(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.UEBERTRAGUNG_FEHLERHAFT);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> abzustimmenNichtBewertbar = new GenericTuple<>(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.NICHT_BEWERTBAR);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> abzustimmenNichtUmsetzbar = new GenericTuple<>(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.NICHT_UMSETZBAR);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> abzustimmenZurueckgezogen = new GenericTuple<>(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER);

        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> nichtBewertbarBestaetigt = new GenericTuple<>(DerivatAnforderungModulStatus.NICHT_BEWERTBAR, ZakStatus.DONE);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> nichtBewertbarBestaetigtAusVorprozess = new GenericTuple<>(DerivatAnforderungModulStatus.NICHT_BEWERTBAR, ZakStatus.BESTAETIGT_AUS_VORPROZESS);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> nichtBewertbarOffen = new GenericTuple<>(DerivatAnforderungModulStatus.NICHT_BEWERTBAR, ZakStatus.PENDING);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> nichtBewertbarZuUebertragen = new GenericTuple<>(DerivatAnforderungModulStatus.NICHT_BEWERTBAR, ZakStatus.UEBERTRAGUNG_FEHLERHAFT);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> nichtBewertbarNichtBewertbar = new GenericTuple<>(DerivatAnforderungModulStatus.NICHT_BEWERTBAR, ZakStatus.NICHT_BEWERTBAR);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> nichtBewertbarNichtUmsetzbar = new GenericTuple<>(DerivatAnforderungModulStatus.NICHT_BEWERTBAR, ZakStatus.NICHT_UMSETZBAR);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> nichtBewertbarZurueckgezogen = new GenericTuple<>(DerivatAnforderungModulStatus.NICHT_BEWERTBAR, ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER);

        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> keineWeiterverfolgungBestaetigt = new GenericTuple<>(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG, ZakStatus.DONE);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> keineWeiterverfolgungBestaetigtAusVorprozess = new GenericTuple<>(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG, ZakStatus.BESTAETIGT_AUS_VORPROZESS);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> keineWeiterverfolgungOffen = new GenericTuple<>(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG, ZakStatus.PENDING);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> keineWeiterverfolgungZuUebertragen = new GenericTuple<>(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG, ZakStatus.UEBERTRAGUNG_FEHLERHAFT);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> keineWeiterverfolgungNichtBewertbar = new GenericTuple<>(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG, ZakStatus.NICHT_BEWERTBAR);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> keineWeiterverfolgungNichtUmsetzbar = new GenericTuple<>(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG, ZakStatus.NICHT_UMSETZBAR);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> keineWeiterverfolgungZurueckgezogen = new GenericTuple<>(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG, ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER);

        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> unzureichendeAQualitaetBestaetigt = new GenericTuple<>(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET, ZakStatus.DONE);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> unzureichendeAQualitaetBestaetigtAusVorprozess = new GenericTuple<>(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET, ZakStatus.BESTAETIGT_AUS_VORPROZESS);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> unzureichendeAQualitaetOffen = new GenericTuple<>(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET, ZakStatus.PENDING);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> unzureichendeAQualitaetZuUebertragen = new GenericTuple<>(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET, ZakStatus.UEBERTRAGUNG_FEHLERHAFT);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> unzureichendeAQualitaetNichtBewertbar = new GenericTuple<>(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET, ZakStatus.NICHT_BEWERTBAR);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> unzureichendeAQualitaetNichtUmsetzbar = new GenericTuple<>(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET, ZakStatus.NICHT_UMSETZBAR);
        GenericTuple<DerivatAnforderungModulStatus, ZakStatus> unzureichendeAQualitaetZurueckgezogen = new GenericTuple<>(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET, ZakStatus.ZURUECKGEZOGEN_DURCH_ANFORDERER);

        matrixValues.put(angenommenBestaetigt, 1L);
        matrixValues.put(angenommenBestaetigtAusVorprozess, 2L);
        matrixValues.put(angenommenOffen, 3L);
        matrixValues.put(angenommenZuUebertragen, 4L);
        matrixValues.put(angenommenNichtBewertbar, 5L);
        matrixValues.put(angenommenNichtUmsetzbar, 6L);
        matrixValues.put(angenommenZurueckgezogen, 7L);
        matrixValues.put(inKlaerungBestaetigt, 8L);
        matrixValues.put(inKlaerungBestaetigtAusVorprozess, 9L);
        matrixValues.put(inKlaerungOffen, 10L);
        matrixValues.put(inKlaerungZuUebertragen, 11L);
        matrixValues.put(inKlaerungNichtBewertbar, 12L);
        matrixValues.put(inKlaerungNichtUmsetzbar, 13L);
        matrixValues.put(inKlaerungZurueckgezogen, 14L);
        matrixValues.put(abzustimmenBestaetigt, 15L);
        matrixValues.put(abzustimmenBestaetigtAusVorprozess, 16L);
        matrixValues.put(abzustimmenOffen, 17L);
        matrixValues.put(abzustimmenZuUebertragen, 18L);
        matrixValues.put(abzustimmenNichtBewertbar, 19L);
        matrixValues.put(abzustimmenNichtUmsetzbar, 20L);
        matrixValues.put(abzustimmenZurueckgezogen, 21L);
        matrixValues.put(nichtBewertbarBestaetigt, 22L);
        matrixValues.put(nichtBewertbarBestaetigtAusVorprozess, 23L);
        matrixValues.put(nichtBewertbarOffen, 24L);
        matrixValues.put(nichtBewertbarZuUebertragen, 25L);
        matrixValues.put(nichtBewertbarNichtBewertbar, 26L);
        matrixValues.put(nichtBewertbarNichtUmsetzbar, 27L);
        matrixValues.put(nichtBewertbarZurueckgezogen, 28L);
        matrixValues.put(keineWeiterverfolgungBestaetigt, 29L);
        matrixValues.put(keineWeiterverfolgungBestaetigtAusVorprozess, 30L);
        matrixValues.put(keineWeiterverfolgungOffen, 31L);
        matrixValues.put(keineWeiterverfolgungZuUebertragen, 32L);
        matrixValues.put(keineWeiterverfolgungNichtBewertbar, 33L);
        matrixValues.put(keineWeiterverfolgungNichtUmsetzbar, 34L);
        matrixValues.put(keineWeiterverfolgungZurueckgezogen, 35L);
        matrixValues.put(unzureichendeAQualitaetBestaetigt, 36L);
        matrixValues.put(unzureichendeAQualitaetBestaetigtAusVorprozess, 37L);
        matrixValues.put(unzureichendeAQualitaetOffen, 38L);
        matrixValues.put(unzureichendeAQualitaetZuUebertragen, 39L);
        matrixValues.put(unzureichendeAQualitaetNichtBewertbar, 40L);
        matrixValues.put(unzureichendeAQualitaetNichtUmsetzbar, 41L);
        matrixValues.put(unzureichendeAQualitaetZurueckgezogen, 42L);
    }

    private void generateTotalsByZakStatus() {
        String cellValue;
        Long arithValue;

        totalsByZakStatus[0] = Character.toString('\u01A9');

        for (int i = 1; i <= 7; i++) {
            arithValue = (6 * i * 1L + 105);
            cellValue = arithValue.toString();
            totalsByZakStatus[i] = cellValue;
        }
    }

    private void generateTotalsByAMToolStatus() {
        String cellValue;
        Long arithValue;

        totalsByAMToolStatus[0] = Character.toString('\u01A9');

        for (int i = 1; i <= 6; i++) {
            arithValue = (1 + 7 * (i - 1)) * 7L + 21L;
            cellValue = arithValue.toString();
            totalsByAMToolStatus[i] = cellValue;
        }
    }

    private void generateStatusAbgleichValues() {
        String cellValue;
        Long arithValue;

        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j <= 7; j++) {
                arithValue = ((i - 1) * 7 + j) * 1L;
                cellValue = arithValue.toString();
                statusAbgleichValues[i - 1][j - 1] = cellValue;
            }
        }
    }

}
