package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


class EntryExitTimeStampTest {

    public static final long DAYS = 172800000L;

    EntryExitTimeStamp timeStamp;

    @BeforeEach
    void setUp() {
        timeStamp = new EntryExitTimeStamp();
    }

    @Test
    void toStringTest() {
        final String result = timeStamp.toString();
        assertThat(result, is("EntryExitTimeStamp{entry=null, exit=null}"));
    }

    @Test
    void entry() {
        timeStamp.entry();
        final Date entry = timeStamp.getEntry();
        assertThat(entry, is(notNullValue()));
    }

    @Test
    void exit() {
        timeStamp.exit();
        final Date exit = timeStamp.getExit();
        assertThat(exit, is(notNullValue()));
    }

    @Test
    void entryDefaultIsNull() {
        final Date entry = timeStamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void entryGetterSetter() {
        Date date = new Date();
        timeStamp.setEntry(date);
        final Date entry = timeStamp.getEntry();
        assertThat(entry, is(date));
    }

    @Test
    void removeEntry() {
        Date date = new Date();
        timeStamp.setEntry(date);
        timeStamp.removeEntry();
        final Date entry = timeStamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void exitDefaultIsNull() {
        final Date exit = timeStamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void exitGetterSetter() {
        Date date = new Date();
        timeStamp.setExit(date);
        final Date exit = timeStamp.getExit();
        assertThat(exit, is(date));
    }

    @Test
    void removeExit() {
        Date date = new Date();
        timeStamp.setExit(date);
        timeStamp.removeExit();
        final Date exit = timeStamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void clearEntry() {
        Date date = new Date();
        timeStamp.setEntry(date);
        timeStamp.clear();
        final Date entry = timeStamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void clearExit() {
        Date date = new Date();
        timeStamp.setExit(date);
        timeStamp.clear();
        final Date exit = timeStamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void isActiveDefault() {
        final boolean active = timeStamp.isActive();
        assertTrue(active);
    }

    @Test
    void isActiveEntryNotNull() {
        timeStamp.setEntry(new Date());
        final boolean active = timeStamp.isActive();
        assertTrue(active);
    }

    @Test
    void isActiveExitNotNull() {
        timeStamp.setExit(new Date());
        final boolean active = timeStamp.isActive();
        assertFalse(active);
    }

    @Test
    void getEntryExitDifferenceBothNull() {
        final Long difference = timeStamp.getEntryExitDifference();
        assertThat(difference, is(0L));
    }

    @Test
    void getEntryExitDifference() {

        Date now = new Date();

        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(now.getTime() - DAYS);

        timeStamp.setEntry(calendar.getTime());

        final Long difference = timeStamp.getEntryExitDifference();
        final Long anotherDiff = difference - DAYS;
        final double value = anotherDiff.doubleValue();
        assertThat(value, is(closeTo(0, 100.0)));
    }
}