package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapWithSize.aMapWithSize;
import static org.hamcrest.collection.IsMapWithSize.anEmptyMap;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class KeyFigureQueryFestgestelltInFilterTest {

    @Mock
    private ReportingFilter reportingFilter;
    @Mock
    private IdSearchFilter filter;
    @Mock
    private Set<Long> ids;
    private QueryPart queryPart;

    @BeforeEach
    void setUp() {
        queryPart = new QueryPartDTO();

        when(reportingFilter.getDerivatFilter()).thenReturn(filter);
    }

    private void setFilterIds() {
        when(filter.getSelectedIdsAsSet()).thenReturn(ids);
    }

    @Test
    void appendQueryInactive() {
        setFilterInactive();

        KeyFigureQueryFestgestelltInFilter.append(reportingFilter, queryPart);
        assertThat(queryPart.getQuery(), is(""));
    }

    @Test
    void appendQueryActive() {
        setFilterActive();
        setFilterIds();

        KeyFigureQueryFestgestelltInFilter.append(reportingFilter, queryPart);
        assertThat(queryPart.getQuery(), is(" AND f.id IN :festgestelltIn "));
    }

    @Test
    void appendParameterMapInactive() {
        setFilterInactive();

        KeyFigureQueryFestgestelltInFilter.append(reportingFilter, queryPart);
        assertThat(queryPart.getParameterMap(), anEmptyMap());
    }

    @Test
    void appendParameterMapActive() {
        setFilterActive();
        setFilterIds();

        KeyFigureQueryFestgestelltInFilter.append(reportingFilter, queryPart);
        assertThat(queryPart.getParameterMap(), aMapWithSize(1));
    }

    private void setFilterActive() {
        when(filter.isActive()).thenReturn(true);
    }

    private void setFilterInactive() {
        when(filter.isActive()).thenReturn(false);
    }
}