package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungHistoryDto;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.enums.Status;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingMeldungStatusTransitionEntyDateServiceTest {

    @Mock
    private AnforderungMeldungHistoryService meldungHistoryService;
    @InjectMocks
    private ReportingMeldungStatusTransitionEntryDateService reportingMeldungStatusTransitionEntyDateService;

    @Mock
    private Meldung meldung;

    private AnforderungHistoryDto historyEntry1;
    private AnforderungHistoryDto historyEntry2;

    private List<AnforderungHistoryDto> meldungHistory;

    private Date time1;
    private Date time2;

    @BeforeEach
    void setUp() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2018,03,05);
        time1 = calendar.getTime();
        calendar.set(2018,02,05);
        time2 = calendar.getTime();
        historyEntry1 = AnforderungHistoryDto.newBuilder().withZeitpunkt(time1).withAuf("gemeldet").build();
        historyEntry2 = AnforderungHistoryDto.newBuilder().withZeitpunkt(time2).withAuf("gemeldet").build();
        meldungHistory = new ArrayList<>();
        meldungHistory.add(historyEntry1);
        meldungHistory.add(historyEntry2);
    }

    @Test
    void getEntryDateForMeldungIsLatestValue() {
        when(meldung.getStatus()).thenReturn(Status.M_GEMELDET);
        when(meldungHistoryService.getHistoryForAnforderung(any(),any())).thenReturn(meldungHistory);
        final Optional<Date> entryDate = reportingMeldungStatusTransitionEntyDateService.getEntryDateForMeldung(meldung);
        assertThat(entryDate.get(), is(time1));
    }

    @Test
    void getEntryDateForMeldungIsPresentForHistoryEntry() {
        when(meldung.getStatus()).thenReturn(Status.M_GEMELDET);
        when(meldungHistoryService.getHistoryForAnforderung(any(),any())).thenReturn(meldungHistory);
        final Optional<Date> entryDate = reportingMeldungStatusTransitionEntyDateService.getEntryDateForMeldung(meldung);
        assertTrue(entryDate.isPresent());
    }

    @Test
    void getEntryDateForMeldungIsNotPresentForNoHistoryEntry() {
        when(meldung.getStatus()).thenReturn(Status.M_GEMELDET);
        when(meldungHistoryService.getHistoryForAnforderung(any(),any())).thenReturn(Collections.emptyList());
        final Optional<Date> entryDate = reportingMeldungStatusTransitionEntyDateService.getEntryDateForMeldung(meldung);
        assertFalse(entryDate.isPresent());
    }

    @Test
    void getEntryDateForMeldungIsNotPresentForStatusGeloescht() {
        when(meldung.getStatus()).thenReturn(Status.M_GELOESCHT);
        final Optional<Date> entryDate = reportingMeldungStatusTransitionEntyDateService.getEntryDateForMeldung(meldung);
        assertFalse(entryDate.isPresent());
    }

    @Test
    void getEntryDateForMeldungIsNotPresentForStatusUnstimmig() {
        when(meldung.getStatus()).thenReturn(Status.M_UNSTIMMIG);
        final Optional<Date> entryDate = reportingMeldungStatusTransitionEntyDateService.getEntryDateForMeldung(meldung);
        assertFalse(entryDate.isPresent());
    }

    @Test
    void getEntryDateForMeldungIsNotPresentForStatusEntwurf() {
        when(meldung.getStatus()).thenReturn(Status.M_ENTWURF);
        final Optional<Date> entryDate = reportingMeldungStatusTransitionEntyDateService.getEntryDateForMeldung(meldung);
        assertFalse(entryDate.isPresent());
    }

    @Test
    void getEntryDateForMeldungIsNotPresentForStatusAngelegt() {
        when(meldung.getStatus()).thenReturn(Status.M_ANGELEGT);
        final Optional<Date> entryDate = reportingMeldungStatusTransitionEntyDateService.getEntryDateForMeldung(meldung);
        assertFalse(entryDate.isPresent());
    }
}