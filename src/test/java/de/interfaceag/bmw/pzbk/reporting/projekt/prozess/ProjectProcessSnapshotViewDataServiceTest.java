package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot.ProjectProcessSnapshot;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot.ProjectProcessSnapshotEntry;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot.ProjectProcessSnapshotReadService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProjectProcessSnapshotViewDataServiceTest {

    private static final ProjectReportingProcessKeyFigure KEY_FIGURE = ProjectReportingProcessKeyFigure.L4;
    private static final int VALUE = 1;
    @Mock
    private ProjectProcessSnapshotReadService projectProcessSnapshotReadService;
    @InjectMocks
    private ProjectProcessSnapshotViewDataService projectProcessSnapshotViewDataService;

    @Mock
    private UrlParameter urlParameter;
    @Mock
    private ProjectProcessSnapshot snapshot;
    @Mock
    private ProjectProcessSnapshotEntry entry;

    private Collection<ProjectProcessSnapshotEntry> entries;

    @Test
    void testGetEmptyKeyFiguresWhenSnapshotIdIsNotPresent() {
        when(urlParameter.getValue(any())).thenReturn(Optional.empty());

        Optional<ProjectReportingProcessKeyFigureDataCollection> result = projectProcessSnapshotViewDataService.getKeyFiguresForSnapshot(urlParameter);
        MatcherAssert.assertThat(result, CoreMatchers.is(Optional.empty()));
    }

    @ParameterizedTest
    @ValueSource(strings = {"a", "AA", "A1", "!", "", " ", "1 ", " 1"})
    void testGetEmptyKeyFiguresWhenSnapshotIdDoesNotMatchFormal(String parameter) {
        when(urlParameter.getValue(any())).thenReturn(Optional.of(parameter));

        Optional<ProjectReportingProcessKeyFigureDataCollection> result = projectProcessSnapshotViewDataService.getKeyFiguresForSnapshot(urlParameter);
        MatcherAssert.assertThat(result, CoreMatchers.is(Optional.empty()));
    }

    @ParameterizedTest
    @ValueSource(strings = {"1", "11", "123"})
    void testSearchForSnapshotByIdWhenSnapshotIdIsPresent() {
        setupIdIsPresent();

        projectProcessSnapshotViewDataService.getKeyFiguresForSnapshot(urlParameter);

        verify(projectProcessSnapshotReadService, times(1)).getById(any());
    }

    @Test
    void testResultIsPresentWhenSnapshotIdIsPresent() {
        setupIdIsPresent();
        setupEntries();

        Optional<ProjectReportingProcessKeyFigureDataCollection> result = projectProcessSnapshotViewDataService.getKeyFiguresForSnapshot(urlParameter);

        MatcherAssert.assertThat(result.isPresent(), CoreMatchers.is(Boolean.TRUE));
    }

    @Test
    void testDataForKeyFigureIsPresentWhenSnapshotIdIsPresent() {
        setupIdIsPresent();
        setupEntries();

        Optional<ProjectReportingProcessKeyFigureDataCollection> result = projectProcessSnapshotViewDataService.getKeyFiguresForSnapshot(urlParameter);

        if(result.isPresent()) {
            final ProjectReportingProcessKeyFigureDataCollection keyFigureDataCollection = result.get();
            final Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = keyFigureDataCollection.getDataForKeyFigure(KEY_FIGURE);
            MatcherAssert.assertThat(dataForKeyFigure.isPresent(), CoreMatchers.is(Boolean.TRUE));
        } else {
            Assertions.fail("result is not present");
        }
    }

    @Test
    void testValueForKeyFigureWhenSnapshotIdIsPresent() {
        setupIdIsPresent();
        setupEntries();

        Optional<ProjectReportingProcessKeyFigureDataCollection> result = projectProcessSnapshotViewDataService.getKeyFiguresForSnapshot(urlParameter);

        if(result.isPresent()) {
            final ProjectReportingProcessKeyFigureDataCollection keyFigureDataCollection = result.get();
            final Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = keyFigureDataCollection.getDataForKeyFigure(KEY_FIGURE);
            if (dataForKeyFigure.isPresent()) {
                final Long value = dataForKeyFigure.get().getValue();
                MatcherAssert.assertThat(value.intValue(), CoreMatchers.is(VALUE));
            } else {
                Assertions.fail("value is not present");
            }
        } else {
            Assertions.fail("result is not present");
        }
    }

    @Test
    void testKeyFigureForKeyFigureWhenSnapshotIdIsPresent() {
        setupIdIsPresent();
        setupEntries();

        Optional<ProjectReportingProcessKeyFigureDataCollection> result = projectProcessSnapshotViewDataService.getKeyFiguresForSnapshot(urlParameter);

        if(result.isPresent()) {
            final ProjectReportingProcessKeyFigureDataCollection keyFigureDataCollection = result.get();
            final Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = keyFigureDataCollection.getDataForKeyFigure(KEY_FIGURE);
            if (dataForKeyFigure.isPresent()) {
                final ProjectReportingProcessKeyFigure keyFigure = dataForKeyFigure.get().getKeyFigure();
                MatcherAssert.assertThat(keyFigure, CoreMatchers.is(KEY_FIGURE));
            } else {
                Assertions.fail("value is not present");
            }
        } else {
            Assertions.fail("result is not present");
        }
    }

    private void setupEntries() {
        entries = new ArrayList<>();
        entries.add(entry);
        when(entry.getKeyFigure()).thenReturn(KEY_FIGURE);
        when(entry.getValue()).thenReturn(VALUE);
        when(snapshot.getEntries()).thenReturn(entries);
    }

    private void setupIdIsPresent() {
        when(urlParameter.getValue(any())).thenReturn(Optional.of("1"));
        when(projectProcessSnapshotReadService.getById(any())).thenReturn(Optional.of(snapshot));
    }
}