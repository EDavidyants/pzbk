package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamCurrentObjectsFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamGeloeschteObjekteKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamInputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FreigegebenCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsIterableContaining.hasItems;

class FachteamSumKeyFigureCalculatorTest {

    private String LABEL = "Label";
    private Collection<FachteamKeyFigure> keyFigures;
    private Collection<Long> ids1;
    private Collection<Long> ids2;

    @BeforeEach
    void setUp() {
        ids1 = Arrays.asList(1L, 42L);
        ids2 = Arrays.asList(2L, 42L);
        keyFigures = new ArrayList<>();
    }

    @Test
    void computeSumKeyFiguresLabel() {
        final FachteamKeyFigure fachteamKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamKeyFigure.getSensorCocDisplayName(), is(LABEL));
    }

    @Test
    void computeSumKeyFiguresKeyFigureResultSize() {
        final FachteamKeyFigure fachteamKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamKeyFigure.getKeyFigures(), hasSize(5));
    }

    @Test
    void computeSumKeyFiguresDefaultDurchlaufzeit() {
        final FachteamKeyFigure fachteamKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamKeyFigure.getDurchlaufzeit(), is(0));
    }

    @Test
    void computeSumKeyFiguresCalculateDurchlaufzeit() {
        FachteamKeyFigure fachteamKeyFigure = buildFachteamDurchlaufzeitKeyFigure(5);
        keyFigures.add(fachteamKeyFigure);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getDurchlaufzeit(), is(5));
    }

    @Test
    void computeSumKeyFiguresCalculateDurchlaufzeitMultipleEntries() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamDurchlaufzeitKeyFigure(5);
        FachteamKeyFigure fachteamKeyFigure2 = buildFachteamDurchlaufzeitKeyFigure(9);
        keyFigures.add(fachteamKeyFigure1);
        keyFigures.add(fachteamKeyFigure2);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getDurchlaufzeit(), is(7));
    }

    @Test
    void computeSumKeyFiguresAnforderungIdsForFreigegebenCurrentKeyFigure() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFreigebenCurrentKeyFigure(ids1);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FREIGEGEBEN_CURRENT).getAnforderungIds(), hasItems(1L, 42L));
    }

    @Test
    void computeSumKeyFiguresFreigegebenCurrentKeyFigureValue() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFreigebenCurrentKeyFigure(ids1);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FREIGEGEBEN_CURRENT).getValue(), is(2));
    }

    @Test
    void computeSumKeyFiguresAnforderungIdsForFreigegebenCurrentKeyFigureMultipleInputsWithSameIds() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFreigebenCurrentKeyFigure(ids1);
        FachteamKeyFigure fachteamKeyFigure2 = buildFachteamKeyFigureWithFreigebenCurrentKeyFigure(ids1);
        keyFigures.add(fachteamKeyFigure1);
        keyFigures.add(fachteamKeyFigure2);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FREIGEGEBEN_CURRENT).getAnforderungIds(), hasItems(1L, 42L));
    }

    @Test
    void computeSumKeyFiguresFreigegebenCurrentKeyFigureMultipleInputsWithSameIdsValue() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFreigebenCurrentKeyFigure(ids1);
        FachteamKeyFigure fachteamKeyFigure2 = buildFachteamKeyFigureWithFreigebenCurrentKeyFigure(ids1);
        keyFigures.add(fachteamKeyFigure1);
        keyFigures.add(fachteamKeyFigure2);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FREIGEGEBEN_CURRENT).getValue(), is(4));
    }

    @Test
    void computeSumKeyFiguresAnforderungIdsForFreigegebenCurrentKeyFigureMultipleInputsWithDifferentIds() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFreigebenCurrentKeyFigure(ids1);
        FachteamKeyFigure fachteamKeyFigure2 = buildFachteamKeyFigureWithFreigebenCurrentKeyFigure(ids2);
        keyFigures.add(fachteamKeyFigure1);
        keyFigures.add(fachteamKeyFigure2);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FREIGEGEBEN_CURRENT).getAnforderungIds(), hasItems(1L, 2L, 42L));
    }

    @Test
    void computeSumKeyFiguresFreigegebenCurrentKeyFigureMultipleInputsWithDifferentIdsValue() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFreigebenCurrentKeyFigure(ids1);
        FachteamKeyFigure fachteamKeyFigure2 = buildFachteamKeyFigureWithFreigebenCurrentKeyFigure(ids2);
        keyFigures.add(fachteamKeyFigure1);
        keyFigures.add(fachteamKeyFigure2);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FREIGEGEBEN_CURRENT).getValue(), is(4));
    }

    @Test
    void computeSumKeyFiguresAnforderungIdsForFachteamGeloeschtKeyFigure() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_GELOESCHT).getAnforderungIds(), hasItems(1L, 42L));
    }

    @Test
    void computeSumKeyFiguresMeldungIdsForFachteamGeloeschtKeyFigure() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_GELOESCHT).getMeldungIds(), hasItems(2L, 42L));
    }

    @Test
    void computeSumKeyFiguresFachteamGeloeschtKeyFigureValue() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_GELOESCHT).getValue(), is(4));
    }

    @Test
    void computeSumKeyFiguresAnforderungIdsForFachteamOutputKeyFigure() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_OUTPUT).getAnforderungIds(), hasItems(1L, 42L));
    }

    @Test
    void computeSumKeyFiguresMeldungIdsForFachteamOutputKeyFigure() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_OUTPUT).getMeldungIds(), hasItems(2L, 42L));
    }

    @Test
    void computeSumKeyFiguresFachteamOutputKeyFigureValue() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_OUTPUT).getValue(), is(4));
    }

    @Test
    void computeSumKeyFiguresAnforderungIdsForFachteamCurrentKeyFigure() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_CURRENT).getAnforderungIds(), hasItems(1L, 42L));
    }

    @Test
    void computeSumKeyFiguresMeldungIdsForFachteamCurrentKeyFigure() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_CURRENT).getMeldungIds(), hasItems(2L, 42L));
    }

    @Test
    void computeSumKeyFiguresFachteamCurrentKeyFigureValue() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_CURRENT).getValue(), is(4));
    }

    @Test
    void computeSumKeyFiguresAnforderungIdsForFachteamInputKeyFigure() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_ENTRY_SUM).getAnforderungIds(), hasItems(1L, 42L));
    }

    @Test
    void computeSumKeyFiguresMeldungIdsForFachteamInputKeyFigure() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_ENTRY_SUM).getMeldungIds(), hasItems(2L, 42L));
    }

    @Test
    void computeSumKeyFiguresFachteamInputKeyFigureValue() {
        FachteamKeyFigure fachteamKeyFigure1 = buildFachteamKeyFigureWithFachteamKeyFigures(ids1, ids2);
        keyFigures.add(fachteamKeyFigure1);
        final FachteamKeyFigure fachteamSumKeyFigure = FachteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_ENTRY_SUM).getValue(), is(4));
    }


    private FachteamKeyFigure buildFachteamKeyFigureWithFachteamKeyFigures(Collection<Long> anforderungIds, Collection<Long> meldungIds) {
        DurchlaufzeitKeyFigure durchlaufZeitKeyFigure = getDurchlaufZeitKeyFigure(5);
        KeyFigureCollection keyFigureCollection = new KeyFigureCollection();
        keyFigureCollection.add(new FachteamGeloeschteObjekteKeyFigure(anforderungIds, meldungIds));
        keyFigureCollection.add(new FachteamOutputKeyFigure(anforderungIds, meldungIds, 0L));
        keyFigureCollection.add(new FachteamCurrentObjectsFigure(meldungIds, anforderungIds, 0L));
        keyFigureCollection.add(new FachteamInputKeyFigure(meldungIds, anforderungIds, 0L));
        return new FachteamKeyFigure(1L, LABEL, keyFigureCollection, durchlaufZeitKeyFigure);
    }

    private FachteamKeyFigure buildFachteamKeyFigureWithFreigebenCurrentKeyFigure(Collection<Long> anforderungIds) {
        DurchlaufzeitKeyFigure durchlaufZeitKeyFigure = getDurchlaufZeitKeyFigure(5);
        KeyFigureCollection keyFigureCollection = new KeyFigureCollection();
        keyFigureCollection.add(new FreigegebenCurrentKeyFigure(anforderungIds));
        return new FachteamKeyFigure(1L, LABEL, keyFigureCollection, durchlaufZeitKeyFigure);
    }

    private FachteamKeyFigure buildFachteamDurchlaufzeitKeyFigure(int durchlaufzeitValue) {
        DurchlaufzeitKeyFigure durchlaufZeitKeyFigure = getDurchlaufZeitKeyFigure(durchlaufzeitValue);
        KeyFigureCollection keyFigureCollection = new KeyFigureCollection();
        return new FachteamKeyFigure(1L, LABEL, keyFigureCollection, durchlaufZeitKeyFigure);
    }

    private static DurchlaufzeitKeyFigure getDurchlaufZeitKeyFigure(int value) {
        return new DurchlaufzeitKeyFigure() {
            @Override
            public KeyType getKeyType() {
                return KeyType.DURCHLAUFZEIT_FACHTEAM;
            }

            @Override
            public int getKeyTypeId() {
                return KeyType.DURCHLAUFZEIT_FACHTEAM.getId();
            }

            @Override
            public int getValue() {
                return value;
            }
        };
    }
}