package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatusService;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.reporting.SelectedDerivatFilterService;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot.StatusabgleichSnapshot;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot.StatusabgleichSnapshotReadService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ReportingStatusabgleichFacadePublicTest {

    @Mock
    DerivatService derivatService;
    @Mock
    StatusabgleichKennzahlenService kennzahlenService;
    @Mock
    StatusabgleichSnapshotReadService snapshotReadService;
    @Mock
    SensorCocService sensorCocService;
    @Mock
    ModulService modulService;
    @Mock
    DerivatStatusService derivatStatusService;
    @Mock
    private SelectedDerivatFilterService selectedDerivatFilterService;

    @InjectMocks
    ReportingStatusabgleichFacade facade;

    @Mock
    HttpServletRequest request;

    UrlParameter urlParameter;
    @Mock
    Derivat derivat;

    @Mock
    StatusabgleichSnapshot snapshot;

    @BeforeEach
    public void setUp() {
        urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("derivat", "42");
        when(derivatService.getAllDerivate()).thenReturn(Collections.EMPTY_LIST);
        when(snapshotReadService.getSnapshotDatesForDerivat(any())).thenReturn(Collections.EMPTY_LIST);
    }

    @Test
    public void testGetPublicViewDataReturnLatestSnapshot() throws Exception {
        when(selectedDerivatFilterService.getDerivatForUrlParameter(any())).thenReturn(Optional.of(derivat));
        urlParameter.addOrReplaceParamter("derivat", "E20");
        facade.getPublicViewData(urlParameter);
        verify(kennzahlenService, never()).getKennzahlenForDerivat(any());
        verify(snapshotReadService, times(1)).getLatestSnapshotForDerivat(any());
    }

    @Test
    public void testGetPublicViewDataReturnSelectedSnapshot() throws Exception {
        when(selectedDerivatFilterService.getDerivatForUrlParameter(any())).thenReturn(Optional.of(derivat));
        when(snapshotReadService.getById(any())).thenReturn(Optional.of(snapshot));
        urlParameter.addOrReplaceParamter("derivat", "E20");
        urlParameter.addOrReplaceParamter("snapshot", "1");
        facade.getPublicViewData(urlParameter);
        verify(kennzahlenService, never()).getKennzahlenForDerivat(any());
        verify(snapshotReadService, times(1)).getById(any());
        verify(snapshotReadService, never()).getLatestSnapshotForDerivat(any());
    }

    @Test
    public void testGetPublicViewDataReturnSelectedSnapshotNotPresent() throws Exception {
        when(selectedDerivatFilterService.getDerivatForUrlParameter(any())).thenReturn(Optional.of(derivat));
        when(snapshotReadService.getById(any())).thenReturn(Optional.empty());
        urlParameter.addOrReplaceParamter("derivat", "E20");
        urlParameter.addOrReplaceParamter("snapshot", "1");
        facade.getPublicViewData(urlParameter);
        verify(kennzahlenService, never()).getKennzahlenForDerivat(any());
        verify(snapshotReadService, times(1)).getById(any());
        verify(snapshotReadService, times(1)).getLatestSnapshotForDerivat(any());
    }

    @Test
    public void testGetPublicViewDataReturnSelectedSnapshotNoValidId() throws Exception {
        when(selectedDerivatFilterService.getDerivatForUrlParameter(any())).thenReturn(Optional.of(derivat));
        urlParameter.addOrReplaceParamter("derivat", "E20");
        urlParameter.addOrReplaceParamter("snapshot", "1A");
        facade.getPublicViewData(urlParameter);
        verify(kennzahlenService, never()).getKennzahlenForDerivat(any());
        verify(snapshotReadService, never()).getById(any());
        verify(snapshotReadService, times(1)).getLatestSnapshotForDerivat(any());
    }

}
