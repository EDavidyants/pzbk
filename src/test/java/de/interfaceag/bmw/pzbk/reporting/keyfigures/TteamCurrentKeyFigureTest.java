package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.IsIterableContaining.hasItems;

class TteamCurrentKeyFigureTest {

    private TteamCurrentKeyFigure keyFigure;

    private boolean langlaufer = true;
    private Collection<Long> ids;

    @BeforeEach
    void setUp() {
        ids = asList(1L, 42L);
        keyFigure = new TteamCurrentKeyFigure(ids, langlaufer);
    }

    @Test
    void isLanglaufer() {
        final boolean keyFigureLanglaufer = keyFigure.isLanglaufer();
        assertThat(keyFigureLanglaufer, is(langlaufer));
    }

    @Test
    void getAnforderungIds() {
        final Collection<Long> anforderungIds = keyFigure.getAnforderungIds();
        assertThat(anforderungIds, hasItems(1L, 42L));
    }

    @Test
    void getMeldungIds() {
        final Collection<Long> meldungIds = keyFigure.getMeldungIds();
        assertThat(meldungIds, is(empty()));
    }
}