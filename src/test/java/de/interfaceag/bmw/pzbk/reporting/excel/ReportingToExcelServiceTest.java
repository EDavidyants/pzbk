package de.interfaceag.bmw.pzbk.reporting.excel;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTuple;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTupleCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.langlaufer.DurchlaufzeitDto;
import de.interfaceag.bmw.pzbk.reporting.langlaufer.DurchlaufzeitService;
import de.interfaceag.bmw.pzbk.reporting.processoverview.ProcessOverviewKeyFigures;
import de.interfaceag.bmw.pzbk.reporting.processoverview.ProcessOverviewViewData;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingToExcelServiceTest {

    @InjectMocks
    private ReportingToExcelService reportingToExcelService;

    @Mock
    private UrlParameter urlParameter;

    @Mock
    private ReportToExcelDao reportToExcelDao;

    @Mock
    private DurchlaufzeitService durchlaufzeitService;

    @Mock
    private ExcelDownloadService excelDownloadService;

    @Mock
    private LocalizationService localizationService;

    @Test
    void getReportingDataForKeyFigure_without_reportingfilter() {
        // GIVEN
        when(reportToExcelDao.getReportingSupportDataForMeldungen(any())).thenReturn(Collections.emptyList());
        when(reportToExcelDao.getReportingSupportDataForAnforderungen(any())).thenReturn(getReportingExcelDataDtoAnforderung());

        // WHEN
        Collection<ReportingExcelDataDto> reportingDataForKeyFigure = this.reportingToExcelService.getReportingDataForKeyFigure(KeyType.FACHTEAM_CURRENT, getProcessOverviewViewData());

        // THEN
        Assert.assertFalse(reportingDataForKeyFigure.isEmpty());
        Assert.assertEquals(2, reportingDataForKeyFigure.size());

        ReportingExcelDataDto reportingExcelDataDto = new ArrayList<>(reportingDataForKeyFigure).get(0);
        checkFirstResult(reportingExcelDataDto);
        Assert.assertNull(reportingExcelDataDto.getDurchlaufzeit());
        Assert.assertNull(reportingExcelDataDto.isLanglaufer());
    }

    @Test
    void getReportingDataForKeyFigure_with_durchlaufzeit() {
        // GIVEN
        when(reportToExcelDao.getReportingSupportDataForMeldungen(any())).thenReturn(Collections.emptyList());
        when(reportToExcelDao.getReportingSupportDataForAnforderungen(any())).thenReturn(getReportingExcelDataDtoAnforderung());
        when(durchlaufzeitService.getFachteamDurchlaufzeiten(any())).thenReturn(getDurchlaufzeiten());

        // WHEN
        Collection<ReportingExcelDataDto> reportingDataForKeyFigure = this.reportingToExcelService.getReportingDataForKeyFigure(KeyType.FACHTEAM_CURRENT, getProcessOverviewViewData(), getReportingFilter());

        // THEN
        Assert.assertFalse(reportingDataForKeyFigure.isEmpty());
        Assert.assertEquals(2, reportingDataForKeyFigure.size());

        ReportingExcelDataDto reportingExcelDataDto = new ArrayList<>(reportingDataForKeyFigure).get(0);
        checkFirstResult(reportingExcelDataDto);
        Assert.assertNotNull(reportingExcelDataDto.getDurchlaufzeit());
        Assert.assertTrue(reportingExcelDataDto.isLanglaufer());
    }

    @Test
    void getReportingDataForKeyFigure_with_empty_durchlaufzeit() {
        // GIVEN
        when(reportToExcelDao.getReportingSupportDataForMeldungen(any())).thenReturn(Collections.emptyList());
        when(reportToExcelDao.getReportingSupportDataForAnforderungen(any())).thenReturn(getReportingExcelDataDtoAnforderung());
        when(durchlaufzeitService.getFachteamDurchlaufzeiten(any())).thenReturn(Collections.emptyList());

        // WHEN
        Collection<ReportingExcelDataDto> reportingDataForKeyFigure = this.reportingToExcelService.getReportingDataForKeyFigure(KeyType.FACHTEAM_CURRENT, getProcessOverviewViewData(), getReportingFilter());

        // THEN
        Assert.assertFalse(reportingDataForKeyFigure.isEmpty());
        Assert.assertEquals(2, reportingDataForKeyFigure.size());

        ReportingExcelDataDto reportingExcelDataDto = new ArrayList<>(reportingDataForKeyFigure).get(0);
        checkFirstResult(reportingExcelDataDto);
        Assert.assertNull(reportingExcelDataDto.getDurchlaufzeit());
        Assert.assertNull(reportingExcelDataDto.isLanglaufer());
    }

    @Test
    void downloadExcelExport() {
        when(localizationService.getCurrentLocale()).thenReturn(Locale.GERMAN);

        this.reportingToExcelService.downloadExcelExport(getProcessOverviewViewData());
        verify(excelDownloadService, times(1)).downloadExcelExport(eq("ReportingData"), any());
    }

    @Test
    void downloadExcelDataForKeyFigure_with_excelData() {
        when(localizationService.getCurrentLocale()).thenReturn(Locale.GERMAN);
        this.reportingToExcelService.downloadExcelDataForKeyFigure(KeyType.FACHTEAM_CURRENT, new ArrayList<>());
        verify(excelDownloadService, times(1)).downloadExcelExport(eq("ReportingData"), any());
    }

    @Test
    void downloadExcelExportForKeyFigure_with_KeyTypViewData() {
        when(localizationService.getCurrentLocale()).thenReturn(Locale.GERMAN);
        this.reportingToExcelService.downloadExcelExportForKeyFigure(KeyType.FACHTEAM_CURRENT, getProcessOverviewViewData());
        verify(excelDownloadService, times(1)).downloadExcelExport(eq("ReportingData"), any());
    }

    @Test
    void downloadExcelExportForKeyFigure_with_ViewData() {
        this.reportingToExcelService.downloadDevelopmentExport(getProcessOverviewViewData());
        verify(excelDownloadService, times(1)).downloadExcelExport(eq("DevelopmentData"), any());
    }

    private void checkFirstResult(ReportingExcelDataDto reportingExcelDataDto) {
        Assert.assertEquals("fachId", reportingExcelDataDto.getFachId());
        Assert.assertEquals("anforderungstext", reportingExcelDataDto.getAnforderungstext());
        Assert.assertEquals("kommentar", reportingExcelDataDto.getKommentar());
        Assert.assertEquals("fachteam", reportingExcelDataDto.getFachteam());
        Assert.assertEquals("", reportingExcelDataDto.getTteam());
        Assert.assertEquals(Status.A_INARBEIT, reportingExcelDataDto.getStatus());
        Assert.assertNotNull(reportingExcelDataDto.getStatusDatum());
        Assert.assertNotNull(reportingExcelDataDto.getAenderungsdatum());
        Assert.assertNotNull(reportingExcelDataDto.getErstellungsDatum());
    }

    private ProcessOverviewViewData getProcessOverviewViewData() {
        return new ProcessOverviewViewData(
                getProcessOverviewKeyFigures(),
                getReportingFilter(),
                null, "");
    }

    private ProcessOverviewKeyFigures getProcessOverviewKeyFigures() {
        return new ProcessOverviewKeyFigures(getKeyFigure(), "");
    }

    private Collection<KeyFigure> getKeyFigure() {
        Collection<KeyFigure> keyFigures = new ArrayList<>();
        keyFigures.add(new KeyFigure() {
            @Override
            public Collection<IdTypeTuple> getIdTypeTuples() {
                return new ArrayList<>();
            }

            @Override
            public Collection<Long> getMeldungIds() {
                return new ArrayList<>();
            }

            @Override
            public Collection<Long> getAnforderungIds() {
                return Arrays.asList(1L, 2L);
            }

            @Override
            public String getMeldungIdsAsString() {
                return null;
            }

            @Override
            public String getAnforderungIdsAsString() {
                return null;
            }

            @Override
            public String getKeyTypeAsString() {
                return null;
            }

            @Override
            public boolean isLanglaufer() {
                return false;
            }

            @Override
            public Long getRunTime() {
                return 1L;
            }

            @Override
            public void restrictToIdTypeTuples(IdTypeTupleCollection validIdTypeTuples) {

            }

            @Override
            public KeyType getKeyType() {
                return KeyType.FACHTEAM_CURRENT;
            }

            @Override
            public int getKeyTypeId() {
                return 5;
            }

            @Override
            public int getValue() {
                return 0;
            }
        });
        return keyFigures;
    }

    private ReportingFilter getReportingFilter() {
        return new ReportingFilter(Page.SUCHE, urlParameter, Collections.emptyList(),
                Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), new Date(), Collections.emptyList());
    }

    private List<ReportingExcelDataDto> getReportingExcelDataDtoAnforderung() {
        return Arrays.asList(
                new ReportingExcelDataDto(1L, Type.ANFORDERUNG, "fachId", "anforderungstext", "kommentar", "fachteam", Status.A_INARBEIT, new Date(), new Date(), new Date()),
                new ReportingExcelDataDto(2L, Type.ANFORDERUNG, "fachId", "anforderungstext", "kommentar", "fachteam", Status.A_INARBEIT, new Date(), new Date(), new Date())
        );
    }

    private List<DurchlaufzeitDto> getDurchlaufzeiten() {
        return Arrays.asList(
                new DurchlaufzeitDto(Type.ANFORDERUNG, 1L, 10L, 2),
                new DurchlaufzeitDto(Type.ANFORDERUNG, 2L, 10L, 2)

        );
    }
}