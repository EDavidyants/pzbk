package de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot;

import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 *
 * @author sl
 */
public class SnapshotFilterConverterTest {

    private final List<Object[]> objects = new ArrayList<>();

    private Date date1;

    @BeforeEach
    public void setUp() {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(1992, 4, 7, 6, 52);
        date1 = calendar1.getTime();

        Object[] object1 = new Object[2];
        object1[0] = 1L;
        object1[1] = date1;

        objects.add(object1);
    }

    @Test
    public void testFromObjectArraySize() {
        List<SnapshotFilter> results = SnapshotFilterConverter.fromObjectArray(objects);
        assertThat(results, hasSize(1));
    }

    @Test
    public void testFromObjectArrayId() {
        List<SnapshotFilter> results = SnapshotFilterConverter.fromObjectArray(objects);
        SnapshotFilter result = results.get(0);
        assertThat(result.getId(), is(1L));
    }

    @Test
    public void testFromObjectArrayDate() {
        List<SnapshotFilter> results = SnapshotFilterConverter.fromObjectArray(objects);
        SnapshotFilter result = results.get(0);
        String expected = "1992.05.07";
        assertThat(result.getDate(), is(expected));
    }

}
