package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.validation;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureConstraint;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureValidationResult;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingCollectionEqualsConstraintTest {

    private static final String VALIDATION__FAILED = "Validation Failed";

    KeyFigureConstraint constraint;

    @Mock
    ProjectReportingProcessKeyFigureData left;
    @Mock
    ProjectReportingProcessKeyFigureData right;

    @BeforeEach
    public void setUp() {
        constraint = ProjectReportingCollectionEqualsConstraint.build()
                .addToLeftSide(left)
                .addToRightSide(right)
                .withValidationMessageFailed(VALIDATION__FAILED)
                .build();
    }

    @Test
    public void testValidateValid() {
        when(left.getValue()).thenReturn(1L);
        when(right.getValue()).thenReturn(1L);

        KeyFigureValidationResult result = constraint.validate();
        boolean valid = result.isValid();
        MatcherAssert.assertThat(valid, is(true));
    }

    @Test
    public void testValidateInvalid() {
        setupInvalid();

        KeyFigureValidationResult result = constraint.validate();
        boolean valid = result.isValid();
        MatcherAssert.assertThat(valid, is(false));
    }

    @Test
    public void testValidateInvalidFailedMessage() {
        setupInvalid();

        KeyFigureValidationResult result = constraint.validate();
        String validationMessage = result.getValidationMessage();
        MatcherAssert.assertThat(validationMessage, is("G1 = G2 Validation Failed 1 = 2"));
    }

    private void setupInvalid() {
        when(left.getValue()).thenReturn(1L);
        when(left.getKeyFigure()).thenReturn(ProjectReportingProcessKeyFigure.G1);
        when(right.getValue()).thenReturn(2L);
        when(right.getKeyFigure()).thenReturn(ProjectReportingProcessKeyFigure.G2);
    }

}
