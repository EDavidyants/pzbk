package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries;

import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter.KeyFigureQueryDateFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Calendar;
import java.util.Date;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class KeyFigureQueryDateFilterTest {

    @Mock
    private ReportingFilter filter;
    @Mock
    private DateSearchFilter vonDateFilter;
    @Mock
    private DateSearchFilter bisDateFilter;

    @BeforeEach
    void setup() {
        when(filter.getVonDateFilter()).thenReturn(vonDateFilter);
        when(filter.getBisDateFilter()).thenReturn(bisDateFilter);

        final Calendar calendar = Calendar.getInstance();
        calendar.set(2018,Calendar.JANUARY,2);
        Date startDate = calendar.getTime();
        calendar.set(2018,Calendar.JANUARY,7);
        Date endDate = calendar.getTime();
        when(vonDateFilter.getSelected()).thenReturn(startDate);
        when(bisDateFilter.getSelected()).thenReturn(endDate);
    }

    @Test
    void append() {
        QueryPart queryPart = new QueryPartDTO();
        KeyFigureQueryDateFilter.append(filter, queryPart);
        final String query = queryPart.getQuery();
        MatcherAssert.assertThat(query, CoreMatchers.is(" AND r.entryDate BETWEEN '2018-01-02' AND '2018-01-08'"));
    }
}