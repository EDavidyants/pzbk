package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.TechnologieFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapWithSize.aMapWithSize;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class KeyFigureFilterUtilsTest {

    @Mock
    private ReportingFilter filter;
    @Mock
    private IdSearchFilter idSearchFilter;
    @Mock
    private TechnologieFilter technologieFilter;
    @Mock
    private DateSearchFilter dateFilter;

    private QueryPart queryPart;

    @BeforeEach
    void setUp() {
        queryPart = new QueryPartDTO();

        when(technologieFilter.isActive()).thenReturn(true);
        when(idSearchFilter.isActive()).thenReturn(true);

        when(filter.getTteamFilter()).thenReturn(idSearchFilter);
        when(filter.getSensorCocFilter()).thenReturn(idSearchFilter);
        when(filter.getDerivatFilter()).thenReturn(idSearchFilter);
        when(filter.getModulSeTeamFilter()).thenReturn(idSearchFilter);
        when(filter.getWerkFilter()).thenReturn(idSearchFilter);

        when(filter.getTechnologieFilter()).thenReturn(technologieFilter);

        when(filter.getTteamFilter().isActive()).thenReturn(true);
        Set<Long> ids = new HashSet<>();
        ids.add(1L);
        when(filter.getTteamFilter().getSelectedIdsAsSet()).thenReturn(ids);
    }

    private void setupDateFilter() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2019);
        calendar.set(Calendar.MONTH, 6);
        calendar.set(Calendar.DAY_OF_MONTH, 6);
        when(dateFilter.getSelected()).thenReturn(calendar.getTime());
        when(filter.getVonDateFilter()).thenReturn(dateFilter);
        when(filter.getBisDateFilter()).thenReturn(dateFilter);
    }

    @Test
    void appendFilterWithDateFilterQuery() {
        setupDateFilter();
        KeyFigureFilterUtils.appendAnforderungFilterWithDateFilter(filter, queryPart);
        assertThat(queryPart.getQuery(), is(" AND r.entryDate BETWEEN '2019-07-06' AND '2019-07-07' AND a.tteam.id IN :tteamIds  AND a.sensorCoc.sensorCocId IN :sensorCocIds  AND u.seTeam.id IN :modulSeTeamIds  AND a.sensorCoc.ortung IN :technologien  AND a.werk.id IN :werke  AND f.id IN :festgestelltIn "));
    }

    @Test
    void appendAnforderungFilterWithDateFilterParameters() {
        setupDateFilter();
        KeyFigureFilterUtils.appendAnforderungFilterWithDateFilter(filter, queryPart);
        assertThat(queryPart.getParameterMap(), aMapWithSize(6));
    }

    @Test
    void appendAnforderungFilterWithoutDateFilterrQuery() {
        KeyFigureFilterUtils.appendAnforderungFilterWithoutDateFilter(filter, queryPart);
        assertThat(queryPart.getQuery(), is(" AND a.tteam.id IN :tteamIds  AND a.sensorCoc.sensorCocId IN :sensorCocIds  AND u.seTeam.id IN :modulSeTeamIds  AND a.sensorCoc.ortung IN :technologien  AND a.werk.id IN :werke  AND f.id IN :festgestelltIn "));
    }

    @Test
    void appendAnforderungFilterWithoutDateFilterParameters() {
        KeyFigureFilterUtils.appendAnforderungFilterWithoutDateFilter(filter, queryPart);
        assertThat(queryPart.getParameterMap(), aMapWithSize(6));
    }

    @Test
    void appendMeldungFilterWithoutDateFilterParameters() {
        KeyFigureFilterUtils.appendMeldungFilterWithoutDateFilter(filter, queryPart);
        assertThat(queryPart.getParameterMap(), aMapWithSize(6));
    }

    @Test
    void appendMeldungFilterWithoutDateFilterQuery() {
        KeyFigureFilterUtils.appendMeldungFilterWithoutDateFilter(filter, queryPart);
        assertThat(queryPart.getQuery(), is(" AND a.tteam.id IN :tteamIds  AND m.sensorCoc.sensorCocId IN :sensorCocIds  AND u.seTeam.id IN :modulSeTeamIds  AND m.sensorCoc.ortung IN :technologien  AND a.werk.id IN :werke  AND f.id IN :festgestelltIn "));
    }

}