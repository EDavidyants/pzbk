package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.excel;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollectionDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingExcelExportUtilsContentTest {

    private static final String SHEETNAME = "SHEET";

    private ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat;

    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData0;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData1;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData2;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData3;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData4;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData5;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData6;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData7;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData8;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData9;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData10;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData11;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData12;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData13;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData14;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData15;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData16;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData17;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData18;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData19;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData20;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData21;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData22;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData23;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData24;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData25;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData26;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData27;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData28;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData29;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData30;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData31;

    private final Date date = null;

    @BeforeEach
    public void setup() {
        Map<Integer, ProjectReportingProcessKeyFigureData> keyFigureDataMap = new HashMap<>();
        keyFigureDataMap.put(0, keyFigureData0);
        keyFigureDataMap.put(1, keyFigureData1);
        keyFigureDataMap.put(2, keyFigureData2);
        keyFigureDataMap.put(3, keyFigureData3);
        keyFigureDataMap.put(4, keyFigureData4);
        keyFigureDataMap.put(5, keyFigureData5);
        keyFigureDataMap.put(6, keyFigureData6);
        keyFigureDataMap.put(7, keyFigureData7);
        keyFigureDataMap.put(8, keyFigureData8);
        keyFigureDataMap.put(9, keyFigureData9);
        keyFigureDataMap.put(10, keyFigureData10);
        keyFigureDataMap.put(11, keyFigureData11);
        keyFigureDataMap.put(12, keyFigureData12);
        keyFigureDataMap.put(13, keyFigureData13);
        keyFigureDataMap.put(14, keyFigureData14);
        keyFigureDataMap.put(15, keyFigureData15);
        keyFigureDataMap.put(16, keyFigureData16);
        keyFigureDataMap.put(17, keyFigureData17);
        keyFigureDataMap.put(18, keyFigureData18);
        keyFigureDataMap.put(19, keyFigureData19);
        keyFigureDataMap.put(20, keyFigureData20);
        keyFigureDataMap.put(21, keyFigureData21);
        keyFigureDataMap.put(22, keyFigureData22);
        keyFigureDataMap.put(23, keyFigureData23);
        keyFigureDataMap.put(24, keyFigureData24);
        keyFigureDataMap.put(25, keyFigureData25);
        keyFigureDataMap.put(26, keyFigureData26);
        keyFigureDataMap.put(27, keyFigureData27);
        keyFigureDataMap.put(28, keyFigureData28);
        keyFigureDataMap.put(29, keyFigureData29);
        keyFigureDataMap.put(30, keyFigureData30);
        keyFigureDataMap.put(31, keyFigureData31);

        keyFiguresForDerivat = ProjectReportingProcessKeyFigureDataCollectionDto.forMap(keyFigureDataMap, date);

        when(keyFigureData0.getValue()).thenReturn(0L);
        when(keyFigureData1.getValue()).thenReturn(1L);
        when(keyFigureData2.getValue()).thenReturn(2L);
        when(keyFigureData3.getValue()).thenReturn(3L);
        when(keyFigureData4.getValue()).thenReturn(4L);
        when(keyFigureData5.getValue()).thenReturn(5L);
        when(keyFigureData6.getValue()).thenReturn(6L);
        when(keyFigureData7.getValue()).thenReturn(7L);
        when(keyFigureData8.getValue()).thenReturn(8L);
        when(keyFigureData9.getValue()).thenReturn(9L);
        when(keyFigureData10.getValue()).thenReturn(10L);
        when(keyFigureData11.getValue()).thenReturn(11L);
        when(keyFigureData12.getValue()).thenReturn(12L);
        when(keyFigureData13.getValue()).thenReturn(13L);
        when(keyFigureData14.getValue()).thenReturn(14L);
        when(keyFigureData15.getValue()).thenReturn(15L);
        when(keyFigureData16.getValue()).thenReturn(16L);
        when(keyFigureData17.getValue()).thenReturn(17L);
        when(keyFigureData18.getValue()).thenReturn(18L);
        when(keyFigureData19.getValue()).thenReturn(19L);
        when(keyFigureData20.getValue()).thenReturn(20L);
        when(keyFigureData21.getValue()).thenReturn(21L);
        when(keyFigureData22.getValue()).thenReturn(22L);
        when(keyFigureData23.getValue()).thenReturn(23L);
        when(keyFigureData24.getValue()).thenReturn(24L);
        when(keyFigureData25.getValue()).thenReturn(25L);
        when(keyFigureData26.getValue()).thenReturn(26L);
        when(keyFigureData27.getValue()).thenReturn(27L);
        when(keyFigureData28.getValue()).thenReturn(28L);
        when(keyFigureData29.getValue()).thenReturn(29L);
        when(keyFigureData30.getValue()).thenReturn(30L);
        when(keyFigureData31.getValue()).thenReturn(31L);
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn0() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(0);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn1() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(1);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(1.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn2() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(2);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(2.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn3() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(3);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(3.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn4() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(4);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(4.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn5() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(5);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(5.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn6() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(6);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(6.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn7() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(7);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(7.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn8() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(8);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(8.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn9() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(9);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(9.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn10() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(10);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(10.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn11() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(11);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(11.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn12() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(12);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(12.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn13() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(13);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(13.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn14() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(14);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(14.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn15() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(15);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(15.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn16() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(16);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(16.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn17() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(17);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(17.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn18() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(18);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(18.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn19() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(19);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(19.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn20() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(20);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(20.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn21() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(21);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(21.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn22() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(22);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(22.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn23() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(23);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(23.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn24() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(24);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(24.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn25() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(25);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(25.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn26() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(26);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(26.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn27() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(27);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(27.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn28() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(28);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(28.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn29() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(29);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(29.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn30() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(30);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(30.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn31() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(31);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(31.0));
    }

}
