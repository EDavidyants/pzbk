package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class VereinbarungOutputKeyFigureTest {

    private final Collection<Long> idsFreigegeben = Arrays.asList(1L, 2L);
    private final Collection<Long> idsGeloescht = Arrays.asList(2L, 3L, 4L);
    private final Collection<Long> idsProzessbaukasten = Arrays.asList(5L, 6L, 7L);

    private VereinbarungFreigegebenKeyFigure vereinbarungFreigegebenKeyFigure;
    private VereinbarungGeloeschtKeyFigure vereinbarungGeloeschtKeyFigure;
    private VereinbarungUnstimmigKeyFigure vereinbarungUnstimmigKeyFigure;
    private VereinbarungProzessbaukastenKeyFigure vereinbarungProzessbaukastenKeyFigure;

    @Before
    public void setUp() {
        initKeyFigures(idsFreigegeben, idsGeloescht, idsProzessbaukasten);
    }

    private void initKeyFigures(Collection<Long> idsFreigegeben, Collection<Long> idsGeloescht, Collection<Long> idProzessbaukasten) {
        vereinbarungFreigegebenKeyFigure = new VereinbarungFreigegebenKeyFigure(idsFreigegeben, 0L);
        vereinbarungGeloeschtKeyFigure = new VereinbarungGeloeschtKeyFigure(idsGeloescht, 0L);
        vereinbarungUnstimmigKeyFigure = new VereinbarungUnstimmigKeyFigure(idsGeloescht, 0L);
        vereinbarungProzessbaukastenKeyFigure = new VereinbarungProzessbaukastenKeyFigure(idProzessbaukasten);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresBothParamtersNull() throws InvalidDataException {
        VereinbarungOutputKeyFigure result = VereinbarungOutputKeyFigure.buildFromKeyFigures(null, null, null, null);
        assertEquals(KeyType.VEREINBARUNG_OUTPUT.getId(), result.getKeyTypeId());
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresFirsParamterNotNull() throws InvalidDataException {
        VereinbarungOutputKeyFigure.buildFromKeyFigures(vereinbarungFreigegebenKeyFigure, null, null, null);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresSecondParamterNotNull() throws InvalidDataException {
        VereinbarungOutputKeyFigure.buildFromKeyFigures(null, vereinbarungGeloeschtKeyFigure, null, null);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresThirdParamterNotNull() throws InvalidDataException {
        VereinbarungOutputKeyFigure.buildFromKeyFigures(null, null, vereinbarungUnstimmigKeyFigure, null);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresFourthParamterNotNull() throws InvalidDataException {
        VereinbarungOutputKeyFigure.buildFromKeyFigures(null, null, null, vereinbarungProzessbaukastenKeyFigure);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresFirstParamterNull() throws InvalidDataException {
        VereinbarungOutputKeyFigure.buildFromKeyFigures(null, vereinbarungGeloeschtKeyFigure, vereinbarungUnstimmigKeyFigure, vereinbarungProzessbaukastenKeyFigure);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresSecondParamterNull() throws InvalidDataException {
        VereinbarungOutputKeyFigure.buildFromKeyFigures(vereinbarungFreigegebenKeyFigure, null, vereinbarungUnstimmigKeyFigure, vereinbarungProzessbaukastenKeyFigure);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresThirdParamterNull() throws InvalidDataException {
        VereinbarungOutputKeyFigure.buildFromKeyFigures(vereinbarungFreigegebenKeyFigure, vereinbarungGeloeschtKeyFigure, null, vereinbarungProzessbaukastenKeyFigure);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresFourthParamterNull() throws InvalidDataException {
        VereinbarungOutputKeyFigure.buildFromKeyFigures(vereinbarungFreigegebenKeyFigure, vereinbarungGeloeschtKeyFigure, vereinbarungUnstimmigKeyFigure, null);
    }

    @Test
    public void testBuildFromKeyFiguresKeyTypeId() throws InvalidDataException {
        VereinbarungOutputKeyFigure result = VereinbarungOutputKeyFigure.buildFromKeyFigures(vereinbarungFreigegebenKeyFigure, vereinbarungGeloeschtKeyFigure, vereinbarungUnstimmigKeyFigure, vereinbarungProzessbaukastenKeyFigure);
        assertEquals(KeyType.VEREINBARUNG_OUTPUT.getId(), result.getKeyTypeId());
    }

    @Test
    public void testBuildFromKeyFiguresCollectionSize() throws InvalidDataException {
        VereinbarungOutputKeyFigure result = VereinbarungOutputKeyFigure.buildFromKeyFigures(vereinbarungFreigegebenKeyFigure, vereinbarungGeloeschtKeyFigure, vereinbarungUnstimmigKeyFigure, vereinbarungProzessbaukastenKeyFigure);
        assertEquals(11, result.getIdTypeTuples().size());
    }

    @Test
    public void testBuildFromKeyFiguresEmptyCollection() throws InvalidDataException {
        VereinbarungFreigegebenKeyFigure keyFigureWithEmptyCollection = new VereinbarungFreigegebenKeyFigure(Collections.EMPTY_LIST, 0L);
        VereinbarungOutputKeyFigure result = VereinbarungOutputKeyFigure.buildFromKeyFigures(keyFigureWithEmptyCollection, vereinbarungGeloeschtKeyFigure, vereinbarungUnstimmigKeyFigure, vereinbarungProzessbaukastenKeyFigure);
        assertEquals(9, result.getIdTypeTuples().size());
    }

    @Test
    public void testBuildFromKeyFiguresRuntimeNotNull() throws InvalidDataException {
        VereinbarungOutputKeyFigure result = VereinbarungOutputKeyFigure.buildFromKeyFigures(vereinbarungFreigegebenKeyFigure, vereinbarungGeloeschtKeyFigure, vereinbarungUnstimmigKeyFigure, vereinbarungProzessbaukastenKeyFigure);
        assertNotNull(result.getRunTime());
    }

    @Test
    public void testBuildFromKeyFiguresRuntime() throws InvalidDataException {
        VereinbarungOutputKeyFigure result = VereinbarungOutputKeyFigure.buildFromKeyFigures(vereinbarungFreigegebenKeyFigure, vereinbarungGeloeschtKeyFigure, vereinbarungUnstimmigKeyFigure, vereinbarungProzessbaukastenKeyFigure);
        assertEquals(0L, (long) result.getRunTime());
    }

}
