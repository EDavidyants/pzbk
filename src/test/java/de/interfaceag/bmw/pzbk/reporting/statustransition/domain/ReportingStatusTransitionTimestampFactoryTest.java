package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTransitionTimestampFactoryTest {

    @InjectMocks
    private ReportingStatusTransitionTimestampFactory reportingStatusTransitionTimestampFactory;
    @Mock
    private Date entryDate;
    @Mock
    private Date exitDate;

    @Test
    void getNewEntryTimeStampEntry() {
        final EntryExitTimeStamp result = reportingStatusTransitionTimestampFactory.getNewEntryTimeStamp(entryDate);
        final Date entry = result.getEntry();
        assertThat(entry, notNullValue());
    }

    @Test
    void getNewEntryTimeStampExit() {
        final EntryExitTimeStamp result = reportingStatusTransitionTimestampFactory.getNewEntryTimeStamp(entryDate);
        final Date exit = result.getExit();
        assertThat(exit, nullValue());
    }

    @Test
    void getNewExitTimeStampEntry() {
        final EntryExitTimeStamp result = reportingStatusTransitionTimestampFactory.getNewExitTimeStamp(exitDate);
        final Date entry = result.getEntry();
        assertThat(entry, nullValue());
    }

    @Test
    void getNewExitTimeStampExit() {
        final EntryExitTimeStamp result = reportingStatusTransitionTimestampFactory.getNewExitTimeStamp(exitDate);
        final Date exit = result.getExit();
        assertThat(exit, notNullValue());
    }

    @Test
    void getNewEmptyTimeStampEntry() {
        final EntryExitTimeStamp result = reportingStatusTransitionTimestampFactory.getNewEmptyTimeStamp();
        final Date entry = result.getEntry();
        assertThat(entry, nullValue());
    }

    @Test
    void getNewEmptyTimeStampExit() {
        final EntryExitTimeStamp result = reportingStatusTransitionTimestampFactory.getNewEmptyTimeStamp();
        final Date exit = result.getExit();
        assertThat(exit, nullValue());
    }
}
