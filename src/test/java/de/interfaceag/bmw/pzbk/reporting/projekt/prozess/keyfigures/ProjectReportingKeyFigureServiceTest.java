package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessAmpelThresholdService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingKeyFigureServiceTest {

    @Mock
    ProjectReportingVereinbarungKeyFigureService vereinbarungKeyFigureService;
    @Mock
    ProjectReportingUmsetzungsverwaltungKeyFigureService umsetzungsverwaltungKeyFigureService;
    @Mock
    ProjectReportingAusleitungKeyFigureService ausleitungKeyFigureService;

    @Mock
    ProjectProcessAmpelThresholdService ampelThresholdService;

    @InjectMocks
    ProjectReportingKeyFigureService keyFigureService;

    @Mock
    Derivat derivat;

    @Mock
    ProjectProcessFilter filter;

    Map<Integer, ProjectReportingProcessKeyFigureData> vereinbarungVKBGResult;
    Map<Integer, ProjectReportingProcessKeyFigureData> vereinbarungZVResult;
    Map<Integer, ProjectReportingProcessKeyFigureData> umsetzungsberstaetivungVKBGResult;
    Map<Integer, ProjectReportingProcessKeyFigureData> umsetzungsberstaetivungVBBGResult;
    Map<Integer, ProjectReportingProcessKeyFigureData> umsetzungsberstaetivungBBGResult;
    Map<Integer, ProjectReportingProcessKeyFigureData> ausleitungResult;

    @Mock
    ProjectReportingProcessKeyFigureData keyFigureData1;
    @Mock
    ProjectReportingProcessKeyFigureData keyFigureData2;
    @Mock
    ProjectReportingProcessKeyFigureData keyFigureData3;
    @Mock
    ProjectReportingProcessKeyFigureData keyFigureData4;
    @Mock
    ProjectReportingProcessKeyFigureData keyFigureData5;
    @Mock
    ProjectReportingProcessKeyFigureData keyFigureData6;

    @BeforeEach
    public void setUp() {
        vereinbarungVKBGResult = new HashMap<>();
        vereinbarungZVResult = new HashMap<>();
        umsetzungsberstaetivungVKBGResult = new HashMap<>();
        umsetzungsberstaetivungVBBGResult = new HashMap<>();
        umsetzungsberstaetivungBBGResult = new HashMap<>();
        ausleitungResult = new HashMap<>();

        vereinbarungVKBGResult.put(1, keyFigureData1);
        vereinbarungVKBGResult.put(2, keyFigureData2);

        vereinbarungZVResult.put(3, keyFigureData3);

        umsetzungsberstaetivungVKBGResult.put(4, keyFigureData4);

        umsetzungsberstaetivungVBBGResult.put(5, keyFigureData5);

        umsetzungsberstaetivungBBGResult.put(6, keyFigureData6);

        ausleitungResult.put(4, keyFigureData4);
        ausleitungResult.put(5, keyFigureData5);

        when(vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(any(), any())).thenReturn(vereinbarungVKBGResult);
        when(vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(any(), any())).thenReturn(vereinbarungZVResult);
        when(umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVBBG(any(), any(), any())).thenReturn(umsetzungsberstaetivungVBBGResult);
        when(umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(any(), any(), any())).thenReturn(umsetzungsberstaetivungBBGResult);
        when(umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungVKBG(any(), any(), any())).thenReturn(umsetzungsberstaetivungVKBGResult);
        when(vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(any(), any())).thenReturn(vereinbarungVKBGResult);
        when(vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(any(), any())).thenReturn(vereinbarungZVResult);
        when(ausleitungKeyFigureService.getKeyFiguresForAusleitung(any(), any())).thenReturn(ausleitungResult);
    }

    @Test
    public void testComputeKeyFiguresForDerivatWithFilterContainsKey1() {
        ProjectReportingProcessKeyFigureDataCollection result = keyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = result.getDataForKeyFigure(ProjectReportingProcessKeyFigure.G2);
        Assertions.assertTrue(dataForKeyFigure.isPresent());
    }

    @Test
    public void testComputeKeyFiguresForDerivatWithFilterContainsKeyValue1() {
        ProjectReportingProcessKeyFigureDataCollection result = keyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = result.getDataForKeyFigure(ProjectReportingProcessKeyFigure.G2);
        MatcherAssert.assertThat(dataForKeyFigure.get(), is(keyFigureData1));
    }

    @Test
    public void testComputeKeyFiguresForDerivatWithFilterContainsKey2() {
        ProjectReportingProcessKeyFigureDataCollection result = keyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = result.getDataForKeyFigure(ProjectReportingProcessKeyFigure.G3);
        Assertions.assertTrue(dataForKeyFigure.isPresent());
    }

    @Test
    public void testComputeKeyFiguresForDerivatWithFilterContainsKeyValue2() {
        ProjectReportingProcessKeyFigureDataCollection result = keyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = result.getDataForKeyFigure(ProjectReportingProcessKeyFigure.G3);
        MatcherAssert.assertThat(dataForKeyFigure.get(), is(keyFigureData2));
    }

    @Test
    public void testComputeKeyFiguresForDerivatWithFilterContainsKey3() {
        ProjectReportingProcessKeyFigureDataCollection result = keyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = result.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L1);
        Assertions.assertTrue(dataForKeyFigure.isPresent());
    }

    @Test
    public void testComputeKeyFiguresForDerivatWithFilterContainsKeyValue3() {
        ProjectReportingProcessKeyFigureDataCollection result = keyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = result.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L1);
        MatcherAssert.assertThat(dataForKeyFigure.get(), is(keyFigureData3));
    }

    @Test
    public void testComputeKeyFiguresForDerivatWithFilterContainsKey4() {
        ProjectReportingProcessKeyFigureDataCollection result = keyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = result.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L2);
        Assertions.assertTrue(dataForKeyFigure.isPresent());
    }

    @Test
    public void testComputeKeyFiguresForDerivatWithFilterContainsKeyValue4() {
        ProjectReportingProcessKeyFigureDataCollection result = keyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = result.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L2);
        MatcherAssert.assertThat(dataForKeyFigure.get(), is(keyFigureData4));
    }

    @Test
    public void testComputeKeyFiguresForDerivatWithFilterContainsKey5() {
        ProjectReportingProcessKeyFigureDataCollection result = keyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = result.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L3);
        Assertions.assertTrue(dataForKeyFigure.isPresent());
    }

    @Test
    public void testComputeKeyFiguresForDerivatWithFilterContainsKeyValue5() {
        ProjectReportingProcessKeyFigureDataCollection result = keyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = result.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L3);
        MatcherAssert.assertThat(dataForKeyFigure.get(), is(keyFigureData5));
    }

    @Test
    public void testComputeKeyFiguresForDerivatWithFilterContainsKey6() {
        ProjectReportingProcessKeyFigureDataCollection result = keyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = result.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L4);
        Assertions.assertTrue(dataForKeyFigure.isPresent());
    }

    @Test
    public void testComputeKeyFiguresForDerivatWithFilterContainsKeyValue6() {
        ProjectReportingProcessKeyFigureDataCollection result = keyFigureService.computeKeyFiguresForDerivat(derivat, filter);
        Optional<ProjectReportingProcessKeyFigureData> dataForKeyFigure = result.getDataForKeyFigure(ProjectReportingProcessKeyFigure.L4);
        MatcherAssert.assertThat(dataForKeyFigure.get(), is(keyFigureData6));
    }

}
