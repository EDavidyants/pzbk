package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
class ReportingFachteamViewDataTest {

    @Mock
    private ReportingFilter filter;
    @Mock
    private FachteamKeyFigures fachteamKeyFigures;

    private ReportingFachteamViewData viewData;

    @BeforeEach
    void setUp() {
        viewData = new ReportingFachteamViewData(fachteamKeyFigures, filter);
    }

    @Test
    void getFachteamKeyFigures() {
        final FachteamKeyFigures viewDataFachteamKeyFigures = viewData.getFachteamKeyFigures();
        assertThat(viewDataFachteamKeyFigures, is(fachteamKeyFigures));
    }

    @Test
    void getFilter() {
        final ReportingFilter viewDataFilter = viewData.getFilter();
        assertThat(viewDataFilter, is(filter));
    }
}