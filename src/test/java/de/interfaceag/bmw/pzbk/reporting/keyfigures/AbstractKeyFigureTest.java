package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.enums.Type;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.IsIterableContaining.hasItem;
import static org.junit.jupiter.api.Assertions.assertFalse;

class AbstractKeyFigureTest {

    private FachteamCurrentObjectsFigure keyFigure;

    private Long runtime = 42L;
    private Collection<Long> anforderungIds;
    private Collection<Long> meldungIds;

    @BeforeEach
    void setUp() {
        anforderungIds = Arrays.asList(1L, 2L);
        meldungIds = Arrays.asList(1L, 2L, 3L);
        keyFigure = new FachteamCurrentObjectsFigure(meldungIds, anforderungIds, runtime);
    }

    @Test
    void toString1() {
        final String result = keyFigure.toString();
        assertThat(result, is("Anzahl Objekte im Fachteam: {value=5}"));
    }

    @Test
    void restrictToIdTypeTuplesEmptyInput() {
        keyFigure.restrictToIdTypeTuples(new IdTypeTupleCollection());
        final Collection<IdTypeTuple> idTypeTuples = keyFigure.getIdTypeTuples();
        assertThat(idTypeTuples, empty());
    }

    @Test
    void restrictToIdTypeTuplesNullInput() {
        keyFigure.restrictToIdTypeTuples(null);
        final Collection<IdTypeTuple> idTypeTuples = keyFigure.getIdTypeTuples();
        assertThat(idTypeTuples, empty());
    }

    @Test
    void restrictToIdTypeTuplesCorrectId() {
        IdTypeTupleCollection validIdTypeTuples = new IdTypeTupleCollection();
        IdTypeTuple validTuple1 = new IdTypeTuple(Type.ANFORDERUNG, 1L);
        validIdTypeTuples.add(validTuple1);
        keyFigure.restrictToIdTypeTuples(validIdTypeTuples);
        final Collection<Long> anforderungIds = keyFigure.getAnforderungIds();
        assertThat(anforderungIds, hasItem(1L));
    }

    @Test
    void restrictToIdTypeTuplesCorrectSize() {
        IdTypeTupleCollection validIdTypeTuples = new IdTypeTupleCollection();
        IdTypeTuple validTuple1 = new IdTypeTuple(Type.ANFORDERUNG, 1L);
        validIdTypeTuples.add(validTuple1);
        keyFigure.restrictToIdTypeTuples(validIdTypeTuples);
        final Collection<Long> anforderungIds = keyFigure.getAnforderungIds();
        assertThat(anforderungIds, hasSize(1));
    }

    @Test
    void restrictToIdTypeTuplesValue() {
        IdTypeTupleCollection validIdTypeTuples = new IdTypeTupleCollection();
        IdTypeTuple validTuple1 = new IdTypeTuple(Type.ANFORDERUNG, 1L);
        validIdTypeTuples.add(validTuple1);
        keyFigure.restrictToIdTypeTuples(validIdTypeTuples);
        final int value = keyFigure.getValue();
        assertThat(value, is(1));
    }

    @Test
    void getIdTypeTuplesSize() {
        final Collection<IdTypeTuple> idTypeTuples = keyFigure.getIdTypeTuples();
        assertThat(idTypeTuples, hasSize(5));
    }

    @Test
    void getKeyType() {
        final KeyType keyType = keyFigure.getKeyType();
        assertThat(keyType, is(KeyType.FACHTEAM_CURRENT));
    }

    @Test
    void getKeyTypeId() {
        final int keyTypeId = keyFigure.getKeyTypeId();
        assertThat(keyTypeId, is(KeyType.FACHTEAM_CURRENT.getId()));
    }

    @Test
    void getMeldungIds() {
        final Collection<Long> keyFigureMeldungIds = keyFigure.getMeldungIds();
        assertThat(keyFigureMeldungIds, is(meldungIds));
    }

    @Test
    void getAnforderungIds() {
        final Collection<Long> keyFigureAnforderungIds = keyFigure.getAnforderungIds();
        assertThat(keyFigureAnforderungIds, is(anforderungIds));
    }

    @Test
    void getMeldungIdsAsString() {
        final String meldungIdsAsString = keyFigure.getMeldungIdsAsString();
        assertThat(meldungIdsAsString, is("1,2,3"));
    }

    @Test
    void getAnforderungIdsAsString() {
        final String anforderungIdsAsString = keyFigure.getAnforderungIdsAsString();
        assertThat(anforderungIdsAsString, is("1,2"));
    }

    @Test
    void getKeyTypeAsString() {
        final String keyTypeAsString = keyFigure.getKeyTypeAsString();
        assertThat(keyTypeAsString, is("Anzahl Objekte im Fachteam"));
    }

    @Test
    void isLanglaufer() {
        final boolean langlaufer = keyFigure.isLanglaufer();
        assertFalse(langlaufer);
    }

    @Test
    void getRunTime() {
        final Long keyFigureRunTime = keyFigure.getRunTime();
        assertThat(keyFigureRunTime, is(runtime));
    }
}