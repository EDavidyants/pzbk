package de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.StatusabgleichKennzahlen;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.aMapWithSize;
import static org.junit.Assert.assertThat;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class StatusabgleichSnapshotConverterConvertToKennzahlenTest {

    @Mock
    private Derivat derivat;

    private StatusabgleichSnapshot snapshot;

    @BeforeEach
    public void setUp() {
        snapshot = new StatusabgleichSnapshot(derivat);
        StatusabgleichSnapshotEntry entry = new StatusabgleichSnapshotEntry(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.PENDING, 1);
        StatusabgleichSnapshotEntry entry2 = new StatusabgleichSnapshotEntry(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.DONE, 2);
        snapshot.addEntry(entry);
        snapshot.addEntry(entry2);
    }

    @Test
    public void testConvertToKennzahlenMapSize() {
        StatusabgleichKennzahlen kennzahlen = StatusabgleichSnapshotConverter.convertToKennzahlen(snapshot);
        Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> matrixValues = kennzahlen.getMatrixValues();
        assertThat(matrixValues, aMapWithSize(2));
    }

    @Test
    public void testConvertToKennzahlenEntry1() {
        StatusabgleichKennzahlen kennzahlen = StatusabgleichSnapshotConverter.convertToKennzahlen(snapshot);
        Long result = kennzahlen.getValue(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.PENDING);
        assertThat(result, is(1L));
    }

    @Test
    public void testConvertToKennzahlenEntry2() {
        StatusabgleichKennzahlen kennzahlen = StatusabgleichSnapshotConverter.convertToKennzahlen(snapshot);
        Long result = kennzahlen.getValue(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.DONE);
        assertThat(result, is(2L));
    }

    @Test
    public void testConvertToKennzahlenNoEntry() {
        StatusabgleichKennzahlen kennzahlen = StatusabgleichSnapshotConverter.convertToKennzahlen(snapshot);
        Long result = kennzahlen.getValue(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.BESTAETIGT_AUS_VORPROZESS);
        assertThat(result, is(0L));
    }

}
