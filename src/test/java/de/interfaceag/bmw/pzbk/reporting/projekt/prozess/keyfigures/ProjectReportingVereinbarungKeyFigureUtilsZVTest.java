package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessAmpelThresholdService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsMapContaining;
import org.hamcrest.collection.IsMapWithSize;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingVereinbarungKeyFigureUtilsZVTest {

    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR6;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR7;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR8;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR9;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR10;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR11;

    @InjectMocks
    private final ProjectProcessAmpelThresholdService ampelThresholdService = new ProjectProcessAmpelThresholdService();

    Map<Integer, ProjectReportingProcessKeyFigureData> keyFigureData;

    @BeforeEach
    public void setUp() {
        when(ProjectReportingProcessKeyFigureDataR6.getValue()).thenReturn(4L);
        when(ProjectReportingProcessKeyFigureDataR7.getValue()).thenReturn(5L);
        when(ProjectReportingProcessKeyFigureDataR8.getValue()).thenReturn(6L);
        when(ProjectReportingProcessKeyFigureDataR9.getValue()).thenReturn(7L);
        when(ProjectReportingProcessKeyFigureDataR10.getValue()).thenReturn(8L);
        when(ProjectReportingProcessKeyFigureDataR11.getValue()).thenReturn(9L);

        keyFigureData = new HashMap<>();
        keyFigureData.put(ProjectReportingProcessKeyFigure.R6.getId(), ProjectReportingProcessKeyFigureDataR6);
        keyFigureData.put(ProjectReportingProcessKeyFigure.R7.getId(), ProjectReportingProcessKeyFigureDataR7);
        keyFigureData.put(ProjectReportingProcessKeyFigure.R8.getId(), ProjectReportingProcessKeyFigureDataR8);
        keyFigureData.put(ProjectReportingProcessKeyFigure.R9.getId(), ProjectReportingProcessKeyFigureDataR9);
        keyFigureData.put(ProjectReportingProcessKeyFigure.R10.getId(), ProjectReportingProcessKeyFigureDataR10);
        keyFigureData.put(ProjectReportingProcessKeyFigure.R11.getId(), ProjectReportingProcessKeyFigureDataR11);
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungZVResultSize() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungZV(keyFigureData, ampelThresholdService.getThresholdForVereinbarungZV());
        MatcherAssert.assertThat(result, IsMapWithSize.aMapWithSize(9));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungZVContainsR3() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungZV(keyFigureData, ampelThresholdService.getThresholdForVereinbarungZV());
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R3.getId()));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungZVR3Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungZV(keyFigureData, ampelThresholdService.getThresholdForVereinbarungZV());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R3.getId());
        Long dataValue = data.getValue();
        MatcherAssert.assertThat(dataValue, is(22L));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungZVContainsR4() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungZV(keyFigureData, ampelThresholdService.getThresholdForVereinbarungZV());
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R4.getId()));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungZVR4Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungZV(keyFigureData, ampelThresholdService.getThresholdForVereinbarungZV());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R4.getId());
        Long dataValue = data.getValue();
        MatcherAssert.assertThat(dataValue, is(17L));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungZVContainsR5() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungZV(keyFigureData, ampelThresholdService.getThresholdForVereinbarungZV());
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R5.getId()));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungZVR5Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungZV(keyFigureData, ampelThresholdService.getThresholdForVereinbarungZV());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R5.getId());
        Long dataValue = data.getValue();
        MatcherAssert.assertThat(dataValue, is(18L));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungZVR5Ampel() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungZV(keyFigureData, ampelThresholdService.getThresholdForVereinbarungZV());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R5.getId());
        boolean isAmpelGreen = data.isAmpelGreen();
        Assertions.assertFalse(isAmpelGreen);
    }
}
