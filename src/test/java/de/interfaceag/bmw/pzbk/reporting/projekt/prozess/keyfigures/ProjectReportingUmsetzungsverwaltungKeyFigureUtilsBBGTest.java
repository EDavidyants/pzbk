package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessAmpelThresholdService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsMapContaining;
import org.hamcrest.collection.IsMapWithSize;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingUmsetzungsverwaltungKeyFigureUtilsBBGTest {

    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR21;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR22;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR23;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR24;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataR25;

    @InjectMocks
    private final ProjectProcessAmpelThresholdService ampelThresholdService = new ProjectProcessAmpelThresholdService();

    Map<Integer, ProjectReportingProcessKeyFigureData> keyFigureData;

    @BeforeEach
    public void setUp() {
        when(ProjectReportingProcessKeyFigureDataR21.getValue()).thenReturn(4L);
        when(ProjectReportingProcessKeyFigureDataR22.getValue()).thenReturn(5L);
        when(ProjectReportingProcessKeyFigureDataR23.getValue()).thenReturn(6L);
        when(ProjectReportingProcessKeyFigureDataR24.getValue()).thenReturn(7L);

        keyFigureData = new HashMap<>();
        keyFigureData.put(ProjectReportingProcessKeyFigure.R21.getId(), ProjectReportingProcessKeyFigureDataR21);
        keyFigureData.put(ProjectReportingProcessKeyFigure.R22.getId(), ProjectReportingProcessKeyFigureDataR22);
        keyFigureData.put(ProjectReportingProcessKeyFigure.R23.getId(), ProjectReportingProcessKeyFigureDataR23);
        keyFigureData.put(ProjectReportingProcessKeyFigure.R24.getId(), ProjectReportingProcessKeyFigureDataR24);
        keyFigureData.put(ProjectReportingProcessKeyFigure.R25.getId(), ProjectReportingProcessKeyFigureDataR25);
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBGResultSize() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungBBG());
        MatcherAssert.assertThat(result, IsMapWithSize.aMapWithSize(7));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBGContainsR19() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungBBG());
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R19.getId()));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBGR19Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungBBG());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R19.getId());
        Long dataValue = data.getValue();
        MatcherAssert.assertThat(dataValue, is(22L));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBGContainsR20() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungBBG());
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R20.getId()));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBGR20Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungBBG());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R20.getId());
        Long dataValue = data.getValue();
        MatcherAssert.assertThat(dataValue, is(18L));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBGR20Ampel() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungBBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungBBG());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R20.getId());
        boolean isAmpelGreen = data.isAmpelGreen();
        Assertions.assertFalse(isAmpelGreen);
    }

}
