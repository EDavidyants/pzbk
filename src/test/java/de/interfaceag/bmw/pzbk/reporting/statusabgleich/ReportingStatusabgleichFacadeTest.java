package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatusService;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.reporting.SelectedDerivatFilterService;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot.StatusabgleichSnapshot;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot.StatusabgleichSnapshotReadService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
//@MockitoSettings(strictness = Strictness.LENIENT)
public class ReportingStatusabgleichFacadeTest {

    @Mock
    private DerivatService derivatService;
    @Mock
    private StatusabgleichKennzahlenService kennzahlenService;
    @Mock
    private StatusabgleichSnapshotReadService snapshotReadService;
    @Mock
    private SensorCocService sensorCocService;
    @Mock
    DerivatStatusService derivatStatusService;
    @Mock
    SelectedDerivatFilterService selectedDerivatFilterService;

    @InjectMocks
    private ReportingStatusabgleichFacade facade;

    @Mock
    HttpServletRequest request;
    private UrlParameter urlParameter;
    @Mock
    private Derivat derivat;

    @Mock
    private StatusabgleichSnapshot snapshot;
    @Mock
    private ModulService modulService;

    @BeforeEach
    public void setUp() {
        urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("derivat", "42");
        urlParameter.addOrReplaceParamter("sensorCocFilter", "42");

        Mockito.lenient().when(selectedDerivatFilterService.getDerivatForUrlParameter(any())).thenReturn(Optional.of(derivat));
        Mockito.lenient().when(derivatService.getAllDerivate()).thenReturn(Collections.EMPTY_LIST);
        Mockito.lenient().when(snapshotReadService.getSnapshotDatesForDerivat(any())).thenReturn(Collections.EMPTY_LIST);
        when(sensorCocService.getAllSortByTechnologie()).thenReturn(Collections.EMPTY_LIST);
        Mockito.lenient().when(sensorCocService.getAllSortByTechnologie()).thenReturn(Collections.EMPTY_LIST);
        Mockito.lenient().when(modulService.getAllModule()).thenReturn(Collections.EMPTY_LIST);

    }

    @Test
    public void testGetViewDataWithoutSnapshot() throws Exception {
        facade.getViewData(urlParameter);
        verify(kennzahlenService, times(1)).getKennzahlenForDerivatWithFilter(any(), any());
        verify(snapshotReadService, never()).getById(any());
    }

    @Test
    public void testGetViewDataWithSnapshot() throws Exception {
        urlParameter.addOrReplaceParamter("snapshot", "42");
        when(snapshotReadService.getById(any())).thenReturn(Optional.of(snapshot));
        facade.getViewData(urlParameter);
        verify(kennzahlenService, never()).getKennzahlenForDerivatWithFilter(any(), any());
        verify(snapshotReadService, times(1)).getById(any());
    }

    @Test
    public void testGetViewDataWithSnapshotAndInvalidFormat() throws Exception {
        urlParameter.addOrReplaceParamter("snapshot", "A42");
        facade.getViewData(urlParameter);
        verify(kennzahlenService, times(1)).getKennzahlenForDerivatWithFilter(any(), any());
        verify(snapshotReadService, never()).getById(any());
    }

    @Test
    public void testGetViewDataWithSnapshotAndNoResult() throws Exception {
        urlParameter.addOrReplaceParamter("snapshot", "142");
        when(snapshotReadService.getById(any())).thenReturn(Optional.empty());

        facade.getViewData(urlParameter);
        verify(kennzahlenService, times(1)).getKennzahlenForDerivatWithFilter(any(), any());
        verify(snapshotReadService, times(1)).getById(any());
    }

    @Test
    public void testGetDrilldownDataValidParameter() {
        when(kennzahlenService.computeAnforderungIdWithFilterAndTablePosition(any(), any(), anyInt(), anyInt())).thenReturn(Collections.emptyList());

        facade.showAnforderungsuebersichtForPosition(
                new Derivat("42", "42"),
                new ReportingStatusabgleichFilter(urlParameter, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList()),
                1, 1);

        verify(kennzahlenService, times(1)).computeAnforderungIdWithFilterAndTablePosition(any(), any(), anyInt(), anyInt());
    }

    @Test
    public void testGetDrilldownDataWrongRowAndColumn() {
        Mockito.lenient().when(kennzahlenService.computeAnforderungIdWithFilterAndTablePosition(any(), any(), anyInt(), anyInt())).thenReturn(Collections.emptyList());

        facade.showAnforderungsuebersichtForPosition(
                new Derivat("42", "42"),
                new ReportingStatusabgleichFilter(urlParameter, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList()),
                0, 0);

        verify(kennzahlenService, never()).computeAnforderungIdWithFilterAndTablePosition(any(), any(), anyInt(), anyInt());
    }

    @Test
    public void testGetDrilldownDataWrongRow() {
        Mockito.lenient().when(kennzahlenService.computeAnforderungIdWithFilterAndTablePosition(any(), any(), anyInt(), anyInt())).thenReturn(Collections.emptyList());

        facade.showAnforderungsuebersichtForPosition(
                new Derivat("42", "42"),
                new ReportingStatusabgleichFilter(urlParameter, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList()),
                0, 1);

        verify(kennzahlenService, never()).computeAnforderungIdWithFilterAndTablePosition(any(), any(), anyInt(), anyInt());
    }

    @Test
    public void testGetDrilldownDataWithNullFilter() {
        Mockito.lenient().when(kennzahlenService.computeAnforderungIdWithFilterAndTablePosition(any(), any(), anyInt(), anyInt())).thenReturn(Collections.emptyList());

        facade.showAnforderungsuebersichtForPosition(
                new Derivat("42", "42"),
                null,
                0, 1);

        verify(kennzahlenService, never()).computeAnforderungIdWithFilterAndTablePosition(any(), any(), anyInt(), anyInt());
    }

    @Test
    public void testGetDrilldownDataWithNullDerivat() {
        Mockito.lenient().when(kennzahlenService.computeAnforderungIdWithFilterAndTablePosition(any(), any(), anyInt(), anyInt())).thenReturn(Collections.emptyList());

        facade.showAnforderungsuebersichtForPosition(
                null,
                new ReportingStatusabgleichFilter(urlParameter, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList()),
                0, 1);

        verify(kennzahlenService, never()).computeAnforderungIdWithFilterAndTablePosition(any(), any(), anyInt(), anyInt());
    }
}
