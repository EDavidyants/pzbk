package de.interfaceag.bmw.pzbk.reporting.config;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.entities.ReportingConfig;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.assertj.core.api.Java6Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ReportingConfigDaoIT extends IntegrationTest {

    ReportingConfigDao reportingConfigDao = new ReportingConfigDao();


    @BeforeEach
    void setUp() throws IllegalAccessException {
        super.begin();

        FieldUtils.writeField(reportingConfigDao, "entityManager", super.getEntityManager(), true);

        if (!isInitDataSet()) {
            reportingConfigDao.initReportingConfigData();
        }

        super.commit();
        super.begin();
    }

    @Test
    public void testUpdateAndGetConfigKeyWord() {

        reportingConfigDao.persistReportingConfig(new ReportingConfig("start_date", "22.22.2222"));

        ReportingConfig startDateFromDatabase = reportingConfigDao.getConfigByKeyword("start_date");

        Java6Assertions.assertThat(startDateFromDatabase.getValue()).contains("22.22.2222");
    }

    @Test
    public void testInvalidKeyWord() {

        ReportingConfig startDateFromDatabase = reportingConfigDao.getConfigByKeyword("hubbaBubba");

        Java6Assertions.assertThat(startDateFromDatabase).isNull();
    }

    @Test
    public void testKeywordIsNull() {

        ReportingConfig startDateFromDatabase = reportingConfigDao.getConfigByKeyword(null);

        Java6Assertions.assertThat(startDateFromDatabase).isNull();
    }


    private boolean isInitDataSet() {
        ReportingConfig startDateFromDatabase = reportingConfigDao.getConfigByKeyword("start_date");

        return startDateFromDatabase != null;
    }


}