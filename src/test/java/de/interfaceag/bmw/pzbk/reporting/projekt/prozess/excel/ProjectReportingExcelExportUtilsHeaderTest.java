package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.excel;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingExcelExportUtilsHeaderTest {

    private static final String SHEETNAME = "SHEET";

    @Mock
    Derivat derivat;

    @Mock
    ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat;

    @Test
    public void testGenerateFileName() {
        when(derivat.getName()).thenReturn("E20");
        String result = ProjectReportingExcelExportUtils.generateFilenName(derivat);
        MatcherAssert.assertThat(result, is("Reporting E20"));
    }

    @Test
    public void testGenerateExcelFileForKeyFiguresWorkbookNotNull() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Assertions.assertNotNull(result);
    }

    @Test
    public void generateExcelFileForKeyFiguresSheetNotNull() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Assertions.assertNotNull(sheet);
    }

    @Test
    public void generateExcelFileForKeyFiguresSheetName() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        String sheetName = sheet.getSheetName();
        MatcherAssert.assertThat(sheetName, is(SHEETNAME));
    }

    @Test
    public void generateExcelFileForKeyFiguresNumberOfRows() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Integer lastRowNum = sheet.getLastRowNum();
        MatcherAssert.assertThat(lastRowNum, is(1));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderRowNumberOfColumns() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Integer lastCellNum = (int) row.getLastCellNum();
        MatcherAssert.assertThat(lastCellNum, is(32));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentRowNumberOfColumns() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Integer lastCellNum = (int) row.getLastCellNum();
        MatcherAssert.assertThat(lastCellNum, is(32));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn0() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(0);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("Anforderungen freigegeben"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn1() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(1);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("Anforderungen ausgeleitet VKBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn2() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(2);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids ausgeleitet VKBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn3() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(3);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("Anforderungen ausgeleitet ZV"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn4() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(4);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids ausgeleitet ZV"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn5() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(5);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids angenommen VKBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn6() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(6);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids in Klaerung VKBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn7() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(7);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids abzustimmen VKBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn8() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(8);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids nicht bewertbar VKBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn9() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(9);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids keine Weiterverfolgung VKBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn10() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(10);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids unzureichende Anforderungsqualitaet VKBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn11() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(11);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids angenommen ZV"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn12() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(12);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids in Klaerung ZV"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn13() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(13);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids abzustimmen ZV"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn14() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(14);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids nicht bewertbar ZV"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn15() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(15);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids keine Weiterverfolgung ZV"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn16() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(16);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids unzureichende Anforderungsqualitaet ZV"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn17() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(17);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids umgesetzt VKBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn18() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(18);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids offen VKBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn19() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(19);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids Bewertung nicht moeglich VKBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn20() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(20);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids nicht umgesetzt VKBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn21() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(21);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids nicht relevant VKBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn22() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(22);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids umgesetzt VBBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn23() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(23);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids offen VBBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn24() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(24);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids Bewertung nicht moeglich VBBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn25() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(25);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids nicht umgesetzt VBBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn26() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(26);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids nicht relevant VBBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn27() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(27);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids umgesetzt BBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn28() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(28);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids offen BBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn29() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(29);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids Bewertung nicht moeglich BBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn30() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(30);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids nicht umgesetzt BBG"));
    }

    @Test
    public void generateExcelFileForKeyFiguresHeaderColumn31() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(31);
        String cellValue = cell.getStringCellValue();
        MatcherAssert.assertThat(cellValue, is("ZAK Ids nicht relevant BBG"));
    }

}
