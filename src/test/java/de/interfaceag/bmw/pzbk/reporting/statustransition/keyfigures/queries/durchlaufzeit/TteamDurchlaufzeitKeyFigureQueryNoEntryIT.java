package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTuple;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapWithSize.anEmptyMap;

class TteamDurchlaufzeitKeyFigureQueryNoEntryIT extends AbstractTteamDurchlaufzeitKeyFigureQueryIT {

    @Test
    void computeIdTypeDurchlaufzeitWithoutEntry() {
        final Map<IdTypeTuple, Long> idTypeTupleLongMap = TteamDurchlaufzeitKeyFigureQuery.computeIdTypeDurchlaufzeit(getFilter(), getEntityManager());
        assertThat(idTypeTupleLongMap, anEmptyMap());
    }
}