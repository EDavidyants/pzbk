package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.shared.dto.GenericTuple;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class StatusabgleichKennzahlenTest {

    private StatusabgleichKennzahlen kennzahlen;

    @BeforeEach
    public void setup() {
        Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> matrixValues = buildMatrixValues();
        kennzahlen = new StatusabgleichKennzahlen(matrixValues);
    }

    private static Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> buildMatrixValues() {
        Map<Tuple<DerivatAnforderungModulStatus, ZakStatus>, Long> matrixValues = new HashMap<>();
        matrixValues.put(new GenericTuple<>(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.DONE), 1L);
        matrixValues.put(new GenericTuple<>(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.PENDING), 1L);
        return matrixValues;
    }

    @Test
    public void testEmpty() {
        StatusabgleichKennzahlen empty = StatusabgleichKennzahlen.empty();
        Assertions.assertEquals(Collections.EMPTY_MAP, empty.getMatrixValues());
    }

    @Test
    public void testGetDerivatAnforderungModulStatusValue_Collection() {
        Collection<DerivatAnforderungModulStatus> status = Arrays.asList(DerivatAnforderungModulStatus.ABZUSTIMMEN);
        long result = kennzahlen.getDerivatAnforderungModulStatusValue(status);
        Assertions.assertEquals(2L, result);
    }

    @Test
    public void testGetDerivatAnforderungModulStatusValue_CollectionNoValue() {
        Collection<DerivatAnforderungModulStatus> status = Arrays.asList(DerivatAnforderungModulStatus.ANGENOMMEN);
        long result = kennzahlen.getDerivatAnforderungModulStatusValue(status);
        Assertions.assertEquals(0L, result);
    }

    @Test
    public void testGetDerivatAnforderungModulStatusValue_CollectionEmpty() {
        Collection<DerivatAnforderungModulStatus> status = Collections.EMPTY_LIST;
        long result = kennzahlen.getDerivatAnforderungModulStatusValue(status);
        Assertions.assertEquals(0L, result);
    }

    @Test
    public void testGetDerivatAnforderungModulStatusValue_DerivatAnforderungModulStatus() {
        long result = kennzahlen.getDerivatAnforderungModulStatusValue(DerivatAnforderungModulStatus.ABZUSTIMMEN);
        Assertions.assertEquals(2L, result);
    }

    @Test
    public void testGetDerivatAnforderungModulStatusValue_DerivatAnforderungModulStatusEmpty() {
        long result = kennzahlen.getDerivatAnforderungModulStatusValue(DerivatAnforderungModulStatus.ANGENOMMEN);
        Assertions.assertEquals(0L, result);
    }

    @Test
    public void testGetZakStatusValue_Collection() {
        Collection<ZakStatus> status = Arrays.asList(ZakStatus.DONE, ZakStatus.PENDING);
        long result = kennzahlen.getZakStatusValue(status);
        Assertions.assertEquals(2L, result);
    }

    @Test
    public void testGetZakStatusValue_CollectionDone() {
        Collection<ZakStatus> status = Arrays.asList(ZakStatus.DONE);
        long result = kennzahlen.getZakStatusValue(status);
        Assertions.assertEquals(1L, result);
    }

    @Test
    public void testGetZakStatusValue_CollectionEmpty() {
        Collection<ZakStatus> status = Collections.EMPTY_LIST;
        long result = kennzahlen.getZakStatusValue(status);
        Assertions.assertEquals(0L, result);
    }

    @Test
    public void testGetZakStatusValue_ZakStatus() {
        long result = kennzahlen.getZakStatusValue(ZakStatus.DONE);
        Assertions.assertEquals(1L, result);
    }

    @Test
    public void testGetValue_Collection_Collection() {
        Collection<DerivatAnforderungModulStatus> derivatAnforderungModulStatus = Arrays.asList(DerivatAnforderungModulStatus.ABZUSTIMMEN);
        Collection<ZakStatus> zakStatus = Arrays.asList(ZakStatus.DONE);
        long result = kennzahlen.getValue(derivatAnforderungModulStatus, zakStatus);
        Assertions.assertEquals(1L, result);
    }

    @Test
    public void testGetValue_Collection_Collection_NoValue() {
        Collection<DerivatAnforderungModulStatus> derivatAnforderungModulStatus = Arrays.asList(DerivatAnforderungModulStatus.ABZUSTIMMEN);
        Collection<ZakStatus> zakStatus = Arrays.asList(ZakStatus.BESTAETIGT_AUS_VORPROZESS);
        long result = kennzahlen.getValue(derivatAnforderungModulStatus, zakStatus);
        Assertions.assertEquals(0L, result);
    }

    @Test
    public void testGetValue_Collection_Collection_Empty() {
        Collection<DerivatAnforderungModulStatus> derivatAnforderungModulStatus = Collections.EMPTY_LIST;
        Collection<ZakStatus> zakStatus = Collections.EMPTY_LIST;
        long result = kennzahlen.getValue(derivatAnforderungModulStatus, zakStatus);
        Assertions.assertEquals(0L, result);
    }

    @Test
    public void testGetValue_DerivatAnforderungModulStatus_ZakStatus() {
        long result = kennzahlen.getValue(DerivatAnforderungModulStatus.ABZUSTIMMEN, ZakStatus.DONE);
        Assertions.assertEquals(1L, result);
    }

}
