package de.interfaceag.bmw.pzbk.reporting.excel;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.langlaufer.DurchlaufzeitService;
import de.interfaceag.bmw.pzbk.reporting.processoverview.ProcessOverviewKeyFigures;
import de.interfaceag.bmw.pzbk.reporting.processoverview.ProcessOverviewViewData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ReportingToExcelServiceDurchlaufzeitTest {

    @Mock
    private ProcessOverviewViewData viewData;

    @Mock
    private ProcessOverviewKeyFigures keyFigures;

    @Mock
    private KeyFigureCollection keyFiguresCollection;

    @Mock
    private KeyFigure keyFigure;

    @Mock
    private ReportToExcelDao reportToExcelDao;

    @Mock
    private ReportingFilter reportingFilter;

    @Mock
    private DurchlaufzeitService durchlaufzeitService;

    @InjectMocks
    private ReportingToExcelService reportingToExcelService;

    @BeforeEach
    public void setUp() {
        when(viewData.getProcessOverviewKeyFigures()).thenReturn(keyFigures);
        when(keyFigures.getKeyFigures()).thenReturn(keyFiguresCollection);
        when(keyFiguresCollection.getAllMeldungIds()).thenReturn(Collections.emptySet());

        List<Long> anforderungIdsList = Arrays.asList(1L, 2L);
        when(keyFiguresCollection.getAllAnforderungIds()).thenReturn(new HashSet(anforderungIdsList));

        when(reportToExcelDao.getReportingSupportDataForMeldungen(any())).thenReturn(Collections.emptyList());
    }

    @ParameterizedTest
    @EnumSource(KeyType.class)
    public void testGetDurchlaufzeitForKeyTypeAndReportingFilter(KeyType keyType) {

        when(keyFigures.getKeyFigureByTypeId(keyType.getId())).thenReturn(keyFigure);

        reportingToExcelService.getReportingDataForKeyFigure(keyType, viewData, reportingFilter);

        switch (keyType) {
            case FACHTEAM_CURRENT:
                verify(durchlaufzeitService).getFachteamDurchlaufzeiten(reportingFilter);
                break;

            case TTEAM_CURRENT:
                verify(durchlaufzeitService).getTteamDurchlaufzeiten(reportingFilter);
                break;

            case VEREINBARUNG_CURRENT:
                verify(durchlaufzeitService).getVereinbarungDurchlaufzeiten(reportingFilter);
                break;

            default:
                verify(durchlaufzeitService, never()).getFachteamDurchlaufzeiten(reportingFilter);
                verify(durchlaufzeitService, never()).getTteamDurchlaufzeiten(reportingFilter);
                verify(durchlaufzeitService, never()).getVereinbarungDurchlaufzeiten(reportingFilter);
                break;
        }

    }

}
