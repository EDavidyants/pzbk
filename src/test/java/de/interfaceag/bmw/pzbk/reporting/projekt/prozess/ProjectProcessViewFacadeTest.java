package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.excel.ProjectReportingExcelExportService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectProcessViewFacadeTest {

    @Mock
    ProjectReportingExcelExportService projectReportingExcelExportService;
    @InjectMocks
    ProjectProcessViewFacade facade;

    @Mock
    Derivat derivat;

    @Mock
    ProjectProcessFilter filter;

    @Test
    public void testDownloadExcelExport() {
        facade.downloadExcelExport(derivat, filter);
        verify(projectReportingExcelExportService, times(1)).downloadKeyFiguresAsExcelFile(derivat, filter);
    }

}
