package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingMeldungStatusTransitionZuordnungToAnforderungServiceTest {

    @Mock
    private ReportingMeldungStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Mock
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Mock
    private ReportingMeldungStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Mock
    private ReportingMeldungStatusTransitionEntryDateService entryDateService;

    @InjectMocks
    private ReportingMeldungStatusTransitionZuordnungToAnforderungService reportingMeldungStatusTransitionZuordnungToAnforderungService;

    @Mock
    private Meldung meldung;

    private Date entryDate;
    @Mock
    private Date date;
    @Mock
    private EntryExitTimeStamp exitTimeStamp;
    @Mock
    private EntryExitTimeStamp entryTimeStamp;

    private ReportingMeldungStatusTransition reportingStatusTransition;

    @BeforeEach
    void setUp() {
        reportingStatusTransition = new ReportingMeldungStatusTransition();
        entryDate = new Date();
        when(entryDateService.getEntryDateForMeldung(meldung)).thenReturn(Optional.of(entryDate));
        when(statusTransitionDatabaseAdapter.getLatest(meldung)).thenReturn(Optional.empty());
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForMeldungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);
        when(reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(any(), any())).thenReturn(entryTimeStamp);
    }

    @Test
    void addMeldungToExistingAnforderungExit() {
        final ReportingMeldungStatusTransition statusTransition = reportingMeldungStatusTransitionZuordnungToAnforderungService.addMeldungToExistingAnforderung(meldung, date);
        final EntryExitTimeStamp exit = statusTransition.getGemeldet();
        assertThat(exit, is(exitTimeStamp));
    }

    @Test
    void addMeldungToExistingAnforderungEntry() {
        final ReportingMeldungStatusTransition statusTransition = reportingMeldungStatusTransitionZuordnungToAnforderungService.addMeldungToExistingAnforderung(meldung, date);
        final EntryExitTimeStamp entry = statusTransition.getExistingAnforderungZugeordnet();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void addMeldungToNewAnforderungExit() {
        final ReportingMeldungStatusTransition statusTransition = reportingMeldungStatusTransitionZuordnungToAnforderungService.addMeldungToNewAnforderung(meldung, date);
        final EntryExitTimeStamp exit = statusTransition.getGemeldet();
        assertThat(exit, is(exitTimeStamp));
    }

    @Test
    void addMeldungToNewAnforderungEntry() {
        final ReportingMeldungStatusTransition statusTransition = reportingMeldungStatusTransitionZuordnungToAnforderungService.addMeldungToNewAnforderung(meldung, date);
        final EntryExitTimeStamp entry = statusTransition.getNewAnforderungZugeordnet();
        assertThat(entry, is(entryTimeStamp));
    }
}
