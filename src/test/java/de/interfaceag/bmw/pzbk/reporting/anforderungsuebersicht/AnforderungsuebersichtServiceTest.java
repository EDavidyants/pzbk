package de.interfaceag.bmw.pzbk.reporting.anforderungsuebersicht;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

/**
 *
 * @author evda
 */
public class AnforderungsuebersichtServiceTest {

    @Test
    public void getDatumLabelFachteam() {
        String labelActual = AnforderungsuebersichtService.getDatumLabel(KeyType.FACHTEAM_CURRENT.getId());
        Assertions.assertEquals("Datum Eingang", labelActual);
    }

    @Test
    public void getDatumLabelTteam() {
        String labelActual = AnforderungsuebersichtService.getDatumLabel(KeyType.TTEAM_CURRENT.getId());
        Assertions.assertEquals("Datum Eingang", labelActual);
    }

    @Test
    public void getDatumLabelVereinbarung() {
        String labelActual = AnforderungsuebersichtService.getDatumLabel(KeyType.VEREINBARUNG_CURRENT.getId());
        Assertions.assertEquals("Datum Eingang", labelActual);
    }

    @Test
    public void getDatumLabelFreigabe() {
        String labelActual = AnforderungsuebersichtService.getDatumLabel(KeyType.FREIGEGEBEN_CURRENT.getId());
        Assertions.assertEquals("Datum Freigabe", labelActual);
    }

    @Test
    public void getDatumLabelNeueVersion() {
        String labelActual = AnforderungsuebersichtService.getDatumLabel(KeyType.FACHTEAM_NEW_VERSION_ANFORDERUNG.getId());
        Assertions.assertEquals("Datum Neuversionierung", labelActual);
    }

    @Test
    public void getDatumLabelGeloeschtFachteam() {
        String labelActual = AnforderungsuebersichtService.getDatumLabel(KeyType.FACHTEAM_GELOESCHT.getId());
        Assertions.assertEquals("Datum gelöscht", labelActual);
    }

    @Test
    public void getDatumLabelGeloeschtTteam() {
        String labelActual = AnforderungsuebersichtService.getDatumLabel(KeyType.TTEAM_GELOESCHT.getId());
        Assertions.assertEquals("Datum gelöscht", labelActual);
    }

    @Test
    public void getDatumLabelGeloeschtVereinbarung() {
        String labelActual = AnforderungsuebersichtService.getDatumLabel(KeyType.VEREINBARUNG_GELOESCHT.getId());
        Assertions.assertEquals("Datum gelöscht", labelActual);
    }

    @Test
    public void getDatumLabelZuordnung() {
        String labelActual = AnforderungsuebersichtService.getDatumLabel(KeyType.VEREINBARUNG_PZBK.getId());
        Assertions.assertEquals("Datum PZBK Zuordnung", labelActual);
    }

}
