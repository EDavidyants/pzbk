package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingMeldungStatusTransitionZuordnungToAnforderungServiceWithoutEntryDateTest {

    @Mock
    private ReportingMeldungStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Mock
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Mock
    private ReportingMeldungStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Mock
    private ReportingMeldungStatusTransitionEntryDateService entryDateService;

    @InjectMocks
    private ReportingMeldungStatusTransitionZuordnungToAnforderungService reportingMeldungStatusTransitionZuordnungToAnforderungService;

    @Mock
    private Meldung meldung;
    @Mock
    private Date date;

    @BeforeEach
    void setUp() {
        when(entryDateService.getEntryDateForMeldung(meldung)).thenReturn(Optional.empty());
    }

    @Test
    void addMeldungToExistingAnforderung() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            reportingMeldungStatusTransitionZuordnungToAnforderungService.addMeldungToExistingAnforderung(meldung, date);
        });
    }

    @Test
    void addMeldungToNewAnforderung() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            reportingMeldungStatusTransitionZuordnungToAnforderungService.addMeldungToNewAnforderung(meldung, date);
        });
    }
}
