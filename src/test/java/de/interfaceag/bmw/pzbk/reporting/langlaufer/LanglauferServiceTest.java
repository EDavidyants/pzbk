package de.interfaceag.bmw.pzbk.reporting.langlaufer;

import de.interfaceag.bmw.pzbk.berechtigung.AnforderungBerechtigungService;
import de.interfaceag.bmw.pzbk.berechtigung.MeldungBerechtigungService;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ContextMocker;
import de.interfaceag.bmw.pzbk.reporting.config.ReportingConfigService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LanglauferServiceTest {

    @Mock
    private ReportingConfigService reportingConfigService;
    @Mock
    private AnforderungBerechtigungService anforderungBerechtigungService;
    @Mock
    private MeldungBerechtigungService meldungBerechtigungService;
    @Mock
    private DurchlaufzeitService durchlaufzeitService;
    @InjectMocks
    private LanglauferService langlauferService;

    private List<Long> langlauferIds;

    @Mock
    private DurchlaufzeitDto durchlaufzeitDto;
    private Collection<DurchlaufzeitDto> durchlaufzeiten;

    @BeforeEach
    void setUp() {
        langlauferIds = Collections.singletonList(1L);
        durchlaufzeiten = Collections.singletonList(durchlaufzeitDto);
    }

    @Test
    void getLanglauferFachTeamAnforderungIds() {
        when(durchlaufzeitService.getFachteamDurchlaufzeiten(any())).thenReturn(durchlaufzeiten);
        when(durchlaufzeitDto.isLanglaufer()).thenReturn(true);
        when(durchlaufzeitDto.getId()).thenReturn(42L);
        final Collection<Long> durchlaufzeitIds = Collections.singletonList(42L);

        when(anforderungBerechtigungService.restrictToAuthorizedAnforderungen(durchlaufzeitIds)).thenReturn(durchlaufzeitIds);
        ContextMocker.mockFacesContextForUrlParamterUtils();

        final List<Long> result = langlauferService.getFachteamLanglauferAnforderungIds();
        assertThat(result, hasSize(1));
    }

    @Test
    void getLanglauferTteamIds() {
        when(durchlaufzeitDto.isLanglaufer()).thenReturn(true);
        when(durchlaufzeitDto.getId()).thenReturn(1L);
        when(durchlaufzeitService.getTteamDurchlaufzeiten(any())).thenReturn(durchlaufzeiten);
        when(anforderungBerechtigungService.restrictToAuthorizedAnforderungen(langlauferIds)).thenReturn(langlauferIds);
        ContextMocker.mockFacesContextForUrlParamterUtils();

        final List<Long> result = langlauferService.getTteamLanglauferIds();
        assertThat(result, hasSize(1));
    }

    @Test
    void getLanglauferFachTeamMeldungIds() {
        when(durchlaufzeitService.getFachteamDurchlaufzeiten(any())).thenReturn(durchlaufzeiten);
        when(durchlaufzeitDto.isLanglaufer()).thenReturn(true);
        when(durchlaufzeitDto.getId()).thenReturn(42L);

        final Collection<Long> durchlaufzeitIds = Collections.singletonList(42L);
        when(meldungBerechtigungService.restrictToAuthorizedMeldungen(durchlaufzeitIds)).thenReturn(durchlaufzeitIds);
        ContextMocker.mockFacesContextForUrlParamterUtils();

        final List<Long> result = langlauferService.getFachteamLanglauferMeldungIds();
        assertThat(result, hasSize(1));
    }

    @Test
    void getLanglauferCountBySensorCoc() {
        when(durchlaufzeitService.getFachteamDurchlaufzeiten(any())).thenReturn(durchlaufzeiten);
        when(durchlaufzeitDto.isLanglaufer()).thenReturn(true);

        ContextMocker.mockFacesContextForUrlParamterUtils();

        SensorCoc sensorCoc = new SensorCoc();
        int result = this.langlauferService.getFachteamLanglauferCountBySensorCoc(sensorCoc);
        assertThat(result, is(2));
    }

}