package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamCurrentObjectsFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;

import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FachteamKeyFigureTest {

    private static final long SENSORCOCID = 42L;
    private static final String SENSORCOCNAME = "SENSORCOC";
    private static final int LANGLAUFER_COUNT = 1;

    @Mock
    private DurchlaufzeitKeyFigure durchlaufzeitKeyFigure;
    private KeyFigureCollection keyFigures;

    private FachteamKeyFigure fachteamKeyFigure;

    @BeforeEach
    void setUp() {
        keyFigures = new KeyFigureCollection();
        fachteamKeyFigure = new FachteamKeyFigure(SENSORCOCID, SENSORCOCNAME, keyFigures, durchlaufzeitKeyFigure);
    }

    @Test
    void getSensorCocId() {
        final long sensorCocId = fachteamKeyFigure.getSensorCocId();
        assertThat(sensorCocId, is(SENSORCOCID));
    }

    @Test
    void getSensorCocDisplayName() {
        final String sensorCocDisplayName = fachteamKeyFigure.getSensorCocDisplayName();
        assertThat(sensorCocDisplayName, is(SENSORCOCNAME));
    }

    @Test
    void getKeyFigures() {
        final Collection<KeyFigure> fachteamKeyFigureKeyFigures = fachteamKeyFigure.getKeyFigures();
        assertThat(fachteamKeyFigureKeyFigures, is(keyFigures));
    }

    @Test
    void getKeyFigureByTypeId() {
        final FachteamCurrentObjectsFigure fachteamCurrentObjectsFigure = new FachteamCurrentObjectsFigure(emptyList(), emptyList(), 0L);
        keyFigures.add(fachteamCurrentObjectsFigure);
        final KeyFigure keyFigureByType = fachteamKeyFigure.getKeyFigureByTypeId(KeyType.FACHTEAM_CURRENT.getId());
        assertThat(keyFigureByType, is(fachteamCurrentObjectsFigure));
    }

    @Test
    void getKeyFigureByType() {
        final FachteamCurrentObjectsFigure fachteamCurrentObjectsFigure = new FachteamCurrentObjectsFigure(emptyList(), emptyList(), 0L);
        keyFigures.add(fachteamCurrentObjectsFigure);
        final KeyFigure keyFigureByType = fachteamKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_CURRENT);
        assertThat(keyFigureByType, is(fachteamCurrentObjectsFigure));
    }

    @Test
    void getDurchlaufzeit() {
        Integer durchlaufzeit = 7;
        when(durchlaufzeitKeyFigure.getValue()).thenReturn(durchlaufzeit);
        final int fachteamKeyFigureDurchlaufzeit = fachteamKeyFigure.getDurchlaufzeit();
        assertThat(fachteamKeyFigureDurchlaufzeit, is(durchlaufzeit));
    }

    @Test
    void constructorForLanglaufer() {
        fachteamKeyFigure = new FachteamKeyFigure(SENSORCOCID, SENSORCOCNAME, keyFigures, durchlaufzeitKeyFigure, LANGLAUFER_COUNT);
        Assert.assertEquals(fachteamKeyFigure.getLanglauferCount(), LANGLAUFER_COUNT);
    }
}