package de.interfaceag.bmw.pzbk.reporting.statustransition.migration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * @author fn
 */
class AnforderungHistoryActionConverterTest {

    @Test
    public void convertActionCreation() {

        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Anforderung", "", "neu angelegt");
        Assertions.assertEquals(AnforderungHistoryAction.CREATION, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"a", "asdfghj", "q1234567", "aq123456", "aq1234567"})
    public void convertActionCreationWithSeverelVon(String von) {

        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Anforderung", von, "neu angelegt");
        Assertions.assertEquals(AnforderungHistoryAction.CREATION, result);
    }

    @Test
    public void convertActionCreationWithoutAuf() {

        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Anforderung", "", "");
        Assertions.assertEquals(AnforderungHistoryAction.OTHER, result);
    }

    @Test
    public void convertActionCreationWithoutObjektName() {

        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("", "", "neu angelegt");
        Assertions.assertEquals(AnforderungHistoryAction.OTHER, result);
    }

    @Test
    public void convertActionNewVersion() {

        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("neue Version von Anforderung", "", "");
        Assertions.assertEquals(AnforderungHistoryAction.NEW_VERSION, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"a", "asdfghj", "q1234567", "aq123456", "aq1234567"})
    public void convertActionNewVersionWithServerlVonAuf(String vonAuf) {

        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("neue Version von Anforderung", vonAuf, vonAuf);
        Assertions.assertEquals(AnforderungHistoryAction.NEW_VERSION, result);
    }

    @Test
    public void convertActionAddToProzessbaukasten() {

        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Zuordnung Prozessbaukasten", "zugeordnet", "");
        Assertions.assertEquals(AnforderungHistoryAction.ADD_TO_PROZESSBAUKASTEN, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"a", "asdfghj", "q1234567", "aq123456", "aq1234567"})
    public void convertActionAddToProzessbaukastenWithServerelAuf(String auf) {

        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Zuordnung Prozessbaukasten", "zugeordnet", auf);
        Assertions.assertEquals(AnforderungHistoryAction.ADD_TO_PROZESSBAUKASTEN, result);
    }

    @Test
    public void convertActionAddToProzessbaukastenWithoutVon() {

        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("", "zugeordnet", "");
        Assertions.assertEquals(AnforderungHistoryAction.OTHER, result);
    }

    @Test
    public void convertActionAddToProzessbaukastenWithoutObjektName() {

        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Zuordnung Prozessbaukasten", "", "");
        Assertions.assertEquals(AnforderungHistoryAction.OTHER, result);
    }

    @Test
    public void convertActionRemoveFromProzessbaukasten() {

        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Zuordnung Prozessbaukasten", "", "Zuordnung entfernt");
        Assertions.assertEquals(AnforderungHistoryAction.REMOVE_FROM_PROZESSBAUKASTEN, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"a", "asdfghj", "q1234567", "aq123456", "aq1234567"})
    public void convertActionRemoveFromProzessbaukastenWithServeralVon(String von) {

        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Zuordnung Prozessbaukasten", von, "Zuordnung entfernt");
        Assertions.assertEquals(AnforderungHistoryAction.REMOVE_FROM_PROZESSBAUKASTEN, result);
    }

    @Test
    public void convertActionRestore() {
        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Status", "gelöscht", "in Arbeit");
        Assertions.assertEquals(AnforderungHistoryAction.RESTORE, result);
    }

    @Test
    public void convertActionStatusChange() {

        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Status", "", "");
        Assertions.assertEquals(AnforderungHistoryAction.OTHER, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"a", "asdfghj", "q1234567", "aq123456", "aq1234567"})
    public void convertActionStatusChangeSeveralVonAuf(String vonAuf) {
        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Status", vonAuf, vonAuf);
        Assertions.assertEquals(AnforderungHistoryAction.OTHER, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "a", "asdfghj", "q1234567", "aq123456", "aq1234567"})
    public void convertActionOther(String all) {
        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert(all, all, all);
        Assertions.assertEquals(AnforderungHistoryAction.OTHER, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"in Arbeit", "abgestimmt", "plausibilisiert", "unstimmig", "freigegeben", "Keine Weiterverfolgung", "gelöscht"})
    void convertActionStatusChangeWithValidStatusAuf(String auf) {
        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Status", "in Arbeit", auf);
        Assertions.assertEquals(AnforderungHistoryAction.STATUS_CHANGE, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"in Arbeit", "abgestimmt", "plausibilisiert", "unstimmig", "freigegeben", "Keine Weiterverfolgung", "gelöscht"})
    void convertActionStatusChangeWithValidStatusAufAndEmptyStatusVon(String auf) {
        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Status", "", auf);
        Assertions.assertEquals(AnforderungHistoryAction.OTHER, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"in Arbeit", "abgestimmt", "plausibilisiert", "unstimmig", "freigegeben", "Keine Weiterverfolgung"})
    void convertActionStatusChangeWithValidStatusVon(String von) {
        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Status", von, "in Arbeit");
        Assertions.assertEquals(AnforderungHistoryAction.STATUS_CHANGE, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"in Arbeit", "abgestimmt", "plausibilisert", "unstimmig", "freigegeben", "Keine Weiterverfolgung", "gelöscht"})
    void convertActionStatusChangeWithValidStatusVonAndEmptyStatusAuf(String von) {
        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Status", von, "");
        Assertions.assertEquals(AnforderungHistoryAction.OTHER, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"freigegeben (erneut freizugeben)", "freigegeben (in Überarbeitung)"})
    void convertActionStatusChangeWithInvalidStatusAuf(String auf) {
        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Status", "in Arbeit", auf);
        Assertions.assertEquals(AnforderungHistoryAction.OTHER, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"freigegeben (erneut freizugeben)", "freigegeben (in Überarbeitung)"})
    void convertActionStatusChangeWithInvalidStatusVon(String von) {
        AnforderungHistoryAction result = AnforderungHistoryActionConverter.convert("Status", von, "in Arbeit");
        Assertions.assertEquals(AnforderungHistoryAction.OTHER, result);
    }

}
