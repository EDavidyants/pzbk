package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * @author lomu
 */
@ExtendWith(MockitoExtension.class)
public class ReportingStatusTransitionChangeMeldungStatusServiceTest {

    @Mock
    private AnforderungMeldungHistoryService historyService;
    @Mock
    private ReportingMeldungStatusTransitionCreatePort reportingMeldungStatusTransitionCreatePort;
    @Mock
    private ReportingMeldungStatusTransitionDatabaseAdapter reportingMeldungStatusTransitionDatabaseAdapter;
    @Mock
    private ReportingMeldungStatusTransitionEntryDateService entryDateService;
    @Mock
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @InjectMocks
    private ReportingStatusTransitionChangeMeldungStatusService changeMeldungStatusService;

    Status statusToRecover = Status.M_GEMELDET;

    private ReportingMeldungStatusTransition transition;
    Meldung meldung = new Meldung();

    @Mock
    private Date date;
    @Mock
    private Date entryDate;
    @Mock
    private EntryExitTimeStamp entryTimeStamp;
    @Mock
    private EntryExitTimeStamp exitTimeStamp;

    @BeforeEach
    void setUp() {
        transition = new ReportingMeldungStatusTransition();
        doNothing().when(reportingMeldungStatusTransitionDatabaseAdapter).save(any());
        when(reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(any(), any())).thenReturn(entryTimeStamp);
    }

    @Test
    void testMeldungGemeldetGemeldetEntry() {
        when(reportingMeldungStatusTransitionCreatePort.getNewStatusTransitionForMeldungWithEntryDate(any(), any())).thenReturn(transition);
        when(entryDateService.getEntryDateForMeldung(meldung)).thenReturn(Optional.of(entryDate));

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_ENTWURF, Status.M_GEMELDET, date);

        assertThat(result.getGemeldet(), is(entryTimeStamp));
    }

    @Test
    void testMeldungGemeldetToUnstimmigExit() {
        when(reportingMeldungStatusTransitionDatabaseAdapter.getLatest(any())).thenReturn(Optional.of(transition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_GEMELDET, Status.M_UNSTIMMIG, date);

        assertThat(result.getGemeldet(), is(exitTimeStamp));
    }

    @Test
    void testMeldungGemeldetToUnstimmigEntry() {
        when(reportingMeldungStatusTransitionDatabaseAdapter.getLatest(any())).thenReturn(Optional.of(transition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_GEMELDET, Status.M_UNSTIMMIG, date);

        assertThat(result.getUnstimmig(), is(entryTimeStamp));
    }

    @Test
    void testMeldungUnstimmigToGemeldetExit() {
        when(reportingMeldungStatusTransitionCreatePort.getNewStatusTransitionForMeldungWithEntryDate(any(), any())).thenReturn(transition);
        when(entryDateService.getEntryDateForMeldung(meldung)).thenReturn(Optional.of(entryDate));
        when(reportingMeldungStatusTransitionDatabaseAdapter.getLatest(any())).thenReturn(Optional.of(transition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_UNSTIMMIG, Status.M_GEMELDET, date);

        assertThat(result.getUnstimmig(), is(exitTimeStamp));
    }

    @Test
    void testMeldungUnstimmigToGemeldetEntry() {
        when(reportingMeldungStatusTransitionCreatePort.getNewStatusTransitionForMeldungWithEntryDate(any(), any())).thenReturn(transition);
        when(entryDateService.getEntryDateForMeldung(meldung)).thenReturn(Optional.of(entryDate));
        when(reportingMeldungStatusTransitionDatabaseAdapter.getLatest(any())).thenReturn(Optional.of(transition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_UNSTIMMIG, Status.M_GEMELDET, date);

        assertThat(result.getGemeldet(), is(entryTimeStamp));
    }

    @Test
    void testMeldungGemeldetToGeloeschtExit() {
        when(reportingMeldungStatusTransitionDatabaseAdapter.getLatest(any())).thenReturn(Optional.of(transition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_GEMELDET, Status.M_GELOESCHT, date);

        assertThat(result.getGemeldet(), is(exitTimeStamp));
    }

    @Test
    void testMeldungGemeldetToGeloeschtEntry() {
        when(reportingMeldungStatusTransitionDatabaseAdapter.getLatest(any())).thenReturn(Optional.of(transition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_GEMELDET, Status.M_GELOESCHT, date);

        assertThat(result.getGeloescht(), is(entryTimeStamp));
    }

    @Test
    void testMeldungUnstimmigToGeloeschtExit() {
        when(reportingMeldungStatusTransitionDatabaseAdapter.getLatest(any())).thenReturn(Optional.of(transition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_UNSTIMMIG, Status.M_GELOESCHT, date);

        assertThat(result.getUnstimmig(), is(exitTimeStamp));
    }

    @Test
    void testMeldungUnstimmigToGeloeschtEntry() {
        when(reportingMeldungStatusTransitionDatabaseAdapter.getLatest(any())).thenReturn(Optional.of(transition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_UNSTIMMIG, Status.M_GELOESCHT, date);

        assertThat(result.getGeloescht(), is(entryTimeStamp));
    }

    @Test
    void testMeldungGeloeschtToGemeldetExit() {
        when(historyService.getLastStatusByObjectnameAnforderungId(any(), any())).thenReturn(Status.M_GEMELDET);
        EntryExitTimeStamp timeStamp = new EntryExitTimeStamp();
        timeStamp.entry();
        transition.setGeloescht(timeStamp);

        when(reportingMeldungStatusTransitionCreatePort.getNewStatusTransitionForMeldungWithEntryDate(any(), any())).thenReturn(transition);
        when(entryDateService.getEntryDateForMeldung(meldung)).thenReturn(Optional.of(entryDate));
        when(reportingMeldungStatusTransitionDatabaseAdapter.getLatest(any())).thenReturn(Optional.of(transition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_GELOESCHT, Status.M_GEMELDET, date);

        assertThat(result.getGeloescht(), is(exitTimeStamp));
    }

    @Test
    void testMeldungGeloeschtToGemeldetEntry() {
        when(historyService.getLastStatusByObjectnameAnforderungId(any(), any())).thenReturn(Status.M_GEMELDET);
        EntryExitTimeStamp timeStamp = new EntryExitTimeStamp();
        timeStamp.entry();
        transition.setGeloescht(timeStamp);

        when(reportingMeldungStatusTransitionCreatePort.getNewStatusTransitionForMeldungWithEntryDate(any(), any())).thenReturn(transition);
        when(entryDateService.getEntryDateForMeldung(meldung)).thenReturn(Optional.of(entryDate));
        when(reportingMeldungStatusTransitionDatabaseAdapter.getLatest(any())).thenReturn(Optional.of(transition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_GELOESCHT, Status.M_GEMELDET, date);

        assertThat(result.getGemeldet(), is(entryTimeStamp));
    }

    @Test
    void testMeldungGeloeschtToUnstimmigExit() {
        when(historyService.getLastStatusByObjectnameAnforderungId(any(), any())).thenReturn(Status.M_UNSTIMMIG);
        EntryExitTimeStamp timeStamp = new EntryExitTimeStamp();
        timeStamp.entry();
        transition.setGeloescht(timeStamp);

        when(reportingMeldungStatusTransitionCreatePort.getNewStatusTransitionForMeldungWithEntryDate(any(), any())).thenReturn(transition);
        when(entryDateService.getEntryDateForMeldung(meldung)).thenReturn(Optional.of(entryDate));
        when(reportingMeldungStatusTransitionDatabaseAdapter.getLatest(any())).thenReturn(Optional.of(transition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_GELOESCHT, Status.M_UNSTIMMIG, date);

        assertThat(result.getGeloescht(), is(exitTimeStamp));
    }

    @Test
    void testMeldungGeloeschtToUnstimmigEntry() {
        when(historyService.getLastStatusByObjectnameAnforderungId(any(), any())).thenReturn(Status.M_UNSTIMMIG);
        EntryExitTimeStamp timeStamp = new EntryExitTimeStamp();
        timeStamp.entry();
        transition.setGeloescht(timeStamp);

        when(reportingMeldungStatusTransitionCreatePort.getNewStatusTransitionForMeldungWithEntryDate(any(), any())).thenReturn(transition);
        when(entryDateService.getEntryDateForMeldung(meldung)).thenReturn(Optional.of(entryDate));
        when(reportingMeldungStatusTransitionDatabaseAdapter.getLatest(any())).thenReturn(Optional.of(transition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_GELOESCHT, Status.M_UNSTIMMIG, date);

        assertThat(result.getUnstimmig(), is(entryTimeStamp));
    }

    @Test
    void testMeldungZugeordnetToDeleted() {
        when(reportingMeldungStatusTransitionDatabaseAdapter.getLatest(any())).thenReturn(Optional.of(transition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);

        ReportingMeldungStatusTransition result = changeMeldungStatusService.saveStatusChange(meldung, Status.M_ZUGEORDNET, Status.M_GELOESCHT, date);

        assertThat(result.getGeloescht(), is(entryTimeStamp));
    }
}
