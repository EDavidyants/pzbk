package de.interfaceag.bmw.pzbk.reporting.projekt.dashboard;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.HashSet;
import java.util.Set;

class ProjektReportingDashboardViewPermissionTest {


    private ProjektReportingDashboardViewPermission projektReportingDashboardViewPermission;


    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testPagePermission(Rolle rolle) {

        Set<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);
        projektReportingDashboardViewPermission = new ProjektReportingDashboardViewPermission(rollen);
        boolean result = projektReportingDashboardViewPermission.getPage();

        switch (rolle) {
            case ADMIN:
            case SENSORCOCLEITER:
            case T_TEAMLEITER:
            case SCL_VERTRETER:
            case TTEAM_VERTRETER:
            case ANFORDERER:
            case E_COC:
                Assertions.assertTrue(result);
                break;
            case SENSOR:
            case TTEAMMITGLIED:
            case UMSETZUNGSBESTAETIGER:
            case SENSOR_EINGESCHRAENKT:
            case LESER:
                Assertions.assertFalse(result);
        }

    }

}