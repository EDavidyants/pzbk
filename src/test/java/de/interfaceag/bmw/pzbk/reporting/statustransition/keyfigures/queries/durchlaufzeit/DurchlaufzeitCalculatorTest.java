package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit;

import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTuple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapContaining.hasKey;
import static org.hamcrest.collection.IsMapContaining.hasValue;
import static org.hamcrest.collection.IsMapWithSize.aMapWithSize;

class DurchlaufzeitCalculatorTest {

    DurchlaufzeitPart durchlaufzeitPart;
    DurchlaufzeitPart durchlaufzeitPart2;
    Collection<DurchlaufzeitPart> durchlaufzeitParts;

    @BeforeEach
    void setup() {
        durchlaufzeitParts = new ArrayList<>();

        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2019);
        calendar.set(Calendar.MONTH, 5);
        calendar.set(Calendar.DAY_OF_MONTH, 7);
        final Date end = calendar.getTime();
        calendar.set(Calendar.MONTH, 6);
        calendar.set(Calendar.DAY_OF_MONTH, 19);
        final Date start = calendar.getTime();

        durchlaufzeitPart = new DurchlaufzeitPart(42, Type.ANFORDERUNG, start, end);
        durchlaufzeitPart2 = new DurchlaufzeitPart(42, Type.ANFORDERUNG, new Date(), null);

        durchlaufzeitParts.add(durchlaufzeitPart);

    }

    @Test
    void computeDurchlaufzeit() {
        final Long result = DurchlaufzeitCalculator.computeDurchlaufzeit(durchlaufzeitParts);
        assertThat(result, is(42L));
    }

    @Test
    void computeDurchlaufzeitEmptyInput() {
        final Long result = DurchlaufzeitCalculator.computeDurchlaufzeit(Collections.emptyList());
        assertThat(result, is(0L));
    }


    @Test
    void computeDurchlaufzeitThreeCollections() {
        final Long result = DurchlaufzeitCalculator.computeDurchlaufzeit(durchlaufzeitParts, durchlaufzeitParts, durchlaufzeitParts);
        assertThat(result, is(42L));
    }

    @Test
    void computeDurchlaufzeitForActiveIdTypeTupleResultSize() {
        durchlaufzeitParts.add(durchlaufzeitPart2);
        final Map<IdTypeTuple, Long> result = DurchlaufzeitCalculator.computeDurchlaufzeitForActiveIdTypeTuple(durchlaufzeitParts);
        assertThat(result, aMapWithSize(1));
    }

    @Test
    void computeDurchlaufzeitForActiveIdTypeTupleResultKey() {
        durchlaufzeitParts.add(durchlaufzeitPart2);
        final Map<IdTypeTuple, Long> result = DurchlaufzeitCalculator.computeDurchlaufzeitForActiveIdTypeTuple(durchlaufzeitParts);
        assertThat(result, hasKey(new IdTypeTuple(Type.ANFORDERUNG, 42L)));
    }

    @Test
    void computeDurchlaufzeitForActiveIdTypeTupleResultValue() {
        durchlaufzeitParts.add(durchlaufzeitPart2);
        final Map<IdTypeTuple, Long> result = DurchlaufzeitCalculator.computeDurchlaufzeitForActiveIdTypeTuple(durchlaufzeitParts);
        assertThat(result, hasValue(42L));
    }
}