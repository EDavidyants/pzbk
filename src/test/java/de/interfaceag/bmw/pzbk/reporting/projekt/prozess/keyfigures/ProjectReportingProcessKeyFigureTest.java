package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.Optional;

/**
 *
 * @author sl
 */
public class ProjectReportingProcessKeyFigureTest {

    @ParameterizedTest
    @EnumSource(ProjectReportingProcessKeyFigure.class)
    public void testGetId(ProjectReportingProcessKeyFigure keyFigure) {
        int result = keyFigure.getId();
        int expected;
        switch (keyFigure) {
            case G1:
                expected = 0;
                break;
            case G2:
                expected = 1;
                break;
            case G3:
                expected = 2;
                break;
            case L1:
                expected = 3;
                break;
            case L2:
                expected = 4;
                break;
            case L3:
                expected = 5;
                break;
            case L4:
                expected = 6;
                break;
            case L5:
                expected = 7;
                break;
            case L6:
                expected = 8;
                break;
            case L7:
                expected = 9;
                break;
            case L8:
                expected = 10;
                break;
            case L9:
                expected = 11;
                break;
            case L10:
                expected = 12;
                break;
            case L11:
                expected = 13;
                break;
            case L12:
                expected = 14;
                break;
            case L13:
                expected = 15;
                break;
            case L14:
                expected = 16;
                break;
            case L15:
                expected = 17;
                break;
            case L16:
                expected = 18;
                break;
            case R1:
                expected = 19;
                break;
            case R2:
                expected = 20;
                break;
            case R3:
                expected = 21;
                break;
            case R4:
                expected = 22;
                break;
            case R5:
                expected = 23;
                break;
            case R6:
                expected = 24;
                break;
            case R7:
                expected = 25;
                break;
            case R8:
                expected = 26;
                break;
            case R9:
                expected = 27;
                break;
            case R10:
                expected = 28;
                break;
            case R11:
                expected = 29;
                break;
            case R12:
                expected = 30;
                break;
            case R13:
                expected = 31;
                break;
            case R14:
                expected = 32;
                break;
            case R15:
                expected = 33;
                break;
            case R16:
                expected = 34;
                break;
            case R17:
                expected = 35;
                break;
            case R18:
                expected = 36;
                break;
            case R19:
                expected = 37;
                break;
            case R20:
                expected = 38;
                break;
            case R21:
                expected = 39;
                break;
            case R22:
                expected = 40;
                break;
            case R23:
                expected = 41;
                break;
            case R24:
                expected = 42;
                break;
            case R25:
                expected = 43;
                break;
            default:
                throw new AssertionError(keyFigure.name());
        }
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testGetByValidId() {
        Optional<ProjectReportingProcessKeyFigure> result = ProjectReportingProcessKeyFigure.getById(0);
        Optional<ProjectReportingProcessKeyFigure> expected = Optional.of(ProjectReportingProcessKeyFigure.G1);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testGetByInvalidIdSmallerZero() {
        Optional<ProjectReportingProcessKeyFigure> result = ProjectReportingProcessKeyFigure.getById(-1);
        Optional<ProjectReportingProcessKeyFigure> expected = Optional.empty();
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testGetByInvalidIdTooLarge() {
        Optional<ProjectReportingProcessKeyFigure> result = ProjectReportingProcessKeyFigure.getById(44);
        Optional<ProjectReportingProcessKeyFigure> expected = Optional.empty();
        Assertions.assertEquals(expected, result);
    }

}
