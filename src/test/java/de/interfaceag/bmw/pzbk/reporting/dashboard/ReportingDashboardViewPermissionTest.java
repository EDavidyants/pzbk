package de.interfaceag.bmw.pzbk.reporting.dashboard;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ReportingDashboardViewPermissionTest {

    private ReportingDashboardViewPermission viewPermission;
    private Set<Rolle> roles;

    @BeforeEach
    void setUp() {
        roles = new HashSet<>();
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isPage(Rolle role) {
        roles.add(role);
        viewPermission = new ReportingDashboardViewPermission(roles);
        final boolean page = viewPermission.isPage();

        switch (role) {
            case ADMIN:
            case ANFORDERER:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case E_COC:
                assertTrue(page);
                break;
            default:
                assertFalse(page);
                break;
        }
    }


    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isProcessOverviewDetail(Rolle role) {
        roles.add(role);
        viewPermission = new ReportingDashboardViewPermission(roles);
        final boolean page = viewPermission.isProcessOverviewDetail();

        switch (role) {
            case ADMIN:
            case ANFORDERER:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case E_COC:
                assertTrue(page);
                break;
            default:
                assertFalse(page);
                break;
        }
    }


    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isProcessOverviewSimple(Rolle role) {
        roles.add(role);
        viewPermission = new ReportingDashboardViewPermission(roles);
        final boolean page = viewPermission.isProcessOverviewSimple();

        switch (role) {
            case ADMIN:
            case ANFORDERER:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case E_COC:
                assertTrue(page);
                break;
            default:
                assertFalse(page);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isTechnologieListView(Rolle role) {
        roles.add(role);
        viewPermission = new ReportingDashboardViewPermission(roles);
        final boolean page = viewPermission.isTechnologieListView();

        switch (role) {
            case ADMIN:
                assertTrue(page);
                break;
            default:
                assertFalse(page);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isTteamListView(Rolle role) {
        roles.add(role);
        viewPermission = new ReportingDashboardViewPermission(roles);
        final boolean page = viewPermission.isTteamListView();

        switch (role) {
            case ADMIN:
                assertTrue(page);
                break;
            default:
                assertFalse(page);
                break;
        }
    }

}