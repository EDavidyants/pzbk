package de.interfaceag.bmw.pzbk.reporting.config;

import de.interfaceag.bmw.pzbk.entities.ReportingConfig;
import org.assertj.core.api.Java6Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingConfigServiceTest {

    @InjectMocks
    private ReportingConfigService reportingConfigService;
    @Mock
    private ReportingConfigDao reportingConfigDao;

    @Test
    void getStartDate() {
        when(reportingConfigDao.getConfigByKeyword(any())).thenReturn(new ReportingConfig("start_date", "01.01.2018"));
        Date startDate = reportingConfigService.getStartDateValue();

        Java6Assertions.assertThat(startDate)
                .hasDayOfMonth(1)
                .hasMonth(1)
                .hasYear(2018);
    }

    @Test
    void getInvalidStartDate() {
        when(reportingConfigDao.getConfigByKeyword(any())).thenReturn(new ReportingConfig("start_date", "blubba"));
        Date startDate = reportingConfigService.getStartDateValue();

        Java6Assertions.assertThat(startDate).isNotNull();
    }

    @Test
    void testPersistReportingConfigValue() {
        when(reportingConfigDao.getConfigByKeyword(any())).thenReturn(new ReportingConfig("start_date", "01.01.2018" ));

        reportingConfigService.persistReportingConfigValue("22.22.2222");
    }

    @Test
    void testPersistLangTteamlauferValue() {
        when(reportingConfigDao.getConfigByKeyword(any())).thenReturn(new ReportingConfig("langlaufer_tteam", "42" ));

        reportingConfigService.persistLanglauferTteam("45");
    }

    @Test
    void testPersistLangFachteamlauferValue() {
        when(reportingConfigDao.getConfigByKeyword(any())).thenReturn(new ReportingConfig("langlaufer_fachteam", "42" ));

        reportingConfigService.persistLanglauferFachteam("45");
    }

    @Test
    void testGetLanglauferTteamThreshhold() {
        when(reportingConfigDao.getConfigByKeyword(any())).thenReturn(new ReportingConfig("langlaufer_tteam", "42" ));

        Java6Assertions.assertThat(reportingConfigService.getLanglauferTteamThreshold()).isEqualTo(42);
    }

    @Test
    void testGetLanglauferFachteamThreshhold() {
        when(reportingConfigDao.getConfigByKeyword(any())).thenReturn(new ReportingConfig("langlaufer_fachteam", "42" ));

        Java6Assertions.assertThat(reportingConfigService.getLanglauferFachteamThreshold()).isEqualTo(42);
    }

    @Test
    void testGetDefaultThreshhold() {
        when(reportingConfigDao.getConfigByKeyword(any())).thenReturn(new ReportingConfig("langlaufer_tteam", "Peter Lustig" ));
        Java6Assertions.assertThat(reportingConfigService.getLanglauferFachteamThreshold()).isEqualTo(50);
    }

    //--------------------------- Keyword Checker -------------------------------------------------------

    @Test
    void getKeywordStartDate() {
        assertThat(ReportingConfigService.KEYWORD_START_DATE, is("start_date"));
    }

    @Test
    void getKeywordLanglauferTteam() {
        assertThat(ReportingConfigService.KEYWORD_LANGLAUFER_TTEAM, is("langlaufer_tteam"));
    }

    @Test
    void getKeywordLanglauferFachteam() {
        assertThat(ReportingConfigService.KEYWORD_LANGLAUFER_FACHTEAM, is("langlaufer_fachteam"));
    }

    @Test
    void getKeywordDefaultThreshold() {
        assertThat(ReportingConfigService.DEFAULT_THRESHOLD, is(50));
    }
}