package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ContextMocker;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingFachteamViewFacadeTest {

    @Mock
    private ReportingFilterService reportingFilterService;
    @Mock
    private FachteamKeyFigureService fachteamKeyFigureService;
    @InjectMocks
    private ReportingFachteamViewFacade reportingFachteamViewFacade;

    @Mock
    private ReportingFilter filter;
    @Mock
    private FachteamKeyFigures fachteamKeyFigures;
    
    @BeforeEach
    void setUp() {
        ContextMocker.mockFacesContextForUrlParamterUtils();
        when(reportingFilterService.getReportingFilter(any(), any())).thenReturn(filter);
        when(fachteamKeyFigureService.getFachteamKeyFigures(any())).thenReturn(fachteamKeyFigures);
    }

    @Test
    void getReportingViewDataType() {
        final ReportingFachteamViewData reportingViewData = reportingFachteamViewFacade.getReportingViewData();
        assertThat(reportingViewData, instanceOf(ReportingFachteamViewData.class));
    }

    @Test
    void getReportingViewDataFilter() {
        final ReportingFachteamViewData reportingViewData = reportingFachteamViewFacade.getReportingViewData();
        final ReportingFilter reportingViewDataFilter = reportingViewData.getFilter();
        assertThat(reportingViewDataFilter, is(filter));
    }

    @Test
    void getReportingViewDataKeyFigures() {
        final ReportingFachteamViewData reportingViewData = reportingFachteamViewFacade.getReportingViewData();
        final FachteamKeyFigures reportingViewDataFachteamKeyFigures = reportingViewData.getFachteamKeyFigures();
        assertThat(reportingViewDataFachteamKeyFigures, is(fachteamKeyFigures));
    }
}