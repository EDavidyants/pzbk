package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.excel;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingExcelExportUtilsContentNotValueTest {

    private static final String SHEETNAME = "SHEET";

    @Mock
    private ProjectReportingProcessKeyFigureDataCollection keyFiguresForDerivat;

    @Test
    public void testGenerateExcelFileForKeyFiguresContentColumn0() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(0);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn1() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(1);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn2() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(2);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn3() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(3);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn4() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(4);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn5() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(5);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn6() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(6);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn7() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(7);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn8() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(8);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn9() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(9);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn10() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(10);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn11() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(11);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn12() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(12);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn13() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(13);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn14() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(14);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn15() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(15);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn16() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(16);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn17() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(17);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn18() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(18);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn19() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(19);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn20() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(20);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn21() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(21);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn22() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(22);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn23() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(23);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn24() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(24);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn25() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(25);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn26() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(26);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn27() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(27);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn28() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(28);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn29() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(29);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn30() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(30);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

    @Test
    public void generateExcelFileForKeyFiguresContentColumn31() {
        Workbook result = ProjectReportingExcelExportUtils.generateExcelFileForKeyFigures(keyFiguresForDerivat, SHEETNAME);
        Sheet sheet = result.getSheet(SHEETNAME);
        Row row = sheet.getRow(1);
        Cell cell = row.getCell(31);
        Double cellValue = cell.getNumericCellValue();
        MatcherAssert.assertThat(cellValue, is(0.0));
    }

}
