package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessAmpelThresholdService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsMapContaining;
import org.hamcrest.collection.IsMapWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl, evda
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingUmsetzungsverwaltungKeyFigureServiceBBGTest {

    private static final Long VEREINBARUNG_BBG_ANGENOMMEN = 50L;

    @Mock
    ProjectReportingUmsetzungsverwaltungKeyFigureDao umsetzungsverwaltungKeyFigureDao;

    @Mock
    ProjectProcessAmpelThresholdService ampelThresholdService;

    @InjectMocks
    ProjectReportingUmsetzungsverwaltungKeyFigureService umsetzungsverwaltungKeyFigureService;

    @Mock
    Derivat derivat;

    @Mock
    ProjectProcessFilter filter;

    List<Object[]> queryResults;
    List<Object[]> queryResultsOffen;

    @BeforeEach
    public void setUp() {
        Object[] queryResult1 = new Object[2];
        queryResult1[0] = UmsetzungsBestaetigungStatus.OFFEN.getId();
        queryResult1[1] = 1L;

        Object[] queryResult2 = new Object[2];
        queryResult2[0] = UmsetzungsBestaetigungStatus.UMGESETZT.getId();
        queryResult2[1] = 2L;

        Object[] queryResult3 = new Object[2];
        queryResult3[0] = UmsetzungsBestaetigungStatus.BEWERTUNG_NICHT_MOEGLICH.getId();
        queryResult3[1] = 3L;

        Object[] queryResult4 = new Object[2];
        queryResult4[0] = UmsetzungsBestaetigungStatus.NICHT_UMGESETZT.getId();
        queryResult4[1] = 4L;

        Object[] queryResult5 = new Object[2];
        queryResult5[0] = UmsetzungsBestaetigungStatus.NICHT_RELEVANT.getId();
        queryResult5[1] = 5L;

        queryResults = new ArrayList<>();
        queryResults.add(queryResult1);
        queryResults.add(queryResult2);
        queryResults.add(queryResult3);
        queryResults.add(queryResult4);
        queryResults.add(queryResult5);

        when(umsetzungsverwaltungKeyFigureDao.getQueryDataForUmsetzungsverwaltung(any(), any(), any(ProjectProcessFilter.class))).thenReturn(queryResults);
        when(ampelThresholdService.getThresholdForUmsetzungsverwaltungBBG()).thenReturn(0.95);
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterResultSize() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapWithSize.aMapWithSize(7));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterContainsR20() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R20.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterR20Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R20.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(4L));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterContainsL10() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R19.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterL10Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R19.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(45L));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterContainsOffen() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R22.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterOffenValue() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R22.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(36L));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterContainsUmsegesetzt() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R21.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterUmgesetztValue() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R21.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(2L));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterContainsBewertungNichtMoeglich() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R23.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterBewertungNichtMoeglichValue() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R23.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(3L));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterContainsNichtUmgesetzt() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R24.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterNichtUmgesetztValue() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R24.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(4L));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterContainsNichtRelevant() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R25.getId()));
    }

    @Test
    public void testGetKeyFiguresForUmsetzungsverwaltungBBGWithFilterNichtRelevantValue() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = umsetzungsverwaltungKeyFigureService.getKeyFiguresForUmsetzungsverwaltungBBG(derivat, VEREINBARUNG_BBG_ANGENOMMEN, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R25.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(5L));
    }

}
