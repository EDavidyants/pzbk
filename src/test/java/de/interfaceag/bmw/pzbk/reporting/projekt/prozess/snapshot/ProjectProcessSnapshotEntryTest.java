package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;

class ProjectProcessSnapshotEntryTest {

    private static final long ONE = 1L;
    private ProjectProcessSnapshotEntry snapshotEntry;

    @BeforeEach
    void setup() {
        snapshotEntry = new ProjectProcessSnapshotEntry();
    }

    @Test
    void getId() {
        final Long id = snapshotEntry.getId();
        MatcherAssert.assertThat(id, CoreMatchers.is(CoreMatchers.nullValue()));
    }

    @Test
    void setId() {
        snapshotEntry.setId(ONE);
        final Long id = snapshotEntry.getId();
        MatcherAssert.assertThat(id, CoreMatchers.is(ONE));
    }

    @Test
    void getKeyFigure() {
        final ProjectReportingProcessKeyFigure keyFigure = snapshotEntry.getKeyFigure();
        MatcherAssert.assertThat(keyFigure, CoreMatchers.is(ProjectReportingProcessKeyFigure.G1));
    }

    @Test
    void setKeyFigure() {
        snapshotEntry.setKeyFigure(ProjectReportingProcessKeyFigure.L4);
        final ProjectReportingProcessKeyFigure keyFigure = snapshotEntry.getKeyFigure();
        MatcherAssert.assertThat(keyFigure, CoreMatchers.is(ProjectReportingProcessKeyFigure.L4));
    }

    @Test
    void getValue() {
        final int value = snapshotEntry.getValue();
        MatcherAssert.assertThat(value, CoreMatchers.is(0));
    }

    @Test
    void setValue() {
        snapshotEntry.setValue(1);
        final int value = snapshotEntry.getValue();
        MatcherAssert.assertThat(value, CoreMatchers.is(1));
    }

    @Test
    void constructorValue() {
        snapshotEntry = new ProjectProcessSnapshotEntry(ProjectReportingProcessKeyFigure.L4, 2);
        final int value = snapshotEntry.getValue();
        MatcherAssert.assertThat(value, CoreMatchers.is(2));
    }

    @Test
    void constructorKeyFigure() {
        snapshotEntry = new ProjectProcessSnapshotEntry(ProjectReportingProcessKeyFigure.L4, 2);
        final ProjectReportingProcessKeyFigure keyFigure = snapshotEntry.getKeyFigure();
        MatcherAssert.assertThat(keyFigure , CoreMatchers.is(ProjectReportingProcessKeyFigure.L4));
    }

    @Test
    void testToString() {
        final int value = 1;
        final long id = 1L;
        final ProjectReportingProcessKeyFigure keyFigure = ProjectReportingProcessKeyFigure.L4;

        snapshotEntry.setId(id);
        snapshotEntry.setValue(1);
        snapshotEntry.setKeyFigure(keyFigure);

        final String expected =  "ProjectProcessSnapshotEntry{" +
                "id=" + id +
                ", keyFigure=" + keyFigure.getId() +
                ", value=" + value +
                '}';

        final String result = snapshotEntry.toString();
        MatcherAssert.assertThat(result, is(expected));
    }

}