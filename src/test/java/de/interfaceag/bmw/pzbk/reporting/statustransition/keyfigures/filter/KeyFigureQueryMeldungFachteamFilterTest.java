package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapWithSize.aMapWithSize;
import static org.hamcrest.collection.IsMapWithSize.anEmptyMap;
import static org.hamcrest.text.IsEmptyString.emptyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class KeyFigureQueryMeldungFachteamFilterTest {

    @Mock
    private ReportingFilter filter;
    @Mock
    private IdSearchFilter sensorCocFilter;

    private QueryPart queryPart;

    @BeforeEach
    void setUp() {
        queryPart = new QueryPartDTO();
        when(filter.getSensorCocFilter()).thenReturn(sensorCocFilter);
    }

    private static void setupActive(IdSearchFilter tteamFilter) {
        when(tteamFilter.isActive()).thenReturn(Boolean.TRUE);

        Set<Long> ids = new HashSet<>();
        ids.add(1L);
        when(tteamFilter.getSelectedIdsAsSet()).thenReturn(ids);
    }

    private void setupInactive() {
        when(sensorCocFilter.isActive()).thenReturn(Boolean.FALSE);
    }

    @Test
    void appendNotActive() {
        setupInactive();
        KeyFigureQueryMeldungFachteamFilter.append(filter, queryPart);
        final String query = queryPart.getQuery();
        assertThat(query, is(emptyString()));
    }

    @Test
    void appendNotActiveParameterMap() {
        setupInactive();
        KeyFigureQueryMeldungFachteamFilter.append(filter, queryPart);
        final Map<String, Object> parameterMap = queryPart.getParameterMap();
        assertThat(parameterMap, is(anEmptyMap()));
    }

    @Test
    void appendActive() {
        setupActive(sensorCocFilter);
        KeyFigureQueryMeldungFachteamFilter.append(filter, queryPart);
        final String query = queryPart.getQuery();
        assertThat(query, is(" AND m.sensorCoc.sensorCocId IN :sensorCocIds "));
    }


    @Test
    void appendActiveParameterMap() {
        setupActive(sensorCocFilter);
        KeyFigureQueryMeldungFachteamFilter.append(filter, queryPart);
        final Map<String, Object> parameterMap = queryPart.getParameterMap();
        assertThat(parameterMap, is(aMapWithSize(1)));
    }

}