package de.interfaceag.bmw.pzbk.reporting.statustransition.meldung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingMeldungStatusTransitionZuordnungToAnforderungAufhebenServiceTest {

    @Mock
    private ReportingMeldungStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Mock
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Mock
    private ReportingMeldungStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Mock
    private ReportingMeldungStatusTransitionEntryDateService entryDateService;
    @InjectMocks
    private ReportingMeldungStatusTransitionZuordnungToAnforderungAufhebenService reportingMeldungStatusTransitionZuordnungToAnforderungAufhebenService;

    @Mock
    private Anforderung anforderung;
    @Mock
    private Meldung meldung;

    private Date entryDate;
    @Mock
    private Date date;
    @Mock
    private EntryExitTimeStamp entryTimeStamp;

    @Mock
    private EntryExitTimeStamp existingAnforderungZugeordnet;
    @Mock
    private EntryExitTimeStamp newAnforderungZugeordnet;

    private ReportingMeldungStatusTransition reportingStatusTransition;

    @BeforeEach
    void setUp() {
        reportingStatusTransition = new ReportingMeldungStatusTransition();
        entryDate = new Date();
        when(entryDateService.getEntryDateForMeldung(meldung)).thenReturn(Optional.of(entryDate));
        when(statusTransitionDatabaseAdapter.getLatest(meldung)).thenReturn(Optional.empty());
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForMeldungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(any(), any())).thenReturn(entryTimeStamp);
    }

    @Test
    void removeMeldungFromAnforderungExitExistingAnforderungZugeordnetExit() {
        reportingStatusTransition.setExistingAnforderungZugeordnet(existingAnforderungZugeordnet);
        when(existingAnforderungZugeordnet.isActive()).thenReturn(Boolean.TRUE);

        reportingMeldungStatusTransitionZuordnungToAnforderungAufhebenService.removeMeldungFromAnforderung(meldung, anforderung, date);

        verify(reportingStatusTransitionTimeStampUpdatePort, times(1)).updateExitForTimeStamp(existingAnforderungZugeordnet, date);
    }

    @Test
    void removeMeldungFromAnforderungExitNewAnforderungZugeordnet() {
        reportingStatusTransition.setNewAnforderungZugeordnet(newAnforderungZugeordnet);
        when(newAnforderungZugeordnet.isActive()).thenReturn(Boolean.TRUE);

        reportingMeldungStatusTransitionZuordnungToAnforderungAufhebenService.removeMeldungFromAnforderung(meldung, anforderung, date);

        verify(reportingStatusTransitionTimeStampUpdatePort, times(1)).updateExitForTimeStamp(newAnforderungZugeordnet, date);
    }

    @Test
    void removeMeldungFromAnforderungEntry() {
        final ReportingMeldungStatusTransition statusTransition = reportingMeldungStatusTransitionZuordnungToAnforderungAufhebenService.removeMeldungFromAnforderung(meldung, anforderung, date);
        final EntryExitTimeStamp entry = statusTransition.getGemeldet();
        verify(statusTransitionDatabaseAdapter, times(2)).save(any());
        assertThat(entry, is(entryTimeStamp));
    }
}
