package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTransitionCreateAnforderungAsNewVersionServiceTest {

    @Mock
    private ReportingStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Mock
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Mock
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Mock
    private ReportingStatusTransitionAnforderungEntyDateService entyDateService;

    @InjectMocks
    private ReportingStatusTransitionCreateAnforderungAsNewVersionService createAnforderungAsNewVersionService;

    @Mock
    private Anforderung anforderung;

    private ReportingStatusTransition reportingStatusTransition;

    @Mock
    private EntryExitTimeStamp entryTimeStamp;

    private Date entryDate;

    @Mock
    private Date date;

    @BeforeEach
    void setUp() {
        reportingStatusTransition = new ReportingStatusTransition();
        entryDate = new Date();
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(any(), any())).thenReturn(entryTimeStamp);
    }

    @Test
    void createAnforderungAsNewVersionEntry() {
        final ReportingStatusTransition statusTransition = createAnforderungAsNewVersionService.createAnforderungAsNewVersion(anforderung, date);
        final EntryExitTimeStamp entry = statusTransition.getInArbeit();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void createAnforderungAsNewVersionNewVersionTrue() {
        final ReportingStatusTransition statusTransition = createAnforderungAsNewVersionService.createAnforderungAsNewVersion(anforderung, date);
        final boolean newVersion = statusTransition.isNewVersion();
        assertTrue(newVersion);
    }
}
