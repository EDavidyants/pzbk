package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.filter.DateSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.ReportingStatusTransitionDao;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;

import java.util.Date;

abstract class AbstractTteamDurchlaufzeitKeyFigureQueryIT extends IntegrationTest {

    private ReportingStatusTransitionDao reportingStatusTransitionDao = new ReportingStatusTransitionDao();
    private AnforderungDao anforderungDao = new AnforderungDao();

    private ReportingFilter filter;

    @BeforeEach
    void setUp() throws IllegalAccessException {

        UrlParameter urlParameter = new UrlParameter();
        filter = new ReportingFilter(urlParameter);

        final DateSearchFilter bisDateFilter = filter.getBisDateFilter();
        bisDateFilter.setSelected(new Date());

        FieldUtils.writeField(reportingStatusTransitionDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(anforderungDao, "entityManager", super.getEntityManager(), true);

        super.begin();
        getEntityManager().createNativeQuery("DELETE FROM reportingstatustransition").executeUpdate();
        super.commit();
        super.begin();

    }

    public ReportingStatusTransitionDao getReportingStatusTransitionDao() {
        return reportingStatusTransitionDao;
    }

    public AnforderungDao getAnforderungDao() {
        return anforderungDao;
    }

    public ReportingFilter getFilter() {
        return filter;
    }
}