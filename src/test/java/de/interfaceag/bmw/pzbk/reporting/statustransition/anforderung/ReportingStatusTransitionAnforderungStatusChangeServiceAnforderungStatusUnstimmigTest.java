package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTransitionAnforderungStatusChangeServiceAnforderungStatusUnstimmigTest {

    @Mock
    private ReportingStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Mock
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Mock
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Mock
    private ReportingStatusTransitionAnforderungEntyDateService entyDateService;

    @InjectMocks
    private ReportingStatusTransitionAnforderungStatusChangeService reportingStatusTransitionAnforderungStatusChangeService;

    @Mock
    private Anforderung anforderung;

    private ReportingStatusTransition reportingStatusTransition;

    private ReportingStatusTransition newReportingStatusTransition;

    @Mock
    private EntryExitTimeStamp exitTimeStamp;
    @Mock
    private EntryExitTimeStamp entryTimeStamp;
    @Mock
    private Date date;

    private Date entryDate;

    @BeforeEach
    void setUp() {
        reportingStatusTransition = new ReportingStatusTransition();
        newReportingStatusTransition = new ReportingStatusTransition();
        entryDate = new Date();
        when(statusTransitionDatabaseAdapter.getLatestForAnforderung(anforderung)).thenReturn(Optional.of(reportingStatusTransition));
        when(reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(any(), any())).thenReturn(entryTimeStamp);
    }

    @Test
    void changeAnforderungStatusUnstimmigToAbgestimmtExitWithFachteamActiveNewRow() {
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(newReportingStatusTransition);
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);
        reportingStatusTransition.setFachteamUnstimmig(exitTimeStamp);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_UNSTIMMIG, Status.A_FTABGESTIMMT, date);
        assertThat(statusTransition, is(newReportingStatusTransition));
    }

    @Test
    void changeAnforderungStatusUnstimmigToAbgestimmtExitWithFachteamActiveTimeStamp() {
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(newReportingStatusTransition);
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);
        reportingStatusTransition.setFachteamUnstimmig(exitTimeStamp);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_UNSTIMMIG, Status.A_FTABGESTIMMT, date);
        final EntryExitTimeStamp exit = reportingStatusTransition.getFachteamUnstimmig();
        assertThat(exit, is(exitTimeStamp));
    }

    @Test
    void changeAnforderungStatusUnstimmigToAbgestimmtEntryWithFachteamActiveTimeStamp() {
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(newReportingStatusTransition);
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);
        reportingStatusTransition.setFachteamUnstimmig(exitTimeStamp);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_UNSTIMMIG, Status.A_FTABGESTIMMT, date);
        final EntryExitTimeStamp entry = statusTransition.getAbgestimmt();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void changeAnforderungStatusUnstimmigToGeloeschtExitWithFachteamActive() {
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);
        reportingStatusTransition.setFachteamUnstimmig(exitTimeStamp);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_UNSTIMMIG, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp exit = statusTransition.getFachteamUnstimmig();
        assertThat(exit, is(exitTimeStamp));
    }

    @Test
    void changeAnforderungStatusUnstimmigToGeloeschtEntryWithFachteamActive() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_UNSTIMMIG, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp entry = statusTransition.getFachteamGeloescht();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void changeAnforderungStatusUnstimmigToGeloeschtExitWithTteamActive() {
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);
        reportingStatusTransition.setTteamUnstimmig(exitTimeStamp);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_UNSTIMMIG, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp exit = statusTransition.getTteamUnstimmig();
        assertThat(exit, is(exitTimeStamp));
    }

    @Test
    void changeAnforderungStatusUnstimmigToGeloeschtEntryWithTteamActive() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_UNSTIMMIG, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp entry = statusTransition.getFachteamGeloescht();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void changeAnforderungStatusUnstimmigToGeloeschtExitWithVereinbarungActive() {
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);
        reportingStatusTransition.setVereinbarungUnstimmig(exitTimeStamp);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_UNSTIMMIG, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp exit = statusTransition.getVereinbarungUnstimmig();
        assertThat(exit, is(exitTimeStamp));
    }

    @Test
    void changeAnforderungStatusUnstimmigToGeloeschtEntryWithVereinbarungActive() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_UNSTIMMIG, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp entry = statusTransition.getFachteamGeloescht();
        assertThat(entry, is(entryTimeStamp));
    }
}
