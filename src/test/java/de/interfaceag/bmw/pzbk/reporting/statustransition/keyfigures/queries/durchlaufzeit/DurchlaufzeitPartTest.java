package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit;

import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTuple;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DurchlaufzeitPartTest {

    private static final Date NOW = new Date();
    public static final long DAYS = 172800000L;

    private DurchlaufzeitPart durchlaufzeitPart;
    private DurchlaufzeitPart anotherDurchlaufzeitPart;

    @Test
    void getEntryExitDifferenceBothNull() {
        durchlaufzeitPart = new DurchlaufzeitPart(42L, null);
        final Long difference = durchlaufzeitPart.getEntryExitDifference();
        assertThat(difference, is(0L));
    }

    @Test
    void getEntryExitDifference() {
        Date now = new Date();

        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(now.getTime() - DAYS);

        durchlaufzeitPart = new DurchlaufzeitPart(42L, calendar.getTime());

        final Long difference = durchlaufzeitPart.getEntryExitDifference();
        final Long anotherDiff = difference - DAYS;
        final double value = anotherDiff.doubleValue();
        assertThat(value, is(closeTo(0, 100.0)));
    }

    @Test
    void notEqualsType() {
        durchlaufzeitPart = new DurchlaufzeitPart(42L, NOW);
        durchlaufzeitPart.setAnforderung();
        anotherDurchlaufzeitPart = new DurchlaufzeitPart(42L, NOW);
        anotherDurchlaufzeitPart.setMeldung();
        final boolean result = durchlaufzeitPart.equals(anotherDurchlaufzeitPart);
        assertFalse(result);
    }

    @Test
    void notEqualsId() {
        durchlaufzeitPart = new DurchlaufzeitPart(42L, NOW);
        durchlaufzeitPart.setAnforderung();
        anotherDurchlaufzeitPart = new DurchlaufzeitPart(44L, NOW);
        anotherDurchlaufzeitPart.setAnforderung();
        final boolean result = durchlaufzeitPart.equals(anotherDurchlaufzeitPart);
        assertFalse(result);
    }

    @Test
    void equalsIdAndType() {
        durchlaufzeitPart = new DurchlaufzeitPart(42L, NOW);
        durchlaufzeitPart.setAnforderung();
        anotherDurchlaufzeitPart = new DurchlaufzeitPart(42L, NOW);
        anotherDurchlaufzeitPart.setAnforderung();
        final boolean result = durchlaufzeitPart.equals(anotherDurchlaufzeitPart);
        assertTrue(result);
    }

    @Test
    void getId() {
        durchlaufzeitPart = new DurchlaufzeitPart(42L, NOW);
        final long id = durchlaufzeitPart.getId();
        assertThat(id, is(42L));
    }

    @Test
    void getType() {
        durchlaufzeitPart = new DurchlaufzeitPart(42L, NOW);
        durchlaufzeitPart.setAnforderung();
        final Type type = durchlaufzeitPart.getType();
        assertThat(type, is(Type.ANFORDERUNG));
    }

    @Test
    void getEntry() {
        durchlaufzeitPart = new DurchlaufzeitPart(42L, NOW);
        final Date entry = durchlaufzeitPart.getEntry();
        assertThat(entry, is(NOW));
    }

    @Test
    void getExit() {
        durchlaufzeitPart = new DurchlaufzeitPart(42L, NOW);
        final Date exit = durchlaufzeitPart.getExit();
        assertThat(exit, nullValue());
    }

    @Test
    void getIdTypeTuple() {
        durchlaufzeitPart = new DurchlaufzeitPart(42L, NOW);
        durchlaufzeitPart.setAnforderung();
        final IdTypeTuple idTypeTuple = durchlaufzeitPart.getIdTypeTuple();
        assertThat(idTypeTuple, is(new IdTypeTuple(Type.ANFORDERUNG, 42L)));
    }

    @Test
    void isActive() {
        durchlaufzeitPart = new DurchlaufzeitPart(42L, NOW);
        final boolean active = durchlaufzeitPart.isActive();
        assertTrue(active);
    }
}