package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamDurchlaufzeitKeyFigure;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class TteamDurchlaufzeitKeyFigureQueryKeyFigureNoEntryIT extends AbstractTteamDurchlaufzeitKeyFigureQueryIT {

    @Test
    void computeIdTypeDurchlaufzeitWithoutEntry() {
        final TteamDurchlaufzeitKeyFigure keyFigure = TteamDurchlaufzeitKeyFigureQuery.compute(getFilter(), getEntityManager());
        assertThat(keyFigure.getValue(), is(0));
    }
}