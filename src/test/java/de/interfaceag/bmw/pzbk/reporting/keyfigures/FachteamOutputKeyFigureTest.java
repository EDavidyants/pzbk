package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachteamOutputKeyFigureTest {

    private final Collection<Long> idsAbgestimmt = Arrays.asList(1L, 2L);
    private final Collection<Long> idsGeloescht = Arrays.asList(2L, 3L, 4L);
    private final Collection<Long> idsProzessbaukasten = Arrays.asList(5L, 6L, 7L);
    private final Collection<Long> idsExistingToAnforderung = Arrays.asList(8L, 9L, 10L);
    private final Collection<Long> meldungIds = Arrays.asList(1L, 2L);

    private FachteamAnforderungAbgestimmtKeyFigure fachteamAnforderungAbgestimmtKeyFigure;
    private FachteamGeloeschteObjekteKeyFigure fachteamGeloeschteObjekteKeyFigure;
    private FachteamProzessbaukastenKeyFigure fachteamProzessbaukastenKeyFigure;
    private FachteamMeldungExistingAnforderungKeyFigure fachteamMeldungExistingAnforderungKeyFigurel;

    @Before
    public void setUp() {
        initKeyFigures(idsAbgestimmt, idsGeloescht, meldungIds, idsProzessbaukasten, idsExistingToAnforderung);
    }

    private void initKeyFigures(Collection<Long> idsAbgestimmt, Collection<Long> idsGeloescht, Collection<Long> meldungIdsGeloescht, Collection<Long> idsProzessbaukasten, Collection<Long> idsExistingToAnforderung) {
        fachteamAnforderungAbgestimmtKeyFigure = new FachteamAnforderungAbgestimmtKeyFigure(idsAbgestimmt, 0L);
        fachteamGeloeschteObjekteKeyFigure = new FachteamGeloeschteObjekteKeyFigure(idsGeloescht, meldungIdsGeloescht, 0L);
        fachteamProzessbaukastenKeyFigure = new FachteamProzessbaukastenKeyFigure(idsProzessbaukasten);
        fachteamMeldungExistingAnforderungKeyFigurel = new FachteamMeldungExistingAnforderungKeyFigure(idsExistingToAnforderung);

    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresAllParamtersNull() throws InvalidDataException {
        FachteamOutputKeyFigure.buildFromKeyFigures(null, null, null, null);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresFirstParamterNull() throws InvalidDataException {
        FachteamOutputKeyFigure.buildFromKeyFigures(null, fachteamGeloeschteObjekteKeyFigure, fachteamProzessbaukastenKeyFigure, fachteamMeldungExistingAnforderungKeyFigurel);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresSecondParamterNull() throws InvalidDataException {
        FachteamOutputKeyFigure.buildFromKeyFigures(fachteamAnforderungAbgestimmtKeyFigure, null, fachteamProzessbaukastenKeyFigure, fachteamMeldungExistingAnforderungKeyFigurel);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresThirdParamterNull() throws InvalidDataException {
        FachteamOutputKeyFigure.buildFromKeyFigures(fachteamGeloeschteObjekteKeyFigure, fachteamGeloeschteObjekteKeyFigure, null, fachteamMeldungExistingAnforderungKeyFigurel);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresFourthParamterNull() throws InvalidDataException {
        FachteamOutputKeyFigure.buildFromKeyFigures(fachteamGeloeschteObjekteKeyFigure, fachteamGeloeschteObjekteKeyFigure, fachteamProzessbaukastenKeyFigure, null);
    }

    @Test
    public void testBuildFromKeyFiguresKeyTypeId() throws InvalidDataException {
        FachteamOutputKeyFigure result = FachteamOutputKeyFigure.buildFromKeyFigures(fachteamAnforderungAbgestimmtKeyFigure, fachteamGeloeschteObjekteKeyFigure, fachteamProzessbaukastenKeyFigure, fachteamMeldungExistingAnforderungKeyFigurel);
        assertEquals(KeyType.FACHTEAM_OUTPUT.getId(), result.getKeyTypeId());
    }

    @Test
    public void testBuildFromKeyFiguresCollectionSize() throws InvalidDataException {
        FachteamOutputKeyFigure result = FachteamOutputKeyFigure.buildFromKeyFigures(fachteamAnforderungAbgestimmtKeyFigure, fachteamGeloeschteObjekteKeyFigure, fachteamProzessbaukastenKeyFigure, fachteamMeldungExistingAnforderungKeyFigurel);
        assertEquals(13, result.getIdTypeTuples().size());
    }

    @Test
    public void testBuildFromKeyFiguresEmptyCollection() throws InvalidDataException {
        FachteamAnforderungAbgestimmtKeyFigure keyFigureWithEmptyCollection = new FachteamAnforderungAbgestimmtKeyFigure(Collections.EMPTY_LIST, 0L);
        FachteamOutputKeyFigure result = FachteamOutputKeyFigure.buildFromKeyFigures(keyFigureWithEmptyCollection, fachteamGeloeschteObjekteKeyFigure, fachteamProzessbaukastenKeyFigure, fachteamMeldungExistingAnforderungKeyFigurel);
        assertEquals(11, result.getIdTypeTuples().size());
    }

    @Test
    public void testBuildFromKeyFiguresRuntimeNotNull() throws InvalidDataException {
        FachteamOutputKeyFigure result = FachteamOutputKeyFigure.buildFromKeyFigures(fachteamAnforderungAbgestimmtKeyFigure, fachteamGeloeschteObjekteKeyFigure, fachteamProzessbaukastenKeyFigure, fachteamMeldungExistingAnforderungKeyFigurel);
        assertNotNull(result.getRunTime());
    }

    @Test
    public void testBuildFromKeyFiguresRuntime() throws InvalidDataException {
        FachteamOutputKeyFigure result = FachteamOutputKeyFigure.buildFromKeyFigures(fachteamAnforderungAbgestimmtKeyFigure, fachteamGeloeschteObjekteKeyFigure, fachteamProzessbaukastenKeyFigure, fachteamMeldungExistingAnforderungKeyFigurel);
        assertEquals(0L, (long) result.getRunTime());
    }

}
