package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class SumKeyFigureUtilsTest {

    @Mock
    ProjectReportingProcessKeyFigureData keyFigureData1;
    @Mock
    ProjectReportingProcessKeyFigureData keyFigureData2;
    @Mock
    ProjectReportingProcessKeyFigureData keyFigureData3;
    @Mock
    ProjectReportingProcessKeyFigureData keyFigureData4;
    @Mock
    ProjectReportingProcessKeyFigureData keyFigureData5;

    ProjectReportingProcessKeyFigure keyFigure;

    private final Double thresholdForGreenAmpel = 0.85;

    Collection<ProjectReportingProcessKeyFigureData> keyFigureDataCollection1;
    Collection<ProjectReportingProcessKeyFigureData> keyFigureDataCollection2;
    Collection<ProjectReportingProcessKeyFigureData> keyFigureDataCollection3;

    @BeforeEach
    public void setUp() {
        keyFigure = ProjectReportingProcessKeyFigure.G1;

        keyFigureDataCollection1 = Arrays.asList(keyFigureData1, keyFigureData2);
        keyFigureDataCollection2 = Arrays.asList(keyFigureData3, keyFigureData4);
        keyFigureDataCollection3 = Arrays.asList(keyFigureData3, keyFigureData5);

    }

    private void mockCollection1() {
        when(keyFigureData1.getValue()).thenReturn(1L);
        when(keyFigureData2.getValue()).thenReturn(2L);
    }

    private void mockCollection2() {
        when(keyFigureData3.getValue()).thenReturn(3L);
        when(keyFigureData4.getValue()).thenReturn(4L);
    }

    private void mockCollection3() {
        when(keyFigureData3.getValue()).thenReturn(3L);
        when(keyFigureData5.getValue()).thenReturn(3L);
    }

    @Test
    public void testComputeSumKeyFigureKeyFigure() {
        mockCollection1();
        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeSumKeyFigure(keyFigure, keyFigureDataCollection1);
        MatcherAssert.assertThat(keyFigure, is(result.getKeyFigure()));
    }

    @Test
    public void testComputeSumKeyFigureKeyFigureEmpty() {
        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeSumKeyFigure(keyFigure, Collections.emptyList());
        MatcherAssert.assertThat(keyFigure, is(result.getKeyFigure()));
    }

    @Test
    public void testComputeSumKeyFigureSum() {
        mockCollection1();
        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeSumKeyFigure(keyFigure, keyFigureDataCollection1);
        MatcherAssert.assertThat(3L, is(result.getValue()));
    }

    @Test
    public void testComputeRatioKeyFigureKeyFigure() {
        mockCollection1();
        mockCollection2();
        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeRatioKeyFigure(keyFigure, keyFigureDataCollection1, keyFigureDataCollection2, thresholdForGreenAmpel);
        MatcherAssert.assertThat(keyFigure, is(result.getKeyFigure()));
    }

    @Test
    public void testComputeRatioKeyFigureRatioForEmptyCollection() {
        mockCollection1();
        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeRatioKeyFigure(keyFigure, keyFigureDataCollection1, Collections.emptyList(), thresholdForGreenAmpel);
        MatcherAssert.assertThat(100L, is(result.getValue()));
    }

    @Test
    public void testComputeRatioKeyFigureRatio() {
        mockCollection1();
        mockCollection2();
        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeRatioKeyFigure(keyFigure, keyFigureDataCollection1, keyFigureDataCollection2, thresholdForGreenAmpel);
        // 1 + 2 / 3 + 4 = 0.428
        MatcherAssert.assertThat(43L, is(result.getValue()));
    }

    @Test
    public void testComputeRatioKeyFigureRatioWithRedAmpel() {
        mockCollection1();
        mockCollection2();
        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeRatioKeyFigure(keyFigure, keyFigureDataCollection1, keyFigureDataCollection2, thresholdForGreenAmpel);
        // 1 + 2 / 3 + 4 = 0.428
        Assertions.assertEquals(result.isAmpelGreen(), false);
    }

    @Test
    public void testComputeRatioKeyFigureRatioWithGreenAmpel() {
        mockCollection3();
        mockCollection2();
        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeRatioKeyFigure(keyFigure, keyFigureDataCollection3, keyFigureDataCollection2, thresholdForGreenAmpel);
        // 3 + 3 / 3 + 4 = 0.857
        Assertions.assertEquals(result.isAmpelGreen(), true);
    }

    @Test
    public void testComputeDifferenceKeyFigureNegative() {
        mockCollection2();
        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeDifferenceKeyFigure(keyFigure, 0L, keyFigureDataCollection2);
        MatcherAssert.assertThat(0L, is(result.getValue()));
    }

    @Test
    public void testComputeDifferenceKeyFigure() {
        mockCollection2();
        ProjectReportingProcessKeyFigureData result = SumKeyFigureUtils.computeDifferenceKeyFigure(keyFigure, 14L, keyFigureDataCollection2);
        MatcherAssert.assertThat(7L, is(result.getValue()));
    }
}
