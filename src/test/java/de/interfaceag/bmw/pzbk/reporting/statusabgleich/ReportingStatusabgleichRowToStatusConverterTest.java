package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ReportingStatusabgleichRowToStatusConverterTest {

    @Test
    public void testConvertToDerivatAnforderungModulStatusRow0() {
        Collection<DerivatAnforderungModulStatus> result = ReportingStatusabgleichRowToStatusConverter.convertToDerivatAnforderungModulStatus(0);
        Assertions.assertEquals(Collections.EMPTY_SET, result);
    }

    @Test
    public void testConvertToDerivatAnforderungModulStatusRow1() {
        Collection<DerivatAnforderungModulStatus> result = ReportingStatusabgleichRowToStatusConverter.convertToDerivatAnforderungModulStatus(1);
        Assertions.assertEquals(Arrays.asList(DerivatAnforderungModulStatus.ANGENOMMEN), result);
    }

    @Test
    public void testConvertToDerivatAnforderungModulStatusRow2() {
        Collection<DerivatAnforderungModulStatus> result = ReportingStatusabgleichRowToStatusConverter.convertToDerivatAnforderungModulStatus(2);
        Assertions.assertEquals(Arrays.asList(DerivatAnforderungModulStatus.IN_KLAERUNG), result);
    }

    @Test
    public void testConvertToDerivatAnforderungModulStatusRow3() {
        Collection<DerivatAnforderungModulStatus> result = ReportingStatusabgleichRowToStatusConverter.convertToDerivatAnforderungModulStatus(3);
        Assertions.assertEquals(Arrays.asList(DerivatAnforderungModulStatus.ABZUSTIMMEN), result);
    }

    @Test
    public void testConvertToDerivatAnforderungModulStatusRow4() {
        Collection<DerivatAnforderungModulStatus> result = ReportingStatusabgleichRowToStatusConverter.convertToDerivatAnforderungModulStatus(4);
        Assertions.assertEquals(Arrays.asList(DerivatAnforderungModulStatus.NICHT_BEWERTBAR), result);
    }

    @Test
    public void testConvertToDerivatAnforderungModulStatusRow5() {
        Collection<DerivatAnforderungModulStatus> result = ReportingStatusabgleichRowToStatusConverter.convertToDerivatAnforderungModulStatus(5);
        Assertions.assertEquals(Arrays.asList(DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG), result);
    }

    @Test
    public void testConvertToDerivatAnforderungModulStatusRow6() {
        Collection<DerivatAnforderungModulStatus> result = ReportingStatusabgleichRowToStatusConverter.convertToDerivatAnforderungModulStatus(6);
        Assertions.assertEquals(Arrays.asList(DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET), result);
    }

    @Test
    public void testConvertToDerivatAnforderungModulStatusRow7() {
        Collection<DerivatAnforderungModulStatus> result = ReportingStatusabgleichRowToStatusConverter.convertToDerivatAnforderungModulStatus(7);
        Assertions.assertEquals(Collections.EMPTY_SET, result);
    }

}
