package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class FachteamLanglauferKeyFigureSumCalculatorTest {

    private Collection<FachteamKeyFigure> keyFigures;

    @BeforeEach
    void setUp() {
        keyFigures = new ArrayList<>();
    }


    @Test
    void getLanglauferSumWithNoEntry() {
        final int langlauferSum = FachteamLanglauferKeyFigureSumCalculator.getLanglauferSum(keyFigures);
        assertThat(langlauferSum, is(0));
    }

    @Test
    void getLanglauferSumWithOneEntry() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1));
        final int langlauferSum = FachteamLanglauferKeyFigureSumCalculator.getLanglauferSum(keyFigures);
        assertThat(langlauferSum, is(1));
    }

    @Test
    void getLanglauferSumWithTwoEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3));
        final int langlauferSum = FachteamLanglauferKeyFigureSumCalculator.getLanglauferSum(keyFigures);
        assertThat(langlauferSum, is(4));
    }

    @Test
    void getLanglauferSumWithThreeEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(42));
        final int langlauferSum = FachteamLanglauferKeyFigureSumCalculator.getLanglauferSum(keyFigures);
        assertThat(langlauferSum, is(46));
    }

    private FachteamKeyFigure getFachteamKeyFigureWithDurchlaufzeit(int langlauferCount) {
        final KeyFigureCollection keyFigureCollection = new KeyFigureCollection();
        DurchlaufzeitKeyFigure durchlaufzeitKeyFigure = getDurchlaufzeitKeyFigureWithValue(1);
        return new FachteamKeyFigure(1L, "SensorCoc1", keyFigureCollection, durchlaufzeitKeyFigure, langlauferCount);
    }

    private DurchlaufzeitKeyFigure getDurchlaufzeitKeyFigureWithValue(int value) {
        return new DurchlaufzeitKeyFigure() {
            @Override
            public KeyType getKeyType() {
                return null;
            }

            @Override
            public int getKeyTypeId() {
                return 0;
            }

            @Override
            public int getValue() {
                return value;
            }
        };
    }

}