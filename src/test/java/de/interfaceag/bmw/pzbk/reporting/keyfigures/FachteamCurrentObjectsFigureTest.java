package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsIterableContaining.hasItems;

class FachteamCurrentObjectsFigureTest {

    private FachteamCurrentObjectsFigure keyFigure;

    private boolean langlaufer = true;
    private Collection<Long> meldungIds;
    private Collection<Long> anforderungIds;

    @BeforeEach
    void setUp() {
        meldungIds = asList(1L, 42L);
        anforderungIds = asList(2L, 42L);
        keyFigure = new FachteamCurrentObjectsFigure(meldungIds, anforderungIds, langlaufer);
    }

    @Test
    void isLanglaufer() {
        final boolean keyFigureLanglaufer = keyFigure.isLanglaufer();
        assertThat(keyFigureLanglaufer, is(langlaufer));
    }

    @Test
    void getAnforderungIds() {
        final Collection<Long> keyFigureAnforderungIds = keyFigure.getAnforderungIds();
        assertThat(keyFigureAnforderungIds, hasItems(2L, 42L));
    }

    @Test
    void getMeldungIds() {
        final Collection<Long> keyFigureMeldungIds = keyFigure.getMeldungIds();
        assertThat(keyFigureMeldungIds, hasItems(1L, 42L));
    }

}