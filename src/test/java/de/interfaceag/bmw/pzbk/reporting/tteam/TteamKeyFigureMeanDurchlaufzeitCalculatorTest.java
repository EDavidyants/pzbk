package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class TteamKeyFigureMeanDurchlaufzeitCalculatorTest {

    Collection<TteamKeyFigure> keyFigures;

    @BeforeEach
    void setUp() {
        keyFigures = new ArrayList<>();
    }

    @Test
    void computeMeanTteamDurchlaufzeitNoEntry() {
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanTteamDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(0));
    }

    @Test
    void computeMeanTteamDurchlaufzeitOneEntry() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanTteamDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(1));
    }

    @Test
    void computeMeanTteamDurchlaufzeitTwoEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3, 4444));
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanTteamDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(2));
    }

    @Test
    void computeMeanTteamDurchlaufzeitThreeEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3, 4));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(12, 13));
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanTteamDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(5));
    }

    @Test
    void computeMeanTteamDurchlaufzeitFourEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3, 4));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(12, 13));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(42, 42));
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanTteamDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(15));
    }

    @Test
    void computeMeanTteamDurchlaufzeitFiveEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3, 4));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(12, 13));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(42, 43));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(222, 223));
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanTteamDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(56));
    }

    @Test
    void computeMeanVereinbarungDurchlaufzeitNoEntry() {
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanVereinbarungDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(0));
    }

    @Test
    void computeMeanVereinbarungDurchlaufzeitOneEntry() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanVereinbarungDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(2));
    }

    @Test
    void computeMeanVereinbarungDurchlaufzeitTwoEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3, 4));
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanVereinbarungDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(3));
    }

    @Test
    void computeMeanVereinbarungDurchlaufzeitThreeEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3, 4));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(12, 13));
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanVereinbarungDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(6));
    }

    @Test
    void computeMeanVereinbarungDurchlaufzeitFourEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3, 4));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(12, 13));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(42, 42));
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanVereinbarungDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(15));
    }

    @Test
    void computeMeanVereinbarungDurchlaufzeitFiveEntries() {
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(1, 2));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(3, 4));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(12, 13));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(42, 43));
        keyFigures.add(getFachteamKeyFigureWithDurchlaufzeit(222, 223));
        final int durchlaufzeit = TteamKeyFigureMeanDurchlaufzeitCalculator.computeMeanVereinbarungDurchlaufzeit(keyFigures);
        assertThat(durchlaufzeit, is(57));
    }


    private TteamKeyFigure getFachteamKeyFigureWithDurchlaufzeit(int tteamDurchlaufzeit, int vereinbarungDurchlaufzeit) {
        final KeyFigureCollection keyFigureCollection = new KeyFigureCollection();
        DurchlaufzeitKeyFigure tteamDurchlaufzeitKeyFigure = getDurchlaufzeitKeyFigureWithValue(tteamDurchlaufzeit);
        DurchlaufzeitKeyFigure vereinbarungDurchlaufzeitKeyFigure = getDurchlaufzeitKeyFigureWithValue(vereinbarungDurchlaufzeit);
        return new TteamKeyFigure(1L, "SensorCoc1", keyFigureCollection, tteamDurchlaufzeitKeyFigure, vereinbarungDurchlaufzeitKeyFigure, 0, 0);
    }

    private DurchlaufzeitKeyFigure getDurchlaufzeitKeyFigureWithValue(int value) {
        return new DurchlaufzeitKeyFigure() {
            @Override
            public KeyType getKeyType() {
                return null;
            }

            @Override
            public int getKeyTypeId() {
                return 0;
            }

            @Override
            public int getValue() {
                return value;
            }
        };
    }
}