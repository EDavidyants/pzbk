package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.validation;

import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureConstraint;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureValidationResult;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingConstraintFactoryTest {

    @Mock
    LocalizationService localizationService;

    @InjectMocks
    ProjectReportingConstraintFactory factory;

    @Mock
    private ProjectReportingProcessKeyFigureDataCollection keyFigureDataCollection;

    @Mock
    ProjectReportingProcessKeyFigureData keyFigureData1;
    @Mock
    ProjectReportingProcessKeyFigureData keyFigureData2;

    @Test
    public void testGetProjectReportingConstraintG3eqL1L2() throws Exception {
        setupG3eqL1L2();

        when(keyFigureData1.getValue()).thenReturn(2L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintG3eqL1L2(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void testGetProjectReportingConstraintG3eqL1L2Invalid() throws Exception {
        setupG3eqL1L2();
        setupKeyFigureDataKeyFigureValue();

        when(keyFigureData1.getValue()).thenReturn(1L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintG3eqL1L2(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(false));
    }

    @Test
    public void testGetProjectReportingConstraintG3eqL4L5L6L7L8L9() throws Exception {
        setupG3eqL4L5L6L7L8L9();

        when(keyFigureData1.getValue()).thenReturn(6L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintG3eqL4L5L6L7L8L9(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void testGetProjectReportingConstraintG3eqL4L5L6L7L8L9Invalid() throws Exception {
        setupG3eqL4L5L6L7L8L9();
        setupKeyFigureDataKeyFigureValue();

        when(keyFigureData1.getValue()).thenReturn(6L);
        when(keyFigureData2.getValue()).thenReturn(2L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintG3eqL4L5L6L7L8L9(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(false));
    }

    @Test
    public void testGetProjectReportingConstraintL1eqL4L5L6L7() throws Exception {
        setupL1eqL4L5L6L7();

        when(keyFigureData1.getValue()).thenReturn(4L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintL1eqL4L5L6L7(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void testGetProjectReportingConstraintL1eqL4L5L6L7Invalid() throws Exception {
        setupL1eqL4L5L6L7();
        setupKeyFigureDataKeyFigureValue();

        when(keyFigureData1.getValue()).thenReturn(3L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintL1eqL4L5L6L7(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(false));
    }

    @Test
    public void testGetProjectReportingConstraintL2eqL8L9() throws Exception {
        setupL2eqL8L9();

        when(keyFigureData1.getValue()).thenReturn(2L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintL2eqL8L9(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void testGetProjectReportingConstraintL2eqL8L9Invalid() throws Exception {
        setupL2eqL8L9();
        setupKeyFigureDataKeyFigureValue();

        when(keyFigureData1.getValue()).thenReturn(1L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintL2eqL8L9(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(false));
    }

    @Test
    public void testGetProjectReportingConstraintL10eqL12L13L14L15() throws Exception {
        setupL10eqL12L13L14L15();

        when(keyFigureData1.getValue()).thenReturn(4L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintL10eqL12L13L14L15(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void testGetProjectReportingConstraintL10eqL12L13L14L15Invalid() throws Exception {
        setupL10eqL12L13L14L15();
        setupKeyFigureDataKeyFigureValue();

        when(keyFigureData1.getValue()).thenReturn(2L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintL10eqL12L13L14L15(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(false));
    }

    @Test
    public void testGetProjectReportingConstraintR2G3eqR3R4() throws Exception {
        setupR2G3eqR3R4();

        when(keyFigureData1.getValue()).thenReturn(1L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintR2G3eqR3R4(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void testGetProjectReportingConstraintR2G3eqR3R4Invalid() throws Exception {
        setupR2G3eqR3R4();
        setupKeyFigureDataKeyFigureValue();

        when(keyFigureData1.getValue()).thenReturn(2L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintR2G3eqR3R4(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(false));
    }

    @Test
    public void testGetProjectReportingConstraintR3eqR6R7R8R9() throws Exception {
        setupR3eqR6R7R8R9();

        when(keyFigureData1.getValue()).thenReturn(4L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintR3eqR6R7R8R9(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void testGetProjectReportingConstraintR3eqR6R7R8R9Invalid() throws Exception {
        setupR3eqR6R7R8R9();
        setupKeyFigureDataKeyFigureValue();

        when(keyFigureData1.getValue()).thenReturn(3L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintR3eqR6R7R8R9(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(false));
    }

    @Test
    public void testGetProjectReportingConstraintR4eqR10R11() throws Exception {
        setupR4eqR10R11();

        when(keyFigureData1.getValue()).thenReturn(2L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintR4eqR10R11(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void testGetProjectReportingConstraintR4eqR10R11Invalid() throws Exception {
        setupR4eqR10R11();
        setupKeyFigureDataKeyFigureValue();

        when(keyFigureData1.getValue()).thenReturn(1L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintR4eqR10R11(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(false));
    }

    @Test
    public void testGetProjectReportingConstraintR12eqR14R15R16R17() throws Exception {
        setupR12eqR14R15R16R17();

        when(keyFigureData1.getValue()).thenReturn(4L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintR12eqR14R15R16R17(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void testGetProjectReportingConstraintR12eqR14R15R16R17Invalid() throws Exception {
        setupR12eqR14R15R16R17();
        setupKeyFigureDataKeyFigureValue();

        when(keyFigureData1.getValue()).thenReturn(3L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintR12eqR14R15R16R17(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(false));
    }

    @Test
    public void testGetProjectReportingConstraintR19eqR21R22R23R24() throws Exception {
        setupR19eqR21R22R23R24();

        when(keyFigureData1.getValue()).thenReturn(4L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintR19eqR21R22R23R24(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void testGetProjectReportingConstraintR19eqR21R22R23R24Invalid() throws Exception {
        setupR19eqR21R22R23R24();
        setupKeyFigureDataKeyFigureValue();

        when(keyFigureData1.getValue()).thenReturn(3L);
        when(keyFigureData2.getValue()).thenReturn(1L);

        KeyFigureConstraint result = factory.getProjectReportingConstraintR19eqR21R22R23R24(keyFigureDataCollection);
        KeyFigureValidationResult validationResult = result.validate();
        MatcherAssert.assertThat(validationResult.isValid(), is(false));
    }

    private void setupKeyFigureDataKeyFigureValue() {
        when(keyFigureData1.getKeyFigure()).thenReturn(ProjectReportingProcessKeyFigure.G3);
        when(keyFigureData2.getKeyFigure()).thenReturn(ProjectReportingProcessKeyFigure.L1);
    }

    private void setupG3eqL1L2() {
        when(keyFigureDataCollection.getDataForKeyFigure(any(ProjectReportingProcessKeyFigure.class)))
                .thenAnswer(invocation -> {
                    ProjectReportingProcessKeyFigure argument = (ProjectReportingProcessKeyFigure) invocation.getArgument(0);
                    switch (argument) {
                        case G3:
                            return Optional.of(keyFigureData1);
                        case L1:
                        case L2:
                            return Optional.of(keyFigureData2);
                        default:
                            return Optional.empty();
                    }
                });
    }

    private void setupG3eqL4L5L6L7L8L9() {
        when(keyFigureDataCollection.getDataForKeyFigure(any(ProjectReportingProcessKeyFigure.class)))
                .thenAnswer(invocation -> {
                    ProjectReportingProcessKeyFigure argument = (ProjectReportingProcessKeyFigure) invocation.getArgument(0);
                    switch (argument) {
                        case G3:
                            return Optional.of(keyFigureData1);
                        case L4:
                        case L5:
                        case L6:
                        case L7:
                        case L8:
                        case L9:
                            return Optional.of(keyFigureData2);
                        default:
                            return Optional.empty();
                    }
                });
    }

    private void setupL1eqL4L5L6L7() {
        when(keyFigureDataCollection.getDataForKeyFigure(any(ProjectReportingProcessKeyFigure.class)))
                .thenAnswer(invocation -> {
                    ProjectReportingProcessKeyFigure argument = (ProjectReportingProcessKeyFigure) invocation.getArgument(0);
                    switch (argument) {
                        case L1:
                            return Optional.of(keyFigureData1);
                        case L4:
                        case L5:
                        case L6:
                        case L7:
                            return Optional.of(keyFigureData2);
                        default:
                            return Optional.empty();
                    }
                });
    }

    private void setupL2eqL8L9() {
        when(keyFigureDataCollection.getDataForKeyFigure(any(ProjectReportingProcessKeyFigure.class)))
                .thenAnswer(invocation -> {
                    ProjectReportingProcessKeyFigure argument = (ProjectReportingProcessKeyFigure) invocation.getArgument(0);
                    switch (argument) {
                        case L2:
                            return Optional.of(keyFigureData1);
                        case L8:
                        case L9:
                            return Optional.of(keyFigureData2);
                        default:
                            return Optional.empty();
                    }
                });
    }

    private void setupL10eqL12L13L14L15() {
        when(keyFigureDataCollection.getDataForKeyFigure(any(ProjectReportingProcessKeyFigure.class)))
                .thenAnswer(invocation -> {
                    ProjectReportingProcessKeyFigure argument = (ProjectReportingProcessKeyFigure) invocation.getArgument(0);
                    switch (argument) {
                        case L10:
                            return Optional.of(keyFigureData1);
                        case L12:
                        case L13:
                        case L14:
                        case L15:
                            return Optional.of(keyFigureData2);
                        default:
                            return Optional.empty();
                    }
                });
    }

    private void setupR2G3eqR3R4() {
        when(keyFigureDataCollection.getDataForKeyFigure(any(ProjectReportingProcessKeyFigure.class)))
                .thenAnswer(invocation -> {
                    ProjectReportingProcessKeyFigure argument = (ProjectReportingProcessKeyFigure) invocation.getArgument(0);
                    switch (argument) {
                        case R2:
                        case G3:
                            return Optional.of(keyFigureData1);
                        case R3:
                        case R4:
                            return Optional.of(keyFigureData2);
                        default:
                            return Optional.empty();
                    }
                });
    }

    private void setupR3eqR6R7R8R9() {
        when(keyFigureDataCollection.getDataForKeyFigure(any(ProjectReportingProcessKeyFigure.class)))
                .thenAnswer(invocation -> {
                    ProjectReportingProcessKeyFigure argument = (ProjectReportingProcessKeyFigure) invocation.getArgument(0);
                    switch (argument) {
                        case R3:
                            return Optional.of(keyFigureData1);
                        case R6:
                        case R7:
                        case R8:
                        case R9:
                            return Optional.of(keyFigureData2);
                        default:
                            return Optional.empty();
                    }
                });
    }

    private void setupR4eqR10R11() {
        when(keyFigureDataCollection.getDataForKeyFigure(any(ProjectReportingProcessKeyFigure.class)))
                .thenAnswer(invocation -> {
                    ProjectReportingProcessKeyFigure argument = (ProjectReportingProcessKeyFigure) invocation.getArgument(0);
                    switch (argument) {
                        case R4:
                            return Optional.of(keyFigureData1);
                        case R10:
                        case R11:
                            return Optional.of(keyFigureData2);
                        default:
                            return Optional.empty();
                    }
                });
    }

    private void setupR12eqR14R15R16R17() {
        when(keyFigureDataCollection.getDataForKeyFigure(any(ProjectReportingProcessKeyFigure.class)))
                .thenAnswer(invocation -> {
                    ProjectReportingProcessKeyFigure argument = (ProjectReportingProcessKeyFigure) invocation.getArgument(0);
                    switch (argument) {
                        case R12:
                            return Optional.of(keyFigureData1);
                        case R14:
                        case R15:
                        case R16:
                        case R17:
                            return Optional.of(keyFigureData2);
                        default:
                            return Optional.empty();
                    }
                });
    }

    private void setupR19eqR21R22R23R24() {
        when(keyFigureDataCollection.getDataForKeyFigure(any(ProjectReportingProcessKeyFigure.class)))
                .thenAnswer(invocation -> {
                    ProjectReportingProcessKeyFigure argument = (ProjectReportingProcessKeyFigure) invocation.getArgument(0);
                    switch (argument) {
                        case R19:
                            return Optional.of(keyFigureData1);
                        case R21:
                        case R22:
                        case R23:
                        case R24:
                            return Optional.of(keyFigureData2);
                        default:
                            return Optional.empty();
                    }
                });
    }
}
