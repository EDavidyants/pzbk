package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class TteamOutputKeyFigureTest {

    private final Collection<Long> idsPlausibilisiert = Arrays.asList(1L, 2L);
    private final Collection<Long> idsGeloescht = Arrays.asList(2L, 3L, 4L);
    private final Collection<Long> idsUnstimmig = Arrays.asList(4L, 5L, 6L);
    private final Collection<Long> idsProzessbaukasten = Arrays.asList(7L, 8L, 9L);

    private TteamPlausibilisiertKeyFigure tteamPlausibilisiertKeyFigure;
    private TteamGeloeschtKeyFigure tteamGeloeschtKeyFigure;
    private TteamUnstimmigKeyFigure tteamUnstimmigKeyFigure;
    private TteamProzessbaukastenKeyFigure tteamProzessbaukastenKeyFigure;

    @Before
    public void setUp() {
        initKeyFigures(idsPlausibilisiert, idsGeloescht, idsUnstimmig, idsProzessbaukasten);
    }

    private void initKeyFigures(Collection<Long> idsPlausibilisiert, Collection<Long> idsGeloescht, Collection<Long> idsUnstimmig, Collection<Long> idsProzessbaukasten) {
        tteamPlausibilisiertKeyFigure = new TteamPlausibilisiertKeyFigure(idsPlausibilisiert, 0L);
        tteamGeloeschtKeyFigure = new TteamGeloeschtKeyFigure(idsGeloescht, 0L);
        tteamUnstimmigKeyFigure = new TteamUnstimmigKeyFigure(idsUnstimmig, 0L);
        tteamProzessbaukastenKeyFigure = new TteamProzessbaukastenKeyFigure(idsProzessbaukasten);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresAllParamtersNull() throws InvalidDataException {
        TteamOutputKeyFigure.buildFromKeyFigures(null, null, null, null);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresFirstParamterNull() throws InvalidDataException {
        TteamOutputKeyFigure.buildFromKeyFigures(null, tteamGeloeschtKeyFigure, tteamUnstimmigKeyFigure, tteamProzessbaukastenKeyFigure);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresSecondParamterNull() throws InvalidDataException {
        TteamOutputKeyFigure.buildFromKeyFigures(tteamPlausibilisiertKeyFigure, null, tteamUnstimmigKeyFigure, tteamProzessbaukastenKeyFigure);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresThirdParamterNull() throws InvalidDataException {
        TteamOutputKeyFigure.buildFromKeyFigures(tteamPlausibilisiertKeyFigure, tteamGeloeschtKeyFigure, null, tteamProzessbaukastenKeyFigure);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresFourthParamterNull() throws InvalidDataException {
        TteamOutputKeyFigure.buildFromKeyFigures(tteamPlausibilisiertKeyFigure, tteamGeloeschtKeyFigure, tteamUnstimmigKeyFigure, null);
    }

    @Test
    public void testBuildFromKeyFiguresKeyTypeId() throws InvalidDataException {
        TteamOutputKeyFigure result = TteamOutputKeyFigure.buildFromKeyFigures(tteamPlausibilisiertKeyFigure, tteamGeloeschtKeyFigure, tteamUnstimmigKeyFigure, tteamProzessbaukastenKeyFigure);
        assertEquals(KeyType.TTEAM_OUTPUT.getId(), result.getKeyTypeId());
    }

    @Test
    public void testBuildFromKeyFiguresCollectionSize() throws InvalidDataException {
        TteamOutputKeyFigure result = TteamOutputKeyFigure.buildFromKeyFigures(tteamPlausibilisiertKeyFigure, tteamGeloeschtKeyFigure, tteamUnstimmigKeyFigure, tteamProzessbaukastenKeyFigure);
        assertEquals(11, result.getIdTypeTuples().size());
    }

    @Test
    public void testBuildFromKeyFiguresEmptyCollection() throws InvalidDataException {
        TteamPlausibilisiertKeyFigure keyFigureWithEmptyCollection = new TteamPlausibilisiertKeyFigure(Collections.EMPTY_LIST, 0L);
        TteamOutputKeyFigure result = TteamOutputKeyFigure.buildFromKeyFigures(keyFigureWithEmptyCollection, tteamGeloeschtKeyFigure, tteamUnstimmigKeyFigure, tteamProzessbaukastenKeyFigure);
        assertEquals(9, result.getIdTypeTuples().size());
    }

    @Test
    public void testBuildFromKeyFiguresRuntimeNotNull() throws InvalidDataException {
        TteamOutputKeyFigure result = TteamOutputKeyFigure.buildFromKeyFigures(tteamPlausibilisiertKeyFigure, tteamGeloeschtKeyFigure, tteamUnstimmigKeyFigure, tteamProzessbaukastenKeyFigure);
        assertNotNull(result.getRunTime());
    }

    @Test
    public void testBuildFromKeyFiguresRuntime() throws InvalidDataException {
        TteamOutputKeyFigure result = TteamOutputKeyFigure.buildFromKeyFigures(tteamPlausibilisiertKeyFigure, tteamGeloeschtKeyFigure, tteamUnstimmigKeyFigure, tteamProzessbaukastenKeyFigure);
        assertEquals(0L, (long) result.getRunTime());
    }

}
