package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.meldung.ReportingMeldungStatusTransitionEntryDateService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTransitionAnforderungEntyDateServiceTest {

    @Mock
    private ReportingMeldungStatusTransitionEntryDateService reportingMeldungStatusTransitionEntryDateService;
    @InjectMocks
    private ReportingStatusTransitionAnforderungEntyDateService reportingStatusTransitionAnforderungEntyDateService;

    @Mock
    private Anforderung anforderung;

    @Mock
    private Date erstellungsdatum;

    private Set<Meldung> meldungen;

    @Mock
    private Meldung meldung1;
    @Mock
    private Meldung meldung2;

    @Mock
    private Date entryDate;
    @Mock
    private Date entryDate2;

    @BeforeEach
    void setup() {
        meldungen = new HashSet<>();
        meldungen.add(meldung1);
    }

    @Test
    void getEntryDateForAnforderungWithNoMeldung() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            reportingStatusTransitionAnforderungEntyDateService.getEntryDateForAnforderung(anforderung);
        });
    }

    @Test
    void getEntryDateForAnforderungWithOneMeldungAndNoEntryDate() {
        when(anforderung.getMeldungen()).thenReturn(meldungen);
        when(reportingMeldungStatusTransitionEntryDateService.getEntryDateForMeldung(any())).thenReturn(Optional.empty());
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            reportingStatusTransitionAnforderungEntyDateService.getEntryDateForAnforderung(anforderung);
        });
    }

    @Test
    void getEntryDateForAnforderungWithHigherVersion() {
        when(anforderung.getVersion()).thenReturn(2);
        when(anforderung.getMeldungen()).thenReturn(meldungen);
        when(anforderung.getErstellungsdatum()).thenReturn(erstellungsdatum);
        final Date entryDate = reportingStatusTransitionAnforderungEntyDateService.getEntryDateForAnforderung(anforderung);
        assertThat(entryDate, is(erstellungsdatum));
    }

    @Test
    void getEntryDateForAnforderungWithOneMeldung() {
        when(anforderung.getMeldungen()).thenReturn(meldungen);
        when(reportingMeldungStatusTransitionEntryDateService.getEntryDateForMeldung(any())).thenReturn(Optional.of(entryDate));
        final Date entryDate = reportingStatusTransitionAnforderungEntyDateService.getEntryDateForAnforderung(anforderung);
        assertThat(entryDate, is(entryDate));
    }

    @Test
    void getEntryDateForAnforderungWithMultipleMeldungenAndNoEntryDate() {
        meldungen.add(meldung2);
        when(anforderung.getMeldungen()).thenReturn(meldungen);
        when(reportingMeldungStatusTransitionEntryDateService.getEntryDateForMeldung(any())).thenReturn(Optional.empty());
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            reportingStatusTransitionAnforderungEntyDateService.getEntryDateForAnforderung(anforderung);
        });
    }

    @Test
    void getEntryDateForAnforderungWithMultipleMeldungenLatestDate() {
        meldungen.add(meldung2);
        when(anforderung.getMeldungen()).thenReturn(meldungen);
        when(reportingMeldungStatusTransitionEntryDateService.getEntryDateForMeldung(any()))
                .thenReturn(Optional.of(entryDate))
                .thenReturn(Optional.of(entryDate2));
        when(entryDate2.before(entryDate)).thenReturn(Boolean.TRUE);

        final Date entryDate = reportingStatusTransitionAnforderungEntyDateService.getEntryDateForAnforderung(anforderung);
        assertThat(entryDate, is(entryDate2));
    }
}