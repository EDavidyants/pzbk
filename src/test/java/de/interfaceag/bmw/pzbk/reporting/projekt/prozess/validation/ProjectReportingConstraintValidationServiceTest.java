package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.validation;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureDataCollection;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureConstraint;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureValidationResult;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;

import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingConstraintValidationServiceTest {

    @Mock
    ProjectReportingConstraintFactory constraintFactory;
    @InjectMocks
    ProjectReportingConstraintValidationService constraintValidationService;

    @Mock
    ProjectReportingProcessKeyFigureDataCollection dataCollection;

    @Mock
    KeyFigureConstraint constraint1;
    @Mock
    KeyFigureConstraint constraint2;

    @Mock
    KeyFigureValidationResult validationResult1;
    @Mock
    KeyFigureValidationResult validationResult2;

    @BeforeEach
    public void setUp() {
        when(constraintFactory.getProjectReportingConstraintG3eqL1L2(dataCollection)).thenReturn(constraint1);
        when(constraintFactory.getProjectReportingConstraintG3eqL4L5L6L7L8L9(dataCollection)).thenReturn(constraint2);
        when(constraintFactory.getProjectReportingConstraintL2eqL8L9(dataCollection)).thenReturn(constraint2);
        when(constraintFactory.getProjectReportingConstraintL10eqL12L13L14L15(dataCollection)).thenReturn(constraint2);
        when(constraintFactory.getProjectReportingConstraintR2G3eqR3R4(dataCollection)).thenReturn(constraint2);
        when(constraintFactory.getProjectReportingConstraintR3eqR6R7R8R9(dataCollection)).thenReturn(constraint2);
        when(constraintFactory.getProjectReportingConstraintR4eqR10R11(dataCollection)).thenReturn(constraint2);
        when(constraintFactory.getProjectReportingConstraintR12eqR14R15R16R17(dataCollection)).thenReturn(constraint2);
        when(constraintFactory.getProjectReportingConstraintR19eqR21R22R23R24(dataCollection)).thenReturn(constraint2);

        when(constraint1.validate()).thenReturn(validationResult1);
        when(constraint2.validate()).thenReturn(validationResult2);

        when(validationResult1.isValid()).thenReturn(Boolean.FALSE);
        when(validationResult2.isValid()).thenReturn(Boolean.TRUE);
    }

    @Test
    public void testValidateKeyFiguresResultSizeOneFalse() throws Exception {
        when(validationResult1.isValid()).thenReturn(Boolean.FALSE);
        when(validationResult2.isValid()).thenReturn(Boolean.TRUE);

        Collection<KeyFigureValidationResult> validationResult = constraintValidationService.validateKeyFigures(dataCollection);
        MatcherAssert.assertThat(validationResult, IsCollectionWithSize.hasSize(1));
    }

    @Test
    public void testValidateKeyFiguresResultSizeAllFalse() throws Exception {
        when(validationResult1.isValid()).thenReturn(Boolean.FALSE);
        when(validationResult2.isValid()).thenReturn(Boolean.FALSE);

        Collection<KeyFigureValidationResult> validationResult = constraintValidationService.validateKeyFigures(dataCollection);
        MatcherAssert.assertThat(validationResult, IsCollectionWithSize.hasSize(9));
    }

    @Test
    public void testValidateKeyFiguresResultContainsOneFalse() throws Exception {
        when(validationResult1.isValid()).thenReturn(Boolean.FALSE);
        when(validationResult2.isValid()).thenReturn(Boolean.TRUE);

        Collection<KeyFigureValidationResult> validationResult = constraintValidationService.validateKeyFigures(dataCollection);
        MatcherAssert.assertThat(validationResult, IsIterableContaining.hasItem(validationResult1));
    }

    @Test
    public void testValidateKeyFiguresResultContainsAllFalse() throws Exception {
        when(validationResult1.isValid()).thenReturn(Boolean.FALSE);
        when(validationResult2.isValid()).thenReturn(Boolean.FALSE);

        Collection<KeyFigureValidationResult> validationResult = constraintValidationService.validateKeyFigures(dataCollection);
        MatcherAssert.assertThat(validationResult, IsIterableContaining.hasItems(validationResult1, validationResult2));
    }

}
