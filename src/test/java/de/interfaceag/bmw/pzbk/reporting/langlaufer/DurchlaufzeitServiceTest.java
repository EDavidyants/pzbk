package de.interfaceag.bmw.pzbk.reporting.langlaufer;

import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.config.ReportingConfigService;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.IdTypeTuple;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungDurchlaufzeitKeyFigure;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsIterableContaining.hasItem;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DurchlaufzeitServiceTest {

    private static final int THRESHOLD = 12;

    @Mock
    private DurchlaufzeitDao durchlaufzeitDao;

    @Mock
    private ReportingConfigService reportingConfigService;

    @InjectMocks
    private DurchlaufzeitService durchlaufzeitService;

    @Mock
    private ReportingFilter filter;

    private Map<IdTypeTuple, Long> durchlaufzeitMap;

    @BeforeEach
    void setUp() {
        durchlaufzeitMap = new HashMap<>();
        durchlaufzeitMap.put(new IdTypeTuple(Type.ANFORDERUNG, 42L), 7L);
    }

    @Test
    void getFachteamDurchlaufzeitenResultSize() {
        when(durchlaufzeitDao.getFachteamIdTypeTupleDurchlaufzeitMap(any())).thenReturn(durchlaufzeitMap);

        final Collection<DurchlaufzeitDto> fachteamDurchlaufzeiten = durchlaufzeitService.getFachteamDurchlaufzeiten(filter);

        verify(durchlaufzeitDao, times(1)).getFachteamIdTypeTupleDurchlaufzeitMap(any());
        assertThat(fachteamDurchlaufzeiten, hasSize(1));
    }

    @Test
    void getFachteamDurchlaufzeitenResultValue() {
        when(durchlaufzeitDao.getFachteamIdTypeTupleDurchlaufzeitMap(any())).thenReturn(durchlaufzeitMap);
        when(reportingConfigService.getLanglauferFachteamThreshold()).thenReturn(THRESHOLD);

        DurchlaufzeitDto expextedResult = new DurchlaufzeitDto(Type.ANFORDERUNG, 42L, 7L, THRESHOLD);
        final Collection<DurchlaufzeitDto> fachteamDurchlaufzeiten = durchlaufzeitService.getFachteamDurchlaufzeiten(filter);

        verify(durchlaufzeitDao, times(1)).getFachteamIdTypeTupleDurchlaufzeitMap(any());
        verify(reportingConfigService, times(1)).getLanglauferFachteamThreshold();

        assertThat(fachteamDurchlaufzeiten, hasItem(expextedResult));
    }

    @Test
    void getFachteamDurchlaufzeit() {
        FachteamDurchlaufzeitKeyFigure result = new FachteamDurchlaufzeitKeyFigure(10);
        when(durchlaufzeitDao.getFachteamDurchlaufzeitKeyFigure(any())).thenReturn(result);

        FachteamDurchlaufzeitKeyFigure fachteamDurchlaufzeit = durchlaufzeitService.getFachteamDurchlaufzeitKeyFigure(filter);

        verify(durchlaufzeitDao, times(1)).getFachteamDurchlaufzeitKeyFigure(any());
        Assert.assertEquals(result.getValue(), fachteamDurchlaufzeit.getValue());
    }

    @Test
    void getVereinbarungDurchlaufzeit() {
        VereinbarungDurchlaufzeitKeyFigure result = new VereinbarungDurchlaufzeitKeyFigure(10);
        when(durchlaufzeitDao.getVereinbarungDurchlaufzeitKeyFigure(any())).thenReturn(result);

        VereinbarungDurchlaufzeitKeyFigure vereinbarungDurchlaufzeit = durchlaufzeitService.getVereinbarungDurchlaufzeitKeyFigure(filter);

        verify(durchlaufzeitDao, times(1)).getVereinbarungDurchlaufzeitKeyFigure(any());
        Assert.assertEquals(result.getValue(), vereinbarungDurchlaufzeit.getValue());
    }

    @Test
    void getTteamDurchlaufzeit() {
        TteamDurchlaufzeitKeyFigure result = new TteamDurchlaufzeitKeyFigure(10);
        when(durchlaufzeitDao.getTteamDurchlaufzeitKeyFigure(any())).thenReturn(result);

        TteamDurchlaufzeitKeyFigure tteamDurchlaufzeit = durchlaufzeitService.getTteamDurchlaufzeitKeyFigure(filter);

        verify(durchlaufzeitDao, times(1)).getTteamDurchlaufzeitKeyFigure(any());
        Assert.assertEquals(result.getValue(), tteamDurchlaufzeit.getValue());
    }
}