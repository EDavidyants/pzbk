package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterViewPermission;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.HashSet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingTteamControllerTest {

    @InjectMocks
    private ReportingTteamController reportingTteamController;

    @Mock
    private ReportingTteamViewData viewData;

    @Mock
    private ReportingFilter reportingFilter;

    @Mock
    private ReportingFilterViewPermission reportingFilterViewPermission;

    @Mock
    private Session session;

    @Mock
    private UserPermissions userPermissions;

    @Test
    void init() {
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRoles()).thenReturn(new HashSet<>(Arrays.asList(Rolle.ADMIN)));

        reportingTteamController.init();
        verify(session, times(1)).setLocationForView();
    }

    @Test
    void filter() {
        String url = "http://localhost:58222/pzbk/reporting.xhtml";
        when(viewData.getFilter()).thenReturn(reportingFilter);
        when(reportingFilter.getUrl()).thenReturn(url);
        Assert.assertEquals(url, reportingTteamController.filter());
    }

    @Test
    void reset() {
        String url = "http://localhost:58222/pzbk/reporting.xhtml";
        when(viewData.getFilter()).thenReturn(reportingFilter);
        when(reportingFilter.getResetUrl()).thenReturn(url);
        Assert.assertEquals(url, reportingTteamController.reset());
    }

    @Test
    void getFilter() {
        when(viewData.getFilter()).thenReturn(reportingFilter);
        Assert.assertEquals(reportingFilter, reportingTteamController.getFilter());
    }

    @Test
    void downloadExcelExport() {
        try {
            reportingTteamController.downloadExcelExport();
        } catch (UnsupportedOperationException e) {
            Assert.assertEquals("Not supported yet.", e.getMessage());
        }
    }

    @Test
    void getFilterViewPermission() {
        Assert.assertEquals(reportingFilterViewPermission, reportingTteamController.getFilterViewPermission());
    }
}