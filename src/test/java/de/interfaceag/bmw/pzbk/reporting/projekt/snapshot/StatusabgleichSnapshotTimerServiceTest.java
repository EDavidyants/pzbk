package de.interfaceag.bmw.pzbk.reporting.projekt.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot.ProjectProcessSnapshotWriteService;
import de.interfaceag.bmw.pzbk.reporting.statusabgleich.snapshot.StatusabgleichSnapshotWriteService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class StatusabgleichSnapshotTimerServiceTest {

    @Mock
    private DerivatService derivatService;
    @Mock
    private ProjectProcessSnapshotWriteService projectProcessSnapshotWriteService;
    @Mock
    private StatusabgleichSnapshotWriteService statusabgleichSnapshotWriteService;

    @InjectMocks
    private ProjectReportingSnapshotService projectReportingSnapshotService;

    private List<Derivat> derivate;
    @Mock
    private Derivat derivat;
    @Mock
    private Derivat derivat2;

    @BeforeEach
    public void setUp() {
        derivate = new ArrayList<>();
        derivate.add(derivat);
        derivate.add(derivat2);
        when(derivatService.getAllDerivateWithZielvereinbarungFalse()).thenReturn(derivate);
    }

    @Test
    public void testCreateStatusabgleichSnapshots() {
        projectReportingSnapshotService.createSnapshotsForActiveDerivate();
        verify(statusabgleichSnapshotWriteService, times(2)).createNewSnapshotForDerivat(any());
    }

    @Test
    public void testCreateProjectProcessSnapshots() {
        projectReportingSnapshotService.createSnapshotsForActiveDerivate();
        verify(projectProcessSnapshotWriteService, times(2)).createNewSnapshotForDerivat(any());
    }

}
