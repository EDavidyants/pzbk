package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

class EntryExitTimeStampBuilderTest {

    private EntryExitTimeStampBuilder builder;

    @BeforeEach
    void setUp() {
        builder = new EntryExitTimeStampBuilder();
    }

    @Test
    void setEntry() {
        Date date = new Date();
        builder.setEntry(date);
        final EntryExitTimeStamp timeStamp = builder.build();
        final Date entry = timeStamp.getEntry();
        assertThat(entry, is(date));
    }

    @Test
    void setExit() {
        Date date = new Date();
        builder.setExit(date);
        final EntryExitTimeStamp timeStamp = builder.build();
        final Date entry = timeStamp.getExit();
        assertThat(entry, is(date));
    }

    @Test
    void buildDefaultValuesEntry() {
        final EntryExitTimeStamp timeStamp = builder.build();
        final Date entry = timeStamp.getEntry();
        assertThat(entry, is(nullValue()));
    }

    @Test
    void buildDefaultValuesExit() {
        final EntryExitTimeStamp timeStamp = builder.build();
        final Date exit = timeStamp.getExit();
        assertThat(exit, is(nullValue()));
    }

    @Test
    void buildNotNull() {
        final EntryExitTimeStamp timeStamp = builder.build();
        assertThat(timeStamp, is(notNullValue()));
    }
}