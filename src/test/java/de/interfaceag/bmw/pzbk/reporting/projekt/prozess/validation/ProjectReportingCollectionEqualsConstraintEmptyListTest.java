package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.validation;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureConstraint;
import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureValidationResult;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingCollectionEqualsConstraintEmptyListTest {

    private static final String VALIDATION__FAILED = "Validation Failed";

    private KeyFigureConstraint constraint;
    @Mock
    private ProjectReportingProcessKeyFigureData keyFigureData;

    @BeforeEach
    public void setUp() {
        constraint = ProjectReportingCollectionEqualsConstraint.build()
                .addToRightSide(keyFigureData)
                .withValidationMessageFailed(VALIDATION__FAILED)
                .build();

        when(keyFigureData.getValue()).thenReturn(2L);
        when(keyFigureData.getKeyFigure()).thenReturn(ProjectReportingProcessKeyFigure.G2);
    }

    @Test
    public void testValidateInvalid() {
        KeyFigureValidationResult result = constraint.validate();
        boolean valid = result.isValid();
        MatcherAssert.assertThat(valid, is(false));
    }

    @Test
    public void testValidateInvalidFailedMessage() {
        KeyFigureValidationResult result = constraint.validate();
        String validationMessage = result.getValidationMessage();
        MatcherAssert.assertThat(validationMessage, is(" = G2 Validation Failed 0 = 2"));
    }

}
