package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessAmpelThresholdService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsMapContaining;
import org.hamcrest.collection.IsMapWithSize;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingUmsetzungsverwaltungKeyFigureUtilsVKBGTest {

    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataL12;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataL13;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataL14;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataL15;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataL16;

    @InjectMocks
    private final ProjectProcessAmpelThresholdService ampelThresholdService = new ProjectProcessAmpelThresholdService();

    Map<Integer, ProjectReportingProcessKeyFigureData> keyFigureData;

    @BeforeEach
    public void setUp() {
        when(ProjectReportingProcessKeyFigureDataL12.getValue()).thenReturn(4L);
        when(ProjectReportingProcessKeyFigureDataL13.getValue()).thenReturn(5L);
        when(ProjectReportingProcessKeyFigureDataL14.getValue()).thenReturn(6L);
        when(ProjectReportingProcessKeyFigureDataL15.getValue()).thenReturn(7L);

        keyFigureData = new HashMap<>();
        keyFigureData.put(ProjectReportingProcessKeyFigure.L12.getId(), ProjectReportingProcessKeyFigureDataL12);
        keyFigureData.put(ProjectReportingProcessKeyFigure.L13.getId(), ProjectReportingProcessKeyFigureDataL13);
        keyFigureData.put(ProjectReportingProcessKeyFigure.L14.getId(), ProjectReportingProcessKeyFigureDataL14);
        keyFigureData.put(ProjectReportingProcessKeyFigure.L15.getId(), ProjectReportingProcessKeyFigureDataL15);
        keyFigureData.put(ProjectReportingProcessKeyFigure.L16.getId(), ProjectReportingProcessKeyFigureDataL16);
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBGResultSize() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungVKBG());
        MatcherAssert.assertThat(result, IsMapWithSize.aMapWithSize(7));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBGContainsL10() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungVKBG());
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L10.getId()));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBGL10Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungVKBG());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L10.getId());
        Long dataValue = data.getValue();
        MatcherAssert.assertThat(dataValue, is(22L));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBGContainsL11() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungVKBG());
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L11.getId()));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBGL11Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungVKBG());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L11.getId());
        Long dataValue = data.getValue();
        MatcherAssert.assertThat(dataValue, is(18L));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBGL11Ampel() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingUmsetzungsverwaltungKeyFigureUtils.computeSumAndRatioKeyFiguresForUmsetzungsverwaltungVKBG(keyFigureData, ampelThresholdService.getThresholdForUmsetzungsverwaltungVKBG());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L11.getId());
        boolean isAmpelGreen = data.isAmpelGreen();
        Assertions.assertFalse(isAmpelGreen);
    }

}
