package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachteamInputKeyFigureTest {

    // 4 + 2
    private final Collection<Long> idsTteamUnstimmig = Arrays.asList(1L, 2L);
    private final Collection<Long> idsVereinbarungUnstimmig = Arrays.asList(2L, 3L, 4L);

    private TteamUnstimmigKeyFigure tteamUnstimmigKeyFigure;
    private VereinbarungUnstimmigKeyFigure vereinbarungUnstimmigKeyFigure;

    private final Collection<Long> idsGemeldet = Arrays.asList(1L, 2L);
    private final Collection<Long> idsNewVersion = Arrays.asList(2L, 3L, 4L, 5L);

    private FachteamGemeldeteMeldungenKeyFigure fachteamGemeldeteMeldungenKeyFigure;
    private FachteamAnforderungNewVersionKeyFigure fachteamAnforderungNewVersionKeyFigure;
    private FachteamUnstimmigKeyFigure fachteamUnstimmigKeyFigure;

    @Before
    public void setUp() throws InvalidDataException {
        initKeyFigures();
    }

    private void initKeyFigures() throws InvalidDataException {
        tteamUnstimmigKeyFigure = new TteamUnstimmigKeyFigure(idsTteamUnstimmig, 0L);
        vereinbarungUnstimmigKeyFigure = new VereinbarungUnstimmigKeyFigure(idsVereinbarungUnstimmig, 0L);

        fachteamGemeldeteMeldungenKeyFigure = new FachteamGemeldeteMeldungenKeyFigure(idsGemeldet, 0L);
        fachteamAnforderungNewVersionKeyFigure = new FachteamAnforderungNewVersionKeyFigure(idsNewVersion, 0L);

        fachteamUnstimmigKeyFigure = FachteamUnstimmigKeyFigure.buildFromKeyFigures(tteamUnstimmigKeyFigure, vereinbarungUnstimmigKeyFigure);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresAllParamtersNull() throws InvalidDataException {
        FachteamInputKeyFigure.buildFromKeyFigures(null, null, null);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresThirdParamterNull() throws InvalidDataException {
        FachteamInputKeyFigure.buildFromKeyFigures(fachteamGemeldeteMeldungenKeyFigure, fachteamAnforderungNewVersionKeyFigure, null);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresSecondParamterNull() throws InvalidDataException {
        FachteamInputKeyFigure.buildFromKeyFigures(fachteamGemeldeteMeldungenKeyFigure, null, fachteamUnstimmigKeyFigure);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresFirstParamterNull() throws InvalidDataException {
        FachteamInputKeyFigure.buildFromKeyFigures(null, fachteamAnforderungNewVersionKeyFigure, fachteamUnstimmigKeyFigure);
    }

    @Test
    public void testBuildFromKeyFiguresKeyTypeId() throws InvalidDataException {
        FachteamInputKeyFigure result = FachteamInputKeyFigure.buildFromKeyFigures(fachteamGemeldeteMeldungenKeyFigure, fachteamAnforderungNewVersionKeyFigure, fachteamUnstimmigKeyFigure);
        assertEquals(KeyType.FACHTEAM_ENTRY_SUM.getId(), result.getKeyTypeId());
    }

    @Test
    public void testBuildFromKeyFiguresCollectionSize() throws InvalidDataException {
        FachteamInputKeyFigure result = FachteamInputKeyFigure.buildFromKeyFigures(fachteamGemeldeteMeldungenKeyFigure, fachteamAnforderungNewVersionKeyFigure, fachteamUnstimmigKeyFigure);
        assertEquals(11, result.getIdTypeTuples().size());
    }

    @Test
    public void testBuildFromKeyFiguresEmptyCollection() throws InvalidDataException {
        FachteamAnforderungNewVersionKeyFigure keyFigureWithEmptyCollection = new FachteamAnforderungNewVersionKeyFigure(Collections.EMPTY_LIST, 0L);
        FachteamInputKeyFigure result = FachteamInputKeyFigure.buildFromKeyFigures(fachteamGemeldeteMeldungenKeyFigure, keyFigureWithEmptyCollection, fachteamUnstimmigKeyFigure);
        assertEquals(7, result.getIdTypeTuples().size());
    }

    @Test
    public void testBuildFromKeyFiguresRuntimeNotNull() throws InvalidDataException {
        FachteamInputKeyFigure result = FachteamInputKeyFigure.buildFromKeyFigures(fachteamGemeldeteMeldungenKeyFigure, fachteamAnforderungNewVersionKeyFigure, fachteamUnstimmigKeyFigure);
        assertNotNull(result.getRunTime());
    }

    @Test
    public void testBuildFromKeyFiguresRuntime() throws InvalidDataException {
        FachteamInputKeyFigure result = FachteamInputKeyFigure.buildFromKeyFigures(fachteamGemeldeteMeldungenKeyFigure, fachteamAnforderungNewVersionKeyFigure, fachteamUnstimmigKeyFigure);
        assertEquals(0L, (long) result.getRunTime());
    }

}
