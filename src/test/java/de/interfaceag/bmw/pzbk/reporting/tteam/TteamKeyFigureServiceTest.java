package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.langlaufer.LanglauferService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.StatusTransitionKeyFigureService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TteamKeyFigureServiceTest {

    @Mock
    private TteamService tteamService;
    @Mock
    private StatusTransitionKeyFigureService statusTransitionKeyFigureService;
    @Mock
    private LocalizationService localizationService;
    @InjectMocks
    private TteamKeyFigureService tteamKeyFigureService;

    @Mock
    private ReportingFilter filter;
    @Mock
    private Tteam tteam;
    @Mock
    private KeyFigureCollection keyFigureCollection;
    @Mock
    private DurchlaufzeitKeyFigure durchlaufzeitKeyFigure;
    @Mock
    private DurchlaufzeitKeyFigureCollection durchlaufzeitKeyFigures;
    @Mock
    private IdSearchFilter tteamFilter;
    @Mock
    private LanglauferService langlauferService;

    private List<Tteam> tteams;
    private Collection<String> technologien;

    @BeforeEach
    void setUp() {
        tteams = singletonList(tteam);
        technologien = singletonList("Technologie");

        when(filter.getTteamFilter()).thenReturn(tteamFilter);
    }


    @Test
    void getFachteamKeyFiguresResultSizeForOneSensorCoc() {
        when(tteamService.getTteamByIdList(any())).thenReturn(tteams);
        when(tteam.getId()).thenReturn(42L);
        when(statusTransitionKeyFigureService.getKeyFiguresForTteamReporting(filter)).thenReturn(keyFigureCollection);
        when(statusTransitionKeyFigureService.computeDurchlaufzeitKeyFigures(filter)).thenReturn(durchlaufzeitKeyFigures);
        when(durchlaufzeitKeyFigures.getByKeyType(any())).thenReturn(durchlaufzeitKeyFigure);
        when(tteamFilter.isActive()).thenReturn(true);
        when(langlauferService.getTteamLanglauferCountByTteam(tteam)).thenReturn(2);
        when(langlauferService.getVereinbarungLanglauferCountByTteam(tteam)).thenReturn(2);

        final TteamKeyFigures tteamKeyFigures = tteamKeyFigureService.getTteamKeyFigures(filter);
        assertThat(tteamKeyFigures.getKeyFigures(), hasSize(2));

        verify(langlauferService, times(1)).getTteamLanglauferCountByTteam(tteam);
        verify(langlauferService, times(1)).getVereinbarungLanglauferCountByTteam(tteam);
    }

    @Test
    void getFachteamKeyFiguresResultSizeForInactiveTechnologieFilter() {
        when(tteamFilter.isActive()).thenReturn(false);

        final TteamKeyFigures tteamKeyFigures = tteamKeyFigureService.getTteamKeyFigures(filter);
        assertThat(tteamKeyFigures.getKeyFigures(), hasSize(1));
    }
}