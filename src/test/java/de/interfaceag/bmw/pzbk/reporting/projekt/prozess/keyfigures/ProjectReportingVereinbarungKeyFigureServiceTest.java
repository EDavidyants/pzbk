package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.filter.AbstractMultiValueFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessAmpelThresholdService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsMapContaining;
import org.hamcrest.collection.IsMapWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl, evda
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingVereinbarungKeyFigureServiceTest {

    @Mock
    Query query;
    @Mock
    EntityManager entityManager;

    @Mock
    private ProjectProcessAmpelThresholdService ampelThresholdService;

    @InjectMocks
    private ProjectReportingVereinbarungKeyFigureService vereinbarungKeyFigureService;

    @Mock
    Derivat derivat;

    @Mock
    ProjectProcessFilter filter;

    @Mock
    AbstractMultiValueFilter technologieFilter;

    List<Object[]> queryResults;

    private final Double thresholdForGreenAmpel = 0.85;

    @BeforeEach
    public void setUp() {
        Object[] queryResult1 = new Object[2];
        queryResult1[0] = DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId();
        queryResult1[1] = 1L;

        Object[] queryResult2 = new Object[2];
        queryResult2[0] = DerivatAnforderungModulStatus.IN_KLAERUNG.getStatusId();
        queryResult2[1] = 2L;

        Object[] queryResult3 = new Object[2];
        queryResult3[0] = DerivatAnforderungModulStatus.ABZUSTIMMEN.getStatusId();
        queryResult3[1] = 3L;

        Object[] queryResult4 = new Object[2];
        queryResult4[0] = DerivatAnforderungModulStatus.NICHT_BEWERTBAR.getStatusId();
        queryResult4[1] = 4L;

        Object[] queryResult5 = new Object[2];
        queryResult5[0] = DerivatAnforderungModulStatus.KEINE_WEITERVERFOLGUNG.getStatusId();
        queryResult5[1] = 5L;

        Object[] queryResult6 = new Object[2];
        queryResult6[0] = DerivatAnforderungModulStatus.UNZUREICHENDE_ANFORDERUNGSQUALITAET.getStatusId();
        queryResult6[1] = 6L;

        queryResults = new ArrayList<>();
        queryResults.add(queryResult1);
        queryResults.add(queryResult2);
        queryResults.add(queryResult3);
        queryResults.add(queryResult4);
        queryResults.add(queryResult5);
        queryResults.add(queryResult6);

        when(filter.getTechnologieFilter()).thenReturn(technologieFilter);
        when(technologieFilter.isActive()).thenReturn(Boolean.TRUE);
        when(technologieFilter.getSelectedValuestringsAsList()).thenReturn(Arrays.asList("Technologie Gesamtfahrzeug"));
        when(entityManager.createQuery(any(String.class))).thenReturn(query);
        when(query.getResultList()).thenReturn(queryResults);
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterResultSize() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        MatcherAssert.assertThat(result, IsMapWithSize.aMapWithSize(9));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterContainsL1() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L1.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterL1Value() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L1.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(10L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterContainsL2() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L2.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterL2Value() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L2.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(11L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterContainsL3() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L3.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterL3Value() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L3.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(10L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterContainsAngenommen() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L4.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterAngenommenValue() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L4.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(1L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterContainsInKlaerung() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L5.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterInKlaerungValue() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L5.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(2L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterContainsAbzustimmen() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L6.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterAbzustimmenValue() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L6.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(3L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterContainsNichtBewertbar() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L7.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterNichtBewertbarValue() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L7.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(4L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterContainsKeineWeiterverfolgung() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L8.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterKeineWeiterverfolgungValue() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L8.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(5L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterContainsUnzureichendeAnforderungsqualitaet() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L9.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungVKBGWithFilterUnzureichendeAnforderungsqualitaetValue() {
        when(ampelThresholdService.getThresholdForVereinbarungVKBG()).thenReturn(0.85);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungVKBG(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L9.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(6L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterResultSize() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        MatcherAssert.assertThat(result, IsMapWithSize.aMapWithSize(9));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterContainsAngenommen() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R6.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterAngenommenValue() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R6.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(1L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterContainsR3() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R3.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterR3nValue() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R3.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(10L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterContainsR4() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R4.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterR4Value() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R4.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(11L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterContainsR5() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R5.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterR5Value() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R5.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(10L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterContainsInKlaerung() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R7.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterInKlaerungValue() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R7.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(2L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterContainsAbzustimmen() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R8.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterAbzustimmenValue() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R8.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(3L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterContainsNichtBewertbar() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R9.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterNichtBewertbarValue() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R9.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(4L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterContainsKeineWeiterverfolgung() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R10.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterKeineWeiterverfolgungValue() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R10.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(5L));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterContainsUnzureichendeAnforderungsqualitaet() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.R11.getId()));
    }

    @Test
    public void testGetKeyFiguresForVereinbarungZVWithFilterUnzureichendeAnforderungsqualitaetValue() {
        when(ampelThresholdService.getThresholdForVereinbarungZV()).thenReturn(0.95);
        Map<Integer, ProjectReportingProcessKeyFigureData> result = vereinbarungKeyFigureService.getKeyFiguresForVereinbarungZV(derivat, filter);
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.R11.getId());
        Long value = data.getValue();
        MatcherAssert.assertThat(value, is(6L));
    }

}
