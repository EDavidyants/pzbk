package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamAnforderungAbgestimmtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamCurrentKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamOutputKeyFigure;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsIterableContaining.hasItems;

class TteamSumKeyFigureCalculatorTest {

    @Test
    void computeSumKeyFigures() {
    }

    private String LABEL = "Label";
    private Collection<TteamKeyFigure> keyFigures;
    private Collection<Long> ids1;
    private Collection<Long> ids2;

    @BeforeEach
    void setUp() {
        ids1 = Arrays.asList(1L, 42L);
        ids2 = Arrays.asList(2L, 42L);
        keyFigures = new ArrayList<>();
    }

    @Test
    void computeSumKeyFiguresLabel() {
        final TteamKeyFigure fachteamKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamKeyFigure.getTteamName(), is(LABEL));
    }

    @Test
    void computeSumKeyFiguresKeyFigureResultSize() {
        final TteamKeyFigure fachteamKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamKeyFigure.getKeyFigures(), hasSize(9));
    }

    @Test
    void computeSumKeyFiguresDefaultTteamDurchlaufzeit() {
        final TteamKeyFigure fachteamKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamKeyFigure.getTteamDurchlaufzeit(), is(0));
    }

    @Test
    void computeSumKeyFiguresDefaultVereinbarungDurchlaufzeit() {
        final TteamKeyFigure fachteamKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamKeyFigure.getVereinbarungDurchlaufzeit(), is(0));
    }

    @Test
    void computeSumKeyFiguresCalculateTteamDurchlaufzeit() {
        TteamKeyFigure fachteamKeyFigure = buildTteamDurchlaufzeitKeyFigure(5);
        keyFigures.add(fachteamKeyFigure);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getTteamDurchlaufzeit(), is(5));
    }

    @Test
    void computeSumKeyFiguresCalculateVereinbarungDurchlaufzeit() {
        TteamKeyFigure fachteamKeyFigure = buildTteamDurchlaufzeitKeyFigure(5);
        keyFigures.add(fachteamKeyFigure);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getVereinbarungDurchlaufzeit(), is(5));
    }

    @Test
    void computeSumKeyFiguresCalculateTteamDurchlaufzeitMultipleEntries() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamDurchlaufzeitKeyFigure(5);
        TteamKeyFigure fachteamKeyFigure2 = buildTteamDurchlaufzeitKeyFigure(9);
        keyFigures.add(tteamKeyFigure1);
        keyFigures.add(fachteamKeyFigure2);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getTteamDurchlaufzeit(), is(7));
    }

    @Test
    void computeSumKeyFiguresCalculateVereinbarungDurchlaufzeitMultipleEntries() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamDurchlaufzeitKeyFigure(5);
        TteamKeyFigure fachteamKeyFigure2 = buildTteamDurchlaufzeitKeyFigure(9);
        keyFigures.add(tteamKeyFigure1);
        keyFigures.add(fachteamKeyFigure2);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getVereinbarungDurchlaufzeit(), is(7));
    }

    @Test
    void computeSumKeyFiguresAnforderungIdsForTteamCurrentKeyFigureMultipleInputsWithSameIds() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamCurrentKeyFigure(ids1);
        TteamKeyFigure fachteamKeyFigure2 = buildTteamKeyFigureWithTteamCurrentKeyFigure(ids1);
        keyFigures.add(tteamKeyFigure1);
        keyFigures.add(fachteamKeyFigure2);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.TTEAM_CURRENT).getAnforderungIds(), hasItems(1L, 42L));
    }

    @Test
    void computeSumKeyFiguresTteamCurrentKeyFigureMultipleInputsWithSameIdsValue() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamCurrentKeyFigure(ids1);
        TteamKeyFigure fachteamKeyFigure2 = buildTteamKeyFigureWithTteamCurrentKeyFigure(ids1);
        keyFigures.add(tteamKeyFigure1);
        keyFigures.add(fachteamKeyFigure2);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.TTEAM_CURRENT).getValue(), is(4));
    }

    @Test
    void computeSumKeyFiguresAnforderungIdsForTteamCurrentKeyFigureMultipleInputsWithDifferentIds() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamCurrentKeyFigure(ids1);
        TteamKeyFigure fachteamKeyFigure2 = buildTteamKeyFigureWithTteamCurrentKeyFigure(ids2);
        keyFigures.add(tteamKeyFigure1);
        keyFigures.add(fachteamKeyFigure2);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.TTEAM_CURRENT).getAnforderungIds(), hasItems(1L, 2L, 42L));
    }

    @Test
    void computeSumKeyFiguresTteamCurrentKeyFigureMultipleInputsWithDifferentIdsValue() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamCurrentKeyFigure(ids1);
        TteamKeyFigure fachteamKeyFigure2 = buildTteamKeyFigureWithTteamCurrentKeyFigure(ids2);
        keyFigures.add(tteamKeyFigure1);
        keyFigures.add(fachteamKeyFigure2);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.TTEAM_CURRENT).getValue(), is(4));
    }

    @Test
    void computeSumKeyFiguresAnforderungIdsForTteamGeloeschtKeyFigure() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamKeyFigures(ids1, ids2);
        keyFigures.add(tteamKeyFigure1);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.TTEAM_GELOESCHT).getAnforderungIds(), hasItems(1L, 42L));
    }

    @Test
    void computeSumKeyFiguresMeldungIdsForTteamGeloeschtKeyFigure() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamKeyFigures(ids1, ids2);
        keyFigures.add(tteamKeyFigure1);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.TTEAM_GELOESCHT).getMeldungIds(), IsEmptyCollection.empty());
    }

    @Test
    void computeSumKeyFiguresTteamGeloeschtKeyFigureValue() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamKeyFigures(ids1, ids2);
        keyFigures.add(tteamKeyFigure1);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.TTEAM_GELOESCHT).getValue(), is(2));
    }

    @Test
    void computeSumKeyFiguresAnforderungIdsForTteamOutputKeyFigure() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamKeyFigures(ids1, ids2);
        keyFigures.add(tteamKeyFigure1);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.TTEAM_OUTPUT).getAnforderungIds(), hasItems(1L, 42L));
    }

    @Test
    void computeSumKeyFiguresMeldungIdsForTteamOutputKeyFigure() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamKeyFigures(ids1, ids2);
        keyFigures.add(tteamKeyFigure1);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.TTEAM_OUTPUT).getMeldungIds(), IsEmptyCollection.empty());
    }

    @Test
    void computeSumKeyFiguresTteamOutputKeyFigureValue() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamKeyFigures(ids1, ids2);
        keyFigures.add(tteamKeyFigure1);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.TTEAM_OUTPUT).getValue(), is(2));
    }

    @Test
    void computeSumKeyFiguresAnforderungIdsForTteamCurrentKeyFigure() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamKeyFigures(ids1, ids2);
        keyFigures.add(tteamKeyFigure1);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.TTEAM_CURRENT).getAnforderungIds(), hasItems(1L, 42L));
    }

    @Test
    void computeSumKeyFiguresMeldungIdsForTteamCurrentKeyFigure() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamKeyFigures(ids1, ids2);
        keyFigures.add(tteamKeyFigure1);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.TTEAM_CURRENT).getMeldungIds(), IsEmptyCollection.empty());
    }

    @Test
    void computeSumKeyFiguresTteamCurrentKeyFigureValue() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamKeyFigures(ids1, ids2);
        keyFigures.add(tteamKeyFigure1);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.TTEAM_CURRENT).getValue(), is(2));
    }

    @Test
    void computeSumKeyFiguresAnforderungIdsForTteamInputKeyFigure() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamKeyFigures(ids1, ids2);
        keyFigures.add(tteamKeyFigure1);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_ANFORDERUNG_ABGESTIMMT).getAnforderungIds(), hasItems(1L, 42L));
    }

    @Test
    void computeSumKeyFiguresMeldungIdsForTteamInputKeyFigure() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamKeyFigures(ids1, ids2);
        keyFigures.add(tteamKeyFigure1);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_ANFORDERUNG_ABGESTIMMT).getMeldungIds(), IsEmptyCollection.empty());
    }

    @Test
    void computeSumKeyFiguresTteamInputKeyFigureValue() {
        TteamKeyFigure tteamKeyFigure1 = buildTteamKeyFigureWithTteamKeyFigures(ids1, ids2);
        keyFigures.add(tteamKeyFigure1);
        final TteamKeyFigure fachteamSumKeyFigure = TteamSumKeyFigureCalculator.computeSumKeyFigures(keyFigures, LABEL);
        assertThat(fachteamSumKeyFigure.getKeyFigureByType(KeyType.FACHTEAM_ANFORDERUNG_ABGESTIMMT).getValue(), is(2));
    }


    private TteamKeyFigure buildTteamKeyFigureWithTteamKeyFigures(Collection<Long> anforderungIds, Collection<Long> meldungIds) {
        DurchlaufzeitKeyFigure tteamDurchlaufZeitKeyFigure = getDurchlaufZeitKeyFigure(5, KeyType.DURCHLAUFZEIT_TTEAM);
        DurchlaufzeitKeyFigure vereinbarungDurchlaufZeitKeyFigure = getDurchlaufZeitKeyFigure(5, KeyType.DURCHLAUFZEIT_VEREINBARUNG);
        KeyFigureCollection keyFigureCollection = new KeyFigureCollection();
        keyFigureCollection.add(new TteamGeloeschtKeyFigure(anforderungIds));
        keyFigureCollection.add(new TteamOutputKeyFigure(anforderungIds));
        keyFigureCollection.add(new TteamCurrentKeyFigure(anforderungIds));
        keyFigureCollection.add(new FachteamAnforderungAbgestimmtKeyFigure(anforderungIds));
        return new TteamKeyFigure(1L, LABEL, keyFigureCollection, tteamDurchlaufZeitKeyFigure, vereinbarungDurchlaufZeitKeyFigure, 0, 0);
    }

    private TteamKeyFigure buildTteamKeyFigureWithTteamCurrentKeyFigure(Collection<Long> anforderungIds) {
        DurchlaufzeitKeyFigure tteamDurchlaufZeitKeyFigure = getDurchlaufZeitKeyFigure(5, KeyType.DURCHLAUFZEIT_TTEAM);
        DurchlaufzeitKeyFigure vereinbarungDurchlaufZeitKeyFigure = getDurchlaufZeitKeyFigure(5, KeyType.DURCHLAUFZEIT_VEREINBARUNG);
        KeyFigureCollection keyFigureCollection = new KeyFigureCollection();
        keyFigureCollection.add(new TteamCurrentKeyFigure(anforderungIds));
        return new TteamKeyFigure(1L, LABEL, keyFigureCollection, tteamDurchlaufZeitKeyFigure, vereinbarungDurchlaufZeitKeyFigure, 0, 0);
    }

    private TteamKeyFigure buildTteamDurchlaufzeitKeyFigure(int durchlaufzeitValue) {
        DurchlaufzeitKeyFigure tteamDurchlaufZeitKeyFigure = getDurchlaufZeitKeyFigure(durchlaufzeitValue, KeyType.DURCHLAUFZEIT_TTEAM);
        DurchlaufzeitKeyFigure vereinbarungDurchlaufZeitKeyFigure = getDurchlaufZeitKeyFigure(durchlaufzeitValue, KeyType.DURCHLAUFZEIT_VEREINBARUNG);
        KeyFigureCollection keyFigureCollection = new KeyFigureCollection();
        return new TteamKeyFigure(1L, LABEL, keyFigureCollection, tteamDurchlaufZeitKeyFigure, vereinbarungDurchlaufZeitKeyFigure, 0, 0);
    }

    private static DurchlaufzeitKeyFigure getDurchlaufZeitKeyFigure(int value, KeyType keyType) {
        return new DurchlaufzeitKeyFigure() {
            @Override
            public KeyType getKeyType() {
                return keyType;
            }

            @Override
            public int getKeyTypeId() {
                return keyType.getId();
            }

            @Override
            public int getValue() {
                return value;
            }
        };
    }
}