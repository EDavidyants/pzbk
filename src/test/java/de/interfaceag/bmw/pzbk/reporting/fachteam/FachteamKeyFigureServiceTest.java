package de.interfaceag.bmw.pzbk.reporting.fachteam;

import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.TechnologieFilter;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.DurchlaufzeitKeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyFigureCollection;
import de.interfaceag.bmw.pzbk.reporting.langlaufer.LanglauferService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.StatusTransitionKeyFigureService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Collections;

import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FachteamKeyFigureServiceTest {

    @Mock
    private SensorCocService sensorCocService;
    @Mock
    private StatusTransitionKeyFigureService statusTransitionKeyFigureService;
    @Mock
    private LocalizationService localizationService;
    @InjectMocks
    private FachteamKeyFigureService fachteamKeyFigureService;

    @Mock
    private ReportingFilter filter;
    @Mock
    private SensorCoc sensorCoc;
    @Mock
    private KeyFigureCollection keyFigureCollection;
    @Mock
    private DurchlaufzeitKeyFigure durchlaufzeitKeyFigure;
    @Mock
    private DurchlaufzeitKeyFigureCollection durchlaufzeitKeyFigures;
    @Mock
    private TechnologieFilter technologieFilter;
    @Mock
    private IdSearchFilter sensorCocFilter;
    @Mock
    private LanglauferService langlauferService;


    private Collection<SensorCoc> sensorCocs;
    private Collection<String> technologien;

    @BeforeEach
    void setUp() {
        sensorCocs = singletonList(sensorCoc);
        technologien = singletonList("Technologie");

        when(filter.getTechnologieFilter()).thenReturn(technologieFilter);
        when(technologieFilter.getSelectedStringValues()).thenReturn(technologien);
    }

    @Test
    void getFachteamKeyFiguresResultSizeForOneSensorCoc() {
        when(sensorCocService.getSensorCocsByOrtung(any())).thenReturn(sensorCocs);
        when(sensorCoc.getSensorCocId()).thenReturn(42L);
        when(statusTransitionKeyFigureService.getKeyFiguresForFachteamReporting(filter)).thenReturn(keyFigureCollection);
        when(statusTransitionKeyFigureService.computeDurchlaufzeitKeyFigures(filter)).thenReturn(durchlaufzeitKeyFigures);
        when(durchlaufzeitKeyFigures.getByKeyType(any())).thenReturn(durchlaufzeitKeyFigure);
        when(filter.getSensorCocFilter()).thenReturn(sensorCocFilter);
        when(technologieFilter.isActive()).thenReturn(true);
        when(langlauferService.getFachteamLanglauferCountBySensorCoc(sensorCoc)).thenReturn(2);

        final FachteamKeyFigures fachteamKeyFigures = fachteamKeyFigureService.getFachteamKeyFigures(filter);
        assertThat(fachteamKeyFigures.getKeyFigures(), hasSize(2));

        verify(langlauferService, times(1)).getFachteamLanglauferCountBySensorCoc(sensorCoc);
    }

    @Test
    void getFachteamKeyFiguresResultSizeForNoSensorCoc() {
        when(sensorCocService.getSensorCocsByOrtung(any())).thenReturn(Collections.emptyList());
        when(technologieFilter.isActive()).thenReturn(true);

        final FachteamKeyFigures fachteamKeyFigures = fachteamKeyFigureService.getFachteamKeyFigures(filter);
        assertThat(fachteamKeyFigures.getKeyFigures(), hasSize(1));
    }

    @Test
    void getFachteamKeyFiguresResultSizeForInactiveTechnologieFilter() {
        when(technologieFilter.isActive()).thenReturn(false);

        final FachteamKeyFigures fachteamKeyFigures = fachteamKeyFigureService.getFachteamKeyFigures(filter);
        assertThat(fachteamKeyFigures.getKeyFigures(), hasSize(1));
    }
}