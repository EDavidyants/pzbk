package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTransitionAnforderungProzessbaukastenExitServiceTest {

    @Mock
    private ReportingStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Mock
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Mock
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Mock
    private ReportingStatusTransitionAnforderungEntyDateService entyDateService;
    @Mock
    private ReportingStatusTransitionAnforderungStatusChangeService anforderungStatusChangeService;
    @InjectMocks
    private ReportingStatusTransitionAnforderungProzessbaukastenExitService anforderungProzessbaukastenExitService;

    @Mock
    private Anforderung anforderung;

    private ReportingStatusTransition reportingStatusTransition;
    private ReportingStatusTransition newReportingStatusTransition;

    @Mock
    private EntryExitTimeStamp exitTimeStamp;
    @Mock
    private EntryExitTimeStamp updatedTimeStamp;
    @Mock
    private Date date;

    private Date entryDate;

    @BeforeEach
    void setUp() {
        reportingStatusTransition = new ReportingStatusTransition();
        newReportingStatusTransition = new ReportingStatusTransition();
        entryDate = new Date();
        when(statusTransitionDatabaseAdapter.getLatestForAnforderung(anforderung)).thenReturn(Optional.empty());
    }

    @Test
    void removeAnforderungFromProzessbaukastenExitIfProzessbaukastenFreigegebenIsActive() {
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        reportingStatusTransition.setFreigegebenProzessbaukasten(exitTimeStamp);
        anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, Status.A_INARBEIT, date);
        verify(reportingStatusTransitionTimeStampUpdatePort, times(1)).removeExitForTimeStamp(exitTimeStamp);
    }

    @Test
    void removeAnforderungFromProzessbaukastenExitIfProzessbaukastenFreigegebenIsActiveCorrectValueIsSetNoNewRowIsCreated() {
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        reportingStatusTransition.setFreigegebenProzessbaukasten(exitTimeStamp);
        anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, Status.A_INARBEIT, date);
        final EntryExitTimeStamp vereinbarungProzessbaukasten = reportingStatusTransition.getFreigegebenProzessbaukasten();
        assertThat(vereinbarungProzessbaukasten, is(exitTimeStamp));
    }

    @Test
    void removeAnforderungFromProzessbaukastenExitIfProzessbaukastenFreigegebenIsActiveCorrectValueIsSet() {
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        reportingStatusTransition.setFreigegebenProzessbaukasten(exitTimeStamp);
        anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, Status.A_INARBEIT, date);
        verify(reportingStatusTransitionTimeStampUpdatePort, times(1)).removeExitForTimeStamp(exitTimeStamp);
    }

    @Test
    void removeAnforderungFromProzessbaukastenExitIfProzessbaukastenTteamIsActive() {
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        reportingStatusTransition.setTteamProzessbaukasten(exitTimeStamp);
        anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, Status.A_INARBEIT, date);
        verify(reportingStatusTransitionTimeStampUpdatePort, times(1)).removeExitForTimeStamp(exitTimeStamp);
    }

    @Test
    void removeAnforderungFromProzessbaukastenExitIfProzessbaukastenTteamIsActiveCorrectValueIsSetNoNewRowIsCreated() {
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        reportingStatusTransition.setTteamProzessbaukasten(exitTimeStamp);
        anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, Status.A_INARBEIT, date);
        final EntryExitTimeStamp vereinbarungProzessbaukasten = reportingStatusTransition.getTteamProzessbaukasten();
        assertThat(vereinbarungProzessbaukasten, is(exitTimeStamp));
    }

    @Test
    void removeAnforderungFromProzessbaukastenExitIfProzessbaukastenTteamIsActiveCorrectValueIsSet() {
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        reportingStatusTransition.setTteamProzessbaukasten(exitTimeStamp);
        anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, Status.A_INARBEIT, date);
        verify(reportingStatusTransitionTimeStampUpdatePort, times(1)).removeExitForTimeStamp(exitTimeStamp);
    }

    @Test
    void removeAnforderungFromProzessbaukastenExitIfProzessbaukastenFachteamIsActive() {
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        reportingStatusTransition.setFachteamProzessbaukasten(exitTimeStamp);
        anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, Status.A_INARBEIT, date);
        verify(reportingStatusTransitionTimeStampUpdatePort, times(1)).removeExitForTimeStamp(exitTimeStamp);
    }

    @Test
    void removeAnforderungFromProzessbaukastenExitIfProzessbaukastenFachteamIsActiveCorrectValueIsSet() {
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        reportingStatusTransition.setFachteamProzessbaukasten(exitTimeStamp);
        anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, Status.A_INARBEIT, date);
        final EntryExitTimeStamp vereinbarungProzessbaukasten = reportingStatusTransition.getFachteamProzessbaukasten();
        assertThat(vereinbarungProzessbaukasten, is(exitTimeStamp));
    }

    @Test
    void removeAnforderungFromProzessbaukastenExitIfProzessbaukastenVereinbarungIsActive() {
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        reportingStatusTransition.setVereinbarungProzessbaukasten(exitTimeStamp);
        anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, Status.A_INARBEIT, date);
        verify(reportingStatusTransitionTimeStampUpdatePort, times(1)).removeExitForTimeStamp(exitTimeStamp);
    }

    @Test
    void removeAnforderungFromProzessbaukastenExitIfProzessbaukastenVereinbarungIsActiveCorrectValueIsSet() {
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(exitTimeStamp.isActive()).thenReturn(Boolean.TRUE);
        reportingStatusTransition.setVereinbarungProzessbaukasten(exitTimeStamp);
        anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, Status.A_INARBEIT, date);
        final EntryExitTimeStamp vereinbarungProzessbaukasten = reportingStatusTransition.getVereinbarungProzessbaukasten();
        assertThat(vereinbarungProzessbaukasten, is(exitTimeStamp));
    }

    @Test
    void removeAnforderungFromProzessbaukastenEntry() {
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);

        final ReportingStatusTransition statusTransition = anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, Status.A_INARBEIT, date);
        assertThat(statusTransition, is(reportingStatusTransition));
    }

    @ParameterizedTest
    @EnumSource(Status.class)
    void createNewRowForStatus(Status status) {
        switch (status) {
            case A_INARBEIT:
                when(statusTransitionDatabaseAdapter.getLatestForAnforderung(anforderung)).thenReturn(Optional.of(reportingStatusTransition));

                anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, status, date);
                verify(reportingStatusTransitionCreatePort, never()).getNewStatusTransitionForAnforderungWithEntryDate(any(), any());
                break;
            case A_FTABGESTIMMT:
            case A_GELOESCHT:
            case A_PLAUSIB:
            case A_KEINE_WEITERVERFOLG:
            case A_FREIGEGEBEN:
            case A_UNSTIMMIG:
                when(statusTransitionDatabaseAdapter.getLatestForAnforderung(anforderung)).thenReturn(Optional.of(reportingStatusTransition));
                when(anforderungStatusChangeService.updateEntryForStatus(any(), any(), any(), any())).thenReturn(newReportingStatusTransition);

                anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, status, date);
                verify(reportingStatusTransitionCreatePort, never()).getNewStatusTransitionForAnforderungWithEntryDate(any(), any());
                break;
            default:
                when(statusTransitionDatabaseAdapter.getLatestForAnforderung(anforderung)).thenReturn(Optional.of(reportingStatusTransition));
                anforderungProzessbaukastenExitService.removeAnforderungFromProzessbaukasten(anforderung, status, date);
                break;
        }

    }
}
