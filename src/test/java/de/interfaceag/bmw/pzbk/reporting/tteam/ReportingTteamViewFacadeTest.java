package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ContextMocker;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilterService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingTteamViewFacadeTest {

    @Mock
    private ReportingFilterService reportingFilterService;
    @Mock
    private TteamKeyFigureService tteamKeyFigureService;
    @InjectMocks
    private ReportingTteamViewFacade reportingTteamViewFacade;

    @Mock
    private ReportingFilter filter;
    @Mock
    private TteamKeyFigures tteamKeyFigures;

    @BeforeEach
    void setUp() {
        ContextMocker.mockFacesContextForUrlParamterUtils();
        when(reportingFilterService.getReportingFilter(any(), any())).thenReturn(filter);
        when(tteamKeyFigureService.getTteamKeyFigures(any())).thenReturn(tteamKeyFigures);
    }

    @Test
    void getReportingViewDataType() {
        final ReportingTteamViewData reportingViewData = reportingTteamViewFacade.getReportingViewData();
        assertThat(reportingViewData, instanceOf(ReportingTteamViewData.class));
    }

    @Test
    void getReportingViewDataFilter() {
        final ReportingTteamViewData reportingViewData = reportingTteamViewFacade.getReportingViewData();
        final ReportingFilter reportingViewDataFilter = reportingViewData.getFilter();
        assertThat(reportingViewDataFilter, is(filter));
    }

    @Test
    void getReportingViewDataKeyFigures() {
        final ReportingTteamViewData reportingViewData = reportingTteamViewFacade.getReportingViewData();
        final TteamKeyFigures reportingViewDataTteamKeyFigures = reportingViewData.getTteamKeyFigures();
        assertThat(reportingViewDataTteamKeyFigures, is(tteamKeyFigures));
    }
}