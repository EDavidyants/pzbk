package de.interfaceag.bmw.pzbk.reporting.projekt.prozess;

import de.interfaceag.bmw.pzbk.reporting.validation.KeyFigureValidationResult;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectProcessViewDataTest {

    private static final String HEADER__MESSAGE = "Header Message";
    private static final String VALIDATION__MESSAGE = "Validation Message";

    @Mock
    ProjectProcessFilter filter;

    Collection<KeyFigureValidationResult> validationResults;

    @Mock
    KeyFigureValidationResult validationResult;

    private ProjectProcessViewData viewData;

    @BeforeEach
    public void setUp() {
        validationResults = new ArrayList<>();
        validationResults.add(validationResult);

        viewData = ProjectProcessViewData.withFilter(filter)
                .withValidationHeaderMessage(HEADER__MESSAGE)
                .withValidationResults(validationResults)
                .get();
    }

    @Test
    public void testGetValidationMessage() {
        when(validationResult.getValidationMessage()).thenReturn(VALIDATION__MESSAGE);
        String result = viewData.getValidationMessage();
        MatcherAssert.assertThat(VALIDATION__MESSAGE, is(result));
    }

    @Test
    public void testGetValidationHeaderMessage() {
        String result = viewData.getValidationHeaderMessage();
        MatcherAssert.assertThat(HEADER__MESSAGE, is(result));
    }

}
