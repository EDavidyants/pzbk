package de.interfaceag.bmw.pzbk.reporting.statusabgleich;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.shared.dto.Tuple;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class StatusCombinationBuilderTest {

    @Test
    public void testBuildCombinationsOneCombination() {
        Collection<DerivatAnforderungModulStatus> derivatAnforderungModulStatus = Arrays.asList(DerivatAnforderungModulStatus.ABZUSTIMMEN);
        Collection<ZakStatus> zakStatus = Arrays.asList(ZakStatus.DONE);
        Set<Tuple<DerivatAnforderungModulStatus, ZakStatus>> result = StatusCombinationBuilder.buildCombinations(derivatAnforderungModulStatus, zakStatus);
        Assertions.assertEquals(1, result.size());
    }

    @Test
    public void testBuildCombinationsMultipleCombinations() {
        Collection<DerivatAnforderungModulStatus> derivatAnforderungModulStatus
                = Arrays.asList(DerivatAnforderungModulStatus.ABZUSTIMMEN, DerivatAnforderungModulStatus.ANGENOMMEN);
        Collection<ZakStatus> zakStatus = Arrays.asList(ZakStatus.DONE, ZakStatus.EMPTY);
        Set<Tuple<DerivatAnforderungModulStatus, ZakStatus>> result = StatusCombinationBuilder.buildCombinations(derivatAnforderungModulStatus, zakStatus);
        Assertions.assertEquals(4, result.size());
    }

}
