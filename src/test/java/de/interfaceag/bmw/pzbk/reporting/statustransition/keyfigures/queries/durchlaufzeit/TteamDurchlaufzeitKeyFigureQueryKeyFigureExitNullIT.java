package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.queries.durchlaufzeit;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamDurchlaufzeitKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.shared.utils.DateUtils;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class TteamDurchlaufzeitKeyFigureQueryKeyFigureExitNullIT extends AbstractTteamDurchlaufzeitKeyFigureQueryIT {

    @Test
    void computeIdTypeDurchlaufzeitWithEntryExitNull() {
        Anforderung anforderung = new Anforderung();
        anforderung.setId(1L);
        getAnforderungDao().persistAnforderung(anforderung);

        final ReportingStatusTransition newEntry = new ReportingStatusTransition();
        EntryExitTimeStamp abgestimmt = new EntryExitTimeStamp();
        abgestimmt.setEntry(DateUtils.subtractDays(new Date(), 5));
        abgestimmt.setExit(null);
        newEntry.setAbgestimmt(abgestimmt);
        newEntry.setAnforderung(anforderung);
        getReportingStatusTransitionDao().save(newEntry);

        super.commit();
        super.begin();

        final TteamDurchlaufzeitKeyFigure keyFigure = TteamDurchlaufzeitKeyFigureQuery.compute(getFilter(), getEntityManager());
        assertThat(keyFigure.getValue(), is(5));
    }
}