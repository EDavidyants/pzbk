package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapWithSize.aMapWithSize;
import static org.hamcrest.collection.IsMapWithSize.anEmptyMap;
import static org.hamcrest.text.IsEmptyString.emptyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class KeyFigureQueryModulSeTeamFilterTest {


    @Mock
    private ReportingFilter filter;
    @Mock
    private IdSearchFilter modulSeTeamFilter;

    private QueryPart queryPart;

    @BeforeEach
    void setUp() {
        queryPart = new QueryPartDTO();
        when(filter.getModulSeTeamFilter()).thenReturn(modulSeTeamFilter);
    }

    private static void setupActive(IdSearchFilter tteamFilter) {
        when(tteamFilter.isActive()).thenReturn(Boolean.TRUE);
        Set<Long> ids = new HashSet<>();
        ids.add(1L);
        when(tteamFilter.getSelectedIdsAsSet()).thenReturn(ids);
    }

    private void setupInactive() {
        when(modulSeTeamFilter.isActive()).thenReturn(Boolean.FALSE);
    }

    @Test
    void appendNotActive() {
        setupInactive();
        KeyFigureQueryModulSeTeamFilter.append(filter, queryPart);
        final String query = queryPart.getQuery();
        assertThat(query, is(emptyString()));
    }

    @Test
    void appendNotActiveParameterMap() {
        setupInactive();
        KeyFigureQueryModulSeTeamFilter.append(filter, queryPart);
        final Map<String, Object> parameterMap = queryPart.getParameterMap();
        assertThat(parameterMap, is(anEmptyMap()));
    }

    @Test
    void appendActive() {
        setupActive(modulSeTeamFilter);
        KeyFigureQueryModulSeTeamFilter.append(filter, queryPart);
        final String query = queryPart.getQuery();
        assertThat(query, is(" AND u.seTeam.id IN :modulSeTeamIds "));
    }


    @Test
    void appendActiveParameterMap() {
        setupActive(modulSeTeamFilter);
        KeyFigureQueryModulSeTeamFilter.append(filter, queryPart);
        final Map<String, Object> parameterMap = queryPart.getParameterMap();
        assertThat(parameterMap, is(aMapWithSize(1)));
    }

}