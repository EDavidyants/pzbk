package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.filter.AbstractMultiValueFilter;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessFilter;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingUmsetzungsverwaltungKeyFigureDaoTest {

    @Mock
    Query query;
    @Mock
    EntityManager entityManager;
    @InjectMocks
    ProjectReportingUmsetzungsverwaltungKeyFigureDao umsetzungsverwaltungKeyFigureDao;

    @Mock
    Derivat derivat;

    @Mock
    ProjectProcessFilter filter;

    @Mock
    AbstractMultiValueFilter technologieFilter;

    List<Object[]> queryResults;

    @BeforeEach
    public void setUp() {
        Object[] queryResult1 = new Object[2];
        queryResult1[0] = UmsetzungsBestaetigungStatus.OFFEN;
        queryResult1[1] = 1L;

        Object[] queryResult2 = new Object[2];
        queryResult2[0] = UmsetzungsBestaetigungStatus.UMGESETZT;
        queryResult2[1] = 2L;

        Object[] queryResult3 = new Object[2];
        queryResult3[0] = UmsetzungsBestaetigungStatus.BEWERTUNG_NICHT_MOEGLICH;
        queryResult3[1] = 3L;

        Object[] queryResult4 = new Object[2];
        queryResult4[0] = UmsetzungsBestaetigungStatus.NICHT_UMGESETZT;
        queryResult4[1] = 4L;

        Object[] queryResult5 = new Object[2];
        queryResult5[0] = UmsetzungsBestaetigungStatus.NICHT_RELEVANT;
        queryResult5[1] = 5L;

        queryResults = new ArrayList<>();
        queryResults.add(queryResult1);
        queryResults.add(queryResult2);
        queryResults.add(queryResult3);
        queryResults.add(queryResult4);
        queryResults.add(queryResult5);

        when(entityManager.createNativeQuery(any(String.class))).thenReturn(query);
        when(query.getResultList()).thenReturn(queryResults);

    }

    @Test
    public void testGetQueryDataForUmsetzungsverwaltungWithFilter() {
        when(filter.getTechnologieFilter()).thenReturn(technologieFilter);
        when(technologieFilter.isActive()).thenReturn(Boolean.TRUE);
        when(technologieFilter.getSelectedValuestringsAsList()).thenReturn(Arrays.asList("Technologie Gesamtfahrzeug"));
        when(query.getResultList()).thenReturn(queryResults);
        List<Object[]> result = umsetzungsverwaltungKeyFigureDao.getQueryDataForUmsetzungsverwaltung(derivat, KovAPhase.VKBG, filter);
        MatcherAssert.assertThat(result, IsCollectionWithSize.hasSize(5));
    }

}
