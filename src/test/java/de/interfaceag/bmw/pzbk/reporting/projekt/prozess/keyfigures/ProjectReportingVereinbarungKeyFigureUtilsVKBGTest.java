package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.keyfigures;

import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.ProjectProcessAmpelThresholdService;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model.ProjectReportingProcessKeyFigureData;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsMapContaining;
import org.hamcrest.collection.IsMapWithSize;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ProjectReportingVereinbarungKeyFigureUtilsVKBGTest {

    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataL4;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataL5;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataL6;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataL7;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataL8;
    @Mock
    ProjectReportingProcessKeyFigureData ProjectReportingProcessKeyFigureDataL9;

    @InjectMocks
    private final ProjectProcessAmpelThresholdService ampelThresholdService = new ProjectProcessAmpelThresholdService();

    Map<Integer, ProjectReportingProcessKeyFigureData> keyFigureData;

    @BeforeEach
    public void setUp() {
        when(ProjectReportingProcessKeyFigureDataL4.getValue()).thenReturn(4L);
        when(ProjectReportingProcessKeyFigureDataL5.getValue()).thenReturn(5L);
        when(ProjectReportingProcessKeyFigureDataL6.getValue()).thenReturn(6L);
        when(ProjectReportingProcessKeyFigureDataL7.getValue()).thenReturn(7L);
        when(ProjectReportingProcessKeyFigureDataL8.getValue()).thenReturn(8L);
        when(ProjectReportingProcessKeyFigureDataL9.getValue()).thenReturn(9L);

        keyFigureData = new HashMap<>();
        keyFigureData.put(ProjectReportingProcessKeyFigure.L4.getId(), ProjectReportingProcessKeyFigureDataL4);
        keyFigureData.put(ProjectReportingProcessKeyFigure.L5.getId(), ProjectReportingProcessKeyFigureDataL5);
        keyFigureData.put(ProjectReportingProcessKeyFigure.L6.getId(), ProjectReportingProcessKeyFigureDataL6);
        keyFigureData.put(ProjectReportingProcessKeyFigure.L7.getId(), ProjectReportingProcessKeyFigureDataL7);
        keyFigureData.put(ProjectReportingProcessKeyFigure.L8.getId(), ProjectReportingProcessKeyFigureDataL8);
        keyFigureData.put(ProjectReportingProcessKeyFigure.L9.getId(), ProjectReportingProcessKeyFigureDataL9);
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungVKBGResultSize() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungVKBG(keyFigureData, ampelThresholdService.getThresholdForVereinbarungVKBG());
        MatcherAssert.assertThat(result, IsMapWithSize.aMapWithSize(9));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungVKBGContainsL1() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungVKBG(keyFigureData, ampelThresholdService.getThresholdForVereinbarungVKBG());
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L1.getId()));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungVKBGL1Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungVKBG(keyFigureData, ampelThresholdService.getThresholdForVereinbarungVKBG());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L1.getId());
        Long dataValue = data.getValue();
        MatcherAssert.assertThat(dataValue, is(22L));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungVKBGContainsL2() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungVKBG(keyFigureData, ampelThresholdService.getThresholdForVereinbarungVKBG());
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L2.getId()));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungVKBGL2Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungVKBG(keyFigureData, ampelThresholdService.getThresholdForVereinbarungVKBG());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L2.getId());
        Long dataValue = data.getValue();
        MatcherAssert.assertThat(dataValue, is(17L));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungVKBGContainsL3() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungVKBG(keyFigureData, ampelThresholdService.getThresholdForVereinbarungVKBG());
        MatcherAssert.assertThat(result, IsMapContaining.hasKey(ProjectReportingProcessKeyFigure.L3.getId()));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungVKBGL3Value() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungVKBG(keyFigureData, ampelThresholdService.getThresholdForVereinbarungVKBG());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L3.getId());
        Long dataValue = data.getValue();
        MatcherAssert.assertThat(dataValue, is(18L));
    }

    @Test
    public void testComputeSumAndRatioKeyFiguresForVereinbarungVKBGL3Ampel() {
        Map<Integer, ProjectReportingProcessKeyFigureData> result = ProjectReportingVereinbarungKeyFigureUtils.computeSumAndRatioKeyFiguresForVereinbarungVKBG(keyFigureData, ampelThresholdService.getThresholdForVereinbarungVKBG());
        ProjectReportingProcessKeyFigureData data = result.get(ProjectReportingProcessKeyFigure.L3.getId());
        boolean isAmpelGreen = data.isAmpelGreen();
        Assertions.assertFalse(isAmpelGreen);
    }
}
