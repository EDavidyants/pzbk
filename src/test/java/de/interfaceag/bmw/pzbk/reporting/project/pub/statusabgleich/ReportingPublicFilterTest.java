package de.interfaceag.bmw.pzbk.reporting.project.pub.statusabgleich;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.filter.SnapshotFilter;
import de.interfaceag.bmw.pzbk.reporting.project.pub.ReportingPublicFilter;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ReportingPublicFilterTest {

    ReportingPublicFilter filter;

    @Mock
    UrlParameter urlParameter;
    @Mock
    Collection<SnapshotFilter> allSnapshots;

    @BeforeEach
    public void setUp() {
        when(urlParameter.getValue(any())).thenReturn(Optional.of("E20"));
        filter = new ReportingPublicFilter(urlParameter, allSnapshots, Page.REPORTING_PROJECT_PUBLIC_STATUSABGLEICH);
    }

    @Test
    public void testResetPage() {
        String reset = filter.reset();
        assertThat(reset, startsWith("statusabgleich.xhtml"));
    }

    @Test
    public void testResetDerivatFilter() {
        String reset = filter.reset();
        assertThat(reset, containsString("derivat=E20"));
    }

    @Test
    public void testFilterDerivatPage() {
        String reset = filter.filter();
        assertThat(reset, startsWith("statusabgleich.xhtml"));
    }

    @Test
    public void testFilterDerivatFilter() {
        String reset = filter.filter();
        assertThat(reset, containsString("derivat=E20"));
    }

}
