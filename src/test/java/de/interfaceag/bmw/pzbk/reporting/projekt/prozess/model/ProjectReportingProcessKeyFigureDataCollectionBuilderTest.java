package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.model;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProjectReportingProcessKeyFigureDataCollectionBuilderTest {

    private static final ProjectReportingProcessKeyFigure G_1 = ProjectReportingProcessKeyFigure.G1;
    private static final long ONE = 1L;
    private static final long ZERO = 0L;

    private ProjectReportingProcessKeyFigureDataCollectionBuilder builder;

    @BeforeEach
    void setUp() {
        builder = new ProjectReportingProcessKeyFigureDataCollectionBuilder();
    }

    @Test
    void buildWithoutKeyFigure() {
        final ProjectReportingProcessKeyFigureDataCollection result = builder.build();
        final Long value = result.getValueForKeyFigure(G_1);
        MatcherAssert.assertThat(value, CoreMatchers.is(ZERO));
    }

    @Test
    void buildWithKeyFigure() {
        final ProjectReportingProcessKeyFigureData keyFigureData = getKeyFigureData(ONE);
        builder.withKeyFigure(keyFigureData);
        final ProjectReportingProcessKeyFigureDataCollection result = builder.build();
        final Long value = result.getValueForKeyFigure(G_1);
        MatcherAssert.assertThat(value, CoreMatchers.is(ONE));
    }

    @Test
    void buildWithTwoCallsForKeyFigure() {
        final ProjectReportingProcessKeyFigureData keyFigureData1 = getKeyFigureData(ONE);
        final ProjectReportingProcessKeyFigureData keyFigureData2 = getKeyFigureData(ZERO);

        builder.withKeyFigure(keyFigureData1);
        builder.withKeyFigure(keyFigureData2);

        final ProjectReportingProcessKeyFigureDataCollection result2 = builder.build();
        final Long value2 = result2.getValueForKeyFigure(G_1);
        MatcherAssert.assertThat(value2, CoreMatchers.is(ZERO));
    }

    private static ProjectReportingProcessKeyFigureData getKeyFigureData(Long value) {
        return new ProjectReportingProcessKeyFigureData() {
            @Override
            public ProjectReportingProcessKeyFigure getKeyFigure() {
                return G_1;
            }

            @Override
            public Long getValue() {
                return value;
            }

            @Override
            public boolean isAmpelGreen() {
                return false;
            }
        };
    }

}
