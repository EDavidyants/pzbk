package de.interfaceag.bmw.pzbk.reporting.statustransition.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTranstionTimeStampServiceTest {

    @Mock
    private ReportingStatusTransitionTimestampFactory timestampFactory;
    @InjectMocks
    private ReportingStatusTranstionTimeStampService reportingStatusTranstionTimeStampService;
    @Mock
    private Date date;
    @Mock
    private EntryExitTimeStamp timeStamp;
    @Mock
    private EntryExitTimeStamp newTimeStamp;

    @Test
    void updateEntryForTimeStampNull() {
        when(timestampFactory.getNewEntryTimeStamp(any())).thenReturn(newTimeStamp);
        final EntryExitTimeStamp result = reportingStatusTranstionTimeStampService.updateEntryForTimeStamp(null, date);
        assertThat(result, is(newTimeStamp));
    }

    @Test
    void updateEntryForTimeStamp() {
        reportingStatusTranstionTimeStampService.updateEntryForTimeStamp(timeStamp, date);
        verify(timeStamp, times(1)).setEntry(date);
    }

    @Test
    void updateExitForTimeStampNull() {
        when(timestampFactory.getNewExitTimeStamp(any())).thenReturn(newTimeStamp);
        final EntryExitTimeStamp result = reportingStatusTranstionTimeStampService.updateExitForTimeStamp(null, date);
        assertThat(result, is(newTimeStamp));
    }

    @Test
    void updateExitForTimeStamp() {
        reportingStatusTranstionTimeStampService.updateExitForTimeStamp(timeStamp, date);
        verify(timeStamp, times(1)).setExit(date);
    }

}
