package de.interfaceag.bmw.pzbk.reporting.keyfigures;

import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class FachteamUnstimmigKeyFigureTest {

    private final Collection<Long> idsTteamUnstimmig = Arrays.asList(1L, 2L);
    private final Collection<Long> idsVereinbarungUnstimmig = Arrays.asList(2L, 3L, 4L);

    private TteamUnstimmigKeyFigure tteamUnstimmigKeyFigure;
    private VereinbarungUnstimmigKeyFigure vereinbarungUnstimmigKeyFigure;

    @Before
    public void setUp() {
        initKeyFigures(idsTteamUnstimmig, idsVereinbarungUnstimmig);
    }

    private void initKeyFigures(Collection<Long> idsTteamUnstimmig, Collection<Long> idsVereinbarungUnstimmig) {
        tteamUnstimmigKeyFigure = new TteamUnstimmigKeyFigure(idsTteamUnstimmig, 0L);
        vereinbarungUnstimmigKeyFigure = new VereinbarungUnstimmigKeyFigure(idsVereinbarungUnstimmig, 0L);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresAllParamtersNull() throws InvalidDataException {
        FachteamUnstimmigKeyFigure.buildFromKeyFigures(null, null);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresSecondParamterNull() throws InvalidDataException {
        FachteamUnstimmigKeyFigure.buildFromKeyFigures(tteamUnstimmigKeyFigure, null);
    }

    @Test(expected = InvalidDataException.class)
    public void testBuildFromKeyFiguresFirstParamterNull() throws InvalidDataException {
        FachteamUnstimmigKeyFigure.buildFromKeyFigures(null, vereinbarungUnstimmigKeyFigure);
    }

    @Test
    public void testBuildFromKeyFiguresKeyTypeId() throws InvalidDataException {
        FachteamUnstimmigKeyFigure result = FachteamUnstimmigKeyFigure.buildFromKeyFigures(tteamUnstimmigKeyFigure, vereinbarungUnstimmigKeyFigure);
        assertEquals(KeyType.FACHTEAM_ANFORDERUNG_UNSTIMMIG.getId(), result.getKeyTypeId());
    }

    @Test
    public void testBuildFromKeyFiguresCollectionSize() throws InvalidDataException {
        FachteamUnstimmigKeyFigure result = FachteamUnstimmigKeyFigure.buildFromKeyFigures(tteamUnstimmigKeyFigure, vereinbarungUnstimmigKeyFigure);
        assertEquals(5, result.getIdTypeTuples().size());
    }

    @Test
    public void testBuildFromKeyFiguresEmptyCollection() throws InvalidDataException {
        TteamUnstimmigKeyFigure keyFigureWithEmptyCollection = new TteamUnstimmigKeyFigure(Collections.EMPTY_LIST, 0L);
        FachteamUnstimmigKeyFigure result = FachteamUnstimmigKeyFigure.buildFromKeyFigures(keyFigureWithEmptyCollection, vereinbarungUnstimmigKeyFigure);
        assertEquals(3, result.getIdTypeTuples().size());
    }

    @Test
    public void testBuildFromKeyFiguresRuntimeNotNull() throws InvalidDataException {
        FachteamUnstimmigKeyFigure result = FachteamUnstimmigKeyFigure.buildFromKeyFigures(tteamUnstimmigKeyFigure, vereinbarungUnstimmigKeyFigure);
        assertNotNull(result.getRunTime());
    }

    @Test
    public void testBuildFromKeyFiguresRuntime() throws InvalidDataException {
        FachteamUnstimmigKeyFigure result = FachteamUnstimmigKeyFigure.buildFromKeyFigures(tteamUnstimmigKeyFigure, vereinbarungUnstimmigKeyFigure);
        assertEquals(0L, (long) result.getRunTime());
    }

}
