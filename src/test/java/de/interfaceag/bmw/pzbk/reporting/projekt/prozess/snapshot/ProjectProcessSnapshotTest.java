package de.interfaceag.bmw.pzbk.reporting.projekt.prozess.snapshot;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsIterableWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;

@ExtendWith(MockitoExtension.class)
class ProjectProcessSnapshotTest {

    private ProjectProcessSnapshot snapshot;

    @Mock
    private Derivat derivat;
    @Mock
    private Collection<ProjectProcessSnapshotEntry> entries;

    @BeforeEach
    void setUp() {
        snapshot = new ProjectProcessSnapshot();
    }

    @Test
    void testToString() {
        final Date datum = new Date();
        final long id = 1L;

        derivat = TestDataFactory.generateDerivat();
        snapshot = new ProjectProcessSnapshot(derivat, Collections.emptyList());
        snapshot.setId(id);
        snapshot.setDatum(datum);

        final String expected = "ProjectProcessSnapshot{" +
                "id=" + id +
                ", datum=" + datum +
                ", derivat=" + derivat +
                ", entries=[]" +
                '}';

        final String result = snapshot.toString();
        MatcherAssert.assertThat(result, is(expected));
    }

    @Test
    void getId() {
        final Long id = snapshot.getId();
        MatcherAssert.assertThat(id, is(CoreMatchers.nullValue()));
    }

    @Test
    void setId() {
        snapshot.setId(1L);
        final Long id = snapshot.getId();
        MatcherAssert.assertThat(id, is(1L));
    }

    @Test
    void getDatum() {
        final Date datum = snapshot.getDatum();
        MatcherAssert.assertThat(datum, is(CoreMatchers.notNullValue()));
    }

    @Test
    void setDatum() {
        final Date datum1 = new Date();
        snapshot.setDatum(datum1);
        final Date datum = snapshot.getDatum();
        MatcherAssert.assertThat(datum, is(datum1));
    }

    @Test
    void getDerivat() {
        final Derivat derivat = snapshot.getDerivat();
        MatcherAssert.assertThat(derivat, is(CoreMatchers.nullValue()));
    }

    @Test
    void setDerivat() {
        snapshot.setDerivat(derivat);
        final Derivat result = snapshot.getDerivat();
        MatcherAssert.assertThat(result, is(derivat));
    }

    @Test
    void getEntries() {
        final Collection<ProjectProcessSnapshotEntry> entries = snapshot.getEntries();
        MatcherAssert.assertThat(entries, IsIterableWithSize.iterableWithSize(0));
    }

    @Test
    void getConstructorEntries() {
        snapshot = new ProjectProcessSnapshot(derivat, entries);
        final Collection<ProjectProcessSnapshotEntry> result = snapshot.getEntries();
        MatcherAssert.assertThat(result, is(entries));
    }

    @Test
    void getConstructorDerivat() {
        snapshot = new ProjectProcessSnapshot(derivat, entries);
        final Derivat result = snapshot.getDerivat();
        MatcherAssert.assertThat(result, is(derivat));
    }
}