package de.interfaceag.bmw.pzbk.reporting.statustransition.keyfigures.filter;

import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapWithSize.aMapWithSize;
import static org.hamcrest.collection.IsMapWithSize.anEmptyMap;
import static org.hamcrest.text.IsEmptyString.emptyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class KeyFigureQueryWerkFilterTest {

    @Mock
    private ReportingFilter filter;
    @Mock
    private IdSearchFilter werkFilter;

    private QueryPart queryPart;

    @BeforeEach
    void setUp() {
        queryPart = new QueryPartDTO();
        when(filter.getWerkFilter()).thenReturn(werkFilter);
    }

    private static void setupActive(IdSearchFilter filter) {
        when(filter.isActive()).thenReturn(Boolean.TRUE);
        when(filter.getSelectedIdsAsSet()).thenReturn(Collections.emptySet());
    }

    private void setupInactive() {
        when(werkFilter.isActive()).thenReturn(Boolean.FALSE);
    }

    @Test
    void appendNotActive() {
        setupInactive();
        KeyFigureQueryWerkFilter.append(filter, queryPart);
        final String query = queryPart.getQuery();
        assertThat(query, is(emptyString()));
    }

    @Test
    void appendNotActiveParameterMap() {
        setupInactive();
        KeyFigureQueryWerkFilter.append(filter, queryPart);
        final Map<String, Object> parameterMap = queryPart.getParameterMap();
        assertThat(parameterMap, is(anEmptyMap()));
    }

    @Test
    void appendActive() {
        setupActive(werkFilter);
        KeyFigureQueryWerkFilter.append(filter, queryPart);
        final String query = queryPart.getQuery();
        assertThat(query, is(" AND a.werk.id IN :werke "));
    }


    @Test
    void appendActiveParameterMap() {
        setupActive(werkFilter);
        KeyFigureQueryWerkFilter.append(filter, queryPart);
        final Map<String, Object> parameterMap = queryPart.getParameterMap();
        assertThat(parameterMap, is(aMapWithSize(1)));
    }

}