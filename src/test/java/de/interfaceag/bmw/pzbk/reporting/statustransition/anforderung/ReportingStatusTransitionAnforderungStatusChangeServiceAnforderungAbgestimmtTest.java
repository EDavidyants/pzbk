package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionCreatePort;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransitionTimeStampUpdatePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTransitionAnforderungStatusChangeServiceAnforderungAbgestimmtTest {

    @Mock
    private ReportingStatusTransitionCreatePort reportingStatusTransitionCreatePort;
    @Mock
    private ReportingStatusTransitionTimeStampUpdatePort reportingStatusTransitionTimeStampUpdatePort;
    @Mock
    private ReportingStatusTransitionDatabaseAdapter statusTransitionDatabaseAdapter;
    @Mock
    private ReportingStatusTransitionAnforderungEntyDateService entyDateService;

    @InjectMocks
    private ReportingStatusTransitionAnforderungStatusChangeService reportingStatusTransitionAnforderungStatusChangeService;

    @Mock
    private Anforderung anforderung;

    private ReportingStatusTransition reportingStatusTransition;

    @Mock
    private EntryExitTimeStamp exitTimeStamp;
    @Mock
    private EntryExitTimeStamp entryTimeStamp;
    @Mock
    private Date date;

    private Date entryDate;

    @BeforeEach
    void setUp() {
        reportingStatusTransition = new ReportingStatusTransition();
        entryDate = new Date();
        when(entyDateService.getEntryDateForAnforderung(anforderung)).thenReturn(entryDate);
        when(statusTransitionDatabaseAdapter.getLatestForAnforderung(anforderung)).thenReturn(Optional.empty());
        when(reportingStatusTransitionCreatePort.getNewStatusTransitionForAnforderungWithEntryDate(any(), any())).thenReturn(reportingStatusTransition);
        when(reportingStatusTransitionTimeStampUpdatePort.updateExitForTimeStamp(any(), any())).thenReturn(exitTimeStamp);
        when(reportingStatusTransitionTimeStampUpdatePort.updateEntryForTimeStamp(any(), any())).thenReturn(entryTimeStamp);
    }

    @Test
    void changeAnforderungStatusAbgestimmtToPlausibilisiertExit() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_FTABGESTIMMT, Status.A_PLAUSIB, date);
        final EntryExitTimeStamp exit = statusTransition.getAbgestimmt();
        assertThat(exit, is(exitTimeStamp));
    }

    @Test
    void changeAnforderungStatusAbgestimmtToPlausibilisiertEntry() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_FTABGESTIMMT, Status.A_PLAUSIB, date);
        final EntryExitTimeStamp entry = statusTransition.getPlausibilisiert();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void changeAnforderungStatusAbgestimmtToGeloeschtExit() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_FTABGESTIMMT, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp exit = statusTransition.getAbgestimmt();
        assertThat(exit, is(exitTimeStamp));
    }

    @Test
    void changeAnforderungStatusAbgestimmtToGeloeschtEntry() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_FTABGESTIMMT, Status.A_GELOESCHT, date);
        final EntryExitTimeStamp entry = statusTransition.getTteamGeloescht();
        assertThat(entry, is(entryTimeStamp));
    }

    @Test
    void changeAnforderungStatusAbgestimmtToUnstimmigExit() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_FTABGESTIMMT, Status.A_UNSTIMMIG, date);
        final EntryExitTimeStamp exit = statusTransition.getAbgestimmt();
        assertThat(exit, is(exitTimeStamp));
    }

    @Test
    void changeAnforderungStatusAbgestimmtToUnstimmigEntry() {
        final ReportingStatusTransition statusTransition = reportingStatusTransitionAnforderungStatusChangeService.changeAnforderungStatus(anforderung, Status.A_FTABGESTIMMT, Status.A_UNSTIMMIG, date);
        final EntryExitTimeStamp entry = statusTransition.getTteamUnstimmig();
        assertThat(entry, is(entryTimeStamp));
    }

}
