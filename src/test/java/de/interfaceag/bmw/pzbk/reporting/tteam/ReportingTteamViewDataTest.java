package de.interfaceag.bmw.pzbk.reporting.tteam;

import de.interfaceag.bmw.pzbk.reporting.ReportingFilter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
class ReportingTteamViewDataTest {

    @Mock
    private ReportingFilter filter;
    @Mock
    private TteamKeyFigures keyFigures;

    private ReportingTteamViewData viewData;

    @BeforeEach
    void setUp() {
        viewData = new ReportingTteamViewData(keyFigures, filter);
    }

    @Test
    void getTteamKeyFigures() {
        final TteamKeyFigures viewDataTteamKeyFigures = viewData.getTteamKeyFigures();
        assertThat(viewDataTteamKeyFigures, is(keyFigures));
    }

    @Test
    void getFilter() {
        final ReportingFilter viewDataFilter = viewData.getFilter();
        assertThat(viewDataFilter, is(filter));
    }
}