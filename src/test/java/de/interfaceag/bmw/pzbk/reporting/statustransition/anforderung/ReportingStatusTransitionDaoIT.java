package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.EntryExitTimeStamp;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsIterableContaining.hasItem;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ReportingStatusTransitionDaoIT extends IntegrationTest {

    private ReportingStatusTransitionDao reportingStatusTransitionDao = new ReportingStatusTransitionDao();
    private AnforderungDao anforderungDao = new AnforderungDao();

    @BeforeEach
    void setUp() throws IllegalAccessException {

        super.begin();

        // override field entityManager in themenklammerDao with private access
        FieldUtils.writeField(reportingStatusTransitionDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(anforderungDao, "entityManager", super.getEntityManager(), true);

    }

    @Test
    void save() {
        Anforderung anforderung = new Anforderung();
        anforderung.setId(1L);
        anforderungDao.persistAnforderung(anforderung);

        final ReportingStatusTransition newEntry = new ReportingStatusTransition();
        EntryExitTimeStamp abgestimmt = new EntryExitTimeStamp();
        abgestimmt.entry();
        newEntry.setAbgestimmt(abgestimmt);
        newEntry.setAnforderung(anforderung);
        newEntry.setId(2L);
        reportingStatusTransitionDao.save(newEntry);
        super.commit();
        super.begin();

        // test is executed in second place. therefore the generated id is 2!
        // when executing the test alone the id will be 1
        final Optional<ReportingStatusTransition> byId = reportingStatusTransitionDao.getById(2L);
        assertTrue(byId.isPresent());
    }

    @Test
    void saveUpdate() {
        Anforderung anforderung = new Anforderung();
        anforderung.setId(1L);
        anforderungDao.persistAnforderung(anforderung);

        final ReportingStatusTransition newEntry = new ReportingStatusTransition();
        EntryExitTimeStamp abgestimmt = new EntryExitTimeStamp();
        abgestimmt.entry();
        newEntry.setAbgestimmt(abgestimmt);
        newEntry.setAnforderung(anforderung);
        newEntry.setId(2L);
        reportingStatusTransitionDao.save(newEntry);
        super.commit();
        super.begin();

        // update case
        newEntry.setPlausibilisiert(abgestimmt);
        reportingStatusTransitionDao.save(newEntry);

        // test is executed in second place. therefore the generated id is 2!
        // when executing the test alone the id will be 1
        final Optional<ReportingStatusTransition> byId = reportingStatusTransitionDao.getById(2L);
        assertTrue(byId.isPresent());
    }

    @Test
    void remove() {
        Anforderung anforderung = new Anforderung();
        anforderung.setFachId("FachId");
        anforderung.setId(1L);
        anforderungDao.persistAnforderung(anforderung);

        final ReportingStatusTransition newEntry = new ReportingStatusTransition();
        EntryExitTimeStamp abgestimmt = new EntryExitTimeStamp();
        abgestimmt.entry();
        newEntry.setAbgestimmt(abgestimmt);
        newEntry.setAnforderung(anforderung);
        reportingStatusTransitionDao.save(newEntry);


        final Long id = newEntry.getId();
        final Optional<ReportingStatusTransition> byId = reportingStatusTransitionDao.getById(id);
        assertTrue(byId.isPresent());

        reportingStatusTransitionDao.remove(newEntry);
        super.commit();
        super.begin();

        final Optional<ReportingStatusTransition> byIdAfterRemove = reportingStatusTransitionDao.getById(id);
        assertFalse(byIdAfterRemove.isPresent());
    }

    @Test
    void getAllStatustransitionsForAnforderung() {
        Anforderung anforderung = new Anforderung();
        anforderung.setFachId("FachId");
        anforderung.setId(1L);
        anforderungDao.persistAnforderung(anforderung);

        final ReportingStatusTransition newEntry = new ReportingStatusTransition();
        EntryExitTimeStamp abgestimmt = new EntryExitTimeStamp();
        abgestimmt.entry();
        newEntry.setAbgestimmt(abgestimmt);
        newEntry.setAnforderung(anforderung);
        reportingStatusTransitionDao.save(newEntry);

        final List<ReportingStatusTransition> allStatustransitionsForAnforderung = reportingStatusTransitionDao.getAllStatustransitionsForAnforderung(anforderung);
        assertThat(allStatustransitionsForAnforderung, hasSize(1));
        assertThat(allStatustransitionsForAnforderung, hasItem(newEntry));
    }

    @Test
    void getLatestForAnforderung() {
        Anforderung anforderung = new Anforderung();
        anforderung.setId(1L);
        anforderungDao.persistAnforderung(anforderung);

        final ReportingStatusTransition newEntry = new ReportingStatusTransition();
        EntryExitTimeStamp abgestimmt = new EntryExitTimeStamp();
        abgestimmt.entry();
        newEntry.setAbgestimmt(abgestimmt);
        newEntry.setAnforderung(anforderung);
        reportingStatusTransitionDao.save(newEntry);
        final Optional<ReportingStatusTransition> latestForAnforderung = reportingStatusTransitionDao.getLatestForAnforderung(anforderung);
        assertTrue(latestForAnforderung.isPresent());
        assertThat(latestForAnforderung.get(), is(newEntry));
    }

    @Test
    void isHistoryAlreadyExistingBeforeInsert() {
        Anforderung anforderung = new Anforderung();
        anforderung.setFachId("A123");
        anforderung.setVersion(12);
        anforderungDao.persistAnforderung(anforderung);

        final boolean historyAlreadyExisting = reportingStatusTransitionDao.isHistoryAlreadyExisting(anforderung);
        assertFalse(historyAlreadyExisting);
    }

    @Test
    void isHistoryAlreadyExistingAfterInsert() {
        Anforderung anforderung = new Anforderung();
        anforderung.setFachId("FachId");
        anforderung.setId(1L);
        anforderungDao.persistAnforderung(anforderung);

        final ReportingStatusTransition newEntry = new ReportingStatusTransition();
        EntryExitTimeStamp abgestimmt = new EntryExitTimeStamp();
        abgestimmt.entry();
        newEntry.setAbgestimmt(abgestimmt);
        newEntry.setAnforderung(anforderung);
        reportingStatusTransitionDao.save(newEntry);

        final boolean historyAlreadyExisting = reportingStatusTransitionDao.isHistoryAlreadyExisting(anforderung);
        assertTrue(historyAlreadyExisting);
    }
}