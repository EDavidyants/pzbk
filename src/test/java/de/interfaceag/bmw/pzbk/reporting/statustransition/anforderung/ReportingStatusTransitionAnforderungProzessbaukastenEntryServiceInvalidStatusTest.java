package de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.enums.Status;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

@ExtendWith(MockitoExtension.class)
class ReportingStatusTransitionAnforderungProzessbaukastenEntryServiceInvalidStatusTest {

    @InjectMocks
    private ReportingStatusTransitionAnforderungProzessbaukastenEntryService reportingStatusTransitionAnforderungProzessbaukastenEntryService;

    @Mock
    private Anforderung anforderung;
    @Mock
    private Date date;

    @Test
    void addAnforderungToProzessbaukastenKeineWeiterverfolgungEntry() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            reportingStatusTransitionAnforderungProzessbaukastenEntryService.addAnforderungToProzessbaukasten(anforderung, Status.A_KEINE_WEITERVERFOLG, date);
        });
    }

    @Test
    void addAnforderungToProzessbaukastenGeloeschtEntry() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            reportingStatusTransitionAnforderungProzessbaukastenEntryService.addAnforderungToProzessbaukasten(anforderung, Status.A_GELOESCHT, date);
        });
    }
}
