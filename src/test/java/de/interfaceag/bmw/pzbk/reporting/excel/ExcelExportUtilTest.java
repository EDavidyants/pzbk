package de.interfaceag.bmw.pzbk.reporting.excel;

import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.Type;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.KeyType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Assert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

class ExcelExportUtilTest {

    @ParameterizedTest
    @EnumSource(KeyType.class)
    void createExcelExport_withNoData(KeyType keyType) {
        // GIVEN
        String sheetName = keyType.getId() + "_" + keyType.name();
        if (sheetName.length() > 31) {
            sheetName = sheetName.substring(0, 31);
        }

        // WHEN
        Workbook excelExport = ExcelExportUtil.createExcelExport(keyType, new ArrayList<>(), Locale.GERMAN);

        // THEN
        Assert.assertNotNull(excelExport.getSheet(sheetName));
        Assert.assertEquals(sheetName, excelExport.getSheetAt(0).getSheetName());
        Row row = excelExport.getSheetAt(0).getRow(0);
        checkHeaderRow(row, keyType);

        // No data is given
        row = excelExport.getSheetAt(0).getRow(1);
        Assert.assertNull(row);
    }

    @ParameterizedTest
    @EnumSource(KeyType.class)
    void createExcelExport_withData(KeyType keyType) {
        // GIVEN
        String sheetName = keyType.getId() + "_" + keyType.name();
        if (sheetName.length() > 31) {
            sheetName = sheetName.substring(0, 31);
        }

        // WHEN
        Workbook excelExport = ExcelExportUtil.createExcelExport(keyType, getReportingExcelDataDto(), Locale.GERMAN);

        // THEN
        Assert.assertNotNull(excelExport.getSheet(sheetName));
        Assert.assertEquals(sheetName, excelExport.getSheetAt(0).getSheetName());

        Row row = excelExport.getSheetAt(0).getRow(0);
        checkHeaderRow(row, keyType);

        // data is given
        row = excelExport.getSheetAt(0).getRow(1);
        checkDataRow(row, keyType);
    }

    private void checkHeaderRow(Row row, KeyType keyType) {
        Assert.assertNotNull(row);

        Assert.assertEquals("Fach ID", row.getCell(0).getStringCellValue());
        Assert.assertEquals("Anforderungstext", row.getCell(1).getStringCellValue());
        Assert.assertEquals("Kommentare zur Anforderung", row.getCell(2).getStringCellValue());
        Assert.assertEquals("Fachteam", row.getCell(3).getStringCellValue());
        Assert.assertEquals("T-Team", row.getCell(4).getStringCellValue());
        Assert.assertEquals("Status", row.getCell(5).getStringCellValue());
        Assert.assertEquals("Status Änderungsdatum", row.getCell(6).getStringCellValue());
        Assert.assertEquals("Erstellungsdatum", row.getCell(7).getStringCellValue());
        Assert.assertEquals("Änderungsdatum", row.getCell(8).getStringCellValue());

        if (hasDurchlaufzeitAndLanglaufer(keyType)) {
            Assert.assertEquals("Durchlaufzeit", row.getCell(9).getStringCellValue());
            Assert.assertEquals("Langläufer", row.getCell(10).getStringCellValue());
        }

    }

    private static boolean hasDurchlaufzeitAndLanglaufer(KeyType keyType) {
        return keyType == KeyType.FACHTEAM_CURRENT
                || keyType == KeyType.TTEAM_CURRENT
                || keyType == KeyType.VEREINBARUNG_CURRENT;
    }

    private void checkDataRow(Row row, KeyType keyType) {
        Assert.assertNotNull(row);

        Assert.assertEquals("fachId", row.getCell(0).getStringCellValue());
        Assert.assertEquals("anforderungstext", row.getCell(1).getStringCellValue());
        Assert.assertEquals("kommentar", row.getCell(2).getStringCellValue());
        Assert.assertEquals("fachteam", row.getCell(3).getStringCellValue());
        Assert.assertEquals("", row.getCell(4).getStringCellValue());
        Assert.assertEquals("in Arbeit", row.getCell(5).getStringCellValue());
        Assert.assertNotNull(row.getCell(6).getStringCellValue());
        Assert.assertNotNull(row.getCell(7).getStringCellValue());
        Assert.assertNotNull(row.getCell(8).getStringCellValue());

        if (hasDurchlaufzeitAndLanglaufer(keyType)) {
            Assert.assertNotNull(row.getCell(9).getNumericCellValue());
            Assert.assertFalse(row.getCell(10).getBooleanCellValue());
        }

    }

    private List<ReportingExcelDataDto> getReportingExcelDataDto() {
        return Arrays.asList(
                new ReportingExcelDataDto(1L, Type.ANFORDERUNG, "fachId", "anforderungstext", "kommentar", "fachteam", Status.A_INARBEIT, new Date(), new Date(), new Date(), 2L, false),
                new ReportingExcelDataDto(2L, Type.ANFORDERUNG, "fachId", "anforderungstext", "kommentar", "fachteam", Status.A_INARBEIT, new Date(), new Date(), new Date(), 10L, true)
        );
    }
}
