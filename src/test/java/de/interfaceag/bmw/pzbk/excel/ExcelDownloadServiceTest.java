package de.interfaceag.bmw.pzbk.excel;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ExcelDownloadServiceTest {

    @InjectMocks
    private final ExcelDownloadService excelDownloadService = new ExcelDownloadService();

    @Test
    public void downloadExcelExportNullFileName() {
        Workbook workbook = new XSSFWorkbook();
        Assertions.assertFalse(excelDownloadService.downloadExcelExport(null, workbook));
    }

    @Test
    public void downloadExcelExportNullWorkbook() {
        Assertions.assertFalse(excelDownloadService.downloadExcelExport("if", null));
    }

    @Test
    public void downloadExcelExportNull() {
        Assertions.assertFalse(excelDownloadService.downloadExcelExport(null, null));
    }
}
