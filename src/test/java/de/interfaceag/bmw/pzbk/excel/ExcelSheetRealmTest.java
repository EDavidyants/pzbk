package de.interfaceag.bmw.pzbk.excel;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class ExcelSheetRealmTest {

    public ExcelSheetRealmTest() {
    }

    private ExcelSheetRealm createDefaultExcelSheetRealm() {
        ExcelSheetRealm esr = new ExcelSheetRealm("esr");
        List<String> chain1 = new ArrayList<>();
        chain1.add("a1");
        chain1.add("b1");
        chain1.add("c1");
        esr.addChain(chain1);
        List<String> chain2 = new ArrayList<>();
        chain2.add("a2");
        chain2.add("b2");
        chain2.add("c2");
        esr.addChain(chain2);
        return esr;
    }

    /**
     * Test of addChain method, of class ExcelSheetRealm.
     */
    @Test
    public void testAddChain() {
        ExcelSheetRealm esr = new ExcelSheetRealm("esr");
        List<String> chain = new ArrayList<>();
        chain.add("a");
        esr.addChain(chain);
        List<List<String>> chains = esr.getChains();

        assertEquals(1, chains.size());

        assertEquals(1, chains.get(0).size());

        assertEquals("a", chains.get(0).get(0));
    }

    /**
     * Test of getOrthogonalChain method, of class ExcelSheetRealm.
     */
    @Test
    public void testGetOrthogonalChain() {
        ExcelSheetRealm esr = createDefaultExcelSheetRealm();
        List<List<String>> chains = esr.getChains();
        List<String> chain1 = chains.get(0);
        List<String> chain2 = chains.get(1);
        String element0 = chain1.get(0);
        String element1 = chain2.get(0);
        List<String> orthoChain = esr.getOrthogonalChain(0, true);
        assertEquals(element0, orthoChain.get(0));
        assertEquals(element1, orthoChain.get(1));

        ExcelSheetRealm esrWithEmptyElements = new ExcelSheetRealm("esr");
        List<String> chainA = new ArrayList<>();
        List<String> chainB = new ArrayList<>();
        List<String> chainC = new ArrayList<>();
        chainA.add("a");
        chainB.add("");
        chainC.add("c");
        esrWithEmptyElements.addChain(chainA);
        esrWithEmptyElements.addChain(chainB);
        esrWithEmptyElements.addChain(chainC);

        List<String> chainWithEmptyFields = esrWithEmptyElements.getOrthogonalChain(0, true);
        assertEquals(3, chainWithEmptyFields.size());

        List<String> chainWithoutEmptyFields = esrWithEmptyElements.getOrthogonalChain(0, false);
        assertEquals(2, chainWithoutEmptyFields.size());
    }

    /**
     * Test of hashCode method, of class ExcelSheetRealm.
     */
    @Test
    public void testHashCode() {
        ExcelSheetRealm esr1 = createDefaultExcelSheetRealm();
        ExcelSheetRealm esr2 = createDefaultExcelSheetRealm();
        assertTrue(esr1.hashCode() == esr2.hashCode());
    }

    /**
     * Test of equals method, of class ExcelSheetRealm.
     */
    @Test
    public void testEquals() {
        ExcelSheetRealm esr1 = createDefaultExcelSheetRealm();
        ExcelSheetRealm esr2 = createDefaultExcelSheetRealm();
        assertTrue(esr1.equals(esr2));
    }

}
