package de.interfaceag.bmw.pzbk.excel;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.globalsearch.AnforderungListService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author fn
 */
public class ExcelExportAnforderungListTest {

    @InjectMocks
    private final AnforderungListService anforderungListService = new AnforderungListService();

    private Row row;
    private Anforderung anforderung;

    @Before
    public void setup() {

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Suchergebnisse");

        anforderung = TestDataFactory.generateAnforderung();
        row = anforderungListService.createRowForAnforderung(anforderung, sheet.createRow(0));

    }

    @Test
    public void excelExportAnforderungListRowLength() {
        Assertions.assertEquals(23, row.getLastCellNum());
    }

    @Test
    public void excelEportAnforderungListCell0() {
        Assertions.assertEquals("A42 | V1", row.getCell(0).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell1() {
        Assertions.assertEquals("in Arbeit", row.getCell(1).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell2() {
        Assertions.assertEquals("Sen Sor | IF-Lab", row.getCell(2).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell3() {
        Assertions.assertEquals("Wert", row.getCell(3).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell4() {
        Assertions.assertEquals("Stärke", row.getCell(4).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell5() {
        Assertions.assertEquals("Beschreibung der Stärke Schwäche", row.getCell(5).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell6() {
        Assertions.assertEquals("T-Ressort>Ortung>Technologie", row.getCell(6).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell7() {
        Assertions.assertEquals("T-Team Name", row.getCell(7).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell8() {
        Assertions.assertEquals("Beschreibung der Anforderung DE", row.getCell(8).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell9() {
        Assertions.assertEquals(new SimpleDateFormat("yyyy-MM-dd").format(new Date()), row.getCell(9).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell10() {
        Assertions.assertEquals(new SimpleDateFormat("yyyy-MM-dd").format(new Date()), row.getCell(10).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell11() {
        Assertions.assertEquals(new SimpleDateFormat("yyyy-MM-dd").format(new Date()), row.getCell(11).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell12() {
        Assertions.assertEquals("Referenzen", row.getCell(12).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell13() {
        Assertions.assertEquals("Wert", row.getCell(13).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell14() {
        Assertions.assertEquals("Lösungsvorschlag", row.getCell(14).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell15() {
        Assertions.assertEquals("Auswirkung: Wert", row.getCell(15).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell16() {
        Assertions.assertEquals("Kommentar zur Anforderung", row.getCell(16).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell17() {
        Assertions.assertEquals("M42", row.getCell(17).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell18() {
        Assertions.assertEquals("Name > SE-TEAM", row.getCell(18).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell19() {
        Assertions.assertEquals("Werk", row.getCell(19).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell20() {
        Assertions.assertEquals("Ersteller", row.getCell(20).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell21() {
        Assertions.assertEquals("DOORS", row.getCell(21).getStringCellValue());
    }

    @Test
    public void excelEportAnforderungListCell22() {
        Assertions.assertEquals("ZWEI", row.getCell(22).getStringCellValue());
    }
}
