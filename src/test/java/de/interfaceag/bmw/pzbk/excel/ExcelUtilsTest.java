package de.interfaceag.bmw.pzbk.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author chsc
 */
public class ExcelUtilsTest {

    private static final Logger LOG = LoggerFactory.getLogger(ExcelUtilsTest.class);

    private static final String EXCEL_FILE_PATH = "src/main/resources/TestTemplates/ExcelTestExample.xlsx";

    private static final String SHEET_ALLG = "Allgemein";

    private Sheet getTestSheet(String name) {
        Sheet sheet = null;
        try {
            XSSFWorkbook wb = new XSSFWorkbook(EXCEL_FILE_PATH);

            sheet = wb.getSheet(name);
        } catch (IOException ex) {
            LOG.error(null, ex);
        }

        return sheet;
    }

    @Test
    public void rowToString() {
        try {
            XSSFWorkbook wb = new XSSFWorkbook(EXCEL_FILE_PATH);

            Sheet sheet = wb.getSheetAt(0);
            Row row = sheet.getRow(1);

            assertEquals("DiesisteinTestZeile_2Zeile_2", ExcelUtils.rowToString(row, "", false));
            assertEquals("DiesisteinTestZeile_2Zeile_2", ExcelUtils.rowToString(row, null, false));
            assertEquals("Dies ist ein Test Zeile_2 Zeile_2", ExcelUtils.rowToString(row, " ", false));
            assertEquals("Dies,ist,ein,Test,Zeile_2,Zeile_2", ExcelUtils.rowToString(row, ",", false));

            assertEquals("Dies,,ist,,ein,,Test,Zeile_2,,Zeile_2", ExcelUtils.rowToString(row, ",", true));

        } catch (IOException ex) {
            LOG.error(null, ex);
        }
    }

    @Test
    public void rowToStringList() {
        try {
            XSSFWorkbook wb = new XSSFWorkbook(EXCEL_FILE_PATH);

            Sheet sheet = wb.getSheetAt(0);
            Row row = sheet.getRow(1);

            List<String> expected = new ArrayList<>();
            expected.add("Dies");
            expected.add("ist");
            expected.add("ein");
            expected.add("Test");
            expected.add("Zeile_2");
            expected.add("Zeile_2");
            assertEquals(expected, ExcelUtils.rowToStringList(row, false));

            expected.clear();
            expected.add("Dies");
            expected.add("");
            expected.add("ist");
            expected.add("");
            expected.add("ein");
            expected.add("");
            expected.add("Test");
            expected.add("Zeile_2");
            expected.add("");
            expected.add("Zeile_2");
            assertEquals(expected, ExcelUtils.rowToStringList(row, true));

        } catch (IOException ex) {
            LOG.error(null, ex);
        }
    }

    @Test
    public void getCellValueTest() {
        String CELL_STRING = "Test";
        String CELL_INT = "" + 12345;
        String CELL_DOUBLE = "" + 1.2345;
        String CELL_FORMULA = "2+3";

        try {
            boolean isEof = false;

            XSSFWorkbook wb = new XSSFWorkbook(EXCEL_FILE_PATH);

            Sheet sheet = wb.getSheetAt(0);
            Row row = sheet.getRow(0);

            if (row.getCell(0).getCellType() == Cell.CELL_TYPE_BLANK) {
                isEof = true;
            } else {

                LOG.info("String {0} == {1}", new Object[]{CELL_STRING, row.getCell(0).getStringCellValue()});
                assertEquals(CELL_STRING, ExcelUtils.cellToString(row.getCell(0)));

                LOG.info("int {0} == {1}", new Object[]{CELL_INT, ExcelUtils.cellToString(row.getCell(1))});
                assertEquals(CELL_INT, ExcelUtils.cellToString(row.getCell(1)));

                LOG.info("double {0} == {1}", new Object[]{CELL_DOUBLE, ExcelUtils.cellToString(row.getCell(2))});
                assertEquals(CELL_DOUBLE, ExcelUtils.cellToString(row.getCell(2)));

                LOG.info("Formula: {0} == {1}", new Object[]{CELL_FORMULA, row.getCell(4)});
                assertEquals(CELL_FORMULA, row.getCell(4).getCellFormula());
            }

        } catch (IOException ex) {
            LOG.error(null, ex);
        }

        boolean t = true;
        assertTrue(t);
    }

    @Test
    public void loadColumnTest() {
        Sheet sheet = getTestSheet(SHEET_ALLG);
        List<String> columnUe1 = ExcelUtils.loadColumn(7, 0, 0, sheet);
        assertEquals(15, columnUe1.size());
        ExcelUtils.loadColumn(7, 0, sheet);
        assertEquals(15, columnUe1.size());
        columnUe1 = ExcelUtils.loadColumn(7, 1, 0, sheet);
        assertEquals(14, columnUe1.size());
        columnUe1 = ExcelUtils.loadColumn(7, 0, 3, sheet);
        assertEquals(3, columnUe1.size());
    }

    @Test
    public void getColByTagTest() {
        Sheet sheet = getTestSheet(SHEET_ALLG);
        String ueDoppelt = "doppelt";
        String ue1 = "Überschrift_1";
        int anzUe1 = 1;
        int anzUeDoppelt = 2;

        // Teste Anzahl gefundener Spalten
        List<List<String>> colsDoppelt = ExcelUtils.getColByTag(sheet, 0, ueDoppelt, 0);
        assertEquals(anzUeDoppelt, colsDoppelt.size());

        List<List<String>> colsUe1 = ExcelUtils.getColByTag(sheet, 0, ue1, 0);
        assertEquals(anzUe1, colsUe1.size());

        // Teste hole gesamte Spalte
        assertEquals(15, colsUe1.get(0).size());
        // Teste Offset start
        colsUe1 = ExcelUtils.getColByTag(sheet, 0, ue1, 1, 0);
        assertEquals(14, colsUe1.get(0).size());
        // Teste Offset end
        colsUe1 = ExcelUtils.getColByTag(sheet, 0, ue1, 0, 3);
        assertEquals(3, colsUe1.get(0).size());

    }

    @Test
    public void getColNumByTagTest() {
        Sheet sheet = getTestSheet(SHEET_ALLG);
        List<Integer> tags = ExcelUtils.getColNumByTag(sheet, 1, "Test");
        assertEquals(1, tags.size());
        assertEquals(6, tags.get(0).intValue());
    }

    @Test
    public void getRealmsForColumnTest() {
        Sheet sheet = getTestSheet(SHEET_ALLG);
        Collection<ExcelSheetRealm> realms = ExcelUtils.getRealmsForColumn(sheet, "Test", 1);
        assertEquals(2, realms.size());
    }

}
