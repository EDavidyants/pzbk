package de.interfaceag.bmw.pzbk.excel;

import de.interfaceag.bmw.pzbk.enums.ExcelSheetForProd;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;

import java.util.Iterator;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
public class ExcelExportExistingDataAuswirkungTest {

    @InjectMocks
    private final ExcelExportService excelExportService = new ExcelExportService();

    private Sheet sheet;

    @Before
    public void setup() {
        excelExportService.configService = mock(ConfigService.class);
        when(excelExportService.configService.getWertStringByAttribut(any())).thenReturn(TestDataFactory.generateAuswirkungList());

        Workbook resultWorkbook = excelExportService.createRowsForAuswirkungen(new XSSFWorkbook());
        sheet = resultWorkbook.getSheet(ExcelSheetForProd.AUSWIRKUNGEN.getBezeichnung());
    }

    @Test
    public void testExcelStructureRow0Length() {
        Row row = sheet.getRow(0);
        short lastCellNum = row.getLastCellNum();
        Assertions.assertEquals(1, lastCellNum);
    }

    @Test
    public void testExcelStructureRow1Length() {
        Row row = sheet.getRow(1);
        short lastCellNum = row.getLastCellNum();
        Assertions.assertEquals(1, lastCellNum);
    }

    @Test
    public void testExcelStructureLength() {
        int lastRowNum = sheet.getLastRowNum();
        Assertions.assertEquals(1, lastRowNum);
    }

    @Test
    public void testExcelStructureRow0() {
        Row row = sheet.getRow(0);
        Iterator<Cell> iterator = row.cellIterator();
        while (iterator.hasNext()) {
            Cell cell = iterator.next();
            int index = cell.getColumnIndex();
            String cellValue = ExcelUtils.cellToString(cell);
            switch (index) {
                case 0:
                    Assertions.assertEquals("Auswirkung 1", cellValue);
                    break;
                default:
                    Assertions.fail("Cell at " + Integer.toString(index) + " not expected!");
                    break;
            }
        }
    }

    @Test
    public void testExcelStructureRow1() {
        Row row = sheet.getRow(1);
        Iterator<Cell> iterator = row.cellIterator();
        while (iterator.hasNext()) {
            Cell cell = iterator.next();
            int index = cell.getColumnIndex();
            String cellValue = ExcelUtils.cellToString(cell);
            switch (index) {
                case 0:
                    Assertions.assertEquals("Auswirkung 2", cellValue);
                    break;
                default:
                    Assertions.fail("Cell at " + Integer.toString(index) + " not expected!");
                    break;
            }
        }
    }

}
