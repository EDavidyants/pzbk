package de.interfaceag.bmw.pzbk.config;

import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ThemenklammerConfigurationFacadeTest {

    @Mock
    private ThemenklammerService themenklammerService;
    @InjectMocks
    private ThemenklammerConfigurationFacade themenklammerConfigurationFacade;

    @Test
    void saveThemenklammernNullInput() {
        themenklammerConfigurationFacade.saveThemenklammern(null);
        verify(themenklammerService, never()).saveDtos(any());
    }

    @Test
    void saveThemenklammernNonnNllInput() {
        themenklammerConfigurationFacade.saveThemenklammern(Collections.emptyList());
        verify(themenklammerService, times(1)).saveDtos(any());
    }
}