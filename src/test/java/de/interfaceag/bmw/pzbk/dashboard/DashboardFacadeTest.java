package de.interfaceag.bmw.pzbk.dashboard;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigungantrag.arbeitsvorrat.BerechtigungAntragArbeitsvorratService;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.DashboardStep;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

/**
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
class DashboardFacadeTest {

    @Mock
    private BerechtigungAntragArbeitsvorratService berechtigungAntragArbeitsvorratService;
    @Mock
    private DashboardStatusService dashboardStatusService;

    @Mock
    private ConfigService configService;

    @Mock
    private Session session;

    @Mock
    private UserPermissions<BerechtigungDto> userPermissions;

    @InjectMocks
    private DashboardFacade dashboardFacade;

    private Mitarbeiter user;
    private List<Rolle> allUserRollen;

    private DashboardResultDTO stepOne;
    private DashboardResultDTO stepTwo;
    private DashboardResultDTO stepThree;
    private DashboardResultDTO stepFour;
    private DashboardResultDTO stepFive;
    private DashboardResultDTO stepSix;
    private DashboardResultDTO stepSeven;
    private DashboardResultDTO stepEight;
    private DashboardResultDTO stepNine;
    private DashboardResultDTO stepTen;
    private DashboardResultDTO stepEleven;
    private DashboardResultDTO stepTwelve;
    private DashboardResultDTO stepThirteen;
    private DashboardResultDTO stepFourteen;
    private DashboardResultDTO arbeitsvorratBerechtigungAntrag;

    private Collection stepOneMeldungIds;
    private Collection stepTwoMeldungIds;
    private Collection stepFourteenMeldungIds;

    private Collection stepTwoAnforderungIds;
    private Collection stepThreeAnforderungIds;
    private Collection stepFourAnforderungIds;
    private Collection stepFiveAnforderungIds;
    private Collection stepSixAnforderungIds;
    private Collection stepTenAnforderungIds;
    private Collection stepFourteenAnforderungIds;

    private Collection stepElevenDerivatAnforderungModulIds;
    private Collection stepTwelveDerivatAnforderungModulIds;
    private Collection stepThirteenDerivatAnforderungModulIds;
    int bestaetigerZuBestimmenNumber;
    int offeneBestaetigungenNumber;

    @BeforeEach
    void setUp() {
        arbeitsvorratBerechtigungAntrag = new DashboardResultDTO();
        arbeitsvorratBerechtigungAntrag.setCount(42);

        prepareTestData();
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(configService.getVersionFromProperty()).thenReturn("1.20.0");
    }

    @Test
    void testForRoleAdmin() {
        allUserRollen.add(Rolle.ADMIN);
        when(userPermissions.getRollen()).thenReturn(allUserRollen);
        DashboardViewData viewData = dashboardFacade.getViewData();
        Assertions.assertEquals("1.20.0", viewData.getVersion());
        Assertions.assertTrue(viewData.getSteps().isEmpty());
    }

    @Test
    void testForRoleTteamMitglied() {
        allUserRollen.add(Rolle.TTEAMMITGLIED);
        when(userPermissions.getRollen()).thenReturn(allUserRollen);
        DashboardViewData viewData = dashboardFacade.getViewData();
        Assertions.assertEquals("1.20.0", viewData.getVersion());
        Assertions.assertTrue(viewData.getSteps().isEmpty());
    }

    @Test
    void testForRoleSensor() {
        allUserRollen.add(Rolle.SENSOR);
        when(userPermissions.getRollen()).thenReturn(allUserRollen);

        buildStepOne();
        buildStepTwo();
        buildStepThree();
        buildStepFour();

        doReturn(stepOne).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG, allUserRollen);
        doReturn(stepTwo).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG, allUserRollen);
        doReturn(stepThree).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT, allUserRollen);
        doReturn(stepFour).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN, allUserRollen);

        DashboardViewData viewData = dashboardFacade.getViewData();

        // Assertions
        Assertions.assertEquals("1.20.0", viewData.getVersion());
        Assertions.assertEquals(4, viewData.getSteps().size());
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG));
        Assertions.assertEquals(10, viewData.getStepCount(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertEquals(7, viewData.getStepCount(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
    }

    @Test
    void testForRoleSensorEingeschraenkt() {
        allUserRollen.add(Rolle.SENSOR_EINGESCHRAENKT);
        when(userPermissions.getRollen()).thenReturn(allUserRollen);

        buildStepOne();
        buildStepTwo();
        buildStepThree();
        buildStepFour();

        doReturn(stepOne).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG, allUserRollen);
        doReturn(stepTwo).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG, allUserRollen);
        doReturn(stepThree).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT, allUserRollen);
        doReturn(stepFour).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN, allUserRollen);

        DashboardViewData viewData = dashboardFacade.getViewData();

        // Assertions
        Assertions.assertEquals("1.20.0", viewData.getVersion());
        Assertions.assertEquals(4, viewData.getSteps().size());
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG));
        Assertions.assertEquals(10, viewData.getStepCount(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertEquals(7, viewData.getStepCount(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
    }

    @Test
    void testForRoleSensorCocLeiter() {
        allUserRollen.add(Rolle.SENSORCOCLEITER);
        when(userPermissions.getRollen()).thenReturn(allUserRollen);

        buildStepOne();
        buildStepTwo();
        buildStepThree();
        buildStepFour();
        buildStepFive();
        buildStepSix();
        buildStepEight();
        buildStepNine();
        buildStepFourteen();

        doReturn(stepOne).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG, allUserRollen);
        doReturn(stepTwo).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG, allUserRollen);
        doReturn(stepThree).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT, allUserRollen);
        doReturn(stepFour).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN, allUserRollen);
        doReturn(stepFive).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_VEREINBART, allUserRollen);
        doReturn(stepSix).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_UMGESETZT, allUserRollen);
        doReturn(stepEight).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.BESTAETIGER_ZUORDNEN, allUserRollen);
        doReturn(stepNine).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.UMSETZUNG_BESTAETIGEN, allUserRollen);
        doReturn(stepFourteen).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.LANGLAEUFER, allUserRollen);
        doReturn(arbeitsvorratBerechtigungAntrag).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.BERECHTIGUNG_ANTRAG, allUserRollen);

        DashboardViewData viewData = dashboardFacade.getViewData();

        // Assertions
        Assertions.assertEquals("1.20.0", viewData.getVersion());
        Assertions.assertEquals(10, viewData.getSteps().size());
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_VEREINBART));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_UMGESETZT));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.BESTAETIGER_ZUORDNEN));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.UMSETZUNG_BESTAETIGEN));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.LANGLAEUFER));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.BERECHTIGUNG_ANTRAG));

        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG));
        Assertions.assertEquals(10, viewData.getStepCount(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertEquals(7, viewData.getStepCount(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_VEREINBART));
        Assertions.assertEquals(2, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_UMGESETZT));
        Assertions.assertEquals(2, viewData.getStepCount(DashboardStep.BESTAETIGER_ZUORDNEN));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.UMSETZUNG_BESTAETIGEN));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.LANGLAEUFER));
        Assertions.assertEquals(42, viewData.getStepCount(DashboardStep.BERECHTIGUNG_ANTRAG));
    }

    @Test
    void testForRoleSensorCocVertreter() {
        allUserRollen.add(Rolle.SCL_VERTRETER);
        when(userPermissions.getRollen()).thenReturn(allUserRollen);


        buildStepOne();
        buildStepTwo();
        buildStepThree();
        buildStepFour();
        buildStepFive();
        buildStepSix();
        buildStepEight();
        buildStepNine();
        buildStepFourteen();

        doReturn(stepOne).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG, allUserRollen);
        doReturn(stepTwo).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG, allUserRollen);
        doReturn(stepThree).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT, allUserRollen);
        doReturn(stepFour).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN, allUserRollen);
        doReturn(stepFive).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_VEREINBART, allUserRollen);
        doReturn(stepSix).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_UMGESETZT, allUserRollen);
        doReturn(stepEight).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.BESTAETIGER_ZUORDNEN, allUserRollen);
        doReturn(stepNine).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.UMSETZUNG_BESTAETIGEN, allUserRollen);
        doReturn(stepFourteen).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.LANGLAEUFER, allUserRollen);
        doReturn(arbeitsvorratBerechtigungAntrag).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.BERECHTIGUNG_ANTRAG, allUserRollen);

        DashboardViewData viewData = dashboardFacade.getViewData();

        // Assertions
        Assertions.assertEquals("1.20.0", viewData.getVersion());
        Assertions.assertEquals(10, viewData.getSteps().size());
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_VEREINBART));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_UMGESETZT));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.BESTAETIGER_ZUORDNEN));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.UMSETZUNG_BESTAETIGEN));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.LANGLAEUFER));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.BERECHTIGUNG_ANTRAG));

        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG));
        Assertions.assertEquals(10, viewData.getStepCount(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertEquals(7, viewData.getStepCount(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_VEREINBART));
        Assertions.assertEquals(2, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_UMGESETZT));
        Assertions.assertEquals(2, viewData.getStepCount(DashboardStep.BESTAETIGER_ZUORDNEN));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.UMSETZUNG_BESTAETIGEN));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.LANGLAEUFER));
        Assertions.assertEquals(42, viewData.getStepCount(DashboardStep.BERECHTIGUNG_ANTRAG));
    }

    @Test
    void testForRoleUmsetzungsbestaetiger() {
        allUserRollen.add(Rolle.UMSETZUNGSBESTAETIGER);
        when(userPermissions.getRollen()).thenReturn(allUserRollen);

        buildStepNine();
        doReturn(stepNine).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.UMSETZUNG_BESTAETIGEN, allUserRollen);

        DashboardViewData viewData = dashboardFacade.getViewData();

        // Assertions
        Assertions.assertEquals("1.20.0", viewData.getVersion());
        Assertions.assertEquals(1, viewData.getSteps().size());
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.UMSETZUNG_BESTAETIGEN));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.UMSETZUNG_BESTAETIGEN));
    }

    @Test
    void testForRoleECoC() {
        allUserRollen.add(Rolle.E_COC);
        when(userPermissions.getRollen()).thenReturn(allUserRollen);

        buildStepThree();
        buildStepFour();

        doReturn(stepThree).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT, allUserRollen);
        doReturn(stepFour).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN, allUserRollen);

        DashboardViewData viewData = dashboardFacade.getViewData();

        // Assertions
        Assertions.assertEquals("1.20.0", viewData.getVersion());
        Assertions.assertEquals(2, viewData.getSteps().size());
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertEquals(7, viewData.getStepCount(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
    }

    @Test
    void testForRoleTteamLeiter() {
        allUserRollen.add(Rolle.T_TEAMLEITER);
        when(userPermissions.getRollen()).thenReturn(allUserRollen);


        buildStepThree();
        buildStepFour();
        buildStepTen();
        buildStepThirteen();
        buildStepFourteen();

        doReturn(stepThree).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT, allUserRollen);
        doReturn(stepFour).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN, allUserRollen);
        doReturn(stepTen).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET, allUserRollen);
        doReturn(stepThirteen).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK, allUserRollen);
        doReturn(stepFourteen).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.LANGLAEUFER, allUserRollen);
        doReturn(arbeitsvorratBerechtigungAntrag).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.BERECHTIGUNG_ANTRAG, allUserRollen);

        DashboardViewData viewData = dashboardFacade.getViewData();

        // Assertions
        Assertions.assertEquals("1.20.0", viewData.getVersion());
        Assertions.assertEquals(6, viewData.getSteps().size());
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.LANGLAEUFER));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.BERECHTIGUNG_ANTRAG));

        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertEquals(7, viewData.getStepCount(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertEquals(1, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET));
        Assertions.assertEquals(1, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.LANGLAEUFER));
        Assertions.assertEquals(42, viewData.getStepCount(DashboardStep.BERECHTIGUNG_ANTRAG));
    }

    @Test
    void testForRoleTteamVertreter() {
        allUserRollen.add(Rolle.TTEAM_VERTRETER);
        when(userPermissions.getRollen()).thenReturn(allUserRollen);

        buildStepThree();
        buildStepFour();
        buildStepTen();
        buildStepThirteen();
        buildStepFourteen();

        doReturn(stepThree).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT, allUserRollen);
        doReturn(stepFour).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN, allUserRollen);
        doReturn(stepTen).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET, allUserRollen);
        doReturn(stepThirteen).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK, allUserRollen);
        doReturn(stepFourteen).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.LANGLAEUFER, allUserRollen);
        doReturn(arbeitsvorratBerechtigungAntrag).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.BERECHTIGUNG_ANTRAG, allUserRollen);

        DashboardViewData viewData = dashboardFacade.getViewData();

        // Assertions
        Assertions.assertEquals("1.20.0", viewData.getVersion());
        Assertions.assertEquals(6, viewData.getSteps().size());
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.LANGLAEUFER));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.BERECHTIGUNG_ANTRAG));

        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertEquals(7, viewData.getStepCount(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertEquals(1, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET));
        Assertions.assertEquals(1, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK));
        Assertions.assertEquals(4, viewData.getStepCount(DashboardStep.LANGLAEUFER));
        Assertions.assertEquals(42, viewData.getStepCount(DashboardStep.BERECHTIGUNG_ANTRAG));
    }

    @Test
    void testForRoleAnforderer() {
        allUserRollen.add(Rolle.ANFORDERER);
        when(userPermissions.getRollen()).thenReturn(allUserRollen);

        buildStepFour();
        buildStepEleven();
        buildStepTwelve();

        doReturn(stepFour).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN, allUserRollen);
        doReturn(stepEleven).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.NICHT_UMGESETZTEN_ANFORDERUNGEN, allUserRollen);
        doReturn(stepTwelve).when(dashboardStatusService).calculateDashboardStepForSession(DashboardStep.ANFORDERUNGEN_OFFEN_IN_ZAK, allUserRollen);

        DashboardViewData viewData = dashboardFacade.getViewData();

        // Assertions
        Assertions.assertEquals("1.20.0", viewData.getVersion());
        Assertions.assertEquals(3, viewData.getSteps().size());
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.NICHT_UMGESETZTEN_ANFORDERUNGEN));
        Assertions.assertTrue(viewData.getSteps().containsKey(DashboardStep.ANFORDERUNGEN_OFFEN_IN_ZAK));

        Assertions.assertEquals(7, viewData.getStepCount(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertEquals(2, viewData.getStepCount(DashboardStep.NICHT_UMGESETZTEN_ANFORDERUNGEN));
        Assertions.assertEquals(3, viewData.getStepCount(DashboardStep.ANFORDERUNGEN_OFFEN_IN_ZAK));
    }

    private Set<Long> generateIdsFromCollection(Collection<Long> ids) {
        Set<Long> idsAsSet = new HashSet<>();
        Iterator<Long> iterator = ids.iterator();
        while (iterator.hasNext()) {
            Long id = iterator.next();
            idsAsSet.add(id);
        }

        return idsAsSet;
    }

    private void buildStepOne() {
        stepOne = new DashboardResultDTO();
        stepOne.addMeldungIds(generateIdsFromCollection(stepOneMeldungIds));
    }

    private void buildStepTwo() {
        stepTwo = new DashboardResultDTO();
        stepTwo.addMeldungIds(generateIdsFromCollection(stepTwoMeldungIds));
        stepTwo.addAnforderungIds(generateIdsFromCollection(stepTwoAnforderungIds));
    }

    private void buildStepThree() {
        stepThree = new DashboardResultDTO();
        stepThree.addAnforderungIds(generateIdsFromCollection(stepThreeAnforderungIds));
    }

    private void buildStepFour() {
        stepFour = new DashboardResultDTO();
        stepFour.addAnforderungIds(generateIdsFromCollection(stepFourAnforderungIds));
    }

    private void buildStepFive() {
        stepFive = new DashboardResultDTO();
        stepFive.addAnforderungIds(generateIdsFromCollection(stepFiveAnforderungIds));
    }

    private void buildStepSix() {
        stepSix = new DashboardResultDTO();
        stepSix.addAnforderungIds(generateIdsFromCollection(stepSixAnforderungIds));
    }

    private void buildStepEight() {
        stepEight = new DashboardResultDTO();
        stepEight.addBestaetigerZuBestimmen(bestaetigerZuBestimmenNumber);
    }

    private void buildStepNine() {
        stepNine = new DashboardResultDTO();
        stepNine.addOffeneBestaetigungen(offeneBestaetigungenNumber);
    }

    private void buildStepTen() {
        stepTen = new DashboardResultDTO();
        stepTen.addAnforderungIds(generateIdsFromCollection(stepTenAnforderungIds));
    }

    private void buildStepEleven() {
        stepEleven = new DashboardResultDTO();
        stepEleven.addDerivatAnforderungModulIds(generateIdsFromCollection(stepElevenDerivatAnforderungModulIds));
    }

    private void buildStepTwelve() {
        stepTwelve = new DashboardResultDTO();
        stepTwelve.addDerivatAnforderungModulIds(generateIdsFromCollection(stepTwelveDerivatAnforderungModulIds));
    }

    private void buildStepThirteen() {
        stepThirteen = new DashboardResultDTO();
        stepThirteen.addDerivatAnforderungModulIds(generateIdsFromCollection(stepThirteenDerivatAnforderungModulIds));
    }

    private void buildStepFourteen() {
        stepFourteen = new DashboardResultDTO();
        stepFourteen.addMeldungIds(generateIdsFromCollection(stepFourteenMeldungIds));
        stepFourteen.addAnforderungIds(generateIdsFromCollection(stepFourteenAnforderungIds));
    }

    private void prepareTestData() {
        user = TestDataFactory.generateMitarbeiter("Michaela", "Fisher", "Abt.-16", "q666680");
        allUserRollen = new ArrayList<>();

        stepOneMeldungIds = Arrays.asList(1L, 2L, 3L, 4L);
        stepTwoMeldungIds = Arrays.asList(5L, 6L, 7L, 8L, 9L, 10L);
        stepFourteenMeldungIds = Arrays.asList(8L, 9L);

        stepTwoAnforderungIds = Arrays.asList(11L, 12L, 13L, 14L);
        stepThreeAnforderungIds = Arrays.asList(15L, 16L, 17L, 18L);
        stepFourAnforderungIds = Arrays.asList(19L, 20L, 21L, 22L, 23L, 24L, 25L);
        stepFiveAnforderungIds = Arrays.asList(19L, 20L, 21L, 22L);
        stepSixAnforderungIds = Arrays.asList(19L, 20L);
        stepTenAnforderungIds = Collections.singletonList(22L);
        stepFourteenAnforderungIds = Arrays.asList(20L, 21L);

        stepElevenDerivatAnforderungModulIds = Arrays.asList(111L, 112L);
        stepTwelveDerivatAnforderungModulIds = Arrays.asList(114L, 115L, 116L);
        stepThirteenDerivatAnforderungModulIds = Collections.singletonList(117L);

        bestaetigerZuBestimmenNumber = 2;
        offeneBestaetigungenNumber = 4;
    }

}
