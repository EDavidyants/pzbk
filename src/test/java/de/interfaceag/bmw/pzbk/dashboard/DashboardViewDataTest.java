package de.interfaceag.bmw.pzbk.dashboard;

import de.interfaceag.bmw.pzbk.enums.DashboardStep;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author evda
 */
public class DashboardViewDataTest {

    private DashboardViewData dashboardViewData;
    private DashboardViewData.Builder builder;
    private DashboardResultDTO stepData;

    @Before
    public void setUp() {
        builder = DashboardViewData.getBuilder();
        stepData = new DashboardResultDTO();
        Set<Long> anforderungIds = new HashSet();
        anforderungIds.addAll(Arrays.asList(1L, 2L, 3L, 4L));
        stepData.addAnforderungIds(anforderungIds);
    }

    @Test
    public void testVersion() {
        dashboardViewData = builder.withVersion("1.20.0").build();
        Assertions.assertEquals("1.20.0", dashboardViewData.getVersion());
    }

    @Test
    public void testViewPermission() {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Arrays.asList(Rolle.SENSORCOCLEITER));
        dashboardViewData = builder.withViewPermission(viewPermission).build();
        Assertions.assertEquals(viewPermission, dashboardViewData.getViewPermission());
    }

    @Test
    public void testStepsMap() {
        dashboardViewData = builder.forStep(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT, stepData)
                .forStep(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN, stepData).build();
        Map<DashboardStep, DashboardResultWithIDs> steps = dashboardViewData.getSteps();
        Assertions.assertEquals(2, steps.size());
        Assertions.assertTrue(steps.containsKey(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(steps.containsKey(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
    }

    @Test
    public void testGetStepCount() {
        dashboardViewData = builder.forStep(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT, stepData).build();
        Assertions.assertEquals(4, dashboardViewData.getStepCount(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
    }

    @Test
    public void testGetStepData() {
        dashboardViewData = builder.forStep(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT, stepData).build();
        Assertions.assertEquals(stepData, dashboardViewData.getStepData(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
    }

    @Test
    public void testToStringForEmptyStepsMap() {
        dashboardViewData = builder.build();
        String toString = "No relevant Dashboard Steps";
        Assertions.assertEquals(toString, dashboardViewData.toString());
    }

    @Test
    public void testToStringForNotEmptyStepsMap() {
        dashboardViewData = builder.forStep(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT, stepData).build();
        String toString = "Dashboard Steps:\nDashboard Step 3: Abgestimmt | plausibilisiert: 4\n";
        Assertions.assertEquals(toString, dashboardViewData.toString());
    }

}
