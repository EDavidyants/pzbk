package de.interfaceag.bmw.pzbk.dashboard;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungEditDtoImpl;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamVertreterBerechtigung;
import de.interfaceag.bmw.pzbk.berechtigung.dto.UserPermissionsDTO;
import de.interfaceag.bmw.pzbk.dao.StatusUebergangDao;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class DashboardStatusServiceTest {

    @Mock
    private StatusUebergangDao statusUebergangDao;

    @Mock
    private Session session;

    @InjectMocks
    private final DashboardStatusService dashboardStatusService = new DashboardStatusService();

    private List<BerechtigungDto> userBerechtigungen;
    private BerechtigungDto sensorCocBerechtigungSensor;
    private BerechtigungDto sensorCocBerechtigungSensorTwo;
    private BerechtigungDto sensorCocBerechtigungSensorEing;
    private BerechtigungDto sensorCocBerechtigungSCL;
    private BerechtigungDto sensorCocBerechtigungSCLVertreter;
    private BerechtigungDto sensorCocBerechtigungUBOne;
    private BerechtigungDto sensorCocBerechtigungUBTwo;
    private BerechtigungDto zakEinordnungBerechtigungAnforderer;
    private BerechtigungDto derivatBerechtigungAnforderer;
    private BerechtigungDto tteamBerechtigungTTL;
    private BerechtigungDto modulSeTeamBerechtigungEcoc;

    private Mitarbeiter user;
    private List<Rolle> userRollen;
    private UserPermissions<BerechtigungDto> userPermissions;

    private List<Status> statusList;

    @BeforeEach
    public void setUp() {
        userBerechtigungen = new ArrayList<>();
        sensorCocBerechtigungSensor = new BerechtigungEditDtoImpl(3L, "TGF_Bedienkomfort", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SENSOR);
        sensorCocBerechtigungSensorTwo = new BerechtigungEditDtoImpl(4L, "TGF_Antrieb", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SENSOR);
        sensorCocBerechtigungSensorEing = new BerechtigungEditDtoImpl(5L, "TGF_E-Mobilität", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SENSOR_EINGESCHRAENKT);
        sensorCocBerechtigungSCL = new BerechtigungEditDtoImpl(4L, "TGF_Antrieb", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SENSORCOCLEITER);
        sensorCocBerechtigungSCLVertreter = new BerechtigungEditDtoImpl(6L, "TGF_Sitze", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SCL_VERTRETER);
        sensorCocBerechtigungUBOne = new BerechtigungEditDtoImpl(4L, "TGF_Antrieb", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.UMSETZUNGSBESTAETIGER);
        sensorCocBerechtigungUBTwo = new BerechtigungEditDtoImpl(6L, "TGF_Sitze", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.UMSETZUNGSBESTAETIGER);
        zakEinordnungBerechtigungAnforderer = new BerechtigungEditDtoImpl(null, "AK Direkt / AG: Herstellbarkeit SSA", BerechtigungZiel.ZAK_EINORDNUNG, Rechttype.SCHREIBRECHT, Rolle.ANFORDERER);
        derivatBerechtigungAnforderer = new BerechtigungEditDtoImpl(1L, "E20", BerechtigungZiel.DERIVAT, Rechttype.SCHREIBRECHT, Rolle.ANFORDERER);
        tteamBerechtigungTTL = new BerechtigungEditDtoImpl(12L, "AF", BerechtigungZiel.TTEAM, Rechttype.SCHREIBRECHT, Rolle.T_TEAMLEITER);
        modulSeTeamBerechtigungEcoc = new BerechtigungEditDtoImpl(4L, "SeTeam 1", BerechtigungZiel.MODULSETEAM, Rechttype.SCHREIBRECHT, Rolle.E_COC);

        user = TestDataFactory.generateMitarbeiter("Max", "Mustermann", "Abt.-2", "q1111111");
        when(session.getUser()).thenReturn(user);
    }

    @Test
    public void testSensorStepOne() {
        userBerechtigungen.add(sensorCocBerechtigungSensor);
        userBerechtigungen.add(sensorCocBerechtigungSensorTwo);

        statusList = Arrays.asList(Status.M_ENTWURF, Status.M_UNSTIMMIG);

        userRollen = Arrays.asList(Rolle.SENSOR);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getMeldungenForSensorInStatus(statusList, user, Arrays.asList(3L, 4L))).thenReturn(Arrays.asList(123L, 345L, 678L));

        int stepOneAnzahl = dashboardStatusService.getDashboardCountForStep(1);
        Assertions.assertEquals(3, stepOneAnzahl);
    }

    @Test
    public void testSensorStepTwo() {
        userBerechtigungen.add(sensorCocBerechtigungSensor);
        userBerechtigungen.add(sensorCocBerechtigungSensorTwo);

        statusList = Arrays.asList(Status.M_GEMELDET, Status.A_INARBEIT, Status.A_UNSTIMMIG);

        userRollen = Arrays.asList(Rolle.SENSOR);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getMeldungenForSensorInStatus(statusList, user, Arrays.asList(3L, 4L))).thenReturn(Arrays.asList(987L));
        when(statusUebergangDao.getAnforderungenForSensorInStatus(statusList, user, Arrays.asList(3L, 4L))).thenReturn(Arrays.asList(45L, 76L, 88L));

        int stepTwoAnzahl = dashboardStatusService.getDashboardCountForStep(2);
        Assertions.assertEquals(4, stepTwoAnzahl);
    }

    @Test
    public void testSensorStepThree() {
        userBerechtigungen.add(sensorCocBerechtigungSensor);
        userBerechtigungen.add(sensorCocBerechtigungSensorTwo);

        statusList = Arrays.asList(Status.A_PLAUSIB, Status.A_FTABGESTIMMT);

        userRollen = Arrays.asList(Rolle.SENSOR);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForSensorInStatus(statusList, user, Arrays.asList(3L, 4L))).thenReturn(Arrays.asList(11L, 77L, 101L));

        int stepThreeAnzahl = dashboardStatusService.getDashboardCountForStep(3);
        Assertions.assertEquals(3, stepThreeAnzahl);
    }

    @Test
    public void testSensorStepFour() {
        userBerechtigungen.add(sensorCocBerechtigungSensor);
        userBerechtigungen.add(sensorCocBerechtigungSensorTwo);

        statusList = Arrays.asList(Status.A_FREIGEGEBEN);

        userRollen = Arrays.asList(Rolle.SENSOR);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForSensorInStatus(statusList, user, Arrays.asList(3L, 4L))).thenReturn(Arrays.asList(12L, 17L));

        int stepFourAnzahl = dashboardStatusService.getDashboardCountForStep(4);
        Assertions.assertEquals(2, stepFourAnzahl);
    }

    @Test
    public void testSensorEingStepOne() {
        userBerechtigungen.add(sensorCocBerechtigungSensorEing);

        statusList = Arrays.asList(Status.M_ENTWURF, Status.M_UNSTIMMIG);

        userRollen = Arrays.asList(Rolle.SENSOR_EINGESCHRAENKT);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getMeldungenForSensorInStatus(statusList, user, Arrays.asList(5L))).thenReturn(Arrays.asList(123L, 345L, 678L));

        int stepOneAnzahl = dashboardStatusService.getDashboardCountForStep(1);
        Assertions.assertEquals(3, stepOneAnzahl);
    }

    @Test
    public void testSensorEingStepTwo() {
        userBerechtigungen.add(sensorCocBerechtigungSensorEing);

        statusList = Arrays.asList(Status.M_GEMELDET, Status.A_INARBEIT, Status.A_UNSTIMMIG);

        userRollen = Arrays.asList(Rolle.SENSOR_EINGESCHRAENKT);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getMeldungenForSensorInStatus(statusList, user, Arrays.asList(5L))).thenReturn(Arrays.asList(987L));
        when(statusUebergangDao.getAnforderungenForSensorInStatus(statusList, user, Arrays.asList(5L))).thenReturn(Arrays.asList(45L, 76L, 88L));

        int stepTwoAnzahl = dashboardStatusService.getDashboardCountForStep(2);
        Assertions.assertEquals(4, stepTwoAnzahl);
    }

    @Test
    public void testSensorEingStepThree() {
        userBerechtigungen.add(sensorCocBerechtigungSensorEing);

        statusList = Arrays.asList(Status.A_PLAUSIB, Status.A_FTABGESTIMMT);

        userRollen = Arrays.asList(Rolle.SENSOR_EINGESCHRAENKT);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForSensorInStatus(statusList, user, Arrays.asList(5L))).thenReturn(Arrays.asList(11L, 77L, 101L));

        int stepThreeAnzahl = dashboardStatusService.getDashboardCountForStep(3);
        Assertions.assertEquals(3, stepThreeAnzahl);
    }

    @Test
    public void testSensorEingStepFour() {
        userBerechtigungen.add(sensorCocBerechtigungSensorEing);

        statusList = Arrays.asList(Status.A_FREIGEGEBEN);

        userRollen = Arrays.asList(Rolle.SENSOR_EINGESCHRAENKT);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForSensorInStatus(statusList, user, Arrays.asList(5L))).thenReturn(Arrays.asList(12L, 17L));

        int stepFourAnzahl = dashboardStatusService.getDashboardCountForStep(4);
        Assertions.assertEquals(2, stepFourAnzahl);
    }

    @Test
    public void testSCLStepOne() {
        userBerechtigungen.add(sensorCocBerechtigungSCL);
        userBerechtigungen.add(sensorCocBerechtigungUBOne);

        statusList = Arrays.asList(Status.M_ENTWURF, Status.M_UNSTIMMIG);

        userRollen = Arrays.asList(Rolle.SENSORCOCLEITER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getMeldungenForSensorCocLeiterInStatus(statusList, Arrays.asList(4L))).thenReturn(Arrays.asList(123L, 345L, 678L));

        int stepOneAnzahl = dashboardStatusService.getDashboardCountForStep(1);
        Assertions.assertEquals(3, stepOneAnzahl);
    }

    @Test
    public void testSCLStepTwo() {
        userBerechtigungen.add(sensorCocBerechtigungSCL);
        userBerechtigungen.add(sensorCocBerechtigungUBOne);

        statusList = Arrays.asList(Status.M_GEMELDET, Status.A_INARBEIT, Status.A_UNSTIMMIG);

        userRollen = Arrays.asList(Rolle.SENSORCOCLEITER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getMeldungenForSensorCocLeiterInStatus(statusList, Arrays.asList(4L))).thenReturn(Arrays.asList(987L));
        when(statusUebergangDao.getAnforderungenForSensorCocLeiterInStatus(statusList, Arrays.asList(4L))).thenReturn(Arrays.asList(45L, 76L, 88L));

        int stepTwoAnzahl = dashboardStatusService.getDashboardCountForStep(2);
        Assertions.assertEquals(4, stepTwoAnzahl);
    }

    @Test
    public void testSCLStepThree() {
        userBerechtigungen.add(sensorCocBerechtigungSCL);
        userBerechtigungen.add(sensorCocBerechtigungUBOne);

        statusList = Arrays.asList(Status.A_PLAUSIB, Status.A_FTABGESTIMMT);

        userRollen = Arrays.asList(Rolle.SENSORCOCLEITER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForSensorCocLeiterInStatus(statusList, Arrays.asList(4L))).thenReturn(Arrays.asList(11L, 77L, 101L));

        int stepThreeAnzahl = dashboardStatusService.getDashboardCountForStep(3);
        Assertions.assertEquals(3, stepThreeAnzahl);
    }

    @Test
    public void testSCLStepFour() {
        userBerechtigungen.add(sensorCocBerechtigungSCL);
        userBerechtigungen.add(sensorCocBerechtigungUBOne);

        statusList = Arrays.asList(Status.A_FREIGEGEBEN);

        userRollen = Arrays.asList(Rolle.SENSORCOCLEITER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForSensorCocLeiterInStatus(statusList, Arrays.asList(4L))).thenReturn(Arrays.asList(12L, 17L));

        int stepFourAnzahl = dashboardStatusService.getDashboardCountForStep(4);
        Assertions.assertEquals(2, stepFourAnzahl);
    }

    @Test
    public void testSCLStepFive() {
        userBerechtigungen.add(sensorCocBerechtigungSCL);
        userBerechtigungen.add(sensorCocBerechtigungUBOne);

        userRollen = Arrays.asList(Rolle.SENSORCOCLEITER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.processStepFive(Arrays.asList(4L))).thenReturn(Arrays.asList(41L, 32L, 65L, 26L, 48L));

        int stepFiveAnzahl = dashboardStatusService.getDashboardCountForStep(5);
        Assertions.assertEquals(5, stepFiveAnzahl);
    }

    @Test
    public void testSCLStepSix() {
        userBerechtigungen.add(sensorCocBerechtigungSCL);
        userBerechtigungen.add(sensorCocBerechtigungUBOne);

        userRollen = Arrays.asList(Rolle.SENSORCOCLEITER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.processStepSix(Arrays.asList(4L))).thenReturn(Arrays.asList(22L, 55L, 56L));

        int stepSixAnzahl = dashboardStatusService.getDashboardCountForStep(6);
        Assertions.assertEquals(3, stepSixAnzahl);
    }

    @Test
    public void testSCLStepEight() {
        userBerechtigungen.add(sensorCocBerechtigungSCL);
        userBerechtigungen.add(sensorCocBerechtigungUBOne);

        userRollen = Arrays.asList(Rolle.SENSORCOCLEITER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.processStepEight(Arrays.asList(4L))).thenReturn(2);

        int stepEightAnzahl = dashboardStatusService.getDashboardCountForStep(8);
        Assertions.assertEquals(2, stepEightAnzahl);
    }

    @Test
    public void testSCLStepNine() {
        userBerechtigungen.add(sensorCocBerechtigungSCL);
        userBerechtigungen.add(sensorCocBerechtigungUBOne);

        userRollen = Arrays.asList(Rolle.SENSORCOCLEITER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.processStepNine(user, Arrays.asList(4L))).thenReturn(4);

        int stepNineAnzahl = dashboardStatusService.getDashboardCountForStep(9);
        Assertions.assertEquals(4, stepNineAnzahl);
    }

    @Test
    public void testSCLVertreterStepOne() {
        userBerechtigungen.add(sensorCocBerechtigungSCLVertreter);
        userBerechtigungen.add(sensorCocBerechtigungUBTwo);

        statusList = Arrays.asList(Status.M_ENTWURF, Status.M_UNSTIMMIG);

        userRollen = Arrays.asList(Rolle.SCL_VERTRETER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getMeldungenForSensorCocLeiterInStatus(statusList, Arrays.asList(6L))).thenReturn(Arrays.asList(123L, 345L, 678L));

        int stepOneAnzahl = dashboardStatusService.getDashboardCountForStep(1);
        Assertions.assertEquals(3, stepOneAnzahl);
    }

    @Test
    public void testSCLVertreterStepTwo() {
        userBerechtigungen.add(sensorCocBerechtigungSCLVertreter);
        userBerechtigungen.add(sensorCocBerechtigungUBTwo);

        statusList = Arrays.asList(Status.M_GEMELDET, Status.A_INARBEIT, Status.A_UNSTIMMIG);

        userRollen = Arrays.asList(Rolle.SCL_VERTRETER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getMeldungenForSensorCocLeiterInStatus(statusList, Arrays.asList(6L))).thenReturn(Arrays.asList(987L));
        when(statusUebergangDao.getAnforderungenForSensorCocLeiterInStatus(statusList, Arrays.asList(6L))).thenReturn(Arrays.asList(45L, 76L, 88L));

        int stepTwoAnzahl = dashboardStatusService.getDashboardCountForStep(2);
        Assertions.assertEquals(4, stepTwoAnzahl);
    }

    @Test
    public void testSCLVertreterStepThree() {
        userBerechtigungen.add(sensorCocBerechtigungSCLVertreter);
        userBerechtigungen.add(sensorCocBerechtigungUBTwo);

        statusList = Arrays.asList(Status.A_PLAUSIB, Status.A_FTABGESTIMMT);

        userRollen = Arrays.asList(Rolle.SCL_VERTRETER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForSensorCocLeiterInStatus(statusList, Arrays.asList(6L))).thenReturn(Arrays.asList(11L, 77L, 101L));

        int stepThreeAnzahl = dashboardStatusService.getDashboardCountForStep(3);
        Assertions.assertEquals(3, stepThreeAnzahl);
    }

    @Test
    public void testSCLVertreterStepFour() {
        userBerechtigungen.add(sensorCocBerechtigungSCLVertreter);
        userBerechtigungen.add(sensorCocBerechtigungUBTwo);

        statusList = Arrays.asList(Status.A_FREIGEGEBEN);

        userRollen = Arrays.asList(Rolle.SCL_VERTRETER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForSensorCocLeiterInStatus(statusList, Arrays.asList(6L))).thenReturn(Arrays.asList(12L, 17L));

        int stepFourAnzahl = dashboardStatusService.getDashboardCountForStep(4);
        Assertions.assertEquals(2, stepFourAnzahl);
    }

    @Test
    public void testSCLVertreterStepFive() {
        userBerechtigungen.add(sensorCocBerechtigungSCLVertreter);
        userBerechtigungen.add(sensorCocBerechtigungUBTwo);

        userRollen = Arrays.asList(Rolle.SCL_VERTRETER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.processStepFive(Arrays.asList(6L))).thenReturn(Arrays.asList(41L, 32L, 65L, 26L, 48L));

        int stepFiveAnzahl = dashboardStatusService.getDashboardCountForStep(5);
        Assertions.assertEquals(5, stepFiveAnzahl);
    }

    @Test
    public void testSCLVertreterStepSix() {
        userBerechtigungen.add(sensorCocBerechtigungSCLVertreter);
        userBerechtigungen.add(sensorCocBerechtigungUBTwo);

        userRollen = Arrays.asList(Rolle.SCL_VERTRETER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.processStepSix(Arrays.asList(6L))).thenReturn(Arrays.asList(22L, 55L, 56L));

        int stepSixAnzahl = dashboardStatusService.getDashboardCountForStep(6);
        Assertions.assertEquals(3, stepSixAnzahl);
    }

    @Test
    public void testSCLVertreterStepEight() {
        userBerechtigungen.add(sensorCocBerechtigungSCLVertreter);
        userBerechtigungen.add(sensorCocBerechtigungUBTwo);

        userRollen = Arrays.asList(Rolle.SCL_VERTRETER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.processStepEight(Arrays.asList(6L))).thenReturn(2);

        int stepEightAnzahl = dashboardStatusService.getDashboardCountForStep(8);
        Assertions.assertEquals(2, stepEightAnzahl);
    }

    @Test
    public void testSCLVertreterStepNine() {
        userBerechtigungen.add(sensorCocBerechtigungSCLVertreter);
        userBerechtigungen.add(sensorCocBerechtigungUBTwo);

        userRollen = Arrays.asList(Rolle.SCL_VERTRETER, Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.processStepNine(user, Arrays.asList(6L))).thenReturn(4);

        int stepNineAnzahl = dashboardStatusService.getDashboardCountForStep(9);
        Assertions.assertEquals(4, stepNineAnzahl);
    }

    @Test
    public void testUmsetzungsbestaetigerStepNine() {
        userBerechtigungen.add(sensorCocBerechtigungUBTwo);

        userRollen = Arrays.asList(Rolle.UMSETZUNGSBESTAETIGER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.processStepNine(user, Arrays.asList(6L))).thenReturn(4);

        int stepNineAnzahl = dashboardStatusService.getDashboardCountForStep(9);
        Assertions.assertEquals(4, stepNineAnzahl);
    }

    @Test
    public void testTTLStepThree() {
        userBerechtigungen.add(tteamBerechtigungTTL);

        statusList = Arrays.asList(Status.A_PLAUSIB, Status.A_FTABGESTIMMT);

        userRollen = Arrays.asList(Rolle.T_TEAMLEITER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForTteamLeiterInStatus(statusList, Arrays.asList(12L))).thenReturn(Arrays.asList(11L, 77L, 101L));

        int stepThreeAnzahl = dashboardStatusService.getDashboardCountForStep(3);
        Assertions.assertEquals(3, stepThreeAnzahl);
    }

    @Test
    public void testTTLStepFour() {
        userBerechtigungen.add(tteamBerechtigungTTL);

        statusList = Arrays.asList(Status.A_FREIGEGEBEN);

        userRollen = Arrays.asList(Rolle.T_TEAMLEITER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForTteamLeiterInStatus(statusList, Arrays.asList(12L))).thenReturn(Arrays.asList(12L, 17L));

        int stepFourAnzahl = dashboardStatusService.getDashboardCountForStep(4);
        Assertions.assertEquals(2, stepFourAnzahl);
    }

    @Test
    public void testTTLStepTen() {
        userBerechtigungen.add(tteamBerechtigungTTL);

        userRollen = Arrays.asList(Rolle.T_TEAMLEITER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.processStepTen(Arrays.asList(12L))).thenReturn(Arrays.asList(221L, 145L, 147L));

        int stepTenAnzahl = dashboardStatusService.getDashboardCountForStep(10);
        Assertions.assertEquals(3, stepTenAnzahl);
    }

    @Test
    public void testTTLStepThirteen() {
        userBerechtigungen.add(tteamBerechtigungTTL);

        List<ZakStatus> zakStatus = Arrays.asList(ZakStatus.NICHT_UMSETZBAR);

        userRollen = Arrays.asList(Rolle.T_TEAMLEITER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getUmsetzungInZAKForTteamleiter(Arrays.asList(12L), zakStatus)).thenReturn(Arrays.asList(63L, 91L));

        int stepThirteenAnzahl = dashboardStatusService.getDashboardCountForStep(13);
        Assertions.assertEquals(2, stepThirteenAnzahl);
    }

    @Test
    public void testTTLVertreterStepThree() {
        statusList = Arrays.asList(Status.A_PLAUSIB, Status.A_FTABGESTIMMT);

        userRollen = Arrays.asList(Rolle.TTEAM_VERTRETER);
        TteamVertreterBerechtigung ttvBerechtigung = new TteamVertreterBerechtigung(Arrays.asList(9L));
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, ttvBerechtigung);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForTteamLeiterInStatus(statusList, Arrays.asList(9L))).thenReturn(Arrays.asList(11L, 77L, 101L));

        int stepThreeAnzahl = dashboardStatusService.getDashboardCountForStep(3);
        Assertions.assertEquals(3, stepThreeAnzahl);
    }

    @Test
    public void testTTLVertreterStepFour() {
        statusList = Arrays.asList(Status.A_FREIGEGEBEN);

        userRollen = Arrays.asList(Rolle.TTEAM_VERTRETER);
        TteamVertreterBerechtigung ttvBerechtigung = new TteamVertreterBerechtigung(Arrays.asList(9L));
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, ttvBerechtigung);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForTteamLeiterInStatus(statusList, Arrays.asList(9L))).thenReturn(Arrays.asList(12L, 17L));

        int stepFourAnzahl = dashboardStatusService.getDashboardCountForStep(4);
        Assertions.assertEquals(2, stepFourAnzahl);
    }

    @Test
    public void testTTLVertreterStepTen() {
        userRollen = Arrays.asList(Rolle.TTEAM_VERTRETER);
        TteamVertreterBerechtigung ttvBerechtigung = new TteamVertreterBerechtigung(Arrays.asList(9L));
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, ttvBerechtigung);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.processStepTen(Arrays.asList(9L))).thenReturn(Arrays.asList(221L, 145L, 147L));

        int stepTenAnzahl = dashboardStatusService.getDashboardCountForStep(10);
        Assertions.assertEquals(3, stepTenAnzahl);
    }

    @Test
    public void testTTLVertreterStepThirteen() {
        List<ZakStatus> zakStatus = Arrays.asList(ZakStatus.NICHT_UMSETZBAR);

        userRollen = Arrays.asList(Rolle.TTEAM_VERTRETER);
        TteamVertreterBerechtigung ttvBerechtigung = new TteamVertreterBerechtigung(Arrays.asList(9L));
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, ttvBerechtigung);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getUmsetzungInZAKForTteamleiter(Arrays.asList(9L), zakStatus)).thenReturn(Arrays.asList(63L, 91L));

        int stepThirteenAnzahl = dashboardStatusService.getDashboardCountForStep(13);
        Assertions.assertEquals(2, stepThirteenAnzahl);
    }

    @Test
    public void testEcocStepThree() {
        userBerechtigungen.add(modulSeTeamBerechtigungEcoc);

        statusList = Arrays.asList(Status.A_PLAUSIB);

        userRollen = Arrays.asList(Rolle.E_COC);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForEcocInStatus(statusList, Arrays.asList(4L))).thenReturn(Arrays.asList(11L, 77L, 101L));

        int stepThreeAnzahl = dashboardStatusService.getDashboardCountForStep(3);
        Assertions.assertEquals(3, stepThreeAnzahl);
    }

    @Test
    public void testEcocStepFour() {
        userBerechtigungen.add(modulSeTeamBerechtigungEcoc);

        statusList = Arrays.asList(Status.A_FREIGEGEBEN);

        userRollen = Arrays.asList(Rolle.E_COC);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForEcocInStatus(statusList, Arrays.asList(4L))).thenReturn(Arrays.asList(12L, 17L));

        int stepFourAnzahl = dashboardStatusService.getDashboardCountForStep(4);
        Assertions.assertEquals(2, stepFourAnzahl);
    }

    @Test
    public void testAnfordererStepFour() {
        userBerechtigungen.add(zakEinordnungBerechtigungAnforderer);
        userBerechtigungen.add(derivatBerechtigungAnforderer);

        statusList = Arrays.asList(Status.A_FREIGEGEBEN);

        userRollen = Arrays.asList(Rolle.ANFORDERER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        List<String> zakEinordnungen = Arrays.asList("AK Direkt / AG: Herstellbarkeit SSA");

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getAnforderungenForAnfordererInStatus(statusList, zakEinordnungen)).thenReturn(Arrays.asList(12L, 17L));

        int stepFourAnzahl = dashboardStatusService.getDashboardCountForStep(4);
        Assertions.assertEquals(2, stepFourAnzahl);
    }

    @Test
    public void testAnfordererStepEleven() {
        userBerechtigungen.add(zakEinordnungBerechtigungAnforderer);
        userBerechtigungen.add(derivatBerechtigungAnforderer);

        userRollen = Arrays.asList(Rolle.ANFORDERER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        List<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatus = Arrays.asList(UmsetzungsBestaetigungStatus.NICHT_UMGESETZT);
        List<String> zakEinordnungen = Arrays.asList("AK Direkt / AG: Herstellbarkeit SSA");
        List<Long> derivatIds = Arrays.asList(1L);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getUmsetzungInZAKForAnfordererByUBStatus(zakEinordnungen, derivatIds, umsetzungsBestaetigungStatus)).thenReturn(Arrays.asList(18L, 19L));

        int stepElevenAnzahl = dashboardStatusService.getDashboardCountForStep(11);
        Assertions.assertEquals(2, stepElevenAnzahl);
    }

    @Test
    public void testAnfordererStepTwelve() {
        userBerechtigungen.add(zakEinordnungBerechtigungAnforderer);
        userBerechtigungen.add(derivatBerechtigungAnforderer);

        userRollen = Arrays.asList(Rolle.ANFORDERER);
        userPermissions = new UserPermissionsDTO<>(userRollen, userBerechtigungen, null, null);

        List<ZakStatus> zakStatus = Arrays.asList(ZakStatus.PENDING);
        List<String> zakEinordnungen = Arrays.asList("AK Direkt / AG: Herstellbarkeit SSA");
        List<Long> derivatIds = Arrays.asList(1L);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(statusUebergangDao.getUmsetzungInZAKForAnfordererByZakStatus(zakEinordnungen, derivatIds, zakStatus)).thenReturn(Arrays.asList(21L, 24L));

        int stepTwelveAnzahl = dashboardStatusService.getDashboardCountForStep(12);
        Assertions.assertEquals(2, stepTwelveAnzahl);
    }

}
