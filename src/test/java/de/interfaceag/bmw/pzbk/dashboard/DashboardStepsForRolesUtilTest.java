package de.interfaceag.bmw.pzbk.dashboard;

import de.interfaceag.bmw.pzbk.enums.DashboardStep;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

/**
 * @author evda
 */
public class DashboardStepsForRolesUtilTest {

    @Test
    public void testRelevantDashboardStepsForAdmin() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Collections.singletonList(Rolle.ADMIN));
        Assertions.assertEquals(1, steps.size());
        Assertions.assertTrue(steps.contains(DashboardStep.BERECHTIGUNG_ANTRAG));
    }

    @Test
    public void testRelevantDashboardStepsForTteamMitglied() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Collections.singletonList(Rolle.TTEAMMITGLIED));
        Assertions.assertTrue(steps.isEmpty());
    }

    @Test
    public void testRelevantDashboardStepsForSensor() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Collections.singletonList(Rolle.SENSOR));
        Assertions.assertEquals(4, steps.size());
        Assertions.assertTrue(steps.contains(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG));
        Assertions.assertTrue(steps.contains(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(steps.contains(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));

    }

    @Test
    public void testRelevantDashboardStepsForSensorEingeschraenkt() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Collections.singletonList(Rolle.SENSOR_EINGESCHRAENKT));
        Assertions.assertEquals(4, steps.size());
        Assertions.assertTrue(steps.contains(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG));
        Assertions.assertTrue(steps.contains(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(steps.contains(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
    }

    @Test
    public void testRelevantDashboardStepsForSensorCocLeiter() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Arrays.asList(Rolle.SENSORCOCLEITER, Rolle.UMSETZUNGSBESTAETIGER));
        Assertions.assertEquals(10, steps.size());
        Assertions.assertTrue(steps.contains(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG));
        Assertions.assertTrue(steps.contains(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(steps.contains(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_VEREINBART));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_UMGESETZT));
        Assertions.assertTrue(steps.contains(DashboardStep.BESTAETIGER_ZUORDNEN));
        Assertions.assertTrue(steps.contains(DashboardStep.UMSETZUNG_BESTAETIGEN));
        Assertions.assertTrue(steps.contains(DashboardStep.LANGLAEUFER));
        Assertions.assertTrue(steps.contains(DashboardStep.BERECHTIGUNG_ANTRAG));
    }

    @Test
    public void testRelevantDashboardStepsForSensorCocVertreter() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Arrays.asList(Rolle.SCL_VERTRETER, Rolle.UMSETZUNGSBESTAETIGER));
        Assertions.assertEquals(10, steps.size());
        Assertions.assertTrue(steps.contains(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG));
        Assertions.assertTrue(steps.contains(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(steps.contains(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_VEREINBART));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_UMGESETZT));
        Assertions.assertTrue(steps.contains(DashboardStep.BESTAETIGER_ZUORDNEN));
        Assertions.assertTrue(steps.contains(DashboardStep.UMSETZUNG_BESTAETIGEN));
        Assertions.assertTrue(steps.contains(DashboardStep.LANGLAEUFER));
        Assertions.assertTrue(steps.contains(DashboardStep.BERECHTIGUNG_ANTRAG));
    }

    @Test
    public void testRelevantDashboardStepsForSensorCocVertreterAndSensor() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Arrays.asList(Rolle.SENSOR, Rolle.SCL_VERTRETER, Rolle.UMSETZUNGSBESTAETIGER));
        Assertions.assertEquals(10, steps.size());
        Assertions.assertTrue(steps.contains(DashboardStep.MELDUNGEN_IN_ENTWURF_OR_UNSTIMMIG));
        Assertions.assertTrue(steps.contains(DashboardStep.GEMELDETEN_MELDUNGEN_OR_ANFORDERUNGEN_IN_ARBEIT_OR_UNSTIMMIG));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(steps.contains(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_VEREINBART));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_UMGESETZT));
        Assertions.assertTrue(steps.contains(DashboardStep.BESTAETIGER_ZUORDNEN));
        Assertions.assertTrue(steps.contains(DashboardStep.UMSETZUNG_BESTAETIGEN));
        Assertions.assertTrue(steps.contains(DashboardStep.LANGLAEUFER));
        Assertions.assertTrue(steps.contains(DashboardStep.BERECHTIGUNG_ANTRAG));
    }

    @Test
    public void testRelevantDashboardStepsForUmsetzungsbestaetiger() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Collections.singletonList(Rolle.UMSETZUNGSBESTAETIGER));
        Assertions.assertEquals(1, steps.size());
        Assertions.assertTrue(steps.contains(DashboardStep.UMSETZUNG_BESTAETIGEN));
    }

    @Test
    public void testRelevantDashboardStepsForECoC() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Collections.singletonList(Rolle.E_COC));
        Assertions.assertEquals(2, steps.size());
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(steps.contains(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
    }

    @Test
    public void testRelevantDashboardStepsForTteamLeiter() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Collections.singletonList(Rolle.T_TEAMLEITER));
        Assertions.assertEquals(6, steps.size());
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(steps.contains(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK));
        Assertions.assertTrue(steps.contains(DashboardStep.LANGLAEUFER));
        Assertions.assertTrue(steps.contains(DashboardStep.BERECHTIGUNG_ANTRAG));
    }

    @Test
    public void testRelevantDashboardStepsForTteamVertreter() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Collections.singletonList(Rolle.TTEAM_VERTRETER));
        Assertions.assertEquals(6, steps.size());
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(steps.contains(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK));
        Assertions.assertTrue(steps.contains(DashboardStep.LANGLAEUFER));
        Assertions.assertTrue(steps.contains(DashboardStep.BERECHTIGUNG_ANTRAG));
    }

    @Test
    public void testRelevantDashboardStepsForTteamLeiterAndECoC() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Arrays.asList(Rolle.E_COC, Rolle.T_TEAMLEITER));
        Assertions.assertEquals(6, steps.size());
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(steps.contains(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK));
        Assertions.assertTrue(steps.contains(DashboardStep.LANGLAEUFER));
        Assertions.assertTrue(steps.contains(DashboardStep.BERECHTIGUNG_ANTRAG));
    }

    @Test
    public void testRelevantDashboardStepsForAnforderer() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Collections.singletonList(Rolle.ANFORDERER));
        Assertions.assertEquals(3, steps.size());
        Assertions.assertTrue(steps.contains(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertTrue(steps.contains(DashboardStep.NICHT_UMGESETZTEN_ANFORDERUNGEN));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_OFFEN_IN_ZAK));
    }

    @Test
    public void testRelevantDashboardStepsForAnfordererAndTteamLeiter() {
        Set<DashboardStep> steps = DashboardStepsForRolesUtil.getDashbaordStepsForRoles(Arrays.asList(Rolle.T_TEAMLEITER, Rolle.ANFORDERER));
        Assertions.assertEquals(8, steps.size());
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_IN_ABGESTIMMT_OR_PLAUSIBILISIERT));
        Assertions.assertTrue(steps.contains(DashboardStep.FREIGEGEBENEN_ANFORDERUNGEN));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_WITH_UNZUREICHENDE_ANFORDERUNGSQUALITAET));
        Assertions.assertTrue(steps.contains(DashboardStep.NICHT_UMGESETZTEN_ANFORDERUNGEN));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_OFFEN_IN_ZAK));
        Assertions.assertTrue(steps.contains(DashboardStep.ANFORDERUNGEN_ABGELEHNT_IN_ZAK));
        Assertions.assertTrue(steps.contains(DashboardStep.LANGLAEUFER));
        Assertions.assertTrue(steps.contains(DashboardStep.BERECHTIGUNG_ANTRAG));

    }

}
