package de.interfaceag.bmw.pzbk.dashboard;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.Arrays;
import java.util.Collections;

class DashboardStepViewPermissionTest {

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testMenuButtonVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getMenuButton();
        Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht MenuButton nicht!");
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepOneToFourPanelVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepOneToFourPanel();

        switch (role) {
            case ADMIN:
            case UMSETZUNGSBESTAETIGER:
            case LESER:
            case TTEAMMITGLIED:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepOneToFourPanel!");
                break;
            default:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepOneToFourPanel nicht!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepOneVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepOne();

        switch (role) {
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepOne nicht!");
                break;
            default:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepOne!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepTwoVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepTwo();

        switch (role) {
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepTwo nicht!");
                break;
            default:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepTwo!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepThreeVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Arrays.asList(role));
        boolean isRendered = viewPermission.getStepThree();

        switch (role) {
            case UMSETZUNGSBESTAETIGER:
            case ANFORDERER:
            case ADMIN:
            case LESER:
            case TTEAMMITGLIED:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepThree!");
                break;
            default:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepThree nicht!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepFourVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepFour();

        switch (role) {
            case UMSETZUNGSBESTAETIGER:
            case ADMIN:
            case LESER:
            case TTEAMMITGLIED:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepFour!");
                break;
            default:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepFour nicht!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepFiveToSevenPanelVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepFiveToSevenPanel();

        switch (role) {
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepFiveToSevenPanel nicht!");
                break;
            default:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepFiveToSevenPanel!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepFiveVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepFive();

        switch (role) {
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepFive nicht!");
                break;
            default:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepFive!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepSixVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepSix();

        switch (role) {
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepSix nicht!");
                break;
            default:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepSix!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepEightToThirteenPanelVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepEightToThirteenPanel();

        switch (role) {
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case UMSETZUNGSBESTAETIGER:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case ANFORDERER:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepEightToThirteenPanel nicht!");
                break;
            default:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepEightToThirteenPanel!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepEightVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepEight();

        switch (role) {
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepEight nicht!");
                break;
            default:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepEight!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepNineVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepNine();

        switch (role) {
            case UMSETZUNGSBESTAETIGER:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepNine nicht!");
                break;
            default:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepNine!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepTenVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepTen();

        switch (role) {
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepTen nicht!");
                break;
            default:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepTen!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepElevenVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepEleven();

        switch (role) {
            case ANFORDERER:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepEleven nicht!");
                break;
            default:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepEleven!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepTwelveVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepTwelve();

        switch (role) {
            case ANFORDERER:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepTwelve nicht!");
                break;
            default:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepTwelve!");
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void testStepThirteenVisible(Rolle role) {
        DashboardViewPermission viewPermission = new DashboardViewPermission(Collections.singletonList(role));
        boolean isRendered = viewPermission.getStepThirteen();

        switch (role) {
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
                Assertions.assertTrue(isRendered, "Fehler: Rolle " + role.name() + " sieht StepThirteen nicht!");
                break;
            default:
                Assertions.assertFalse(isRendered, "Fehler: Rolle " + role.name() + " sieht StepThirteen!");
                break;
        }
    }

}
