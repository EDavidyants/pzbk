package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung;

import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterAntragDto;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragMitarbeiterConverterTest {

    private static final long ANTRAG_ID = 42L;
    private static final Rolle ROLLE = Rolle.SENSORCOCLEITER;
    private static final Rechttype RECHTTYPE = Rechttype.SCHREIBRECHT;
    private static final Date erstellungsdatum = new Date(2019, 1,1);

    @Mock
    private Berechtigungsantrag berechtigungsantrag;

    @Mock
    private SensorCoc sensorCoc;
    @Mock
    private Tteam tteam;
    @Mock
    private ModulSeTeam modulSeTeam;

    @BeforeEach
    void setUp() {
        when(berechtigungsantrag.getId()).thenReturn(ANTRAG_ID);
        when(berechtigungsantrag.getRolle()).thenReturn(ROLLE);
        when(berechtigungsantrag.getRechttype()).thenReturn(RECHTTYPE);
        when(berechtigungsantrag.getErstellungsdatum()).thenReturn(erstellungsdatum);
    }

    @Test
    void convertToBerechtigungAntragMitarbeiterErstellungsdatumValue() {
        final BerechtigungAntragMitarbeiterAntragDto result = BerechtigungAntragMitarbeiterConverter.convertToBerechtigungAntragMitarbeiter(berechtigungsantrag);
        assertThat(result.getErstellungsdatum(), is(erstellungsdatum));
    }

    @Test
    void convertToBerechtigungAntragMitarbeiterKommentarValue() {
        final BerechtigungAntragMitarbeiterAntragDto result = BerechtigungAntragMitarbeiterConverter.convertToBerechtigungAntragMitarbeiter(berechtigungsantrag);
        assertThat(result.getKommentar(), is(nullValue()));
    }

    @Test
    void convertToBerechtigungAntragMitarbeiterAntragIdValue() {
        final BerechtigungAntragMitarbeiterAntragDto result = BerechtigungAntragMitarbeiterConverter.convertToBerechtigungAntragMitarbeiter(berechtigungsantrag);
        assertThat(result.getBerechtigungAntragId(), is(ANTRAG_ID));
    }

    @Test
    void convertToBerechtigungAntragMitarbeiterRolleValue() {
        final BerechtigungAntragMitarbeiterAntragDto result = BerechtigungAntragMitarbeiterConverter.convertToBerechtigungAntragMitarbeiter(berechtigungsantrag);
        assertThat(result.getRolle(), is(ROLLE));
    }

    @Test
    void convertToBerechtigungAntragMitarbeiterRechttypeValue() {
        final BerechtigungAntragMitarbeiterAntragDto result = BerechtigungAntragMitarbeiterConverter.convertToBerechtigungAntragMitarbeiter(berechtigungsantrag);
        assertThat(result.getRechttype(), is(RECHTTYPE));
    }

    @Test
    void convertToBerechtigungAntragMitarbeiterDefaultBerechtigungLabelValue() {
        when(berechtigungsantrag.getRechttype()).thenReturn(RECHTTYPE);

        final BerechtigungAntragMitarbeiterAntragDto result = BerechtigungAntragMitarbeiterConverter.convertToBerechtigungAntragMitarbeiter(berechtigungsantrag);
        assertThat(result.getBerechtigungLabel(), is(""));
    }

    @Test
    void convertToBerechtigungAntragMitarbeiterSensorCocBerechtigungLabelValue() {
        when(berechtigungsantrag.getSensorCoc()).thenReturn(sensorCoc);
        final String value = "SensorCoc LORENZ!";
        when(sensorCoc.pathToString()).thenReturn(value);

        final BerechtigungAntragMitarbeiterAntragDto result = BerechtigungAntragMitarbeiterConverter.convertToBerechtigungAntragMitarbeiter(berechtigungsantrag);
        assertThat(result.getBerechtigungLabel(), is(value));
    }

    @Test
    void convertToBerechtigungAntragMitarbeiterTteamBerechtigungLabelValue() {
        when(berechtigungsantrag.getTteam()).thenReturn(tteam);
        final String value = "Tteam LORENZ!";
        when(tteam.getTeamName()).thenReturn(value);

        final BerechtigungAntragMitarbeiterAntragDto result = BerechtigungAntragMitarbeiterConverter.convertToBerechtigungAntragMitarbeiter(berechtigungsantrag);
        assertThat(result.getBerechtigungLabel(), is(value));
    }

    @Test
    void convertToBerechtigungAntragMitarbeiterModulSeTeamBerechtigungLabelValue() {
        when(berechtigungsantrag.getModulSeTeam()).thenReturn(modulSeTeam);
        final String value = "ModulSeTeam LORENZ!";
        when(modulSeTeam.getName()).thenReturn(value);

        final BerechtigungAntragMitarbeiterAntragDto result = BerechtigungAntragMitarbeiterConverter.convertToBerechtigungAntragMitarbeiter(berechtigungsantrag);
        assertThat(result.getBerechtigungLabel(), is(value));
    }

}