package de.interfaceag.bmw.pzbk.berechtigungantrag.history;

import de.interfaceag.bmw.pzbk.berechtigungantrag.BerechtigungAntragService;
import de.interfaceag.bmw.pzbk.berechtigungantrag.history.dto.BerechtigungAntragVerwaltungHistoryDto;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragVerwaltungHistoryServiceTest {

    @InjectMocks
    private BerechtigungAntragVerwaltungHistoryService berechtigungAntragVerwaltungHistoryService;

    @Mock
    private BerechtigungAntragService berechtigungAntragService;

    @Test
    void testGetAllNotOffenBerechtigungAntraegeForSensorCoc() {

        Berechtigungsantrag berechtigungsantrag = TestDataFactory.generateBerechtigungsantragForSensorCoc();

        when(berechtigungAntragService.getAllNotOffenBerechtigungAntraege()).thenReturn(Collections.singletonList(berechtigungsantrag));

        List<BerechtigungAntragVerwaltungHistoryDto> result = berechtigungAntragVerwaltungHistoryService.getAllNotOffenBerechtigungAntraege();

        Assertions.assertEquals(berechtigungsantrag.getSensorCoc().pathToString(), result.get(0).getBerechtigung());
    }

    @Test
    void testGetAllNotOffenBerechtigungAntraegeForTteam() {

        Berechtigungsantrag berechtigungsantrag = TestDataFactory.generateBerechtigungsantragForTteam();

        when(berechtigungAntragService.getAllNotOffenBerechtigungAntraege()).thenReturn(Collections.singletonList(berechtigungsantrag));

        List<BerechtigungAntragVerwaltungHistoryDto> result = berechtigungAntragVerwaltungHistoryService.getAllNotOffenBerechtigungAntraege();

        Assertions.assertEquals(berechtigungsantrag.getTteam().getTeamName(), result.get(0).getBerechtigung());
    }

    @Test
    void testGetAllNotOffenBerechtigungAntraegeForModulSeTeam() {

        Berechtigungsantrag berechtigungsantrag = TestDataFactory.generateBerechtigungsantragForModulSeTeam();

        when(berechtigungAntragService.getAllNotOffenBerechtigungAntraege()).thenReturn(Collections.singletonList(berechtigungsantrag));

        List<BerechtigungAntragVerwaltungHistoryDto> result = berechtigungAntragVerwaltungHistoryService.getAllNotOffenBerechtigungAntraege();

        Assertions.assertEquals(berechtigungsantrag.getModulSeTeam().getName(), result.get(0).getBerechtigung());
    }

    @Test
    void testGetAllNotOffenBerechtigungAntraegeForFullBerechtigungsantrag() {

        Berechtigungsantrag berechtigungsantrag = TestDataFactory.generateBerechtigungsantragForModulSeTeam();

        when(berechtigungAntragService.getAllNotOffenBerechtigungAntraege()).thenReturn(Collections.singletonList(berechtigungsantrag));

        List<BerechtigungAntragVerwaltungHistoryDto> result = berechtigungAntragVerwaltungHistoryService.getAllNotOffenBerechtigungAntraege();

        Assertions.assertEquals(berechtigungsantrag.getAntragsteller().toString(), result.get(0).getAntragsteller());
        Assertions.assertEquals(berechtigungsantrag.getStatus(), result.get(0).getAntragStatus());
        Assertions.assertEquals(berechtigungsantrag.getBearbeiter().toString(), result.get(0).getBearbeiter());
        Assertions.assertEquals(berechtigungsantrag.getKommentar(), result.get(0).getKommentar());
        Assertions.assertEquals(berechtigungsantrag.getRolle(), result.get(0).getRolle());
        Assertions.assertEquals(berechtigungsantrag.getRechttype(), result.get(0).getRechttype());
        Assertions.assertEquals(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(berechtigungsantrag.getErstellungsdatum()), result.get(0).getErstelungsdatum());
        Assertions.assertEquals(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(berechtigungsantrag.getAenderungsdatum()), result.get(0).getAenderungsdatum());
        Assertions.assertEquals(berechtigungsantrag.getModulSeTeam().getName(), result.get(0).getBerechtigung());
    }
}