package de.interfaceag.bmw.pzbk.berechtigungantrag;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.dao.MitarbeiterDao;
import de.interfaceag.bmw.pzbk.dao.ModulDao;
import de.interfaceag.bmw.pzbk.dao.SensorCocDao;
import de.interfaceag.bmw.pzbk.dao.TteamDao;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.AntragStatus;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.assertj.core.api.Java6Assertions;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

class BerechtigungAntragDaoIT extends IntegrationTest {

    private BerechtigungAntragDao berechtigungAntragDao = new BerechtigungAntragDao();
    private MitarbeiterDao mitarbeiterDao = new MitarbeiterDao();
    private TteamDao tteamDao = new TteamDao();
    private SensorCocDao sensorCocDao = new SensorCocDao();
    private ModulDao modulDao = new ModulDao();

    private Mitarbeiter antragsteller;
    private SensorCoc sensorCoc;
    private ModulSeTeam modulSeTeam;
    private Tteam tteam;


    @BeforeEach
    void setUp() throws IllegalAccessException {

        super.begin();

        FieldUtils.writeField(berechtigungAntragDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(mitarbeiterDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(modulDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(tteamDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(sensorCocDao, "entityManager", super.getEntityManager(), true);

        buildBerechtigungAntrag();

        super.commit();
        super.begin();
    }

    private void buildBerechtigungAntrag() {


        antragsteller = TestDataFactory.generateMitarbeiter("Felix", "Firefox", "Abteilung 1");
        mitarbeiterDao.persistMitarbeiter(antragsteller);

        Mitarbeiter bearbeiter = TestDataFactory.generateMitarbeiter("Klaus", "Kleber", "Abteilung 1");
        mitarbeiterDao.persistMitarbeiter(bearbeiter);

        tteam = TestDataFactory.generateTteam("tteam1");
        tteamDao.saveTteam(tteam);


        sensorCoc = TestDataFactory.generateSensorCoc();
        sensorCocDao.persistSensorCoc(sensorCoc);

        Modul modul = TestDataFactory.generateModul("Modul1", "Fachbereich1", "Beschreibung");
        modulDao.persistModule(Arrays.asList(modul));

        ModulKomponente modulkomponente = TestDataFactory.generateModulKomponente();
        modulDao.persistModulKomponente(modulkomponente);


        modulSeTeam = TestDataFactory.generateModulSeTeam(modul, Arrays.asList(modulkomponente));
        modulDao.persistSeTeam(modulSeTeam);


        Berechtigungsantrag berechtigungsantrag1 = buildBerechtigungAntragForSensorCoc(antragsteller, bearbeiter, sensorCoc);
        Berechtigungsantrag berechtigungsantrag2 = buildBerechtigungAntragForTteam(antragsteller, bearbeiter, tteam);
        Berechtigungsantrag berechtigungsantrag3 = buildBerechtigungAntragForModulSeTeam(antragsteller, bearbeiter, modulSeTeam);

        berechtigungAntragDao.save(berechtigungsantrag1);
        berechtigungAntragDao.save(berechtigungsantrag2);
        berechtigungAntragDao.save(berechtigungsantrag3);

    }

    private Berechtigungsantrag buildBerechtigungAntragForSensorCoc(Mitarbeiter antragsteller, Mitarbeiter bearbeiter, SensorCoc sensorCoc) {
        Berechtigungsantrag berechtigungsantrag = buildDefaultBerechtgiungAntrag(antragsteller, bearbeiter);
        berechtigungsantrag.setRolle(Rolle.SENSOR);
        berechtigungsantrag.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigungsantrag.setSensorCoc(sensorCoc);
        return berechtigungsantrag;
    }

    private Berechtigungsantrag buildBerechtigungAntragForTteam(Mitarbeiter antragsteller, Mitarbeiter bearbeiter, Tteam tteam) {
        Berechtigungsantrag berechtigungsantrag = buildDefaultBerechtgiungAntrag(antragsteller, bearbeiter);
        berechtigungsantrag.setRolle(Rolle.TTEAMMITGLIED);
        berechtigungsantrag.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigungsantrag.setTteam(tteam);
        berechtigungsantrag.setStatus(AntragStatus.ANGENOMMEN);
        return berechtigungsantrag;
    }

    private Berechtigungsantrag buildBerechtigungAntragForModulSeTeam(Mitarbeiter antragsteller, Mitarbeiter bearbeiter, ModulSeTeam modulSeTeam) {
        Berechtigungsantrag berechtigungsantrag = buildDefaultBerechtgiungAntrag(antragsteller, bearbeiter);
        berechtigungsantrag.setRolle(Rolle.E_COC);
        berechtigungsantrag.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigungsantrag.setModulSeTeam(modulSeTeam);
        return berechtigungsantrag;
    }

    private Berechtigungsantrag buildDefaultBerechtgiungAntrag(Mitarbeiter antragsteller, Mitarbeiter bearbeiter) {
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag();
        berechtigungsantrag.setAntragsteller(antragsteller);
        berechtigungsantrag.setErstellungsdatum(new Date());
        berechtigungsantrag.setKommentar("Kommentar");
        berechtigungsantrag.setBearbeiter(bearbeiter);
        berechtigungsantrag.setAenderungsdatum(new Date());
        berechtigungsantrag.setStatus(AntragStatus.OFFEN);
        return berechtigungsantrag;
    }

    @Test
    void getAllOpenBerechtigungAntraegeForViewTest() {
        List<Berechtigungsantrag> resultList = berechtigungAntragDao.getAllOpenBerechtigungAntraegeForView();
        MatcherAssert.assertThat(resultList, IsCollectionWithSize.hasSize(2));
    }

    @Test
    void getAllNotOffenBerechtigungAntraegeTest() {
        List<Berechtigungsantrag> resultList = berechtigungAntragDao.getAllNotOffenBerechtigungAntraege();
        MatcherAssert.assertThat(resultList, IsCollectionWithSize.hasSize(1));
    }

    @Test
    void getAntraegeByAntragstellerRolleRechttypSensorCocTest() {
        List<Berechtigungsantrag> resultList = berechtigungAntragDao.getAntraegeByAntragstellerRolleRechttypSensorCoc(antragsteller, Rolle.SENSOR, Rechttype.SCHREIBRECHT, sensorCoc);
        MatcherAssert.assertThat(resultList, IsCollectionWithSize.hasSize(1));
    }

    @Test
    void getAntraegeByAntragstellerRolleRechttypTteamTest() {
        List<Berechtigungsantrag> resultList = berechtigungAntragDao.getAntraegeByAntragstellerRolleRechttypTteam(antragsteller, Rolle.TTEAMMITGLIED, Rechttype.SCHREIBRECHT, tteam);
        MatcherAssert.assertThat(resultList, IsCollectionWithSize.hasSize(1));
    }

    @Test
    void getAntraegeByAntragstellerRolleRechttypModulSeTeamTest() {
        List<Berechtigungsantrag> resultList = berechtigungAntragDao.getAntraegeByAntragstellerRolleRechttypModulSeTeam(antragsteller, Rolle.E_COC, Rechttype.SCHREIBRECHT, modulSeTeam);
        MatcherAssert.assertThat(resultList, IsCollectionWithSize.hasSize(1));
    }

    @Test
    void testGetAntraegeByAntragId() {
        Berechtigungsantrag antraegeByAntragId = berechtigungAntragDao.getAntraegByAntragId(1L);
        Java6Assertions.assertThat(antraegeByAntragId).isNotNull();
    }

    @Test
    void testGetAntraegeByAntragIdWithInvalidId() {
        Berechtigungsantrag antraegeByAntragId = berechtigungAntragDao.getAntraegByAntragId(100L);
        Java6Assertions.assertThat(antraegeByAntragId).isNull();
    }


}