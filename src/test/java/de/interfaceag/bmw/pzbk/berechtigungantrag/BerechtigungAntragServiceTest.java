package de.interfaceag.bmw.pzbk.berechtigungantrag;

import de.interfaceag.bmw.pzbk.berechtigungantrag.mail.BerechtigungsantragMailService;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterAntrag;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterAntragDto;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.AntragStatus;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.session.Session;
import java.sql.Date;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragServiceTest {

    @Mock
    private BerechtigungAntragDao berechtigungAntragDao;

    @Mock
    private BerechtigungsantragMailService berechtigungsantragMailService;

    @InjectMocks
    private BerechtigungAntragService berechtigungAntragService;

    @Mock
    private Berechtigungsantrag berechtigungsantrag1;
    @Mock
    private Mitarbeiter mitarbeiter;
    @Mock
    private SensorCoc sensorCoc;
    @Mock
    private Tteam tteam;
    @Mock
    private ModulSeTeam modulSeTeam;
    @Mock
    private Session session;
    @Mock
    private BerechtigungService berechtigungService;
    @Mock
    private TteamService tteamService;

    @Test
    void persistBerechtigungAntragTest() {
        berechtigungAntragService.persistBerechtigungAntrag(berechtigungsantrag1);
        verify(berechtigungAntragDao, times(1)).save(berechtigungsantrag1);
    }

    @Test
    void getAllNotOffenBerechtigungAntraegeTest() {
        berechtigungAntragService.getAllNotOffenBerechtigungAntraege();
        verify(berechtigungAntragDao, times(1)).getAllNotOffenBerechtigungAntraege();
    }

    @Test
    void getAllOffenBerechtigungAntraegeTest() {
        berechtigungAntragService.getAllOpenBerechtigungAntraegeForView();
        verify(berechtigungAntragDao, times(1)).getAllOpenBerechtigungAntraegeForView();
    }

    @Test
    void getAntraegeByAntragstellerRolleRechttypSensorCocTest() {
        berechtigungAntragService.getAntraegeByAntragstellerRolleRechttypSensorCoc(mitarbeiter, Rolle.SENSOR, Rechttype.SCHREIBRECHT, sensorCoc);
        verify(berechtigungAntragDao, times(1)).getAntraegeByAntragstellerRolleRechttypSensorCoc(mitarbeiter, Rolle.SENSOR, Rechttype.SCHREIBRECHT, sensorCoc);
    }

    @Test
    void getAntraegeByAntragstellerRolleRechttypTteamTest() {
        berechtigungAntragService.getAntraegeByAntragstellerRolleRechttypTteam(mitarbeiter, Rolle.TTEAMMITGLIED, Rechttype.SCHREIBRECHT, tteam);
        verify(berechtigungAntragDao, times(1)).getAntraegeByAntragstellerRolleRechttypTteam(mitarbeiter, Rolle.TTEAMMITGLIED, Rechttype.SCHREIBRECHT, tteam);
    }

    @Test
    void getAntraegeByAntragstellerRolleRechttypModulSeTeam() {
        berechtigungAntragService.getAntraegeByAntragstellerRolleRechttypModulSeTeam(mitarbeiter, Rolle.E_COC, Rechttype.SCHREIBRECHT, modulSeTeam);
        verify(berechtigungAntragDao, times(1)).getAntraegeByAntragstellerRolleRechttypModulSeTeam(mitarbeiter, Rolle.E_COC, Rechttype.SCHREIBRECHT, modulSeTeam);
    }

    @Test
    void antragAblehnenTest() {
        when(berechtigungAntragDao.getAntraegByAntragId(any())).thenReturn(new Berechtigungsantrag());
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("mitarbeiter"));

        BerechtigungAntragMitarbeiterAntrag antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.SENSOR, Rechttype.SCHREIBRECHT, "label", new Date(2019, 1, 1));

        berechtigungAntragService.antragAblehnen(antrag);

        verify(berechtigungAntragDao, times(1)).save(any());
        verify(berechtigungsantragMailService, times(1)).sendMailBasedOnAntragStatus(any());
    }

    @Test
    void antragAnnehmenTestAsSensorWithSchreibrecht() {
        BerechtigungAntragMitarbeiterAntrag antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.SENSOR, Rechttype.SCHREIBRECHT, "label", new Date(2019, 1, 1));
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag(TestDataFactory.createMitarbeiter("Hans"), Rolle.SENSOR, Rechttype.SCHREIBRECHT, AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setSensorCoc(TestDataFactory.generateSensorCoc());

        when(berechtigungAntragDao.getAntraegByAntragId(any())).thenReturn(berechtigungsantrag);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("mitarbeiter"));

        berechtigungAntragService.antragAnnehmen(antrag);

        verify(berechtigungService, times(1)).grantRoleSensor(any(), any(), any());
        verify(berechtigungsantragMailService, times(1)).sendMailBasedOnAntragStatus(any());
    }

    @Test
    void antragAnnehmenTestAsSensorWithLeserecht() {
        BerechtigungAntragMitarbeiterAntrag antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.SENSOR, Rechttype.LESERECHT, "label", new Date(2019, 1, 1));
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag(TestDataFactory.createMitarbeiter("Hans"), Rolle.SENSOR, Rechttype.LESERECHT, AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setSensorCoc(TestDataFactory.generateSensorCoc());

        when(berechtigungAntragDao.getAntraegByAntragId(any())).thenReturn(berechtigungsantrag);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("mitarbeiter"));

        berechtigungAntragService.antragAnnehmen(antrag);

        verify(berechtigungService, times(1)).grantRoleSensor(any(), any(), any());
        verify(berechtigungsantragMailService, times(1)).sendMailBasedOnAntragStatus(any());
    }

    @Test
    void antragAnnehmenTestAsSensorEingeschraenktWithSchreibrecht() {
        BerechtigungAntragMitarbeiterAntrag antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.SENSOR_EINGESCHRAENKT, Rechttype.SCHREIBRECHT, "label", new Date(2019, 1, 1));
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag(TestDataFactory.createMitarbeiter("Hans"), Rolle.SENSOR_EINGESCHRAENKT, Rechttype.SCHREIBRECHT, AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setSensorCoc(TestDataFactory.generateSensorCoc());

        when(berechtigungAntragDao.getAntraegByAntragId(any())).thenReturn(berechtigungsantrag);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("mitarbeiter"));

        berechtigungAntragService.antragAnnehmen(antrag);

        verify(berechtigungService, times(1)).grantRoleSensorEingeschraenkt(any(), any(), any());
        verify(berechtigungsantragMailService, times(1)).sendMailBasedOnAntragStatus(any());
    }

    @Test
    void antragAnnehmenTestAsSensorEingeschraenktWithLeserecht() {
        BerechtigungAntragMitarbeiterAntrag antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.SENSOR_EINGESCHRAENKT, Rechttype.LESERECHT, "label", new Date(2019, 1, 1));
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag(TestDataFactory.createMitarbeiter("Hans"), Rolle.SENSOR_EINGESCHRAENKT, Rechttype.LESERECHT, AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setSensorCoc(TestDataFactory.generateSensorCoc());

        when(berechtigungAntragDao.getAntraegByAntragId(any())).thenReturn(berechtigungsantrag);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("mitarbeiter"));

        berechtigungAntragService.antragAnnehmen(antrag);

        verify(berechtigungService, times(1)).grantRoleSensorEingeschraenkt(any(), any(), any());
        verify(berechtigungsantragMailService, times(1)).sendMailBasedOnAntragStatus(any());
    }

    @Test
    void antragAnnehmenTestAsTteamMitgliedtWithSchreibrecht() {
        BerechtigungAntragMitarbeiterAntrag antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.TTEAMMITGLIED, Rechttype.SCHREIBRECHT, "label", new Date(2019, 1, 1));
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag(TestDataFactory.createMitarbeiter("Hans"), Rolle.TTEAMMITGLIED, Rechttype.SCHREIBRECHT, AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setTteam(TestDataFactory.generateTteam());

        when(berechtigungAntragDao.getAntraegByAntragId(any())).thenReturn(berechtigungsantrag);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("mitarbeiter"));

        berechtigungAntragService.antragAnnehmen(antrag);

        verify(berechtigungService, times(1)).grantRoleTteamMitglied(any(), any(), any());
        verify(berechtigungsantragMailService, times(1)).sendMailBasedOnAntragStatus(any());
    }

    @Test
    void antragAnnehmenTestAsTteamMitgliedtWithLeserecht() {
        BerechtigungAntragMitarbeiterAntrag antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.TTEAMMITGLIED, Rechttype.LESERECHT, "label", new Date(2019, 1, 1));
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag(TestDataFactory.createMitarbeiter("Hans"), Rolle.TTEAMMITGLIED, Rechttype.LESERECHT, AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setTteam(TestDataFactory.generateTteam());

        when(berechtigungAntragDao.getAntraegByAntragId(any())).thenReturn(berechtigungsantrag);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("mitarbeiter"));

        berechtigungAntragService.antragAnnehmen(antrag);

        verify(berechtigungService, times(1)).grantRoleTteamMitglied(any(), any(), any());
        verify(berechtigungsantragMailService, times(1)).sendMailBasedOnAntragStatus(any());
    }

    @Test
    void antragAnnehmenTestAsEcoctWithSchreibrecht() {
        BerechtigungAntragMitarbeiterAntrag antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.E_COC, Rechttype.SCHREIBRECHT, "label", new Date(2019, 1, 1));
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag(TestDataFactory.createMitarbeiter("Hans"), Rolle.E_COC, Rechttype.SCHREIBRECHT, AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setSensorCoc(TestDataFactory.generateSensorCoc());

        when(berechtigungAntragDao.getAntraegByAntragId(any())).thenReturn(berechtigungsantrag);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("mitarbeiter"));

        berechtigungAntragService.antragAnnehmen(antrag);

        verify(berechtigungService, times(1)).grantRoleEcoc(any(), any(), any());
        verify(berechtigungsantragMailService, times(1)).sendMailBasedOnAntragStatus(any());
    }

    @Test
    void antragAnnehmenTestAsEcoctWithLeserecht() {
        BerechtigungAntragMitarbeiterAntrag antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.E_COC, Rechttype.LESERECHT, "label", new Date(2019, 1, 1));
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag(TestDataFactory.createMitarbeiter("Hans"), Rolle.E_COC, Rechttype.LESERECHT, AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setSensorCoc(TestDataFactory.generateSensorCoc());

        when(berechtigungAntragDao.getAntraegByAntragId(any())).thenReturn(berechtigungsantrag);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("mitarbeiter"));

        berechtigungAntragService.antragAnnehmen(antrag);

        verify(berechtigungService, times(1)).grantRoleEcoc(any(), any(), any());
    }

    @Test
    void antragAnnehmenTestAsSensorWithBerechtigungAlreadyGiven() {
        BerechtigungAntragMitarbeiterAntrag antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.SENSOR, Rechttype.LESERECHT, "label", new Date(2019, 1, 1));
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag(TestDataFactory.createMitarbeiter("Hans"), Rolle.SENSOR, Rechttype.LESERECHT, AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setSensorCoc(TestDataFactory.generateSensorCoc());

        when(berechtigungAntragDao.getAntraegByAntragId(any())).thenReturn(berechtigungsantrag);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("mitarbeiter"));
        when(berechtigungService.getSensorCocForMitarbeiterAndRolle(any(), any(), any())).thenReturn(TestDataFactory.generateSensorCocs());

        berechtigungAntragService.antragAnnehmen(antrag);

        verify(berechtigungService, never()).grantRoleSensor(any(), any(), any());
    }

    @Test
    void antragAnnehmenTestAsSensorEingeschraenktWithBerechtigungAlreadyGiven() {
        BerechtigungAntragMitarbeiterAntrag antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.SENSOR_EINGESCHRAENKT, Rechttype.LESERECHT, "label", new Date(2019, 1, 1));
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag(TestDataFactory.createMitarbeiter("Hans"), Rolle.SENSOR_EINGESCHRAENKT, Rechttype.LESERECHT, AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setSensorCoc(TestDataFactory.generateSensorCoc());

        when(berechtigungAntragDao.getAntraegByAntragId(any())).thenReturn(berechtigungsantrag);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("mitarbeiter"));
        when(berechtigungService.getSensorCocForMitarbeiterAndRolle(any(), any(), any())).thenReturn(TestDataFactory.generateSensorCocs());

        berechtigungAntragService.antragAnnehmen(antrag);

        verify(berechtigungService, never()).grantRoleSensorEingeschraenkt(any(), any(), any());
    }

    @Test
    void antragAnnehmenTestAsTteammitgliedWithBerechtigungAlreadyGiven() {
        BerechtigungAntragMitarbeiterAntrag antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.TTEAMMITGLIED, Rechttype.LESERECHT, "label", new Date(2019, 1, 1));
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag(TestDataFactory.createMitarbeiter("Hans"), Rolle.TTEAMMITGLIED, Rechttype.LESERECHT, AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setTteam(TestDataFactory.generateTteam());

        when(berechtigungAntragDao.getAntraegByAntragId(any())).thenReturn(berechtigungsantrag);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("mitarbeiter"));
        when(tteamService.getTteamIdsforTteamMitgliedAndRechttype(any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateTteam().getId()));

        berechtigungAntragService.antragAnnehmen(antrag);

        verify(berechtigungService, never()).grantRoleTteamMitglied(any(), any(), any());
    }

    @Test
    void antragAnnehmenTestAsEcocWithBerechtigungAlreadyGiven() {
        BerechtigungAntragMitarbeiterAntrag antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.E_COC, Rechttype.LESERECHT, "label", new Date(2019, 1, 1));
        Berechtigungsantrag berechtigungsantrag = new Berechtigungsantrag(TestDataFactory.createMitarbeiter("Hans"), Rolle.E_COC, Rechttype.LESERECHT, AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setModulSeTeam(TestDataFactory.generateModulSeTeam(1L, TestDataFactory.generateModul(), TestDataFactory.generateModulKomponenten()));

        when(berechtigungAntragDao.getAntraegByAntragId(any())).thenReturn(berechtigungsantrag);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("mitarbeiter"));
        when(berechtigungService.getAuthorizedModulSeTeamListForMitarbeiter(any())).thenReturn(Arrays.asList(TestDataFactory.generateModulSeTeam(1L, TestDataFactory.generateModul(), TestDataFactory.generateModulKomponenten())));

        berechtigungAntragService.antragAnnehmen(antrag);

        verify(berechtigungService, never()).grantRoleEcoc(any(), any(), any());
    }

}
