package de.interfaceag.bmw.pzbk.berechtigungantrag.permission;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;
import org.hamcrest.collection.IsEmptyCollection;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragVerwaltungPermissionServiceTest {

    @Mock
    private Session session;
    @InjectMocks
    private BerechtigungAntragVerwaltungPermissionService berechtigungAntragVerwaltungPermissionService;

    @Mock
    private UserPermissions<BerechtigungDto> userPermissions;
    private Set<Rolle> roles = new HashSet<>();
    private List<Berechtigungsantrag> allBerechtigungsAntraege = new ArrayList<>();

    @Mock
    private Berechtigungsantrag berechtigungsantrag;


    @BeforeEach
    void setUp() {
        allBerechtigungsAntraege.add(berechtigungsantrag);
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRoles()).thenReturn(roles);
    }

    @Test
    void restrictToCurrentUserForRoleAdmin() {
        roles.add(Rolle.ADMIN);
        final Collection<Berechtigungsantrag> result = berechtigungAntragVerwaltungPermissionService.restrictToCurrentUser(allBerechtigungsAntraege);
        assertThat(result, IsIterableContaining.hasItem(berechtigungsantrag));
    }

    @Test
    void restrictToCurrentUserForRoleNotAuthorizedRoles() {
        roles.add(Rolle.UMSETZUNGSBESTAETIGER);
        roles.add(Rolle.E_COC);
        roles.add(Rolle.TTEAMMITGLIED);
        roles.add(Rolle.LESER);
        roles.add(Rolle.ANFORDERER);
        roles.add(Rolle.SENSOR);
        roles.add(Rolle.SENSOR_EINGESCHRAENKT);
        final Collection<Berechtigungsantrag> result = berechtigungAntragVerwaltungPermissionService.restrictToCurrentUser(allBerechtigungsAntraege);
        assertThat(result, IsEmptyCollection.empty());
    }

}