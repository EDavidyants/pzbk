package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung;

import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiter;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterAntrag;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterAntragDto;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragsverwaltungViewData;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.permission.BerechtigungAntragsverwaltungViewPermission;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ContextMocker;
import org.assertj.core.api.Java6Assertions;
import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragsverwaltungControllerTest {

    @Mock
    private BerechtigungAntragsverwaltungViewData viewData;
    @Mock
    private BerechtigungAntragsverwaltungViewPermission viewPermission;
    @InjectMocks
    private BerechtigungAntragsverwaltungController berechtigungAntragsverwaltungController;
    @Mock
    private BerechtigungAntragsverwaltungFacade berechtigungAntragsverwaltungFacade;


    @Mock
    private BerechtigungAntragMitarbeiter selectedMitarbeiterAntraege;

    private List<BerechtigungAntragMitarbeiterAntrag> antraege = new ArrayList<>();

    @Test
    void getViewData() {
        final BerechtigungAntragsverwaltungViewData controllerViewData = berechtigungAntragsverwaltungController.getViewData();
        assertThat(controllerViewData, is(viewData));
    }

    @Test
    void getViewPermission() {
        final BerechtigungAntragsverwaltungViewPermission controllerViewPermission = berechtigungAntragsverwaltungController.getViewPermission();
        assertThat(controllerViewPermission, is(viewPermission));
    }

    @Test
    void openAntraege() {
        when(selectedMitarbeiterAntraege.getAntraege()).thenReturn(antraege);
        berechtigungAntragsverwaltungController.openAntraege(selectedMitarbeiterAntraege);
        final List<BerechtigungAntragMitarbeiterAntrag> controllerSelectedAntraege = berechtigungAntragsverwaltungController.getSelectedAntraege();
        assertThat(controllerSelectedAntraege, is(antraege));
    }

    @Test
    void getSelectedAntraegeDefault() {
        final List<BerechtigungAntragMitarbeiterAntrag> controllerSelectedAntraege = berechtigungAntragsverwaltungController.getSelectedAntraege();
        assertThat(controllerSelectedAntraege, is(IsNull.nullValue()));
    }

    @Test
    void setSelectedAntraege() {
        berechtigungAntragsverwaltungController.setSelectedAntraege(antraege);
        final List<BerechtigungAntragMitarbeiterAntrag> controllerSelectedAntraege = berechtigungAntragsverwaltungController.getSelectedAntraege();
        assertThat(controllerSelectedAntraege, is(antraege));
    }

    @Test
    void getAntragToChange() {
        BerechtigungAntragMitarbeiterAntragDto antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.SENSOR, Rechttype.SCHREIBRECHT, "label", new Date(2019, 1, 1));
        berechtigungAntragsverwaltungController.setAntragToChange(antrag);

        BerechtigungAntragMitarbeiterAntrag antragToChange = berechtigungAntragsverwaltungController.getAntragToChange();

        assertThat(antragToChange, is(antrag));
    }

    @Test
    void testAntragAngenommenClicked() {
        BerechtigungAntragMitarbeiterAntragDto antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.SENSOR, Rechttype.SCHREIBRECHT, "label", new Date(2019, 1, 1));

        berechtigungAntragsverwaltungController.setAntragToChange(antrag);
        List<BerechtigungAntragMitarbeiterAntrag> antraege = new ArrayList<>();
        antraege.add(antrag);
        berechtigungAntragsverwaltungController.setSelectedAntraege(antraege);

        berechtigungAntragsverwaltungController.antragAngenommenClicked();

        Java6Assertions.assertThat(berechtigungAntragsverwaltungController.getSelectedAntraege()).isEmpty();
    }

    @MockitoSettings(strictness = Strictness.LENIENT)
    @Test
    void testAntragAbgelehntClicked() {
        BerechtigungAntragMitarbeiterAntragDto antrag = new BerechtigungAntragMitarbeiterAntragDto(1L, Rolle.SENSOR, Rechttype.SCHREIBRECHT, "label", new Date(2019, 1, 1));
        antrag.setKommentar("Kommentar");
        ContextMocker.mockRequestContext();
        berechtigungAntragsverwaltungController.setAntragToChange(antrag);
        List<BerechtigungAntragMitarbeiterAntrag> antraege = new ArrayList<>();
        antraege.add(antrag);
        berechtigungAntragsverwaltungController.setSelectedAntraege(antraege);

        berechtigungAntragsverwaltungController.antragAbgelehntClicked();

        Java6Assertions.assertThat(berechtigungAntragsverwaltungController.getSelectedAntraege()).isEmpty();
    }
}