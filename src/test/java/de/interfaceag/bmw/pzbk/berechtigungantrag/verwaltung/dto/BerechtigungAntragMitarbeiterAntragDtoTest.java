package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto;

import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.nullValue;

class BerechtigungAntragMitarbeiterAntragDtoTest {

    private static final long BERECHTIGUNG_ANTRAG_ID = 42L;
    private static final Rolle ROLLE = Rolle.SENSORCOCLEITER;
    private static final Rechttype RECHTTYPE = Rechttype.SCHREIBRECHT;
    private static final String LABEL = "LABEL";

    private BerechtigungAntragMitarbeiterAntragDto berechtigungAntragMitarbeiterAntragDto;

    @BeforeEach
    void setUp() {
        Date date = new Date(2019, 1, 1);
        date.setHours(12);
        date.setMinutes(13);
        berechtigungAntragMitarbeiterAntragDto = new BerechtigungAntragMitarbeiterAntragDto(BERECHTIGUNG_ANTRAG_ID, ROLLE, RECHTTYPE, LABEL, date);
    }

    @Test
    void testGetErstellungsdateAsString() {
        final String date = berechtigungAntragMitarbeiterAntragDto.getErstellungsdatumForAusgabe();
        assertThat(date, is("01.02.3919 12:13"));
    }


    @Test
    void toString1() {
        final String result = berechtigungAntragMitarbeiterAntragDto.toString();
        assertThat(result, is("BerechtigungAntragMitarbeiterAntragDto[berechtigungAntragId=42, rolle=SensorCoC-Leiter, rechttype=Schreibrecht, berechtigungLabel='LABEL', kommentar='null']"));
    }

    @Test
    void getBerechtigungAntragId() {
        final Long result = berechtigungAntragMitarbeiterAntragDto.getBerechtigungAntragId();
        assertThat(result, is(BERECHTIGUNG_ANTRAG_ID));
    }

    @Test
    void getKommentar() {
        final String result = berechtigungAntragMitarbeiterAntragDto.getKommentar();
        assertThat(result, is(nullValue()));
    }

    @Test
    void setKommentar() {
        final String kommentar = "KOMMENTAR";
        berechtigungAntragMitarbeiterAntragDto.setKommentar(kommentar);
        final String result = berechtigungAntragMitarbeiterAntragDto.getKommentar();
        assertThat(result, is(kommentar));
    }

    @Test
    void getRolle() {
        final Rolle result = berechtigungAntragMitarbeiterAntragDto.getRolle();
        assertThat(result, is(ROLLE));
    }

    @Test
    void getRechttype() {
        final Rechttype result = berechtigungAntragMitarbeiterAntragDto.getRechttype();
        assertThat(result, is(RECHTTYPE));
    }

    @Test
    void getBerechtigungLabel() {
        final String result = berechtigungAntragMitarbeiterAntragDto.getBerechtigungLabel();
        assertThat(result, is(LABEL));
    }

    @Test
    void equalsTestSameObject() {
        assertTrue(berechtigungAntragMitarbeiterAntragDto.equals(berechtigungAntragMitarbeiterAntragDto));
    }

    @Test
    void equalsTestNull() {
        assertFalse(berechtigungAntragMitarbeiterAntragDto.equals(null));
    }

    @Test
    void equalsTestDifferentKommentar() {
        BerechtigungAntragMitarbeiterAntragDto compareAntrag = new BerechtigungAntragMitarbeiterAntragDto(BERECHTIGUNG_ANTRAG_ID, ROLLE, RECHTTYPE, LABEL, new Date(2019, 1, 1));
        compareAntrag.setKommentar("123");

        berechtigungAntragMitarbeiterAntragDto.setKommentar("456");

        //import test!! don't change the equals. Kommentar can not be a field to compare
        assertTrue(this.berechtigungAntragMitarbeiterAntragDto.equals(compareAntrag));
    }

    @Test
    void hashcodeDifferentKommentar() {
        BerechtigungAntragMitarbeiterAntragDto compareAntrag = new BerechtigungAntragMitarbeiterAntragDto(BERECHTIGUNG_ANTRAG_ID, ROLLE, RECHTTYPE, LABEL, new Date(2019, 1, 1));
        compareAntrag.setKommentar("123");

        berechtigungAntragMitarbeiterAntragDto.setKommentar("456");

        //import test!! don't change the equals. Kommentar can not be a field to compare
        assertTrue(this.berechtigungAntragMitarbeiterAntragDto.hashCode() == compareAntrag.hashCode());
    }


}