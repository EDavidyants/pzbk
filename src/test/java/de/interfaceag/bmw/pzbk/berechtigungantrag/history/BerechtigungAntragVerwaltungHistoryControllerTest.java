package de.interfaceag.bmw.pzbk.berechtigungantrag.history;

import de.interfaceag.bmw.pzbk.berechtigungantrag.history.dto.BerechtigungAntragVerwaltungHistoryViewData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragVerwaltungHistoryControllerTest {

    @Mock
    private BerechtigungAntragVerwaltungHistoryViewData viewData;
    @InjectMocks
    private BerechtigungAntragVerwaltungHistoryController berechtigungAntragVerwaltungHistoryController;

    @Test
    void testControllerInit() {
        Assertions.assertEquals(viewData, berechtigungAntragVerwaltungHistoryController.getViewData());
    }

}