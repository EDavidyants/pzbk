package de.interfaceag.bmw.pzbk.berechtigungantrag.permission;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigungantrag.permission.BerechtigungAntragVerwaltungPermissionService;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;
import org.hamcrest.collection.IsIterableWithSize;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragVerwaltungPermissionServiceForSensorCocLeiterTest {

    private static final long ID = 42L;

    @Mock
    private Session session;
    @InjectMocks
    private BerechtigungAntragVerwaltungPermissionService berechtigungAntragVerwaltungPermissionService;

    @Mock
    private UserPermissions<BerechtigungDto> userPermissions;
    private Set<Rolle> roles = new HashSet<>();
    private List<Berechtigungsantrag> allBerechtigungsAntraege = new ArrayList<>();

    @Mock
    private Berechtigungsantrag berechtigungsantrag;
    @Mock
    private Berechtigungsantrag anotherBerechtigungsantrag;
    @Mock
    private SensorCoc sensorCoc;
    private List<BerechtigungDto> berechtigungen = new ArrayList<>();
    @Mock
    private BerechtigungDto berechtigungDto;

    @BeforeEach
    void setUp() {
        allBerechtigungsAntraege.add(berechtigungsantrag);
        allBerechtigungsAntraege.add(anotherBerechtigungsantrag);
        berechtigungen.add(berechtigungDto);

        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRoles()).thenReturn(roles);
        when(berechtigungsantrag.getSensorCoc()).thenReturn(sensorCoc);
        when(sensorCoc.getSensorCocId()).thenReturn(ID);
        when(userPermissions.getSensorCocAsSensorCocLeiterSchreibend()).thenReturn(berechtigungen);
        when(berechtigungDto.getId()).thenReturn(ID);
    }

    @Test
    void restrictToCurrentUserForRoleSensorCocLeiter() {
        roles.add(Rolle.SENSORCOCLEITER);
        final Collection<Berechtigungsantrag> result = berechtigungAntragVerwaltungPermissionService.restrictToCurrentUser(allBerechtigungsAntraege);
        assertThat(result, IsIterableContaining.hasItem(berechtigungsantrag));
        assertThat(result, IsIterableWithSize.iterableWithSize(1));
    }

}