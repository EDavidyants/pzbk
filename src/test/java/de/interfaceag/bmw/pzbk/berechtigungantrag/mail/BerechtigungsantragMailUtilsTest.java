package de.interfaceag.bmw.pzbk.berechtigungantrag.mail;

import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.AntragStatus;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class BerechtigungsantragMailUtilsTest {

    private Date date;
    private Mitarbeiter user;
    private List<Berechtigungsantrag> berechtigungen;

    @Mock
    private Berechtigungsantrag berechtigungWrite;

    @Mock
    private Berechtigungsantrag berechtigungWriteTwo;

    @Mock
    private Berechtigungsantrag berechtigungRead;

    @Mock
    private Berechtigungsantrag berechtigungReadTwo;

    @BeforeEach
    public void setUp() {
        user = TestDataFactory.generateMitarbeiter("Zack", "Logan", "Abt.-2", "q125896");
        user.setEmail("zack.logan@interface-ag.de");
        berechtigungen = new ArrayList<>();
    }

    @Test
    public void testGetFormattedDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2019, 8, 3, 8, 15, 41);
        date = calendar.getTime();
        String dateString = BerechtigungsantragMailUtils.getFormattedDate(date);
        Assertions.assertEquals("03.09.2019 08:15:41", dateString);
    }

    @Test
    public void testGetAntragsteller() {
        when(berechtigungWrite.getAntragsteller()).thenReturn(user);
        berechtigungen.add(berechtigungWrite);
        String userName = BerechtigungsantragMailUtils.getAntragsteller(berechtigungen);
        Assertions.assertEquals("Zack Logan", userName);
    }

    @ParameterizedTest
    @EnumSource(value = Rolle.class, names = {"SENSOR", "SENSOR_EINGESCHRAENKT", "TTEAMMITGLIED", "E_COC"})
    public void testGetRolle(Rolle rolle) {
        when(berechtigungWrite.getRolle()).thenReturn(rolle);
        berechtigungen.add(berechtigungWrite);
        String userName = BerechtigungsantragMailUtils.getRolle(berechtigungen);
        switch (rolle) {
            case SENSOR:
                Assertions.assertEquals("Sensor", userName);
                break;
            case SENSOR_EINGESCHRAENKT:
                Assertions.assertEquals("Sensor eingeschränkt", userName);
                break;
            case TTEAMMITGLIED:
                Assertions.assertEquals("T-Team Mitglied", userName);
                break;
            case E_COC:
                Assertions.assertEquals("E-CoC", userName);
                break;
            default:
                break;
        }
    }

    @Test
    public void testGetSchreibberechtigungenEmpty() {
        when(berechtigungRead.getRechttype()).thenReturn(Rechttype.LESERECHT);
        berechtigungen.add(berechtigungRead);
        String result = BerechtigungsantragMailUtils.getSchreibberechtigungen(berechtigungen);
        Assertions.assertEquals("", result);
    }

    @Test
    public void testGetLeseberechtigungenEmpty() {
        when(berechtigungWrite.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        berechtigungen.add(berechtigungWrite);
        String result = BerechtigungsantragMailUtils.getLeseberechtigungen(berechtigungen);
        Assertions.assertEquals("", result);
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testGetSchreibberechtigungenSensorCocOne(AntragStatus status) {
        when(berechtigungWrite.getRolle()).thenReturn(Rolle.SENSOR);
        when(berechtigungWrite.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungWrite.getStatus()).thenReturn(status);
        SensorCoc sensorCoc = new SensorCoc();
        sensorCoc.setTechnologie("TGF_Sitze");
        when(berechtigungWrite.getSensorCoc()).thenReturn(sensorCoc);

        String kommentar = "kommentar";
        if (status == AntragStatus.ABGELEHNT) {
            when(berechtigungWrite.getKommentar()).thenReturn(kommentar);
        }

        berechtigungen.add(berechtigungWrite);
        String result = BerechtigungsantragMailUtils.getSchreibberechtigungen(berechtigungen);
        String html = "";
        switch (status) {
            case OFFEN: {
                html = "<ul style='margin-top: 0px;'><li>TGF_Sitze</li></ul>";
                break;
            }

            case ANGENOMMEN: {
                html = "<ul style='margin-top: 0px;'><li>TGF_Sitze</li></ul>";
                break;
            }

            case ABGELEHNT: {
                html = "<ul style='margin-top: 0px;'><li>TGF_Sitze<br/><div style='font-style: italic;'>" + kommentar + "</div></li></ul>";
                break;
            }

            default:
                break;
        }

        Assertions.assertEquals(html, result);
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testGetSchreibberechtigungenSensorCocTwo(AntragStatus status) {
        when(berechtigungWrite.getRolle()).thenReturn(Rolle.SENSOR);
        SensorCoc sensorCocOne = new SensorCoc();
        sensorCocOne.setTechnologie("TGF_Sitze");
        SensorCoc sensorCocTwo = new SensorCoc();
        sensorCocTwo.setTechnologie("TGF_Antrieb");
        when(berechtigungWrite.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungWrite.getSensorCoc()).thenReturn(sensorCocOne);
        when(berechtigungWriteTwo.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungWriteTwo.getSensorCoc()).thenReturn(sensorCocTwo);
        when(berechtigungWrite.getStatus()).thenReturn(status);
        when(berechtigungWriteTwo.getStatus()).thenReturn(status);

        String kommentar = "kommentar";
        String kommentarTwo = "kommentarTwo";
        if (status == AntragStatus.ABGELEHNT) {
            when(berechtigungWrite.getKommentar()).thenReturn(kommentar);
            when(berechtigungWriteTwo.getKommentar()).thenReturn(kommentarTwo);
        }

        berechtigungen.add(berechtigungWrite);
        berechtigungen.add(berechtigungWriteTwo);
        String result = BerechtigungsantragMailUtils.getSchreibberechtigungen(berechtigungen);
        String html = "";
        switch (status) {
            case OFFEN: {
                html = "<ul style='margin-top: 0px;'><li>TGF_Sitze</li><li>TGF_Antrieb</li></ul>";
                break;
            }

            case ANGENOMMEN: {
                html = "<ul style='margin-top: 0px;'><li>TGF_Sitze</li><li>TGF_Antrieb</li></ul>";
                break;
            }

            case ABGELEHNT: {
                html = "<ul style='margin-top: 0px;'><li>TGF_Sitze<br/><div style='font-style: italic;'>"
                        + kommentar + "</div></li>"
                        + "<li>TGF_Antrieb<br/><div style='font-style: italic;'>"
                        + kommentarTwo + "</div></li></ul>";
                break;
            }

            default:
                break;
        }

        Assertions.assertEquals(html, result);
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testGetSchreibberechtigungenTteamOne(AntragStatus status) {
        when(berechtigungWrite.getRolle()).thenReturn(Rolle.TTEAMMITGLIED);
        when(berechtigungWrite.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungWrite.getStatus()).thenReturn(status);
        Tteam tteam = new Tteam("Unterboden");
        when(berechtigungWrite.getTteam()).thenReturn(tteam);

        String kommentar = "kommentar";
        if (status == AntragStatus.ABGELEHNT) {
            when(berechtigungWrite.getKommentar()).thenReturn(kommentar);
        }

        berechtigungen.add(berechtigungWrite);
        String result = BerechtigungsantragMailUtils.getSchreibberechtigungen(berechtigungen);
        String html = "";
        switch (status) {
            case OFFEN: {
                html = "<ul style='margin-top: 0px;'><li>Unterboden</li></ul>";
                break;
            }

            case ANGENOMMEN: {
                html = "<ul style='margin-top: 0px;'><li>Unterboden</li></ul>";
                break;
            }

            case ABGELEHNT: {
                html = "<ul style='margin-top: 0px;'><li>Unterboden<br/><div style='font-style: italic;'>" + kommentar + "</div></li></ul>";
                break;
            }

            default:
                break;
        }

        Assertions.assertEquals(html, result);
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testGetSchreibberechtigungenTteamTwo(AntragStatus status) {
        when(berechtigungWrite.getRolle()).thenReturn(Rolle.TTEAMMITGLIED);
        Tteam tteamOne = new Tteam("Unterboden");
        Tteam tteamTwo = new Tteam("Verdeck");
        when(berechtigungWrite.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungWrite.getTteam()).thenReturn(tteamOne);
        when(berechtigungWriteTwo.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungWriteTwo.getTteam()).thenReturn(tteamTwo);
        when(berechtigungWrite.getStatus()).thenReturn(status);
        when(berechtigungWriteTwo.getStatus()).thenReturn(status);

        String kommentar = "kommentar";
        String kommentarTwo = "kommentarTwo";
        if (status == AntragStatus.ABGELEHNT) {
            when(berechtigungWrite.getKommentar()).thenReturn(kommentar);
            when(berechtigungWriteTwo.getKommentar()).thenReturn(kommentarTwo);
        }

        berechtigungen.add(berechtigungWrite);
        berechtigungen.add(berechtigungWriteTwo);
        String result = BerechtigungsantragMailUtils.getSchreibberechtigungen(berechtigungen);
        String html = "";
        switch (status) {
            case OFFEN: {
                html = "<ul style='margin-top: 0px;'><li>Unterboden</li><li>Verdeck</li></ul>";
                break;
            }

            case ANGENOMMEN: {
                html = "<ul style='margin-top: 0px;'><li>Unterboden</li><li>Verdeck</li></ul>";
                break;
            }

            case ABGELEHNT: {
                html = "<ul style='margin-top: 0px;'><li>Unterboden<br/><div style='font-style: italic;'>"
                        + kommentar + "</div></li>"
                        + "<li>Verdeck<br/><div style='font-style: italic;'>"
                        + kommentarTwo + "</div></li></ul>";
                break;
            }

            default:
                break;
        }

        Assertions.assertEquals(html, result);
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testGetSchreibberechtigungenModulSeTeamOne(AntragStatus status) {
        when(berechtigungWrite.getRolle()).thenReturn(Rolle.E_COC);
        ModulSeTeam modulSeTeam = new ModulSeTeam("SeTeam 13", new Modul("4 Modul"), new HashSet<>());
        when(berechtigungWrite.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungWrite.getStatus()).thenReturn(status);
        when(berechtigungWrite.getModulSeTeam()).thenReturn(modulSeTeam);

        String kommentar = "kommentar";
        if (status == AntragStatus.ABGELEHNT) {
            when(berechtigungWrite.getKommentar()).thenReturn(kommentar);
        }

        berechtigungen.add(berechtigungWrite);
        String result = BerechtigungsantragMailUtils.getSchreibberechtigungen(berechtigungen);
        String html = "";
        switch (status) {
            case OFFEN: {
                html = "<ul style='margin-top: 0px;'><li>4 Modul > SeTeam 13</li></ul>";
                break;
            }

            case ANGENOMMEN: {
                html = "<ul style='margin-top: 0px;'><li>4 Modul > SeTeam 13</li></ul>";
                break;
            }

            case ABGELEHNT: {
                html = "<ul style='margin-top: 0px;'><li>4 Modul > SeTeam 13<br/><div style='font-style: italic;'>" + kommentar + "</div></li></ul>";
                break;
            }

            default:
                break;
        }

        Assertions.assertEquals(html, result);
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testGetSchreibberechtigungenModulSeTeamTwo(AntragStatus status) {
        when(berechtigungWrite.getRolle()).thenReturn(Rolle.E_COC);
        ModulSeTeam modulSeTeamOne = new ModulSeTeam("SeTeam 13", new Modul("4 Modul"), new HashSet<>());
        ModulSeTeam modulSeTeamTwo = new ModulSeTeam("SeTeam 14", new Modul("5 Modul"), new HashSet<>());
        when(berechtigungWrite.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungWrite.getModulSeTeam()).thenReturn(modulSeTeamOne);
        when(berechtigungWriteTwo.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungWriteTwo.getModulSeTeam()).thenReturn(modulSeTeamTwo);
        when(berechtigungWrite.getStatus()).thenReturn(status);
        when(berechtigungWriteTwo.getStatus()).thenReturn(status);

        String kommentar = "kommentar";
        String kommentarTwo = "kommentarTwo";
        if (status == AntragStatus.ABGELEHNT) {
            when(berechtigungWrite.getKommentar()).thenReturn(kommentar);
            when(berechtigungWriteTwo.getKommentar()).thenReturn(kommentarTwo);
        }

        berechtigungen.add(berechtigungWrite);
        berechtigungen.add(berechtigungWriteTwo);
        String result = BerechtigungsantragMailUtils.getSchreibberechtigungen(berechtigungen);
        String html = "";
        switch (status) {
            case OFFEN: {
                html = "<ul style='margin-top: 0px;'><li>4 Modul > SeTeam 13</li><li>5 Modul > SeTeam 14</li></ul>";
                break;
            }

            case ANGENOMMEN: {
                html = "<ul style='margin-top: 0px;'><li>4 Modul > SeTeam 13</li><li>5 Modul > SeTeam 14</li></ul>";
                break;
            }

            case ABGELEHNT: {
                html = "<ul style='margin-top: 0px;'><li>4 Modul > SeTeam 13<br/><div style='font-style: italic;'>"
                        + kommentar + "</div></li>"
                        + "<li>5 Modul > SeTeam 14<br/><div style='font-style: italic;'>"
                        + kommentarTwo + "</div></li></ul>";
                break;
            }

            default:
                break;
        }

        Assertions.assertEquals(html, result);
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testGetLeseberechtigungenSensorCocOne(AntragStatus status) {
        when(berechtigungRead.getRolle()).thenReturn(Rolle.SENSOR);
        when(berechtigungRead.getRechttype()).thenReturn(Rechttype.LESERECHT);
        when(berechtigungRead.getStatus()).thenReturn(status);
        SensorCoc sensorCoc = new SensorCoc();
        sensorCoc.setTechnologie("TGF_Sitze");
        when(berechtigungRead.getSensorCoc()).thenReturn(sensorCoc);

        String kommentar = "kommentar";
        if (status == AntragStatus.ABGELEHNT) {
            when(berechtigungRead.getKommentar()).thenReturn(kommentar);
        }

        berechtigungen.add(berechtigungRead);
        String result = BerechtigungsantragMailUtils.getLeseberechtigungen(berechtigungen);
        String html = "";
        switch (status) {
            case OFFEN: {
                html = "<ul style='list-style-type: circle; margin-top: 0px;'><li>TGF_Sitze</li></ul>";
                break;
            }

            case ANGENOMMEN: {
                html = "<ul style='list-style-type: circle; margin-top: 0px;'><li>TGF_Sitze</li></ul>";
                break;
            }

            case ABGELEHNT: {
                html = "<ul style='list-style-type: circle; margin-top: 0px;'><li>TGF_Sitze<br/><div style='font-style: italic;'>" + kommentar + "</div></li></ul>";
                break;
            }

            default:
                break;
        }

        Assertions.assertEquals(html, result);
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testGetLeseberechtigungenSensorCocTwo(AntragStatus status) {
        when(berechtigungRead.getRolle()).thenReturn(Rolle.SENSOR);
        SensorCoc sensorCocOne = new SensorCoc();
        sensorCocOne.setTechnologie("TGF_Sitze");
        SensorCoc sensorCocTwo = new SensorCoc();
        sensorCocTwo.setTechnologie("TGF_Antrieb");
        when(berechtigungRead.getRechttype()).thenReturn(Rechttype.LESERECHT);
        when(berechtigungRead.getSensorCoc()).thenReturn(sensorCocOne);
        when(berechtigungReadTwo.getRechttype()).thenReturn(Rechttype.LESERECHT);
        when(berechtigungReadTwo.getSensorCoc()).thenReturn(sensorCocTwo);
        when(berechtigungRead.getStatus()).thenReturn(status);
        when(berechtigungReadTwo.getStatus()).thenReturn(status);

        String kommentar = "kommentar";
        String kommentarTwo = "kommentarTwo";
        if (status == AntragStatus.ABGELEHNT) {
            when(berechtigungRead.getKommentar()).thenReturn(kommentar);
            when(berechtigungReadTwo.getKommentar()).thenReturn(kommentarTwo);
        }

        berechtigungen.add(berechtigungRead);
        berechtigungen.add(berechtigungReadTwo);
        String result = BerechtigungsantragMailUtils.getLeseberechtigungen(berechtigungen);
        String html = "";
        switch (status) {
            case OFFEN: {
                html = "<ul style='list-style-type: circle; margin-top: 0px;'><li>TGF_Sitze</li><li>TGF_Antrieb</li></ul>";
                break;
            }

            case ANGENOMMEN: {
                html = "<ul style='list-style-type: circle; margin-top: 0px;'><li>TGF_Sitze</li><li>TGF_Antrieb</li></ul>";
                break;
            }

            case ABGELEHNT: {
                html = "<ul style='list-style-type: circle; margin-top: 0px;'><li>TGF_Sitze<br/><div style='font-style: italic;'>"
                        + kommentar + "</div></li>"
                        + "<li>TGF_Antrieb<br/><div style='font-style: italic;'>"
                        + kommentarTwo + "</div></li></ul>";
                break;
            }

            default:
                break;
        }

        Assertions.assertEquals(html, result);
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testGetLeseberechtigungenTteamOne(AntragStatus status) {
        when(berechtigungRead.getRolle()).thenReturn(Rolle.TTEAMMITGLIED);
        Tteam tteam = new Tteam("Unterboden");
        when(berechtigungRead.getRechttype()).thenReturn(Rechttype.LESERECHT);
        when(berechtigungRead.getStatus()).thenReturn(status);
        when(berechtigungRead.getTteam()).thenReturn(tteam);

        String kommentar = "kommentar";
        if (status == AntragStatus.ABGELEHNT) {
            when(berechtigungRead.getKommentar()).thenReturn(kommentar);
        }

        berechtigungen.add(berechtigungRead);
        String result = BerechtigungsantragMailUtils.getLeseberechtigungen(berechtigungen);
        String html = "";
        switch (status) {
            case OFFEN: {
                html = "<ul style='list-style-type: circle; margin-top: 0px;'><li>Unterboden</li></ul>";
                break;
            }

            case ANGENOMMEN: {
                html = "<ul style='list-style-type: circle; margin-top: 0px;'><li>Unterboden</li></ul>";
                break;
            }

            case ABGELEHNT: {
                html = "<ul style='list-style-type: circle; margin-top: 0px;'><li>Unterboden<br/><div style='font-style: italic;'>" + kommentar + "</div></li></ul>";
                break;
            }

            default:
                break;
        }

        Assertions.assertEquals(html, result);
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testGetLeseberechtigungenTteamTwo(AntragStatus status) {
        when(berechtigungRead.getRolle()).thenReturn(Rolle.TTEAMMITGLIED);
        Tteam tteamOne = new Tteam("Unterboden");
        Tteam tteamTwo = new Tteam("Verdeck");
        when(berechtigungRead.getRechttype()).thenReturn(Rechttype.LESERECHT);
        when(berechtigungRead.getTteam()).thenReturn(tteamOne);
        when(berechtigungReadTwo.getRechttype()).thenReturn(Rechttype.LESERECHT);
        when(berechtigungReadTwo.getTteam()).thenReturn(tteamTwo);
        when(berechtigungRead.getStatus()).thenReturn(status);
        when(berechtigungReadTwo.getStatus()).thenReturn(status);

        String kommentar = "kommentar";
        String kommentarTwo = "kommentarTwo";
        if (status == AntragStatus.ABGELEHNT) {
            when(berechtigungRead.getKommentar()).thenReturn(kommentar);
            when(berechtigungReadTwo.getKommentar()).thenReturn(kommentarTwo);
        }

        berechtigungen.add(berechtigungRead);
        berechtigungen.add(berechtigungReadTwo);
        String result = BerechtigungsantragMailUtils.getLeseberechtigungen(berechtigungen);
        String html = "";
        switch (status) {
            case OFFEN: {
                html = "<ul style='list-style-type: circle; margin-top: 0px;'><li>Unterboden</li><li>Verdeck</li></ul>";
                break;
            }

            case ANGENOMMEN: {
                html = "<ul style='list-style-type: circle; margin-top: 0px;'><li>Unterboden</li><li>Verdeck</li></ul>";
                break;
            }

            case ABGELEHNT: {
                html = "<ul style='list-style-type: circle; margin-top: 0px;'><li>Unterboden<br/><div style='font-style: italic;'>"
                        + kommentar + "</div></li>"
                        + "<li>Verdeck<br/><div style='font-style: italic;'>"
                        + kommentarTwo + "</div></li></ul>";
                break;
            }

            default:
                break;
        }

        Assertions.assertEquals(html, result);
    }

    @Test
    public void testGetSchreibberechtigungenAnzahlEmpty() {
        String result = BerechtigungsantragMailUtils.getSchreibberechtigungenAnzahl(berechtigungen);
        Assertions.assertEquals("", result);
    }

    @Test
    public void testGetSchreibberechtigungenAnzahlEnEmpty() {
        String result = BerechtigungsantragMailUtils.getSchreibberechtigungenAnzahlEn(berechtigungen);
        Assertions.assertEquals("", result);
    }

    @Test
    public void testGetLeseberechtigungenAnzahlEmpty() {
        String result = BerechtigungsantragMailUtils.getLeseberechtigungenAnzahl(berechtigungen);
        Assertions.assertEquals("", result);
    }

    @Test
    public void testGetLeseberechtigungenAnzahlEnEnEmpty() {
        String result = BerechtigungsantragMailUtils.getLeseberechtigungenAnzahlEn(berechtigungen);
        Assertions.assertEquals("", result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"SensorCoC", "T-Team", "ModulSeTeam"})
    public void testGetSchreibberechtigungenAnzahlOne(String berechtigungZiel) {
        when(berechtigungWrite.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        Rolle rolle = getBerechtigungRolle(berechtigungZiel);
        when(berechtigungWrite.getRolle()).thenReturn(rolle);
        berechtigungen.add(berechtigungWrite);
        String result = BerechtigungsantragMailUtils.getSchreibberechtigungenAnzahl(berechtigungen);
        String html = "<label>Schreibberechtigungen für " + berechtigungZiel + ":" + "</label>";
        Assertions.assertEquals(html, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"SensorCoC", "T-Team", "ModulSeTeam"})
    public void testGetSchreibberechtigungenAnzahlEnOne(String berechtigungZiel) {
        when(berechtigungWrite.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        Rolle rolle = getBerechtigungRolle(berechtigungZiel);
        when(berechtigungWrite.getRolle()).thenReturn(rolle);
        berechtigungen.add(berechtigungWrite);
        String result = BerechtigungsantragMailUtils.getSchreibberechtigungenAnzahlEn(berechtigungen);
        String html = "<label>Write Authorisations for " + berechtigungZiel + ":" + "</label>";
        Assertions.assertEquals(html, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"SensorCoC", "T-Team", "ModulSeTeam"})
    public void testGetLeseberechtigungenAnzahlOne(String berechtigungZiel) {
        when(berechtigungRead.getRechttype()).thenReturn(Rechttype.LESERECHT);
        Rolle rolle = getBerechtigungRolle(berechtigungZiel);
        when(berechtigungRead.getRolle()).thenReturn(rolle);
        berechtigungen.add(berechtigungRead);
        String result = BerechtigungsantragMailUtils.getLeseberechtigungenAnzahl(berechtigungen);
        String html = "<label>Leseberechtigungen für " + berechtigungZiel + ":" + "</label>";
        Assertions.assertEquals(html, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"SensorCoC", "T-Team", "ModulSeTeam"})
    public void testGetLeseberechtigungenAnzahlEnOne(String berechtigungZiel) {
        when(berechtigungRead.getRechttype()).thenReturn(Rechttype.LESERECHT);
        Rolle rolle = getBerechtigungRolle(berechtigungZiel);
        when(berechtigungRead.getRolle()).thenReturn(rolle);
        berechtigungen.add(berechtigungRead);
        String result = BerechtigungsantragMailUtils.getLeseberechtigungenAnzahlEn(berechtigungen);
        String html = "<label>Read Authorisations for " + berechtigungZiel + ":" + "</label>";
        Assertions.assertEquals(html, result);
    }

    private Rolle getBerechtigungRolle(String berechtigungZiel) {
        Rolle rolle = null;
        switch (berechtigungZiel) {
            case "SensorCoC":
                rolle = Rolle.SENSOR;
                break;
            case "T-Team":
                rolle = Rolle.TTEAMMITGLIED;
                break;
            case "ModulSeTeam":
                rolle = Rolle.E_COC;
                break;
            default:
                break;
        }
        return rolle;
    }

    @ParameterizedTest
    @EnumSource(value = AntragStatus.class, names = {"ANGENOMMEN", "ABGELEHNT"})
    public void testGetStatusUpdateMessage(AntragStatus status) {
        when(berechtigungWrite.getStatus()).thenReturn(status);
        berechtigungen.add(berechtigungWrite);

        String statusUpdateMessage = "";
        switch (status) {
            case ANGENOMMEN: {
                statusUpdateMessage = "angenommen";
                break;
            }

            case ABGELEHNT: {
                statusUpdateMessage = "abgelehnt";
                break;
            }

            default:
                break;

        }

        String result = BerechtigungsantragMailUtils.getStatusUpdateMessage(berechtigungen);
        Assertions.assertEquals(statusUpdateMessage, result);
    }

    @ParameterizedTest
    @EnumSource(value = AntragStatus.class, names = {"ANGENOMMEN", "ABGELEHNT"})
    public void testGetStatusUpdateMessageEn(AntragStatus status) {
        when(berechtigungWrite.getStatus()).thenReturn(status);
        berechtigungen.add(berechtigungWrite);

        String statusUpdateMessage = "";
        switch (status) {
            case ANGENOMMEN: {
                statusUpdateMessage = "granted to You";
                break;
            }

            case ABGELEHNT: {
                statusUpdateMessage = "denied";
                break;
            }

            default:
                break;

        }

        String result = BerechtigungsantragMailUtils.getStatusUpdateMessageEn(berechtigungen);
        Assertions.assertEquals(statusUpdateMessage, result);
    }

    @Test
    public void testGetKommentarForNoKommentar() {
        when(berechtigungWrite.getKommentar()).thenReturn(null);
        String result = BerechtigungsantragMailUtils.getKommentar(berechtigungWrite);
        Assertions.assertEquals("", result);
    }

    @Test
    public void testGetKommentarForSomeKommentar() {
        when(berechtigungWrite.getKommentar()).thenReturn("kommentar");
        String result = BerechtigungsantragMailUtils.getKommentar(berechtigungWrite);
        Assertions.assertEquals("kommentar", result);
    }

}
