package de.interfaceag.bmw.pzbk.berechtigungantrag.mail;

import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.AntragStatus;
import de.interfaceag.bmw.pzbk.enums.Attribut;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.mail.MailArgs;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.mail.MailGeneratorService;
import de.interfaceag.bmw.pzbk.shared.mail.MailSendService;
import de.interfaceag.bmw.pzbk.shared.mail.MailSessionInitializer;
import de.interfaceag.bmw.pzbk.shared.mail.MailTemplatesService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javax.mail.Message;
import javax.mail.MessagingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class BerechtigungsantragMailServiceTest {

    @Mock
    private MailSendService mailSendService;

    @Mock
    private MailGeneratorService mailGeneratorService;

    @Mock
    private LocalizationService localizationService;

    @Mock
    private MailTemplatesService mailTemplatesService;

    @Mock
    private Session session;

    @Mock
    private Date date;

    @Mock
    private Berechtigungsantrag berechtigungsantrag;

    @Mock
    private Berechtigungsantrag berechtigungsantragTwo;

    @InjectMocks
    private BerechtigungsantragMailService berechtigungsantragMailService;

    private List<Berechtigungsantrag> berechtigungsantraege;

    private Mitarbeiter antragsteller;
    private Mitarbeiter bearbeiter;

    @BeforeEach
    public void setUp() throws MessagingException {
        antragsteller = TestDataFactory.generateMitarbeiter("Zack", "Logan", "Abt.-2", "q125896");
        antragsteller.setEmail("zack.logan@interface-ag.de");

        bearbeiter = TestDataFactory.generateMitarbeiter("Max", "Admin", "Abt.-2", "q2222222");
        bearbeiter.setEmail("max.admin@interface-ag.de");
        bearbeiter.setTel("1111111");

        berechtigungsantraege = new ArrayList<>();

        when(mailGeneratorService.generateLogoHtml()).thenCallRealMethod();
        when(mailGeneratorService.buildContentFromTemplate(any(String.class), any(MailArgs.class))).thenCallRealMethod();
        when(mailGeneratorService.getMailSession()).thenReturn(MailSessionInitializer.getMailSession());
    }

    @Test
    public void testSendConfirmationMailForSensorWriteOnly() throws MessagingException, IOException {
        when(session.getUser()).thenReturn(antragsteller);
        when(berechtigungsantrag.getRolle()).thenReturn(Rolle.SENSOR);
        when(berechtigungsantragTwo.getRolle()).thenReturn(Rolle.SENSOR);
        when(berechtigungsantrag.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungsantragTwo.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        SensorCoc sensorCocOne = new SensorCoc();
        sensorCocOne.setTechnologie("TGF_Antrieb");
        SensorCoc sensorCocTwo = new SensorCoc();
        sensorCocTwo.setTechnologie("TGF_Sitze");
        when(berechtigungsantrag.getSensorCoc()).thenReturn(sensorCocOne);
        when(berechtigungsantragTwo.getSensorCoc()).thenReturn(sensorCocTwo);
        when(berechtigungsantrag.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantragTwo.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantrag.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(berechtigungsantragTwo.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(mailTemplatesService.getMailTemplate(Attribut.BERECHTIGUNGSANTRAG_EINGANGSBESTAETIGUNG)).thenReturn(readTemplateFromFile(AntragStatus.OFFEN));
        berechtigungsantraege.add(berechtigungsantrag);
        berechtigungsantraege.add(berechtigungsantragTwo);
        when(localizationService.getValue("mail_berechtigungsantrag_eingangsbestaetigung")).thenReturn("Berechtigungsantrag Eingangsbestaetigung");
        when(mailGeneratorService.isProductionEnvironment()).thenReturn(false);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();

        berechtigungsantragMailService.sendMailBasedOnAntragStatus(berechtigungsantraege);
        verify(mailSendService).sendMail(any(Message.class));
    }

    @Test
    public void testSendConfirmationMailForSensorReadOnly() throws MessagingException, IOException {
        when(session.getUser()).thenReturn(antragsteller);
        when(berechtigungsantrag.getRolle()).thenReturn(Rolle.SENSOR);
        when(berechtigungsantragTwo.getRolle()).thenReturn(Rolle.SENSOR);
        when(berechtigungsantrag.getRechttype()).thenReturn(Rechttype.LESERECHT);
        when(berechtigungsantragTwo.getRechttype()).thenReturn(Rechttype.LESERECHT);
        SensorCoc sensorCocOne = new SensorCoc();
        sensorCocOne.setTechnologie("TGF_Antrieb");
        SensorCoc sensorCocTwo = new SensorCoc();
        sensorCocTwo.setTechnologie("TGF_Sitze");
        when(berechtigungsantrag.getSensorCoc()).thenReturn(sensorCocOne);
        when(berechtigungsantragTwo.getSensorCoc()).thenReturn(sensorCocTwo);
        when(berechtigungsantrag.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantragTwo.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantrag.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(berechtigungsantragTwo.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(mailTemplatesService.getMailTemplate(Attribut.BERECHTIGUNGSANTRAG_EINGANGSBESTAETIGUNG)).thenReturn(readTemplateFromFile(AntragStatus.OFFEN));
        berechtigungsantraege.add(berechtigungsantrag);
        berechtigungsantraege.add(berechtigungsantragTwo);
        when(localizationService.getValue("mail_berechtigungsantrag_eingangsbestaetigung")).thenReturn("Berechtigungsantrag Eingangsbestaetigung");
        when(mailGeneratorService.isProductionEnvironment()).thenReturn(false);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();

        berechtigungsantragMailService.sendMailBasedOnAntragStatus(berechtigungsantraege);
        verify(mailSendService).sendMail(any(Message.class));
    }

    @Test
    public void testSendConfirmationMailForSensorWriteAndRead() throws MessagingException, IOException {
        when(session.getUser()).thenReturn(antragsteller);
        when(berechtigungsantrag.getRolle()).thenReturn(Rolle.SENSOR);
        when(berechtigungsantragTwo.getRolle()).thenReturn(Rolle.SENSOR);
        when(berechtigungsantrag.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungsantragTwo.getRechttype()).thenReturn(Rechttype.LESERECHT);
        SensorCoc sensorCocOne = new SensorCoc();
        sensorCocOne.setTechnologie("TGF_Antrieb");
        SensorCoc sensorCocTwo = new SensorCoc();
        sensorCocTwo.setTechnologie("TGF_Sitze");
        when(berechtigungsantrag.getSensorCoc()).thenReturn(sensorCocOne);
        when(berechtigungsantragTwo.getSensorCoc()).thenReturn(sensorCocTwo);
        when(berechtigungsantrag.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantragTwo.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantrag.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(berechtigungsantragTwo.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(mailTemplatesService.getMailTemplate(Attribut.BERECHTIGUNGSANTRAG_EINGANGSBESTAETIGUNG)).thenReturn(readTemplateFromFile(AntragStatus.OFFEN));
        berechtigungsantraege.add(berechtigungsantrag);
        berechtigungsantraege.add(berechtigungsantragTwo);
        when(localizationService.getValue("mail_berechtigungsantrag_eingangsbestaetigung")).thenReturn("Berechtigungsantrag Eingangsbestaetigung");
        when(mailGeneratorService.isProductionEnvironment()).thenReturn(false);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();

        berechtigungsantragMailService.sendMailBasedOnAntragStatus(berechtigungsantraege);
        verify(mailSendService).sendMail(any(Message.class));
    }

    @Test
    public void testSendConfirmationMailForSensorEingWriteOnly() throws MessagingException, IOException {
        when(session.getUser()).thenReturn(antragsteller);
        when(berechtigungsantrag.getRolle()).thenReturn(Rolle.SENSOR_EINGESCHRAENKT);
        when(berechtigungsantragTwo.getRolle()).thenReturn(Rolle.SENSOR_EINGESCHRAENKT);
        when(berechtigungsantrag.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungsantragTwo.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        SensorCoc sensorCocOne = new SensorCoc();
        sensorCocOne.setTechnologie("TGF_Antrieb");
        SensorCoc sensorCocTwo = new SensorCoc();
        sensorCocTwo.setTechnologie("TGF_Sitze");
        when(berechtigungsantrag.getSensorCoc()).thenReturn(sensorCocOne);
        when(berechtigungsantragTwo.getSensorCoc()).thenReturn(sensorCocTwo);
        when(berechtigungsantrag.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantragTwo.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantrag.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(berechtigungsantragTwo.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(mailTemplatesService.getMailTemplate(Attribut.BERECHTIGUNGSANTRAG_EINGANGSBESTAETIGUNG)).thenReturn(readTemplateFromFile(AntragStatus.OFFEN));
        berechtigungsantraege.add(berechtigungsantrag);
        berechtigungsantraege.add(berechtigungsantragTwo);
        when(localizationService.getValue("mail_berechtigungsantrag_eingangsbestaetigung")).thenReturn("Berechtigungsantrag Eingangsbestaetigung");
        when(mailGeneratorService.isProductionEnvironment()).thenReturn(false);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();

        berechtigungsantragMailService.sendMailBasedOnAntragStatus(berechtigungsantraege);
        verify(mailSendService).sendMail(any(Message.class));
    }

    @Test
    public void testSendConfirmationMailForSensorEingReadOnly() throws MessagingException, IOException {
        when(session.getUser()).thenReturn(antragsteller);
        when(berechtigungsantrag.getRolle()).thenReturn(Rolle.SENSOR_EINGESCHRAENKT);
        when(berechtigungsantragTwo.getRolle()).thenReturn(Rolle.SENSOR_EINGESCHRAENKT);
        when(berechtigungsantrag.getRechttype()).thenReturn(Rechttype.LESERECHT);
        when(berechtigungsantragTwo.getRechttype()).thenReturn(Rechttype.LESERECHT);
        SensorCoc sensorCocOne = new SensorCoc();
        sensorCocOne.setTechnologie("TGF_Antrieb");
        SensorCoc sensorCocTwo = new SensorCoc();
        sensorCocTwo.setTechnologie("TGF_Sitze");
        when(berechtigungsantrag.getSensorCoc()).thenReturn(sensorCocOne);
        when(berechtigungsantragTwo.getSensorCoc()).thenReturn(sensorCocTwo);
        when(berechtigungsantrag.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantragTwo.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantrag.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(berechtigungsantragTwo.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(mailTemplatesService.getMailTemplate(Attribut.BERECHTIGUNGSANTRAG_EINGANGSBESTAETIGUNG)).thenReturn(readTemplateFromFile(AntragStatus.OFFEN));
        berechtigungsantraege.add(berechtigungsantrag);
        berechtigungsantraege.add(berechtigungsantragTwo);
        when(localizationService.getValue("mail_berechtigungsantrag_eingangsbestaetigung")).thenReturn("Berechtigungsantrag Eingangsbestaetigung");
        when(mailGeneratorService.isProductionEnvironment()).thenReturn(false);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();

        berechtigungsantragMailService.sendMailBasedOnAntragStatus(berechtigungsantraege);
        verify(mailSendService).sendMail(any(Message.class));
    }

    @Test
    public void testSendConfirmationMailForSensorEingWriteAndRead() throws MessagingException, IOException {
        when(session.getUser()).thenReturn(antragsteller);
        when(berechtigungsantrag.getRolle()).thenReturn(Rolle.SENSOR_EINGESCHRAENKT);
        when(berechtigungsantragTwo.getRolle()).thenReturn(Rolle.SENSOR_EINGESCHRAENKT);
        when(berechtigungsantrag.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungsantragTwo.getRechttype()).thenReturn(Rechttype.LESERECHT);
        SensorCoc sensorCocOne = new SensorCoc();
        sensorCocOne.setTechnologie("TGF_Antrieb");
        SensorCoc sensorCocTwo = new SensorCoc();
        sensorCocTwo.setTechnologie("TGF_Sitze");
        when(berechtigungsantrag.getSensorCoc()).thenReturn(sensorCocOne);
        when(berechtigungsantragTwo.getSensorCoc()).thenReturn(sensorCocTwo);
        when(berechtigungsantrag.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantragTwo.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantrag.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(berechtigungsantragTwo.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(mailTemplatesService.getMailTemplate(Attribut.BERECHTIGUNGSANTRAG_EINGANGSBESTAETIGUNG)).thenReturn(readTemplateFromFile(AntragStatus.OFFEN));
        berechtigungsantraege.add(berechtigungsantrag);
        berechtigungsantraege.add(berechtigungsantragTwo);
        when(localizationService.getValue("mail_berechtigungsantrag_eingangsbestaetigung")).thenReturn("Berechtigungsantrag Eingangsbestaetigung");
        when(mailGeneratorService.isProductionEnvironment()).thenReturn(false);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();

        berechtigungsantragMailService.sendMailBasedOnAntragStatus(berechtigungsantraege);
        verify(mailSendService).sendMail(any(Message.class));
    }

    @Test
    public void testSendConfirmationMailForTteamMitgliedWriteOnly() throws MessagingException, IOException {
        when(session.getUser()).thenReturn(antragsteller);
        when(berechtigungsantrag.getRolle()).thenReturn(Rolle.TTEAMMITGLIED);
        when(berechtigungsantragTwo.getRolle()).thenReturn(Rolle.TTEAMMITGLIED);
        when(berechtigungsantrag.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungsantragTwo.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        Tteam tteamOne = new Tteam("Verdeck");
        Tteam tteamTwo = new Tteam("Unterboden");
        when(berechtigungsantrag.getTteam()).thenReturn(tteamOne);
        when(berechtigungsantragTwo.getTteam()).thenReturn(tteamTwo);
        when(berechtigungsantrag.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantragTwo.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantrag.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(berechtigungsantragTwo.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(mailTemplatesService.getMailTemplate(Attribut.BERECHTIGUNGSANTRAG_EINGANGSBESTAETIGUNG)).thenReturn(readTemplateFromFile(AntragStatus.OFFEN));
        berechtigungsantraege.add(berechtigungsantrag);
        berechtigungsantraege.add(berechtigungsantragTwo);
        when(localizationService.getValue("mail_berechtigungsantrag_eingangsbestaetigung")).thenReturn("Berechtigungsantrag Eingangsbestaetigung");
        when(mailGeneratorService.isProductionEnvironment()).thenReturn(false);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();

        berechtigungsantragMailService.sendMailBasedOnAntragStatus(berechtigungsantraege);
        verify(mailSendService).sendMail(any(Message.class));
    }

    @Test
    public void testSendConfirmationMailForTteamMitgliedReadOnly() throws MessagingException, IOException {
        when(session.getUser()).thenReturn(antragsteller);
        when(berechtigungsantrag.getRolle()).thenReturn(Rolle.TTEAMMITGLIED);
        when(berechtigungsantragTwo.getRolle()).thenReturn(Rolle.TTEAMMITGLIED);
        when(berechtigungsantrag.getRechttype()).thenReturn(Rechttype.LESERECHT);
        when(berechtigungsantragTwo.getRechttype()).thenReturn(Rechttype.LESERECHT);
        Tteam tteamOne = new Tteam("Verdeck");
        Tteam tteamTwo = new Tteam("Unterboden");
        when(berechtigungsantrag.getTteam()).thenReturn(tteamOne);
        when(berechtigungsantragTwo.getTteam()).thenReturn(tteamTwo);
        when(berechtigungsantrag.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantragTwo.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantrag.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(berechtigungsantragTwo.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(mailTemplatesService.getMailTemplate(Attribut.BERECHTIGUNGSANTRAG_EINGANGSBESTAETIGUNG)).thenReturn(readTemplateFromFile(AntragStatus.OFFEN));
        berechtigungsantraege.add(berechtigungsantrag);
        berechtigungsantraege.add(berechtigungsantragTwo);
        when(localizationService.getValue("mail_berechtigungsantrag_eingangsbestaetigung")).thenReturn("Berechtigungsantrag Eingangsbestaetigung");
        when(mailGeneratorService.isProductionEnvironment()).thenReturn(false);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();

        berechtigungsantragMailService.sendMailBasedOnAntragStatus(berechtigungsantraege);
        verify(mailSendService).sendMail(any(Message.class));
    }

    @Test
    public void testSendConfirmationMailForTteamMitgliedWriteAndRead() throws MessagingException, IOException {
        when(session.getUser()).thenReturn(antragsteller);
        when(berechtigungsantrag.getRolle()).thenReturn(Rolle.TTEAMMITGLIED);
        when(berechtigungsantragTwo.getRolle()).thenReturn(Rolle.TTEAMMITGLIED);
        when(berechtigungsantrag.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungsantragTwo.getRechttype()).thenReturn(Rechttype.LESERECHT);
        Tteam tteamOne = new Tteam("Verdeck");
        Tteam tteamTwo = new Tteam("Unterboden");
        when(berechtigungsantrag.getTteam()).thenReturn(tteamOne);
        when(berechtigungsantragTwo.getTteam()).thenReturn(tteamTwo);
        when(berechtigungsantrag.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantragTwo.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantrag.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(berechtigungsantragTwo.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(mailTemplatesService.getMailTemplate(Attribut.BERECHTIGUNGSANTRAG_EINGANGSBESTAETIGUNG)).thenReturn(readTemplateFromFile(AntragStatus.OFFEN));
        berechtigungsantraege.add(berechtigungsantrag);
        berechtigungsantraege.add(berechtigungsantragTwo);
        when(localizationService.getValue("mail_berechtigungsantrag_eingangsbestaetigung")).thenReturn("Berechtigungsantrag Eingangsbestaetigung");
        when(mailGeneratorService.isProductionEnvironment()).thenReturn(false);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();

        berechtigungsantragMailService.sendMailBasedOnAntragStatus(berechtigungsantraege);
        verify(mailSendService).sendMail(any(Message.class));
    }

    @Test
    public void testSendConfirmationMailForEcoc() throws MessagingException, IOException {
        when(session.getUser()).thenReturn(antragsteller);
        when(berechtigungsantrag.getRolle()).thenReturn(Rolle.E_COC);
        when(berechtigungsantragTwo.getRolle()).thenReturn(Rolle.E_COC);
        when(berechtigungsantrag.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungsantragTwo.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        ModulSeTeam modulSeTeamOne = new ModulSeTeam("SeTeam 13", new Modul("4 Modul"), new HashSet<>());
        ModulSeTeam modulSeTeamTwo = new ModulSeTeam("SeTeam 14", new Modul("5 Modul"), new HashSet<>());
        when(berechtigungsantrag.getModulSeTeam()).thenReturn(modulSeTeamOne);
        when(berechtigungsantragTwo.getModulSeTeam()).thenReturn(modulSeTeamTwo);
        when(berechtigungsantrag.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantragTwo.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantrag.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(berechtigungsantragTwo.getStatus()).thenReturn(AntragStatus.OFFEN);
        when(mailTemplatesService.getMailTemplate(Attribut.BERECHTIGUNGSANTRAG_EINGANGSBESTAETIGUNG)).thenReturn(readTemplateFromFile(AntragStatus.OFFEN));
        berechtigungsantraege.add(berechtigungsantrag);
        berechtigungsantraege.add(berechtigungsantragTwo);
        when(localizationService.getValue("mail_berechtigungsantrag_eingangsbestaetigung")).thenReturn("Berechtigungsantrag Eingangsbestaetigung");
        when(mailGeneratorService.isProductionEnvironment()).thenReturn(false);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();

        berechtigungsantragMailService.sendMailBasedOnAntragStatus(berechtigungsantraege);
        verify(mailSendService).sendMail(any(Message.class));
    }

    @ParameterizedTest
    @EnumSource(value = AntragStatus.class, names = {"ANGENOMMEN", "ABGELEHNT"})
    public void testSendAntragStatusChangeMail(AntragStatus status) throws MessagingException {
        when(session.getUser()).thenReturn(bearbeiter);
        when(berechtigungsantrag.getRolle()).thenReturn(Rolle.SENSOR);
        when(berechtigungsantrag.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        SensorCoc sensorCocOne = new SensorCoc();
        sensorCocOne.setTechnologie("TGF_Antrieb");
        when(berechtigungsantrag.getSensorCoc()).thenReturn(sensorCocOne);
        when(berechtigungsantrag.getAntragsteller()).thenReturn(antragsteller);
        when(berechtigungsantrag.getStatus()).thenReturn(status);
        when(berechtigungsantrag.getKommentar()).thenReturn("Kommentar");
        berechtigungsantraege.add(berechtigungsantrag);
        when(localizationService.getValue("mail_berechtigungsantrag_status")).thenReturn("Berechtigungsantrag Status");
        doReturn("Berechtigungsantrag aktualisiert").when(localizationService).getValue("mail_berechtigungsantrag_status_change");
        when(mailTemplatesService.getMailTemplate(Attribut.BERECHTIGUNGSANTRAG_STATUS_UPDATE)).thenReturn(readTemplateFromFile(status));
        when(mailGeneratorService.isProductionEnvironment()).thenReturn(false);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();

        berechtigungsantragMailService.sendMailBasedOnAntragStatus(berechtigungsantraege);
        verify(mailSendService).sendMail(any(Message.class));
    }

    private String readTemplateFromFile(AntragStatus status) {
        String filePath = null;
        switch (status) {
            case OFFEN:
                filePath = "/MailTemplates/MailBerechtigungsantragEingangsbestaetigung.html";
                break;
            case ANGENOMMEN:
            case ABGELEHNT:
                filePath = "/MailTemplates/MailBerechtigungsantragAktualisiert.html";
                break;
            default:
                break;
        }

        if (filePath == null) {
            return "";
        }

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(BerechtigungsantragMailServiceTest.class.getResourceAsStream(filePath), "UTF-8"))) {
            StringBuilder stringBuilder = new StringBuilder();
            String line = bufferedReader.readLine();

            while (line != null) {
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
                line = bufferedReader.readLine();
            }
            String fileContent = stringBuilder.toString();
            return fileContent;

        } catch (IOException ex) {
        }

        return "";
    }

}
