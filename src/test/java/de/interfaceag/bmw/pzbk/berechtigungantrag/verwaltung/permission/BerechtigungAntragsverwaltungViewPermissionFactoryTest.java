package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.permission;

import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.permission.BerechtigungAntragsverwaltungViewPermission;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.permission.BerechtigungAntragsverwaltungViewPermissionFactory;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragsverwaltungViewPermissionFactoryTest {

    @InjectMocks
    private BerechtigungAntragsverwaltungViewPermissionFactory berechtigungAntragsverwaltungViewPermissionFactory;

    @Mock
    private Session session;
    @Mock
    private UserPermissions userPermissions;

    @BeforeEach
    void setUp() {
        when(session.getUserPermissions()).thenReturn(userPermissions);
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getPageViewPermission(Rolle userRole) {
        when(userPermissions.getRoles()).thenReturn(Stream.of(userRole).collect(Collectors.toSet()));
        final BerechtigungAntragsverwaltungViewPermission result = berechtigungAntragsverwaltungViewPermissionFactory.getViewPermission();

        switch (userRole) {
            case ADMIN:
            case SENSORCOCLEITER:
            case T_TEAMLEITER:
            case SCL_VERTRETER:
            case TTEAM_VERTRETER:
                Assertions.assertTrue(result.getPage());
                break;
            default:
                Assertions.assertFalse(result.getPage());
                break;
        }

    }
}