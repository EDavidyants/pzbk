package de.interfaceag.bmw.pzbk.berechtigungantrag;

import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragServicePersistTest {

    @Mock
    private BerechtigungAntragDao berechtigungAntragDao;
    @InjectMocks
    private BerechtigungAntragService berechtigungAntragService;

    @Mock
    private Berechtigungsantrag berechtigungsantrag1;

    @Test
    void persistBerechtigungAntrag() {
        berechtigungAntragService.persistBerechtigungAntrag(berechtigungsantrag1);
        verify(berechtigungAntragDao, times(1)).save(berechtigungsantrag1);
    }

}