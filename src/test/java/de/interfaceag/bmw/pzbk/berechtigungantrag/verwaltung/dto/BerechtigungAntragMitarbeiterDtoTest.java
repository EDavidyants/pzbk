package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto;

import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterAntrag;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterDto;
import org.hamcrest.collection.IsEmptyCollection;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragMitarbeiterDtoTest {

    private static final String MITARBEITER_LABEL = "Lorenz";
    private static final long MITARBEITER_ID = 42L;

    private BerechtigungAntragMitarbeiterDto berechtigungAntragMitarbeiterDto;

    @Mock
    private BerechtigungAntragMitarbeiterAntrag berechtigungAntragMitarbeiterAntrag;

    @BeforeEach
    void setUp() {
        berechtigungAntragMitarbeiterDto = new BerechtigungAntragMitarbeiterDto(MITARBEITER_ID, MITARBEITER_LABEL);
    }

    @Test
    void toString1() {
        final String result = berechtigungAntragMitarbeiterDto.toString();
        assertThat(result, is("BerechtigungAntragMitarbeiterDto[mitarbeiterId=42, mitarbeiterLabel='Lorenz', antraege=[]]"));
    }

    @Test
    void getMitarbeiterLabel() {
        final String result = berechtigungAntragMitarbeiterDto.getMitarbeiterLabel();
        assertThat(result, is(MITARBEITER_LABEL));
    }

    @Test
    void getMitarbeiterId() {
        final Long result = berechtigungAntragMitarbeiterDto.getMitarbeiterId();
        assertThat(result, is(MITARBEITER_ID));
    }

    @Test
    void getAntraege() {
        final List<BerechtigungAntragMitarbeiterAntrag> result = berechtigungAntragMitarbeiterDto.getAntraege();
        assertThat(result, IsEmptyCollection.empty());
    }

    @Test
    void addAntrag() {
        berechtigungAntragMitarbeiterDto.addAntrag(berechtigungAntragMitarbeiterAntrag);
        final List<BerechtigungAntragMitarbeiterAntrag> result = berechtigungAntragMitarbeiterDto.getAntraege();
        assertThat(result, IsIterableContaining.hasItem(berechtigungAntragMitarbeiterAntrag));
    }
}