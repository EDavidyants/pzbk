package de.interfaceag.bmw.pzbk.berechtigungantrag.history;

import de.interfaceag.bmw.pzbk.berechtigungantrag.history.dto.BerechtigungAntragVerwaltungHistoryDto;
import de.interfaceag.bmw.pzbk.berechtigungantrag.history.dto.BerechtigungAntragVerwaltungHistoryViewData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragVerwaltungHistoryFacadeTest {

    @Mock
    private BerechtigungAntragVerwaltungHistoryService berechtigungAntragVerwaltungHistoryService;
    @InjectMocks
    private BerechtigungAntragVerwaltungViewDataFactory berechtigungAntragVerwaltungViewDataFactory;

    @Mock
    private BerechtigungAntragVerwaltungHistoryDto berechtigungAntragVerwaltungHistoryDto;

    @Test
    void testInitViewData() {
        List<BerechtigungAntragVerwaltungHistoryDto> resultList = Collections.singletonList(berechtigungAntragVerwaltungHistoryDto);

        when(berechtigungAntragVerwaltungHistoryService.getAllNotOffenBerechtigungAntraege()).thenReturn(resultList);

        BerechtigungAntragVerwaltungHistoryViewData viewDate = berechtigungAntragVerwaltungViewDataFactory.initViewData();

        Assertions.assertEquals(resultList, viewDate.getBerechtigungAntragVerwaltungHistoryDtoList());
    }

}