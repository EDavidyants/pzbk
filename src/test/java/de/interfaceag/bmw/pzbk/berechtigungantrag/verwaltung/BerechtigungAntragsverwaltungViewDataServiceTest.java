package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung;

import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiter;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragsverwaltungViewData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragsverwaltungViewDataServiceTest {

    @Mock
    private BerechtigungAntragVerwaltungService berechtigungAntragVerwaltungService;
    @InjectMocks
    private BerechtigungAntragsverwaltungViewDataFactory berechtigungAntragsverwaltungViewDataService;

    @Mock
    private List<BerechtigungAntragMitarbeiter> allOpenBerechtigungAntraegeForView;

    @BeforeEach
    void setUp() {
        when(berechtigungAntragVerwaltungService.getAllOpenBerechtigungAntraegeForView()).thenReturn(allOpenBerechtigungAntraegeForView);
    }

    @Test
    void getViewData() {
        final BerechtigungAntragsverwaltungViewData viewData = berechtigungAntragsverwaltungViewDataService.getViewData();
        final List<BerechtigungAntragMitarbeiter> result = viewData.getBerechtigungAntragMitarbeiter();
        assertThat(result, is(allOpenBerechtigungAntraegeForView));
    }
}