package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung;

import de.interfaceag.bmw.pzbk.berechtigungantrag.BerechtigungAntragService;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterAntrag;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiterAntragDto;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragsverwaltungFacadeTest {

    @InjectMocks
    private BerechtigungAntragsverwaltungFacade berechtigungAntragsverwaltungFacade;

    @Mock
    private BerechtigungAntragService berechtigungAntragService;

    private BerechtigungAntragMitarbeiterAntrag berechtigungAntragMitarbeiterAntrag;

    @BeforeEach
    void setUp() {
        berechtigungAntragMitarbeiterAntrag = new BerechtigungAntragMitarbeiterAntragDto(1L , Rolle.SENSOR, Rechttype.SCHREIBRECHT, "Label", new Date(2019, 1, 1));
    }

    @Test
    void antragAnnehmen() {
        berechtigungAntragsverwaltungFacade.antragAnnehmen(berechtigungAntragMitarbeiterAntrag);

        verify(berechtigungAntragService, times(1)).antragAnnehmen(any());
        verify(berechtigungAntragService, times(1)).getAllOpenBerechtigungAntraegeForView();
    }

    @Test
    void antragAblehnen() {
        berechtigungAntragsverwaltungFacade.antragAblehnen(berechtigungAntragMitarbeiterAntrag);

        verify(berechtigungAntragService, times(1)).antragAblehnen(any());
        verify(berechtigungAntragService, times(1)).getAllOpenBerechtigungAntraegeForView();
    }
}