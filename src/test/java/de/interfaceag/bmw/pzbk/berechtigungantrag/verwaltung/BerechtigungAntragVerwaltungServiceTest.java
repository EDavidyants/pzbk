package de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung;

import de.interfaceag.bmw.pzbk.berechtigungantrag.BerechtigungAntragService;
import de.interfaceag.bmw.pzbk.berechtigungantrag.permission.BerechtigungAntragVerwaltungPermissionService;
import de.interfaceag.bmw.pzbk.berechtigungantrag.verwaltung.dto.BerechtigungAntragMitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BerechtigungAntragVerwaltungServiceTest {

    @Mock
    private BerechtigungAntragService berechtigungAntragService;
    @Mock
    private BerechtigungAntragVerwaltungPermissionService berechtigungAntragVerwaltungPermissionService;
    @InjectMocks
    private BerechtigungAntragVerwaltungService berechtigungAntragVerwaltungService;

    @Mock
    private Berechtigungsantrag berechtigungsantrag1;
    @Mock
    private Berechtigungsantrag berechtigungsantrag2;
    @Mock
    private Berechtigungsantrag berechtigungsantrag3;

    @Mock
    private Mitarbeiter antragsteller1;
    @Mock
    private Mitarbeiter antragsteller2;

    @Mock
    private SensorCoc sensorCoc1;
    @Mock
    private SensorCoc sensorCoc2;

    @BeforeEach
    void setUp() {
        List<Berechtigungsantrag> allOpenBerechtigungAntraegeForView = Arrays.asList(berechtigungsantrag1, berechtigungsantrag2, berechtigungsantrag3);

        when(antragsteller1.getId()).thenReturn(42L);
        when(antragsteller1.toString()).thenReturn("DER Lorenz");

        when(antragsteller2.getId()).thenReturn(43L);
        when(antragsteller2.toString()).thenReturn("DER andere Lorenz");

        when(sensorCoc1.pathToString()).thenReturn("SENSORCOC 1");
        when(sensorCoc2.pathToString()).thenReturn("SENSORCOC 2");

        when(berechtigungsantrag1.getAntragsteller()).thenReturn(antragsteller1);
        when(berechtigungsantrag1.getId()).thenReturn(1L);
        when(berechtigungsantrag1.getRolle()).thenReturn(Rolle.SENSOR);
        when(berechtigungsantrag1.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungsantrag1.getSensorCoc()).thenReturn(sensorCoc1);
        when(berechtigungsantrag1.getErstellungsdatum()).thenReturn(new Date(2019, 1, 1));

        when(berechtigungsantrag2.getAntragsteller()).thenReturn(antragsteller1);
        when(berechtigungsantrag2.getId()).thenReturn(2L);
        when(berechtigungsantrag2.getRolle()).thenReturn(Rolle.SENSOR);
        when(berechtigungsantrag2.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungsantrag2.getSensorCoc()).thenReturn(sensorCoc2);
        when(berechtigungsantrag2.getErstellungsdatum()).thenReturn(new Date(2019, 1, 1));

        when(berechtigungsantrag3.getAntragsteller()).thenReturn(antragsteller2);
        when(berechtigungsantrag3.getId()).thenReturn(3L);
        when(berechtigungsantrag3.getRolle()).thenReturn(Rolle.SENSOR);
        when(berechtigungsantrag3.getRechttype()).thenReturn(Rechttype.SCHREIBRECHT);
        when(berechtigungsantrag3.getSensorCoc()).thenReturn(sensorCoc1);
        when(berechtigungsantrag3.getErstellungsdatum()).thenReturn(new Date(2019, 1, 1));

        when(berechtigungAntragService.getAllOpenBerechtigungAntraegeForView()).thenReturn(allOpenBerechtigungAntraegeForView);
        when(berechtigungAntragVerwaltungPermissionService.restrictToCurrentUser(allOpenBerechtigungAntraegeForView)).thenReturn(allOpenBerechtigungAntraegeForView);
    }

    @Test
    void getAllOpenBerechtigungAntraegeForViewResultSize() {
        final List<BerechtigungAntragMitarbeiter> result = berechtigungAntragVerwaltungService.getAllOpenBerechtigungAntraegeForView();
        MatcherAssert.assertThat(result, IsCollectionWithSize.hasSize(2));
    }

    @Test
    void getAllOpenBerechtigungAntraegeForViewAntragCount() {
        final List<BerechtigungAntragMitarbeiter> result = berechtigungAntragVerwaltungService.getAllOpenBerechtigungAntraegeForView();
        final long count = result.stream().map(BerechtigungAntragMitarbeiter::getAntraege).mapToLong(List::size).sum();
        MatcherAssert.assertThat(count, CoreMatchers.is(3L));
    }

}