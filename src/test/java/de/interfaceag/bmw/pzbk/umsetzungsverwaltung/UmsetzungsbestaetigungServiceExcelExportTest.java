package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.Umsetzungsbestaetigung;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.excel.ExcelDownloadService;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungExcelDto;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class UmsetzungsbestaetigungServiceExcelExportTest {

    @Mock
    private DerivatService derivatService;

    @Mock
    private LocalizationService localizationService;

    @Mock
    private ExcelDownloadService excelDownloadService;

    @InjectMocks
    private final UmsetzungsbestaetigungService umsetzungsbestaetigungService = new UmsetzungsbestaetigungService();

    private Anforderung anforderung;
    private Modul modul;
    private Derivat derivat;
    private Derivat derivat2;
    private KovAPhase vabg0;
    private KovAPhase vabg1;
    private KovAPhase vabg2;
    private KovAPhaseImDerivat kovaVABG0;
    private KovAPhaseImDerivat kovaVABG1;

    private KovAPhaseImDerivat kovaVABG21;
    private KovAPhaseImDerivat kovaVABG22;
    private UmsetzungsbestaetigungDto umsetzungFirstPhase;
    private UmsetzungsbestaetigungDto umsetzungPhaseWithPrevious;
    private UmsetzungsbestaetigungDto umsetzungPhaseWithoutPrevious;

    @BeforeEach
    public void setUp() {
        anforderung = TestDataFactory.generateAnforderung();
        modul = TestDataFactory.generateModul();
        derivat = TestDataFactory.generateDerivat("E20", "Bezeichnung E20", "LI");
        derivat2 = TestDataFactory.generateDerivat("F12", "Bezeichnung F12", "LU");
        vabg0 = KovAPhase.VABG0;
        vabg1 = KovAPhase.VABG1;
        vabg2 = KovAPhase.VABG2;
        kovaVABG0 = new KovAPhaseImDerivat(derivat, vabg0);
        kovaVABG1 = new KovAPhaseImDerivat(derivat, vabg1);

        kovaVABG21 = new KovAPhaseImDerivat(derivat2, vabg1);
        kovaVABG22 = new KovAPhaseImDerivat(derivat2, vabg2);

        UmsetzungsBestaetigungStatus previousStatus1 = UmsetzungsBestaetigungStatus.OFFEN;
        UmsetzungsBestaetigungStatus previousStatus2 = UmsetzungsBestaetigungStatus.UMGESETZT;

        String previousString1 = "Test 1";
        String umsetzungsbestaetiger = "Karl Marx";

        umsetzungFirstPhase = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(new Umsetzungsbestaetigung(anforderung, kovaVABG0, modul), previousStatus1, previousString1, umsetzungsbestaetiger);
        umsetzungFirstPhase.setComment("Kommentar");
        umsetzungFirstPhase.setStatus(UmsetzungsBestaetigungStatus.UMGESETZT);
        umsetzungPhaseWithPrevious = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(new Umsetzungsbestaetigung(anforderung, kovaVABG1, modul), previousStatus2, umsetzungFirstPhase.getComment(), umsetzungsbestaetiger);

        umsetzungPhaseWithoutPrevious = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(new Umsetzungsbestaetigung(anforderung, kovaVABG22, modul), null, null, umsetzungsbestaetiger);

        umsetzungPhaseWithPrevious.setStatus(UmsetzungsBestaetigungStatus.UMGESETZT);
        umsetzungPhaseWithoutPrevious.setStatus(UmsetzungsBestaetigungStatus.UMGESETZT);

        umsetzungPhaseWithPrevious.setComment("Kommentar One");
        umsetzungPhaseWithoutPrevious.setComment("Kommentar two");

    }

    @Test
    public void testDownloadExcelExportFirstPhase() {
        mockLocalization();

        List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungs = Arrays.asList(umsetzungFirstPhase);
        umsetzungsbestaetigungService.downloadExcelExport(umsetzungsbestaetigungs, kovaVABG0, vabg0);

        verify(excelDownloadService, times(1)).downloadExcelExport(any(String.class), any(Workbook.class));
    }

    @Test
    public void testDownloadExcelExportPhaseWithPrevious() {
        mockLocalization();

        List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungs = Arrays.asList(umsetzungFirstPhase, umsetzungPhaseWithPrevious);
        umsetzungsbestaetigungService.downloadExcelExport(umsetzungsbestaetigungs, kovaVABG1, vabg1);

        verify(excelDownloadService, times(1)).downloadExcelExport(any(String.class), any(Workbook.class));
    }

    @Test
    public void testDownloadExcelExportPhaseWithoutPrevious() {
        mockLocalization();

        List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungs = Arrays.asList(umsetzungPhaseWithoutPrevious);
        umsetzungsbestaetigungService.downloadExcelExport(umsetzungsbestaetigungs, kovaVABG22, vabg2);

        verify(excelDownloadService, times(1)).downloadExcelExport(any(String.class), any(Workbook.class));
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungFirstPhaseSensorCoc() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungFirstPhase, false);

        Assertions.assertEquals(anforderung.getSensorCoc().getTechnologie(), excelData.getSensorCoc());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungFirstPhaseAnforderung() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungFirstPhase, false);

        Assertions.assertEquals(anforderung.toString(), excelData.getAnforderung());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungFirstPhaseBeschreibung() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungFirstPhase, false);

        Assertions.assertEquals(anforderung.getBeschreibungAnforderungDe(), excelData.getBeschreibung());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungFirstPhaseStatus() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungFirstPhase, false);

        Assertions.assertEquals(umsetzungFirstPhase.getStatus().getStatusBezeichnung(), excelData.getKovaPhaseStatus());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungFirstPhaseKommentar() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungFirstPhase, false);

        Assertions.assertEquals(umsetzungFirstPhase.getComment(), excelData.getKovaPhaseKommentar());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithPreviousPhaseSensorCoc() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithPrevious, true);

        Assertions.assertEquals(anforderung.getSensorCoc().getTechnologie(), excelData.getSensorCoc());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithPreviousPhaseAnforderung() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithPrevious, true);

        Assertions.assertEquals(anforderung.toString(), excelData.getAnforderung());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithPreviousPhaseBeschreibung() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithPrevious, true);

        Assertions.assertEquals(anforderung.getBeschreibungAnforderungDe(), excelData.getBeschreibung());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithPreviousPhaseVorigeStatus() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithPrevious, true);

        Assertions.assertEquals(umsetzungFirstPhase.getStatus().getStatusBezeichnung(), excelData.getVorigeKovaPhaseStatus());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithPreviousPhaseVorigeKommentar() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithPrevious, true);

        Assertions.assertEquals(umsetzungFirstPhase.getComment(), excelData.getVorigeKovaPhaseKommentar());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithPreviousPhaseStatus() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithPrevious, true);

        Assertions.assertEquals(umsetzungPhaseWithPrevious.getStatus().getStatusBezeichnung(), excelData.getKovaPhaseStatus());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithPreviousPhaseKommentar() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithPrevious, true);

        Assertions.assertEquals(umsetzungPhaseWithPrevious.getComment(), excelData.getKovaPhaseKommentar());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithoutPreviousPhaseSensorCoc() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithoutPrevious, true);

        Assertions.assertEquals(anforderung.getSensorCoc().getTechnologie(), excelData.getSensorCoc());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithoutPreviousPhaseAnforderung() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithoutPrevious, true);

        Assertions.assertEquals(anforderung.toString(), excelData.getAnforderung());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithoutPreviousPhaseBeschreibung() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithoutPrevious, true);

        Assertions.assertEquals(anforderung.getBeschreibungAnforderungDe(), excelData.getBeschreibung());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithoutPreviousPhaseVorigeStatus() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");
        doReturn("keine Daten vorhanden").when(localizationService).getValue("umsetzungsbestaetigung_keineDatenVorhanden");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithoutPrevious, true);

        Assertions.assertEquals("keine Daten vorhanden", excelData.getVorigeKovaPhaseStatus());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithoutPreviousPhaseVorigeKommentar() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithoutPrevious, true);

        Assertions.assertEquals("", excelData.getVorigeKovaPhaseKommentar());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithoutPreviousPhaseStatus() {
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithoutPrevious, true);

        Assertions.assertEquals(umsetzungPhaseWithoutPrevious.getStatus().getStatusBezeichnung(), excelData.getKovaPhaseStatus());
    }

    @Test
    public void testCreateExcelDataForUmsetzungsbestaetigungWithoutPreviousPhaseKommentar() {

        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");

        UmsetzungsbestaetigungExcelDto excelData = umsetzungsbestaetigungService.createExcelDataForUmsetzungsbestaetigung(umsetzungPhaseWithoutPrevious, true);

        Assertions.assertEquals(umsetzungPhaseWithoutPrevious.getComment(), excelData.getKovaPhaseKommentar());
    }

    private void mockLocalization() {
        doReturn("Umsetzungsbestaetigung").when(localizationService).getValue("umsetzungsbestaetigung_umsetzungsbestaetigung");
        doReturn("SensorCoc").when(localizationService).getValue("umsetzungsbestaetigung_sensorCoc");
        doReturn("Anforderung").when(localizationService).getValue("umsetzungsbestaetigung_anforderung");
        doReturn("Beschreibung").when(localizationService).getValue("umsetzungsbestaetigung_beschreibung");
        doReturn("Status").when(localizationService).getValue("umsetzungsbestaetigung_status");
        doReturn("Kommentar").when(localizationService).getValue("umsetzungsbestaetigung_kommentar");
        doReturn("umgesetzt").when(localizationService).getValue("umsetzungsbestaetigung_status_umgesetzt");
    }

}
