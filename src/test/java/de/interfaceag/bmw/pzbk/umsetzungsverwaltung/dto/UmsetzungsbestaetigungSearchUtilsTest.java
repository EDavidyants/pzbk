package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.MultiValueEnumSearchFilter;
import de.interfaceag.bmw.pzbk.filter.TextSearchFilter;
import de.interfaceag.bmw.pzbk.filter.ThemenklammerFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.hamcrest.collection.IsMapContaining;
import org.hamcrest.collection.IsMapWithSize;
import org.hamcrest.text.IsEmptyString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.hamcrest.collection.IsMapWithSize.aMapWithSize;
import static org.hamcrest.collection.IsMapWithSize.anEmptyMap;
import static org.hamcrest.text.IsEmptyString.emptyString;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UmsetzungsbestaetigungSearchUtilsTest {

    @Mock
    private MultiValueEnumSearchFilter<UmsetzungsBestaetigungStatus> umsetzungsBestaetigungStatusFilter;
    @Mock
    private IdSearchFilter idSearchFilter;
    @Mock
    private Set<Long> ids;
    @Mock
    private TextSearchFilter textSearchFilter;
    @Mock
    private KovAPhaseImDerivat kovAPhaseImDerivat;
    @Mock
    private List<Long> userSensorCoc;
    @Mock
    private Derivat derivat;

    private QueryPart queryPart;

    @Mock
    private ThemenklammerFilter themenklammerFilter;

    @BeforeEach
    void setUp() {
        queryPart = new QueryPartDTO();
    }

    @ParameterizedTest
    @EnumSource(KovAPhase.class)
    void getBaseQueryForDerivatAnforderungModulVKBG(KovAPhase kovAPhase) {
        when(kovAPhaseImDerivat.getDerivat()).thenReturn(derivat);
        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(kovAPhase);
        final QueryPart query = UmsetzungsbestaetigungSearchUtils.getBaseQueryForDerivatAnforderungModul(kovAPhaseImDerivat, userSensorCoc);

        switch (kovAPhase) {
            case VABG0:
            case VABG1:
            case VABG2:
            case VA_VKBG:
            case VKBG:
                assertThat(query.getQuery(), is("SELECT DISTINCT dam FROM DerivatAnforderungModul dam " +
                        "INNER JOIN dam.zuordnungAnforderungDerivat zad INNER JOIN zad.anforderung a " +
                        "INNER JOIN zad.derivat d LEFT JOIN a.prozessbaukasten p " +
                        "LEFT JOIN a.prozessbaukastenThemenklammern pt " +
                        "WHERE a.sensorCoc.sensorCocId IN :sensorCocs AND d.id = :derivatId " +
                        "AND dam.statusVKBG = :status "));
                break;
            case VA_VBBG:
            case VBBG:
            case BBG:
                assertThat(query.getQuery(), is("SELECT DISTINCT dam FROM DerivatAnforderungModul dam INNER JOIN dam.zuordnungAnforderungDerivat zad " +
                        "INNER JOIN zad.anforderung a INNER JOIN zad.derivat d LEFT JOIN a.prozessbaukasten p LEFT JOIN a.prozessbaukastenThemenklammern pt " +
                        "WHERE a.sensorCoc.sensorCocId IN :sensorCocs AND d.id = :derivatId AND dam.statusZV = :status "));
                break;
            default:
                fail();
        }
    }

    @Test
    void getBaseQueryForDerivatAnforderungModulParameterMap() {
        when(kovAPhaseImDerivat.getDerivat()).thenReturn(derivat);
        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(KovAPhase.VABG0);
        final QueryPart query = UmsetzungsbestaetigungSearchUtils.getBaseQueryForDerivatAnforderungModul(kovAPhaseImDerivat, userSensorCoc);
        assertThat(query.getParameterMap(), aMapWithSize(3));
    }

    @Test
    void getBaseQueryForDerivatAnforderungModulParameterMapDerivatValue() {
        when(kovAPhaseImDerivat.getDerivat()).thenReturn(derivat);
        Long id = 42L;
        when(derivat.getId()).thenReturn(id);
        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(KovAPhase.VABG0);
        final QueryPart query = UmsetzungsbestaetigungSearchUtils.getBaseQueryForDerivatAnforderungModul(kovAPhaseImDerivat, userSensorCoc);
        assertThat(query.getParameterMap(), hasEntry("derivatId", id));
    }

    @Test
    void getBaseQueryForDerivatAnforderungModulParameterMapStatusValue() {
        when(kovAPhaseImDerivat.getDerivat()).thenReturn(derivat);
        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(KovAPhase.VABG0);
        final QueryPart query = UmsetzungsbestaetigungSearchUtils.getBaseQueryForDerivatAnforderungModul(kovAPhaseImDerivat, userSensorCoc);
        assertThat(query.getParameterMap(), hasEntry("status", DerivatAnforderungModulStatus.ANGENOMMEN.getStatusId()));
    }

    @Test
    void getBaseQueryForDerivatAnforderungModulParameterMapSencorCocValue() {
        when(kovAPhaseImDerivat.getDerivat()).thenReturn(derivat);
        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(KovAPhase.VABG0);
        final QueryPart query = UmsetzungsbestaetigungSearchUtils.getBaseQueryForDerivatAnforderungModul(kovAPhaseImDerivat, userSensorCoc);
        assertThat(query.getParameterMap(), hasEntry("sensorCocs", userSensorCoc));
    }

    @Test
    void getBaseQueryForExistingUmsetzungsbestaetigungWithoutProzessbaukastenFilter() {
        final QueryPart query = UmsetzungsbestaetigungSearchUtils.getBaseQueryForExistingUmsetzungsbestaetigung(42L, userSensorCoc, false);
        assertThat(query.getQuery(), is("SELECT DISTINCT um FROM Umsetzungsbestaetigung um " +
                "INNER JOIN um.anforderung a LEFT JOIN a.prozessbaukastenThemenklammern pt WHERE um.kovaImDerivat.id = :kovAPhaseImDerivatId " +
                "AND um.status != :status AND a.sensorCoc.sensorCocId IN :sensorCocs "));
    }

    @Test
    void getBaseQueryForExistingUmsetzungsbestaetigungWithoutProzessbaukastenFilterParameterMapSize() {
        final QueryPart query = UmsetzungsbestaetigungSearchUtils.getBaseQueryForExistingUmsetzungsbestaetigung(42L, userSensorCoc, false);
        assertThat(query.getParameterMap(), aMapWithSize(3));
    }

    @Test
    void getBaseQueryForExistingUmsetzungsbestaetigungWithProzessbaukastenFilter() {
        final QueryPart query = UmsetzungsbestaetigungSearchUtils.getBaseQueryForExistingUmsetzungsbestaetigung(42L, userSensorCoc, true);
        assertThat(query.getQuery(), is("SELECT DISTINCT um FROM Umsetzungsbestaetigung um INNER JOIN um.anforderung a " +
                "LEFT JOIN a.prozessbaukastenThemenklammern pt " +
                "LEFT JOIN a.prozessbaukasten p WHERE um.kovaImDerivat.id = :kovAPhaseImDerivatId " +
                "AND um.status != :status AND a.sensorCoc.sensorCocId IN :sensorCocs "));
    }

    @Test
    void getBaseQueryForExistingUmsetzungsbestaetigungWithProzessbaukastenFilterParameterMapSize() {
        final QueryPart query = UmsetzungsbestaetigungSearchUtils.getBaseQueryForExistingUmsetzungsbestaetigung(42L, userSensorCoc, true);
        assertThat(query.getParameterMap(), aMapWithSize(3));
    }

    @Test
    void getBaseQueryForExistingUmsetzungsbestaetigungParameterMapKovaPhaseValue() {
        final QueryPart query = UmsetzungsbestaetigungSearchUtils.getBaseQueryForExistingUmsetzungsbestaetigung(42L, userSensorCoc, true);
        assertThat(query.getParameterMap(), hasEntry("kovAPhaseImDerivatId", 42L));
    }

    @Test
    void getBaseQueryForExistingUmsetzungsbestaetigungParameterMapStatusValue() {
        final QueryPart query = UmsetzungsbestaetigungSearchUtils.getBaseQueryForExistingUmsetzungsbestaetigung(42L, userSensorCoc, true);
        assertThat(query.getParameterMap(), hasEntry("status", UmsetzungsBestaetigungStatus.OFFEN));
    }

    @Test
    void getBaseQueryForExistingUmsetzungsbestaetigungParameterMapSensorCocValue() {
        final QueryPart query = UmsetzungsbestaetigungSearchUtils.getBaseQueryForExistingUmsetzungsbestaetigung(42L, userSensorCoc, true);
        assertThat(query.getParameterMap(), hasEntry("sensorCocs", userSensorCoc));
    }

    @Test
    void getBeschreibungQueryInactive() {
        when(textSearchFilter.isActive()).thenReturn(false);
        UmsetzungsbestaetigungSearchUtils.getBeschreibungQuery(queryPart, textSearchFilter);
        assertThat(queryPart.getQuery(), emptyString());
    }

    @Test
    void getBeschreibungQueryInactiveParameterMap() {
        when(textSearchFilter.isActive()).thenReturn(false);
        UmsetzungsbestaetigungSearchUtils.getBeschreibungQuery(queryPart, textSearchFilter);
        assertThat(queryPart.getParameterMap(), anEmptyMap());
    }

    @Test
    void getBeschreibungQuery() {
        when(textSearchFilter.isActive()).thenReturn(true);
        UmsetzungsbestaetigungSearchUtils.getBeschreibungQuery(queryPart, textSearchFilter);
        assertThat(queryPart.getQuery(), is(" AND LOWER(a.beschreibungAnforderungDe) LIKE LOWER(:beschreibung)"));
    }

    @Test
    void getBeschreibungQueryParameterMap() {
        when(textSearchFilter.isActive()).thenReturn(true);
        UmsetzungsbestaetigungSearchUtils.getBeschreibungQuery(queryPart, textSearchFilter);
        assertThat(queryPart.getParameterMap(), aMapWithSize(1));
    }

    @Test
    void getBeschreibungQueryParameterMapValue() {
        String value = "VALUE";
        when(textSearchFilter.isActive()).thenReturn(true);
        when(textSearchFilter.getAsString()).thenReturn(value);
        UmsetzungsbestaetigungSearchUtils.getBeschreibungQuery(queryPart, textSearchFilter);
        assertThat(queryPart.getParameterMap(), hasEntry("beschreibung", "%" + value + "%"));
    }

    @Test
    void getSensorCocQueryInactive() {
        when(idSearchFilter.isActive()).thenReturn(false);
        UmsetzungsbestaetigungSearchUtils.getSensorCocQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getQuery(), emptyString());
    }

    @Test
    void getSensorCocQueryInactiveParameterMap() {
        when(idSearchFilter.isActive()).thenReturn(false);
        UmsetzungsbestaetigungSearchUtils.getSensorCocQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getParameterMap(), anEmptyMap());
    }

    @Test
    void getSensorCocQuery() {
        when(idSearchFilter.isActive()).thenReturn(true);
        when(idSearchFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getSensorCocQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getQuery(), is(" AND a.sensorCoc.sensorCocId IN :sensorCocs"));
    }

    @Test
    void getSensorCocQueryParameterMap() {
        when(idSearchFilter.isActive()).thenReturn(true);
        when(idSearchFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getSensorCocQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getParameterMap(), aMapWithSize(1));
    }

    @Test
    void getSensorCocQueryParameterMapValue() {
        when(idSearchFilter.isActive()).thenReturn(true);
        when(idSearchFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getSensorCocQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getParameterMap(), hasEntry("sensorCocs", ids));
    }

    @Test
    void getProzessbaukastenQueryInactive() {
        when(idSearchFilter.isActive()).thenReturn(false);
        UmsetzungsbestaetigungSearchUtils.getProzessbaukastenQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getQuery(), emptyString());
    }

    @Test
    void getProzessbaukastenQueryInactiveParameterMap() {
        when(idSearchFilter.isActive()).thenReturn(false);
        UmsetzungsbestaetigungSearchUtils.getProzessbaukastenQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getParameterMap(), anEmptyMap());
    }

    @Test
    void getProzessbaukastenQuery() {
        when(idSearchFilter.isActive()).thenReturn(true);
        when(idSearchFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getProzessbaukastenQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getQuery(), is(" AND p.id IN :prozessbaukastenIds"));
    }

    @Test
    void getProzessbaukastenQueryParameterMap() {
        when(idSearchFilter.isActive()).thenReturn(true);
        when(idSearchFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getProzessbaukastenQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getParameterMap(), aMapWithSize(1));
    }

    @Test
    void getProzessbaukastenQueryParameterMapValue() {
        when(idSearchFilter.isActive()).thenReturn(true);
        when(idSearchFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getProzessbaukastenQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getParameterMap(), hasEntry("prozessbaukastenIds", ids));
    }

    @Test
    void getDerivatAnforderungModulModulQueryInactive() {
        when(idSearchFilter.isActive()).thenReturn(false);
        UmsetzungsbestaetigungSearchUtils.getDerivatAnforderungModulModulQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getQuery(), emptyString());
    }

    @Test
    void getDerivatAnforderungModulModulQueryInactiveParameterMap() {
        when(idSearchFilter.isActive()).thenReturn(false);
        UmsetzungsbestaetigungSearchUtils.getDerivatAnforderungModulModulQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getParameterMap(), anEmptyMap());
    }

    @Test
    void getDerivatAnforderungModulModulQuery() {
        when(idSearchFilter.isActive()).thenReturn(true);
        when(idSearchFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getDerivatAnforderungModulModulQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getQuery(), is(" AND dam.modul.id IN :damModulIds"));
    }

    @Test
    void getDerivatAnforderungModulModulQueryParameterMap() {
        when(idSearchFilter.isActive()).thenReturn(true);
        when(idSearchFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getDerivatAnforderungModulModulQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getParameterMap(), aMapWithSize(1));
    }

    @Test
    void getDerivatAnforderungModulModulQueryParameterMapValue() {
        when(idSearchFilter.isActive()).thenReturn(true);
        when(idSearchFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getDerivatAnforderungModulModulQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getParameterMap(), hasEntry("damModulIds", ids));
    }

    @Test
    void getUmsetzungsbestaetigungModulQueryInactive() {
        when(idSearchFilter.isActive()).thenReturn(false);
        UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungModulQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getQuery(), emptyString());
    }

    @Test
    void getUmsetzungsbestaetigungModulQueryInactiveParamterMap() {
        when(idSearchFilter.isActive()).thenReturn(false);
        UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungModulQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getParameterMap(), anEmptyMap());
    }

    @Test
    void getUmsetzungsbestaetigungModulQuery() {
        when(idSearchFilter.isActive()).thenReturn(true);
        when(idSearchFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungModulQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getQuery(), is(" AND um.modul.id IN :damModulIds"));
    }

    @Test
    void getUmsetzungsbestaetigungModulQueryParameterMap() {
        when(idSearchFilter.isActive()).thenReturn(true);
        when(idSearchFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungModulQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getParameterMap(), aMapWithSize(1));
    }

    @Test
    void getUmsetzungsbestaetigungModulQueryParameterMapValue() {
        when(idSearchFilter.isActive()).thenReturn(true);
        when(idSearchFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungModulQuery(queryPart, idSearchFilter);
        assertThat(queryPart.getParameterMap(), hasEntry("damModulIds", ids));
    }

    @Test
    void getUmsetzungsbestaetigungStatusQueryForOnlyOffenStatus() {
        UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungStatusQueryForOnlyOffenStatus(queryPart);
        assertThat(queryPart.getQuery(), is(" AND um.status != :umsetzungsBestaetigungStatus "));
    }

    @Test
    void getUmsetzungsbestaetigungStatusQueryForOnlyOffenStatusParameterMapSize() {
        UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungStatusQueryForOnlyOffenStatus(queryPart);
        assertThat(queryPart.getParameterMap(), aMapWithSize(1));
    }

    @Test
    void getUmsetzungsbestaetigungStatusQueryForOnlyOffenStatusParameterMapValue() {
        UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungStatusQueryForOnlyOffenStatus(queryPart);
        assertThat(queryPart.getParameterMap(), hasEntry("umsetzungsBestaetigungStatus", UmsetzungsBestaetigungStatus.OFFEN));
    }

    @Test
    void getUmsetzungsbestaetigungStatusQueryFalse() {
        UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungStatusQuery(queryPart, umsetzungsBestaetigungStatusFilter);
        assertThat(queryPart.getQuery(), is(" AND 1 = 0"));
    }

    @Test
    void getUmsetzungsbestaetigungStatusQueryValue() {
        List<UmsetzungsBestaetigungStatus> umsetzungsbestaetigungStatusValues = Collections.singletonList(UmsetzungsBestaetigungStatus.UMGESETZT);
        when(umsetzungsBestaetigungStatusFilter.getAsEnumList()).thenReturn(umsetzungsbestaetigungStatusValues);
        UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungStatusQuery(queryPart, umsetzungsBestaetigungStatusFilter);
        assertThat(queryPart.getQuery(), is(" AND um.status IN :umsetzungsBestaetigungStatus "));
    }

    @Test
    void getUmsetzungsbestaetigungStatusQueryParamterMapSize() {
        List<UmsetzungsBestaetigungStatus> umsetzungsbestaetigungStatusValues = Collections.singletonList(UmsetzungsBestaetigungStatus.UMGESETZT);
        when(umsetzungsBestaetigungStatusFilter.getAsEnumList()).thenReturn(umsetzungsbestaetigungStatusValues);
        UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungStatusQuery(queryPart, umsetzungsBestaetigungStatusFilter);
        assertThat(queryPart.getParameterMap(), aMapWithSize(1));
    }

    @Test
    void getUmsetzungsbestaetigungStatusQueryParamterMapValue() {
        List<UmsetzungsBestaetigungStatus> umsetzungsbestaetigungStatusValues = Collections.singletonList(UmsetzungsBestaetigungStatus.UMGESETZT);
        when(umsetzungsBestaetigungStatusFilter.getAsEnumList()).thenReturn(umsetzungsbestaetigungStatusValues);
        UmsetzungsbestaetigungSearchUtils.getUmsetzungsbestaetigungStatusQuery(queryPart, umsetzungsBestaetigungStatusFilter);
        assertThat(queryPart.getParameterMap(), hasEntry("umsetzungsBestaetigungStatus", umsetzungsbestaetigungStatusValues));
    }

    @Test
    void getThemenklammerQueryInactiveQuery() {
        when(themenklammerFilter.isActive()).thenReturn(false);
        UmsetzungsbestaetigungSearchUtils.getThemenklammerQuery(queryPart, themenklammerFilter);
        assertThat(queryPart.getQuery(), IsEmptyString.emptyString());
    }

    @Test
    void getThemenklammerQueryInactiveParameterMap() {
        when(themenklammerFilter.isActive()).thenReturn(false);
        UmsetzungsbestaetigungSearchUtils.getThemenklammerQuery(queryPart, themenklammerFilter);
        assertThat(queryPart.getParameterMap(), IsMapWithSize.anEmptyMap());
    }

    @Test
    void getThemenklammerQueryActiveQuery() {
        when(themenklammerFilter.isActive()).thenReturn(true);
        UmsetzungsbestaetigungSearchUtils.getThemenklammerQuery(queryPart, themenklammerFilter);
        assertThat(queryPart.getQuery(), is(" AND pt.themenklammer.id IN :themenklammerIds"));
    }

    @Test
    void getThemenklammerQueryActiveParameterMapSize() {
        when(themenklammerFilter.isActive()).thenReturn(true);
        when(themenklammerFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getThemenklammerQuery(queryPart, themenklammerFilter);
        assertThat(queryPart.getParameterMap(), IsMapWithSize.aMapWithSize(1));
    }

    @Test
    void getThemenklammerQueryActiveParameterMapValues() {
        when(themenklammerFilter.isActive()).thenReturn(true);
        when(themenklammerFilter.getSelectedIdsAsSet()).thenReturn(ids);
        UmsetzungsbestaetigungSearchUtils.getThemenklammerQuery(queryPart, themenklammerFilter);
        assertThat(queryPart.getParameterMap(), IsMapContaining.hasEntry("themenklammerIds", ids));
    }

}