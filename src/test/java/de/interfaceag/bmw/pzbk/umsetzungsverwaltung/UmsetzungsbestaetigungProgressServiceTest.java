package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.dao.KovAPhaseImDerivatDao;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.HashSet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author fn
 */
@ExtendWith(MockitoExtension.class)
public class UmsetzungsbestaetigungProgressServiceTest {

    @Mock
    private KovAPhaseImDerivatDao kovAPhaseImDerivatDao;

    @Mock
    private Session session;

    @Mock
    private UserPermissions userPermissions;

    @Mock
    private BerechtigungService berechtigungService;

    @InjectMocks
    private UmsetzungsbestaetigungProgressService umsetzungsbestaetigungProgressService;

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetNumberOfAllUmsetzungsbestaetigungenForKovaPhaseImDerivatForRolle(Rolle rolle) {
        KovAPhaseImDerivat kovAPhaseImDerivat = TestDataFactory.generateKovAPhaseImDerivat();

        when(kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(any())).thenReturn(kovAPhaseImDerivat);
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRoles()).thenReturn(new HashSet<>(Arrays.asList(rolle)));

        long result;

        switch (rolle) {
            case ADMIN:
                umsetzungsbestaetigungProgressService.getNumberOfAllUmsetzungsbestaetigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfAllAnforderungenInUmsetzungsbestaetigung(any(), any());
                break;
            case SENSORCOCLEITER:
                when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Test User"));
                when(userPermissions.getSensorCocAsSensorCocLeiterSchreibend()).thenReturn(TestDataFactory.generateUserPermissions().getSensorCocAsSensorCocLeiterSchreibend());
                umsetzungsbestaetigungProgressService.getNumberOfAllUmsetzungsbestaetigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfAllAnforderungenInUmsetzungsbestaetigung(any(), any());
                break;
            case SCL_VERTRETER:
                when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Test User"));
                when(userPermissions.getSensorCocAsVertreterSCLSchreibend()).thenReturn(TestDataFactory.generateUserPermissions().getSensorCocAsVertreterSCLSchreibend());
                umsetzungsbestaetigungProgressService.getNumberOfAllUmsetzungsbestaetigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfAllAnforderungenInUmsetzungsbestaetigung(any(), any());
                break;
            case UMSETZUNGSBESTAETIGER:
                when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Test User"));
                when(kovAPhaseImDerivatDao.getSensorCocIdsForUmsetzungsbestaetiger(anyLong(), anyLong())).thenReturn(Arrays.asList(1L));
                umsetzungsbestaetigungProgressService.getNumberOfAllUmsetzungsbestaetigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfAllAnforderungenInUmsetzungsbestaetigung(any(), any());
                break;
            case ANFORDERER:
                when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Test User"));
                when(berechtigungService.getAuthorizedSensorCocListForMitarbeiter(any())).thenReturn(Arrays.asList(1L));
                umsetzungsbestaetigungProgressService.getNumberOfAllUmsetzungsbestaetigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfAllAnforderungenInUmsetzungsbestaetigung(any(), any());
                break;
            default:
                result = umsetzungsbestaetigungProgressService.getNumberOfAllUmsetzungsbestaetigungenForKovaPhaseImDerivat(1L);
                Assertions.assertEquals(0L, result);
                break;
        }

    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetNumberofUmgesetzteUmsetzungsbestaetigungenForKovaPhaseImDerivatForRolle(Rolle rolle) {
        KovAPhaseImDerivat kovAPhaseImDerivat = TestDataFactory.generateKovAPhaseImDerivat();

        when(kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(any())).thenReturn(kovAPhaseImDerivat);
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRoles()).thenReturn(new HashSet<>(Arrays.asList(rolle)));

        long result;

        switch (rolle) {
            case ADMIN:
                umsetzungsbestaetigungProgressService.getNumberofUmgesetzteUmsetzungsbestaetigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfAllUmgesetzteAnforderungenInUmsetzungsbestaetigung(any(), any());
                break;
            case SENSORCOCLEITER:
                when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Test User"));
                when(userPermissions.getSensorCocAsSensorCocLeiterSchreibend()).thenReturn(TestDataFactory.generateUserPermissions().getSensorCocAsSensorCocLeiterSchreibend());
                umsetzungsbestaetigungProgressService.getNumberofUmgesetzteUmsetzungsbestaetigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfAllUmgesetzteAnforderungenInUmsetzungsbestaetigung(any(), any());
                break;
            case SCL_VERTRETER:
                when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Test User"));
                when(userPermissions.getSensorCocAsVertreterSCLSchreibend()).thenReturn(TestDataFactory.generateUserPermissions().getSensorCocAsVertreterSCLSchreibend());
                umsetzungsbestaetigungProgressService.getNumberofUmgesetzteUmsetzungsbestaetigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfAllUmgesetzteAnforderungenInUmsetzungsbestaetigung(any(), any());
                break;
            case UMSETZUNGSBESTAETIGER:
                when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Test User"));
                when(kovAPhaseImDerivatDao.getSensorCocIdsForUmsetzungsbestaetiger(anyLong(), anyLong())).thenReturn(Arrays.asList(1L));
                umsetzungsbestaetigungProgressService.getNumberofUmgesetzteUmsetzungsbestaetigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfAllUmgesetzteAnforderungenInUmsetzungsbestaetigung(any(), any());
                break;
            case ANFORDERER:
                when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Test User"));
                when(berechtigungService.getAuthorizedSensorCocListForMitarbeiter(any())).thenReturn(Arrays.asList(1L));
                umsetzungsbestaetigungProgressService.getNumberofUmgesetzteUmsetzungsbestaetigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfAllUmgesetzteAnforderungenInUmsetzungsbestaetigung(any(), any());
                break;
            default:
                result = umsetzungsbestaetigungProgressService.getNumberofUmgesetzteUmsetzungsbestaetigungenForKovaPhaseImDerivat(1L);
                Assertions.assertEquals(0L, result);
                break;
        }

    }

}
