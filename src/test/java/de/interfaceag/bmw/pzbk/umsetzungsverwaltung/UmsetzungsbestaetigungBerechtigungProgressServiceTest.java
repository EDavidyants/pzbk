package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.dao.KovAPhaseImDerivatDao;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.HashSet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author fn
 */
@ExtendWith(MockitoExtension.class)
public class UmsetzungsbestaetigungBerechtigungProgressServiceTest {

    @Mock
    private KovAPhaseImDerivatDao kovAPhaseImDerivatDao;

    @Mock
    private Session session;

    @Mock
    private UserPermissions userPermissions;

    @InjectMocks
    private UmsetzungsbestaetigungBerechtigungProgressService umsetzungsbestaetigungBerechtigungProgressService;

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetNumberOfGesetzteBerechtigungenForKovaPhaseImDerivatForRolle(Rolle rolle) {
        KovAPhaseImDerivat kovAPhaseImDerivat = TestDataFactory.generateKovAPhaseImDerivat();

        when(kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(any())).thenReturn(kovAPhaseImDerivat);
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRoles()).thenReturn(new HashSet<>(Arrays.asList(rolle)));

        long result;

        switch (rolle) {
            case ADMIN:
                umsetzungsbestaetigungBerechtigungProgressService.getNumberOfGesetzteBerechtigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfSensorCocsForKovaPhaseImDerivatWithUmsetzer(any(), any());
                break;
            case SENSORCOCLEITER:
                when(userPermissions.getSensorCocAsSensorCocLeiterSchreibend()).thenReturn(TestDataFactory.generateUserPermissions().getSensorCocAsSensorCocLeiterSchreibend());
                umsetzungsbestaetigungBerechtigungProgressService.getNumberOfGesetzteBerechtigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfSensorCocsForKovaPhaseImDerivatWithUmsetzer(any(), any());
                break;
            case SCL_VERTRETER:
                when(userPermissions.getSensorCocAsVertreterSCLSchreibend()).thenReturn(TestDataFactory.generateUserPermissions().getSensorCocAsVertreterSCLSchreibend());
                umsetzungsbestaetigungBerechtigungProgressService.getNumberOfGesetzteBerechtigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfSensorCocsForKovaPhaseImDerivatWithUmsetzer(any(), any());
                break;
            default:
                result = umsetzungsbestaetigungBerechtigungProgressService.getNumberOfGesetzteBerechtigungenForKovaPhaseImDerivat(1L);
                Assertions.assertEquals(0L, result);
                break;
        }

    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testGetNumberOfAllBerechtigungenForKovaPhaseImDerivatForRolle(Rolle rolle) {
        KovAPhaseImDerivat kovAPhaseImDerivat = TestDataFactory.generateKovAPhaseImDerivat();

        when(kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(any())).thenReturn(kovAPhaseImDerivat);
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRoles()).thenReturn(new HashSet<>(Arrays.asList(rolle)));

        long result;

        switch (rolle) {
            case ADMIN:
                umsetzungsbestaetigungBerechtigungProgressService.getNumberOfAllBerechtigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfSensorCocsForKovaPhaseImDerivat(any(), any());
                break;
            case SENSORCOCLEITER:
                when(userPermissions.getSensorCocAsSensorCocLeiterSchreibend()).thenReturn(TestDataFactory.generateUserPermissions().getSensorCocAsSensorCocLeiterSchreibend());
                umsetzungsbestaetigungBerechtigungProgressService.getNumberOfAllBerechtigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfSensorCocsForKovaPhaseImDerivat(any(), any());
                break;
            case SCL_VERTRETER:
                when(userPermissions.getSensorCocAsVertreterSCLSchreibend()).thenReturn(TestDataFactory.generateUserPermissions().getSensorCocAsVertreterSCLSchreibend());
                umsetzungsbestaetigungBerechtigungProgressService.getNumberOfAllBerechtigungenForKovaPhaseImDerivat(1L);
                verify(kovAPhaseImDerivatDao, times(1)).getNumberOfSensorCocsForKovaPhaseImDerivat(any(), any());
                break;
            default:
                result = umsetzungsbestaetigungBerechtigungProgressService.getNumberOfAllBerechtigungenForKovaPhaseImDerivat(1L);
                Assertions.assertEquals(0L, result);
                break;
        }

    }
}
