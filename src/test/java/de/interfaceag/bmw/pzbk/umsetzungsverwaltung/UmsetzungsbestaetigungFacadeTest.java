package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.dao.KovAPhaseImDerivatDao;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewData;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewFilter;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewPermission;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.HashSet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author fn
 */
@ExtendWith(MockitoExtension.class)
public class UmsetzungsbestaetigungFacadeTest {

    @Mock
    private Session session;

    @Mock
    private KovAPhaseImDerivatDao kovAPhaseImDerivatDao;
    @Mock
    private HttpServletRequest request;
    @Mock
    private BerechtigungService berechtigungService;
    @Mock
    private ModulService modulService;
    @Mock
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;
    @Mock
    private DerivatService derivatService;
    @Mock
    private UmsetzungsbestaetigungBerechtigungService umsetzungsbestaetigungBerechtigungService;
    @Mock
    private UmsetzungsverwaltungTableDataSortService umsetzungsverwaltungTableDataSortService;
    @Mock
    private ProzessbaukastenReadService prozessbaukastenReadService;
    @Mock
    private UmsetzungsbestaetigungFilterService umsetzungsbestaetigungFilterService;

    @InjectMocks
    private UmsetzungsbestaetigungFacade facade;

    @Mock
    private UmsetzungsbestaetigungViewFilter filter;

    @BeforeEach
    public void setUp() {
        when(request.getParameterMap()).thenReturn(new HashMap<>());
        when(session.getUserPermissions()).thenReturn(TestDataFactory.generateUserPermissions());

    }

    @Test
    public void testInitViewDataWithId() {
        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("id", "1");

        when(umsetzungsbestaetigungFilterService.getUmsetzungsbestaetigungViewFilter(any(), any(), any())).thenReturn(filter);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Max"));
        when(kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(any())).thenReturn(TestDataFactory.generateKovAPhaseImDerivat());
        when(umsetzungsbestaetigungService.getFilterdUmsetzungsbestaetigung(any(), any(), any())).thenReturn(TestDataFactory.generateUmsetzungsBestaetigungDtoList());
        when(umsetzungsbestaetigungBerechtigungService.getUmsetzungsbestaetigerForKovaPhaseImDerivat(any())).thenReturn(new HashSet<>(TestDataFactory.createMitarbeiter(10, "Max")));

        UmsetzungsbestaetigungViewData viewData = facade.initViewData(urlParameter);
        UmsetzungsbestaetigungViewPermission viewPermission = viewData.getViewPermission();
        UmsetzungsbestaetigungViewFilter filter = viewData.getViewFilter();

        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewPermission),
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertNotNull(filter));

    }

    @Test
    public void testInitViewDataWithOutId() {
        UrlParameter urlParameter = new UrlParameter(request);

        when(kovAPhaseImDerivatDao.getKovAPhaseImDerivatById(any())).thenReturn(null);

        UmsetzungsbestaetigungViewData viewData = facade.initViewData(urlParameter);

        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNull(viewData.getViewPermission()),
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertNull(viewData.getViewFilter()));

    }

}
