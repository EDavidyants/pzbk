package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.HashSet;
import java.util.Set;

class UmsetzungsbestaetigungViewPermissionTest {

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getPage(Rolle rolle) {
        Set<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);

        UrlParameter urlParameter = new UrlParameter();

        UmsetzungsbestaetigungViewPermission viewPermission = new UmsetzungsbestaetigungViewPermission(rollen, KovAStatus.AKTIV, urlParameter);

        switch (rolle) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case UMSETZUNGSBESTAETIGER:
            case ANFORDERER:
                Assertions.assertTrue(viewPermission.getPage());
                break;
            default:
                Assertions.assertFalse(viewPermission.getPage());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getEditButtonEditTrue(Rolle rolle) {
        Set<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);

        UrlParameter urlParameter = new UrlParameter();
        urlParameter.addOrReplaceParamter("editMode", "true");

        UmsetzungsbestaetigungViewPermission viewPermission = new UmsetzungsbestaetigungViewPermission(rollen, KovAStatus.AKTIV, urlParameter);


        Assertions.assertFalse(viewPermission.getEditButton());

    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getEditButtonEditFalse(Rolle rolle) {
        Set<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);

        UrlParameter urlParameter = new UrlParameter();

        UmsetzungsbestaetigungViewPermission viewPermission = new UmsetzungsbestaetigungViewPermission(rollen, KovAStatus.AKTIV, urlParameter);

        switch (rolle) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case UMSETZUNGSBESTAETIGER:
                Assertions.assertTrue(viewPermission.getEditButton());
                break;
            default:
                Assertions.assertFalse(viewPermission.getEditButton());
                break;
        }

    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getProcessChangesButtonEditModeTrue(Rolle rolle) {
        Set<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);

        UrlParameter urlParameter = new UrlParameter();
        urlParameter.addOrReplaceParamter("editMode", "true");

        UmsetzungsbestaetigungViewPermission viewPermission = new UmsetzungsbestaetigungViewPermission(rollen, KovAStatus.AKTIV, urlParameter);


        switch (rolle) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case UMSETZUNGSBESTAETIGER:
                Assertions.assertTrue(viewPermission.getProcessChangesButton());
                break;
            default:
                Assertions.assertFalse(viewPermission.getProcessChangesButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getProcessChangesButtonEditModeFalse(Rolle rolle) {
        Set<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);

        UrlParameter urlParameter = new UrlParameter();

        UmsetzungsbestaetigungViewPermission viewPermission = new UmsetzungsbestaetigungViewPermission(rollen, KovAStatus.AKTIV, urlParameter);


        Assertions.assertFalse(viewPermission.getProcessChangesButton());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getCancelButtonEditModeTrue(Rolle rolle) {
        Set<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);

        UrlParameter urlParameter = new UrlParameter();
        urlParameter.addOrReplaceParamter("editMode", "true");

        UmsetzungsbestaetigungViewPermission viewPermission = new UmsetzungsbestaetigungViewPermission(rollen, KovAStatus.AKTIV, urlParameter);


        switch (rolle) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
            case UMSETZUNGSBESTAETIGER:
                Assertions.assertTrue(viewPermission.getCancelButton());
                break;
            default:
                Assertions.assertFalse(viewPermission.getCancelButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getCancelButtonEditModeFalse(Rolle rolle) {
        Set<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);

        UrlParameter urlParameter = new UrlParameter();

        UmsetzungsbestaetigungViewPermission viewPermission = new UmsetzungsbestaetigungViewPermission(rollen, KovAStatus.AKTIV, urlParameter);

        Assertions.assertFalse(viewPermission.getCancelButton());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getEingetrageneUmsetzerHover(Rolle rolle) {
        Set<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);

        UrlParameter urlParameter = new UrlParameter();

        UmsetzungsbestaetigungViewPermission viewPermission = new UmsetzungsbestaetigungViewPermission(rollen, KovAStatus.AKTIV, urlParameter);

        switch (rolle) {
            case ADMIN:
            case ANFORDERER:
                Assertions.assertTrue(viewPermission.getEingetrageneUmsetzerHover());
                break;
            default:
                Assertions.assertFalse(viewPermission.getEingetrageneUmsetzerHover());
                break;
        }

    }

}