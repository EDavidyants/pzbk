package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class UmsetzungsbestaetigungDtoForDerivatAnforderungModulTest {

    private static final UmsetzungsBestaetigungStatus LASTPHASESTATUS = UmsetzungsBestaetigungStatus.UMGESETZT;
    private static final String LASTPHASECOMMENT = "LASTPHASECOMMENT";
    private static final String ANFORDERUNG = "ANFORDERUNG";
    private static final String FACHID = "FACHID";
    private static final String BESCHREIBUNG = "BESCHREIBUNG DER ANFORDERUNG MIT TEST TEXT DER LAENGE 42 BESCHREIBUNG DER ANFORDERUNG MIT TEST TEXT DER LAENGE 42 BESCHREIBUNG DER ANFORDERUNG MIT TEST TEXT DER LAENGE 42";
    private static final String BESCHREIBUNG_SHORT = "BESCHREIBUNG DER ANFORDERUNG MIT TEST TEXT DER LAENGE 42 BESCHREIBUNG DER ANFORD...";
    private static final long ANFORDERUNGID = 1L;
    private static final int ANFORDERUNGVERSION = 1;
    private static final long PROZESSBAUKASTENID = 2L;
    private static final String UMSETZUNGSBESTAETIGER = "Karl Marx";

    @Mock
    DerivatAnforderungModul derivatAnforderungModul;
    @Mock
    Anforderung anforderung;
    @Mock
    SensorCoc sensorCoc;
    @Mock
    Modul modul;
    @Mock
    Prozessbaukasten prozessbaukasten;

    @BeforeEach
    public void setUp() {
        when(derivatAnforderungModul.getAnforderung()).thenReturn(anforderung);
        when(anforderung.getId()).thenReturn(ANFORDERUNGID);
        when(anforderung.getVersion()).thenReturn(ANFORDERUNGVERSION);
        when(anforderung.toString()).thenReturn(ANFORDERUNG);
        when(anforderung.getFachId()).thenReturn(FACHID);
        when(anforderung.getBeschreibungAnforderungDe()).thenReturn(BESCHREIBUNG);
        when(anforderung.getSensorCoc()).thenReturn(sensorCoc);
        when(derivatAnforderungModul.getModul()).thenReturn(modul);
        when(anforderung.getGueltigerProzessbaukasten()).thenReturn(Optional.of(prozessbaukasten));
        when(prozessbaukasten.getId()).thenReturn(PROZESSBAUKASTENID);
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulAnforderung() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigung.getAnforderung();
        MatcherAssert.assertThat(result, is(ANFORDERUNG));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulAnforderungId() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Long result = umsetzungsbestaetigung.getAnforderungId();
        MatcherAssert.assertThat(result, is(ANFORDERUNGID));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulFachId() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigung.getFachId();
        MatcherAssert.assertThat(result, is(FACHID));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulVersion() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigung.getVersion();
        MatcherAssert.assertThat(result, is(Integer.toString(ANFORDERUNGVERSION)));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulBeschreibungShort() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigung.getShortText();
        MatcherAssert.assertThat(result, is(BESCHREIBUNG_SHORT));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulBeschreibungFull() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigung.getFullText();
        MatcherAssert.assertThat(result, is(BESCHREIBUNG));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulSensorCoc() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        SensorCoc result = umsetzungsbestaetigung.getSensorCoc();
        MatcherAssert.assertThat(result, is(sensorCoc));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulModul() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Modul result = umsetzungsbestaetigung.getModul();
        MatcherAssert.assertThat(result, is(modul));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulStatus() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        UmsetzungsBestaetigungStatus result = umsetzungsbestaetigung.getStatus();
        MatcherAssert.assertThat(result, is(UmsetzungsBestaetigungStatus.OFFEN));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulComment() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigung.getComment();
        MatcherAssert.assertThat(result, is(""));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulLastPhaseStatus() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Optional<UmsetzungsBestaetigungStatus> result = umsetzungsbestaetigung.getLastPhasestatus();
        MatcherAssert.assertThat(result, is(Optional.of(LASTPHASESTATUS)));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulLastPhaseComment() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigung.getLastPhaseComment();
        MatcherAssert.assertThat(result, is(LASTPHASECOMMENT));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulProzessbaukastenId() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Optional<Long> result = umsetzungsbestaetigung.getProzessbaukastenId();
        MatcherAssert.assertThat(result, is(Optional.of(PROZESSBAUKASTENID)));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulIsUmgesetzt() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Boolean result = umsetzungsbestaetigung.isUmgesetzt();
        MatcherAssert.assertThat(result, is(Boolean.FALSE));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulId() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Long result = umsetzungsbestaetigung.getId();
        MatcherAssert.assertThat(result, is(0L));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulAnforderungFachId() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigung.getAnforderungFachId();
        MatcherAssert.assertThat(result, is(FACHID));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulLastPhaseStatusString() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigung.getLastPhasestatusString();
        MatcherAssert.assertThat(result, is(LASTPHASESTATUS.getStatusBezeichnung()));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulIsUmsetzungsbestaetigungUmgesetzt() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Boolean result = umsetzungsbestaetigung.isUmsetzungsbestaetigungUmgesetzt();
        MatcherAssert.assertThat(result, is(Boolean.FALSE));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulEingetrageneUmsetzungsbestaetiger() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigung = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(derivatAnforderungModul, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigung.getEingetrageneUmsetzungsbestaetiger();
        MatcherAssert.assertThat(result, is(UMSETZUNGSBESTAETIGER));
    }

}
