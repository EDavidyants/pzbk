package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.dao.UBzuKovaPhaseImDerivatUndSensorCoCDao;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.UBzuKovaPhaseImDerivatUndSensorCoC;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
public class UmsetzungsbestaetigungBerechtigungServiceTest {

    private UmsetzungsbestaetigungBerechtigungService umsetzungsbestaetigungBerechtigungService;

    @Before
    public void setup() {
        umsetzungsbestaetigungBerechtigungService = new UmsetzungsbestaetigungBerechtigungService();
        umsetzungsbestaetigungBerechtigungService.berechtigungService = mock(BerechtigungService.class);
        umsetzungsbestaetigungBerechtigungService.derivatAnforderungModulService = mock(DerivatAnforderungModulService.class);
        umsetzungsbestaetigungBerechtigungService.ubKovaDerivatDao = mock(UBzuKovaPhaseImDerivatUndSensorCoCDao.class);
    }

    @Test
    public void createMissingUmsetzungsbestaetigungBerechtigungForKovaPhaseTest() {
        KovAPhaseImDerivat kovAPhaseImDerivat = TestDataFactory.generateKovAPhaseImDerivat();
        Mitarbeiter mitarbeiter = TestDataFactory.generateMitarbeiter("Fach", "Teamsprecher", "IF-2");
        when(umsetzungsbestaetigungBerechtigungService.derivatAnforderungModulService.getSensorCocForDerivat(any())).thenReturn(TestDataFactory.generateSensorCocs());
        when(umsetzungsbestaetigungBerechtigungService.berechtigungService.getSensorCoCLeiterOfSensorCoc(any())).thenReturn(Optional.of(mitarbeiter));
        List<SensorCoc> sensorCocs = Arrays.asList(TestDataFactory.generateSensorCoc());
        when(umsetzungsbestaetigungBerechtigungService.derivatAnforderungModulService.getSensorCocForVereinbarungenWithStatusAngenommenAndKovaPhaseAndSensorCocIdNotInList(any(KovAPhaseImDerivat.class), any())).thenReturn(sensorCocs);
        when(umsetzungsbestaetigungBerechtigungService.ubKovaDerivatDao.getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList(any(KovAPhaseImDerivat.class))).thenReturn(new ArrayList<>());

        List<UBzuKovaPhaseImDerivatUndSensorCoC> result = umsetzungsbestaetigungBerechtigungService.createMissingUmsetzungsbestaetigungBerechtigungForKovaPhase(kovAPhaseImDerivat);

        assertAll("Groesse",
                () -> {
                    assertEquals(1, result.size());
                    UBzuKovaPhaseImDerivatUndSensorCoC u0 = result.get(0);

                    assertAll("UBzuKovaPhaseImDerivatUndSensorCoC",
                            () -> assertEquals("VABG1", u0.getKovaImDerivat().getKovAPhase().getBezeichnung()),
                            () -> assertEquals("E46", u0.getKovaImDerivat().getDerivat().getName()),
                            () -> assertEquals(TestDataFactory.generateSensorCoc().getOrdnungsstruktur(), u0.getSensorCoc().getOrdnungsstruktur())
                    );
                }
        );
    }

    @Test
    public void createUBzuKovaPhaseImDerivatUndSensorCoC() {
        KovAPhaseImDerivat kovAPhaseImDerivat = TestDataFactory.generateKovAPhaseImDerivat();
        when(umsetzungsbestaetigungBerechtigungService.derivatAnforderungModulService.getSensorCocForDerivat(any())).thenReturn(TestDataFactory.generateSensorCocs());

        List<UBzuKovaPhaseImDerivatUndSensorCoC> result = umsetzungsbestaetigungBerechtigungService.createUBzuKovaPhaseImDerivatUndSensorCoC(kovAPhaseImDerivat, new ArrayList<>());

        assertAll("Groesse",
                () -> {
                    assertEquals(1, result.size());
                    UBzuKovaPhaseImDerivatUndSensorCoC u0 = result.get(0);

                    assertAll("UBzuKovaPhaseImDerivatUndSensorCoC",
                            () -> assertEquals("VABG1", u0.getKovaImDerivat().getKovAPhase().getBezeichnung()),
                            () -> assertEquals("E46", u0.getKovaImDerivat().getDerivat().getName()),
                            () -> assertEquals(TestDataFactory.generateSensorCoc().getOrdnungsstruktur(), u0.getSensorCoc().getOrdnungsstruktur()),
                            () -> assertEquals(new ArrayList<>(), u0.getUmsetzungsbestaetiger())
                    );
                }
        );
    }

}
