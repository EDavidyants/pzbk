package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.filter.pages.KovAPhasenViewFilter;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.KovAPhasenViewPermission;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.KovaPhasenViewData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.HashMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author fn
 */
@ExtendWith(MockitoExtension.class)
public class KovAPhasenViewFacadeTest {

    @Mock
    private Session session;
    @Mock
    private KovAPhaseImDerivatSearchService kovAPhaseImDerivatSearchService;
    @Mock
    private HttpServletRequest request;
    @Mock
    private BerechtigungService berechtigungService;
    @InjectMocks
    private KovAPhasenViewFacade kovAPhasenViewFacade;

    @BeforeEach
    public void setUp() {
        when(request.getParameterMap()).thenReturn(new HashMap<>());
        when(session.getUserPermissions()).thenReturn(TestDataFactory.generateUserPermissions());
        when(kovAPhaseImDerivatSearchService.getKovAPhasenImDerivat(any(), any())).thenReturn(TestDataFactory.genereateKovAPhasenImDerivat());
        when(berechtigungService.getDerivateForCurrentUser()).thenReturn(Collections.singletonList(TestDataFactory.generateDerivat()));
    }

    @Test
    public void testInitViewData() {
        UrlParameter urlParameter = new UrlParameter(request);
        KovaPhasenViewData viewData = kovAPhasenViewFacade.getViewData(urlParameter);
        KovAPhasenViewPermission viewPermission = viewData.getViewPermission();
        KovAPhasenViewFilter filter = viewData.getFilter();
        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewPermission),
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertNotNull(filter),
                () -> Assertions.assertEquals(10, viewData.getKovAPhasenImDerivat().size()));

    }
}
