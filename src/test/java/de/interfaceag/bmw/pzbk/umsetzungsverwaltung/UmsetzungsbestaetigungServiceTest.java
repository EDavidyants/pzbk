package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.dao.DerivatAnforderungModulDao;
import de.interfaceag.bmw.pzbk.dao.UmsetzungsbestaetigungDao;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Umsetzungsbestaetigung;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.ProcessResultDto;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungSearchService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewFilter;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author fn
 */
@ExtendWith(MockitoExtension.class)
public class UmsetzungsbestaetigungServiceTest {

    @Mock
    private LocalizationService localizationService;

    @Mock
    private AnforderungService anforderungService;

    @Mock
    private UmsetzungsbestaetigungDao umsetzungsbestaetigungDao;

    @Mock
    private DerivatAnforderungModulDao derivatAnforderungModulDao;

    @Mock
    private DerivatAnforderungModulService derivatAnforderungModulService;

    @Mock
    private HttpServletRequest request;
    @Mock
    private Session session;
    @Mock
    private SensorCocService sensorCocService;
    @Mock
    private UmsetzungsbestaetigungBerechtigungService umsetzungsbestaetigungBerechtigungService;
    @Mock
    private BerechtigungService berechtigungService;
    @Mock
    private Umsetzungsbestaetigung umsetzungsbestaetigung;
    @Mock
    private UmsetzungsbestaetigungSearchService umsetzungsbestaetigungSearchService;

    @InjectMocks
    private UmsetzungsbestaetigungService umsetzungsbestaetigungService;

    private KovAPhaseImDerivat kovAPhaseImDerivat;

    private Mitarbeiter currentUser;

    @BeforeEach
    public void setUp() {

        kovAPhaseImDerivat = TestDataFactory.generateKovAPhaseImDerivat();

        currentUser = TestDataFactory.createMitarbeiter("Test");

    }

    @Test
    public void processUmsetzungsbestatigungChangeDTONullTest() {

        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = null;

        ProcessResultDto processResultDto = umsetzungsbestaetigungService.processUmsetzungsbestaetigungChange(umsetzungsbestaetigungDto, currentUser, kovAPhaseImDerivat);

        Assertions.assertAll("processResultNotNull",
                () -> Assertions.assertNotNull(processResultDto),
                () -> Assertions.assertFalse(processResultDto.isSuccessful()),
                () -> Assertions.assertEquals("umsetzungsbestaetigung is null", processResultDto.getMessage()));
    }

    @Test
    public void processUmsetzungsbestatigungChangeKovaImDerivatNullTest() {

        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = TestDataFactory.generateUmsetzungsbestaetigungDto();

        currentUser = null;

        ProcessResultDto processResultDto = umsetzungsbestaetigungService.processUmsetzungsbestaetigungChange(umsetzungsbestaetigungDto, currentUser, kovAPhaseImDerivat);

        Assertions.assertAll("processResultNotNull",
                () -> Assertions.assertNotNull(processResultDto),
                () -> Assertions.assertFalse(processResultDto.isSuccessful()),
                () -> Assertions.assertEquals("currentUser is null", processResultDto.getMessage()));
    }

    @Test
    public void processUmsetzungsbestatigungChangeCurrentUserNullTest() {

        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = TestDataFactory.generateUmsetzungsbestaetigungDto();

        kovAPhaseImDerivat = null;

        ProcessResultDto processResultDto = umsetzungsbestaetigungService.processUmsetzungsbestaetigungChange(umsetzungsbestaetigungDto, currentUser, kovAPhaseImDerivat);

        Assertions.assertAll("processResultNotNull",
                () -> Assertions.assertNotNull(processResultDto),
                () -> Assertions.assertFalse(processResultDto.isSuccessful()),
                () -> Assertions.assertEquals("kovaImDerivat is null", processResultDto.getMessage()));
    }

    @Test
    public void processUmsetzungsbestatigungChangeForEmtyCommentNichtUmgesetztTest() {

        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = TestDataFactory.generateUmsetzungsbestaetigungDto();

        umsetzungsbestaetigungDto.setStatus(UmsetzungsBestaetigungStatus.NICHT_UMGESETZT);

        umsetzungsbestaetigungDto.setComment("");

        when(localizationService.getCurrentLocale()).thenReturn(Locale.GERMAN);

        ProcessResultDto processResultDto = umsetzungsbestaetigungService.processUmsetzungsbestaetigungChange(umsetzungsbestaetigungDto, currentUser, kovAPhaseImDerivat);

        Assertions.assertAll("processResultNotNull",
                () -> Assertions.assertNotNull(processResultDto),
                () -> Assertions.assertFalse(processResultDto.isSuccessful()),
                () -> Assertions.assertEquals("Es ist kein gültiger Kommentar vorhanden!", processResultDto.getMessage()));

    }

    @Test
    public void processUmsetzungsbestatigungChangeForEmtyCommentBewertungNichtMoeglichTest() {

        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = TestDataFactory.generateUmsetzungsbestaetigungDto();

        umsetzungsbestaetigungDto.setStatus(UmsetzungsBestaetigungStatus.BEWERTUNG_NICHT_MOEGLICH);

        umsetzungsbestaetigungDto.setComment("");

        when(localizationService.getCurrentLocale()).thenReturn(Locale.GERMAN);

        ProcessResultDto processResultDto = umsetzungsbestaetigungService.processUmsetzungsbestaetigungChange(umsetzungsbestaetigungDto, currentUser, kovAPhaseImDerivat);

        Assertions.assertAll("processResultNotNull",
                () -> Assertions.assertNotNull(processResultDto),
                () -> Assertions.assertFalse(processResultDto.isSuccessful()),
                () -> Assertions.assertEquals("Es ist kein gültiger Kommentar vorhanden!", processResultDto.getMessage()));
    }

    @Test
    public void processUmsetzungsbestatigungChangeForEmtyCommentNichtRelevantTest() {

        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = TestDataFactory.generateUmsetzungsbestaetigungDto();

        umsetzungsbestaetigungDto.setStatus(UmsetzungsBestaetigungStatus.NICHT_RELEVANT);

        umsetzungsbestaetigungDto.setComment("");

        when(localizationService.getCurrentLocale()).thenReturn(Locale.GERMAN);

        ProcessResultDto processResultDto = umsetzungsbestaetigungService.processUmsetzungsbestaetigungChange(umsetzungsbestaetigungDto, currentUser, kovAPhaseImDerivat);

        Assertions.assertAll("processResultNotNull",
                () -> Assertions.assertNotNull(processResultDto),
                () -> Assertions.assertFalse(processResultDto.isSuccessful()),
                () -> Assertions.assertEquals("Es ist kein gültiger Kommentar vorhanden!", processResultDto.getMessage()));
    }

    @ParameterizedTest
    @EnumSource(UmsetzungsBestaetigungStatus.class)
    public void processUmsetzungsbestatigungChangeForSetCommentTest(UmsetzungsBestaetigungStatus umsetzungsBestaetigungStatus) {

        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = TestDataFactory.generateUmsetzungsbestaetigungDto();

        umsetzungsbestaetigungDto.setStatus(umsetzungsBestaetigungStatus);

        umsetzungsbestaetigungDto.setComment("Status Change Kommentar");

        umsetzungsbestaetigungDto.setDialogComment("Status Change Kommentar");

        when(localizationService.getCurrentLocale()).thenReturn(Locale.GERMAN);

        when(anforderungService.getAnforderungById(any())).thenReturn(TestDataFactory.generateAnforderung());

        when(umsetzungsbestaetigungDao.getUmsetzungsbestaetigungByAnforderungModulDerivatPhase(any(), any(), any(), any())).thenReturn(Optional.of(TestDataFactory.generateUmsetyungsbestaetigung()));

        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Max"));

        ProcessResultDto processResultDto = umsetzungsbestaetigungService.processUmsetzungsbestaetigungChange(umsetzungsbestaetigungDto, currentUser, kovAPhaseImDerivat);

        switch (umsetzungsBestaetigungStatus) {
            case OFFEN:
            case NICHT_UMGESETZT:
            case BEWERTUNG_NICHT_MOEGLICH:
            case NICHT_RELEVANT:
            case KEINE_WEITERVERFOLGUNG:
            case UMGESETZT:
            case NICHT_RELEVANT_KEINE_WEITERVERFOLGUNG:
            case NICHT_UMGESETZT_KEINE_WEITERVERFOLGUNG:
            case NICHT_UMGESETZT_WEITERVERFOLGUNG:
                Assertions.assertAll("processResultNotNull",
                        () -> Assertions.assertNotNull(processResultDto),
                        () -> Assertions.assertTrue(processResultDto.isSuccessful()),
                        () -> Assertions.assertEquals("", processResultDto.getMessage()));
                break;
            default:
                break;
        }


    }

    @Test
    public void getFilteredUmsetzungsbestaetigungForOffenFilterTest() {

        when(request.getParameterMap()).thenReturn(new HashMap<>());
        UrlParameter urlParameter = new UrlParameter(request);
        List<SensorCoc> allSensorCoc = TestDataFactory.generateSensorCocs();
        List<Modul> allModule = TestDataFactory.generateModule();
        List<Long> longIdList = TestDataFactory.generateLongList();

        UmsetzungsbestaetigungViewFilter filter = new UmsetzungsbestaetigungViewFilter(urlParameter, allSensorCoc, allModule, KovAPhase.VABG0, KovAPhase.VABG0, Collections.emptyList(), Collections.emptyList());

        SearchFilterObject filterObject = getUmsetzungsbestaetigungStatusFilterObjectForName(filter, UmsetzungsBestaetigungStatus.OFFEN.getStatusBezeichnung());
        Set<SearchFilterObject> selected = new HashSet<>();
        selected.add(filterObject);

        filter.getUmsetzungsbestaetigungStatusFilter().setSelectedValues(selected);

        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Max"));
        when(session.getUserPermissions()).thenReturn(TestDataFactory.generateUserPermissions());
        when(sensorCocService.getAllSensorCocIds()).thenReturn(longIdList);
        when(umsetzungsbestaetigungDao.getUmsetzungsbestaetigungByDerivatAndPhaseForUserSensorCoc(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateUmsetzungsbestaetigung()));
        when(umsetzungsbestaetigungSearchService.getVereinbarungWithStatusAngenommen(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateDerivatAnforderungModul()));

        List<UmsetzungsbestaetigungDto> result = umsetzungsbestaetigungService.getFilterdUmsetzungsbestaetigung(filter, kovAPhaseImDerivat, KovAPhase.VABG0);

        Assertions.assertEquals(1, result.size());

    }

    @Test
    public void getFilteredUmsetzungsbestaetigungForOffenAndUmgesetztFilterTest() {

        when(request.getParameterMap()).thenReturn(new HashMap<>());
        UrlParameter urlParameter = new UrlParameter(request);
        List<SensorCoc> allSensorCoc = TestDataFactory.generateSensorCocs();
        List<Modul> allModule = TestDataFactory.generateModule();
        List<Long> longIdList = TestDataFactory.generateLongList();

        UmsetzungsbestaetigungViewFilter filter = new UmsetzungsbestaetigungViewFilter(urlParameter, allSensorCoc, allModule, KovAPhase.VABG0, KovAPhase.VABG0, Collections.emptyList(), Collections.emptyList());

        Set<SearchFilterObject> selected = generateFilterObjectsForOffenAndUmgesetztFilter(filter);

        filter.getUmsetzungsbestaetigungStatusFilter().setSelectedValues(selected);

        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Max"));
        when(session.getUserPermissions()).thenReturn(TestDataFactory.generateUserPermissions());
        when(sensorCocService.getAllSensorCocIds()).thenReturn(longIdList);
        when(umsetzungsbestaetigungDao.getUmsetzungsbestaetigungByDerivatAndPhaseForUserSensorCoc(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateUmsetzungsbestaetigung()));
        when(umsetzungsbestaetigungSearchService.getVereinbarungWithStatusAngenommen(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateDerivatAnforderungModul()));
        when(umsetzungsbestaetigungSearchService.getExistingUmsetzungsbestaetigung(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateUmsetzungsbestaetigung()));

        List<UmsetzungsbestaetigungDto> result = umsetzungsbestaetigungService.getFilterdUmsetzungsbestaetigung(filter, kovAPhaseImDerivat, KovAPhase.VABG0);

        Assertions.assertEquals(2, result.size());

    }

    @Test
    public void getFilteredUmsetzungsbestaetigungForEingetrageneUmsetzungsbestaetigerNotnull() {

        when(request.getParameterMap()).thenReturn(new HashMap<>());
        UrlParameter urlParameter = new UrlParameter(request);
        List<SensorCoc> allSensorCoc = TestDataFactory.generateSensorCocs();
        List<Modul> allModule = TestDataFactory.generateModule();
        List<Long> longIdList = TestDataFactory.generateLongList();

        UmsetzungsbestaetigungViewFilter filter = new UmsetzungsbestaetigungViewFilter(urlParameter, allSensorCoc, allModule, KovAPhase.VABG0, KovAPhase.VABG0, Collections.emptyList(), Collections.emptyList());

        SearchFilterObject filterObject = getUmsetzungsbestaetigungStatusFilterObjectForName(filter, UmsetzungsBestaetigungStatus.OFFEN.getStatusBezeichnung());
        Set<SearchFilterObject> selected = new HashSet<>();
        selected.add(filterObject);

        filter.getUmsetzungsbestaetigungStatusFilter().setSelectedValues(selected);

        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Max"));
        when(session.getUserPermissions()).thenReturn(TestDataFactory.generateUserPermissions());
        when(sensorCocService.getAllSensorCocIds()).thenReturn(longIdList);
        when(umsetzungsbestaetigungDao.getUmsetzungsbestaetigungByDerivatAndPhaseForUserSensorCoc(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateUmsetzungsbestaetigung()));
        when(umsetzungsbestaetigungSearchService.getVereinbarungWithStatusAngenommen(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateDerivatAnforderungModul()));

        when(umsetzungsbestaetigungBerechtigungService.getUBzuKovaPhaseImDerivatUndSensorCoCByDerivatSensorCocAndPhase(any(), any(), any())).thenReturn(Optional.of(TestDataFactory.getUBzuKovaPhaseImDerivatUndSensorCoC()));

        List<UmsetzungsbestaetigungDto> result = umsetzungsbestaetigungService.getFilterdUmsetzungsbestaetigung(filter, kovAPhaseImDerivat, KovAPhase.VABG0);


        Assertions.assertEquals("Karl Marx", result.get(0).getEingetrageneUmsetzungsbestaetiger());

    }

    @Test
    public void getFilteredUmsetzungsbestaetigungForEingetrageneUmsetzungsbestaetigerNotnullForUmgesetztFilter() {

        when(request.getParameterMap()).thenReturn(new HashMap<>());
        UrlParameter urlParameter = new UrlParameter(request);
        List<SensorCoc> allSensorCoc = TestDataFactory.generateSensorCocs();
        List<Modul> allModule = TestDataFactory.generateModule();
        List<Long> longIdList = TestDataFactory.generateLongList();

        UmsetzungsbestaetigungViewFilter filter = new UmsetzungsbestaetigungViewFilter(urlParameter, allSensorCoc, allModule, KovAPhase.VABG0, KovAPhase.VABG0, Collections.emptyList(), Collections.emptyList());

        SearchFilterObject filterObject = getUmsetzungsbestaetigungStatusFilterObjectForName(filter, UmsetzungsBestaetigungStatus.UMGESETZT.getStatusBezeichnung());
        Set<SearchFilterObject> selected = new HashSet<>();
        selected.add(filterObject);

        filter.getUmsetzungsbestaetigungStatusFilter().setSelectedValues(selected);

        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Max"));
        when(session.getUserPermissions()).thenReturn(TestDataFactory.generateUserPermissions());
        when(sensorCocService.getAllSensorCocIds()).thenReturn(longIdList);
        when(umsetzungsbestaetigungDao.getUmsetzungsbestaetigungByDerivatAndPhaseForUserSensorCoc(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateUmsetzungsbestaetigung()));

        when(umsetzungsbestaetigungSearchService.getVereinbarungWithStatusAngenommen(any(), any(), any())).thenReturn(Collections.emptyList());
        when(umsetzungsbestaetigungSearchService.getExistingUmsetzungsbestaetigung(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateUmsetzungsbestaetigung()));
        when(umsetzungsbestaetigungBerechtigungService.getBerechtigungenListForKovAPhaseImDerivatAndSensorCoc(any(), any())).thenReturn(Optional.of(TestDataFactory.getUBzuKovaPhaseImDerivatUndSensorCoC()));

        List<UmsetzungsbestaetigungDto> result = umsetzungsbestaetigungService.getFilterdUmsetzungsbestaetigung(filter, kovAPhaseImDerivat, KovAPhase.VABG0);


        Assertions.assertEquals("Karl Marx", result.get(0).getEingetrageneUmsetzungsbestaetiger());

    }

    @Test
    public void getFilteredUmsetzungsbestaetigungForEingetrageneUmsetzungsbestaetigerNull() {

        when(request.getParameterMap()).thenReturn(new HashMap<>());
        UrlParameter urlParameter = new UrlParameter(request);
        List<SensorCoc> allSensorCoc = TestDataFactory.generateSensorCocs();
        List<Modul> allModule = TestDataFactory.generateModule();
        List<Long> longIdList = TestDataFactory.generateLongList();

        UmsetzungsbestaetigungViewFilter filter = new UmsetzungsbestaetigungViewFilter(urlParameter, allSensorCoc, allModule, KovAPhase.VABG0, KovAPhase.VABG0, Collections.emptyList(), Collections.emptyList());

        SearchFilterObject filterObject = getUmsetzungsbestaetigungStatusFilterObjectForName(filter, UmsetzungsBestaetigungStatus.OFFEN.getStatusBezeichnung());
        Set<SearchFilterObject> selected = new HashSet<>();
        selected.add(filterObject);

        filter.getUmsetzungsbestaetigungStatusFilter().setSelectedValues(selected);

        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Max"));
        when(session.getUserPermissions()).thenReturn(TestDataFactory.generateUserPermissions());
        when(sensorCocService.getAllSensorCocIds()).thenReturn(longIdList);
        when(umsetzungsbestaetigungDao.getUmsetzungsbestaetigungByDerivatAndPhaseForUserSensorCoc(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateUmsetzungsbestaetigung()));
        when(umsetzungsbestaetigungSearchService.getVereinbarungWithStatusAngenommen(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateDerivatAnforderungModul()));
        when(umsetzungsbestaetigungBerechtigungService.getUBzuKovaPhaseImDerivatUndSensorCoCByDerivatSensorCocAndPhase(any(), any(), any())).thenReturn(Optional.empty());
        when(localizationService.getValue("umsetzungsbestaetigung_sensorcoc_hover_bestaetiger_nichtgesetzt")).thenReturn("Kein Eintrag");

        List<UmsetzungsbestaetigungDto> result = umsetzungsbestaetigungService.getFilterdUmsetzungsbestaetigung(filter, kovAPhaseImDerivat, KovAPhase.VABG0);


        Assertions.assertEquals("Kein Eintrag", result.get(0).getEingetrageneUmsetzungsbestaetiger());

    }

    @Test
    public void getFilteredUmsetzungsbestaetigungForUmgesetztFilterTest() {

        when(request.getParameterMap()).thenReturn(new HashMap<>());
        UrlParameter urlParameter = new UrlParameter(request);
        List<SensorCoc> allSensorCoc = TestDataFactory.generateSensorCocs();
        List<Modul> allModule = TestDataFactory.generateModule();
        List<Long> longIdList = TestDataFactory.generateLongList();

        UmsetzungsbestaetigungViewFilter filter = new UmsetzungsbestaetigungViewFilter(urlParameter, allSensorCoc, allModule, KovAPhase.VABG0, KovAPhase.VABG0, Collections.emptyList(), Collections.emptyList());

        SearchFilterObject filterObject = getUmsetzungsbestaetigungStatusFilterObjectForName(filter, UmsetzungsBestaetigungStatus.UMGESETZT.getStatusBezeichnung());
        Set<SearchFilterObject> selected = new HashSet<>();
        selected.add(filterObject);

        filter.getUmsetzungsbestaetigungStatusFilter().setSelectedValues(selected);

        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Max"));
        when(session.getUserPermissions()).thenReturn(TestDataFactory.generateUserPermissions());
        when(sensorCocService.getAllSensorCocIds()).thenReturn(longIdList);
        when(umsetzungsbestaetigungDao.getUmsetzungsbestaetigungByDerivatAndPhaseForUserSensorCoc(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateUmsetzungsbestaetigung()));
        when(umsetzungsbestaetigungSearchService.getVereinbarungWithStatusAngenommen(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateDerivatAnforderungModul()));
        when(umsetzungsbestaetigungSearchService.getExistingUmsetzungsbestaetigung(any(), any(), any())).thenReturn(Arrays.asList(TestDataFactory.generateUmsetzungsbestaetigung()));

        List<UmsetzungsbestaetigungDto> result = umsetzungsbestaetigungService.getFilterdUmsetzungsbestaetigung(filter, kovAPhaseImDerivat, KovAPhase.VABG0);

        Assertions.assertEquals(2, result.size());

    }

    private Set<SearchFilterObject> generateFilterObjectsForOffenAndUmgesetztFilter(UmsetzungsbestaetigungViewFilter filter) {
        SearchFilterObject filterObject1 = getUmsetzungsbestaetigungStatusFilterObjectForName(filter, UmsetzungsBestaetigungStatus.OFFEN.getStatusBezeichnung());
        SearchFilterObject filterObject2 = getUmsetzungsbestaetigungStatusFilterObjectForName(filter, UmsetzungsBestaetigungStatus.UMGESETZT.getStatusBezeichnung());
        Set<SearchFilterObject> selected = new HashSet<>();
        selected.add(filterObject1);
        selected.add(filterObject2);
        return selected;
    }

    private SearchFilterObject getUmsetzungsbestaetigungStatusFilterObjectForName(UmsetzungsbestaetigungViewFilter filter, String name) {
        List<SearchFilterObject> allObjects = filter.getUmsetzungsbestaetigungStatusFilter().getAll();
        for (SearchFilterObject searchFilterObject : allObjects) {
            if (name.equals(searchFilterObject.getName())) {
                return searchFilterObject;
            }
        }
        return null;
    }
}
