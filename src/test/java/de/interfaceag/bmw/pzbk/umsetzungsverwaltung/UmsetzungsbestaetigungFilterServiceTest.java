package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.filter.IdSearchFilter;
import de.interfaceag.bmw.pzbk.filter.SearchFilterObject;
import de.interfaceag.bmw.pzbk.filter.dto.ProzessbaukastenFilterDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungViewFilter;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.IsIterableContaining.hasItem;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UmsetzungsbestaetigungFilterServiceTest {

    @Mock
    private Session session;
    @Mock
    private BerechtigungService berechtigungService;
    @Mock
    private ModulService modulService;
    @Mock
    private ProzessbaukastenReadService prozessbaukastenReadService;
    @Mock
    private ThemenklammerService themenklammerService;
    @InjectMocks
    private UmsetzungsbestaetigungFilterService umsetzungsbestaetigungFilterService;

    @Mock
    private UrlParameter urlParameter;
    @Mock
    private KovAPhaseImDerivat kovAPhaseImDerivat;
    private KovAPhase previousPhase;

    private ProzessbaukastenFilterDto prozessbaukastenFilterDto;
    private List<ProzessbaukastenFilterDto> prozessbaukastenFilters;

    @Mock
    private SensorCoc sensorcoc;
    @Mock
    private SensorCoc anotherSensorCoc;
    private List<SensorCoc> sensorcocs;

    @Mock
    private Modul modul;
    @Mock
    private Modul anotherModul;
    private List<Modul> module;

    @Mock
    private UmsetzungsbestaetigungDto umsetzungsbestaetigungDto;
    private List<UmsetzungsbestaetigungDto> umsetzungsbestaetigungen;

    @Test
    void getUmsetzungsbestaetigungViewFilterProzessbaukastenFilterResultSize() {
        prozessbaukastenFilterDto = new ProzessbaukastenFilterDto(42L, "Fach Id", 1, "Bezeichnung");
        prozessbaukastenFilters = Collections.singletonList(prozessbaukastenFilterDto);
        sensorcocs = Collections.singletonList(sensorcoc);
        module = Collections.singletonList(modul);
        previousPhase = KovAPhase.VABG0;

        when(prozessbaukastenReadService.getAllFilterDto()).thenReturn(prozessbaukastenFilters);
        when(berechtigungService.getSensorCocForMitarbeiter(any(), any())).thenReturn(sensorcocs);
        when(modulService.getAllModule()).thenReturn(module);

        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(previousPhase);

        final UmsetzungsbestaetigungViewFilter umsetzungsbestaetigungViewFilter = umsetzungsbestaetigungFilterService.getUmsetzungsbestaetigungViewFilter(urlParameter, kovAPhaseImDerivat, previousPhase);
        final IdSearchFilter prozessbaukastenFilter = umsetzungsbestaetigungViewFilter.getProzessbaukastenFilter();
        final List<SearchFilterObject> all = prozessbaukastenFilter.getAll();
        assertThat(all, hasSize(1));
    }

    @Test
    void getUmsetzungsbestaetigungViewFilterProzessbaukastenFilterItemValue() {
        prozessbaukastenFilterDto = new ProzessbaukastenFilterDto(42L, "Fach Id", 1, "Bezeichnung");
        prozessbaukastenFilters = Collections.singletonList(prozessbaukastenFilterDto);
        sensorcocs = Collections.singletonList(sensorcoc);
        module = Collections.singletonList(modul);
        previousPhase = KovAPhase.VABG0;

        when(prozessbaukastenReadService.getAllFilterDto()).thenReturn(prozessbaukastenFilters);
        when(berechtigungService.getSensorCocForMitarbeiter(any(), any())).thenReturn(sensorcocs);
        when(modulService.getAllModule()).thenReturn(module);

        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(previousPhase);

        final UmsetzungsbestaetigungViewFilter umsetzungsbestaetigungViewFilter = umsetzungsbestaetigungFilterService.getUmsetzungsbestaetigungViewFilter(urlParameter, kovAPhaseImDerivat, previousPhase);
        final IdSearchFilter prozessbaukastenFilter = umsetzungsbestaetigungViewFilter.getProzessbaukastenFilter();
        final List<SearchFilterObject> all = prozessbaukastenFilter.getAll();
        assertThat(all, hasItem(new SearchFilterObject(42L, prozessbaukastenFilterDto.toString())));
    }

    @Test
    void getUmsetzungsbestaetigungViewFilterModulFilterResultSize() {
        prozessbaukastenFilterDto = new ProzessbaukastenFilterDto(42L, "Fach Id", 1, "Bezeichnung");
        prozessbaukastenFilters = Collections.singletonList(prozessbaukastenFilterDto);
        sensorcocs = Collections.singletonList(sensorcoc);
        module = Collections.singletonList(modul);
        previousPhase = KovAPhase.VABG0;

        when(prozessbaukastenReadService.getAllFilterDto()).thenReturn(prozessbaukastenFilters);
        when(berechtigungService.getSensorCocForMitarbeiter(any(), any())).thenReturn(sensorcocs);
        when(modulService.getAllModule()).thenReturn(module);
        when(modul.getId()).thenReturn(1L);
        when(modul.getName()).thenReturn("Name");

        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(previousPhase);

        final UmsetzungsbestaetigungViewFilter umsetzungsbestaetigungViewFilter = umsetzungsbestaetigungFilterService.getUmsetzungsbestaetigungViewFilter(urlParameter, kovAPhaseImDerivat, previousPhase);
        final IdSearchFilter modulFilter = umsetzungsbestaetigungViewFilter.getModulFilter();
        final List<SearchFilterObject> all = modulFilter.getAll();
        assertThat(all, hasSize(1));
    }

    @Test
    void getUmsetzungsbestaetigungViewFilterModulFilterItemValue() {
        prozessbaukastenFilterDto = new ProzessbaukastenFilterDto(42L, "Fach Id", 1, "Bezeichnung");
        prozessbaukastenFilters = Collections.singletonList(prozessbaukastenFilterDto);
        sensorcocs = Collections.singletonList(sensorcoc);
        module = Collections.singletonList(modul);
        previousPhase = KovAPhase.VABG0;

        when(prozessbaukastenReadService.getAllFilterDto()).thenReturn(prozessbaukastenFilters);
        when(berechtigungService.getSensorCocForMitarbeiter(any(), any())).thenReturn(sensorcocs);
        when(modulService.getAllModule()).thenReturn(module);
        when(modul.getId()).thenReturn(1L);
        when(modul.getName()).thenReturn("Name");

        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(previousPhase);

        final UmsetzungsbestaetigungViewFilter umsetzungsbestaetigungViewFilter = umsetzungsbestaetigungFilterService.getUmsetzungsbestaetigungViewFilter(urlParameter, kovAPhaseImDerivat, previousPhase);
        final IdSearchFilter modulFilter = umsetzungsbestaetigungViewFilter.getModulFilter();
        final List<SearchFilterObject> all = modulFilter.getAll();
        assertThat(all, hasItem(new SearchFilterObject(1L, "Name")));
    }

    @Test
    void getUmsetzungsbestaetigungViewFilterSensorCocFilterResultSize() {
        prozessbaukastenFilterDto = new ProzessbaukastenFilterDto(42L, "Fach Id", 1, "Bezeichnung");
        prozessbaukastenFilters = Collections.singletonList(prozessbaukastenFilterDto);
        sensorcocs = Collections.singletonList(sensorcoc);
        module = Collections.singletonList(modul);
        previousPhase = KovAPhase.VABG0;

        when(prozessbaukastenReadService.getAllFilterDto()).thenReturn(prozessbaukastenFilters);
        when(berechtigungService.getSensorCocForMitarbeiter(any(), any())).thenReturn(sensorcocs);
        when(modulService.getAllModule()).thenReturn(module);
        when(sensorcoc.getSensorCocId()).thenReturn(1L);
        when(sensorcoc.pathToStringShort()).thenReturn("PATH");

        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(previousPhase);

        final UmsetzungsbestaetigungViewFilter umsetzungsbestaetigungViewFilter = umsetzungsbestaetigungFilterService.getUmsetzungsbestaetigungViewFilter(urlParameter, kovAPhaseImDerivat, previousPhase);
        final IdSearchFilter sensorCocFilter = umsetzungsbestaetigungViewFilter.getSensorCocFilter();
        final List<SearchFilterObject> all = sensorCocFilter.getAll();
        assertThat(all, hasSize(1));
    }

    @Test
    void getUmsetzungsbestaetigungViewFilterSensorCocFilterItemValue() {
        prozessbaukastenFilterDto = new ProzessbaukastenFilterDto(42L, "Fach Id", 1, "Bezeichnung");
        prozessbaukastenFilters = Collections.singletonList(prozessbaukastenFilterDto);
        sensorcocs = Collections.singletonList(sensorcoc);
        module = Collections.singletonList(modul);
        previousPhase = KovAPhase.VABG0;

        when(prozessbaukastenReadService.getAllFilterDto()).thenReturn(prozessbaukastenFilters);
        when(berechtigungService.getSensorCocForMitarbeiter(any(), any())).thenReturn(sensorcocs);
        when(modulService.getAllModule()).thenReturn(module);
        when(sensorcoc.getSensorCocId()).thenReturn(1L);
        when(sensorcoc.pathToStringShort()).thenReturn("PATH");

        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(previousPhase);

        final UmsetzungsbestaetigungViewFilter umsetzungsbestaetigungViewFilter = umsetzungsbestaetigungFilterService.getUmsetzungsbestaetigungViewFilter(urlParameter, kovAPhaseImDerivat, previousPhase);
        final IdSearchFilter sensorCocFilter = umsetzungsbestaetigungViewFilter.getSensorCocFilter();
        final List<SearchFilterObject> all = sensorCocFilter.getAll();
        assertThat(all, hasItem(new SearchFilterObject(1L, "PATH")));
    }


    @Test
    void setRestrictedFilterForUmsetzungsbestaetigungProzessbaukastenFilter() {
        prozessbaukastenFilterDto = new ProzessbaukastenFilterDto(42L, "Fach Id", 1, "Bezeichnung");
        prozessbaukastenFilters = Collections.singletonList(prozessbaukastenFilterDto);
        sensorcocs = Collections.singletonList(sensorcoc);
        module = Collections.singletonList(modul);
        previousPhase = KovAPhase.VABG0;

        when(prozessbaukastenReadService.getAllFilterDto()).thenReturn(prozessbaukastenFilters);
        when(berechtigungService.getSensorCocForMitarbeiter(any(), any())).thenReturn(sensorcocs);
        when(modulService.getAllModule()).thenReturn(module);
        when(sensorcoc.getSensorCocId()).thenReturn(1L);
        when(sensorcoc.pathToStringShort()).thenReturn("PATH");
        when(modul.getId()).thenReturn(2L);
        when(modul.getName()).thenReturn("Name");

        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(previousPhase);

        when(umsetzungsbestaetigungDto.getProzessbaukastenId()).thenReturn(Optional.empty());
        when(umsetzungsbestaetigungDto.getSensorCoc()).thenReturn(sensorcoc);
        when(umsetzungsbestaetigungDto.getModul()).thenReturn(modul);

        umsetzungsbestaetigungen = Collections.singletonList(umsetzungsbestaetigungDto);

        final UmsetzungsbestaetigungViewFilter filter = umsetzungsbestaetigungFilterService.getUmsetzungsbestaetigungViewFilter(urlParameter, kovAPhaseImDerivat, previousPhase);
        umsetzungsbestaetigungFilterService.setRestrictedFilterForUmsetzungsbestaetigung(filter, umsetzungsbestaetigungen, urlParameter);
        final IdSearchFilter prozessbaukastenFilter = filter.getProzessbaukastenFilter();
        final List<SearchFilterObject> all = prozessbaukastenFilter.getAll();
        assertThat(all, empty());
    }

    @Test
    void setRestrictedFilterForUmsetzungsbestaetigungSensorCocFilter() {
        prozessbaukastenFilterDto = new ProzessbaukastenFilterDto(42L, "Fach Id", 1, "Bezeichnung");
        prozessbaukastenFilters = Collections.singletonList(prozessbaukastenFilterDto);
        sensorcocs = Arrays.asList(sensorcoc, anotherSensorCoc);
        module = Arrays.asList(modul, anotherModul);
        previousPhase = KovAPhase.VABG0;

        when(prozessbaukastenReadService.getAllFilterDto()).thenReturn(prozessbaukastenFilters);
        when(berechtigungService.getSensorCocForMitarbeiter(any(), any())).thenReturn(sensorcocs);
        when(modulService.getAllModule()).thenReturn(module);
        when(sensorcoc.getSensorCocId()).thenReturn(1L);
        when(sensorcoc.pathToStringShort()).thenReturn("PATH");
        when(modul.getId()).thenReturn(2L);
        when(modul.getName()).thenReturn("Name");

        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(previousPhase);

        when(umsetzungsbestaetigungDto.getProzessbaukastenId()).thenReturn(Optional.empty());
        when(umsetzungsbestaetigungDto.getSensorCoc()).thenReturn(sensorcoc);
        when(umsetzungsbestaetigungDto.getModul()).thenReturn(modul);

        umsetzungsbestaetigungen = Collections.singletonList(umsetzungsbestaetigungDto);

        final UmsetzungsbestaetigungViewFilter filter = umsetzungsbestaetigungFilterService.getUmsetzungsbestaetigungViewFilter(urlParameter, kovAPhaseImDerivat, previousPhase);
        umsetzungsbestaetigungFilterService.setRestrictedFilterForUmsetzungsbestaetigung(filter, umsetzungsbestaetigungen, urlParameter);
        final IdSearchFilter sensorCocFilter = filter.getSensorCocFilter();
        final List<SearchFilterObject> all = sensorCocFilter.getAll();
        assertThat(all, IsCollectionWithSize.hasSize(1));
    }

    @Test
    void setRestrictedFilterForUmsetzungsbestaetigungModulFilter() {
        prozessbaukastenFilterDto = new ProzessbaukastenFilterDto(42L, "Fach Id", 1, "Bezeichnung");
        prozessbaukastenFilters = Collections.singletonList(prozessbaukastenFilterDto);
        sensorcocs = Arrays.asList(sensorcoc, anotherSensorCoc);
        module = Arrays.asList(modul, anotherModul);
        previousPhase = KovAPhase.VABG0;

        when(prozessbaukastenReadService.getAllFilterDto()).thenReturn(prozessbaukastenFilters);
        when(berechtigungService.getSensorCocForMitarbeiter(any(), any())).thenReturn(sensorcocs);
        when(modulService.getAllModule()).thenReturn(module);
        when(sensorcoc.getSensorCocId()).thenReturn(1L);
        when(sensorcoc.pathToStringShort()).thenReturn("PATH");
        when(modul.getId()).thenReturn(2L);
        when(modul.getName()).thenReturn("Name");

        when(kovAPhaseImDerivat.getKovAPhase()).thenReturn(previousPhase);

        when(umsetzungsbestaetigungDto.getProzessbaukastenId()).thenReturn(Optional.empty());
        when(umsetzungsbestaetigungDto.getSensorCoc()).thenReturn(sensorcoc);
        when(umsetzungsbestaetigungDto.getModul()).thenReturn(modul);

        umsetzungsbestaetigungen = Collections.singletonList(umsetzungsbestaetigungDto);

        final UmsetzungsbestaetigungViewFilter filter = umsetzungsbestaetigungFilterService.getUmsetzungsbestaetigungViewFilter(urlParameter, kovAPhaseImDerivat, previousPhase);
        umsetzungsbestaetigungFilterService.setRestrictedFilterForUmsetzungsbestaetigung(filter, umsetzungsbestaetigungen, urlParameter);
        final IdSearchFilter modulFilter = filter.getModulFilter();
        final List<SearchFilterObject> all = modulFilter.getAll();
        assertThat(all, IsCollectionWithSize.hasSize(1));
    }
}