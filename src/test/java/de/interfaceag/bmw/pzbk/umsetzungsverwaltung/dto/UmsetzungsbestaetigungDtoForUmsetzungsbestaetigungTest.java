package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Umsetzungsbestaetigung;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class UmsetzungsbestaetigungDtoForUmsetzungsbestaetigungTest {

    private static final UmsetzungsBestaetigungStatus STATUS = UmsetzungsBestaetigungStatus.UMGESETZT;
    private static final UmsetzungsBestaetigungStatus LASTPHASESTATUS = UmsetzungsBestaetigungStatus.UMGESETZT;
    private static final String LASTPHASECOMMENT = "LASTPHASECOMMENT";
    private static final String ANFORDERUNG = "ANFORDERUNG";
    private static final String FACHID = "FACHID";
    private static final String BESCHREIBUNG = "BESCHREIBUNG DER ANFORDERUNG MIT TEST TEXT DER LAENGE 42 BESCHREIBUNG DER ANFORDERUNG MIT TEST TEXT DER LAENGE 42 BESCHREIBUNG DER ANFORDERUNG MIT TEST TEXT DER LAENGE 42";
    private static final String BESCHREIBUNG_SHORT = "BESCHREIBUNG DER ANFORDERUNG MIT TEST TEXT DER LAENGE 42 BESCHREIBUNG DER ANFORD...";
    private static final long ANFORDERUNGID = 1L;
    private static final int ANFORDERUNGVERSION = 1;
    private static final long PROZESSBAUKASTENID = 2L;
    private static final String KOMMENTAR = "KOMMENTAR";
    private static final String UMSETZUNGSBESTAETIGER = "Hans Karl, Max Mustermann";

    @Mock
    private Umsetzungsbestaetigung umsetzungsbestaetigung;
    @Mock
    Anforderung anforderung;
    @Mock
    SensorCoc sensorCoc;
    @Mock
    Modul modul;
    @Mock
    Prozessbaukasten prozessbaukasten;

    @BeforeEach
    public void setUp() {
        when(umsetzungsbestaetigung.getAnforderung()).thenReturn(anforderung);
        when(anforderung.getId()).thenReturn(ANFORDERUNGID);
        when(anforderung.getVersion()).thenReturn(ANFORDERUNGVERSION);
        when(anforderung.toString()).thenReturn(ANFORDERUNG);
        when(anforderung.getFachId()).thenReturn(FACHID);
        when(anforderung.getBeschreibungAnforderungDe()).thenReturn(BESCHREIBUNG);
        when(anforderung.getSensorCoc()).thenReturn(sensorCoc);
        when(umsetzungsbestaetigung.getModul()).thenReturn(modul);
        when(umsetzungsbestaetigung.getKommentar()).thenReturn(KOMMENTAR);
        when(umsetzungsbestaetigung.getStatus()).thenReturn(STATUS);
        when(anforderung.getGueltigerProzessbaukasten()).thenReturn(Optional.of(prozessbaukasten));
        when(prozessbaukasten.getId()).thenReturn(PROZESSBAUKASTENID);
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigungDto.getAnforderung();
        MatcherAssert.assertThat(result, is(ANFORDERUNG));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigungId() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Long result = umsetzungsbestaetigungDto.getAnforderungId();
        MatcherAssert.assertThat(result, is(ANFORDERUNGID));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulFachId() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigungDto.getFachId();
        MatcherAssert.assertThat(result, is(FACHID));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulVersion() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigungDto.getVersion();
        MatcherAssert.assertThat(result, is(Integer.toString(ANFORDERUNGVERSION)));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulBeschreibungShort() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigungDto.getShortText();
        MatcherAssert.assertThat(result, is(BESCHREIBUNG_SHORT));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulBeschreibungFull() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigungDto.getFullText();
        MatcherAssert.assertThat(result, is(BESCHREIBUNG));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulSensorCoc() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        SensorCoc result = umsetzungsbestaetigungDto.getSensorCoc();
        MatcherAssert.assertThat(result, is(sensorCoc));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulModul() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Modul result = umsetzungsbestaetigungDto.getModul();
        MatcherAssert.assertThat(result, is(modul));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulStatus() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        UmsetzungsBestaetigungStatus result = umsetzungsbestaetigungDto.getStatus();
        MatcherAssert.assertThat(result, is(STATUS));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulComment() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigungDto.getComment();
        MatcherAssert.assertThat(result, is(KOMMENTAR));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulLastPhaseStatus() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Optional<UmsetzungsBestaetigungStatus> result = umsetzungsbestaetigungDto.getLastPhasestatus();
        MatcherAssert.assertThat(result, is(Optional.of(LASTPHASESTATUS)));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulLastPhaseComment() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigungDto.getLastPhaseComment();
        MatcherAssert.assertThat(result, is(LASTPHASECOMMENT));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulProzessbaukastenId() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Optional<Long> result = umsetzungsbestaetigungDto.getProzessbaukastenId();
        MatcherAssert.assertThat(result, is(Optional.of(PROZESSBAUKASTENID)));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulIsUmgesetzt() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Boolean result = umsetzungsbestaetigungDto.isUmgesetzt();
        MatcherAssert.assertThat(result, is(Boolean.TRUE));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulId() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Long result = umsetzungsbestaetigungDto.getId();
        MatcherAssert.assertThat(result, is(0L));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulAnforderungFachId() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigungDto.getAnforderungFachId();
        MatcherAssert.assertThat(result, is(FACHID));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulLastPhaseStatusString() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        String result = umsetzungsbestaetigungDto.getLastPhasestatusString();
        MatcherAssert.assertThat(result, is(LASTPHASESTATUS.getStatusBezeichnung()));
    }

    @Test
    public void testBuildUmsetzungsbestaetigungDtoForDerivatAnforderungModulIsUmsetzungsbestaetigungUmgesetzt() {
        UmsetzungsbestaetigungDto umsetzungsbestaetigungDto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForUmsetzungsbestaetigung(umsetzungsbestaetigung, LASTPHASESTATUS, LASTPHASECOMMENT, UMSETZUNGSBESTAETIGER);
        Boolean result = umsetzungsbestaetigungDto.isUmsetzungsbestaetigungUmgesetzt();
        MatcherAssert.assertThat(result, is(Boolean.TRUE));
    }

}
