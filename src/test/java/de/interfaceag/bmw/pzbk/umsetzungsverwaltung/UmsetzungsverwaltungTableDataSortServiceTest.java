package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOrders;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungenOrderService;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungDto;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class UmsetzungsverwaltungTableDataSortServiceTest {

    @Mock
    private ProzessbaukastenAnforderungenOrderService prozessbaukastenAnforderungenOrderService;

    @InjectMocks
    private UmsetzungsverwaltungTableDataSortService umsetzungsverwaltungTableDataSortService;

    @Mock
    private UmsetzungsbestaetigungDto umsetzungsbestaetigungDto1;
    @Mock
    private UmsetzungsbestaetigungDto umsetzungsbestaetigungDto2;
    @Mock
    private UmsetzungsbestaetigungDto umsetzungsbestaetigungDto3;

    List<UmsetzungsbestaetigungDto> vereinbarungen;

    @Mock
    ProzessbaukastenAnforderungOrders prozessbaukastenAnforderungOrders;

    @BeforeEach
    public void setUp() {
        vereinbarungen = new ArrayList<>();
        vereinbarungen.add(umsetzungsbestaetigungDto1);
        vereinbarungen.add(umsetzungsbestaetigungDto2);
        vereinbarungen.add(umsetzungsbestaetigungDto3);
    }

    private void setupList() {
        when(umsetzungsbestaetigungDto2.getAnforderungId()).thenReturn(2L);
        when(umsetzungsbestaetigungDto3.getAnforderungId()).thenReturn(3L);

        when(umsetzungsbestaetigungDto1.getProzessbaukastenId()).thenReturn(Optional.empty());
        when(umsetzungsbestaetigungDto2.getProzessbaukastenId()).thenReturn(Optional.of(1L));
        when(umsetzungsbestaetigungDto3.getProzessbaukastenId()).thenReturn(Optional.of(1L));

        when(prozessbaukastenAnforderungenOrderService.getAnforderungenOrderForProzessbaukasten(any())).thenReturn(prozessbaukastenAnforderungOrders);
        when(prozessbaukastenAnforderungOrders.getOrderForProzessbaukastenAnforderung(anyLong(), anyLong()))
                .thenAnswer(invocation -> {
                    Long argument = (Long) invocation.getArgument(1);
                    if (2L == argument) {
                        return 0;
                    } else if (3L == argument) {
                        return 1;
                    } else {
                        return null;
                    }
                });

    }

    @Test
    public void testSortTableDataBeforeSize() {
        MatcherAssert.assertThat(vereinbarungen, Matchers.hasSize(3));
    }

    @Test
    public void testSortTableDataBeforeOrder() {
        MatcherAssert.assertThat(vereinbarungen, Matchers.contains(umsetzungsbestaetigungDto1, umsetzungsbestaetigungDto2, umsetzungsbestaetigungDto3));
    }

    @Test
    public void testSortTableData() {
        setupList();
        List<UmsetzungsbestaetigungDto> result = umsetzungsverwaltungTableDataSortService.sortTableData(vereinbarungen);
        MatcherAssert.assertThat(result, Matchers.contains(umsetzungsbestaetigungDto2, umsetzungsbestaetigungDto3, umsetzungsbestaetigungDto1));
    }

    @Test
    public void testSortTableDataForEmptyCollection() {
        when(umsetzungsbestaetigungDto1.getProzessbaukastenId()).thenReturn(Optional.empty());
        when(umsetzungsbestaetigungDto2.getProzessbaukastenId()).thenReturn(Optional.empty());
        when(umsetzungsbestaetigungDto3.getProzessbaukastenId()).thenReturn(Optional.empty());

        List<UmsetzungsbestaetigungDto> result = umsetzungsverwaltungTableDataSortService.sortTableData(vereinbarungen);
        MatcherAssert.assertThat(result, Matchers.contains(umsetzungsbestaetigungDto1, umsetzungsbestaetigungDto2, umsetzungsbestaetigungDto3));
    }

}
