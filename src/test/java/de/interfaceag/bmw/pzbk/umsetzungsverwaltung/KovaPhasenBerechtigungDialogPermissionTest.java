package de.interfaceag.bmw.pzbk.umsetzungsverwaltung;

import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.ArrayList;
import java.util.Collection;

class KovaPhasenBerechtigungDialogPermissionTest {

    private Collection<Rolle> roles;

    @BeforeEach
    void setUp() {
        roles = new ArrayList<>();
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isAllowedToEditForKovAStatusOffen(Rolle rolle) {
        roles.add(rolle);
        boolean result = KovaPhasenBerechtigungDialogPermission.isAllowedToEdit(roles, KovAStatus.OFFEN);
        MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.FALSE));
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isAllowedToEditForKovAStatusKonfiguriert(Rolle rolle) {
        roles.add(rolle);
        boolean result = KovaPhasenBerechtigungDialogPermission.isAllowedToEdit(roles, KovAStatus.KONFIGURIERT);

        switch (rolle) {
            case ADMIN:
            case SENSORCOCLEITER:
            case SCL_VERTRETER:
                MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.TRUE));
                break;
            default:
                MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.FALSE));
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isAllowedToEditForKovAStatusAktiv(Rolle rolle) {
        roles.add(rolle);
        boolean result = KovaPhasenBerechtigungDialogPermission.isAllowedToEdit(roles, KovAStatus.AKTIV);

        if (rolle == Rolle.ADMIN) {
            MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.TRUE));
        } else {
            MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.FALSE));
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void isAllowedToEditForKovAStatusAbgeschlossen(Rolle rolle) {
        roles.add(rolle);
        boolean result = KovaPhasenBerechtigungDialogPermission.isAllowedToEdit(roles, KovAStatus.ABGESCHLOSSEN);
        MatcherAssert.assertThat(result, CoreMatchers.is(Boolean.FALSE));
    }
}