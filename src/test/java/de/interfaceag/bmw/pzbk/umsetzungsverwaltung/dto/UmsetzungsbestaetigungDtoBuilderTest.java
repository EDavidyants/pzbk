package de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto;

import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.prozessbaukasten.dto.ProzessbaukastenLabelDto;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsInstanceOf;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class UmsetzungsbestaetigungDtoBuilderTest {

    private static final long ANFORDERUNG_ID = 1L;
    private static final String ANFORDERUNG = "ANFORDERUNG";
    private static final String FACHID = "FACHID";
    private static final String VERSION = "VERSION";
    private static final String SHORT = "SHORT";
    private static final String FULL = "FULL";
    private static final String COMMENT = "COMMENT";
    private static final UmsetzungsBestaetigungStatus STATUS = UmsetzungsBestaetigungStatus.UMGESETZT;

    @Mock
    SensorCoc sensorCoc;
    @Mock
    Modul modul;

    UmsetzungsbestaetigungDtoBuilder builder;

    @BeforeEach
    public void setUp() {
        builder = new UmsetzungsbestaetigungDtoBuilder();
    }

    @Test
    public void testSetAnforderungId() {
        builder.setAnforderungId(ANFORDERUNG_ID);
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result.getAnforderungId(), is(ANFORDERUNG_ID));
    }

    @Test
    public void testSetAnforderung() {
        builder.setAnforderung(ANFORDERUNG);
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result.getAnforderung(), is(ANFORDERUNG));
    }

    @Test
    public void testSetFachId() {
        builder.setFachId(FACHID);
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result.getFachId(), is(FACHID));
    }

    @Test
    public void testSetVersion() {
        builder.setVersion(VERSION);
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result.getVersion(), is(VERSION));
    }

    @Test
    public void testSetShortText() {
        builder.setShortText(SHORT);
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result.getShortText(), is(SHORT));
    }

    @Test
    public void testSetFullText() {
        builder.setFullText(FULL);
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result.getFullText(), is(FULL));
    }

    @Test
    public void testSetSensorCoc() {
        builder.setSensorCoc(sensorCoc);
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result.getSensorCoc(), is(sensorCoc));
    }

    @Test
    public void testSetModul() {
        builder.setModul(modul);
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result.getModul(), is(modul));
    }

    @Test
    public void testSetStatus() {
        builder.setStatus(STATUS);
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result.getStatus(), is(STATUS));
    }

    @Test
    public void testSetComment() {
        builder.setComment(COMMENT);
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result.getComment(), is(COMMENT));
    }

    @Test
    public void testSetLastPhasestatus() {
        builder.setLastPhasestatus(STATUS);
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result.getLastPhasestatus(), is(Optional.of(STATUS)));
    }

    @Test
    public void testSetLastPhaseComment() {
        builder.setLastPhaseComment(COMMENT);
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result.getLastPhaseComment(), is(COMMENT));
    }

    @Test
    public void testWithProzessbaukastenId() {
        builder.withProzessbaukastenLabelDto(new ProzessbaukastenLabelDto(1L, "P123", 42));
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result.getProzessbaukastenId(), is(Optional.of(1L)));
    }

    @Test
    public void testCreateUmsetzungsbestaetigungDto() {
        UmsetzungsbestaetigungDto result = builder.createUmsetzungsbestaetigungDto();
        MatcherAssert.assertThat(result, IsInstanceOf.any(UmsetzungsbestaetigungDto.class));
    }

}
