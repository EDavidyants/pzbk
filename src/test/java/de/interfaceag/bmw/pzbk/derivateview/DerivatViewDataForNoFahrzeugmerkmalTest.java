package de.interfaceag.bmw.pzbk.derivateview;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmaleProgressDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class DerivatViewDataForNoFahrzeugmerkmalTest {

    @Mock
    private DerivatViewFilter viewFilter;

    private DerivatViewData viewData;

    private Derivat derivat;

    @BeforeEach
    void setUp() {
        List<String> allProduktlinien = Collections.emptyList();
        LocalDate currentDate = LocalDate.now();
        LocalDate startDate = LocalDate.now();
        List<Derivat> derivate = generateDerivate();
        int totalNumberOfFahrzeugmerkmale = 0;
        Collection<FahrzeugmerkmaleProgressDto> fahrzeugmerkmaleProgressDtos = Collections.emptyList();

        viewData = new DerivatViewData(new Derivat(),
                derivate, totalNumberOfFahrzeugmerkmale,
                fahrzeugmerkmaleProgressDtos, allProduktlinien,
                "", currentDate, startDate, viewFilter);
    }

    @Test
    void getProgressValueForDerivat1() {
        long progressValue = viewData.getProgressValueForDerivat(derivat);
        assertThat(progressValue, is(100L));
    }


    @Test
    void testGetProgressBarLabelForDerivat1() {
        String progressLabel = viewData.getProgressBarLabelForDerivat(derivat);
        assertThat(progressLabel, is("0 / 0"));
    }

    @Test
    void testIsAbgeschlossenForDerivat1() {
        boolean isAbgeschlossen = viewData.isAbgeschlossenForDerivat(derivat);
        assertTrue(isAbgeschlossen);
    }

    private List<Derivat> generateDerivate() {
        derivat = new Derivat("E20", "Bezeichnung E20", "LI");
        derivat.setId(1L);
        return Collections.singletonList(derivat);
    }

}
