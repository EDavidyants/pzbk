package de.interfaceag.bmw.pzbk.derivateview;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusChangeDTO;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatusNames;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ContextMocker;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import de.interfaceag.bmw.pzbk.shared.utils.PageUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.primefaces.context.RequestContext;

import javax.faces.context.FacesContext;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DerivatControllerTest {


    @Mock
    private Session session;

    @Mock
    private DerivateViewPermission derivateViewPermission;

    @Mock
    private DerivatViewFacade derivatViewFacade;

    @Mock
    private DerivatStatusNames derivatStatusNames;

    @Mock
    private DerivatViewData derivatViewData;

    @Mock
    private DerivatViewFilter derivatViewFilter;

    @Mock
    private DerivatStatusChangeDTO derivatStatusChangeDTO;

    private RequestContext requestContext;

    private FacesContext facesContext;

    @Spy
    @InjectMocks
    private DerivatController derivatController;


    @Mock
    private UserPermissions<BerechtigungDto> userPermission;

    private Set<Rolle> roles;

    @BeforeEach
    void setUp() {

    }

    @Test
    void initTest() {

        roles = new HashSet<>();
        when(session.getUserPermissions()).thenReturn(userPermission);
        when(userPermission.getRoles()).thenReturn(roles);
        doReturn(new UrlParameter()).when(derivatController).getUrlParameter();

        derivatController.init();


        verify(derivatViewFacade, times(1)).getInitData(any());
    }

    @Test
    void getFilterTest() {

        when(derivatViewData.getFilter()).thenReturn(derivatViewFilter);

        DerivatViewFilter filter = derivatController.getFilter();

        Assertions.assertEquals(filter, derivatViewFilter);

    }

    @Test
    void resetTest() {

        when(derivatViewData.getFilter()).thenReturn(derivatViewFilter);

        derivatController.reset();
        verify(derivatViewFilter, times(1)).getResetUrl();
    }

    @Test
    void filterTest() {

        when(derivatViewData.getFilter()).thenReturn(derivatViewFilter);

        derivatController.filter();
        verify(derivatViewFilter, times(1)).getUrl();
    }

    @Test
    void saveNewDerivatTest() {

        when(derivatViewData.getDerivat()).thenReturn(TestDataFactory.generateDerivat());
        when(derivatViewData.getFilter()).thenReturn(derivatViewFilter);

        derivatController.saveNewDerivat();

        verify(derivatViewFacade, times(1)).createNewDerivat(any());
    }

    @Test
    void openDeleteDerivatDialogTest() {
        Derivat derivat = TestDataFactory.generateDerivat();
        requestContext = ContextMocker.mockRequestContext();
        doReturn(requestContext).when(derivatController).getCurrentRequestInstance();


        derivatController.openDeleteDerivatDialog(derivat);

        verify(requestContext, times(1)).update("dialog:derivatDeleteConfirmDialogForm");
        verify(requestContext, times(1)).execute("PF('derivatDeleteConfirmDialog').show()");
    }

    @Test
    void openChangeDerivatStatusDialogTest() {
        Derivat derivat = TestDataFactory.generateDerivat();
        requestContext = ContextMocker.mockRequestContext();
        doReturn(requestContext).when(derivatController).getCurrentRequestInstance();


        derivatController.openChangeDerivatStatusDialog(derivat);

        verify(requestContext, times(1)).update("dialog:derivatChangeStatusDialogForm");
        verify(requestContext, times(1)).execute("PF('derivatChangeStatusDialog').show()");
    }

    @Test
    void aktualisierePhasenTest() {

        facesContext = ContextMocker.mockFacesContext();
        doReturn(facesContext).when(derivatController).getCurrentFacesInstance();
        when(derivatViewFacade.aktualisierePhasen()).thenReturn(new HashMap<>());

        derivatController.aktualisierePhasen();

        verify(facesContext, times(1)).addMessage(any(), any());
    }

    @Test
    void getDerivatStatusChangeDTOTest() {
        DerivatStatusChangeDTO result = derivatController.getDerivatStatusChangeDTO();

        Assertions.assertEquals(derivatStatusChangeDTO, result);
    }

    @Test
    void getDerivatDeleteMessageTest() {

        derivatController.getDerivatDeleteMessage();

        verify(derivatViewFacade, times(1)).getDerivatDeleteMessage(any());

    }

    @Test
    void getDerivatZuordnungenTest() {

        derivatController.getDerivatZuordnungen();

        verify(derivatViewFacade, times(1)).getDerivatZuordnungen(any());

    }

    @Test
    void proceedWithDerivatRemovalTest() {

        when(derivatViewData.getFilteredDerivate()).thenReturn(TestDataFactory.genereteDerivate());
        when(derivatViewFacade.getUpdatedDerivate(any())).thenReturn(TestDataFactory.genereteDerivate());
        when(derivatViewFacade.getUpdatedSelectedDerivate(any(), any())).thenReturn(TestDataFactory.genereteDerivate());

        requestContext = ContextMocker.mockRequestContext();
        doReturn(requestContext).when(derivatController).getCurrentRequestInstance();
        doReturn(new UrlParameter()).when(derivatController).getUrlParameter();

        derivatController.proceedWithDerivatRemoval();

        verify(derivatViewFacade, times(1)).changeStatusToInactive(any());
        verify(requestContext, times(1)).update("derivatAccordionPanel:derivateForm:derivateTable");

    }

    @Test
    void restoreDeletedDerivatTest() {
        when(derivatViewData.getFilteredDerivate()).thenReturn(TestDataFactory.genereteDerivate());
        when(derivatViewFacade.getUpdatedDerivate(any())).thenReturn(TestDataFactory.genereteDerivate());
        when(derivatViewFacade.getUpdatedSelectedDerivate(any(), any())).thenReturn(TestDataFactory.genereteDerivate());

        requestContext = ContextMocker.mockRequestContext();
        doReturn(requestContext).when(derivatController).getCurrentRequestInstance();
        doReturn(new UrlParameter()).when(derivatController).getUrlParameter();

        derivatController.restoreDeletedDerivat(TestDataFactory.generateDerivat());

        verify(derivatViewFacade, times(1)).restoreDeletedDerivat(any());
        verify(requestContext, times(1)).update("derivatAccordionPanel:derivateForm:derivateTable");

    }

    @Test
    void filterByAktiveStatusTest() {
        when(derivatViewData.getOnlyActiveFilter()).thenReturn(true);
        when(derivatViewFacade.getUpdatedFilteredDerivate(any(), any())).thenReturn(TestDataFactory.genereteDerivate());

        requestContext = ContextMocker.mockRequestContext();
        doReturn(requestContext).when(derivatController).getCurrentRequestInstance();

        derivatController.filterByAktiveStatus();

        verify(requestContext, times(1)).update("derivatAccordionPanel:derivateForm:derivateTable");

    }

    @Test
    void changeDerivatStatus() {
        when(derivatViewData.getSelectedDerivat()).thenReturn(TestDataFactory.generateDerivat());
        String result = derivatController.changeDerivatStatus(DerivatStatus.OFFEN);
        verify(derivatViewFacade, times(1)).changeDerivatStatus(any(), any());
        Assertions.assertEquals(result, PageUtils.getRedirectUrlForPage(Page.PROJEKTVERWALTUNG));
    }

    @Test
    void convertDerivatStatusToStringTest() {
        derivatController.convertDerivatStatusToString(TestDataFactory.generateDerivat());
        verify(derivatStatusNames, times(1)).getNameForStatus(any());

    }

    @Test
    void isStatusChangeEnabledTest() {
        roles = new HashSet<>();
        when(session.getUserPermissions()).thenReturn(userPermission);
        when(userPermission.getRoles()).thenReturn(roles);
        boolean result = derivatController.isStatusChangeEnabled(TestDataFactory.generateDerivat());
        Assertions.assertFalse(result);
    }
}