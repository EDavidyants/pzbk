package de.interfaceag.bmw.pzbk.derivateview;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.dto.FahrzeugmerkmaleProgressDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class DerivatViewDataTest {

    @Mock
    private DerivatViewFilter viewFilter;

    private DerivatViewData viewData;

    private List<String> allProduktlinien;
    private LocalDate currentDate;
    private LocalDate startDate;
    private Derivat derivat1;
    private Derivat derivat2;
    private Derivat derivat3;
    private List<Derivat> derivate;
    private int totalNumberOfFahrzeugmerkmale;
    private Collection<FahrzeugmerkmaleProgressDto> fahrzeugmerkmaleProgressDtos;

    @BeforeEach
    public void setUp() {
        allProduktlinien = generateProduktlinien();
        currentDate = LocalDate.now();
        startDate = LocalDate.now();
        derivate = generateDerivate();
        totalNumberOfFahrzeugmerkmale = 2;
        fahrzeugmerkmaleProgressDtos = generateFahrzeugmerkmaleProgressDtos();

        viewData = new DerivatViewData(new Derivat(),
                derivate, totalNumberOfFahrzeugmerkmale,
                fahrzeugmerkmaleProgressDtos, allProduktlinien,
                "", currentDate, startDate, viewFilter);

    }

    @Test
    public void getProgressValueForDerivat1() {
        long progressValue = viewData.getProgressValueForDerivat(derivat1);
        assertThat(progressValue, is(100L));
    }

    @Test
    public void getProgressValueForDerivat2() {
        long progressValue = viewData.getProgressValueForDerivat(derivat2);
        assertThat(progressValue, is(50L));
    }

    @Test
    public void getProgressValueForDerivat3() {
        long progressValue = viewData.getProgressValueForDerivat(derivat3);
        assertThat(progressValue, is(0L));
    }

    @Test
    public void testGetProgressBarLabelForDerivat1() {
        String progressLabel = viewData.getProgressBarLabelForDerivat(derivat1);
        assertThat(progressLabel, is("2 / 2"));
    }

    @Test
    public void testGetProgressBarLabelForDerivat2() {
        String progressLabel = viewData.getProgressBarLabelForDerivat(derivat2);
        assertThat(progressLabel, is("1 / 2"));
    }

    @Test
    public void testGetProgressBarLabelForDerivat3() {
        String progressLabel = viewData.getProgressBarLabelForDerivat(derivat3);
        assertThat(progressLabel, is("0 / 2"));
    }

    @Test
    public void testIsAbgeschlossenForDerivat1() {
        boolean isAbgeschlossen = viewData.isAbgeschlossenForDerivat(derivat1);
        assertTrue(isAbgeschlossen);
    }

    @Test
    public void testIsAbgeschlossenForDerivat2() {
        boolean isAbgeschlossen = viewData.isAbgeschlossenForDerivat(derivat2);
        assertFalse(isAbgeschlossen);
    }

    @Test
    public void testIsAbgeschlossenForDerivat3() {
        boolean isAbgeschlossen = viewData.isAbgeschlossenForDerivat(derivat3);
        assertFalse(isAbgeschlossen);
    }

    private List<String> generateProduktlinien() {
        List<String> produktlinien = new ArrayList<>();
        produktlinien.add("LI");
        produktlinien.add("LK");
        produktlinien.add("LU");
        produktlinien.add("LG");
        return produktlinien;
    }

    private List<Derivat> generateDerivate() {
        derivat1 = new Derivat("E20", "Bezeichnung E20", "LI");
        derivat1.setId(1L);

        derivat2 = new Derivat("F12", "Bezeichnung F12", "LU");
        derivat2.setId(2L);

        derivat3 = new Derivat("M1", "Bezeichnung M1", "LG");
        derivat3.setId(3L);

        return Arrays.asList(derivat1, derivat2, derivat3);
    }

    private Collection<FahrzeugmerkmaleProgressDto> generateFahrzeugmerkmaleProgressDtos() {
        Collection<FahrzeugmerkmaleProgressDto> progressDtos = new HashSet<>();
        progressDtos.add(new FahrzeugmerkmaleProgressDto(1L, 2L));
        progressDtos.add(new FahrzeugmerkmaleProgressDto(2L, 1L));
        return progressDtos;
    }

}
