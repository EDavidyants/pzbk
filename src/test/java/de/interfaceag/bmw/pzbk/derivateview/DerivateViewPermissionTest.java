package de.interfaceag.bmw.pzbk.derivateview;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.HashSet;

class DerivateViewPermissionTest {

    private DerivateViewPermission derivateViewPermission;


    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getPage(Rolle rolle) {
        HashSet<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);
        derivateViewPermission = new DerivateViewPermission(rollen);
        switch (rolle) {
            case ADMIN:
            case ANFORDERER:
                Assertions.assertTrue(derivateViewPermission.getPage());
                break;
            default:
                Assertions.assertFalse(derivateViewPermission.getPage());
                break;
        }

    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getNeuesDerivatPanel(Rolle rolle) {
        HashSet<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);
        derivateViewPermission = new DerivateViewPermission(rollen);
        switch (rolle) {
            case ADMIN:
                Assertions.assertTrue(derivateViewPermission.getNeuesDerivatPanel());
                break;
            default:
                Assertions.assertFalse(derivateViewPermission.getNeuesDerivatPanel());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getAktualisierungButton(Rolle rolle) {
        HashSet<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);
        derivateViewPermission = new DerivateViewPermission(rollen);
        switch (rolle) {
            case ADMIN:
                Assertions.assertTrue(derivateViewPermission.getAktualisierungButton());
                break;
            default:
                Assertions.assertFalse(derivateViewPermission.getAktualisierungButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getDerivatBearbeitenButton(Rolle rolle) {
        HashSet<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);
        derivateViewPermission = new DerivateViewPermission(rollen);
        switch (rolle) {
            case ADMIN:
                Assertions.assertTrue(derivateViewPermission.getDerivatBearbeitenButton());
                break;
            default:
                Assertions.assertFalse(derivateViewPermission.getDerivatBearbeitenButton());
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    void getKovaPhasenBearbeitenButton(Rolle rolle) {
        HashSet<Rolle> rollen = new HashSet<>();
        rollen.add(rolle);
        derivateViewPermission = new DerivateViewPermission(rollen);
        switch (rolle) {
            case ADMIN:
            case ANFORDERER:
                Assertions.assertTrue(derivateViewPermission.getKovaPhasenBearbeitenButton());
                break;
            default:
                Assertions.assertFalse(derivateViewPermission.getKovaPhasenBearbeitenButton());
                break;
        }
    }
}