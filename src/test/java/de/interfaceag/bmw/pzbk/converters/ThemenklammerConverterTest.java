package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.text.IsEmptyString.emptyString;

class ThemenklammerConverterTest {

    private ThemenklammerDto themenklammerDto1;
    private ThemenklammerDto themenklammerDto2;

    private List<ThemenklammerDto> themenklammern;

    private ThemenklammerConverter themenklammerConverter;

    @BeforeEach
    void setUp() {
        themenklammerDto1 = new ThemenklammerDto(1L, "Bezeichnung1");
        themenklammerDto2 = new ThemenklammerDto(2L, "Bezeichnung2");
        themenklammern = new ArrayList<>();

        themenklammern.add(themenklammerDto1);
        themenklammern.add(themenklammerDto2);
        themenklammerConverter = new ThemenklammerConverter(themenklammern);
    }

    @Test
    void getAsObjectNullInput() {
        final ThemenklammerDto result = themenklammerConverter.getAsObject(null, null, null);
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectEmptyStringInput() {
        final ThemenklammerDto result = themenklammerConverter.getAsObject(null, null, "");
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectNotIdInput() {
        final ThemenklammerDto result = themenklammerConverter.getAsObject(null, null, "Lorenz");
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectNotInList() {
        final ThemenklammerDto result = themenklammerConverter.getAsObject(null, null, "3");
        assertThat(result, nullValue());
    }

    @ParameterizedTest
    @ValueSource(strings = {"1", "2"})
    void getAsObjectInList(String input) {
        final ThemenklammerDto result = themenklammerConverter.getAsObject(null, null, input);
        assertThat(result, notNullValue());
    }

    @ParameterizedTest
    @ValueSource(strings = {"1", "2"})
    void getAsObjectForNullInitializedConstructor(String input) {
        themenklammerConverter = new ThemenklammerConverter(null);
        final ThemenklammerDto result = themenklammerConverter.getAsObject(null, null, input);
        assertThat(result, nullValue());
    }

    @Test
    void getAsStringNullInput() {
        final String result = themenklammerConverter.getAsString(null, null, null);
        assertThat(result, emptyString());
    }

    @Test
    void getAsString() {
        final String result = themenklammerConverter.getAsString(null, null, themenklammerDto1);
        assertThat(result, is(themenklammerDto1.getId().toString()));
    }
}