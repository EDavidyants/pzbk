package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class ZuordnungStatusConverterTest {

    private ZuordnungStatusConverter converter;

    @BeforeEach
    void setUp() {
        converter = new ZuordnungStatusConverter();
    }

    @ParameterizedTest
    @ValueSource(strings = {"0", "2", "7", "9"})
    void getAsObject(String value) {
        final ZuordnungStatus status = converter.getAsObject(null, null, value);
        final ZuordnungStatus expected;
        switch (value) {
            case "0":
                expected = ZuordnungStatus.OFFEN;
                break;
            case "2":
                expected = ZuordnungStatus.KEINE_ZUORDNUNG;
                break;
            case "7":
                expected = ZuordnungStatus.ZUGEORDNET;
                break;
            default:
                expected = null;
        }
        assertThat(status, is(expected));
    }

    @ParameterizedTest
    @EnumSource(ZuordnungStatus.class)
    void getAsString(ZuordnungStatus status) {
        final String asString = converter.getAsString(null, null, status);
        assertThat(asString, is(Integer.toString(status.getId())));
    }
}