package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.nullValue;

class DerivatConverterTest {


    private DerivatConverter derivatConverter;

    @BeforeEach
    void setUp() {

        derivatConverter = new DerivatConverter(TestDataFactory.genereteDerivate());
    }

    @Test
    void getAsObjectNullInput() {
        Object result = derivatConverter.getAsObject(null, null, null);
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectEmptyStringInput() {
        final Object result = derivatConverter.getAsObject(null, null, "");
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectNotIdInput() {
        final Object result = derivatConverter.getAsObject(null, null, "Lorenz");
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectDerivatIdStringInput() {
        Derivat testDerivat = TestDataFactory.generateDerivat();
        final Derivat result = (Derivat) derivatConverter.getAsObject(null, null, "1");
        Assertions.assertEquals(testDerivat, result);
    }

    @Test
    void getAsObjectWronIdStringInput() {
        Derivat testDerivat = TestDataFactory.generateDerivat();
        final Derivat result = (Derivat) derivatConverter.getAsObject(null, null, "2");
        Assertions.assertNotEquals(testDerivat, result);
        assertThat(result, nullValue());
    }

    @Test
    void getAsStringNullInput() {
        String result = derivatConverter.getAsString(null, null, null);
        Assertions.assertEquals(result, null);
    }

    @Test
    void getAsStringNotIdInput() {
        String result = derivatConverter.getAsString(null, null, "Lorenz");
        assertThat(result, nullValue());
    }

    @Test
    void getAsStringDateStringInput() {
        Derivat testDerivat = TestDataFactory.generateDerivat();
        final String result = derivatConverter.getAsString(null, null, testDerivat);
        Assertions.assertEquals(result, "1");
    }
}