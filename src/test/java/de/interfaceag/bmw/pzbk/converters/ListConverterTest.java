package de.interfaceag.bmw.pzbk.converters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.nullValue;

class ListConverterTest {

    private ListConverter listConverter;

    @BeforeEach
    void setUp() {
        listConverter = new ListConverter();
    }

    @Test
    void getAsObjectNullInput() {
        List result = listConverter.getAsObject(null, null, null);
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectEmptyStringInput() {
        final List result = listConverter.getAsObject(null, null, "");
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectNotIdInput() {
        final List result = listConverter.getAsObject(null, null, "Lorenz");
        assertThat(result, notNullValue());
        assertThat(result.size(), is(1));
    }

    @Test
    void getAsStringNullInput() {
        String result = listConverter.getAsString(null, null, null);
        assertThat(result, nullValue());
    }

    @Test
    void getAsStringEmptyListInput() {
        final List<String> list = Collections.emptyList();
        String result = listConverter.getAsString(null, null, list);
        assertThat(result, nullValue());
    }

    @Test
    void getAsStringValidListInput() {
        final List<String> list = Collections.singletonList("1");
        String result = listConverter.getAsString(null, null, list);
        assertThat(result, is(list.toString()));
    }


}