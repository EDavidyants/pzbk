package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.nullValue;

class DerivatAnforderungModulStatusConverterTest {


    private DerivatAnforderungModulStatusConverter derivatAnforderungModulStatusConverter;

    @BeforeEach
    void setUp() {
        derivatAnforderungModulStatusConverter = new DerivatAnforderungModulStatusConverter();
    }

    @Test
    void getAsObjectNullInput() {
        Object result = derivatAnforderungModulStatusConverter.getAsObject(null, null, null);
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectEmptyStringInput() {
        final Object result = derivatAnforderungModulStatusConverter.getAsObject(null, null, "");
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectNotIdInput() {
        final Object result = derivatAnforderungModulStatusConverter.getAsObject(null, null, "/n");
        assertThat(result, nullValue());
    }

    @ParameterizedTest
    @EnumSource(DerivatAnforderungModulStatus.class)
    void getAsObjectDateStringInput(DerivatAnforderungModulStatus status) {

        final DerivatAnforderungModulStatus result = (DerivatAnforderungModulStatus) derivatAnforderungModulStatusConverter.getAsObject(null, null, status.getStatusBezeichnung());
        Assertions.assertEquals(result, status);
    }

    @Test
    void getAsStringNullInput() {
        String result = derivatAnforderungModulStatusConverter.getAsString(null, null, null);
        assertThat(result, nullValue());
    }

    @Test
    void getAsStringNotIdInput() {
        String result = derivatAnforderungModulStatusConverter.getAsString(null, null, "Lorenz");
        assertThat(result, nullValue());
    }

    @ParameterizedTest
    @EnumSource(DerivatAnforderungModulStatus.class)
    void getAsStringDateStringInput(DerivatAnforderungModulStatus status) {

        final String result = derivatAnforderungModulStatusConverter.getAsString(null, null, status);
        Assertions.assertEquals(result, status.toString());
    }
}