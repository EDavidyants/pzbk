package de.interfaceag.bmw.pzbk.converters;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.faces.convert.ConverterException;
import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.nullValue;

class DateTimeConverterTest {


    private DateTimeConverter dateTimeConverter;

    @BeforeEach
    void setUp() {
        dateTimeConverter = new DateTimeConverter();
    }

    @Test
    void getAsObjectNullInput() {
        Object result = dateTimeConverter.getAsObject(null, null, null);
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectEmptyStringInput() {
        final Object result = dateTimeConverter.getAsObject(null, null, "");
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectNotIdInput() {
        Assertions.assertThrows(ConverterException.class, () -> {
            dateTimeConverter.getAsObject(null, null, "Lorenz");
        });
    }

    @Test
    void getAsObjectDateStringInput() {
        LocalDate testDate = LocalDate.of(2019, 8, 29);
        final LocalDate result = (LocalDate) dateTimeConverter.getAsObject(null, null, "2019-08-29");
        Assertions.assertEquals(result, testDate);
    }

    @Test
    void getAsStringNullInput() {
        String result = dateTimeConverter.getAsString(null, null, null);
        Assertions.assertEquals(result, "");
    }

    @Test
    void getAsStringNotIdInput() {
        Assertions.assertThrows(ConverterException.class, () -> {
            dateTimeConverter.getAsString(null, null, "Lorenz");
        });
    }

    @Test
    void getAsStringDateStringInput() {
        LocalDate testDate = LocalDate.of(2019, 8, 29);
        final String result = dateTimeConverter.getAsString(null, null, testDate);
        Assertions.assertEquals(result, "2019-08-29");
    }

}