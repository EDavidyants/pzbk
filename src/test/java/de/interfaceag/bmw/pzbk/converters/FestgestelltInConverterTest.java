package de.interfaceag.bmw.pzbk.converters;

import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.nullValue;

class FestgestelltInConverterTest {


    private FestgestelltInConverter festgestelltInConverter;

    @BeforeEach
    void setUp() {

        festgestelltInConverter = new FestgestelltInConverter(TestDataFactory.generateFestgestelltInList());
    }

    @Test
    void getAsObjectNullInput() {
        Object result = festgestelltInConverter.getAsObject(null, null, null);
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectEmptyStringInput() {
        final Object result = festgestelltInConverter.getAsObject(null, null, "");
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectNotIdInput() {
        final Object result = festgestelltInConverter.getAsObject(null, null, "Lorenz");
        assertThat(result, nullValue());
    }

    @Test
    void getAsObjectDerivatIdStringInput() {
        FestgestelltIn festgestelltIn = TestDataFactory.generateFestgestelltIn();
        final FestgestelltIn result = (FestgestelltIn) festgestelltInConverter.getAsObject(null, null, "Wert");
        Assertions.assertEquals(festgestelltIn, result);
    }


    @Test
    void getAsStringNullInput() {
        String result = festgestelltInConverter.getAsString(null, null, null);
        assertThat(result, nullValue());
    }

    @Test
    void getAsStringNotIdInput() {
        String result = festgestelltInConverter.getAsString(null, null, "Lorenz");
        assertThat(result, nullValue());
    }

    @Test
    void getAsStringDateStringInput() {
        FestgestelltIn festgestelltIn = TestDataFactory.generateFestgestelltIn();
        final String result = festgestelltInConverter.getAsString(null, null, festgestelltIn);
        Assertions.assertEquals(result, "Wert");
    }
}