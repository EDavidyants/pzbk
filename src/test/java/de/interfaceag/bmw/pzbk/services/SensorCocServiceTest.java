package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamMitgliedBerechtigung;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamVertreterBerechtigung;
import de.interfaceag.bmw.pzbk.berechtigung.dto.UserPermissionsDTO;
import de.interfaceag.bmw.pzbk.dao.SensorCocDao;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.session.Session;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class SensorCocServiceTest {

    @Mock
    private SensorCocDao sensorCocDao;
    @Mock
    private BerechtigungService berechtigungService;

    @Mock
    private Session session;

    @Mock
    private TteamMitgliedBerechtigung tteamMitgliedBerechtigung;
    @Mock
    private TteamVertreterBerechtigung tteamVertreterBerechtigung;
    @Mock
    private List<BerechtigungDto> berechtigungen;
    @Mock
    private List<Rolle> rollen;

    @InjectMocks
    private SensorCocService sensorCocService;

    @Mock
    List<SensorCoc> allSensorCocs;
    @Mock
    SensorCoc sensorCoc;
    @Mock
    Mitarbeiter mitarbeiter;

    @BeforeEach
    public void setUp() {
    }

    @Test
    public void testGetAllSortByTechnologie() throws Exception {
        when(sensorCocDao.getAllSensorCoCsSortByTechnologie()).thenReturn(allSensorCocs);
        List<SensorCoc> result = sensorCocService.getAllSortByTechnologie();
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocById() throws Exception {
        when(sensorCocDao.getSensorCocById(any(Long.class))).thenReturn(sensorCoc);
        SensorCoc result = sensorCocService.getSensorCocById(1L);
        assertThat(result, is(sensorCoc));
    }

    @Test
    public void testGetSensorCocByIdSensorCocLeiterLookup() throws Exception {
        when(sensorCocDao.getSensorCocById(any(Long.class))).thenReturn(sensorCoc);
        when(berechtigungService.getSensorCoCLeiterOfSensorCoc(sensorCoc)).thenReturn(Optional.of(mitarbeiter));
        sensorCocService.getSensorCocById(1L);
        verify(sensorCoc, times(1)).setSensorCoCLeiter(mitarbeiter);
    }

    @Test
    public void testGetSensorCocByIdSensorCocLeiterInserted() throws Exception {
        when(sensorCocDao.getSensorCocById(any(Long.class))).thenReturn(sensorCoc);
        sensorCocService.getSensorCocById(1L);
        verify(berechtigungService, times(1)).getSensorCoCLeiterOfSensorCoc(sensorCoc);
    }

    @Test
    public void testGetSensorCocsByIdListEmptyList() throws Exception {
        when(sensorCocDao.getSensorCocsByIdList(any())).thenReturn(allSensorCocs);
        List<SensorCoc> result = sensorCocService.getSensorCocsByIdList(Collections.emptyList());
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocsByIdListNull() throws Exception {
        when(sensorCocDao.getSensorCocsByIdList(any())).thenReturn(allSensorCocs);
        List<SensorCoc> result = sensorCocService.getSensorCocsByIdList(null);
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocsByIdList() throws Exception {
        List<Long> ids = Arrays.asList(1L);
        when(sensorCocDao.getSensorCocsByIdList(any())).thenReturn(allSensorCocs);
        List<SensorCoc> result = sensorCocService.getSensorCocsByIdList(ids);
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocByZakEinordnungNull() throws Exception {
        when(sensorCocDao.getSensorCocByZakEinordnung(any())).thenReturn(allSensorCocs);
        List<SensorCoc> result = sensorCocService.getSensorCocByZakEinordnung(null);
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocByZakEinordnungEmptyString() throws Exception {
        when(sensorCocDao.getSensorCocByZakEinordnung(any())).thenReturn(allSensorCocs);
        List<SensorCoc> result = sensorCocService.getSensorCocByZakEinordnung("");
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocByZakEinordnung() throws Exception {
        when(sensorCocDao.getSensorCocByZakEinordnung(any())).thenReturn(allSensorCocs);
        List<SensorCoc> result = sensorCocService.getSensorCocByZakEinordnung("ZAK");
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocByTechnologieNull() throws Exception {
        when(sensorCocDao.getSensorCocsByTechnologie(any())).thenReturn(allSensorCocs);
        List<SensorCoc> result = sensorCocService.getSensorCocsByTechnologie(null);
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocByTechnologieEmptyString() throws Exception {
        when(sensorCocDao.getSensorCocsByTechnologie(any())).thenReturn(allSensorCocs);
        List<SensorCoc> result = sensorCocService.getSensorCocsByTechnologie("");
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocByTechnologie() throws Exception {
        when(sensorCocDao.getSensorCocsByTechnologie(any())).thenReturn(allSensorCocs);
        List<SensorCoc> result = sensorCocService.getSensorCocsByTechnologie("Technologie");
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocsByOrtung() throws Exception {
        List<String> ortungen = Arrays.asList("Ortung");
        when(sensorCocDao.getSensorCocsByOrtung(any())).thenReturn(allSensorCocs);
        Collection<SensorCoc> result = sensorCocService.getSensorCocsByOrtung(ortungen);
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocsByOrtungNull() throws Exception {
        when(sensorCocDao.getSensorCocsByOrtung(any())).thenReturn(allSensorCocs);
        Collection<SensorCoc> result = sensorCocService.getSensorCocsByOrtung(null);
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocsByOrtungEmptyList() throws Exception {
        when(sensorCocDao.getSensorCocsByOrtung(any())).thenReturn(allSensorCocs);
        Collection<SensorCoc> result = sensorCocService.getSensorCocsByOrtung(Collections.emptyList());
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testgetSensorCocByTechnologieWithOneResult() {
        when(sensorCocDao.getSensorCocsByTechnologie(any(String.class))).thenReturn(allSensorCocs);
        when(allSensorCocs.size()).thenReturn(1);
        when(allSensorCocs.get(0)).thenReturn(sensorCoc);
        SensorCoc result = sensorCocService.getSensorCocByTechnologie("");
        assertThat(result, is(sensorCoc));
    }

    @Test
    public void testgetSensorCocByTechnologieWithMultipleResult() {
        when(sensorCocDao.getSensorCocsByTechnologie(any(String.class))).thenReturn(allSensorCocs);
        when(allSensorCocs.size()).thenReturn(2);
        SensorCoc result = sensorCocService.getSensorCocByTechnologie("");
        assertThat(result, is(nullValue()));
    }

    @Test
    public void testGetSensorCocsByTechnologieList() throws Exception {
        when(sensorCocDao.getSensorCocsByTechnologieList(any())).thenReturn(allSensorCocs);
        List<SensorCoc> result = sensorCocService.getSensorCocsByTechnologieList(Collections.emptyList());
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testPersistSensorCoc() throws Exception {
        sensorCocService.persistSensorCoc(sensorCoc);
        verify(sensorCocDao, times(1)).persistSensorCoc(sensorCoc);
    }

    @Test
    public void testGetSensorCocsByRessortTechnologieOrtung() throws Exception {
        when(sensorCocDao.getSensorCocsByRessortTechnologieOrtung(any(), any(), any())).thenReturn(allSensorCocs);
        List<SensorCoc> result = sensorCocService.getSensorCocsByRessortTechnologieOrtung("", "", "");
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocsByRessortTechnologieOrtungByRoleWithAuthorizationForAll() throws Exception {
        when(sensorCocDao.getSensorCocsByRessortTechnologieOrtung(any(), any(), any())).thenReturn(allSensorCocs);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Test"));
        when(session.getUserPermissions()).thenReturn(TestDataFactory.generateUserPermissions());
        List<SensorCoc> result = sensorCocService.getSensorCocsByRessortTechnologieOrtungByRole("", "", "");
        assertThat(result, is(allSensorCocs));
    }

    @Test
    public void testGetSensorCocsByRessortTechnologieOrtungByRoleWithoutAuthorizationForAll() throws Exception {
        when(sensorCocDao.getSensorCocsByRessortTechnologieOrtungInList(any(), any(), any(), any())).thenReturn(Collections.emptyList());
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("Test"));
        UserPermissionsDTO userPermissions = new UserPermissionsDTO(rollen,
                berechtigungen,
                tteamMitgliedBerechtigung,
                tteamVertreterBerechtigung
        );
        when(session.getUserPermissions()).thenReturn(userPermissions);
        List<SensorCoc> result = sensorCocService.getSensorCocsByRessortTechnologieOrtungByRole("", "", "");
        assertThat(result, is(Collections.emptyList()));
    }

    @Test
    public void testGetAllZakEinordnungSize() throws Exception {
        List<String> zakEinordnungen = Arrays.asList("ZAK1", "ZAK2", "ZAK2", "ZAK3", "ZAK3");
        when(sensorCocDao.getAllZakEinordnung()).thenReturn(zakEinordnungen);
        List<String> result = sensorCocService.getAllZakEinordnung();
        assertThat(result, IsCollectionWithSize.hasSize(3));
    }

    @Test
    public void testGetAllZakEinordnungContainsAllDifferentResults() throws Exception {
        List<String> zakEinordnungen = Arrays.asList("ZAK1", "ZAK2", "ZAK2", "ZAK3", "ZAK3");
        when(sensorCocDao.getAllZakEinordnung()).thenReturn(zakEinordnungen);
        List<String> result = sensorCocService.getAllZakEinordnung();
        assertThat(result, IsIterableContaining.hasItems("ZAK1", "ZAK2", "ZAK3"));
    }

    @Test
    public void testGetAllSensorCocIds() throws Exception {
        List<Long> ids = Arrays.asList(1L);
        when(sensorCocDao.getAllSensorCocIds()).thenReturn(ids);
        List<Long> result = sensorCocService.getAllSensorCocIds();
        assertThat(result, is(ids));
    }

    @Test
    public void testConvertSensorCocToSensorCocId() {
        List<SensorCoc> sensorCocs = TestDataFactory.createSensorCocs(3);
        List<Long> tteamIds = sensorCocService.getSensorCocIdsFromSensorCocList(sensorCocs);

        assertThat(tteamIds.size(), is(3));
    }

}
