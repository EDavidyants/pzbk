package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.DbFileDao;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.enums.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@RunWith(MockitoJUnitRunner.class)
public class FileServiceTest {

    @Mock
    private DbFileDao dbFileDao;

    @Mock
    private AnforderungService anforderungService;

    @Mock
    private ConfigService configService;

    private Anhang anhangOne;

    @InjectMocks
    private final FileService fileService = new FileService();

    @Before
    public void SetUp() {
        TestDataFactory testDataFactory = new TestDataFactory();
        anhangOne = testDataFactory.createAnhangFromFile("tukan.jpg");
        when(configService.getBasePath()).thenReturn("target");
    }

    @Test
    public void reduceToRelativePath() {
        String relativePath = FileService.reduceToRelativePath("/var/pzbk/A/8946/test.hello");
        assertEquals("/A/8946/test.hello", relativePath);
    }

    @Test
    public void writeAnhangToFileMeldungTest() {
        Boolean isWritten = fileService.writeAnhangToFile(anhangOne, ContentType.MELDUNG, 123L);
        Assertions.assertTrue(isWritten);
    }

    @Test
    public void writeAnhangToFileAnforderungTest() {
        Boolean isWritten = fileService.writeAnhangToFile(anhangOne, ContentType.ANFORDERUNG, 123L);
        Assertions.assertTrue(isWritten);
    }

    @Test
    public void writeAnhangToFileProzessbaukastenTest() {
        Boolean isWritten = fileService.writeAnhangToFile(anhangOne, ContentType.PZBK, 123L);
        Assertions.assertTrue(isWritten);
    }

    @Test
    public void writeAnhangToFileOtherTest() {
        Boolean isWritten = fileService.writeAnhangToFile(anhangOne, null, 123L);
        Assertions.assertFalse(isWritten);
    }

    @Test
    public void writeAnhangToFileNullIdTest() {
        Boolean isWritten = fileService.writeAnhangToFile(anhangOne, ContentType.PZBK, null);
        Assertions.assertFalse(isWritten);
    }

    @Test
    public void writeAnhangToFileInvalidAnhangTest() {
        Anhang invalidAnhang = TestDataFactory.generateAnhangOfNullContent();
        Boolean isWritten = fileService.writeAnhangToFile(invalidAnhang, ContentType.PZBK, 123L);
        Assertions.assertFalse(isWritten);
    }

}
