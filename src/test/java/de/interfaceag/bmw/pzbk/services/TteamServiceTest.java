package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.entities.Tteam;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
class TteamServiceTest {

    @InjectMocks
    private TteamService tteamService;



    @Test
    public void getTteamIdsByTteamListTest() {
        List<Tteam> tteams = TestDataFactory.createListOfTteams(3);
        List<Long> tteamIds = tteamService.getTteamIdsByTteamList(tteams);

        assertThat(tteamIds.size(), is(3));
    }

}