package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungEditDtoImpl;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamMitgliedBerechtigung;
import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamVertreterBerechtigung;
import de.interfaceag.bmw.pzbk.berechtigung.dto.UserPermissionsDTO;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Auswirkung;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Bild;
import de.interfaceag.bmw.pzbk.entities.DbFile;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModulHistory;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.InfoText;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenAnforderungKonzept;
import de.interfaceag.bmw.pzbk.entities.ProzessbaukastenThemenklammer;
import de.interfaceag.bmw.pzbk.entities.ReferenzSystemLink;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Themenklammer;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.UBzuKovaPhaseImDerivatUndSensorCoC;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import de.interfaceag.bmw.pzbk.entities.Umsetzungsbestaetigung;
import de.interfaceag.bmw.pzbk.entities.Werk;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.entities.Zielwert;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.Kategorie;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.ReferenzSystem;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.UmsetzungsBestaetigungStatus;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.exceptions.InvalidDataException;
import de.interfaceag.bmw.pzbk.permissions.EditPermission;
import de.interfaceag.bmw.pzbk.permissions.RolePermission;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.prozessbaukasten.AnforderungenTabDTO;
import de.interfaceag.bmw.pzbk.prozessbaukasten.KonzeptDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenAnforderung;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenAnforderungenViewData;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenAnhangTabViewData;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenStammdatenViewData;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenViewData;
import de.interfaceag.bmw.pzbk.prozessbaukasten.view.ProzessbaukastenViewPermission;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.AnforderungenProzessbaukastenZuordnenDTO;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.AnforderungenProzessbaukastenZuordnenViewData;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDTO;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenDialogViewData;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamAnforderungAbgestimmtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamAnforderungNewVersionKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamGeloeschteObjekteKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamGemeldeteMeldungenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamInputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamMeldungExistingAnforderungKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.FachteamUnstimmigKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamPlausibilisiertKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.TteamUnstimmigKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungFreigegebenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungGeloeschtKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungOutputKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungProzessbaukastenKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.keyfigures.VereinbarungUnstimmigKeyFigure;
import de.interfaceag.bmw.pzbk.reporting.statustransition.migration.AnforderungHistoryMigrationDto;
import de.interfaceag.bmw.pzbk.reporting.statustransition.migration.MeldungHistoryMigrationDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.KovAPhaseImDerivatDto;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.dto.UmsetzungsbestaetigungDto;
import de.interfaceag.bmw.pzbk.zak.ZakUpdateDto;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class TestDataFactory implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(TestDataFactory.class);

    /**
     * Erstallt size Testmeldungen mit dem angegebenen Status. Das Argument
     * idStart dient dazu festzulegen mit welchen Ids die Meldungen erzeugt
     * werden sollen. Gibt man beispielsweise für size 10, und idStart 20 an,
     * werden den 10 erstellten Meldungen die Id's 20 - 29 zugewiesen.
     *
     * @param size
     * @param status
     * @param idStart
     * @return
     */
    public static List<Meldung> createTestMeldungen(int size, Status status, Long idStart) {
        List<Meldung> result = new ArrayList<>();
        List<SensorCoc> scocs = createSensorCocs(size);
        Long idMeldung = idStart;
        for (int i = 0; i < size; i++) {
            Meldung meldung = new Meldung();
            meldung.setId(idMeldung);
            idMeldung++;
            meldung.setStatus(status);
            meldung.setSensor(createMitarbeiter("Sensor_" + i));
            meldung.setSensorCoc(scocs.get(i));
            result.add(meldung);
        }
        return result;
    }

    public static List<SensorCoc> createSensorCocs(int size) {
        List<SensorCoc> result = new ArrayList<>();
        List<String> zakEinordnungen = createZakEinordnungen(size);
        for (int i = 0; i < size; i++) {
            SensorCoc scoc = new SensorCoc();
            scoc.setSensorCocId(new Long(i));
            scoc.setOrtung("ortung_" + i);
            scoc.setRessort("ressort_" + i);
            scoc.setTechnologie("technologie_" + i);
            scoc.setZakEinordnung(zakEinordnungen.get(i));
            String mitarbeiterSupplement = "SensorCoC-Leiter_" + scoc.getTechnologie();
            scoc.setSensorCoCLeiter(createMitarbeiter(mitarbeiterSupplement));
            result.add(scoc);
        }
        return result;
    }

    /**
     * Erstellt eine Liste von Mitarbeitern, deren Attriuten das angegebene
     * Supplement angehänt wird um eine bessere Individualisierung der Testdaten
     * zu ermöglichen.
     *
     * @param size
     * @param supplement
     * @return
     */
    public static List<Mitarbeiter> createMitarbeiter(int size, String supplement) {
        List<Mitarbeiter> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            Mitarbeiter m = Mitarbeiter.createTestAnwender("", "");
            m.setQNumber("q" + i + "_" + supplement);
            m.setVorname("vorname_" + i + "_" + supplement);
            m.setNachname("nachname_" + i + "_" + supplement);
            result.add(m);
        }
        return result;
    }

    /**
     * Erstellt einen Mitarbeiter dessen Attriute das angegebene Supplement
     * angehänt wird um eine bessere Individualisierung der Testdaten zu
     * ermöglichen.
     *
     * @param supplement
     * @return
     */
    public static Mitarbeiter createMitarbeiter(String supplement) {
        Mitarbeiter mitarbeiter = Mitarbeiter.createTestAnwender(supplement, "q2222222");
        return mitarbeiter;
    }

    public static List<String> createZakEinordnungen(int size) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            result.add("zakEinordnung_" + i);
        }
        return result;
    }

    public static List<ZakUebertragung> generateZakUebertragungen(Anforderung anforderung, Derivat derivat, Modul modul, Date date) {
        ZakUebertragung zakUebertragung = generateZakUebertragung(anforderung, derivat, modul, date);
        List<ZakUebertragung> result = new ArrayList<>();
        result.add(zakUebertragung);
        return result;
    }

    public static ZakUebertragung generateZakUebertragung(Anforderung anforderung, Derivat derivat, Modul modul, Date date, ZakStatus zakStatus) {
        anforderung.setId(1L);
        ZuordnungAnforderungDerivat anforderungDerivat = new ZuordnungAnforderungDerivat(anforderung, derivat, ZuordnungStatus.OFFEN);
        DerivatAnforderungModul derivatAnforderungModul = new DerivatAnforderungModul(anforderungDerivat, modul);
        derivatAnforderungModul.setId(1L);
        derivatAnforderungModul.setAnforderer(generateMitarbeiter("Anf", "Orderer", "IF-B1"));
        modul.setId(1L);
        ZakUebertragung zakUebertragung = new ZakUebertragung(derivatAnforderungModul);
        zakUebertragung.setId(1L);
        zakUebertragung.setZakId("42");
        zakUebertragung.setUebertragungId(1L);
        zakUebertragung.setZakKommentar("ZAK Kommentar");
        zakUebertragung.setErsteller("ZAK Uebertrager");
        zakUebertragung.setAenderungsdatum(date);
        zakUebertragung.setZakStatus(zakStatus);
        return zakUebertragung;
    }

    public static ZakUebertragung generateZakUebertragungWithoutId(DerivatAnforderungModul derivatAnforderungModul, Date date, ZakStatus zakStatus) {

        ZakUebertragung zakUebertragung = new ZakUebertragung(derivatAnforderungModul);
        zakUebertragung.setZakId("42");
        zakUebertragung.setUebertragungId(1L);
        zakUebertragung.setZakKommentar("ZAK Kommentar");
        zakUebertragung.setErsteller("ZAK Uebertrager");
        zakUebertragung.setAenderungsdatum(date);
        zakUebertragung.setZakStatus(zakStatus);
        return zakUebertragung;
    }

    public static ZakUebertragung generateZakUebertragung(Anforderung anforderung, Derivat derivat, Modul modul, Date date) {
        return generateZakUebertragung(anforderung, derivat, modul, date, ZakStatus.DONE);
    }

    public static List<Anforderung> generateListOfAnforderung() {
        List<Anforderung> returnList = new ArrayList<>();
        for (int i = 1; i < 20; i++) {
            String id = i + "";
            returnList.add(generateAnforderungWithFachIdAndVersion(Long.parseLong(id), "A" + i, 1));
        }
        return returnList;
    }

    public static List<AnforderungenProzessbaukastenZuordnenDTO> generateListOfProzessbaukasenAnforderungenDTO() {
        List<AnforderungenProzessbaukastenZuordnenDTO> returnList = new ArrayList<>();
        for (Anforderung anforderung : generateListOfAnforderung()) {
            returnList.add(new AnforderungenProzessbaukastenZuordnenDTO(anforderung.getFachId(), anforderung.getVersion(), anforderung.getBeschreibungAnforderungDe()));
        }
        return returnList;
    }

    public static Anforderung generateAnforderungWithFachIdAndVersion(Long id, String fachId, Integer version) {
        Anforderung anforderung = generateAnforderungWithFachid(fachId);
        anforderung.setId(id);
        anforderung.setVersion(version);
        return anforderung;
    }

    public static Anforderung generateAnforderung() {
        return generateAnforderungWithFachid("A42");
    }

    public static Anforderung generateAnforderungWithFachid(String fachid) {
        Anforderung anforderung = generateAnforderungWithoutId(fachid, generateTteam(), generateMitarbeiter("Sen", "Sor", "IF-Lab"),
                generateSensorCoc(), generateModulSeTeams(generateModul(),
                        generateModulKomponenten()), generateMeldung(), generateFestgestelltInSet(), generateWerk());
        anforderung.setId(2L);
        return anforderung;
    }

    public static Anforderung generateAnforderungWithoutId(String fachid, Tteam tteam, Mitarbeiter mitarbeiter, SensorCoc sensorCoc, List<ModulSeTeam> modulSeTeam,
            Meldung meldung, Set<FestgestelltIn> festgestelltIn, Werk werk) {
        Anforderung anforderung = new Anforderung(fachid, 1, "Kommentar");
        anforderung.setSensorCoc(sensorCoc);
        anforderung.setAnfoFreigabeBeiModul(generateAnforderungFreigabeBeiModulSeTeamsWithMitarbeiter(anforderung, modulSeTeam, mitarbeiter));
        anforderung.setBeschreibungAnforderungDe("Beschreibung der Anforderung DE");
        anforderung.setZeitpktUmsetzungsbest(true);
        anforderung.setAenderungsdatum(new Date());
        anforderung.setBeschreibungAnforderungEn("Beschreibung der Anforderung EN");
        anforderung.addAuswirkung(generateAuswirkung());
        anforderung.addUmsetzer(generateUmsetzer(modulSeTeam.get(0)));
        anforderung.setBeschreibungStaerkeSchwaeche("Beschreibung der Stärke Schwäche");
        anforderung.setErstellungsdatum(new Date());
        anforderung.setKommentarAnforderung("Kommentar zur Anforderung");
        anforderung.setLoesungsvorschlag("Lösungsvorschlag");
        anforderung.setPhasenbezug(true);
        anforderung.setReferenzen("Referenzen");
        anforderung.setSensor(mitarbeiter);
        anforderung.setStaerkeSchwaeche(true);
        anforderung.setUrsache("Ursache");
        anforderung.setUmsetzenderBereich("Umsetzender Bereich");
        anforderung.setZielwert(generateZielwert());
        anforderung.setFestgestelltIn(festgestelltIn);
        anforderung.setStatus(Status.A_INARBEIT);
        anforderung.setWerk(werk);
        anforderung.setTteam(tteam);
        anforderung.setZeitpunktStatusaenderung(new Date());
        Set<Meldung> zugewiesendeMeldungen = new HashSet<>();
        zugewiesendeMeldungen.add(meldung);
        anforderung.setMeldungen(zugewiesendeMeldungen);
        anforderung.setErsteller("Ersteller");
        anforderung.setReferenzSystemLinks(Arrays.asList(new ReferenzSystemLink(ReferenzSystem.DOORS, "ReferenzsystemID")));
        anforderung.setKategorie(Kategorie.ZWEI);

        return anforderung;
    }

    public static Anforderung generateNewVersionAnforderung() {
        Anforderung anforderung = generateAnforderung();
        anforderung.setVersion(2);
        return anforderung;
    }

    public static Meldung generateMeldung() {
        Meldung meldung = new Meldung();
        meldung.setId(1L);
        meldung.setFachId("M42");
        meldung.setKommentarAnforderung("Kommentar");
        meldung.setSensorCoc(generateSensorCoc());
        meldung.setBeschreibungAnforderungDe("Beschreibung der Anforderung DE");
        meldung.setAenderungsdatum(new Date());
        meldung.setBeschreibungAnforderungEn("Beschreibung der Anforderung EN");
        meldung.addAuswirkung(generateAuswirkung());
        meldung.setBeschreibungStaerkeSchwaeche("Beschreibung der Stärke Schwäche");
        meldung.setErstellungsdatum(new Date());
        meldung.setKommentarAnforderung("Kommentar zur Anforderung");
        meldung.setLoesungsvorschlag("Lösungsvorschlag");
        meldung.setReferenzen("Referenzen");
        meldung.setSensor(generateMitarbeiter("Sen", "Sor", "IF-Lab"));
        meldung.setStaerkeSchwaeche(true);
        meldung.setUrsache("Ursache");
        meldung.setUmsetzenderBereich("Umsetzender Bereich");
        meldung.setZielwert(generateZielwert());
        meldung.setFestgestelltIn(generateFestgestelltInSet());

        return meldung;
    }

    public static Zielwert generateZielwert() {
        Zielwert zielwert = new Zielwert();
        zielwert.setWert("Wert");
        return zielwert;
    }

    public static Werk generateWerk() {
        Werk werk = new Werk();
        werk.setName("Werk");
        return werk;
    }

    public static List<Tteam> createListOfTteams(int size) {
        List<Tteam> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            Tteam tteam = new Tteam();
            tteam.setId(new Long(i));
            tteam.setTeamName("teamname" + i);
            result.add(tteam);
        }
        return result;
    }

    public static Tteam generateTteam() {
        Tteam tteam = new Tteam("T-Team Name");
        tteam.setId(1L);
        return tteam;
    }

    public static Tteam generateTteamWithoutId() {
        Tteam tteam = new Tteam("T-Team Name");
        return tteam;
    }

    public static Tteam generateTteam(String teamName) {
        Tteam tteam = new Tteam(teamName);
        return tteam;
    }

    public static Umsetzer generateUmsetzer() {
        return new Umsetzer(generateModulSeTeam(generateModul(), generateModulKomponenten()));
    }

    public static Umsetzer generateUmsetzer(ModulSeTeam modulSeTeam) {
        return new Umsetzer(modulSeTeam);

    }

    public static Auswirkung generateAuswirkung() {
        Auswirkung auswirkung = new Auswirkung();
        auswirkung.setAuswirkung("Auswirkung");
        auswirkung.setWert("Wert");
        return auswirkung;
    }

    public static List<Modul> generateModule() {
        return Stream.of(generateFullModul()).collect(Collectors.toList());
    }

    public static Modul generateFullModul() {
        Modul modul = generateModulWithoutId();

        modul.setId(1L);
        return modul;
    }

    public static Modul generateModulWithoutId() {
        Modul modul = new Modul("modulName", "Fachbereich", "beschreibung");
        ModulKomponente komponente = new ModulKomponente("ppg1", "komponente1");
        ModulSeTeam seTeam = new ModulSeTeam("seTeam", modul,
                Stream.of(komponente).collect(Collectors.toSet()));
        komponente.setSeTeam(seTeam);
        modul.addSeTeam(seTeam);
        return modul;
    }

    public static Modul generateFullModulWithSuffix(String suffix) {
        Modul modul = new Modul(appendSuffixToStringName("modulName", suffix), appendSuffixToStringName("Fachbereich", suffix), appendSuffixToStringName("beschreibung", suffix));
        ModulKomponente komponente = new ModulKomponente(appendSuffixToStringName("ppg", suffix), appendSuffixToStringName("komponente", suffix));
        ModulSeTeam seTeam = new ModulSeTeam(appendSuffixToStringName("seTeam", suffix), modul,
                Stream.of(komponente).collect(Collectors.toSet()));
        komponente.setSeTeam(seTeam);
        modul.addSeTeam(seTeam);
        return modul;
    }

    private static String appendSuffixToStringName(String varName, String suffix) {
        suffix = (suffix == null) ? "" : suffix;
        return varName + "_" + suffix;
    }

    public static Modul generateModul() {
        Modul modul = new Modul("Name");
        modul.setId(1L);
        return modul;
    }

    public static Modul generateModul(String name, String fachbereich, String beschreibung) {
        Modul modul = new Modul();
        modul.setName(name);
        modul.setFachBereich(fachbereich);
        modul.setBeschreibung(beschreibung);
        return modul;
    }

    public static Modul generateModul(Long id, String name, String fachbereich, String beschreibung) {
        Modul modul = new Modul();
        modul.setId(id);
        modul.setName(name);
        modul.setFachBereich(fachbereich);
        modul.setBeschreibung(beschreibung);
        return modul;
    }

    public static List<ModulKomponente> generateModulKomponenten() {
        List<ModulKomponente> result = new ArrayList<>();
        result.add(generateModulKomponente());
        return result;
    }

    public static ModulKomponente generateModulKomponente() {
        return new ModulKomponente("PPG", "Name");
    }

    public static ModulKomponente generateModulKomponente(Long id) {
        ModulKomponente modulKomponente = new ModulKomponente("PPG", "Name");
        modulKomponente.setId(id);
        return modulKomponente;
    }

    public static ModulKomponente generateModulKomponenteWithSuffix(String suffix) {
        suffix = (suffix == null) ? "" : suffix;
        return new ModulKomponente(appendSuffixToStringName("ppg", suffix), appendSuffixToStringName("komponente", suffix));
    }

    public static List<ModulSeTeam> generateModulSeTeams(Modul modul, List<ModulKomponente> modulKomponenten) {
        List<ModulSeTeam> result = new ArrayList<>();
        result.add(generateModulSeTeam(modul, modulKomponenten));
        return result;
    }

    public static ModulSeTeam generateModulSeTeam(Long id, Modul modul, List<ModulKomponente> modulKomponenten) {
        ModulSeTeam modulSeTeam = new ModulSeTeam("SE-TEAM", modul, new HashSet<>(modulKomponenten));
        modulSeTeam.setId(id);
        return modulSeTeam;
    }

    public static ModulSeTeam generateModulSeTeam(Modul modul, List<ModulKomponente> modulKomponenten) {
        ModulSeTeam modulSeTeam = new ModulSeTeam("SE-TEAM", modul, new HashSet<>(modulKomponenten));
        return modulSeTeam;
    }

    public static List<AnforderungFreigabeBeiModulSeTeam> generateAnforderungFreigabeBeiModulSeTeams(Anforderung anforderung, List<ModulSeTeam> modulSeTeams) {
        List<AnforderungFreigabeBeiModulSeTeam> result = new ArrayList<>();
        modulSeTeams.forEach(se -> result.add(generateAnforderungFreigabeBeiModulSeTeam(anforderung, se)));
        return result;
    }

    public static List<AnforderungFreigabeBeiModulSeTeam> generateAnforderungFreigabeBeiModulSeTeamsWithMitarbeiter(Anforderung anforderung, List<ModulSeTeam> modulSeTeams, Mitarbeiter mitarbeiter) {
        List<AnforderungFreigabeBeiModulSeTeam> result = new ArrayList<>();
        modulSeTeams.forEach(se -> result.add(generateAnforderungFreigabeBeiModulSeTeam(anforderung, se, mitarbeiter)));
        return result;
    }

    public static AnforderungFreigabeBeiModulSeTeam generateAnforderungFreigabeBeiModulSeTeam(Anforderung anforderung, ModulSeTeam modulSeTeam) {
        return generateAnforderungFreigabeBeiModulSeTeam(anforderung, modulSeTeam, generateMitarbeiter("Bear", "Beiter", "IF-2"));
    }

    public static AnforderungFreigabeBeiModulSeTeam generateAnforderungFreigabeBeiModulSeTeam(Anforderung anforderung, ModulSeTeam modulSeTeam, Mitarbeiter mitarbeiter) {
        AnforderungFreigabeBeiModulSeTeam anforderungFreigabe = new AnforderungFreigabeBeiModulSeTeam(anforderung, modulSeTeam);
        anforderungFreigabe.setKommentar("Kommentar");
        anforderungFreigabe.setBearbeiter(mitarbeiter);
        anforderungFreigabe.setDatum(new Date());
        return anforderungFreigabe;
    }

    public static SensorCoc generateSensorCoc() {
        SensorCoc sensorCoc = new SensorCoc();
        sensorCoc.setZakEinordnung("ZAK Einordnung");
        sensorCoc.setRessort("T-Ressort");
        sensorCoc.setOrtung("Ortung");
        sensorCoc.setTechnologie("Technologie");
        sensorCoc.setSensorCoCLeiter(generateMitarbeiter("Hans", "Bauer", "IF-007"));
        return sensorCoc;
    }

    public static SensorCoc generateSensorCoc(Long id) {
        SensorCoc sensorCoc = new SensorCoc();
        sensorCoc.setSensorCocId(id);
        sensorCoc.setZakEinordnung("ZAK Einordnung");
        sensorCoc.setRessort("T-Ressort");
        sensorCoc.setOrtung("Ortung");
        sensorCoc.setTechnologie("Technologie");
        sensorCoc.setSensorCoCLeiter(generateMitarbeiter("Hans", "Bauer", "IF-007"));
        return sensorCoc;
    }

    public static List<SensorCoc> generateSensorCocs() {
        List<SensorCoc> result = new ArrayList<>();
        result.add(generateSensorCoc());
        return result;
    }

    public static Mitarbeiter generateMitarbeiter(String vorname, String nachname, String abteilung) {
        Mitarbeiter mitarbeiter = new Mitarbeiter(vorname, nachname);
        mitarbeiter.setAbteilung(abteilung);
        mitarbeiter.setQNumber("Q123456");
        return mitarbeiter;
    }

    public static Mitarbeiter generateMitarbeiter(Long id, String vorname, String nachname, String abteilung) {
        Mitarbeiter mitarbeiter = new Mitarbeiter(vorname, nachname);
        mitarbeiter.setAbteilung(abteilung);
        mitarbeiter.setQNumber("Q123456");
        mitarbeiter.setId(id);
        return mitarbeiter;
    }

    public static Mitarbeiter generateMitarbeiter(String vorname, String nachname, String abteilung, String qnumber) {
        Mitarbeiter mitarbeiter = new Mitarbeiter(vorname, nachname);
        mitarbeiter.setAbteilung(abteilung);
        mitarbeiter.setQNumber(qnumber);
        return mitarbeiter;
    }

    public static Derivat generateDerivat() {
        Derivat derivat = new Derivat("E46", "Bezeichnung");
        derivat.setId(1L);
        return derivat;
    }

    public static Derivat generateDerivat(String name, String bezeichnung, String produktlinie) {
        Derivat derivat = new Derivat(name, bezeichnung, produktlinie);
        derivat.setId(1L);
        return derivat;
    }

    public static Derivat generateDerivatWithoutId(String name, String bezeichnung, String produktlinie) {
        Derivat derivat = new Derivat(name, bezeichnung, produktlinie);
        return derivat;
    }

    public static List<Derivat> genereteDerivate() {
        return Arrays.asList(generateDerivat());
    }

    public static List<FestgestelltIn> generateFestgestelltInList() {
        List<FestgestelltIn> result = new ArrayList<>();
        result.add(generateFestgestelltIn());
        return result;
    }

    public static Set<FestgestelltIn> generateFestgestelltInSet() {
        Set<FestgestelltIn> result = new HashSet<>();
        result.add(generateFestgestelltIn());
        return result;
    }

    public static FestgestelltIn generateFestgestelltIn() {
        FestgestelltIn festgestelltIn = new FestgestelltIn("Wert");
        festgestelltIn.setId(1L);
        return festgestelltIn;
    }

    public static FestgestelltIn generateFestgestelltInWithoutId() {
        FestgestelltIn festgestelltIn = new FestgestelltIn("Wert");
        return festgestelltIn;
    }

    public static List<KovAPhaseImDerivat> generateKovAPhaseImDerivatList() {
        List<KovAPhaseImDerivat> returnList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            returnList.add(generateKovAPhaseImDerivat());
        }
        return returnList;
    }

    public static KovAPhaseImDerivat generateKovAPhaseImDerivat() {

        return generateKovAPhaseImDerivat(generateDerivat());
    }

    public static KovAPhaseImDerivat generateKovAPhaseImDerivat(Derivat derivat) {
        KovAPhaseImDerivat kovAPhaseImDerivat = new KovAPhaseImDerivat(derivat, KovAPhase.VABG1);

        kovAPhaseImDerivat.setStartDate(LocalDate.now());
        kovAPhaseImDerivat.setEndDate(LocalDate.now());
        return kovAPhaseImDerivat;
    }

    public static KovAPhaseImDerivat generateKovAPhaseImDerivatId(Long id) {
        KovAPhaseImDerivat kovAPhaseImDerivat = generateKovAPhaseImDerivat();
        kovAPhaseImDerivat.setId(1L);
        return kovAPhaseImDerivat;
    }

    public static List<KovAPhaseImDerivatDto> genereateKovAPhasenImDerivat() {
        List<KovAPhaseImDerivatDto> returnList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            returnList.add(genereateKovAPhaseImDerivatDto());
        }
        return returnList;
    }

    public static KovAPhaseImDerivatDto genereateKovAPhaseImDerivatDto() {
        KovAPhaseImDerivatDto dto = new KovAPhaseImDerivatDto(generateKovAPhaseImDerivat());
        dto.setBerechtigungFinished(false);
        dto.setFinished(false);
        dto.setBerechtigungProgressValue(20L);
        dto.setProgressValue(200L);
        dto.setBerechtigungProgressLabel("10 / 20");
        dto.setProgressLabel("87 / 200");
        return dto;
    }

    public static Anhang generateAnhang() {
        return new Anhang(generateMitarbeiter("An", "Hang", "IF-1"), "Test.xlsx", "xlsx", new byte[0]);
    }

    public static Anhang generateAnhangOfNullContent() {
        Anhang anhang = generateAnhang();
        anhang.setContent(null);
        return anhang;
    }

    public static ZuordnungAnforderungDerivat generateZuordnungAnforderungDerivat() {
        return new ZuordnungAnforderungDerivat(generateAnforderung(), generateDerivat(), ZuordnungStatus.OFFEN);
    }

    public static ZuordnungAnforderungDerivat generateZuordnungAnforderungDerivatWithoutId(Anforderung anforderung, Derivat derivat) {
        return new ZuordnungAnforderungDerivat(anforderung, derivat, ZuordnungStatus.OFFEN);
    }

    public static ZuordnungAnforderungDerivat generateFullZuordnungAnforderungDerivat(ZuordnungStatus status, Boolean nachZakUebertragen, Boolean zakUebertragungErfolgreich, Boolean zurKentnisGenommen) {
        ZuordnungAnforderungDerivat zuordnungAnforderungDerivat = new ZuordnungAnforderungDerivat(generateAnforderung(), generateDerivat(), status);
        zuordnungAnforderungDerivat.setNachZakUebertragen(nachZakUebertragen);
        zuordnungAnforderungDerivat.setZakUebertragungErfolgreich(zakUebertragungErfolgreich);

        zuordnungAnforderungDerivat.setZurKenntnisGenommen(zurKentnisGenommen);
        return zuordnungAnforderungDerivat;
    }

    public static List<String> generateAuswirkungList() {
        return Stream.of("Auswirkung 1", "Auswirkung 2").collect(Collectors.toList());
    }

    public static List<String> generateProduktlinien() {
        return Stream.of("LG").collect(Collectors.toList());
    }

    public static List<InfoText> generateInfotext() {
        return Stream.of(new InfoText(
                "anfoAuswirkInput",
                "Auswirkungen",
                "InfoText für Auswirkungen")).collect(Collectors.toList());
    }

    public static List<Tteam> generateTteams() {
        return Arrays.asList(generateTteam("tteam1"));
    }

    public static DerivatAnforderungModul generateDerivatAnforderungModulWithoutId(Modul modul, ZuordnungAnforderungDerivat zuordnungAnforderungDerivat, Mitarbeiter mitarbeiter) {
        DerivatAnforderungModul derivatAnforderungModul = new DerivatAnforderungModul();
        derivatAnforderungModul.setAnforderer(mitarbeiter);
        derivatAnforderungModul.setKovAPhase(KovAPhase.VABG1);
        derivatAnforderungModul.setModul(modul);
        derivatAnforderungModul.setStatusVKBG(DerivatAnforderungModulStatus.ABZUSTIMMEN);
        derivatAnforderungModul.setStatusZV(DerivatAnforderungModulStatus.ABZUSTIMMEN);
        derivatAnforderungModul.setUmsetzungsBestaetigungStatus(UmsetzungsBestaetigungStatus.UMGESETZT);
        derivatAnforderungModul.setVereinbarung(generateZakUebertragungWithoutId(derivatAnforderungModul, new Date(), ZakStatus.EMPTY));
        derivatAnforderungModul.setZakStatus(ZakStatus.DONE);
        derivatAnforderungModul.setZuordnungAnforderungDerivat(zuordnungAnforderungDerivat);
        derivatAnforderungModul.setLatestHistoryEntry(new DerivatAnforderungModulHistory());
        return derivatAnforderungModul;

    }

    public static DerivatAnforderungModul generateDerivatAnforderungModul() {
        DerivatAnforderungModul derivatAnforderungModul = new DerivatAnforderungModul();
        derivatAnforderungModul.setAnforderer(createMitarbeiter("test"));
        derivatAnforderungModul.setId(1L);
        derivatAnforderungModul.setKovAPhase(KovAPhase.VABG1);
        derivatAnforderungModul.setModul(generateModul());
        derivatAnforderungModul.setStatusVKBG(DerivatAnforderungModulStatus.ABZUSTIMMEN);
        derivatAnforderungModul.setStatusZV(DerivatAnforderungModulStatus.ABZUSTIMMEN);
        derivatAnforderungModul.setUmsetzungsBestaetigungStatus(UmsetzungsBestaetigungStatus.UMGESETZT);
        derivatAnforderungModul.setVereinbarung(generateZakUebertragung(generateAnforderung(), generateDerivat(), generateModul(), new Date()));
        derivatAnforderungModul.setZakStatus(ZakStatus.DONE);
        derivatAnforderungModul.setZuordnungAnforderungDerivat(generateZuordnungAnforderungDerivat());
        derivatAnforderungModul.setLatestHistoryEntry(new DerivatAnforderungModulHistory());
        return derivatAnforderungModul;
    }

    public static Umsetzungsbestaetigung generateUmsetzungsbestaetigung() {
        Umsetzungsbestaetigung umsetzungsbestaetigung = new Umsetzungsbestaetigung(generateAnforderungWithFachIdAndVersion(3L, "A50", 1), generateKovAPhaseImDerivat(), generateFullModul());
        umsetzungsbestaetigung.setDatum(new Date());
        umsetzungsbestaetigung.setId(1L);
        umsetzungsbestaetigung.setKommentar("Kommentar");
        umsetzungsbestaetigung.setStatus(UmsetzungsBestaetigungStatus.OFFEN);
        umsetzungsbestaetigung.setUmsetzungsbestaetiger(createMitarbeiter("Max"));
        return umsetzungsbestaetigung;
    }

    public static Optional<Prozessbaukasten> generateOptionalProzessbaukasten() {
        return Optional.of(generateProzessbaukasten());
    }

    public static Prozessbaukasten generateProzessbaukasten() {
        return generateProzessbaukaten("P1234", 1);
    }

    public static Prozessbaukasten generateProzessbaukaten(String fachId, Integer version) {
        Prozessbaukasten prozessbaukasten = new Prozessbaukasten(fachId, version);
        prozessbaukasten.setAenderungsdatum(new Date());
        List<Anforderung> anforderungList = new ArrayList<>();
        anforderungList.add(generateAnforderung());
        prozessbaukasten.setAnforderungen(anforderungList);
        prozessbaukasten.setBeschreibung("Test Beschreibung");
        prozessbaukasten.setBezeichnung("Test Bezeichnung");
        prozessbaukasten.setErstellungsdatum(new Date());
        prozessbaukasten.setId(1L);
        prozessbaukasten.setLeadTechnologie(generateMitarbeiter("Test", "Name", "F42"));
        prozessbaukasten.setStandardisierterFertigungsprozess("Fertigungsprozess");
        prozessbaukasten.setStatus(ProzessbaukastenStatus.ANGELEGT);
        prozessbaukasten.setTteam(generateTteam());
        prozessbaukasten.setErstanlaeufer(generateDerivat());
        return prozessbaukasten;
    }

    public static List<Prozessbaukasten> generateListOfProzessbaukaesten() {
        List<Prozessbaukasten> listProzessbaukasten = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            listProzessbaukasten.add(generateProzessbaukaten("P200" + i, 1));
        }
        return listProzessbaukasten;
    }

    public static ProzessbaukastenZuordnenDialogViewData generateProzessbaukastenZuordnenDialogViewData() {
        ProzessbaukastenZuordnenDialogViewData prozessbaukastenZuordnenDialogViewData = new ProzessbaukastenZuordnenDialogViewData();

        List<ProzessbaukastenZuordnenDTO> zuortenbareProzessbaukaesten = generateListOfProzessbaukaesten().stream()
                .map(prozessbaukasten -> new ProzessbaukastenZuordnenDTO(prozessbaukasten.getId(), prozessbaukasten.getFachId(), prozessbaukasten.getVersion(),
                        prozessbaukasten.getBezeichnung(), prozessbaukasten.getBeschreibung())).collect(Collectors.toList());
        prozessbaukastenZuordnenDialogViewData.setProzessbaukastenChoosable(zuortenbareProzessbaukaesten);

        return prozessbaukastenZuordnenDialogViewData;
    }

    //generiert nicht leeren Anhang aus Datei zum Testen in nahe zu echten Umgebungen
    public Anhang createAnhangFromFile(String filename) {
        Mitarbeiter mitarbeiter = generateMitarbeiter("Max", "Admin", "Abt.-2");
        Anhang anh = null;
        String relativePath = "attachments/" + filename;
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(relativePath).getFile());
            LOG.info(file.getAbsolutePath());
            byte[] fileByteArray = Files.readAllBytes(file.toPath());
            LOG.info("Attachment {0} size is {1} bytes", new Object[]{filename, fileByteArray.length});
            String flname = filename.substring(0, filename.indexOf("."));
            String flextension = filename.substring(filename.indexOf(".") + 1);
            DbFile dbf = new DbFile(flname, flextension, fileByteArray);
            anh = new Anhang(mitarbeiter, filename, flextension, dbf);

        } catch (IOException ex) {
            LOG.error(null, ex);
        }

        return anh;
    }

    public static Konzept generateKonzept(String bezeichnung, String beschreibung) {
        Konzept konzept = new Konzept();
        konzept.setId(1L);
        konzept.setBezeichnung(bezeichnung);
        konzept.setBeschreibung(beschreibung);
        return konzept;
    }

    public Konzept generateKonzeptWithAnhang(String bezeichnung, String beschreibung, String filename) {
        Anhang anhang = createAnhangFromFile(filename);
        byte[] content = anhang.getContent().getContent();
        Bild bild = new Bild(content, content, content);

        Konzept konzept = new Konzept(bezeichnung, beschreibung, anhang);
        konzept.setBild(bild);
        return konzept;
    }

    public static UserPermissions<BerechtigungDto> generateUserPermissions() {

        List<BerechtigungDto> berechtigungen = new ArrayList<>();
        SensorCoc sensorCoc = generateSensorCoc();
        BerechtigungEditDtoImpl sensorLesendBerechtigung = new BerechtigungEditDtoImpl(
                sensorCoc.getSensorCocId(), sensorCoc.pathToStringShort(),
                BerechtigungZiel.SENSOR_COC, Rechttype.LESERECHT, Rolle.SENSOR);
        BerechtigungEditDtoImpl sensorSchreibendBerechtigung = new BerechtigungEditDtoImpl(
                sensorCoc.getSensorCocId(), sensorCoc.pathToStringShort(),
                BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SENSOR);
        BerechtigungEditDtoImpl sensorEingeschraenktBerechtigung = new BerechtigungEditDtoImpl(
                sensorCoc.getSensorCocId(), sensorCoc.pathToStringShort(),
                BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SENSOR_EINGESCHRAENKT);
        BerechtigungEditDtoImpl sensorCocLeiterSchreibBerechtigung = new BerechtigungEditDtoImpl(
                sensorCoc.getSensorCocId(), sensorCoc.pathToStringShort(),
                BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SENSORCOCLEITER);
        BerechtigungEditDtoImpl sensorCocLeiterLeseBerechtigung = new BerechtigungEditDtoImpl(
                sensorCoc.getSensorCocId(), sensorCoc.pathToStringShort(),
                BerechtigungZiel.SENSOR_COC, Rechttype.LESERECHT, Rolle.SENSORCOCLEITER);
        BerechtigungEditDtoImpl sensorCocVertreterSchreibBerechtigung = new BerechtigungEditDtoImpl(
                sensorCoc.getSensorCocId(), sensorCoc.pathToStringShort(),
                BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SCL_VERTRETER);
        BerechtigungEditDtoImpl sensorCocVertreterLeseBerechtigung = new BerechtigungEditDtoImpl(
                sensorCoc.getSensorCocId(), sensorCoc.pathToStringShort(),
                BerechtigungZiel.SENSOR_COC, Rechttype.LESERECHT, Rolle.SCL_VERTRETER);
        EditPermission adminEditPermission = RolePermission.builder()
                .authorizeWriteForRole(Rolle.ADMIN).compareTo(new HashSet<>(Rolle.getAllRolles())).get();
        BerechtigungEditDtoImpl zakeinordnung = new BerechtigungEditDtoImpl(null, "AK Direkt / AG: Herstellbarkeit E/E",
                BerechtigungZiel.ZAK_EINORDNUNG, Rechttype.SCHREIBRECHT,
                Rolle.ANFORDERER, adminEditPermission);
        Tteam tteam = generateTteam();
        BerechtigungEditDtoImpl tteamBerechtigung = new BerechtigungEditDtoImpl(tteam.getId(), tteam.getTeamName(),
                BerechtigungZiel.TTEAM, Rechttype.SCHREIBRECHT, Rolle.T_TEAMLEITER);
        Modul modul = generateFullModul();
        List<ModulKomponente> modulKomponenten = generateModulKomponenten();
        ModulSeTeam modulSeTeam = generateModulSeTeam(modul, modulKomponenten);

        BerechtigungEditDtoImpl modulSeTeamBerechtigung = new BerechtigungEditDtoImpl(modulSeTeam.getId(), modulSeTeam.getName(),
                BerechtigungZiel.MODULSETEAM, Rechttype.SCHREIBRECHT, Rolle.E_COC);
        Derivat derivat = generateDerivat();
        BerechtigungEditDtoImpl derivatBerechtigung = new BerechtigungEditDtoImpl(derivat.getId(), derivat.getName(),
                BerechtigungZiel.DERIVAT, Rechttype.SCHREIBRECHT, Rolle.ANFORDERER);
        BerechtigungEditDtoImpl adminBerechtigung = new BerechtigungEditDtoImpl(null, "Admin", BerechtigungZiel.SYSTEM,
                Rechttype.KEIN, Rolle.ADMIN);

        berechtigungen.add(sensorLesendBerechtigung);
        berechtigungen.add(sensorSchreibendBerechtigung);
        berechtigungen.add(sensorEingeschraenktBerechtigung);
        berechtigungen.add(sensorCocLeiterSchreibBerechtigung);
        berechtigungen.add(sensorCocLeiterLeseBerechtigung);
        berechtigungen.add(sensorCocVertreterSchreibBerechtigung);
        berechtigungen.add(sensorCocVertreterLeseBerechtigung);
        berechtigungen.add(zakeinordnung);
        berechtigungen.add(tteamBerechtigung);
        berechtigungen.add(modulSeTeamBerechtigung);
        berechtigungen.add(derivatBerechtigung);
        berechtigungen.add(adminBerechtigung);

        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(Arrays.asList(1L), Arrays.asList(1L));
        TteamVertreterBerechtigung tteamVertreterBerechtigung = new TteamVertreterBerechtigung(Arrays.asList(1L));

        return new UserPermissionsDTO<>(Rolle.getAllRolles(), berechtigungen,
                tteamMitgliedBerechtigung, tteamVertreterBerechtigung);

    }

    public static Optional<Mitarbeiter> generateOptionalTteamLeiter() {

        return Optional.of(generateMitarbeiter("Max", "T-TeamLeiter", "AF"));
    }

    public static List<UmsetzungsbestaetigungDto> generateUmsetzungsBestaetigungDtoList() {
        List<UmsetzungsbestaetigungDto> returnList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            returnList.add(generateUmsetzungsbestaetigungDto());
        }
        return returnList;
    }

    public static UmsetzungsbestaetigungDto generateUmsetzungsbestaetigungDto() {
        UmsetzungsbestaetigungDto dto = UmsetzungsbestaetigungDto.buildUmsetzungsbestaetigungDtoForDerivatAnforderungModul(generateDerivatAnforderungModul(), UmsetzungsBestaetigungStatus.UMGESETZT, "bearbeitet", "Hans Markus");
        dto.setId(1L);
        return dto;
    }

    public static Umsetzungsbestaetigung generateUmsetyungsbestaetigung() {
        Umsetzungsbestaetigung umsetzungsbestaetigung = new Umsetzungsbestaetigung(generateAnforderung(), generateKovAPhaseImDerivat(), generateFullModul());
        umsetzungsbestaetigung.setKommentar("Kommentar");
        umsetzungsbestaetigung.setId(1L);
        umsetzungsbestaetigung.setUmsetzungsbestaetiger(createMitarbeiter("Test"));
        umsetzungsbestaetigung.setDatum(new Date());
        return umsetzungsbestaetigung;
    }

    public static ZakUpdateDto generateZakUpdateDto() {
        ZakUpdateDto zakUpdateDto = new ZakUpdateDto(generateDerivat(), "ZakUrl", ZakUpdateDto.Status.OK, "Status Message");
        return zakUpdateDto;
    }

    public static List<Long> generateLongList() {
        List<Long> returnList = new ArrayList<>();
        for (Integer i = 0; i < 20; i++) {
            returnList.add(i.longValue());
        }
        return returnList;
    }

    public static MeldungHistoryMigrationDto generateMeldungHistoryMigrationDtoForCreation(Long meldungId) {
        return new MeldungHistoryMigrationDto(meldungId, new Date(), "Meldung", "", "neu angelegt");
    }

    public static MeldungHistoryMigrationDto generateMeldungHistoryMigrationDtoForStatusChange(Long meldungId, Status von, Status auf) {
        return new MeldungHistoryMigrationDto(meldungId, new Date(), "Status", von.getStatusBezeichnung(), auf.getStatusBezeichnung());
    }

    public static MeldungHistoryMigrationDto generateMeldungHistoryMigrationDtoForAddToNewAnforderung(Long meldungId) {
        return new MeldungHistoryMigrationDto(meldungId, new Date(), "Status", "gemeldet", "zugeordnet");
    }

    public static MeldungHistoryMigrationDto generateMeldungHistoryMigrationDtoForAddToExistingAnforderung(Long meldungId) {
        return new MeldungHistoryMigrationDto(meldungId, new Date(), "Kommentar zum Statuswechsel", "", "wurde der bestehenden Anforderung");
    }

    public static MeldungHistoryMigrationDto generateMeldungHistoryMigrationDtoForRemoveFromAnforderung(Long meldungId) {
        return new MeldungHistoryMigrationDto(meldungId, new Date(), "Status", "zugeordnet", "gemeldet");
    }

    public static AnforderungHistoryMigrationDto generateAnforderungHistoryMigrationDtoForCreation(Long anforderungId) {
        return new AnforderungHistoryMigrationDto(anforderungId, new Date(), "Anforderung", "", "neu angelegt");
    }

    public static AnforderungHistoryMigrationDto generateAnforderungHistoryMigrationDtoForStatusChange(Long anforderungId, Status von, Status auf) {
        return new AnforderungHistoryMigrationDto(anforderungId, new Date(), "Status", von.getStatusBezeichnung(), auf.getStatusBezeichnung());
    }

    public static AnforderungHistoryMigrationDto generateAnforderungHistoryMigrationDtoForAddToProzessbaukasten(Long anforderungId) {
        return new AnforderungHistoryMigrationDto(anforderungId, new Date(), "Zuordnung Prozessbaukasten", "zugeordnet", "");
    }

    public static AnforderungHistoryMigrationDto generateAnforderungHistoryMigrationDtoForRemoveFromProzessbaukasten(Long anforderungId) {
        return new AnforderungHistoryMigrationDto(anforderungId, new Date(), "Zuordnung Prozessbaukasten", "", "Zuordnung entfernt");
    }

    public static AnforderungHistoryMigrationDto generateAnforderungHistoryMigrationDtoForNewVersion(Long anforderungId) {
        return new AnforderungHistoryMigrationDto(anforderungId, new Date(), "neue Version von Anforderung", "", "");
    }

    public static List<AnforderungHistoryMigrationDto> generateAnforderungHistoryMigrationDtosForAnforderungAllStatus(Long anforderungId) {
        List<AnforderungHistoryMigrationDto> returnList = new ArrayList<>();
        returnList.add(generateAnforderungHistoryMigrationDtoForCreation(anforderungId));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_INARBEIT, Status.A_FTABGESTIMMT));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_FTABGESTIMMT, Status.A_UNSTIMMIG));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_UNSTIMMIG, Status.A_FTABGESTIMMT));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_FTABGESTIMMT, Status.A_PLAUSIB));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_PLAUSIB, Status.A_UNSTIMMIG));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_UNSTIMMIG, Status.A_FTABGESTIMMT));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_FTABGESTIMMT, Status.A_PLAUSIB));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_PLAUSIB, Status.A_GELOESCHT));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_GELOESCHT, Status.A_INARBEIT));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_INARBEIT, Status.A_FTABGESTIMMT));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_FTABGESTIMMT, Status.A_GELOESCHT));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_GELOESCHT, Status.A_INARBEIT));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_INARBEIT, Status.A_GELOESCHT));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_GELOESCHT, Status.A_INARBEIT));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_INARBEIT, Status.A_FTABGESTIMMT));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_FTABGESTIMMT, Status.A_PLAUSIB));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_PLAUSIB, Status.A_FREIGEGEBEN));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_FREIGEGEBEN, Status.A_KEINE_WEITERVERFOLG));
        returnList.add(generateAnforderungHistoryMigrationDtoForStatusChange(anforderungId, Status.A_KEINE_WEITERVERFOLG, Status.A_GELOESCHT));
        return returnList;
    }

    public static List<MeldungHistoryMigrationDto> generateMeldungHistoryMigrationDtosForMeldungAllStatus(Long meldungId) {
        List<MeldungHistoryMigrationDto> returnList = new ArrayList<>();
        returnList.add(generateMeldungHistoryMigrationDtoForCreation(meldungId));
        returnList.add(generateMeldungHistoryMigrationDtoForStatusChange(meldungId, Status.M_ENTWURF, Status.M_GEMELDET));
        returnList.add(generateMeldungHistoryMigrationDtoForStatusChange(meldungId, Status.M_GEMELDET, Status.M_UNSTIMMIG));
        returnList.add(generateMeldungHistoryMigrationDtoForStatusChange(meldungId, Status.M_UNSTIMMIG, Status.M_GELOESCHT));
        returnList.add(generateMeldungHistoryMigrationDtoForStatusChange(meldungId, Status.M_GELOESCHT, Status.M_UNSTIMMIG));
        returnList.add(generateMeldungHistoryMigrationDtoForStatusChange(meldungId, Status.M_UNSTIMMIG, Status.M_GEMELDET));
        returnList.add(generateMeldungHistoryMigrationDtoForStatusChange(meldungId, Status.M_GEMELDET, Status.M_GELOESCHT));
        returnList.add(generateMeldungHistoryMigrationDtoForStatusChange(meldungId, Status.M_GELOESCHT, Status.M_GEMELDET));
        return returnList;
    }

    public static List<AnforderungHistoryMigrationDto> generateAnforderungHistoryMigrationDtosForAnforderungWithProzessbaukasten(Long anforderungId) {
        List<AnforderungHistoryMigrationDto> returnList = new ArrayList<>();
        returnList.add(generateAnforderungHistoryMigrationDtoForCreation(anforderungId));
        returnList.add(generateAnforderungHistoryMigrationDtoForAddToProzessbaukasten(anforderungId));
        returnList.add(generateAnforderungHistoryMigrationDtoForRemoveFromProzessbaukasten(anforderungId));

        return returnList;
    }

    public static FachteamUnstimmigKeyFigure generateFachteamUnstimmigKeyFigure() throws InvalidDataException {
        return FachteamUnstimmigKeyFigure.buildFromKeyFigures(generateTteamUnstimmigKeyFigure(), generateVereinbarungUnstimmigKeyFigure());
    }

    public static TteamUnstimmigKeyFigure generateTteamUnstimmigKeyFigure() {
        return new TteamUnstimmigKeyFigure(Arrays.asList(1L));
    }

    public static FachteamInputKeyFigure generateFachteamInputKeyFigure() throws InvalidDataException {
        return FachteamInputKeyFigure.buildFromKeyFigures(generateFachteamUnstimmigKeyFigure(), generateFachteamAnforderungNewVersionKeyFigure(), generateFachteamGemeldeteMeldungenKeyFigure());
    }

    public static FachteamGemeldeteMeldungenKeyFigure generateFachteamGemeldeteMeldungenKeyFigure() {
        return new FachteamGemeldeteMeldungenKeyFigure(Arrays.asList(1L));
    }

    public static FachteamAnforderungNewVersionKeyFigure generateFachteamAnforderungNewVersionKeyFigure() {
        return new FachteamAnforderungNewVersionKeyFigure(Arrays.asList(1L), 1L);
    }

    public static FachteamOutputKeyFigure generateFachteamOutputKeyFigure() throws InvalidDataException {
        return FachteamOutputKeyFigure.buildFromKeyFigures(generateFachteamAnforderungAbgestimmtKeyFigure(), generateFachteamGeloeschteObjekteKeyFigure(), generateFachteamProzessbaukastenKeyFigure(), generateFachteamMeldungExistingAnforderungKeyFigure());
    }

    public static FachteamProzessbaukastenKeyFigure generateFachteamProzessbaukastenKeyFigure() {
        return new FachteamProzessbaukastenKeyFigure(Arrays.asList(1L));
    }

    public static FachteamMeldungExistingAnforderungKeyFigure generateFachteamMeldungExistingAnforderungKeyFigure() {
        return new FachteamMeldungExistingAnforderungKeyFigure(Arrays.asList(1L));
    }

    public static FachteamGeloeschteObjekteKeyFigure generateFachteamGeloeschteObjekteKeyFigure() {
        return new FachteamGeloeschteObjekteKeyFigure(Arrays.asList(1L), Arrays.asList(1L));
    }

    public static FachteamAnforderungAbgestimmtKeyFigure generateFachteamAnforderungAbgestimmtKeyFigure() {
        return new FachteamAnforderungAbgestimmtKeyFigure(Arrays.asList(1L));
    }

    public static TteamOutputKeyFigure generateTteamOutputKeyFigure() throws InvalidDataException {
        return TteamOutputKeyFigure.buildFromKeyFigures(generateTteamPlausibilisiertKeyFigure(), generateTteamGeloeschtKeyFigure(), generateTteamUnstimmigKeyFigure(), generateTteamProzessbaukastenKeyFigure());
    }

    public static TteamProzessbaukastenKeyFigure generateTteamProzessbaukastenKeyFigure() {
        return new TteamProzessbaukastenKeyFigure(Arrays.asList(1L));
    }

    public static TteamGeloeschtKeyFigure generateTteamGeloeschtKeyFigure() {
        return new TteamGeloeschtKeyFigure(Arrays.asList(1L));
    }

    public static TteamPlausibilisiertKeyFigure generateTteamPlausibilisiertKeyFigure() {
        return new TteamPlausibilisiertKeyFigure(Arrays.asList(1L));
    }

    public static VereinbarungOutputKeyFigure generateVereinbarungOutputKeyFigure() throws InvalidDataException {
        return VereinbarungOutputKeyFigure.buildFromKeyFigures(generateVereinbarungFreigegebenKeyFigure(), generateVereinbarungGeloeschtKeyFigure(), generateVereinbarungUnstimmigKeyFigure(), generateVereinbarungProzessbaukastenKeyFigure());
    }

    public static VereinbarungProzessbaukastenKeyFigure generateVereinbarungProzessbaukastenKeyFigure() {
        return new VereinbarungProzessbaukastenKeyFigure(Arrays.asList(1L));
    }

    public static VereinbarungUnstimmigKeyFigure generateVereinbarungUnstimmigKeyFigure() {
        return new VereinbarungUnstimmigKeyFigure(Arrays.asList(1L));
    }

    public static VereinbarungGeloeschtKeyFigure generateVereinbarungGeloeschtKeyFigure() {
        return new VereinbarungGeloeschtKeyFigure(Arrays.asList(1L));
    }

    public static VereinbarungFreigegebenKeyFigure generateVereinbarungFreigegebenKeyFigure() {
        return new VereinbarungFreigegebenKeyFigure(Arrays.asList(1L));
    }

    public static AnforderungHistory generateAnforderungHistory(Long id, Long anforderungId, String auf, String von, String benutzer, Date date, String kennzeichen, String objektName) {

        AnforderungHistory anforderungHistory = new AnforderungHistory();
        anforderungHistory.setAnforderungId(anforderungId);
        anforderungHistory.setAuf(auf);
        anforderungHistory.setVon(von);
        anforderungHistory.setBenutzer(benutzer);
        anforderungHistory.setDatum(date);
        anforderungHistory.setId(id);
        anforderungHistory.setKennzeichen(kennzeichen);
        anforderungHistory.setObjektName(objektName);

        return anforderungHistory;

    }

    public static ProzessbaukastenAnforderungKonzept generateProzessbaukastenAnforderungKonzept() {
        ProzessbaukastenAnforderungKonzept prozessbaukastenAnforderungKonzept = new ProzessbaukastenAnforderungKonzept();
        prozessbaukastenAnforderungKonzept.setId(1L);
        prozessbaukastenAnforderungKonzept.setKonzept(generateKonzept("Test", "Test"));
        prozessbaukastenAnforderungKonzept.setAnforderung(generateAnforderung());
        prozessbaukastenAnforderungKonzept.setProzessbaukasten(generateProzessbaukasten());
        return prozessbaukastenAnforderungKonzept;
    }

    public static List<ProzessbaukastenAnforderung> generateProzessbaukastenAnforderung() {
        List<ProzessbaukastenAnforderung> prozessbaukastenAnforderung = AnforderungenTabDTO.forAnforderungen(Arrays.asList(generateAnforderung()), Arrays.asList(generateProzessbaukastenAnforderungKonzept()), generateProzessbaukasten());
        return prozessbaukastenAnforderung;
    }

    public static ProzessbaukastenAnforderungenViewData generateProzessbaukastenAnforderungenViewData() {
        ProzessbaukastenAnforderungenViewData anforderungenViewData = ProzessbaukastenAnforderungenViewData.getBuilder()
                .withAnforderungen(generateProzessbaukastenAnforderung())
                .withFilteredAnforderungen(generateProzessbaukastenAnforderung())
                .build();
        return anforderungenViewData;

    }

    public static ProzessbaukastenAnhangTabViewData generateProzessbaukastenAnhangTabViewData() {
        ProzessbaukastenAnhangTabViewData tabViewData = ProzessbaukastenAnhangTabViewData.newBuilder()
                .withBeauftragungTPK(new Anhang())
                .withGenehmigungTPK(new Anhang())
                .withGrafikUmfang(new Anhang())
                .withKonzeptbaum(new Anhang())
                .withStartbrief(new Anhang())
                .withWeitereAnhaenge(Arrays.asList(new Anhang()))
                .build();

        return tabViewData;
    }

    public static AnforderungenProzessbaukastenZuordnenDTO generateAnforderungenProzessbaukastenZuordnenDTO() {
        AnforderungenProzessbaukastenZuordnenDTO anforderungenProzessbaukastenZuordnenDTO = new AnforderungenProzessbaukastenZuordnenDTO("A1234", 1, "Beschreibung");
        return anforderungenProzessbaukastenZuordnenDTO;
    }

    public static AnforderungenProzessbaukastenZuordnenViewData generateAnforderungenProzessbaukastenZuordnenViewData() {
        AnforderungenProzessbaukastenZuordnenViewData anforderungenProzessbaukastenZuordnenViewData = new AnforderungenProzessbaukastenZuordnenViewData();
        anforderungenProzessbaukastenZuordnenViewData.setZugeordneteAnforderungen(Arrays.asList(generateAnforderungenProzessbaukastenZuordnenDTO()));
        return anforderungenProzessbaukastenZuordnenViewData;
    }

    public static KonzeptDto generateKonzeptDto() {
        KonzeptDto konzeptDto = new KonzeptDto(1L, "Bezeichnung", "Beschreibung");
        return konzeptDto;
    }

    public static ProzessbaukastenStammdatenViewData generateProzessbaukastenStammdatenViewData() {
        ProzessbaukastenStammdatenViewData prozessbaukastenStammdatenViewData = ProzessbaukastenStammdatenViewData.getBuilder()
                .withBeschreibung("Beschreibung")
                .withErstanlaeufer("Erstanlaufer")
                .withLeadTechnologie("Lead Technologie")
                .withStandardisierterFertigungsprozess("StandardisierterFertigungsprozess")
                .withTteam("Tteam")
                .withTteamleiter("TteamLeiter")
                .withKonzepte(Arrays.asList(generateKonzeptDto()))
                .build();
        return prozessbaukastenStammdatenViewData;
    }

    public static TteamMitgliedBerechtigung generateTteamMitgliedBerechtigung() {
        TteamMitgliedBerechtigung tteamMitgliedBerechtigung = new TteamMitgliedBerechtigung(generateLongList(), generateLongList());
        return tteamMitgliedBerechtigung;
    }

    public static ProzessbaukastenViewPermission generateProzessbaukastenViewPermission() {
        ProzessbaukastenViewPermission prozessbaukastenViewPermission = ProzessbaukastenViewPermission.forProzessbaukasten(generateProzessbaukasten())
                .forTteamIdsAsTteamleiter(generateLongList())
                .forTteamIdsAsTteamVertreter(generateLongList())
                .forTteamMitgliedBerechtigung(generateTteamMitgliedBerechtigung())
                .forUserRoles(new HashSet<>(Rolle.getAllRolles()))
                .build();

        return prozessbaukastenViewPermission;
    }

    public static ProzessbaukastenViewData generateProzessbaukastenViewData() {
        ProzessbaukastenViewData viewData = ProzessbaukastenViewData.getBuilder()
                .withAnforderungenTabViewData(generateProzessbaukastenAnforderungenViewData())
                .withAnhangTabViewData(generateProzessbaukastenAnhangTabViewData())
                .withAnforderungProzessbaukastenZuordnenViewData(generateAnforderungenProzessbaukastenZuordnenViewData())
                .withBezeichnung("Bezeichnung")
                .withNaechsteStatus(Arrays.asList(ProzessbaukastenStatus.values()))
                .withStammdaten(generateProzessbaukastenStammdatenViewData())
                .withStatus(ProzessbaukastenStatus.ANGELEGT)
                .withStatusLabel("StatusLabel")
                .withViewPermission(generateProzessbaukastenViewPermission())
                .withFachId("P1234")
                .withId("1L")
                .withProzessbaukastenOrigin(Optional.of(generateProzessbaukasten()))
                .withVersion("1")
                .build();

        return viewData;

    }

    public static String generateAbteilung() {
        return "Abteilung 1";
    }

    public static List<String> generateAbteilungen() {
        return Arrays.asList(generateAbteilung());

    }

    public static Fahrzeugmerkmal generateFahrzeugmerkmal(String merkmalName) {
        Fahrzeugmerkmal fahrzeugmerkmal = new Fahrzeugmerkmal();
        fahrzeugmerkmal.setMerkmal(merkmalName);
        return fahrzeugmerkmal;
    }

    public static FahrzeugmerkmalAuspraegung generateAuspraegung(String auspraegungName) {
        FahrzeugmerkmalAuspraegung auspraegung = new FahrzeugmerkmalAuspraegung();
        auspraegung.setAuspraegung(auspraegungName);
        auspraegung.setFahrzeugmerkmal(generateFahrzeugmerkmal("Merkmal1"));
        return auspraegung;
    }

    public static List<FahrzeugmerkmalAuspraegung> generateAuspraegungen() {
        return Arrays.asList(generateAuspraegung("Auspraegung1"));
    }

    public static List<FahrzeugmerkmalAuspraegung> generateAuspraegungen(int count) {
        List<FahrzeugmerkmalAuspraegung> returnList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            returnList.add(generateAuspraegung("Auspraegung" + i));
        }

        return returnList;
    }

    public static ProzessbaukastenThemenklammer generateProzessbaukastenThemenklammer(Prozessbaukasten prozessbaukasten, Themenklammer themenklammer) {
        ProzessbaukastenThemenklammer prozessbaukastenThemenklammer = new ProzessbaukastenThemenklammer();
        prozessbaukastenThemenklammer.setProzessbaukasten(prozessbaukasten);
        prozessbaukastenThemenklammer.setThemenklammer(themenklammer);
        return prozessbaukastenThemenklammer;
    }

    public static UBzuKovaPhaseImDerivatUndSensorCoC getUBzuKovaPhaseImDerivatUndSensorCoC() {
        UBzuKovaPhaseImDerivatUndSensorCoC uBzuKovaPhaseImDerivatUndSensorCoC = new UBzuKovaPhaseImDerivatUndSensorCoC();
        uBzuKovaPhaseImDerivatUndSensorCoC.setUmsetzungsbestaetiger(Arrays.asList(generateMitarbeiter("Karl", "Marx", "Abt. 15")));
        uBzuKovaPhaseImDerivatUndSensorCoC.setKovaImDerivat(generateKovAPhaseImDerivat());
        uBzuKovaPhaseImDerivatUndSensorCoC.setSensorCoc(generateSensorCoc());

        return uBzuKovaPhaseImDerivatUndSensorCoC;
    }

    public static Berechtigungsantrag generateBerechtigungsantragForSensorCoc() {
        Berechtigungsantrag returnAntrag = generateBerechtigungsantrag();
        returnAntrag.setSensorCoc(generateSensorCoc());
        return returnAntrag;
    }

    public static Berechtigungsantrag generateBerechtigungsantragForTteam() {
        Berechtigungsantrag returnAntrag = generateBerechtigungsantrag();
        returnAntrag.setTteam(generateTteamWithoutId());
        return returnAntrag;
    }

    public static Berechtigungsantrag generateBerechtigungsantragForModulSeTeam() {
        Berechtigungsantrag returnAntrag = generateBerechtigungsantrag();
        returnAntrag.setModulSeTeam(generateModulSeTeam(generateModulWithoutId(), generateModulKomponenten()));
        return returnAntrag;
    }

    private static Berechtigungsantrag generateBerechtigungsantrag() {
        Berechtigungsantrag returnAntrag = new Berechtigungsantrag();
        returnAntrag.setRechttype(Rechttype.SCHREIBRECHT);
        returnAntrag.setRolle(Rolle.SENSOR);
        returnAntrag.setAenderungsdatum(new Date());
        returnAntrag.setBearbeiter(generateMitarbeiter("test", "test", "test", "q123456"));
        returnAntrag.setKommentar("Kommentar");
        returnAntrag.setErstellungsdatum(new Date());
        returnAntrag.setAntragsteller(generateMitarbeiter("test", "test", "test", "q123456"));
        return returnAntrag;
    }

}
