package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungEditDtoImpl;
import de.interfaceag.bmw.pzbk.dao.ZuordnungAnforderungDerivatDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.TruePermission;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungServiceDerivatZuordnungenWithBerechtigungTest {

    @Mock
    private Session session;

    @Mock
    private ZuordnungAnforderungDerivatDao zuordnungAnforderungDerivatDao;

    @Mock
    private ZuordnungAnforderungDerivat zuordnung1;

    @Mock
    private ZuordnungAnforderungDerivat zuordnung2;

    @Mock
    private ZuordnungAnforderungDerivat zuordnung3;

    @Mock
    private ZuordnungAnforderungDerivat zuordnung4;

    @Mock
    private ZuordnungAnforderungDerivat zuordnung5;

    @Mock
    private Anforderung anforderung;

    @Mock
    private Derivat derivatE20;

    @Mock
    private Derivat derivatE22;

    @Mock
    private Derivat derivatE23;

    @Mock
    private Derivat derivatE24;

    @Mock
    private Derivat derivatF12;

    @Mock
    private UserPermissions<BerechtigungDto> userPermissions;

    @InjectMocks
    private ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;

    private final List<BerechtigungDto> berechtigungen = new ArrayList<>();

    @BeforeEach
    public void setUp() {

    }

    @Test
    public void testForAdmin() {
        when(session.hasRole(Rolle.ADMIN)).thenReturn(Boolean.TRUE);
        when(zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatForAnforderung(anforderung))
                .thenReturn(Arrays.asList(zuordnung2, zuordnung3, zuordnung5, zuordnung4, zuordnung1));

        List<ZuordnungAnforderungDerivat> expected = Arrays.asList(zuordnung2, zuordnung3, zuordnung5, zuordnung4, zuordnung1);
        List<ZuordnungAnforderungDerivat> actual = zuordnungAnforderungDerivatService.getDerivatZuordnungenWithBerechtigungForAnforderung(anforderung);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testForAnforderer() {
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(session.hasRole(Rolle.ADMIN)).thenReturn(Boolean.FALSE);

        BerechtigungDto berechtigungE20 = new BerechtigungEditDtoImpl(1L, "E20", BerechtigungZiel.DERIVAT, Rechttype.SCHREIBRECHT, Rolle.ANFORDERER, new TruePermission());
        BerechtigungDto berechtigungE22 = new BerechtigungEditDtoImpl(2L, "E22", BerechtigungZiel.DERIVAT, Rechttype.SCHREIBRECHT, Rolle.ANFORDERER, new TruePermission());
        BerechtigungDto berechtigungF12 = new BerechtigungEditDtoImpl(3L, "F12", BerechtigungZiel.DERIVAT, Rechttype.SCHREIBRECHT, Rolle.ANFORDERER, new TruePermission());
        berechtigungen.add(berechtigungE20);
        berechtigungen.add(berechtigungE22);
        berechtigungen.add(berechtigungF12);
        when(userPermissions.getDerivatAsAnfordererSchreibend()).thenReturn(berechtigungen);

        when(zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatForAnforderungAndDerivatIds(anforderung, Arrays.asList(1L, 2L, 3L))).thenReturn(Arrays.asList(zuordnung2, zuordnung3, zuordnung1));

        List<ZuordnungAnforderungDerivat> expected = Arrays.asList(zuordnung2, zuordnung3, zuordnung1);
        List<ZuordnungAnforderungDerivat> actual = zuordnungAnforderungDerivatService.getDerivatZuordnungenWithBerechtigungForAnforderung(anforderung);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testForNotAuthorizedUser() {
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(session.hasRole(Rolle.ADMIN)).thenReturn(Boolean.FALSE);
        when(userPermissions.getDerivatAsAnfordererSchreibend()).thenReturn(berechtigungen);
        List<ZuordnungAnforderungDerivat> actual = zuordnungAnforderungDerivatService.getDerivatZuordnungenWithBerechtigungForAnforderung(anforderung);
        Assertions.assertTrue(actual.isEmpty());
    }

}
