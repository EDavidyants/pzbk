package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ActiveIndex;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenTab;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.ProzessbaukastenLinkData;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuModel;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;

/**
 *
 * @author evda
 */
@RunWith(MockitoJUnitRunner.class)
public class AnforderungServiceGetZugeordneteProzessbaukastenTest {

    @Spy
    private AnforderungService anforderungService;

    private List<ProzessbaukastenLinkData> zugeordneteProzessbaukasten;

    @Before
    public void setUp() {
        zugeordneteProzessbaukasten = new ArrayList<>();
    }

    @Test
    public void testEmptyModel() {
        MenuModel zugeordneteProzessbaukastenMenuItems = anforderungService.getZugeordneteProzessbaukastenMenuItems(zugeordneteProzessbaukasten);
        List<MenuElement> menuItems = zugeordneteProzessbaukastenMenuItems.getElements();
        Assertions.assertTrue(menuItems.isEmpty());
    }

    @Test
    public void testModelWithOneElementMenuItemsSize() {
        ProzessbaukastenLinkData linkData = new ProzessbaukastenLinkData("P13", 1, "Prozessbaukasten 13");
        zugeordneteProzessbaukasten.add(linkData);
        MenuModel zugeordneteProzessbaukastenMenuItems = anforderungService.getZugeordneteProzessbaukastenMenuItems(zugeordneteProzessbaukasten);
        List<MenuElement> menuItems = zugeordneteProzessbaukastenMenuItems.getElements();
        Assertions.assertEquals(1, menuItems.size());
    }

    @Test
    public void testModelWithTwoElementsMenuItemsSize() {
        ProzessbaukastenLinkData linkData = new ProzessbaukastenLinkData("P13", 1, "Prozessbaukasten 13");
        ProzessbaukastenLinkData linkData2 = new ProzessbaukastenLinkData("P13", 2, "Prozessbaukasten 13");
        zugeordneteProzessbaukasten.add(linkData);
        zugeordneteProzessbaukasten.add(linkData2);
        MenuModel zugeordneteProzessbaukastenMenuItems = anforderungService.getZugeordneteProzessbaukastenMenuItems(zugeordneteProzessbaukasten);
        List<MenuElement> menuItems = zugeordneteProzessbaukastenMenuItems.getElements();
        Assertions.assertEquals(2, menuItems.size());
    }

    @Test
    public void testMenuItemsGetUrlForProzessbaukastenCall() {
        ProzessbaukastenLinkData linkData = new ProzessbaukastenLinkData("P13", 1, "Prozessbaukasten 13");
        zugeordneteProzessbaukasten.add(linkData);
        anforderungService.getZugeordneteProzessbaukastenMenuItems(zugeordneteProzessbaukasten);

        verify(anforderungService).getUrlForProzessbaukasten(linkData);
    }

    @Test
    public void testMenuItemsUrlPage() {
        ProzessbaukastenLinkData linkData = new ProzessbaukastenLinkData("P13", 1, "Prozessbaukasten 13");
        zugeordneteProzessbaukasten.add(linkData);
        anforderungService.getZugeordneteProzessbaukastenMenuItems(zugeordneteProzessbaukasten);

        String url = anforderungService.getUrlForProzessbaukasten(linkData);
        Assertions.assertTrue(url.startsWith(Page.PROZESSBAUKASTEN_VIEW.getUrl()));
    }

    @Test
    public void testMenuItemsUrlFachId() {
        ProzessbaukastenLinkData linkData = new ProzessbaukastenLinkData("P13", 1, "Prozessbaukasten 13");
        zugeordneteProzessbaukasten.add(linkData);
        anforderungService.getZugeordneteProzessbaukastenMenuItems(zugeordneteProzessbaukasten);

        String url = anforderungService.getUrlForProzessbaukasten(linkData);
        Assertions.assertTrue(url.contains("fachId=P13"));
    }

    @Test
    public void testMenuItemsUrlVersion() {
        ProzessbaukastenLinkData linkData = new ProzessbaukastenLinkData("P13", 1, "Prozessbaukasten 13");
        zugeordneteProzessbaukasten.add(linkData);
        anforderungService.getZugeordneteProzessbaukastenMenuItems(zugeordneteProzessbaukasten);

        String url = anforderungService.getUrlForProzessbaukasten(linkData);
        Assertions.assertTrue(url.contains("version=1"));
    }

    @Test
    public void testMenuItemsUrlPageActiveIndex() {
        ProzessbaukastenLinkData linkData = new ProzessbaukastenLinkData("P13", 1, "Prozessbaukasten 13");
        zugeordneteProzessbaukasten.add(linkData);
        anforderungService.getZugeordneteProzessbaukastenMenuItems(zugeordneteProzessbaukasten);

        String url = anforderungService.getUrlForProzessbaukasten(linkData);
        Assertions.assertTrue(url.contains("activeIndex=" + ActiveIndex.forTab(ProzessbaukastenTab.STAMMDATEN).getIndex()));
    }
    
    @Test
    public void testToStringOfProzessbaukastenLinkData() {
        ProzessbaukastenLinkData linkData = new ProzessbaukastenLinkData("P13", 1, "Prozessbaukasten 13");
        String labelFact = linkData.toString();
        String labelExpected = linkData.getFachId() + " | V" + linkData.getVersion() + " : " + linkData.getBezeichnung();
        Assertions.assertEquals(labelExpected, labelFact);
    }
    
    @Test
    public void test2ToStringOfProzessbaukastenLinkData() {
        ProzessbaukastenLinkData linkData = new ProzessbaukastenLinkData("P13", 1, "Prozessbaukasten 13");
        String labelFact = linkData.toString();
        String labelExpected = "P13 | V1 : Prozessbaukasten 13";
        Assertions.assertEquals(labelExpected, labelFact);
    }

}
