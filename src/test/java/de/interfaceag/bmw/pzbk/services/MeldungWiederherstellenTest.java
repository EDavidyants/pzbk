package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.mail.MailService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingMeldungStatusTransition;
import de.interfaceag.bmw.pzbk.reporting.statustransition.meldung.MeldungReportingStatusTransitionAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class MeldungWiederherstellenTest {

    AnforderungService anforderungService;

    Mitarbeiter user;

    Meldung meldung;

    private ReportingMeldungStatusTransition reportingStatusTransition;

    @Before
    public void init() {
        anforderungService = new AnforderungService();
        anforderungService.historyService = mock(AnforderungMeldungHistoryService.class);
        anforderungService.mailService = mock(MailService.class);
        anforderungService.anforderungDao = mock(AnforderungDao.class);
        anforderungService.fileService = mock(FileService.class);
        anforderungService.changeMeldungStatusService = mock(MeldungReportingStatusTransitionAdapter.class);

        user = TestDataFactory.generateMitarbeiter("mit", "arbeiter", "IF");
        meldung = TestDataFactory.generateMeldung();

        Mockito.doNothing()
                .when(anforderungService.historyService)
                .writeHistoryForChangedValuesOfAnfoMgmtObject(any(), any(), any());

        Mockito.doNothing()
                .when(anforderungService.mailService)
                .sendMailAnforderungChanged(any(), any());

        Mockito.doNothing()
                .when(anforderungService.fileService)
                .writeAnhaengeToFile(any());

        Mockito.when(anforderungService.anforderungDao.saveAnforderung(any())).thenReturn(1L);

        Mockito.when(anforderungService.changeMeldungStatusService.changeMeldungStatus(any(), any(), any())).thenReturn(reportingStatusTransition);

    }

    @Test
    public void restoreMeldung() {
        Mockito.when(anforderungService.historyService.getLastStatusByObjectnameAnforderungId(any(), any()))
                .thenReturn(Status.M_ENTWURF);

        String result = anforderungService.restoreMeldung(meldung, user);
        Assertions.assertEquals("", result, "result not correct");
    }

    @Test
    public void restoreMeldungNullStatus() {
        Mockito.when(anforderungService.historyService.getLastStatusByObjectnameAnforderungId(any(), any()))
                .thenReturn(null);

        String result = anforderungService.restoreMeldung(meldung, user);
        Assertions.assertEquals(
                "Die Meldung kann nicht zurückgesetzt werden.",
                result, "result not correct");
    }

    @Test
    public void restoreAnforderungNull() {
        String result = anforderungService.restoreMeldung(null, null);
        Assertions.assertEquals("meldung or user is null", result);
    }

    @Test
    public void restoreMeldungStatus() {
        Status status = Status.M_GEMELDET;
        Meldung result = AnforderungService.restoreMeldungStatus(meldung, status);
        Assertions.assertEquals(status, result.getStatus(), "Meldung Status nicht identisch.");
    }

}
