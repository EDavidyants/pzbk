package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.BerechtigungDao;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author Christian
 */
@ExtendWith(MockitoExtension.class)
public class BerechtigungServiceTest {

    @Mock
    private SensorCocService sensorCocService;

    @Mock
    private BerechtigungDao berechtigungDao;

    @InjectMocks
    BerechtigungService berechtigungService;
    Mitarbeiter mitarbeiter;

    @BeforeEach
    public void setup() {
        mitarbeiter = TestDataFactory.createMitarbeiter("");

    }

    @Test
    public void testGetSensorCocForMitarbeiterAndRolleIsADMIN() {
        berechtigungService.getSensorCocForMitarbeiterAndRolle(mitarbeiter, Rolle.ADMIN, true);
        verify(sensorCocService, times(1)).getAllSortByTechnologie();
    }

    @Test
    public void getSensorCocForMitarbeiterAndRolle_mit_Rolle_ANFORDERER_soll_getSensorCocsByIdList_aufrufen() {

        when(berechtigungDao.getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype(any(), any(),
                any(), any())).thenReturn(Arrays.asList("42"));

        berechtigungService.getSensorCocForMitarbeiterAndRolle(mitarbeiter, Rolle.SENSORCOCLEITER, true);
        verify(sensorCocService, times(1)).getSensorCocsByIdList(any());
    }

    @Test
    public void getSensorCocForMitarbeiterAndRolle_mit_Rolle_ANFORDERER_soll_getSensorCocsByIdList_aufrufen2() {
        when(berechtigungDao.getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype(any(), any(),
                any(), any())).thenReturn(Collections.EMPTY_LIST);

        berechtigungService.getSensorCocForMitarbeiterAndRolle(mitarbeiter, Rolle.SENSORCOCLEITER, true);
        verify(sensorCocService, never()).getSensorCocsByIdList(any());
    }

    @Test
    public void testGetSensorCocForMitarbeiterAndRolleIsANFORDERER2() {
        berechtigungService.getSensorCocForMitarbeiterAndRolle(mitarbeiter, Rolle.ANFORDERER, true);
        verify(sensorCocService, times(1)).getAllSortByTechnologie();
    }

}
