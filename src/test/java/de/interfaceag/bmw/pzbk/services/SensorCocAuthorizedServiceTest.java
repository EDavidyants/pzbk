package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SensorCocAuthorizedServiceTest {

    @Mock
    private SensorCocService sensorCocService;
    @Mock
    private BerechtigungService berechtigungService;
    @Mock
    private Session session;
    @InjectMocks
    private SensorCocAuthorizedService sensorCocAuthorizedService;


    @Test
    public void testUserIsNotAuthorizedToStripSensorCocPermission() {
        when(session.hasRole(any())).thenReturn(true);
        when(berechtigungService.getSensorCocForMitarbeiter(any(), anyBoolean())).thenReturn(Collections.emptyList());
        assertThat(sensorCocAuthorizedService.isUserAuthorizedToEditSensorCocPermission(1L), is(false));

    }

    @Test
    public void testUserIsSensorCocLeiterAuthorizedToStripSensorCocPermission() {
        when(session.hasRole(any())).thenReturn(true);
        when(berechtigungService.getSensorCocForMitarbeiter(any(), anyBoolean())).thenReturn(TestDataFactory.createSensorCocs(1));
        when(sensorCocService.getSensorCocIdsFromSensorCocList(any())).thenCallRealMethod();

        assertThat(sensorCocAuthorizedService.isUserAuthorizedToEditSensorCocPermission(0L), is(true));

    }

    @Test
    public void testUserIsAuthorizedToStripSensorCocPermission() {
        assertThat(sensorCocAuthorizedService.isUserAuthorizedToEditSensorCocPermission(1L), is(true));
    }

}