package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuModel;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungServiceZugeordneteMeldungenMenuTest {

    @Mock
    private Anforderung anforderung;

    @Mock
    private Meldung meldung1;

    @Mock
    private Meldung meldung2;

    @Mock
    private SensorCoc sensorCoc;

    @InjectMocks
    private AnforderungService anforderungService;

    private final String technologie = "TMO_E/E Hardware";
    private final Set<Meldung> meldungen = new HashSet<>();

    @BeforeEach
    public void setUp() {

    }

    @Test
    public void testForAnforderungMitMeldungenForUserMitZuordnenRecht() {
        when(meldung1.getFachId()).thenReturn("M1");
        when(meldung2.getFachId()).thenReturn("M2");
        when(meldung1.getSensorCoc()).thenReturn(sensorCoc);
        when(meldung2.getSensorCoc()).thenReturn(sensorCoc);
        when(sensorCoc.getTechnologie()).thenReturn(technologie);

        meldungen.addAll(Arrays.asList(meldung1, meldung2));
        when(anforderung.getMeldungen()).thenReturn(meldungen);

        boolean hasRightToZuordnen = true;

        MenuModel menuModel = anforderungService.getZugeordneteMeldungenMenuItemsForAnforderung(anforderung, hasRightToZuordnen);
        List<MenuElement> menuItems = menuModel.getElements();
        Assertions.assertEquals(3, menuItems.size());
    }

    @Test
    public void testForAnforderungMitMeldungenForUserOhneZuordnenRecht() {
        when(meldung1.getFachId()).thenReturn("M1");
        when(meldung2.getFachId()).thenReturn("M2");
        when(meldung1.getSensorCoc()).thenReturn(sensorCoc);
        when(meldung2.getSensorCoc()).thenReturn(sensorCoc);
        when(sensorCoc.getTechnologie()).thenReturn(technologie);

        meldungen.addAll(Arrays.asList(meldung1, meldung2));
        when(anforderung.getMeldungen()).thenReturn(meldungen);

        boolean hasRightToZuordnen = false;

        MenuModel menuModel = anforderungService.getZugeordneteMeldungenMenuItemsForAnforderung(anforderung, hasRightToZuordnen);
        List<MenuElement> menuItems = menuModel.getElements();
        Assertions.assertEquals(2, menuItems.size());
    }

    @Test
    public void testForAnforderungOhneMeldungenMitZuordnenRecht() {
        when(anforderung.getMeldungen()).thenReturn(meldungen);

        boolean hasRightToZuordnen = true;

        MenuModel menuModel = anforderungService.getZugeordneteMeldungenMenuItemsForAnforderung(anforderung, hasRightToZuordnen);
        List<MenuElement> menuItems = menuModel.getElements();
        Assertions.assertEquals(1, menuItems.size());
    }

    @Test
    public void testForAnforderungOhneMeldungenOhneZuordnenRecht() {
        when(anforderung.getMeldungen()).thenReturn(meldungen);

        boolean hasRightToZuordnen = false;

        MenuModel menuModel = anforderungService.getZugeordneteMeldungenMenuItemsForAnforderung(anforderung, hasRightToZuordnen);
        List<MenuElement> menuItems = menuModel.getElements();
        Assertions.assertEquals(0, menuItems.size());
    }

}
