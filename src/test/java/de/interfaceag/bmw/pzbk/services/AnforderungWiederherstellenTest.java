package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.mail.MailService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.AnforderungReportingStatusTransitionAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AnforderungWiederherstellenTest {

    AnforderungService anforderungService;

    Mitarbeiter user;

    Anforderung anforderung;

    @Before
    public void init() {
        anforderungService = new AnforderungService();
        anforderungService.historyService = mock(AnforderungMeldungHistoryService.class);
        anforderungService.mailService = mock(MailService.class);
        anforderungService.anforderungDao = mock(AnforderungDao.class);
        anforderungService.fileService = mock(FileService.class);
        anforderungService.anforderungReportingStatusTransitionAdapter = mock(AnforderungReportingStatusTransitionAdapter.class);

        user = TestDataFactory.generateMitarbeiter("mit", "arbeiter", "IF");
        anforderung = TestDataFactory.generateAnforderung();

        Mockito.doNothing()
                .when(anforderungService.historyService)
                .writeHistoryForChangedValuesOfAnfoMgmtObject(any(), any(), any());

        Mockito.doNothing()
                .when(anforderungService.mailService)
                .sendMailAnforderungChanged(any(), any());

        Mockito.doNothing()
                .when(anforderungService.fileService)
                .writeAnhaengeToFile(any());

        Mockito.when(anforderungService.anforderungDao.saveAnforderung(any())).thenReturn(1L);

    }

    @Test
    public void restoreAnforderung() {
        Mockito.when(anforderungService.historyService.getLastStatusByObjectnameAnforderungId(any(), any()))
                .thenReturn(Status.A_FTABGESTIMMT);

        String result = anforderungService.restoreAnforderung(anforderung, user);
        Assertions.assertEquals("", result, "result not correct");
    }

    @Test
    public void restoreAnforderungNullStatus() {
        Mockito.when(anforderungService.historyService.getLastStatusByObjectnameAnforderungId(any(), any()))
                .thenReturn(null);

        String result = anforderungService.restoreAnforderung(anforderung, user);
        Assertions.assertEquals("", result, "result not correct");
    }

    @Test
    public void restoreAnforderungKeineWeiterverfolgung() {
        Mockito.when(anforderungService.historyService.getLastStatusByObjectnameAnforderungId(any(), any()))
                .thenReturn(Status.A_KEINE_WEITERVERFOLG);

        String result = anforderungService.restoreAnforderung(anforderung, user);
        Assertions.assertEquals("Die Anforderung kann nicht zurückgesetzt werden, da sie schon im Status 'Keine Weiterverfolgung' war. Bitte eine neue Version erstellen.",
                result, "result not correct");
    }

    @Test
    public void restoreAnforderungNull() {
        String result = anforderungService.restoreAnforderung(null, null);
        Assertions.assertEquals("anforderung or user is null", result);
    }

    @Test
    public void restoreAnforderungStatus() {
        Anforderung result = anforderungService.restoreAnforderungStatus(anforderung);
        Assertions.assertEquals(Status.A_INARBEIT, result.getStatus(), "Anforderung Status nicht in Arbeit.");
    }

}
