package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.folgeprozessttl.ZuordnungAnforderungDerivatDTO;
import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.dao.ZuordnungAnforderungDerivatDao;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAnforderungDerivatService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungServiceTest {

    @Mock
    private ZuordnungAnforderungDerivatDao zuordnungAnforderungDerivatDao;

    @Mock
    UserPermissions userPermissions;

    @Mock
    private Session session;

    @Mock
    private AnforderungMeldungHistoryService anforderungMeldungHistoryService;

    @Mock
    private AnforderungDao anforderungDao;

    @Mock
    private TteamService tteamService;

    @InjectMocks
    private ZuordnungAnforderungDerivatService zads;

    @InjectMocks
    private AnforderungService anforderungService;

    @BeforeEach
    public void setUp() {

        anforderungService.setZuordnungAnforderungDerivatService(zads);
        when(session.getUser()).thenReturn(TestDataFactory.createMitarbeiter(""));
    }

    @Test
    public void acceptZurKenntnisGenommen() {
        ZuordnungAnforderungDerivat zuordnungAnforderungDerivat = TestDataFactory.generateZuordnungAnforderungDerivat();

        ZuordnungAnforderungDerivatDTO dto = new ZuordnungAnforderungDerivatDTO(1L, 2L,
                zuordnungAnforderungDerivat.getDerivat().getName(),
                zuordnungAnforderungDerivat.getStatus(), true, false, null);

        when(anforderungService.getZuordnungAnforderungDerivatService().zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatById(1L))
                .thenReturn(zuordnungAnforderungDerivat);
        anforderungService.acceptZurKenntnisGenommen(dto);
        Assertions.assertTrue(zuordnungAnforderungDerivat.isZurKenntnisGenommen());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void isUserBerechtigtForAnforderungTest(Rolle role) {

        List<Rolle> mitarbeiterRollen = new ArrayList<>();
        mitarbeiterRollen.add(role);
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRollen()).thenReturn(mitarbeiterRollen);
        when(anforderungDao.getAnforderungById(1L)).thenReturn(TestDataFactory.generateAnforderung());

        Boolean result;

        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case ANFORDERER:
                result = anforderungService.isUserBerechtigtForAnforderung(1L);
                Assertions.assertTrue(result);
                break;
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
            case SCL_VERTRETER:
            case SENSORCOCLEITER:
            case E_COC:
            case UMSETZUNGSBESTAETIGER:
                when(anforderungDao.getAnforderungForIdAndBerechtigung(anyLong(), any(), any())).thenReturn(TestDataFactory.generateAnforderung());
                result = anforderungService.isUserBerechtigtForAnforderung(1L);
                Assertions.assertTrue(result);
                break;
            case TTEAMMITGLIED:
                when(anforderungDao.getAnforderungForIdAndBerechtigung(anyLong(), any(), any())).thenReturn(null);
                when(tteamService.getTteamIdsforTteamMitgliedAndRechttype(any(), anyInt())).thenReturn(Arrays.asList(1L));
                result = anforderungService.isUserBerechtigtForAnforderung(1L);
                Assertions.assertTrue(result);
                break;
            case LESER:
                result = anforderungService.isUserBerechtigtForAnforderung(1L);
                Assertions.assertFalse(result);
                break;
            default:
                break;

        };

    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void isUserBerechtigtForAnforderungNoSencorCocTest(Rolle role) {

        List<Rolle> mitarbeiterRollen = new ArrayList<>();
        mitarbeiterRollen.add(role);
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRollen()).thenReturn(mitarbeiterRollen);
        when(anforderungDao.getAnforderungById(1L)).thenReturn(TestDataFactory.generateAnforderung());

        Boolean result;

        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case ANFORDERER:
                result = anforderungService.isUserBerechtigtForAnforderung(1L);
                Assertions.assertTrue(result);
                break;
            case SENSOR:
            case SENSOR_EINGESCHRAENKT:
            case SCL_VERTRETER:
            case SENSORCOCLEITER:
            case E_COC:
            case UMSETZUNGSBESTAETIGER:
                when(anforderungDao.getAnforderungForIdAndBerechtigung(anyLong(), any(), any())).thenReturn(null);
                result = anforderungService.isUserBerechtigtForAnforderung(1L);
                Assertions.assertFalse(result);
                break;
            case TTEAMMITGLIED:
                when(anforderungDao.getAnforderungForIdAndBerechtigung(anyLong(), any(), any())).thenReturn(null);
                when(tteamService.getTteamIdsforTteamMitgliedAndRechttype(any(), anyInt())).thenReturn(Arrays.asList(1L));
                result = anforderungService.isUserBerechtigtForAnforderung(1L);
                Assertions.assertTrue(result);
                break;
            case LESER:
                result = anforderungService.isUserBerechtigtForAnforderung(1L);
                Assertions.assertFalse(result);
                break;
            default:
                break;

        };

    }

    @Test
    public void tteamMitgliedNotberechtigtForAnforderungTest() {
        List<Rolle> mitarbeiterRollen = new ArrayList<>();
        mitarbeiterRollen.add(Rolle.TTEAMMITGLIED);
        when(session.getUserPermissions()).thenReturn(userPermissions);
        when(userPermissions.getRollen()).thenReturn(mitarbeiterRollen);
        when(anforderungDao.getAnforderungById(1L)).thenReturn(TestDataFactory.generateAnforderung());
        when(anforderungDao.getAnforderungForIdAndBerechtigung(anyLong(), any(), any())).thenReturn(null);
        when(tteamService.getTteamIdsforTteamMitgliedAndRechttype(any(), anyInt())).thenReturn(new ArrayList());
        Boolean result = anforderungService.isUserBerechtigtForAnforderung(1L);
        Assertions.assertFalse(result);
    }

}
