package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ContextMocker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.primefaces.model.menu.MenuModel;

import javax.faces.component.UIViewRoot;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungServiceVersionsMenuItemsTest {

    @Mock
    private Anforderung anforderung;

    @Mock
    private Anforderung version2;

    @Mock
    private Anforderung version3;

    @Mock
    private AnforderungDao anforderungDao;

    @Mock
    private UIViewRoot viewRoot;

    @InjectMocks
    private AnforderungService anforderungService;

    private Date erstellungsDatum;

    @BeforeEach
    public void setUp() {
        erstellungsDatum = Calendar.getInstance().getTime();
        when(anforderung.getFachId()).thenReturn("A1");
        when(anforderung.getVersion()).thenReturn(1);
        when(version2.getVersion()).thenReturn(2);
        when(version3.getVersion()).thenReturn(3);
        when(anforderung.getErstellungsdatum()).thenReturn(erstellungsDatum);
        when(version2.getErstellungsdatum()).thenReturn(erstellungsDatum);
        when(version3.getErstellungsdatum()).thenReturn(erstellungsDatum);
        when(anforderungDao.fetchVersionDataForFachId("A1")).thenReturn(Arrays.asList(anforderung, version2, version3));
    }

    @Test
    public void testForUserOhneBerechtigungForNeueVersion() {
        MenuModel menuItems = anforderungService.getVersionsMenuItemsForAnforderung(anforderung, false);
        int menuSize = menuItems.getElements().size();
        Assertions.assertEquals(3, menuSize);
    }

    @MockitoSettings(strictness = Strictness.LENIENT)
    @Test
    public void testForUserMitBerechtigungForNeueVersion() {
        when(ContextMocker.mockFacesContext().getViewRoot()).thenReturn(viewRoot);
        when(viewRoot.createUniqueId()).thenReturn("1234567");
        MenuModel menuItems = anforderungService.getVersionsMenuItemsForAnforderung(anforderung, true);
        int menuSize = menuItems.getElements().size();
        Assertions.assertEquals(4, menuSize);
    }

}
