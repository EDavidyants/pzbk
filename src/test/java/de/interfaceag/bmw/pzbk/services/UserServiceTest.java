package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.MitarbeiterDao;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

/**
 *
 * @author sl
 */
public class UserServiceTest {

    private UserService userService;

    @Before
    public void setup() {
        userService = new UserService();
        userService.mitarbeiterDao = mock(MitarbeiterDao.class);
    }

    @Test
    public void updateUserData() {
        Mitarbeiter currentUser = TestDataFactory.generateMitarbeiter("Hans", "Ledig", "IF-007");
        Mitarbeiter ldapUser = TestDataFactory.generateMitarbeiter("Hans", "Verheiratet", "IF-009");
        Mitarbeiter result = userService.updateUserData(currentUser, ldapUser);
        assertAll("Abgleich der persoenlichen Daten",
                () -> assertEquals("Hans", result.getVorname()),
                () -> assertEquals("Verheiratet", result.getNachname()),
                () -> assertEquals("IF-009", result.getAbteilung())
        );
    }
}
