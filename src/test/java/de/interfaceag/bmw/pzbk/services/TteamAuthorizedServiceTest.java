package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.session.Session;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TteamAuthorizedServiceTest {

    @Mock
    private TteamService tteamService;
    @Mock
    private BerechtigungService berechtigungService;
    @Mock
    private Session session;
    @InjectMocks
    private TteamAuthorizedService tteamAuthorizedService;


    @Test
    public void testUserIsNotAuthorizedToStripTteamPermission() {
        when(session.hasRole(any())).thenReturn(true);
        when(berechtigungService.getTteamsForMitarbeiter(any())).thenReturn(Collections.emptyList());
        assertThat(tteamAuthorizedService.isUserAuthorizedToEditTteamPermission(1L), is(false));

    }

    @Test
    public void testUserIsTteamLeiterAndAuthorizedToStripTteamPermission() {
        when(session.hasRole(any())).thenReturn(true);
        when(berechtigungService.getTteamsForMitarbeiter(any())).thenReturn(TestDataFactory.createListOfTteams(1));
        when(tteamService.getTteamIdsByTteamList(any())).thenCallRealMethod();
        assertThat(tteamAuthorizedService.isUserAuthorizedToEditTteamPermission(0L), is(true));

    }

    @Test
    public void testUserIsAuthorizedToStripTteamPermission() {
        assertThat(tteamAuthorizedService.isUserAuthorizedToEditTteamPermission(1L), is(true));

    }
}