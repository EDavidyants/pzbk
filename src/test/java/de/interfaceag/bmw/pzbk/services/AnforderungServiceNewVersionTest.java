package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.InvocationTargetException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungServiceNewVersionTest {

    @Mock
    AnforderungDao anforderungDao;
    @InjectMocks
    AnforderungService anforderungService;

    Anforderung anforderung;

    @BeforeEach
    public void setup() {
        anforderung = TestDataFactory.generateNewVersionAnforderung();
        when(anforderungDao.getAnforderungById(any())).thenReturn(anforderung);
    }

    @Test
    public void testGetNewVersionForAnforderungResetFreigabe() throws IllegalAccessException, InvocationTargetException {
        Anforderung newAnforderung = anforderungService.createNewVersionForAnforderung(1L);
        AnforderungFreigabeBeiModulSeTeam freigabeCopy = newAnforderung.getAnfoFreigabeBeiModul().get(0);
        assertFalse(freigabeCopy.isFreigabe());
    }

    @Test
    public void testGetNewVersionForAnforderungitUgpdateAnforderung() throws IllegalAccessException, InvocationTargetException {
        Anforderung newAnforderung = anforderungService.createNewVersionForAnforderung(1L);
        AnforderungFreigabeBeiModulSeTeam freigabeCopy = newAnforderung.getAnfoFreigabeBeiModul().get(0);
        Anforderung result = freigabeCopy.getAnforderung();
        MatcherAssert.assertThat(result, is(newAnforderung));
    }

}
