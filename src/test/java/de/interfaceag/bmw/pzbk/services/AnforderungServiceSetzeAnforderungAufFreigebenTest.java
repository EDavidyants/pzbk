package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.mail.MailService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.anforderung.AnforderungReportingStatusTransitionAdapter;
import de.interfaceag.bmw.pzbk.reporting.statustransition.domain.ReportingStatusTransition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungServiceSetzeAnforderungAufFreigebenTest {

    @Mock
    private AnforderungDao anforderungDao;

    @Mock
    private AnforderungReportingStatusTransitionAdapter reportingStatusTransitionAdapter;

    @Mock
    private AnforderungMeldungHistoryService historyService;

    @Mock
    private FileService fileService;

    @Mock
    private MailService mailService;

    @InjectMocks
    private AnforderungService anforderungService;

    private Mitarbeiter user;
    private Anforderung anforderung;
    private Anforderung prevVersion1;
    private Anforderung prevVersion2;
    private Anforderung prevVersion3;
    private Anforderung prevVersion4;
    private Anforderung prevVersion5;
    private Anforderung prevVersion6;

    @BeforeEach
    public void setUp() {
        user = TestDataFactory.generateMitarbeiter("Max", "Anforderer", "Abt.-1", "q9999999");

        anforderung = TestDataFactory.generateAnforderung();
        anforderung.setVersion(7);
        anforderung.setStatus(Status.A_PLAUSIB);

        prevVersion1 = TestDataFactory.generateAnforderung();
        prevVersion1.setVersion(1);
        prevVersion1.setStatus(Status.A_INARBEIT);

        prevVersion2 = TestDataFactory.generateAnforderung();
        prevVersion2.setVersion(2);
        prevVersion2.setStatus(Status.A_FTABGESTIMMT);

        prevVersion3 = TestDataFactory.generateAnforderung();
        prevVersion3.setVersion(3);
        prevVersion3.setStatus(Status.A_PLAUSIB);

        prevVersion4 = TestDataFactory.generateAnforderung();
        prevVersion4.setVersion(4);
        prevVersion4.setStatus(Status.A_GELOESCHT);

        prevVersion5 = TestDataFactory.generateAnforderung();
        prevVersion5.setVersion(5);
        prevVersion5.setStatus(Status.A_KEINE_WEITERVERFOLG);

        prevVersion6 = TestDataFactory.generateAnforderung();
        prevVersion6.setVersion(6);
        prevVersion6.setStatus(Status.A_FREIGEGEBEN);

        when(reportingStatusTransitionAdapter.changeAnforderungStatus(any(Anforderung.class), any(Status.class), any(Status.class))).thenReturn(new ReportingStatusTransition());
        when(anforderungDao.getAnforderungById(any(Long.class))).thenReturn(anforderung);
        doNothing().when(historyService).writeHistoryForChangedValuesOfAnfoMgmtObject(user, anforderung, anforderung);
        doNothing().when(mailService).sendMailAnforderungChanged(any(Anforderung.class), any(Mitarbeiter.class));
        when(anforderungDao.saveAnforderung(any(Anforderung.class))).thenReturn(123L);
        doNothing().when(fileService).writeAnhaengeToFile(any(Anforderung.class));

        when(anforderungDao.getVorigeVersionenForAnforderung(anforderung))
                .thenReturn(Arrays.asList(prevVersion1, prevVersion2, prevVersion3, prevVersion4, prevVersion5, prevVersion6));

        anforderungService.setzeAnforderungAufFreigebenAndWriteHistory(anforderung, "Status Change", user);

    }

    @Test
    public void testStatusChangeForAnforderung() {
        Status expected = Status.A_FREIGEGEBEN;
        Status fact = anforderung.getStatus();
        Assertions.assertEquals(expected, fact);
    }

    @Test
    public void testStatusChangeForVersionInArbeit() {
        Status expected = Status.A_KEINE_WEITERVERFOLG;
        Status fact = prevVersion1.getStatus();
        Assertions.assertEquals(expected, fact);
    }

    @Test
    public void testStatusChangeForVersionAbgestimmt() {
        Status expected = Status.A_KEINE_WEITERVERFOLG;
        Status fact = prevVersion2.getStatus();
        Assertions.assertEquals(expected, fact);
    }

    @Test
    public void testStatusChangeForVersionPlausibilisiert() {
        Status expected = Status.A_KEINE_WEITERVERFOLG;
        Status fact = prevVersion3.getStatus();
        Assertions.assertEquals(expected, fact);
    }

    @Test
    public void testStatusChangeForVersionGeloescht() {
        Status expected = Status.A_GELOESCHT;
        Status fact = prevVersion4.getStatus();
        Assertions.assertEquals(expected, fact);
    }

    @Test
    public void testStatusChangeForVersionKeineWeiterverfolgung() {
        Status expected = Status.A_KEINE_WEITERVERFOLG;
        Status fact = prevVersion5.getStatus();
        Assertions.assertEquals(expected, fact);
    }

    @Test
    public void testStatusChangeForVersionFreigegeben() {
        Status expected = Status.A_KEINE_WEITERVERFOLG;
        Status fact = prevVersion6.getStatus();
        Assertions.assertEquals(expected, fact);
    }

    @Test
    public void testStatusChangeForVersionInArbeitHistorisiert() {
        verify(historyService).writeHistoryForChangedKeyValue(user, prevVersion1, "Keine Weiterverfolgung", "in Arbeit", "status");
    }

    @Test
    public void testStatusChangeForVersionAbgestimmtHistorisiert() {
        verify(historyService).writeHistoryForChangedKeyValue(user, prevVersion2, "Keine Weiterverfolgung", "abgestimmt", "status");
    }

    @Test
    public void testStatusChangeForVersionPlausibilisiertHistorisiert() {
        verify(historyService).writeHistoryForChangedKeyValue(user, prevVersion3, "Keine Weiterverfolgung", "plausibilisiert", "status");
    }

    @Test
    public void testStatusChangeForVersionGeloeschtKeineHistorisierung() {
        verify(historyService, never()).writeHistoryForChangedKeyValue(user, prevVersion4, "Keine Weiterverfolgung", "gelöscht", "status");
    }

    @Test
    public void testStatusChangeForVersionKeineWeiterverfolgungKeineHistorisierung() {
        verify(historyService, never()).writeHistoryForChangedKeyValue(user, prevVersion5, "Keine Weiterverfolgung", "Keine Weiterverfolgung", "status");
    }

    @Test
    public void testStatusChangeForVersionFreigegebenHistorisiert() {
        verify(historyService).writeHistoryForChangedKeyValue(user, prevVersion6, "Keine Weiterverfolgung", "freigegeben", "status");
    }

    @Test
    public void testWriteStatusTransitionForFreigegebeneAnforderung() {
        verify(reportingStatusTransitionAdapter).changeAnforderungStatus(anforderung, Status.A_PLAUSIB, Status.A_FREIGEGEBEN);
    }

    @Test
    public void testWriteStatusTransitionForPreviousVersionInArbeit() {
        verify(reportingStatusTransitionAdapter).changeAnforderungStatus(prevVersion1, Status.A_INARBEIT, Status.A_KEINE_WEITERVERFOLG);
    }

    @Test
    public void testWriteStatusTransitionForPreviousVersionAbgestimmt() {
        verify(reportingStatusTransitionAdapter).changeAnforderungStatus(prevVersion2, Status.A_FTABGESTIMMT, Status.A_KEINE_WEITERVERFOLG);
    }

    @Test
    public void testWriteStatusTransitionForPreviousVersionPlausibilisiert() {
        verify(reportingStatusTransitionAdapter).changeAnforderungStatus(prevVersion3, Status.A_PLAUSIB, Status.A_KEINE_WEITERVERFOLG);
    }

    @Test
    public void testWriteStatusTransitionForPreviousVersionGeloescht() {
        verify(reportingStatusTransitionAdapter, never()).changeAnforderungStatus(prevVersion4, Status.A_GELOESCHT, Status.A_KEINE_WEITERVERFOLG);
    }

    @Test
    public void testWriteStatusTransitionForPreviousVersionKeineWeiterverfolgung() {
        verify(reportingStatusTransitionAdapter, never()).changeAnforderungStatus(prevVersion5, Status.A_KEINE_WEITERVERFOLG, Status.A_KEINE_WEITERVERFOLG);
    }

    @Test
    public void testWriteStatusTransitionForPreviousVersionFreigegeben() {
        verify(reportingStatusTransitionAdapter).changeAnforderungStatus(prevVersion6, Status.A_FREIGEGEBEN, Status.A_KEINE_WEITERVERFOLG);
    }

}
