package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.enums.Status;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungServiceModulFreigabeTest {

    @Mock
    private AnforderungDao anforderungDao;

    @Mock
    private AnforderungFreigabeService anforderungFreigabeService;

    @InjectMocks
    private final AnforderungService anforderungService = new AnforderungService();

    private Anforderung anforderungVorigeVersion;
    private Anforderung anforderung;
    private ModulSeTeam seTeamOne;
    private ModulSeTeam seTeamTwo;
    private AnforderungFreigabeBeiModulSeTeam modulFreigabeOne;
    private AnforderungFreigabeBeiModulSeTeam modulFreigabeTwo;

    @BeforeEach
    public void setUp() {

        anforderungVorigeVersion = TestDataFactory.generateAnforderungWithFachIdAndVersion(123L, "A123", 1);
        anforderungVorigeVersion.setStatus(Status.A_FREIGEGEBEN);

        anforderung = TestDataFactory.generateAnforderungWithFachIdAndVersion(123L, "A123", 2);
        anforderung.setStatus(Status.A_PLAUSIB);

        seTeamOne = TestDataFactory.generateModulSeTeam(TestDataFactory.generateFullModulWithSuffix("1"), Arrays.asList(TestDataFactory.generateModulKomponenteWithSuffix("1")));
        seTeamTwo = TestDataFactory.generateModulSeTeam(TestDataFactory.generateFullModulWithSuffix("2"), Arrays.asList(TestDataFactory.generateModulKomponenteWithSuffix("2")));
        modulFreigabeOne = TestDataFactory.generateAnforderungFreigabeBeiModulSeTeam(anforderung, seTeamOne);
        modulFreigabeTwo = TestDataFactory.generateAnforderungFreigabeBeiModulSeTeam(anforderung, seTeamTwo);
        List<AnforderungFreigabeBeiModulSeTeam> modulFreigaben = new ArrayList<>();
        modulFreigaben.add(modulFreigabeOne);
        modulFreigaben.add(modulFreigabeTwo);

        anforderung.setAnfoFreigabeBeiModul(modulFreigaben);

    }

    /**
     * Testet dass die Ueberpruefung nach anstehender Freigabe keine
     * Nebenwirkung auf Anforderung Status hat in diesem Fall steht die Freigabe
     * vor, da nur noch ein Modul freizugeben ist, hier gibt dazu die vorige
     * Version der Anforderung in Status freigegeben
     */
    @Test
    public void testKeineFreigabeBeiCheckForAnstehendeFreigabeForNewVersion() {

        when(anforderungDao.getAnforderungByFachIdVersion(any(String.class), any(Integer.class))).thenReturn(anforderungVorigeVersion);
        when(anforderungFreigabeService.getFreigegebeneModulSeTeamsForAnforderung(anforderung)).thenReturn(Arrays.asList(modulFreigabeOne));
        when(anforderungFreigabeService.countAllModulSeTeamsForAnforderung(anforderung)).thenReturn(2);

        Boolean checkFreizugeben = anforderungService.checkFreizugeben(anforderung, modulFreigabeTwo);

        Assertions.assertTrue(checkFreizugeben);
        Assertions.assertTrue(anforderung.getStatus() == Status.A_PLAUSIB);
    }

    /**
     * Testet dass die Ueberpruefung nach anstehender Freigabe keine
     * Nebenwirkung auf Anforderung Status hat in diesem Fall steht keine
     * Freigabe vor - mehr als ein Modul muss freigegeben werden, hier gibt die
     * vorige Version der Anforderung in Status freigegeben
     */
    @Test
    public void testKeineFreigabeBeiCheckForNichtAnstehendeFreigabeForNewVersion() {

        when(anforderungDao.getAnforderungByFachIdVersion(any(String.class), any(Integer.class))).thenReturn(anforderungVorigeVersion);
        when(anforderungFreigabeService.getFreigegebeneModulSeTeamsForAnforderung(anforderung)).thenReturn(Collections.EMPTY_LIST);
        when(anforderungFreigabeService.countAllModulSeTeamsForAnforderung(anforderung)).thenReturn(2);

        Boolean checkFreizugeben = anforderungService.checkFreizugeben(anforderung, modulFreigabeTwo);

        Assertions.assertFalse(checkFreizugeben);
        Assertions.assertTrue(anforderung.getStatus() == Status.A_PLAUSIB);
    }

    /**
     * Testet dass die Ueberpruefung nach anstehender Freigabe keine
     * Nebenwirkung auf Anforderung Status hat in diesem Fall steht die Freigabe
     * vor, da nur noch ein Modul freizugeben ist, hier gibt die einzige Version
     * der Anforderung
     */
    @Test
    public void testKeineFreigabeBeimCheckForEinzigeVersion() {

        when(anforderungDao.getAnforderungByFachIdVersion(any(String.class), any(Integer.class))).thenReturn(null);

        Boolean checkFreizugeben = anforderungService.checkFreizugeben(anforderung, modulFreigabeTwo);

        Assertions.assertFalse(checkFreizugeben);
        Assertions.assertTrue(anforderung.getStatus() == Status.A_PLAUSIB);
    }

}
