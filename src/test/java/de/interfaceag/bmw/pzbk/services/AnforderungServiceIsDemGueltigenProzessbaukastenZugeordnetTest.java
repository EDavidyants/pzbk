package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungServiceIsDemGueltigenProzessbaukastenZugeordnetTest {
    
    @Mock
    private AnforderungDao anforderungDao;
    
    @InjectMocks
    private final AnforderungService anforderungService = new AnforderungService();
    
    private Anforderung anforderung;
    
    
    @BeforeEach
    public void setUp() {
        anforderung = TestDataFactory.generateAnforderung();
    }
    
    @Test
    public void testWithNullProzessbaukasten() {
        when(anforderungDao.getPreviousVersionsWithGueltigenProzessbaukasten(anforderung)).thenReturn(Collections.EMPTY_LIST);
        
        Boolean isdemGueltigenProzessbaukastenZugeordnet = anforderungService.isDemGueltigenProzessbaukastenZugeordnet(anforderung);
        Assertions.assertFalse(isdemGueltigenProzessbaukastenZugeordnet);        
    }
    
    @Test
    public void testWithOneProzessbaukasten() {
        when(anforderungDao.getPreviousVersionsWithGueltigenProzessbaukasten(anforderung)).thenReturn(Arrays.asList(TestDataFactory.generateAnforderung()));
        
        Boolean isdemGueltigenProzessbaukastenZugeordnet = anforderungService.isDemGueltigenProzessbaukastenZugeordnet(anforderung);
        Assertions.assertTrue(isdemGueltigenProzessbaukastenZugeordnet);        
    }
    
    @Test
    public void testWithMultipleProzessbaukasten() {
        when(anforderungDao.getPreviousVersionsWithGueltigenProzessbaukasten(anforderung)).thenReturn(Arrays.asList(TestDataFactory.generateAnforderung(), TestDataFactory.generateAnforderung()));
        
        Boolean isdemGueltigenProzessbaukastenZugeordnet = anforderungService.isDemGueltigenProzessbaukastenZugeordnet(anforderung);
        Assertions.assertTrue(isdemGueltigenProzessbaukastenZugeordnet);        
    }
    
}
