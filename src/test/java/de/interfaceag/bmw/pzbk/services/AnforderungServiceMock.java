package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.enums.Status;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class AnforderungServiceMock extends AnforderungService {

    private final List<Meldung> meldungen;
    private final List<SensorCoc> scocs;

    public AnforderungServiceMock() {
        this.meldungen = new ArrayList<>();
        this.scocs = new ArrayList<>();

        Long idStartMeldungen = 0L;
        for (Status status : Status.values()) {
            List<Meldung> meldungenToAdd = TestDataFactory.createTestMeldungen(10, status, idStartMeldungen);
            idStartMeldungen += 10;
            this.meldungen.addAll(meldungenToAdd);
        }

        for (Meldung meldung : meldungen) {
            if (!scocs.contains(meldung.getSensorCoc())) {
                scocs.add(meldung.getSensorCoc());
            }
        }
    }

    public List<SensorCoc> getAllUsedSensorCocs() {
        return Collections.unmodifiableList(scocs);
    }

    /**
     *
     * @param a Anforderung to save
     */
    @Override
    public Long saveAnforderung(Anforderung a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Long saveMeldung(Meldung m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Anforderung> getAllAnforderung() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Meldung> getAllMeldungen() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Anforderung> getAllAnforderungenWithThisMeldung(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<AnforderungFreigabeBeiModulSeTeam> anforderungForModulSeTeamFreigeben(Anforderung anforderung, List<ModulSeTeam> moduls, String kommentar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Anforderung anforderungForAllMyModulSeTeamsFreigeben(Anforderung anforderung, Mitarbeiter me, String kommentar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    /**
     *
     * @param id id of requested Anforderung
     * @param version
     * @return Anforderung with ID id
     */
    public Anforderung getAnforderungById(Long id, Integer version) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public List<Meldung> getAllMeldungenBySensorCocAndStatus(List<SensorCoc> scoc, List<Status> status) {
        List<Meldung> result = new ArrayList<>();
        for (Meldung meldung : this.meldungen) {
            if (scoc.contains(meldung.getSensorCoc()) && status.contains(meldung.getStatus())) {
                result.add(meldung);
            }
        }
        return result;
    }

    @Override
    public Meldung getMeldungById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public Anforderung createAnforderungForMeldung(Long mid) throws IllegalAccessException, InvocationTargetException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }

    @Override
    public Anforderung createNewVersionForAnforderung(Long aid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
