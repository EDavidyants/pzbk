package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.dao.UrlEncodingDao;
import de.interfaceag.bmw.pzbk.entities.UrlEncoding;
import de.interfaceag.bmw.pzbk.enums.Page;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class UrlEncondingServiceTest {

    private UrlEncodingService urlEncodingService;

    @Before
    public void setup() {
        urlEncodingService = new UrlEncodingService();
        urlEncodingService.urlEncodingDao = mock(UrlEncodingDao.class);
    }

    @Test
    public void createNewUrlEncodingForPageAndParameter() {
        Page page = Page.SUCHE;
        String urlParameter = "fid=A4242v1,M424242";

        when(urlEncodingService.getUrlEncodingByUrlId(any())).thenReturn(null);

        UrlEncoding result = urlEncodingService.createNewUrlEncodingForPageAndParameter(page, urlParameter);
        assertAll("Abgleich der Werte",
                () -> assertEquals(page.getBezeichnung(), result.getPage().getBezeichnung()),
                () -> assertEquals(urlParameter, result.getUrlParameter())
        );
    }

    @Test
    public void compareUrlIdEqual() {
        Page page1 = Page.SUCHE;
        String urlParameter1 = "fid=A4242v1,M424242";

        Page page2 = Page.SUCHE;
        String urlParameter2 = "fid=A4242v1,M424242";

        when(urlEncodingService.getUrlEncodingByUrlId(any())).thenReturn(null);

        UrlEncoding result1 = urlEncodingService.createNewUrlEncodingForPageAndParameter(page1, urlParameter1);
        UrlEncoding result2 = urlEncodingService.createNewUrlEncodingForPageAndParameter(page2, urlParameter2);

        assertEquals(result1.getUrlId(), result2.getUrlId());
    }

    @Test
    public void compareUrlIdNotEqualCase1() {
        Page page1 = Page.SUCHE;
        String urlParameter1 = "fid=A4242v1,M424242";

        Page page2 = Page.SUCHE;
        String urlParameter2 = "fid=A4242v1,M424241";

        when(urlEncodingService.getUrlEncodingByUrlId(any())).thenReturn(null);

        UrlEncoding result1 = urlEncodingService.createNewUrlEncodingForPageAndParameter(page1, urlParameter1);
        UrlEncoding result2 = urlEncodingService.createNewUrlEncodingForPageAndParameter(page2, urlParameter2);

        assertNotEquals(result1.getUrlId(), result2.getUrlId());
    }

    @Test
    public void compareUrlIdNotEqualCase2() {
        Page page1 = Page.SUCHE;
        String urlParameter1 = "fid=A4242v1,M424242";

        Page page2 = Page.BERICHTSWESEN;
        String urlParameter2 = "fid=A4242v1,M424242";

        when(urlEncodingService.getUrlEncodingByUrlId(any())).thenReturn(null);

        UrlEncoding result1 = urlEncodingService.createNewUrlEncodingForPageAndParameter(page1, urlParameter1);
        UrlEncoding result2 = urlEncodingService.createNewUrlEncodingForPageAndParameter(page2, urlParameter2);

        assertNotEquals(result1.getUrlId(), result2.getUrlId());
    }

}
