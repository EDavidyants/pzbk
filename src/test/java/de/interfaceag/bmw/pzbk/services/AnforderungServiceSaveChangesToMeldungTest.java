package de.interfaceag.bmw.pzbk.services;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.entities.Anhang;
import de.interfaceag.bmw.pzbk.entities.Auswirkung;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.mail.MailService;
import de.interfaceag.bmw.pzbk.reporting.statustransition.meldung.MeldungReportingStatusTransitionAdapter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungServiceSaveChangesToMeldungTest {

    @Mock
    private AnforderungDao anforderungDao;

    @Mock
    private FileService fileService;

    @Mock
    private MeldungReportingStatusTransitionAdapter changeMeldungStatusService;

    @Mock
    private AnforderungMeldungHistoryService historyService;

    @Mock
    private ImageService imageService;

    @Mock
    private MailService mailService;

    private Meldung meldung;
    private Meldung changedMeldung;

    private Mitarbeiter user;
    private String statusChangeKommentar;

    @InjectMocks
    private AnforderungService anforderungService;

    @BeforeEach
    public void setUp() {
        meldung = TestDataFactory.generateMeldung();
        changedMeldung = TestDataFactory.generateMeldung();
        user = TestDataFactory.generateMitarbeiter("Zack", "Logan", "Abt-2", "q2222222");
        doNothing().when(fileService).writeAnhaengeToFile(any());
        doNothing().when(mailService).sendMailAnforderungChanged(any(), any());
        statusChangeKommentar = "status change";
    }

    @Test
    public void testForNewMeldung() {
        meldung.setId(null);
        meldung.setNextStatus(null);
        when(anforderungDao.saveMeldung(meldung)).thenReturn(1L);
        anforderungService.saveChangesToMeldung(meldung, statusChangeKommentar, user, null);
        verify(changeMeldungStatusService, never()).changeMeldungStatus(any(), any(), any());
        verify(historyService).writeHistoryForNewAnfoMgmtObject(user, meldung);
    }

    @Test
    public void testForEditedMeldungInEntwurf() {
        changedMeldung.setNextStatus(null);
        when(anforderungDao.saveMeldung(changedMeldung)).thenReturn(1L);
        when(anforderungDao.getMeldungById(1L)).thenReturn(meldung);
        anforderungService.saveChangesToMeldung(changedMeldung, statusChangeKommentar, user, null);
        verify(changeMeldungStatusService, never()).changeMeldungStatus(any(), any(), any());
        verify(historyService).writeHistoryForChangedValuesOfAnfoMgmtObject(user, changedMeldung, meldung);
    }

    @Test
    public void testForJustGemeldeteMeldung() {
        meldung.setStatus(Status.M_ENTWURF);
        changedMeldung.setNextStatus(Status.M_GEMELDET);
        changedMeldung.setStatus(Status.M_ENTWURF);
        when(anforderungDao.saveMeldung(changedMeldung)).thenReturn(1L);
        when(anforderungDao.getMeldungById(1L)).thenReturn(meldung);
        anforderungService.saveChangesToMeldung(changedMeldung, statusChangeKommentar, user, null);
        verify(changeMeldungStatusService).changeMeldungStatus(meldung, Status.M_ENTWURF, Status.M_GEMELDET);
        verify(historyService).writeHistoryForChangedValuesOfAnfoMgmtObject(user, meldung, meldung);
    }

    @Test
    public void testForEditedGemeldeteMeldung() {
        meldung.setStatus(Status.M_GEMELDET);
        changedMeldung.setNextStatus(Status.M_GEMELDET);
        changedMeldung.setStatus(Status.M_GEMELDET);
        when(anforderungDao.saveMeldung(changedMeldung)).thenReturn(1L);
        when(anforderungDao.getMeldungById(1L)).thenReturn(meldung);
        anforderungService.saveChangesToMeldung(changedMeldung, statusChangeKommentar, user, null);
        verify(changeMeldungStatusService, never()).changeMeldungStatus(any(), any(), any());
        verify(historyService).writeHistoryForChangedValuesOfAnfoMgmtObject(user, meldung, meldung);
    }

    @Test
    public void testForJustZurueckgewieseneMeldung() {
        meldung.setStatus(Status.M_GEMELDET);
        changedMeldung.setNextStatus(Status.M_UNSTIMMIG);
        changedMeldung.setStatus(Status.M_GEMELDET);
        when(anforderungDao.saveMeldung(changedMeldung)).thenReturn(1L);
        when(anforderungDao.getMeldungById(1L)).thenReturn(meldung);
        anforderungService.saveChangesToMeldung(changedMeldung, statusChangeKommentar, user, null);
        verify(changeMeldungStatusService).changeMeldungStatus(meldung, Status.M_GEMELDET, Status.M_UNSTIMMIG);
        verify(historyService).writeHistoryForChangedValuesOfAnfoMgmtObject(user, meldung, meldung);
    }

    @Test
    public void testForEditedZurueckgewieseneMeldung() {
        meldung.setStatus(Status.M_UNSTIMMIG);
        changedMeldung.setNextStatus(Status.M_UNSTIMMIG);
        changedMeldung.setStatus(Status.M_UNSTIMMIG);
        when(anforderungDao.saveMeldung(changedMeldung)).thenReturn(1L);
        when(anforderungDao.getMeldungById(1L)).thenReturn(meldung);
        anforderungService.saveChangesToMeldung(changedMeldung, statusChangeKommentar, user, null);
        verify(changeMeldungStatusService, never()).changeMeldungStatus(any(), any(), any());
        verify(historyService).writeHistoryForChangedValuesOfAnfoMgmtObject(user, meldung, meldung);
    }

    @Test
    public void testForJustZugeordneteMeldung() {
        meldung.setStatus(Status.M_GEMELDET);
        changedMeldung.setNextStatus(Status.M_ZUGEORDNET);
        changedMeldung.setStatus(Status.M_GEMELDET);
        when(anforderungDao.saveMeldung(changedMeldung)).thenReturn(1L);
        when(anforderungDao.getMeldungById(1L)).thenReturn(meldung);
        anforderungService.saveChangesToMeldung(changedMeldung, statusChangeKommentar, user, null);
        verify(changeMeldungStatusService).changeMeldungStatus(meldung, Status.M_GEMELDET, Status.M_ZUGEORDNET);
        verify(historyService).writeHistoryForChangedValuesOfAnfoMgmtObject(user, meldung, meldung);
    }

    @Test
    public void testForEditedMeldungInEntwurfForAuswirkungen() {
        List<Auswirkung> auswirkungenOld = new ArrayList<>();
        Auswirkung auswirkung1 = TestDataFactory.generateAuswirkung();
        auswirkung1.setId(1L);
        Auswirkung auswirkung2 = TestDataFactory.generateAuswirkung();
        auswirkung2.setId(2L);
        auswirkungenOld.add(auswirkung1);
        auswirkungenOld.add(auswirkung2);

        List<Auswirkung> auswirkungenNew = new ArrayList<>();
        auswirkungenNew.add(auswirkung2);

        meldung.setStatus(Status.M_GEMELDET);
        meldung.setAuswirkungen(auswirkungenOld);
        changedMeldung.setNextStatus(Status.M_GEMELDET);
        changedMeldung.setStatus(Status.M_GEMELDET);
        changedMeldung.setAuswirkungen(auswirkungenNew);

        when(anforderungDao.saveMeldung(changedMeldung)).thenReturn(1L);
        when(anforderungDao.getMeldungById(1L)).thenReturn(meldung);
        anforderungService.saveChangesToMeldung(changedMeldung, statusChangeKommentar, user, null);
        verify(changeMeldungStatusService, never()).changeMeldungStatus(any(), any(), any());
        verify(historyService).writeHistoryForChangedValuesOfAnfoMgmtObject(user, changedMeldung, meldung);
    }

    @Test
    public void testForEditedMeldungInEntwurfForFestgestelltIn() {
        Set<FestgestelltIn> festgestelltInOld = new HashSet<>();
        FestgestelltIn festgestelltInOne = TestDataFactory.generateFestgestelltInWithoutId();
        festgestelltInOne.setId(1L);
        FestgestelltIn festgestelltInTwo = TestDataFactory.generateFestgestelltInWithoutId();
        festgestelltInTwo.setId(2L);
        festgestelltInOld.add(festgestelltInOne);
        festgestelltInOld.add(festgestelltInTwo);

        Set<FestgestelltIn> festgestelltInNew = new HashSet<>();
        festgestelltInNew.add(festgestelltInTwo);

        meldung.setStatus(Status.M_GEMELDET);
        meldung.setFestgestelltIn(festgestelltInOld);
        changedMeldung.setNextStatus(Status.M_GEMELDET);
        changedMeldung.setStatus(Status.M_GEMELDET);
        changedMeldung.setFestgestelltIn(festgestelltInNew);

        when(anforderungDao.saveMeldung(changedMeldung)).thenReturn(1L);
        when(anforderungDao.getMeldungById(1L)).thenReturn(meldung);
        anforderungService.saveChangesToMeldung(changedMeldung, statusChangeKommentar, user, null);
        verify(changeMeldungStatusService, never()).changeMeldungStatus(any(), any(), any());
        verify(historyService).writeHistoryForChangedValuesOfAnfoMgmtObject(user, changedMeldung, meldung);
    }

    @Test
    public void testForEditedMeldungInEntwurfForAnhaenge() {
        List<Anhang> anhaenge = new ArrayList<>();
        Anhang anhangOne = TestDataFactory.generateAnhang();
        anhaenge.add(anhangOne);

        meldung.setStatus(Status.M_GEMELDET);
        meldung.setAnhaenge(anhaenge);
        changedMeldung.setNextStatus(Status.M_GEMELDET);
        changedMeldung.setStatus(Status.M_GEMELDET);
        changedMeldung.setAnhaenge(new ArrayList<>());

        when(anforderungDao.saveMeldung(changedMeldung)).thenReturn(1L);
        when(anforderungDao.getMeldungById(1L)).thenReturn(meldung);
        anforderungService.saveChangesToMeldung(changedMeldung, statusChangeKommentar, user, null);
        verify(changeMeldungStatusService, never()).changeMeldungStatus(any(), any(), any());
        verify(historyService).writeHistoryForChangedValuesOfAnfoMgmtObject(user, changedMeldung, meldung);
    }

}
