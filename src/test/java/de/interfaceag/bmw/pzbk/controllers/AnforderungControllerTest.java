package de.interfaceag.bmw.pzbk.controllers;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMitMeldungFacade;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.dto.AnforderungViewPermission;
import de.interfaceag.bmw.pzbk.controller.AnforderungController;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.InvocationTargetException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungControllerTest {

    @Mock
    private AnforderungService anforderungService;

    @Mock
    private AnforderungMitMeldungFacade facade;

    @Mock
    private AnforderungViewPermission viewPermission;

    @Mock
    private Anforderung anforderung;

    @InjectMocks
    private AnforderungController anforderungController;

    @BeforeEach
    public void setUp() {

    }

    @Test
    public void createNewVersionForAnforderungTest() throws IllegalAccessException, InvocationTargetException {
        anforderungService.saveAnforderung(anforderung);
        anforderungService.createNewVersionForAnforderung(anforderung.getId());
        verify(anforderungService).createNewVersionForAnforderung(anforderung.getId());
    }

    @Test
    public void testGetVersionsMenuItems() {
        when(viewPermission.getNewVersionButton()).thenReturn(true);
        anforderungController.getVersionsMenuItems();
        verify(facade).getVersionsMenuItemsForAnforderung(anforderung, true);
    }

    @Test
    public void testGetZugeordneteMeldungenMenuItems() {
        when(viewPermission.getMeldungZuordnenButton()).thenReturn(true);
        anforderungController.getZugeordneteMeldungenMenuItems();
        verify(facade).getZugeordneteMeldungenMenuItemsForAnforderung(anforderung, true);
    }

}
