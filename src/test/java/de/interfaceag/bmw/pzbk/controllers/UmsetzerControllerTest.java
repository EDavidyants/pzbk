package de.interfaceag.bmw.pzbk.controllers;

import de.interfaceag.bmw.pzbk.controller.dialogs.UmsetzerController;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.Umsetzer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.doReturn;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class UmsetzerControllerTest {

    @Spy
    private final UmsetzerController umsetzerController = new UmsetzerController();

    private Umsetzer umsetzerOne;
    private Umsetzer umsetzerTwo;
    private Umsetzer umsetzerThree;
    private Set<Umsetzer> umsetzerPresent;
    private Umsetzer selectedUmsetzer;

    ModulKomponente komponenteOne;
    private ModulSeTeam seTeamOne;

    ModulKomponente komponenteTwo;
    ModulSeTeam seTeamTwo;

    @BeforeEach
    public void setUp() {
        umsetzerPresent = new HashSet();
        umsetzerController.setDialogUmsetzerSet(umsetzerPresent);

        Modul modul = new Modul("modulName", "Fachbereich", "beschreibung");

        komponenteOne = new ModulKomponente("ppg1", "komponente1");
        seTeamOne = new ModulSeTeam("seTeamOne", modul,
                Stream.of(komponenteOne).collect(Collectors.toSet()));
        komponenteOne.setSeTeam(seTeamOne);
        modul.addSeTeam(seTeamOne);

        komponenteTwo = new ModulKomponente("ppg2", "komponente2");
        seTeamTwo = new ModulSeTeam("seTeamTwo", modul,
                Stream.of(komponenteTwo).collect(Collectors.toSet()));
        komponenteTwo.setSeTeam(seTeamTwo);
        modul.addSeTeam(seTeamTwo);

        umsetzerOne = new Umsetzer(seTeamOne, komponenteOne);
        umsetzerTwo = new Umsetzer(seTeamTwo, komponenteTwo);
        umsetzerThree = new Umsetzer(seTeamOne, null);

    }

    @Test
    public void testWithEmptyUmsetzerSet() {
        doReturn(umsetzerOne).when(umsetzerController).getUmsetzer();
        umsetzerController.addUmsetzer();
        Assertions.assertEquals(1, umsetzerPresent.size());
    }

    @Test
    public void testWithAnotherKomponente() {
        umsetzerPresent.add(umsetzerOne);
        doReturn(umsetzerTwo).when(umsetzerController).getUmsetzer();
        umsetzerController.addUmsetzer();
        Assertions.assertEquals(2, umsetzerPresent.size());
    }

    @Test
    public void testWithSameSeTeamAndNoKomponente() {
        umsetzerPresent.add(umsetzerThree);
        doReturn(umsetzerOne).when(umsetzerController).getUmsetzer();
        umsetzerController.addUmsetzer();
        Assertions.assertEquals(1, umsetzerPresent.size());
        selectedUmsetzer = umsetzerPresent.stream()
                .filter(u -> u.getSeTeam().equals(seTeamOne)).findAny().get();
        Assertions.assertEquals(komponenteOne, selectedUmsetzer.getKomponente());
    }

    @Test
    public void testWithSameSeTeamAndKomponente() {
        umsetzerPresent.add(umsetzerOne);
        doReturn(umsetzerThree).when(umsetzerController).getUmsetzer();
        umsetzerController.addUmsetzer();
        Assertions.assertEquals(1, umsetzerPresent.size());
        selectedUmsetzer = umsetzerPresent.stream()
                .filter(u -> u.getSeTeam().equals(seTeamOne)).findAny().get();
        Assertions.assertEquals(komponenteOne, selectedUmsetzer.getKomponente());
    }

}
