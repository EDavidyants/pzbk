package de.interfaceag.bmw.pzbk.datenschutzerklaerung;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class DatenschutzerklaerungControllerTest {

    @Mock
    private DatenschutzerklaerungFacade facade;

    @InjectMocks
    private DatenschutzerklaerungController controller;

    private DatenschutzerklaerungViewData viewData;

    @BeforeEach
    public void setUp() {
        String datenschutzerklaerungText = "Life is good";
        Mitarbeiter user = TestDataFactory.generateMitarbeiter("Zack", "Logan", "Abt.-2");
        boolean readAndUnderstood = false;
        boolean akzeptiert = false;
        viewData = DatenschutzerklaerungViewData.builder().forUser(user)
                .withDatenschutzerklaerungReadAndUnderstood(readAndUnderstood)
                .withDatenschutzerklaerungAkzeptiert(akzeptiert)
                .withDatenschutzerklaerungText(datenschutzerklaerungText)
                .build();

        when(facade.initViewData()).thenReturn(viewData);
        controller.init();
    }

    @Test
    public void testNotReadyToBeAkzeptiert() {
        boolean notReadyToBeAkzeptiert = controller.notReadyToBeAkzeptiert();
        Assertions.assertTrue(notReadyToBeAkzeptiert);
    }

    @Test
    public void testUpdateDatenschutzerklaerungForUser() {
        controller.updateDatenschutzerklaerungForUser();
        verify(facade).updateDatenschutzerklaerungForUser(viewData);
    }

}
