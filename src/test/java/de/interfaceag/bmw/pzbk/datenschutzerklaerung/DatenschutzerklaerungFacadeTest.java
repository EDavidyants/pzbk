package de.interfaceag.bmw.pzbk.datenschutzerklaerung;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.services.UserService;
import de.interfaceag.bmw.pzbk.session.SessionEdit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class DatenschutzerklaerungFacadeTest {

    @Mock
    private SessionEdit session;

    @Mock
    private UserService userService;

    @Mock
    private LocalizationService localizationService;

    @InjectMocks
    private DatenschutzerklaerungFacade facade;

    private DatenschutzerklaerungViewData viewData;
    private Mitarbeiter user;
    private boolean akzeptiert;
    private boolean readAndUnderstood;
    private String datenschutzerklaerungText;

    @BeforeEach
    public void setUp() {
        user = TestDataFactory.generateMitarbeiter("Zack", "Logan", "Abt.-2");
        when(session.getUser()).thenReturn(user);
        akzeptiert = false;
        readAndUnderstood = false;
        when(session.userHasDatenschutzerklaerungAkzeptiert()).thenReturn(akzeptiert);
        datenschutzerklaerungText = "Life is good";
        when(localizationService.getValue("datenschutzerklaerung.text")).thenReturn(datenschutzerklaerungText);

        viewData = facade.initViewData();
    }

    @Test
    public void testInitViewData() {
        DatenschutzerklaerungViewData viewDataToCheck = viewData = DatenschutzerklaerungViewData.builder().forUser(user)
                .withDatenschutzerklaerungReadAndUnderstood(readAndUnderstood)
                .withDatenschutzerklaerungAkzeptiert(akzeptiert)
                .withDatenschutzerklaerungText(datenschutzerklaerungText)
                .build();

        Assertions.assertEquals(viewData, viewDataToCheck);
    }

    @Test
    public void testUpdateDatenschutzerklaerungForUserSuccessful() {
        boolean wasUpdated = facade.updateDatenschutzerklaerungForUser(viewData);
        Assertions.assertTrue(wasUpdated);
    }

    @Test
    public void testUpdateDatenschutzerklaerungForUserSaved() {
        facade.updateDatenschutzerklaerungForUser(viewData);
        verify(userService).saveMitarbeiter(user);
    }

    @Test
    public void testUpdateDatenschutzerklaerungForUserSessionUserUpdated() {
        facade.updateDatenschutzerklaerungForUser(viewData);
        verify(session).setUser(user);
    }

}
