package de.interfaceag.bmw.pzbk.datenschutzerklaerung;

import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class DatenschutzerklaerungViewDataTest {

    private Mitarbeiter user;
    private boolean readAndUnderstood;
    private boolean akzeptiert;
    private DatenschutzerklaerungViewData viewData;
    private String datenschutzerklaerungText;

    private DatenschutzerklaerungViewData viewDataOther;

    @BeforeEach
    public void setUp() {
        datenschutzerklaerungText = "Life is good";
        user = TestDataFactory.generateMitarbeiter("Zack", "Logan", "Abt.-2");
        readAndUnderstood = false;
        akzeptiert = false;
        viewData = DatenschutzerklaerungViewData.builder().forUser(user)
                .withDatenschutzerklaerungReadAndUnderstood(readAndUnderstood)
                .withDatenschutzerklaerungAkzeptiert(akzeptiert)
                .withDatenschutzerklaerungText(datenschutzerklaerungText)
                .build();
    }

    @Test
    public void testToString() {
        String toString = viewData.toString();
        Assertions.assertEquals("Zack Logan | Abt.-2 hat Datenschutzerklaerung noch nicht akzeptiert", toString);
    }

    @Test
    public void testHashCodeSame() {
        viewDataOther = DatenschutzerklaerungViewData.builder().forUser(user)
                .withDatenschutzerklaerungReadAndUnderstood(readAndUnderstood)
                .withDatenschutzerklaerungAkzeptiert(akzeptiert)
                .withDatenschutzerklaerungText(datenschutzerklaerungText)
                .build();

        int hashCodeOther = viewDataOther.hashCode();
        Assertions.assertEquals(viewData.hashCode(), hashCodeOther);
    }

    @Test
    public void testHashCodeWithOtherUser() {
        Mitarbeiter otherUser = TestDataFactory.generateMitarbeiter("Cody", "Coleman", "Abt.-2");
        viewDataOther = DatenschutzerklaerungViewData.builder().forUser(otherUser)
                .withDatenschutzerklaerungReadAndUnderstood(readAndUnderstood)
                .withDatenschutzerklaerungAkzeptiert(akzeptiert)
                .withDatenschutzerklaerungText(datenschutzerklaerungText)
                .build();

        int hashCodeOther = viewDataOther.hashCode();
        Assertions.assertNotEquals(viewData.hashCode(), hashCodeOther);
    }

    @Test
    public void testHashCodeWithOtherReadAndUnderstood() {
        boolean readAndUnderstoodOther = true;
        viewDataOther = DatenschutzerklaerungViewData.builder().forUser(user)
                .withDatenschutzerklaerungReadAndUnderstood(readAndUnderstoodOther)
                .withDatenschutzerklaerungAkzeptiert(akzeptiert)
                .withDatenschutzerklaerungText(datenschutzerklaerungText)
                .build();

        int hashCodeOther = viewDataOther.hashCode();
        Assertions.assertNotEquals(viewData.hashCode(), hashCodeOther);
    }

    @Test
    public void testHashCodeWithOtherAkzeptiert() {
        boolean akzeptiertOther = true;
        viewDataOther = DatenschutzerklaerungViewData.builder().forUser(user)
                .withDatenschutzerklaerungReadAndUnderstood(readAndUnderstood)
                .withDatenschutzerklaerungAkzeptiert(akzeptiertOther)
                .withDatenschutzerklaerungText(datenschutzerklaerungText)
                .build();

        int hashCodeOther = viewDataOther.hashCode();
        Assertions.assertNotEquals(viewData.hashCode(), hashCodeOther);
    }

    @Test
    public void testHashCodeWithOtherDatenschutzerklaerungText() {
        String datenschutzerklaerungTextOther = "Other privacy agreement";
        viewDataOther = DatenschutzerklaerungViewData.builder().forUser(user)
                .withDatenschutzerklaerungReadAndUnderstood(readAndUnderstood)
                .withDatenschutzerklaerungAkzeptiert(akzeptiert)
                .withDatenschutzerklaerungText(datenschutzerklaerungTextOther)
                .build();

        int hashCodeOther = viewDataOther.hashCode();
        Assertions.assertNotEquals(viewData.hashCode(), hashCodeOther);
    }

    @Test
    public void testEqualsSame() {
        viewDataOther = DatenschutzerklaerungViewData.builder().forUser(user)
                .withDatenschutzerklaerungReadAndUnderstood(readAndUnderstood)
                .withDatenschutzerklaerungAkzeptiert(akzeptiert)
                .withDatenschutzerklaerungText(datenschutzerklaerungText)
                .build();

        boolean isEquals = viewData.equals(viewDataOther);
        Assertions.assertTrue(isEquals);
    }

    @Test
    public void testEqualsWithOtherUser() {
        Mitarbeiter otherUser = TestDataFactory.generateMitarbeiter("Cody", "Coleman", "Abt.-2");
        viewDataOther = DatenschutzerklaerungViewData.builder().forUser(otherUser)
                .withDatenschutzerklaerungReadAndUnderstood(readAndUnderstood)
                .withDatenschutzerklaerungAkzeptiert(akzeptiert)
                .withDatenschutzerklaerungText(datenschutzerklaerungText)
                .build();

        boolean isEquals = viewData.equals(viewDataOther);
        Assertions.assertFalse(isEquals);
    }

    @Test
    public void testEqualsWithOtherReadAndUnderstood() {
        boolean readAndUnderstoodOther = true;
        viewDataOther = DatenschutzerklaerungViewData.builder().forUser(user)
                .withDatenschutzerklaerungReadAndUnderstood(readAndUnderstoodOther)
                .withDatenschutzerklaerungAkzeptiert(akzeptiert)
                .withDatenschutzerklaerungText(datenschutzerklaerungText)
                .build();

        boolean isEquals = viewData.equals(viewDataOther);
        Assertions.assertFalse(isEquals);
    }

    @Test
    public void testEqualsWithOtherAkzeptiert() {
        boolean akzeptiertOther = true;
        viewDataOther = DatenschutzerklaerungViewData.builder().forUser(user)
                .withDatenschutzerklaerungReadAndUnderstood(readAndUnderstood)
                .withDatenschutzerklaerungAkzeptiert(akzeptiertOther)
                .withDatenschutzerklaerungText(datenschutzerklaerungText)
                .build();

        boolean isEquals = viewData.equals(viewDataOther);
        Assertions.assertFalse(isEquals);
    }

    @Test
    public void testEqualsWithOtherDatenschutzerklaerungText() {
        String datenschutzerklaerungTextOther = "Other privacy agreement";
        viewDataOther = DatenschutzerklaerungViewData.builder().forUser(user)
                .withDatenschutzerklaerungReadAndUnderstood(readAndUnderstood)
                .withDatenschutzerklaerungAkzeptiert(akzeptiert)
                .withDatenschutzerklaerungText(datenschutzerklaerungTextOther)
                .build();

        boolean isEquals = viewData.equals(viewDataOther);
        Assertions.assertFalse(isEquals);
    }

    @Test
    public void testGetUser() {
        Assertions.assertEquals(user, viewData.getUser());
    }

    @Test
    public void testIsDatenschutzerklaerungAkzeptiert() {
        Assertions.assertEquals(akzeptiert, viewData.isDatenschutzerklaerungAkzeptiert());
    }

    @Test
    public void testGetDatenschutzerklaerungText() {
        Assertions.assertEquals(datenschutzerklaerungText, viewData.getDatenschutzerklaerungText());
    }

    @Test
    public void testIsDatenschutzerklaerungReadAndUnderstood() {
        Assertions.assertEquals(readAndUnderstood, viewData.isDatenschutzerklaerungReadAndUnderstood());
    }

    @Test
    public void testSetDatenschutzerklaerungReadAndUnderstood() {
        boolean newValue = !readAndUnderstood;
        viewData.setDatenschutzerklaerungReadAndUnderstood(newValue);
        Assertions.assertEquals(newValue, viewData.isDatenschutzerklaerungReadAndUnderstood());
    }

}
