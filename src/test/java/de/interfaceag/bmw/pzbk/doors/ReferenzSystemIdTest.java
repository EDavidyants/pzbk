package de.interfaceag.bmw.pzbk.doors;

import de.interfaceag.bmw.pzbk.enums.ReferenzSystem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ReferenzSystemIdTest {

    private static final String FAILMESSAGE = "The id must not be changed! If this id changes the data in the database is invalid!";

    @Test
    void ReferenzSystemDOORSIdTest() {
        int id = ReferenzSystem.DOORS.getId();
        Assertions.assertEquals(1, id, FAILMESSAGE);
    }
}
