package de.interfaceag.bmw.pzbk.themenklammer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
class ThemenklammerTestdataServiceTest {

    @Mock
    private ThemenklammerService themenklammerService;
    @InjectMocks
    private ThemenklammerTestdataService themenklammerTestdataService;

    @Mock
    private List<ThemenklammerDto> themenklammern;

    @BeforeEach
    void setUp() {
    }

    @Test
    void generateTestdataNoEntriesPresent() {
        when(themenklammerService.getAll()).thenReturn(Collections.emptyList());
        themenklammerTestdataService.generateTestdata();
        verify(themenklammerService, times(1)).saveDtos(any());
    }

    @Test
    void generateTestdataEntryPresent() {
        when(themenklammerService.getAll()).thenReturn(themenklammern);
        when(themenklammern.isEmpty()).thenReturn(false);
        themenklammerTestdataService.generateTestdata();
        verify(themenklammerService, never()).saveDtos(any());
    }
}