package de.interfaceag.bmw.pzbk.themenklammer;

import de.interfaceag.bmw.pzbk.entities.Themenklammer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsIterableContaining.hasItem;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ThemenklammerServiceTest {

    @Mock
    private ThemenklammerDao themenklammerDao;
    @InjectMocks
    private ThemenklammerService themenklammerService;

    @Mock
    private Themenklammer themenklammer;
    private List<Themenklammer> themenklammern;

    private ThemenklammerDto themenklammerDto;
    private Collection<ThemenklammerDto> dtos;

    @BeforeEach
    void setUp() {
        themenklammern = Collections.singletonList(themenklammer);
    }

    @Test
    void saveDtosNewEntry() {
        themenklammerDto = new ThemenklammerDto(null, "Bezeichnung");
        dtos = Collections.singletonList(themenklammerDto);
        themenklammerService.saveDtos(dtos);
        verify(themenklammerDao, never()).find(any());
        verify(themenklammerDao, times(1)).save(any());
    }

    @Test
    void saveDtosNewEntryWithoutBezeichnung() {
        themenklammerDto = new ThemenklammerDto(null, " ");
        dtos = Collections.singletonList(themenklammerDto);
        themenklammerService.saveDtos(dtos);
        verify(themenklammerDao, never()).find(any());
        verify(themenklammerDao, never()).save(any());
    }

    @Test
    void saveDtosNewEntryWithBezeichnung() {
        themenklammerDto = new ThemenklammerDto(null, "Bezeichnung");
        dtos = Collections.singletonList(themenklammerDto);
        themenklammerService.saveDtos(dtos);
        verify(themenklammerDao, never()).find(any());
        verify(themenklammerDao, times(1)).save(any());
    }

    @Test
    void saveDtosExistingEntryUnchanged() {
        themenklammerDto = new ThemenklammerDto(42L, "Bezeichnung");
        dtos = Collections.singletonList(themenklammerDto);
        themenklammerService.saveDtos(dtos);
        verify(themenklammerDao, never()).find(any());
        verify(themenklammerDao, never()).save(any());
    }

    @Test
    void saveDtosExistingEntryChanged() {
        themenklammerDto = new ThemenklammerDto(42L, "Bezeichnung");
        themenklammerDto.setBezeichnung("NEU");
        dtos = Collections.singletonList(themenklammerDto);
        when(themenklammerDao.find(any())).thenReturn(Optional.of(themenklammer));

        themenklammerService.saveDtos(dtos);
        verify(themenklammerDao, times(1)).find(any());
        verify(themenklammerDao, times(1)).save(any());
        verify(themenklammer, times(1)).setBezeichnung("NEU");
    }

    @Test
    void getAllResultSize() {
        when(themenklammer.getId()).thenReturn(42L);
        when(themenklammer.getBezeichnung()).thenReturn("Bezeichnung");
        when(themenklammerDao.getAll()).thenReturn(themenklammern);
        final List<ThemenklammerDto> all = themenklammerService.getAll();
        assertThat(all, hasSize(1));
    }

    @Test
    void getAllResultValue() {
        when(themenklammer.getId()).thenReturn(42L);
        when(themenklammer.getBezeichnung()).thenReturn("Bezeichnung");
        when(themenklammerDao.getAll()).thenReturn(themenklammern);
        final List<ThemenklammerDto> all = themenklammerService.getAll();
        assertThat(all, hasItem(new ThemenklammerDto(42L, "Bezeichnung")));
    }
}