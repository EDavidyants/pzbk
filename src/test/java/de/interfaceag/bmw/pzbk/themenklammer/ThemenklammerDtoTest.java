package de.interfaceag.bmw.pzbk.themenklammer;

import de.interfaceag.bmw.pzbk.shared.objectIds.ThemenklammerId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ThemenklammerDtoTest {

    private static final String IKEA = "IKEA";
    private static final long ID = 42L;

    private ThemenklammerDto themenklammerDto;

    @BeforeEach
    void setUp() {
        themenklammerDto = new ThemenklammerDto();
    }

    @Test
    void toString1() {
        themenklammerDto = new ThemenklammerDto(ID, IKEA);
        final String result = themenklammerDto.toString();
        assertThat(result, is("ThemenklammerDto{id=42, currentBezeichnung='IKEA', bezeichnung='IKEA'}"));
    }

    @Test
    void toStringAfterChange() {
        themenklammerDto = new ThemenklammerDto(ID, IKEA);
        themenklammerDto.setBezeichnung("NEU");
        final String result = themenklammerDto.toString();
        assertThat(result, is("ThemenklammerDto{id=42, currentBezeichnung='IKEA', bezeichnung='NEU'}"));
    }

    @Test
    void isNewWithoutIdAndWithoutBezeichnung() {
        final boolean result = themenklammerDto.isNew();
        assertFalse(result);
    }

    @Test
    void isNewWithoutIdAndWithBezeichnung() {
        themenklammerDto.setBezeichnung("NEU");
        final boolean result = themenklammerDto.isNew();
        assertTrue(result);
    }

    @Test
    void isNewWithoutIdAndWithSpace() {
        themenklammerDto.setBezeichnung(" ");
        final boolean result = themenklammerDto.isNew();
        assertFalse(result);
    }

    @Test
    void isNewWithIdAndWithoutBezeichnung() {
        themenklammerDto = new ThemenklammerDto(ID, null);
        final boolean result = themenklammerDto.isNew();
        assertFalse(result);
    }

    @Test
    void isNewWithIdAndWithBezeichnung() {
        themenklammerDto = new ThemenklammerDto(ID, IKEA);
        final boolean result = themenklammerDto.isNew();
        assertFalse(result);
    }

    @Test
    void isChangedWithoutId() {
        themenklammerDto = new ThemenklammerDto();
        final boolean result = themenklammerDto.isChanged();
        assertFalse(result);
    }

    @Test
    void isChangedWithoutChange() {
        themenklammerDto = new ThemenklammerDto(ID, IKEA);
        final boolean result = themenklammerDto.isChanged();
        assertFalse(result);
    }

    @Test
    void isChangedWithChange() {
        themenklammerDto = new ThemenklammerDto(ID, IKEA);
        themenklammerDto.setBezeichnung("NEU");
        final boolean result = themenklammerDto.isChanged();
        assertTrue(result);
    }

    @Test
    void getThemenklammerId() {
        themenklammerDto = new ThemenklammerDto(ID, IKEA);
        final ThemenklammerId themenklammerId = themenklammerDto.getThemenklammerId();
        assertThat(themenklammerId, is(new ThemenklammerId(ID)));
    }

    @Test
    void getId() {
        themenklammerDto = new ThemenklammerDto(ID, IKEA);
        final Long id = themenklammerDto.getId();
        assertThat(id, is(ID));
    }

    @Test
    void getBezeichnungDefault() {
        final String bezeichnung = themenklammerDto.getBezeichnung();
        assertThat(bezeichnung, nullValue());
    }

    @Test
    void getBezeichnung() {
        themenklammerDto = new ThemenklammerDto(IKEA);
        final String bezeichnung = themenklammerDto.getBezeichnung();
        assertThat(bezeichnung, is(IKEA));
    }

    @Test
    void hashCode1() {
        final int hashCode = themenklammerDto.hashCode();
        assertThat(hashCode, is(861101));
    }
}
