package de.interfaceag.bmw.pzbk.themenklammer;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.entities.Themenklammer;
import de.interfaceag.bmw.pzbk.shared.objectIds.ThemenklammerId;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ThemenklammerDaoIT extends IntegrationTest {

    private ThemenklammerDao themenklammerDao = new ThemenklammerDao();

    @BeforeEach
    void setUp() throws IllegalAccessException {
        // start transaction before test case
        super.begin();

        // override field entityManager in themenklammerDao with private access
        FieldUtils.writeField(themenklammerDao, "entityManager", super.getEntityManager(), true);

        // write entity to h2 database
        themenklammerDao.save(new Themenklammer("1"));
    }

    @Test
    void find() {
        final Optional<Themenklammer> optionalThemenklammer = themenklammerDao.find(new ThemenklammerId(1L));

        assertTrue(optionalThemenklammer.isPresent());

        final Themenklammer themenklammer = optionalThemenklammer.get();

        assertThat(themenklammer.getId(), is(1L));
        assertThat(themenklammer.getBezeichnung(), is("1"));
    }

    @Test
    void saveUpdate() {
        final Optional<Themenklammer> optionalThemenklammer = themenklammerDao.find(new ThemenklammerId(1L));
        assertTrue(optionalThemenklammer.isPresent());
        final Themenklammer themenklammer = optionalThemenklammer.get();
        themenklammer.setBezeichnung("TEST");
        themenklammerDao.save(themenklammer);
        final Optional<Themenklammer> optionalThemenklammer2 = themenklammerDao.find(new ThemenklammerId(1L));
        assertTrue(optionalThemenklammer2.isPresent());
        final Themenklammer themenklammer2 = optionalThemenklammer2.get();
        assertThat(themenklammer2.getBezeichnung(), is("TEST"));
    }

    @Test
    void getAll() {
        final List<Themenklammer> all = themenklammerDao.getAll();
        assertThat(all, hasSize(1));
    }

}