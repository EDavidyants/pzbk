package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.dao.AnforderungDao;
import de.interfaceag.bmw.pzbk.dao.DerivatDao;
import de.interfaceag.bmw.pzbk.dao.ModulDao;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungModulSeTeamFahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatFahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.Fahrzeugmerkmal;
import de.interfaceag.bmw.pzbk.entities.FahrzeugmerkmalAuspraegung;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalAuspraegungDao;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.FahrzeugmerkmalDao;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungDao;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.derivatzuordnung.FahrzeugmerkmalDerivatZuordnungDao;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import de.interfaceag.bmw.pzbk.shared.objectIds.DerivatId;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.assertj.core.api.Java6Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class ZuordnungAutomaticProcessDaoIT extends IntegrationTest {

    // --------------------- DAOs ---------------------
    private ZuordnungAutomaticProcessDao zuordnungAutomaticProcessDao = new ZuordnungAutomaticProcessDao();
    private FahrzeugmerkmalDerivatZuordnungDao derivatFahrzeugmerkmalDao = new FahrzeugmerkmalDerivatZuordnungDao();
    private FahrzeugmerkmalDao fahrzeugmerkmalDao = new FahrzeugmerkmalDao();
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungDao anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao = new AnforderungModulSeTeamFahrzeugmerkmalAuspraegungDao();
    private DerivatDao derivatDao = new DerivatDao();
    private FahrzeugmerkmalAuspraegungDao fahrzeugmerkmalAuspraegungDao = new FahrzeugmerkmalAuspraegungDao();
    private ModulDao modulDao = new ModulDao();
    private AnforderungDao anforderungDao = new AnforderungDao();

    // -------------- Global Variables --------------

    private Derivat derivat;
    private Fahrzeugmerkmal fahrzeugmerkmal;
    private List<FahrzeugmerkmalAuspraegung> auspraegungen = new ArrayList<>();
    private DerivatFahrzeugmerkmal derivatFahrzeugmerkmal;
    private Anforderung anforderung;
    private Anforderung anforderungWithoutMatchingAuspraegung;
    private ModulSeTeam modulSeTeam;
    private Derivat derivatWithoutAuspraegungen;
    private Derivat derivatNotRelevant;
    private DerivatFahrzeugmerkmal derivatFahrzeugmerkmalWithoutAuspraegungen;
    private DerivatFahrzeugmerkmal derivatFahrzeugmerkmalIsNotRelevant;


    @BeforeEach
    void setUp() throws IllegalAccessException {
        super.begin();

        FieldUtils.writeField(zuordnungAutomaticProcessDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(derivatFahrzeugmerkmalDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(fahrzeugmerkmalDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(fahrzeugmerkmalAuspraegungDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(derivatDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(modulDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(anforderungDao, "entityManager", super.getEntityManager(), true);

        if (!isInitAlreadyRun()) {
            createAnforderung();
            createModulSeTeam();
            createFahrzeugmerkmal();
            createFahrzeugmerkmalAuspraegung();
            createFahrzeugmerkmalDerivatZuordnung();
            createAnforderungModulSeTeamFahrzeugmerkmalAuspraegung();
        }

        super.commit();
        super.begin();
    }

    private void createAnforderung() {
        anforderung = new Anforderung("A1", 1, "Kommentar");
        anforderungDao.persistAnforderung(anforderung);

        anforderung = anforderungDao.getUniqueAnforderungByFachId("A1");

        anforderungWithoutMatchingAuspraegung = new Anforderung("A2", 1, "Kommentar2");
        anforderungDao.persistAnforderung(anforderungWithoutMatchingAuspraegung);

        anforderungWithoutMatchingAuspraegung = anforderungDao.getUniqueAnforderungByFachId("A2");
    }

    private void createModulSeTeam() {
        ModulKomponente modulKomponente = TestDataFactory.generateModulKomponente();
        modulDao.persistModulKomponente(modulKomponente);
        Set<ModulKomponente> modulKomponentes = new HashSet<>();

        Modul modul = TestDataFactory.generateModul();
        List<Modul> moduls = new ArrayList<>();
        moduls.add(modul);
        modulDao.persistModule(moduls);

        modulKomponentes.add(modulKomponente);
        modulSeTeam = new ModulSeTeam("Modul",
                modulDao.getModulByName("Name"),
                modulKomponentes);

        modulDao.persistSeTeam(modulSeTeam);
        ModulSeTeam result = modulDao.getSeTeamById(1L);

        if (result != null) {
            modulSeTeam = result;
        }
    }

    private void createFahrzeugmerkmalAuspraegung() {
        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung1 = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "Auspraegung 1");
        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung2 = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, "Auspraegung 2");

        fahrzeugmerkmalAuspraegungDao.save(fahrzeugmerkmalAuspraegung1);
        fahrzeugmerkmalAuspraegungDao.save(fahrzeugmerkmalAuspraegung2);

        auspraegungen.addAll(fahrzeugmerkmalAuspraegungDao.getAll());

    }

    private void createFahrzeugmerkmal() {
        fahrzeugmerkmal = new Fahrzeugmerkmal("Fahrzeugmerkmal");
        fahrzeugmerkmalDao.save(fahrzeugmerkmal);

        fahrzeugmerkmal = fahrzeugmerkmalDao.getAll().get(0);
    }

    private void createFahrzeugmerkmalDerivatZuordnung() {
        derivat = new Derivat("Derivat", "Bezeichnung", "Produktlinie");
        derivatDao.persistDerivat(derivat);
        derivat = derivatDao.getDerivatByName("Derivat");

        derivatFahrzeugmerkmal = new DerivatFahrzeugmerkmal(derivat, fahrzeugmerkmal, auspraegungen);
        derivatFahrzeugmerkmalDao.save(derivatFahrzeugmerkmal);


        derivatWithoutAuspraegungen = new Derivat("Empty Derivat", "Bezeichnung2", "Produktlinie2");
        derivatDao.persistDerivat(derivatWithoutAuspraegungen);
        derivatWithoutAuspraegungen = derivatDao.getDerivatByName("Empty Derivat");

        derivatFahrzeugmerkmalWithoutAuspraegungen = new DerivatFahrzeugmerkmal(derivatWithoutAuspraegungen, fahrzeugmerkmal, Collections.emptyList());
        derivatFahrzeugmerkmalDao.save(derivatFahrzeugmerkmalWithoutAuspraegungen);

        derivatNotRelevant = new Derivat("DerivatNotRelevant", "Bezeichnung3", "Produktlinie3");
        derivatDao.persistDerivat(derivatNotRelevant);
        derivatNotRelevant = derivatDao.getDerivatByName("DerivatNotRelevant");

        derivatFahrzeugmerkmalIsNotRelevant = new DerivatFahrzeugmerkmal(derivatNotRelevant, fahrzeugmerkmal, Collections.emptyList());
        derivatFahrzeugmerkmalIsNotRelevant.setNotRelevant(true);
        derivatFahrzeugmerkmalDao.save(derivatFahrzeugmerkmalIsNotRelevant);

    }

    private void createAnforderungModulSeTeamFahrzeugmerkmalAuspraegung() {
        AnforderungModulSeTeamFahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegung = new AnforderungModulSeTeamFahrzeugmerkmalAuspraegung(
                anforderung, modulSeTeam, auspraegungen.get(0)
        );
        anforderungModulSeTeamFahrzeugmerkmalAuspraegungDao.save(anforderungModulSeTeamFahrzeugmerkmalAuspraegung);

    }

    private boolean isInitAlreadyRun() {
        return anforderungDao.getAllAnforderung().size() > 0;
    }

    @Test
    void testDerivatAndAnforderungMatchOneAuspraegung() {
        List<Anforderung> anforderungForFahrzeugmerkmalAndAuspraegungen = zuordnungAutomaticProcessDao.getAnforderungForFahrzeugmerkmalAndAuspraegungen(new DerivatId(derivat.getId()), Collections.singletonList(new AnforderungId(anforderung.getId())));
        Java6Assertions.assertThat(anforderungForFahrzeugmerkmalAndAuspraegungen).contains(anforderung);
    }

    @Test
    void testDerivatAndAnforderungMatchNoAuspraegung() {
        List<Anforderung> anforderungForFahrzeugmerkmalAndAuspraegungen = zuordnungAutomaticProcessDao.getAnforderungForFahrzeugmerkmalAndAuspraegungen(new DerivatId(derivat.getId()), Collections.singletonList(new AnforderungId(anforderungWithoutMatchingAuspraegung.getId())));
        Java6Assertions.assertThat(anforderungForFahrzeugmerkmalAndAuspraegungen).isEmpty();
    }

    @Test
    void testListOfAnforderungIdsIsEmpty() {
        List<Anforderung> anforderungForFahrzeugmerkmalAndAuspraegungen = zuordnungAutomaticProcessDao.getAnforderungForFahrzeugmerkmalAndAuspraegungen(new DerivatId(derivat.getId()), Collections.emptyList());
        Java6Assertions.assertThat(anforderungForFahrzeugmerkmalAndAuspraegungen).isEmpty();
    }

    @Test
    void testDerivatWithoutAuspraegungen() {
        List<Anforderung> anforderungForFahrzeugmerkmalAndAuspraegungen = zuordnungAutomaticProcessDao.getAnforderungForFahrzeugmerkmalAndAuspraegungen(new DerivatId(derivatWithoutAuspraegungen.getId()), Collections.singletonList(new AnforderungId(anforderung.getId())));
        Java6Assertions.assertThat(anforderungForFahrzeugmerkmalAndAuspraegungen).isEmpty();
    }

    @Test
    void testDerivatFahrzeugmerkmalIsNotRelevant() {
        List<Anforderung> anforderungForFahrzeugmerkmalAndAuspraegungen = zuordnungAutomaticProcessDao.getAnforderungForFahrzeugmerkmalAndAuspraegungen(new DerivatId(derivatNotRelevant.getId()), Collections.singletonList(new AnforderungId(anforderung.getId())));
        Java6Assertions.assertThat(anforderungForFahrzeugmerkmalAndAuspraegungen).isEmpty();
    }

}