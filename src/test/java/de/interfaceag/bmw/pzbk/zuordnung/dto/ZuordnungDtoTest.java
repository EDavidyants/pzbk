package de.interfaceag.bmw.pzbk.zuordnung.dto;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.dto.ProzessbaukastenLabelDto;
import de.interfaceag.bmw.pzbk.vereinbarung.dto.ProzessbaukastenLinkData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ZuordnungDtoTest {

    private static final Long ANFORDERUNGID = 1L;
    private static final String ANFORDERUNGLABEL = "A12 | 1";
    private static final String ANFORDERUNGFACHID = "A12";
    private static final Integer ANFORDERUNGVERSION = 1;
    private static final String ANFORDERUNGBESCHREIBUNG = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonu";
    private static final String ANFORDERUNGBESCHREIBUNGSHORT = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam n...";
    private static final boolean STANDARDVEREINBARUNG = true;

    private ProzessbaukastenLabelDto prozessbaukastenLabelDto;

    @Mock
    List<ProzessbaukastenLinkData> prozessbaukastenLinkData;
    Map<DerivatDto, DerivatZuordnung> derivatZuordnungen = new HashMap<>();

    @Mock
    DerivatDto derivat;
    @Mock
    DerivatZuordnung derivatZuordnung;

    ZuordnungDto zuordnung;

    @BeforeEach
    public void setUp() {
        prozessbaukastenLabelDto = new ProzessbaukastenLabelDto(1L, "P123", 1);
        prozessbaukastenLinkData = new ArrayList<>();
        derivatZuordnungen.put(derivat, derivatZuordnung);
        zuordnung = new ZuordnungDto(ANFORDERUNGID, ANFORDERUNGLABEL, ANFORDERUNGFACHID, ANFORDERUNGVERSION,
                ANFORDERUNGBESCHREIBUNG, STANDARDVEREINBARUNG, derivatZuordnungen, prozessbaukastenLabelDto);
    }

    @Test
    public void testGetAnforderungId() {
        Long result = zuordnung.getAnforderungId();
        assertThat(ANFORDERUNGID, is(result));
    }

    @Test
    public void testGetAnforderungLabel() {
        String result = zuordnung.getAnforderungLabel();
        assertThat(ANFORDERUNGLABEL, is(result));
    }

    @Test
    public void testGetAnforderungFachId() {
        String result = zuordnung.getAnforderungFachId();
        assertThat(ANFORDERUNGFACHID, is(result));
    }

    @Test
    public void testGetAnforderungVersion() {
        Integer result = zuordnung.getAnforderungVersion();
        assertThat(ANFORDERUNGVERSION, is(result));
    }

    @Test
    public void testGetAnforderungBeschreibungFull() {
        String result = zuordnung.getAnforderungBeschreibungFull();
        assertThat(ANFORDERUNGBESCHREIBUNG, is(result));
    }

    @Test
    public void testGetAnforderungBeschreibungShort() {
        String result = zuordnung.getAnforderungBeschreibungShort();
        assertThat(ANFORDERUNGBESCHREIBUNGSHORT, is(result));
    }

    @Test
    public void testGetAnforderungBeschreibungShortForShortText() {
        String beschreibung = "Beschreibung";
        zuordnung = new ZuordnungDto(ANFORDERUNGID, ANFORDERUNGLABEL, ANFORDERUNGFACHID, ANFORDERUNGVERSION,
                beschreibung, STANDARDVEREINBARUNG, derivatZuordnungen, null);
        String result = zuordnung.getAnforderungBeschreibungShort();
        assertThat(beschreibung, is(result));
    }

    @Test
    public void testGetProzessbaukasten() {
        boolean result = zuordnung.isProzessbaukastenZugeordnet();
        assertTrue(result);
    }

    @Test
    public void testGetProzessbaukastenLabel() {
        ProzessbaukastenLabelDto prozessbaukastenLabelDto = zuordnung.getProzessbaukastenLabel();
        assertThat(prozessbaukastenLabelDto.getId(), is(1L));
    }

    @Test
    public void testGetProzessbaukastenNull() {
        zuordnung = new ZuordnungDto(ANFORDERUNGID, ANFORDERUNGLABEL, ANFORDERUNGFACHID, ANFORDERUNGVERSION,
                ANFORDERUNGBESCHREIBUNG, STANDARDVEREINBARUNG, derivatZuordnungen, prozessbaukastenLabelDto);
        boolean result = zuordnung.isProzessbaukastenZugeordnet();
        assertTrue(result);
    }

    @Test
    public void testGetZuordnungForDerivat() {
        DerivatZuordnung result = zuordnung.getZuordnungForDerivat(derivat);
        assertThat(derivatZuordnung, is(result));
    }

    @Test
    public void testIsStandardvereinbarung() {
        boolean result = zuordnung.isStandardvereinbarung();
        assertThat(STANDARDVEREINBARUNG, is(result));
    }

}
