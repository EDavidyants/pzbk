package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOrder;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOrderDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOrders;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungOrdersDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungTuple;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenAnforderungenOrderService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.sort.ProzessbaukastenIdAnforderungIdTuple;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import de.interfaceag.bmw.pzbk.zuordnung.dto.ZuordnungDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ZuordnungTableDataSortServiceTest {

    @Mock
    private ProzessbaukastenAnforderungenOrderService prozessbaukastenAnforderungenOrderService;

    @Mock
    private ZuordnungDto zuordnung1;
    @Mock
    private ZuordnungDto zuordnung2;
    @Mock
    private ZuordnungDto zuordnung3;
    @Mock
    private ZuordnungDto zuordnung4;
    @Mock
    private ZuordnungDto zuordnung5;
    @Mock
    private ZuordnungDto zuordnung6;

    @Mock
    private ProzessbaukastenAnforderungOrderDto prozessbaukastenAnforderungOrder1;
    @Mock
    private ProzessbaukastenAnforderungOrderDto prozessbaukastenAnforderungOrder2;
    @Mock
    private ProzessbaukastenAnforderungOrderDto prozessbaukastenAnforderungOrder3;

    @InjectMocks
    private final ZuordnungTableDataSortService zuordnungTableDataSortService = new ZuordnungTableDataSortService();

    @BeforeEach
    public void setUp() {

    }

    @Test
    public void testZuordnungenOrderBeimNachZakMode() {
        when(zuordnung1.getAnforderungFachId()).thenReturn("A123");
        when(zuordnung2.getAnforderungFachId()).thenReturn("A21");
        when(zuordnung3.getAnforderungFachId()).thenReturn("A12");

        final List<Zuordnung> zuordnungen = Arrays.asList(zuordnung1, zuordnung2, zuordnung3);
        List<Zuordnung> sortedZuordnungen = zuordnungTableDataSortService.sortTableData(zuordnungen);

        Assertions.assertEquals("A123", sortedZuordnungen.get(0).getAnforderungFachId());
        Assertions.assertEquals("A21", sortedZuordnungen.get(1).getAnforderungFachId());
        Assertions.assertEquals("A12", sortedZuordnungen.get(2).getAnforderungFachId());
    }

    @Test
    public void testZuordnungenOrderBeimZuordnenMode() {
        when(zuordnung1.getAnforderungFachId()).thenReturn("A12");
        when(zuordnung2.getAnforderungFachId()).thenReturn("A123");
        when(zuordnung3.getAnforderungFachId()).thenReturn("A21");
        when(zuordnung4.getAnforderungFachId()).thenReturn("A11");
        when(zuordnung5.getAnforderungFachId()).thenReturn("A2");
        when(zuordnung6.getAnforderungFachId()).thenReturn("A1");

        when(zuordnung4.getAnforderungId()).thenReturn(11L);
        when(zuordnung5.getAnforderungId()).thenReturn(2L);
        when(zuordnung6.getAnforderungId()).thenReturn(1L);

        when(zuordnung1.isProzessbaukastenZugeordnet()).thenReturn(false);
        when(zuordnung2.isProzessbaukastenZugeordnet()).thenReturn(false);
        when(zuordnung3.isProzessbaukastenZugeordnet()).thenReturn(false);
        when(zuordnung4.isProzessbaukastenZugeordnet()).thenReturn(true);
        when(zuordnung5.isProzessbaukastenZugeordnet()).thenReturn(true);
        when(zuordnung6.isProzessbaukastenZugeordnet()).thenReturn(true);

        when(zuordnung1.getProzessbaukastenId()).thenReturn(Optional.empty());
        when(zuordnung2.getProzessbaukastenId()).thenReturn(Optional.empty());
        when(zuordnung3.getProzessbaukastenId()).thenReturn(Optional.empty());
        when(zuordnung4.getProzessbaukastenId()).thenReturn(Optional.ofNullable(2L));
        when(zuordnung5.getProzessbaukastenId()).thenReturn(Optional.ofNullable(1L));
        when(zuordnung6.getProzessbaukastenId()).thenReturn(Optional.ofNullable(1L));

        ProzessbaukastenAnforderungTuple prozessbaukastenAnforderungTuple1 = new ProzessbaukastenIdAnforderungIdTuple(11L, 2L);
        ProzessbaukastenAnforderungTuple prozessbaukastenAnforderungTuple2 = new ProzessbaukastenIdAnforderungIdTuple(2L, 1L);
        ProzessbaukastenAnforderungTuple prozessbaukastenAnforderungTuple3 = new ProzessbaukastenIdAnforderungIdTuple(1L, 1L);

        when(prozessbaukastenAnforderungOrder2.getPosition()).thenReturn(2L);
        when(prozessbaukastenAnforderungOrder3.getPosition()).thenReturn(1L);

        Map<ProzessbaukastenAnforderungTuple, ProzessbaukastenAnforderungOrder> prozessbaukastenAnforderungOrdersMap = new HashMap<>();
        prozessbaukastenAnforderungOrdersMap.put(prozessbaukastenAnforderungTuple1, prozessbaukastenAnforderungOrder1);
        prozessbaukastenAnforderungOrdersMap.put(prozessbaukastenAnforderungTuple2, prozessbaukastenAnforderungOrder2);
        prozessbaukastenAnforderungOrdersMap.put(prozessbaukastenAnforderungTuple3, prozessbaukastenAnforderungOrder3);

        ProzessbaukastenAnforderungOrders prozessbaukastenAnforderungOrders = new ProzessbaukastenAnforderungOrdersDto(prozessbaukastenAnforderungOrdersMap);
        when(prozessbaukastenAnforderungenOrderService.getAnforderungenOrderForProzessbaukasten(any())).thenReturn(prozessbaukastenAnforderungOrders);

        final List<Zuordnung> zuordnungen = Arrays.asList(zuordnung1, zuordnung2, zuordnung3, zuordnung4, zuordnung5, zuordnung6);
        List<Zuordnung> sortedZuordnungen = zuordnungTableDataSortService.sortTableData(zuordnungen);

        Assertions.assertEquals("A1", sortedZuordnungen.get(0).getAnforderungFachId());
        Assertions.assertEquals("A2", sortedZuordnungen.get(1).getAnforderungFachId());
        Assertions.assertEquals("A11", sortedZuordnungen.get(2).getAnforderungFachId());
        Assertions.assertEquals("A123", sortedZuordnungen.get(3).getAnforderungFachId());
        Assertions.assertEquals("A21", sortedZuordnungen.get(4).getAnforderungFachId());
        Assertions.assertEquals("A12", sortedZuordnungen.get(5).getAnforderungFachId());
    }

}
