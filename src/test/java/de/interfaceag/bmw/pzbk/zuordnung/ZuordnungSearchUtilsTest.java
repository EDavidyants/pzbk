package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.filter.ThemenklammerFilter;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPart;
import de.interfaceag.bmw.pzbk.shared.dto.QueryPartDTO;
import org.hamcrest.collection.IsMapContaining;
import org.hamcrest.collection.IsMapWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapWithSize.anEmptyMap;
import static org.hamcrest.text.IsEmptyString.emptyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ZuordnungSearchUtilsTest {

    private QueryPart queryPart;
    @Mock
    private ThemenklammerFilter themenklammerFilter;
    private Set<Long> ids;

    @BeforeEach
    void setUp() {
        ids = Collections.singleton(42L);
        queryPart = new QueryPartDTO();
    }

    @Test
    void getThemenklammerQueryInactive() {
        when(themenklammerFilter.isActive()).thenReturn(false);
        ZuordnungSearchUtils.getThemenklammerQuery(queryPart, themenklammerFilter);
        assertThat(queryPart.getQuery(), emptyString());
    }

    @Test
    void getThemenklammerQueryActive() {
        when(themenklammerFilter.isActive()).thenReturn(true);
        ZuordnungSearchUtils.getThemenklammerQuery(queryPart, themenklammerFilter);
        assertThat(queryPart.getQuery(), is(" AND pt.themenklammer.id IN :themenklammerIds"));
    }

    @Test
    void getThemenklammerQueryParameterMapInactive() {
        when(themenklammerFilter.isActive()).thenReturn(false);
        ZuordnungSearchUtils.getThemenklammerQuery(queryPart, themenklammerFilter);
        assertThat(queryPart.getParameterMap(), anEmptyMap());
    }

    @Test
    void getThemenklammerQueryParameterMapSizeActive() {
        when(themenklammerFilter.isActive()).thenReturn(true);
        ZuordnungSearchUtils.getThemenklammerQuery(queryPart, themenklammerFilter);
        assertThat(queryPart.getParameterMap(), IsMapWithSize.aMapWithSize(1));
    }

    @Test
    void getThemenklammerQueryParameterMapValueActive() {
        when(themenklammerFilter.getSelectedIdsAsSet()).thenReturn(ids);
        when(themenklammerFilter.isActive()).thenReturn(true);
        ZuordnungSearchUtils.getThemenklammerQuery(queryPart, themenklammerFilter);
        assertThat(queryPart.getParameterMap(), IsMapContaining.hasEntry("themenklammerIds", ids));
    }
}