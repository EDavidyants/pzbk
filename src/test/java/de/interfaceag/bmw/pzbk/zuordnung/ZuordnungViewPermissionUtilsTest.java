package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.when;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ExtendWith(MockitoExtension.class)
public class ZuordnungViewPermissionUtilsTest {

    private static final String FALSEMESSAGE = "edit button should not be visible";
    private static final String TRUEMESSAGE = "edit button should be visible";

    @Mock
    DerivatDto derivat;

    Set<Rolle> roles;

    @BeforeEach
    public void setup() {
        roles = Stream.of(Rolle.ADMIN).collect(Collectors.toSet());
    }

    @Test
    public void getViewPermissionTrueTrue() {
        ZuordnungViewPermission viewPermission = ZuordnungViewPermissionUtils.getViewPermission(roles, true, true, derivat);
        Assertions.assertFalse(viewPermission.isEditButton(), FALSEMESSAGE);
    }

    @Test
    public void getViewPermissionTrueFalse() {
        ZuordnungViewPermission viewPermission = ZuordnungViewPermissionUtils.getViewPermission(roles, true, false, derivat);
        Assertions.assertFalse(viewPermission.isEditButton(), FALSEMESSAGE);
    }

    @Test
    public void getViewPermissionFalseTrue() {
        ZuordnungViewPermission viewPermission = ZuordnungViewPermissionUtils.getViewPermission(roles, false, true, derivat);
        Assertions.assertFalse(viewPermission.isEditButton(), FALSEMESSAGE);
    }

    @Test
    public void getViewPermissionFalseFalse() {
        when(derivat.isVereinbarungActive()).thenReturn(Boolean.TRUE);
        ZuordnungViewPermission viewPermission = ZuordnungViewPermissionUtils.getViewPermission(roles, false, false, derivat);
        Assertions.assertTrue(viewPermission.isEditButton(), TRUEMESSAGE);
    }

}
