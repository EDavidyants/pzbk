package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.zuordnung.automatic.AutomaticZuordnenDialogViewData;
import de.interfaceag.bmw.pzbk.zuordnung.automatic.ZuordnungAutomaticProcessService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ZuordnungViewFacadeTest {

    @InjectMocks
    private ZuordnungViewFacade zuordnungViewFacade;

    @Mock
    private ZuordnungAutomaticProcessService zuordnungAutomaticProcessService;

    @Mock
    private ZuordnungViewData viewData;


    @Test
    void automatischZuordnen() {
        when(zuordnungAutomaticProcessService.assignAnforderungenAutomatic(any())).thenReturn(AutomaticZuordnenDialogViewData.newBuilder().build());
        AutomaticZuordnenDialogViewData dialogViewData = zuordnungViewFacade.automatischZuordnen(viewData);
        Assert.assertNotNull(dialogViewData);
    }
}