package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import de.interfaceag.bmw.pzbk.zak.ZakVereinbarungService;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ZuordnungProcessZakServiceTest {

    @Mock
    Session session;
    @Mock
    DerivatService derivatService;
    @Mock
    AnforderungService anforderungService;
    @Mock
    DerivatAnforderungModulService derivatAnforderungModulService;
    @Mock
    ZakVereinbarungService zakVereinbarungService;
    @InjectMocks
    ZuordnungProcessZakService zuordnungProcessZakService;

    List<Zuordnung> selectedTableData;

    @Mock
    DerivatDto selectedDerivat;
    @Mock
    Zuordnung zuordnung;
    @Mock
    Zuordnung zuordnung2;
    @Mock
    Derivat derivat;
    @Mock
    Mitarbeiter mitarbeiter;
    @Mock
    Anforderung anforderung;

    @Mock
    List<DerivatAnforderungModul> vereinbarungen;

    @BeforeEach
    public void setUp() {
        selectedTableData = new ArrayList<>();
        selectedTableData.add(zuordnung);
        selectedTableData.add(zuordnung2);

        when(derivatService.getDerivatById(any())).thenReturn(derivat);
        when(anforderungService.getAnforderungById(any())).thenReturn(anforderung);
        when(zuordnung.getAnforderungId()).thenReturn(1L);
        when(zuordnung2.getAnforderungId()).thenReturn(2L);
        when(derivatAnforderungModulService.getDerivatAnforderungModulByAnforderungAndDerivat(anforderung, derivat)).thenReturn(vereinbarungen);
    }

    @Test
    public void testProcessZakChanges() throws Exception {
        zuordnungProcessZakService.processZakChanges(selectedTableData, selectedDerivat);
        verify(zakVereinbarungService, times(2)).sendModuleNachZak(vereinbarungen);
    }

}
