package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.zuordnung.dto.DerivatZuordnung;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ZuordnungProcessChangeServiceTest {

    @Mock
    Session session;
    @Mock
    AnforderungService anforderungService;
    @Mock
    ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;
    @InjectMocks
    ZuordnungProcessChangeService zuordnungProcessChangeService;

    @Mock
    Mitarbeiter user;
    @Mock
    Derivat derivat;

    List<Zuordnung> zuordnungen;

    @Mock
    Zuordnung zuordnung;
    @Mock
    Zuordnung zuordnung2;
    @Mock
    DerivatZuordnung derivatZuordnung;
    @Mock
    Anforderung anforderung;

    @BeforeEach
    public void setUp() {
        zuordnungen = new ArrayList<>();
        zuordnungen.add(zuordnung);
        zuordnungen.add(zuordnung2);
    }

    @Test
    public void testProcessZuordnungChangesInBulkMode() throws Exception {
        when(session.getUser()).thenReturn(user);
        when(anforderungService.getAnforderungById(any())).thenReturn(anforderung);
        zuordnungProcessChangeService.processZuordnungChangesInBulkMode(ZuordnungStatus.ZUGEORDNET, derivat, zuordnungen);
        verify(zuordnungAnforderungDerivatService, times(2)).updateZuordnungAnforderungDerivatStatus(any(), any(), any(), any());
    }

    @Test
    public void testProcessZuordnungChangesInSingleMode() throws Exception {
        when(session.getUser()).thenReturn(user);
        when(anforderungService.getAnforderungById(any())).thenReturn(anforderung);
        when(derivatZuordnung.isChanged()).thenReturn(true);
        when(zuordnung.getZuordnungForDerivat(any())).thenReturn(derivatZuordnung);
        when(zuordnung2.getZuordnungForDerivat(any())).thenReturn(derivatZuordnung);
        when(zuordnung.getAnforderungId()).thenReturn(1L);
        when(zuordnung2.getAnforderungId()).thenReturn(1L);
        when(derivatZuordnung.getStatus()).thenReturn(ZuordnungStatus.ZUGEORDNET);
        zuordnungProcessChangeService.processZuordnungChangesInSingleMode(derivat, zuordnungen);
        verify(zuordnungAnforderungDerivatService, times(2)).updateZuordnungAnforderungDerivatStatus(any(), any(), any(), any());
    }

    @Test
    public void testProcessZuordnungChangesInSingleModeNotChanged() throws Exception {
        when(zuordnung.getZuordnungForDerivat(any())).thenReturn(derivatZuordnung);
        when(zuordnung2.getZuordnungForDerivat(any())).thenReturn(derivatZuordnung);
        when(derivatZuordnung.isChanged()).thenReturn(false);
        zuordnungProcessChangeService.processZuordnungChangesInSingleMode(derivat, zuordnungen);
        verify(zuordnungAnforderungDerivatService, never()).updateZuordnungAnforderungDerivatStatus(any(), any(), any(), any());
    }
}
