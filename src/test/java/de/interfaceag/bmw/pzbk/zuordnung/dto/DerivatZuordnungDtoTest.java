package de.interfaceag.bmw.pzbk.zuordnung.dto;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungZakStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class DerivatZuordnungDtoTest {

    private static final ZuordnungStatus STATUS = ZuordnungStatus.ZUGEORDNET;
    private static final ZuordnungZakStatus ZAKSTATUS = ZuordnungZakStatus.OFFEN;

    @Mock
    DerivatDto derivat;
    @Mock
    DerivatDto otherDerivat;

    DerivatZuordnungDto derivatZuordnung;

    DerivatStatus zuordnungStatus = DerivatStatus.VEREINARBUNG_VKBG;

    @BeforeEach
    public void setUp() {
        derivatZuordnung = new DerivatZuordnungDto(STATUS, derivat, ZAKSTATUS, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, zuordnungStatus, "");
    }

    @Test
    public void testGetStatus() {
        ZuordnungStatus result = derivatZuordnung.getStatus();
        assertThat(STATUS, is(result));
    }

    @Test
    public void testSetStatus() {
        ZuordnungStatus otherStatus = ZuordnungStatus.OFFEN;
        derivatZuordnung.setStatus(otherStatus);
        ZuordnungStatus result = derivatZuordnung.getStatus();
        assertThat(otherStatus, is(result));
    }

    @Test
    public void testGetDerivat() {
        DerivatDto result = derivatZuordnung.getDerivat();
        assertThat(derivat, is(result));
    }

    @Test
    public void testIsStatusChangeEnabledZuordnenModeFalse() {
        when(derivat.getId()).thenReturn(1L);
        boolean result = derivatZuordnung.isStatusChangeEnabled(derivat, Boolean.FALSE);
        assertThat(Boolean.FALSE, is(result));
    }

    @Test
    public void testIsStatusChangeEnabledZuordnenModeTrueAndSameDerivat() {
        when(derivat.getId()).thenReturn(1L);
        when(derivat.getStatus()).thenReturn(DerivatStatus.VEREINARBUNG_VKBG);
        boolean result = derivatZuordnung.isStatusChangeEnabled(derivat, Boolean.TRUE);
        assertThat(Boolean.TRUE, is(result));
    }

    @ParameterizedTest
    @EnumSource(DerivatStatus.class)
    public void testIsStatusChangeEnabledZuordnungModeTrueAndSameDerivatWithStatus(DerivatStatus status) {
        when(derivat.getId()).thenReturn(1L);
        when(derivat.getStatus()).thenReturn(status);
        boolean result = derivatZuordnung.isStatusChangeEnabled(derivat, Boolean.TRUE);
        switch (status) {
            case VEREINARBUNG_VKBG:
            case VEREINBARUNG_ZV:
                assertThat(Boolean.TRUE, is(result));
                break;
            default:
                assertThat(Boolean.FALSE, is(result));
                break;
        }
    }

    @Test
    public void testIsStatusChangeEnabledZuordnenModeTrueAndNotSameDerivat() {
        when(derivat.getId()).thenReturn(1L);
        when(otherDerivat.getId()).thenReturn(2L);
        boolean result = derivatZuordnung.isStatusChangeEnabled(otherDerivat, Boolean.TRUE);
        assertThat(Boolean.FALSE, is(result));
    }

    @ParameterizedTest
    @EnumSource(ZuordnungStatus.class)
    public void testGetNextStatus(ZuordnungStatus status) {
        derivatZuordnung.setStatus(status);
        List<ZuordnungStatus> result = derivatZuordnung.getNextStatus();
        List<ZuordnungStatus> expected = status.getNextStatus();
        assertThat(expected, is(result));
    }

    @Test
    public void testIsZakStatusRenderedStandardvereinbarung() {
        boolean result = derivatZuordnung.isZakStatusRendered();
        assertThat(Boolean.TRUE, is(result));
    }

    @Test
    public void testIsZakStatusRenderedNoStandardvereinbarungAndNachZakUebertragen() {
        derivatZuordnung = new DerivatZuordnungDto(STATUS, derivat, ZAKSTATUS, Boolean.TRUE, Boolean.TRUE, Boolean.FALSE, zuordnungStatus, "");
        boolean result = derivatZuordnung.isZakStatusRendered();
        assertThat(Boolean.TRUE, is(result));
    }

    @Test
    public void testIsZakStatusRenderedStandardvereinbarungAndNotNachZakUebertragen() {
        derivatZuordnung = new DerivatZuordnungDto(STATUS, derivat, ZAKSTATUS, Boolean.TRUE, Boolean.FALSE, Boolean.TRUE, zuordnungStatus, "");
        boolean result = derivatZuordnung.isZakStatusRendered();
        assertThat(Boolean.TRUE, is(result));
    }

    @Test
    public void testIsZakStatusRenderedNoStandardvereinbarungAndNotNachZakUebertragen() {
        derivatZuordnung = new DerivatZuordnungDto(STATUS, derivat, ZAKSTATUS, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE, zuordnungStatus, "");
        boolean result = derivatZuordnung.isZakStatusRendered();
        assertThat(Boolean.FALSE, is(result));
    }

    @Test
    public void testIsZakUebertragungSuccessfulZakUebertragungSuccessfulAndStandardvereinbarung() {
        boolean result = derivatZuordnung.isZakUebertragungSuccessful();
        assertThat(Boolean.TRUE, is(result));
    }

    @Test
    public void testIsZakUebertragungSuccessfulZakUebertragungSuccessfulAndNoStandardvereinbarung() {
        derivatZuordnung = new DerivatZuordnungDto(STATUS, derivat, ZAKSTATUS, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE, zuordnungStatus, "");
        boolean result = derivatZuordnung.isZakUebertragungSuccessful();
        assertThat(Boolean.TRUE, is(result));
    }

    @Test
    public void testIsZakUebertragungSuccessfulZakUebertragungNotSuccessfulAndStandardvereinbarung() {
        derivatZuordnung = new DerivatZuordnungDto(STATUS, derivat, ZAKSTATUS, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, zuordnungStatus, "");
        boolean result = derivatZuordnung.isZakUebertragungSuccessful();
        assertThat(Boolean.TRUE, is(result));
    }

    @Test
    public void testIsZakUebertragungSuccessfulZakUebertragungNotSuccessfulAndNotStandardvereinbarung() {
        derivatZuordnung = new DerivatZuordnungDto(STATUS, derivat, ZAKSTATUS, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, zuordnungStatus, "");
        boolean result = derivatZuordnung.isZakUebertragungSuccessful();
        assertThat(Boolean.FALSE, is(result));
    }

    @Test
    public void testIsNachZakUebertragenFalse() {
        derivatZuordnung = new DerivatZuordnungDto(STATUS, derivat, ZAKSTATUS, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, zuordnungStatus, "");
        boolean result = derivatZuordnung.isZakUebertragungSuccessful();
        assertThat(Boolean.FALSE, is(result));
    }

    @Test
    public void testIsNachZakUebertragenTrue() {
        boolean result = derivatZuordnung.isZakUebertragungSuccessful();
        assertThat(Boolean.TRUE, is(result));
    }

    @Test
    public void testGetZakStatus() {
        ZuordnungZakStatus result = derivatZuordnung.getZakStatus();
        assertThat(ZAKSTATUS, is(result));
    }

    @Test
    public void testGetZakStatusTooltip() {
        String result = derivatZuordnung.getZakStatusTooltip();
        assertThat("", is(result));
    }

    @Test
    public void testIsChangedInitial() {
        boolean result = derivatZuordnung.isChanged();
        assertThat(Boolean.FALSE, is(result));
    }

    @Test
    public void testIsChangedAfterStatusChange() {
        derivatZuordnung.setStatus(ZuordnungStatus.OFFEN);
        boolean result = derivatZuordnung.isChanged();
        assertThat(Boolean.TRUE, is(result));
    }

    @ParameterizedTest
    @EnumSource(DerivatStatus.class)
    public void testGetZeitpunktZuornungForDerivatStatus(DerivatStatus status) {
        derivatZuordnung = new DerivatZuordnungDto(STATUS, derivat, ZAKSTATUS, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, status, "");
        String result = derivatZuordnung.getZeitpunktZuordnung();
        String expected;
        switch (status) {
            case VEREINARBUNG_VKBG:
                expected = "VKBG";
                break;
            case VEREINBARUNG_ZV:
                expected = "ZV";
                break;
            default:
                expected = "";
                break;
        }
        assertThat(expected, is(result));
    }

    @ParameterizedTest
    @EnumSource(DerivatStatus.class)
    public void testIsZeitpunktZuordnungRendered(DerivatStatus status) {
        derivatZuordnung = new DerivatZuordnungDto(STATUS, derivat, ZAKSTATUS, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, status, "");
        boolean result = derivatZuordnung.isZeitpunktZuordnungRendered();
        boolean expected;
        switch (status) {
            case VEREINARBUNG_VKBG:
            case VEREINBARUNG_ZV:
                expected = Boolean.TRUE;
                break;
            default:
                expected = Boolean.FALSE;
                break;
        }
        assertThat(expected, is(result));
    }

}
