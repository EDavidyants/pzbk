package de.interfaceag.bmw.pzbk.zuordnung;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ZuordnungZakStatusUtilsTest {

    ZuordnungViewPermission viewPermission;

    @ParameterizedTest
    @ValueSource(strings = {"noch nicht übertragen", "nach ZAK übertragen", "42"})
    public void pageRolePermissionsEditMode(String s) {

        ZuordnungZakStatus result = ZuordnungZakStatusUtils.getZuordnungZakStatusForString(s);

        switch (s) {
            case "noch nicht übertragen":
                Assertions.assertEquals(ZuordnungZakStatus.OFFEN, result);
                break;
            case "nach ZAK übertragen":
                Assertions.assertEquals(ZuordnungZakStatus.ZAK, result);
                break;
            default:
                Assertions.assertNull(result);
                break;
        }
    }

    @Test
    public void getZuordnungZakStatusForBooleanTrue() {
        ZuordnungZakStatus result = ZuordnungZakStatusUtils.getZuordnungZakStatusForBoolean(true);
        Assertions.assertEquals(ZuordnungZakStatus.ZAK, result);
    }

    @Test
    public void getZuordnungZakStatusForBooleanFalse() {
        ZuordnungZakStatus result = ZuordnungZakStatusUtils.getZuordnungZakStatusForBoolean(false);
        Assertions.assertEquals(ZuordnungZakStatus.OFFEN, result);
    }

    @Test
    public void getAllZuordnungZakStatusCount() {
        long count = ZuordnungZakStatusUtils.getAllZuordnungZakStatus().count();
        Assertions.assertEquals(2L, count, "there should only be two values!");
    }

    @Test
    public void getAllgetAllZuordnungZakStatusAsListSize() {
        int size = ZuordnungZakStatusUtils.getAllgetAllZuordnungZakStatusAsList().size();
        Assertions.assertEquals(2, size, "there should only be two values!");
    }

}
