package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.dao.ZuordnungAnforderungDerivatDao;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.doNothing;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ZuordnungAnforderungDerivatServiceNachZakUebertragungTest {

    @Mock
    private ZuordnungAnforderungDerivatDao zuordnungDao;

    @InjectMocks
    private ZuordnungAnforderungDerivatService zuordnungService;

    private ZuordnungAnforderungDerivat zuordnung;

    @BeforeEach
    public void setUp() {
        zuordnung = TestDataFactory.generateZuordnungAnforderungDerivat();
        doNothing().when(zuordnungDao).persistZuordnungAnforderungDerivat(zuordnung);
    }

    @Test
    public void testSetDerivatAnforderungAsNachZakUebertragenForFehlerhaft() {
        zuordnungService.setDerivatAnforderungAsNachZakUebertragen(zuordnung, true);
        Assertions.assertFalse(zuordnung.isZakUebertragungErfolgreich());
    }

    @Test
    public void testSetDerivatAnforderungAsNachZakUebertragenForErfolgreich() {
        zuordnungService.setDerivatAnforderungAsNachZakUebertragen(zuordnung, false);
        Assertions.assertTrue(zuordnung.isZakUebertragungErfolgreich());
    }

}
