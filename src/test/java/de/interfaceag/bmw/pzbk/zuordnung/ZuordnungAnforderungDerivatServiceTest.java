package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.dao.ZuordnungAnforderungDerivatDao;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungHistory;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.umsetzungsverwaltung.UmsetzungsbestaetigungService;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ZuordnungAnforderungDerivatServiceTest {

    @Mock
    BerechtigungService berechtigungService;
    @Mock
    DerivatAnforderungModulService derivatAnforderungModulService;
    @Mock
    ZuordnungAnforderungDerivatDao zuordnungAnforderungDerivatDao;
    @Mock
    AnforderungMeldungHistoryService anforderungMeldungHistoryService;
    @Mock
    UmsetzungsbestaetigungService umsetzungsbestaetigungService;
    @InjectMocks
    ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;

    @Mock
    Anforderung anforderung;
    @Mock
    Derivat derivat;
    @Mock
    Mitarbeiter mitarbeiter;
    @Mock
    ZuordnungAnforderungDerivat zuordnungAnforderungDerivat;

    private void setupForAnforderungFreigegebenAndZuordnungNotPresent() {
        when(anforderung.getStatus()).thenReturn(Status.A_FREIGEGEBEN);
        when(berechtigungService.isAnfordererForDerivat(any(), any())).thenReturn(Boolean.TRUE);
        when(zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatForAnforderungDerivat(any(), any())).thenReturn(null);
    }

    @ParameterizedTest
    @EnumSource(ZuordnungStatus.class)
    public void testAssignAnforderungToDerivatAnforderungFreigegebenAndZuordnungNotPresent(ZuordnungStatus status) {
        setupForAnforderungFreigegebenAndZuordnungNotPresent();
        when(derivat.isVereinbarungActive()).thenReturn(Boolean.TRUE);

        switch (status) {
            case ZUGEORDNET:
            case KEINE_ZUORDNUNG:
                when(derivat.getStatus()).thenReturn(DerivatStatus.VEREINARBUNG_VKBG);
                zuordnungAnforderungDerivatService.assignAnforderungToDerivat(status, anforderung, derivat, "", mitarbeiter);
                verify(zuordnungAnforderungDerivatDao, times(1)).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));
                break;
            default:
                zuordnungAnforderungDerivatService.assignAnforderungToDerivat(status, anforderung, derivat, "", mitarbeiter);
                verify(zuordnungAnforderungDerivatDao, never()).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(ZuordnungStatus.class)
    public void testAssignAnforderungToDerivatAnforderungFreigegebenAndZuordnungNotPresentDerivatVereinbarungInactive(ZuordnungStatus status) {
        setupForAnforderungFreigegebenAndZuordnungNotPresent();
        when(derivat.isVereinbarungActive()).thenReturn(Boolean.FALSE);
        zuordnungAnforderungDerivatService.assignAnforderungToDerivat(status, anforderung, derivat, "", mitarbeiter);
        verify(zuordnungAnforderungDerivatDao, never()).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));
    }

    @ParameterizedTest
    @EnumSource(ZuordnungStatus.class)
    public void testAssignAnforderungToDerivatAnforderungNotFreigegeben(ZuordnungStatus status) {
        when(anforderung.getStatus()).thenReturn(Status.A_KEINE_WEITERVERFOLG);
        zuordnungAnforderungDerivatService.assignAnforderungToDerivat(status, anforderung, derivat, "", mitarbeiter);
        verify(zuordnungAnforderungDerivatDao, never()).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));
    }

    @ParameterizedTest
    @EnumSource(ZuordnungStatus.class)
    public void testCreateVereinbarungForNewZuordnungForAnforderungFreigegebenAndZuordnungNotPresent(ZuordnungStatus status) {
        when(anforderung.getStatus()).thenReturn(Status.A_FREIGEGEBEN);
        when(berechtigungService.isAnfordererForDerivat(any(), any())).thenReturn(Boolean.TRUE);
        when(zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatForAnforderungDerivat(any(), any())).thenReturn(null);

        switch (status) {
            case ZUGEORDNET:
                when(derivat.isVereinbarungActive()).thenReturn(Boolean.TRUE);
                when(derivat.getStatus()).thenReturn(DerivatStatus.VEREINARBUNG_VKBG);
                zuordnungAnforderungDerivatService.assignAnforderungToDerivat(status, anforderung, derivat, "", mitarbeiter);
                verify(derivatAnforderungModulService, times(1)).createDerivatAnforderungModulForAnforderungDerivatZuordnung(any(), any(), any(), any());
                break;
            case KEINE_ZUORDNUNG:
                zuordnungAnforderungDerivatService.assignAnforderungToDerivat(status, anforderung, derivat, "", mitarbeiter);
                verify(derivatAnforderungModulService, never()).createDerivatAnforderungModulForAnforderungDerivatZuordnung(any(), any(), any(), any());
                break;
            default:
                zuordnungAnforderungDerivatService.assignAnforderungToDerivat(status, anforderung, derivat, "", mitarbeiter);
                verify(derivatAnforderungModulService, never()).createDerivatAnforderungModulForAnforderungDerivatZuordnung(any(), any(), any(), any());
                break;
        }
    }

    @Test
    public void testAssignAnforderungToDerivatAnforderungNull() {
        zuordnungAnforderungDerivatService.assignAnforderungToDerivat(ZuordnungStatus.ZUGEORDNET, null, derivat, "", mitarbeiter);
        verify(zuordnungAnforderungDerivatDao, never()).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));
    }

    @Test
    public void testAssignAnforderungToDerivatDerivatNull() {
        zuordnungAnforderungDerivatService.assignAnforderungToDerivat(ZuordnungStatus.ZUGEORDNET, anforderung, null, "", mitarbeiter);
        verify(zuordnungAnforderungDerivatDao, never()).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));
    }

    @ParameterizedTest
    @EnumSource(ZuordnungStatus.class)
    public void testAssignAnforderungToDerivatAnforderungFreigegebenAndZuordnungPresent(ZuordnungStatus status
    ) {
        setupForAnforderungFreigegebenAndZuordnungNotPresent();
        when(zuordnungAnforderungDerivatDao.getZuordnungAnforderungDerivatForAnforderungDerivat(any(), any())).thenReturn(zuordnungAnforderungDerivat);

        zuordnungAnforderungDerivatService.assignAnforderungToDerivat(status, anforderung, derivat, "", mitarbeiter);
        verify(zuordnungAnforderungDerivatDao, never()).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));
    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForEmptyStatus() {


        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(null, zuordnungAnforderungDerivat, mitarbeiter);
        verify(zuordnungAnforderungDerivatDao, never()).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));

    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForEmptyZuordnungAnforderungDerivat() {


        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.ZUGEORDNET, null, mitarbeiter);
        verify(zuordnungAnforderungDerivatDao, never()).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));

    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForOffenStatusRemoveUmsetzungsbestaetiungForAnforderungDerivat() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.ZUGEORDNET, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.OFFEN, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(umsetzungsbestaetigungService, times(1)).removeUmsetzungsbestaetigungenForAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));

    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForOffenStatusRemoveDerivatAnforderungModul() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.ZUGEORDNET, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.OFFEN, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(derivatAnforderungModulService, times(1)).removeDerivatAnforderungModulForZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));

    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForOffenStatusRemoveZuordnungAnforderungDerivat() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.ZUGEORDNET, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.OFFEN, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(zuordnungAnforderungDerivatDao, times(1)).removeZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));

    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForOffenStatusPersistHistoryForZuordnung() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.ZUGEORDNET, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.OFFEN, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(anforderungMeldungHistoryService, times(1)).persistAnforderungHistory(any(AnforderungHistory.class));

    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForKeineZuordnungStatusToZuordnung() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.KEINE_ZUORDNUNG, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.ZUGEORDNET, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(derivatAnforderungModulService, times(1)).createDerivatAnforderungModulForAnforderungDerivatZuordnung(any(ZuordnungAnforderungDerivat.class), any(DerivatAnforderungModulStatus.class), any(String.class), any(Mitarbeiter.class));

    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForKeineZuordnungStatusToZuordnungPersist() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.KEINE_ZUORDNUNG, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.ZUGEORDNET, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(zuordnungAnforderungDerivatDao, times(1)).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));

    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForKeineZuordnungStatusToZuordnungCreateHistory() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.KEINE_ZUORDNUNG, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.ZUGEORDNET, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(anforderungMeldungHistoryService, times(1)).persistAnforderungHistory(any(AnforderungHistory.class));

    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForZugeordnetStatusToKeineZuordnungRemoveUmsetzungsbestaetigung() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.ZUGEORDNET, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.KEINE_ZUORDNUNG, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(umsetzungsbestaetigungService, times(1)).removeUmsetzungsbestaetigungenForAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));

    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForZugeordnetStatusToKeineZuordnungRemoveDerivatAnforderungModul() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.ZUGEORDNET, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.KEINE_ZUORDNUNG, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(derivatAnforderungModulService, times(1)).removeDerivatAnforderungModulForZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));

    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForZugeordnetStatusToKeineZuordnungPersist() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.ZUGEORDNET, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.KEINE_ZUORDNUNG, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(zuordnungAnforderungDerivatDao, times(1)).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));

    }


    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForZugeordnetStatusToKeineZuordnungCreateHistory() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.ZUGEORDNET, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.KEINE_ZUORDNUNG, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(anforderungMeldungHistoryService, times(1)).persistAnforderungHistory(any(AnforderungHistory.class));


    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForOffenStatusToZugeordnetPersist() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.OFFEN, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.ZUGEORDNET, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(zuordnungAnforderungDerivatDao, times(1)).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));


    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForOffenStatusToZugeordnetCreateHistory() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.OFFEN, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.ZUGEORDNET, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(anforderungMeldungHistoryService, times(1)).persistAnforderungHistory(any(AnforderungHistory.class));


    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForOffenStatusToKeineZuordnungPersist() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.OFFEN, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.ZUGEORDNET, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(zuordnungAnforderungDerivatDao, times(1)).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));


    }

    @Test
    public void testupdateZuordnungAnforderungDerivatStatusForOffenStatusToKeineZuordnungCreateHistory() {

        ZuordnungAnforderungDerivat zuordnungAnforderungDerivatForTest = TestDataFactory.generateFullZuordnungAnforderungDerivat(ZuordnungStatus.OFFEN, false, false, false);

        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.ZUGEORDNET, zuordnungAnforderungDerivatForTest, mitarbeiter);

        verify(anforderungMeldungHistoryService, times(1)).persistAnforderungHistory(any(AnforderungHistory.class));


    }
}
