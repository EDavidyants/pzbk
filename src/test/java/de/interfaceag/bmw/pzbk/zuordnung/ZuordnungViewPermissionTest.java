package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.when;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
@ExtendWith(MockitoExtension.class)
public class ZuordnungViewPermissionTest {

    @Mock
    DerivatDto derivat;

    ZuordnungViewPermission viewPermission;

    @BeforeEach
    public void setUp() {
        when(derivat.isVereinbarungActive()).thenReturn(Boolean.TRUE);
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void pageRolePermissionsEditMode(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        viewPermission = new ZuordnungViewPermission(userRoles, true, derivat);

        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case TTEAM_VERTRETER:
            case ANFORDERER:
                Assertions.assertTrue(viewPermission.isPage(), role.toString() + " should have permissions!");
                break;
            default:
                Assertions.assertFalse(viewPermission.isPage(), role.toString() + " should have no permissions!");
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editRolePermissionsNoEditMode(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        viewPermission = new ZuordnungViewPermission(userRoles, false, derivat);

        switch (role) {
            case ADMIN:
            case ANFORDERER:
                Assertions.assertTrue(viewPermission.isEditButton(), role.toString() + " should have permissions!");
                break;
            default:
                Assertions.assertFalse(viewPermission.isEditButton(), role.toString() + " should have no permissions!");
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void editRolePermissionsEditMode(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);
        viewPermission = new ZuordnungViewPermission(userRoles, true, derivat);
        Assertions.assertFalse(viewPermission.isEditButton(), "should not be visible!");
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void automaticAssignmentRolePermissionsNoEditMode(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        viewPermission = new ZuordnungViewPermission(userRoles, false, derivat);

        switch (role) {
            case ADMIN:
            case ANFORDERER:
                Assertions.assertTrue(viewPermission.isAutomaticAssignment(), role.toString() + " should have permissions!");
                break;
            default:
                Assertions.assertFalse(viewPermission.isAutomaticAssignment(), role.toString() + " should have no permissions!");
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void automaticAssignmentRolePermissionsEditMode(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);
        viewPermission = new ZuordnungViewPermission(userRoles, true, derivat);
        Assertions.assertFalse(viewPermission.isAutomaticAssignment(), "should not be visible!");
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void zakButtonRolePermissionsNoEditMode(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        viewPermission = new ZuordnungViewPermission(userRoles, false, derivat);

        switch (role) {
            case ADMIN:
            case ANFORDERER:
                Assertions.assertTrue(viewPermission.isZakButton(), role.toString() + " should have permissions!");
                break;
            default:
                Assertions.assertFalse(viewPermission.isZakButton(), role.toString() + " should have no permissions!");
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void zakButtonRolePermissionsEditMode(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);
        viewPermission = new ZuordnungViewPermission(userRoles, true, derivat);
        Assertions.assertFalse(viewPermission.isZakButton(), "should not be visible!");
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void processChangesButtonRolePermissionsEditMode(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        viewPermission = new ZuordnungViewPermission(userRoles, true, derivat);

        switch (role) {
            case ADMIN:
            case ANFORDERER:
                Assertions.assertTrue(viewPermission.isProcessChangesButton(), role.toString() + " should have permissions!");
                break;
            default:
                Assertions.assertFalse(viewPermission.isProcessChangesButton(), role.toString() + " should have no permissions!");
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void processChangesButtonRolePermissionsNoEditMode(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);
        viewPermission = new ZuordnungViewPermission(userRoles, false, derivat);
        Assertions.assertFalse(viewPermission.isProcessChangesButton(), "should not be visible!");
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void cancelButtonRolePermissionsEditMode(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        viewPermission = new ZuordnungViewPermission(userRoles, true, derivat);

        switch (role) {
            case ADMIN:
            case ANFORDERER:
                Assertions.assertTrue(viewPermission.isProcessChangesButton(), role.toString() + " should have permissions!");
                break;
            default:
                Assertions.assertFalse(viewPermission.isProcessChangesButton(), role.toString() + " should have no permissions!");
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void cancelButtonRolePermissionsNoEditMode(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);
        viewPermission = new ZuordnungViewPermission(userRoles, false, derivat);
        Assertions.assertFalse(viewPermission.isProcessChangesButton(), "should not be visible!");
    }

}
