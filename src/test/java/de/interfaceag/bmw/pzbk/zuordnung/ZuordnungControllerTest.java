package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ContextMocker;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.primefaces.context.RequestContext;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ZuordnungControllerTest {

    @InjectMocks
    private ZuordnungController zuordnungController;

    @Mock
    private ZuordnungViewFacade zuordnungViewFacade;

    @Mock
    private ZuordnungViewData zuordnungViewData;

    private RequestContext requestContext;

    @Test
    void isOnlyOneDerivatFiltered() {
        when(zuordnungViewData.getFilteredDerivate()).thenReturn(Collections.singletonList(mock(DerivatDto.class)));
        Assert.assertTrue(zuordnungController.isOnlyOneDerivatFiltered());
    }

    @Test
    void noneDerivatFiltered() {
        when(zuordnungViewData.getFilteredDerivate()).thenReturn(Collections.emptyList());
        Assert.assertFalse(zuordnungController.isOnlyOneDerivatFiltered());
    }

    @Test
    void processAutomaticAssignment() {
        requestContext = ContextMocker.mockRequestContext();

        when(zuordnungViewData.getFilteredDerivate()).thenReturn(Collections.singletonList(mock(DerivatDto.class)));
        zuordnungController.automatischZuordnen();

        verify(zuordnungViewFacade, times(1)).automatischZuordnen(any());
        verify(requestContext, times(1)).update("zuordnungNavBar:automatischZuordnenDialogForm:automatischZuordnenDialogContent");
        verify(requestContext, times(1)).execute("PF('automatischZuordnenDialog').show()");

    }

    @Test
    void getDerivat() {
        when(zuordnungViewData.getSelectedDerivat()).thenReturn(mock(DerivatDto.class));

        DerivatDto derivat = zuordnungController.getDerivat();
        Assert.assertNotNull(derivat);
    }

    @Test
    void getViewPermission() {
        when(zuordnungViewData.getViewPermission()).thenReturn(mock(ZuordnungViewPermission.class));
        ZuordnungViewPermission viewPermission = zuordnungController.getViewPermission();
        Assert.assertNotNull(viewPermission);
    }

    @Test
    void setAllSelected_false() {
        zuordnungController.setAllSelected(false);
        verify(zuordnungViewData, times(1)).setAllSelected(false);
    }

    @Test
    void setAllSelected_true() {
        zuordnungController.setAllSelected(true);
        verify(zuordnungViewData, times(1)).setAllSelected(true);
    }
}