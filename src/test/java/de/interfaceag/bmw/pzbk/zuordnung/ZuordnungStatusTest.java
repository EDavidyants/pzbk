package de.interfaceag.bmw.pzbk.zuordnung;

import org.hamcrest.collection.IsIterableWithSize;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class ZuordnungStatusTest {

    @ParameterizedTest
    @EnumSource(ZuordnungStatus.class)
    void getId(ZuordnungStatus status) {
        final int id = status.getId();
        Integer expectedId = null;
        switch (status) {
            case OFFEN:
                expectedId = 0;
                break;
            case KEINE_ZUORDNUNG:
                expectedId = 2;
                break;
            case ZUGEORDNET:
                expectedId = 7;
                break;
        }
        assertThat(id, is(expectedId));
    }

    @Test
    void getByNullId() {
        final Optional<ZuordnungStatus> status = ZuordnungStatus.getById(null);
        final Optional<ZuordnungStatus> expected = Optional.empty();
        assertThat(status, is(expected));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
    void getById(Integer id) {
        final Optional<ZuordnungStatus> status = ZuordnungStatus.getById(id);
        final Optional<ZuordnungStatus> expected;
        switch (id) {
            case 0:
                expected = Optional.of(ZuordnungStatus.OFFEN);
                break;
            case 2:
                expected = Optional.of(ZuordnungStatus.KEINE_ZUORDNUNG);
                break;
            case 7:
                expected = Optional.of(ZuordnungStatus.ZUGEORDNET);
                break;
            default:
                expected = Optional.empty();
                break;
        }
        assertThat(status, is(expected));
    }

    @ParameterizedTest
    @EnumSource(ZuordnungStatus.class)
    void toString1(ZuordnungStatus status) {
        final String toString = status.toString();
        final String bezeichnung = status.getStatusBezeichnung();
        assertThat(toString, is(bezeichnung));
    }

    @ParameterizedTest
    @EnumSource(ZuordnungStatus.class)
    void getStatusBezeichnung(ZuordnungStatus status) {
        final String bezeichnung = status.getStatusBezeichnung();
        final String expected;
        switch (status) {
            case OFFEN:
                expected = "offen";
                break;
            case KEINE_ZUORDNUNG:
                expected = "keine Zuordnung";
                break;
            case ZUGEORDNET:
                expected = "zugeordnet";
                break;
            default:
                expected = "";
                break;
        }
        assertThat(bezeichnung, is(expected));
    }

    @Test
    void getAll() {
        final List<ZuordnungStatus> all = ZuordnungStatus.getAll();
        assertThat(all, IsIterableWithSize.iterableWithSize(3));
    }

    @Test
    void getNextStatusForStatusOffen() {
        final ZuordnungStatus status = ZuordnungStatus.OFFEN;
        final List<ZuordnungStatus> nextStatus = status.getNextStatus();
        assertThat(nextStatus, IsIterableContaining.hasItems(ZuordnungStatus.ZUGEORDNET, ZuordnungStatus.KEINE_ZUORDNUNG));
    }

    @Test
    void getNextStatusForStatusZugeordnet() {
        final ZuordnungStatus status = ZuordnungStatus.ZUGEORDNET;
        final List<ZuordnungStatus> nextStatus = status.getNextStatus();
        assertThat(nextStatus, IsIterableContaining.hasItem(ZuordnungStatus.OFFEN));
    }


    @Test
    void getNextStatusForStatusKeineZuordnung() {
        final ZuordnungStatus status = ZuordnungStatus.KEINE_ZUORDNUNG;
        final List<ZuordnungStatus> nextStatus = status.getNextStatus();
        assertThat(nextStatus, IsIterableContaining.hasItem(ZuordnungStatus.ZUGEORDNET));
    }
}