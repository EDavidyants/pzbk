package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ZuordnungProcessServiceTest {

    @Mock
    DerivatService derivatService;
    @Mock
    ZuordnungProcessChangeService zuordnungProcessChangeService;
    @Mock
    ZuordnungProcessZakService zuordnungProcessZakService;

    @InjectMocks
    ZuordnungProcessService zuordnungProcessService;

    @Mock
    ZuordnungViewData viewData;
    @Mock
    DerivatDto derivat;

    @Mock
    List<Zuordnung> selectedTableData;

    @Test
    public void testProcessChangesZuordnenModeWithBulk() throws Exception {
        when(viewData.isZuordnenMode()).thenReturn(Boolean.TRUE);
        when(viewData.getGroupStatus()).thenReturn(ZuordnungStatus.ZUGEORDNET);
        when(viewData.getSelectedTableData()).thenReturn(selectedTableData);
        when(viewData.getSelectedDerivat()).thenReturn(derivat);
        when(selectedTableData.isEmpty()).thenReturn(Boolean.FALSE);
        when(derivat.getId()).thenReturn(1L);

        zuordnungProcessService.processChanges(viewData);
        verify(zuordnungProcessChangeService, times(1)).processZuordnungChangesInBulkMode(any(), any(), any());
    }

    @Test
    public void testProcessChangesZuordnenModeSingleModeSelectedListEmpty() throws Exception {
        when(viewData.isZuordnenMode()).thenReturn(Boolean.TRUE);
        when(viewData.getGroupStatus()).thenReturn(ZuordnungStatus.ZUGEORDNET);
        when(viewData.getSelectedTableData()).thenReturn(selectedTableData);
        when(viewData.getSelectedDerivat()).thenReturn(derivat);
        when(selectedTableData.isEmpty()).thenReturn(Boolean.TRUE);
        when(derivat.getId()).thenReturn(1L);

        zuordnungProcessService.processChanges(viewData);
        verify(zuordnungProcessChangeService, times(1)).processZuordnungChangesInSingleMode(any(), any());
    }

    @Test
    public void testProcessChangesZuordnenModeSingleModeGroupStatusNull() throws Exception {
        when(viewData.isZuordnenMode()).thenReturn(Boolean.TRUE);
        when(viewData.getGroupStatus()).thenReturn(null);
        when(viewData.getSelectedTableData()).thenReturn(selectedTableData);
        when(viewData.getSelectedDerivat()).thenReturn(derivat);
        when(selectedTableData.isEmpty()).thenReturn(Boolean.TRUE);
        when(derivat.getId()).thenReturn(1L);

        zuordnungProcessService.processChanges(viewData);
        verify(zuordnungProcessChangeService, times(1)).processZuordnungChangesInSingleMode(any(), any());
    }

    @Test
    public void testProcessChangesZakMode() throws Exception {
        when(viewData.isZuordnenMode()).thenReturn(Boolean.FALSE);
        when(viewData.isZakMode()).thenReturn(Boolean.TRUE);
        zuordnungProcessService.processChanges(viewData);
        verify(zuordnungProcessZakService, times(1)).processZakChanges(any(), any());
    }

}
