package de.interfaceag.bmw.pzbk.zuordnung.automatic;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungAutomaticProcessDao;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungProcessChangeService;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungViewData;
import de.interfaceag.bmw.pzbk.zuordnung.dto.DerivatZuordnung;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ZuordnungAutomaticProcessServiceTest {

    @InjectMocks
    private ZuordnungAutomaticProcessService zuordnungAutomaticProcessService;
    @Mock
    private ZuordnungProcessChangeService zuordnungProcessChangeService;
    @Mock
    private ZuordnungAutomaticProcessDao automaticProcessDao;
    @Mock
    private DerivatService derivatService;

    @Mock
    private ZuordnungViewData zuordnungViewData;
    @Mock
    private Zuordnung zuordnung;
    @Mock
    private DerivatDto derivatDto;
    @Mock
    private Anforderung anforderung;
    @Mock
    private DerivatZuordnung derivatZuordnung;
    @Mock
    private Derivat derivat;
    @Mock
    private ZuordnungAnforderungDerivat zuordnungAnforderungDerivat;

    @Test
    void assignAnforderungenAutomatic_without_assignment() {
        ZuordnungViewData viewData = mock(ZuordnungViewData.class);
        DerivatDto derivatDto = mock(DerivatDto.class);

        when(viewData.getSelectedDerivat()).thenReturn(derivatDto);
        when(derivatDto.getId()).thenReturn(1L);
        when(automaticProcessDao.getAnforderungForFahrzeugmerkmalAndAuspraegungen(any(), any())).thenReturn(Collections.emptyList());

        AutomaticZuordnenDialogViewData dialogViewData = zuordnungAutomaticProcessService.assignAnforderungenAutomatic(viewData);

        Assert.assertNull(dialogViewData);
    }

    @Test
    void assignAnforderungenAutomatic_with_assignment() {
        ZuordnungViewData viewData = mock(ZuordnungViewData.class);
        DerivatDto derivatDto = mock(DerivatDto.class);
        List<Anforderung> anforderungen = Collections.singletonList(mock(Anforderung.class));

        when(viewData.getSelectedDerivat()).thenReturn(derivatDto);
        when(derivatDto.getId()).thenReturn(1L);
        when(automaticProcessDao.getAnforderungForFahrzeugmerkmalAndAuspraegungen(any(), any())).thenReturn(anforderungen);
        when(derivatService.getDerivatById(any())).thenReturn(mock(Derivat.class));

        AutomaticZuordnenDialogViewData dialogViewData = zuordnungAutomaticProcessService.assignAnforderungenAutomatic(viewData);

        verify(zuordnungProcessChangeService, times(1)).processZuordnungChangesInBulkMode(any(), any(), any());
        Assert.assertNotNull(dialogViewData);
    }

    @Test
    void assignAnforderungenAutomatic_withViewData() {
        when(zuordnungViewData.getSelectedDerivat()).thenReturn(derivatDto);
        when(zuordnungViewData.getTableData()).thenReturn(Collections.singletonList(zuordnung));
        when(zuordnungViewData.getAnforderungen()).thenReturn(Collections.singletonList(anforderung));
        when(derivatDto.getId()).thenReturn(1L);
        when(zuordnung.getZuordnungForDerivat(any())).thenReturn(derivatZuordnung);
        when(zuordnung.getAnforderungId()).thenReturn(1L);
        when(derivatZuordnung.getStatus()).thenReturn(ZuordnungStatus.OFFEN);
        when(anforderung.getId()).thenReturn(1L);
        when(anforderung.getBeschreibungAnforderungDe()).thenReturn("meine Beschreibung");
        when(zuordnungAnforderungDerivat.getAnforderung()).thenReturn(anforderung);

        when(automaticProcessDao.getAnforderungForFahrzeugmerkmalAndAuspraegungen(any(), any())).thenReturn(Collections.singletonList(anforderung));
        when(derivatService.getDerivatById(any())).thenReturn(derivat);
        when(zuordnungProcessChangeService.processZuordnungChangesInBulkMode(any(), any(), any())).thenReturn(Collections.singletonList(zuordnungAnforderungDerivat));

        AutomaticZuordnenDialogViewData dialogViewData = zuordnungAutomaticProcessService.assignAnforderungenAutomatic(zuordnungViewData);

        verify(zuordnungProcessChangeService, times(1)).processZuordnungChangesInBulkMode(any(), any(), any());
        Assert.assertNotNull(dialogViewData);
        Assert.assertEquals(1, dialogViewData.getAutomaticZuordnenDtoList().size());
    }

    @Test
    void assignAnforderungenAutomatic_withViewData_no_assignment() {
        when(zuordnungViewData.getSelectedDerivat()).thenReturn(derivatDto);
        when(zuordnungViewData.getTableData()).thenReturn(Collections.singletonList(zuordnung));
        when(zuordnungViewData.getAnforderungen()).thenReturn(Collections.singletonList(anforderung));
        when(derivatDto.getId()).thenReturn(1L);
        when(zuordnung.getZuordnungForDerivat(any())).thenReturn(derivatZuordnung);
        when(zuordnung.getAnforderungId()).thenReturn(1L);
        when(derivatZuordnung.getStatus()).thenReturn(ZuordnungStatus.OFFEN);
        when(anforderung.getId()).thenReturn(1L);

        when(automaticProcessDao.getAnforderungForFahrzeugmerkmalAndAuspraegungen(any(), any())).thenReturn(Collections.singletonList(anforderung));
        when(derivatService.getDerivatById(any())).thenReturn(derivat);
        when(zuordnungProcessChangeService.processZuordnungChangesInBulkMode(any(), any(), any())).thenReturn(Collections.emptyList());

        AutomaticZuordnenDialogViewData dialogViewData = zuordnungAutomaticProcessService.assignAnforderungenAutomatic(zuordnungViewData);

        verify(zuordnungProcessChangeService, times(1)).processZuordnungChangesInBulkMode(any(), any(), any());
        Assert.assertNotNull(dialogViewData);
        Assert.assertTrue(dialogViewData.getAutomaticZuordnenDtoList().isEmpty());
    }
}