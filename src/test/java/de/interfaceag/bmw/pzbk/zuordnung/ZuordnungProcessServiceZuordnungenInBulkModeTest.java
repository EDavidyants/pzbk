package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.derivat.DerivatDto;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.zuordnung.dto.Zuordnung;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ZuordnungProcessServiceZuordnungenInBulkModeTest {

    @Mock
    private ZuordnungProcessChangeService zuordnungProcessChangeService;

    @Mock
    private DerivatDto derivatDto;

    @Mock
    private Derivat derivat;

    @Mock
    private ZuordnungViewData viewData;

    @Mock
    private DerivatService derivatService;

    @InjectMocks
    private final ZuordnungProcessService zuordnungProcessService = new ZuordnungProcessService();

    private Class<?>[] params;
    private Method processZuordnungenInBulkModeMethod;

    @Mock
    private List<Zuordnung> tableData;
    @Mock
    private List<Zuordnung> selectedTableData;

    @BeforeEach
    public void setUp() {
        params = new Class<?>[]{ZuordnungViewData.class};

        try {
            processZuordnungenInBulkModeMethod = zuordnungProcessService.getClass().getDeclaredMethod("processZuordnungenInBulkMode", params);
            processZuordnungenInBulkModeMethod.setAccessible(true);

        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(ZuordnungProcessServiceZuordnungenInBulkModeTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        when(viewData.getGroupStatus()).thenReturn(ZuordnungStatus.ZUGEORDNET);

        when(viewData.getSelectedDerivat()).thenReturn(derivatDto);
        when(derivatDto.getId()).thenReturn(1L);
        when(derivatDto.getName()).thenReturn("E20");
        when(derivatService.getDerivatById(1L)).thenReturn(derivat);

    }

    @Test
    public void testProcessZuordnungForSeveralAnforderungen() {
        try {
            when(viewData.isAllSelected()).thenReturn(Boolean.FALSE);
            when(viewData.getSelectedTableData()).thenReturn(selectedTableData);

            Object[] viewDataPresent = new Object[]{viewData};
            processZuordnungenInBulkModeMethod.invoke(zuordnungProcessService, viewDataPresent);

            verify(zuordnungProcessChangeService, times(1)).processZuordnungChangesInBulkMode(ZuordnungStatus.ZUGEORDNET, derivat, selectedTableData);

        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(ZuordnungProcessServiceZuordnungenInBulkModeTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void testProcessZuordnungForAllAnforderungen() {
        try {
            when(viewData.isAllSelected()).thenReturn(Boolean.TRUE);
            when(viewData.getTableData()).thenReturn(tableData);

            Object[] viewDataPresent = new Object[]{viewData};
            processZuordnungenInBulkModeMethod.invoke(zuordnungProcessService, viewDataPresent);

            verify(zuordnungProcessChangeService, times(1)).processZuordnungChangesInBulkMode(ZuordnungStatus.ZUGEORDNET, derivat, tableData);

        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(ZuordnungProcessServiceZuordnungenInBulkModeTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
