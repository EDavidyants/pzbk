package de.interfaceag.bmw.pzbk.zuordnung.automatic.util;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.zuordnung.automatic.AutomaticZuordnenDialogViewData;
import de.interfaceag.bmw.pzbk.zuordnung.automatic.AutomaticZuordnenDto;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Collections;

class ZuordnungToDtoConverterTest {

    private static final String FACH_ID = "FACH_ID";
    private static final Integer VERSION = 11;
    private static final String BESCHREIBUNG = "HALLO, das ist mein Kommentar";
    private static final String LONG_BESCHREIBUNG = "Das ist eine lange Beschreibung, sprich mehr als 30 Zeichen. Mal schauen was passiert";

    @Test
    void buildAutomaticZuordnenDialogViewData_nullSafe() {
        AutomaticZuordnenDialogViewData dialogViewData = ZuordnungToDtoConverter.buildAutomaticZuordnenDialogViewData(null);
        Assert.assertNotNull(dialogViewData);
    }

    @Test
    void buildAutomaticZuordnenDialogViewData_emptyCollection() {
        AutomaticZuordnenDialogViewData dialogViewData = ZuordnungToDtoConverter.buildAutomaticZuordnenDialogViewData(Collections.emptyList());
        Assert.assertNotNull(dialogViewData);
    }

    @Test
    void buildAutomaticZuordnenDialogViewData_withData() {
        AutomaticZuordnenDialogViewData dialogViewData = ZuordnungToDtoConverter.buildAutomaticZuordnenDialogViewData(Collections.singletonList(getZuordnungAnforderungDerivat()));
        Assert.assertNotNull(dialogViewData);
        Assert.assertEquals(1, dialogViewData.getAutomaticZuordnenDtoList().size());

        AutomaticZuordnenDto automaticZuordnenDto = dialogViewData.getAutomaticZuordnenDtoList().get(0);
        Assert.assertEquals(BESCHREIBUNG, automaticZuordnenDto.getAnforderungDescriptionLong());
        Assert.assertEquals(BESCHREIBUNG, automaticZuordnenDto.getAnforderungDescriptionShort());
        Assert.assertEquals(FACH_ID, automaticZuordnenDto.getAnforderungFachId());
        Assert.assertEquals(VERSION, automaticZuordnenDto.getAnforderungVersion());
    }

    @Test
    void buildAutomaticZuordnenDialogViewData_withLongData() {
        ZuordnungAnforderungDerivat zuordnungAnforderungDerivat = getZuordnungAnforderungDerivat();
        zuordnungAnforderungDerivat.getAnforderung().setBeschreibungAnforderungDe(LONG_BESCHREIBUNG);

        AutomaticZuordnenDialogViewData dialogViewData = ZuordnungToDtoConverter.buildAutomaticZuordnenDialogViewData(Collections.singletonList(zuordnungAnforderungDerivat));
        Assert.assertNotNull(dialogViewData);
        Assert.assertEquals(1, dialogViewData.getAutomaticZuordnenDtoList().size());

        AutomaticZuordnenDto automaticZuordnenDto = dialogViewData.getAutomaticZuordnenDtoList().get(0);
        Assert.assertEquals(LONG_BESCHREIBUNG, automaticZuordnenDto.getAnforderungDescriptionLong());
        Assert.assertEquals("Das ist eine lange Beschreibu...", automaticZuordnenDto.getAnforderungDescriptionShort());
        Assert.assertEquals(FACH_ID, automaticZuordnenDto.getAnforderungFachId());
        Assert.assertEquals(VERSION, automaticZuordnenDto.getAnforderungVersion());
    }

    private ZuordnungAnforderungDerivat getZuordnungAnforderungDerivat() {
        ZuordnungAnforderungDerivat zuordnungAnforderungDerivat = new ZuordnungAnforderungDerivat();
        zuordnungAnforderungDerivat.setAnforderung(getAnforderung());
        return zuordnungAnforderungDerivat;
    }

    private Anforderung getAnforderung() {
        Anforderung anforderung = new Anforderung(FACH_ID, VERSION);
        anforderung.setBeschreibungAnforderungDe(BESCHREIBUNG);
        return anforderung;
    }
}