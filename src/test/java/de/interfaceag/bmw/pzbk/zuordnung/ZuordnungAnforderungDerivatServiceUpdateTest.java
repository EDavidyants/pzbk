package de.interfaceag.bmw.pzbk.zuordnung;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungMeldungHistoryService;
import de.interfaceag.bmw.pzbk.dao.ZuordnungAnforderungDerivatDao;
import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.vereinbarung.DerivatAnforderungModulService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@ExtendWith(MockitoExtension.class)
public class ZuordnungAnforderungDerivatServiceUpdateTest {

    @Mock
    BerechtigungService berechtigungService;
    @Mock
    DerivatAnforderungModulService derivatAnforderungModulService;
    @Mock
    ZuordnungAnforderungDerivatDao zuordnungAnforderungDerivatDao;
    @Mock
    AnforderungMeldungHistoryService anforderungMeldungHistoryService;
    @InjectMocks
    ZuordnungAnforderungDerivatService zuordnungAnforderungDerivatService;

    @Mock
    Anforderung anforderung;
    @Mock
    Derivat derivat;
    @Mock
    Mitarbeiter mitarbeiter;
    @Mock
    ZuordnungAnforderungDerivat zuordnungAnforderungDerivat;

    @Test
    public void testUpdateZuordnungAnforderungDerivatStatusNullValues() {
        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(null, null, null);
        verify(zuordnungAnforderungDerivatDao, never()).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));
        verify(anforderungMeldungHistoryService, never()).persistAnforderungHistory(any());
    }

    @Test
    public void testUpdateZuordnungAnforderungDerivatStatusSameStatus() {
        when(zuordnungAnforderungDerivat.getStatus()).thenReturn(ZuordnungStatus.ZUGEORDNET);
        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.ZUGEORDNET, zuordnungAnforderungDerivat, mitarbeiter);
        verify(zuordnungAnforderungDerivatDao, never()).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));
        verify(anforderungMeldungHistoryService, never()).persistAnforderungHistory(any());
    }

    @Test
    public void testUpdateZuordnungAnforderungDerivatStatusNewStatus() {
        when(zuordnungAnforderungDerivat.getStatus()).thenReturn(ZuordnungStatus.OFFEN);
        when(zuordnungAnforderungDerivat.getDerivat()).thenReturn(derivat);
        when(zuordnungAnforderungDerivat.getAnforderung()).thenReturn(anforderung);
        when(derivat.getStatus()).thenReturn(DerivatStatus.ZV);
        when(anforderung.getId()).thenReturn(1L);
        zuordnungAnforderungDerivatService.updateZuordnungAnforderungDerivatStatus(ZuordnungStatus.ZUGEORDNET, zuordnungAnforderungDerivat, mitarbeiter);
        verify(zuordnungAnforderungDerivatDao, times(1)).persistZuordnungAnforderungDerivat(any(ZuordnungAnforderungDerivat.class));
        verify(anforderungMeldungHistoryService, times(1)).persistAnforderungHistory(any());
    }

}
