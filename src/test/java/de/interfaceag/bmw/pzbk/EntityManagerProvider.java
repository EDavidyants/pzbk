package de.interfaceag.bmw.pzbk;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class EntityManagerProvider implements TestRule {

    private EntityManager entityManager;
    private EntityTransaction transaction;

    private EntityManagerProvider(String unitName) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(unitName);
        this.entityManager = emf.createEntityManager();
        this.transaction = entityManager.getTransaction();
    }

    public static EntityManagerProvider forIntegrationTests() {
        return new EntityManagerProvider("integration-test");
    }

    public void begin() {
        this.transaction.begin();
    }

    public void commit() {
        this.transaction.commit();
    }

    public EntityTransaction getTransaction() {
        return this.transaction;
    }

    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                base.evaluate();
                entityManager.clear();
            }
        };
    }

}
