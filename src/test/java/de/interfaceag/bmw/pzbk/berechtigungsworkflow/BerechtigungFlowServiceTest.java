package de.interfaceag.bmw.pzbk.berechtigungsworkflow;

import de.interfaceag.bmw.pzbk.berechtigung.dto.TteamMitgliedBerechtigung;
import de.interfaceag.bmw.pzbk.berechtigungantrag.BerechtigungAntragService;
import de.interfaceag.bmw.pzbk.berechtigungantrag.mail.BerechtigungsantragMailService;
import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungFlowViewData;
import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungIdTuple;
import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungTyp;
import de.interfaceag.bmw.pzbk.entities.Berechtigungsantrag;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.services.UserService;
import de.interfaceag.bmw.pzbk.session.Session;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BerechtigungFlowServiceTest {

    @InjectMocks
    private BerechtigungFlowService berechtigungFlowService;
    @Mock
    private SensorCocService sensorCocService;
    @Mock
    private ModulService modulService;
    @Mock
    private TteamService tteamService;
    @Mock
    private BerechtigungAntragService berechtigungAntragService;
    @Mock
    private Session session;
    @Mock
    private UserPermissions userPermissions;
    @Mock
    private BerechtigungFlowViewData viewData;
    @Mock
    private Mitarbeiter mitarbeiter;
    @Mock
    private UserService userService;
    @Mock
    private UserSearchService userSearchService;
    @Mock
    private Berechtigungsantrag berechtigungsantrag;
    @Mock
    private BerechtigungsantragMailService berechtigungsantragMailService;

    @Test
    void getBerechtigungForRolle_no_user_permission() {
        when(viewData.getSelectedRole()).thenReturn(Rolle.SENSOR);

        SensorCoc sensorCoc1 = new SensorCoc();
        sensorCoc1.setSensorCocId(1L);
        sensorCoc1.setTechnologie("technologie");
        when(sensorCocService.getAllSortByTechnologie()).thenReturn(Collections.singletonList(sensorCoc1));

        berechtigungFlowService.getBerechtigungForRolle(viewData);

        verify(sensorCocService, times(1)).getAllSortByTechnologie();
        verifyZeroInteractions(userPermissions);
        verify(viewData, times(1)).setBerechtigungenForRoleWithSchreibrecht(any());
        verify(viewData, times(1)).setBerechtigungenForRoleWithLeserecht(any());
    }

    @Test
    void getBerechtigungForRolle_SENSORCOC() {
        when(viewData.getSelectedRole()).thenReturn(Rolle.SENSOR);
        when(session.getUserPermissions()).thenReturn(userPermissions);

        SensorCoc sensorCoc1 = new SensorCoc();
        sensorCoc1.setSensorCocId(1L);
        sensorCoc1.setTechnologie("technologie");
        when(sensorCocService.getAllSortByTechnologie()).thenReturn(Collections.singletonList(sensorCoc1));

        berechtigungFlowService.getBerechtigungForRolle(viewData);

        verify(sensorCocService, times(1)).getAllSortByTechnologie();
        verify(userPermissions, times(2)).getSensorCocAsSensorSchreibend();
        verify(userPermissions, times(1)).getSensorCocAsSensorLesend();
        verify(viewData, times(1)).setBerechtigungenForRoleWithSchreibrecht(any());
        verify(viewData, times(1)).setBerechtigungenForRoleWithLeserecht(any());
    }

    @Test
    void getBerechtigungForRolle_SETEAM() {
        when(viewData.getSelectedRole()).thenReturn(Rolle.E_COC);
        when(session.getUserPermissions()).thenReturn(userPermissions);

        ModulSeTeam modulSeTeam = new ModulSeTeam();
        modulSeTeam.setId(1L);
        modulSeTeam.setName("name");
        when(modulService.getAllSeTeams()).thenReturn(Collections.singletonList(modulSeTeam));

        berechtigungFlowService.getBerechtigungForRolle(viewData);

        verify(modulService, times(1)).getAllSeTeams();
        verify(userPermissions, times(1)).getModulSeTeamAsEcocSchreibend();
        verify(viewData, times(1)).setBerechtigungenForRoleWithSchreibrecht(any());
        verify(viewData, times(1)).setBerechtigungenForRoleWithLeserecht(any());
    }

    @Test
    void getBerechtigungForRolle_TTEAM() {
        when(viewData.getSelectedRole()).thenReturn(Rolle.TTEAMMITGLIED);
        when(session.getUserPermissions()).thenReturn(userPermissions);

        Tteam tteam = new Tteam();
        tteam.setId(1L);
        tteam.setTeamName("name");
        when(tteamService.getAllTteams()).thenReturn(Collections.singletonList(tteam));

        TteamMitgliedBerechtigung berechtigung = mock(TteamMitgliedBerechtigung.class);
        when(berechtigung.getTteamIdsMitLeserechten()).thenReturn(Collections.singletonList(2L));
        when(berechtigung.getTteamIdsMitSchreibrechten()).thenReturn(Collections.singletonList(2L));
        when(userPermissions.getTteamMitgliedBerechtigung()).thenReturn(berechtigung);

        berechtigungFlowService.getBerechtigungForRolle(viewData);

        verify(tteamService, times(1)).getAllTteams();
        verify(userPermissions, times(3)).getTteamMitgliedBerechtigung();
        verify(berechtigung, times(2)).getTteamIdsMitSchreibrechten();
        verify(berechtigung, times(1)).getTteamIdsMitLeserechten();
        verify(viewData, times(1)).setBerechtigungenForRoleWithSchreibrecht(any());
        verify(viewData, times(1)).setBerechtigungenForRoleWithLeserecht(any());
    }

    @Test
    void persistAntrag_no_data() {
        berechtigungFlowService.persistAntrag(viewData);
        verifyZeroInteractions(berechtigungAntragService);
    }

    @Test
    void persistAntrag_with_data() {
        BerechtigungIdTuple berechtigungIdTuple1 = new BerechtigungIdTuple(1L, "value");
        BerechtigungIdTuple berechtigungIdTuple2 = new BerechtigungIdTuple(2L, "value2");
        BerechtigungDto berechtigungDto = new BerechtigungDto(BerechtigungTyp.SENSORCOC, Arrays.asList(berechtigungIdTuple1, berechtigungIdTuple2));

        when(session.getUser()).thenReturn(mitarbeiter);
        when(viewData.getSelectedRole()).thenReturn(Rolle.SENSOR);
        when(viewData.getSelectedBerechtigungenWithLeserecht()).thenReturn(Collections.singletonList(berechtigungIdTuple1));
        when(viewData.getSelectedBerechtigungenWithSchreibrecht()).thenReturn(Collections.singletonList(berechtigungIdTuple2));
        when(viewData.getBerechtigungenForRoleWithSchreibrecht()).thenReturn(berechtigungDto);
        when(viewData.isDatenschutzAngenommen()).thenReturn(true);
        when(viewData.getBerechtigungenForRoleWithLeserecht()).thenReturn(berechtigungDto);
        when(userSearchService.isExistsMitarbeiter(any())).thenReturn(true);

        List<Berechtigungsantrag> berechtigungsantrags = berechtigungFlowService.persistAntrag(viewData);

        verify(berechtigungAntragService, times(2)).persistBerechtigungAntrag(any());
        verify(mitarbeiter, times(2)).setDatenschutzAngenommen(true);

        Assert.assertEquals(2, berechtigungsantrags.size());
    }

    @Test
    void persistAntragWithDataAntragAlreadyExistForSensorCoc() {
        BerechtigungIdTuple berechtigungIdTuple1 = new BerechtigungIdTuple(1L, "value");
        BerechtigungIdTuple berechtigungIdTuple2 = new BerechtigungIdTuple(2L, "value2");
        BerechtigungDto berechtigungDto = new BerechtigungDto(BerechtigungTyp.SENSORCOC, Arrays.asList(berechtigungIdTuple1, berechtigungIdTuple2));

        when(session.getUser()).thenReturn(mitarbeiter);
        when(viewData.getSelectedRole()).thenReturn(Rolle.SENSOR);
        when(viewData.getSelectedBerechtigungenWithLeserecht()).thenReturn(Collections.singletonList(berechtigungIdTuple1));
        when(viewData.getSelectedBerechtigungenWithSchreibrecht()).thenReturn(Collections.singletonList(berechtigungIdTuple2));
        when(viewData.getBerechtigungenForRoleWithSchreibrecht()).thenReturn(berechtigungDto);
        when(viewData.isDatenschutzAngenommen()).thenReturn(true);
        when(viewData.getBerechtigungenForRoleWithLeserecht()).thenReturn(berechtigungDto);
        when(berechtigungAntragService.getAntraegeByAntragstellerRolleRechttypSensorCoc(any(), any(), any(), any())).thenReturn(Collections.singletonList(berechtigungsantrag));

        List<Berechtigungsantrag> berechtigungsantrags = berechtigungFlowService.persistAntrag(viewData);

        verify(berechtigungAntragService, never()).persistBerechtigungAntrag(any());
        verify(mitarbeiter, times(2)).setDatenschutzAngenommen(true);

        Assert.assertEquals(0, berechtigungsantrags.size());
    }

    @Test
    void persistAntragWithDataAntragAlreadyExistForTteam() {
        BerechtigungIdTuple berechtigungIdTuple1 = new BerechtigungIdTuple(1L, "value");
        BerechtigungIdTuple berechtigungIdTuple2 = new BerechtigungIdTuple(2L, "value2");
        BerechtigungDto berechtigungDto = new BerechtigungDto(BerechtigungTyp.TTEAM, Arrays.asList(berechtigungIdTuple1, berechtigungIdTuple2));

        when(session.getUser()).thenReturn(mitarbeiter);
        when(viewData.getSelectedRole()).thenReturn(Rolle.SENSOR);
        when(viewData.getSelectedBerechtigungenWithLeserecht()).thenReturn(Collections.singletonList(berechtigungIdTuple1));
        when(viewData.getSelectedBerechtigungenWithSchreibrecht()).thenReturn(Collections.singletonList(berechtigungIdTuple2));
        when(viewData.getBerechtigungenForRoleWithSchreibrecht()).thenReturn(berechtigungDto);
        when(viewData.isDatenschutzAngenommen()).thenReturn(true);
        when(viewData.getBerechtigungenForRoleWithLeserecht()).thenReturn(berechtigungDto);
        when(berechtigungAntragService.getAntraegeByAntragstellerRolleRechttypTteam(any(), any(), any(), any())).thenReturn(Collections.singletonList(berechtigungsantrag));

        List<Berechtigungsantrag> berechtigungsantrags = berechtigungFlowService.persistAntrag(viewData);

        verify(berechtigungAntragService, never()).persistBerechtigungAntrag(any());
        verify(mitarbeiter, times(2)).setDatenschutzAngenommen(true);

        Assert.assertEquals(0, berechtigungsantrags.size());
    }

    @Test
    void persistAntragWithDataAntragAlreadyExistModulSeTeam() {
        BerechtigungIdTuple berechtigungIdTuple1 = new BerechtigungIdTuple(1L, "value");
        BerechtigungIdTuple berechtigungIdTuple2 = new BerechtigungIdTuple(2L, "value2");
        BerechtigungDto berechtigungDto = new BerechtigungDto(BerechtigungTyp.SETEAM, Arrays.asList(berechtigungIdTuple1, berechtigungIdTuple2));

        when(session.getUser()).thenReturn(mitarbeiter);
        when(viewData.getSelectedRole()).thenReturn(Rolle.SENSOR);
        when(viewData.getSelectedBerechtigungenWithLeserecht()).thenReturn(Collections.singletonList(berechtigungIdTuple1));
        when(viewData.getSelectedBerechtigungenWithSchreibrecht()).thenReturn(Collections.singletonList(berechtigungIdTuple2));
        when(viewData.getBerechtigungenForRoleWithSchreibrecht()).thenReturn(berechtigungDto);
        when(viewData.isDatenschutzAngenommen()).thenReturn(true);
        when(viewData.getBerechtigungenForRoleWithLeserecht()).thenReturn(berechtigungDto);
        when(berechtigungAntragService.getAntraegeByAntragstellerRolleRechttypModulSeTeam(any(), any(), any(), any())).thenReturn(Collections.singletonList(berechtigungsantrag));

        List<Berechtigungsantrag> berechtigungsantrags = berechtigungFlowService.persistAntrag(viewData);

        verify(berechtigungAntragService, never()).persistBerechtigungAntrag(any());
        verify(mitarbeiter, times(2)).setDatenschutzAngenommen(true);

        Assert.assertEquals(0, berechtigungsantrags.size());
    }

    @Test
    void persistAntrag_with_new_User() {
        BerechtigungIdTuple berechtigungIdTuple1 = new BerechtigungIdTuple(1L, "value");
        BerechtigungDto berechtigungDto = new BerechtigungDto(BerechtigungTyp.SENSORCOC, Arrays.asList(berechtigungIdTuple1));

        when(session.getUser()).thenReturn(mitarbeiter);
        when(viewData.getSelectedRole()).thenReturn(Rolle.SENSOR);
        when(viewData.getSelectedBerechtigungenWithLeserecht()).thenReturn(Collections.singletonList(berechtigungIdTuple1));
        when(viewData.isDatenschutzAngenommen()).thenReturn(true);
        when(userSearchService.isExistsMitarbeiter(any())).thenReturn(false);
        when(viewData.getBerechtigungenForRoleWithLeserecht()).thenReturn(berechtigungDto);

        List<Berechtigungsantrag> berechtigungsantrags = berechtigungFlowService.persistAntrag(viewData);

        verify(userService, times(1)).saveMitarbeiter(mitarbeiter);

    }

    @Test
    void persistAntrag_with_existing_User() {
        BerechtigungIdTuple berechtigungIdTuple1 = new BerechtigungIdTuple(1L, "value");
        BerechtigungIdTuple berechtigungIdTuple2 = new BerechtigungIdTuple(2L, "value2");
        BerechtigungDto berechtigungDto = new BerechtigungDto(BerechtigungTyp.SENSORCOC, Arrays.asList(berechtigungIdTuple1, berechtigungIdTuple2));

        when(session.getUser()).thenReturn(mitarbeiter);
        when(viewData.getSelectedRole()).thenReturn(Rolle.SENSOR);
        when(viewData.getSelectedBerechtigungenWithLeserecht()).thenReturn(Collections.singletonList(berechtigungIdTuple1));
        when(viewData.getSelectedBerechtigungenWithSchreibrecht()).thenReturn(Collections.singletonList(berechtigungIdTuple2));
        when(viewData.getBerechtigungenForRoleWithSchreibrecht()).thenReturn(berechtigungDto);
        when(viewData.isDatenschutzAngenommen()).thenReturn(true);
        when(userSearchService.isExistsMitarbeiter(any())).thenReturn(true);
        when(viewData.getBerechtigungenForRoleWithLeserecht()).thenReturn(berechtigungDto);

        berechtigungFlowService.persistAntrag(viewData);

        verify(userService, times(0)).saveMitarbeiter(mitarbeiter);

    }

    @Test
    void isDatenschutzAngenommen_no_user() {
        when(session.getUser()).thenReturn(mitarbeiter);
        boolean datenschutzAngenommen = berechtigungFlowService.isDatenschutzAngenommen();
        Assert.assertFalse(datenschutzAngenommen);
    }

    @Test
    void isDatenschutzAngenommen_with_user() {
        when(session.getUser()).thenReturn(mitarbeiter);
        when(mitarbeiter.isDatenschutzAngenommen()).thenReturn(true);
        boolean datenschutzAngenommen = berechtigungFlowService.isDatenschutzAngenommen();
        Assert.assertTrue(datenschutzAngenommen);
    }

    @Test
    void testSendEmailByPersistAntrag() {
        berechtigungFlowService.persistAntrag(viewData);
        verify(berechtigungsantragMailService).sendMailBasedOnAntragStatus(any());
    }
}
