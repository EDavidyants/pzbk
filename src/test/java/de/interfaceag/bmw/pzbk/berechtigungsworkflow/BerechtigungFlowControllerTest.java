package de.interfaceag.bmw.pzbk.berechtigungsworkflow;

import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungFlowViewData;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.assertj.core.api.Java6Assertions;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BerechtigungFlowControllerTest {

    @InjectMocks
    private BerechtigungFlowController berechtigungFlowController;
    @Mock
    private BerechtigungFlowFacade berechtigungFlowFacade;
    @Mock
    private BerechtigungFlowViewData berechtigungFlowViewData;

    @Test
    void updateBerechtigungenForRole() {
        when(berechtigungFlowViewData.getSelectedRole()).thenReturn(Rolle.ADMIN);
        berechtigungFlowController.updateBerechtigungenForRole();
        verify(berechtigungFlowFacade, times(1)).getBerechtigungForRolle(any());
    }

    @Test
    void getViewData() {
        BerechtigungFlowViewData viewdata = berechtigungFlowController.getViewData();
        Java6Assertions.assertThat(viewdata.getChooseableRoles()).isNotNull();
    }

    @Test
    void persistAntragWithDatenschutz() {
        when(berechtigungFlowViewData.isDatenschutzAngenommen()).thenReturn(true);

        berechtigungFlowController.persistAntrag();
        verify(berechtigungFlowFacade, times(1)).persistAntrag(any());
    }

    @Test
    void persistAntragWithoutDatenschutz() {
        when(berechtigungFlowViewData.isDatenschutzAngenommen()).thenReturn(false);

        berechtigungFlowController.persistAntrag();
        verify(berechtigungFlowFacade, never()).persistAntrag(any());
    }

    @Test
    void displayNoneWhenEcoc() {
        when(berechtigungFlowViewData.getSelectedRole()).thenReturn(Rolle.E_COC);
        String style = berechtigungFlowController.displayNoneWhenEcoc();
        Assert.assertEquals("display: none", style);
    }
}