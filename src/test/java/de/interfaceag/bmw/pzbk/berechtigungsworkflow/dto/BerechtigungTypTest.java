package de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.assertj.core.api.Java6Assertions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

class BerechtigungTypTest {

    @Test
    void getBerechtigungTypByRolleSensor() {
        Optional<BerechtigungTyp> optionalBerechtigungTypByRolle = BerechtigungTyp.getBerechtigungTypByRolle(Rolle.SENSOR);
        final BerechtigungTyp berechtigungTyp = optionalBerechtigungTypByRolle.get();
        Java6Assertions.assertThat(berechtigungTyp).isEqualByComparingTo(BerechtigungTyp.SENSORCOC);
    }

    @Test
    void getBerechtigungTypByRolleSensorEingeschraenkt() {
        Optional<BerechtigungTyp> optionalBerechtigungTypByRolle = BerechtigungTyp.getBerechtigungTypByRolle(Rolle.SENSOR_EINGESCHRAENKT);
        final BerechtigungTyp berechtigungTyp = optionalBerechtigungTypByRolle.get();
        Java6Assertions.assertThat(berechtigungTyp).isEqualByComparingTo(BerechtigungTyp.SENSORCOC);
    }

    @Test
    void getBerechtigungTypByRolleECoc() {
        Optional<BerechtigungTyp> optionalBerechtigungTypByRolle = BerechtigungTyp.getBerechtigungTypByRolle(Rolle.E_COC);
        final BerechtigungTyp berechtigungTyp = optionalBerechtigungTypByRolle.get();
        Java6Assertions.assertThat(berechtigungTyp).isEqualByComparingTo(BerechtigungTyp.SETEAM);
    }

    @Test
    void getBerechtigungTypByRolleTteamMitglied() {
        Optional<BerechtigungTyp> optionalBerechtigungTypByRolle = BerechtigungTyp.getBerechtigungTypByRolle(Rolle.TTEAMMITGLIED);
        final BerechtigungTyp berechtigungTyp = optionalBerechtigungTypByRolle.get();
        Java6Assertions.assertThat(berechtigungTyp).isEqualByComparingTo(BerechtigungTyp.TTEAM);
    }

    @Test
    void getBerechtigungTypByRolleAdmin() {
        Optional<BerechtigungTyp> optionalBerechtigungTypByRolle = BerechtigungTyp.getBerechtigungTypByRolle(Rolle.ADMIN);
        Assertions.assertFalse(optionalBerechtigungTypByRolle.isPresent());
    }


}