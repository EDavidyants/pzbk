package de.interfaceag.bmw.pzbk.berechtigungsworkflow;

import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungFlowViewData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BerechtigungFlowFacadeTest {

    @InjectMocks
    private BerechtigungFlowFacade berechtigungFlowFacade;
    @Mock
    private BerechtigungFlowService berechtigungFlowService;
    @Mock
    private BerechtigungFlowViewData berechtigungFlowViewData;

    @Test
    void getBerechtigungForRolle() {
        berechtigungFlowFacade.getBerechtigungForRolle(berechtigungFlowViewData);
        verify(berechtigungFlowService, times(1)).getBerechtigungForRolle(any());
    }

    @Test
    void persistAntrag() {
        berechtigungFlowFacade.persistAntrag(berechtigungFlowViewData);

        verify(berechtigungFlowService, times(1)).persistAntrag(any());
    }

    @Test
    void isDatenschutzAngenommen() {
        when(berechtigungFlowService.isDatenschutzAngenommen()).thenReturn(true);

        berechtigungFlowFacade.isDatenschutzAngenommen();
        verify(berechtigungFlowService, times(1)).isDatenschutzAngenommen();

    }

}