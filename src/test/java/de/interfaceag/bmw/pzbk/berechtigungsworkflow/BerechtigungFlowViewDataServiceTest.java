package de.interfaceag.bmw.pzbk.berechtigungsworkflow;

import de.interfaceag.bmw.pzbk.berechtigungsworkflow.dto.BerechtigungFlowViewData;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.assertj.core.api.Java6Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BerechtigungFlowViewDataServiceTest {

    @InjectMocks
    private BerechtigungFlowViewDataService berechtigungFlowViewDataService;

    @Test
    void buildDefaultViewData() {
        BerechtigungFlowViewData berechtigungFlowViewData = berechtigungFlowViewDataService.buildDefaultViewData();

        Java6Assertions.assertThat(berechtigungFlowViewData.getChooseableRoles()).hasSize(4);
        Java6Assertions.assertThat(berechtigungFlowViewData.getSelectedRole()).isEqualByComparingTo(Rolle.SENSOR);

    }
}