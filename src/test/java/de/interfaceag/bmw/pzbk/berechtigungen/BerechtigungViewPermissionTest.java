package de.interfaceag.bmw.pzbk.berechtigungen;

import de.interfaceag.bmw.pzbk.berechtigung.BerechtigungViewPermission;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author fn
 */
public class BerechtigungViewPermissionTest {

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testBuildPermissionForBerechtigungPage(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        BerechtigungViewPermission viewPermission = BerechtigungViewPermission.builder()
                .forUserRoles(userRoles)
                .build();

        boolean result = viewPermission.getPage();
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case SENSORCOCLEITER:
                Assertions.assertTrue(result);
                break;
            default:
                Assertions.assertFalse(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testBuildPermissionForBerechtigungSensorCocFilter(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        BerechtigungViewPermission viewPermission = BerechtigungViewPermission.builder()
                .forUserRoles(userRoles)
                .build();

        boolean result = viewPermission.getSensorCocFilter();
        switch (role) {
            case ADMIN:
            case SENSORCOCLEITER:
                Assertions.assertTrue(result);
                break;
            default:
                Assertions.assertFalse(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testBuildPermissionForBerechtigungTteamFilter(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        BerechtigungViewPermission viewPermission = BerechtigungViewPermission.builder()
                .forUserRoles(userRoles)
                .build();

        boolean result = viewPermission.getTteamFilter();
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
                Assertions.assertTrue(result);
                break;
            default:
                Assertions.assertFalse(result);
                break;
        }
    }

}
