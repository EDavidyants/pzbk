package de.interfaceag.bmw.pzbk.berechtigungen;

import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.permissions.components.MitarbeiterBearbeiterDialogViewPermission;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author fn
 */
public class MitarbeiterBearbeitenDialogViewPermissionTest {

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testBuildPermissionForMitarbeiterBearbeiterDialogAddPermissionButton(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        MitarbeiterBearbeiterDialogViewPermission viewPermission = new MitarbeiterBearbeiterDialogViewPermission(new HashSet<>(Rolle.getAllRolles()), userRoles);

        boolean result = viewPermission.getAddPermissionButton();
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
            case SENSORCOCLEITER:
                Assertions.assertTrue(result);
                break;
            default:
                Assertions.assertFalse(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testBuildPermissionForMitarbeiterBearbeiterDialogRemoveRoleButton(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        MitarbeiterBearbeiterDialogViewPermission viewPermission = new MitarbeiterBearbeiterDialogViewPermission(new HashSet<>(Rolle.getAllRolles()), userRoles);

        boolean result = viewPermission.getRemoveRoleButton();
        switch (role) {
            case ADMIN:
                Assertions.assertTrue(result);
                break;
            default:
                Assertions.assertFalse(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testBuildPermissionForMitarbeiterBearbeiterDialogSensorTab(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        MitarbeiterBearbeiterDialogViewPermission viewPermission = new MitarbeiterBearbeiterDialogViewPermission(new HashSet<>(Rolle.getAllRolles()), userRoles);

        boolean result = viewPermission.getSensorTab();
        switch (role) {
            case ADMIN:
            case SENSORCOCLEITER:
                Assertions.assertTrue(result);
                break;
            default:
                Assertions.assertFalse(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testBuildPermissionForMitarbeiterBearbeiterDialogAdminTab(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        MitarbeiterBearbeiterDialogViewPermission viewPermission = new MitarbeiterBearbeiterDialogViewPermission(new HashSet<>(Rolle.getAllRolles()), userRoles);

        boolean result = viewPermission.getAdminTab();
        switch (role) {
            case ADMIN:
                Assertions.assertTrue(result);
                break;
            default:
                Assertions.assertFalse(result);
                break;
        }
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testBuildPermissionForMitarbeiterBearbeiterDialogTteamleiterTab(Rolle role) {
        Set<Rolle> userRoles = new HashSet<>();
        userRoles.add(role);

        MitarbeiterBearbeiterDialogViewPermission viewPermission = new MitarbeiterBearbeiterDialogViewPermission(new HashSet<>(Rolle.getAllRolles()), userRoles);

        boolean result = viewPermission.getTteamLeiterTab();
        switch (role) {
            case ADMIN:
            case T_TEAMLEITER:
                Assertions.assertTrue(result);
                break;
            default:
                Assertions.assertFalse(result);
                break;
        }
    }
}
