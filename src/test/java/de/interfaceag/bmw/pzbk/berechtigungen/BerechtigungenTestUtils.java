package de.interfaceag.bmw.pzbk.berechtigungen;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class BerechtigungenTestUtils {

    private static final Logger LOG = LoggerFactory.getLogger(BerechtigungenTestUtils.class);

    public static final String EXCEL_FILE_PATH = "src/main/resources/TestTemplates/ExcelTestExample.xlsx";

    public static Sheet getTestSheet() {
        Sheet sheet = null;
        try {
            XSSFWorkbook wb = new XSSFWorkbook(EXCEL_FILE_PATH);
            sheet = wb.getSheet("Rechte");
        } catch (IOException ex) {
            LOG.error(null, ex);
        }
        return sheet;
    }
}
