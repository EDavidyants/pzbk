package de.interfaceag.bmw.pzbk.berechtigungen;

import de.interfaceag.bmw.pzbk.berechtigung.BerechtigungsExcelRealm;
import de.interfaceag.bmw.pzbk.berechtigung.BerechtigungsUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class BerechtigungsUtilsTest {

    private static final String Q1 = "Q1 (eingeschränkter Sensor)";
    private static final String Q2 = "Q2 (Sensor)";
    private static final String Q3 = "Q3 (SensorCoC-Leiter)";
    private static final String Q4 = "Q4 (Anforderer)";

    public BerechtigungsUtilsTest() {
    }

    @Test
    public void testLoadBerechtigungenFromSheet() {
        Sheet sheet = BerechtigungenTestUtils.getTestSheet();
        Collection<BerechtigungsExcelRealm> brs = BerechtigungsUtils.loadBerechtigungenFromExcelSheet(sheet, "Q-Nummer");

        assertEquals(4, brs.size());
        List<BerechtigungsExcelRealm> realms = new ArrayList<>(brs);

        BerechtigungsExcelRealm r = realms.get(0);
        assertEquals(Q1, r.getQnumber());
        assertEquals(true, r.isEingeschraenkt());
        assertEquals(2, r.getSensors().size());
        assertEquals(3, r.getLeserechte().size());
        assertEquals(0, r.getSensorCocs().size());
        assertEquals(0, r.getZakEinordnungen().size());
        assertEquals(0, r.getPlVorschlaege().size());

        r = realms.get(1);
        assertEquals(Q2, r.getQnumber());
        assertEquals(false, r.isEingeschraenkt());
        assertEquals(2, r.getSensors().size());
        assertEquals(3, r.getLeserechte().size());
        assertEquals(0, r.getSensorCocs().size());
        assertEquals(0, r.getZakEinordnungen().size());
        assertEquals(0, r.getPlVorschlaege().size());

        r = realms.get(2);
        assertEquals(Q3, r.getQnumber());
        assertEquals(false, r.isEingeschraenkt());
        assertEquals(0, r.getSensors().size());
        assertEquals(5, r.getLeserechte().size());
        assertEquals(3, r.getSensorCocs().size());
        assertEquals(0, r.getZakEinordnungen().size());
        assertEquals(0, r.getPlVorschlaege().size());

        r = realms.get(3);
        assertEquals(Q4, r.getQnumber());
        assertEquals(false, r.isEingeschraenkt());
        assertEquals(0, r.getSensors().size());
        assertEquals(0, r.getLeserechte().size());
        assertEquals(0, r.getSensorCocs().size());
        assertEquals(1, r.getZakEinordnungen().size());
        assertEquals(2, r.getPlVorschlaege().size());
    }

}
