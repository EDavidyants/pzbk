package de.interfaceag.bmw.pzbk.berechtigungen;

import de.interfaceag.bmw.pzbk.berechtigung.BerechtigungsExcelRealm;
import de.interfaceag.bmw.pzbk.berechtigung.BerechtigungsUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class BerechtigungsExcelRealmTest {

    public BerechtigungsExcelRealmTest() {
    }

    /**
     * Test of hashCode method, of class BerechtigungsExcelRealm.
     */
    @Test
    public void testHashCode() {
        Sheet sheet = BerechtigungenTestUtils.getTestSheet();
        List<BerechtigungsExcelRealm> brs1 = new ArrayList<>(BerechtigungsUtils.loadBerechtigungenFromExcelSheet(sheet, "Q-Nummer"));
        List<BerechtigungsExcelRealm> brs2 = new ArrayList<>(BerechtigungsUtils.loadBerechtigungenFromExcelSheet(sheet, "Q-Nummer"));
        assertTrue(brs1.get(0).hashCode() == brs2.get(0).hashCode());
        assertTrue(brs1.get(0).hashCode() != brs2.get(1).hashCode());
    }

    /**
     * Test of equals method, of class BerechtigungsExcelRealm.
     */
    @Test
    public void testEquals() {
        Sheet sheet = BerechtigungenTestUtils.getTestSheet();
        List<BerechtigungsExcelRealm> brs1 = new ArrayList<>(BerechtigungsUtils.loadBerechtigungenFromExcelSheet(sheet, "Q-Nummer"));
        List<BerechtigungsExcelRealm> brs2 = new ArrayList<>(BerechtigungsUtils.loadBerechtigungenFromExcelSheet(sheet, "Q-Nummer"));
        assertTrue(brs1.get(0).equals(brs2.get(0)));
        assertFalse(brs1.get(0).equals(brs2.get(1)));
    }

}
