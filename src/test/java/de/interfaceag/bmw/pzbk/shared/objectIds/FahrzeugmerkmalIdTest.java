package de.interfaceag.bmw.pzbk.shared.objectIds;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FahrzeugmerkmalIdTest {

    private FahrzeugmerkmalId fahrzeugmerkmalId;

    @BeforeEach
    void setUp() {
        fahrzeugmerkmalId = new FahrzeugmerkmalId(42L);
    }

    @Test
    void toStringTest() {
        final String result = fahrzeugmerkmalId.toString();
        MatcherAssert.assertThat(result, CoreMatchers.is("FahrzeugmerkmalId[id=42]"));
    }

    @Test
    void getId() {
        final Long id = fahrzeugmerkmalId.getId();
        assertThat(id, is(42L));
    }

    @Test
    void equalsOtherClass() {
        final boolean result = fahrzeugmerkmalId.equals(new MeldungId(42L));
        assertFalse(result);
    }

    @Test
    void equalsSameObject() {
        final boolean result = fahrzeugmerkmalId.equals(fahrzeugmerkmalId);
        assertTrue(result);
    }

    @Test
    void hashCodeTest() {
        final int hashCode = fahrzeugmerkmalId.hashCode();
        assertThat(hashCode, is(73));
    }

}