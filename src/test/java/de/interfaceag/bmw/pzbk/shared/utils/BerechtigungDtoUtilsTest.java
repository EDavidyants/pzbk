package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungEditDtoImpl;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author evda
 */
public class BerechtigungDtoUtilsTest {

    private List<BerechtigungDto> userBerechtigungen;
    private BerechtigungDto sensorCocBerechtigungSensor;
    private BerechtigungDto sensorCocBerechtigungSensorTwo;
    private BerechtigungDto sensorCocBerechtigungSensorEing;
    private BerechtigungDto sensorCocBerechtigungSCL;
    private BerechtigungDto sensorCocBerechtigungSCLVertreter;
    private BerechtigungDto sensorCocBerechtigungUBOne;
    private BerechtigungDto sensorCocBerechtigungUNTwo;
    private BerechtigungDto zakEinordnungBerechtigungAnforderer;
    private BerechtigungDto derivatBerechtigungAnforderer;
    private BerechtigungDto tteamBerechtigungTTL;
    private BerechtigungDto tteamBerechtigungTTLVertreter;
    private BerechtigungDto modulSeTeamBerechtigungEcoc;

    @Before
    public void setUp() {
        userBerechtigungen = new ArrayList<>();
        sensorCocBerechtigungSensor = new BerechtigungEditDtoImpl(3L, "TGF_Bedienkomfort", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SENSOR);
        sensorCocBerechtigungSensorTwo = new BerechtigungEditDtoImpl(4L, "TGF_Antrieb", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SENSOR);
        sensorCocBerechtigungSensorEing = new BerechtigungEditDtoImpl(5L, "TGF_E-Mobilität", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SENSOR_EINGESCHRAENKT);
        sensorCocBerechtigungSCL = new BerechtigungEditDtoImpl(4L, "TGF_Antrieb", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SENSORCOCLEITER);
        sensorCocBerechtigungSCLVertreter = new BerechtigungEditDtoImpl(6L, "TGF_Sitze", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.SCL_VERTRETER);
        sensorCocBerechtigungUBOne = new BerechtigungEditDtoImpl(4L, "TGF_Antrieb", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.UMSETZUNGSBESTAETIGER);
        sensorCocBerechtigungUNTwo = new BerechtigungEditDtoImpl(6L, "TGF_Sitze", BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT, Rolle.UMSETZUNGSBESTAETIGER);
        zakEinordnungBerechtigungAnforderer = new BerechtigungEditDtoImpl(null, "AK Direkt / AG: Herstellbarkeit SSA", BerechtigungZiel.ZAK_EINORDNUNG, Rechttype.SCHREIBRECHT, Rolle.ANFORDERER);
        derivatBerechtigungAnforderer = new BerechtigungEditDtoImpl(1L, "E20", BerechtigungZiel.DERIVAT, Rechttype.SCHREIBRECHT, Rolle.ANFORDERER);
        tteamBerechtigungTTL = new BerechtigungEditDtoImpl(12L, "AF", BerechtigungZiel.TTEAM, Rechttype.SCHREIBRECHT, Rolle.T_TEAMLEITER);
        tteamBerechtigungTTLVertreter = new BerechtigungEditDtoImpl(9L, "IuK", BerechtigungZiel.TTEAM, Rechttype.SCHREIBRECHT, Rolle.TTEAM_VERTRETER);
        modulSeTeamBerechtigungEcoc = new BerechtigungEditDtoImpl(4L, "SeTeam 1", BerechtigungZiel.MODULSETEAM, Rechttype.SCHREIBRECHT, Rolle.E_COC);

        userBerechtigungen.add(sensorCocBerechtigungSensor);
        userBerechtigungen.add(sensorCocBerechtigungSensorTwo);
        userBerechtigungen.add(sensorCocBerechtigungSensorEing);
        userBerechtigungen.add(sensorCocBerechtigungSCL);
        userBerechtigungen.add(sensorCocBerechtigungSCLVertreter);
        userBerechtigungen.add(sensorCocBerechtigungUBOne);
        userBerechtigungen.add(sensorCocBerechtigungUNTwo);
        userBerechtigungen.add(zakEinordnungBerechtigungAnforderer);
        userBerechtigungen.add(derivatBerechtigungAnforderer);
        userBerechtigungen.add(tteamBerechtigungTTL);
        userBerechtigungen.add(tteamBerechtigungTTLVertreter);
        userBerechtigungen.add(modulSeTeamBerechtigungEcoc);
    }

    @Test
    public void testSelectTteamBerechtigungenSize() {
        List<Long> berechtigungIds = BerechtigungDtoUtils.selectTteamBerechtigungen(userBerechtigungen);
        Assertions.assertEquals(2, berechtigungIds.size());
    }

    @Test
    public void testSelectTteamBerechtigungenElements() {
        List<Long> berechtigungIds = BerechtigungDtoUtils.selectTteamBerechtigungen(userBerechtigungen);
        Assertions.assertTrue(berechtigungIds.contains(12L));
        Assertions.assertTrue(berechtigungIds.contains(9L));
    }

    @Test
    public void testSelectZakEinordnungBerechtigingenSize() {
        List<String> berechtigungen = BerechtigungDtoUtils.selectZakEinordnungBerechtigingen(userBerechtigungen);
        Assertions.assertEquals(1, berechtigungen.size());
    }

    @Test
    public void testSelectZakEinordnungBerechtigingenElements() {
        List<String> berechtigungen = BerechtigungDtoUtils.selectZakEinordnungBerechtigingen(userBerechtigungen);
        Assertions.assertTrue(berechtigungen.contains("AK Direkt / AG: Herstellbarkeit SSA"));
    }

    @Test
    public void testSelectDerivatBerechtigungenSize() {
        List<Long> berechtigungIds = BerechtigungDtoUtils.selectDerivatBerechtigungen(userBerechtigungen);
        Assertions.assertEquals(1, berechtigungIds.size());
    }

    @Test
    public void testSelectDerivatBerechtigungenElements() {
        List<Long> berechtigungIds = BerechtigungDtoUtils.selectDerivatBerechtigungen(userBerechtigungen);
        Assertions.assertTrue(berechtigungIds.contains(1L));
    }

    @Test
    public void testSelectSensorCocBerechtigingenSize() {
        List<Long> berechtigungIds = BerechtigungDtoUtils.selectSensorCocBerechtigingen(userBerechtigungen);
        Assertions.assertEquals(7, berechtigungIds.size());
    }

    @Test
    public void testSelectSensorCocBerechtigingenElements() {
        List<Long> berechtigungIds = BerechtigungDtoUtils.selectSensorCocBerechtigingen(userBerechtigungen);
        Assertions.assertTrue(berechtigungIds.contains(3L));
        Assertions.assertTrue(berechtigungIds.contains(4L));
        Assertions.assertTrue(berechtigungIds.contains(5L));
        Assertions.assertTrue(berechtigungIds.contains(6L));
    }

    @Test
    public void testSelectSensorCocBerechtigungenForUmsetzungbestaetigungSize() {
        List<Long> berechtigungIds = BerechtigungDtoUtils.selectSensorCocBerechtigungenForUmsetzungbestaetigung(userBerechtigungen);
        Assertions.assertEquals(2, berechtigungIds.size());
    }

    @Test
    public void testSelectSensorCocBerechtigungenForUmsetzungbestaetigungElements() {
        List<Long> berechtigungIds = BerechtigungDtoUtils.selectSensorCocBerechtigungenForUmsetzungbestaetigung(userBerechtigungen);
        Assertions.assertTrue(berechtigungIds.contains(4L));
        Assertions.assertTrue(berechtigungIds.contains(6L));
    }

    @Test
    public void testSelectSensorCocBerechtigungenForSCLAndVertreterSize() {
        List<Long> berechtigungIds = BerechtigungDtoUtils.selectSensorCocBerechtigungenForSCLAndVertreter(userBerechtigungen);
        Assertions.assertEquals(2, berechtigungIds.size());
    }

    @Test
    public void testSelectSensorCocBerechtigungenForSCLAndVertreterElements() {
        List<Long> berechtigungIds = BerechtigungDtoUtils.selectSensorCocBerechtigungenForSCLAndVertreter(userBerechtigungen);
        Assertions.assertTrue(berechtigungIds.contains(4L));
        Assertions.assertTrue(berechtigungIds.contains(6L));
    }

    @Test
    public void testSelectModulSeTeamBerechtigingenSize() {
        List<Long> berechtigungIds = BerechtigungDtoUtils.selectModulSeTeamBerechtigingen(userBerechtigungen);
        Assertions.assertEquals(1, berechtigungIds.size());
    }

    @Test
    public void testSelectModulSeTeamBerechtigingenElements() {
        List<Long> berechtigungIds = BerechtigungDtoUtils.selectModulSeTeamBerechtigingen(userBerechtigungen);
        Assertions.assertTrue(berechtigungIds.contains(4L));
    }

}
