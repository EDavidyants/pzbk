package de.interfaceag.bmw.pzbk.shared.utils;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class ZipUtilsTest {

    private static final Logger LOG = LoggerFactory.getLogger(ZipUtilsTest.class);

    @Test
    public void compressString() {
        try {
            ZipUtils.compress("test");
        } catch (IOException ex) {
            LOG.error(ex.toString());
            Assertions.fail(ex);
        }
    }

    @Test
    public void compressDecompressString() {
        final String string = "test";
        try {
            byte[] compress = ZipUtils.compress(string);
            String result = ZipUtils.decompress(compress);
            Assertions.assertEquals(string, result);
        } catch (IOException ex) {
            LOG.error(ex.toString());
            Assertions.fail(ex);
        }
    }

}
