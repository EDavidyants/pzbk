package de.interfaceag.bmw.pzbk.shared.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author Christian Schauer christian.schauer at interface-ag.de
 */
public class HtmlUtilsTest {

    public HtmlUtilsTest() {
    }

    /**
     * Test of insertHtmlLineBreaks method, of class HtmlUtils.
     */
    @Test
    public void testInsertHtmlLineBreaks() {
        String input = "a\nb";
        String expResult = "a<br>b";
        String result = HtmlUtils.insertHtmlLineBreaks(input);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTitle method, of class HtmlUtils.
     */
    @Test
    public void testGetTitle() {
        String title = "hello world";
        String content = "abc<title>" + title + "</title>";
        String expResult = title;
        String result = HtmlUtils.getTitle(content);
        assertEquals(expResult, result);

        content = "abc <title>";
        expResult = "";
        result = HtmlUtils.getTitle(content);
        assertEquals(expResult, result);

        content = "abc";
        expResult = "";
        result = HtmlUtils.getTitle(content);
        assertEquals(expResult, result);
    }

}
