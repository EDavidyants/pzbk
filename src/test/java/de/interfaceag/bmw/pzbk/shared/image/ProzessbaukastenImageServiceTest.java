package de.interfaceag.bmw.pzbk.shared.image;

import de.interfaceag.bmw.pzbk.services.ImageService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
@RunWith(MockitoJUnitRunner.class)
public class ProzessbaukastenImageServiceTest {

    @Mock
    private ImageService imageService;

    private Map<String, String> requestParameterMap;

    private ProzessbaukastenImageParameters imageParameters;

    @InjectMocks
    private final ProzessbaukastenImageService prozessbaukastenImageService = new ProzessbaukastenImageService();

    @Before
    public void setup() {
        requestParameterMap = new HashMap<>();
        requestParameterMap.put("fachId", "A12");
        requestParameterMap.put("version", "1");
        requestParameterMap.put("isLargeImage", "true");
        when(imageService.getProzessbaukastenGrafikUmfangBild(any(), anyInt(), any())).thenReturn(new byte[1]);
        when(imageService.getProzessbaukastenKonzeptbaumBild(any(), anyInt(), any())).thenReturn(new byte[1]);
        when(imageService.loadDefaultImage()).thenReturn(new byte[1]);
    }

    @Test
    public void getProzessbaukastenGrafikUmfangImageNullImage() {
        requestParameterMap.put("bild", "grafikUmfang");
        imageParameters = ProzessbaukastenImageParameters.forRequest(requestParameterMap);

        when(imageService.getProzessbaukastenGrafikUmfangBild(any(), anyInt(), any())).thenReturn(null);
        prozessbaukastenImageService.getProzessbaukastenImage(imageParameters);
        verify(imageService, times(1)).loadDefaultImage();
    }

    @Test
    public void getProzessbaukastenKonzeptbaumImageNullImage() {
        requestParameterMap.put("bild", "konzeptbaum");
        imageParameters = ProzessbaukastenImageParameters.forRequest(requestParameterMap);

        when(imageService.getProzessbaukastenKonzeptbaumBild(any(), anyInt(), any())).thenReturn(null);
        prozessbaukastenImageService.getProzessbaukastenImage(imageParameters);
        verify(imageService, times(1)).loadDefaultImage();
    }

    @Test
    public void getProzessbaukastenGrafikUmfangImage() {
        requestParameterMap.put("bild", "grafikUmfang");
        imageParameters = ProzessbaukastenImageParameters.forRequest(requestParameterMap);

        prozessbaukastenImageService.getProzessbaukastenImage(imageParameters);
        verify(imageService, times(1)).getProzessbaukastenGrafikUmfangBild(any(), anyInt(), any());
    }

    @Test
    public void getProzessbaukastenKonzeptbaumImage() {
        requestParameterMap.put("bild", "konzeptbaum");
        imageParameters = ProzessbaukastenImageParameters.forRequest(requestParameterMap);

        prozessbaukastenImageService.getProzessbaukastenImage(imageParameters);
        verify(imageService, times(1)).getProzessbaukastenKonzeptbaumBild(any(), anyInt(), any());
    }

}
