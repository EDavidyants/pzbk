package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.AnforderungFreigabeBeiModulSeTeam;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AnforderungUtilsTest {

    @Mock
    private Anforderung anforderung;
    @Mock
    private AnforderungFreigabeBeiModulSeTeam freigabe1;
    @Mock
    private AnforderungFreigabeBeiModulSeTeam freigabe2;

    @Test
    void isStandardvereinbarungNoneZak() {
        final Boolean zak1 = Boolean.FALSE;
        final Boolean zak2 = Boolean.FALSE;
        setupFristFreigabe(zak1);
        setupSecondFreigabe(zak2);
        setupFreigaben();
        final boolean result = AnforderungUtils.isStandardvereinbarung(anforderung);
        assertThat(result, is(Boolean.TRUE));
    }

    @Test
    void isStandardvereinbarungFirstZak() {
        final Boolean zak1 = Boolean.TRUE;
        setupFristFreigabe(zak1);
        setupFreigaben();
        final boolean result = AnforderungUtils.isStandardvereinbarung(anforderung);
        assertThat(result, is(Boolean.FALSE));
    }

    @Test
    void isStandardvereinbarungSecondZak() {
        final Boolean zak1 = Boolean.FALSE;
        final Boolean zak2 = Boolean.TRUE;
        setupFristFreigabe(zak1);
        setupSecondFreigabe(zak2);
        setupFreigaben();
        final boolean result = AnforderungUtils.isStandardvereinbarung(anforderung);
        assertThat(result, is(Boolean.FALSE));
    }


    @Test
    void isStandardvereinbarungAllZak() {
        final Boolean zak1 = Boolean.TRUE;
        setupFristFreigabe(zak1);
        setupFreigaben();
        final boolean result = AnforderungUtils.isStandardvereinbarung(anforderung);
        assertThat(result, is(Boolean.FALSE));
    }

    private void setupFreigaben() {
        List<AnforderungFreigabeBeiModulSeTeam> freigaben = Arrays.asList(freigabe1, freigabe2);
        when(anforderung.getAnfoFreigabeBeiModul()).thenReturn(freigaben);
    }

    private void setupFristFreigabe(Boolean zak1) {
        when(freigabe1.isZak()).thenReturn(zak1);
    }

    private void setupSecondFreigabe(Boolean zak2) {
        when(freigabe2.isZak()).thenReturn(zak2);
    }
}