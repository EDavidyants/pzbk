package de.interfaceag.bmw.pzbk.shared.mail;

import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.mail.MailArgs;
import de.interfaceag.bmw.pzbk.services.ConfigService;
import java.util.Date;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class MailGeneratorServiceTest {

    @Mock
    private MailArgs mailValues;

    @Mock
    private LocalizationService localizationService;

    @Mock
    private ConfigService configService;

    @Mock
    private Date date;

    @InjectMocks
    private MailGeneratorService mailGeneratorService;

    private static final String RECIPIENT = "test@interface-ag.de";
    private static Session mailSession;

    @BeforeEach
    public void setUp() {
        mailSession = MailSessionInitializer.getMailSession();
        mailGeneratorService = mock(MailGeneratorService.class);
    }

    @Test
    public void testGenerateLogoHtml() {
        when(mailGeneratorService.generateLogoHtml()).thenCallRealMethod();
        String expected = "<html><div id='headerDiv' style='margin: 0;'>"
                + "<img src='cid:z158mTnRp' alt='BMW Logo'/>"
                + "</div><br/></html>";
        String html = mailGeneratorService.generateLogoHtml();
        Assertions.assertEquals(expected, html);
    }

    @Test
    public void testCreateNoReplyMailMessageNotNull() throws MessagingException {
        when(mailGeneratorService.getMailSession()).thenReturn(mailSession);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();
        Message message = mailGeneratorService.createNoReplyMailMessage("Application receipt confirmation", RECIPIENT, new MimeBodyPart[]{new MimeBodyPart()});
        Assertions.assertNotNull(message);
    }

    @Test
    public void testCreateNoReplyMailMessageSubject() throws MessagingException {
        when(mailGeneratorService.getMailSession()).thenReturn(mailSession);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();
        Message message = mailGeneratorService.createNoReplyMailMessage("Application receipt confirmation", RECIPIENT, new MimeBodyPart[]{new MimeBodyPart()});
        String subject = message.getSubject();
        Assertions.assertEquals("Application receipt confirmation", subject);
    }

    @Test
    public void testCreateNoReplyMailMessageFromAddress() throws MessagingException {
        when(mailGeneratorService.getMailSession()).thenReturn(mailSession);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();
        Message message = mailGeneratorService.createNoReplyMailMessage("Application receipt confirmation", RECIPIENT, new MimeBodyPart[]{new MimeBodyPart()});
        String fromAddress = message.getFrom()[0].toString();
        Assertions.assertEquals("PZBK-Mail-Service@interface-ag.de", fromAddress);
    }

    @Test
    public void testCreateNoReplyMailMessageRecipientAddress() throws MessagingException {
        when(mailGeneratorService.getMailSession()).thenReturn(mailSession);
        when(mailGeneratorService.createNoReplyMailMessage(any(), any(), any())).thenCallRealMethod();
        Message message = mailGeneratorService.createNoReplyMailMessage("Application receipt confirmation", RECIPIENT, new MimeBodyPart[]{new MimeBodyPart()});
        String fromAddress = message.getRecipients(Message.RecipientType.TO)[0].toString();
        Assertions.assertEquals(RECIPIENT, fromAddress);
    }

}
