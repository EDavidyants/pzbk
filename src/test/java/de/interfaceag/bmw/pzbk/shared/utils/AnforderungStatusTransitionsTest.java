package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.StatusUtils;
import de.interfaceag.bmw.pzbk.enums.Status;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AnforderungStatusTransitionsTest {

    @Test
    public void testStatusTransitionsForAInArbeit() {
        Set<Status> result = StatusUtils.getAllNextStatusAsSet(Status.A_INARBEIT);
        Set<Status> expextedResult = new HashSet<>(Arrays.asList(Status.A_UNSTIMMIG,
                Status.A_FTABGESTIMMT, Status.A_GELOESCHT));
        assertEquals(expextedResult, result);
    }

    @Test
    public void testStatusTransitionsForAUnstimmig() {
        Set<Status> result = StatusUtils.getAllNextStatusAsSet(Status.A_UNSTIMMIG);
        Set<Status> expextedResult = new HashSet<>(Arrays.asList(Status.A_FTABGESTIMMT, Status.A_GELOESCHT));
        assertEquals(expextedResult, result);
    }

    @Test
    public void testStatusTransitionsForAAbgestimmt() {
        Set<Status> result = StatusUtils.getAllNextStatusAsSet(Status.A_FTABGESTIMMT);
        Set<Status> expextedResult = new HashSet<>(Arrays.asList(Status.A_PLAUSIB,
                Status.A_UNSTIMMIG, Status.A_GELOESCHT));
        assertEquals(expextedResult, result);
    }

    @Test
    public void testStatusTransitionsForAPlausibilisiert() {
        Set<Status> result = StatusUtils.getAllNextStatusAsSet(Status.A_PLAUSIB);
        Set<Status> expextedResult = new HashSet<>(Arrays.asList(Status.A_UNSTIMMIG, Status.A_GELOESCHT));
        assertEquals(expextedResult, result);
    }

    @Test
    public void testStatusTransitionsForAFreigegeben() {
        Set<Status> result = StatusUtils.getAllNextStatusAsSet(Status.A_FREIGEGEBEN);
        Set<Status> expextedResult = new HashSet<>(Arrays.asList(Status.A_KEINE_WEITERVERFOLG));
        assertEquals(expextedResult, result);
    }

    @Test
    public void testStatusTransitionsForAGeloescht() {
        Set<Status> result = StatusUtils.getAllNextStatusAsSet(Status.A_KEINE_WEITERVERFOLG);
        Set<Status> expextedResult = new HashSet<>(Arrays.asList(Status.A_GELOESCHT));
        assertEquals(expextedResult, result);
    }

}
