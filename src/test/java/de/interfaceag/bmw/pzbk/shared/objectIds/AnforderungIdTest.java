package de.interfaceag.bmw.pzbk.shared.objectIds;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AnforderungIdTest {

    private AnforderungId anforderungId;

    @BeforeEach
    void setUp() {
        anforderungId = new AnforderungId(42L);
    }

    @Test
    void getId() {
        final Long id = anforderungId.getId();
        assertThat(id, is(42L));
    }

    @Test
    void equalsOtherClass() {
        final boolean result = anforderungId.equals(new MeldungId(42L));
        assertFalse(result);
    }

    @Test
    void equalsSameObject() {
        final boolean result = anforderungId.equals(anforderungId);
        assertTrue(result);
    }

    @Test
    void hashCodeTest() {
        final int hashCode = anforderungId.hashCode();
        assertThat(hashCode, is(73));
    }
}