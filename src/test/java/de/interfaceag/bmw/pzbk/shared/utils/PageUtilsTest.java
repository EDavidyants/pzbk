package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.enums.Page;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ActiveIndex;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenTab;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class PageUtilsTest {

    @Test
    public void getParamterAsStringNull() {
        String result = PageUtils.getParamterAsString(null);
        assertEquals("?faces-redirect=true", result);
    }

    @Test
    public void getParamterAsStringEmpty() {
        Map<String, String> paramterMap = new HashMap<>();
        String result = PageUtils.getParamterAsString(paramterMap);
        assertEquals("?faces-redirect=true", result);
    }

    @Test
    public void getParamterAsStringOneParamter() {
        Map<String, String> paramterMap = new HashMap<>();
        paramterMap.put("id", Long.toString(1L));
        String result = PageUtils.getParamterAsString(paramterMap);
        assertEquals("?id=1&faces-redirect=true", result);
    }

    @Test
    public void getParamterAsStringTwoParamters() {
        Map<String, String> paramterMap = new HashMap<>();
        paramterMap.put("test", "true");
        paramterMap.put("id", Long.toString(1L));
        String result = PageUtils.getParamterAsString(paramterMap);
        assertEquals("?test=true&id=1&faces-redirect=true", result);
    }

    @Test
    public void getRedirectUrlForPage() {
        Page page = Page.DASHBOARD;
        String result = PageUtils.getRedirectUrlForPage(page);
        assertEquals("dashboard.xhtml?faces-redirect=true", result);
    }

    @Test
    public void getUrlForPageWithParamter() {
        Page page = Page.DASHBOARD;
        Map<String, String> paramterMap = new HashMap<>();
        paramterMap.put("id", Long.toString(1L));
        String result = PageUtils.getUrlForPageWithParamter(page, paramterMap);
        assertEquals("dashboard.xhtml?id=1&faces-redirect=true", result);
    }

    @Test
    public void getUrlForPageWithNoParamter() {
        Page page = Page.DASHBOARD;
        Map<String, String> paramterMap = new HashMap<>();
        String result = PageUtils.getUrlForPageWithParamter(page, paramterMap);
        assertEquals("dashboard.xhtml?faces-redirect=true", result);
    }

    @Test
    public void getUrlForPageWithNullParamter() {
        Page page = Page.DASHBOARD;
        String result = PageUtils.getUrlForPageWithParamter(page, null);
        assertEquals("dashboard.xhtml?faces-redirect=true", result);
    }

    @Test
    public void getUrlForPageWithIdNotSupported() {
        Page page = Page.DASHBOARD;
        String result = PageUtils.getUrlForPageWithId(page, 1);
        assertEquals("", result);
    }

    @Test
    public void getUrlForPageWithIdSupported() {
        Page page = Page.ANFORDERUNGVIEW;
        String result = PageUtils.getUrlForPageWithId(page, 1);
        assertEquals("anforderungView.xhtml?id=1&faces-redirect=true", result);
    }

    @Test
    public void testGetUrlForProzessbaukastenView() {
        String fachId = "fachId";
        Integer version = 1;
        ActiveIndex activeIndex = ActiveIndex.forTab(ProzessbaukastenTab.ANFORDERUNGEN);
        String result = PageUtils.getUrlForProzessbaukastenView(fachId, version, activeIndex);
        assertEquals("prozessbaukastenView.xhtml?activeIndex=1&fachId=fachId&version=1&faces-redirect=true", result);
    }

    @Test
    public void testGetUrlForProzessbaukastenEdit() {
        String fachId = "fachId";
        String version = "1";
        ActiveIndex activeIndex = ActiveIndex.forTab(ProzessbaukastenTab.ANFORDERUNGEN);
        String result = PageUtils.getUrlForProzessbaukastenEdit(fachId, version, activeIndex);
        assertEquals("prozessbaukastenEdit.xhtml?activeIndex=1&fachId=fachId&version=1&faces-redirect=true", result);
    }

}
