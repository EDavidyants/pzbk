package de.interfaceag.bmw.pzbk.shared.objectIds;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FahrzeugmerkmalAuspraegungIdTest {

    private FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId;

    @BeforeEach
    void setUp() {
        fahrzeugmerkmalAuspraegungId = new FahrzeugmerkmalAuspraegungId(42L);
    }

    @Test
    void getId() {
        final Long id = fahrzeugmerkmalAuspraegungId.getId();
        assertThat(id, is(42L));
    }

    @Test
    void equalsOtherClass() {
        final boolean result = fahrzeugmerkmalAuspraegungId.equals(new MeldungId(42L));
        assertFalse(result);
    }

    @Test
    void equalsSameObject() {
        final boolean result = fahrzeugmerkmalAuspraegungId.equals(fahrzeugmerkmalAuspraegungId);
        assertTrue(result);
    }

    @Test
    void hashCodeTest() {
        final int hashCode = fahrzeugmerkmalAuspraegungId.hashCode();
        assertThat(hashCode, is(73));
    }

}