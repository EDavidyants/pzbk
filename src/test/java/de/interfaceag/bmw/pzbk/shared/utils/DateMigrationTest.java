package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.migration.DateMigrationUtils;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author sl
 */
public class DateMigrationTest {

    @Test
    public void checkForValidErstellungsdatumUpdateAfterTest() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2018, 1, 2);
        Date currentErstellungsdatum = calendar.getTime();
        Date newDate = new Date();
        boolean result = DateMigrationUtils.checkForValidDateUpdate(currentErstellungsdatum, newDate);
        Assertions.assertFalse(result);
    }

    @Test
    public void checkForValidErstellungsdatumUpdateBeforeTest() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2018, 0, 2);
        Date currentErstellungsdatum = calendar.getTime();
        Date newDate = new Date();

        boolean result = DateMigrationUtils.checkForValidDateUpdate(currentErstellungsdatum, newDate);
        Assertions.assertTrue(result);
    }

    @Test
    public void checkForValidErstellungsdatumUpdateNullCurrentTest() {
        Date currentErstellungsdatum = null;
        Date newDate = new Date();
        boolean result = DateMigrationUtils.checkForValidDateUpdate(currentErstellungsdatum, newDate);
        Assertions.assertTrue(result);
    }

    @Test
    public void checkForValidErstellungsdatumUpdateNullNewTest() {
        Date currentErstellungsdatum = new Date();
        Date newDate = null;
        boolean result = DateMigrationUtils.checkForValidDateUpdate(currentErstellungsdatum, newDate);
        Assertions.assertFalse(result);
    }

}
