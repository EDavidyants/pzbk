package de.interfaceag.bmw.pzbk.shared;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

class RunTimeTest {

    @Test
    void startIsNotNull() {
        final RunTime runTime = RunTime.start();
        assertThat(runTime, notNullValue());
    }

    @Test
    void stopIsNotNull() {
        final RunTime runTime = RunTime.start().stop();
        assertThat(runTime, notNullValue());
    }

    @Test
    void getTimeWithoutStop() {
        final long time = RunTime.start().getTime();
        assertThat(time, is(0L));
    }

    @Test
    void getTimeWithStop() throws InterruptedException {
        final RunTime runTime = RunTime.start();
        Thread.sleep(1);
        final long time = runTime.stop().getTime();
        assertThat(time, Matchers.greaterThan(0L));
    }
}