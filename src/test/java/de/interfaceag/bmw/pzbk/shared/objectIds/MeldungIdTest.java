package de.interfaceag.bmw.pzbk.shared.objectIds;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class MeldungIdTest {

    private MeldungId meldungId;

    @BeforeEach
    void setUp() {
        meldungId = new MeldungId(42L);
    }

    @Test
    void getId() {
        final Long id = meldungId.getId();
        assertThat(id, is(42L));
    }
}