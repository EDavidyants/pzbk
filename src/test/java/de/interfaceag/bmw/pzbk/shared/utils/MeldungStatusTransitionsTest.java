package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.StatusUtils;
import de.interfaceag.bmw.pzbk.enums.Status;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class MeldungStatusTransitionsTest {

    @Test
    public void testStatusTransitionsForMEntwurf() {
        Set<Status> result = StatusUtils.getAllNextStatusAsSet(Status.M_ENTWURF);
        Set<Status> expextedResult = new HashSet<>(Arrays.asList(Status.M_GELOESCHT, Status.M_GEMELDET));
        assertEquals(expextedResult, result);
    }

    @Test
    public void testStatusTransitionsForMGemeldet() {
        Set<Status> result = StatusUtils.getAllNextStatusAsSet(Status.M_GEMELDET);
        Set<Status> expextedResult = new HashSet<>(Arrays.asList(Status.M_UNSTIMMIG,
                Status.M_ZUGEORDNET, Status.M_GELOESCHT));
        assertEquals(expextedResult, result);
    }

    @Test
    public void testStatusTransitionsForMUnstimmig() {
        Set<Status> result = StatusUtils.getAllNextStatusAsSet(Status.M_UNSTIMMIG);
        Set<Status> expextedResult = new HashSet<>(Arrays.asList(Status.M_GEMELDET, Status.M_GELOESCHT));
        assertEquals(expextedResult, result);
    }

    @Test
    public void testStatusTransitionsForMZugeordnet() {
        Set<Status> result = StatusUtils.getAllNextStatusAsSet(Status.M_ZUGEORDNET);
        Set<Status> expextedResult = new HashSet<>(Arrays.asList(Status.A_INARBEIT));
        assertEquals(expextedResult, result);
    }

}
