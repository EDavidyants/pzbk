package de.interfaceag.bmw.pzbk.shared.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author sl
 */
public class DateUtilsTest {

    private Date date;
    private LocalDate localDate;

    @BeforeEach
    public void setUp() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1992, 4, 7, 6, 52, 0);
        date = calendar.getTime();
        localDate = LocalDateTime.ofInstant(calendar.toInstant(), calendar.getTimeZone().toZoneId()).toLocalDate();
    }

    @Test
    public void testConvertToLocalDate() {
        LocalDate result = DateUtils.convertToLocalDate(date);
        assertThat(localDate, is(result));
    }

    @Test
    public void testGetDateForLocalDateNull() {
        Date result = DateUtils.getDateForLocalDate(null);
        assertThat(result, is(nullValue()));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 4, 5, 6, 7})
    void isMonday(int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2019, 04, day);
        final Date monday = calendar.getTime();
        final boolean result = DateUtils.isMonday(monday);

        if (day == 6) {
            assertThat(result, is(Boolean.TRUE));
        } else {
            assertThat(result, is(Boolean.FALSE));
        }

    }

    @Test
    void convertLongToDaysZero() {
        final long days = DateUtils.convertLongToDays(0L);
        assertThat(days, is(0L));
    }

    @Test
    void convertLongToDaysOneDay() {
        final long days = DateUtils.convertLongToDays(86400000L);
        assertThat(days, is(1L));
    }

    @Test
    void convertLongToDaysTwoDays() {
        final long days = DateUtils.convertLongToDays(2*86400000L);
        assertThat(days, is(2L));
    }

    @Test
    public void testGetMinDate() {
        Calendar expected = Calendar.getInstance();
        expected.set(2018, 2, 1);

        Date minDate = DateUtils.getMinDate();
        Calendar result = Calendar.getInstance();
        result.setTime(minDate);

        Assertions.assertTrue(isSameDay(expected, result));
    }

    @Test
    public void testGetMaxDate() {
        Date maxDate = DateUtils.getMaxDate();
        Calendar result = Calendar.getInstance();
        result.setTime(maxDate);

        Calendar today = Calendar.getInstance();
        today.setTime(new Date());

        Assertions.assertTrue(isSameDay(result, today));
    }

    private static boolean isSameDay(Calendar calendar1, Calendar calendar2) {
        return calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR)
                && calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR);
    }

}
