package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author evda
 */
public class ObjectTypeValidationUtilsTest {

    @Test
    public void isMeldung() {
        Meldung meldung = new Meldung();
        Assertions.assertTrue(ObjectTypeValidationUtils.isMeldung(meldung));
    }

    @Test
    public void isNotMeldung() {
        Anforderung anforderung = new Anforderung();
        Assertions.assertFalse(ObjectTypeValidationUtils.isMeldung(anforderung));

        Prozessbaukasten prozessbaukasten = new Prozessbaukasten();
        Assertions.assertFalse(ObjectTypeValidationUtils.isMeldung(prozessbaukasten));
    }

    @Test
    public void isAnforderung() {
        Anforderung anforderung = new Anforderung();
        Assertions.assertTrue(ObjectTypeValidationUtils.isAnforderung(anforderung));
    }

    @Test
    public void isNotAnforderung() {
        Meldung meldung = new Meldung();
        Assertions.assertFalse(ObjectTypeValidationUtils.isAnforderung(meldung));

        Prozessbaukasten prozessbaukasten = new Prozessbaukasten();
        Assertions.assertFalse(ObjectTypeValidationUtils.isAnforderung(prozessbaukasten));
    }

    @Test
    public void isProzessbaukasten() {
        Prozessbaukasten prozessbaukasten = new Prozessbaukasten();
        Assertions.assertTrue(ObjectTypeValidationUtils.isProzessbaukasten(prozessbaukasten));
    }

    @Test
    public void isNotProzessbaukasten() {
        Meldung meldung = new Meldung();
        Assertions.assertFalse(ObjectTypeValidationUtils.isProzessbaukasten(meldung));

        Anforderung anforderung = new Anforderung();
        Assertions.assertFalse(ObjectTypeValidationUtils.isProzessbaukasten(anforderung));
    }

    @Test
    public void isAnforderungOhneMeldung() {
        Anforderung anforderung = new Anforderung();
        Assertions.assertTrue(ObjectTypeValidationUtils.isAnforderungOhneMeldung(anforderung));

        anforderung.setMeldungen(new HashSet<>());
        Assertions.assertTrue(ObjectTypeValidationUtils.isAnforderungOhneMeldung(anforderung));
    }

    @Test
    public void isNotAnforderungOhneMeldung() {
        Anforderung anforderung = new Anforderung();
        Set<Meldung> meldungen = new HashSet<>();
        meldungen.add(new Meldung());
        anforderung.setMeldungen(meldungen);
        Assertions.assertFalse(ObjectTypeValidationUtils.isAnforderungOhneMeldung(anforderung));
    }

}
