package de.interfaceag.bmw.pzbk.shared.mail;

import java.io.Serializable;
import java.util.Properties;
import javax.mail.Session;

/**
 *
 * @author evda
 */
public class MailSessionInitializer implements Serializable {

    public static Session getMailSession() {
        Properties properties = new Properties();
        properties.put("mail.smtp.user", "PZBK-Mail-Service@interface-ag.de");
        properties.put("mail.smtp.password", "Dak04609");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.socketFactory.port", "587");
        properties.put("mail.smtp.socketFactory.fallback", "false");
        properties.put("mail.smtp.ssl.trust", "smtp.office365.com");
        properties.put("mail.smtp.ssl.enable", "false");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.starttls.required", "true");

        Session session = Session.getInstance(properties);
        return session;
    }

}
