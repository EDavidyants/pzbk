package de.interfaceag.bmw.pzbk.shared.math;

import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class MeanTest {

    private Stream<Integer> input;

    @Test
    void computeMeanEmptyInput() {
        input = Stream.empty();
        final Integer result = Mean.computeMean(input);
        assertThat(result, is(0));
    }

    @Test
    void computeMeanOneInput() {
        input = Stream.of(1);
        final Integer result = Mean.computeMean(input);
        assertThat(result, is(1));
    }

    @Test
    void computeMeanTwoInputs() {
        input = Stream.of(1, 2);
        final Integer result = Mean.computeMean(input);
        assertThat(result, is(2));
    }

    @Test
    void computeMeanThreeInputs() {
        input = Stream.of(1, 42, 3);
        final Integer result = Mean.computeMean(input);
        assertThat(result, is(15));
    }

    @Test
    void computeMeanFourInputs() {
        input = Stream.of(1, 2, 3, 67);
        final Integer result = Mean.computeMean(input);
        assertThat(result, is(18));
    }
}