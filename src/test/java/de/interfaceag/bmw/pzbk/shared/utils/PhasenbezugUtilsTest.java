package de.interfaceag.bmw.pzbk.shared.utils;

import de.interfaceag.bmw.pzbk.enums.Phasenbezug;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class PhasenbezugUtilsTest {

    @Test
    public void getPhasenbezugForBooleanTrue() {
        Phasenbezug result = PhasenbezugUtils.getPhasenbezugForBoolean(true);
        Assertions.assertEquals("Konzeptrelevant", result.getBeschreibung());
    }

    @Test
    public void getPhasenbezugForBooleanFalse() {
        Phasenbezug result = PhasenbezugUtils.getPhasenbezugForBoolean(false);
        Assertions.assertEquals("Architekturrelevant", result.getBeschreibung());
    }

    @Test
    public void getPhasenbezugForStringTrue() {
        Phasenbezug result = PhasenbezugUtils.getPhasenbezugForString("Konzeptrelevant");
        Assertions.assertEquals(true, result.getValue());
    }

    @Test
    public void getPhasenbezugForStringFalse() {
        Phasenbezug result = PhasenbezugUtils.getPhasenbezugForString("Architekturrelevant");
        Assertions.assertEquals(false, result.getValue());
    }

    @Test
    public void getAllPhasenbezugSize() {
        long count = PhasenbezugUtils.getAllPhasenbezug().count();
        Assertions.assertEquals(2L, count);
    }

}
