package de.interfaceag.bmw.pzbk.shared.objectIds;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsIterableContaining.hasItems;

class DomainObjectFactoryTest {

    private Collection<Long> ids;

    @BeforeEach
    void setUp() {
        ids = Arrays.asList(1L, 2L, 42L);
    }

    @Test
    void forAnforderungIdsSize() {
        final Collection<AnforderungId> anforderungIds = ObjectIdFactory.forAnforderungIds(ids);
        assertThat(anforderungIds, hasSize(3));
    }

    @Test
    void forAnforderungIdsContains() {
        final Collection<AnforderungId> anforderungIds = ObjectIdFactory.forAnforderungIds(ids);
        assertThat(anforderungIds, hasItems(new AnforderungId(1L), new AnforderungId(2L), new AnforderungId(42L)));
    }

    @Test
    void forMeldungIdsSize() {
        final Collection<MeldungId> anforderungIds = ObjectIdFactory.forMeldungIds(ids);
        assertThat(anforderungIds, hasSize(3));
    }

    @Test
    void forMeldungIdsContains() {
        final Collection<MeldungId> anforderungIds = ObjectIdFactory.forMeldungIds(ids);
        assertThat(anforderungIds, hasItems(new MeldungId(1L), new MeldungId(2L), new MeldungId(42L)));
    }
}