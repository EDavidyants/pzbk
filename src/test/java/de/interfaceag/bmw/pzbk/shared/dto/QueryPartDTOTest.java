package de.interfaceag.bmw.pzbk.shared.dto;

import org.hamcrest.collection.IsMapWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author sl
 */
public class QueryPartDTOTest {

    private static final String QUERY = "query";
    private static final String SORT = "sort";

    private QueryPartDTO queryPart;

    @BeforeEach
    public void setup() {
        queryPart = new QueryPartDTO();
    }

    @Test
    public void testEmptyConstructorQuery() {
        String query = queryPart.getQuery();
        assertThat(query, is(""));
    }

    @Test
    public void testEmptyConstructorParameterMap() {
        Map<String, Object> parameterMap = queryPart.getParameterMap();
        assertThat(parameterMap, IsMapWithSize.aMapWithSize(0));
    }

    @Test
    public void testConstructorQuery() {
        queryPart = new QueryPartDTO(QUERY);
        String query = queryPart.getQuery();
        assertThat(query, is(QUERY));
    }

    @Test
    public void testConstructorParameterMap() {
        queryPart = new QueryPartDTO(QUERY);
        Map<String, Object> parameterMap = queryPart.getParameterMap();
        assertThat(parameterMap, IsMapWithSize.aMapWithSize(0));
    }

    @Test
    public void testMergeQuery() {
        QueryPart queryPart1 = new QueryPartDTO(QUERY);
        QueryPart queryPart2 = new QueryPartDTO(QUERY);
        QueryPart result = queryPart1.merge(queryPart2);
        String query = result.getQuery();
        assertThat(query, is(QUERY.concat(QUERY)));
    }

    @Test
    public void testMergeParametersMapSize() {
        QueryPart queryPart1 = new QueryPartDTO(QUERY);
        queryPart1.put("key1", "value1");
        QueryPart queryPart2 = new QueryPartDTO(QUERY);
        queryPart2.put("key2", "value2");
        QueryPart result = queryPart1.merge(queryPart2);
        Map<String, Object> parameterMap = result.getParameterMap();
        assertThat(parameterMap, IsMapWithSize.aMapWithSize(2));
    }

    @Test
    public void testAppend() {
        queryPart.append(QUERY);
        String query = queryPart.getQuery();
        assertThat(query, is(QUERY));
    }

    @Test
    public void testDeleteFrom() {
        queryPart.append("TEST");
        final QueryPartDTO result = queryPart.deleteFrom(2);
        final String resultQuery = result.getQuery();
        assertThat(resultQuery, is("TE"));
    }

    @Test
    public void testPut() {
        queryPart.put("key1", "value1");
        Map<String, Object> parameterMap = queryPart.getParameterMap();
        assertThat(parameterMap, IsMapWithSize.aMapWithSize(1));
    }

    @Test
    public void testPutAll() {
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("key1", "value1");
        parameterMap.put("key2", "value2");
        queryPart.putAll(parameterMap);
        Map<String, Object> result = queryPart.getParameterMap();
        assertThat(result, IsMapWithSize.aMapWithSize(2));
    }

    @Test
    public void testToString() {
        queryPart = new QueryPartDTO(QUERY);
        String toString = queryPart.toString();
        String query = queryPart.getQuery();
        assertThat(toString, is(query));
    }

    @Test
    public void testGetQuery() {
        queryPart = new QueryPartDTO(QUERY);
        String query = queryPart.getQuery();
        assertThat(query, is(QUERY));
    }

    @Test
    public void testGetQueryWithSorting() {
        queryPart = new QueryPartDTO(QUERY);
        queryPart.setSort(SORT);
        String query = queryPart.getQueryWithSorting();
        assertThat(query, is(QUERY.concat(" ").concat(SORT)));
    }

    @Test
    public void testSetQuery() {
        queryPart.setQuery(QUERY);
        String query = queryPart.getQuery();
        assertThat(query, is(QUERY));
    }

    @Test
    public void testGetSort() {
        queryPart = new QueryPartDTO(QUERY);
        queryPart.setSort(SORT);
        String query = queryPart.getSort();
        assertThat(query, is(SORT));
    }

    @Test
    public void testAppendNativeParametersQuery() {
        Collection<Long> values = Arrays.asList(1L, 2L);
        queryPart.appendNativeParameters(values);
        String query = queryPart.getQuery();
        assertThat(query, is("(?,?)"));
    }

    @Test
    public void testAppendNativeParametersParameterMap() {
        Collection<Long> values = Arrays.asList(1L, 2L);
        queryPart.appendNativeParameters(values);
        Map<String, Object> result = queryPart.getParameterMap();
        assertThat(result, IsMapWithSize.aMapWithSize(2));
    }

    @Test
    public void testPutNativeParameter() {
        queryPart.putNativeParameter(1L);
        Map<String, Object> result = queryPart.getParameterMap();
        assertThat(result, IsMapWithSize.aMapWithSize(1));
    }

}
