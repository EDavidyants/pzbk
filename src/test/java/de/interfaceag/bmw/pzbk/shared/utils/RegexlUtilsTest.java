package de.interfaceag.bmw.pzbk.shared.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class RegexlUtilsTest {

    @ParameterizedTest(name = "matchesFachIdTrue #{index} with [{arguments}]")
    @ValueSource(strings = {"A1", "A12", "M1", "M12", "A100000", "M100000"})
    void matchesFachIdTrue(String s) {
        Assertions.assertTrue(RegexUtils.matchesFachId(s));
    }

    @ParameterizedTest
    @ValueSource(strings = {"12", "A"})
    void matchesFachIdFalse(String s) {
        Assertions.assertFalse(RegexUtils.matchesFachId(s));
    }

    @ParameterizedTest(name = "matchesFachIdTrue #{index} with [{arguments}]")
    @ValueSource(strings = {"A1", "A12", "A100000"})
    void matchesAnforderungFachIdTrue(String s) {
        Assertions.assertTrue(RegexUtils.matchesAnforderungFachId(s));
    }

    @ParameterizedTest
    @ValueSource(strings = {"12", "A", "M1", "M12", "M100000"})
    void matchesAnforderungFachIddFalse(String s) {
        Assertions.assertFalse(RegexUtils.matchesAnforderungFachId(s));
    }

    @ParameterizedTest(name = "matchesFachIdTrue #{index} with [{arguments}]")
    @ValueSource(strings = {"M1", "M12", "M100000"})
    void matchesMeldungFachIdTrue(String s) {
        Assertions.assertTrue(RegexUtils.matchesMeldungFachId(s));
    }

    @ParameterizedTest
    @ValueSource(strings = {"12", "A", "M", "A12", "A1", "A10000"})
    void matchesMeldungFachIdFalse(String s) {
        Assertions.assertFalse(RegexUtils.matchesMeldungFachId(s));
    }

    @ParameterizedTest
    @ValueSource(strings = {"12", "13215161", "0"})
    void matchesZakUebertragungIdTrue(String s) {
        Assertions.assertTrue(RegexUtils.matchesZakUebertragungId(s));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "A", "b123"})
    void matchesZakUebertragungIdFalse(String s) {
        Assertions.assertFalse(RegexUtils.matchesZakUebertragungId(s));
    }
    
    @ParameterizedTest
    @ValueSource(strings = {"","a","asdfghj","q1234567","aq123456","aq1234567"})
    void matchesQnumberFalse(String qnumber){
        Assertions.assertFalse(RegexUtils.matchesQnumber(qnumber));
    }
    @ParameterizedTest
    @ValueSource(strings = {"q123456","qx12345","qabcerf","qx12fd3"})
    void matchesQnumberTrue(String qnumber){
         Assertions.assertTrue(RegexUtils.matchesQnumber(qnumber));
    } 

}
