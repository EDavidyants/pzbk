package de.interfaceag.bmw.pzbk.systemMonitoring;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.SystemMonitoringDto;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.SystemMonitoringViewData;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static de.interfaceag.bmw.pzbk.systemMonitoring.SystemMonitoringFacade.DERIVAT_FILTER_MATCHER;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SystemMonitoringFacadeTest {

    @InjectMocks
    private SystemMonitoringFacade facade;

    @Mock
    private SystemMonitoringService systemMonitoringService;

    @BeforeEach
    void setUp() {
    }

    @Test
    void test_derivat_filter_matcher() {
        Assert.assertEquals(DERIVAT_FILTER_MATCHER, "Derivat ");
    }

    @Test
    void test_InitViewData_no_filter_selection() {
        List<SystemMonitoringDto> response = new ArrayList<>();
        when(this.systemMonitoringService.getAllZakUebertragungForTimeRange(any(), any(), any())).thenReturn(response);

        SystemMonitoringViewData systemMonitoringViewData = this.facade.changeFilterSetting(new Date(), new Date(), null);
        Assert.assertTrue(systemMonitoringViewData.getMonitoringDtoList().isEmpty());
    }

    @Test
    void test_InitViewData_with_filter_selection() {
        when(this.systemMonitoringService.getAllZakUebertragungForTimeRange(any(Date.class), any(), any()))
                .thenReturn(Collections.singletonList(this.getSystemMonitoringDto()));

        SystemMonitoringViewData systemMonitoringViewData = this.facade.changeFilterSetting(new Date(), new Date(), null);

        Assert.assertEquals(1, systemMonitoringViewData.getMonitoringDtoList().size());
    }

    @Test
    void test_addLogEntry() {
        this.facade.addLogEntry(any(), any(), any());
        verify(systemMonitoringService, times(1)).addLogEntry(any(), any(), any());
    }

    @Test
    void test_getAllDerivate() {
        when(systemMonitoringService.getAllDerivate()).thenReturn(Collections.emptyList());

        List<Derivat> derivate = this.facade.getAllDerivate();
        verify(this.systemMonitoringService, times(1)).getAllDerivate();
        Assert.assertTrue(derivate.isEmpty());
    }

    private SystemMonitoringDto getSystemMonitoringDto() {
        return SystemMonitoringDto.newBuilder()
                .withAenderungsdatum(new Date())
                .withAnforderungFachId("f855202b-96ce-41ad-abe5-5567ad09a361")
                .withAnforderungStatus(Status.A_FREIGEGEBEN)
                .withDerivatId(1L)
                .withDerivatName("DerivatName")
                .withDerivatProduktlinie("Li")
                .withDerivatStatus(DerivatStatus.OFFEN)
                .withNachZakUebertragen(false)
                .withPreviousDerivatStatus(DerivatStatus.OFFEN)
                .withUebertragungId(1L)
                .withZakId("95a733f8-b4d8-4dba-ba67-df6849b37219")
                .withZakRepsonse("zak repsonse")
                .withZakstatus(ZakStatus.EMPTY)
                .withZakUebertragungErfolgreich(false)
                .build();
    }

}
