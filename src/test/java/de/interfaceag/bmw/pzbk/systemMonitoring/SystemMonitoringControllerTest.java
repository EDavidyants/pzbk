package de.interfaceag.bmw.pzbk.systemMonitoring;

import de.interfaceag.bmw.pzbk.filter.SingleDerivatFilter;
import de.interfaceag.bmw.pzbk.filter.VonBisDateSearchFilter;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ContextMocker;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.SystemMonitoringViewData;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.SystemMonitoringViewFilter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SystemMonitoringControllerTest {

    @InjectMocks
    private SystemMonitoringController systemMonitoringController;
    @Mock
    private SystemMonitoringViewFilter systemMonitoringViewFilter;
    @Mock
    private SystemMonitoringFacade systemMonitoringFacade;
    @Mock
    private VonBisDateSearchFilter vonBisDateSearchFilter;
    @Mock
    private SingleDerivatFilter derivatFilter;

    @BeforeEach
    void setUp() {
        ContextMocker.mockFacesContextForComponentLookup();

        when(systemMonitoringFacade.changeFilterSetting(any(), any(), any())).thenReturn(initalData());
        when(systemMonitoringViewFilter.getDatumFilter()).thenReturn(vonBisDateSearchFilter);
        when(systemMonitoringViewFilter.getDerivatFilter()).thenReturn(derivatFilter);
        when(systemMonitoringViewFilter.getDatumFilter().getVon()).thenReturn(new Date());
        when(systemMonitoringViewFilter.getDatumFilter().getBis()).thenReturn(new Date());
        this.systemMonitoringController.changeFilterEvent();
    }

    @Test
    void changeFilterEvent() {
        verify(systemMonitoringViewFilter, times(4)).getDatumFilter();
        verify(systemMonitoringFacade, times(1)).changeFilterSetting(any(Date.class), any(Date.class), eq(null));
    }

    private SystemMonitoringViewData initalData() {
        return SystemMonitoringViewData.newBuilder()
                .withLogEntryDtosList(Collections.emptyList())
                .withMonitoringDtoList(Collections.emptyList())
                .build();
    }
}