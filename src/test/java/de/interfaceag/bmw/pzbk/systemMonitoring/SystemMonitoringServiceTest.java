package de.interfaceag.bmw.pzbk.systemMonitoring;

import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.LogService;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.LogEntryDto;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.SystemMonitoringDto;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.SystemMonitoringTestData;
import de.interfaceag.bmw.pzbk.zak.ZakUebertragungService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SystemMonitoringServiceTest {

    @InjectMocks
    private SystemMonitoringService monitoringService;

    @Mock
    private ZakUebertragungService zakUebertragungService;

    @Mock
    private DerivatService derivatService;

    @Mock
    private LogService logService;

    @Test
    void test_getAllZakUebertragungForTimeRange() {
        when(this.zakUebertragungService.getAllZakUebertragungForTimeRange(any(Date.class), any(), any()))
                .thenReturn(Collections.singletonList(getZakUebtragung()));

        List<SystemMonitoringDto> allZakUebertragungForTimeRange = this.monitoringService.getAllZakUebertragungForTimeRange(new Date(), null, null);
        verify(zakUebertragungService, times(1)).getAllZakUebertragungForTimeRange(any(), any(), any());
        Assert.assertEquals(1, allZakUebertragungForTimeRange.size());
    }

    @Test
    void test_getAllDerivate() {
        when(this.derivatService.getAllDerivate()).thenReturn(Collections.singletonList(this.getDerivat()));

        List<Derivat> allDerivate = this.monitoringService.getAllDerivate();
        verify(derivatService, times(1)).getAllDerivate();
        Assert.assertEquals(1, allDerivate.size());
    }

    @Test
    void test_getZAKLogEntriesForTimeRange() {
        when(this.logService.getZAKLogEntriesForTimeRange(any(Date.class), any(Date.class))).thenReturn(Collections.emptyList());
        List<LogEntryDto> logs = this.monitoringService.getZAKLogEntriesForTimeRange(new Date(), new Date());
        verify(logService, times(1)).getZAKLogEntriesForTimeRange(any(), any());
        Assert.assertTrue(logs.isEmpty());
    }

    @Test
    void test_addLogEntry() {
        this.monitoringService.addLogEntry(any(), any(), any());
        verify(logService, times(1)).addLogEntry(any(), any(), any());
    }

    private ZakUebertragung getZakUebtragung() {
        return SystemMonitoringTestData.getZakuebertragung();
    }

    private Derivat getDerivat() {
        return SystemMonitoringTestData.getDerivat();
    }
}
