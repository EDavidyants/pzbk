package de.interfaceag.bmw.pzbk.systemMonitoring.model;

import de.interfaceag.bmw.pzbk.entities.LogEntry;
import de.interfaceag.bmw.pzbk.enums.LogLevel;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Date;

class LogEntryDtoMapperTest {

    @Test
    void test_buildLogEntryDto_nullValue() {
        Assert.assertEquals(LogEntryDtoMapper.buildLogEntryDto(null), null);
    }

    @Test
    void test_buildLogEntryDto() {
        LogEntryDto logEntryDto = LogEntryDtoMapper.buildLogEntryDto(this.getLogEntry());

        Assert.assertEquals(logEntryDto.getId(), (Long) 1L);
        Assert.assertNotNull(logEntryDto.getDate());
        Assert.assertEquals(logEntryDto.getLogLevel(), LogLevel.INFO);
        Assert.assertEquals(logEntryDto.getLogValue(), "value");
    }

    private LogEntry getLogEntry() {
        LogEntry log = new LogEntry();
        log.setId(1L);
        log.setLogLevel(LogLevel.INFO);
        log.setLogValue("value");
        log.setDatum(new Date());
        return log;
    }
}