package de.interfaceag.bmw.pzbk.systemMonitoring.model;

import de.interfaceag.bmw.pzbk.derivat.DerivatStatus;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;

import java.util.Date;

public final class SystemMonitoringTestData {

    private SystemMonitoringTestData() {

    }

    public static ZakUebertragung getZakuebertragung() {
        ZakUebertragung zakUebertragung = new ZakUebertragung();
        zakUebertragung.setId(1L);
        zakUebertragung.setUebertragungId(1L);
        zakUebertragung.setZakStatus(ZakStatus.DONE);
        zakUebertragung.setAenderungsdatum(new Date());
        zakUebertragung.setZakId("9d19d259-9977-49e0-870a-ebcceea90674");
        zakUebertragung.setZakResponse("response");

        zakUebertragung.setDerivatAnforderungModul(getDerivatAnforderungModul(zakUebertragung));

        return zakUebertragung;
    }

    public static DerivatAnforderungModul getDerivatAnforderungModul(ZakUebertragung zakUebertragung) {
        DerivatAnforderungModul derivatAnforderungModul = new DerivatAnforderungModul();
        derivatAnforderungModul.setZuordnungAnforderungDerivat(getZuordnungAnforderungDerivat());
        derivatAnforderungModul.setVereinbarung(zakUebertragung);
        derivatAnforderungModul.setId(1L);
        derivatAnforderungModul.setZakStatus(ZakStatus.DONE);
        derivatAnforderungModul.setModul(getModul());
        derivatAnforderungModul.setAnforderer(new Mitarbeiter());

        return derivatAnforderungModul;
    }

    public static ZuordnungAnforderungDerivat getZuordnungAnforderungDerivat() {
        ZuordnungAnforderungDerivat zuordnungAnforderungDerivat = new ZuordnungAnforderungDerivat();
        zuordnungAnforderungDerivat.setDerivat(getDerivat());
        zuordnungAnforderungDerivat.setStatus(ZuordnungStatus.ZUGEORDNET);
        zuordnungAnforderungDerivat.setNachZakUebertragen(false);
        zuordnungAnforderungDerivat.setZurKenntnisGenommen(false);
        zuordnungAnforderungDerivat.setZakUebertragungErfolgreich(false);
        zuordnungAnforderungDerivat.setAnforderung(getAnforderung());
        zuordnungAnforderungDerivat.setId(1L);

        return zuordnungAnforderungDerivat;
    }

    public static Anforderung getAnforderung() {
        Anforderung anforderung = new Anforderung();
        anforderung.setId(1L);
        anforderung.setVersion(1);
        return anforderung;
    }

    public static Modul getModul() {
        Modul modul = new Modul();
        modul.setId(1L);
        modul.setFachBereich("fachbereich");
        modul.setName("Name");
        return modul;
    }

    public static Derivat getDerivat() {
        Derivat derivat = new Derivat();
        derivat.setName("DerivatName");
        derivat.setStatus(DerivatStatus.OFFEN);
        derivat.setProduktlinie("Li");
        derivat.setZakUpdate(new Date());
        derivat.setId(1L);
        return derivat;
    }
}
