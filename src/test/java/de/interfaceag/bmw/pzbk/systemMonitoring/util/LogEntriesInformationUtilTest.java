package de.interfaceag.bmw.pzbk.systemMonitoring.util;

import de.interfaceag.bmw.pzbk.systemMonitoring.model.LogEntryDto;
import de.interfaceag.bmw.pzbk.systemMonitoring.model.LogSummaryInformationDto;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

class LogEntriesInformationUtilTest {

    @Test
    void test_getInformationForLogEntries() {
        LogSummaryInformationDto informationForLogEntries = LogEntriesInformationUtil.getInformationForLogEntries(this.getLogEntries());
        Assert.assertNotNull(informationForLogEntries.getLastZakPost());
        Assert.assertNotNull(informationForLogEntries.getLastZakGet());
    }

    @Test
    void test_filterByString() {
        List<LogEntryDto> filteredLogs = LogEntriesInformationUtil.filterByString(this.getLogEntries(), "Derivat I3");
        Assert.assertNotNull(filteredLogs);
        Assert.assertEquals(4, filteredLogs.size());
    }

    private List<LogEntryDto> getLogEntries() {
        return Arrays.asList(
                LogEntryDto.newBuilder()
                        .withId(1L)
                        .withLogValue("POST abgeschlossen. 867 von 867 Übertragungen waren nicht erfolgreich.")
                        .withDate(new Date())
                        .build(),
                LogEntryDto.newBuilder()
                        .withId(2L)
                        .withLogValue("Es gibt keine neuen Updates für Derivat I3, Produktlinie: LG.")
                        .withDate(new Date())
                        .build(),
                LogEntryDto.newBuilder()
                        .withId(3L)
                        .withLogValue("Es wurden keine ZakUebertragungen für Derivat I3 geupdated.")
                        .withDate(new Date())
                        .build(),
                LogEntryDto.newBuilder()
                        .withId(4L)
                        .withLogValue("Das XML für Derivat I3 enthält keine Ergebnisse.")
                        .withDate(new Date())
                        .build(),
                LogEntryDto.newBuilder()
                        .withId(5L)
                        .withLogValue("Starte ZAK Update für Derivat I3")
                        .withDate(new Date())
                        .build()
        );
    }
}