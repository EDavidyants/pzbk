package de.interfaceag.bmw.pzbk.systemMonitoring.model;

import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class SystemMonitoringMapperTest {

    @Test
    public void test_buildSystemMonitoringDto_for_null() {
        SystemMonitoringDto systemMonitoringDto = SystemMonitoringMapper.buildSystemMonitoringDto(null);
        Assert.assertNotNull(systemMonitoringDto);
    }

    @Test
    public void test_buildSystemMonitoringDto() {
        SystemMonitoringDto systemMonitoringDto = SystemMonitoringMapper.buildSystemMonitoringDto(SystemMonitoringTestData.getZakuebertragung());

        Assert.assertNotNull(systemMonitoringDto.getAenderungsdatum());
        Assert.assertEquals(systemMonitoringDto.getZakstatus(), ZakStatus.DONE);
        Assert.assertEquals(systemMonitoringDto.getUebertragungId().longValue(), 1L);
        Assert.assertEquals(systemMonitoringDto.getZakId(), "9d19d259-9977-49e0-870a-ebcceea90674");
        Assert.assertEquals(systemMonitoringDto.getZakResponse(), "response");
        Assert.assertEquals(systemMonitoringDto.getDerivatProduktlinie(), "Li");
    }
}
