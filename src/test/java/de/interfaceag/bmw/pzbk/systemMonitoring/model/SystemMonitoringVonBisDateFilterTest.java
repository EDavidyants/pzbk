package de.interfaceag.bmw.pzbk.systemMonitoring.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class SystemMonitoringVonBisDateFilterTest {

    @Test
    void test_prefix() {
        Assert.assertEquals("zak-uebertragungen", SystemMonitoringVonBisDateFilter.PARAMETERPRAEFIX);
    }

}