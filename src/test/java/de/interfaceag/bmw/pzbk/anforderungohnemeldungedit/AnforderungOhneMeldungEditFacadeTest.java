package de.interfaceag.bmw.pzbk.anforderungohnemeldungedit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.AnforderungEditFahrzeugmerkmalDialogService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.AnforderungEditFahrzeugmerkmalSaveService;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ImageService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.session.UserSession;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author fn
 */
public class AnforderungOhneMeldungEditFacadeTest {

    private static final Logger LOG = LoggerFactory.getLogger(AnforderungOhneMeldungEditFacadeTest.class.getName());

    private AnforderungOhneMeldungEditFacade viewFacade;

    private HttpServletRequest request;

    @Before
    public void setUp() {

        viewFacade = mock(AnforderungOhneMeldungEditFacade.class);
        viewFacade.anforderungService = mock(AnforderungService.class);
        viewFacade.imageService = mock(ImageService.class);
        viewFacade.session = mock(UserSession.class);
        viewFacade.berechtigungService = mock(BerechtigungService.class);
        viewFacade.tteamService = mock(TteamService.class);
        viewFacade.sensorCocService = mock(SensorCocService.class);
        viewFacade.modulService = mock(ModulService.class);
        viewFacade.anforderungEditFahrzeugmerkmalService = mock(AnforderungEditFahrzeugmerkmalDialogService.class);
        viewFacade.anforderungModulSeTeamFahrzeugmerkmalAuspraegungService = mock(AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService.class);
        viewFacade.anforderungEditFahrzeugmerkmalSaveService = mock(AnforderungEditFahrzeugmerkmalSaveService.class);

        request = mock(HttpServletRequest.class);
        when(request.getParameterMap()).thenReturn(new HashMap<>());
    }

    @Test
    public void getViewDataForCorrectFachIdAndVersion() {
        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "A42");
        urlParameter.addOrReplaceParamter("version", "1");

        when(viewFacade.anforderungService.getAnforderungByFachIdVersion("A42", 1)).thenReturn(TestDataFactory.generateAnforderung());
        when(viewFacade.session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("test"));
        when(viewFacade.anforderungService.isUserBerechtigtForAnforderung(any())).thenReturn(Boolean.TRUE);
        when(viewFacade.anforderungService.getAnforderungWorkCopyByAnfoderung(any())).thenReturn(TestDataFactory.generateAnforderung());
        Set<Rolle> rolesWithWritePermissions = new HashSet<>();
        rolesWithWritePermissions.add(Rolle.ADMIN);
        when(viewFacade.getUserRolesWithWritePermissions(any())).thenReturn(rolesWithWritePermissions);
        when(viewFacade.getViewData(any())).thenCallRealMethod();

        AnforderungOhneMeldungEditViewObjekt viewObjekt = viewFacade.getViewData(urlParameter);
        AnforderungOhneMeldungEditViewData viewData = viewObjekt.getViewData().get();
        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewObjekt),
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertTrue(viewData.getFachId().equals("A42")),
                () -> Assertions.assertTrue(viewData.getVersion().equals(1)));

    }

    @Test
    public void getViewDataForIncorrectFachId() {

        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "12");
        urlParameter.addOrReplaceParamter("version", "1");

        when(viewFacade.getViewData(any())).thenCallRealMethod();

        AnforderungOhneMeldungEditViewObjekt viewObjekt = viewFacade.getViewData(urlParameter);
        Assertions.assertFalse(viewObjekt.getViewData().isPresent());

    }

    @Test
    public void getViewDataForIncorrectVersion() {

        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "42");
        urlParameter.addOrReplaceParamter("version", "0");
        when(viewFacade.getViewData(any())).thenCallRealMethod();
        AnforderungOhneMeldungEditViewObjekt viewObjekt = viewFacade.getViewData(urlParameter);
        Assertions.assertFalse(viewObjekt.getViewData().isPresent());

    }

    @Test
    public void getViewDataForIDWithIncorrectFachID() {
        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "42");
        urlParameter.addOrReplaceParamter("version", "1");
        urlParameter.addOrReplaceParamter("id", "42");

        when(viewFacade.anforderungService.getAnforderungById(any())).thenReturn(TestDataFactory.generateAnforderung());
        when(viewFacade.session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("test"));
        when(viewFacade.anforderungService.isUserBerechtigtForAnforderung(any())).thenReturn(Boolean.TRUE);
        when(viewFacade.anforderungService.getAnforderungWorkCopyByAnfoderung(any())).thenReturn(TestDataFactory.generateAnforderung());
        Set<Rolle> rolesWithWritePermissions = new HashSet<>();
        rolesWithWritePermissions.add(Rolle.ADMIN);
        when(viewFacade.getUserRolesWithWritePermissions(any())).thenReturn(rolesWithWritePermissions);
        when(viewFacade.getViewData(any())).thenCallRealMethod();

        AnforderungOhneMeldungEditViewObjekt viewObjekt = viewFacade.getViewData(urlParameter);
        AnforderungOhneMeldungEditViewData viewData = viewObjekt.getViewData().get();
        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewObjekt),
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertTrue(viewData.getFachId().equals("A42")),
                () -> Assertions.assertTrue(viewData.getVersion().equals(1)));

    }

    @Test
    public void getViewDataForIDWithIncorrectVersion() {
        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "A42");
        urlParameter.addOrReplaceParamter("version", "A1");
        urlParameter.addOrReplaceParamter("id", "42");

        when(viewFacade.anforderungService.getAnforderungById(any())).thenReturn(TestDataFactory.generateAnforderung());
        when(viewFacade.session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("test"));
        when(viewFacade.anforderungService.isUserBerechtigtForAnforderung(any())).thenReturn(Boolean.TRUE);
        when(viewFacade.anforderungService.getAnforderungWorkCopyByAnfoderung(any())).thenReturn(TestDataFactory.generateAnforderung());
        Set<Rolle> rolesWithWritePermissions = new HashSet<>();
        rolesWithWritePermissions.add(Rolle.ADMIN);
        when(viewFacade.getUserRolesWithWritePermissions(any())).thenReturn(rolesWithWritePermissions);
        when(viewFacade.getViewData(any())).thenCallRealMethod();

        AnforderungOhneMeldungEditViewObjekt viewObjekt = viewFacade.getViewData(urlParameter);
        AnforderungOhneMeldungEditViewData viewData = viewObjekt.getViewData().get();
        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewObjekt),
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertTrue(viewData.getFachId().equals("A42")),
                () -> Assertions.assertTrue(viewData.getVersion().equals(1)));

    }

    @Test
    public void getViewDataNewAnforderungForCorrectFachIdAndVersion() {
        try {
            UrlParameter urlParameter = new UrlParameter(request);
            urlParameter.addOrReplaceParamter("fachId", "A42");
            urlParameter.addOrReplaceParamter("version", "1");
            urlParameter.addOrReplaceParamter("created", "new");

            when(viewFacade.anforderungService.createNewVersionForAnforderung("A42", 1)).thenReturn(TestDataFactory.generateNewVersionAnforderung());

            when(viewFacade.session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("test"));
            when(viewFacade.anforderungService.isUserBerechtigtForAnforderung(any())).thenReturn(Boolean.TRUE);
            when(viewFacade.anforderungService.getAnforderungWorkCopyByAnfoderung(any())).thenReturn(TestDataFactory.generateNewVersionAnforderung());
            Set<Rolle> rolesWithWritePermissions = new HashSet<>();
            rolesWithWritePermissions.add(Rolle.ADMIN);
            when(viewFacade.getUserRolesWithWritePermissions(any())).thenReturn(rolesWithWritePermissions);
            when(viewFacade.getViewData(any())).thenCallRealMethod();

            AnforderungOhneMeldungEditViewObjekt viewObjekt = viewFacade.getViewData(urlParameter);
            AnforderungOhneMeldungEditViewData viewData = viewObjekt.getViewData().get();
            Assertions.assertAll("ViewObjektNotNull",
                    () -> Assertions.assertNotNull(viewObjekt),
                    () -> Assertions.assertNotNull(viewData),
                    () -> Assertions.assertTrue(viewData.getFachId().equals("A42")),
                    () -> Assertions.assertTrue(viewData.getVersion().equals(2)));
        } catch (IllegalAccessException | InvocationTargetException ex) {
            LOG.error(null, ex);
        }

    }

    @Test
    public void getViewDataNewAnforderungForInCorrectFachIdAndVersionWithId() {
        try {
            UrlParameter urlParameter = new UrlParameter(request);
            urlParameter.addOrReplaceParamter("fachId", "42");
            urlParameter.addOrReplaceParamter("version", "A1");
            urlParameter.addOrReplaceParamter("created", "new");
            urlParameter.addOrReplaceParamter("id", "42");

            when(viewFacade.anforderungService.createNewVersionForAnforderung(any())).thenReturn(TestDataFactory.generateNewVersionAnforderung());
            when(viewFacade.session.getUser()).thenReturn(TestDataFactory.createMitarbeiter("test"));
            when(viewFacade.anforderungService.isUserBerechtigtForAnforderung(any())).thenReturn(Boolean.TRUE);
            when(viewFacade.anforderungService.getAnforderungWorkCopyByAnfoderung(any())).thenReturn(TestDataFactory.generateNewVersionAnforderung());
            Set<Rolle> rolesWithWritePermissions = new HashSet<>();
            rolesWithWritePermissions.add(Rolle.ADMIN);
            when(viewFacade.getUserRolesWithWritePermissions(any())).thenReturn(rolesWithWritePermissions);
            when(viewFacade.getViewData(any())).thenCallRealMethod();

            AnforderungOhneMeldungEditViewObjekt viewObjekt = viewFacade.getViewData(urlParameter);
            AnforderungOhneMeldungEditViewData viewData = viewObjekt.getViewData().get();
            Assertions.assertAll("ViewObjektNotNull",
                    () -> Assertions.assertNotNull(viewObjekt),
                    () -> Assertions.assertNotNull(viewData),
                    () -> Assertions.assertTrue(viewData.getFachId().equals("A42")),
                    () -> Assertions.assertTrue(viewData.getVersion().equals(2)));
        } catch (IllegalAccessException | InvocationTargetException ex) {
            LOG.error(null, ex);
        }

    }

}
