package de.interfaceag.bmw.pzbk.anforderungohnemeldungedit;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.Collections;

/**
 * @author fn
 */
public class AnforderungOhneMeldungEditViewDataTest {

    @Test
    public void getViewDataFromCorrectAnforderung() {

        Anforderung anforderung = TestDataFactory.generateAnforderung();
        AnforderungOhneMeldungEditViewData viewData = AnforderungOhneMeldungEditViewData.forAnforderung(anforderung, Collections.emptyList());

        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertTrue(viewData.getFachId().equals("A42")),
                () -> Assertions.assertTrue(viewData.getVersion().equals(1)));
    }

    @Test
    public void getViewDataFromIncorrectAnforderung() {

        Anforderung anforderung = null;

        AnforderungOhneMeldungEditViewData viewData = AnforderungOhneMeldungEditViewData.forAnforderung(anforderung, Collections.emptyList());

        Assertions.assertNotNull(viewData);

    }

    @Test
    public void getViewDataForNewAnforderung() {

        Anforderung anforderung = TestDataFactory.generateNewVersionAnforderung();
        Mitarbeiter mitarbeiter = TestDataFactory.createMitarbeiter("Mitarbeiter");

        AnforderungOhneMeldungEditViewData viewData = AnforderungOhneMeldungEditViewData.forNewAnforderung(anforderung, mitarbeiter);

        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertTrue(viewData.getFachId().equals("")),
                () -> Assertions.assertTrue(viewData.getVersion().equals(1)),
                () -> Assertions.assertTrue(viewData.getErsteller().equals(mitarbeiter.getName())));

    }

    @Test
    public void getViewDataForIncorrectNewAnforderung() {

        Anforderung anforderung = null;
        Mitarbeiter mitarbeiter = TestDataFactory.createMitarbeiter("Mitarbeiter");

        AnforderungOhneMeldungEditViewData viewData = AnforderungOhneMeldungEditViewData.forNewAnforderung(anforderung, mitarbeiter);

        Assertions.assertAll("ViewObjektNotNull",
                () -> Assertions.assertNotNull(viewData),
                () -> Assertions.assertTrue(viewData.getFachId().equals("")),
                () -> Assertions.assertTrue(viewData.getVersion().equals(1)),
                () -> Assertions.assertTrue(viewData.getErsteller().equals("")));

    }
}
