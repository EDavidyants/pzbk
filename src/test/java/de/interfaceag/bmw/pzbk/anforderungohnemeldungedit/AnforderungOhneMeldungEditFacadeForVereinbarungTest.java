package de.interfaceag.bmw.pzbk.anforderungohnemeldungedit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.AnforderungEditFahrzeugmerkmalDialogService;
import de.interfaceag.bmw.pzbk.anforderungsworkflow.edit.AnforderungEditFahrzeugmerkmalSaveService;
import de.interfaceag.bmw.pzbk.berechtigung.dto.BerechtigungDto;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.VereinbarungType;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.fahrzeugmerkmal.anforderungszuordnung.AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService;
import de.interfaceag.bmw.pzbk.permissions.UserPermissions;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.BerechtigungService;
import de.interfaceag.bmw.pzbk.services.ImageService;
import de.interfaceag.bmw.pzbk.services.ModulService;
import de.interfaceag.bmw.pzbk.services.SensorCocService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.session.Session;
import de.interfaceag.bmw.pzbk.shared.dto.UrlParameter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.when;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class AnforderungOhneMeldungEditFacadeForVereinbarungTest {

    @Mock
    private HttpServletRequest request;

    @Mock
    private ProzessbaukastenReadService prozessbaukastenReadService;

    @Mock
    private AnforderungService anforderungService;

    @Mock
    private ImageService imageService;

    @Mock
    private Session session;

    @Mock
    private BerechtigungService berechtigungService;

    @Mock
    private TteamService tteamService;

    @Mock
    private SensorCocService sensorCocService;

    @Mock
    private ModulService modulService;

    @Mock
    private AnforderungEditFahrzeugmerkmalDialogService anforderungEditFahrzeugmerkmalDialogService;

    @Mock
    private AnforderungEditFahrzeugmerkmalSaveService anforderungEditFahrzeugmerkmalSaveService;

    @Mock
    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegungService anforderungModulSeTeamFahrzeugmerkmalAuspraegungService;

    @Mock
    private Prozessbaukasten prozessbaukasten;

    @Mock
    private UserPermissions<BerechtigungDto> userPermissions;

    @InjectMocks
    private AnforderungOhneMeldungEditFacade viewFacade;

    private Set<Rolle> roles;
    private Anforderung anforderung;
    private Mitarbeiter user;

    @BeforeEach
    public void setUp() {
        user = TestDataFactory.generateMitarbeiter("Max", "Admin", "Abt-2");
        roles = new HashSet<>();
        roles.add(Rolle.ADMIN);

        when(request.getParameterMap()).thenReturn(new HashMap<>());
        when(session.getUserPermissions()).thenReturn(userPermissions);

    }

    @Test
    public void testVereinbarungTypeForNewAnforderungWithPzbkFachIdAndVersion() {

        anforderung = new Anforderung();

        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("pzbkFachId", "P13");
        urlParameter.addOrReplaceParamter("pzbkVersion", "1");
        urlParameter.addOrReplaceParamter("new", "true");
        when(session.getUser()).thenReturn(user);
        when(userPermissions.getRolesWithWritePermissionsForNewAnforderung()).thenReturn(roles);

        Optional<Prozessbaukasten> prozessbaukastenOptional = Optional.ofNullable(prozessbaukasten);
        when(viewFacade.prozessbaukastenReadService.getByFachIdAndVersion("P13", 1)).thenReturn(prozessbaukastenOptional);
        when(anforderungService.createNewAnforderung()).thenReturn(anforderung);
        when(anforderungService.getAnforderungWorkCopyByAnfoderung(anforderung)).thenReturn(anforderung);

        AnforderungOhneMeldungEditViewObjekt viewObjekt = viewFacade.getViewData(urlParameter);
        AnforderungOhneMeldungEditViewData viewData = viewObjekt.getViewData().get();
        VereinbarungType defaultVereinbarungType = viewData.getDefaultVereinbarungType();
        Assertions.assertEquals(VereinbarungType.STANDARD, defaultVereinbarungType);

    }

    @Test
    public void testVereinbarungTypeInUmsetzerDialogViewDataForNewAnforderungWithPzbkFachIdAndVersion() {

        anforderung = new Anforderung();

        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("pzbkFachId", "P13");
        urlParameter.addOrReplaceParamter("pzbkVersion", "1");
        urlParameter.addOrReplaceParamter("new", "true");
        when(session.getUser()).thenReturn(user);
        when(userPermissions.getRolesWithWritePermissionsForNewAnforderung()).thenReturn(roles);

        Optional<Prozessbaukasten> prozessbaukastenOptional = Optional.ofNullable(prozessbaukasten);
        when(viewFacade.prozessbaukastenReadService.getByFachIdAndVersion("P13", 1)).thenReturn(prozessbaukastenOptional);
        when(anforderungService.createNewAnforderung()).thenReturn(anforderung);
        when(anforderungService.getAnforderungWorkCopyByAnfoderung(anforderung)).thenReturn(anforderung);

        AnforderungOhneMeldungEditViewObjekt viewObjekt = viewFacade.getViewData(urlParameter);
        UmsetzerDialogViewData viewData = viewObjekt.getUmsetzerDialogViewData();
        VereinbarungType defaultVereinbarungType = viewData.getDefaultVereinbarungType();
        Assertions.assertEquals(VereinbarungType.STANDARD, defaultVereinbarungType);

    }

    @ParameterizedTest
    @ValueSource(ints = {4, 5, 6, 8, 9, 13, 14})
    public void testVereinbarungTypeForAnforderungInStatus(int statusId) {

        Status status = Status.getStatusById(statusId);

        anforderung = TestDataFactory.generateAnforderung();
        anforderung.setStatus(status);

        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "A11");
        urlParameter.addOrReplaceParamter("version", "1");

        when(anforderungService.getAnforderungByFachIdVersion("A11", 1)).thenReturn(anforderung);
        when(anforderungService.getAnforderungWorkCopyByAnfoderung(anforderung)).thenReturn(anforderung);
        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getConfiguredModulSeTeamsForAnforderung(anforderung)).thenReturn(Collections.emptyList());

        AnforderungOhneMeldungEditViewObjekt viewObjekt = viewFacade.getViewData(urlParameter);
        AnforderungOhneMeldungEditViewData viewData = viewObjekt.getViewData().get();
        VereinbarungType defaultVereinbarungType = viewData.getDefaultVereinbarungType();

        switch (status) {
            case A_INARBEIT:
            case A_UNSTIMMIG:
            case A_FTABGESTIMMT:
            case A_PLAUSIB:
            case A_FREIGEGEBEN:
                Assertions.assertEquals(VereinbarungType.ZAK, defaultVereinbarungType);
                break;

            case A_ANGELEGT_IN_PROZESSBAUKASTEN:
            case A_GENEHMIGT_IN_PROZESSBAUKASTEN:
                Assertions.assertEquals(VereinbarungType.STANDARD, defaultVereinbarungType);
                break;

            default:
                break;
        }

    }

    @ParameterizedTest
    @ValueSource(ints = {4, 5, 6, 8, 9, 13, 14})
    public void testVereinbarungTypeInUmsetzungDialogViewDataForAnforderungInStatus(int statusId) {

        Status status = Status.getStatusById(statusId);

        anforderung = TestDataFactory.generateAnforderung();
        anforderung.setStatus(status);

        UrlParameter urlParameter = new UrlParameter(request);
        urlParameter.addOrReplaceParamter("fachId", "A11");
        urlParameter.addOrReplaceParamter("version", "1");

        when(anforderungService.getAnforderungByFachIdVersion("A11", 1)).thenReturn(anforderung);
        when(anforderungService.getAnforderungWorkCopyByAnfoderung(anforderung)).thenReturn(anforderung);
        when(anforderungModulSeTeamFahrzeugmerkmalAuspraegungService.getConfiguredModulSeTeamsForAnforderung(anforderung)).thenReturn(Collections.emptyList());

        AnforderungOhneMeldungEditViewObjekt viewObjekt = viewFacade.getViewData(urlParameter);
        UmsetzerDialogViewData viewData = viewObjekt.getUmsetzerDialogViewData();
        VereinbarungType defaultVereinbarungType = viewData.getDefaultVereinbarungType();

        switch (status) {
            case A_INARBEIT:
            case A_UNSTIMMIG:
            case A_FTABGESTIMMT:
            case A_PLAUSIB:
            case A_FREIGEGEBEN:
                Assertions.assertEquals(VereinbarungType.ZAK, defaultVereinbarungType);
                break;

            case A_ANGELEGT_IN_PROZESSBAUKASTEN:
            case A_GENEHMIGT_IN_PROZESSBAUKASTEN:
                Assertions.assertEquals(VereinbarungType.STANDARD, defaultVereinbarungType);
                break;

            default:
                break;
        }

    }

}
