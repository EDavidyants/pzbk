package de.interfaceag.bmw.pzbk.anforderungohnemeldungedit;

import de.interfaceag.bmw.pzbk.anforderungsworkflow.AnforderungTitelbildValidator;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AnforderungOhneMeldungTitelbildValidatorTest {

    @Mock
    private AnforderungOhneMeldungEditViewData viewData;
    @Mock
    private AnforderungOhneMeldungEditWorkFields workFields;

    @Mock
    private Prozessbaukasten prozessbaukasten;

    @Test
    void validateWithProzessbaukasten() {
        when(workFields.getProzessbaukasten()).thenReturn(Optional.of(prozessbaukasten));

        final boolean result = AnforderungOhneMeldungTitelbildValidator.validate(viewData, workFields);
        assertTrue(result);
    }

    @Test
    void validateWithoutProzessbaukasten() {
        when(workFields.getProzessbaukasten()).thenReturn(Optional.empty());

        final boolean expectedResult = AnforderungTitelbildValidator.isValid(viewData, viewData.getAnhangForBild());
        final boolean result = AnforderungOhneMeldungTitelbildValidator.validate(viewData, workFields);
        assertThat(result, is(expectedResult));
    }
}