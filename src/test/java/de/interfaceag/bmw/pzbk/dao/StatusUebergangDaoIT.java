package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.UBzuKovaPhaseImDerivatUndSensorCoC;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.DerivatAnforderungModulStatus;
import de.interfaceag.bmw.pzbk.enums.KovAPhase;
import de.interfaceag.bmw.pzbk.enums.KovAStatus;
import de.interfaceag.bmw.pzbk.zuordnung.ZuordnungStatus;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

/**
 * @author evda
 */
class StatusUebergangDaoIT extends IntegrationTest {

    private StatusUebergangDao statusUebergangDao = new StatusUebergangDao();
    private SensorCocDao sensorCocDao = new SensorCocDao();
    private AnforderungDao anforderungDao = new AnforderungDao();
    private ModulDao modulDao = new ModulDao();
    private DerivatDao derivatDao = new DerivatDao();
    private ZuordnungAnforderungDerivatDao zuordnungAnforderungDerivatDao = new ZuordnungAnforderungDerivatDao();
    private DerivatAnforderungModulDao derivatAnforderungModulDao = new DerivatAnforderungModulDao();
    private KovAPhaseImDerivatDao kovAPhaseImDerivatDao = new KovAPhaseImDerivatDao();
    private MitarbeiterDao mitarbeiterDao = new MitarbeiterDao();
    private UBzuKovaPhaseImDerivatUndSensorCoCDao uBzuKovaPhaseImDerivatUndSensorCoCDao = new UBzuKovaPhaseImDerivatUndSensorCoCDao();

    private SensorCoc sensorCoc1;
    private SensorCoc sensorCoc2;
    private SensorCoc sensorCoc3;
    private SensorCoc sensorCoc4;
    private SensorCoc sensorCoc5;

    private Anforderung anforderung1;
    private Anforderung anforderung2;
    private Anforderung anforderung3;

    private ZuordnungAnforderungDerivat zuordnung1;
    private ZuordnungAnforderungDerivat zuordnung2;
    private ZuordnungAnforderungDerivat zuordnung3;
    private ZuordnungAnforderungDerivat zuordnung4;

    private DerivatAnforderungModul derivatAnforderungModul1;
    private DerivatAnforderungModul derivatAnforderungModul2;
    private DerivatAnforderungModul derivatAnforderungModul3;
    private DerivatAnforderungModul derivatAnforderungModul4;
    private DerivatAnforderungModul derivatAnforderungModul5;
    private DerivatAnforderungModul derivatAnforderungModul6;
    private DerivatAnforderungModul derivatAnforderungModul7;
    private DerivatAnforderungModul derivatAnforderungModul8;

    private KovAPhaseImDerivat kovaPhase1;
    private KovAPhaseImDerivat kovaPhase2;

    private Mitarbeiter umsetzungsbestaetiger;

    private UBzuKovaPhaseImDerivatUndSensorCoC ubZuKovaPhaseImDerivat;

    @BeforeEach
    void setUp() throws IllegalAccessException {
        // start transaction before test case

        super.begin();

        // override field entityManager in fahrzeugmerkmalDao with private access
        FieldUtils.writeField(statusUebergangDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(sensorCocDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(anforderungDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(modulDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(derivatDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(zuordnungAnforderungDerivatDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(derivatAnforderungModulDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(kovAPhaseImDerivatDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(mitarbeiterDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(uBzuKovaPhaseImDerivatUndSensorCoCDao, "entityManager", super.getEntityManager(), true);

        writeDataIntoEntitiesForTests();

        super.commit();

        super.begin();
    }

    @Test
    public void testForNullSensorCocInput() {
        int result = statusUebergangDao.processStepEight(null);
        MatcherAssert.assertThat(result, is(0));
    }

    @Test
    public void testForValidSensorCocInput() {
        final List<Long> sensorCocIds = Arrays.asList(sensorCoc1.getSensorCocId(), sensorCoc2.getSensorCocId(), sensorCoc3.getSensorCocId(), sensorCoc4.getSensorCocId(), sensorCoc5.getSensorCocId());
        int result = statusUebergangDao.processStepEight(sensorCocIds);
        MatcherAssert.assertThat(result, is(0));
    }

    @Test
    public void testSensorCocs() {
        List<SensorCoc> sensorCocs = sensorCocDao.getAllSensorCoCsSortByTechnologie();
        MatcherAssert.assertThat(sensorCocs, hasSize(5));
    }

    @Test
    public void testSensorCocsByIdList() {
        final List<Long> ids = Arrays.asList(sensorCoc1.getSensorCocId(), sensorCoc2.getSensorCocId(), sensorCoc3.getSensorCocId(), sensorCoc4.getSensorCocId(), sensorCoc5.getSensorCocId());
        List<SensorCoc> sensorCocs = sensorCocDao.getSensorCocsByIdList(ids);
        MatcherAssert.assertThat(sensorCocs, hasSize(5));
    }

    @Test
    public void testAnforderungen() {
        List<Anforderung> anforderungen = anforderungDao.getAllAnforderung();
        MatcherAssert.assertThat(anforderungen, hasSize(3));
    }

    @Test
    public void testModule() {
        List<Modul> module = modulDao.getAllModule();
        MatcherAssert.assertThat(module, hasSize(2));
    }

    @Test
    public void testDerivate() {
        List<Derivat> derivate = derivatDao.findAllExistingDerivate();
        MatcherAssert.assertThat(derivate, hasSize(2));
    }

    @Test
    public void testDerivatAnforderungModule() {
        List<DerivatAnforderungModul> derivatAnforderungModule = derivatAnforderungModulDao.getAllDerivatAnforderungModulen();
        MatcherAssert.assertThat(derivatAnforderungModule, hasSize(8));
    }

    @Test
    public void testAngenommeneDerivatAnforderungModuleForAnforderungId() {
        List<DerivatAnforderungModul> derivatAnforderungModule = getDerivatAnforderungModuleInStatusForAnforderungId(anforderung2.getId());
        MatcherAssert.assertThat(derivatAnforderungModule, hasSize(1));
    }

    private List<DerivatAnforderungModul> getDerivatAnforderungModuleInStatusForAnforderungId(long anforderungId) {
        return derivatAnforderungModulDao.getDerivatAnforderungModulByAnforderungStatus(anforderungId, DerivatAnforderungModulStatus.ANGENOMMEN);
    }

    @Test
    public void testMitarbeiter() {
        Optional<Mitarbeiter> mitarbeiterToFind = mitarbeiterDao.getMitarbeiterByQnumber("q48526");
        Assertions.assertTrue(mitarbeiterToFind.isPresent());
        Mitarbeiter mitarbeiter = mitarbeiterToFind.get();
        MatcherAssert.assertThat(mitarbeiter.getName(), is("Zack Logan"));
    }

    private void writeDataIntoEntitiesForTests() {
        // SensorCoc
        sensorCoc1 = new SensorCoc();
        sensorCoc1.setRessort("T-Ressort");
        sensorCoc1.setOrtung("Technologie Gesamtfahrzeug");
        sensorCoc1.setTechnologie("TGF_Antrieb");

        sensorCoc2 = new SensorCoc();
        sensorCoc2.setRessort("T-Ressort");
        sensorCoc2.setOrtung("Technologie Montage");
        sensorCoc2.setTechnologie("TMO_Verdecksysteme");

        sensorCoc3 = new SensorCoc();
        sensorCoc3.setRessort("T-Ressort");
        sensorCoc3.setOrtung("Technologie Antriebs- und Fahrwerkssyteme");
        sensorCoc3.setTechnologie("TA_V-Motorenmontage");

        sensorCoc4 = new SensorCoc();
        sensorCoc4.setRessort("T-Ressort");
        sensorCoc4.setOrtung("Technologie Gesamtfahrzeug");
        sensorCoc4.setTechnologie("TGF_Hybrid");

        sensorCoc5 = new SensorCoc();
        sensorCoc5.setRessort("T-Ressort");
        sensorCoc5.setOrtung("Technologie Lackierte Karosserie");
        sensorCoc5.setTechnologie("LaKa_TU_Bodengruppe");

        List<SensorCoc> sensorCocs = Arrays.asList(sensorCoc1, sensorCoc2, sensorCoc3, sensorCoc4, sensorCoc5);
        saveSensorCocs(sensorCocs);

        // Anforderung
        anforderung1 = new Anforderung("A1", 1);
        anforderung1.setSensorCoc(sensorCoc1);

        anforderung2 = new Anforderung("A2", 1);
        anforderung2.setSensorCoc(sensorCoc2);

        anforderung3 = new Anforderung("A3", 1);
        anforderung3.setSensorCoc(sensorCoc3);

        List<Anforderung> anforderungen = Arrays.asList(anforderung1, anforderung2, anforderung3);
        saveAnforderungen(anforderungen);

        // Modul
        Modul modul1 = new Modul("Modul 1");
        Modul modul2 = new Modul("Modul 2");
        modulDao.persistModule(Arrays.asList(modul1, modul2));

        // Derivat
        Derivat derivat1 = new Derivat("E20", "Derivat E20", "LI");
        Derivat derivat2 = new Derivat("F12", "Derivat F12", "LU");
        List<Derivat> derivate = Arrays.asList(derivat1, derivat2);
        saveDerivate(derivate);

        // ZuordnungAnforderungDerivat
        zuordnung1 = new ZuordnungAnforderungDerivat(anforderung1, derivat1, ZuordnungStatus.ZUGEORDNET);
        zuordnung2 = new ZuordnungAnforderungDerivat(anforderung1, derivat2, ZuordnungStatus.ZUGEORDNET);
        zuordnung3 = new ZuordnungAnforderungDerivat(anforderung2, derivat1, ZuordnungStatus.ZUGEORDNET);
        zuordnung4 = new ZuordnungAnforderungDerivat(anforderung3, derivat2, ZuordnungStatus.ZUGEORDNET);
        List<ZuordnungAnforderungDerivat> zuordnungen = Arrays.asList(zuordnung1, zuordnung2, zuordnung3, zuordnung4);
        saveZuordnungenAnforderungDerivat(zuordnungen);

        // DerivatAnforderungModul
        derivatAnforderungModul1 = new DerivatAnforderungModul(zuordnung1, modul1);
        derivatAnforderungModul1.setStatusZV(DerivatAnforderungModulStatus.ANGENOMMEN);

        derivatAnforderungModul2 = new DerivatAnforderungModul(zuordnung1, modul2);
        derivatAnforderungModul2.setStatusZV(DerivatAnforderungModulStatus.ABZUSTIMMEN);

        derivatAnforderungModul3 = new DerivatAnforderungModul(zuordnung2, modul1);
        derivatAnforderungModul3.setStatusZV(DerivatAnforderungModulStatus.ANGENOMMEN);

        derivatAnforderungModul4 = new DerivatAnforderungModul(zuordnung2, modul2);
        derivatAnforderungModul4.setStatusZV(DerivatAnforderungModulStatus.ABZUSTIMMEN);

        derivatAnforderungModul5 = new DerivatAnforderungModul(zuordnung3, modul1);
        derivatAnforderungModul5.setStatusZV(DerivatAnforderungModulStatus.ANGENOMMEN);

        derivatAnforderungModul6 = new DerivatAnforderungModul(zuordnung3, modul2);
        derivatAnforderungModul6.setStatusZV(DerivatAnforderungModulStatus.IN_KLAERUNG);

        derivatAnforderungModul7 = new DerivatAnforderungModul(zuordnung4, modul1);
        derivatAnforderungModul7.setStatusZV(DerivatAnforderungModulStatus.ANGENOMMEN);

        derivatAnforderungModul8 = new DerivatAnforderungModul(zuordnung4, modul2);
        derivatAnforderungModul8.setStatusZV(DerivatAnforderungModulStatus.NICHT_BEWERTBAR);

        List<DerivatAnforderungModul> derivatAnforderungModule = Arrays.asList(derivatAnforderungModul1, derivatAnforderungModul2,
                derivatAnforderungModul3, derivatAnforderungModul4, derivatAnforderungModul5, derivatAnforderungModul6,
                derivatAnforderungModul7, derivatAnforderungModul8);

        saveDerivatAnforderungModule(derivatAnforderungModule);

        // KovAPhaseImDerivat
        kovaPhase1 = new KovAPhaseImDerivat(derivat1, KovAPhase.VA_VKBG);
        kovaPhase1.setKovaStatus(KovAStatus.KONFIGURIERT);

        kovaPhase2 = new KovAPhaseImDerivat(derivat2, KovAPhase.VA_VBBG);
        kovaPhase2.setKovaStatus(KovAStatus.AKTIV);

        List<KovAPhaseImDerivat> kovaPhasenImDerivat = Arrays.asList(kovaPhase1, kovaPhase2);
        saveKovAPhasenImDerivat(kovaPhasenImDerivat);

        // Mitarbeiter
        umsetzungsbestaetiger = new Mitarbeiter("Zack", "Logan");
        umsetzungsbestaetiger.setQNumber("q48526");
        saveMitarbeiter(umsetzungsbestaetiger);

        // UBzuKovaPhaseImDerivatUndSensorCoC
        ubZuKovaPhaseImDerivat = new UBzuKovaPhaseImDerivatUndSensorCoC(sensorCoc1, kovaPhase1, umsetzungsbestaetiger);
        uBzuKovaPhaseImDerivatUndSensorCoCDao.persistUBzuKovaPhaseImDerivatUndSensorCoC(ubZuKovaPhaseImDerivat);

    }

    private void saveSensorCocs(Collection<SensorCoc> sensorCocs) {
        sensorCocs.forEach(sensorCoc -> sensorCocDao.persistSensorCoc(sensorCoc));
    }

    private void saveAnforderungen(Collection<Anforderung> anforderungen) {
        anforderungen.forEach(anforderung -> anforderungDao.persistAnforderung(anforderung));
    }

    private void saveDerivate(Collection<Derivat> derivate) {
        derivate.forEach(derivat -> derivatDao.persistDerivat(derivat));
    }

    private void saveZuordnungenAnforderungDerivat(Collection<ZuordnungAnforderungDerivat> zuordnungen) {
        zuordnungen.forEach(zuordnung -> zuordnungAnforderungDerivatDao.persistZuordnungAnforderungDerivat(zuordnung));
    }

    private void saveDerivatAnforderungModule(Collection<DerivatAnforderungModul> derivatAnforderungModule) {
        derivatAnforderungModule.forEach(derivatAnforderungModul -> derivatAnforderungModulDao.persistDerivatAnforderungModul(derivatAnforderungModul));
    }

    private void saveKovAPhasenImDerivat(Collection<KovAPhaseImDerivat> kovaPhasenImDerivat) {
        kovaPhasenImDerivat.forEach(kovaPhase -> kovAPhaseImDerivatDao.persistKovAPhaseImDerivat(kovaPhase));
    }

    private void saveMitarbeiter(Mitarbeiter mitarbeiter) {
        Optional<Mitarbeiter> found = mitarbeiterDao.getMitarbeiterByQnumber(mitarbeiter.getQNumber());
        if (!found.isPresent()) {
            mitarbeiterDao.persistMitarbeiter(mitarbeiter);
        }
    }

}
