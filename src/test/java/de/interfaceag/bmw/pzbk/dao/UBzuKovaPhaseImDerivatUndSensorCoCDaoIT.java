package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.KovAPhaseImDerivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.UBzuKovaPhaseImDerivatUndSensorCoC;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


class UBzuKovaPhaseImDerivatUndSensorCoCDaoIT extends IntegrationTest {

    UBzuKovaPhaseImDerivatUndSensorCoCDao uBzuKovaPhaseImDerivatUndSensorCoCDao = new UBzuKovaPhaseImDerivatUndSensorCoCDao();
    SensorCocDao sensorCocDao = new SensorCocDao();
    KovAPhaseImDerivatDao kovAPhaseImDerivatDao = new KovAPhaseImDerivatDao();
    MitarbeiterDao mitarbeiterDao = new MitarbeiterDao();
    DerivatDao derivatDao = new DerivatDao();

    SensorCoc sensorCoc;
    KovAPhaseImDerivat kovAPhaseImDerivat;
    Mitarbeiter mitarbeiter;
    Derivat derivat;

    @BeforeEach
    void setUp() throws IllegalAccessException {

        super.begin();

        FieldUtils.writeField(uBzuKovaPhaseImDerivatUndSensorCoCDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(sensorCocDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(kovAPhaseImDerivatDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(mitarbeiterDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(derivatDao, "entityManager", super.getEntityManager(), true);


        generateSensorCoc();
        generateDerivat();
        generateKovAPhaseImDerivat();
        generateMitarbeiter();

        super.commit();
        super.begin();
    }

    void generateSensorCoc() {
        sensorCoc = TestDataFactory.generateSensorCoc();
        sensorCocDao.persistSensorCoc(sensorCoc);
    }

    void generateDerivat() {
        derivat = TestDataFactory.generateDerivat();
        derivatDao.persistDerivat(derivat);
    }

    void generateKovAPhaseImDerivat() {
        kovAPhaseImDerivat = TestDataFactory.generateKovAPhaseImDerivat(derivat);
        kovAPhaseImDerivatDao.persistKovAPhaseImDerivat(kovAPhaseImDerivat);
    }

    void generateMitarbeiter() {
        mitarbeiter = TestDataFactory.generateMitarbeiter("Max", "Mustarmann", "Abt. 15");
        mitarbeiterDao.persistMitarbeiter(mitarbeiter);
    }


    @Test
    void persistUBzuKovaPhaseImDerivatUndSensorCoCTest() {

        UBzuKovaPhaseImDerivatUndSensorCoC uBzuKovaPhaseImDerivatUndSensorCoC = new UBzuKovaPhaseImDerivatUndSensorCoC();
        uBzuKovaPhaseImDerivatUndSensorCoC.setSensorCoc(sensorCoc);
        uBzuKovaPhaseImDerivatUndSensorCoC.setKovaImDerivat(kovAPhaseImDerivat);
        uBzuKovaPhaseImDerivatUndSensorCoC.setUmsetzungsbestaetiger(Collections.singletonList(mitarbeiter));


        uBzuKovaPhaseImDerivatUndSensorCoCDao.persistUBzuKovaPhaseImDerivatUndSensorCoC(uBzuKovaPhaseImDerivatUndSensorCoC);

        Optional<UBzuKovaPhaseImDerivatUndSensorCoC> result = uBzuKovaPhaseImDerivatUndSensorCoCDao.getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList(kovAPhaseImDerivat, sensorCoc);

        Assertions.assertEquals(uBzuKovaPhaseImDerivatUndSensorCoC, result.get());
    }

    @Test
    void persistUBzuKovaPhaseImDerivatUndSensorCoCsTest() {

        UBzuKovaPhaseImDerivatUndSensorCoC uBzuKovaPhaseImDerivatUndSensorCoC = new UBzuKovaPhaseImDerivatUndSensorCoC();
        uBzuKovaPhaseImDerivatUndSensorCoC.setSensorCoc(sensorCoc);
        uBzuKovaPhaseImDerivatUndSensorCoC.setKovaImDerivat(kovAPhaseImDerivat);
        uBzuKovaPhaseImDerivatUndSensorCoC.setUmsetzungsbestaetiger(Collections.singletonList(mitarbeiter));


        uBzuKovaPhaseImDerivatUndSensorCoCDao.persistUBzuKovaPhaseImDerivatUndSensorCoCs(Collections.singletonList(uBzuKovaPhaseImDerivatUndSensorCoC));

        Optional<UBzuKovaPhaseImDerivatUndSensorCoC> result = uBzuKovaPhaseImDerivatUndSensorCoCDao.getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList(kovAPhaseImDerivat, sensorCoc);

        Assertions.assertEquals(uBzuKovaPhaseImDerivatUndSensorCoC, result.get());
    }


    @Test
    void removeUBzuKovaPhaseImDerivatUndSensorCoCsTest() {

        UBzuKovaPhaseImDerivatUndSensorCoC uBzuKovaPhaseImDerivatUndSensorCoC = new UBzuKovaPhaseImDerivatUndSensorCoC();
        uBzuKovaPhaseImDerivatUndSensorCoC.setSensorCoc(sensorCoc);
        uBzuKovaPhaseImDerivatUndSensorCoC.setKovaImDerivat(kovAPhaseImDerivat);
        uBzuKovaPhaseImDerivatUndSensorCoC.setUmsetzungsbestaetiger(Collections.singletonList(mitarbeiter));


        uBzuKovaPhaseImDerivatUndSensorCoCDao.persistUBzuKovaPhaseImDerivatUndSensorCoCs(Collections.singletonList(uBzuKovaPhaseImDerivatUndSensorCoC));

        uBzuKovaPhaseImDerivatUndSensorCoCDao.removeUBzuKovaPhaseImDerivatUndSensorCoC(uBzuKovaPhaseImDerivatUndSensorCoC);

        Optional<UBzuKovaPhaseImDerivatUndSensorCoC> result = uBzuKovaPhaseImDerivatUndSensorCoCDao.getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList(kovAPhaseImDerivat, sensorCoc);

        Assertions.assertFalse(result.isPresent());
    }

    @Test
    void getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatListTest() {
        generateUByuKovaPhaseImDerivatUndSensorCoc();

        List<UBzuKovaPhaseImDerivatUndSensorCoC> result = uBzuKovaPhaseImDerivatUndSensorCoCDao.getUBzuKovaPhaseImDerivatUndSensorCoCByKovAPhaseImDerivatList(kovAPhaseImDerivat);
        Assertions.assertFalse(result.isEmpty());
        Assertions.assertEquals(1, result.size());

    }

    @Test
    void getSensorCocIdsAsUmsetzungsverwalterForKovaPhaseImDerivatTest() {
        generateUByuKovaPhaseImDerivatUndSensorCoc();

        Collection<Long> result = uBzuKovaPhaseImDerivatUndSensorCoCDao.getSensorCocIdsAsUmsetzungsverwalterForKovaPhaseImDerivat(kovAPhaseImDerivat, mitarbeiter);
        Assertions.assertFalse(result.isEmpty());
        Assertions.assertEquals(1, result.size());

    }

    void generateUByuKovaPhaseImDerivatUndSensorCoc() {
        UBzuKovaPhaseImDerivatUndSensorCoC uBzuKovaPhaseImDerivatUndSensorCoC = new UBzuKovaPhaseImDerivatUndSensorCoC();
        uBzuKovaPhaseImDerivatUndSensorCoC.setSensorCoc(sensorCoc);
        uBzuKovaPhaseImDerivatUndSensorCoC.setKovaImDerivat(kovAPhaseImDerivat);
        uBzuKovaPhaseImDerivatUndSensorCoC.setUmsetzungsbestaetiger(Collections.singletonList(mitarbeiter));


        uBzuKovaPhaseImDerivatUndSensorCoCDao.persistUBzuKovaPhaseImDerivatUndSensorCoCs(Collections.singletonList(uBzuKovaPhaseImDerivatUndSensorCoC));
    }
}