package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.entities.Wert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author sl
 */
public class WertDaoTest {

    private WertDao wertDao;

    @Before
    public void setup() {
        wertDao = new WertDao();
        wertDao.em = mock(EntityManager.class);
    }

    @Test
    public void getWertByNullAttribut() {
        mockWertQuery(Wert.QUERY_BY_ATTRIBUT, generateWertList());
        List<Wert> result = wertDao.getWertByAttribut(null);
        assertTrue(result.isEmpty());
    }

    @Test
    public void getWertStringByNullAttribut() {
        mockStringQuery(Wert.QUERY_ALL_WERTE_BY_ATTRIBUT, generateStringList());
        List<String> result = wertDao.getWertStringByAttribut(null);
        assertTrue(result.isEmpty());
    }

    @Test
    public void getWertByNullAttributAndWert() {
        mockWertQuery(Wert.QUERY_BY_ATTRIBUT_AND_WERT, generateWertList());
        List<Wert> result = wertDao.getWertByAttributAndWert(null, "DER Wert");
        assertTrue(result.isEmpty());
    }

    @Test
    public void getWertByNullAttributAndNullWert() {
        mockWertQuery(Wert.QUERY_BY_ATTRIBUT_AND_WERT, generateWertList());
        List<Wert> result = wertDao.getWertByAttributAndWert(null, null);
        assertTrue(result.isEmpty());
    }

    // ---------- utils --------------------------------------------------------
    private List<Wert> generateWertList() {
        List<Wert> result = new ArrayList<>();
        result.add(new Wert());
        return result;
    }

    private List<String> generateStringList() {
        List<String> result = new ArrayList<>();
        result.add("IF-Lab 2.0");
        return result;
    }

    private void mockWertQuery(String name, List<Wert> results) {
        TypedQuery mockedQuery = mock(TypedQuery.class);
        when(mockedQuery.getResultList()).thenReturn(results);
        when(wertDao.em.createNamedQuery(name, Wert.class)).thenReturn(mockedQuery);
        when(mockedQuery.setParameter(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(mockedQuery);
    }

    private void mockStringQuery(String name, List<String> results) {
        TypedQuery mockedQuery = mock(TypedQuery.class);
        when(mockedQuery.getResultList()).thenReturn(results);
        when(wertDao.em.createNamedQuery(name, String.class)).thenReturn(mockedQuery);
        when(mockedQuery.setParameter(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(mockedQuery);
    }

}
