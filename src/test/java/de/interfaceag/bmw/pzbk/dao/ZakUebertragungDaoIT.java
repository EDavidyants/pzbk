package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.DerivatAnforderungModul;
import de.interfaceag.bmw.pzbk.entities.FestgestelltIn;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.Werk;
import de.interfaceag.bmw.pzbk.entities.ZakUebertragung;
import de.interfaceag.bmw.pzbk.entities.ZuordnungAnforderungDerivat;
import de.interfaceag.bmw.pzbk.enums.ZakStatus;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;


/**
 * @author sl
 */
public class ZakUebertragungDaoIT extends IntegrationTest {

    private ZakUebertragungDao zakUebertragungDao = new ZakUebertragungDao();
    private AnforderungDao anforderungDao = new AnforderungDao();
    private ModulDao modulDao = new ModulDao();
    private ZuordnungAnforderungDerivatDao zuordnungAnforderungDerivatDao = new ZuordnungAnforderungDerivatDao();
    private DerivatAnforderungModulDao derivatAnforderungModulDao = new DerivatAnforderungModulDao();
    private DerivatDao derivatDao = new DerivatDao();
    private TteamDao tteamDao = new TteamDao();
    private MitarbeiterDao mitarbeiterDao = new MitarbeiterDao();
    private SensorCocDao sensorCocDao = new SensorCocDao();
    private FestgestelltInDao festgestelltInDao = new FestgestelltInDao();
    private WerkDao werkDao = new WerkDao();


    private Anforderung anforderung;
    private Modul modul;
    private Derivat derivat;
    private ZuordnungAnforderungDerivat zuordnungAnforderungDerivat;
    private DerivatAnforderungModul derivatAnforderungModul;
    private ZakUebertragung zakUebertragung;


    @BeforeEach
    public void setup() throws IllegalAccessException {

        super.begin();

        FieldUtils.writeField(zakUebertragungDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(anforderungDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(modulDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(zuordnungAnforderungDerivatDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(derivatAnforderungModulDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(derivatDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(tteamDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(mitarbeiterDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(sensorCocDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(festgestelltInDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(werkDao, "entityManager", super.getEntityManager(), true);

        saveForTests();

        super.commit();
        super.begin();
    }

    void saveForTests() {
        modul = TestDataFactory.generateModulWithoutId();
        modulDao.persistModule(Arrays.asList(modul));

        ModulSeTeam modulSeTeam = TestDataFactory.generateModulSeTeam(modul, TestDataFactory.generateModulKomponenten());
        modulDao.persistSeTeam(modulSeTeam);

        derivat = TestDataFactory.generateDerivatWithoutId("Name", "Bezeichnung", "Produktlinie");
        derivatDao.persistDerivat(derivat);

        Tteam tteam = TestDataFactory.generateTteamWithoutId();
        tteamDao.saveTteam(tteam);

        Mitarbeiter mitarbeiter = TestDataFactory.generateMitarbeiter("Sen", "Sor", "IF-Lab");
        mitarbeiterDao.persistMitarbeiter(mitarbeiter);

        SensorCoc sensorCoc = TestDataFactory.generateSensorCoc();
        sensorCocDao.persistSensorCoc(sensorCoc);

        Meldung meldung = new Meldung();
        anforderungDao.persistMeldung(meldung);

        FestgestelltIn festgestelltIn = TestDataFactory.generateFestgestelltInWithoutId();
        festgestelltInDao.persistFestgestelltIn(festgestelltIn);

        Werk werk = TestDataFactory.generateWerk();
        werkDao.persistWerk(werk);

        anforderung = TestDataFactory.generateAnforderungWithoutId("A42", tteam, mitarbeiter, sensorCoc, Arrays.asList(modulSeTeam), meldung, Collections.singleton(festgestelltIn), werk);
        anforderungDao.persistAnforderung(anforderung);


        zuordnungAnforderungDerivat = TestDataFactory.generateZuordnungAnforderungDerivatWithoutId(anforderung, derivat);
        zuordnungAnforderungDerivatDao.persistZuordnungAnforderungDerivat(zuordnungAnforderungDerivat);


        derivatAnforderungModul = TestDataFactory.generateDerivatAnforderungModulWithoutId(modul, zuordnungAnforderungDerivat, mitarbeiter);
        derivatAnforderungModulDao.persistDerivatAnforderungModul(derivatAnforderungModul);


        zakUebertragung = generateZakUebertragungList(derivatAnforderungModul);
        zakUebertragungDao.persistZakUebertragung(zakUebertragung);


    }

    @Test
    public void getZakUebertragungByNullId() {
        ZakUebertragung z = zakUebertragungDao.getZakUebertragungById(null);
        Assertions.assertNull(z);
    }

    @Test
    public void getZakUebertragungByNotNullIdEmptyResult() {
        ZakUebertragung z = zakUebertragungDao.getZakUebertragungById(zakUebertragung.getId());
        Assertions.assertEquals(zakUebertragung, z);
    }

    @Test
    public void getZakUebertragungByNotNullId() {
        ZakUebertragung z = zakUebertragungDao.getZakUebertragungById(1L);
        Assertions.assertNotNull(z);
    }

    @Test
    public void getZakUebertragungByNullDerivatAnforderungModul() {
        List<ZakUebertragung> result = zakUebertragungDao.getZakUebertragungByDerivatAnforderungModul(null);
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void getZakUebertragungByDerivatAnforderungModul() {
        List<ZakUebertragung> result = zakUebertragungDao.getZakUebertragungByDerivatAnforderungModul(new DerivatAnforderungModul());
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void getZakUebertragungByNullStatus() {
        List<ZakUebertragung> result = zakUebertragungDao.getZakUebertragungByStatus(null);
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void getZakUebertragungByStatus() {
        List<ZakUebertragung> result = zakUebertragungDao.getZakUebertragungByStatus(ZakStatus.EMPTY);
        assertThat(result, hasSize(2));
    }

    @Test
    public void getZakUebertragungByNullDerivatAnforderungModulList() {
        List<ZakUebertragung> result = zakUebertragungDao.getZakUebertragungByDerivatAnforderungModulList(null);
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void getZakUebertragungByEmptyDerivatAnforderungModulList() {
        List<ZakUebertragung> result = zakUebertragungDao.getZakUebertragungByDerivatAnforderungModulList(new ArrayList<>());
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void getZakUebertragungByDerivatAnforderungModulList() {
        List<ZakUebertragung> result = zakUebertragungDao.getZakUebertragungByDerivatAnforderungModulList(generateDerivatAnforderungModulList());
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void getCountByNullAnforderungAndModulAndStatusList() {
        Long result = zakUebertragungDao.getCountByAnforderungAndModulAndStatusList(null, new Modul(), generateZakStatusList());
        Assertions.assertFalse(result.equals(1L));
    }

    @Test
    public void getCountByAnforderungAndNullModulAndStatusList() {
        Long result = zakUebertragungDao.getCountByAnforderungAndModulAndStatusList(new Anforderung(), null, generateZakStatusList());
        Assertions.assertFalse(result.equals(1L));
    }

    @Test
    public void getCountByAnforderungAndModulAndNullStatusList() {
        Long result = zakUebertragungDao.getCountByAnforderungAndModulAndStatusList(new Anforderung(), new Modul(), null);
        Assertions.assertFalse(result.equals(1L));
    }

    @Test
    public void getCountByAnforderungAndModulAndEmptyStatusList() {
        Long result = zakUebertragungDao.getCountByAnforderungAndModulAndStatusList(new Anforderung(), new Modul(), new ArrayList<>());
        Assertions.assertFalse(result.equals(1L));
    }

    @Test
    public void getCountByNullAnforderungAndModulAndNullStatusList() {
        Long result = zakUebertragungDao.getCountByAnforderungAndModulAndStatusList(null, new Modul(), null);
        Assertions.assertFalse(result.equals(1L));
    }

    @Test
    public void getCountByAnforderungAndNullModulAndNullStatusList() {
        Long result = zakUebertragungDao.getCountByAnforderungAndModulAndStatusList(new Anforderung(), null, null);
        Assertions.assertFalse(result.equals(1L));
    }

    @Test
    public void getCountByNullAnforderungAndNullModulAndStatusList() {
        Long result = zakUebertragungDao.getCountByAnforderungAndModulAndStatusList(null, null, generateZakStatusList());
        Assertions.assertFalse(result.equals(1L));
    }

    @Test
    public void getCountByNullAnforderungAndNullModulAndNullStatusList() {
        Long result = zakUebertragungDao.getCountByAnforderungAndModulAndStatusList(null, null, null);
        Assertions.assertFalse(result.equals(1L));
    }

    @Test
    public void getCountByAnforderungAndModulAndStatusList() {
        long result = zakUebertragungDao.getCountByAnforderungAndModulAndStatusList(new Anforderung(), new Modul(), generateZakStatusList());
        Assertions.assertEquals(0L, result);
    }

    // ---------- utils --------------------------------------------------------


    private ZakUebertragung generateZakUebertragungList(DerivatAnforderungModul derivatAnforderungModul) {

        ZakUebertragung zakUebertragung = new ZakUebertragung();
        zakUebertragung.setUebertragungId(1L);
        zakUebertragung.setZakStatus(ZakStatus.EMPTY);
        zakUebertragung.setAenderungsdatum(new Date());
        zakUebertragung.setZakKommentar("Kommentar");
        zakUebertragung.setZakResponse("Response");
        zakUebertragung.setDerivatAnforderungModul(derivatAnforderungModul);
        zakUebertragung.setErsteller("Ersteller");
        zakUebertragung.setErstellungsdatum(new Date());
        zakUebertragung.setGeneratedXml("XML");

        return zakUebertragung;
    }

    private List<DerivatAnforderungModul> generateDerivatAnforderungModulList() {
        List<DerivatAnforderungModul> result = new ArrayList<>();
        result.add(TestDataFactory.generateDerivatAnforderungModul());
        return result;
    }


    private List<ZakStatus> generateZakStatusList() {
        List<ZakStatus> result = new ArrayList<>();
        result.add(ZakStatus.EMPTY);
        return result;
    }

//    private void mockQuery(String name, Long result) {
//        List<Long> resultList = new ArrayList<>();
//        resultList.add(result);
//        TypedQuery mockedQuery = mock(TypedQuery.class);
//        when(mockedQuery.getResultList()).thenReturn(resultList);
//        when(zakUebertragungDao.em.createNamedQuery(name, Long.class)).thenReturn(mockedQuery);
//        when(mockedQuery.setParameter(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(mockedQuery);
//    }
//
//    private void mockQuery(String name, Class c, List<ZakUebertragung> results) {
//        TypedQuery mockedQuery = mock(TypedQuery.class);
//        when(mockedQuery.getResultList()).thenReturn(results);
//        when(zakUebertragungDao.em.createNamedQuery(name, c)).thenReturn(mockedQuery);
//        when(mockedQuery.setParameter(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(mockedQuery);
//    }

}
