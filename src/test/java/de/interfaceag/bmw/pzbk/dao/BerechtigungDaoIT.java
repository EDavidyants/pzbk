package de.interfaceag.bmw.pzbk.dao;

import de.interfaceag.bmw.pzbk.IntegrationTest;
import de.interfaceag.bmw.pzbk.berechtigung.dao.TteamMitgliedDAO;
import de.interfaceag.bmw.pzbk.berechtigung.dao.TteamVertreterDAO;
import de.interfaceag.bmw.pzbk.entities.Berechtigung;
import de.interfaceag.bmw.pzbk.entities.BerechtigungHistory;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Mitarbeiter;
import de.interfaceag.bmw.pzbk.entities.Modul;
import de.interfaceag.bmw.pzbk.entities.ModulKomponente;
import de.interfaceag.bmw.pzbk.entities.ModulSeTeam;
import de.interfaceag.bmw.pzbk.entities.SensorCoc;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.entities.UserToRole;
import de.interfaceag.bmw.pzbk.entities.rollen.TTeamMitglied;
import de.interfaceag.bmw.pzbk.entities.rollen.TteamVertreter;
import de.interfaceag.bmw.pzbk.enums.BerechtigungZiel;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

class BerechtigungDaoIT extends IntegrationTest {

    private BerechtigungDao berechtigungDao = new BerechtigungDao();


    private MitarbeiterDao mitarbeiterDao = new MitarbeiterDao();
    private TteamVertreterDAO tteamVertreterDAO = new TteamVertreterDAO();
    private TteamMitgliedDAO tteamMitgliedDAO = new TteamMitgliedDAO();
    private TteamDao tteamDao = new TteamDao();
    private SensorCocDao sensorCocDao = new SensorCocDao();
    private DerivatDao derivatDao = new DerivatDao();
    private ModulDao modulDao = new ModulDao();
    private SensorCoc sensorCoc;
    private Derivat derivat;
    private ModulSeTeam modulSeTeam;
    private Tteam tteam;

    private Mitarbeiter testMitarbeiter;

    @BeforeEach
    void setUp() throws IllegalAccessException {

        super.begin();

        FieldUtils.writeField(berechtigungDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(mitarbeiterDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(tteamVertreterDAO, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(tteamMitgliedDAO, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(tteamDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(sensorCocDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(derivatDao, "entityManager", super.getEntityManager(), true);
        FieldUtils.writeField(modulDao, "entityManager", super.getEntityManager(), true);

        buildBerechtigungen();

        super.commit();
        super.begin();
    }

    private void buildBerechtigungen() {

        testMitarbeiter = TestDataFactory.generateMitarbeiter("Klaus", "Haus", "Abteilung 1");
        Mitarbeiter testMitarbeiter2 = TestDataFactory.generateMitarbeiter("Peter", "Lustig", "Abteilung 1");
        Mitarbeiter testMitarbeiter3 = TestDataFactory.generateMitarbeiter("Felix", "Firefox", "Abteilung 1");

        mitarbeiterDao.persistMitarbeiter(testMitarbeiter);
        mitarbeiterDao.persistMitarbeiter(testMitarbeiter2);
        mitarbeiterDao.persistMitarbeiter(testMitarbeiter3);

        tteam = TestDataFactory.generateTteam("tteam1");

        TTeamMitglied tteamMitglied = generateTteamMitgliedForMitarbeiter(testMitarbeiter2, tteam);
        TteamVertreter tteamVertreter = generateTteamVertreterForMitarbeiter(testMitarbeiter3, tteam);

        tteamDao.saveTteam(tteam);

        tteamMitgliedDAO.persistTteamMitglied(tteamMitglied);
        tteamVertreterDAO.persistTteamVertreter(tteamVertreter);

        Berechtigung berechtigung = generateAdminBerechtigungForMitarbeiter(testMitarbeiter);
        berechtigungDao.persistBerechtigung(berechtigung);

        sensorCoc = TestDataFactory.generateSensorCoc();
        sensorCocDao.persistSensorCoc(sensorCoc);

        Berechtigung berechtigungSensor = generateSensorBerechtigung(testMitarbeiter, sensorCoc.getSensorCocId());
        berechtigungDao.persistBerechtigung(berechtigungSensor);

        Berechtigung berechtigungSensorCocLeiter = generateSernsorCocLeiterBerechtigung(testMitarbeiter, sensorCoc.getSensorCocId());
        berechtigungDao.persistBerechtigung(berechtigungSensorCocLeiter);

        Berechtigung berechtigungSensorCocVertreter = generateSensorCocVertreterBerechtigung(testMitarbeiter, sensorCoc.getSensorCocId());
        berechtigungDao.persistBerechtigung(berechtigungSensorCocVertreter);

        Berechtigung berechtigungUmsetzungsbestaetiger = generateUmsetzungsbestaetigerBerechtigung(testMitarbeiter, sensorCoc.getSensorCocId());
        berechtigungDao.persistBerechtigung(berechtigungUmsetzungsbestaetiger);

        Berechtigung berechtigungTteamLeiter = generateTteamLeiterBerechtigung(testMitarbeiter, tteam.getId());
        berechtigungDao.persistBerechtigung(berechtigungTteamLeiter);

        derivat = TestDataFactory.generateDerivat("DerivatName", "DerivatBezeichung", "Produktlinie");
        derivatDao.persistDerivat(derivat);

        Berechtigung berechtigungAnforderer = generateAnfordererBerechtigung(testMitarbeiter, derivat.getId());
        berechtigungDao.persistBerechtigung(berechtigungAnforderer);

        Modul modul = TestDataFactory.generateModul("Modul1", "Fachbereich1", "Beschreibung");
        modulDao.persistModule(Arrays.asList(modul));

        ModulKomponente modulkomponente = TestDataFactory.generateModulKomponente();
        modulDao.persistModulKomponente(modulkomponente);


        modulSeTeam = TestDataFactory.generateModulSeTeam(modul, Arrays.asList(modulkomponente));
        modulDao.persistSeTeam(modulSeTeam);

        Berechtigung berechtigungECoC = generateECoCBerechtigung(testMitarbeiter, modulSeTeam.getId());
        berechtigungDao.persistBerechtigung(berechtigungECoC);

        generateRolesInUserToRolesForUser(testMitarbeiter);

    }

    private void generateRolesInUserToRolesForUser(Mitarbeiter mitarbeiter) {
        List<Rolle> allroles = Rolle.getAllRolles();
        allroles.forEach(rolle -> {
            UserToRole userToRole = new UserToRole();
            userToRole.setqNumber(mitarbeiter.getQNumber());
            userToRole.setRole(rolle.getRolleRawString());
            berechtigungDao.persistNewRolleForUser(userToRole);
        });

    }

    private Berechtigung generateAdminBerechtigungForMitarbeiter(Mitarbeiter testMitarbeiter) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(testMitarbeiter);
        berechtigung.setRolle(Rolle.ADMIN);

        return berechtigung;
    }

    private TTeamMitglied generateTteamMitgliedForMitarbeiter(Mitarbeiter testMitarbeiter, Tteam tteam) {
        TTeamMitglied tteamMitglied = new TTeamMitglied();
        tteamMitglied.setMitarbeiter(testMitarbeiter);
        tteamMitglied.setRechttype(Rechttype.SCHREIBRECHT);
        tteamMitglied.setTteam(tteam);
        return tteamMitglied;
    }

    private TteamVertreter generateTteamVertreterForMitarbeiter(Mitarbeiter testMitarbeiter, Tteam tteam) {
        TteamVertreter tteamVertreter = new TteamVertreter();
        tteamVertreter.setMitarbeiter(testMitarbeiter);
        tteamVertreter.setTteam(tteam);
        return tteamVertreter;
    }

    private Berechtigung generateSensorBerechtigung(Mitarbeiter mitarbeiter, Long sensorCocId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.SENSOR);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.SENSOR_COC);
        berechtigung.setFuerId(sensorCocId.toString());
        return berechtigung;
    }

    private Berechtigung generateSernsorCocLeiterBerechtigung(Mitarbeiter mitarbeiter, Long sensorCocId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.SENSORCOCLEITER);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.SENSOR_COC);
        berechtigung.setFuerId(sensorCocId.toString());
        return berechtigung;
    }

    private Berechtigung generateSensorCocVertreterBerechtigung(Mitarbeiter mitarbeiter, Long sensorCocId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.SCL_VERTRETER);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.SENSOR_COC);
        berechtigung.setFuerId(sensorCocId.toString());
        return berechtigung;
    }

    private Berechtigung generateAnfordererBerechtigung(Mitarbeiter mitarbeiter, Long derivatId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.ANFORDERER);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.DERIVAT);
        berechtigung.setFuerId(derivatId.toString());
        return berechtigung;
    }

    private Berechtigung generateECoCBerechtigung(Mitarbeiter mitarbeiter, Long modulSeTeamNameId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.E_COC);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.MODULSETEAM);
        berechtigung.setFuerId(modulSeTeamNameId.toString());
        return berechtigung;
    }

    private Berechtigung generateTteamLeiterBerechtigung(Mitarbeiter mitarbeiter, Long tteamId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.T_TEAMLEITER);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.TTEAM);
        berechtigung.setFuerId(tteamId.toString());
        return berechtigung;
    }

    private Berechtigung generateUmsetzungsbestaetigerBerechtigung(Mitarbeiter mitarbeiter, Long sensorCocId) {
        Berechtigung berechtigung = new Berechtigung();
        berechtigung.setMitarbeiter(mitarbeiter);
        berechtigung.setRolle(Rolle.UMSETZUNGSBESTAETIGER);
        berechtigung.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigung.setFuer(BerechtigungZiel.SENSOR_COC);
        berechtigung.setFuerId(sensorCocId.toString());
        return berechtigung;
    }


    @Test
    void persistAndGetBerechtigungHistoryByIdTest() {

        Mitarbeiter berechtigter = TestDataFactory.generateMitarbeiter("Karl", "Maurer", "Abteilung 1");
        Mitarbeiter zustendiger = TestDataFactory.generateMitarbeiter("Klaus", "Kanone", "Abteilung 1");
        Date datum = new Date();

        BerechtigungHistory berechtigungHistory = new BerechtigungHistory();
        berechtigungHistory.setBerechtigter(berechtigter.getFullName());
        berechtigungHistory.setBerechtigterId(testMitarbeiter.getId().toString());
        berechtigungHistory.setDatum(datum);
        berechtigungHistory.setErteilt(true);
        berechtigungHistory.setFuer(String.valueOf(1L));
        berechtigungHistory.setFuerId(String.valueOf(1L));
        berechtigungHistory.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigungHistory.setRolle(Rolle.SENSOR.getRolleRawString());
        berechtigungHistory.setZustaendigerId(testMitarbeiter.getId().toString());
        berechtigungHistory.setZustaendiger(zustendiger.getFullName());

        berechtigungDao.persistBerechtigungHistory(berechtigungHistory);


        BerechtigungHistory result = berechtigungDao.getBerechtigungHistoryById(1L);

        Assertions.assertEquals(berechtigungHistory, result);
    }

    @Test
    void getBerechtigungHistoryForNullId() {
        BerechtigungHistory result = berechtigungDao.getBerechtigungHistoryById(null);
        Assertions.assertEquals(null, result);
    }


    @Test
    void getBerechtigungByMitarbeiter() {
        List<Berechtigung> foundBerechtigung = berechtigungDao.getBerechtigungByMitarbeiter(testMitarbeiter);
        MatcherAssert.assertThat(foundBerechtigung, IsCollectionWithSize.hasSize(8));

    }

    @Test
    void getBerechtigungByMitarbeiterAndRolle() {
        List<Berechtigung> foundBerechtigung = berechtigungDao.getBerechtigungByMitarbeiterAndRolle(testMitarbeiter, Rolle.SENSOR);
        MatcherAssert.assertThat(foundBerechtigung, IsCollectionWithSize.hasSize(1));

    }

    @Test
    void getBerechtigungByMitarbeiterAndRolleAndZiel() {
        List<Berechtigung> foundBerechtigung = berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZiel(testMitarbeiter, Rolle.SENSOR, BerechtigungZiel.SENSOR_COC);
        MatcherAssert.assertThat(foundBerechtigung, IsCollectionWithSize.hasSize(1));

    }

    @Test
    void getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype() {
        List<String> foundBerechtigungFuerId = berechtigungDao.getBerechtigungFuerIdByMitarbeiterAndRolleAndZielWithRechttype(testMitarbeiter, Rolle.SENSOR, BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT);
        MatcherAssert.assertThat(foundBerechtigungFuerId, IsCollectionWithSize.hasSize(1));
    }

    @Test
    void getBerechtigungByMitarbeiterAndRolleAndZielById() {
        List<Berechtigung> foundBerechtigung = berechtigungDao.getBerechtigungByMitarbeiterAndRolleAndZielById(testMitarbeiter, Rolle.SENSOR, sensorCoc.getSensorCocId().toString(), BerechtigungZiel.SENSOR_COC);
        MatcherAssert.assertThat(foundBerechtigung, IsCollectionWithSize.hasSize(1));

    }

    @Test
    void getMitarbeiterForDefinierteBerechtigungByRolleAndZielId() {
        List<Mitarbeiter> foundMitarbeiter = berechtigungDao.getMitarbeiterForDefinierteBerechtigungByRolleAndZielId(Rolle.SENSOR, sensorCoc.getSensorCocId().toString(), BerechtigungZiel.SENSOR_COC, Rechttype.SCHREIBRECHT);
        MatcherAssert.assertThat(foundMitarbeiter, IsCollectionWithSize.hasSize(1));

    }

    @Test
    void getAuthorizedSensorCocForSensor() {

        List<Long> foundSensorCocs = berechtigungDao.getAuthorizedSensorCocForSensor(testMitarbeiter);
        MatcherAssert.assertThat(foundSensorCocs, IsCollectionWithSize.hasSize(1));

    }

}