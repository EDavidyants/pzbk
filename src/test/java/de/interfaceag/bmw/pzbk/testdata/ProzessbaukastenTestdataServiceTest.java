package de.interfaceag.bmw.pzbk.testdata;

import de.interfaceag.bmw.pzbk.entities.Anforderung;
import de.interfaceag.bmw.pzbk.entities.Derivat;
import de.interfaceag.bmw.pzbk.entities.Konzept;
import de.interfaceag.bmw.pzbk.entities.Meldung;
import de.interfaceag.bmw.pzbk.entities.Prozessbaukasten;
import de.interfaceag.bmw.pzbk.entities.Tteam;
import de.interfaceag.bmw.pzbk.enums.ProzessbaukastenStatus;
import de.interfaceag.bmw.pzbk.enums.Status;
import de.interfaceag.bmw.pzbk.exceptions.UserNotFoundException;
import de.interfaceag.bmw.pzbk.localization.LocalizationService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenAnforderungKonzeptService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenReadService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.ProzessbaukastenStatusChangeService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.history.ProzessbaukastenHistoryService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenCreateAnforderungZuordnungService;
import de.interfaceag.bmw.pzbk.prozessbaukasten.zuordnen.ProzessbaukastenZuordnenService;
import de.interfaceag.bmw.pzbk.services.AnforderungService;
import de.interfaceag.bmw.pzbk.services.DerivatService;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import de.interfaceag.bmw.pzbk.services.TteamService;
import de.interfaceag.bmw.pzbk.services.UserSearchService;
import de.interfaceag.bmw.pzbk.shared.objectIds.AnforderungId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class ProzessbaukastenTestdataServiceTest {

    @Mock
    private TteamService tteamService;
    @Mock
    private ProzessbaukastenService prozessbaukastenService;
    @Mock
    private UserSearchService userSearchService;
    @Mock
    private DerivatService derivatService;
    @Mock
    private AnforderungService anforderungService;
    @Mock
    private ProzessbaukastenAnforderungKonzeptService prozessbaukastenAnforderungKonzeptService;
    @Mock
    private MeldungTestdataService meldungTestdataService;
    @Mock
    private AnforderungTestdataService anforderungTestdataService;
    @Mock
    private ProzessbaukastenZuordnenService prozessbaukastenZuordnenService;
    @Mock
    private ProzessbaukastenStatusChangeService prozessbaukastenStatusChangeService;
    @Mock
    private LocalizationService localizationService;
    @Mock
    private ProzessbaukastenHistoryService prozessbaukastenHistoryService;
    @Mock
    private ProzessbaukastenReadService prozessbaukastenReadService;
    @Mock
    private ProzessbaukastenCreateAnforderungZuordnungService createAnforderungZuordnungService;
    @InjectMocks
    private final ProzessbaukastenTestdataService prozessbaukastenTestdataService = new ProzessbaukastenTestdataService();

    private List<Tteam> tteams;
    private List<Derivat> derivate;

    @BeforeEach
    public void setUp() {
        when(userSearchService.getMitarbeiterByQnumber("q2222222")).thenReturn(TestDataFactory.generateMitarbeiter("Max", "Admin", "Abt.2", "q2222222"));

        tteams = new ArrayList<>();
        Tteam tteamVerdeck = TestDataFactory.generateTteam("Verdeck");
        Tteam tteamAF = TestDataFactory.generateTteam("AF");
        Tteam tteamUnterboden = TestDataFactory.generateTteam("Unterboden");
        tteams.add(tteamVerdeck);
        tteams.add(tteamAF);
        tteams.add(tteamUnterboden);

        when(tteamService.getAllTteams()).thenReturn(tteams);

        derivate = new ArrayList<>();
        Derivat e20 = TestDataFactory.generateDerivat("E20", "Bezeichnung E20", "LI");
        Derivat e21 = TestDataFactory.generateDerivat("E21", "Bezeichnung E21", "LI");
        Derivat f16 = TestDataFactory.generateDerivat("F16", "Bezeichnung F16", "LK");
        derivate.add(e20);
        derivate.add(e21);
        derivate.add(f16);

        when(derivatService.getAllErstanlaeufer()).thenReturn(derivate);

        Prozessbaukasten origin = TestDataFactory.generateProzessbaukaten("P123", 1);
        origin.setTteam(tteams.get(getRandomListIndex(tteams)));
        origin.setErstanlaeufer(derivate.get(getRandomListIndex(derivate)));

        when(prozessbaukastenService.createRealCopy(any())).thenReturn(Optional.ofNullable(origin));
        when(meldungTestdataService.getRandomSensorCoc()).thenReturn(TestDataFactory.generateSensorCoc());
        Meldung meldung = TestDataFactory.generateMeldung();
        meldung.setStatus(Status.M_ZUGEORDNET);
        when(meldungTestdataService.generateMeldung(any(), any(), any())).thenReturn(meldung);
        when(anforderungTestdataService.generateAnforderung(any(), any(), any())).thenReturn(new AnforderungId(1L));
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        anforderung.setStatus(Status.A_FREIGEGEBEN);
        when(anforderungService.getAnforderungById(1L)).thenReturn(anforderung);
    }

    @Test
    public void testAnzahlProzessbaukasten() throws UserNotFoundException {
        List<Prozessbaukasten> result = prozessbaukastenTestdataService.generateProzessbaukastenWithRandomDataForAdmin();
        int resultSize = (result != null) ? result.size() : 0;
        Assertions.assertEquals(20, resultSize);
    }

    @Test
    public void testAnzahlProzessbaukastenWithTteam() throws UserNotFoundException {
        List<Prozessbaukasten> prozessbaukasten = prozessbaukastenTestdataService.generateProzessbaukastenWithRandomDataForAdmin()
                .stream().filter(p -> p.getStatus() == ProzessbaukastenStatus.GUELTIG).collect(Collectors.toList());

        prozessbaukasten.forEach((p) -> {
            Assertions.assertTrue(p.getTteam() != null && tteams.contains(p.getTteam()));
        });

    }

    @Test
    public void testAnzahlProzessbaukastenWithErstlaeufer() throws UserNotFoundException {
        List<Prozessbaukasten> prozessbaukasten = prozessbaukastenTestdataService.generateProzessbaukastenWithRandomDataForAdmin()
                .stream().filter(p -> p.getStatus() == ProzessbaukastenStatus.GUELTIG).collect(Collectors.toList());

        prozessbaukasten.forEach((p) -> {
            Assertions.assertTrue(p.getErstanlaeufer() != null && derivate.contains(p.getErstanlaeufer()));
        });

    }

    @Test
    public void testAnzahlProzessbaukastenWithAnforderungenWithKonzept() throws UserNotFoundException {

        List<Prozessbaukasten> prozessbaukasten = prozessbaukastenTestdataService.generateProzessbaukastenWithRandomDataForAdmin()
                .stream().filter(p -> p.getStatus() == ProzessbaukastenStatus.GUELTIG).collect(Collectors.toList());

        int counter = 0;
        counter = prozessbaukasten.stream().map((p) -> p.getAnforderungen().size()).reduce(counter, Integer::sum);

        verify(prozessbaukastenAnforderungKonzeptService, times(counter)).createAnforderungKonzept(any(Anforderung.class), any(Prozessbaukasten.class), any(Konzept.class));

    }

    private int getRandomListIndex(List<?> values) {
        int index = RandomUtils.getRandomForSize(values.size());
        return index;
    }

}
