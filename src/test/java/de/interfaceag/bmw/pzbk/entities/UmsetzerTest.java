package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class UmsetzerTest {

    @Test
    public void toStringNull() {
        Umsetzer umsetzer = new Umsetzer();
        String result = umsetzer.toString();
        Assertions.assertEquals("", result);
    }

    @Test
    public void toStringOnlySeTeam() {
        ModulSeTeam seTeam = TestDataFactory.generateModulSeTeam(null, new ArrayList<>());
        Umsetzer umsetzer = new Umsetzer(seTeam);
        String result = umsetzer.toString();
        Assertions.assertEquals("SE-TEAM", result);
    }

    @Test
    public void toStringSeTeamModul() {
        Modul modul = TestDataFactory.generateModul();
        ModulSeTeam seTeam = TestDataFactory.generateModulSeTeam(modul, new ArrayList<>());
        Umsetzer umsetzer = new Umsetzer(seTeam);
        String result = umsetzer.toString();
        Assertions.assertEquals("Name > SE-TEAM", result);
    }

    @Test
    public void toStringFull() {
        Modul modul = TestDataFactory.generateModul();
        ModulKomponente komponente = TestDataFactory.generateModulKomponente();
        ModulSeTeam seTeam = TestDataFactory.generateModulSeTeam(modul, new ArrayList<>());
        Umsetzer umsetzer = new Umsetzer(seTeam, komponente);
        String result = umsetzer.toString();
        Assertions.assertEquals("Name > SE-TEAM > Name", result);
    }

}
