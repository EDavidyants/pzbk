package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 * @author evda
 */
public class KonzeptTest {

    private Konzept theKonzept;
    private Konzept otherKonzept;

    @Before
    public void setUp() {
        theKonzept = TestDataFactory.generateKonzept("ORESUND", "Konzept one");
        otherKonzept = TestDataFactory.generateKonzept("KLIPPAN", "Konzept two");
    }

    @Test
    public void testCompareToByBezeichnungOne() {
        int compareToFact = theKonzept.compareTo(otherKonzept);
        Assertions.assertTrue(compareToFact > 0);
    }

    @Test
    public void testCompareToByBezeichnungTwo() {
        int compareToFact = otherKonzept.compareTo(theKonzept);
        Assertions.assertTrue(compareToFact < 0);
    }

    @Test
    public void testCompareToByIdOne() {
        theKonzept.setBezeichnung("KLIPPAN");
        theKonzept.setId(123L);
        otherKonzept.setId(256L);
        int compareToFact = otherKonzept.compareTo(theKonzept);
        assertThat(compareToFact, is(0));
    }

    @Test
    public void testCompareToByIdTwo() {
        theKonzept.setBezeichnung("KLIPPAN");
        theKonzept.setId(123L);
        otherKonzept.setId(256L);
        int compareToFact = theKonzept.compareTo(otherKonzept);
        assertThat(compareToFact, is(0));
    }

}
