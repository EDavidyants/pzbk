package de.interfaceag.bmw.pzbk.entities;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.Collections;

/**
 *
 * @author evda
 */
public class ModulSeTeamTest {

    @Test
    public void testToString() {
        Modul modul = new Modul("Modul 1");
        ModulSeTeam modulSeTeam = new ModulSeTeam("SeTeam 11", modul, Collections.emptySet());
        String toStringFact = modulSeTeam.toString();
        String toStringExpected = "Modul 1 > SeTeam 11";
        Assertions.assertEquals(toStringExpected, toStringFact);
    }

}
