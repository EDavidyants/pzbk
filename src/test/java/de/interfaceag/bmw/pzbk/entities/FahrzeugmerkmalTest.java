package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FahrzeugmerkmalTest {

    private static final String MERKMAL = "merkmal";
    private static final long ID = 42L;


    private Fahrzeugmerkmal fahrzeugmerkmal;

    @BeforeEach
    void setUp() {
        fahrzeugmerkmal = new Fahrzeugmerkmal(MERKMAL);
    }

    @Test
    void toString1() {
        fahrzeugmerkmal.setId(ID);
        final String result = fahrzeugmerkmal.toString();
        assertThat(result, is("Fahrzeugmerkmal{id=42, merkmal='merkmal'}"));
    }

    @Test
    void equalsIdNotEqual() {
        fahrzeugmerkmal.setId(ID);
        Fahrzeugmerkmal anotherFahrzeugmerkmal = new Fahrzeugmerkmal(MERKMAL);
        final boolean result = fahrzeugmerkmal.equals(anotherFahrzeugmerkmal);
        assertFalse(result);
    }

    @Test
    void equalsMerkmalNotEqual() {
        Fahrzeugmerkmal anotherFahrzeugmerkmal = new Fahrzeugmerkmal(MERKMAL + " ");
        final boolean result = fahrzeugmerkmal.equals(anotherFahrzeugmerkmal);
        assertFalse(result);
    }

    @Test
    void equalsEqual() {
        fahrzeugmerkmal.setId(ID);
        Fahrzeugmerkmal anotherFahrzeugmerkmal = new Fahrzeugmerkmal(MERKMAL);
        anotherFahrzeugmerkmal.setId(ID);
        final boolean result = fahrzeugmerkmal.equals(anotherFahrzeugmerkmal);
        assertTrue(result);
    }

    @Test
    void equalsNull() {
        final boolean result = fahrzeugmerkmal.equals(null);
        assertFalse(result);
    }

    @Test
    void equalsAnotherObject() {
        final boolean result = fahrzeugmerkmal.equals(new Object());
        assertFalse(result);
    }

    @Test
    void hashCode1() {
        final int result = fahrzeugmerkmal.hashCode();
        assertThat(result, is(953797840));
    }

    @Test
    void getId() {
        fahrzeugmerkmal.setId(ID);
        final Long id = fahrzeugmerkmal.getId();
        assertThat(id, is(ID));
    }

    @Test
    void getFahrzeugmerkmalIdNullId() {
        final FahrzeugmerkmalId fahrzeugmerkmalId = fahrzeugmerkmal.getFahrzeugmerkmalId();
        assertThat(fahrzeugmerkmalId, is(new FahrzeugmerkmalId(null)));
    }

    @Test
    void getFahrzeugmerkmalId() {
        fahrzeugmerkmal.setId(ID);
        final FahrzeugmerkmalId fahrzeugmerkmalId = fahrzeugmerkmal.getFahrzeugmerkmalId();
        assertThat(fahrzeugmerkmalId, is(new FahrzeugmerkmalId(ID)));
    }

    @Test
    void getMerkmal() {
        final String merkmal = fahrzeugmerkmal.getMerkmal();
        assertThat(merkmal, is(MERKMAL));
    }

    @Test
    void setMerkmal() {
        fahrzeugmerkmal.setMerkmal(MERKMAL + MERKMAL);
        final String merkmal = fahrzeugmerkmal.getMerkmal();
        assertThat(merkmal, is(MERKMAL + MERKMAL));
    }
}