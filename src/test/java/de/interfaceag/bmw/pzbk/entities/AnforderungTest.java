package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AnforderungTest {

    @Test
    public void testAnforderungToStringMethodValue() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Assert.assertEquals("A42 | V1", anforderung.toString());
    }

    @Test
    public void testIsProzessbaukastenZugeordnetPositive() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        Prozessbaukasten prozessbaukasten = TestDataFactory.generateProzessbaukasten();
        Set<Prozessbaukasten> prozessbaukaesten = new HashSet<>();
        prozessbaukaesten.add(prozessbaukasten);
        anforderung.setProzessbaukasten(prozessbaukaesten);
        Boolean isZugeordnet = anforderung.isProzessbaukastenZugeordnet();
        Assert.assertTrue(isZugeordnet);
    }

    @Test
    public void testIsProzessbaukastenZugeordnetNegativeForEmpty() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        anforderung.setProzessbaukasten(new HashSet<>());
        Boolean isZugeordnet = anforderung.isProzessbaukastenZugeordnet();
        Assert.assertFalse(isZugeordnet);
    }

    @Test
    public void testIsProzessbaukastenZugeordnetNegativeForNull() {
        Anforderung anforderung = TestDataFactory.generateAnforderung();
        anforderung.setProzessbaukasten(null);
        Boolean isZugeordnet = anforderung.isProzessbaukastenZugeordnet();
        Assert.assertFalse(isZugeordnet);
    }

}
