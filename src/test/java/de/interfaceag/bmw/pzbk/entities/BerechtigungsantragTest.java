package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.AntragStatus;
import de.interfaceag.bmw.pzbk.enums.Rechttype;
import de.interfaceag.bmw.pzbk.enums.Rolle;
import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;

/**
 *
 * @author evda
 */
@ExtendWith(MockitoExtension.class)
public class BerechtigungsantragTest {

    private Berechtigungsantrag berechtigungsantrag;
    private Mitarbeiter antragsteller;
    private Mitarbeiter bearbeiter;
    private SensorCoc sensorCoc;
    private Tteam tteam;
    private ModulSeTeam modulSeTeam;

    @BeforeEach
    public void setUp() {
        Calendar calendar = new GregorianCalendar(2019, 9, 25);
        antragsteller = TestDataFactory.generateMitarbeiter(1L, "Zack", "Logan", "Abt.-2");
        bearbeiter = TestDataFactory.generateMitarbeiter(1L, "Max", "Admin", "Abt.-2");
        sensorCoc = TestDataFactory.generateSensorCoc();
        sensorCoc.setTechnologie("TGF_Sitze");
        tteam = TestDataFactory.generateTteam();
        tteam.setTeamName("AF");
        modulSeTeam = TestDataFactory.generateModulSeTeam(1L, TestDataFactory.generateModul("Modul 1", "Fachbereich", "Beschreibung"), Collections.emptyList());
        modulSeTeam.setName("SeTeam7");
        berechtigungsantrag = new Berechtigungsantrag();
        berechtigungsantrag.setId(1L);
        berechtigungsantrag.setErstellungsdatum(calendar.getTime());
        berechtigungsantrag.setAntragsteller(antragsteller);
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testGetAntragBeschluss(AntragStatus status) {
        Calendar calendar = new GregorianCalendar(2019, 9, 26);
        berechtigungsantrag.setAenderungsdatum(calendar.getTime());
        berechtigungsantrag.setStatus(status);
        berechtigungsantrag.setBearbeiter(bearbeiter);
        String kommentar = "For this reason..";
        berechtigungsantrag.setKommentar(kommentar);
        String beschluss = berechtigungsantrag.getAntragBeschluss();
        switch (status) {
            case OFFEN:
                Assertions.assertEquals("Beschluss steht aus.", beschluss);
                break;

            case ANGENOMMEN:
                Assertions.assertEquals("Angenommen am 26.10.2019 von Max Admin | Abt.-2 mit Kommentar 'For this reason..'.", beschluss);
                break;

            case ABGELEHNT:
                Assertions.assertEquals("Abgelehnt am 26.10.2019 von Max Admin | Abt.-2 mit Kommentar 'For this reason..'.", beschluss);
                break;

            default:
                break;
        }
    }

    @Test
    public void testGetBerechtigungZielForSensorCoc() {
        berechtigungsantrag.setSensorCoc(sensorCoc);
        String berechtigungZiel = berechtigungsantrag.getBerechtigungZiel();
        Assertions.assertEquals("SensorCoC 'TGF_Sitze'", berechtigungZiel);
    }

    @Test
    public void testGetBerechtigungZielForTteam() {
        berechtigungsantrag.setTteam(tteam);
        String berechtigungZiel = berechtigungsantrag.getBerechtigungZiel();
        Assertions.assertEquals("T-Team 'AF'", berechtigungZiel);
    }

    @Test
    public void testGetBerechtigungZielForModulSeTteam() {
        berechtigungsantrag.setModulSeTeam(modulSeTeam);
        String berechtigungZiel = berechtigungsantrag.getBerechtigungZiel();
        Assertions.assertEquals("ModulSeTeam 'Modul 1 > SeTeam7'", berechtigungZiel);
    }

    @Test
    public void testGetBerechtigungZielForNotPresentBerechtigungZiel() {
        String berechtigungZiel = berechtigungsantrag.getBerechtigungZiel();
        Assertions.assertEquals("Kein gueltige Berechtigungsziel", berechtigungZiel);
    }

    @Test
    public void testToStringForNochOffen() {
        berechtigungsantrag.setRolle(Rolle.SENSOR);
        berechtigungsantrag.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigungsantrag.setSensorCoc(sensorCoc);

        String toString = berechtigungsantrag.toString();
        String expected = "Berechtigungsantrag ID 1 von Zack Logan | Abt.-2 fuer Rolle Sensor mit Rechttype Schreibrecht "
                + "fuer SensorCoC 'TGF_Sitze' wurde am 25.10.2019 erstellt.\n"
                + "Beschluss steht aus.";

        Assertions.assertEquals(expected, toString);
    }

    @Test
    public void testToStringForAngenommenWithKommentar() {
        berechtigungsantrag.setRolle(Rolle.SENSOR);
        berechtigungsantrag.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigungsantrag.setSensorCoc(sensorCoc);
        Calendar calendar = new GregorianCalendar(2019, 9, 26);
        berechtigungsantrag.setAenderungsdatum(calendar.getTime());
        berechtigungsantrag.setStatus(AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setBearbeiter(bearbeiter);
        String kommentar = "For this reason..";
        berechtigungsantrag.setKommentar(kommentar);

        String toString = berechtigungsantrag.toString();
        String expected = "Berechtigungsantrag ID 1 von Zack Logan | Abt.-2 fuer Rolle Sensor mit Rechttype Schreibrecht "
                + "fuer SensorCoC 'TGF_Sitze' wurde am 25.10.2019 erstellt.\n"
                + "Angenommen am 26.10.2019 von Max Admin | Abt.-2 mit Kommentar 'For this reason..'.";

        Assertions.assertEquals(expected, toString);
    }

    @Test
    public void testToStringForAngenommenWithKommentarAndNullErstellungsdatum() {
        berechtigungsantrag.setErstellungsdatum(null);
        berechtigungsantrag.setRolle(Rolle.SENSOR);
        berechtigungsantrag.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigungsantrag.setSensorCoc(sensorCoc);
        Calendar calendar = new GregorianCalendar(2019, 9, 26);
        berechtigungsantrag.setAenderungsdatum(calendar.getTime());
        berechtigungsantrag.setStatus(AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setBearbeiter(bearbeiter);
        String kommentar = "For this reason..";
        berechtigungsantrag.setKommentar(kommentar);

        String toString = berechtigungsantrag.toString();
        String expected = "Berechtigungsantrag ID 1 von Zack Logan | Abt.-2 fuer Rolle Sensor mit Rechttype Schreibrecht "
                + "fuer SensorCoC 'TGF_Sitze' wurde erstellt.\n"
                + "Angenommen am 26.10.2019 von Max Admin | Abt.-2 mit Kommentar 'For this reason..'.";

        Assertions.assertEquals(expected, toString);
    }

    @Test
    public void testToStringForAngenommenWithKommentarAndNullAenderungsdatum() {
        berechtigungsantrag.setRolle(Rolle.SENSOR);
        berechtigungsantrag.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigungsantrag.setSensorCoc(sensorCoc);
        berechtigungsantrag.setStatus(AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setAenderungsdatum(null);
        berechtigungsantrag.setBearbeiter(bearbeiter);
        String kommentar = "For this reason..";
        berechtigungsantrag.setKommentar(kommentar);

        String toString = berechtigungsantrag.toString();
        String expected = "Berechtigungsantrag ID 1 von Zack Logan | Abt.-2 fuer Rolle Sensor mit Rechttype Schreibrecht "
                + "fuer SensorCoC 'TGF_Sitze' wurde am 25.10.2019 erstellt.\n"
                + "Angenommen von Max Admin | Abt.-2 mit Kommentar 'For this reason..'.";

        Assertions.assertEquals(expected, toString);
    }

    @Test
    public void testToStringForAngenommenWithKommentarAndNullErstellungsAndAenderungsdatum() {
        berechtigungsantrag.setErstellungsdatum(null);
        berechtigungsantrag.setRolle(Rolle.SENSOR);
        berechtigungsantrag.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigungsantrag.setSensorCoc(sensorCoc);
        berechtigungsantrag.setStatus(AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setAenderungsdatum(null);
        berechtigungsantrag.setBearbeiter(bearbeiter);
        String kommentar = "For this reason..";
        berechtigungsantrag.setKommentar(kommentar);

        String toString = berechtigungsantrag.toString();
        String expected = "Berechtigungsantrag ID 1 von Zack Logan | Abt.-2 fuer Rolle Sensor mit Rechttype Schreibrecht "
                + "fuer SensorCoC 'TGF_Sitze' wurde erstellt.\n"
                + "Angenommen von Max Admin | Abt.-2 mit Kommentar 'For this reason..'.";

        Assertions.assertEquals(expected, toString);
    }

    @Test
    public void testToStringForAngenommenWithoutKommentar() {
        berechtigungsantrag.setRolle(Rolle.SENSOR);
        berechtigungsantrag.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigungsantrag.setSensorCoc(sensorCoc);
        Calendar calendar = new GregorianCalendar(2019, 9, 26);
        berechtigungsantrag.setAenderungsdatum(calendar.getTime());
        berechtigungsantrag.setStatus(AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setBearbeiter(bearbeiter);
        String kommentar = null;
        berechtigungsantrag.setKommentar(kommentar);

        String toString = berechtigungsantrag.toString();
        String expected = "Berechtigungsantrag ID 1 von Zack Logan | Abt.-2 fuer Rolle Sensor mit Rechttype Schreibrecht "
                + "fuer SensorCoC 'TGF_Sitze' wurde am 25.10.2019 erstellt.\n"
                + "Angenommen am 26.10.2019 von Max Admin | Abt.-2.";

        Assertions.assertEquals(expected, toString);
    }

    @Test
    public void testToStringForAngenommenWithoutKommentar2() {
        berechtigungsantrag.setRolle(Rolle.SENSOR);
        berechtigungsantrag.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigungsantrag.setSensorCoc(sensorCoc);
        Calendar calendar = new GregorianCalendar(2019, 9, 26);
        berechtigungsantrag.setAenderungsdatum(calendar.getTime());
        berechtigungsantrag.setStatus(AntragStatus.ANGENOMMEN);
        berechtigungsantrag.setBearbeiter(bearbeiter);
        String kommentar = "";
        berechtigungsantrag.setKommentar(kommentar);

        String toString = berechtigungsantrag.toString();
        String expected = "Berechtigungsantrag ID 1 von Zack Logan | Abt.-2 fuer Rolle Sensor mit Rechttype Schreibrecht "
                + "fuer SensorCoC 'TGF_Sitze' wurde am 25.10.2019 erstellt.\n"
                + "Angenommen am 26.10.2019 von Max Admin | Abt.-2.";

        Assertions.assertEquals(expected, toString);
    }

    @Test
    public void testToStringForAbgelehnt() {
        berechtigungsantrag.setRolle(Rolle.SENSOR);
        berechtigungsantrag.setRechttype(Rechttype.SCHREIBRECHT);
        berechtigungsantrag.setSensorCoc(sensorCoc);
        Calendar calendar = new GregorianCalendar(2019, 9, 26);
        berechtigungsantrag.setAenderungsdatum(calendar.getTime());
        berechtigungsantrag.setStatus(AntragStatus.ABGELEHNT);
        berechtigungsantrag.setBearbeiter(bearbeiter);
        String kommentar = "For this reason..";
        berechtigungsantrag.setKommentar(kommentar);

        String toString = berechtigungsantrag.toString();
        String expected = "Berechtigungsantrag ID 1 von Zack Logan | Abt.-2 fuer Rolle Sensor mit Rechttype Schreibrecht "
                + "fuer SensorCoC 'TGF_Sitze' wurde am 25.10.2019 erstellt.\n"
                + "Abgelehnt am 26.10.2019 von Max Admin | Abt.-2 mit Kommentar 'For this reason..'.";

        Assertions.assertEquals(expected, toString);
    }

    @ParameterizedTest
    @EnumSource(AntragStatus.class)
    public void testSetGetStatus(AntragStatus status) {
        berechtigungsantrag.setStatus(status);
        Assertions.assertEquals(status, berechtigungsantrag.getStatus());
    }

    @ParameterizedTest
    @EnumSource(Rolle.class)
    public void testSetGetRolle(Rolle rolle) {
        berechtigungsantrag.setRolle(rolle);
        Assertions.assertEquals(rolle, berechtigungsantrag.getRolle());
    }

    @ParameterizedTest
    @EnumSource(Rechttype.class)
    public void testSetGetRechttype(Rechttype rechttype) {
        berechtigungsantrag.setRechttype(rechttype);
        Assertions.assertEquals(rechttype, berechtigungsantrag.getRechttype());
    }

}
