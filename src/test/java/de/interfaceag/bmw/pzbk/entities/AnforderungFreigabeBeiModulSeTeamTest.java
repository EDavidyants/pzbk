package de.interfaceag.bmw.pzbk.entities;

import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;

/**
 *
 * @author sl
 */
public class AnforderungFreigabeBeiModulSeTeamTest {

    AnforderungFreigabeBeiModulSeTeam freigabeBeiModulSeTeam;

    @BeforeEach
    public void setup() {
        freigabeBeiModulSeTeam = new AnforderungFreigabeBeiModulSeTeam();
    }

    @Test
    public void testToStringForNullValues() {
        String result = freigabeBeiModulSeTeam.toString();
        MatcherAssert.assertThat(result, is("Anforderung ID: null in SE-Team: null von null abgelehnt"));
    }

}
