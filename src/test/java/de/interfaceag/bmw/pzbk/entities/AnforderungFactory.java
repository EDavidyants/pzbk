package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.enums.Status;

import java.util.Date;

/**
 *
 * @author Christian Schauer
 */
public class AnforderungFactory {

    public static Anforderung createAnforderung(String supplement, boolean boolValue, String qNumberPrefix) {
        Anforderung a = new Anforderung();

        Date d1 = new Date(0);
        a.setAenderungsdatum(d1);
        a.addAnhang(Anhang.createTestAnhang(supplement, d1));
        a.addAuswirkung(Auswirkung.createTestAuswirkung(supplement));
        a.setBeschreibungAnforderungDe("BeschreibungAnforderung_" + supplement);
        a.setBeschreibungAnforderungEn("BeschreibungAnforderung_" + supplement);
        a.setBeschreibungStaerkeSchwaeche("BeschreibungCharakteristik_" + supplement);
        a.setStaerkeSchwaeche(boolValue);
        a.setErstellungsdatum(d1);

        //a.addFestgestelltIn(Fahrzeugprojekt.createTestFahrzeugprojekt(supplement));
        a.setPotentialStandardisierung(boolValue);
        a.setKommentarAnforderung("KommentarAnforderung_" + supplement);
        a.setLoesungsvorschlag("Loesungsvorschlag_" + supplement);
        a.setPhasenbezug(boolValue);
        a.setReferenzen("Referenzen_" + supplement);
        a.setSensor(Mitarbeiter.createTestAnwender("Sensor_" + supplement, qNumberPrefix + 2));
        a.setSensorCoc(SensorCoc.createTestSensorCoc("SensorCoc_" + supplement, qNumberPrefix + 3));
        a.setStatus(Status.getStatusByBezeichnung(supplement));
        a.setUmsetzenderBereich("UmsetzenderBereich_" + supplement);
        //a.addUmsetzer(Fahrzeugmodul.createTestFahrzeugmodul(supplement));
        a.setUrsache("Ursache_" + supplement);
        a.setZielwert(Zielwert.createTestZielwert(supplement));

        return a;
    }

}
