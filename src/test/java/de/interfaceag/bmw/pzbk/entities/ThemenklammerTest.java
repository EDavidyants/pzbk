package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.themenklammer.ThemenklammerDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class ThemenklammerTest {

    private static final long ID = 42L;
    private static final String BEZEICHNUNG = "Bezeichnung";

    private Themenklammer themenklammer;

    @BeforeEach
    void setUp() {
        themenklammer = new Themenklammer(BEZEICHNUNG);
        themenklammer.setId(ID);
    }

    @Test
    void getDtoIdValue() {
        final ThemenklammerDto dto = themenklammer.getDto();
        assertThat(dto.getId(), is(ID));
    }

    @Test
    void getDtoBezeichnungValue() {
        final ThemenklammerDto dto = themenklammer.getDto();
        assertThat(dto.getBezeichnung(), is(BEZEICHNUNG));
    }
}