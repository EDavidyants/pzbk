package de.interfaceag.bmw.pzbk.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class MitarbeiterToStringTest {

    private static final String VORNAME = "VORNAME";
    private static final String NACHNAME = "NACHNAME";
    private static final String ABTEILUNG = "ABTEILUNG";
    private static final String EXPECTED_TO_STRING = VORNAME + " " + NACHNAME + " | " + ABTEILUNG;
    private static final String EXPECTED_NAME = VORNAME + " " + NACHNAME;

    private Mitarbeiter mitarbeiter;

    @BeforeEach
    void setUp() {
        mitarbeiter = new Mitarbeiter();
        mitarbeiter.setVorname(VORNAME);
        mitarbeiter.setNachname(NACHNAME);
        mitarbeiter.setAbteilung(ABTEILUNG);
    }

    @Test
    void toString1() {
        final String result = mitarbeiter.toString();
        assertThat(result, is(EXPECTED_TO_STRING));
    }

    @Test
    void getFullName() {
        final String result = mitarbeiter.getFullName();
        assertThat(result, is(EXPECTED_NAME));
    }

    @Test
    void getName() {
        final String result = mitarbeiter.getName();
        assertThat(result, is(EXPECTED_NAME));
    }
}