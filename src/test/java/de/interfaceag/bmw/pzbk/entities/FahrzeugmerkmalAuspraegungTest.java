package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.shared.objectIds.FahrzeugmerkmalAuspraegungId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class FahrzeugmerkmalAuspraegungTest {

    private static final String AUSPRAEGUNG = "auspraegung";
    private static final long ID = 42L;

    private FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung;
    private FahrzeugmerkmalAuspraegung anotherFahrzeugmerkmalAuspraegung;

    @Mock
    private Fahrzeugmerkmal fahrzeugmerkmal;
    @Mock
    private Fahrzeugmerkmal anotherFahrzeugmerkmal;

    @BeforeEach
    void setUp() {
        fahrzeugmerkmalAuspraegung = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, AUSPRAEGUNG);
    }

    @Test
    void toString1() {
        final String result = fahrzeugmerkmalAuspraegung.toString();
        assertThat(result, is("FahrzeugmerkmalAuspraegung{id=null, fahrzeugmerkmal=fahrzeugmerkmal, auspraegung='auspraegung'}"));
    }

    @Test
    void equalsIdNotEqual() {
        fahrzeugmerkmalAuspraegung.setId(ID);
        anotherFahrzeugmerkmalAuspraegung = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, AUSPRAEGUNG);
        anotherFahrzeugmerkmal.setId(43L);

        final boolean result = fahrzeugmerkmalAuspraegung.equals(anotherFahrzeugmerkmalAuspraegung);
        assertFalse(result);
    }

    @Test
    void equalsFahrzeugmerkmalNotEqual() {
        fahrzeugmerkmalAuspraegung.setId(ID);
        anotherFahrzeugmerkmalAuspraegung = new FahrzeugmerkmalAuspraegung(anotherFahrzeugmerkmal, AUSPRAEGUNG);
        anotherFahrzeugmerkmal.setId(ID);

        final boolean result = fahrzeugmerkmalAuspraegung.equals(anotherFahrzeugmerkmalAuspraegung);
        assertFalse(result);
    }

    @Test
    void equalsAuspraegungNotEqual() {
        fahrzeugmerkmalAuspraegung.setId(ID);
        anotherFahrzeugmerkmalAuspraegung = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, AUSPRAEGUNG + AUSPRAEGUNG);
        anotherFahrzeugmerkmal.setId(ID);

        final boolean result = fahrzeugmerkmalAuspraegung.equals(anotherFahrzeugmerkmalAuspraegung);
        assertFalse(result);
    }

    @Test
    void equalsEqual() {
        fahrzeugmerkmalAuspraegung.setId(ID);
        anotherFahrzeugmerkmalAuspraegung = new FahrzeugmerkmalAuspraegung(fahrzeugmerkmal, AUSPRAEGUNG);
        anotherFahrzeugmerkmalAuspraegung.setId(ID);

        final boolean result = fahrzeugmerkmalAuspraegung.equals(anotherFahrzeugmerkmalAuspraegung);
        assertTrue(result);
    }

    @Test
    void equalsNull() {
        final boolean result = fahrzeugmerkmalAuspraegung.equals(null);
        assertFalse(result);
    }

    @Test
    void equalsAnotherObject() {
        final boolean result = fahrzeugmerkmalAuspraegung.equals(new Object());
        assertFalse(result);
    }

    @Test
    void getId() {
        fahrzeugmerkmalAuspraegung.setId(ID);
        final Long id = fahrzeugmerkmalAuspraegung.getId();
        assertThat(id, is(ID));
    }

    @Test
    void getFahrzeugmerkmalAuspraegungId() {
        fahrzeugmerkmalAuspraegung.setId(ID);
        final FahrzeugmerkmalAuspraegungId fahrzeugmerkmalAuspraegungId = fahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegungId();
        assertThat(fahrzeugmerkmalAuspraegungId, is(new FahrzeugmerkmalAuspraegungId(ID)));
    }

    @Test
    void getFahrzeugmerkmal() {
        final Fahrzeugmerkmal fahrzeugmerkmalAuspraegungFahrzeugmerkmal = fahrzeugmerkmalAuspraegung.getFahrzeugmerkmal();
        assertThat(fahrzeugmerkmalAuspraegungFahrzeugmerkmal, is(fahrzeugmerkmal));
    }

    @Test
    void setFahrzeugmerkmal() {
        fahrzeugmerkmalAuspraegung.setFahrzeugmerkmal(anotherFahrzeugmerkmal);
        final Fahrzeugmerkmal fahrzeugmerkmalAuspraegungFahrzeugmerkmal = fahrzeugmerkmalAuspraegung.getFahrzeugmerkmal();
        assertThat(fahrzeugmerkmalAuspraegungFahrzeugmerkmal, is(anotherFahrzeugmerkmal));
    }

    @Test
    void getAuspraegung() {
        final String auspraegung = fahrzeugmerkmalAuspraegung.getAuspraegung();
        assertThat(auspraegung, is(AUSPRAEGUNG));
    }

    @Test
    void setAuspraegung() {
        fahrzeugmerkmalAuspraegung.setAuspraegung("Stefan war hier");
        final String auspraegung = fahrzeugmerkmalAuspraegung.getAuspraegung();
        assertThat(auspraegung, is("Stefan war hier"));
    }
}