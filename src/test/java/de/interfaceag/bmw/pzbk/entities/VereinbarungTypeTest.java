package de.interfaceag.bmw.pzbk.entities;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class VereinbarungTypeTest {

    @Test
    public void toStringTest() {
        assertEquals(VereinbarungType.STANDARD.getLabel(), VereinbarungType.STANDARD.toString());
    }

    @Test
    public void getLabelStandardTest() {
        assertEquals("STD", VereinbarungType.STANDARD.getLabel());
    }

    @Test
    public void getLabelZakTest() {
        assertEquals("in ZAK", VereinbarungType.ZAK.getLabel());
    }

    @Test
    public void getIdStandardTest() {
        assertEquals(0, VereinbarungType.STANDARD.getId());
    }

    @Test
    public void getIdZakTest() {
        assertEquals(1, VereinbarungType.ZAK.getId());
    }

    @Test
    public void isZakForZakTest() {
        assertTrue(VereinbarungType.ZAK.isZak());
    }

    @Test
    public void isZakForStandardTest() {
        assertFalse(VereinbarungType.STANDARD.isZak());
    }

    @Test
    public void getById0Test() {
        VereinbarungType result = VereinbarungType.getById(0);
        assertEquals(VereinbarungType.STANDARD, result);
    }

    @Test
    public void getById1Test() {
        VereinbarungType result = VereinbarungType.getById(1);
        assertEquals(VereinbarungType.ZAK, result);
    }

    @Test
    public void getById2Test() {
        VereinbarungType result = VereinbarungType.getById(2);
        assertEquals(VereinbarungType.PZBK, result);
    }


    @Test
    public void getById3Test() {
        VereinbarungType result = VereinbarungType.getById(3);
        assertEquals(null, result);
    }

    @Test
    public void getVereinbarungTypeForZAKTrueTest() {
        VereinbarungType result = VereinbarungType.getVereinbarungTypeForZAK(true);
        assertEquals(VereinbarungType.ZAK, result);
    }

    @Test
    public void getVereinbarungTypeForZAKFalseTest() {
        VereinbarungType result = VereinbarungType.getVereinbarungTypeForZAK(false);
        assertEquals(VereinbarungType.STANDARD, result);
    }
}
