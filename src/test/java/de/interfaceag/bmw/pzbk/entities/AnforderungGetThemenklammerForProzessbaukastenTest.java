package de.interfaceag.bmw.pzbk.entities;

import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.IsIterableContaining.hasItem;
import static org.hamcrest.core.IsIterableContaining.hasItems;

@ExtendWith(MockitoExtension.class)
class AnforderungGetThemenklammerForProzessbaukastenTest {

    @Mock
    private Prozessbaukasten prozessbaukasten1;
    @Mock
    private Prozessbaukasten prozessbaukasten2;
    @Mock
    private Themenklammer themenklammer1;
    @Mock
    private Themenklammer themenklammer2;

    ProzessbaukastenThemenklammer prozessbaukastenThemenklammer;

    private Anforderung anforderung;

    @BeforeEach
    void setUp() {
        prozessbaukastenThemenklammer = new ProzessbaukastenThemenklammer(prozessbaukasten1, themenklammer1);
        ProzessbaukastenThemenklammer prozessbaukastenThemenklammer2 = new ProzessbaukastenThemenklammer(prozessbaukasten1, themenklammer2);
        ProzessbaukastenThemenklammer prozessbaukastenThemenklammer3 = new ProzessbaukastenThemenklammer(prozessbaukasten2, themenklammer2);

        anforderung = new Anforderung();
        anforderung.addProzessbaukastenThemenklammer(prozessbaukastenThemenklammer);
        anforderung.addProzessbaukastenThemenklammer(prozessbaukastenThemenklammer2);
        anforderung.addProzessbaukastenThemenklammer(prozessbaukastenThemenklammer3);
    }

    @Test
    void getThemenklammernForNullResultSize() {
        final List<Themenklammer> themenklammern = anforderung.getThemenklammernForProzessbaukasten(null);
        assertThat(themenklammern, empty());
    }

    @Test
    void getThemenklammernForProzessbaukasten1ResultSize() {
        final List<Themenklammer> themenklammern = anforderung.getThemenklammernForProzessbaukasten(prozessbaukasten1);
        assertThat(themenklammern, hasSize(2));
    }

    @Test
    void getThemenklammernForProzessbaukasten2ResultSize() {
        final List<Themenklammer> themenklammern = anforderung.getThemenklammernForProzessbaukasten(prozessbaukasten2);
        assertThat(themenklammern, hasSize(1));
    }

    @Test
    void getThemenklammernForProzessbaukasten1ResultValue() {
        final List<Themenklammer> themenklammern = anforderung.getThemenklammernForProzessbaukasten(prozessbaukasten1);
        assertThat(themenklammern, hasItems(themenklammer1, themenklammer2));
    }

    @Test
    void getThemenklammernForProzessbaukasten2ResultValue() {
        final List<Themenklammer> themenklammern = anforderung.getThemenklammernForProzessbaukasten(prozessbaukasten1);
        assertThat(themenklammern, hasItem(themenklammer2));
    }

    @Test
    void addProzessbaukastenThemenklammer() {
        anforderung.getProzessbaukastenThemenklammern().clear();
        anforderung.addProzessbaukastenThemenklammer(prozessbaukastenThemenklammer);
        assertThat(anforderung.getProzessbaukastenThemenklammern(), IsCollectionWithSize.hasSize(1));
    }

    @Test
    void removeProzessbaukastenThemenklammer() {
        anforderung.removeProzessbaukastenThemenklammer(prozessbaukastenThemenklammer);
        assertThat(anforderung.getProzessbaukastenThemenklammern(), IsCollectionWithSize.hasSize(2));
    }
}