package de.interfaceag.bmw.pzbk.entities;

import de.interfaceag.bmw.pzbk.services.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DerivatFahrzeugmerkmalTest {

    @Test
    void testEqualsForId() {
        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal1 = new DerivatFahrzeugmerkmal();
        derivatFahrzeugmerkmal1.setId(1L);
        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal2 = new DerivatFahrzeugmerkmal();
        derivatFahrzeugmerkmal2.setId(2L);

        Assertions.assertNotEquals(derivatFahrzeugmerkmal1, derivatFahrzeugmerkmal2);
    }

    @Test
    void testEqualsForDerivat() {
        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal1 = new DerivatFahrzeugmerkmal();
        derivatFahrzeugmerkmal1.setDerivat(TestDataFactory.generateDerivat("Derivat1", "Beschreibung1", "Produktlinie 1"));
        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal2 = new DerivatFahrzeugmerkmal();
        derivatFahrzeugmerkmal2.setDerivat(TestDataFactory.generateDerivat("Derivat2", "Beschreibung2", "Produktlinie 2"));

        Assertions.assertNotEquals(derivatFahrzeugmerkmal1, derivatFahrzeugmerkmal2);
    }

    @Test
    void testEqualsForFahrzeugMerkmal() {
        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal1 = new DerivatFahrzeugmerkmal();
        derivatFahrzeugmerkmal1.setFahrzeugmerkmal(TestDataFactory.generateFahrzeugmerkmal("Merkmal 1"));
        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal2 = new DerivatFahrzeugmerkmal();
        derivatFahrzeugmerkmal2.setFahrzeugmerkmal(TestDataFactory.generateFahrzeugmerkmal("Merkmal 2"));

        Assertions.assertNotEquals(derivatFahrzeugmerkmal1, derivatFahrzeugmerkmal2);
    }

    @Test
    void testEqualsForRelevant() {
        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal1 = new DerivatFahrzeugmerkmal();
        derivatFahrzeugmerkmal1.setNotRelevant(true);
        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal2 = new DerivatFahrzeugmerkmal();
        derivatFahrzeugmerkmal2.setNotRelevant(false);

        Assertions.assertNotEquals(derivatFahrzeugmerkmal1, derivatFahrzeugmerkmal2);
    }

    @Test
    void testEqualsForListAuspraegung() {
        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal1 = new DerivatFahrzeugmerkmal();
        derivatFahrzeugmerkmal1.setAuspraegungen(TestDataFactory.generateAuspraegungen(1));
        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal2 = new DerivatFahrzeugmerkmal();
        derivatFahrzeugmerkmal2.setAuspraegungen(TestDataFactory.generateAuspraegungen(2));

        Assertions.assertNotEquals(derivatFahrzeugmerkmal1, derivatFahrzeugmerkmal2);
    }

    @Test
    void testToString() {
        DerivatFahrzeugmerkmal derivatFahrzeugmerkmal = new DerivatFahrzeugmerkmal();
        derivatFahrzeugmerkmal.setId(1L);
        derivatFahrzeugmerkmal.setAuspraegungen(TestDataFactory.generateAuspraegungen(2));
        derivatFahrzeugmerkmal.setDerivat(TestDataFactory.generateDerivat());
        derivatFahrzeugmerkmal.setNotRelevant(false);
        derivatFahrzeugmerkmal.setFahrzeugmerkmal(TestDataFactory.generateFahrzeugmerkmal("Merkmal1"));
        String expacted = "DerivatFahrzeugmerkmal{id=1," +
                " derivat= Derivat: E46, Bezeichnung: Bezeichnung, " +
                "fahrzeugmerkmal=Fahrzeugmerkmal{id=null, merkmal='Merkmal1'}, " +
                "auspraegungen=[FahrzeugmerkmalAuspraegung{id=null, fahrzeugmerkmal=Fahrzeugmerkmal{id=null, merkmal='Merkmal1'}, auspraegung='Auspraegung0'}, " +
                "FahrzeugmerkmalAuspraegung{id=null, fahrzeugmerkmal=Fahrzeugmerkmal{id=null, merkmal='Merkmal1'}, auspraegung='Auspraegung1'}], " +
                "isNotRelevant=false}";
        Assertions.assertEquals(expacted, derivatFahrzeugmerkmal.toString());
    }


}