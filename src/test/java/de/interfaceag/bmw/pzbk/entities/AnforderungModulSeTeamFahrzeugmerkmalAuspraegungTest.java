package de.interfaceag.bmw.pzbk.entities;

import org.assertj.core.api.Java6Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.nullValue;

@ExtendWith(MockitoExtension.class)
class AnforderungModulSeTeamFahrzeugmerkmalAuspraegungTest {

    private AnforderungModulSeTeamFahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegung;

    @Mock
    private Anforderung anforderung;
    @Mock
    private ModulSeTeam modulSeTeam;
    @Mock
    private FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung;

    @BeforeEach
    void setUp() {
        anforderungModulSeTeamFahrzeugmerkmalAuspraegung = new AnforderungModulSeTeamFahrzeugmerkmalAuspraegung(anforderung, modulSeTeam, fahrzeugmerkmalAuspraegung);
    }

    @Test
    void defaultConstructorValues() {
        anforderungModulSeTeamFahrzeugmerkmalAuspraegung = new AnforderungModulSeTeamFahrzeugmerkmalAuspraegung();

        final Long id = anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getId();
        assertThat(id, nullValue());

        final Anforderung fahrzeugmerkmalAuspraegungAnforderung = anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getAnforderung();
        assertThat(fahrzeugmerkmalAuspraegungAnforderung, nullValue());

        final ModulSeTeam fahrzeugmerkmalAuspraegungModulSeTeam = anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getModulSeTeam();
        assertThat(fahrzeugmerkmalAuspraegungModulSeTeam, nullValue());

        final FahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegungFahrzeugmerkmalAuspraegung = anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegung();
        assertThat(anforderungModulSeTeamFahrzeugmerkmalAuspraegungFahrzeugmerkmalAuspraegung, nullValue());
    }


    @Test
    void toString1() {
        anforderungModulSeTeamFahrzeugmerkmalAuspraegung.setId(42L);
        final String result = anforderungModulSeTeamFahrzeugmerkmalAuspraegung.toString();
        assertThat(result, is("AnforderungModulSeTeamFahrzeugmerkmalAuspraegung{id=42, anforderung=anforderung, modulSeTeam=modulSeTeam, fahrzeugmerkmalAuspraegung=fahrzeugmerkmalAuspraegung}"));
    }

    @Test
    void getId() {
        anforderungModulSeTeamFahrzeugmerkmalAuspraegung.setId(42L);
        final Long id = anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getId();
        assertThat(id, is(42L));
    }

    @Test
    void getAnforderung() {
        final Anforderung fahrzeugmerkmalAuspraegungAnforderung = anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getAnforderung();
        assertThat(fahrzeugmerkmalAuspraegungAnforderung, is(anforderung));
    }

    @Test
    void setAnforderung() {
        anforderungModulSeTeamFahrzeugmerkmalAuspraegung.setAnforderung(null);
        final Anforderung fahrzeugmerkmalAuspraegungAnforderung = anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getAnforderung();
        assertThat(fahrzeugmerkmalAuspraegungAnforderung, nullValue());
    }

    @Test
    void getModulSeTeam() {
        final ModulSeTeam fahrzeugmerkmalAuspraegungModulSeTeam = anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getModulSeTeam();
        assertThat(fahrzeugmerkmalAuspraegungModulSeTeam, is(modulSeTeam));
    }

    @Test
    void setModulSeTeam() {
        anforderungModulSeTeamFahrzeugmerkmalAuspraegung.setModulSeTeam(null);
        final ModulSeTeam fahrzeugmerkmalAuspraegungModulSeTeam = anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getModulSeTeam();
        assertThat(fahrzeugmerkmalAuspraegungModulSeTeam, nullValue());
    }

    @Test
    void getFahrzeugmerkmalAuspraegung() {
        final FahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegungFahrzeugmerkmalAuspraegung = anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegung();
        assertThat(anforderungModulSeTeamFahrzeugmerkmalAuspraegungFahrzeugmerkmalAuspraegung, is(fahrzeugmerkmalAuspraegung));
    }

    @Test
    void setFahrzeugmerkmalAuspraegung() {
        anforderungModulSeTeamFahrzeugmerkmalAuspraegung.setFahrzeugmerkmalAuspraegung(null);
        final FahrzeugmerkmalAuspraegung anforderungModulSeTeamFahrzeugmerkmalAuspraegungFahrzeugmerkmalAuspraegung = anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getFahrzeugmerkmalAuspraegung();
        assertThat(anforderungModulSeTeamFahrzeugmerkmalAuspraegungFahrzeugmerkmalAuspraegung, nullValue());
    }

    @Test
    void getAttributeStringForHistory() {
        ModulSeTeam modulSeTeam = new ModulSeTeam();
        modulSeTeam.setName("ModulSeTeam singt manchmal Karaoke");

        FahrzeugmerkmalAuspraegung fahrzeugmerkmalAuspraegung = new FahrzeugmerkmalAuspraegung();
        fahrzeugmerkmalAuspraegung.setFahrzeugmerkmal(new Fahrzeugmerkmal("Das Merkmal namens Jürgen"));

        anforderungModulSeTeamFahrzeugmerkmalAuspraegung.setModulSeTeam(modulSeTeam);
        anforderungModulSeTeamFahrzeugmerkmalAuspraegung.setFahrzeugmerkmalAuspraegung(fahrzeugmerkmalAuspraegung);
        Java6Assertions.assertThat(anforderungModulSeTeamFahrzeugmerkmalAuspraegung.getAttributeStringForHistory()).contains("ModulSeTeam singt manchmal Karaoke - Fahrzeugmerkmal:Das Merkmal namens Jürgen");
    }
}