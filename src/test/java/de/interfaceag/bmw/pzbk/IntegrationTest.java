package de.interfaceag.bmw.pzbk;

import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;

import javax.persistence.EntityManager;

public abstract class IntegrationTest {

    @Rule
    private EntityManagerProvider entityManagerProvider = EntityManagerProvider.forIntegrationTests();

    private EntityManager entityManager = entityManagerProvider.getEntityManager();

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void begin() {
        entityManagerProvider.begin();
    }

    public void commit() {
        entityManagerProvider.commit();
    }

    @AfterEach
    protected void restoreDatabase() {
        restartTransaction();
        getEntityManager().createNativeQuery("SET REFERENTIAL_INTEGRITY FALSE").executeUpdate();
        restartTransaction();
        getEntityManager().createNativeQuery("TRUNCATE TABLE anforderung").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE anforderung_anforderung_freigabe_bei_modul").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE anforderung_anhang").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE anforderung_auswirkung").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE anforderung_detektor").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE anforderung_festgestelltin").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE anforderung_freigabe_bei_modul").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE anforderung_history").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE anforderung_referenz_system_link").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE anforderung_umsetzer").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE anhang").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE auswirkung").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE berechtigung").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE berechtigung_derivat").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE berechtigunghistory").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE bild").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE dbfile").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE derivat").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE derivatanforderungmodul").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE derivatanforderungmodulhistory").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE fahrzeugmerkmal").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE fahrzeugmerkmalauspraegung").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE festgestelltin").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE infotext").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE konzept").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE kov_a_phase_im_derivat").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE kovaphase").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE localgroup").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE meldung").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE meldung_anforderung").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE meldung_anhang").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE meldung_auswirkung").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE meldung_detektor").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE meldung_festgestelltin").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE mitarbeiter").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE mitarbeiter_selectedderivate").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE mitarbeiter_selectedzakderivate").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE modul").executeUpdate();

        restartTransaction();

        getEntityManager().createNativeQuery("TRUNCATE TABLE modul_modulseteam").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE modulbild").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE modulkomponente").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE modulseteam").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE modulseteam_modulkomponente").executeUpdate();
//        getEntityManager().createNativeQuery("TRUNCATE TABLE projectprocesssnapshot").executeUpdate();
//        getEntityManager().createNativeQuery("TRUNCATE TABLE projectprocesssnapshot_projectprocesssnapshotentry").executeUpdate();
//        getEntityManager().createNativeQuery("TRUNCATE TABLE projectprocesssnapshotentry").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE prozessbaukasten").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE prozessbaukasten_anforderung").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE prozessbaukasten_anhang").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE prozessbaukasten_history").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE prozessbaukasten_konzept").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE prozessbaukastenanforderungkonzept").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE prozessbaukasten_history").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE referenz_system_link").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE search_filter").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE searchfilter_longlistvalue").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE searchfilter_stringlistvalue").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE searchfilter_statuslistvalue").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE sensorcoc").executeUpdate();
//        getEntityManager().createNativeQuery("TRUNCATE TABLE statusabgleichsnapshot").executeUpdate();
//        getEntityManager().createNativeQuery("TRUNCATE TABLE statusabgleichsnapshot_statusabgleichsnapshotentry").executeUpdate();
//        getEntityManager().createNativeQuery("TRUNCATE TABLE statusabgleichsnapshotentry").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE t_role_group").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE t_role_user").executeUpdate();

        restartTransaction();

        getEntityManager().createNativeQuery("TRUNCATE TABLE t_team").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE t_users").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE themenklammer").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE tteammitglied").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE tteamvertreter").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE ubzukovaphaseimderivatundsensorcoc").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE ubzukovaphaseimderivatundsensorcoc_mitarbeiter").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE umsetzer").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE umsetzungsbestaetigung").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE urlencoding").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE user_defined_search").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE user_defined_search_search_filter").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE user_to_group").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE werk").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE wert").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE zakuebertragung").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE zuordnunganforderungderivat").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE reportingstatustransition").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE reportingmeldungstatustransition").executeUpdate();
        getEntityManager().createNativeQuery("TRUNCATE TABLE berechtigungsantrag").executeUpdate();
        getEntityManager().createNativeQuery("SET REFERENTIAL_INTEGRITY TRUE").executeUpdate();
        commit();
    }

    private void restartTransaction() {
        commit();
        begin();
    }
}
