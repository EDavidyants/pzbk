package de.interfaceag.bmw.pzbk.admin.terminal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

/**
 *
 * @author sl (stefan.luchs@interface-ag.de)
 */
public class AdminTerminalUtilsTest {

    @Test
    void getCommandIsPresent() {
        Optional<AdminTerminalCommand> command = AdminTerminalUtils.getCommand("help");
        Assertions.assertTrue(command.isPresent());
    }

    @Test
    void getCommandNull() {
        Optional<AdminTerminalCommand> command = AdminTerminalUtils.getCommand(null);
        Assertions.assertFalse(command.isPresent());
    }

    @Test
    void getCommandEquals() {
        Optional<AdminTerminalCommand> command = AdminTerminalUtils.getCommand("help");
        Assertions.assertEquals(AdminTerminalCommand.HELP, command.get());
    }

    @Test
    void getCommandEqualsTrim() {
        Optional<AdminTerminalCommand> command = AdminTerminalUtils.getCommand(" help ");
        Assertions.assertEquals(AdminTerminalCommand.HELP, command.get());
    }

    @Test
    void getCommandEqualsCase() {
        Optional<AdminTerminalCommand> command = AdminTerminalUtils.getCommand("HeLp");
        Assertions.assertEquals(AdminTerminalCommand.HELP, command.get());
    }

    @Test
    void verifyParamCountNull() {
        Assertions.assertFalse(AdminTerminalUtils.verifyParamCount(null, null));
    }

}
